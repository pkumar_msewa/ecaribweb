package com.travelkhana.util;

import com.payqwikweb.app.metadatas.UrlMetadatas;

public class TravelkhanaUtil {
	
	private static final String BASE_URL_TEST = "https://stage.travelkhana.com/api/";// TEST URL
	private static final String BASE_URL_LIVE = "https://beta.travelkhana.com/api/";// LIVE URL
	public static final String  USER = "User-Agent";
	public static final String  VALUE = "Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36";
	public static final String Travelkhana_Username = getTKUserName();
	public static final String Travelkhana_Password = getTKPassword();
	
	public static final String Travelkhana_Username_TEST = "vpayqwik";
	public static final String Travelkhana_Password_TEST = "a!MjzXBjka49nx0";
	public static final String Travelkhana_Username_LIVE = "vpayqwik";
	public static final String Travelkhana_Password_LIVE = "a!MjzXBjka49nx0";
	
	public static final String trainList = getTKUrl()+"static/trainsList";
	public static final String stationList = getTKUrl()+"static/stationNameByTrain/";
	public static final String menuInTime =  getTKUrl()+"orderBooking/menuInTime/";
	public static final String trainRoutesMenu =  getTKUrl()+"orderBooking/trainRoutesNew?station=";
	public static final String outletInTime =  getTKUrl()+"orderBooking/outletInTime?arrivalTime=";
	public static final String menuPriceCal =  getTKUrl()+"orderBooking/menuPriceCalculation";
	public static final String trackOrder  =   getTKUrl()+"orderBooking/trackUserOrder?orderId=";
	public static final String placeOrder = getTKUrl()+"order";
	public static final String applyCoupon = getTKUrl()+"applycoupon/";
	
	
	 public static String getTKUrl(){
	    	if(UrlMetadatas.PRODUCTION){
	    		return BASE_URL_LIVE;
	    	}else{
	    		return BASE_URL_TEST;
	    	}
	    }
	 
	 public static String getTKUserName(){
	    	if(UrlMetadatas.PRODUCTION){
	    		return Travelkhana_Username_LIVE;
	    	}else{
	    		return Travelkhana_Username_TEST;
	    	}
	    }
	 
	 public static String getTKPassword(){
	    	if(UrlMetadatas.PRODUCTION){
	    		return Travelkhana_Password_LIVE;
	    	}else{
	    		return Travelkhana_Password_TEST;
	    	}
	    }
}
