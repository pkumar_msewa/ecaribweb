package com.aadhar.model;

public class AadharOTPResponse {

	private String code;
	private boolean success;
	private String message;
	private String otpRequestId;
	
	public String getOtpRequestId() {
		return otpRequestId;
	}

	public void setOtpRequestId(String otpRequestId) {
		this.otpRequestId = otpRequestId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
