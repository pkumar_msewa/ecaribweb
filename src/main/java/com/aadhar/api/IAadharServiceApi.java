package com.aadhar.api;

import com.aadhar.model.AadharOTPDTO;
import com.aadhar.model.AadharOTPResponse;
import com.aadhar.model.AadharOTPValidateDTO;
import com.aadhar.model.AadharOTPValidateResponse;
import com.aadhar.model.UserOTPDTO;
import com.aadhar.model.UserOTPResponse;

public interface IAadharServiceApi {

	AadharOTPResponse getAadharOtp(AadharOTPDTO aadharOtp);
	
	AadharOTPValidateResponse validateAadharOtp(AadharOTPValidateDTO aadharOtp);
	
	AadharOTPValidateResponse saveAadhar(AadharOTPValidateDTO dto);

	UserOTPResponse getUserOTP(UserOTPDTO dto);
	
	UserOTPResponse getUserOTPValidate(UserOTPDTO dto);

}
