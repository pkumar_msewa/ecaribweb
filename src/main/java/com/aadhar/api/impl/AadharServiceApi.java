package com.aadhar.api.impl;

import org.codehaus.jettison.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import com.aadhar.api.IAadharServiceApi;
import com.aadhar.model.AadharOTPDTO;
import com.aadhar.model.AadharOTPResponse;
import com.aadhar.model.AadharOTPValidateDTO;
import com.aadhar.model.AadharOTPValidateResponse;
import com.aadhar.model.UserOTPDTO;
import com.aadhar.model.UserOTPResponse;
import com.aadhar.util.AadharUtil;
import com.payqwikweb.app.metadatas.UrlMetadatas;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Language;
import com.payqwikweb.app.model.ResponseStatus;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.Version;
import com.payqwikweb.app.utils.SecurityUtils;
import com.payqwikweb.util.JSONParserUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;

public class AadharServiceApi implements IAadharServiceApi {

	@Override
	public AadharOTPResponse getAadharOtp(AadharOTPDTO dto) {
		AadharOTPResponse result = new AadharOTPResponse();
		try {
			Client client = Client.create();
			
			WebResource webResource = client.resource(AadharUtil.getAadharServicesUrl()).queryParam("reqtype", AadharUtil.REQ_TYPE)
					.queryParam("aadhar", dto.getAadharNumber());
			ClientResponse resp = webResource.get(ClientResponse.class);
			String strResponse = resp.getEntity(String.class);
			if (resp.getStatus() == 200) {
				 if (strResponse != null) {
					Document html = Jsoup.parse(strResponse);
					String aadharResponse = html.body().text();
					String parse=aadharResponse.replace("|", ",");
					String[] aadharCode=parse.split(",");
					if(aadharCode[0].equalsIgnoreCase("000")){
						result.setCode("S00");
						result.setSuccess(true);
						result.setOtpRequestId(aadharCode[1]);
						result.setMessage(aadharCode[aadharCode.length-1]);
					}else if(aadharCode[0].equalsIgnoreCase("001")){
						if(aadharCode[1].equalsIgnoreCase("111")){
							result.setCode("F00");
							result.setSuccess(false);
							result.setMessage(aadharCode[2]);
						}else{
							result.setCode("F00");
							result.setSuccess(false);
							if(aadharCode.length>2)
							result.setMessage(aadharCode[2]);
						}
					}else if(aadharCode[0].equalsIgnoreCase("002")){
						result.setCode("F00");
						result.setSuccess(false);
						result.setMessage("Technical Problem , Please Try After Sometime.");
					}
				 }
			 } else {
				result.setSuccess(false);
				result.setMessage("Response from Aadhar Api is " + resp.getStatus());
		  }
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public AadharOTPValidateResponse validateAadharOtp(AadharOTPValidateDTO dto) {
		AadharOTPValidateResponse result = new AadharOTPValidateResponse();
		try {
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(AadharUtil.getAadharServicesUrl()).queryParam("reqtype", AadharUtil.OTP_VALIDATE)
					.queryParam("aadhar", dto.getAadharNumber())
					.queryParam("otp", dto.getKey())
					.queryParam("otpreqid", dto.getOtpreqId());
			ClientResponse resp = webResource.get(ClientResponse.class);
			String strResponse = resp.getEntity(String.class);
			if (resp.getStatus() == 200) {
				if (strResponse != null) {
					Document html = Jsoup.parse(strResponse);
					String aadharResponse = html.body().text();
					String parse=aadharResponse.replace("|", ";");
					String[] aadharCode=parse.split(";");
					if(aadharCode[0].equalsIgnoreCase("000") && aadharCode[1].equalsIgnoreCase("Y")){
						result.setCode("S00");
						result.setSuccess(true);
						result.setMessage(aadharCode[2]);
						result.setCustomerName(aadharCode[3]);
						result.setCustomerAddress(aadharCode[4]);
					}else if(aadharCode[0].equalsIgnoreCase("001") && aadharCode[1].equalsIgnoreCase("N")){
						result.setCode("F00");
						result.setSuccess(false);
						result.setMessage(aadharCode[2]);
					}else if(aadharCode[0].equalsIgnoreCase("002")){
						result.setCode("F03");
						result.setSuccess(false);
						result.setMessage(aadharCode[2]);
					}else{
						result.setSuccess(false);
						result.setMessage("service is down for maintance") ;
					}
				}
			} else {
				result.setSuccess(false);
				result.setMessage("Response from Aadhar Api is " + resp.getStatus());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	 }
	
	@Override
	public AadharOTPValidateResponse saveAadhar(AadharOTPValidateDTO dto) {
		AadharOTPValidateResponse resp = new AadharOTPValidateResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", dto.getSessionId());
			payload.put("aadharNumber", dto.getAadharNumber());
			payload.put("customerName", dto.getCustomerName());
			payload.put("customerAddress", dto.getCustomerAddress());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(
					UrlMetadatas.saveAadharUrl(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
							org.json.JSONObject details = jobj.getJSONObject("details");
							resp.setCustomerName(JSONParserUtil.getString(details, "name"));
							resp.setCustomerAddress(JSONParserUtil.getString(details, "address"));
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setMessage(message);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
		}
		return resp;
	}
	
	
	@Override
	public UserOTPResponse getUserOTP(UserOTPDTO dto) {
		UserOTPResponse resp = new UserOTPResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", dto.getSessionId());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(
					UrlMetadatas.getUserOTPUrl(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setMessage(message);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
		}
		return resp;
	}
	
	
	@Override
	public UserOTPResponse getUserOTPValidate(UserOTPDTO dto) {
		UserOTPResponse resp = new UserOTPResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", dto.getSessionId());
			payload.put("userOTP", dto.getUserOTP());	
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(
					UrlMetadatas.getUserOTPValidateUrl(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setMessage(message);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
		}
		return resp;
	}


}
