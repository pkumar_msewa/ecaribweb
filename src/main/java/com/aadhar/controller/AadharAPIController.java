package com.aadhar.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.aadhar.api.IAadharServiceApi;
import com.aadhar.model.AadharOTPDTO;
import com.aadhar.model.AadharOTPResponse;
import com.aadhar.model.AadharOTPValidateDTO;
import com.aadhar.model.AadharOTPValidateResponse;
import com.aadhar.model.UserOTPDTO;
import com.aadhar.model.UserOTPResponse;
import com.aadhar.util.AadharUtil;
import com.payqwikweb.api.IAuthenticationApi;
import com.payqwikweb.app.api.IUserApi;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.util.AES;
import com.payqwikweb.util.Authorities;

@Controller
@RequestMapping("/Api/{version}/{role}/{device}/{language}/AadharService")
public class AadharAPIController {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private MessageSource messageSource;
	 

	private IUserApi userApi;
	private IAadharServiceApi aadharServiceApi;
	private final IAuthenticationApi authenticationApi;

	public AadharAPIController(IUserApi userApi, IAadharServiceApi aadharServiceApi,
			IAuthenticationApi authenticationApi) {
		this.userApi = userApi;
		this.aadharServiceApi = aadharServiceApi;
		this.authenticationApi = authenticationApi;

	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	@RequestMapping(value = "/GetUserOTP", method = RequestMethod.POST)
	public ResponseEntity<UserOTPResponse> getUserOTP(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody UserOTPDTO dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws Exception {

		UserOTPResponse resp = new UserOTPResponse();
		try {
			String decryptData = AES.decrypt(dto.getEncryptData());
			System.err.println("decrypt data ::" + decryptData);
			UserOTPDTO aadharRequest = AadharUtil.prepareUserOtpRequest(decryptData);
			if (version.equalsIgnoreCase("v1")) {
				if (role.equalsIgnoreCase("User")) {
					if (device.equalsIgnoreCase("Android") || device.equalsIgnoreCase("IOS")) {
						if (language.equalsIgnoreCase("EN")) {
							resp = aadharServiceApi.getUserOTP(aadharRequest);
						} else {
							resp.setSuccess(false);
							resp.setMessage("Only EN locale available");
						}
					} else {
						resp.setSuccess(false);
						resp.setMessage("Unauthorized Device");
					}
				} else {
					resp.setSuccess(false);
					resp.setMessage("Unauthorized Role");
				}
			} else {
				resp.setSuccess(false);
				resp.setMessage("Invalid Version");
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setMessage("We are sorry for inconvenience, Please try again later .");
		}
		return new ResponseEntity<>(resp, HttpStatus.OK);
	}

	@RequestMapping(value = "/GetUserOTPValidate", method = RequestMethod.POST)
	public ResponseEntity<UserOTPResponse> getUserOTPValidate(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody UserOTPDTO dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws Exception {

		UserOTPResponse resp = new UserOTPResponse();
		try {
			if (version.equalsIgnoreCase("v1")) {
				if (role.equalsIgnoreCase("User")) {
					if (device.equalsIgnoreCase("Android")) {
						if (language.equalsIgnoreCase("EN")) {
						String decryptData = AES.decrypt(dto.getEncryptData());
						UserOTPDTO aadharRequest = AadharUtil.prepareUserOtpValidateRequest(decryptData);
						resp = aadharServiceApi.getUserOTPValidate(aadharRequest);
						} else {
							resp.setSuccess(false);
							resp.setMessage("Only EN locale available");
						}
					 }else if(device.equalsIgnoreCase("Ios")){
						 if (language.equalsIgnoreCase("EN")) {
//								String decryptData = CryptLib.decrypt(dto.getEncryptData(),key,iv);
//								UserOTPDTO aadharRequest = AadharUtil.prepareUserOtpValidateRequest(decryptData);
//								resp = aadharServiceApi.getUserOTPValidate(aadharRequest);
								} else {
									resp.setSuccess(false);
									resp.setMessage("Only EN locale available");
								}
					} else {
						resp.setSuccess(false);
						resp.setMessage("Unauthorized Device");
					}
				} else {
					resp.setSuccess(false);
					resp.setMessage("Unauthorized Role");
				}
			} else {
				resp.setSuccess(false);
				resp.setMessage("Invalid Version");
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setMessage("We are sorry for inconvenience, Please try again later .");
		}
		return new ResponseEntity<>(resp, HttpStatus.OK);
	}

	@RequestMapping(value = "/GetAadharOTP", method = RequestMethod.POST)
	public ResponseEntity<AadharOTPResponse> getAadharOTP(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody AadharOTPDTO dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws Exception {

		AadharOTPResponse resp = new AadharOTPResponse();
		try {
			if (version.equalsIgnoreCase("v1")) {
				if (role.equalsIgnoreCase("User")) {
					if (device.equalsIgnoreCase("Android")) {
						String decryptData = AES.decrypt(dto.getEncryptData());
						AadharOTPDTO aadharRequest = AadharUtil.prepareOtpRequest(decryptData);
						String sessionId = aadharRequest.getSessionId();
						if (language.equalsIgnoreCase("EN")) {
							if (sessionId != null) {
								String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
								if (authority != null) {
									if (authority.contains(Authorities.USER)
											&& authority.contains(Authorities.AUTHENTICATED)) {
										resp = aadharServiceApi.getAadharOtp(aadharRequest);
									} else {
										resp.setSuccess(false);
										resp.setCode("F06");
										resp.setMessage("User Authentication Failed");
									}
								} else {
									resp.setSuccess(false);
									resp.setCode("F03");
									resp.setMessage("Invaid Session");
								}
							} else {
								resp.setSuccess(false);
								resp.setCode("F03");
								resp.setMessage("Session null");
							}
						} else {
							resp.setSuccess(false);
							resp.setMessage("Only EN locale available");
						}
						}else if(device.equalsIgnoreCase("Ios")){
							String sessionId = dto.getSessionId();
							if (language.equalsIgnoreCase("EN")) {
								if (sessionId != null) {
									String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
									if (authority != null) {
										if (authority.contains(Authorities.USER)
												&& authority.contains(Authorities.AUTHENTICATED)) {
											resp = aadharServiceApi.getAadharOtp(dto);
										} else {
											resp.setSuccess(false);
											resp.setCode("F06");
											resp.setMessage("User Authentication Failed");
										}
									} else {
										resp.setSuccess(false);
										resp.setCode("F03");
										resp.setMessage("Invaid Session");
									}
								} else {
									resp.setSuccess(false);
									resp.setCode("F03");
									resp.setMessage("Session null");
								}
							} else {
								resp.setSuccess(false);
								resp.setMessage("Only EN locale available");
						}
					} else {
						resp.setSuccess(false);
						resp.setMessage("Unauthorized Device");
					}
				} else {
					resp.setSuccess(false);
					resp.setMessage("Unauthorized Role");
				}
			} else {
				resp.setSuccess(false);
				resp.setMessage("Invalid Version");
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setMessage("We are sorry for inconvenience, Please try again later .");
		}
		return new ResponseEntity<>(resp, HttpStatus.OK);
	}

	@RequestMapping(value = "/GetAadharOTPValidate", method = RequestMethod.POST)
	public ResponseEntity<AadharOTPValidateResponse> getAadharOTPValidate(
			@PathVariable(value = "version") String version, @PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody AadharOTPValidateDTO dto, @RequestHeader(value = "hash", required = false) String hash,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {

		AadharOTPValidateResponse resp = new AadharOTPValidateResponse();
		try {
			if (version.equalsIgnoreCase("v1")) {
				if (role.equalsIgnoreCase("User")) {
					if (device.equalsIgnoreCase("Android")) {
						String decryptData = AES.decrypt(dto.getEncryptData());
						AadharOTPValidateDTO aadharRequest = AadharUtil.prepareOtpValidateRequest(decryptData);
						String sessionId = aadharRequest.getSessionId();
						if (language.equalsIgnoreCase("EN")) {
							if (sessionId != null) {
								String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
								if (authority != null) {
									if (authority.contains(Authorities.USER)
											&& authority.contains(Authorities.AUTHENTICATED)) {
										resp = aadharServiceApi.validateAadharOtp(aadharRequest);
										if (resp.isSuccess()) {
											aadharRequest.setCustomerName(resp.getCustomerName());
											aadharRequest.setCustomerAddress(resp.getCustomerAddress());
											resp = aadharServiceApi.saveAadhar(aadharRequest);
										}
										return new ResponseEntity<AadharOTPValidateResponse>(resp, HttpStatus.OK);
									} else {
										resp.setSuccess(false);
										resp.setCode("F06");
										resp.setMessage("User Authentication Failed");
									}
								} else {
									resp.setSuccess(false);
									resp.setCode("F03");
									resp.setMessage("Invaid Session");
								}
							} else {
								resp.setSuccess(false);
								resp.setCode("F03");
								resp.setMessage("Session null");
							}
						} else {
							resp.setSuccess(false);
							resp.setMessage("Only EN locale available");
						}
						}else if(device.equalsIgnoreCase("Ios")){
						String sessionId = dto.getSessionId();
						if (language.equalsIgnoreCase("EN")) {
							if (sessionId != null) {
								String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
								if (authority != null) {
									if (authority.contains(Authorities.USER)
											&& authority.contains(Authorities.AUTHENTICATED)) {
										resp = aadharServiceApi.validateAadharOtp(dto);
										if (resp.isSuccess()) {
											dto.setCustomerName(resp.getCustomerName());
											dto.setCustomerAddress(resp.getCustomerAddress());
											resp = aadharServiceApi.saveAadhar(dto);
										}
										return new ResponseEntity<AadharOTPValidateResponse>(resp, HttpStatus.OK);
									} else {
										resp.setSuccess(false);
										resp.setCode("F06");
										resp.setMessage("User Authentication Failed");
									}
								} else {
									resp.setSuccess(false);
									resp.setCode("F03");
									resp.setMessage("Invaid Session");
								}
							} else {
								resp.setSuccess(false);
								resp.setCode("F03");
								resp.setMessage("Session null");
							}
						} else {
							resp.setSuccess(false);
							resp.setMessage("Only EN locale available");
					}
					} else {
						resp.setSuccess(false);
						resp.setMessage("Unauthorized Device");
					}
				} else {
					resp.setSuccess(false);
					resp.setMessage("Unauthorized Role");
				}
			} else {
				resp.setSuccess(false);
				resp.setMessage("Invalid Version");
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setMessage("We are sorry for inconvenience, Please try again later .");
		}
		return new ResponseEntity<>(resp, HttpStatus.OK);
	}

}
