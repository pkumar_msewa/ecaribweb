package com.aadhar.util;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.aadhar.model.AadharOTPDTO;
import com.aadhar.model.AadharOTPValidateDTO;
import com.aadhar.model.UserOTPDTO;
import com.payqwikweb.app.metadatas.UrlMetadatas;

public class AadharUtil {
	
	public static final String BASE_URL_TEST = "https://www.vijayabankonline.com/epmt_uat/AadharService.aspx";// LIVE URL
	
	public static final String BASE_URL_LIVE = "http://172.16.7.225:8888/AadharService.aspx";
//	172.16.7.225
	
	public static final String REQ_TYPE="OTPREQ";
	
	public static final String OTP_VALIDATE="OTPVALIDATE";
	
	 public static String getAadharServicesUrl(){
	    	if(UrlMetadatas.PRODUCTION){
	    		return BASE_URL_LIVE;
	    	}else{
	    		return BASE_URL_TEST;
	    	}
	    }
	
	public static AadharOTPDTO prepareOtpRequest(String decdata) {
		AadharOTPDTO req= new AadharOTPDTO();
		try {
			JSONObject obj=new JSONObject(decdata);
			req.setSessionId(obj.getString("sessionId"));
			req.setAadharNumber(obj.getString("aadharNumber"));
		return req;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return req;
	}
	
	public static AadharOTPValidateDTO prepareOtpValidateRequest(String decdata) {
		AadharOTPValidateDTO req= new AadharOTPValidateDTO();
		try {
			JSONObject obj=new JSONObject(decdata);
			req.setSessionId(obj.getString("sessionId"));
			req.setAadharNumber(obj.getString("aadharNumber"));
			req.setKey(obj.getString("key"));
			req.setOtpreqId(obj.getString("otpreqId"));
		return req;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return req;
	}
	
	public static UserOTPDTO prepareUserOtpRequest(String decdata) {
		UserOTPDTO req= new UserOTPDTO();
		try {
			JSONObject obj=new JSONObject(decdata);
			req.setSessionId(obj.getString("sessionId"));
		return req;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return req;
	}
	
	public static UserOTPDTO prepareUserOtpValidateRequest(String decdata) {
		UserOTPDTO req= new UserOTPDTO();
		try {
			JSONObject obj=new JSONObject(decdata);
			req.setSessionId(obj.getString("sessionId"));
			req.setUserOTP(obj.getString("userOTP"));
		return req;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return req;
	}

}
