package com.letsManage.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.letsManage.Api.IMasterAdmin;
import com.letsManage.model.dto.BulkMailRequestDTO;
import com.letsManage.model.dto.BulkSmsRequest;
import com.letsManage.model.dto.ChangePasswordDto;
import com.letsManage.model.dto.GCMRequest;
import com.letsManage.model.dto.GetTransDTO;
import com.letsManage.model.dto.MailRequesrtDTO;
import com.letsManage.model.dto.SmsRequest;
import com.letsManage.model.dto.SuperAdminAdd;
import com.letsManage.model.response.ResponseDTO;
import com.letsManage.model.response.ResponseStatus;
import com.payqwikweb.app.model.request.UserInfoRequest;
import com.payqwikweb.app.model.response.BlockUnBlockUserResponse;


@Controller
@RequestMapping("/Api/{version}/{role}/{device}/{language}/ProjectInfo")
public class MasterAdminController {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final IMasterAdmin iMasterAdmin;
	public MasterAdminController(IMasterAdmin iMasterAdmin) {
		this.iMasterAdmin = iMasterAdmin;
		
		
	}
	@RequestMapping(value = "/ListCount", method = RequestMethod.GET)
	ResponseEntity<ResponseDTO> getListCount(@PathVariable(value = "version") String version,@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestHeader(value = "SecretKey", required = true) String key, HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		if (role.equalsIgnoreCase("MASTERADMIN")) {
			System.err.println("inside role");
			result =iMasterAdmin.ListCount(key);
			
		} else {
			result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}
	

	@RequestMapping(value = "/VPayQwikUser", method = RequestMethod.GET)
	ResponseEntity<ResponseDTO> getAllUserTransactionCount(@PathVariable(value = "version") String version,@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestHeader(value = "SecretKey", required = true) String key, HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		if (role.equalsIgnoreCase("MASTERADMIN")) {
			System.err.println("inside role");
			result =iMasterAdmin.getAllList(key);
			
		} else {
			result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/DebitTransaction", method = RequestMethod.GET)
	ResponseEntity<ResponseDTO> getDebitTransactions(@PathVariable(value = "version") String version,@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestHeader(value = "SecretKey", required = true) String key, HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		if (role.equalsIgnoreCase("MASTERADMIN")) {
			System.err.println("inside role");
			result =iMasterAdmin.getDebitList(key);
			
		} else {
			result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}
	@RequestMapping(value = "/CreditTransaction", method = RequestMethod.GET)
	ResponseEntity<ResponseDTO> getCreditTransactions(@PathVariable(value = "version") String version,@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestHeader(value = "SecretKey", required = true) String key, HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		if (role.equalsIgnoreCase("MASTERADMIN")) {
			System.err.println("inside role");
			result =iMasterAdmin.getCreditList(key);
			
		} else {
			result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/ServiceList", method = RequestMethod.GET)
	ResponseEntity<ResponseDTO> getServiceList(@PathVariable(value = "version") String version,@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestHeader(value = "SecretKey", required = true) String key, HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		if (role.equalsIgnoreCase("MASTERADMIN")) {
			System.err.println("inside role");
			result =iMasterAdmin.getServiceList(key);
			
		} else {
			result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/DateWiseTrans", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE },produces = {MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getDateWiseTrans(@PathVariable(value = "version") String version,@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestHeader(value = "SecretKey", required = true) String key, @RequestBody GetTransDTO dto,HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		if (role.equalsIgnoreCase("MASTERADMIN")) {
			System.err.println("inside datewisetrans");
			result =iMasterAdmin.getDateWiseTrans(key,dto);
			
		} else {
			result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/superAdminList", method = RequestMethod.GET)
	ResponseEntity<ResponseDTO> debitTransactions(@PathVariable(value = "version") String version,@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestHeader(value = "SecretKey", required = true) String key, HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		if (role.equalsIgnoreCase("MASTERADMIN")) {
			System.err.println("inside role");
			result =iMasterAdmin.getSuperAdminList(key);
			
		} else {
			result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/addSuperAdmin", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE },produces = {MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> addsuperadmin(@PathVariable(value = "version") String version,@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestHeader(value = "SecretKey", required = true) String key, @RequestBody SuperAdminAdd dto,HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		if (role.equalsIgnoreCase("MASTERADMIN")) {
			System.err.println("inside datewisetrans");
			result =iMasterAdmin.addSuperAdmin(key,dto);
			
		} else {
			result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/ChangePwdSuperAdmin", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE },produces = {MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> chpwdsuperadmin(@PathVariable(value = "version") String version,@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestHeader(value = "SecretKey", required = true) String key, @RequestBody ChangePasswordDto dto,HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		if (role.equalsIgnoreCase("MASTERADMIN")) {
			System.err.println("inside datewisetrans");
			result =iMasterAdmin.changePasswordSuperAdmin(key,dto);
			
		} else {
			result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/BlockSuperAdmin", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE },produces = {MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> blocksuperadmin(@PathVariable(value = "version") String version,@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestHeader(value = "SecretKey", required = true) String key, @RequestBody ChangePasswordDto dto,HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		if (role.equalsIgnoreCase("MASTERADMIN")) {
			System.err.println("inside datewisetrans");
			result =iMasterAdmin.blockSuperAdmin(key,dto);
			
		} else {
			result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/SendMail", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE },produces = {MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> sendmail(@PathVariable(value = "version") String version,@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestHeader(value = "SecretKey", required = true) String key, @RequestBody MailRequesrtDTO dto,HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		if (role.equalsIgnoreCase("MASTERADMIN")) {
			System.err.println("inside datewisetrans");
			result =iMasterAdmin.sendmail(key,dto);
			
		} else {
			result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/SendBulkMail", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE },produces = {MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> sendbulkmail(@PathVariable(value = "version") String version,@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestHeader(value = "SecretKey", required = true) String key, @RequestBody BulkMailRequestDTO dto,HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		if (role.equalsIgnoreCase("MASTERADMIN")) {
			System.err.println("inside datewisetrans");
			result =iMasterAdmin.sendbulkmail(key,dto);
			
		} else {
			result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/SendSms", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE },produces = {MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> sendsms(@PathVariable(value = "version") String version,@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestHeader(value = "SecretKey", required = true) String key, @RequestBody SmsRequest dto,HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		if (role.equalsIgnoreCase("MASTERADMIN")) {
			System.err.println("inside datewisetrans");
			result =iMasterAdmin.sendsms(key,dto);
			
		} else {
			result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/SendBulkSms", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE },produces = {MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> sendbulksms(@PathVariable(value = "version") String version,@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestHeader(value = "SecretKey", required = true) String key, @RequestBody BulkSmsRequest dto,HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		if (role.equalsIgnoreCase("MASTERADMIN")) {
			System.err.println("inside datewisetrans");
			result =iMasterAdmin.sendbulksms(key,dto);
			
		} else {
			result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}
	@RequestMapping(value = "/userList/{userType}", method = RequestMethod.GET, consumes = {
			MediaType.APPLICATION_JSON_VALUE },produces = {MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> userList(@PathVariable(value = "version") String version,@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,@PathVariable(value = "userType") String userType,
			@RequestHeader(value = "SecretKey", required = true) String key,HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		if (role.equalsIgnoreCase("MASTERADMIN")) {
			System.err.println("inside datewisetrans");
			result =iMasterAdmin.getUserList(key,userType);
			
		} else {
			result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/userListFilter/{userType}", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE },produces = {MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> userListFilter(@PathVariable(value = "version") String version,@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,@PathVariable(value = "userType") String userType,
			@RequestHeader(value = "SecretKey", required = true) String key, @RequestBody GetTransDTO dto,HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		if (role.equalsIgnoreCase("MASTERADMIN")) {
			System.err.println("inside datewisetrans");
			result =iMasterAdmin.getUserListFilter(key,userType,dto);
			
		} else {
			result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/SendGcm", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE },produces = {MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> SendGcm(@PathVariable(value = "version") String version,@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestHeader(value = "SecretKey", required = true) String key, @RequestBody GCMRequest dto,HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		if (role.equalsIgnoreCase("MASTERADMIN")) {
			System.err.println("inside datewisetrans");
			result =iMasterAdmin.sendgcm(key,dto);
			
		} else {
			result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}
	
	
	
	@RequestMapping(value = "/userProfile", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE },produces = {MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> userProfile(@PathVariable(value = "version") String version,@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestHeader(value = "SecretKey", required = true) String key, @RequestBody UserInfoRequest dto,HttpServletRequest request, HttpServletResponse response) {
		ResponseDTO result = new ResponseDTO();
		if (role.equalsIgnoreCase("MASTERADMIN")) {
			System.err.println("user profile");
			result =iMasterAdmin.userProfile(key,dto);
			
		} else {
			result.setStatus(ResponseStatus.UNAUTHORIZED_ROLE);
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/blockUser/{userName}", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE },produces = {MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<BlockUnBlockUserResponse> blockUser(@PathVariable(value = "version") String version,@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,@PathVariable(value = "userName") String uname,
			@RequestHeader(value = "SecretKey", required = true) String key, HttpServletRequest request, HttpServletResponse response) {
		BlockUnBlockUserResponse result = new BlockUnBlockUserResponse();
		if (role.equalsIgnoreCase("MASTERADMIN")) {
			System.err.println("user b");
			result =iMasterAdmin.blockUser(key,uname);
		} 
		else {
			result.setStatus("Un-Authorized Role");
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
		return new ResponseEntity<BlockUnBlockUserResponse>(result, HttpStatus.OK);
	}
	@RequestMapping(value = "/unBlockUser/{userName}", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE },produces = {MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<BlockUnBlockUserResponse> unblockUser(@PathVariable(value = "version") String version,@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,@PathVariable(value = "userName") String uname,
			@RequestHeader(value = "SecretKey", required = true) String key,HttpServletRequest request, HttpServletResponse response) {
		BlockUnBlockUserResponse result = new BlockUnBlockUserResponse();
		if (role.equalsIgnoreCase("MASTERADMIN")) {
			System.err.println("user profile");
			result =iMasterAdmin.unblockUser(key,uname);
		}
		else {
			result.setStatus("Un-Authorized Role");
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
		return new ResponseEntity<BlockUnBlockUserResponse>(result, HttpStatus.OK);
	}
	@RequestMapping(value = "/kycupdate/{userName}", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE },produces = {MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<BlockUnBlockUserResponse> kycupdate(@PathVariable(value = "version") String version,@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,@PathVariable(value = "userName") String uname,
			@RequestHeader(value = "SecretKey", required = true) String key, @RequestBody UserInfoRequest dto,HttpServletRequest request, HttpServletResponse response) {
		BlockUnBlockUserResponse result = new BlockUnBlockUserResponse();
		if (role.equalsIgnoreCase("MASTERADMIN")) {
			System.err.println("kyc update");
			result =iMasterAdmin.kycUpdate(key,uname);
		} 
		else {
			result.setStatus("Un-Authorized Role");
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
		return new ResponseEntity<BlockUnBlockUserResponse>(result, HttpStatus.OK);
	}
	@RequestMapping(value = "/nonKycUpdate/{userName}", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE },produces = {MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<BlockUnBlockUserResponse> nonkycupdate(@PathVariable(value = "version") String version,@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,@PathVariable(value = "userName") String uname,
			@RequestHeader(value = "SecretKey", required = true) String key,HttpServletRequest request, HttpServletResponse response) {
		BlockUnBlockUserResponse result = new BlockUnBlockUserResponse();
		if (role.equalsIgnoreCase("MASTERADMIN")) {
			System.err.println("non kyc update");
			result =iMasterAdmin.NonKycUpdate(key,uname);
			
		} else {
			result.setStatus("Un-Authorized Role");
			result.setMessage("Failed, Unauthorized user.");
			result.setDetails("Failed, Unauthorized user.");
		}
		return new ResponseEntity<BlockUnBlockUserResponse>(result, HttpStatus.OK);
	}
}
