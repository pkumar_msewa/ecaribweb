package com.letsManage.Api.Impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.letsManage.Api.IMasterAdmin;
import com.letsManage.model.dto.BulkMailRequestDTO;
import com.letsManage.model.dto.BulkSmsRequest;
import com.letsManage.model.dto.ChangePasswordDto;
import com.letsManage.model.dto.DateTransListDTO;
import com.letsManage.model.dto.DebitCreditDTO;
import com.letsManage.model.dto.DebitCreditListDTO;
import com.letsManage.model.dto.GCMRequest;
import com.letsManage.model.dto.GCMResponse;
import com.letsManage.model.dto.GetTransDTO;
import com.letsManage.model.dto.GetUserDto;
import com.letsManage.model.dto.GetUserListCountDTO;
import com.letsManage.model.dto.ListCountDTO;
import com.letsManage.model.dto.MailRequesrtDTO;
import com.letsManage.model.dto.ServiceListDTO;
import com.letsManage.model.dto.SmsRequest;
import com.letsManage.model.dto.SuperAdminAdd;
import com.letsManage.model.dto.SuperAdminDTO;
import com.letsManage.model.dto.SuperAdminListDTO;
import com.letsManage.model.dto.UserTransactionListDTO;
import com.letsManage.model.dto.transCountChartDTO;
import com.letsManage.model.response.ResponseDTO;
import com.letsManage.model.response.ResponseStatus;
import com.payqwikweb.app.metadatas.UrlMetadatas;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Language;
import com.payqwikweb.app.model.UserAccountInfo;
import com.payqwikweb.app.model.UserBasicInfo;
import com.payqwikweb.app.model.UserListDTO;
import com.payqwikweb.app.model.UserLoginInfo;
import com.payqwikweb.app.model.UserTransactionsInfo;
import com.payqwikweb.app.model.Version;
import com.payqwikweb.app.model.request.UserInfoRequest;
import com.payqwikweb.app.model.response.BlockUnBlockUserResponse;
import com.payqwikweb.app.utils.SecurityUtils;
import com.payqwikweb.util.APIUtils;
import com.payqwikweb.util.Authorities;
import com.payqwikweb.util.JSONParserUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;

public class MasterAdminApiImpl implements IMasterAdmin{
	@Override
	public ResponseDTO ListCount(String Key) {
		ResponseDTO result=new ResponseDTO();

		try {
             System.err.println(Key);
			Client c = Client.create();
			c.addFilter(new LoggingFilter(System.out));
			WebResource webResource = c.resource(UrlMetadatas.VPayQwikListCount(Version.VERSION_1, Authorities.MASTERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json").header("SecretKey", Key).post(ClientResponse.class);

			System.err.println(response);

			String strResponse = response.getEntity(String.class);
			System.err.println("Response :: " + strResponse);
			if (response.getStatus() != 200) {
				result.setStatus(ResponseStatus.FAILURE);
				result.setCode("F00");
				result.setMessage("Service unavailable");
				result.setDetails(null);
			}
			else{
				if (strResponse != null) {
					ListCountDTO dto=new ListCountDTO();
					JSONObject jobj=new JSONObject(strResponse);
					String strDetails=jobj.getString("details");
					final String code=jobj.getString("code");
					if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code))
					{
					if (strDetails!=null) {
					JSONObject jobj1=jobj.getJSONObject("details");
					dto.setUserCount(jobj1.getLong("userCount"));
					dto.setTransactionCount(jobj1.getLong("transactionCount"));
					dto.setLoadMoneyCount(jobj1.getLong("loadMoneyCount"));
					dto.setRechargeCount(jobj1.getLong("rechargeCount"));
					dto.setBillPayCount(jobj1.getLong("billPayCount"));
					dto.setTotalUsersprev1(jobj1.getLong("totalUsersprev1"));
					dto.setTotalUsersprev2(jobj1.getLong("totalUsersprev2"));
					dto.setTotalTransprev1(jobj1.getLong("totalTransprev1"));
					dto.setTotalTransprev2(jobj1.getLong("totalTransprev2"));
					
					
					
					result.setCode(code);
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("All List Count ");
						result.setDetails(dto);
				      }
					}
				}
			}}				
			 catch (Exception e) {
			e.printStackTrace();
			result.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			result.setMessage("Service Unavailable");
			result.setDetails(null);
			result.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR);
		}

		return result;
	}
	
	
	@Override
	public ResponseDTO getAllList(String Key) {
		ResponseDTO result=new ResponseDTO();

		try {
             System.err.println(Key);
			Client c = Client.create();
			c.addFilter(new LoggingFilter(System.out));
			WebResource webResource = c.resource(UrlMetadatas.getAllUserCount(Version.VERSION_1, Authorities.MASTERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json").header("SecretKey", Key).post(ClientResponse.class);

			System.err.println(response);

			String strResponse = response.getEntity(String.class);
			System.err.println("Response :: " + strResponse);
			List<UserTransactionListDTO> listDTO=new ArrayList<>();
			if (response.getStatus() != 200) {
				result.setStatus(ResponseStatus.FAILURE);
				result.setCode("F00");
				result.setMessage("Service unavailable");
				result.setDetails(null);
			}
			else{
				if (strResponse != null) {
					GetUserListCountDTO dto=new GetUserListCountDTO();
					JSONObject jobj=new JSONObject(strResponse);
					String strDetails=jobj.getString("details");
					final String code=jobj.getString("code");
					if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code))
					{
					if (strDetails!=null) {
					JSONObject jobj1=jobj.getJSONObject("details");
					dto.setCreditCount(jobj1.getLong("creditCount"));
					dto.setDebitCount(jobj1.getLong("debitCount"));
					
					JSONArray cList=jobj1.getJSONArray("listDTO");
					System.err.println("ARRAY IS :: " + cList);
					for(int i=0;i<cList.length();i++)
					{
						UserTransactionListDTO dto1=new UserTransactionListDTO();
					     dto1.setId(cList.getJSONObject(i).getLong("id"));
					     dto1.setName(cList.getJSONObject(i).getString("name"));
					     dto1.setTid(cList.getJSONObject(i).getString("tid"));
					     dto1.setTransactionType(cList.getJSONObject(i).getString("transactionType"));
					     dto1.setDate(cList.getJSONObject(i).getString("date"));
					     dto1.setCredit(cList.getJSONObject(i).getLong("credit"));
					     dto1.setDebit(cList.getJSONObject(i).getLong("debit"));
					     dto1.setStatus(cList.getJSONObject(i).getString("status"));
					     listDTO.add(dto1);
					 }
					
					   dto.setListDTO(listDTO);
					   JSONObject jobj2=jobj1.getJSONObject("transCount");
						transCountChartDTO dto2=new transCountChartDTO();
						dto2.setPresent(jobj2.getLong("present"));
						dto2.setCurrent(jobj2.getLong("current"));
						dto2.setPrevmonth1(jobj2.getLong("prevmonth1"));
						dto2.setPrevmonth2(jobj2.getLong("prevmonth2"));
						dto.setTransDTO(dto2);
				       result.setCode(code);
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("All user Count /transaction List");
						result.setDetails(dto);
				      }
					}
				}
			}}				
			 catch (Exception e) {
			e.printStackTrace();
			result.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			result.setMessage("Service Unavailable");
			result.setDetails(null);
			result.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR);
		}

		return result;
	}

	@Override
	public ResponseDTO getDebitList(String key) {
		ResponseDTO result=new ResponseDTO();

		try {
             System.err.println(key);
			Client c = Client.create();
			c.addFilter(new LoggingFilter(System.out));
			WebResource webResource = c.resource(UrlMetadatas.getDebitList(Version.VERSION_1, Authorities.MASTERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json").header("SecretKey", key).post(ClientResponse.class);

			System.err.println(response);

			String strResponse = response.getEntity(String.class);
			System.err.println("Response :: " + strResponse);
			List<DebitCreditListDTO> listDTO=new ArrayList<>();
			if (response.getStatus() != 200) {
				result.setStatus(ResponseStatus.FAILURE);
				result.setCode("F00");
				result.setMessage("Service unavailable");
				result.setDetails(null);
			}
			else{
				if (strResponse != null) {
					DebitCreditDTO dto=new DebitCreditDTO();
					JSONObject jobj=new JSONObject(strResponse);
					String strDetails=jobj.getString("details");
					final String code=jobj.getString("code");
					if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code))
					{
					if (strDetails!=null) {
					JSONObject jobj1=jobj.getJSONObject("details");
					JSONArray cList=jobj1.getJSONArray("listDTO");
					System.err.println("ARRAY IS :: " + cList);
					for(int i=0;i<cList.length();i++)
					{
						DebitCreditListDTO dto1=new DebitCreditListDTO();
					     dto1.setId(i+1);
					     dto1.setName(cList.getJSONObject(i).getString("name"));
					     dto1.setTid(cList.getJSONObject(i).getString("tid"));
					     dto1.setTransactionType(cList.getJSONObject(i).getString("transactionType"));
					     dto1.setDate(cList.getJSONObject(i).getString("date"));
					     dto1.setDebitCredit(cList.getJSONObject(i).getDouble("debitCredit"));
					     dto1.setStatus(cList.getJSONObject(i).getString("status"));
					     listDTO.add(dto1);
					 }
					   dto.setListDTO(listDTO);
				       result.setCode(code);
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("All Debit List");
						result.setDetails(dto);
				      }
					}
				}
			}}				
			 catch (Exception e) {
			e.printStackTrace();
			result.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			result.setMessage("Service Unavailable");
			result.setDetails(null);
			result.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR);
		}

		return result;
	}

	@Override
	public ResponseDTO getCreditList(String key) {
		ResponseDTO result=new ResponseDTO();

		try {
             System.err.println(key);
			Client c = Client.create();
			c.addFilter(new LoggingFilter(System.out));
			WebResource webResource = c.resource(UrlMetadatas.getCreditList(Version.VERSION_1, Authorities.MASTERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json").header("SecretKey", key).post(ClientResponse.class);

			System.err.println(response);

			String strResponse = response.getEntity(String.class);
			System.err.println("Response :: " + strResponse);
			List<DebitCreditListDTO> listDTO=new ArrayList<>();
			if (response.getStatus() != 200) {
				result.setStatus(ResponseStatus.FAILURE);
				result.setCode("F00");
				result.setMessage("Service unavailable");
				result.setDetails(null);
			}
			else{
				if (strResponse != null) {
					DebitCreditDTO dto=new DebitCreditDTO();
					JSONObject jobj=new JSONObject(strResponse);
					String strDetails=jobj.getString("details");
					final String code=jobj.getString("code");
					if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code))
					{
					if (strDetails!=null) {
					JSONObject jobj1=jobj.getJSONObject("details");
					
					JSONArray cList=jobj1.getJSONArray("listDTO");
					System.err.println("ARRAY IS :: " + cList);
					for(int i=0;i<cList.length();i++)
					{
						DebitCreditListDTO dto1=new DebitCreditListDTO();
					     dto1.setId(cList.getJSONObject(i).getLong("id"));
					     dto1.setName(cList.getJSONObject(i).getString("name"));
					     dto1.setTid(cList.getJSONObject(i).getString("tid"));
					     dto1.setTransactionType(cList.getJSONObject(i).getString("transactionType"));
					     dto1.setDate(cList.getJSONObject(i).getString("date"));
					     dto1.setDebitCredit(cList.getJSONObject(i).getLong("debitCredit"));
     				     dto1.setStatus(cList.getJSONObject(i).getString("status"));
					     listDTO.add(dto1);
					 }
					   dto.setListDTO(listDTO);
				       result.setCode(code);
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("All Credit List");
						result.setDetails(dto);
				      }
					}
				}
			}}				
			 catch (Exception e) {
			e.printStackTrace();
			result.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			result.setMessage("Service Unavailable");
			result.setDetails(null);
			result.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR);
		}

		return result;
	}

	@Override
	public ResponseDTO getServiceList(String Key) {
		ResponseDTO result=new ResponseDTO();

		try {
             System.err.println(Key);
			Client c = Client.create();
			c.addFilter(new LoggingFilter(System.out));
			WebResource webResource = c.resource(UrlMetadatas.getServiceList(Version.VERSION_1, Authorities.MASTERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json").header("SecretKey", Key).post(ClientResponse.class);

			System.err.println(response);

			String strResponse = response.getEntity(String.class);
			System.err.println("Response :: " + strResponse);
			if (response.getStatus() != 200) {
				result.setStatus(ResponseStatus.FAILURE);
				result.setCode("F00");
				result.setMessage("Service unavailable");
				result.setDetails(null);
			}
			else{
				if(strResponse != null) {
					ServiceListDTO dto=new ServiceListDTO ();
					JSONObject jobj=new JSONObject(strResponse);
					String strDetails=jobj.getString("details");
					final String code=jobj.getString("code");
					if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code))
					{
					if (strDetails!=null) {
					JSONObject jobj1=jobj.getJSONObject("details");
					dto.setSendMoney(jobj1.getLong("sendMoney"));
					dto.setPoolAccount(jobj1.getLong("poolAccount"));
					    result.setCode(code);
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("All Service LIST ");
						result.setDetails(dto);
				      }
					}
				}
			}}				
			 catch (Exception e) {
			e.printStackTrace();
			result.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			result.setMessage("Service Unavailable");
			result.setDetails(null);
			result.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR);
		}

		return result;
	}
	
	@Override
	public ResponseDTO getDateWiseTrans(String Key,GetTransDTO transDTO) {
		ResponseDTO result=new ResponseDTO();

		try {
			JSONObject payload = new JSONObject();
			payload.put("from",transDTO.getFrom());
			payload.put("to",transDTO.getTo());
             System.err.println(Key);
			Client c = Client.create();
			c.addFilter(new LoggingFilter(System.out));
			WebResource webResource = c.resource(UrlMetadatas.getDateWiseTrans(Version.VERSION_1, Authorities.MASTERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json").header("SecretKey", Key).post(ClientResponse.class,payload.toString());

			System.err.println(response);

			String strResponse = response.getEntity(String.class);
			System.err.println("Response :: " + strResponse);
			List<UserTransactionListDTO> listDTO=new ArrayList<>();
			if (response.getStatus() != 200) {
				result.setStatus(ResponseStatus.FAILURE);
				result.setCode("F00");
				result.setMessage("Service unavailable");
				result.setDetails(null);
			}
			else{
				if (strResponse != null) {
					DateTransListDTO dto=new DateTransListDTO();
					JSONObject jobj=new JSONObject(strResponse);
					String strDetails=jobj.getString("details");
					final String code=jobj.getString("code");
					if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code))
					{
					if (strDetails!=null) {
					JSONObject jobj1=jobj.getJSONObject("details");
					JSONArray cList=jobj1.getJSONArray("listDTO");
					System.err.println("ARRAY IS :: " + cList);
					for(int i=0;i<cList.length();i++)
					{
						UserTransactionListDTO dto1=new UserTransactionListDTO();
					     dto1.setId(cList.getJSONObject(i).getLong("id"));
					     dto1.setName(cList.getJSONObject(i).getString("name"));
					     dto1.setTid(cList.getJSONObject(i).getString("tid"));
					     dto1.setTransactionType(cList.getJSONObject(i).getString("transactionType"));
					     dto1.setDate(cList.getJSONObject(i).getString("date"));
					     dto1.setCredit(cList.getJSONObject(i).getLong("credit"));
					     dto1.setDebit(cList.getJSONObject(i).getLong("debit"));
					     dto1.setStatus(cList.getJSONObject(i).getString("status"));
					     listDTO.add(dto1);
					 }
					
					   dto.setListDTO(listDTO);
					    result.setCode(code);
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("All DateWise transaction List");
						result.setDetails(dto);
				      }
					}
				}
			}}				
			 catch (Exception e) {
			e.printStackTrace();
			result.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			result.setMessage("Service Unavailable");
			result.setDetails(null);
			result.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR);
		}

		return result;
	}


	@Override
	public ResponseDTO getSuperAdminList(String key) {
		ResponseDTO result=new ResponseDTO();

		try {
             System.err.println(key);
			Client c = Client.create();
			c.addFilter(new LoggingFilter(System.out));
			WebResource webResource = c.resource(UrlMetadatas.getSuperAdminList(Version.VERSION_1, Authorities.MASTERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json").header("SecretKey", key).post(ClientResponse.class);

			System.err.println(response);

			String strResponse = response.getEntity(String.class);
			System.err.println("Response :: " + strResponse);
			List<SuperAdminDTO> listDTO=new ArrayList<>();
			if (response.getStatus() != 200) {
				result.setStatus(ResponseStatus.FAILURE);
				result.setCode("F00");
				result.setMessage("Service unavailable");
				result.setDetails(null);
			}
			else{
				if (strResponse != null) {
					SuperAdminListDTO dto=new SuperAdminListDTO();
					JSONObject jobj=new JSONObject(strResponse);
					String strDetails=jobj.getString("details");
					final String code=jobj.getString("code");
					if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code))
					{
					if (strDetails!=null) {
					JSONObject jobj1=jobj.getJSONObject("details");
					
					JSONArray cList=jobj1.getJSONArray("listDTO");
					System.err.println("ARRAY IS :: " + cList);
					for(int i=0;i<cList.length();i++)
					{
						SuperAdminDTO dto1=new SuperAdminDTO();
						dto1.setId(i+1);
					     dto1.setSuperName(cList.getJSONObject(i).getString("superName"));
					     dto1.setStatus(cList.getJSONObject(i).getString("status"));
					     dto1.setEmail(cList.getJSONObject(i).getString("email"));
					     dto1.setAddress(cList.getJSONObject(i).getString("address"));
					     dto1.setContactNo(cList.getJSONObject(i).getString("contactNo"));
					     listDTO.add(dto1);
					 }
					   dto.setListDTO(listDTO);
				       result.setCode(code);
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage("All SuperAdmin List");
						result.setDetails(dto);
				      }
					}
				}
			}}				
			 catch (Exception e) {
			e.printStackTrace();
			result.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			result.setMessage("Service Unavailable");
			result.setDetails(null);
			result.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR);
		}

		return result;
	}
	
	@Override
	public ResponseDTO changePasswordSuperAdmin(String Key,ChangePasswordDto dto) {
		ResponseDTO result=new ResponseDTO();

		try {
			JSONObject payload = new JSONObject();
			payload.put("userName", dto.getUserName());
			payload.put("password", dto.getPassword());
             System.err.println(Key);
			Client c = Client.create();
			c.addFilter(new LoggingFilter(System.out));
			WebResource webResource = c.resource(UrlMetadatas.changepasswordSuperAdmin(Version.VERSION_1, Authorities.MASTERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json").header("SecretKey", Key).post(ClientResponse.class,payload.toString());

			System.err.println(response);

			String strResponse = response.getEntity(String.class);
			System.err.println("Response :: " + strResponse);
			
			if (response.getStatus() != 200) {
				result.setStatus(ResponseStatus.FAILURE);
				result.setCode("F00");
				result.setMessage("Service unavailable");
				result.setDetails(null);
			}
			else{
				if (strResponse != null) {
				
					JSONObject jobj=new JSONObject(strResponse);
					String strDetails=jobj.getString("details");
					final String code=jobj.getString("code");
					if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code))
					{
					if (strDetails!=null) {
					    result.setCode(code);
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage(jobj.getString("message"));
						result.setDetails(strDetails);
				      }
					}
					
				}
			}}				
			 catch (Exception e) {
			e.printStackTrace();
			result.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			result.setMessage("Service Unavailable");
			result.setDetails(null);
			result.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR);
		}

		return result;
	}

	
	@Override
	public ResponseDTO addSuperAdmin(String Key,SuperAdminAdd superDto) {
		ResponseDTO result=new ResponseDTO();

		try {
			JSONObject payload = new JSONObject();
			
			payload.put("contactNo",superDto.getContactNo());
			payload.put("email", superDto.getEmail());
			payload.put("firstName",superDto.getFirstName());
			payload.put("lastName", superDto.getLastName());
			payload.put("address",superDto.getAddress());
			payload.put("password", superDto.getPassword());
			payload.put("userName", superDto.getUserName());
			
             System.err.println(Key);
			Client c = Client.create();
			c.addFilter(new LoggingFilter(System.out));
			WebResource webResource = c.resource(UrlMetadatas.addSuperAdmin(Version.VERSION_1, Authorities.MASTERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json").header("SecretKey", Key).post(ClientResponse.class,payload.toString());

			System.err.println(response);

			String strResponse = response.getEntity(String.class);
			System.err.println("Response :: " + strResponse);
			
			if (response.getStatus() != 200) {
				result.setStatus(ResponseStatus.FAILURE);
				result.setCode("F00");
				result.setMessage("Service unavailable");
				result.setDetails(null);
			}
			else{
				if (strResponse != null) {
				
					JSONObject jobj=new JSONObject(strResponse);
					String strDetails=jobj.getString("details");
					final String code=jobj.getString("code");
					if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code))
					{
					if (strDetails!=null) {
					    result.setCode(code);
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage(jobj.getString("message"));
						result.setDetails(strDetails);
				      }
					}
					else if(ResponseStatus.SUCCESS.getValue().equalsIgnoreCase("S01")){
						if (strDetails!=null) {
						    result.setCode("S01");
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage(jobj.getString("message"));
							result.setDetails(strDetails);
					      }	
					}
					
				}
			}}				
			 catch (Exception e) {
			e.printStackTrace();
			result.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			result.setMessage("Service Unavailable");
			result.setDetails(null);
			result.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR);
		}

		return result;
	}

	
	@Override
	public ResponseDTO blockSuperAdmin(String Key,ChangePasswordDto dto) {
		ResponseDTO result=new ResponseDTO();

		try {
			JSONObject payload = new JSONObject();
			
			payload.put("password",dto.getPassword());
			payload.put("userName", dto.getUserName());
			 System.err.println(Key);
			Client c = Client.create();
			c.addFilter(new LoggingFilter(System.out));
			WebResource webResource = c.resource(UrlMetadatas.blockSuperAdmin(Version.VERSION_1, Authorities.MASTERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json").header("SecretKey", Key).post(ClientResponse.class,payload.toString());

			System.err.println(response);

			String strResponse = response.getEntity(String.class);
			System.err.println("Response :: " + strResponse);
			
			if (response.getStatus() != 200) {
				result.setStatus(ResponseStatus.FAILURE);
				result.setCode("F00");
				result.setMessage("Service unavailable");
				result.setDetails(null);
			}
			else{
				if (strResponse != null) {
				
					JSONObject jobj=new JSONObject(strResponse);
					String strDetails=jobj.getString("details");
					final String code=jobj.getString("code");
					if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code))
					{
					if (strDetails!=null) {
					    result.setCode(code);
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage(jobj.getString("message"));
						result.setDetails(strDetails);
				      }
					}
					
				}
			}}				
			 catch (Exception e) {
			e.printStackTrace();
			result.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			result.setMessage("Service Unavailable");
			result.setDetails(null);
			result.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR);
		}

		return result;
	}
	

	@Override
	public ResponseDTO sendsms(String key, SmsRequest sdto) {
		ResponseDTO result=new ResponseDTO();
      try {
			JSONObject payload = new JSONObject();
			payload.put("content",sdto.getContent());
			payload.put("mobile",sdto.getMobile());
		    System.err.println(key);
			Client c = Client.create();
			c.addFilter(new LoggingFilter(System.out));
			WebResource webResource = c.resource(UrlMetadatas.sendsms(Version.VERSION_1, Authorities.MASTERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json").header("SecretKey", key).post(ClientResponse.class,payload.toString());
            System.err.println(response);
            String strResponse = response.getEntity(String.class);
			System.err.println("Response :: " + strResponse);
			
			if (response.getStatus() != 200) {
				result.setStatus(ResponseStatus.FAILURE);
				result.setCode("F00");
				result.setMessage("Service unavailable");
				result.setDetails(null);
			}
			else{
				if (strResponse != null) {
				
					JSONObject jobj=new JSONObject(strResponse);
					String strDetails=jobj.getString("details");
					final String code=jobj.getString("code");
					if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code))
					{
					if (strDetails!=null) {
					    result.setCode(code);
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage(jobj.getString("message"));
						result.setDetails(strDetails);
				      }
					}
					else if(ResponseStatus.FAILURE.getValue().equalsIgnoreCase(code)){
						if (strDetails!=null) {
						    result.setCode("S01");
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage(jobj.getString("message"));
							result.setDetails(strDetails);
					      }	
					}
					
				}
			}}				
			 catch (Exception e) {
			e.printStackTrace();
			result.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			result.setMessage("Service Unavailable");
			result.setDetails(null);
			result.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR);
		}

		return result;	
	}
	
	@Override
	public ResponseDTO sendbulksms(String key, BulkSmsRequest sdto) {
		ResponseDTO result=new ResponseDTO();

		try {
			JSONObject payload = new JSONObject();
			payload.put("userType", sdto.getUserType());
			payload.put("content",sdto.getContent());
			 System.err.println(key);
			Client c = Client.create();
			c.addFilter(new LoggingFilter(System.out));
			WebResource webResource = c.resource(UrlMetadatas.sendbulksms(Version.VERSION_1, Authorities.MASTERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json").header("SecretKey", key).post(ClientResponse.class,payload.toString());

			System.err.println(response);

			String strResponse = response.getEntity(String.class);
			System.err.println("Response :: " + strResponse);
			
			if (response.getStatus() != 200) {
				result.setStatus(ResponseStatus.FAILURE);
				result.setCode("F00");
				result.setMessage("Service unavailable");
				result.setDetails(null);
			}
			else{
				if (strResponse != null) {
				
					JSONObject jobj=new JSONObject(strResponse);
					String strDetails=jobj.getString("details");
					final String code=jobj.getString("code");
					if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code))
					{
					if (strDetails!=null) {
					    result.setCode(code);
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage(jobj.getString("message"));
						result.setDetails(strDetails);
				      }
					}
					
				}
			}}				
			 catch (Exception e) {
			e.printStackTrace();
			result.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			result.setMessage("Service Unavailable");
			result.setDetails(null);
			result.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR);
		}

		return result;	
	}
	
	@Override
	public ResponseDTO sendmail(String key, MailRequesrtDTO sdto) {
		ResponseDTO result=new ResponseDTO();

		try {
			JSONObject payload = new JSONObject();
			payload.put("content",sdto.getContent());
			payload.put("destination",sdto.getDestination());
			payload.put("subject", sdto.getSubject());
			 System.err.println(key);
			Client c = Client.create();
			c.addFilter(new LoggingFilter(System.out));
			WebResource webResource = c.resource(UrlMetadatas.sendmail(Version.VERSION_1, Authorities.MASTERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json").header("SecretKey", key).post(ClientResponse.class,payload.toString());

			System.err.println(response);

			String strResponse = response.getEntity(String.class);
			System.err.println("Response :: " + strResponse);
			
			if (response.getStatus() != 200) {
				result.setStatus(ResponseStatus.FAILURE);
				result.setCode("F00");
				result.setMessage("Service unavailable");
				result.setDetails(null);
			}
			else{
				if (strResponse != null) {
				
					JSONObject jobj=new JSONObject(strResponse);
					String strDetails=jobj.getString("details");
					final String code=jobj.getString("code");
					if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code))
					{
					if (strDetails!=null) {
					    result.setCode(code);
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage(jobj.getString("message"));
						result.setDetails(strDetails);
				      }
					}
					else if(ResponseStatus.SUCCESS.getValue().equalsIgnoreCase("S01")){
						if (strDetails!=null) {
						    result.setCode("S01");
							result.setStatus(ResponseStatus.FAILURE);
							result.setMessage(jobj.getString("message"));
							result.setDetails(strDetails);
					      }	
					}
					
				}
			}}				
			 catch (Exception e) {
			e.printStackTrace();
			result.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			result.setMessage("Service Unavailable");
			result.setDetails(null);
			result.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR);
		}

		return result;	
	}
	
	@Override
	public ResponseDTO sendbulkmail(String key, BulkMailRequestDTO sdto) {
		ResponseDTO result=new ResponseDTO();

		try {
			JSONObject payload = new JSONObject();
			payload.put("userType", sdto.getUserType());
			payload.put("content",sdto.getContent());
			payload.put("subject", sdto.getSubject());
			 System.err.println(key);
			Client c = Client.create();
			c.addFilter(new LoggingFilter(System.out));
			WebResource webResource = c.resource(UrlMetadatas.sendbulkmail(Version.VERSION_1, Authorities.MASTERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json").header("SecretKey", key).post(ClientResponse.class,payload.toString());

			System.err.println(response);

			String strResponse = response.getEntity(String.class);
			System.err.println("Response :: " + strResponse);
			
			if (response.getStatus() != 200) {
				result.setStatus(ResponseStatus.FAILURE);
				result.setCode("F00");
				result.setMessage("Service unavailable");
				result.setDetails(null);
			}
			else{
				if (strResponse != null) {
				
					JSONObject jobj=new JSONObject(strResponse);
					String strDetails=jobj.getString("details");
					final String code=jobj.getString("code");
					if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code))
					{
					if (strDetails!=null) {
					    result.setCode(code);
						result.setStatus(ResponseStatus.SUCCESS);
						result.setMessage(jobj.getString("message"));
						result.setDetails(strDetails);
				      }
					}
					
				}
			}}				
			 catch (Exception e) {
			e.printStackTrace();
			result.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			result.setMessage("Service Unavailable");
			result.setDetails(null);
			result.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR);
		}

		return result;	
	}
	
	@Override
	public ResponseDTO getUserList(String Key,String userType)
	{	ResponseDTO result=new ResponseDTO();

	try {
         System.err.println(Key);
		Client c = Client.create();
		c.addFilter(new LoggingFilter(System.out));
		WebResource webResource = c.resource(UrlMetadatas.getUserList(Version.VERSION_1, Authorities.MASTERADMIN, Device.WEBSITE, Language.ENGLISH,userType));
		ClientResponse response = webResource.accept("application/json").type("application/json").header("SecretKey", Key).post(ClientResponse.class);

		System.err.println(response);

		String strResponse = response.getEntity(String.class);
		System.err.println("Response :: " + strResponse);
		
		if (response.getStatus() != 200) {
			result.setStatus(ResponseStatus.FAILURE);
			result.setCode("F00");
			result.setMessage("Service unavailable");
			result.setDetails(null);
		}
		else{
			if (strResponse != null) {
				GetUserDto dto1=new GetUserDto();
				List<UserListDTO> listDTO=new ArrayList<>();
				JSONObject jobj=new JSONObject(strResponse);
				String strDetails=jobj.getString("details");
				final String code=jobj.getString("code");
				if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code))
				{
				if (strDetails!=null) {
				JSONObject jobj1=jobj.getJSONObject("details");
				JSONArray cList=jobj1.getJSONArray("listdto");
				System.err.println("ARRAY IS :: " + cList);
				for(int i=0;i<cList.length();i++)
				{
					UserListDTO dto=new UserListDTO();
					dto.setAccountType(cList.getJSONObject(i).getString("accountType"));
					dto.setCircleName(cList.getJSONObject(i).getString("circleName"));
					dto.setAuthority(cList.getJSONObject(i).getString("authority"));
					dto.setBalance(cList.getJSONObject(i).getString("balance"));
					dto.setDateOfBirth(cList.getJSONObject(i).getString("dateOfBirth"));
					dto.setEmail(cList.getJSONObject(i).getString("email"));
					dto.setMobile(cList.getJSONObject(i).getString("mobile"));
					dto.setGender(cList.getJSONObject(i).getString("gender"));
					dto.setName(cList.getJSONObject(i).getString("name"));
					dto.setPinCode(cList.getJSONObject(i).getString("pinCode"));
					dto.setPoints(cList.getJSONObject(i).getString("points"));
					dto.setRegistrationDate(cList.getJSONObject(i).getString("registrationDate"));
					dto.setVbankAccountNumber(cList.getJSONObject(i).getString("vbankAccountNumber"));
					listDTO.add(dto);
				 }
				
				   dto1.setListDTO(listDTO);
				  result.setCode(code);
					result.setStatus(ResponseStatus.SUCCESS);
					result.setMessage("All user Count /transaction List");
					result.setDetails(dto1);
			      }
				}
			}
		}}				
		 catch (Exception e) {
		e.printStackTrace();
		result.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
		result.setMessage("Service Unavailable");
		result.setDetails(null);
		result.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR);
	}

	return result;
}
	
	@Override
	public ResponseDTO getUserListFilter(String key,String type,GetTransDTO filter)
	{	ResponseDTO result=new ResponseDTO();

	try {
		JSONObject payload = new JSONObject();
		payload.put("from",filter.getFrom());
		payload.put("to",filter.getTo());
         System.err.println(key);
		Client c = Client.create();
		c.addFilter(new LoggingFilter(System.out));
		WebResource webResource = c.resource(UrlMetadatas.getUserListFilter(Version.VERSION_1, Authorities.MASTERADMIN, Device.WEBSITE, Language.ENGLISH,type));
		ClientResponse response = webResource.accept("application/json").type("application/json").header("SecretKey", key).post(ClientResponse.class,payload);

		System.err.println(response);

		String strResponse = response.getEntity(String.class);
		System.err.println("Response :: " + strResponse);
		
		if (response.getStatus() != 200) {
			result.setStatus(ResponseStatus.FAILURE);
			result.setCode("F00");
			result.setMessage("Service unavailable");
			result.setDetails(null);
		}
		else{
			if (strResponse != null) {
				GetUserDto dto1=new GetUserDto();
				List<UserListDTO> listDTO=new ArrayList<>();
				JSONObject jobj=new JSONObject(strResponse);
				String strDetails=jobj.getString("details");
				final String code=jobj.getString("code");
				if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code))
				{
				if (strDetails!=null) {
				JSONObject jobj1=jobj.getJSONObject("details");
				JSONArray cList=jobj1.getJSONArray("listdto");
				System.err.println("ARRAY IS :: " + cList);
				for(int i=0;i<cList.length();i++)
				{
					UserListDTO dto=new UserListDTO();
					dto.setAccountType(cList.getJSONObject(i).getString("accountType"));
					dto.setCircleName(cList.getJSONObject(i).getString("circleName"));
					dto.setAuthority(cList.getJSONObject(i).getString("authority"));
					dto.setBalance(cList.getJSONObject(i).getString("balance"));
					dto.setDateOfBirth(cList.getJSONObject(i).getString("dateOfBirth"));
					dto.setEmail(cList.getJSONObject(i).getString("email"));
					dto.setMobile(cList.getJSONObject(i).getString("mobile"));
					dto.setGender(cList.getJSONObject(i).getString("gender"));
					dto.setName(cList.getJSONObject(i).getString("name"));
					dto.setPinCode(cList.getJSONObject(i).getString("pinCode"));
					dto.setPoints(cList.getJSONObject(i).getString("points"));
					dto.setRegistrationDate(cList.getJSONObject(i).getString("registrationDate"));
					dto.setVbankAccountNumber(cList.getJSONObject(i).getString("vbankAccountNumber"));
					listDTO.add(dto);
				 }
				
				   dto1.setListDTO(listDTO);
				  result.setCode(code);
					result.setStatus(ResponseStatus.SUCCESS);
					result.setMessage(" user List");
					result.setDetails(dto1);
			      }
				}
			}
		}}				
		 catch (Exception e) {
		e.printStackTrace();
		result.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
		result.setMessage("Service Unavailable");
		result.setDetails(null);
		result.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR);
	}

	return result;
}
	@Override
	public ResponseDTO sendgcm(String key,GCMRequest req)
	{	ResponseDTO result=new ResponseDTO();

	try {
		JSONObject payload = new JSONObject();
		payload.put("page",req.getPage());
		payload.put("size",req.getSize());
         System.err.println(key);
		Client c = Client.create();
		c.addFilter(new LoggingFilter(System.out));
		WebResource webResource = c.resource(UrlMetadatas.gcmnotification(Version.VERSION_1, Authorities.MASTERADMIN, Device.WEBSITE, Language.ENGLISH));
		ClientResponse response = webResource.accept("application/json").type("application/json").header("SecretKey", key).post(ClientResponse.class,payload);

		System.err.println(response);

		String strResponse = response.getEntity(String.class);
		if (response.getStatus() != 200) {
			result.setStatus(ResponseStatus.FAILURE);
			result.setCode("F00");
			result.setMessage("Service unavailable");
			result.setDetails(null);
		} else {
			if (strResponse != null) {
				org.json.JSONObject jobj1 = new org.json.JSONObject(strResponse);
				if (jobj1 != null) {
					final String code = (String) jobj1.get("code");
					if (code.equalsIgnoreCase("S00")) {
						GCMResponse dto=new GCMResponse();
						List<String> gcmIds = new ArrayList<>();
						JSONObject obj = new JSONObject(strResponse);
						JSONObject jobj=obj.getJSONObject("details");
						JSONArray cList=jobj.getJSONArray("listDTO");
						long pages=jobj.getLong("totalPages");
						dto.setPage(pages);
						
						
						if(cList != null) {
								for(int i = 0 ; i < cList.length() ; i++) {
									gcmIds.add(cList.getString(i));
								}
								dto.setListDTO(gcmIds);
							   
							}
					    result.setCode(code);
						result.setStatus(ResponseStatus.SUCCESS);
					    result.setMessage("All user List for gcm notifications");
						result.setDetails(dto);
						}
					} else {
						result.setStatus(ResponseStatus.FAILURE);
					}
				} else {
					result.setStatus(ResponseStatus.FAILURE);
				}
			} 
	}				
		 catch (Exception e) {
		e.printStackTrace();
		result.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
		result.setMessage("Service Unavailable");
		result.setDetails(null);
		result.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR);
	}

	return result;
}
	@Override
	public ResponseDTO userProfile(String Key,UserInfoRequest req)
	{	ResponseDTO result=new ResponseDTO();

	try {
		JSONObject payload = new JSONObject();
		payload.put("username",req.getUsername());
		 System.err.println(Key);
		Client c = Client.create();
		c.addFilter(new LoggingFilter(System.out));
		WebResource webResource = c.resource(UrlMetadatas.userProfile(Version.VERSION_1, Authorities.MASTERADMIN, Device.WEBSITE, Language.ENGLISH));
		ClientResponse response = webResource.accept("application/json").type("application/json").header("SecretKey", Key).post(ClientResponse.class,payload);

		System.err.println(response);

		String strResponse = response.getEntity(String.class);
		System.err.println("Response :: " + strResponse);
		
		if (response.getStatus() != 200) {
			result.setStatus(ResponseStatus.FAILURE);
			result.setCode("F00");
			result.setMessage("Service unavailable");
			result.setDetails(null);
		}
		 else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							result.setStatus(status);
							result.setSuccess(true);
							HashMap<String, Object> details1 = new HashMap<>();
							org.json.JSONObject details = JSONParserUtil.getObject(jobj,"details");
							if(details != null) {
								org.json.JSONArray loginLogs = JSONParserUtil.getArray(details,"loginLogs");
								org.json.JSONArray transactions = JSONParserUtil.getArray(details,"transactions");
								org.json.JSONObject user = JSONParserUtil.getObject(details,"user");
								org.json.JSONObject account = JSONParserUtil.getObject(details,"account");
								List<UserLoginInfo> logs = new ArrayList<>();
								if(loginLogs != null) {
									for(int j = 0 ; j < loginLogs.length(); j++) {
										org.json.JSONObject loginObj = loginLogs.getJSONObject(j);
										UserLoginInfo login = new UserLoginInfo();
										login.setLoginTime(JSONParserUtil.getString(loginObj,"loginTime"));
										login.setDeviceId(JSONParserUtil.getString(loginObj,"deviceId"));
										login.setStatus(JSONParserUtil.getString(loginObj,"status"));
										logs.add(login);
									}
								}
								List<UserTransactionsInfo> transactionList = new ArrayList<>();
								if(transactions != null) {
									for (int i = 0; i < transactions.length(); i++) {
										org.json.JSONObject transaction = transactions.getJSONObject(i);
										UserTransactionsInfo dto = new UserTransactionsInfo();
										dto.setDate(JSONParserUtil.getString(transaction, "date"));
										dto.setAmount(JSONParserUtil.getString(transaction, "amount"));
										dto.setCurrentBalance(JSONParserUtil.getString(transaction, "currentBalance"));
										dto.setDebit(JSONParserUtil.getBoolean(transaction, "debit"));
										dto.setFavourite(JSONParserUtil.getBoolean(transaction, "favourite"));
										dto.setCommissionIdentifier(JSONParserUtil.getString(transaction, "commissionIdentifier"));
										dto.setDescription(JSONParserUtil.getString(transaction, "description"));
										dto.setTransactionRefNo(JSONParserUtil.getString(transaction, "transactionRefNo"));
										dto.setStatus(JSONParserUtil.getString(transaction, "status"));
										transactionList.add(dto);
									}
								}
								UserBasicInfo basic = new UserBasicInfo();
								if(user != null) {
									basic.setRegistrationDate(JSONParserUtil.getString(user,"registrationDate"));
									basic.setAuthority(JSONParserUtil.getString(user,"authority"));
									basic.setEmailStatus(JSONParserUtil.getString(user,"emailStatus"));
									basic.setMobileStatus(JSONParserUtil.getString(user,"mobileStatus"));
									basic.setMobile(JSONParserUtil.getString(user,"mobile"));
									basic.setEmail(JSONParserUtil.getString(user,"email"));
									basic.setName(JSONParserUtil.getString(user,"name"));
									basic.setGender(JSONParserUtil.getString(user,"gender"));
									basic.setDateOfBirth(JSONParserUtil.getString(user,"dateOfBirth"));
									basic.setImage(JSONParserUtil.getString(user,"image"));
									basic.setCircleName(JSONParserUtil.getString(user,"circleName"));
									basic.setPinCode(JSONParserUtil.getString(user,"pinCode"));
									basic.setStateName(JSONParserUtil.getString(user,"stateName"));
									basic.setUsername(JSONParserUtil.getString(user,"username"));
								}
								UserAccountInfo ainfo = new UserAccountInfo();
								if(account != null) {
									ainfo.setBalance(JSONParserUtil.getString(account,"balance"));
									ainfo.setAccountNumber(JSONParserUtil.getString(account,"accountNumber"));
									ainfo.setDescription(JSONParserUtil.getString(account,"description"));
									ainfo.setPoints(JSONParserUtil.getString(account,"points"));
									ainfo.setBranchCode(JSONParserUtil.getString(account,"branchCode"));
									ainfo.setVijayaAccountNo(JSONParserUtil.getString(account,"vijayaAccountNo"));
									ainfo.setTransactionLimit(JSONParserUtil.getString(account,"transactionLimit"));
									ainfo.setBalanceLimit(JSONParserUtil.getString(account,"balanceLimit"));
									ainfo.setName(JSONParserUtil.getString(account,"name"));
									ainfo.setCode(JSONParserUtil.getString(account,"code"));
									ainfo.setDailyLimit(JSONParserUtil.getString(account,"dailyLimit"));
									ainfo.setMonthlyLimit(JSONParserUtil.getString(account,"monthlyLimit"));
									ainfo.setLinkedAccountName(JSONParserUtil.getString(account,"linkedAccountName"));
									ainfo.setLinkedAccountMobile(JSONParserUtil.getString(account,"linkedAccountMobile"));
								}
								details1.put("user",basic);
								details1.put("account",ainfo);
                                details1.put("transactions",transactionList);
								details1.put("loginLogs",logs);
								details1.put("online",JSONParserUtil.getBoolean(details, "online"));
								details1.put("totalDebit",JSONParserUtil.getString(details,"totalDebit") );
								details1.put("totalCredit",JSONParserUtil.getString(details,"totalCredit") );
								result.setDetails(details1);
							}
						} else {
							result.setSuccess(false);
						}
						result.setMessage(message);
						result.setCode(code);
					} else {
						result.setSuccess(false);
						result.setMessage("Service unavailable");
					}
				} else {
					result.setSuccess(false);
					result.setMessage("Service unavailable");
				}
			
		 }} catch (Exception e) {
		e.printStackTrace();
		result.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
		result.setMessage("Service Unavailable");
		result.setDetails(null);
		result.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR);
	}

	return result;
}
	@Override
	public BlockUnBlockUserResponse blockUser(String key,String request) {
		BlockUnBlockUserResponse resp = new BlockUnBlockUserResponse();
		try {
			
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(UrlMetadatas.blockUserOfMasterAdminUrl(Version.VERSION_1, Authorities.MASTERADMIN, Device.WEBSITE, Language.ENGLISH,request));
			ClientResponse response = webResource.accept("application/json").type("application/json").header("SecretKey", key).post(ClientResponse.class);

			
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					} else {
						resp.setSuccess(false);
						resp.setCode("F00");
						resp.setMessage("Service unavailable");
						resp.setStatus("FAILED");
						resp.setResponse(APIUtils.getFailedJSON().toString());
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public BlockUnBlockUserResponse unblockUser(String key,String request) {
		BlockUnBlockUserResponse resp = new BlockUnBlockUserResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("username", request);
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(UrlMetadatas.unblockUserOfMasterAdminUrl(Version.VERSION_1, Authorities.MASTERADMIN, Device.WEBSITE, Language.ENGLISH,request));
			ClientResponse response = webResource.accept("application/json").type("application/json").header("SecretKey", key).post(ClientResponse.class);

			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					} else {
						resp.setSuccess(false);
						resp.setCode("F00");
						resp.setMessage("Service unavailable");
						resp.setStatus("FAILED");
						resp.setResponse(APIUtils.getFailedJSON().toString());
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public BlockUnBlockUserResponse kycUpdate(String key,String request) {
		BlockUnBlockUserResponse resp = new BlockUnBlockUserResponse();
		try {
			
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(UrlMetadatas.kycMasterAdminUrl(Version.VERSION_1, Authorities.MASTERADMIN, Device.WEBSITE, Language.ENGLISH,request));
			ClientResponse response = webResource.accept("application/json").type("application/json").header("SecretKey", key).post(ClientResponse.class);

			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					} else {
						resp.setSuccess(false);
						resp.setCode("F00");
						resp.setMessage("Service unavailable");
						resp.setStatus("FAILED");
						resp.setResponse(APIUtils.getFailedJSON().toString());
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}
	
	@Override
	public BlockUnBlockUserResponse NonKycUpdate(String key,String request) {
		BlockUnBlockUserResponse resp = new BlockUnBlockUserResponse();
		try {
			
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(UrlMetadatas.nonkycMasterAdminUrl(Version.VERSION_1, Authorities.MASTERADMIN, Device.WEBSITE, Language.ENGLISH,request));
			ClientResponse response = webResource.accept("application/json").type("application/json").header("SecretKey", key).post(ClientResponse.class);

			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");

						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					} else {
						resp.setSuccess(false);
						resp.setCode("F00");
						resp.setMessage("Service unavailable");
						resp.setStatus("FAILED");
						resp.setResponse(APIUtils.getFailedJSON().toString());
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}	
	
}