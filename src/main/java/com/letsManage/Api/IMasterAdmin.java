package com.letsManage.Api;

import com.letsManage.model.dto.BulkMailRequestDTO;
import com.letsManage.model.dto.BulkSmsRequest;
import com.letsManage.model.dto.ChangePasswordDto;
import com.letsManage.model.dto.GCMRequest;
import com.letsManage.model.dto.GetTransDTO;
import com.letsManage.model.dto.MailRequesrtDTO;
import com.letsManage.model.dto.SmsRequest;
import com.letsManage.model.dto.SuperAdminAdd;
import com.letsManage.model.request.GetUserList;
import com.letsManage.model.response.ResponseDTO;
import com.payqwikweb.app.model.request.UserInfoRequest;
import com.payqwikweb.app.model.response.BlockUnBlockUserResponse;

public interface IMasterAdmin {
	ResponseDTO ListCount(String key);
	
	ResponseDTO getAllList(String key);
	
	ResponseDTO getDebitList(String key);
	
	ResponseDTO getCreditList(String key);
	
	ResponseDTO getServiceList(String Key);
	
	ResponseDTO getDateWiseTrans(String key,GetTransDTO dto);
	
	ResponseDTO getSuperAdminList(String key);
	
	ResponseDTO addSuperAdmin(String key,SuperAdminAdd sdto);
	
	ResponseDTO changePasswordSuperAdmin(String key,ChangePasswordDto sdto);
	
	ResponseDTO blockSuperAdmin(String key,ChangePasswordDto sdto);

	ResponseDTO getUserList(String Key, String userType);

	BlockUnBlockUserResponse NonKycUpdate(String key, String request);

	ResponseDTO userProfile(String Key, UserInfoRequest req);

	ResponseDTO sendsms(String key, SmsRequest sdto);

	ResponseDTO sendmail(String key, MailRequesrtDTO sdto);

	BlockUnBlockUserResponse unblockUser(String key, String request);

	ResponseDTO sendbulksms(String key, BulkSmsRequest sdto);

	ResponseDTO getUserListFilter(String key, String type, GetTransDTO filter);

	BlockUnBlockUserResponse kycUpdate(String key, String request);

	BlockUnBlockUserResponse blockUser(String key, String request);

	ResponseDTO sendgcm(String key, GCMRequest req);

	ResponseDTO sendbulkmail(String key, BulkMailRequestDTO sdto);
}
