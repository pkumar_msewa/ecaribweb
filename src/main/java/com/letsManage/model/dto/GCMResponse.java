package com.letsManage.model.dto;

import java.util.List;

public class GCMResponse {
private long page;
private List<String> listDTO;
public long getPage() {
	return page;
}
public void setPage(long page) {
	this.page = page;
}
public List<String> getListDTO() {
	return listDTO;
}
public void setListDTO(List<String> listDTO) {
	this.listDTO = listDTO;
}



}
