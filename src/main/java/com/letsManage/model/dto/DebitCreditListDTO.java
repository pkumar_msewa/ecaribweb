package com.letsManage.model.dto;

public class DebitCreditListDTO {
	private long id;

	private String name;

	private String tid;

	private String date;

	private String transactionType;

	private double debitCredit;

	private String status;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}



	public String getTid() {
		return tid;
	}

	public void setTid(String tid) {
		this.tid = tid;
	}






	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}


	
	public double getDebitCredit() {
		return debitCredit;
	}

	public void setDebitCredit(double debitCredit) {
		this.debitCredit = debitCredit;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

}
