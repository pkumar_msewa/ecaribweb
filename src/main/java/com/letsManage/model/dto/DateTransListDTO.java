package com.letsManage.model.dto;

import java.util.List;

public class DateTransListDTO {

private List<UserTransactionListDTO> listDTO;
	
	
	public List<UserTransactionListDTO> getListDTO() {
		return listDTO;
	}

	public void setListDTO(List<UserTransactionListDTO> listDTO) {
		this.listDTO = listDTO;
	}
}
