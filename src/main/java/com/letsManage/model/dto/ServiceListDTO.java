package com.letsManage.model.dto;

public class ServiceListDTO {
	
	
	private long sendMoney;
	
	private double poolAccount;

   public long getSendMoney() {
		return sendMoney;
	}

	public void setSendMoney(long sendMoney) {
		this.sendMoney = sendMoney;
	}

	public double getPoolAccount() {
		return poolAccount;
	}

	public void setPoolAccount(double poolAccount) {
		this.poolAccount = poolAccount;
	}

}
