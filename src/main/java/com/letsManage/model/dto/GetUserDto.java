package com.letsManage.model.dto;

import java.util.List;

import com.payqwikweb.app.model.UserListDTO;

public class GetUserDto {

	
	private List<UserListDTO> listDTO;

	public List<UserListDTO> getListDTO() {
		return listDTO;
	}

	public void setListDTO(List<UserListDTO> listDTO) {
		this.listDTO = listDTO;
	}

	
	
	
}
