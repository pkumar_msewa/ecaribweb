package com.letsManage.model.dto;

import java.util.List;

public class GetUserListCountDTO {

	private long userCount;

	private long transactionCount;
	
     private long debitCount;
	
	private long creditCount;
	
	private List<UserTransactionListDTO> listDTO;
	
	
	public List<UserTransactionListDTO> getListDTO() {
		return listDTO;
	}

	public void setListDTO(List<UserTransactionListDTO> listDTO) {
		this.listDTO = listDTO;
	}

	public long getUserCount() {
		return userCount;
	}

	public void setUserCount(long userCount) {
		this.userCount = userCount;
	}

	public long getTransactionCount() {
		return transactionCount;
	}

	public void setTransactionCount(long transactionCount) {
		this.transactionCount = transactionCount;
	}

	public long getDebitCount() {
		return debitCount;
	}

	public void setDebitCount(long debitCount) {
		this.debitCount = debitCount;
	}

	public long getCreditCount() {
		return creditCount;
	}

	public void setCreditCount(long creditCount) {
		this.creditCount = creditCount;
	}
	
	public transCountChartDTO transDTO;


	public transCountChartDTO getTransDTO() {
		return transDTO;
	}

	public void setTransDTO(transCountChartDTO transDTO) {
		this.transDTO = transDTO;
	}
	
	
	
}
