package com.letsManage.model.request;

import java.io.StringWriter;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonWriter;

public class GetUserList extends AuthenticationRequest implements JSONRequest {

	private String sourceKey;


	public String getSourceKey() {
		return sourceKey;
	}

	public void setSourceKey(String sourceKey) {
		this.sourceKey = sourceKey;
	}
	
	@Override
	public String getJsonRequest() {
		
		JsonObjectBuilder jsonBuilder = Json.createObjectBuilder();
		jsonBuilder.add("key", "");
		jsonBuilder.add("sourceKey", getSourceKey());
		
		JsonObject empObj = jsonBuilder.build();
		StringWriter jsnReqStr = new StringWriter();
		JsonWriter jsonWtr = Json.createWriter(jsnReqStr);
		jsonWtr.writeObject(empObj);
		jsonWtr.close();
		return jsnReqStr.toString();
	}
	
	
}
