package com.vijaya.model;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.payqwikweb.util.CommonUtil;
import com.thirdparty.model.JSONWrapper;

public class FilteredReportDTO implements JSONWrapper{

	private String startDate;
	private String endDate;
	private String paymentMode;
	private String consumerType;
	private String sessionId;
	private String daterange;
	
	
	public String getDaterange() {
		return daterange;
	}

	public void setDaterange(String daterange) {
		this.daterange = daterange;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	public String getConsumerType() {
		return consumerType;
	}

	public void setConsumerType(String consumerType) {
		this.consumerType = consumerType;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	@Override
	public JSONObject toJSON() {
		// TODO Auto-generated method stub
		String[] daterange = null;
		JSONObject json = new JSONObject();
		try {
			if (getDaterange() != null)
				daterange = CommonUtil.getDateRange(getDaterange());
			else {
				daterange = CommonUtil.getDefaultDateRange();
			}
			if (daterange != null) {
				setStartDate(daterange[0]);
				setEndDate(daterange[1]);
			}
			json.put("startDate", getStartDate()+ " 00:00");
			json.put("endDate", getEndDate()+ " 23:59");
			json.put("sessionId", getSessionId());
			json.put("consumerType", getConsumerType());
			json.put("paymentMode", getPaymentMode());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return json;
	}

}
