package com.vijaya.model;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonWriter;
import org.springframework.web.multipart.MultipartFile;

import com.thirdparty.model.JSONRequest;

public class BulkUploadRequest implements JSONRequest {
	private MultipartFile file;
	private MultipartFile otherFile;
	private String fileName;
	private List<ReportRequest> reportDTO;
	private String sessionId;

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}

	public MultipartFile getOtherFile() {
		return otherFile;
	}

	public void setOtherFile(MultipartFile otherFile) {
		this.otherFile = otherFile;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public List<ReportRequest> getReportDTO() {
		return reportDTO;
	}

	public void setReportDTO(List<ReportRequest> reportDTO) {
		this.reportDTO = reportDTO;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	@Override
	public String getJsonRequest() {
		JsonObjectBuilder jsonBuilder = Json.createObjectBuilder();
		JsonArrayBuilder dtoList = Json.createArrayBuilder();
		JsonObjectBuilder mobileDTOs = Json.createObjectBuilder();
		for (int j = 0; j < getReportDTO().size(); j++) {
			mobileDTOs.add("modeOfPayment", getReportDTO().get(j).getModeOfPayment());
			mobileDTOs.add("consumerType", getReportDTO().get(j).getConsumerType());
			mobileDTOs.add("paymentChannel", getReportDTO().get(j).getPaymentChannel());
			mobileDTOs.add("typeOfPayment", getReportDTO().get(j).getTypeOfPayment());
			mobileDTOs.add("transactionType", getReportDTO().get(j).getTransactionType());
			mobileDTOs.add("subDivisonCode", getReportDTO().get(j).getSubDivisonCode());
			mobileDTOs.add("accountId", getReportDTO().get(j).getAccountId());
			mobileDTOs.add("transactionRefId", getReportDTO().get(j).getTransactionRefId());
			mobileDTOs.add("pgId", getReportDTO().get(j).getPgId());
			mobileDTOs.add("vendorRefId", getReportDTO().get(j).getVendorRefId());
			mobileDTOs.add("billAmount", getReportDTO().get(j).getBillAmount());
			mobileDTOs.add("charges", getReportDTO().get(j).getCharges());
			mobileDTOs.add("netAmount", getReportDTO().get(j).getNetAmount());
			mobileDTOs.add("transactionDate", getReportDTO().get(j).getTransactionDate());
			mobileDTOs.add("postedDate", getReportDTO().get(j).getPostedDate());
			mobileDTOs.add("nameOfConsumer", getReportDTO().get(j).getNameOfConsumer());
			mobileDTOs.add("contactNumber", getReportDTO().get(j).getContactNumber());
			mobileDTOs.add("emailId", getReportDTO().get(j).getEmailId());
			mobileDTOs.add("vendorRrn", getReportDTO().get(j).getVendorRrn());
			mobileDTOs.add("status", getReportDTO().get(j).getStatus());

			dtoList.add(mobileDTOs.build());
		}
		jsonBuilder.add("sessionId", getSessionId());
		jsonBuilder.add("mobileDTO", dtoList);

		JsonObject empObj = jsonBuilder.build();

		StringWriter jsnReqStr = new StringWriter();
		JsonWriter jsonWtr = Json.createWriter(jsnReqStr);
		jsonWtr.writeObject(empObj);
		jsonWtr.close();
		return jsnReqStr.toString();
	}

}
