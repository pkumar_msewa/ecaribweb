package com.vijaya.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.payqwikweb.api.IAuthenticationApi;
import com.payqwikweb.app.api.ILogoutApi;
import com.payqwikweb.app.api.IMerchantApi;
import com.payqwikweb.app.api.IUserApi;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.request.AllTransactionRequest;
import com.payqwikweb.app.model.request.AllUserRequest;
import com.payqwikweb.app.model.request.BescomRefundRequest;
import com.payqwikweb.app.model.request.LoginRequest;
import com.payqwikweb.app.model.request.LogoutRequest;
import com.payqwikweb.app.model.request.PagingDTO;
import com.payqwikweb.app.model.request.ReceiptsRequest;
import com.payqwikweb.app.model.request.SessionDTO;
import com.payqwikweb.app.model.request.TransactionFilter;
import com.payqwikweb.app.model.response.AllTransactionResponse;
import com.payqwikweb.app.model.response.GetMerDashValResp;
import com.payqwikweb.app.model.response.LoginResponse;
import com.payqwikweb.app.model.response.LogoutResponse;
import com.payqwikweb.app.model.response.MTransactionResponseDTO;
import com.payqwikweb.app.model.response.ReceiptsResponse;
import com.payqwikweb.app.model.response.TransactionReportResponse;
import com.payqwikweb.app.model.response.UserDetailsResponse;
import com.payqwikweb.app.model.response.UserTransactionResponse;
import com.payqwikweb.model.app.response.MerchantTransactionResponse;
import com.payqwikweb.model.error.LoginError;
import com.payqwikweb.model.web.Status;
import com.payqwikweb.util.Authorities;
import com.payqwikweb.util.CommonUtil;
import com.payqwikweb.util.JSONParserUtil;
import com.payqwikweb.util.ModelMapKey;
import com.payqwikweb.util.StartupUtil;
import com.payqwikweb.validation.CommonValidation;
import com.payqwikweb.validation.LoginValidation;
import com.thirdparty.model.ResponseDTO;
import com.vijaya.api.IMerchantReportApi;
import com.vijaya.model.BulkUploadRequest;
import com.vijaya.model.FilteredReportDTO;
import com.vijaya.model.ReportRequest;

@Controller
@RequestMapping("/Vijaya/Merchant")
public class BescomMerchantController {

	private DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
	private final IMerchantApi merchantApi;
	private final IMerchantReportApi merchantReportApi;
	private final ILogoutApi logoutApi;
	private final LoginValidation loginValidation;
	private final IAuthenticationApi authenticationApi;
	private final IUserApi userApi;

	public BescomMerchantController(IMerchantApi merchantApi,IMerchantReportApi merchantReportApi, ILogoutApi logoutApi, LoginValidation loginValidation,
			IAuthenticationApi authenticationApi, IUserApi userApi) {
		this.merchantApi = merchantApi;
		this.merchantReportApi= merchantReportApi;
		this.logoutApi = logoutApi;
		this.loginValidation = loginValidation;
		this.authenticationApi = authenticationApi;
		this.userApi = userApi;
	}

	@RequestMapping(value = "/Home", method = RequestMethod.GET)
	public String getMerchantLoginPage(@RequestParam(value = "msg", required = false) String message,
			HttpSession session, Model model) {
		LoginRequest dto = new LoginRequest();
		model.addAttribute("login", dto);
		String sessionId = (String) session.getAttribute("msession");
		if (message != null) {
			model.addAttribute(ModelMapKey.MESSAGE, message);
		}
		if (sessionId != null) {
			UserDetailsResponse response = authenticationApi.getUserDetailsFromSession(sessionId);
			if (response != null) {
				String authority = response.getAuthority();
				if (authority != null) {
					if (authority.contains(Authorities.MERCHANT) && authority.contains(Authorities.AUTHENTICATED)) {
						model.addAttribute("user", response);
						return "Vijaya/Home";
					}
				}
			}
		}
		return "Vijaya/Login";
	}

	@RequestMapping(value = "/Home", method = RequestMethod.POST)
	public String getMerchantLoginPage(@ModelAttribute("login") LoginRequest dto, HttpServletRequest request,
			HttpSession session, Model model) {
		dto.setIpAddress(request.getRemoteAddr());
		LoginError error = loginValidation.checkMerchantLogin(dto);
		if (error.isValid()) {
			LoginResponse loginResponse = merchantApi.login(dto);
			if (loginResponse.isSuccess()) {
				model.addAttribute("user", loginResponse);
				session.setAttribute("msession", loginResponse.getSessionId());
				return "Vijaya/Home";
			}
			model.addAttribute(ModelMapKey.MESSAGE, loginResponse.getMessage());
		}
		model.addAttribute("error", error);
		return "Vijaya/Login";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/HomeInAjax")
	public ResponseEntity<GetMerDashValResp> getTotalUsers(HttpServletRequest request, HttpServletResponse response,
			HttpSession session, Model model) throws JSONException {
		GetMerDashValResp entity = new GetMerDashValResp();
		SessionDTO dto = new SessionDTO();
		dto.setSessionId((String) session.getAttribute("ms"));
		UserTransactionResponse userTransactionResponse = merchantApi.getMerchatTransactionValues(dto);
		if (userTransactionResponse != null) {
			if (userTransactionResponse.isSuccess()) {
				JSONObject json = new JSONObject(userTransactionResponse.getResponse());
				if (json != null) {
					JSONObject details = JSONParserUtil.getObject(json, "details");
					entity.setPoolBalance(JSONParserUtil.getDouble(details, "poolBalance"));
					entity.setTotalTxns(JSONParserUtil.getLong(details, "totalTxns"));
					entity.setTotalSuccessTxns(JSONParserUtil.getLong(details, "totalSuccessTxns"));
					entity.setTotalFailedTxns(JSONParserUtil.getLong(details, "totalFailedTxns"));
				}
			}
		}
		return new ResponseEntity<GetMerDashValResp>(entity, HttpStatus.OK);
	}

	@RequestMapping(value = "/Logout", method = RequestMethod.GET)
	public String logoutMerchant(HttpSession session, RedirectAttributes model) {
		String sessionId = (String) session.getAttribute("msession");
		if (sessionId != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.MERCHANT) && authority.contains(Authorities.AUTHENTICATED)) {
					LogoutRequest logoutRequest = new LogoutRequest();
					logoutRequest.setSessionId(sessionId);
					LogoutResponse logoutResponse = logoutApi.logout(logoutRequest, Role.MERCHANT);
					if (logoutResponse.isSuccess()) {
						model.addFlashAttribute(ModelMapKey.MESSAGE, "You've been successfully logged out");
						session.invalidate();
						return "redirect:/Vijaya/Merchant/Home";
					}
				}
			}
		}
		return "Vijaya/Login";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/Transactions")
	public String getTransactionListInJSON(@ModelAttribute PagingDTO dto, HttpSession session, ModelMap model) {
		ReceiptsResponse result = new ReceiptsResponse();
		String sessionId = (String) session.getAttribute("msession");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.MERCHANT) && authority.contains(Authorities.AUTHENTICATED)) {
					ReceiptsRequest receipts = new ReceiptsRequest();
					receipts.setSessionId(sessionId);
					receipts.setPage(0);
					receipts.setSize(1000);
					result = userApi.getBescomMerchantTxns(receipts);
					List<MTransactionResponseDTO> transactionList = new ArrayList<>();
					if (result.isSuccess()) {
						try {
							JSONObject json = new JSONObject(result.getResponse());
							JSONObject details = JSONParserUtil.getObject(json, "details");
							JSONArray content = JSONParserUtil.getArray(details, "content");
							if (content != null) {
								for (int i = 0; i < content.length(); i++) {
									JSONObject temp = content.getJSONObject(i);
									boolean debit = JSONParserUtil.getBoolean(temp, "debit");
									if (true) {
										MTransactionResponseDTO transaction = new MTransactionResponseDTO();
										long milliseconds = JSONParserUtil.getLong(temp, "created");
										Calendar calendar = Calendar.getInstance();
										calendar.setTimeInMillis(milliseconds);
										transaction.setDate(dateFormat.format(calendar.getTime()));
										transaction.setTransactionRefNo(
												JSONParserUtil.getString(temp, "transactionRefNo"));
										transaction.setAmount(JSONParserUtil.getDouble(temp, "amount"));
										transaction.setRetrivalReferenceNo(
												JSONParserUtil.getString(temp, "retrivalReferenceNo"));
										if (JSONParserUtil.getString(temp, "retrivalReferenceNo").equals("null")) {
											transaction.setRetrivalReferenceNo("NA");
										}
										if (JSONParserUtil.getString(temp, "transactionRefNo").equals("null")) {
											transaction.setRetrivalReferenceNo("NA");
										}
										transaction.setStatus(Status.valueOf(JSONParserUtil.getString(temp, "status")));
										transaction.setOrderId(JSONParserUtil.getString(temp, "authReferenceNo"));
										transactionList.add(transaction);
										model.addAttribute("transactionList", transactionList);
									}
								}
							}
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
					return "Vijaya/Transactions";
				}
			}
		}
		return "redirect:/Vijaya/Merchant/Home";
	}

	@RequestMapping(value = "/MTransactions", method = RequestMethod.POST)
	ResponseEntity<AllTransactionResponse> getAllTransactions(SessionDTO sessionDTO) {
		AllTransactionRequest dto = new AllTransactionRequest();
		AllTransactionResponse allTransactionResponse = new AllTransactionResponse();
		try {
			String sessionId = sessionDTO.getSessionId();
			if (sessionId != null) {
				UserDetailsResponse userDetailsResponse = authenticationApi.getUserDetailsFromSession(sessionId);
				if (userDetailsResponse != null) {
					String authority = userDetailsResponse.getAuthority();
					if (authority.contains(Authorities.MERCHANT) && authority.contains(Authorities.AUTHENTICATED)) {
						List<MerchantTransactionResponse> transactionsList = new ArrayList<>();
						dto.setSessionId(sessionId);
						allTransactionResponse = merchantApi.getAllTransaction(dto);
						return new ResponseEntity<AllTransactionResponse>(allTransactionResponse, HttpStatus.OK);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<AllTransactionResponse>(allTransactionResponse, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/TransactionFiltered")
	public String getTransactionList(@ModelAttribute TransactionFilter filter, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) throws ParseException {
		List<MTransactionResponseDTO> transactionList = new ArrayList<>();
		ReceiptsResponse result = new ReceiptsResponse();
		String sessionId = (String) session.getAttribute("msession");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.MERCHANT) && authority.contains(Authorities.AUTHENTICATED)) {
					ReceiptsRequest receipts = new ReceiptsRequest();
					
					String daterange[] = null;
					if (filter != null && !CommonValidation.isNull(filter.getDaterange())) {
						daterange = CommonUtil.getDateRange(filter.getDaterange());
						filter.setStartDate(daterange[0]);
						filter.setEndDate(daterange[1]);
					} else {
						daterange = CommonUtil.getDefaultDateRange();
						filter.setStartDate(daterange[0]);
						filter.setEndDate(daterange[1]);
					}
					
					receipts.setSessionId(sessionId);
					receipts.setPage(0);
					receipts.setSize(100000);
					receipts.setFromDate(filter.getFromDate());
					receipts.setToDate(filter.getToDate());
					result = userApi.getBescomMerchantTxns(receipts);
					if (result.isSuccess()) {
						try {
							JSONObject json = new JSONObject(result.getResponse());
							JSONObject details = JSONParserUtil.getObject(json, "details");
							JSONArray content = JSONParserUtil.getArray(details, "content");
							if (content != null) {
								for (int i = 0; i < content.length(); i++) {
									JSONObject temp = content.getJSONObject(i);
									boolean debit = JSONParserUtil.getBoolean(temp, "debit");
									if (true) {
										MTransactionResponseDTO transaction = new MTransactionResponseDTO();
										long milliseconds = JSONParserUtil.getLong(temp, "created");
										Calendar calendar = Calendar.getInstance();
										calendar.setTimeInMillis(milliseconds);

										transaction.setDate(dateFormat.format(calendar.getTime()));
										transaction.setTransactionRefNo(
												JSONParserUtil.getString(temp, "transactionRefNo"));
										transaction.setAmount(JSONParserUtil.getDouble(temp, "amount"));
										// transaction.setContactNo(JSONParserUtil.getString(temp,
										// "contactNo"));
										// transaction.setEmail(JSONParserUtil.getString(temp,
										// "email"));
										// transaction.setTotalPages(totalPages);
										transaction.setRetrivalReferenceNo(
												JSONParserUtil.getString(temp, "retrivalReferenceNo"));
										transaction.setStatus(Status.valueOf(JSONParserUtil.getString(temp, "status")));
										transaction.setOrderId(JSONParserUtil.getString(temp, "authReferenceNo"));
										transactionList.add(transaction);
										model.addAttribute("transactionList", transactionList);
									}
								}
							}
							return "Vijaya/Transactions";
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
		return "redirect:/Vijaya/Home";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/BulkUpload")
	public String getRequestRefund(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) {
		String sessionId = (String) session.getAttribute("msession");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.MERCHANT) && authority.contains(Authorities.AUTHENTICATED)) {
					return "/Vijaya/ReconReport";
				}

			}
		}
		return "redirect:/Vijaya/Merchant/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/BulkUpload")
	public String submitRequestRefund(@ModelAttribute BulkUploadRequest filter, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model)  {

		String sessionId = (String) session.getAttribute("msession");
		try {
			if (sessionId != null && sessionId.length() != 0) {
				String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
				if (authority != null) {
					if (authority.contains(Authorities.MERCHANT) && authority.contains(Authorities.AUTHENTICATED)) {
						filter.setSessionId(sessionId);
						String rootDirectory = request.getSession().getServletContext().getRealPath("/");
						if(!CommonUtil.isValidCSVFormate(filter.getFile())){
							model.addAttribute("message", "Bad format, please upload csv format only");
							return "Vijaya/ReconReport";
						}
						String path = saveRefundReport(rootDirectory, filter.getFile(), filter.getFileName());
						String file = path.substring(26);
						String fileName = StartupUtil.REPORT_CSV_FILE + file;
						if (fileName != null) {
							List<ReportRequest> sendMoneyList = readFromWalletFile(fileName);
							Iterator<ReportRequest> iterator = sendMoneyList.iterator();
							List<ReportRequest> tokens = new ArrayList<ReportRequest>();
							if (sendMoneyList != null && !sendMoneyList.isEmpty()) {
								while (iterator.hasNext()) {
									ReportRequest dto = iterator.next();
									tokens.add(dto);
								}
								filter.setReportDTO(tokens);
								ResponseDTO responseDTO = merchantReportApi.requestRefund(filter);
								model.addAttribute("message", responseDTO.getMessage());
								return "Vijaya/ReconReport";
							}else{
								model.addAttribute("message", "Data incorrect or found empty data");
								return "Vijaya/ReconReport";
							}
						}
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			model.addAttribute("message", "Internal Server Error");
			return "Vijaya/ReconReport";
		}
		
		return "redirect:/Vijaya/Merchant/Home";
	}

	private String saveRefundReport(String rootDirectory, MultipartFile file, String code) {
		String contentType = file.getContentType();
		String[] fileExtension = contentType.split("/");
		String filePath = null;
		String fileName = String.valueOf(System.currentTimeMillis());
		File dirs = new File(rootDirectory + "/resources/merchantReport/" + file.getOriginalFilename());
		dirs.mkdirs();
		try {
			file.transferTo(dirs);
			filePath = "/resources/merchantReport/" + file.getOriginalFilename();
			return filePath;
		} catch (IOException e) {
			e.printStackTrace();

		}
		return filePath;
	}

	public List<ReportRequest> readFromWalletFile(String fileName) {
		List<ReportRequest> mWallet = new ArrayList<>();
		BufferedReader br = null;
		String line = "";
		try {
			br = new BufferedReader(new FileReader(fileName));
			ReportRequest dto = null;
			while ((line = br.readLine()) != null) {
				Pattern p = Pattern.compile("(([^\"][^,]*)|\"([^\"]*)\"),?");
				Matcher m = p.matcher(line);
				dto = new ReportRequest();
				String value = null;
				int index = 1;
				while (m.find()) {
					if (m.group(2) != null) {
						value = m.group(2);
					}
					if (m.group(3) != null) {
						value = m.group(3);
					}
					if (value != null) {
						if (dto != null) {
							switch (index) {
							case 1:
								dto.setModeOfPayment(value);
								break;
							case 2:
								dto.setConsumerType(value);
								break;
							case 3:
								dto.setPaymentChannel(value);
								break;
							case 4:
								dto.setTypeOfPayment(value);
								break;
							case 5:
								dto.setTransactionType(value);
								break;
							case 6:
								dto.setSubDivisonCode(value);
								break;
							case 7:
								dto.setAccountId(value);
								break;
							case 8:
								dto.setTransactionRefId(value);
								break;
							case 9:
								dto.setPgId(value);
								break;
							case 10:
								dto.setVendorRefId(value);
								break;
							case 11:
								dto.setBillAmount(Double.parseDouble(value));
								break;
							case 12:
								dto.setCharges(Double.parseDouble(value));
								break;
							case 13:
								dto.setNetAmount(Double.parseDouble(value));
								break;
							case 14:
								dto.setTransactionDate(value);
								break;		
							case 15:
								dto.setPostedDate(value);
								break;
							case 16:
								dto.setNameOfConsumer(value);
								break;
							case 17:
								dto.setContactNumber(value);
								break;
							case 18:
								dto.setEmailId(value);
								break;
							case 19:
								dto.setVendorRrn(value);
								break;
							case 20:
								dto.setStatus(value);
								break;	
							default:
								break;
							}
							index = index + 1;
						}
					}
				}
				mWallet.add(dto);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return mWallet;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/MerchantReport")
	public String getUPITransaction(@ModelAttribute PagingDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) {
		String sessionCheck = (String) session.getAttribute("msession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.MERCHANT) && authority.contains(Authorities.AUTHENTICATED)) {
					return "Vijaya/MerchantReport";
				}
			}
		}
		return "redirect:/Vijaya/Merchant/Home";
	}
	
	
	@RequestMapping(method = RequestMethod.POST, value = "/FliteredMerchantReport")
	public String getFilteredTransaction(@ModelAttribute FilteredReportDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) {
		String sessionCheck = (String) session.getAttribute("msession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.MERCHANT) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionCheck);
					TransactionReportResponse listResponse = merchantReportApi.getFilteredMerchantReport(dto);
					if (listResponse.isSuccess()) {
						model.addAttribute("transactionList", listResponse.getReportList());
					} else {
						model.addAttribute(ModelMapKey.ERROR, listResponse.getMessage());
					}
					return "Vijaya/FilteredMerchantReport";
				}
			}
		}
		return "redirect:/Vijaya/Merchant/Home";
	}
}