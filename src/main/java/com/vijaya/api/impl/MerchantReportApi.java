package com.vijaya.api.impl;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jettison.json.JSONObject;

import com.payqwikweb.app.metadatas.UrlMetadatas;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Language;
import com.payqwikweb.app.model.ResponseStatus;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.Version;
import com.payqwikweb.app.model.request.AllUserRequest;
import com.payqwikweb.app.model.response.TransactionReportResponse;
import com.payqwikweb.app.utils.SecurityUtils;
import com.payqwikweb.util.APIUtils;
import com.payqwikweb.util.JSONParserUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.thirdparty.model.ResponseDTO;
import com.vijaya.api.IMerchantReportApi;
import com.vijaya.model.BulkUploadRequest;
import com.vijaya.model.FilteredReportDTO;
import com.vijaya.model.ReportRequest;

public class MerchantReportApi implements IMerchantReportApi {

	@Override
	public ResponseDTO requestRefund(BulkUploadRequest upload) {
		ResponseDTO resp = new ResponseDTO();
		try {
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getMechantBulkUploadUrl(Version.VERSION_1, Role.MERCHANT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(upload.toString()))
					.post(ClientResponse.class, upload.getJsonRequest());
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						resp.setStatus(jobj.getString("status"));
						resp.setCode(jobj.getString("code"));
						resp.setMessage(jobj.getString("message"));
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
		}
		return resp;
	}
	
	@Override
	public TransactionReportResponse getMerchantReport(AllUserRequest userRequest) {
		TransactionReportResponse resp = new TransactionReportResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", userRequest.getSessionId());
			Client client = Client.create();
			WebResource webResource = client
					.resource(UrlMetadatas.getMerchantReport(Version.VERSION_1, Role.MERCHANT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (code.equalsIgnoreCase("S00") && jobj.getString("details") != null
								&& jobj.getString("details").contains("[")) {
							List<ReportRequest> reports = new ArrayList<>();
							resp.setSuccess(true);
							org.json.JSONArray details = JSONParserUtil.getArray(jobj, "details");
							if (details != null && details.length() > 0) {
								for (int i = 0; i < details.length(); i++) {
									org.json.JSONObject json = details.getJSONObject(i);
									ReportRequest dto = new ReportRequest();
									dto.setModeOfPayment(JSONParserUtil.getString(json, "modeOfPayment"));
									dto.setConsumerType(JSONParserUtil.getString(json, "consumerType"));
									dto.setPaymentChannel(JSONParserUtil.getString(json, "paymentChannel"));
									dto.setTypeOfPayment(JSONParserUtil.getString(json, "typeOfPayment"));
									dto.setTransactionType(JSONParserUtil.getString(json, "transactionType"));
									dto.setSubDivisonCode(JSONParserUtil.getString(json, "subDivisonCode"));
									dto.setAccountId(JSONParserUtil.getString(json, "accountId"));
									dto.setTransactionRefId(JSONParserUtil.getString(json, "transactionRefId"));
									dto.setPgId(JSONParserUtil.getString(json, "pgId"));
									dto.setVendorRefId(JSONParserUtil.getString(json, "vendorRefId"));
									dto.setBillAmount(JSONParserUtil.getDouble(json, "billAmount"));
									dto.setCharges(JSONParserUtil.getDouble(json, "charges"));
									dto.setNetAmount(JSONParserUtil.getDouble(json, "netAmount"));
									dto.setTransactionDate(JSONParserUtil.getString(json, "transactionDate"));
									dto.setPostedDate(JSONParserUtil.getString(json, "postedDate"));
									dto.setNameOfConsumer(JSONParserUtil.getString(json, "nameOfConsumer"));
									dto.setContactNumber(JSONParserUtil.getString(json, "contactNumber"));
									dto.setEmailId(JSONParserUtil.getString(json, "emailId"));
									dto.setVendorRrn(JSONParserUtil.getString(json, "vendorRrn"));	
									reports.add(dto);
								}
								resp.setAmount(jobj.getLong("count"));
								resp.setReportList(reports);
							} else {
								resp.setSuccess(false);
								resp.setMessage("Transaction List Empty");
							}
						} else {
							resp.setSuccess(false);
							resp.setMessage("Please try Later");
						}
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					// resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			// resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}
	@Override
	public TransactionReportResponse getFilteredMerchantReport(FilteredReportDTO request) {
		TransactionReportResponse resp = new TransactionReportResponse();
		try {
			Client client = Client.create();
			WebResource webResource = client
					.resource(UrlMetadatas.getFilteredMerchantReport(Version.VERSION_1, Role.MERCHANT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("12345")).post(ClientResponse.class, request.toJSON());
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (code.equalsIgnoreCase("S00") && jobj.getString("details") != null
								&& jobj.getString("details").contains("[")) {
							List<ReportRequest> reports = new ArrayList<>();
							resp.setSuccess(true);
							org.json.JSONArray details = JSONParserUtil.getArray(jobj, "details");
							if (details != null && details.length() > 0) {
								for (int i = 0; i < details.length(); i++) {
									org.json.JSONObject json = details.getJSONObject(i);
									ReportRequest dto = new ReportRequest();
									dto.setModeOfPayment(JSONParserUtil.getString(json, "modeOfPayment"));
									dto.setConsumerType(JSONParserUtil.getString(json, "consumerType"));
									dto.setPaymentChannel(JSONParserUtil.getString(json, "paymentChannel"));
									dto.setTypeOfPayment(JSONParserUtil.getString(json, "typeOfPayment"));
									dto.setTransactionType(JSONParserUtil.getString(json, "transactionType"));
									dto.setSubDivisonCode(JSONParserUtil.getString(json, "subDivisonCode"));
									dto.setAccountId(JSONParserUtil.getString(json, "accountId"));
									dto.setTransactionRefId(JSONParserUtil.getString(json, "transactionRefId"));
									dto.setPgId(JSONParserUtil.getString(json, "pgId"));
									dto.setVendorRefId(JSONParserUtil.getString(json, "vendorRefId"));
									dto.setBillAmount(JSONParserUtil.getDouble(json, "billAmount"));
									dto.setCharges(JSONParserUtil.getDouble(json, "charges"));
									dto.setNetAmount(JSONParserUtil.getDouble(json, "netAmount"));
									dto.setTransactionDate(JSONParserUtil.getString(json, "transactionDate"));
									dto.setPostedDate(JSONParserUtil.getString(json, "postedDate"));
									dto.setNameOfConsumer(JSONParserUtil.getString(json, "nameOfConsumer"));
									dto.setContactNumber(JSONParserUtil.getString(json, "contactNumber"));
									dto.setEmailId(JSONParserUtil.getString(json, "emailId"));
									dto.setVendorRrn(JSONParserUtil.getString(json, "vendorRrn"));
									dto.setStatus(JSONParserUtil.getString(json, "status"));
									reports.add(dto);
								}
								resp.setAmount(jobj.getLong("count"));
								resp.setReportList(reports);
							} else {
								resp.setSuccess(false);
								resp.setMessage("Transaction List Empty");
							}
						} else {
							resp.setSuccess(false);
							resp.setMessage("Please try Later");
						}
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					// resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			// resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

}
