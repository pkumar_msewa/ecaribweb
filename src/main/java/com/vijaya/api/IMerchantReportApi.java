package com.vijaya.api;

import com.payqwikweb.app.model.request.AllUserRequest;
import com.payqwikweb.app.model.response.TransactionReportResponse;
import com.thirdparty.model.ResponseDTO;
import com.vijaya.model.BulkUploadRequest;
import com.vijaya.model.FilteredReportDTO;

public interface IMerchantReportApi {
	
	ResponseDTO requestRefund(BulkUploadRequest upload);

	TransactionReportResponse getMerchantReport(AllUserRequest userRequest);

	TransactionReportResponse getFilteredMerchantReport(FilteredReportDTO request);

}
