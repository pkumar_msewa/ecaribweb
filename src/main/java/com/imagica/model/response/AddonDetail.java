package com.imagica.model.response;

import java.util.List;

public class AddonDetail {

    private long typeId;
    private String type;
    private String currency;
    private long quantity;
    private double totalAddOnCost;
    private double addOnCost;
    private String title;
    private long aid;
    private String imageUrl;
    private List<TaxDetails> taxDetails;

    public long getTypeId() {
        return typeId;
    }

    public void setTypeId(long typeId) {
        this.typeId = typeId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public double getTotalAddOnCost() {
        return totalAddOnCost;
    }

    public void setTotalAddOnCost(double totalAddOnCost) {
        this.totalAddOnCost = totalAddOnCost;
    }

    public double getAddOnCost() {
        return addOnCost;
    }

    public void setAddOnCost(double addOnCost) {
        this.addOnCost = addOnCost;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getAid() {
        return aid;
    }

    public void setAid(long aid) {
        this.aid = aid;
    }


    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public List<TaxDetails> getTaxDetails() {
        return taxDetails;
    }

    public void setTaxDetails(List<TaxDetails> taxDetails) {
        this.taxDetails = taxDetails;
    }

    @Override
    public String toString() {
        return "AddonDetail{" +
                "typeId=" + typeId +
                ", type='" + type + '\'' +
                ", currency='" + currency + '\'' +
                ", quantity=" + quantity +
                ", totalAddOnCost=" + totalAddOnCost +
                ", addOnCost=" + addOnCost +
                ", title='" + title + '\'' +
                ", aid=" + aid +
                ", imageUrl='" + imageUrl + '\'' +
                ", taxDetails=" + taxDetails +
                '}';
    }
}
