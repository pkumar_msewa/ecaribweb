package com.imagica.model.response;

public class CarAddonDetail {

    private long quantity;
    private String addOnName;
    private long addOnId;

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public String getAddOnName() {
        return addOnName;
    }

    public void setAddOnName(String addOnName) {
        this.addOnName = addOnName;
    }

    public long getAddOnId() {
        return addOnId;
    }

    public void setAddOnId(long addOnId) {
        this.addOnId = addOnId;
    }

    @Override
    public String toString() {
        return "CarAddonDetail{" +
                "quantity=" + quantity +
                ", addOnName='" + addOnName + '\'' +
                ", addOnId=" + addOnId +
                '}';
    }
}
