package com.imagica.model.response;

public class TicketDetail {
    private String passengerType;
    private long quantity;

    public String getPassengerType() {
        return passengerType;
    }

    public void setPassengerType(String passengerType) {
        this.passengerType = passengerType;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "TicketDetail{" +
                "passengerType='" + passengerType + '\'' +
                ", quantity=" + quantity +
                '}';
    }
}
