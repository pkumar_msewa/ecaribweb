package com.imagica.model.response;

public class BrunchDetail {

    private long quantity;
    private String addOneName;
    private long addOnId;

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public String getAddOneName() {
        return addOneName;
    }

    public void setAddOneName(String addOneName) {
        this.addOneName = addOneName;
    }

    public long getAddOnId() {
        return addOnId;
    }

    public void setAddOnId(long addOnId) {
        this.addOnId = addOnId;
    }

    @Override
    public String toString() {
        return "BrunchDetail{" +
                "quantity=" + quantity +
                ", addOneName='" + addOneName + '\'' +
                ", addOnId=" + addOnId +
                '}';
    }
}
