package com.imagica.model.response;

import java.util.List;

public class TicketType {

    private String type;
    private long quantity;
    private double costPerQty;
    private double totalCost;
    private long productId;
    private long plu;
    List<TaxDetails> taxDetailsList;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public double getCostPerQty() {
        return costPerQty;
    }

    public void setCostPerQty(double costPerQty) {
        this.costPerQty = costPerQty;
    }

    public double getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(double totalCost) {
        this.totalCost = totalCost;
    }

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public long getPlu() {
        return plu;
    }

    public void setPlu(long plu) {
        this.plu = plu;
    }

    public List<TaxDetails> getTaxDetailsList() {
        return taxDetailsList;
    }

    public void setTaxDetailsList(List<TaxDetails> taxDetailsList) {
        this.taxDetailsList = taxDetailsList;
    }


    @Override
    public String toString() {
        return "TicketType{" +
                "type='" + type + '\'' +
                ", quantity=" + quantity +
                ", costPerQty=" + costPerQty +
                ", totalCost=" + totalCost +
                ", productId=" + productId +
                ", plu=" + plu +
                ", taxDetailsList=" + taxDetailsList +
                '}';
    }
}
