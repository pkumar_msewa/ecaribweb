package com.imagica.model.response;

import java.util.List;

public class SnowMagicaAddOn {
    private String prodType;
    private String ticketDescription;
    private String title;
    private String imageUrl;
    private String currency;
    private List<ImagicaSession> sessionList;
    private TicketType ticketType;

    public String getProdType() {
        return prodType;
    }

    public void setProdType(String prodType) {
        this.prodType = prodType;
    }

    public String getTicketDescription() {
        return ticketDescription;
    }

    public void setTicketDescription(String ticketDescription) {
        this.ticketDescription = ticketDescription;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public List<ImagicaSession> getSessionList() {
        return sessionList;
    }

    public void setSessionList(List<ImagicaSession> sessionList) {
        this.sessionList = sessionList;
    }

    public TicketType getTicketType() {
        return ticketType;
    }

    public void setTicketType(TicketType ticketType) {
        this.ticketType = ticketType;
    }
}
