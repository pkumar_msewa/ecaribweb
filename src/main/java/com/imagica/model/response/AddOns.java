package com.imagica.model.response;

import java.util.List;

public class AddOns {

    private List<GCarAddonDetail> carAddonDetailList;
    private List<BusAddonDetail> busAddonDetailList;
    private PhotoAddonDetail photoAddonDetail;
    private SnowMagicaAddOn snowMagicaAddOn;
    private List<GBrunchDetail> brunchAddon;

    public List<GCarAddonDetail> getCarAddonDetailList() {
        return carAddonDetailList;
    }

    public void setCarAddonDetailList(List<GCarAddonDetail> carAddonDetailList) {
        this.carAddonDetailList = carAddonDetailList;
    }

    public List<BusAddonDetail> getBusAddonDetailList() {
        return busAddonDetailList;
    }

    public void setBusAddonDetailList(List<BusAddonDetail> busAddonDetailList) {
        this.busAddonDetailList = busAddonDetailList;
    }

    public PhotoAddonDetail getPhotoAddonDetail() {
        return photoAddonDetail;
    }

    public void setPhotoAddonDetail(PhotoAddonDetail photoAddonDetail) {
        this.photoAddonDetail = photoAddonDetail;
    }

    public SnowMagicaAddOn getSnowMagicaAddOn() {
        return snowMagicaAddOn;
    }

    public void setSnowMagicaAddOn(SnowMagicaAddOn snowMagicaAddOn) {
        this.snowMagicaAddOn = snowMagicaAddOn;
    }

    public List<GBrunchDetail> getBrunchAddon() {
        return brunchAddon;
    }

    public void setBrunchAddon(List<GBrunchDetail> brunchAddon) {
        this.brunchAddon = brunchAddon;
    }
}
