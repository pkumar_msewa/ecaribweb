package com.imagica.model.response;

import java.util.List;

public class SearchResponse extends BaseResponse{
    private List<PackageDetail> packages;
    private List<Tickets> tickets;
    private String visitDate;
    private String departureDate;
    private String businessUnit;
    private AddOns addOns;
    List<String> errorMsgs;

    public List<String> getErrorMsgs() {
        return errorMsgs;
    }

    public void setErrorMsgs(List<String> errorMsgs) {
        this.errorMsgs = errorMsgs;
    }

    public AddOns getAddOns() {
        return addOns;
    }

    public void setAddOns(AddOns addOns) {
        this.addOns = addOns;
    }

    public List<PackageDetail> getPackages() {
        return packages;
    }

    public void setPackages(List<PackageDetail> packages) {
        this.packages = packages;
    }

    public List<Tickets> getTickets() {
        return tickets;
    }

    public void setTickets(List<Tickets> tickets) {
        this.tickets = tickets;
    }

    public String getVisitDate() {
        return visitDate;
    }

    public void setVisitDate(String visitDate) {
        this.visitDate = visitDate;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public String getBusinessUnit() {
        return businessUnit;
    }

    public void setBusinessUnit(String businessUnit) {
        this.businessUnit = businessUnit;
    }


}
