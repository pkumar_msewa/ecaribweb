package com.imagica.model.response;

import java.util.List;

public class ImagicaSession {

    private long sessionId;
    private String sessionName;
    private List<ImagicaTimeSlots> timeSlots;

    public long getSessionId() {
        return sessionId;
    }

    public void setSessionId(long sessionId) {
        this.sessionId = sessionId;
    }

    public String getSessionName() {
        return sessionName;
    }

    public void setSessionName(String sessionName) {
        this.sessionName = sessionName;
    }

    public List<ImagicaTimeSlots> getTimeSlots() {
        return timeSlots;
    }

    public void setTimeSlots(List<ImagicaTimeSlots> timeSlots) {
        this.timeSlots = timeSlots;
    }
}
