package com.imagica.model.response;

import java.util.List;

public class OrderResponse extends BaseResponse{

    private String uid;
    private String orderId;
    private double totalAmount;
    private double baseAmount;
    private double serviceTax;
    private double sbCess;
    private double kkCess;
    private String currencyCode;
    private List<String> errorMsgs;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public double getBaseAmount() {
        return baseAmount;
    }

    public void setBaseAmount(double baseAmount) {
        this.baseAmount = baseAmount;
    }

    public double getServiceTax() {
        return serviceTax;
    }

    public void setServiceTax(double serviceTax) {
        this.serviceTax = serviceTax;
    }

    public double getSbCess() {
        return sbCess;
    }

    public void setSbCess(double sbCess) {
        this.sbCess = sbCess;
    }

    public double getKkCess() {
        return kkCess;
    }

    public void setKkCess(double kkCess) {
        this.kkCess = kkCess;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public List<String> getErrorsMsgs() {
        return errorMsgs;
    }

    public void setErrorsList(List<String> errorsMsgs) {
        this.errorMsgs = errorsMsgs;
    }
}
