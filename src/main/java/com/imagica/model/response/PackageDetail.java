package com.imagica.model.response;

import java.util.List;

public class PackageDetail {
    private String imageUrl;
    private String description;
    private String title;
    private long quantity;
    private double costPer;
    private double costTotal;
    private String currency;
    private long productId;
    private TravelDetail travelDetail;
    private TicketDetail ticketDetail;
    private BrunchDetail brunchDetail;
    private CarAddonDetail carAddonDetail;
    private List<TaxDetails> taxDetailList;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public double getCostPer() {
        return costPer;
    }

    public void setCostPer(double costPer) {
        this.costPer = costPer;
    }

    public double getCostTotal() {
        return costTotal;
    }

    public void setCostTotal(double costTotal) {
        this.costTotal = costTotal;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public TravelDetail getTravelDetail() {
        return travelDetail;
    }

    public void setTravelDetail(TravelDetail travelDetail) {
        this.travelDetail = travelDetail;
    }

    public TicketDetail getTicketDetail() {
        return ticketDetail;
    }

    public void setTicketDetail(TicketDetail ticketDetail) {
        this.ticketDetail = ticketDetail;
    }

    public BrunchDetail getBrunchDetail() {
        return brunchDetail;
    }

    public void setBrunchDetail(BrunchDetail brunchDetail) {
        this.brunchDetail = brunchDetail;
    }

    public CarAddonDetail getCarAddonDetail() {
        return carAddonDetail;
    }

    public void setCarAddonDetail(CarAddonDetail carAddonDetail) {
        this.carAddonDetail = carAddonDetail;
    }

    public List<TaxDetails> getTaxDetailList() {
        return taxDetailList;
    }

    public void setTaxDetailList(List<TaxDetails> taxDetailList) {
        this.taxDetailList = taxDetailList;
    }

    @Override
    public String toString() {
        return "PackageDetail{" +
                "imageUrl='" + imageUrl + '\'' +
                ", description='" + description + '\'' +
                ", title='" + title + '\'' +
                ", quantity=" + quantity +
                ", costPer=" + costPer +
                ", costTotal=" + costTotal +
                ", currency='" + currency + '\'' +
                ", productId=" + productId +
                ", travelDetail=" + travelDetail +
                ", ticketDetail=" + ticketDetail +
                ", brunchDetail=" + brunchDetail +
                ", carAddonDetail=" + carAddonDetail +
                ", taxDetailList=" + taxDetailList +
                '}';
    }
}
