package com.imagica.model.response;

import java.util.List;

public class Tickets {

    private String description;
    private String productType;
    private String title;
    private String image;
    private String terms;
    private String currency;
    private List<TicketType> ticketTypeList;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTerms() {
        return terms;
    }

    public void setTerms(String terms) {
        this.terms = terms;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public List<TicketType> getTicketTypeList() {
        return ticketTypeList;
    }

    public void setTicketTypeList(List<TicketType> ticketTypeList) {
        this.ticketTypeList = ticketTypeList;
    }

    @Override
    public String toString() {
        return "Tickets{" +
                "description='" + description + '\'' +
                ", productType='" + productType + '\'' +
                ", title='" + title + '\'' +
                ", image='" + image + '\'' +
                ", terms='" + terms + '\'' +
                ", currency='" + currency + '\'' +
                ", ticketTypeList=" + ticketTypeList +
                '}';
    }
}
