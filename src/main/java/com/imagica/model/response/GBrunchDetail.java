package com.imagica.model.response;

import java.util.List;

public class GBrunchDetail {

    private String currencyCode;
    private long productId;
    private long quantity;
    private double totalCost;
    private double costPer;
    private String imageUrl;
    private String description;
    private String title;
    private long aid;
    private String bruchTitle;
    private List<TaxDetails> taxDetailList;

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public double getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(double totalCost) {
        this.totalCost = totalCost;
    }

    public double getCostPer() {
        return costPer;
    }

    public void setCostPer(double costPer) {
        this.costPer = costPer;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getAid() {
        return aid;
    }

    public void setAid(long aid) {
        this.aid = aid;
    }

    public String getBruchTitle() {
        return bruchTitle;
    }

    public void setBruchTitle(String bruchTitle) {
        this.bruchTitle = bruchTitle;
    }

    public List<TaxDetails> getTaxDetailList() {
        return taxDetailList;
    }

    public void setTaxDetailList(List<TaxDetails> taxDetailList) {
        this.taxDetailList = taxDetailList;
    }

    @Override
    public String toString() {
        return "GBrunchDetail{" +
                "currencyCode='" + currencyCode + '\'' +
                ", productId=" + productId +
                ", quantity=" + quantity +
                ", totalCost=" + totalCost +
                ", costPer=" + costPer +
                ", imageUrl='" + imageUrl + '\'' +
                ", description='" + description + '\'' +
                ", title='" + title + '\'' +
                ", aid=" + aid +
                ", bruchTitle='" + bruchTitle + '\'' +
                ", taxDetailList=" + taxDetailList +
                '}';
    }
}
