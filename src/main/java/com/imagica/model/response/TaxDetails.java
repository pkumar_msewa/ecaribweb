package com.imagica.model.response;

public class TaxDetails {

    private String name;
    private double pricePerQty;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPricePerQty() {
        return pricePerQty;
    }

    public void setPricePerQty(double pricePerQty) {
        this.pricePerQty = pricePerQty;
    }

    @Override
    public String toString() {
        return "TaxDetails{" +
                "name='" + name + '\'' +
                ", pricePerQty=" + pricePerQty +
                '}';
    }
}
