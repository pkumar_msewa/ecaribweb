package com.imagica.model.response;

import java.util.List;

public class BusAddonDetail extends AddonDetail{

    private String placeId;
    private String placeName;
    private List<ImagicaBusLocation> locationList;

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    public List<ImagicaBusLocation> getLocationList() {
        return locationList;
    }

    public void setLocationList(List<ImagicaBusLocation> locationList) {
        this.locationList = locationList;
    }

    @Override
    public String toString() {
        return "BusAddonDetail{" +
                "placeId='" + placeId + '\'' +
                ", placeName='" + placeName + '\'' +
                ", locationList=" + locationList +
                '}';
    }
}
