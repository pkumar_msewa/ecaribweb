package com.imagica.model.response;

import java.util.List;

public class PaymentResponse extends BaseResponse{
    private List<String> errorMsg;
    private String profileId;

    public List<String> getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(List<String> errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getProfileId() {
        return profileId;
    }

    public void setProfileId(String profileId) {
        this.profileId = profileId;
    }
}
