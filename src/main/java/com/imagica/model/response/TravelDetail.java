package com.imagica.model.response;

public class TravelDetail {

    private boolean carExists;
    private boolean busExists;

    public boolean isCarExists() {
        return carExists;
    }

    public void setCarExists(boolean carExists) {
        this.carExists = carExists;
    }

    public boolean isBusExists() {
        return busExists;
    }

    public void setBusExists(boolean busExists) {
        this.busExists = busExists;
    }


    @Override
    public String toString() {
        return "TravelDetail{" +
                "carExists=" + carExists +
                ", busExists=" + busExists +
                '}';
    }
}
