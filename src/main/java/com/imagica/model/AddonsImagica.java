package com.imagica.model;

public class AddonsImagica {

	private String type;
	private int quantity;
	private int typeId;
	private String title;
	private long aid;
	private String imageUrl;
	private double addOnCost;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public int getTypeId() {
		return typeId;
	}
	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public long getAid() {
		return aid;
	}
	public void setAid(long aid) {
		this.aid = aid;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public double getAddOnCost() {
		return addOnCost;
	}
	public void setAddOnCost(double addOnCost) {
		this.addOnCost = addOnCost;
	}
	
	
	
}
