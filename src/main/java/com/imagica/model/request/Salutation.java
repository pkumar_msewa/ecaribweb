package com.imagica.model.request;

public enum Salutation {
    MR("Mr."),MRS("Mrs."),MS("Ms.");
    private String key;
    Salutation(String key) {
        this.key = key;
    }
    public String getKey(){
        return key;
    }
}
