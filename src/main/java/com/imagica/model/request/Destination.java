package com.imagica.model.request;

public enum Destination {
    THEME_PARK("Theme Park"),WATER_PARK("Water Park"),SNOMAGICA("Snomagica"),COMBO_PARK("Combo Park");
    private String value;

    Destination(String value){
        this.value = value;
    }

    public String getValue(){
        return value;
    }

}
