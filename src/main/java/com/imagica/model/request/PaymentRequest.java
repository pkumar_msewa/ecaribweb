package com.imagica.model.request;

import com.payqwikweb.model.web.ImagicaAuthDTO;

public class PaymentRequest extends ImagicaAuthDTO {

    private String sessionId;
    private String uid;
    private String amount;
    private String orderId;
    private static final String PAYMENT_METHOD = "wallet";
    private String transactionId;
    private static final String status = "success";
    private static final String CURRENCY = "INR";
    private Salutation salutation;
    private String firstName;
    private String lastName;
    private String emailId;
    private String dob;
    private String mobileNo;
    private String address;
    private String locationCode;
    private String country = "IN";
    private String state;
    private String city;
    private boolean infantPresent;
    private String noOfInfants;

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public Salutation getSalutation() {
        return salutation;
    }

    public void setSalutation(Salutation salutation) {
        this.salutation = salutation;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public static String getPaymentMethod() {
        return PAYMENT_METHOD;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public static String getStatus() {
        return status;
    }

    public static String getCURRENCY() {
        return CURRENCY;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLocationCode() {
        return locationCode;
    }

    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public boolean isInfantPresent() {
        return infantPresent;
    }

    public void setInfantPresent(boolean infantPresent) {
        this.infantPresent = infantPresent;
    }

    public String getNoOfInfants() {
        return noOfInfants;
    }

    public void setNoOfInfants(String noOfInfants) {
        this.noOfInfants = noOfInfants;
    }
}
