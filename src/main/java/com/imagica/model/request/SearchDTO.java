package com.imagica.model.request;

import com.payqwikweb.model.web.ImagicaAuthDTO;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SearchDTO extends ImagicaAuthDTO {
    private String sessionId;
    private DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    private long adult=1;
    private long child;
    private long senior;
    private long college;
    private long totalDays = 1;
    private String dateVisit;
    private String dateDeparture ;
    private Destination  destination;
    private final static String VERSION = "old";

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public long getAdult() {
        return adult;
    }

    public void setAdult(long adult) {
        this.adult = adult;
    }

    public long getChild() {
        return child;
    }

    public void setChild(long child) {
        this.child = child;
    }

    public long getSenior() {
        return senior;
    }

    public void setSenior(long senior) {
        this.senior = senior;
    }

    public long getCollege() {
        return college;
    }

    public void setCollege(long college) {
        this.college = college;
    }

    public long getTotalDays() {
        return totalDays;
    }

    public void setTotalDays(long totalDays) {
        this.totalDays = totalDays;
    }


    public void setDateVisit(String dateVisit) {
        this.dateVisit = dateVisit;
    }

    public String getDateVisit() {
        return dateVisit;
    }

    public String getDateDeparture() {
        return dateDeparture;
    }

    public void setDateDeparture(String dateDeparture) {
        this.dateDeparture = dateDeparture;
    }

    public Destination getDestination() {
        return destination;
    }

    public void setDestination(Destination destination) {
        this.destination = destination;
    }

    public static String getVERSION() {
        return VERSION;
    }
}
