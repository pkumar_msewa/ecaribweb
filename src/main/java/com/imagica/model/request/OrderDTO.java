package com.imagica.model.request;

import com.payqwikweb.model.web.ImagicaAuthDTO;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.NameList;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OrderDTO extends ImagicaAuthDTO {

    private String sessionId;
    private Destination destination;
    private long totaldays;
    private Map<String,Integer> productQuantityMap;
    private List<CarPassengerDetails> carPassengerList;
    private SnowMagicaOrder snowMagicaOrder;
    private CarDetails carDetails;
    private String visitDate;
    private String departureDate;
    private BusOrder busOrder;
    private String coupon;


    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getCoupon() {
        return coupon;
    }

    public void setCoupon(String coupon) {
        this.coupon = coupon;
    }

    public BusOrder getBusOrder() {
        return busOrder;
    }

    public void setBusOrder(BusOrder busOrder) {
        this.busOrder = busOrder;
    }

    public SnowMagicaOrder getSnowMagicaOrder() {
        return snowMagicaOrder;
    }

    public void setSnowMagicaOrder(SnowMagicaOrder snowMagicaOrder) {
        this.snowMagicaOrder = snowMagicaOrder;
    }

    public Destination getDestination() {
        return destination;
    }

    public void setDestination(Destination destination) {
        this.destination = destination;
    }

    public long getTotaldays() {
        return totaldays;
    }

    public void setTotaldays(long totaldays) {
        this.totaldays = totaldays;
    }

    public Map<String, Integer> getProductQuantityMap() {
        return productQuantityMap;
    }


    public CarDetails getCarDetails() {
        return carDetails;
    }

    public void setCarDetails(CarDetails carDetails) {
        this.carDetails = carDetails;
    }

    public String getVisitDate() {
        return visitDate;
    }

    public void setProductQuantityMap(Map<String, Integer> productQuantityMap) {
        this.productQuantityMap = productQuantityMap;
    }

    public void setVisitDate(String visitDate) {
        this.visitDate = visitDate;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public List<CarPassengerDetails> getCarPassengerList() {
        return carPassengerList;
    }

    public void setCarPassengerList(List<CarPassengerDetails> carPassengerList) {
        this.carPassengerList = carPassengerList;
    }

    public static void main(String... args) throws IOException {
        OrderDTO order = new OrderDTO();
        BusOrder busOrder = new BusOrder();
        busOrder.setLocationId(7805);
        busOrder.setPlaceId(7822);
        order.setBusOrder(busOrder);
        order.setSessionId("EBB7EAB4826979CA824BD2740C7F47F0");
        Map<String,Integer> productQty = new HashMap<>();
        productQty.put("590",1);
        productQty.put("66",1);
        order.setProductQuantityMap(productQty);
        order.setDestination(Destination.THEME_PARK);
        ObjectMapper mapper = new ObjectMapper();
        System.err.println(mapper.writeValueAsString(order));
    }
}
