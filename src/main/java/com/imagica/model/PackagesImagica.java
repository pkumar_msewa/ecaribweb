package com.imagica.model;

public class PackagesImagica {

	private String imageUrl;
	private String description;
	private String title;
	private int quantity;
	private double costPer;
	private double costTotal;
	private long productId;
	
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public double getCostPer() {
		return costPer;
	}
	public void setCostPer(double costPer) {
		this.costPer = costPer;
	}
	public double getCostTotal() {
		return costTotal;
	}
	public void setCostTotal(double costTotal) {
		this.costTotal = costTotal;
	}
	public long getProductId() {
		return productId;
	}
	public void setProductId(long productId) {
		this.productId = productId;
	}
	
	
}
