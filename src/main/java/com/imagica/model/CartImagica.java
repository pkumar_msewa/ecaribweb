package com.imagica.model;

public class CartImagica {

	private PackagesImagica packages;
	private TicketsImagica tickets;
	private AddonsImagica addOns;
	private String sessionId;
	
	
	public PackagesImagica getPackages() {
		return packages;
	}
	public void setPackages(PackagesImagica packages) {
		this.packages = packages;
	}
	public TicketsImagica getTickets() {
		return tickets;
	}
	public void setTickets(TicketsImagica tickets) {
		this.tickets = tickets;
	}
	public AddonsImagica getAddOns() {
		return addOns;
	}
	public void setAddOns(AddonsImagica addOns) {
		this.addOns = addOns;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	
}
