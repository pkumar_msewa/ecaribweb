package com.imagica.util;

import com.imagica.model.request.*;
import com.imagica.model.response.*;
import com.payqwikweb.model.web.ImagicaOrderRequest;
import com.payqwikweb.model.web.ImagicaPaymentRequest;
import com.payqwikweb.util.JSONParserUtil;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import org.codehaus.jackson.map.util.JSONPObject;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.util.MultiValueMap;

import javax.ws.rs.core.MultivaluedMap;
import java.util.*;

public class IParkConvertUtil {

    public static ImagicaPaymentRequest convertFromResponse(PaymentResponse paymentResponse) {
        ImagicaPaymentRequest paymentRequest = new ImagicaPaymentRequest();
        if(paymentResponse.isSuccess()) {
            paymentRequest.setProfileId(paymentResponse.getProfileId());
        }
        paymentRequest.setSuccess(paymentResponse.isSuccess());
        return paymentRequest;
    }
    public static TicketType convertTicketType(JSONObject ticket){
        TicketType ticketType = new TicketType();
        if(ticket != null) {
            try {
                ticketType.setTaxDetailsList(getTaxList(JSONParserUtil.getObject(ticket,"tax_details")));
                ticketType.setType(JSONParserUtil.getString(ticket,"type"));
                ticketType.setCostPerQty(Double.parseDouble(JSONParserUtil.getString(ticket,"cost_per")));
                ticketType.setTotalCost(Double.parseDouble(JSONParserUtil.getString(ticket,"total_cost")));
                ticketType.setProductId(Long.parseLong(JSONParserUtil.getString(ticket,"product_id")));
                ticketType.setPlu(Long.parseLong(JSONParserUtil.getString(ticket,"plu")));
                ticketType.setQuantity(Long.parseLong(JSONParserUtil.getString(ticket,"quantity")));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return ticketType;
    }

    public static ImagicaOrderRequest covertFromResponse(OrderResponse response){
        ImagicaOrderRequest request = new ImagicaOrderRequest();
        request.setBaseAmount(response.getBaseAmount());
        request.setKkCess(response.getKkCess());
        request.setOrderId(response.getOrderId());
        request.setSbCess(response.getSbCess());
        request.setServiceTax(response.getServiceTax());
        request.setTotalAmount(response.getTotalAmount());
        request.setUid(response.getUid());
        return request;
    }

    public static MultivaluedMap<String,String> getPaymentForm(PaymentRequest request){
        MultivaluedMap<String,String> paymentForm = new MultivaluedMapImpl();
        paymentForm.add("payment_data[uid]",request.getUid());
        paymentForm.add("payment_data[amount]",request.getAmount());
        paymentForm.add("payment_data[order_id]",request.getOrderId());
        paymentForm.add("payment_data[payment_method]",request.getPaymentMethod());
        paymentForm.add("payment_data[transaction_id]",request.getTransactionId());
        paymentForm.add("payment_data[status]",PaymentRequest.getStatus());
        paymentForm.add("payment_data[currency_code]",PaymentRequest.getCURRENCY());
        paymentForm.add("profile_data[uid]",request.getUid());
        paymentForm.add("profile_data[name_title]",request.getSalutation().getKey());
        paymentForm.add("profile_data[first_name]",request.getFirstName());
        paymentForm.add("profile_data[last_name]",request.getLastName());
        paymentForm.add("profile_data[email_id]",request.getEmailId());
        paymentForm.add("profile_data[dob]",request.getDob());
        paymentForm.add("profile_data[mob_num]",request.getMobileNo());
        paymentForm.add("profile_data[address]",request.getAddress());
        paymentForm.add("profile_data[postal_code]",request.getLocationCode());
        paymentForm.add("profile_data[country]",request.getCountry());
        paymentForm.add("profile_data[state]",request.getState());
        paymentForm.add("profile_data[city]",request.getCity());
        paymentForm.add("profile_data[order_id]",request.getOrderId());
        if(request.isInfantPresent()) {
            paymentForm.add("profile_data[clickfor_infant]","1");
            paymentForm.add("profile_data[num_infant]",request.getNoOfInfants());
        }else {
            paymentForm.add("profile_data[clickfor_infant]","0");
            paymentForm.add("profile_data[num_infant]","0");
        }
        return paymentForm;
    }

    public static double getPriceFromObj(JSONObject price) {
        double cost = 0.0;
        if(price != null) {
            cost = (JSONParserUtil.getLong(price,"amount"))/100.00;
            System.err.println("service tax is::"+JSONParserUtil.getLong(price,"amount"));
        }
        return cost;

    }
    public static MultivaluedMap<String,String> convertToOrderRequest(OrderDTO dto) {
        MultivaluedMap<String,String> orderForm = new MultivaluedMapImpl();
        if(dto != null) {
            orderForm.add("total_day",String.valueOf(dto.getTotaldays()));
            orderForm.add("field_visit_date",dto.getVisitDate());
            orderForm.add("field_departure_date",dto.getDepartureDate());
            orderForm.add("field_evt_res_business_unit",dto.getDestination().getValue());
            Map<String,Integer> productQty = dto.getProductQuantityMap();
            if(productQty != null) {
                Set<String> productIds =  productQty.keySet();
                long size = productIds.size();
                Iterator<String> iterator = productIds.iterator();
                int i = 0;
                while(iterator.hasNext()) {
                    String productId = iterator.next();
                    String keyId = String.format("commerce_product[%d][id]",i);
                    String keyQty = String.format("commerce_product[%d][quantity]",i);
                    orderForm.add(keyId,productId);
                    orderForm.add(keyQty,String.valueOf(productQty.get(productId)));
                    i++;
                }
            }
            SnowMagicaOrder snowMagica = dto.getSnowMagicaOrder();
            if(snowMagica != null) {
                System.err.println(String.valueOf(snowMagica.getSessionId()));
                orderForm.add("field_sessions",String.valueOf(snowMagica.getSessionId()));
                orderForm.add("field_timeslots",String.valueOf(snowMagica.getTimeSlotId()));
            }
            BusOrder bus = dto.getBusOrder();
            if(bus != null) {
                orderForm.add("place_id",String.valueOf(bus.getPlaceId()));
                orderForm.add("location_id",String.valueOf(bus.getLocationId()));
            }
            if(dto.getCoupon() != null) {
                orderForm.add("coupon",dto.getCoupon());
            }
            List<CarPassengerDetails> carPassengerDetails = dto.getCarPassengerList();
            if(carPassengerDetails != null && !carPassengerDetails.isEmpty()) {
                int p = 0;
                for(CarPassengerDetails passenger: carPassengerDetails) {
                    System.err.println(p);
                    String titleKey = String.format("profile_details[%d][title]",p);
                    String fnameKey = String.format("profile_details[%d][fname]",p);
                    String lnameKey = String.format("profile_details[%d][lname]",p);
                    String dobKey = String.format("profile_details[%d][dob]",p);
                    String contactKey = String.format("profile_details[%d][contact]",p);
                    String emailKey = String.format("profile_details[%d][email]",p);
                    orderForm.add(titleKey,passenger.getTitle().getKey());
                    orderForm.add(fnameKey,passenger.getFirstName());
                    orderForm.add(lnameKey,passenger.getLastName());
                    orderForm.add(dobKey,passenger.getDob());
                    orderForm.add(contactKey,passenger.getContact());
                    orderForm.add(emailKey,passenger.getEmail());
                }
                CarDetails carDetails = dto.getCarDetails();
                if(carDetails != null) {
                    orderForm.add("car_details[title]",carDetails.getTitle().getKey());
                    orderForm.add("car_details[fname]",carDetails.getFirstName());
                    orderForm.add("car_details[lname]",carDetails.getLastName());
                    orderForm.add("car_details[pin]",carDetails.getLocationCode());
                    orderForm.add("car_details[country]",carDetails.getCountry());
                    orderForm.add("car_details[city]",carDetails.getCity());
                    orderForm.add("car_details[state]",carDetails.getState());
                    orderForm.add("car_details[contact]",carDetails.getContactNo());
                    orderForm.add("car_details[time]",carDetails.getBookingTime());
                    orderForm.add("car_details[address]",carDetails.getAddress());
                }
            }
        }
        return orderForm;
    }


    public static List<String> getErrorsFromArray(JSONArray errorArray){
       List<String> errors = new ArrayList<>();
       if(errorArray != null) {
           for(int l=0; l < errorArray.length(); l++){
               try {
                   JSONObject obj = errorArray.getJSONObject(l);
                   if(obj != null) {
                	   System.err.println("the error msg is :::::::::"+JSONParserUtil.getString(obj,"errorMsg"));
                       errors.add(JSONParserUtil.getString(obj,"errorMsg"));
                   }
               } catch (JSONException e) {
                   e.printStackTrace();
               }
           }
       }
       return errors;
    }

    public static List<GBrunchDetail> getAddonList(JSONArray brunch) throws JSONException {
        List<GBrunchDetail> addonList = new ArrayList<>();
        if(brunch != null) {
            for(int i = 0; i < brunch.length(); i++) {
                JSONObject temp = brunch.getJSONObject(i);
                Iterator<String> brunchType = temp.keys();
                while(brunchType.hasNext()) {
                    JSONObject typeName = JSONParserUtil.getObject(temp,brunchType.next());
                    if(typeName != null) {
                        GBrunchDetail detail = new GBrunchDetail();
                        detail.setCurrencyCode(JSONParserUtil.getString(typeName,"currency_code"));
                        detail.setProductId(Long.parseLong(JSONParserUtil.getString(typeName,"pid")));
                        detail.setQuantity(Long.parseLong(JSONParserUtil.getString(typeName,"quantity")));
                        detail.setTotalCost(Double.parseDouble(JSONParserUtil.getString(typeName,"total_addon_cost")));
                        detail.setCostPer(Double.parseDouble(JSONParserUtil.getString(typeName,"addon_cost")));
                        detail.setImageUrl(JSONParserUtil.getString(typeName,"img_url"));
                        detail.setDescription(JSONParserUtil.getString(typeName,"description"));
                        detail.setTitle(JSONParserUtil.getString(typeName,"title"));
                        detail.setAid(Long.parseLong(JSONParserUtil.getString(typeName,"aid")));
                        detail.setBruchTitle(JSONParserUtil.getString(typeName,"brunch_title"));
                        detail.setTaxDetailList(getTaxList(JSONParserUtil.getObject(typeName,"tax_details")));
                        addonList.add(detail);
                    }
                }
            }
        }
        return addonList;
    }


    public static PhotoAddonDetail getPhotoAddOnDetail(JSONObject data){
        PhotoAddonDetail photo = new PhotoAddonDetail();
        if(data != null) {
            Iterator<String> keys = data.keys();
            while(keys.hasNext()) {
                JSONObject temp = JSONParserUtil.getObject(data, keys.next());
                if(temp != null) {
                    String type = JSONParserUtil.getString(temp,"type");
                    if(type!=null){
                    if(type.equalsIgnoreCase("Digi Photo")) {
                        photo.setType(JSONParserUtil.getString(temp,"type"));
                        photo.setTypeId(Long.parseLong(JSONParserUtil.getString(temp,"type_id")));
                        photo.setCurrency(JSONParserUtil.getString(temp,"currency_code"));
                        photo.setQuantity(JSONParserUtil.getLong(temp,"quantity"));
                        photo.setTotalAddOnCost(Double.parseDouble(JSONParserUtil.getString(temp,"total_addon_cost")));
                        photo.setAddOnCost(Double.parseDouble(JSONParserUtil.getString(temp,"addon_cost")));
                        photo.setTitle(JSONParserUtil.getString(temp,"title"));
                        photo.setAid(Long.parseLong(JSONParserUtil.getString(temp,"aid")));
                        photo.setImageUrl(JSONParserUtil.getString(temp,"image_url"));
                        photo.setTaxDetails(getTaxList(JSONParserUtil.getObject(temp,"tax_details")));
                    }
                }
                }
            }
        }
        return photo;
    }


    public static List<GCarAddonDetail> getCarAddOnDetail(JSONObject data){
        List<GCarAddonDetail> carAddonList = new ArrayList<>();
        if(data != null) {
            Iterator<String> keys = data.keys();
            while(keys.hasNext()) {
            	//System.err.println("the keys are:::"+keys.next());
                JSONObject temp = JSONParserUtil.getObject(data, keys.next());
                if(temp != null) {
                    String type = JSONParserUtil.getString(temp,"type");
                    System.err.println("the type is::::"+type);
                    if(type!=null){
                    if(type.equalsIgnoreCase("Car")) {
                        GCarAddonDetail carAddonDetail = new GCarAddonDetail();
                        carAddonDetail.setType(JSONParserUtil.getString(temp,"type"));
                        carAddonDetail.setTypeId(Long.parseLong(JSONParserUtil.getString(temp,"type_id")));
                        carAddonDetail.setCurrency(JSONParserUtil.getString(temp,"currency_code"));
                        carAddonDetail.setQuantity(JSONParserUtil.getLong(temp,"quantity"));
                        carAddonDetail.setTotalAddOnCost(Double.parseDouble(JSONParserUtil.getString(temp,"total_addon_cost")));
                        carAddonDetail.setAddOnCost(Double.parseDouble(JSONParserUtil.getString(temp,"addon_cost")));
                        carAddonDetail.setTitle(JSONParserUtil.getString(temp,"title"));
                        carAddonDetail.setAid(Long.parseLong(JSONParserUtil.getString(temp,"aid")));
                        carAddonDetail.setImageUrl(JSONParserUtil.getString(temp,"image_url"));
                        carAddonDetail.setTaxDetails(getTaxList(JSONParserUtil.getObject(temp,"tax_details")));
                        carAddonList.add(carAddonDetail);
                    }
                }
                }
            }
        }
        return carAddonList;
    }


    public static List<BusAddonDetail> getBusAddOnDetail(JSONObject data){
        List<BusAddonDetail> busAddonList = new ArrayList<>();
        if(data != null) {
            Iterator<String> keys = data.keys();
            while(keys.hasNext()) {
                JSONObject temp = JSONParserUtil.getObject(data, keys.next());
              //  System.err.println("the keys are::"+keys.next());
                if(temp != null) {
                    String type = JSONParserUtil.getString(temp,"type");
                    System.err.println("the type is:::"+type);
                    if(type!=null){
                    if(type.equalsIgnoreCase("Bus")) {
                        BusAddonDetail busAddon = new BusAddonDetail();
                        busAddon.setType(JSONParserUtil.getString(temp,"type"));
                        busAddon.setTypeId(Long.parseLong(JSONParserUtil.getString(temp,"type_id")));
                        busAddon.setCurrency(JSONParserUtil.getString(temp,"currency_code"));
                        busAddon.setQuantity(JSONParserUtil.getLong(temp,"quantity"));
                        busAddon.setTotalAddOnCost(Double.parseDouble(JSONParserUtil.getString(temp,"total_addon_cost")));
                        busAddon.setAddOnCost(Double.parseDouble(JSONParserUtil.getString(temp,"addon_cost")));
                        busAddon.setTitle(JSONParserUtil.getString(temp,"title"));
                        busAddon.setAid(Long.parseLong(JSONParserUtil.getString(temp,"aid")));
                        busAddon.setImageUrl(JSONParserUtil.getString(temp,"image_url"));
                        busAddon.setTaxDetails(getTaxList(JSONParserUtil.getObject(temp,"tax_details")));
                        JSONObject pickup = JSONParserUtil.getObject(temp,"pickup_suggestions");
                        if(pickup != null) {
                            busAddon.setPlaceId(JSONParserUtil.getString(pickup,"place_id"));
                            busAddon.setPlaceName(JSONParserUtil.getString(pickup,"place_name"));
                            busAddon.setLocationList(getLocationDetails(JSONParserUtil.getArray(pickup,"location")));
                        }
                        busAddonList.add(busAddon);
                    }
                }
            }
            }
        }
        return busAddonList;
    }

    public static SnowMagicaAddOn getFromObject(JSONObject snowMagica) {
        SnowMagicaAddOn addOn = new SnowMagicaAddOn();
        if(snowMagica != null) {
            JSONObject snowPark = JSONParserUtil.getObject(snowMagica,"Snow Park");
            if(snowPark != null) {
                addOn.setProdType(JSONParserUtil.getString(snowPark,"prodType"));
                addOn.setTicketDescription(JSONParserUtil.getString(snowPark,"ticket_des"));
                addOn.setTitle(JSONParserUtil.getString(snowPark,"title"));
                addOn.setImageUrl(JSONParserUtil.getString(snowPark,"image_url"));
                addOn.setCurrency(JSONParserUtil.getString(snowPark,"currency"));
                addOn.setSessionList(getSession(JSONParserUtil.getObject(snowPark,"session_suggestions")));
                addOn.setTicketType(convertTicketType(JSONParserUtil.getObject(snowPark,"All")));
            }
        }
        return addOn;
    }

    private static List<ImagicaSession> getSession(JSONObject sessionSuggestions) {
        List<ImagicaSession> sessionList = new ArrayList<>();
        if(sessionSuggestions != null) {
            Iterator<String> sessions = sessionSuggestions.keys();
            while(sessions.hasNext()) {
                JSONObject temp = JSONParserUtil.getObject(sessionSuggestions, sessions.next());
                if(temp != null) {
                    ImagicaSession newSession = new ImagicaSession();
                    newSession.setSessionId(JSONParserUtil.getLong(temp,"session_id"));
                    newSession.setSessionName(JSONParserUtil.getString(temp,"session_name"));
                    newSession.setTimeSlots(getTimeSlots(JSONParserUtil.getArray(temp,"timeslots")));
                    sessionList.add(newSession);
                }
            }
        }
        return sessionList;
    }

    private static List<ImagicaTimeSlots> getTimeSlots(JSONArray timeSlotsArray) {
        List<ImagicaTimeSlots> timeSlots = new ArrayList<>();
        for(int k=0;k < timeSlotsArray.length(); k++) {
            try {
                JSONObject tempSlot = timeSlotsArray.getJSONObject(k);
                ImagicaTimeSlots slot = new ImagicaTimeSlots();
                slot.setSlotId(JSONParserUtil.getLong(tempSlot,"timeslot_id"));
                slot.setSlotName(JSONParserUtil.getString(tempSlot,"timeslot_name"));
                timeSlots.add(slot);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return timeSlots;
    }

    private static List<ImagicaBusLocation> getLocationDetails(JSONArray location) {

        List<ImagicaBusLocation> imagicaBusLocations = new ArrayList<>();
        if(location != null) {
            for (int j = 0; j < location.length(); j++) {
                ImagicaBusLocation busLocation = new ImagicaBusLocation();
                try {
                    JSONObject busLocationObject = location.getJSONObject(j);
                    busLocation.setLocationId(Long.parseLong(JSONParserUtil.getString(busLocationObject, "location_id")));
                    busLocation.setLocationName(JSONParserUtil.getString(busLocationObject, "location_name"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                imagicaBusLocations.add(busLocation);
            }
        }
        return imagicaBusLocations;
    }


    public static List<TaxDetails> getTaxList(JSONObject tax) {
        List<TaxDetails> taxDetailsList = new ArrayList<>();
        if(tax != null) {
            Iterator<String> keys = tax.keys();
            while(keys.hasNext()) {
                TaxDetails temp = new TaxDetails();
                    JSONObject json = JSONParserUtil.getObject(tax,keys.next());
                    if(json != null) {
                        temp.setName(JSONParserUtil.getString(json,"name"));
                        temp.setPricePerQty(Double.parseDouble(JSONParserUtil.getString(json,"price_per_quantity")));
                    }
                    taxDetailsList.add(temp);
            }
        }
        return taxDetailsList;
    }

    public static AddOns getAddOns(JSONObject addONS) throws JSONException {
        AddOns addOns = new AddOns();
        if(addONS != null) {
            JSONObject addonData = JSONParserUtil.getObject(addONS,"addon_data");
            if(addonData != null) {
                JSONObject data = JSONParserUtil.getObject(addonData,"data");
                PhotoAddonDetail photo = getPhotoAddOnDetail(data);
                if(photo.getType() != null) {
                    addOns.setPhotoAddonDetail(photo);
                }
                List<GCarAddonDetail> carAddonDetailList = getCarAddOnDetail(data);
                addOns.setCarAddonDetailList(carAddonDetailList);
                List<BusAddonDetail> busAddonList = getBusAddOnDetail(data);
                addOns.setBusAddonDetailList(busAddonList);
            }
            JSONObject snowMagica = JSONParserUtil.getObject(addONS,"snomagica_addon_data");
            if(snowMagica != null) {
                addOns.setSnowMagicaAddOn(getFromObject(snowMagica));
            }
            JSONArray bruchJSON = JSONParserUtil.getArray(addONS,"addon_brunch_data");
            addOns.setBrunchAddon(getAddonList(bruchJSON));
        }
        return addOns;
    }

    public static List<Tickets> getTicketList(JSONObject ticket) {
        List<Tickets> tickets = new ArrayList<>();
        if (ticket != null) {
            Iterator<String> ticketKeys = ticket.keys();
            while (ticketKeys.hasNext()) {
                JSONObject ticketName = JSONParserUtil.getObject(ticket, ticketKeys.next());
                Tickets newTicket = new Tickets();
                if (ticketName != null) {
                    newTicket.setDescription(JSONParserUtil.getString(ticketName, "ticket_des"));
                    newTicket.setProductType(JSONParserUtil.getString(ticketName, "prodType"));
                    newTicket.setTitle(JSONParserUtil.getString(ticketName, "title"));
                    newTicket.setImage(JSONParserUtil.getString(ticketName, "image_url"));
                    newTicket.setTerms(JSONParserUtil.getString(ticketName, "ticket_term"));
                    newTicket.setCurrency(JSONParserUtil.getString(ticketName, "currency"));
                    JSONObject sc = JSONParserUtil.getObject(ticketName, "Senior Citizen");
                    TicketType seniorCitizen = convertTicketType(sc);
                    JSONObject adult = JSONParserUtil.getObject(ticketName, "Adult");
                    TicketType adultCitizen = convertTicketType(adult);
                    JSONObject child = JSONParserUtil.getObject(ticketName, "Child");
                    TicketType childCitizen = convertTicketType(child);
                    JSONObject college = JSONParserUtil.getObject(ticketName, "College");
                    TicketType collegeStudent = convertTicketType(college);
                    List<TicketType> ticketTypeList = new ArrayList<>();
                    if(sc !=  null) {
                        ticketTypeList.add(seniorCitizen);
                    }
                    if(adult != null) {
                        ticketTypeList.add(adultCitizen);
                    }
                    if(child != null) {
                        ticketTypeList.add(childCitizen);
                    }
                    if(college != null) {
                        ticketTypeList.add(collegeStudent);
                    }
                    newTicket.setTicketTypeList(ticketTypeList);
                    tickets.add(newTicket);
                }
            }
        }
            return tickets;
    }
    public static List<PackageDetail> packageDetailList(JSONArray packageArray) throws JSONException {
        List<PackageDetail> packages = new ArrayList<>();
        if(packageArray != null) {
            for(int i=0; i<packageArray.length();i++) {
                JSONObject pack = packageArray.getJSONObject(i);
                PackageDetail packageDetail = new PackageDetail();
                if(pack != null) {
                    packageDetail.setImageUrl(pack.getString("img_url"));
                    packageDetail.setDescription(pack.getString("package_des"));
                    packageDetail.setTitle(pack.getString("package_title"));
                    packageDetail.setQuantity(pack.getLong("quantity"));
                    packageDetail.setCostPer(Double.parseDouble(pack.getString("cost_per")));
                    packageDetail.setCostTotal(Double.parseDouble(pack.getString("cost_total")));
                    packageDetail.setCurrency(pack.getString("currency"));
                    packageDetail.setProductId(Long.parseLong(pack.getString("product_id")));
                    JSONObject details = pack.getJSONObject("details");
                    if(details != null) {
                        TravelDetail travelDetail = new TravelDetail();
                        travelDetail.setBusExists((details.getInt("bus_exists") == 1) ? true : false);
                        travelDetail.setCarExists((details.getInt("car_exists") == 1) ? true : false);
                        packageDetail.setTravelDetail(travelDetail);
                        JSONObject tktDetail = JSONParserUtil.getObject(details,"ticket_details");
                        if(tktDetail != null) {
                            TicketDetail ticketDetail = new TicketDetail();
                            ticketDetail.setPassengerType(JSONParserUtil.getString(tktDetail,"passenger_type"));
                            ticketDetail.setQuantity(Long.parseLong(JSONParserUtil.getString(tktDetail,"quantity")));
                            packageDetail.setTicketDetail(ticketDetail);
                        }
                        JSONObject brunchAddOn = JSONParserUtil.getObject(details,"brunch_addon_details");
                        if(brunchAddOn != null) {
                            BrunchDetail brunchDetail = new BrunchDetail();
                            brunchDetail.setQuantity(JSONParserUtil.getLong(brunchAddOn,"quantity"));
                            brunchDetail.setAddOneName(JSONParserUtil.getString(brunchAddOn,"addon_name"));
                            brunchDetail.setAddOnId(Long.parseLong(JSONParserUtil.getString(brunchAddOn,"addon_id")));
                            packageDetail.setBrunchDetail(brunchDetail);
                        }
                        JSONObject carAddOn = JSONParserUtil.getObject(details,"car_addon_details");
                        if(carAddOn != null) {
                            CarAddonDetail carAddonDetail = new CarAddonDetail();
                            carAddonDetail.setAddOnId(Long.parseLong(JSONParserUtil.getString(carAddOn,"addon_id")));
                            carAddonDetail.setQuantity(JSONParserUtil.getLong(carAddOn,"quantity"));
                            carAddonDetail.setAddOnName(JSONParserUtil.getString(carAddOn,"addon_name"));
                            packageDetail.setCarAddonDetail(carAddonDetail);
                        }
                        JSONObject taxDetails = JSONParserUtil.getObject(details,"tax_details");
                        if(taxDetails != null) {
                            List<TaxDetails> taxDetailsList = new ArrayList<>();
                            Iterator<String> keys =  taxDetails.keys();
                            while(keys.hasNext()) {
                                TaxDetails tax = new TaxDetails();
                                JSONObject taxObject = taxDetails.getJSONObject(keys.next());
                                tax.setName(JSONParserUtil.getString(taxObject,"name"));
                                tax.setPricePerQty(Double.parseDouble(JSONParserUtil.getString(taxObject,"price_per_quantity")));
                                taxDetailsList.add(tax);
                            }
                            packageDetail.setTaxDetailList(taxDetailsList);
                        }
                    }
                }
                packages.add(packageDetail);
            }
        }
        return packages;
    }
    
}
