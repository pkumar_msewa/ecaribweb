package com.imagica.util;

import com.payqwikweb.app.metadatas.UrlMetadatas;

public class ImagicaUtil {

    private static final String BASE_URL_TEST = "https://book-preprod.adlabsimagica.com/";//test
    private static final String BASE_URL_LIVE = "https://api.adlabsimagica.com/";// live
    private static final String B2W_URL = getImagicaUrl()+"b2w/";
    private static final String B2W_SERVICE_URL = getImagicaUrl()+"b2w_services/";
    public static final String LOGIN_URL = B2W_URL+"user/login.json";
    public static final String PARK_SEARCH = B2W_SERVICE_URL+"booknow-suggestions/search.json";
    public static final String BOOK_ORDER = B2W_SERVICE_URL+"book-order.json";
    public static final String PROCESS_PAYMENT = B2W_SERVICE_URL+"payment-process/payment.json";

    public static String getImagicaUrl(){
    	if(UrlMetadatas.PRODUCTION){
    		return BASE_URL_LIVE;
    	}else{
    		return BASE_URL_TEST;
    	}
    }

}




