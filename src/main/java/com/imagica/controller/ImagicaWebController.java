package com.imagica.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.imagica.api.IServicesApi;
import com.imagica.model.AddonsImagica;
import com.imagica.model.CartImagica;
import com.imagica.model.ImagicaProduct;
import com.imagica.model.TicketsImagica;
import com.imagica.model.request.Destination;
import com.imagica.model.request.OrderDTO;
import com.imagica.model.request.OrderUserDetails;
import com.imagica.model.request.PaymentRequest;
import com.imagica.model.request.Salutation;
import com.imagica.model.request.SearchDTO;
import com.imagica.model.response.AddToCartResponseImagica;
import com.imagica.model.response.OrderResponse;
import com.imagica.model.response.PackageDetail;
import com.imagica.model.response.PaymentResponse;
import com.imagica.model.response.SearchResponse;
import com.imagica.util.IParkConvertUtil;
import com.payqwikweb.api.IAuthenticationApi;
import com.payqwikweb.app.api.IUserApi;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.request.SessionDTO;
import com.payqwikweb.model.app.response.AdlabsTicketListResponse;
import com.payqwikweb.model.web.ImagicaAuthDTO;
import com.payqwikweb.model.web.ImagicaOrderRequest;
import com.payqwikweb.model.web.ImagicaOrderResponse;
import com.payqwikweb.model.web.ImagicaPaymentRequest;
import com.payqwikweb.model.web.ImagicaPaymentResponse;
import com.payqwikweb.util.APIUtils;
import com.payqwikweb.util.Authorities;
import com.thirdparty.model.ResponseDTO;

@RequestMapping("/Imagica")
public class ImagicaWebController {

	private IServicesApi servicesApi;
	private final IAuthenticationApi authenticationApi;
	private IUserApi userApi;
	private static SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-YYYY");
	public ImagicaWebController(IServicesApi servicesApi,IAuthenticationApi authenticationApi,IUserApi userApi) {
		super();
		this.servicesApi = servicesApi;
		this.authenticationApi=authenticationApi;
		this.userApi=userApi;
	}
	
	@RequestMapping(value="/SearchTickets",method=RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<SearchResponse> generateTickets(@ModelAttribute SearchDTO dto,HttpServletRequest request,HttpServletResponse response,HttpSession session,Model model) throws JSONException{
		String sessionId = (String) session.getAttribute("sessionId");
		System.err.println("i am here.......");
		SearchResponse resp=new SearchResponse();
		if (sessionId != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)){
					
					SessionDTO sessionDTO = new SessionDTO();
					sessionDTO.setSessionId(sessionId);
					ImagicaAuthDTO imagicaAuthDTO = userApi.getImagicaAuth(sessionDTO);
					if(imagicaAuthDTO.getSuccess()) {
						dto.setCookie(imagicaAuthDTO.getCookie());
						dto.setToken(imagicaAuthDTO.getToken());
						dto.setAdult(1);
						dto.setChild(0);
						dto.setSenior(0);
						dto.setCollege(0);
						dto.setDateVisit(dto.getDateVisit());
						dto.setTotalDays(1);
						resp = servicesApi.getParkSearchResult(dto);
					} else {
						resp.setSuccess(false);
						resp.setMessage(imagicaAuthDTO.getMessage());
					}
				}else {
					resp.setSuccess(false);
					resp.setMessage("Only EN locale available");
				}
			} else {
				resp.setSuccess(false);
				resp.setMessage("Unauthorized Device");
			}
		} else {
			resp.setSuccess(false);
			resp.setMessage("Unauthorized Role");
		}
	
	return new ResponseEntity<>(resp, HttpStatus.OK);
}
	
	@RequestMapping(value = "/SelectEntertainment", method = RequestMethod.GET)
	public String getAccessToken(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
		AdlabsTicketListResponse resp = new AdlabsTicketListResponse();
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
					return "User/Adlabs/ShowTickets";
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("User Authentication Failed");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("User Authority null");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Session null");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return "redirect:/Home";
	}
	
	
	@RequestMapping(value="/SearchITickets",method=RequestMethod.GET)
	public String generateTicketsGET(@ModelAttribute SearchDTO dto,HttpServletRequest request,HttpServletResponse response,HttpSession session,Model model) throws JSONException{
		String sessionId = (String) session.getAttribute("sessionId");
		SearchResponse resp=new SearchResponse();
		if (sessionId != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)){
					session.setAttribute("date",dto.getDateVisit());
					session.setAttribute("destination", dto.getDestination());
					System.err.println("date::"+dto.getDateVisit());
					System.err.println("destination::"+dto.getDestination());
					return "User/Adlabs/ShowTickets";
				}
			}
		}
		
		return "User/Adlabs/ShowTickets";
}
	
	@RequestMapping(value="/AddToCartTickets",method=RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<AddToCartResponseImagica> addToCartTickets(@RequestBody ImagicaProduct dto,HttpServletRequest request,HttpServletResponse response,HttpSession session,Model model) throws JSONException
	{
		String sessionId = (String) session.getAttribute("sessionId");
		System.err.println("i am here.......");
		AddToCartResponseImagica resp=new AddToCartResponseImagica();
		if (sessionId != null) {
			
			String data=dto.getProductId();
			System.out.println(data);
			String a[]=data.split("~");
			
			String b[]=a[1].split("!");
			String c[]=a[2].split("@");
			String d[]=a[3].split("#");
			double totalValue=0;
			HashMap<String, Integer> ojjj=new HashMap<>();
			
			
			for (int i = 0; i < d.length; i++) 
			{
				if(Integer.parseInt(d[i])>0){
				ojjj.put(c[i], Integer.parseInt(d[i]));
				}
			}
			HashMap<String,String> kj=new HashMap<>();
			for(int j=0;j<b.length;j++){
				String fg=b[j];
				String hh=d[j];
				System.err.println(fg+ "--" +hh);
				double ttt=Integer.parseInt(hh)*Double.parseDouble(fg);
				System.err.println("total vakvjkd===="+ttt);
				totalValue+=ttt;
			}
			
			System.out.println(ojjj);
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)){
					TicketsImagica ticket=new TicketsImagica();
					ticket.setType(dto.getTicketType());
					ticket.setCostPerQty(dto.getProductPrice());
					ticket.setDescription(dto.getProductDescription());
					ticket.setImage(dto.getImageUrl());
					ticket.setTitle(dto.getTitle());
					//ticket.setQuantity(Integer.parseInt(dto.getProductQuantity()));
					ticket.setTotalCost(dto.getTotalPrice());
					
					CartImagica cart=new CartImagica();
					cart.setTickets(ticket);
					cart.setSessionId(sessionId);
					session.setAttribute("costPer",ticket.getCostPerQty());
					session.setAttribute("quantity", ticket.getQuantity());
					session.setAttribute("prod_id", dto.getProductId());
					session.setAttribute("productQuantityMap", ojjj);
					if(session.getAttribute("cartTotalValue")!=null){
					double cartd=(Double)session.getAttribute("cartTotalValue");
					System.err.println("previous cart::"+cartd);
					System.err.println("the tot::"+totalValue);
					if(cartd>0){
						
					double xxx=cartd+totalValue;
						System.err.println("final::"+xxx);
						session.removeAttribute("cartTotalValue");
						session.setAttribute("cartTotalValue", xxx);
						resp.setMessage(""+xxx);
						return new ResponseEntity<>(resp, HttpStatus.OK);
					}
				
					}else{
					session.setAttribute("cartTotalValue", totalValue);
					resp.setMessage(""+totalValue);
					}
							} else {
				resp.setMessage("Unauthorized Device");
			}
		} else {
			resp.setMessage("Unauthorized Role");
		}
		}else{
			resp.setMessage("please login again....");
		}
	return new ResponseEntity<>(resp, HttpStatus.OK);
}

	@RequestMapping(value="/AddToCartAddons",method=RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<AddToCartResponseImagica> addToCartaddOns(@RequestBody ImagicaProduct dto,HttpServletRequest request,HttpServletResponse response,HttpSession session,Model model) throws JSONException
	{
		String sessionId = (String) session.getAttribute("sessionId");
		System.err.println("i am here.......");
		AddToCartResponseImagica resp=new AddToCartResponseImagica();
		if (sessionId != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)){
					AddonsImagica addons=new AddonsImagica();
					addons.setType(dto.getTicketType());
					addons.setTitle(dto.getTitle());
					addons.setQuantity(Integer.parseInt(dto.getProductQuantity()));
					addons.setAddOnCost(dto.getProductPrice());
					addons.setImageUrl(dto.getImageUrl());
					CartImagica cart=new CartImagica();
					cart.setAddOns(addons);
					cart.setSessionId(sessionId);
					model.addAttribute("cartImagica", cart);
					resp.setMessage("Item added successfully to cart");
							} else {
				resp.setMessage("Unauthorized Device");
			}
		} else {
			resp.setMessage("Unauthorized Role");
		}
		}else{
			resp.setMessage("please login again....");
		}
	return new ResponseEntity<>(resp, HttpStatus.OK);
}

	@RequestMapping(value="/ShowAddons",method=RequestMethod.GET)
	public String showAddons(HttpServletRequest request,HttpServletResponse response,HttpSession session,Model model) throws JSONException{
		String sessionId = (String) session.getAttribute("sessionId");
		SearchResponse resp=new SearchResponse();
		if (sessionId != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)){
					
					return "User/Adlabs/addOns";
				}
			}
		}
		return "User/Adlabs/addOns";
	}
	
	@RequestMapping(value="/ShowDetails",method=RequestMethod.GET)
	public String showDetails(HttpServletRequest request,HttpServletResponse response,HttpSession session,Model model) throws JSONException{
		String sessionId = (String) session.getAttribute("sessionId");
		SearchResponse resp=new SearchResponse();
		if (sessionId != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)){
					
					return "User/Adlabs/YourDetails";
				}
			}
		}
		return "User/Adlabs/YourDetails";
	}
	
	@RequestMapping(value="/processPayment",method=RequestMethod.POST)
	public String processPaymentImagica(@ModelAttribute OrderUserDetails dto,HttpServletRequest request,HttpServletResponse response,HttpSession session,Model model) throws JSONException, ParseException
	{
		System.err.println("hi m here in payment");
		String sessionId = (String) session.getAttribute("sessionId");
		ImagicaOrderResponse resp = new ImagicaOrderResponse();
		System.err.println("i am here.......");
		if (sessionId != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)){
					double cost=(Double)session.getAttribute("costPer");
					int quantity=(Integer)session.getAttribute("quantity");
					String date=(String)session.getAttribute("date");
					Destination destination=(Destination)session.getAttribute("destination");
					
					String prod=(String)session.getAttribute("prod_id");
					String prod_id='"'+prod+'"';
					Map<String, Integer> productQuantityMap=new HashMap<>();
					productQuantityMap.put(prod, quantity);
					OrderDTO dto1=new OrderDTO();
					Destination dest=(Destination)session.getAttribute("destination");
					SessionDTO sessionDTO = new SessionDTO();
					sessionDTO.setSessionId(sessionId);
					Map<String,Integer> mapQuantity=(Map<String, Integer>) session.getAttribute("productQuantityMap");
					System.err.println("the map is::"+mapQuantity);
					ImagicaAuthDTO imagicaAuthDTO = userApi.getImagicaAuth(sessionDTO);
					if(imagicaAuthDTO.getSuccess()) {
						dto1.setCookie(imagicaAuthDTO.getCookie());
						dto1.setToken(imagicaAuthDTO.getToken());
						dto1.setDestination(dest);
						dto1.setVisitDate(date);
						dto1.setDepartureDate(date);
						dto1.setTotaldays(1);
						dto1.setProductQuantityMap(mapQuantity);
						OrderResponse orderResponse = servicesApi.placeOrderInCart(dto1);
						if(orderResponse.isSuccess()) {
							System.err.println("hi m here.....");
							ImagicaOrderRequest orderRequest = IParkConvertUtil.covertFromResponse(orderResponse);
							orderRequest.setSessionId(sessionId);
							resp = userApi.placeImagicaOrder(orderRequest);
							model.addAttribute("Trxcode",resp.getCode());
							model.addAttribute("Trxmessage", resp.getMessage());
							session.setAttribute("transactionId", resp.getTransactionId());
							session.setAttribute("orderId", resp.getOrderId());
							session.setAttribute("Uid", resp.getUid());
							session.setAttribute("amount",resp.getAmount());
							session.setAttribute("errorMsgs", resp.getErrorMsgs());
							session.setAttribute("firstN", dto.getFirstName());
							session.setAttribute("lastName", dto.getLastName());
							session.setAttribute("salutation", dto.getSalutation());
							session.setAttribute("emailId", dto.getEmail_id());
							Date daob=sdf.parse(dto.getDob());
							session.setAttribute("dob", sdf.format(daob));
							session.setAttribute("mobileNo", dto.getContact_no());
							session.setAttribute("locationCode", dto.getPinCode());
							session.setAttribute("address", dto.getAddress());
							session.setAttribute("city", dto.getCity());
							session.setAttribute("state", dto.getState());
							return "User/Adlabs/PlaceOrder";
					
				}
			}
		}
}
}
		return "User/Adlabs/PlaceOrder";
	}
	
	@RequestMapping(value="/confirmPayment",method=RequestMethod.GET)
	public String confirmPaymentImagica(HttpServletRequest request,HttpServletResponse response,HttpSession session,Model model) throws JSONException
	{
		System.err.println("hi m here in payment");
		String sessionId = (String) session.getAttribute("sessionId");
		ImagicaPaymentResponse resp = new ImagicaPaymentResponse();
		System.err.println("i am here.......");
		if (sessionId != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)){
					
					PaymentRequest dto=new PaymentRequest();
					SessionDTO sessionDTO = new SessionDTO();
					sessionDTO.setSessionId(sessionId);
					ImagicaAuthDTO imagicaAuthDTO = userApi.getImagicaAuth(sessionDTO);
					if(imagicaAuthDTO.getSuccess()) {
						dto.setCookie(imagicaAuthDTO.getCookie());
						dto.setToken(imagicaAuthDTO.getToken());
						dto.setUid((String)session.getAttribute("Uid"));
						dto.setOrderId((String)session.getAttribute("orderId"));
						dto.setTransactionId((String)session.getAttribute("transactionId"));
						dto.setAmount((String)session.getAttribute("amount"));
						dto.setFirstName((String)session.getAttribute("firstN"));
						dto.setLastName((String)session.getAttribute("lastName"));
						dto.setSalutation(Salutation.MR);
						dto.setEmailId((String)session.getAttribute("emailId"));
						dto.setDob((String)session.getAttribute("dob"));
						dto.setMobileNo((String)session.getAttribute("mobileNo"));
						dto.setLocationCode((String)session.getAttribute("locationCode"));
						dto.setAddress((String)session.getAttribute("address"));
						dto.setCity((String)session.getAttribute("city"));
						dto.setState((String)session.getAttribute("state"));
						PaymentResponse paymentResponse =  servicesApi.processPayment(dto);
						ImagicaPaymentRequest paymentRequest = IParkConvertUtil.convertFromResponse(paymentResponse);
						paymentRequest.setSessionId(sessionId);
						paymentRequest.setTransctionId(dto.getTransactionId());
						resp = userApi.processPayment(paymentRequest);
						return "User/Adlabs/SuccessOrder";
					
				}
			}
		}
}
		return "User/Adlabs/SuccessOrder";
	}
}