package com.imagica.controller;

import com.imagica.api.ILoginApi;
import com.imagica.model.request.LoginDTO;
import com.imagica.model.response.LoginResponse;
import com.payqwikweb.util.SecurityUtil;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("/Imagica")
public class LoginController {

    private final ILoginApi loginApi;

    public LoginController(ILoginApi loginApi) {
        this.loginApi = loginApi;
    }

    @RequestMapping(value="/Login",method= RequestMethod.POST,produces = MediaType.APPLICATION_JSON_VALUE,consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<LoginResponse> createLoginSession(@RequestHeader(value="x-api-key") String apiKey, @RequestBody LoginDTO dto, HttpServletRequest request, HttpServletResponse response) {
        LoginResponse result = new LoginResponse();
        if(SecurityUtil.isValidAPIKey(apiKey)) {
            result = loginApi.createSession(dto);
        } else {
            result.setSuccess(false);
            result.setMessage("Not a valid API KEY");
        }
        return new ResponseEntity<LoginResponse>(result, HttpStatus.OK);
    }

}
