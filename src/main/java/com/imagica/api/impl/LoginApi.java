package com.imagica.api.impl;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

import org.json.JSONArray;
import org.json.JSONObject;

import com.imagica.api.ILoginApi;
import com.imagica.model.request.LoginDTO;
import com.imagica.model.response.LoginResponse;
import com.imagica.util.ImagicaUtil;
import com.payqwikweb.util.JSONParserUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;

public class LoginApi implements ILoginApi{


    @Override
    public LoginResponse createSession(LoginDTO dto) {
        LoginResponse result = new LoginResponse();
        System.err.println("username "+dto.getUsername()+" password"+dto.getPassword());
        MultivaluedMap<String,String> loginForm = new MultivaluedMapImpl();
        loginForm.add("username",dto.getUsername());
        loginForm.add("password",dto.getPassword());
        try {
            Client client = Client.create();
            client.addFilter(new LoggingFilter(System.out));
            WebResource webResource = client.resource(ImagicaUtil.LOGIN_URL);
            ClientResponse response = webResource.accept(MediaType.APPLICATION_JSON).post(ClientResponse.class, loginForm);
            System.err.println("response ::" + response);
            String strResponse = response.getEntity(String.class);
            System.err.println(strResponse);
            if(response.getStatus() == 401) {
                result.setSuccess(false);
                JSONArray jsonArray = new JSONArray(strResponse);
                if(jsonArray != null) {
                    result.setMessage(jsonArray.getString(0));
                }else {
                    result.setMessage("Unauthorized User");
                }
            } else if(response.getStatus() == 200) {
                result.setSuccess(true);
                result.setMessage("Login Successful");
                result.setLoginTime(System.currentTimeMillis());
                JSONObject object = new JSONObject(strResponse);
                if(object != null) {
                    result.setSessionName(JSONParserUtil.getString(object,"session_name"));
                    result.setToken(JSONParserUtil.getString(object,"token"));
                    result.setSessionId(JSONParserUtil.getString(object,"sessid"));
                }
            } else {
                    result.setSuccess(false);
                    result.setMessage("Service Unavailable Response Status "+response.getStatus());
            }
        } catch (Exception e) {
            e.printStackTrace();
            result.setSuccess(false);
            result.setMessage("Service Unavailable");
        }
        return result;
    }



    public static void main(String... args){
        String strResponse = "{\n" +
                "    \"package\": [\n" +
                "        {\n" +
                "            \"img_url\": \"https://beta-imagica.adlabsimagica.com/sites/default/files/GSGT_LOGO_1.png\",\n" +
                "            \"package_des\": \"Money saver package - Save over ₹ 2800! Get Regular Tickets for 6, Chauffeur driven A/C car travel for 6 from Mumbai/Pune to Imagica & back and All-day meal (lunch+snacks+dinner) at Imagica.\",\n" +
                "            \"package_title\": \"Family of 6\",\n" +
                "            \"quantity\": 1,\n" +
                "            \"cost_per\": \"14999.00\",\n" +
                "            \"cost_total\": \"14999.00\",\n" +
                "            \"currency\": \"INR\",\n" +
                "            \"product_id\": \"101027\",\n" +
                "            \"details\": {\n" +
                "                \"car_exists\": 1,\n" +
                "                \"bus_exists\": 0,\n" +
                "                \"ticket_details\": {\n" +
                "                    \"passenger_type\": \"Adult\",\n" +
                "                    \"quantity\": \"6\"\n" +
                "                },\n" +
                "                \"brunch_addon_details\": {\n" +
                "                    \"quantity\": 6,\n" +
                "                    \"addon_name\": \"Brunch\",\n" +
                "                    \"addon_id\": \"101024\"\n" +
                "                },\n" +
                "                \"car_addon_details\": {\n" +
                "                    \"quantity\": 1,\n" +
                "                    \"addon_name\": \"Car\",\n" +
                "                    \"addon_id\": \"101023\"\n" +
                "                },\n" +
                "                \"tax_details\": {\n" +
                "                    \"service_tax\": {\n" +
                "                        \"name\": \"Service Tax\",\n" +
                "                        \"price_per_quantity\": \"1278.48\"\n" +
                "                    },\n" +
                "                    \"swachh_tax\": {\n" +
                "                        \"name\": \"Swachh Bharat Abhiyaan Tax\",\n" +
                "                        \"price_per_quantity\": \"45.66\"\n" +
                "                    },\n" +
                "                    \"kkc_tax\": {\n" +
                "                        \"name\": \"Krishi Kalyan Cess\",\n" +
                "                        \"price_per_quantity\": \"45.66\"\n" +
                "                    }\n" +
                "                }\n" +
                "            }\n" +
                "        }\n" +
                "    ],\n" +
                "    \"ticket\": {\n" +
                "        \"Regular Ticket\": {\n" +
                "            \"prodType\": \"tickets\",\n" +
                "            \"ticket_des\": \"<p>Unlimited access to all rides and attractions for the entire day!</p>\",\n" +
                "            \"title\": \"Regular Ticket\",\n" +
                "            \"image_url\": \"https://beta-imagica.adlabsimagica.com/sites/default/files/theme%20park_0.png\",\n" +
                "            \"ticket_term\": \"24\",\n" +
                "            \"currency\": \"INR\",\n" +
                "            \"Senior Citizen\": {\n" +
                "                \"type\": \"Senior Citizen\",\n" +
                "                \"quantity\": \"0\",\n" +
                "                \"cost_per\": \"1399.00\",\n" +
                "                \"total_cost\": \"0.00\",\n" +
                "                \"product_id\": \"101038\",\n" +
                "                \"plu\": \"10450751\",\n" +
                "                \"tax_details\": {\n" +
                "                    \"service_tax\": {\n" +
                "                        \"name\": \"Service Tax\",\n" +
                "                        \"price_per_quantity\": \"195.86\"\n" +
                "                    },\n" +
                "                    \"swachh_tax\": {\n" +
                "                        \"name\": \"Swachh Bharat Abhiyaan Tax\",\n" +
                "                        \"price_per_quantity\": \"7.00\"\n" +
                "                    },\n" +
                "                    \"kkc_tax\": {\n" +
                "                        \"name\": \"Krishi Kalyan Cess\",\n" +
                "                        \"price_per_quantity\": \"7.00\"\n" +
                "                    }\n" +
                "                }\n" +
                "            },\n" +
                "            \"College\": {\n" +
                "                \"type\": \"College\",\n" +
                "                \"quantity\": \"0\",\n" +
                "                \"cost_per\": \"1499.00\",\n" +
                "                \"total_cost\": \"0.00\",\n" +
                "                \"product_id\": \"477\",\n" +
                "                \"plu\": \"10150303\",\n" +
                "                \"tax_details\": {\n" +
                "                    \"service_tax\": {\n" +
                "                        \"name\": \"Service Tax\",\n" +
                "                        \"price_per_quantity\": \"209.86\"\n" +
                "                    },\n" +
                "                    \"swachh_tax\": {\n" +
                "                        \"name\": \"Swachh Bharat Abhiyaan Tax\",\n" +
                "                        \"price_per_quantity\": \"7.50\"\n" +
                "                    },\n" +
                "                    \"kkc_tax\": {\n" +
                "                        \"name\": \"Krishi Kalyan Cess\",\n" +
                "                        \"price_per_quantity\": \"7.50\"\n" +
                "                    }\n" +
                "                }\n" +
                "            },\n" +
                "            \"Adult\": {\n" +
                "                \"type\": \"Adult\",\n" +
                "                \"quantity\": \"1\",\n" +
                "                \"cost_per\": \"1900.00\",\n" +
                "                \"total_cost\": \"1900.00\",\n" +
                "                \"product_id\": \"120\",\n" +
                "                \"plu\": \"10150021\",\n" +
                "                \"tax_details\": {\n" +
                "                    \"service_tax\": {\n" +
                "                        \"name\": \"Service Tax\",\n" +
                "                        \"price_per_quantity\": \"266.00\"\n" +
                "                    },\n" +
                "                    \"swachh_tax\": {\n" +
                "                        \"name\": \"Swachh Bharat Abhiyaan Tax\",\n" +
                "                        \"price_per_quantity\": \"9.50\"\n" +
                "                    },\n" +
                "                    \"kkc_tax\": {\n" +
                "                        \"name\": \"Krishi Kalyan Cess\",\n" +
                "                        \"price_per_quantity\": \"9.50\"\n" +
                "                    }\n" +
                "                }\n" +
                "            },\n" +
                "            \"Child\": {\n" +
                "                \"type\": \"Child\",\n" +
                "                \"quantity\": \"1\",\n" +
                "                \"cost_per\": \"1499.00\",\n" +
                "                \"total_cost\": \"1499.00\",\n" +
                "                \"product_id\": \"475\",\n" +
                "                \"plu\": \"10150302\",\n" +
                "                \"tax_details\": {\n" +
                "                    \"service_tax\": {\n" +
                "                        \"name\": \"Service Tax\",\n" +
                "                        \"price_per_quantity\": \"209.86\"\n" +
                "                    },\n" +
                "                    \"swachh_tax\": {\n" +
                "                        \"name\": \"Swachh Bharat Abhiyaan Tax\",\n" +
                "                        \"price_per_quantity\": \"7.50\"\n" +
                "                    },\n" +
                "                    \"kkc_tax\": {\n" +
                "                        \"name\": \"Krishi Kalyan Cess\",\n" +
                "                        \"price_per_quantity\": \"7.50\"\n" +
                "                    }\n" +
                "                }\n" +
                "            }\n" +
                "        },\n" +
                "        \"Imagica Express\": {\n" +
                "            \"prodType\": \"tickets\",\n" +
                "            \"ticket_des\": \"<p>Cut the queue for select rides and attractions to enjoy more in less time.</p>\",\n" +
                "            \"title\": \"Imagica Express\",\n" +
                "            \"image_url\": \"https://beta-imagica.adlabsimagica.com/sites/default/files/Tickets1-%5BConverted%5D_07.png\",\n" +
                "            \"ticket_term\": \"25\",\n" +
                "            \"currency\": \"INR\",\n" +
                "            \"Adult\": {\n" +
                "                \"type\": \"Adult\",\n" +
                "                \"quantity\": \"1\",\n" +
                "                \"cost_per\": \"2899.00\",\n" +
                "                \"total_cost\": \"2899.00\",\n" +
                "                \"product_id\": \"590\",\n" +
                "                \"plu\": \"10250221\",\n" +
                "                \"tax_details\": {\n" +
                "                    \"service_tax\": {\n" +
                "                        \"name\": \"Service Tax\",\n" +
                "                        \"price_per_quantity\": \"405.86\"\n" +
                "                    },\n" +
                "                    \"swachh_tax\": {\n" +
                "                        \"name\": \"Swachh Bharat Abhiyaan Tax\",\n" +
                "                        \"price_per_quantity\": \"14.50\"\n" +
                "                    },\n" +
                "                    \"kkc_tax\": {\n" +
                "                        \"name\": \"Krishi Kalyan Cess\",\n" +
                "                        \"price_per_quantity\": \"14.50\"\n" +
                "                    }\n" +
                "                }\n" +
                "            },\n" +
                "            \"Child\": {\n" +
                "                \"type\": \"Child\",\n" +
                "                \"quantity\": \"1\",\n" +
                "                \"cost_per\": \"2499.00\",\n" +
                "                \"total_cost\": \"2499.00\",\n" +
                "                \"product_id\": \"591\",\n" +
                "                \"plu\": \"10250222\",\n" +
                "                \"tax_details\": {\n" +
                "                    \"service_tax\": {\n" +
                "                        \"name\": \"Service Tax\",\n" +
                "                        \"price_per_quantity\": \"349.86\"\n" +
                "                    },\n" +
                "                    \"swachh_tax\": {\n" +
                "                        \"name\": \"Swachh Bharat Abhiyaan Tax\",\n" +
                "                        \"price_per_quantity\": \"12.50\"\n" +
                "                    },\n" +
                "                    \"kkc_tax\": {\n" +
                "                        \"name\": \"Krishi Kalyan Cess\",\n" +
                "                        \"price_per_quantity\": \"12.50\"\n" +
                "                    }\n" +
                "                }\n" +
                "            },\n" +
                "            \"College\": {\n" +
                "                \"type\": \"College\",\n" +
                "                \"quantity\": \"0\",\n" +
                "                \"cost_per\": \"2499.00\",\n" +
                "                \"total_cost\": \"0.00\",\n" +
                "                \"product_id\": \"592\",\n" +
                "                \"plu\": \"10250223\",\n" +
                "                \"tax_details\": {\n" +
                "                    \"service_tax\": {\n" +
                "                        \"name\": \"Service Tax\",\n" +
                "                        \"price_per_quantity\": \"349.86\"\n" +
                "                    },\n" +
                "                    \"swachh_tax\": {\n" +
                "                        \"name\": \"Swachh Bharat Abhiyaan Tax\",\n" +
                "                        \"price_per_quantity\": \"12.50\"\n" +
                "                    },\n" +
                "                    \"kkc_tax\": {\n" +
                "                        \"name\": \"Krishi Kalyan Cess\",\n" +
                "                        \"price_per_quantity\": \"12.50\"\n" +
                "                    }\n" +
                "                }\n" +
                "            }\n" +
                "        }\n" +
                "    },\n" +
                "    \"visit_date\": \"16-06-2017\",\n" +
                "    \"departure_date\": \"16-06-2017\",\n" +
                "    \"business_unit\": \"Theme Park\",\n" +
                "    \"addons\": {\n" +
                "        \"addon_data\": {\n" +
                "            \"data\": {\n" +
                "                \"Save 30% on OLA Cab\": {\n" +
                "                    \"type_id\": \"40\",\n" +
                "                    \"currency_code\": \"INR\",\n" +
                "                    \"quantity\": 0,\n" +
                "                    \"type\": \"Car\",\n" +
                "                    \"total_addon_cost\": \"0.00\",\n" +
                "                    \"addon_cost\": \"0.00\",\n" +
                "                    \"title\": \"Save 30% on OLA Cab\",\n" +
                "                    \"aid\": \"101181\",\n" +
                "                    \"description\": null,\n" +
                "                    \"img_url\": \"<img src=\\\"https://beta-imagica.adlabsimagica.com/sites/default/files/styles/thumbnail_ticket/public?itok=s919G4jd\\\" />\",\n" +
                "                    \"tax_details\": {\n" +
                "                        \"service_tax\": {\n" +
                "                            \"name\": \"Service Tax\",\n" +
                "                            \"price_per_quantity\": \"0.00\"\n" +
                "                        },\n" +
                "                        \"swachh_tax\": {\n" +
                "                            \"name\": \"Swachh Bharat Abhiyaan Tax\",\n" +
                "                            \"price_per_quantity\": \"0.00\"\n" +
                "                        },\n" +
                "                        \"kkc_tax\": {\n" +
                "                            \"name\": \"Krishi Kalyan Cess\",\n" +
                "                            \"price_per_quantity\": \"0.00\"\n" +
                "                        }\n" +
                "                    }\n" +
                "                },\n" +
                "                \"AC Bus from Mumbai\": {\n" +
                "                    \"type_id\": \"41\",\n" +
                "                    \"currency_code\": \"INR\",\n" +
                "                    \"quantity\": 5,\n" +
                "                    \"type\": \"Bus\",\n" +
                "                    \"pickup_suggestions\": {\n" +
                "                        \"place_id\": \"7805\",\n" +
                "                        \"place_name\": \"Day Entry - Mumbai\",\n" +
                "                        \"location\": [\n" +
                "                            {\n" +
                "                                \"location_id\": \"7822\",\n" +
                "                                \"location_name\": \"9.45am-Opp. Mcdonalds Restaurant-Kalamboli\"\n" +
                "                            },\n" +
                "                            {\n" +
                "                                \"location_id\": \"7807\",\n" +
                "                                \"location_name\": \"7.05am-Gokulanand Hotel-Dahisar East\"\n" +
                "                            },\n" +
                "                            {\n" +
                "                                \"location_id\": \"7808\",\n" +
                "                                \"location_name\": \"7.10am-Neeta Travels, National Park-Borivali East\"\n" +
                "                            },\n" +
                "                            {\n" +
                "                                \"location_id\": \"7809\",\n" +
                "                                \"location_name\": \"7.15am-Neeta Travels, Samta Nagar Police Station-Kandivali East\"\n" +
                "                            },\n" +
                "                            {\n" +
                "                                \"location_id\": \"7810\",\n" +
                "                                \"location_name\": \"7.25am-Kurar Village Bus Stop, Omkar Builders, Near Sky-Walk-Malad East\"\n" +
                "                            },\n" +
                "                            {\n" +
                "                                \"location_id\": \"7811\",\n" +
                "                                \"location_name\": \"7.30am-Neeta Travels, Before Aarey Signal-Goregaon East\"\n" +
                "                            },\n" +
                "                            {\n" +
                "                                \"location_id\": \"7806\",\n" +
                "                                \"location_name\": \"7.00am-Neeta Travels Opp: Gokul Hotel-Borivali West\"\n" +
                "                            },\n" +
                "                            {\n" +
                "                                \"location_id\": \"7818\",\n" +
                "                                \"location_name\": \"9.00am-Neeta Travels, Vashi Plaza Signal-Vashi\"\n" +
                "                            },\n" +
                "                            {\n" +
                "                                \"location_id\": \"7817\",\n" +
                "                                \"location_name\": \"8.45am-Neeta Travels, Yogi Restaurant-Chembur East\"\n" +
                "                            },\n" +
                "                            {\n" +
                "                                \"location_id\": \"7819\",\n" +
                "                                \"location_name\": \"9.15am-L: P: Signal, Besides Bridge-Nerul\"\n" +
                "                            },\n" +
                "                            {\n" +
                "                                \"location_id\": \"7812\",\n" +
                "                                \"location_name\": \"7.45am-Neeta Travels, Western Tower Building, Next to Bisleri Compound-Andheri East\"\n" +
                "                            },\n" +
                "                            {\n" +
                "                                \"location_id\": \"7821\",\n" +
                "                                \"location_name\": \"9.35am-Three Star Hotel, Hiranandani Complex-Kharghar\"\n" +
                "                            },\n" +
                "                            {\n" +
                "                                \"location_id\": \"7816\",\n" +
                "                                \"location_name\": \"8.30am-Manmandir Travels, Opp. Ashray International hotel-Sion East\"\n" +
                "                            },\n" +
                "                            {\n" +
                "                                \"location_id\": \"7820\",\n" +
                "                                \"location_name\": \"9.25am-Traffic Police Chokie-CBD Belapur\"\n" +
                "                            },\n" +
                "                            {\n" +
                "                                \"location_id\": \"7813\",\n" +
                "                                \"location_name\": \"7.55am-Domestic Airport Neeta Travels, Next to Sahara Star Signal-Vile Parle East\"\n" +
                "                            },\n" +
                "                            {\n" +
                "                                \"location_id\": \"7815\",\n" +
                "                                \"location_name\": \"8.15am-Neeta Travels, Kala Nagar Bus Stop-Bandra East\"\n" +
                "                            },\n" +
                "                            {\n" +
                "                                \"location_id\": \"7814\",\n" +
                "                                \"location_name\": \"8.05am-M: A: Travels, Vakola Signal-Santacruz East\"\n" +
                "                            }\n" +
                "                        ]\n" +
                "                    },\n" +
                "                    \"total_addon_cost\": \"1995.00\",\n" +
                "                    \"addon_cost\": \"399.00\",\n" +
                "                    \"title\": \"AC Bus from Mumbai\",\n" +
                "                    \"aid\": \"66\",\n" +
                "                    \"description\": null,\n" +
                "                    \"img_url\": \"<img src=\\\"https://beta-imagica.adlabsimagica.com/sites/default/files/styles/thumbnail_ticket/public?itok=s919G4jd\\\" />\",\n" +
                "                    \"tax_details\": {\n" +
                "                        \"service_tax\": {\n" +
                "                            \"name\": \"Service Tax\",\n" +
                "                            \"price_per_quantity\": \"0.00\"\n" +
                "                        },\n" +
                "                        \"swachh_tax\": {\n" +
                "                            \"name\": \"Swachh Bharat Abhiyaan Tax\",\n" +
                "                            \"price_per_quantity\": \"0.00\"\n" +
                "                        },\n" +
                "                        \"kkc_tax\": {\n" +
                "                            \"name\": \"Krishi Kalyan Cess\",\n" +
                "                            \"price_per_quantity\": \"0.00\"\n" +
                "                        }\n" +
                "                    }\n" +
                "                },\n" +
                "                \"AC Bus from Pune\": {\n" +
                "                    \"type_id\": \"41\",\n" +
                "                    \"currency_code\": \"INR\",\n" +
                "                    \"quantity\": 0,\n" +
                "                    \"type\": \"Bus\",\n" +
                "                    \"pickup_suggestions\": {\n" +
                "                        \"place_id\": \"7823\",\n" +
                "                        \"place_name\": \"Day Entry - Pune\",\n" +
                "                        \"location\": [\n" +
                "                            {\n" +
                "                                \"location_id\": \"7831\",\n" +
                "                                \"location_name\": \"9.00am-Aundh-PUNE\"\n" +
                "                            },\n" +
                "                            {\n" +
                "                                \"location_id\": \"7830\",\n" +
                "                                \"location_name\": \"8.45am-Pashan Circle-PUNE\"\n" +
                "                            },\n" +
                "                            {\n" +
                "                                \"location_id\": \"7832\",\n" +
                "                                \"location_name\": \"9.10am-Vishal Nagar (Jagtap Dairy) - Neeta Travels-PUNE\"\n" +
                "                            },\n" +
                "                            {\n" +
                "                                \"location_id\": \"7833\",\n" +
                "                                \"location_name\": \"9.20am-Hinjewadi Bridge - Neeta Travels-PUNE\"\n" +
                "                            },\n" +
                "                            {\n" +
                "                                \"location_id\": \"7834\",\n" +
                "                                \"location_name\": \"9.30am-P & D Wakad Indira College-PUNE\"\n" +
                "                            },\n" +
                "                            {\n" +
                "                                \"location_id\": \"7829\",\n" +
                "                                \"location_name\": \"8.40am-Bavdhan-PUNE\"\n" +
                "                            },\n" +
                "                            {\n" +
                "                                \"location_id\": \"7828\",\n" +
                "                                \"location_name\": \"8.35am-Maharaja Complex (Shri Sai Travels)-PUNE\"\n" +
                "                            },\n" +
                "                            {\n" +
                "                                \"location_id\": \"7824\",\n" +
                "                                \"location_name\": \"7.45am-Pune Station (Opp. S. T Stand)-PUNE\"\n" +
                "                            },\n" +
                "                            {\n" +
                "                                \"location_id\": \"7835\",\n" +
                "                                \"location_name\": \"10.30am-Lonavala-PUNE\"\n" +
                "                            },\n" +
                "                            {\n" +
                "                                \"location_id\": \"7826\",\n" +
                "                                \"location_name\": \"8.30am-Kothrud (H. O. Neeta Travels)-PUNE\"\n" +
                "                            },\n" +
                "                            {\n" +
                "                                \"location_id\": \"7827\",\n" +
                "                                \"location_name\": \"8.33am-Kinara Hotels (OM Travels)-PUNE\"\n" +
                "                            },\n" +
                "                            {\n" +
                "                                \"location_id\": \"7825\",\n" +
                "                                \"location_name\": \"8.15am-Swargate (Mitra Mandal Chowk)-PUNE\"\n" +
                "                            }\n" +
                "                        ]\n" +
                "                    },\n" +
                "                    \"total_addon_cost\": \"0.00\",\n" +
                "                    \"addon_cost\": \"299.00\",\n" +
                "                    \"title\": \"AC Bus from Pune\",\n" +
                "                    \"aid\": \"218\",\n" +
                "                    \"description\": null,\n" +
                "                    \"img_url\": \"<img src=\\\"https://beta-imagica.adlabsimagica.com/sites/default/files/styles/thumbnail_ticket/public?itok=s919G4jd\\\" />\",\n" +
                "                    \"tax_details\": {\n" +
                "                        \"service_tax\": {\n" +
                "                            \"name\": \"Service Tax\",\n" +
                "                            \"price_per_quantity\": \"0.00\"\n" +
                "                        },\n" +
                "                        \"swachh_tax\": {\n" +
                "                            \"name\": \"Swachh Bharat Abhiyaan Tax\",\n" +
                "                            \"price_per_quantity\": \"0.00\"\n" +
                "                        },\n" +
                "                        \"kkc_tax\": {\n" +
                "                            \"name\": \"Krishi Kalyan Cess\",\n" +
                "                            \"price_per_quantity\": \"0.00\"\n" +
                "                        }\n" +
                "                    }\n" +
                "                },\n" +
                "                \"AC Bus from Thane\": {\n" +
                "                    \"type_id\": \"41\",\n" +
                "                    \"currency_code\": \"INR\",\n" +
                "                    \"quantity\": 0,\n" +
                "                    \"type\": \"Bus\",\n" +
                "                    \"pickup_suggestions\": {\n" +
                "                        \"place_id\": \"7836\",\n" +
                "                        \"place_name\": \"Day Entry - Thane\",\n" +
                "                        \"location\": [\n" +
                "                            {\n" +
                "                                \"location_id\": \"7837\",\n" +
                "                                \"location_name\": \"7:15am-Theen Hath Naka, KTS Travels, Opp. Eternity Mall-Thane West\"\n" +
                "                            },\n" +
                "                            {\n" +
                "                                \"location_id\": \"7838\",\n" +
                "                                \"location_name\": \"7:25am-Johnson & Johnson Co., LBS Marg Road-Mulund West\"\n" +
                "                            },\n" +
                "                            {\n" +
                "                                \"location_id\": \"7844\",\n" +
                "                                \"location_name\": \"8:30am-Mankhurd-Sion Flyover, Maharashtra Nagar BEST Bus Stop-BARC Signal\"\n" +
                "                            },\n" +
                "                            {\n" +
                "                                \"location_id\": \"7843\",\n" +
                "                                \"location_name\": \"8:15am-Cheda Nagar Taxi Stand, Near Ramabai Nagar Signal-Ghatkoper East\"\n" +
                "                            },\n" +
                "                            {\n" +
                "                                \"location_id\": \"7839\",\n" +
                "                                \"location_name\": \"7:35am-Next to Dream Mall, Opp. Smile Time Holidays-Bhandup West\"\n" +
                "                            },\n" +
                "                            {\n" +
                "                                \"location_id\": \"7840\",\n" +
                "                                \"location_name\": \"7:45am-Gopalas Kitchen, Near Midas Motors, LBS Marg Road-Kanjurmarg West\"\n" +
                "                            },\n" +
                "                            {\n" +
                "                                \"location_id\": \"7841\",\n" +
                "                                \"location_name\": \"7:55am-Godrej BEST Bus Stop-Vikhroli West\"\n" +
                "                            },\n" +
                "                            {\n" +
                "                                \"location_id\": \"7842\",\n" +
                "                                \"location_name\": \"8:05am-Opp. Ghatkoper Bus Depo.-Ghatkoper East\"\n" +
                "                            }\n" +
                "                        ]\n" +
                "                    },\n" +
                "                    \"total_addon_cost\": \"0.00\",\n" +
                "                    \"addon_cost\": \"399.00\",\n" +
                "                    \"title\": \"AC Bus from Thane\",\n" +
                "                    \"aid\": \"434\",\n" +
                "                    \"description\": null,\n" +
                "                    \"img_url\": \"<img src=\\\"https://beta-imagica.adlabsimagica.com/sites/default/files/styles/thumbnail_ticket/public?itok=s919G4jd\\\" />\",\n" +
                "                    \"tax_details\": {\n" +
                "                        \"service_tax\": {\n" +
                "                            \"name\": \"Service Tax\",\n" +
                "                            \"price_per_quantity\": \"0.00\"\n" +
                "                        },\n" +
                "                        \"swachh_tax\": {\n" +
                "                            \"name\": \"Swachh Bharat Abhiyaan Tax\",\n" +
                "                            \"price_per_quantity\": \"0.00\"\n" +
                "                        },\n" +
                "                        \"kkc_tax\": {\n" +
                "                            \"name\": \"Krishi Kalyan Cess\",\n" +
                "                            \"price_per_quantity\": \"0.00\"\n" +
                "                        }\n" +
                "                    }\n" +
                "                },\n" +
                "                \"Professional Photographs (Candid Shots)\": {\n" +
                "                    \"type_id\": \"7759\",\n" +
                "                    \"currency_code\": \"INR\",\n" +
                "                    \"quantity\": 5,\n" +
                "                    \"type\": \"Digi Photo\",\n" +
                "                    \"total_addon_cost\": \"250.00\",\n" +
                "                    \"addon_cost\": \"50.00\",\n" +
                "                    \"title\": \"Professional Photographs (Candid Shots)\",\n" +
                "                    \"aid\": \"571\",\n" +
                "                    \"description\": null,\n" +
                "                    \"img_url\": \"<img src=\\\"https://beta-imagica.adlabsimagica.com/sites/default/files/styles/thumbnail_ticket/public?itok=s919G4jd\\\" />\",\n" +
                "                    \"tax_details\": {\n" +
                "                        \"service_tax\": {\n" +
                "                            \"name\": \"Service Tax\",\n" +
                "                            \"price_per_quantity\": \"0.00\"\n" +
                "                        },\n" +
                "                        \"swachh_tax\": {\n" +
                "                            \"name\": \"Swachh Bharat Abhiyaan Tax\",\n" +
                "                            \"price_per_quantity\": \"0.00\"\n" +
                "                        },\n" +
                "                        \"kkc_tax\": {\n" +
                "                            \"name\": \"Krishi Kalyan Cess\",\n" +
                "                            \"price_per_quantity\": \"0.00\"\n" +
                "                        }\n" +
                "                    }\n" +
                "                },\n" +
                "                \"4 Seater Car\": {\n" +
                "                    \"type_id\": \"40\",\n" +
                "                    \"currency_code\": \"INR\",\n" +
                "                    \"quantity\": 0,\n" +
                "                    \"type\": \"Car\",\n" +
                "                    \"total_addon_cost\": \"0.00\",\n" +
                "                    \"addon_cost\": \"3000.00\",\n" +
                "                    \"title\": \"4 Seater Car\",\n" +
                "                    \"aid\": \"470\",\n" +
                "                    \"description\": null,\n" +
                "                    \"img_url\": \"<img src=\\\"https://beta-imagica.adlabsimagica.com/sites/default/files/styles/thumbnail_ticket/public?itok=s919G4jd\\\" />\",\n" +
                "                    \"tax_details\": {\n" +
                "                        \"service_tax\": {\n" +
                "                            \"name\": \"Service Tax\",\n" +
                "                            \"price_per_quantity\": \"0.00\"\n" +
                "                        },\n" +
                "                        \"swachh_tax\": {\n" +
                "                            \"name\": \"Swachh Bharat Abhiyaan Tax\",\n" +
                "                            \"price_per_quantity\": \"0.00\"\n" +
                "                        },\n" +
                "                        \"kkc_tax\": {\n" +
                "                            \"name\": \"Krishi Kalyan Cess\",\n" +
                "                            \"price_per_quantity\": \"0.00\"\n" +
                "                        }\n" +
                "                    }\n" +
                "                }\n" +
                "            }\n" +
                "        },\n" +
                "        \"snomagica_addon_data\": {\n" +
                "            \"Snow Park\": {\n" +
                "                \"prodType\": \"tickets\",\n" +
                "                \"ticket_des\": \"<p>Play with real snow, climb a mountain wall, play snow basketball, and do the cool dance in sub-zero temperatures.</p>\",\n" +
                "                \"title\": \"Snow Park\",\n" +
                "                \"image_url\": \"<img src=\\\"https://beta-imagica.adlabsimagica.com/sites/default/files/styles/thumbnail_ticket/public/Snow%20Park_0.png?itok=SjP5dKrh\\\" />\",\n" +
                "                \"currency\": \"INR\",\n" +
                "                \"session_suggestions\": {\n" +
                "                    \"Morning\": {\n" +
                "                        \"session_id\": 7733,\n" +
                "                        \"session_name\": \"Morning\",\n" +
                "                        \"timeslots\": [\n" +
                "                            {\n" +
                "                                \"timeslot_id\": 7736,\n" +
                "                                \"timeslot_name\": \"10:00 AM - 10:45 AM\"\n" +
                "                            },\n" +
                "                            {\n" +
                "                                \"timeslot_id\": 7737,\n" +
                "                                \"timeslot_name\": \"11:00 AM - 11:45 AM\"\n" +
                "                            },\n" +
                "                            {\n" +
                "                                \"timeslot_id\": 7738,\n" +
                "                                \"timeslot_name\": \"12:00 PM - 12:45 PM\"\n" +
                "                            },\n" +
                "                            {\n" +
                "                                \"timeslot_id\": 7739,\n" +
                "                                \"timeslot_name\": \"01:00 PM - 01:45 PM\"\n" +
                "                            }\n" +
                "                        ]\n" +
                "                    },\n" +
                "                    \"Afternoon\": {\n" +
                "                        \"session_id\": 7734,\n" +
                "                        \"session_name\": \"Afternoon\",\n" +
                "                        \"timeslots\": [\n" +
                "                            {\n" +
                "                                \"timeslot_id\": 7740,\n" +
                "                                \"timeslot_name\": \"02:00 PM - 02:45 PM\"\n" +
                "                            },\n" +
                "                            {\n" +
                "                                \"timeslot_id\": 7742,\n" +
                "                                \"timeslot_name\": \"03:00 PM - 03:45 PM\"\n" +
                "                            },\n" +
                "                            {\n" +
                "                                \"timeslot_id\": 7743,\n" +
                "                                \"timeslot_name\": \"04:00 PM - 04:45 PM\"\n" +
                "                            },\n" +
                "                            {\n" +
                "                                \"timeslot_id\": 7744,\n" +
                "                                \"timeslot_name\": \"05:00 PM - 05:45 PM\"\n" +
                "                            }\n" +
                "                        ]\n" +
                "                    },\n" +
                "                    \"Evening\": {\n" +
                "                        \"session_id\": 7735,\n" +
                "                        \"session_name\": \"Evening\",\n" +
                "                        \"timeslots\": [\n" +
                "                            {\n" +
                "                                \"timeslot_id\": 7745,\n" +
                "                                \"timeslot_name\": \"06:00 PM - 06:45 PM\"\n" +
                "                            },\n" +
                "                            {\n" +
                "                                \"timeslot_id\": 7746,\n" +
                "                                \"timeslot_name\": \"07:00 PM - 07:45 PM\"\n" +
                "                            }\n" +
                "                        ]\n" +
                "                    }\n" +
                "                },\n" +
                "                \"All\": {\n" +
                "                    \"type\": \"All\",\n" +
                "                    \"quantity\": \"All\",\n" +
                "                    \"cost_per\": \"399.00\",\n" +
                "                    \"total_cost\": \"0.00\",\n" +
                "                    \"product_id\": \"459\",\n" +
                "                    \"tax_details\": {\n" +
                "                        \"service_tax\": {\n" +
                "                            \"name\": \"Service Tax\",\n" +
                "                            \"price_per_quantity\": \"55.86\"\n" +
                "                        },\n" +
                "                        \"swachh_tax\": {\n" +
                "                            \"name\": \"Swachh Bharat Abhiyaan Tax\",\n" +
                "                            \"price_per_quantity\": \"2.00\"\n" +
                "                        },\n" +
                "                        \"kkc_tax\": {\n" +
                "                            \"name\": \"Krishi Kalyan Cess\",\n" +
                "                            \"price_per_quantity\": \"2.00\"\n" +
                "                        }\n" +
                "                    }\n" +
                "                }\n" +
                "            }\n" +
                "        },\n" +
                "        \"addon_brunch_data\": [\n" +
                "            {\n" +
                "                \"Lunch + Snacks + Dinner\": {\n" +
                "                    \"currency_code\": \"INR\",\n" +
                "                    \"pid\": \"552\",\n" +
                "                    \"quantity\": 0,\n" +
                "                    \"total_addon_cost\": \"0.00\",\n" +
                "                    \"addon_cost\": \"485.00\",\n" +
                "                    \"img_url\": \"<img src=\\\"https://beta-imagica.adlabsimagica.com/sites/default/files/styles/thumbnail_ticket/public/Meal_2.png?itok=EYxJj5PW\\\" />\",\n" +
                "                    \"description\": \"<p>Choose from:</p><div><strong>All-day Meal Plan (A la Carte)</strong>:</div><div>Includes Breakfast, Lunch, Snacks &amp; Dinner. Not applicable at Imagica Capital</div><div>&nbsp;</div><div><strong>A la Carte Lunch and Dinner</strong>:</div><div>Includes Lunch, Snacks &amp; Dinner. Not applicable at Imagica Capital</div><div>&nbsp;</div><div><strong>Grand Imagica Lunch</strong>:</div><div>Enjoy unlimited Buffet Lunch at Imagica Capital</div><div>&nbsp;</div><p>&nbsp;</p>\",\n" +
                "                    \"title\": \"Pre-Book your Meal and Save!\",\n" +
                "                    \"aid\": \"552\",\n" +
                "                    \"brunch_title\": \"Lunch + Snacks + Dinner\",\n" +
                "                    \"tax_details\": {\n" +
                "                        \"service_tax\": {\n" +
                "                            \"name\": \"Service Tax\",\n" +
                "                            \"price_per_quantity\": \"0.00\"\n" +
                "                        },\n" +
                "                        \"swachh_tax\": {\n" +
                "                            \"name\": \"Swachh Bharat Abhiyaan Tax\",\n" +
                "                            \"price_per_quantity\": \"0.00\"\n" +
                "                        },\n" +
                "                        \"kkc_tax\": {\n" +
                "                            \"name\": \"Krishi Kalyan Cess\",\n" +
                "                            \"price_per_quantity\": \"0.00\"\n" +
                "                        }\n" +
                "                    }\n" +
                "                }\n" +
                "            },\n" +
                "            {\n" +
                "                \"All Restaurants except Imagica capital\": {\n" +
                "                    \"currency_code\": \"INR\",\n" +
                "                    \"pid\": \"553\",\n" +
                "                    \"quantity\": 0,\n" +
                "                    \"total_addon_cost\": \"0.00\",\n" +
                "                    \"addon_cost\": \"585.00\",\n" +
                "                    \"img_url\": \"<img src=\\\"https://beta-imagica.adlabsimagica.com/sites/default/files/styles/thumbnail_ticket/public/Meal_2.png?itok=EYxJj5PW\\\" />\",\n" +
                "                    \"description\": \"<p>Choose from:</p><div><strong>All-day Meal Plan (A la Carte)</strong>:</div><div>Includes Breakfast, Lunch, Snacks &amp; Dinner. Not applicable at Imagica Capital</div><div>&nbsp;</div><div><strong>A la Carte Lunch and Dinner</strong>:</div><div>Includes Lunch, Snacks &amp; Dinner. Not applicable at Imagica Capital</div><div>&nbsp;</div><div><strong>Grand Imagica Lunch</strong>:</div><div>Enjoy unlimited Buffet Lunch at Imagica Capital</div><div>&nbsp;</div><p>&nbsp;</p>\",\n" +
                "                    \"title\": \"Pre-Book your Meal and Save!\",\n" +
                "                    \"aid\": \"553\",\n" +
                "                    \"brunch_title\": \"All Restaurants except Imagica capital\",\n" +
                "                    \"tax_details\": {\n" +
                "                        \"service_tax\": {\n" +
                "                            \"name\": \"Service Tax\",\n" +
                "                            \"price_per_quantity\": \"0.00\"\n" +
                "                        },\n" +
                "                        \"swachh_tax\": {\n" +
                "                            \"name\": \"Swachh Bharat Abhiyaan Tax\",\n" +
                "                            \"price_per_quantity\": \"0.00\"\n" +
                "                        },\n" +
                "                        \"kkc_tax\": {\n" +
                "                            \"name\": \"Krishi Kalyan Cess\",\n" +
                "                            \"price_per_quantity\": \"0.00\"\n" +
                "                        }\n" +
                "                    }\n" +
                "                }\n" +
                "            }\n" +
                "        ]\n" +
                "    }\n" +
                "}";

//        System.err.println(strResponse.trim());

    }


}
