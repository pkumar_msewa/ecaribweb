package com.imagica.api.impl;

import java.io.IOException;

import javax.ws.rs.core.MultivaluedMap;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.imagica.api.IServicesApi;
import com.imagica.model.request.OrderDTO;
import com.imagica.model.request.PaymentRequest;
import com.imagica.model.request.SearchDTO;
import com.imagica.model.response.OrderResponse;
import com.imagica.model.response.PaymentResponse;
import com.imagica.model.response.SearchResponse;
import com.imagica.util.IParkConvertUtil;
import com.imagica.util.ImagicaUtil;
import com.payqwikweb.util.JSONParserUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;

public class ServicesApi implements IServicesApi {
	
	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public SearchResponse getParkSearchResult(SearchDTO dto) {
        SearchResponse result = new SearchResponse();
        MultivaluedMap<String,String> searchForm = new MultivaluedMapImpl();
        searchForm.add("adult",String.valueOf(dto.getAdult()));
        searchForm.add("child",String.valueOf(dto.getChild()));
        searchForm.add("sc",String.valueOf(dto.getSenior()));
        searchForm.add("college",String.valueOf(dto.getCollege()));
        searchForm.add("total_day",String.valueOf(dto.getTotalDays()));
        searchForm.add("date_visit",dto.getDateVisit());
        searchForm.add("date_departure",dto.getDateVisit());
        searchForm.add("destination",dto.getDestination().getValue());
        searchForm.add("version","new");
        try {
            Client client = Client.create();
            client.addFilter(new LoggingFilter(System.out));
            WebResource webResource = client.resource(ImagicaUtil.PARK_SEARCH);
            ClientResponse resp = webResource.header("cookie",dto.getCookie()).header("x-csrf-token",dto.getToken()).post(ClientResponse.class, searchForm);
            System.err.println("response ::" + resp);
            String strResponse = resp.getEntity(String.class);
            System.err.println(strResponse);
            if(resp.getStatus() == 200) {
                JSONObject response = new JSONObject(strResponse.trim());
                if (response != null) {
                    if(response.has("error")) {
                        JSONArray array = JSONParserUtil.getArray(response,"error");
                        result.setErrorMsgs(IParkConvertUtil.getErrorsFromArray(array));
                    }else {
                        result.setSuccess(true);
                        result.setVisitDate(JSONParserUtil.getString(response, "visit_date"));
                        result.setDepartureDate(JSONParserUtil.getString(response, "departure_date"));
                        result.setBusinessUnit(JSONParserUtil.getString(response, "business_unit"));
                        JSONArray packageArray = JSONParserUtil.getArray(response, "package");
                        result.setPackages(IParkConvertUtil.packageDetailList(packageArray));
                        JSONObject ticket = JSONParserUtil.getObject(response, "ticket");
                        result.setTickets(IParkConvertUtil.getTicketList(ticket));
                        JSONObject addONS = response.getJSONObject("addons");
                        result.setAddOns(IParkConvertUtil.getAddOns(addONS));
                        System.err.println(result.toString());
                        ObjectMapper mapper = new ObjectMapper();
                        mapper.writeValueAsString(result);
                    }
                }else {
                    result.setSuccess(false);
                    result.setMessage("Error Occurred ,Try again later");
                }
            }else {
                result.setSuccess(false);
                result.setMessage("Response from Imagica is "+resp.getStatus());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (JsonGenerationException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public OrderResponse placeOrderInCart(OrderDTO dto) {
        OrderResponse result = new OrderResponse();
        MultivaluedMap<String,String> orderForm = IParkConvertUtil.convertToOrderRequest(dto);
        try {
            Client client = Client.create();
            client.addFilter(new LoggingFilter(System.out));
            WebResource webResource = client.resource(ImagicaUtil.BOOK_ORDER);
            ClientResponse resp = webResource.header("cookie",dto.getCookie()).header("x-csrf-token",dto.getToken()).post(ClientResponse.class, orderForm);
            String strResponse = resp.getEntity(String.class);
            logger.info("response of place order::"+ "::" + resp +"\n \n "+strResponse);
            if(resp.getStatus() == 200) {
                JSONObject response = new JSONObject(strResponse.trim());
                if (response != null) {
                    if(response.has("error")) {
                        JSONArray array = JSONParserUtil.getArray(response,"error");
                        result.setErrorsList(IParkConvertUtil.getErrorsFromArray(array));
                    } else {
                        result.setSuccess(true);
                        result.setUid(JSONParserUtil.getString(response,"uid"));
                        result.setOrderId(JSONParserUtil.getString(response,"order_id"));
                        JSONObject commerceTotal = JSONParserUtil.getObject(response,"commerce_order_total");
                        if(commerceTotal != null) {
                            result.setTotalAmount((JSONParserUtil.getDouble(commerceTotal,"amount"))/100);
                            logger.info("this is total::"+JSONParserUtil.getDouble(commerceTotal,"amount"));
                            logger.info("total price ::" + result.getTotalAmount());
                            result.setCurrencyCode(JSONParserUtil.getString(commerceTotal,"currency"));
                            JSONObject data = JSONParserUtil.getObject(commerceTotal,"data");
                            if(data != null) {
                                JSONArray components = JSONParserUtil.getArray(data,"components");
                                if(components != null) {
                                    for (int c = 0; c < components.length(); c++) {
                                        JSONObject priceObj = components.getJSONObject(c);
                                        String key = JSONParserUtil.getString(priceObj, "name");
                                        if (key.equalsIgnoreCase("base_price")) {
                                            
                                        	result.setBaseAmount(IParkConvertUtil.getPriceFromObj(JSONParserUtil.getObject(priceObj, "price")));
                                        	logger.info("base:"+result.getBaseAmount());
                                        } else if (key.equalsIgnoreCase("service_tax_calculation")) {
                                            result.setServiceTax(IParkConvertUtil.getPriceFromObj(JSONParserUtil.getObject(priceObj, "price")));
                                            logger.info("service::"+result.getServiceTax());
                                        } else if (key.equalsIgnoreCase("swachh_bharat_abhiyaan_tax")) {
                                            result.setSbCess(IParkConvertUtil.getPriceFromObj(JSONParserUtil.getObject(priceObj, "price")));
                                            logger.info("sbcess :::"+result.getSbCess());
                                        } else if (key.equalsIgnoreCase("krishi_kalyan_cess")) {
                                            result.setKkCess(IParkConvertUtil.getPriceFromObj(JSONParserUtil.getObject(priceObj, "price")));
                                            logger.info("the kkCess is:::"+result.getKkCess());
                                        }
                                    }
                                }
                            }
                        }
                    }
                }else {
                    result.setSuccess(false);
                    result.setMessage("Error Occurred ,Try again later");
                }
            }else {
                result.setSuccess(false);
                result.setMessage("Response from Imagica is "+resp.getStatus());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public PaymentResponse processPayment(PaymentRequest dto) {
        PaymentResponse result = new PaymentResponse();
        MultivaluedMap<String,String> orderForm = IParkConvertUtil.getPaymentForm(dto);
        try {
            Client client = Client.create();
            client.addFilter(new LoggingFilter(System.out));
            WebResource webResource = client.resource(ImagicaUtil.PROCESS_PAYMENT);
            client.addFilter(new LoggingFilter(System.out));
            ClientResponse resp = webResource.header("cookie",dto.getCookie()).header("x-csrf-token",dto.getToken()).post(ClientResponse.class, orderForm);
            client.addFilter(new LoggingFilter(System.out));
            String strResponse = resp.getEntity(String.class);
            System.err.println("response is::::" + strResponse);
            if(resp.getStatus() == 200) {
                JSONObject response = new JSONObject(strResponse.trim());
                if (response != null) {
                    if(response.has("success_payment")) {
                        result.setSuccess(true);
                        String profileIdMsg = JSONParserUtil.getString(response,"success_profile");
                        String []msgSplit = profileIdMsg.split(" ");
                        result.setProfileId(msgSplit[msgSplit.length-1]);
                    } else  {
                        if(response.has("error")) {
                            result.setSuccess(false);
                            JSONArray array = JSONParserUtil.getArray(response, "error");
                            result.setErrorMsg(IParkConvertUtil.getErrorsFromArray(array));
                        } else if(response.has("Error_Message")) {
                            result.setMessage(JSONParserUtil.getString(response,"Error_Message"));
                            result.setSuccess(false);
                        }
                    }
                }else {
                    result.setSuccess(false);
                    result.setMessage("Error Occurred ,Try again later");
                }
            }else {
                result.setSuccess(false);
                result.setMessage("Response from Imagica is "+resp.getStatus());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }


}
