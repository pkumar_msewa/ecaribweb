package com.imagica.api;

import com.imagica.model.request.LoginDTO;
import com.imagica.model.response.LoginResponse;

public interface ILoginApi {

    LoginResponse createSession(LoginDTO dto);
}
