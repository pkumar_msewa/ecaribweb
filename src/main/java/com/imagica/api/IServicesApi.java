package com.imagica.api;

import com.imagica.model.request.OrderDTO;
import com.imagica.model.request.PaymentRequest;
import com.imagica.model.request.SearchDTO;
import com.imagica.model.response.OrderResponse;
import com.imagica.model.response.PaymentResponse;
import com.imagica.model.response.SearchResponse;

public interface IServicesApi {

    SearchResponse getParkSearchResult(SearchDTO dto);
    OrderResponse  placeOrderInCart(OrderDTO dto);
    PaymentResponse processPayment(PaymentRequest dto);
}
