package com.payqwik.visa.api;

import com.payqwikweb.app.model.request.GetAccountNumber;
import com.payqwikweb.app.model.request.VisaRequest;
import com.payqwikweb.app.model.response.VisaResponse;

public interface IVisaApi {

	String visaTransactionM2P(String req);
	
	String visaResponseM2P(VisaRequest requests);
	
	String visaFetchStatusM2P(String transactionId);
	
	VisaResponse visaM2PStatus(VisaRequest resp);

	VisaResponse getAccountNumberFromM2PEnc(String req);

	VisaResponse getAccountNumberFromM2P(GetAccountNumber req);
	

}
