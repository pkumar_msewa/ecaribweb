package com.payqwik.visa.impl;

import java.io.StringWriter;
import java.util.ArrayList;

import javax.ws.rs.core.MediaType;

import org.codehaus.jettison.json.JSONObject;

import com.payqwik.visa.api.IVisaApi;
import com.payqwikweb.app.metadatas.UrlMetadatas;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Language;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.Version;
import com.payqwikweb.app.model.request.GetAccountNumber;
import com.payqwikweb.app.model.request.VisaRequest;
import com.payqwikweb.app.model.response.VisaResponse;
import com.payqwikweb.app.utils.SecurityUtils;
import com.payqwikweb.util.APIUtils;
import com.payqwikweb.util.LogCat;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;

public class VisaApi implements IVisaApi{

	@Override
	public String visaTransactionM2P(String req) {
		String resp = "";
		//
		Client client=Client.create();
		client.addFilter(new LoggingFilter(System.out));
		WebResource resource = client.resource("https://vijaya.yappay.in/Yappay/payment-manager/payment");
		StringWriter w = new StringWriter();
		w.write(req);
		ClientResponse clientResponse = resource.accept("application/json").header("TENANT", "VIJAYA")
				.header("Authorization", "Basic YWRtaW46YWRtaW4=").type("application/json")
				.post(ClientResponse.class, req);
		if (clientResponse.getStatus() == 200) {
			String output = clientResponse.getEntity(String.class);
			resp = output;
			System.out.println("OUT :: " + output);
		} else { 
			System.out.println("OUT Failure:: " + clientResponse.getStatus());
		}			
		return resp;
	}

	@Override
	public String visaResponseM2P(VisaRequest request) {
			VisaResponse resp = new VisaResponse();
			try {
				JSONObject payload = new JSONObject();
				payload.put("sessionId", request.getSessionId());
				payload.put("request", request.getRequest());
				LogCat.print(payload.toString());
				Client client = Client.create();
				System.out.println("URL : "+UrlMetadatas.visaInitatedRequestUrl(Version.VERSION_1, Role.USER, Device.ANDROID, Language.ENGLISH));
				WebResource webResource = client.resource(
						UrlMetadatas.visaInitatedRequestUrl(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
				ClientResponse response = webResource.accept("application/json").type("application/json")
						.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
				String strResponse = response.getEntity(String.class);
				if (response.getStatus() != 200) {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				} else {
					if (strResponse != null) {
						LogCat.print(strResponse);
						org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
						if (jobj != null) {
							final String status = (String) jobj.get("status");
							final String code = (String) jobj.get("code");
							final String message = (String) jobj.get("message");
							final Object details = (Object) jobj.get("details");
							if (code.equalsIgnoreCase("S00")) {
								resp.setSuccess(true);
							} else {
								resp.setSuccess(false);
							}
							resp.setCode(code);
							resp.setStatus(status);
							resp.setMessage(message);
							resp.setResponse(strResponse);
							resp.setDetails(details);
						}
					} else {
						resp.setSuccess(false);
						resp.setCode("F00");
						resp.setMessage("Service unavailable");
						resp.setStatus("FAILED");
						resp.setResponse(APIUtils.getFailedJSON().toString());
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());

			}
			return resp.toString();
	}
	
	@Override
	public String visaFetchStatusM2P(String trans) {
		String result="";
		System.out.println("Transaction "+trans);
		try {
			System.out.println("Transaction :: "+trans);
			WebResource resource = Client.create()
					.resource("https://vijaya.yappay.in/Yappay/txn-manager/fetch/"+trans);

			ClientResponse clientResponse = resource.header("Authorization", "Basic YWRtaW46YWRtaW4=")
					.header("TENANT", "VIJAYA").type(MediaType.APPLICATION_JSON).get(ClientResponse.class);

			if (clientResponse.getStatus() == 200) {
				String output = clientResponse.getEntity(String.class);
				System.out.println("OUT :: " + output);
				result = output;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public VisaResponse visaM2PStatus(VisaRequest request) {
		
		VisaResponse resp = new VisaResponse();
		try {
			System.out.println("Request ::"+request.getRequest());
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("request",request.getDetails());
			payload.put("data", request.getData());
			payload.put("amount", "100.0");
			payload.put("txRef", "4714672610517850883");
			payload.put("transactionStatus", "PAYMENT_SUCCESS");
			payload.put("externalTransactionId", "12345678912345678900");
			payload.put("object", request.getRequest());
			payload.put("merchantName", "merchantName");
			payload.put("merchantId", "merchantName");
			payload.put("merchantAddress", "merchantName");
			
			
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.visaInitatedResponseUrl(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					System.out.println("Response from last  :: "+strResponse);
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
//						final Object details = (Object) jobj.get("details");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public VisaResponse getAccountNumberFromM2P(GetAccountNumber req) {
		VisaResponse resp = new VisaResponse();
		try {
			JSONObject payload = new JSONObject();
			ArrayList list = new  ArrayList();
			payload.put("entityId", "CUST001");
			payload.put("firstName",req.getFirstName());
			payload.put("lastName", req.getLastName());
			payload.put("contactNo", req.getUsername());
			payload.put("business", "VIJAYA");
			payload.put("productId", "GENERAL");
			list.add("VISA");
			list.add("MASTER");
			list.add("RUPAY");
			payload.put("network", list);			
			
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.visaInitatedResponseUrl(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("TENANT", "VIJAYA").post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				}
		}catch(Exception e){

		}
		
		
		return resp;
		
	}
	
	
	public static VisaResponse getAccountNumberFromM2PTest(GetAccountNumber req) {
		VisaResponse resp = new VisaResponse();
		
		try {

			JSONObject payload = new JSONObject();
			ArrayList list = new  ArrayList();
			payload.put("entityId", "CUST001");
			payload.put("firstName",req.getFirstName());
			payload.put("lastName", req.getLastName());
			payload.put("contactNo", req.getUsername());
			payload.put("business", "VIJAYA");
			payload.put("productId", "GENERAL");
			list.add("VISA");
			list.add("MASTER");
			list.add("RUPAY");
			payload.put("network", list);			
			
			System.out.println("payload  "+payload);
			Client client = Client.create();
			WebResource webResource = client.resource("https://vijaya.yappay.in/Yappay/registration-manager/v2/register");
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("TENANT", "VIJAYA").post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				System.out.println("OUT put  "+strResponse);
				org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
				org.json.JSONArray arr = null;
				if (jobj != null) {
					arr  = (org.json.JSONArray) jobj.getJSONObject("result").getJSONArray("cardDetails");
						System.out.println("ARR  "+arr);
						org.json.JSONObject j = arr.getJSONObject(0);
						resp.setDetails(j);
				}
				}
		}catch(Exception e){
			}
		
		
		return resp;
		
	}
	
	
	@Override
	public VisaResponse getAccountNumberFromM2PEnc(String req) {
			VisaResponse resp = new VisaResponse();
		try {
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource("https://vijaya.yappay.in/Yappay/registration-manager/v2/register");
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("TENANT", "VIJAYA").post(ClientResponse.class, req);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				resp.setMessage(strResponse);
				resp.setCode("S00");
				}
		}catch(Exception e){
			}
		
		return resp;
	}
	
	}

	

