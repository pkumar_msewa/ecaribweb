package com.payqwik.visa.util;

public class VisaMerchantTransaction {

	
	private String amount;
	private String balance;
	private String transactionType;
	private String type;
	private String time;
	private String txRef;
	private String businessId;
	private String beneficiaryName;
	private String beneficiaryType;
	private String beneficiaryId;
	private String description;
	private String otherPartyName;
	private String otherPartyId;
	private String txnOrigin;
	private String transactionStatus;
	private String status;
	private String yourWallet;
	private String beneficiaryWallet;
	private String externalTransactionId;
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getBalance() {
		return balance;
	}
	public void setBalance(String balance) {
		this.balance = balance;
	}
	public String getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getTxRef() {
		return txRef;
	}
	public void setTxRef(String txRef) {
		this.txRef = txRef;
	}
	public String getBusinessId() {
		return businessId;
	}
	public void setBusinessId(String businessId) {
		this.businessId = businessId;
	}
	public String getBeneficiaryName() {
		return beneficiaryName;
	}
	public void setBeneficiaryName(String beneficiaryName) {
		this.beneficiaryName = beneficiaryName;
	}
	public String getBeneficiaryType() {
		return beneficiaryType;
	}
	public void setBeneficiaryType(String beneficiaryType) {
		this.beneficiaryType = beneficiaryType;
	}
	public String getBeneficiaryId() {
		return beneficiaryId;
	}
	public void setBeneficiaryId(String beneficiaryId) {
		this.beneficiaryId = beneficiaryId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getOtherPartyName() {
		return otherPartyName;
	}
	public void setOtherPartyName(String otherPartyName) {
		this.otherPartyName = otherPartyName;
	}
	public String getOtherPartyId() {
		return otherPartyId;
	}
	public void setOtherPartyId(String otherPartyId) {
		this.otherPartyId = otherPartyId;
	}
	public String getTxnOrigin() {
		return txnOrigin;
	}
	public void setTxnOrigin(String txnOrigin) {
		this.txnOrigin = txnOrigin;
	}
	public String getTransactionStatus() {
		return transactionStatus;
	}
	public void setTransactionStatus(String transactionStatus) {
		this.transactionStatus = transactionStatus;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getYourWallet() {
		return yourWallet;
	}
	public void setYourWallet(String yourWallet) {
		this.yourWallet = yourWallet;
	}
	public String getBeneficiaryWallet() {
		return beneficiaryWallet;
	}
	public void setBeneficiaryWallet(String beneficiaryWallet) {
		this.beneficiaryWallet = beneficiaryWallet;
	}
	public String getExternalTransactionId() {
		return externalTransactionId;
	}
	public void setExternalTransactionId(String externalTransactionId) {
		this.externalTransactionId = externalTransactionId;
	}
	
}
