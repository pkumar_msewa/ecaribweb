package com.payqwik.visa.util;

import com.payqwikweb.model.app.response.PaginationDTO;
import com.payqwikweb.model.app.response.TransactionM2PDTO;

import java.util.List;

public class QRResponseDTO {

	private String qrCode;
	private String message;
	private boolean success;
	private String code;
	private String entityId;
	private List<TransactionM2PDTO> dto;
	private PaginationDTO pagination;

	public PaginationDTO getPagination() {
		return pagination;
	}

	public void setPagination(PaginationDTO pagination) {
		this.pagination = pagination;
	}

	public String getQrCode() {
		return qrCode;
	}

	public void setQrCode(String qrCode) {
		this.qrCode = qrCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public List<TransactionM2PDTO> getDto() {
		return dto;
	}

	public void setDto(List<TransactionM2PDTO> dto) {
		this.dto = dto;
	}
}
