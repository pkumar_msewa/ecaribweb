package com.payqwik.visa.util;
import java.util.HashMap;
import java.util.Map;


public class YappayEncryptedResponse {

	Map<String, String> headers = new HashMap<>();

	String body;

	public Map<String, String> getHeaders() {
		return headers;
	}

	public void setHeaders(Map<String, String> headers) {
		this.headers = headers;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public void addHeader(String key, String value) {
		headers.put(key, value);
	}
}
