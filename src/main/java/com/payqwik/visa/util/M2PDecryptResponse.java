package com.payqwik.visa.util;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.payqwikweb.app.metadatas.UrlMetadatas;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Language;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.Version;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class M2PDecryptResponse {

	public static String m2pDecryptResponse(String enc){
		
		
		String strResponse = null;
			JSONObject payload = new JSONObject();
			try {
				payload.put("enc", enc);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.decryptResponse(Version.VERSION_1, Role.MERCHANT,Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.post(ClientResponse.class,payload);
			if(response!=null && response.getStatus()==200){
				response.getEntity(String.class);
				if (strResponse != null) {
					return strResponse;
				
				
			}
	}
			return strResponse;
	}
}

