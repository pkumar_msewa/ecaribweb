package com.payqwik.visa.util;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.Random;


public class YappayEncryptedRequest {

	private String token; // body, business pvt Key

	private String body; // body, key

	private String entity; // entityId, m2p Pub Key

	private String key; // M2P Pub Key, key

	private String refNo;

	@JsonProperty
	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	@JsonProperty
	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	@JsonProperty
	public String getEntity() {
		return entity;
	}

	public void setEntity(String entity) {
		this.entity = entity;
	}

	@JsonProperty
	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	@JsonProperty
	public String getRefNo() {
		return refNo;
	}

	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}
	

}
