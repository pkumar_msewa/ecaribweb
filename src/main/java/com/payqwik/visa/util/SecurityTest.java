package com.payqwik.visa.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.Signature;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.codehaus.jackson.map.ObjectMapper;

//import com.fasterxml.jackson.core.JsonProcessingException;
//import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
public class SecurityTest {

		/*public static void main(String[] a) {*/
//
		/*SecurityTest test = new SecurityTest();*/

		/*try {
			 PrivateKey k = readPrivateKeyFromFile("/home/ajay/Desktop/visa/KEYS/private.pkcs8");
			 System.out.println("k "+k);
			// System.out.println(test.getmVisaRequest());
			String response = test.sendRequest(test.encodeRequest(test.getmVisaRequest(), "1234123412341243", "VIJAYA"), "http://vijaya.sit.yappay.in:9000/Yappay/payment-manager/payment");
			System.out.println(4);
			System.out.println("Response :: " + response);
			ObjectMapper mapper = new ObjectMapper();
			System.out.println("Result "+test.decodeResponse(mapper.readValue(response, YappayEncryptedResponse.class)));
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
*/
//
//	/*
//	 * { "entityId" : "PIGGY", "entityType" : "BUSINESS", "name" : "PIGGY",
//	 * "address" : "West Mambalam", "city" : "Chennai", "state" : "Tamil Nadu",
//	 * "country" : "India", "pincode" : "600033", "contactNo" : "+919000000001"
//	 * 
//	 * }
//	 */
//
	/*public static String getmVisaRequest() throws IOException {
		
		VisaModel visaModel = new VisaModel();
		visaModel.setFirstName("Ajay");
		visaModel.setLastName("Kumar");
		visaModel.setContactNo("9449147913");
		visaModel.setSenderCardNo("1234567890123456");
		visaModel.setBillRefNo("INVOICE0001");
		visaModel.setMerchantData("0A45673487751OJoe’sSandwich2458123BBangalore42IN53INR6C0000000002507QBill57453289Q");
		visaModel.setToEntityId("");
		visaModel.setFromEntityId("VCust001");
		visaModel.setProductId("GENERAL");
		visaModel.setDescription("Change");
		visaModel.setAmount(100.00);
		visaModel.setTransactionType("C2M");
		visaModel.setTransactionOrigin("MOBILE");
		visaModel.setBusinessEntityId("VIJAYA");
		visaModel.setBusinessType("VIJAYA");
		visaModel.setExternalTransactionId("12345678912345678900");
//		// 0A45673487751OJoe’s
//		// Sandwich2458123BBangalore42IN53INR6C0000000002507QBill57453289Q
//		// 0F1234501110016011CMERCHANT000121237CHENNAI42IN53356M2PYN75005601
		ObjectMapper mapper = new ObjectMapper();
//
		return mapper.writeValueAsString(visaModel);
//
	}*/
//
////	public String getData() throws JsonProcessingException {
////		RegistrationDto dto = new RegistrationDto();
////		dto.setEntityId("PIGGY");
////		dto.setFirstName("PIGGY");
////		dto.setAddress("WM");
////		dto.setCity("Chennai");
////		dto.setState("TN");
////		dto.setCountry("INDIA");
////		dto.setPinCode(600033);
////		dto.setEntityType(EntityType.BUSINESS.getValue());
////		dto.setContactNo("+919500125352");
////		ObjectMapper mapper = new ObjectMapper();
////		return mapper.writeValueAsString(dto);
////	}
//
	public String sendRequest(String body, String url) throws IOException {
		String out=null;
		try {

			WebResource resource = Client.create().resource("http://vijaya.sit.yappay.in:9000/Yappay/payment-manager/payment");

			ClientResponse clientResponse = resource.accept("application/json").header("TENANT", "VIJAYA")
					.header("Authorization", "Basic YWRtaW46YWRtaW4=").type("application/json")
					.post(ClientResponse.class, body);

			if (clientResponse.getStatus() == 200) {
				String output = clientResponse.getEntity(String.class);
				out=output;
				System.out.println("OUT :: " + output);
			} else {
				System.out.println("OUT Failure:: " + clientResponse.getStatus());
			}
		}catch(Exception e){
			
		}
		return out;
	}
//			// System.out.println("3");
//			// httpclient.getCredentialsProvider().setCredentials(new
//			// AuthScope(AuthScope.ANY_HOST, AuthScope.ANY_PORT),
//			// new UsernamePasswordCredentials("admin", "admin"));
//			// System.out.println("4.1");
//			// HttpPost httppost = new HttpPost(url.toString());
//			// System.out.println("4.2");
//			// httppost.setHeader("Content-Type", "application/json");
//			// System.out.println("4.3");
//			// StringEntity se = new StringEntity(body.toString());
//			// System.out.println("4.4");
//			// se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
//			// "application/json"));
//			// System.out.println("4.5");
//			// httppost.setEntity(se);
//			// System.out.println("4.6");
//			// HttpResponse response = null ;
//			// System.out.println("4.7");
//			// httpclient.execute(httppost);
//			// System.out.println("4.8");
//			// String temp = EntityUtils.toString(response.getEntity());
//			// System.out.println("4.9");
//			// return temp;
//			//
//			// } catch (ClientProtocolException e) {
//
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		
//		return out;
//	}
//
	public String encodeRequest(String requestData, String messageRefNo, String entity) throws Exception {
		YappayEncryptedRequest request = new YappayEncryptedRequest();
		byte[] sessionKeyByte = this.generateToken();
		request.setToken(this.generateDigitalSignedToken(requestData));
		request.setBody(this.encryptData(requestData, sessionKeyByte, messageRefNo));
		request.setKey(this.encryptKey(sessionKeyByte));
		request.setEntity(this.encryptKey(entity.getBytes()));
		request.setRefNo(messageRefNo);
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(request);

	}
//
	public String decodeResponse(YappayEncryptedResponse response) throws Exception {
		String sessionKey = response.getHeaders().get("key");
		String token = response.getHeaders().get("hash");
		String messageRefNo = response.getHeaders().get("refNo");
		return this.decryptMessage(response.getBody(), sessionKey, token, messageRefNo);
	}
//
	public String decryptMessage(String xmlResponse, String encSessionKey, String token, String messageRefNo)
			throws Exception {

		byte[] sessionKey = this.decryptSessionKey(encSessionKey);
		String data = decryptWithAESKey(xmlResponse, sessionKey, messageRefNo.getBytes());
		return data;

	}
//
	private String decryptWithAESKey(String inputData, byte[] key, byte[] iv) throws Exception {
		Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
		Cipher cipher = Cipher.getInstance(symmetricKeyAlgorithm, pkiProvider);
		SecretKeySpec secKey = new SecretKeySpec(key, symmetricKeyAlgorithm);

		IvParameterSpec spec = new IvParameterSpec(iv);

		cipher.init(Cipher.DECRYPT_MODE, secKey, spec);
		byte[] newData = cipher.doFinal(Base64.decodeBase64(inputData));
		return new String(newData);

	}
//
	private byte[] decryptSessionKey(String sessionKey) throws Exception {

		PrivateKey privateKey = readPrivateKeyFromFile(busPrivateFile);
		Cipher cipher = Cipher.getInstance(asymmetricKeyAlgorithm);

		cipher.init(Cipher.DECRYPT_MODE, privateKey);
		cipher.update(Base64.decodeBase64(sessionKey));
		byte[] sessionKeyBytes = cipher.doFinal();
		return sessionKeyBytes;

	}
//
	public String encryptData(String requestData, byte[] sessionKey, String messageRefNo) throws Exception {

		SecretKey secKey = new SecretKeySpec(sessionKey, "AES");
		Cipher cipher = Cipher.getInstance(symmetricKeyAlgorithm);
		IvParameterSpec ivSpec = new IvParameterSpec(messageRefNo.getBytes());
		cipher.init(Cipher.ENCRYPT_MODE, secKey, ivSpec);
		byte[] newData = cipher.doFinal(requestData.getBytes());

		return Base64.encodeBase64String(newData);
	}
//
	public String encryptKey(byte[] sessionKey) throws Exception {

		PublicKey pubKey = readPublicKeyFromFile(m2pPublicFile);
		Cipher cipher = Cipher.getInstance(asymmetricKeyAlgorithm);

		cipher.init(Cipher.ENCRYPT_MODE, pubKey);
		byte[] cipherData = cipher.doFinal(sessionKey);
		return Base64.encodeBase64String(cipherData);

	}
//
	public byte[] generateToken() throws Exception {
		KeyGenerator generator = KeyGenerator.getInstance("AES");
		generator.init(128);
		SecretKey key = generator.generateKey();
		byte[] symmetricKey = key.getEncoded();
		return symmetricKey;

	}
//
	public String generateDigitalSignedToken(String requestData) throws Exception {
		Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
		Signature signature = Signature.getInstance(digitalSignatureAlgorithm);
		PrivateKey privateKey = this.readPrivateKeyFromFile(busPrivateFile);
		signature.initSign(privateKey, new SecureRandom());
		byte[] message = requestData.getBytes();
		signature.update(message);
		byte[] sigBytes = signature.sign();
		return Base64.encodeBase64String(sigBytes);

	}
//
	private static PrivateKey readPrivateKeyFromFile(String keyFileName) throws Exception {
		File filePrivateKey = new File(keyFileName);
		FileInputStream fis = new FileInputStream(filePrivateKey);
		byte[] encodedPrivateKey = new byte[(int) filePrivateKey.length()];
		fis.read(encodedPrivateKey);
		fis.close();
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(encodedPrivateKey);
		PrivateKey privateKey = keyFactory.generatePrivate(privateKeySpec);
		return privateKey;
	}
//
	private static PublicKey readPublicKeyFromFile(String keyFileName) throws Exception {

		File filePublicKey = new File(keyFileName);
		FileInputStream fis = new FileInputStream(filePublicKey);
		byte[] encodedPublicKey = new byte[(int) filePublicKey.length()];
		fis.read(encodedPublicKey);
		fis.close();

		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(encodedPublicKey);
		PublicKey publicKey = keyFactory.generatePublic(publicKeySpec);
		return publicKey;
	}
	
	/*
	 * 
	 * Local Server 
	 */
	private String m2pPublicFile = "/usr/local/tomcat7/webapps/ROOT/a.der";
	private String busPrivateFile = "/usr/local/tomcat7/webapps/ROOT/private.pkcs8";
	
	/*
	 * 
	 * Production Server
	 * 
	 */
	
	private String symmetricKeyAlgorithm = "AES/CBC/PKCS5Padding";
	private String asymmetricKeyAlgorithm = "RSA/ECB/PKCS1Padding";
	private String digitalSignatureAlgorithm = "SHA1withRSA";
	private String pkiProvider = "BC";
	public static void call() {
	}
}