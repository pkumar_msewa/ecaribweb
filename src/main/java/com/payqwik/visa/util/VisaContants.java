package com.payqwik.visa.util;

public class VisaContants {

	public static final String SIGN_UP_URL="http://54.169.216.42:8080/merchant/signup";
	public static final String GET_TRANSACTION_LIST_URL="https://merc.yappay.in/Yappay/txn-manager/fetch/success/entity/";
	public static final String GET_STATIC_MERCHANT_QR_CODE_URL="http://54.169.216.42:8080/merchant/v2/generateQrCodes/";
	public static final String GET_DYNAMIC_MERCHANT_QR_CODE_URL="";
	public static final String GET_MERCHANT_BALANCE="";
	public static final String SIGN_UP_LIVE = "https://merc.yappay.in/merchant/v2/signup";
	public static final String GET_LIVE_QR = "https://merc.yappay.in/merchant/qrcode/";
	public static final String FETCH_BALANCE = "https://merc.yappay.in/Yappay/business-entity-manager/fetchbalance/";
}
