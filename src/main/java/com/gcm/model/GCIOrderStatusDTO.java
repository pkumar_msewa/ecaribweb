package com.gcm.model;

/**
 * @author Kamal
 *
 */
public class GCIOrderStatusDTO {
	
	private String token;
	private String receiptNo;
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getReceiptNo() {
		return receiptNo;
	}
	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}
	
	

}
