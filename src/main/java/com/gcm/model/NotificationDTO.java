package com.gcm.model;

import java.util.List;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.thirdparty.model.JSONWrapper;

public class NotificationDTO implements JSONWrapper{
    private List<String> regsitrationIds;
    private String title;
    private String message;
    private String image;
    private boolean imageGCM;
    private String sessionId;
    private List<GCMDto> gcmIds;
    private String status;
    private Long deliveredCount;
    private String createdDate;

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getDeliveredCount() {
		return deliveredCount;
	}

	public void setDeliveredCount(Long deliveredCount) {
		this.deliveredCount = deliveredCount;
	}

	public List<GCMDto> getGcmIds() {
		return gcmIds;
	}

	public void setGcmIds(List<GCMDto> gcmIds) {
		this.gcmIds = gcmIds;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public boolean isImageGCM() {
        return imageGCM;
    }

    public void setImageGCM(boolean imageGCM) {
        this.imageGCM = imageGCM;
    }

    public List<String> getRegsitrationIds() {
        return regsitrationIds;
    }

    public void setRegsitrationIds(List<String> regsitrationIds) {
        this.regsitrationIds = regsitrationIds;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public JSONObject toJSON() {
        JSONObject json = new JSONObject();
        JSONArray ids = new JSONArray();
        JSONObject data = new JSONObject();
        
        for(String l : getRegsitrationIds()){
        	ids.put(l);
        }
//        ids.put("dGjIfLRJKSk:APA91bHnVXZrOFpT8VPDKIgFpeYPQS6QRjou6pPGoQ1QNshiZr9Q_XM6d3mbABXyyBmOKgenyFjVV4YeZvwZ6J4gvUayDGNLgwNuJ20R4NWJZoKLwEESPvDe3DPvtQ09F5u-f-3ZeTDG");
        try {
            json.put("registration_ids", ids);
            data.put("title",getTitle());
            data.put("message",getMessage());
            if(isImageGCM()) {
                data.put("image", getImage());
            }
            json.put("data",data);
        }catch(JSONException e){
            e.printStackTrace();
        }
        return json;
    }


}
