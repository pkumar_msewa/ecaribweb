package com.gcm.model;

public class GCMDto {

	private String gcmId;

	public String getGcmId() {
		return gcmId;
	}

	public void setGcmId(String gcmId) {
		this.gcmId = gcmId;
	}
	
}
