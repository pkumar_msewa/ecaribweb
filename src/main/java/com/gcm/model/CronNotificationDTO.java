package com.gcm.model;

import java.util.List;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.thirdparty.model.JSONWrapper;

public class CronNotificationDTO implements JSONWrapper{
    private List<String> regsitrationIds;
    private String title;
    private String message;
    private String image;
    private boolean imageGCM;
    private String sessionId;
    private List<GCMDto> gcmIds;
    

	public List<GCMDto> getGcmIds() {
		return gcmIds;
	}

	public void setGcmIds(List<GCMDto> gcmIds) {
		this.gcmIds = gcmIds;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public boolean isImageGCM() {
        return imageGCM;
    }

    public void setImageGCM(boolean imageGCM) {
        this.imageGCM = imageGCM;
    }

    public List<String> getRegsitrationIds() {
        return regsitrationIds;
    }

    public void setRegsitrationIds(List<String> regsitrationIds) {
        this.regsitrationIds = regsitrationIds;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public JSONObject toJSON() {
        JSONObject json = new JSONObject();
        JSONArray ids = new JSONArray();
        JSONObject data = new JSONObject();
        for(GCMDto l : getGcmIds()){
            ids.put(l.getGcmId());
        }
        try {
            json.put("registration_ids", ids);
            data.put("title",getTitle());
            data.put("message",getMessage());
            if(isImageGCM()) {
                data.put("image", getImage());
            }
            json.put("data",data);
        }catch(JSONException e){
            e.printStackTrace();
        }
        return json;
    }


}

