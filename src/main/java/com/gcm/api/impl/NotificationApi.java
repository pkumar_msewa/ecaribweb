package com.gcm.api.impl;

import org.codehaus.jettison.json.JSONObject;

import com.gcm.api.INotificationApi;
import com.gcm.model.NotificationDTO;
import com.gcm.utils.APIConstants;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.thirdparty.model.ResponseDTO;

public class NotificationApi implements INotificationApi {
    @Override
    public ResponseDTO sendNotification(NotificationDTO dto) {
    ResponseDTO resp = new ResponseDTO();
		JSONObject json = null;
        try {
			Client client = Client.create();
		WebResource webResource = client.resource(APIConstants.SEND_NOTIFICATION);
            System.err.print(dto.toJSON());
			ClientResponse response = webResource.accept("application/json").type("application/json").header("Authorization","key="+APIConstants.APP_KEY).post(ClientResponse.class, dto.toJSON());
		System.err.println("response ::" + response);
		 String strResponse = response.getEntity(String.class);
		 System.err.println("response of gcm ::" + strResponse);
		org.json.JSONObject jobj=new org.json.JSONObject(strResponse);
		if(jobj!=null){
			if(jobj.getInt("success")==1){
				resp.setCode("S00");
				resp.setValid(true);
				System.err.println("sucess gcm");
			}
			else{
				resp.setCode("F00");
				resp.setValid(false);
				System.err.println("failed gcm");
			}
		}
        } catch (Exception e) {
			e.printStackTrace();
		}
        return resp;
    }
}
