package com.google.recaptcha.api.impl;

import com.google.recaptcha.api.IVerificationApi;
import com.google.recaptcha.model.JCaptchaRequest;
import com.google.recaptcha.model.JCaptchaResponse;
import com.octo.captcha.service.CaptchaServiceException;
import com.octo.captcha.service.multitype.MultiTypeCaptchaService;

public class VerificationApiImpl implements IVerificationApi{

	private MultiTypeCaptchaService captchaService;
    public VerificationApiImpl(MultiTypeCaptchaService captchaService){
        this.captchaService = captchaService;
    }
    
    @Override
    public JCaptchaResponse isValidJCaptcha(JCaptchaRequest request) {
        boolean validCaptcha = false;
        JCaptchaResponse response  = new JCaptchaResponse();
        try {
            validCaptcha = captchaService.validateResponseForID(request.getSessionId(), request.getCaptchaResponse());
        }
        catch (CaptchaServiceException e) {
            e.printStackTrace();
        }
        response.setValid(validCaptcha);
        return response;
    }
}
