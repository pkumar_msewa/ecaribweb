package com.instantpay.controller;

import com.instantpay.model.DueAmount;
import com.instantpay.model.request.AmountRequest;
import com.instantpay.model.request.ValidationRequest;
import com.instantpay.util.IPayConvertUtil;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.instantpay.api.IValidationApi;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RequestMapping("/InstantPay")
public class ValidationController {

    private final IValidationApi validationApi;
    public ValidationController(IValidationApi validationApi){
        this.validationApi = validationApi;
    }

	@RequestMapping(value = "/GetAmount", method = RequestMethod.POST)
	ResponseEntity<String> getExactAmount(@ModelAttribute AmountRequest dto, HttpServletRequest request,
			HttpServletResponse response) {
		String strResponse = "";
		ValidationRequest ndto = null;
		String serviceProvider = dto.getServiceProvider().substring(1);
		dto.setServiceProvider(serviceProvider);
		ndto = IPayConvertUtil.convertGenericBBPSElectricityRequest(dto);
		if (ndto != null) {
			strResponse = validationApi.getAmount(ndto);
		}

		return new ResponseEntity<String>(strResponse, HttpStatus.OK);
	}
 
 
	@RequestMapping(value = "/GetDueAmount", method = RequestMethod.POST)
	ResponseEntity<DueAmount> getExactDueAmount(@ModelAttribute AmountRequest dto, HttpServletRequest request,
			HttpServletResponse response) {
		DueAmount amountResponse = null;
		String serviceProvider = dto.getServiceProvider().substring(1);
		dto.setServiceProvider(serviceProvider);
		ValidationRequest ndto = IPayConvertUtil.convertGenericBBPSElectricityRequest(dto);
		if (ndto != null) {
			amountResponse = validationApi.getDueAmount(ndto);
		}
		return new ResponseEntity<DueAmount>(amountResponse, HttpStatus.OK);
	}

}
