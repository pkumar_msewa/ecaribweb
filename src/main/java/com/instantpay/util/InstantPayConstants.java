package com.instantpay.util;

import com.payqwikweb.app.metadatas.UrlMetadatas;

public class InstantPayConstants {

    private static final String URL = "https://www.instantpay.in/ws/api/";

//	private static final String URL = "http://180.179.213.127/ws/api/";
//    private static final String URL = "https://www.vpayqwik.com/InstantPay/";

    public static final String URL_TRANSACTION = URL + "transaction";
    public static final String URL_PLANS = URL + "plans";

    public static final String URL_BALANCE = URL + "checkwallet";
    public static final String URL_STATUS = URL + "getMIS";
    public static final String URL_SERVICE = URL + "serviceproviders";
      public static final String TEST_TOKEN = "936267b2a4cc1fef34d7396376ca21c0"; // 172.16.3.10 - VIJAYA BANK
//	  public static final String TEST_TOKEN = "7bb88a89aa3682398ed60baeb54ab21b"; // 172.16.7.29 - VIJAYA BANK WEB
//	  public static final String TOKEN = "936267b2a4cc1fef34d7396376ca21c0"; // 66.207.206.54 - MSEWA
	  public static final String LIVE_TOKEN = "7563bd75da0a1d9b38676c81bdd76963"; //// 210.212.204.39 - Vijaya Bank
	 
	 public static final String TOKEN = "7bb88a89aa3682398ed60baeb54ab21b"; //// 49.204.86.246 - Msewa Office
    public static final String AGENT_ID = "565Y10837"; // MSEWA Dealer code
    public static final String OUTLET_ID = "10019339"; // MSEWA Outlet ID for 66.207.206.54
    public static final String REQUEST_FORMAT = "json";
    
    public static final String API_KEY_TOKEN = "token";
	public static final String API_KEY_MODE = "mode";
	public static final String API_KEY_SPKEY = "spkey";
	public static final String API_KEY_AGENTID = "agentid";
	public static final String API_KEY_ACCOUNT = "account";
	public static final String API_KEY_AMOUNT = "amount";
	public static final String API_KEY_OPTIONAL1 = "optional1";
	public static final String API_KEY_OPTIONAL2 = "optional2";
	public static final String API_KEY_OPTIONAL3 = "optional3";
	public static final String API_KEY_OPTIONAL4 = "optional4";
	public static final String API_KEY_OPTIONAL5 = "optional5";
	public static final String API_KEY_OPTIONAL6 = "optional6";
	public static final String API_KEY_OPTIONAL7 = "optional7";
	public static final String API_KEY_OPTIONAL8 = "optional8";
	public static final String API_KEY_OPTIONAL9 = "optional9";
	public static final String API_KEY_BBPS_OUTLET_ID = "outletid";
	public static final String API_KEY_PAYMENT_CHANNEL= "paymentchannel";
	public static final String API_KEY_PAYMENT_MODE = "paymentmode";
	public static final String API_KEY_CUNSUMER_MOBILE = "customermobile";
	public static final String API_KEY_FORMAT = "format";
	public static final String API_KEY_TYPE = "type";
	public static final String USERNAME = "instantpay@payqwik.in";
	public static final String BBPS_OUTLET_ID = "751";
	public static final String PAYMENT_CHANNEL = "AGT";
	public static final String PAYMENT_MODE = "CASH";
	public static final String optional8 = "Remarks";
	public static final String optional9 = "12.9369,77.6407|560095";
	public static final String CUNSUMER_MOBILE = "7022620747";
	
	 public static String getToken(){
	    	if(UrlMetadatas.PRODUCTION){
	    		return LIVE_TOKEN;
	    	}else{
	    		return TEST_TOKEN;
	    	}
	    }
	
	
	



}
