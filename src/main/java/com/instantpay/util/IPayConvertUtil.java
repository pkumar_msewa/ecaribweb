package com.instantpay.util;


import com.instantpay.model.request.AmountRequest;
import com.instantpay.model.request.Format;
import com.instantpay.model.request.Mode;
import com.instantpay.model.request.ValidationRequest;
import com.payqwikweb.app.model.request.BillPaymentCommonDTO;

public class IPayConvertUtil {
	
	public static ValidationRequest convertDTHBillPaymentRequest(BillPaymentCommonDTO request) {
		ValidationRequest response = new ValidationRequest();
		response.setAmount(request.getAmount());
		response.setMode(Mode.VALIDATE);
		response.setAccount(request.getDthNo());
		response.setAgentId(System.currentTimeMillis() + "");
		response.setFormat(Format.JSON);
		response.setSpKey(convertServiceProvider(request.getServiceProvider()));
		response.setToken(InstantPayConstants.getToken());
		return response;
	}

	public static ValidationRequest convertLandlineBillPaymentRequest(BillPaymentCommonDTO request) {
		ValidationRequest response = new ValidationRequest();
		response.setAmount(request.getAmount());
		response.setMode(Mode.VALIDATE);
		response.setAccount(request.getLandlineNumber());
		response.setOptional1(request.getStdCode());
		if (request.getServiceProvider().equalsIgnoreCase("VBGL")) {
			response.setOptional2(request.getAccountNumber());
			response.setOptional3("LLI");
		}
		response.setAgentId(System.currentTimeMillis() + "");
		response.setFormat(Format.JSON);
		response.setSpKey(convertServiceProvider(request.getServiceProvider()));
		response.setToken(InstantPayConstants.getToken());
		return response;
	}

	public static ValidationRequest convertElectricityBillPaymentRequest(BillPaymentCommonDTO request) {
		ValidationRequest response = new ValidationRequest();
		if(!request.getServiceProvider().equalsIgnoreCase("VMME")
				&& !request.getServiceProvider().equalsIgnoreCase("VMDE")
    			&& !request.getServiceProvider().equalsIgnoreCase("VREE") 
    			&& !request.getServiceProvider().equalsIgnoreCase("VTPE")){
    		response.setOutletId(InstantPayConstants.BBPS_OUTLET_ID);
    		response.setOptional8(InstantPayConstants.optional8);
    		response.setOptional9(InstantPayConstants.optional9);
    		response.setCustomerMobile(InstantPayConstants.CUNSUMER_MOBILE);
    		response.setPaymentMode(InstantPayConstants.PAYMENT_MODE);
    		response.setPaymentChannel(InstantPayConstants.PAYMENT_CHANNEL);
    	}
		response.setAccount(request.getAccountNumber());
		response.setAmount(request.getAmount());
		response.setMode(Mode.VALIDATE);
		response.setSpKey(convertServiceProvider(request.getServiceProvider()));
		if (request.getServiceProvider().equalsIgnoreCase("VREE")) {
			response.setOptional1(request.getCycleNumber());
		}else if(request.getServiceProvider().equalsIgnoreCase("VTPE")){
			response.setOptional1(request.getCityName());
		}else if(request.getServiceProvider().equalsIgnoreCase("VMDE")){
			response.setOptional1(request.getBillingUnit());
		}
		response.setToken(InstantPayConstants.getToken());
		response.setAgentId(System.currentTimeMillis() + "");
		response.setFormat(Format.JSON);
		return response;
	}

	public static ValidationRequest convertGasBillPaymentRequest(BillPaymentCommonDTO request) {
		ValidationRequest response = new ValidationRequest();
		if(!request.getServiceProvider().equalsIgnoreCase("VMMG")){
    		response.setOutletId(InstantPayConstants.BBPS_OUTLET_ID);
    		response.setOptional8(InstantPayConstants.optional8);
    		response.setOptional9(InstantPayConstants.optional9);
    		response.setCustomerMobile(InstantPayConstants.CUNSUMER_MOBILE);
    		response.setPaymentMode(InstantPayConstants.PAYMENT_MODE);
    		response.setPaymentChannel(InstantPayConstants.PAYMENT_CHANNEL);
    	}
		response.setAccount(request.getAccountNumber());
		response.setAmount(request.getAmount());
		response.setSpKey(convertServiceProvider(request.getServiceProvider()));
		response.setToken(InstantPayConstants.getToken());
		response.setAgentId(System.currentTimeMillis() + "");
		response.setFormat(Format.JSON);
		response.setMode(Mode.VALIDATE);
		return response;
	}

	public static ValidationRequest convertInsuranceBillPaymentRequest(BillPaymentCommonDTO request) {
		ValidationRequest response = new ValidationRequest();
		response.setAccount(request.getPolicyNumber());
		response.setAmount(request.getAmount());
		response.setSpKey(convertServiceProvider(request.getServiceProvider()));
		response.setOptional1(request.getPolicyDate());
		response.setToken(InstantPayConstants.getToken());
		response.setAgentId(System.currentTimeMillis() + "");
		response.setFormat(Format.JSON);
		return response;
	}

	public static String convertServiceProvider(String serviceProvider) {
		return serviceProvider.substring(1);
	}

    public static ValidationRequest convertDTHRequest(AmountRequest dto){
        ValidationRequest ndto = new ValidationRequest();
        ndto.setSpKey(dto.getServiceProvider());
        ndto.setAccount(dto.getCustomerID());
        ndto.setAgentId(InstantPayConstants.AGENT_ID);
        ndto.setAmount(dto.getAmount());
        ndto.setFormat(Format.JSON);
        ndto.setMode(Mode.VALIDATE);
        ndto.setToken(InstantPayConstants.getToken());
        return ndto;
    }

    public static ValidationRequest convertAirtelLandlineRequest(AmountRequest dto){
        ValidationRequest ndto = new ValidationRequest();
        ndto.setSpKey(dto.getServiceProvider());
        ndto.setAccount(dto.getLandlineNumber());
        ndto.setOptional1(dto.getStdCode());
        ndto.setAgentId(InstantPayConstants.AGENT_ID);
        ndto.setAmount("20");
        ndto.setFormat(Format.JSON);
        ndto.setMode(Mode.VALIDATE);
        ndto.setToken(InstantPayConstants.getToken());
        return ndto;
    }

    public static ValidationRequest convertBSNLLandlineRequest(AmountRequest dto){
        ValidationRequest ndto = new ValidationRequest();
        ndto.setSpKey(dto.getServiceProvider());
        ndto.setAccount(dto.getLandlineNumber());
        ndto.setOptional1(dto.getStdCode());
        ndto.setOptional2(dto.getAccountNumber());
        ndto.setOptional3("LLI");
        ndto.setAgentId(InstantPayConstants.AGENT_ID);
        ndto.setAmount("50");
        ndto.setFormat(Format.JSON);
        ndto.setMode(Mode.VALIDATE);
        ndto.setToken(InstantPayConstants.getToken());
        return ndto;
    }
    public static ValidationRequest convertMTNLLandlineRequest(AmountRequest dto){
        ValidationRequest ndto = new ValidationRequest();
        ndto.setSpKey(dto.getServiceProvider());
        ndto.setAccount(dto.getLandlineNumber());
        ndto.setOptional1(dto.getCustomerID());
        ndto.setAgentId(InstantPayConstants.AGENT_ID);
        ndto.setAmount("50");
        ndto.setFormat(Format.JSON);
        ndto.setMode(Mode.VALIDATE);
        ndto.setToken(InstantPayConstants.getToken());
        return ndto;
    }

    public static ValidationRequest convertRelianceLandlineRequest(AmountRequest dto){
        ValidationRequest ndto = new ValidationRequest();
        ndto.setSpKey(dto.getServiceProvider());
        ndto.setAccount(dto.getLandlineNumber());
        ndto.setOptional1(dto.getStdCode());
        ndto.setAgentId(InstantPayConstants.AGENT_ID);
        ndto.setAmount("50");
        ndto.setFormat(Format.JSON);
        ndto.setMode(Mode.VALIDATE);
        ndto.setToken(InstantPayConstants.getToken());
        return ndto;
    }

    public static ValidationRequest convertTataDocomoLandlineRequest(AmountRequest dto){
        ValidationRequest ndto = new ValidationRequest();
        ndto.setSpKey(dto.getServiceProvider());
        ndto.setAccount(dto.getLandlineNumber());
        ndto.setOptional1(dto.getStdCode());
        ndto.setAgentId(InstantPayConstants.AGENT_ID);
        ndto.setAmount("40");
        ndto.setFormat(Format.JSON);
        ndto.setMode(Mode.VALIDATE);
        ndto.setToken(InstantPayConstants.getToken());
        return ndto;
    }

    public static ValidationRequest convertGenericElectricityRequest(AmountRequest dto){
        ValidationRequest ndto = new ValidationRequest();
        ndto.setSpKey(dto.getServiceProvider());
        ndto.setAccount(dto.getCustomerID());
        ndto.setAgentId(InstantPayConstants.AGENT_ID);
        ndto.setAmount("5");
        ndto.setFormat(Format.JSON);
        ndto.setMode(Mode.VALIDATE);
        ndto.setToken(InstantPayConstants.getToken());
        return ndto;
    }

    public static ValidationRequest convertMDEElectricityRequest(AmountRequest dto){
        ValidationRequest ndto = new ValidationRequest();
        ndto.setSpKey(dto.getServiceProvider());
        ndto.setAccount(dto.getCustomerID());
        ndto.setOptional1(dto.getBillingUnit());
        ndto.setOptional2(dto.getProcessingCycle());
        ndto.setAgentId(InstantPayConstants.AGENT_ID);
        ndto.setAmount("5");
        ndto.setFormat(Format.JSON);
        ndto.setMode(Mode.VALIDATE);
        ndto.setToken(InstantPayConstants.getToken());
        return ndto;
    }

    public static ValidationRequest convertREEElectricityRequest(AmountRequest dto){
        ValidationRequest ndto = new ValidationRequest();
        ndto.setSpKey(dto.getServiceProvider());
        ndto.setAccount(dto.getCustomerID());
        ndto.setOptional1(dto.getCycleNumber());
        ndto.setAgentId(InstantPayConstants.AGENT_ID);
        ndto.setAmount("5");
        ndto.setFormat(Format.JSON);
        ndto.setMode(Mode.VALIDATE);
        ndto.setToken(InstantPayConstants.getToken());
        return ndto;
    }

    public static ValidationRequest convertTPEElectricityRequest(AmountRequest dto){
        ValidationRequest ndto = new ValidationRequest();
        ndto.setSpKey(dto.getServiceProvider());
        ndto.setAccount(dto.getCustomerID());
        ndto.setOptional1(dto.getCityName());
        ndto.setAgentId(InstantPayConstants.AGENT_ID);
        ndto.setAmount("5");
        ndto.setFormat(Format.JSON);
        ndto.setMode(Mode.VALIDATE);
        ndto.setToken(InstantPayConstants.getToken());
        return ndto;
    }

    public static ValidationRequest convertGenericGasRequest(AmountRequest dto){
        ValidationRequest ndto = new ValidationRequest();
        ndto.setSpKey(dto.getServiceProvider());
        ndto.setAccount(dto.getCustomerID());
        ndto.setAgentId(InstantPayConstants.AGENT_ID);
        ndto.setAmount("5");
        ndto.setFormat(Format.JSON);
        ndto.setMode(Mode.VALIDATE);
        ndto.setToken(InstantPayConstants.getToken());
        return ndto;
    }

    public static ValidationRequest convertMMGGasRequest(AmountRequest dto){
        ValidationRequest ndto = new ValidationRequest();
        ndto.setSpKey(dto.getServiceProvider());
        ndto.setAccount(dto.getCustomerID());
        ndto.setOptional1(dto.getBillGroupNumber());
        ndto.setAgentId(InstantPayConstants.AGENT_ID);
        ndto.setAmount("5");
        ndto.setFormat(Format.JSON);
        ndto.setMode(Mode.VALIDATE);
        ndto.setToken(InstantPayConstants.getToken());
        return ndto;
    }

    public static ValidationRequest convertInsuranceRequest(AmountRequest dto){
        ValidationRequest ndto = new ValidationRequest();
        ndto.setSpKey(dto.getServiceProvider());
        ndto.setAccount(dto.getCustomerID());
        ndto.setOptional1(dto.getDateOfBirth());
        ndto.setAgentId(InstantPayConstants.AGENT_ID);
        ndto.setAmount(dto.getAmount());
        ndto.setFormat(Format.JSON);
        ndto.setMode(Mode.VALIDATE);
        ndto.setToken(InstantPayConstants.getToken());
        return ndto;
    }
    
    public static ValidationRequest convertGenericBBPSElectricityRequest(AmountRequest dto){
    	ValidationRequest ndto = new ValidationRequest();
    	if(!dto.getServiceProvider().equalsIgnoreCase("MME") && !dto.getServiceProvider().equalsIgnoreCase("MDE")
    			&& !dto.getServiceProvider().equalsIgnoreCase("REE") && !dto.getServiceProvider().equalsIgnoreCase("TPE")){
    		ndto.setOutletId(InstantPayConstants.BBPS_OUTLET_ID);
    		ndto.setOptional8(InstantPayConstants.optional8);
    		ndto.setOptional9(InstantPayConstants.optional9);
    		ndto.setCustomerMobile(InstantPayConstants.CUNSUMER_MOBILE);
    		ndto.setPaymentMode(InstantPayConstants.PAYMENT_MODE);
    		ndto.setPaymentChannel(InstantPayConstants.PAYMENT_CHANNEL);
    	}
        ndto.setSpKey(dto.getServiceProvider());
        ndto.setAccount(dto.getCustomerID());
        ndto.setAgentId(InstantPayConstants.AGENT_ID);
        ndto.setAmount("5");
        ndto.setFormat(Format.JSON);
        ndto.setMode(Mode.VALIDATE);
        ndto.setToken(InstantPayConstants.getToken());
        return ndto;
    }

}
