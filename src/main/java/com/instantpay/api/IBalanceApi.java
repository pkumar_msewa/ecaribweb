package com.instantpay.api;

import com.instantpay.model.response.BalanceResponse;

public interface IBalanceApi {

	BalanceResponse request();
}
