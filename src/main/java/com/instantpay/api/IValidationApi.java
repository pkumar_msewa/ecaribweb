package com.instantpay.api;

import com.instantpay.model.DueAmount;
import com.instantpay.model.request.ValidationRequest;
import com.thirdparty.model.ValidationResponseDTO;

public interface IValidationApi {
     String getAmount(ValidationRequest dto);
     ValidationResponseDTO validateTransaction(ValidationRequest dto);
	 DueAmount getDueAmount(ValidationRequest dto);
}
