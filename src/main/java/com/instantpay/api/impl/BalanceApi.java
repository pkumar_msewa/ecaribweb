package com.instantpay.api.impl;

import org.json.JSONObject;

import com.instantpay.api.IBalanceApi;
import com.instantpay.model.Balance;
import com.instantpay.model.request.Format;
import com.instantpay.model.response.BalanceResponse;
import com.instantpay.util.InstantPayConstants;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class BalanceApi implements IBalanceApi {

	
	@Override
	public BalanceResponse request() {
		BalanceResponse response = new BalanceResponse();
		try {
			String stringResponse = "";
			WebResource resource = Client.create().resource(InstantPayConstants.URL_BALANCE)
					.queryParam(InstantPayConstants.API_KEY_TOKEN, InstantPayConstants.getToken())
					.queryParam(InstantPayConstants.API_KEY_FORMAT, Format.JSON.getFormat());
			ClientResponse clientResponse = resource.get(ClientResponse.class);
				stringResponse = clientResponse.getEntity(String.class);
			if (clientResponse.getStatus() == 200) {
				Balance balanceResponse = new Balance();
				JSONObject o = new JSONObject(stringResponse);
				String wallet = "Not able to fetch";
				if(o.has("Wallet")) {
				wallet = o.getString("Wallet");
				}
				if (wallet != null) {
					response.setSuccess(true);
					balanceResponse.setWallet(wallet);
				}
				response.setBalance(balanceResponse);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

}
