package com.instantpay.api.impl;

import com.instantpay.api.IPlansApi;
import com.instantpay.model.request.Format;
import com.instantpay.model.request.PlansRequest;
import com.instantpay.util.InstantPayConstants;
import com.payqwikweb.util.JSONParserUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import org.json.JSONObject;

public class PlansApi implements IPlansApi{

    @Override
    public String getPlansApi(PlansRequest dto) {
        String stringResponse = "";
        try {
            WebResource resource = Client.create().resource(InstantPayConstants.URL_PLANS)
                    .queryParam("token", InstantPayConstants.getToken())
                    .queryParam("spkey", dto.getSpKey())
                    .queryParam("circle",dto.getCircle())
                    .queryParam("format", Format.JSON.getFormat());
            ClientResponse clientResponse = resource.get(ClientResponse.class);
            stringResponse = clientResponse.getEntity(String.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return stringResponse;
    }
}
