package com.instantpay.api.impl;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.instantpay.api.IValidationApi;
import com.instantpay.model.DueAmount;
import com.instantpay.model.request.ValidationRequest;
import com.instantpay.util.InstantPayConstants;
import com.payqwikweb.util.JSONParserUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.thirdparty.model.ValidationResponseDTO;

public class ValidationApi implements IValidationApi {
	
	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public String getAmount(ValidationRequest dto) {
        String stringResponse = "";
        try {
        	Client client=Client.create();
        	client.addFilter(new LoggingFilter(System.out));
            WebResource resource = client.resource(InstantPayConstants.URL_TRANSACTION)
                    .queryParam("token", dto.getToken())
                    .queryParam("mode", String.valueOf(dto.getMode()))
                    .queryParam("spkey", dto.getSpKey())
                    .queryParam("agentid", dto.getAgentId())
                    .queryParam("account", dto.getAccount())
                    .queryParam("amount", dto.getAmount())
                    .queryParam("optional1", (dto.getOptional1() == null)?"":dto.getOptional1())
                    .queryParam("optional2", (dto.getOptional2() == null)?"":dto.getOptional2())
                    .queryParam("optional3", (dto.getOptional3() == null)?"":dto.getOptional3())
                    .queryParam("optional4", (dto.getOptional4() == null)?"":dto.getOptional4())
                    .queryParam("optional5", (dto.getOptional5() == null)?"":dto.getOptional5())
                    .queryParam("optional6", (dto.getOptional6() == null)?"":dto.getOptional6())
                    .queryParam("optional7", (dto.getOptional7() == null)?"":dto.getOptional7())
                    .queryParam("optional8", (dto.getOptional8() == null)?"":dto.getOptional8())
                    .queryParam("optional9", (dto.getOptional9() == null)?"":dto.getOptional9())
                    .queryParam("outletid", (dto.getOutletId() == null)?"":dto.getOutletId())
                    .queryParam("paymentchannel", (dto.getPaymentChannel() == null)?"":dto.getPaymentChannel())
                    .queryParam("paymentmode", (dto.getPaymentMode() == null) ?"":dto.getPaymentMode())
                    .queryParam("format", dto.getFormat().getFormat())
                    .queryParam("customermobile", (dto.getCustomerMobile() == null) ?"":dto.getCustomerMobile());
            ClientResponse clientResponse = resource.get(ClientResponse.class);
            if (clientResponse.getStatus() == 200) {
              String  strResponse = clientResponse.getEntity(String.class);
                JSONObject o = new JSONObject(strResponse);
                String ipay_errorcode = JSONParserUtil.getString(o, "ipay_errorcode");
                if(ipay_errorcode != null) {
                  stringResponse=strResponse;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return stringResponse;
    }
    
    
    @Override
    public DueAmount getDueAmount(ValidationRequest dto) {
    	DueAmount amountResponse = new DueAmount();
        try {
        	Client client=Client.create();
        	client.addFilter(new LoggingFilter(System.out));
            WebResource resource = client.resource(InstantPayConstants.URL_TRANSACTION)
                    .queryParam("token", dto.getToken())
                    .queryParam("mode", String.valueOf(dto.getMode()))
                    .queryParam("spkey", dto.getSpKey())
                    .queryParam("agentid", dto.getAgentId())
                    .queryParam("account", dto.getAccount())
                    .queryParam("amount", dto.getAmount())
                    .queryParam("optional1", (dto.getOptional1() == null)?"":dto.getOptional1())
                    .queryParam("optional2", (dto.getOptional2() == null)?"":dto.getOptional2())
                    .queryParam("optional3", (dto.getOptional3() == null)?"":dto.getOptional3())
                    .queryParam("optional4", (dto.getOptional4() == null)?"":dto.getOptional4())
                    .queryParam("optional5", (dto.getOptional5() == null)?"":dto.getOptional5())
                    .queryParam("optional6", (dto.getOptional6() == null)?"":dto.getOptional6())
                    .queryParam("optional7", (dto.getOptional7() == null)?"":dto.getOptional7())
                    .queryParam("optional8", (dto.getOptional8() == null)?"":dto.getOptional8())
                    .queryParam("optional9", (dto.getOptional9() == null)?"":dto.getOptional9())
                    .queryParam("outletid", (dto.getOutletId() == null)?"":dto.getOutletId())
                    .queryParam("paymentchannel", (dto.getPaymentChannel() == null)?"":dto.getPaymentChannel())
                    .queryParam("paymentmode", (dto.getPaymentMode() == null)?"":dto.getPaymentMode())
                    .queryParam("format", dto.getFormat().getFormat())
                    .queryParam("customermobile", (dto.getCustomerMobile() == null)?"":dto.getCustomerMobile());
            ClientResponse clientResponse = resource.get(ClientResponse.class);
            if (clientResponse.getStatus() == 200) {
               String stringResponse = clientResponse.getEntity(String.class);
                JSONObject o = new JSONObject(stringResponse);
                String ipay_errorcode = JSONParserUtil.getString(o, "ipay_errorcode");
                if(ipay_errorcode != null) {
                    if(ipay_errorcode.equalsIgnoreCase("IRA")) {
                        JSONObject particulars = JSONParserUtil.getObject(o,"particulars");
                        if(particulars != null){
                            amountResponse.setDueAmount(JSONParserUtil.getString(particulars,"dueamount"));
                            amountResponse.setDueDate(JSONParserUtil.getString(particulars,"duedate"));
                            amountResponse.setCustomerName(JSONParserUtil.getString(particulars,"customername"));
                            amountResponse.setBillNumber(JSONParserUtil.getString(particulars,"billnumber"));
                            amountResponse.setBillDate(JSONParserUtil.getString(particulars,"billdate"));
                            amountResponse.setBillPeriod(JSONParserUtil.getString(particulars,"billperiod"));
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return amountResponse;
    }

    @Override
    public ValidationResponseDTO validateTransaction(ValidationRequest dto) {
    	ValidationResponseDTO result = new ValidationResponseDTO();
        String stringResponse = "";
        try {
        	Client client = Client.create();
        	client.addFilter(new LoggingFilter(System.out));
            WebResource resource = client.resource(InstantPayConstants.URL_TRANSACTION)
            	      .queryParam("token", dto.getToken())
                      .queryParam("mode", String.valueOf(dto.getMode()))
                      .queryParam("spkey", dto.getSpKey())
                      .queryParam("agentid", dto.getAgentId())
                      .queryParam("account", dto.getAccount())
                      .queryParam("amount", dto.getAmount())
                      .queryParam("optional1", (dto.getOptional1() == null)?"":dto.getOptional1())
                      .queryParam("optional2", (dto.getOptional2() == null)?"":dto.getOptional2())
                      .queryParam("optional3", (dto.getOptional3() == null)?"":dto.getOptional3())
                      .queryParam("optional4", (dto.getOptional4() == null)?"":dto.getOptional4())
                      .queryParam("optional5", (dto.getOptional5() == null)?"":dto.getOptional5())
                      .queryParam("optional6", (dto.getOptional6() == null)?"":dto.getOptional6())
                      .queryParam("optional7", (dto.getOptional7() == null)?"":dto.getOptional7())
                      .queryParam("optional8", (dto.getOptional8() == null)?"":dto.getOptional8())
                      .queryParam("optional9", (dto.getOptional9() == null)?"":dto.getOptional9())
                      .queryParam("outletid", (dto.getOutletId() == null)?"":dto.getOutletId())
                      .queryParam("paymentchannel", (dto.getPaymentChannel() == null)?"":dto.getPaymentChannel())
                      .queryParam("paymentmode", (dto.getPaymentMode() == null)?"":dto.getPaymentMode())
                      .queryParam("format", dto.getFormat().getFormat())
                      .queryParam("customermobile", (dto.getCustomerMobile() == null)?"":dto.getCustomerMobile());
            ClientResponse clientResponse = resource.get(ClientResponse.class);
            if (clientResponse.getStatus() == 200) {
                stringResponse = clientResponse.getEntity(String.class);
                JSONObject o = new JSONObject(stringResponse);
                logger.info("stringResponse ::" + stringResponse);
                String ipay_errorcode = JSONParserUtil.getString(o, "ipay_errorcode");
                if(ipay_errorcode != null) {
                    String ipay_errordesc = JSONParserUtil.getString(o, "ipay_errordesc");
                    if(ipay_errorcode.equalsIgnoreCase("TXN")) {
                    	logger.info("inside TXN code .");
                          result.setSuccess(true);
                          result.setMessage(ipay_errordesc);
                          result.setCode(ipay_errorcode);
                    }else if(ipay_errorcode.equalsIgnoreCase("IRA")){
                    	result.setSuccess(false);
                     	if(o.has("particulars")) {
                        JSONObject particulars = JSONParserUtil.getObject(o, "particulars");
                        String due_amount = JSONParserUtil.getString(particulars, "dueamount");
                         result.setMessage(ipay_errordesc + ".your dueamount is " + due_amount);
                     	}else {
                     		result.setMessage(ipay_errordesc);
                     	}
                    }else{
                        result.setSuccess(false);
                        result.setMessage(ipay_errordesc);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }
}
