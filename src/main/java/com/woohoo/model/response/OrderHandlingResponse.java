package com.woohoo.model.response;

public class OrderHandlingResponse {

	private String code;
	private String message;
	private long handling_amount;
	private boolean success;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	
	public long getHandling_amount() {
		return handling_amount;
	}
	public void setHandling_amount(long handling_amount) {
		this.handling_amount = handling_amount;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	
	
}
