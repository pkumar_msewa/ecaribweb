package com.woohoo.model.response;

import org.hibernate.type.PrimitiveByteArrayBlobType;

import com.sun.org.apache.xml.internal.security.keys.keyresolver.implementations.PrivateKeyResolver;

public class ProductDescriptionResponse {

	private String product_id;
	private String product_name;
	private String description;
	private String short_description;
	private String sku;
	private String min_custom_price;
	private String max_custom_price;
	private String custom_denominations;
	private String product_type;
	private boolean paywithamazon_disable;
	private String base_image;
	private String tnc_mobile;
	private String tnc_web;
	private String tnc_mail;
	private String order_handling_charge;
	private String themes;
	private String code;
	private String message;
	private boolean success;
	
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getProduct_id() {
		return product_id;
	}
	public void setProduct_id(String product_id) {
		this.product_id = product_id;
	}
	public String getProduct_name() {
		return product_name;
	}
	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getShort_description() {
		return short_description;
	}
	public void setShort_description(String short_description) {
		this.short_description = short_description;
	}
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	public String getMin_custom_price() {
		return min_custom_price;
	}
	public void setMin_custom_price(String min_custom_price) {
		this.min_custom_price = min_custom_price;
	}
	public String getMax_custom_price() {
		return max_custom_price;
	}
	public void setMax_custom_price(String max_custom_price) {
		this.max_custom_price = max_custom_price;
	}
	public String getCustom_denominations() {
		return custom_denominations;
	}
	public void setCustom_denominations(String custom_denominations) {
		this.custom_denominations = custom_denominations;
	}
	public String getProduct_type() {
		return product_type;
	}
	public void setProduct_type(String product_type) {
		this.product_type = product_type;
	}
	public boolean isPaywithamazon_disable() {
		return paywithamazon_disable;
	}
	public void setPaywithamazon_disable(boolean paywithamazon_disable) {
		this.paywithamazon_disable = paywithamazon_disable;
	}
	public String getBase_image() {
		return base_image;
	}
	public void setBase_image(String base_image) {
		this.base_image = base_image;
	}
	public String getTnc_mobile() {
		return tnc_mobile;
	}
	public void setTnc_mobile(String tnc_mobile) {
		this.tnc_mobile = tnc_mobile;
	}
	public String getTnc_web() {
		return tnc_web;
	}
	public void setTnc_web(String tnc_web) {
		this.tnc_web = tnc_web;
	}
	public String getTnc_mail() {
		return tnc_mail;
	}
	public void setTnc_mail(String tnc_mail) {
		this.tnc_mail = tnc_mail;
	}
	public String getOrder_handling_charge() {
		return order_handling_charge;
	}
	public void setOrder_handling_charge(String order_handling_charge) {
		this.order_handling_charge = order_handling_charge;
	}
	public String getThemes() {
		return themes;
	}
	public void setThemes(String themes) {
		this.themes = themes;
	}
	
	
	
}
