package com.woohoo.model.request;


public class Product {

	private String product_id;
	private String brand_id;
	private String brand_name;
	private Images images;
	private String min_custom_price;
	private String max_custom_price;
	private String custom_denominations;
	private String imageUrl;
	private String tempUrl;
	private String termsAndAcondition;
	private String brandCode;
	
	public String getBrandCode() {
		return brandCode;
	}
	public void setBrandCode(String brandCode) {
		this.brandCode = brandCode;
	}
	public String getTermsAndAcondition() {
		return termsAndAcondition;
	}
	public void setTermsAndAcondition(String termsAndAcondition) {
		this.termsAndAcondition = termsAndAcondition;
	}
	public String getTempUrl() {
		return tempUrl;
	}
	public void setTempUrl(String tempUrl) {
		this.tempUrl = tempUrl;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public String getProduct_id() {
		return product_id;
	}
	public void setProduct_id(String product_id) {
		this.product_id = product_id;
	}
	public String getBrand_id() {
		return brand_id;
	}
	public void setBrand_id(String brand_id) {
		this.brand_id = brand_id;
	}
	public String getBrand_name() {
		return brand_name;
	}
	public void setBrand_name(String brand_name) {
		this.brand_name = brand_name;
	}
	public Images getImages() {
		return images;
	}
	public void setImages(Images images) {
		this.images = images;
	}
	public String getMin_custom_price() {
		return min_custom_price;
	}
	public void setMin_custom_price(String min_custom_price) {
		this.min_custom_price = min_custom_price;
	}
	public String getMax_custom_price() {
		return max_custom_price;
	}
	public void setMax_custom_price(String max_custom_price) {
		this.max_custom_price = max_custom_price;
	}
	public String getCustom_denominations() {
		return custom_denominations;
	}
	public void setCustom_denominations(String custom_denominations) {
		this.custom_denominations = custom_denominations;
	}
	
	
	
	
}
