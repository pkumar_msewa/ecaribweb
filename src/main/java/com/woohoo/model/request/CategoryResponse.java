package com.woohoo.model.request;

import java.util.List;


public class CategoryResponse {

	private String categoryImage;
	private List<Category> categoryList;
	private String code;
	private boolean success;
	private String message;
	private int max_item;
	private int max_qty;
	
	
	
	public int getMax_item() {
		return max_item;
	}
	public void setMax_item(int max_item) {
		this.max_item = max_item;
	}
	public int getMax_qty() {
		return max_qty;
	}
	public void setMax_qty(int max_qty) {
		this.max_qty = max_qty;
	}
	public String getCategoryImage() {
		return categoryImage;
	}
	public void setCategoryImage(String categoryImage) {
		this.categoryImage = categoryImage;
	}
	public List<Category> getCategoryList() {
		return categoryList;
	}
	public void setCategoryList(List<Category> categoryList) {
		this.categoryList = categoryList;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	
	
}
