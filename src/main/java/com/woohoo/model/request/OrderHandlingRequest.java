package com.woohoo.model.request;

import java.util.List;

public class OrderHandlingRequest {
	
	private List<String> productIds;
	
	private String sessionId; 
	
	private String data;
	

	
	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public List<String> getProductIds() {
		return productIds;
	}

	public void setProductIds(List<String> productIds) {
		this.productIds = productIds;
	}

	
	
	

}
