package com.woohoo.model.request;

public class Category {

	private long category_id;
	private String category_name;
	private String category_image;
	private String child_category;
	
	
	
	public long getCategory_id() {
		return category_id;
	}
	public void setCategory_id(long category_id) {
		this.category_id = category_id;
	}
	public String getCategory_name() {
		return category_name;
	}
	public void setCategory_name(String category_name) {
		this.category_name = category_name;
	}
	public String getCategory_image() {
		return category_image;
	}
	public void setCategory_image(String category_image) {
		this.category_image = category_image;
	}
	public String getChild_category() {
		return child_category;
	}
	public void setChild_category(String child_category) {
		this.child_category = child_category;
	}
	
	
	
}
