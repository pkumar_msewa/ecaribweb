package com.woohoo.model.request;

public class Images {

	private String image;
	private String small_image;
	private String thumbnail;
	private String mobile;
	private String base_image;
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getSmall_image() {
		return small_image;
	}
	public void setSmall_image(String small_image) {
		this.small_image = small_image;
	}
	public String getThumbnail() {
		return thumbnail;
	}
	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getBase_image() {
		return base_image;
	}
	public void setBase_image(String base_image) {
		this.base_image = base_image;
	}
	
	
	
	
}
