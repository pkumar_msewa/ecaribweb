package com.woohoo.model.request;

import java.util.List;

public class WoohooOrderRequest {

	private BillingAddress billingAddress;
	private ShippingAddress shippingAddress;
	private List<ProductValue> productValue;

	private String sessionId;
	private String transactionRefNo;
	private boolean status;
	private String message;
	private String data;
	private String orederId;
	private double price;
	

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public BillingAddress getBillingAddress() {
		return billingAddress;
	}

	public void setBillingAddress(BillingAddress billingAddress) {
		this.billingAddress = billingAddress;
	}

	public ShippingAddress getShippingAddress() {
		return shippingAddress;
	}

	public void setShippingAddress(ShippingAddress shippingAddress) {
		this.shippingAddress = shippingAddress;
	}

	public List<ProductValue> getProductValue() {
		return productValue;
	}

	public void setProductValue(List<ProductValue> productValue) {
		this.productValue = productValue;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getTransactionRefNo() {
		return transactionRefNo;
	}

	public void setTransactionRefNo(String transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getOrederId() {
		return orederId;
	}

	public void setOrederId(String orederId) {
		this.orederId = orederId;
	}

}
