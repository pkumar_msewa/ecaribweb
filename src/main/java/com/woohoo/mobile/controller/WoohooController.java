package com.woohoo.mobile.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.payqwikweb.api.IAuthenticationApi;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.ResponseStatus;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.request.SessionDTO;
import com.payqwikweb.util.Authorities;
import com.woohoo.api.IWoohooApi;
import com.woohoo.model.request.CategoryResponse;
import com.woohoo.model.request.GiftCardStatus;
import com.woohoo.model.request.OrderHandlingRequest;
import com.woohoo.model.request.PlaceOrderRequest;
import com.woohoo.model.response.CategoryProductResponse;
import com.woohoo.model.response.OrderHandlingResponse;
import com.woohoo.model.response.OrderPlaceResponse;
import com.woohoo.model.response.ProductDescriptionResponse;
import com.woohoo.util.AES;
import com.woohoo.util.WoohooUtil;

@Controller
@RequestMapping("/Api/{version}/{role}/{device}/{language}/Woohoo")
public class WoohooController implements MessageSourceAware {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	private MessageSource messageSource;

	private final IWoohooApi woohooApi;
	private final IAuthenticationApi authenticationApi;

	public WoohooController(IWoohooApi woohooApi, IAuthenticationApi authenticationApi) {
		this.woohooApi = woohooApi;
		this.authenticationApi = authenticationApi;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	@RequestMapping(value = "/categories", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<CategoryResponse> getCategories(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language,
			@RequestHeader(value = "hash", required = false) String hash) {
		CategoryResponse response = new CategoryResponse();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				response = woohooApi.getCategories();
				return new ResponseEntity<>(response, HttpStatus.OK);
			} else {
				response.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				response.setMessage(ResponseStatus.UNAUTHORIZED_USER.getKey());

			}
		} else {
			response.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
			response.setMessage(ResponseStatus.UNAUTHORIZED_USER.getKey());

		}
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@RequestMapping(value = "/categories/{categoryId}", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<CategoryProductResponse> getProductsByCategory(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @PathVariable(value = "categoryId") String categoryId,
			@RequestHeader(value = "hash", required = false) String hash) {
		CategoryProductResponse response = new CategoryProductResponse();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				response = woohooApi.getProductByCategory(categoryId);
				return new ResponseEntity<CategoryProductResponse>(response, HttpStatus.OK);
			} else {
				response.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				response.setMessage(ResponseStatus.UNAUTHORIZED_USER.getKey());

			}
		} else {
			response.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
			response.setMessage(ResponseStatus.UNAUTHORIZED_USER.getKey());

		}
		return new ResponseEntity<CategoryProductResponse>(response, HttpStatus.OK);
	}

	@RequestMapping(value = "/products/{productId}", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ProductDescriptionResponse> getProductsById(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @PathVariable(value = "productId") String productId,
			@RequestHeader(value = "hash", required = false) String hash) {
		ProductDescriptionResponse response = new ProductDescriptionResponse();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				response = woohooApi.getByProductId(productId);
				return new ResponseEntity<ProductDescriptionResponse>(response, HttpStatus.OK);
			} else {
				response.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				response.setMessage(ResponseStatus.UNAUTHORIZED_USER.getKey());

			}
		} else {
			response.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
			response.setMessage(ResponseStatus.UNAUTHORIZED_USER.getKey());

		}
		return new ResponseEntity<ProductDescriptionResponse>(response, HttpStatus.OK);
	}

	@RequestMapping(value = "/recheckPrice", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<OrderHandlingResponse> recheckPrice(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody OrderHandlingRequest order,
			@RequestHeader(value = "hash", required = false) String hash) {
		OrderHandlingResponse response = new OrderHandlingResponse();
		OrderHandlingRequest request = new OrderHandlingRequest();
		try {
			String sessionId = (String) order.getSessionId();
			if (sessionId != null && sessionId.length() != 0) {
				String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
				if (authority != null) {
					if (role.equalsIgnoreCase(Role.USER.getValue())) {
						if (device.equalsIgnoreCase(Device.ANDROID.getValue())
								|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
								|| device.equalsIgnoreCase(Device.IOS.getValue())) {
							if (order.getData() != null) {
							//	String decdata = AES.decrypt(order.getData());
								String decdata = null;
								request = WoohooUtil.prepareRecheckRequest(decdata);
								response = woohooApi.checkOrders(request);
								return new ResponseEntity<OrderHandlingResponse>(response, HttpStatus.OK);
							} else {
								response.setCode(ResponseStatus.FAILURE.getValue());
								response.setMessage(ResponseStatus.FAILURE.getKey());
							}
						} else {
							response.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
							response.setMessage(ResponseStatus.UNAUTHORIZED_USER.getKey());

						}
					} else {
						response.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
						response.setMessage(ResponseStatus.UNAUTHORIZED_USER.getKey());

					}
				} else {
					response.setCode(ResponseStatus.INVALID_SESSION.getValue());
					response.setMessage(ResponseStatus.INVALID_SESSION.getKey());
				}
			} else {
				response.setCode(ResponseStatus.INVALID_SESSION.getValue());
				response.setMessage(ResponseStatus.INVALID_SESSION.getKey());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<OrderHandlingResponse>(response, HttpStatus.OK);
	}

/*	@RequestMapping(value = "/processTransaction", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<OrderPlaceResponse> processOrder(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody PlaceOrderRequest order,
			@RequestHeader(value = "hash", required = false) String hash) {
		OrderPlaceResponse response = new OrderPlaceResponse();
		try {
			String sessionId = (String) order.getSessionId();
			if (sessionId != null && sessionId.length() != 0) {
				String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
				if (authority != null) {
					if (role.equalsIgnoreCase(Role.USER.getValue())) {
						if (device.equalsIgnoreCase(Device.ANDROID.getValue())
								|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
								|| device.equalsIgnoreCase(Device.IOS.getValue())) {
							order.setSessionId(sessionId);
							if (order.getData() != null) {
								String decdata = AES.decrypt(order.getData());
								PlaceOrderRequest ord = WoohooUtil.prepareRequest(decdata);
								ord.setSessionId(order.getSessionId());
								response = woohooApi.initiatePayment(ord);
								if (response.getCode().equalsIgnoreCase("S00")) {
									ord.setTransactionRefNo(response.getTransactionRefNo());
									response = woohooApi.placeOrder(ord);
									if (response.isStatus()) {
										response.setTransactionRefNo(ord.getTransactionRefNo());
										response.setSessionId(ord.getSessionId());
										order.setOrederId(response.getOrderNumber());
										response = woohooApi.processResponse(response);
										response.setOrderNumber(order.getOrederId());
										return new ResponseEntity<OrderPlaceResponse>(response, HttpStatus.OK);
									} else {
										response.setTransactionRefNo(ord.getTransactionRefNo());
										response.setSessionId(ord.getSessionId());
										order.setMessage(response.getMessage());
										response.setCard_price(ord.getPrice() + "");
										order.setOrederId(response.getOrderNumber());
										response = woohooApi.processResponse(response);
										response.setMessage(order.getMessage());
										response.setOrderNumber(order.getOrederId());
										return new ResponseEntity<OrderPlaceResponse>(response, HttpStatus.OK);
									}
								} else {
									response.setCode(ResponseStatus.FAILURE.getValue());
									response.setMessage(response.getMessage());
									return new ResponseEntity<OrderPlaceResponse>(response, HttpStatus.OK);
								}
							} else {
								response.setCode(ResponseStatus.FAILURE.getValue());
								response.setMessage(ResponseStatus.FAILURE.getKey());
							}
							return new ResponseEntity<OrderPlaceResponse>(response, HttpStatus.OK);
						} else {
							response.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
							response.setMessage(ResponseStatus.UNAUTHORIZED_USER.getKey());

						}
					} else {
						response.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
						response.setMessage(ResponseStatus.UNAUTHORIZED_USER.getKey());

					}
				} else {
					response.setCode(ResponseStatus.INVALID_SESSION.getValue());
					response.setMessage(ResponseStatus.INVALID_SESSION.getKey());
				}
			} else {
				response.setCode(ResponseStatus.INVALID_SESSION.getValue());
				response.setMessage(ResponseStatus.INVALID_SESSION.getKey());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<OrderPlaceResponse>(response, HttpStatus.OK);
	}*/

	@RequestMapping(value = "/processTransaction", method = RequestMethod.POST)
	public ResponseEntity<OrderPlaceResponse> woohooProcessOrder(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody PlaceOrderRequest dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws Exception {

		OrderPlaceResponse resp = new OrderPlaceResponse();
		try {
			if (version.equalsIgnoreCase("v1")) {
				if (role.equalsIgnoreCase("User")) {
					if (device.equalsIgnoreCase("Android")) {
						if (language.equalsIgnoreCase("EN")) {
							if (dto.getData() != null) {
							//	String decdata = AES.decrypt(dto.getData());
								String decdata = null;
								PlaceOrderRequest ord = WoohooUtil.prepareRequest(decdata);
								ord.setSessionId(dto.getSessionId());
							    resp = woohooApi.processOrder(ord);
							 }
						} else {
							resp.setSuccess(false);
							resp.setMessage("Only EN locale available");
						}
					}else if(device.equalsIgnoreCase("Ios")){
						System.err.println("inside ios controller");
						String sessionId = dto.getSessionId();
						if (language.equalsIgnoreCase("EN")) {
							if (sessionId != null) {
								String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
								if (authority != null) {
									if (authority.contains(Authorities.USER)
											&& authority.contains(Authorities.AUTHENTICATED)) {
										    dto.setSessionId(sessionId);
											resp = woohooApi.processOrder(dto);
										return new ResponseEntity<OrderPlaceResponse>(resp, HttpStatus.OK);
									} else {
										resp.setSuccess(false);
										resp.setCode("F06");
										resp.setMessage("User Authentication Failed");
									}
								} else {
									resp.setSuccess(false);
									resp.setCode("F03");
									resp.setMessage("Invaid Session");
								}
							} else {
								resp.setSuccess(false);
								resp.setCode("F03");
								resp.setMessage("Session null");
							}
						} else {
							resp.setSuccess(false);
							resp.setMessage("Only EN locale available");
					}
					} else {
						resp.setSuccess(false);
						resp.setMessage("Unauthorized Device");
					}
				} else {
					resp.setSuccess(false);
					resp.setMessage("Unauthorized Role");
				}
			} else {
				resp.setSuccess(false);
				resp.setMessage("Invalid Version");
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setMessage("We are sorry for inconvenience, Please try again later .");
		}
		return new ResponseEntity<>(resp, HttpStatus.OK);
	}

	/*
	 * @RequestMapping(value = "/productList", method = RequestMethod.GET,
	 * produces = { MediaType.APPLICATION_JSON_VALUE })
	 * ResponseEntity<CategoryProductResponse>
	 * getProductsByCate(@PathVariable(value = "version") String version,
	 * 
	 * @PathVariable(value = "role") String role, @PathVariable(value =
	 * "device") String device,
	 * 
	 * @PathVariable(value = "language") String language,
	 * 
	 * @RequestHeader(value = "hash", required = false) String hash) {
	 * CategoryProductResponse response = new CategoryProductResponse(); if
	 * (role.equalsIgnoreCase(Role.USER.getValue())) { if
	 * (device.equalsIgnoreCase(Device.ANDROID.getValue()) ||
	 * device.equalsIgnoreCase(Device.WINDOWS.getValue()) ||
	 * device.equalsIgnoreCase(Device.IOS.getValue())) { // response =
	 * woohooApi.getProductByCate(); return new
	 * ResponseEntity<CategoryProductResponse>(response, HttpStatus.OK); } else
	 * { response.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
	 * response.setMessage(ResponseStatus.UNAUTHORIZED_USER.getKey());
	 * 
	 * } } else { response.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
	 * response.setMessage(ResponseStatus.UNAUTHORIZED_USER.getKey());
	 * 
	 * } return new ResponseEntity<CategoryProductResponse>(response,
	 * HttpStatus.OK); }
	 */

	@RequestMapping(value = "/PendingCards", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<OrderPlaceResponse> getPendingGiftCards(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody GiftCardStatus dto,
			@RequestHeader(value = "hash", required = false) String hash) {
		OrderPlaceResponse response = new OrderPlaceResponse();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				response = woohooApi.getPendingStatus(dto.getReferenceNo());
			} else {
				response.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				response.setMessage(ResponseStatus.UNAUTHORIZED_USER.getKey());
			}
			return new ResponseEntity<OrderPlaceResponse>(response, HttpStatus.OK);
		}
		return new ResponseEntity<OrderPlaceResponse>(response, HttpStatus.OK);
	}

	@RequestMapping(value = "/productList", method = RequestMethod.POST)
	public ResponseEntity<CategoryProductResponse> getUserOTP(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody SessionDTO dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws Exception {

		CategoryProductResponse resp = new CategoryProductResponse();
		try {
			String sessionId = dto.getSessionId();
			if (version.equalsIgnoreCase("v1")) {
				if (role.equalsIgnoreCase("User")) {
					if (device.equalsIgnoreCase("Android") || device.equalsIgnoreCase("IOS")) {
						if (language.equalsIgnoreCase("EN")) {
							if (sessionId != null) {
								String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
								if (authority != null) {
									if (authority.contains(Authorities.USER)
											&& authority.contains(Authorities.AUTHENTICATED)) {
										dto.setSessionId(sessionId);
										resp = woohooApi.getProductByCate(dto);
									} else {
										resp.setSuccess(false);
										resp.setCode("F03");
										resp.setMessage("User Authentication Failed");
									}
								} else {
									resp.setSuccess(false);
									resp.setCode("F03");
									resp.setMessage("Invaid Session");
								}
							} else {
								resp.setSuccess(false);
								resp.setCode("F03");
								resp.setMessage("Session null");
							}
						} else {
							resp.setSuccess(false);
							resp.setMessage("Only EN locale available");
						}
					} else {
						resp.setSuccess(false);
						resp.setMessage("Unauthorized Device");
					}
				} else {
					resp.setSuccess(false);
					resp.setMessage("Unauthorized Role");
				}
			} else {
				resp.setSuccess(false);
				resp.setMessage("Invalid Version");
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setMessage("We are sorry for inconvenience, Please try again later .");
		}
		return new ResponseEntity<>(resp, HttpStatus.OK);
	}

}
