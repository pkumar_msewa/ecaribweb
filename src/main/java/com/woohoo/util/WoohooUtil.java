package com.woohoo.util;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.woohoo.model.request.OrderHandlingRequest;
import com.woohoo.model.request.PlaceOrderRequest;
import com.woohoo.model.request.ProductValue;
import com.woohoo.model.request.WoohooOrderRequest;

public class WoohooUtil {
public final static String key="@dm!nVP@yQw!k";

private static final String woohooKey = "5CF67A47D71A60F6759DDDC44C41D36E3B1E0D75";
private static final String woohooToken = "MTUwMTA5MzU3NjIxNzM1NjAwMDAwMDAwMDAwMDU=";

public static String getWoohookey() {
	return woohooKey;
}

public static String getWoohootoken() {
	return woohooToken;
}
	
	public static JSONObject buildOrderRequest(PlaceOrderRequest request){
		
		JSONObject jObject=new JSONObject();
		JSONObject jsonObject=new JSONObject();
		JSONObject billing=new JSONObject();
		JSONObject shipping=new JSONObject();
		JSONArray products=new JSONArray();
		try {
			billing.put("firstname", request.getFirstname());
			billing.put("lastname", request.getLastname());
			billing.put("email", request.getEmail());
			billing.put("telephone", request.getTelephone());
			billing.put("line_1", request.getLine_1());
			billing.put("line_2", request.getLine_2());
			billing.put("city", request.getCity());
			billing.put("region", request.getRegion());
			billing.put("country_id",request.getCountry_id());
			billing.put("postcode", request.getPostcode());
			shipping.put("firstname", request.getFirstname());
			shipping.put("lastname", request.getLastname());
			shipping.put("email", request.getEmail());
			shipping.put("telephone", request.getTelephone());
			shipping.put("line_1", request.getLine_1());
			shipping.put("line_2", request.getLine_2());
			shipping.put("city", request.getCity());
			shipping.put("region", request.getRegion());
			shipping.put("country_id",request.getCountry_id());
			shipping.put("postcode", request.getPostcode());
			jsonObject.put("product_id", request.getProduct_id());
			jsonObject.put("price", request.getPrice());
			jsonObject.put("qty", (int)request.getQty());
			products.put(jsonObject);
			jObject.put("billing", billing);
			jObject.put("shipping", shipping);
			jObject.put("products", products);

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jObject;
	}
	
	
	public static JSONObject buildOrderRequestNew(WoohooOrderRequest request){
		
		JSONObject jObject=new JSONObject();
		JSONObject jsonObject=new JSONObject();
		JSONObject billing=new JSONObject();
		JSONObject shipping=new JSONObject();
		JSONArray products=new JSONArray();
		try {
			billing.put("firstname", request.getBillingAddress().getFirstname());
			billing.put("lastname", request.getBillingAddress().getLastname());
			billing.put("email", request.getBillingAddress().getEmail());
			billing.put("telephone", request.getBillingAddress().getTelephone());
			billing.put("line_1", request.getBillingAddress().getLine_1());
			billing.put("line_2", request.getBillingAddress().getLine_2());
			billing.put("city", request.getBillingAddress().getCity());
			billing.put("region", request.getBillingAddress().getRegion());
			billing.put("country_id",request.getBillingAddress().getCountry_id());
			billing.put("postcode", request.getBillingAddress().getPostcode());
			shipping.put("firstname", request.getShippingAddress().getFirstname());
			shipping.put("lastname", request.getShippingAddress().getLastname());
			shipping.put("email", request.getShippingAddress().getEmail());
			shipping.put("telephone", request.getShippingAddress().getTelephone());
			shipping.put("line_1", request.getShippingAddress().getLine_1());
			shipping.put("line_2", request.getShippingAddress().getLine_2());
			shipping.put("city", request.getShippingAddress().getCity());
			shipping.put("region", request.getShippingAddress().getRegion());
			shipping.put("country_id",request.getShippingAddress().getCountry_id());
			shipping.put("postcode", request.getShippingAddress().getPostcode());
			for(ProductValue value : request.getProductValue()){
				jsonObject.put("product_id", value.getProduct_id());
				jsonObject.put("price", value.getPrice());
				jsonObject.put("qty", value.getQty());
				jsonObject.put("brandCode",value.getBrandCode());
				products.put(jsonObject);
			}
			jObject.put("billing", billing);
			jObject.put("shipping", shipping);
			jObject.put("products", products);

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jObject;
	}

	public static PlaceOrderRequest prepareRequest(String decdata) {
		PlaceOrderRequest req= new PlaceOrderRequest();
		try {
			JSONObject obj=new JSONObject(decdata);
			req.setCity(obj.getString("city"));
			req.setCountry_id(obj.getString("country_id"));
			req.setEmail(obj.getString("email"));
			req.setFirstname(obj.getString("firstname"));
			req.setLastname(obj.getString("lastname"));
			req.setLine_1(obj.getString("line_1"));
			req.setLine_2(obj.getString("line_2"));
			req.setPostcode(obj.getString("postcode"));
			req.setPrice(Double.valueOf(obj.getString("price")));
			req.setProduct_id(obj.getString("product_id"));
			req.setQty(Double.valueOf(obj.getString("qty")));
			req.setRegion(obj.getString("region"));
			req.setTelephone(obj.getString("telephone"));
			req.setBrandCode(obj.getString("brandCode"));
		return req;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return req;
	}

	public static OrderHandlingRequest prepareRecheckRequest(String decdata) {
		OrderHandlingRequest req = new OrderHandlingRequest();
		try {
			JSONObject obj=new JSONObject(decdata);
			JSONArray arr=obj.getJSONArray("productIds");
			List<String> list= new ArrayList<String>();
			for(int i=0; i<arr.length();i++){
				list.add(arr.getString(i));
			}
			req.setProductIds(list);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return req;
	}
}
