package com.woohoo.api.impl;

import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.payqwikweb.app.metadatas.UrlMetadatas;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Language;
import com.payqwikweb.app.model.ResponseStatus;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.Version;
import com.payqwikweb.app.model.request.SessionDTO;
import com.payqwikweb.app.utils.SecurityUtils;
import com.payqwikweb.util.JSONParserUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.woohoo.api.IWoohooApi;
import com.woohoo.model.request.Category;
import com.woohoo.model.request.CategoryResponse;
import com.woohoo.model.request.Images;
import com.woohoo.model.request.OrderHandlingRequest;
import com.woohoo.model.request.PlaceOrderRequest;
import com.woohoo.model.request.Product;
import com.woohoo.model.response.CategoryProductResponse;
import com.woohoo.model.response.OrderHandlingResponse;
import com.woohoo.model.response.OrderPlaceResponse;
import com.woohoo.model.response.ProductDescriptionResponse;
import com.woohoo.util.WoohooUtil;

public class WoohooApiImpl implements IWoohooApi {

	@Override
	public CategoryResponse getCategories() {
		CategoryResponse categoryResponse = new CategoryResponse();
		try {
		
		Client client = Client.create();
		WebResource webResource = client
				.resource(UrlMetadatas.getCategories(Version.VERSION_1, Role.ClIENT, Device.ANDROID, Language.ENGLISH));
		ClientResponse response = webResource.accept("application/json").type("application/json")
				.header("clientIp", Inet4Address.getLocalHost().getHostAddress()).header("cllientKey", UrlMetadatas.getMdexClientKey())
				.header("clientToken", UrlMetadatas.getMdexClientToken()).header("clientApiName", "Category Search")
				.get(ClientResponse.class);
		String strResponse = response.getEntity(String.class);
		
			JSONObject jObject = new JSONObject(strResponse);
			String message = jObject.getString("message");
			boolean success = jObject.getBoolean("success");
			if (response.getStatus() != 200) {
				categoryResponse.setMessage(message);
				categoryResponse.setSuccess(success);
			} else {
				if (success) {
					Integer code = jObject.getInt("code");
					JSONObject data = jObject.getJSONObject("data");
					JSONObject rootObject = data.getJSONObject("root_category");
					JSONObject config = data.getJSONObject("config");
					int max_item = config.getInt("max_item_in_cart");
					int max_qty = config.getInt("product_max_qty_allow");
					String categoryImage = rootObject.getString("category_image");
					categoryResponse.setCode(String.valueOf(code));
					categoryResponse.setSuccess(success);
					categoryResponse.setMessage(message);
					categoryResponse.setCategoryImage(categoryImage);
					JSONArray jArray = rootObject.getJSONArray("category_List");
					List<Category> categoryList = new ArrayList<>();
					if (jArray != null) {
						for (int i = 0; i < jArray.length(); i++) {
							Category category = new Category();
							JSONObject jsonObject = jArray.getJSONObject(i);
							JSONObject parent = jsonObject.getJSONObject("parent_cateory");
							category.setCategory_name(parent.getString("category_name"));
							category.setCategory_id(parent.getLong("category_id"));
							category.setCategory_image(parent.getString("category_image"));
							categoryList.add(category);
						}
					}
					categoryResponse.setCategoryList(categoryList);
					categoryResponse.setMax_item(max_item);
					categoryResponse.setMax_qty(max_qty);
				} else {
					categoryResponse.setMessage(message);
					categoryResponse.setSuccess(success);
				}
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UniformInterfaceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientHandlerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return categoryResponse;
	}

	@Override
	public CategoryProductResponse getProductByCategory(String categoryId) {

		CategoryProductResponse productResponse = new CategoryProductResponse();
		Client client = Client.create();
		WebResource webResource = client.resource(UrlMetadatas.getProductsByCategories(Version.VERSION_1, Role.ClIENT,
				Device.ANDROID, Language.ENGLISH, categoryId));
		ClientResponse response;
		try {
			response = webResource.accept("application/json").type("application/json")
					.header("clientIp", Inet4Address.getLocalHost().getHostAddress()).header("cllientKey", UrlMetadatas.getMdexClientKey())
					.header("clientToken", UrlMetadatas.getMdexClientToken()).header("clientApiName", "Category Search")
					.get(ClientResponse.class);
		String strResponse = response.getEntity(String.class);
			JSONObject jObject = new JSONObject(strResponse);
			String message = jObject.getString("message");
			boolean success = jObject.getBoolean("success");
			int code = jObject.getInt("code");
			if (response.getStatus() != 200) {
				productResponse.setMessage(message);
				productResponse.setSuccess(success);
				productResponse.setCode(String.valueOf(code));
			} else {
				if (success) {
					JSONObject data = jObject.getJSONObject("data");
					productResponse.setCategoryId(data.getString("id"));
					productResponse.setCategoryName(data.getString("name"));
					JSONObject embedded = data.getJSONObject("_embedded");
					JSONArray jArray = embedded.getJSONArray("product");
					if (jArray != null) {
						List<Product> products = new ArrayList<>();
						for (int i = 0; i < jArray.length(); i++) {
							JSONObject jsonObject = jArray.getJSONObject(i);
							String min_custom_price = jsonObject.getString("min_custom_price");
							String max_price = jsonObject.getString("max_custom_price");
							String denomination = jsonObject.getString("custom_denominations");
							String productId = jsonObject.getString("id");
							String brand_id = jsonObject.getString("brand_id");
							String product_name = jsonObject.getString("name");
							JSONObject image = jsonObject.getJSONObject("images");
							String image_url = image.getString("image");
							String small_image = image.getString("small_image");
							String thumbnail = image.getString("thumbnail");
							String base_image = image.getString("base_image");
							Product product = new Product();
							product.setBrand_id(brand_id);
							product.setBrand_name(product_name);
							product.setProduct_id(productId);
							Images im = new Images();
							im.setBase_image(base_image);
							im.setImage(image_url);
							im.setSmall_image(small_image);
							im.setThumbnail(thumbnail);
							product.setImages(im);
							product.setCustom_denominations(denomination);
							product.setMax_custom_price(max_price);
							product.setMin_custom_price(min_custom_price);
							products.add(product);
						}
						productResponse.setProduct(products);
						productResponse.setCode("S00");
						productResponse.setSuccess(true);
					}

				} else {
					productResponse.setCode("F00");
					productResponse.setMessage(message);
					productResponse.setSuccess(false);

				}
			}

		} catch (UniformInterfaceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientHandlerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			productResponse.setCode("F00");
			productResponse.setMessage("opps!! Technical Problem , Please Try After Sometime.");
			productResponse.setSuccess(false);
			e.printStackTrace();
		}
		return productResponse;

	}

	@Override
	public ProductDescriptionResponse getByProductId(String productId) {

		ProductDescriptionResponse descriptionResponse = new ProductDescriptionResponse();
		try {
		Client client = Client.create();
		WebResource webResource = client.resource(UrlMetadatas.getProductsDesciption(Version.VERSION_1, Role.ClIENT,
				Device.ANDROID, Language.ENGLISH, productId));
		
		ClientResponse 	response = webResource.accept("application/json").type("application/json")
						.header("clientIp", Inet4Address.getLocalHost().getHostAddress()).header("cllientKey", UrlMetadatas.getMdexClientKey())
						.header("clientToken", UrlMetadatas.getMdexClientToken()).header("clientApiName", "Category Search")
						.get(ClientResponse.class);
		String strResponse = response.getEntity(String.class);
		
			JSONObject jObject = new JSONObject(strResponse);
			String message = jObject.getString("message");
			boolean success = jObject.getBoolean("success");
			int code = jObject.getInt("code");
			if (response.getStatus() != 200) {
				descriptionResponse.setMessage(message);
				descriptionResponse.setSuccess(success);
				descriptionResponse.setCode(String.valueOf(code));
			} else {
				if (success) {
					JSONObject data = jObject.getJSONObject("data");
					descriptionResponse.setProduct_id(data.getString("id"));
					descriptionResponse.setProduct_name(data.getString("name"));
					descriptionResponse.setShort_description(data.getString("short_description"));
					descriptionResponse.setSku(data.getString("sku"));
					descriptionResponse.setMax_custom_price(data.getString("max_custom_price"));
					descriptionResponse.setMin_custom_price(data.getString("min_custom_price"));
					descriptionResponse.setCustom_denominations(data.getString("custom_denominations"));
					descriptionResponse.setProduct_type(data.getString("product_type"));
					descriptionResponse.setPaywithamazon_disable(data.getBoolean("paywithamazon_disable"));
					descriptionResponse.setBase_image(data.getString("images"));
					descriptionResponse.setTnc_mobile(data.getString("tnc_mobile"));
					descriptionResponse.setTnc_web(data.getString("tnc_web"));
					descriptionResponse.setTnc_mail(data.getString("tnc_mail"));
					descriptionResponse.setOrder_handling_charge(data.getString("order_handling_charge"));
						if (data.getJSONObject("themes").toString() != null) {
							descriptionResponse.setThemes(data.getJSONObject("themes").toString());
						}
					// descriptionResponse.setCode(String.valueOf(code));
					descriptionResponse.setCode("S00");
					descriptionResponse.setMessage(message);
					descriptionResponse.setSuccess(success);
				}

				else {
					descriptionResponse.setCode("F00");
					descriptionResponse.setMessage(message);
					descriptionResponse.setSuccess(false);

				}
			}
	} catch (UniformInterfaceException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (ClientHandlerException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (UnknownHostException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (JSONException e) {
		// TODO Auto-generated catch block
		descriptionResponse.setCode("F00");
		descriptionResponse.setMessage("opps!! Technical Problem , Please Try After Sometime.");
		descriptionResponse.setSuccess(false);
		e.printStackTrace();
	}
		return descriptionResponse;

	}

	@Override
	public OrderHandlingResponse checkOrders(OrderHandlingRequest request) {

		OrderHandlingResponse handlingResponse = new OrderHandlingResponse();
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("productIds", request.getProductIds());

			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getPriceRecheck(Version.VERSION_1, Role.ClIENT, Device.ANDROID, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("clientIp", Inet4Address.getLocalHost().getHostAddress()).header("cllientKey", UrlMetadatas.getMdexClientKey())
					.header("clientToken", UrlMetadatas.getMdexClientToken()).header("clientApiName", "Category Search")
					.post(ClientResponse.class, jsonObject);
			String strResponse = response.getEntity(String.class);
			JSONObject jObject = new JSONObject(strResponse);
			String message = jObject.getString("message");
			boolean success = jObject.getBoolean("success");
			int code = jObject.getInt("code");
			if (response.getStatus() != 200) {
				handlingResponse.setMessage(message);
				handlingResponse.setSuccess(success);
				handlingResponse.setCode(String.valueOf(code));
			} else {
				if (success) {
					handlingResponse.setMessage(message);
					handlingResponse.setSuccess(success);
					handlingResponse.setCode(String.valueOf(code));
					handlingResponse.setHandling_amount(jObject.getJSONObject("data").getLong("handling_amount"));
				} else {
					handlingResponse.setMessage(message);
					handlingResponse.setSuccess(success);
					handlingResponse.setCode(String.valueOf(code));

				}

				// TODO Auto-generated method stub
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UniformInterfaceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientHandlerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return handlingResponse;
	}

	@Override
	public OrderPlaceResponse placeOrder(PlaceOrderRequest request) {
		// TODO Auto-generated method stub
		OrderPlaceResponse orderPlaceResponse = new OrderPlaceResponse();
		try {
		Client client = Client.create();
		client.addFilter(new LoggingFilter(System.out));
		JSONObject payload = WoohooUtil.buildOrderRequest(request);
		WebResource webResource = client.resource(
				UrlMetadatas.processTransaction(Version.VERSION_1, Role.ClIENT, Device.ANDROID, Language.ENGLISH));
		ClientResponse response = webResource.accept("application/json").type("application/json")
				.header("clientIp", Inet4Address.getLocalHost().getHostAddress()).header("cllientKey", UrlMetadatas.getMdexClientKey())
				.header("clientToken", UrlMetadatas.getMdexClientToken()).header("clientApiName", "Category Search")
				.post(ClientResponse.class, payload);

		String strResponse = response.getEntity(String.class);
		System.err.println("the response is::" + strResponse);
		
			JSONObject jsonObject = new JSONObject(strResponse);
			String message = jsonObject.getString("message");
			boolean success = jsonObject.getBoolean("success");
			int code = jsonObject.getInt("code");
			if (response.getStatus() != 200) {
				orderPlaceResponse.setMessage(message);
				orderPlaceResponse.setSuccess(success);
				orderPlaceResponse.setCode(String.valueOf(code));
			} else {
				if (success) {
					JSONObject data = jsonObject.getJSONObject("data");
					String order_status = data.getString("status");
					String order_id = data.getString("order_id");
					String transactionRefNo = data.getString("transactionRefNo");
					String terms = data.getString("termsConditions");
					JSONObject carddetails = data.getJSONObject("carddetails");
					if (carddetails != null) {
						Iterator<String> iterator = carddetails.keys();
						while (iterator.hasNext()) {
							String key = iterator.next();
							orderPlaceResponse.setCardName(key);
							JSONArray jsonArray = carddetails.getJSONArray(key);
							for (int i = 0; i < jsonArray.length(); i++) {
								JSONObject card = jsonArray.getJSONObject(i);
								orderPlaceResponse.setExpiry_date(card.getString("expiry_date"));
								orderPlaceResponse.setCard_price(card.getString("card_price"));
								orderPlaceResponse.setCardnumber(card.getString("cardnumber"));
								orderPlaceResponse.setPin_or_url(card.getString("pin_or_url"));
							}
						}
					}
					orderPlaceResponse.setCode(String.valueOf(code));
					orderPlaceResponse.setMessage(message);
					orderPlaceResponse.setOrder_status(order_status);
					orderPlaceResponse.setSuccess(success);
					orderPlaceResponse.setStatus(success);
					orderPlaceResponse.setOrderNumber(order_id);
					orderPlaceResponse.setRetrivalRefNo(transactionRefNo);
					orderPlaceResponse.setTermsAndConditions(terms);
					orderPlaceResponse.setResponseCode("00");
				} else if ((!success)
						&& (jsonObject.getJSONObject("data").getString("status").equalsIgnoreCase("Processing"))) {
					JSONObject data = jsonObject.getJSONObject("data");
					String order_status = data.getString("status");
					String order_id = data.getString("order_id");
					String transactionRefNo = data.getString("transactionRefNo");
					orderPlaceResponse.setCode(String.valueOf(code));
					orderPlaceResponse.setMessage(message);
					orderPlaceResponse.setOrder_status(order_status);
					orderPlaceResponse.setSuccess(success);
					orderPlaceResponse.setStatus(success);
					orderPlaceResponse.setOrderNumber(order_id);
					orderPlaceResponse.setRetrivalRefNo(transactionRefNo);
					orderPlaceResponse.setResponseCode("11");
				} else {
					orderPlaceResponse.setCode("F00");
					orderPlaceResponse.setMessage(message);
					orderPlaceResponse.setSuccess(false);
					orderPlaceResponse.setStatus(success);
				}
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			orderPlaceResponse.setCode("F00");
			orderPlaceResponse.setMessage("opps!! Technical Problem , Please Try After Sometime.");
			orderPlaceResponse.setSuccess(false);

		} catch (UniformInterfaceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientHandlerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return orderPlaceResponse;
	}

	@Override
	public OrderPlaceResponse initiatePayment(PlaceOrderRequest order) {
		OrderPlaceResponse response = new OrderPlaceResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", order.getSessionId());
			payload.put("firstname", order.getFirstname());
			payload.put("lastname", order.getLastname());
			payload.put("email", order.getEmail());
			payload.put("telephone", order.getTelephone());
			payload.put("price", order.getPrice());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.initiatePayment(Version.VERSION_1, Role.USER, Device.ANDROID, Language.ENGLISH));
			ClientResponse respons = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = respons.getEntity(String.class);
			JSONObject jobj = new JSONObject(strResponse);
			final String code = (String) jobj.get("code");
			if (respons.getStatus() != 200) {
				response.setCode("F00");
				response.setMessage(jobj.getString("message"));
				response.setTransactionRefNo(jobj.getString("transactionRefNo"));
				response.setStatus(false);
			} else {
				if (strResponse != null) {
					if (jobj != null) {
						if (code.equalsIgnoreCase("S00")) {
							response.setCode(code);
							response.setMessage(jobj.getString("message"));
						} else {
							response.setCode(code);
							response.setMessage(jobj.getString("message"));
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setCode(ResponseStatus.FAILURE.getKey());
			response.setMessage(ResponseStatus.FAILURE.getValue());
		}
		return response;
	}
	
	@Override
	public OrderPlaceResponse processOrder(PlaceOrderRequest order) {
		OrderPlaceResponse response = new OrderPlaceResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", order.getSessionId());
			payload.put("firstname", order.getFirstname());
			payload.put("lastname", order.getLastname());
			payload.put("email", order.getEmail());
			payload.put("telephone", order.getTelephone());
			payload.put("price", order.getPrice());
			payload.put("qty", order.getQty());
			payload.put("product_id", order.getProduct_id());
			payload.put("postcode", order.getPostcode());
			payload.put("country_id", order.getCountry_id());
			payload.put("region", order.getRegion());
			payload.put("city", order.getCity());
			payload.put("line_1", order.getLine_1());
			payload.put("line_2", order.getLine_2());
			payload.put("brandCode", order.getBrandCode());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(
					UrlMetadatas.processOrder(Version.VERSION_1, Role.USER, Device.ANDROID, Language.ENGLISH));
			ClientResponse respons = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = respons.getEntity(String.class);
			JSONObject jobj = new JSONObject(strResponse);
			final String code = (String) jobj.get("code");
			if (respons.getStatus() != 200) {
				response.setCode("F00");
				response.setMessage(jobj.getString("message"));
				response.setStatus(false);
			} else {
				if (strResponse != null) {
					if (jobj != null) {
						if (code.equalsIgnoreCase("S00")) {
							response.setSuccess(true);
							response.setCode(code);
							response.setOrder_status(jobj.getString("status"));
							response.setMessage(jobj.getString("message"));
							response.setOrderNumber(jobj.getString("details"));
							response.setTransactionRefNo(jobj.getString("transactionRefNo"));
							response.setRemainingBalance(jobj.getDouble("balance"));
						} else {
							response.setSuccess(false);
							response.setCode(code);
							response.setMessage(jobj.getString("message"));
							response.setOrder_status(jobj.getString("status"));
							response.setRemainingBalance(jobj.getDouble("balance"));
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setCode(ResponseStatus.FAILURE.getKey());
			response.setMessage(ResponseStatus.FAILURE.getValue());
		}
		return response;
	}

	@Override
	public OrderPlaceResponse processResponse(OrderPlaceResponse order) {
		OrderPlaceResponse respo = new OrderPlaceResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", order.getSessionId());
			payload.put("status", order.isStatus());
			payload.put("cardName", order.getCardName());
			payload.put("expiry_date", order.getExpiry_date());
			payload.put("cardnumber", order.getCardnumber());
			payload.put("card_price", order.getCard_price());
			payload.put("pin_or_url", order.getPin_or_url());
			payload.put("orderId", order.getOrderNumber());
			payload.put("transactionRefNo", order.getTransactionRefNo());
			payload.put("retrivalRefNo", order.getRetrivalRefNo());
			payload.put("termsAndConditions", order.getTermsAndConditions());
			payload.put("responseCode", order.getResponseCode());
			System.err.println("Request" + payload.toString());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.processPayment(Version.VERSION_1, Role.USER, Device.ANDROID, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			System.err.println("Payment Response" + strResponse);
			if (response.getStatus() != 200) {
				respo.setCode(ResponseStatus.FAILURE.getKey());
				respo.setMessage(ResponseStatus.FAILURE.getValue());
				respo.setStatus(false);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						respo.setCode(code);
						respo.setMessage(JSONParserUtil.getString(jobj, "message"));
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			respo.setCode(ResponseStatus.FAILURE.getKey());
			respo.setMessage(ResponseStatus.FAILURE.getValue());
		}
		return respo;
	}

	@Override
	public CategoryProductResponse getProductByCate(SessionDTO dto) {
		CategoryProductResponse productResponse = new CategoryProductResponse();
		try {
		JSONObject payload= new JSONObject();
		payload.put("sessinId", dto.getSessionId());
		Client client = Client.create();
		WebResource webResource = client.resource(
				UrlMetadatas.getProductsList(Version.VERSION_1, Role.ClIENT, Device.ANDROID, Language.ENGLISH));
		ClientResponse response;
			response = webResource.accept("application/json").type("application/json")
					.header("clientIp", Inet4Address.getLocalHost().getHostAddress()).header("cllientKey", UrlMetadatas.getMdexClientKey())
					.header("clientToken", UrlMetadatas.getMdexClientToken()).header("clientApiName", "Category Search")
					.get(ClientResponse.class);
		String strResponse = response.getEntity(String.class);
			JSONObject jObject = new JSONObject(strResponse);
			String message = jObject.getString("message");
			boolean success = jObject.getBoolean("success");
			int code = jObject.getInt("code");
			if (response.getStatus() != 200) {
				productResponse.setMessage(message);
				productResponse.setSuccess(success);
				productResponse.setCode(String.valueOf(code));
			} else {
				if (success) {
					JSONArray jArray = jObject.getJSONArray("data");
					if (jArray != null) {
						List<Product> products = new ArrayList<>();
						for (int i = 0; i < jArray.length(); i++) {
							JSONObject jsonObject = jArray.getJSONObject(i);
							String productId = jsonObject.getString("id");
							String product_name = jsonObject.getString("name");
							String image_url = jsonObject.getString("image_url");
							String temp_url = jsonObject.getString("temp_url");
							String brandCode=jsonObject.getString("brandCode");
							Product product = new Product();
							product.setBrand_name(product_name);
							product.setProduct_id(productId);
							product.setImageUrl(image_url);
							product.setTempUrl(temp_url);
							product.setMax_custom_price(jsonObject.getString("max_custom_price"));
							product.setMin_custom_price(jsonObject.getString("min_custom_price"));
							product.setCustom_denominations(jsonObject.getString("custom_denominations"));
							product.setTermsAndAcondition(jsonObject.getString("termsAndConditions"));
							product.setBrandCode(brandCode);
							products.add(product);
						}
						productResponse.setProduct(products);
						productResponse.setCode("S00");
						productResponse.setSuccess(true);
					}

				} else {
					productResponse.setCode("F00");
					productResponse.setMessage(message);
					productResponse.setSuccess(false);

				}
			}

		} catch (UniformInterfaceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientHandlerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (JSONException e) {
		// TODO Auto-generated catch block
		productResponse.setCode("F00");
		productResponse.setMessage("opps!! Technical Problem , Please Try After Sometime.");
		productResponse.setSuccess(false);
		e.printStackTrace();
	}
		
		return productResponse;
	}

	@Override
	public OrderPlaceResponse getPendingStatus(String referenceNo) {
		CategoryProductResponse productResponse = new CategoryProductResponse();
		OrderPlaceResponse orderPlaceResponse = new OrderPlaceResponse();
		try {
		Client client = Client.create();
		client.addFilter(new LoggingFilter(System.out));
		WebResource webResource = client.resource(UrlMetadatas.getStatusCheck(Version.VERSION_1, Role.ClIENT,
				Device.ANDROID, Language.ENGLISH, referenceNo));
		ClientResponse response = webResource.accept("application/json").type("application/json")
				.header("clientIp", Inet4Address.getLocalHost().getHostAddress()).header("cllientKey", UrlMetadatas.getMdexClientKey())
				.header("clientToken", UrlMetadatas.getMdexClientToken()).header("clientApiName", "Category Search")
				.get(ClientResponse.class);
		String strResponse = response.getEntity(String.class);
		
			JSONObject jObject = new JSONObject(strResponse);
			String message = jObject.getString("message");
			boolean success = jObject.getBoolean("success");
			int code = jObject.getInt("code");
			if (response.getStatus() != 200) {
				productResponse.setMessage(message);
				productResponse.setSuccess(success);
				productResponse.setCode(String.valueOf(code));
			} else {
				if ((success) && (jObject.getJSONObject("data").getString("status").equalsIgnoreCase("complete"))) {
					Client client1 = Client.create();
					WebResource webResource1 = client1.resource(UrlMetadatas.getResend(Version.VERSION_1, Role.ClIENT,
							Device.ANDROID, Language.ENGLISH, referenceNo));
					ClientResponse response1 = webResource1.accept("application/json").type("application/json")
							.header("clientIp", Inet4Address.getLocalHost().getHostAddress()).header("cllientKey", UrlMetadatas.getMdexClientKey())
							.header("clientToken", UrlMetadatas.getMdexClientToken())
							.header("clientApiName", "Category Search").get(ClientResponse.class);
					String strResponse1 = response1.getEntity(String.class);
					try {
						JSONObject jsonObject = new JSONObject(strResponse1);
						String message1 = jsonObject.getString("message");
						boolean success1 = jsonObject.getBoolean("success");
						int code1 = jsonObject.getInt("code");
						if (response.getStatus() != 200) {
							orderPlaceResponse.setMessage(message1);
							orderPlaceResponse.setSuccess(success1);
							orderPlaceResponse.setCode(String.valueOf(code1));
						} else {
							if (success1) {
								JSONObject data = jsonObject.getJSONObject("data");
								String order_status = data.getString("status");
								String order_id = data.getString("order_id");
								String transactionRefNo = data.getString("transactionRefNo");
								String terms = data.getString("termsConditions");
								JSONObject carddetails = data.getJSONObject("carddetails");
								if (carddetails != null) {
									Iterator<String> iterator = carddetails.keys();
									while (iterator.hasNext()) {
										String key = iterator.next();
										orderPlaceResponse.setCardName(key);
										JSONArray jsonArray = carddetails.getJSONArray(key);
										for (int i = 0; i < jsonArray.length(); i++) {
											JSONObject card = jsonArray.getJSONObject(i);
											orderPlaceResponse.setExpiry_date(card.getString("expiry_date"));
											orderPlaceResponse.setCard_price(card.getString("card_price"));
											orderPlaceResponse.setCardnumber(card.getString("cardnumber"));
											orderPlaceResponse.setPin_or_url(card.getString("pin_or_url"));
										}
									}
								}
								orderPlaceResponse.setCode(String.valueOf(code1));
								orderPlaceResponse.setMessage(message1);
								orderPlaceResponse.setOrder_status(order_status);
								orderPlaceResponse.setSuccess(success1);
								orderPlaceResponse.setStatus(success1);
								orderPlaceResponse.setOrderNumber(order_id);
								orderPlaceResponse.setRetrivalRefNo(transactionRefNo);
								orderPlaceResponse.setTermsAndConditions(terms);
								orderPlaceResponse.setResponseCode("00");
								return orderPlaceResponse;
							} else {
								orderPlaceResponse.setCode("F00");
								orderPlaceResponse.setMessage(message1);
								orderPlaceResponse.setSuccess(false);
								orderPlaceResponse.setStatus(success1);
							}
						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						orderPlaceResponse.setCode("F00");
						productResponse.setMessage("opps!! Technical Problem , Please Try After Sometime.");
						orderPlaceResponse.setSuccess(false);
					}
				} else {
					String pendingResponse=strResponse;
					orderPlaceResponse.setPendingResponse(pendingResponse);
					return orderPlaceResponse;
				}
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			orderPlaceResponse.setCode("F00");
			productResponse.setMessage("opps!! Technical Problem , Please Try After Sometime.");
			orderPlaceResponse.setSuccess(false);
			orderPlaceResponse.setStatus(false);
			e.printStackTrace();
		} catch (UniformInterfaceException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (ClientHandlerException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (UnknownHostException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return orderPlaceResponse;

	}

}
