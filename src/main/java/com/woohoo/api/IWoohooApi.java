package com.woohoo.api;

import com.payqwikweb.app.model.request.SessionDTO;
import com.woohoo.model.request.CategoryResponse;
import com.woohoo.model.request.OrderHandlingRequest;
import com.woohoo.model.request.PlaceOrderRequest;
import com.woohoo.model.response.CategoryProductResponse;
import com.woohoo.model.response.OrderHandlingResponse;
import com.woohoo.model.response.OrderPlaceResponse;
import com.woohoo.model.response.ProductDescriptionResponse;

public interface IWoohooApi {

	CategoryResponse getCategories();
	CategoryProductResponse getProductByCategory(String categoryId);
	ProductDescriptionResponse getByProductId(String productId);
	OrderHandlingResponse checkOrders(OrderHandlingRequest request);
	OrderPlaceResponse placeOrder(PlaceOrderRequest request);
	OrderPlaceResponse processResponse(OrderPlaceResponse response);
	OrderPlaceResponse initiatePayment(PlaceOrderRequest order);
	OrderPlaceResponse getPendingStatus(String referenceNo);
	CategoryProductResponse getProductByCate(SessionDTO dto);
	OrderPlaceResponse processOrder(PlaceOrderRequest order);
	
}
