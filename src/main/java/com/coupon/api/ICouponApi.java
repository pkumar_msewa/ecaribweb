package com.coupon.api;

import com.payqwikweb.model.web.CarrotFryDTO;

public interface ICouponApi {
	void sendCoupons(String email);
	void sendCarrotFryCoupons(CarrotFryDTO dto);
}
