package com.coupon.api.impl;

import com.coupon.api.ICouponApi;
import com.payqwikweb.app.metadatas.UrlMetadatas;
import com.payqwikweb.model.web.CarrotFryDTO;
import com.payqwikweb.util.CommonUtil;
import com.payqwikweb.util.LogCat;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class CouponApi implements ICouponApi {

	@Override
	public void sendCoupons(String email) {
		Client client = Client.create();
		WebResource webResource = client.resource(UrlMetadatas.COUPONS + "?emailID=" + email);
		webResource.get(ClientResponse.class);
	}

	@Override
	public void sendCarrotFryCoupons(CarrotFryDTO dto) {
		Client client = Client.create();
		WebResource webResource = client.resource(UrlMetadatas.getCarrotfryCouponsURL())
				.queryParam("email",dto.getEmail())
				.queryParam("amount",dto.getAmount())
				.queryParam("mobile",dto.getMobileNo())
				.queryParam("city", CommonUtil.generateRandomCity());
		webResource.get(ClientResponse.class);
	}

}
