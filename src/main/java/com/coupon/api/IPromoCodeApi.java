package com.coupon.api;

import com.payqwikweb.app.model.request.PromoCodeRequest;
import com.payqwikweb.app.model.request.SessionDTO;
import com.payqwikweb.app.model.response.PromoCodeResponse;

public interface IPromoCodeApi {
	PromoCodeResponse generateRequest(PromoCodeRequest request);
	PromoCodeResponse listPromo(SessionDTO session);
	
	PromoCodeResponse generateRequestOfSuperAdmin(PromoCodeRequest request);
	PromoCodeResponse listPromoOfSuperAdmin(SessionDTO session);
	PromoCodeResponse editRequest(PromoCodeRequest request);
}
