package com.gci.controller;

import java.net.Inet4Address;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.gci.model.request.GCIAddOrderDTO;
import com.gci.model.response.AddOrderResponse;
import com.gci.util.GciUtil;
import com.gcm.model.GCIOrderStatusDTO;
import com.payqwikweb.app.metadatas.UrlMetadatas;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Language;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.Version;
import com.payqwikweb.util.JSONParserUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import com.woohoo.model.request.GiftCardStatus;
import com.woohoo.model.request.WoohooOrderRequest;
import com.woohoo.model.response.OrderPlaceResponse;
import com.woohoo.util.WoohooUtil;

@Controller
@RequestMapping("/GciOrder")
public class AddOrderController implements MessageSourceAware {

	private MessageSource messageSource;

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	private DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
	private final SimpleDateFormat dateOnly = new SimpleDateFormat("dd/MM/yyyy");

	@RequestMapping(value = "/Transaction/Process", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<AddOrderResponse> processTransaction(GCIAddOrderDTO request) {
		AddOrderResponse response = new AddOrderResponse();
		try {
			String stringResponse = "";
			JSONObject jo = new JSONObject();
			jo.put("brandHash", request.getBrand());
			jo.put("denomination", request.getAmount());
			jo.put("productType", request.getProduct());
			jo.put("quantity", "1");
			JSONArray ja = new JSONArray();
			ja.put(jo);
			MultivaluedMapImpl formData = new MultivaluedMapImpl();
			formData.add("orderDate", request.getOrderDate());
			formData.add("billingName", request.getBillingName());
			formData.add("billingEmail", request.getBillingEmail());
			formData.add("billingAddressLine1", request.getBillingAddressLine1());
			formData.add("billingCity", request.getBillingCity());
			formData.add("billingState", request.getBillingState());
			formData.add("billingCountry", request.getBillingCountry());
			formData.add("billingZip", request.getBillingZip());
			formData.add("receiversName", request.getReceiversName());
			formData.add("receiversEmail", request.getReceiversEmail());
			formData.add("shippingName", request.getBillingName());
			formData.add("shippingEmail", request.getBillingEmail());
			formData.add("shippingCity", request.getBillingCity());
			formData.add("shippingState", request.getBillingState());
			formData.add("shippingCountry", request.getBillingCountry());
			formData.add("shippingAddressLine1", request.getBillingAddressLine1());
			formData.add("shippingZip", request.getBillingZip());
			formData.add("clientOrderId", request.getClientOrderId());
			formData.add("paymentMode", "NEFT");
			formData.add("paymentStatus", "Payment Received");
			formData.add("products", ja);
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(GciUtil.PROCESS_PAYMENT).queryParam("accessToken",
					request.getToken());
			ClientResponse clientResponse = webResource.post(ClientResponse.class, formData);
			stringResponse = clientResponse.getEntity(String.class);
			org.json.JSONObject o = new org.json.JSONObject(stringResponse);
			String receiptNo = JSONParserUtil.getString(o, "receiptNo");
			// get vouncher
			if (!o.has("error")) {
				Client client1 = Client.create();
				WebResource webResource1 = client1.resource(GciUtil.ORDER_PROCESS + receiptNo).queryParam("accessToken",
						request.getToken());
				ClientResponse clientResponse1 = webResource1.get(ClientResponse.class);
				stringResponse = clientResponse1.getEntity(String.class);
				if (clientResponse1.getStatus() == 200) {
					System.err.println("response ::" + stringResponse);
					org.json.JSONArray array = new org.json.JSONArray(stringResponse);
					for (int i = 0; i < array.length(); i++) {
						org.json.JSONObject json = array.getJSONObject(i);
						response.setVoucherNumber(JSONParserUtil.getString(json, "voucherNumber"));
						response.setVoucherPin(JSONParserUtil.getString(json, "voucherPin"));
						response.setBrandName(JSONParserUtil.getString(json, "brandName"));
						String date = JSONParserUtil.getString(json, "expiryDate");
						Date parseDate = dateOnly.parse(date);
						String expiryDate = format.format(parseDate);
						response.setExpiryDate(expiryDate);
						response.setReceiptNo(receiptNo);
						response.setSuccess(true);
					}
					return new ResponseEntity<AddOrderResponse>(response, HttpStatus.OK);
				} else {
					response.setSuccess(false);
					return new ResponseEntity<AddOrderResponse>(response, HttpStatus.OK);
				}
			} else {
				response.setSuccess(false);
				response.setMessage(JSONParserUtil.getString(o, "message"));
				org.json.JSONArray errorArray=o.getJSONArray("errors");
				for (int i = 0; i < errorArray.length(); i++) {
					org.json.JSONObject json = errorArray.getJSONObject(i);
					response.setErrorCode(JSONParserUtil.getString(json, "code"));
				}
				return new ResponseEntity<AddOrderResponse>(response, HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<AddOrderResponse>(response, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/Voucher/Process", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<String> processVoucher(GCIOrderStatusDTO request) {
		try {
			String stringResponse = "";
			Client client = Client.create();
			WebResource webResource = client.resource(GciUtil.ORDER_PROCESS + request.getReceiptNo())
					.queryParam("accessToken", request.getToken());
			ClientResponse clientResponse = webResource.get(ClientResponse.class);
			stringResponse = clientResponse.getEntity(String.class);
			if (clientResponse.getStatus() == 200) {
				String response = stringResponse;
				return new ResponseEntity<String>(response, HttpStatus.OK);
			} else if (clientResponse.getStatus() == 403) {
				String badReseponse = stringResponse;
				return new ResponseEntity<String>(badReseponse, HttpStatus.OK);
			}
			return new ResponseEntity<String>("", HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>("", HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/Woohoo/Transaction/Process", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE },consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<String> placeOrder(@RequestBody WoohooOrderRequest request) {
		try {
			System.err.println("inside this controller");
			String stringResponse = "";
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			JSONObject payload = WoohooUtil.buildOrderRequestNew(request);
			WebResource webResource = client.resource(
					UrlMetadatas.processTransaction(Version.VERSION_1, Role.ClIENT, Device.ANDROID, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("clientIp", Inet4Address.getLocalHost().getHostAddress()).header("cllientKey", UrlMetadatas.getMdexClientKey())
					.header("clientToken", UrlMetadatas.getMdexClientToken()).header("clientApiName", "Category Search")
					.post(ClientResponse.class, payload);
			if (response.getStatus() == 200) {
				stringResponse = response.getEntity(String.class);
				
				return new ResponseEntity<String>(stringResponse, HttpStatus.OK);
			} else {
				return new ResponseEntity<String>("", HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>("", HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/Woohoo/PendingCards", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE },consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<OrderPlaceResponse> giftCardStatus(@RequestBody GiftCardStatus request) {
		OrderPlaceResponse orderPlaceResponse=new OrderPlaceResponse();
		try {
			String stringResponse = "";
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(
					UrlMetadatas.getStatusCheck(Version.VERSION_1, Role.ClIENT,Device.ANDROID, Language.ENGLISH, request.getReferenceNo()));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("clientIp", Inet4Address.getLocalHost().getHostAddress()).header("cllientKey", UrlMetadatas.getMdexClientKey())
					.header("clientToken", UrlMetadatas.getMdexClientToken()).header("clientApiName", "Category Search")
					.get(ClientResponse.class);
			if (response.getStatus() == 200) {
				stringResponse = response.getEntity(String.class);
				org.json.JSONObject o = new org.json.JSONObject(stringResponse);
				if(o!=null){
					org.json.JSONObject data= o.getJSONObject("data");
					if(data!=null){
						String status=data.getString("status");
						if(status.equalsIgnoreCase("complete")){
							Client client1 = Client.create();
							client.addFilter(new LoggingFilter(System.out));
							WebResource webResource1 = client1.resource(UrlMetadatas.getResend(Version.VERSION_1, Role.ClIENT,
									Device.ANDROID, Language.ENGLISH, request.getReferenceNo()));
							ClientResponse clientResponse1 = webResource1.accept("application/json").type("application/json")
									.header("clientIp", Inet4Address.getLocalHost().getHostAddress()).header("cllientKey", UrlMetadatas.getMdexClientKey())
									.header("clientToken", UrlMetadatas.getMdexClientToken())
									.header("clientApiName", "Category Search").get(ClientResponse.class);
							stringResponse = clientResponse1.getEntity(String.class);
							if (clientResponse1.getStatus() == 200) {
								JSONObject jsonObject = new JSONObject(stringResponse);
								String message = jsonObject.getString("message");
								boolean success = jsonObject.getBoolean("success");
								int code = jsonObject.getInt("code");
									if (success) {
										JSONObject compeleteData = jsonObject.getJSONObject("data");
										String order_status = compeleteData.getString("status");
										String order_id = compeleteData.getString("order_id");
										String transactionRefNo = compeleteData.getString("transactionRefNo");
										String terms = compeleteData.getString("termsConditions");
										JSONObject carddetails = compeleteData.getJSONObject("carddetails");
										if (carddetails != null) {
											Iterator<String> iterator = carddetails.keys();
											while (iterator.hasNext()) {
												String key = iterator.next();
												orderPlaceResponse.setCardName(key);
												JSONArray jsonArray = carddetails.getJSONArray(key);
												for (int i = 0; i < jsonArray.length(); i++) {
													JSONObject card = jsonArray.getJSONObject(i);
													orderPlaceResponse.setExpiry_date(card.getString("expiry_date"));
													orderPlaceResponse.setCard_price(card.getString("card_price"));
													orderPlaceResponse.setCardnumber(card.getString("cardnumber"));
													orderPlaceResponse.setPin_or_url(card.getString("pin_or_url"));
												}
											}
										}
										orderPlaceResponse.setCode(String.valueOf(code));
										orderPlaceResponse.setMessage(message);
										orderPlaceResponse.setOrder_status(order_status);
										orderPlaceResponse.setSuccess(success);
										orderPlaceResponse.setStatus(success);
										orderPlaceResponse.setOrderNumber(order_id);
										orderPlaceResponse.setRetrivalRefNo(transactionRefNo);
										orderPlaceResponse.setTermsAndConditions(terms);
										orderPlaceResponse.setResponseCode("00");
										return new ResponseEntity<OrderPlaceResponse>(orderPlaceResponse, HttpStatus.OK);
								  }
							} else {
								return new ResponseEntity<OrderPlaceResponse>(orderPlaceResponse, HttpStatus.OK);
							}
						}else{
							orderPlaceResponse.setSuccess(false);
							org.json.JSONObject pendingData= o.getJSONObject("data");
							if(pendingData!=null){
							String pendingStatus=pendingData.getString("status");
							orderPlaceResponse.setOrder_status(pendingStatus);
							return new ResponseEntity<OrderPlaceResponse>(orderPlaceResponse, HttpStatus.OK);
							}
							
						}
					}
				}
			} else {
				return new ResponseEntity<OrderPlaceResponse>(orderPlaceResponse, HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<OrderPlaceResponse>(orderPlaceResponse, HttpStatus.OK);
		}
		return new ResponseEntity<OrderPlaceResponse>(orderPlaceResponse, HttpStatus.OK);
	}

}
