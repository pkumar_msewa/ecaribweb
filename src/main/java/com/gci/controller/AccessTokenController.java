package com.gci.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.gci.api.ILoginApi;
import com.gci.model.request.LoginDTO;
import com.gci.model.response.LoginResponse;
import com.payqwikweb.util.SecurityUtil;

@Controller
@RequestMapping("/Gci")
public class AccessTokenController {

	    private final ILoginApi loginApi;

	    public AccessTokenController(ILoginApi loginApi) {
	        this.loginApi = loginApi;
	    }

	    @RequestMapping(value="/AccessToken",method= RequestMethod.POST,produces = MediaType.APPLICATION_JSON_VALUE,consumes = MediaType.APPLICATION_JSON_VALUE)
	    ResponseEntity<LoginResponse> createAccessToken(@RequestHeader(value="x-api-key") String apiKey,@RequestBody LoginDTO dto, HttpServletRequest request, HttpServletResponse response) {
	        LoginResponse result = new LoginResponse();
	        if(SecurityUtil.isValidAPIKey(apiKey)) {
	            result = loginApi.GetAccessToken(dto);
	        } else {
	            result.setSuccess(false);
	            result.setMessage("Not a valid API KEY");
	        }
	        return new ResponseEntity<LoginResponse>(result, HttpStatus.OK);
	    }

}
