package com.gci.model.response;

import java.util.List;

import com.gci.model.request.GCIDenominationsDTO;

public class DenominationResponse extends BaseResponse{
	
	private List<GCIDenominationsDTO> denominationObject;

	public List<GCIDenominationsDTO> getDenominationObject() {
		return denominationObject;
	}

	public void setDenominationObject(List<GCIDenominationsDTO> denominationObject) {
		this.denominationObject = denominationObject;
	}
	
	

}
