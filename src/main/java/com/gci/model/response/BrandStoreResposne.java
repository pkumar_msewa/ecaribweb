package com.gci.model.response;

import java.util.List;

import com.gci.model.request.GCIBrandStoreDTO;

public class BrandStoreResposne extends BaseResponse {
	
	private List<GCIBrandStoreDTO> brandStore;

	public List<GCIBrandStoreDTO> getBrandStore() {
		return brandStore;
	}

	public void setBrandStore(List<GCIBrandStoreDTO> brandStore) {
		this.brandStore = brandStore;
	}


}
