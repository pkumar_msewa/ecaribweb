package com.gci.model.response;

import java.util.List;

import com.gci.model.request.GCIBrandsDTO;

public class BrandResponse extends BaseResponse {

    private List<GCIBrandsDTO> brandObject;
	
	public List<GCIBrandsDTO> getBrandObject() {
		return brandObject;
	}

	public void setBrandObject(List<GCIBrandsDTO> brandObject) {
		this.brandObject = brandObject;
	}
}
