package com.gci.model.response;

public class LoginResponse  extends BaseResponse{

	private String token;
	private String expiryTime;
	
	public String getExpiryTime() {
		return expiryTime;
	}

	public void setExpiryTime(String expiryTime) {
		this.expiryTime = expiryTime;
	}
	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

}
