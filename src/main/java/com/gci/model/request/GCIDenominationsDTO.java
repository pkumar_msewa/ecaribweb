package com.gci.model.request;

public class GCIDenominationsDTO {

	private String denominationId;
	private String denominationName;
	private String skuId;
	private String type;
	private String valueType;

	public String getDenominationId() {
		return denominationId;
	}

	public void setDenominationId(String denominationId) {
		this.denominationId = denominationId;
	}

	public String getDenominationName() {
		return denominationName;
	}

	public void setDenominationName(String denominationName) {
		this.denominationName = denominationName;
	}

	public String getSkuId() {
		return skuId;
	}

	public void setSkuId(String skuId) {
		this.skuId = skuId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getValueType() {
		return valueType;
	}

	public void setValueType(String valueType) {
		this.valueType = valueType;
	}

}
