package com.gci.model.request;

public class GCIAddOrderDTO {

	private String orderDate;
	private String billingName;
	private String billingEmail;
	private String billingAddressLine1;
	private String billingCity;
	private String billingCountry;
	private String billingState;
	private String billingZip;
	private String receiversName;
	private String receiversEmail;
	private String shippingName;
	private String shippingEmail;
	private String shippingCity;
	private String shippingState;
	private String shippingCountry;
	private String shippingAddressLine1;
	private String shippingZip;
	private String clientOrderId;
	private String products;
	private String brandHash;
	private String denomination;
	private String productType;
	private String quantity;
	private String token;
	private String sessionId;
	private String brandName;
	private String amount;
	private String skuId;
	private String brand;
	private String product;

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getBillingZip() {
		return billingZip;
	}

	public void setBillingZip(String billingZip) {
		this.billingZip = billingZip;
	}

	public String getSkuId() {
		return skuId;
	}

	public void setSkuId(String skuId) {
		this.skuId = skuId;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}

	public String getBillingName() {
		return billingName;
	}

	public void setBillingName(String billingName) {
		this.billingName = billingName;
	}

	public String getBillingEmail() {
		return billingEmail;
	}

	public void setBillingEmail(String billingEmail) {
		this.billingEmail = billingEmail;
	}

	public String getBillingAddressLine1() {
		return billingAddressLine1;
	}

	public void setBillingAddressLine1(String billingAddressLine1) {
		this.billingAddressLine1 = billingAddressLine1;
	}

	public String getBillingCity() {
		return billingCity;
	}

	public void setBillingCity(String billingCity) {
		this.billingCity = billingCity;
	}

	public String getBillingCountry() {
		return billingCountry;
	}

	public void setBillingCountry(String billingCountry) {
		this.billingCountry = billingCountry;
	}

	public String getBillingState() {
		return billingState;
	}

	public void setBillingState(String billingState) {
		this.billingState = billingState;
	}

	public String getReceiversName() {
		return receiversName;
	}

	public void setReceiversName(String receiversName) {
		this.receiversName = receiversName;
	}

	public String getReceiversEmail() {
		return receiversEmail;
	}

	public void setReceiversEmail(String receiversEmail) {
		this.receiversEmail = receiversEmail;
	}

	public String getShippingName() {
		return shippingName;
	}

	public void setShippingName(String shippingName) {
		this.shippingName = shippingName;
	}

	public String getShippingEmail() {
		return shippingEmail;
	}

	public void setShippingEmail(String shippingEmail) {
		this.shippingEmail = shippingEmail;
	}

	public String getShippingCity() {
		return shippingCity;
	}

	public void setShippingCity(String shippingCity) {
		this.shippingCity = shippingCity;
	}

	public String getShippingState() {
		return shippingState;
	}

	public void setShippingState(String shippingState) {
		this.shippingState = shippingState;
	}

	public String getShippingCountry() {
		return shippingCountry;
	}

	public void setShippingCountry(String shippingCountry) {
		this.shippingCountry = shippingCountry;
	}

	public String getShippingAddressLine1() {
		return shippingAddressLine1;
	}

	public void setShippingAddressLine1(String shippingAddressLine1) {
		this.shippingAddressLine1 = shippingAddressLine1;
	}

	public String getShippingZip() {
		return shippingZip;
	}

	public void setShippingZip(String shippingZip) {
		this.shippingZip = shippingZip;
	}

	public String getClientOrderId() {
		return clientOrderId;
	}

	public void setClientOrderId(String clientOrderId) {
		this.clientOrderId = clientOrderId;
	}

	public String getProducts() {
		return products;
	}

	public void setProducts(String products) {
		this.products = products;
	}

	public String getBrandHash() {
		return brandHash;
	}

	public void setBrandHash(String brandHash) {
		this.brandHash = brandHash;
	}

	public String getDenomination() {
		return denomination;
	}

	public void setDenomination(String denomination) {
		this.denomination = denomination;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	@Override
	public String toString() {
		return "GCIAddOrderDTO [orderDate=" + orderDate + ", billingName=" + billingName + ", billingEmail="
				+ billingEmail + ", billingAddressLine1=" + billingAddressLine1 + ", billingCity=" + billingCity
				+ ", billingCountry=" + billingCountry + ", billingState=" + billingState + ", billingZip=" + billingZip
				+ ", receiversName=" + receiversName + ", receiversEmail=" + receiversEmail + ", shippingName="
				+ shippingName + ", shippingEmail=" + shippingEmail + ", shippingCity=" + shippingCity
				+ ", shippingState=" + shippingState + ", shippingCountry=" + shippingCountry
				+ ", shippingAddressLine1=" + shippingAddressLine1 + ", shippingZip=" + shippingZip + ", clientOrderId="
				+ clientOrderId + ", products=" + products + ", brandHash=" + brandHash + ", denomination="
				+ denomination + ", productType=" + productType + ", quantity=" + quantity + ", token=" + token
				+ ", sessionId=" + sessionId + ", brandName=" + brandName + ", amount=" + amount + ", skuId=" + skuId
				+ ", brand=" + brand + ", product=" + product + "]";
	}

}
