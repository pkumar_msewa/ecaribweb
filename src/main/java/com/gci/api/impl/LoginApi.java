package com.gci.api.impl;

import javax.ws.rs.core.MediaType;

import org.json.JSONObject;

import com.gci.api.ILoginApi;
import com.gci.model.request.LoginDTO;
import com.gci.model.response.LoginResponse;
import com.gci.util.GciUtil;
import com.payqwikweb.util.JSONParserUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;

public class LoginApi implements ILoginApi{
	
	 @Override
	    public LoginResponse GetAccessToken(LoginDTO dto) {
	        LoginResponse result = new LoginResponse();
	        org.codehaus.jettison.json.JSONObject payload= new org.codehaus.jettison.json.JSONObject();
	        try {
	        	payload.put("SessionId", dto.getSessionId());
	            Client client = Client.create();
	            WebResource webResource = client.resource(GciUtil.LOGIN_URL)
	            		.queryParam("id", GciUtil.ID_VALUE)
	            		.queryParam("key",GciUtil.KEY_VALUE);
	            ClientResponse response = webResource.accept(MediaType.APPLICATION_JSON).get(ClientResponse.class);
	            System.err.println("response ::" + response);
	            String strResponse = response.getEntity(String.class);
	            System.err.println(strResponse);
	            if(response.getStatus() == 403) {
	                JSONObject jsonObject = new JSONObject(strResponse);
	                if(jsonObject != null) {
	                	boolean error=(boolean)jsonObject.get("error");
	                	String message=jsonObject.getString("message");
	                	result.setSuccess(error);
	                    result.setMessage(message);
	                }
	            } else if(response.getStatus() == 200) {
	            	System.err.println("access token gci response ::" + strResponse);
	                JSONObject object = new JSONObject(strResponse);
	                result.setSuccess(true);
	                result.setMessage("Get Access Token");
	                if(object != null) {
	                    result.setToken(JSONParserUtil.getString(object,"accessToken"));
	                    result.setExpiryTime(JSONParserUtil.getString(object,"expiresAt"));
	                }
	            } else {
	                    result.setSuccess(false);
	                    result.setMessage("Response From Merchant  "+response.getStatus());
	            }
	        } catch (Exception e) {
	            e.printStackTrace();
	            result.setSuccess(false);
	            result.setMessage("Service Unavailable");
	        }
	        return result;
	    }

}
