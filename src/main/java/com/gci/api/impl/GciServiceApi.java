package com.gci.api.impl;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ws.rs.core.MediaType;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jettison.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;

import com.gci.api.IGciServiceApi;
import com.gci.model.request.BrandDenominationDTO;
import com.gci.model.request.BrandStoreDTO;
import com.gci.model.request.BrandsDTO;
import com.gci.model.request.GCIBrandStoreDTO;
import com.gci.model.request.GCIBrandsDTO;
import com.gci.model.request.GCIDenominationsDTO;
import com.gci.model.request.OrderDTO;
import com.gci.model.request.OrderStatusDTO;
import com.gci.model.response.AddOrderResponse;
import com.gci.model.response.BrandResponse;
import com.gci.model.response.BrandStoreResposne;
import com.gci.model.response.DenominationResponse;
import com.gci.model.response.OrderStatusResponse;
import com.gci.util.GciUtil;
import com.payqwikweb.app.metadatas.UrlMetadatas;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Language;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.Version;
import com.payqwikweb.app.utils.SecurityUtils;
import com.payqwikweb.model.app.response.GCIOrderVoucher;
import com.payqwikweb.model.app.response.GCIProceedResponse;
import com.payqwikweb.util.APIUtils;
import com.payqwikweb.util.JSONParserUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;

public class GciServiceApi implements IGciServiceApi {

	@Override
	public BrandResponse getGciBrands(BrandsDTO dto) {
		BrandResponse result=new BrandResponse();
		ObjectMapper mapper = new ObjectMapper();
        try {
            Client client = Client.create();
            client.addFilter(new LoggingFilter(System.out));
            WebResource webResource = client.resource(GciUtil.GCI_BRANDS)
            		.queryParam("accessToken", dto.getToken());
            ClientResponse resp = webResource.accept(MediaType.APPLICATION_JSON).get(ClientResponse.class);
            String strResponse = resp.getEntity(String.class);
            List<GCIBrandsDTO> brandResponse=new ArrayList<>();
            if(resp.getStatus() == 403) {
               org.json.JSONObject jsonObject = new org.json.JSONObject(strResponse);
                if(jsonObject != null) {
                	boolean error=(boolean)jsonObject.get("error");
                	String message=jsonObject.getString("message");
                	result.setSuccess(error);
                    result.setMessage(message);
                }
            } else if(resp.getStatus() == 200) {
            	result.setSuccess(true);
            	result.setMessage("Get All brnads of GiftCards");
                JSONArray array = new JSONArray(strResponse);
                if (array != null) {
                	for (int i = 0; i < array.length(); i++) {
                		org.json.JSONObject object = array.getJSONObject(i);
                        GCIBrandsDTO response=new GCIBrandsDTO();
                        response.setMessage("Get All brnads of GiftCards");
                        response.setName(JSONParserUtil.getString(object,"name")); 	
                        response.setHash(JSONParserUtil.getString(object,"hash"));
                        response.setImages(JSONParserUtil.getString(object,"image"));
                        response.setDescription(JSONParserUtil.getString(object,"description"));
                        response.setTerms(JSONParserUtil.getString(object,"terms"));
                        response.setWebsite(JSONParserUtil.getString(object,"website"));
                        response.setOnline(JSONParserUtil.getBoolean(object,"isOnline"));
                        response.setActive(JSONParserUtil.getBoolean(object,"isActive"));
                        response.setSuccess(true);
                        brandResponse.add(response);
                        result.setBrandObject(brandResponse);
                    	mapper.writeValueAsString(result);   
                    }
                }
            }else {
                result.setSuccess(false);
                result.setMessage("Response from Gci is "+resp.getStatus());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (JsonGenerationException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
	
	
	@Override
	public DenominationResponse getGciBrandDenominations(BrandDenominationDTO dto) {
		DenominationResponse result=new DenominationResponse();
		ObjectMapper mapper = new ObjectMapper();
        try {
            Client client = Client.create();
            client.addFilter(new LoggingFilter(System.out));
            WebResource webResource = client.resource(GciUtil.GCI_BRANDS_DENOMINATIONS)
            		.queryParam("accessToken", dto.getToken())
            		.queryParam("hash", dto.getHash());
            ClientResponse resp = webResource.accept(MediaType.APPLICATION_JSON).get(ClientResponse.class);
            String strResponse = resp.getEntity(String.class);
            List<GCIDenominationsDTO> denominationResponse=new ArrayList<>();
            if(resp.getStatus() == 403) {
            	org.json.JSONObject jsonObject = new org.json.JSONObject(strResponse);
                if(jsonObject != null) {
                	boolean error=(boolean)jsonObject.get("error");
                	String message=jsonObject.getString("message");
                	result.setSuccess(error);
                    result.setMessage(message);
                }
            } else if(resp.getStatus() == 200) {
            	result.setMessage("Get All Brand Denominations");
            	result.setSuccess(true);
            	org.json.JSONObject object= new org.json.JSONObject(strResponse);
                     if(object!=null){
                    	 JSONArray array=object.getJSONArray("denominations");
                    	 for (int i = 0; i < array.length(); i++) {
                    		 org.json.JSONObject jsonObject=array.getJSONObject(i);
                    		 GCIDenominationsDTO denominationsDTO=new GCIDenominationsDTO();
                    		 denominationsDTO.setDenominationId(JSONParserUtil.getString(jsonObject,"id"));
                    		 denominationsDTO.setDenominationName(JSONParserUtil.getString(jsonObject,"name"));
                    		 denominationsDTO.setSkuId(JSONParserUtil.getString(jsonObject,"skuId"));
                    		 denominationsDTO.setType(JSONParserUtil.getString(jsonObject,"type"));
                    		 denominationsDTO.setValueType(JSONParserUtil.getString(jsonObject,"valueType"));
                    		 denominationResponse.add(denominationsDTO);
                    		 result.setDenominationObject(denominationResponse);
                    		 mapper.writeValueAsString(result);
                    	 }
                     }
            }else {
                result.setSuccess(false);
                result.setMessage("Response from Gci is "+resp.getStatus());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (JsonGenerationException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
	
	@Override
	public BrandStoreResposne getGciBrandStores(BrandStoreDTO dto) {
		BrandStoreResposne result=new BrandStoreResposne();
		ObjectMapper mapper = new ObjectMapper();
        try {
            Client client = Client.create();
            client.addFilter(new LoggingFilter(System.out));
            WebResource webResource = client.resource(GciUtil.GCI_BRANDS_STORE)
            		.queryParam("accessToken", dto.getToken())
            		.queryParam("hash", dto.getHash());
            ClientResponse resp = webResource.accept(MediaType.APPLICATION_JSON).get(ClientResponse.class);
            String strResponse = resp.getEntity(String.class);
            List<GCIBrandStoreDTO> storeResponse=new ArrayList<>();
            if(resp.getStatus() == 403) {
            	org.json.JSONObject jsonObject = new org.json.JSONObject(strResponse);
                if(jsonObject != null) {
                	boolean error=(boolean)jsonObject.get("error");
                	String message=jsonObject.getString("message");
                	result.setSuccess(error);
                    result.setMessage(message);
                }
            } else if(resp.getStatus() == 200) {
            	result.setMessage("Get All Brand Stores");
            	result.setSuccess(true);
            	org.json.JSONObject object= new org.json.JSONObject(strResponse);
                     if(object!=null){
                    	 JSONArray array=object.getJSONArray("stores");
                    	 if(array.length()!=0){
                    	 for (int i = 0; i < array.length(); i++) {
                    		 org.json.JSONObject jsonObject=array.getJSONObject(i);
                    		 GCIBrandStoreDTO storeDTO=new GCIBrandStoreDTO();
                    		 storeDTO.setAddressLine1(JSONParserUtil.getString(jsonObject,"addressLine1"));
                    		 storeDTO.setAddressLine2(JSONParserUtil.getString(jsonObject,"addressLine2"));
                    		 storeDTO.setLocation(JSONParserUtil.getString(jsonObject,"location"));
                    		 storeDTO.setCity(JSONParserUtil.getString(jsonObject,"city"));
                    		 storeDTO.setState(JSONParserUtil.getString(jsonObject,"state"));
                    		 storeDTO.setCountry(JSONParserUtil.getString(jsonObject,"country"));
                    		 storeDTO.setPin(JSONParserUtil.getString(jsonObject,"pin"));
                    		 storeDTO.setLatitude(JSONParserUtil.getString(jsonObject,"latitude"));
                    		 storeDTO.setLongitude(JSONParserUtil.getString(jsonObject,"longitude"));
                    		 storeDTO.setVerified(JSONParserUtil.getBoolean(jsonObject,"verified"));
                    		 storeDTO.setHash(JSONParserUtil.getString(jsonObject,"hash"));
                    		 storeResponse.add(storeDTO);
                    		 result.setBrandStore(storeResponse);
                    		 mapper.writeValueAsString(result);
                    	 }
                    	 }else{
                    		 result.setSuccess(false);
                             result.setMessage("There is no store for this Brand.");
                    	 }
                     }
            }else {
                result.setSuccess(false);
                result.setMessage("Response from Gci is "+resp.getStatus());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (JsonGenerationException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

	@Override
	public AddOrderResponse getAddOrder(OrderDTO request) {
		AddOrderResponse resp=new AddOrderResponse();
		JSONObject payload = new JSONObject();
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		 Date date = new Date();
		String today=(dateFormat.format(date));
		 try {
		payload.put("sessionId", request.getSessionId());
		payload.put("amount", request.getDenomination());
		payload.put("brandName", request.getBrandName());
		payload.put("orderDate",today);
		payload.put("billingName", request.getBillingName());
		payload.put("billingEmail", request.getBillingEmail());
		payload.put("billingAddressLine1", request.getBillingAddressLine1());
		payload.put("billingCity", request.getBillingCity());
		payload.put("billingState", request.getBillingState());
		payload.put("billingCountry", request.getBillingCountry());
		payload.put("billingZip", request.getBillingZip());
		payload.put("receiversName", request.getReceiversName());
		payload.put("receiversEmail", request.getReceiversEmail());
		payload.put("receiversMobileNumber", request.getReceiversMobileNumber());
		payload.put("shippingName", request.getBillingName());
		payload.put("shippingEmail", request.getBillingEmail());
		payload.put("shippingCity", request.getBillingCity());
		payload.put("shippingState", request.getBillingState());
		payload.put("shippingCountry", request.getBillingCountry());
		payload.put("shippingAddressLine1", request.getBillingAddressLine1());
		payload.put("shippingZip", request.getBillingZip());
		payload.put("clientOrderId", request.getClientOrderId());
		payload.put("brandHash", request.getBrandHash());
		payload.put("productType", request.getProductType());
		payload.put("skuId", request.getSkuId());
		payload.put("token", request.getToken());
		
		Client client = Client.create();
		client.addFilter(new LoggingFilter(System.out));
		WebResource webResource = client.resource(
				UrlMetadatas.getGciCardInitiateURL(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
		ClientResponse response = webResource.accept("application/json").type("application/json")
				.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
		String strResponse = response.getEntity(String.class);
		if (response.getStatus() != 200) {
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage(strResponse);
			//resp.setDetails(details);
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		} else {
			if (strResponse != null) {
				org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
				if (jobj != null) {
					final String status = (String) jobj.get("status");
					final String code = (String) jobj.get("code");
					final String message = (String) jobj.get("message");
					if (code.equalsIgnoreCase("S00")) {
						resp.setSuccess(true);
					} else {
					final String details = (String) jobj.get("details");
					   resp.setDetails(details);
					   resp.setSuccess(false);
					}
					resp.setCode(code);
					resp.setStatus(status);
					resp.setMessage(message);
				}
			} else {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			}
		}
	} catch (Exception e) {
		e.printStackTrace();
		resp.setSuccess(false);
		resp.setCode("F00");
		resp.setMessage("Service unavailable");
		resp.setStatus("FAILED");
		resp.setResponse(APIUtils.getFailedJSON().toString());
	}
	return resp;
	}

	@Override
	public GCIProceedResponse sucees(GCIOrderVoucher request) {
		GCIProceedResponse resp = new GCIProceedResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessioniId", request.getSessioniId());
			payload.put("brandName", request.getBrandName());
			payload.put("amount", request.getAmount());
			payload.put("voucherNumber", request.getVoucherNumber());
			payload.put("voucherPin", request.getVoucherPin());
			payload.put("expiryDate", request.getExpiryDate());
			payload.put("receiptno", request.getReceiptno());
			payload.put("transactionRefNo", request.getTransactionRefNo());
			payload.put("code", request.getCode());
			
			
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.paymentsuccessURL(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.header("hash", SecurityUtils.getHash(payload.toString()))
					.post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						String txnId = jobj.getString("txnId");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							
							final String details = (String) jobj.get("details");
						     System.err.println("+++++++++++++++=======DETAILS:: From SUCCESS:::::::::======="+details);
						   resp.setDetails(details);
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setTxnId(txnId);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}
	
	
	@Override
	public List<OrderStatusResponse> getOrderStatus(OrderStatusDTO dto) {
		OrderStatusResponse result=new OrderStatusResponse();
		List<OrderStatusResponse> statusResponse=new ArrayList<>();
        try {
            Client client = Client.create();
            client.addFilter(new LoggingFilter(System.out));
            WebResource webResource = client.resource(GciUtil.ORDER_PROCESS+dto.getReceiptNo())
            		.queryParam("accessToken", dto.getToken());
            ClientResponse resp = webResource.accept(MediaType.APPLICATION_JSON).get(ClientResponse.class);
            String strResponse = resp.getEntity(String.class);
            if(resp.getStatus() == 403) {
               org.json.JSONObject jsonObject = new org.json.JSONObject(strResponse);
                if(jsonObject != null) {
                	boolean error=(boolean)jsonObject.get("error");
                	String message=jsonObject.getString("message");
                	result.setSuccess(error);
                    result.setMessage(message);
                }
            } else if(resp.getStatus() == 200) {
                	JSONArray array=new JSONArray(strResponse);
                	for (int i = 0; i < array.length(); i++) {
                		OrderStatusResponse response=new OrderStatusResponse();
                		org.json.JSONObject jsonObject = array.getJSONObject(i);
                		response.setMessage("Get All status of GiftCards");
                		response.setBrandName(JSONParserUtil.getString(jsonObject,"brandName"));
                		response.setVoucherPin(JSONParserUtil.getString(jsonObject,"voucherPin"));
                		response.setDenomination(JSONParserUtil.getString(jsonObject,"cardPrice"));
                        response.setVoucherNumber(JSONParserUtil.getString(jsonObject,"voucherNumber"));
                        response.setExpiryDate(JSONParserUtil.getString(jsonObject,"expiryDate"));
                        response.setDispatchStatus("Delivered");
                        response.setSuccess(true);
                        statusResponse.add(response);
                    }
            }else {
                result.setSuccess(false);
                result.setMessage("Response from Gci is "+resp.getStatus());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return statusResponse;
    }
}
