package com.gci.api;

import com.gci.model.request.LoginDTO;
import com.gci.model.response.LoginResponse;

public interface ILoginApi {
	

	LoginResponse GetAccessToken(LoginDTO dto);

}
