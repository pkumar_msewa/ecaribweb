package com.gci.api;

import java.util.List;

import com.gci.model.request.BrandDenominationDTO;
import com.gci.model.request.BrandStoreDTO;
import com.gci.model.request.BrandsDTO;
import com.gci.model.request.OrderDTO;
import com.gci.model.request.OrderStatusDTO;
import com.gci.model.response.AddOrderResponse;
import com.gci.model.response.BrandResponse;
import com.gci.model.response.BrandStoreResposne;
import com.gci.model.response.DenominationResponse;
import com.gci.model.response.OrderStatusResponse;
import com.payqwikweb.model.app.response.GCIOrderVoucher;
import com.payqwikweb.model.app.response.GCIProceedResponse;

public interface IGciServiceApi {
	
	BrandResponse getGciBrands(BrandsDTO dto);

	DenominationResponse getGciBrandDenominations(BrandDenominationDTO dto);

	BrandStoreResposne getGciBrandStores(BrandStoreDTO dto);
	
	AddOrderResponse  getAddOrder(OrderDTO dto);
	
	GCIProceedResponse sucees(GCIOrderVoucher request);

	List<OrderStatusResponse> getOrderStatus(OrderStatusDTO dto);
	
}
