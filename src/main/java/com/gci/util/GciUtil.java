package com.gci.util;

import com.payqwikweb.app.metadatas.UrlMetadatas;

public class GciUtil {
	
	private static final String BASE_URL_TEST = "http://sandbox.giftcardsindia.in/api/";// TEST URL
	private static final String BASE_URL_LIVE = "http://interface.giftcardsindia.in/api/";
    public static final String LOGIN_URL = getGciUrl()+"access-token/get";
    public static final String GCI_BRANDS = getGciUrl()+"channel/brands";
    public static final String GCI_BRANDS_DENOMINATIONS = getGciUrl()+"brand/denominations";
    public static final String GCI_BRANDS_STORE = getGciUrl()+"brand/stores";
    public static final String PROCESS_PAYMENT = getGciUrl()+"order/add";
    public static final String ORDER_PROCESS = getGciUrl()+"order/get/";
    public static final String ORDER_STATUS = getGciUrl()+"order/status/";
    
    public static String getGciUrl(){
    	if(UrlMetadatas.PRODUCTION){
    		return BASE_URL_LIVE;
    	}else{
    		return BASE_URL_TEST;
    	}
    }
    
    /*public static final String ID_VALUE= "68ce6e0a66ae9b3b12d83f96658d827e";//test
    public static final String KEY_VALUE="121b4b4ce66f14177905e36233a8dff13a951e305a81f395ccb049b35925e610";//test
*/    
    
    public static final String ID_VALUE= "68ce6e0a66ae9b3b12d83f96658d827e";//live
    public static final String KEY_VALUE="121b4b4ce66f14177905e36233a8dff13a951e305a81f395ccb049b35925e610";//live

}
