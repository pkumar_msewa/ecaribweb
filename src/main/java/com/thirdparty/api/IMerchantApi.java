package com.thirdparty.api;

import com.payqwikweb.app.model.UpiPayRequest;
import com.thirdparty.model.AuthenticateDTO;
import com.thirdparty.model.AuthenticationResponse;
import com.thirdparty.model.BescomAuthenthicationDTO;
import com.thirdparty.model.ChangePasswordDTO;
import com.thirdparty.model.CheckUpiStatus;
import com.thirdparty.model.ForgotPasswordDTO;
import com.thirdparty.model.LoginDTO;
import com.thirdparty.model.PaymentDTO;
import com.thirdparty.model.RegisterDTO;
import com.thirdparty.model.ResponseDTO;
import com.thirdparty.model.S2SRequest;
import com.thirdparty.model.S2SResponse;
import com.thirdparty.model.StatusCheckDTO;
import com.thirdparty.model.StatusResponse;
import com.thirdparty.model.VerifyDTO;
import com.upi.model.requet.UPIRedirect;
import com.upi.model.response.UPIRedirectResponse;


public interface IMerchantApi {
	AuthenticationResponse authenticateMerchant(AuthenticateDTO dto);
    ResponseDTO authenticateUser(LoginDTO dto);
    ResponseDTO registerUser(RegisterDTO dto);
    ResponseDTO verifyOTP(VerifyDTO dto);
    ResponseDTO forgotPassword(ForgotPasswordDTO dto);
    ResponseDTO validateOTP(VerifyDTO dto);
    ResponseDTO changePassword(ChangePasswordDTO dto);
    ResponseDTO processPayment(PaymentDTO dto);
    StatusResponse checkStatus(StatusCheckDTO dto);
    
    // Merchant Payment By UPI
    AuthenticationResponse authenticateUpiMerchant(BescomAuthenthicationDTO dto);
    ResponseDTO upiMerchantPayment(UpiPayRequest dto);
    UPIRedirectResponse upiRedirectPayment(UPIRedirect dto);
    StatusResponse checkUpiStatus(CheckUpiStatus dto);
    String s2sCall(S2SRequest dto);
    StatusResponse savingS2Sresp(S2SRequest dto);
    S2SResponse besconS2SCall(S2SRequest dto);
    S2SResponse besconRuralS2SCall(S2SRequest dto);
	ResponseDTO validateUserOTP(VerifyDTO dto);
}
