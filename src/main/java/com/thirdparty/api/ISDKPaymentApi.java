package com.thirdparty.api;

import com.payqwikweb.app.model.busdto.SaveSeatDetailsDTO;
import com.payqwikweb.app.model.request.RegistrationRequest;
import com.payqwikweb.app.model.response.RegistrationResponse;
import com.thirdparty.model.BusResponseDTO;
import com.thirdparty.model.MerchantRegisterDTO;
import com.thirdparty.model.SDKAuthenticationDTO;
import com.thirdparty.model.SDKAuthenticationResponse;
import com.thirdparty.model.SDKResponseDTO;
import com.thirdparty.model.VerifyDTO;

public interface ISDKPaymentApi {
	SDKAuthenticationResponse authenticateMerchant(SDKAuthenticationDTO dto);
	SDKResponseDTO validateUserOTP(VerifyDTO dto);
	BusResponseDTO saveSeatDetails(SaveSeatDetailsDTO request, String device);
	RegistrationResponse register(MerchantRegisterDTO request, String device);
	RegistrationResponse resendOTP(MerchantRegisterDTO request, String device);

}
