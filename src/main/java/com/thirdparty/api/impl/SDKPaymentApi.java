package com.thirdparty.api.impl;

import org.codehaus.jettison.json.JSONObject;
import org.json.JSONException;

import com.payqwikweb.app.metadatas.UrlMetadatas;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Language;
import com.payqwikweb.app.model.ResponseStatus;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.Version;
import com.payqwikweb.app.model.busdto.SaveSeatDetailsDTO;
import com.payqwikweb.app.model.request.RegistrationRequest;
import com.payqwikweb.app.model.response.RegistrationResponse;
import com.payqwikweb.app.utils.SecurityUtils;
import com.payqwikweb.model.web.Status;
import com.payqwikweb.model.web.UserType;
import com.payqwikweb.util.APIUtils;
import com.payqwikweb.util.JSONParserUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.thirdparty.api.ISDKPaymentApi;
import com.thirdparty.model.BusResponseDTO;
import com.thirdparty.model.MerchantRegisterDTO;
import com.thirdparty.model.ResponseDTO;
import com.thirdparty.model.SDKAuthenticationDTO;
import com.thirdparty.model.SDKAuthenticationResponse;
import com.thirdparty.model.SDKResponseDTO;
import com.thirdparty.model.VerifyDTO;
import com.thirdparty.util.MerchantConstants;
import com.thirdparty.util.SDKMerchantConstant;

public class SDKPaymentApi implements ISDKPaymentApi {

	@Override
	public SDKAuthenticationResponse authenticateMerchant(SDKAuthenticationDTO dto) {
		// TODO Auto-generated method stub
		SDKAuthenticationResponse resp = new SDKAuthenticationResponse();
		Client client = Client.create();
		client.addFilter(new LoggingFilter(System.out));
		WebResource webResource = client.resource(SDKMerchantConstant.AUTHENTICATE_URL);
		ClientResponse response = webResource.accept("application/json").type("application/json")
				.post(ClientResponse.class, dto.toJSON());
		String strResponse = response.getEntity(String.class);
		if (response.getStatus() == 200) {
			try {
				org.json.JSONObject json = new org.json.JSONObject(strResponse);
				final String code = JSONParserUtil.getString(json, "code");
				resp.setStatus(JSONParserUtil.getString(json, "status"));
				resp.setCode(code);
				resp.setUserExist(JSONParserUtil.getBoolean(json,"userExist"));
				if (code.equalsIgnoreCase("S00")) {
					resp.setSuccess(true);
					String message = JSONParserUtil.getString(json, "message");
					resp.setMessage(message);
					org.json.JSONObject details = JSONParserUtil.getObject(json, "details");
					resp.setSuccessURL(JSONParserUtil.getString(details, "successURL"));
					resp.setFailureURL(JSONParserUtil.getString(details, "failureURL"));
					resp.setImage(JSONParserUtil.getString(details, "image"));
					resp.setMerchantId(JSONParserUtil.getLong(details, "merchantId"));
				} else {
					resp.setSuccess(false);
					resp.setMessage(JSONParserUtil.getString(json, "message"));
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		} 
		return resp;
	}
	
	@Override
	public SDKResponseDTO validateUserOTP(VerifyDTO dto) {
		SDKResponseDTO result = new SDKResponseDTO();
		Client client = Client.create();
		WebResource webResource = client.resource(SDKMerchantConstant.VALIDATE_USER_OTP + "/" + dto.getOtp());
		ClientResponse response = webResource.accept("application/json").type("application/json")
				.header("hash", SecurityUtils.getHash("" + dto.toJSON())).post(ClientResponse.class, dto.toJSON());
		String strResponse = response.getEntity(String.class);
		try {
			org.json.JSONObject json = new org.json.JSONObject(strResponse);
			final String code = JSONParserUtil.getString(json, "code");
			if (code.equalsIgnoreCase("S00")) {
				result.setStatus(JSONParserUtil.getString(json, "status"));
				result.setCode(code);
				result.setMessage(JSONParserUtil.getString(json, "details"));
				result.setSessionId(JSONParserUtil.getString(json, "sessionId"));
				result.setAmount(Double.parseDouble(JSONParserUtil.getString(json, "details3")));
				result.setInfo(JSONParserUtil.getString(json, "details2"));
				result.setDetails(JSONParserUtil.getString(json, "details4"));
			} else {
				result.setStatus(JSONParserUtil.getString(json, "status"));
				result.setCode(code);
				result.setMessage(JSONParserUtil.getString(json, "details"));
			}
		} catch (JSONException e) {
			e.printStackTrace();
			result.setStatus("Service Unavailable");
		}
		return result;
	}
	
	@Override
	public BusResponseDTO saveSeatDetails(SaveSeatDetailsDTO request,String device) {
		BusResponseDTO resp=new BusResponseDTO();
		try {
			String tripId=System.currentTimeMillis()+"";
			JSONObject payload=new JSONObject();
			payload.put("userMobile", request.getMobileNo());
			payload.put("busId", request.getBusid());
			payload.put("totalFare", request.getTotalFare());
			payload.put("busType", request.getBusType());
			payload.put("boardId", request.getBoardId());
			payload.put("boardName", request.getBoardName());
			payload.put("arrTime", request.getArrTime());
			payload.put("deptTime", request.getDepTime());
			payload.put("travelName", request.getTravelName());
			payload.put("source", request.getSource());
			payload.put("journeyDate", request.getJourneyDate());
			payload.put("destination", request.getDestination());
			payload.put("boardTime", request.getBoardTime());
			payload.put("seatDetailsId", tripId);
			
			Client vpqClient = Client.create();
			vpqClient.addFilter(new LoggingFilter(System.out));
			WebResource vpqWebResource = vpqClient.resource(
					UrlMetadatas.saveSDKSeatDetailsURL(Version.VERSION_1, Role.USER, device, Language.ENGLISH));
			ClientResponse vpqresponse = vpqWebResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);

			String vpqstrResponse = vpqresponse.getEntity(String.class);

			if (vpqstrResponse!=null) {
				JSONObject vJObj=new JSONObject(vpqstrResponse);
				String status=vJObj.getString("status");
				String code=vJObj.getString("code");
				String vMessage=vJObj.getString("message");
				String details=vJObj.getString("details");

				if (code.equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
					resp.setStatus(status);
					resp.setCode(code);
					resp.setMessage(vMessage);
					if (details!=null) {
						resp.setDetails(vJObj.getJSONObject("details").getString("tripId"));
					}
				}
				else {
					resp.setStatus(status);
					resp.setCode(code);
					resp.setMessage(vMessage);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus(ResponseStatus.FAILURE.getKey());
		}

		return resp;
	}
	
	@Override
	public RegistrationResponse register(MerchantRegisterDTO request,String device) {

		RegistrationResponse resp = new RegistrationResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("mobileNumber", request.getMobileNumber());
			payload.put("password", request.getPassword());
			payload.put("locationCode", request.getLocationCode());
			payload.put("name", request.getName());
			payload.put("email", request.getEmail());
			payload.put("vbankCustomer",request.isVbankCustomer());
			payload.put("address",request.getAddress());
			payload.put("brand",request.getBrand());
			payload.put("model",request.getModel());
			payload.put("imeiNo",request.getImeiNo());
			payload.put("gender", request.getGender());
			payload.put("dateOfBirth", request.getDateOfBirth());
			
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(
					UrlMetadatas.getSDKMerchantRegisterUrl(Version.VERSION_1, Role.USER, device, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable...");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						resp.setMessage(message);
						String errors = "";
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							org.json.JSONObject details = JSONParserUtil.getObject(jobj,"details");
							if(details != null){
								boolean valid = JSONParserUtil.getBoolean(details,"valid");
								if(!valid){
									final String contactNo = JSONParserUtil.getString(details,"contactNo");
									final String email = JSONParserUtil.getString(details,"email");
									final String pinCode = JSONParserUtil.getString(details,"locationCode");
									if(!contactNo.equals("null")){
										errors = errors+"|"+contactNo;
										resp.setMessage(contactNo);
									}
									if(!email.equals("null")){
										errors = errors+"|"+email;
										resp.setMessage(email);
									}
									if(!pinCode.equals("null")) {
										errors = errors +"|"+pinCode;
										resp.setMessage(pinCode);
									}
								}
							}
							resp.setSuccess(false);
							resp.setDetails(errors);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setResponse(strResponse);
					}
					} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	
	@Override
	public RegistrationResponse resendOTP(MerchantRegisterDTO request,String device) {

		RegistrationResponse resp = new RegistrationResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("mobileNumber", request.getMobileNumber());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(
					UrlMetadatas.getSDKMerchantResendOTPUrl(Version.VERSION_1, Role.USER, device, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable...");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						resp.setMessage(message);
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setMessage(message);
						resp.setCode(code);
						resp.setStatus(status);
						resp.setResponse(strResponse);
					}
					} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

}
