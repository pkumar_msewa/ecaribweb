package com.thirdparty.api.impl;

import java.io.StringWriter;
import java.net.URLEncoder;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonWriter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;

import com.payqwikweb.util.CommonUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.thirdparty.api.IFRMApi;
import com.thirdparty.model.FRMRequestDTO;
import com.thirdparty.model.FRMResponseDTO;
import com.thirdparty.model.MessageBodyDto;
import com.thirdparty.util.FRMConstants;

public class FRMApi implements IFRMApi {
	
	protected final Logger logger= LoggerFactory.getLogger(this.getClass());
	private static final String advice= "allow";
	private static final String enqueue= "true";
	@Override
	public FRMResponseDTO frmDecideFundTransfer(FRMRequestDTO requestDTO) {
		FRMResponseDTO response=null;
		try {
			logger.info("fund transfer api decide call");
			String urlPayload=  getDecideFundTransferJson(requestDTO);
			response = getDecideResponse(urlPayload,requestDTO);
		} catch (Exception e) {
			e.printStackTrace();
		}  
		return response;
	}
	
	// common for all decide api
	private FRMResponseDTO getDecideResponse(String urlPayload, FRMRequestDTO requestDTO) {
		FRMResponseDTO response = new FRMResponseDTO();
		try {
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(FRMConstants.frmDecideURL+"?payload="
			+ URLEncoder.encode(urlPayload , "UTF-8"));	
			ClientResponse clientResponse = resource.accept(MediaType.APPLICATION_JSON_VALUE).type(MediaType.APPLICATION_JSON_VALUE)
					.get(ClientResponse.class);
			String frmResponse = clientResponse.getEntity(String.class);
			if (frmResponse != null && !frmResponse.isEmpty()) {
				if(advice.equalsIgnoreCase(frmResponse.toString())){
					response.setSuccess(true);
					response.setDecideResp(frmResponse);
					response.setEnqueueResp(frmEnqueueResponse(requestDTO,urlPayload));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}  
		return response;
	}
	
	private String getDecideFundTransferJson(FRMRequestDTO dto) {
		JsonObjectBuilder payload = null;
		StringWriter jsnReqStr = new StringWriter();
		try {
			payload = getCommonJson(dto);
			payload.add("msgBody", getMessageBody(dto.getMsgBody()));
			JsonObject empObj = payload.build();
			JsonWriter jsonWtr = Json.createWriter(jsnReqStr);
			jsonWtr.writeObject(empObj);
			jsonWtr.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsnReqStr.toString();
	}
	private JsonObjectBuilder getCommonJson(FRMRequestDTO dto) {
		JsonObjectBuilder payload = Json.createObjectBuilder();
		try {
			payload.add("event-id", dto.getEventId());
			payload = getHostUserId(payload,dto.getEventSubtype());
			payload.add("event-name", dto.getEventName());
			payload.add("eventsubtype", dto.getEventSubtype());
			payload.add("eventtype", dto.getEventType());
			payload.add("source", dto.getSource());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return payload;
	}
	private JsonObjectBuilder getHostUserId(JsonObjectBuilder payload, String enventSubType) {
		switch(enventSubType){
		case "fundstransfer" :
			payload.add("host_user_id", enventSubType);
		}
		return payload;
	}
	private JsonObjectBuilder getMessageBody(MessageBodyDto dto) {
		JsonObjectBuilder mesgBoday = Json.createObjectBuilder();
		try {
			mesgBoday.add("host_id", dto.getHost_id());
			mesgBoday.add("channel", dto.getChannel());
			mesgBoday.add("mobile_no", dto.getMobile_no());
			mesgBoday.add("payee_acct_id", dto.getPayee_acct_id());
			mesgBoday.add("city_code", dto.getCity_code());
			mesgBoday.add("tran_id", dto.getTran_id());
			mesgBoday.add("payee_id", dto.getPayee_id());
			mesgBoday.add("payee_mob_no", dto.getPayee_mob_no());
			mesgBoday.add("payee_code", dto.getPayee_code());
			mesgBoday.add("branch_id", dto.getBranch_id());
			mesgBoday.add("aadhar_no", dto.getAadhar_no());
			mesgBoday.add("sys_time", dto.getSys_time());
			mesgBoday.add("agent_code", dto.getAgent_code());
			mesgBoday.add("acct_name", dto.getAcct_name());
			mesgBoday.add("tran_rmks", dto.getTran_rmks());
			mesgBoday.add("terminal_id", dto.getTerminal_id());
			mesgBoday.add("payee_ifsc_code", dto.getPayee_ifsc_code());
			mesgBoday.add("auth_type", dto.getAuth_type());
			mesgBoday.add("device_id", dto.getDevice_id());
			mesgBoday.add("tran_type", dto.getTran_type());
			mesgBoday.add("vpa", dto.getVpa());
			mesgBoday.add("payee_nick_name", dto.getPayee_nick_name());
			mesgBoday.add("ip_address", dto.getIp_address());
			mesgBoday.add("mcc_id", dto.getMcc_id());
			mesgBoday.add("country_code", dto.getCountry_code());
			mesgBoday.add("cust_card_id", dto.getCust_card_id());
			mesgBoday.add("account_id", dto.getAccount_id());
			mesgBoday.add("user_id", dto.getUser_id());
			mesgBoday.add("corporate_id", dto.getCorporate_id());
			mesgBoday.add("payee_name", dto.getPayee_name());
			mesgBoday.add("tran_amt", dto.getTran_amt());
			mesgBoday.add("network_type", dto.getNetwork_type());
			mesgBoday.add("cust_id", dto.getCust_id());
			mesgBoday.add("tran_crncy_code", dto.getTran_crncy_code());
			mesgBoday.add("eventts", dto.getEventts());
			mesgBoday.add("account_type", dto.getAccount_type());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mesgBoday;
	}
	//Below integration is not in use, it is made for individual call.
	@Override
	public FRMResponseDTO frmEnqueueFundTransfer(FRMRequestDTO requestDTO) {
		FRMResponseDTO response = new FRMResponseDTO();
//		try {
//			String urlPayload=  getDecideFundTransferJson(requestDTO);
//			Client client = Client.create();
//			WebResource resource = client.resource(FRMConstants.frmFtEnqueueURL+"?q=HOST&event_id="
//			+requestDTO.getEventId()+"&entity_id="+requestDTO.getEventId() 
//			+"&event_ts="+CommonUtil.getTimestamp(requestDTO.getMsgBody().getEventts())+"");	
//			System.out.println(resource.toString());
//			ClientResponse clientResponse = resource.accept(MediaType.APPLICATION_JSON_VALUE).type(MediaType.APPLICATION_JSON_VALUE)
//					.post(ClientResponse.class,urlPayload);
//			String frmResponse = clientResponse.getEntity(String.class);
//			if (frmResponse != null && !frmResponse.isEmpty()) {
//					response.setSuccess(false);
//			} else {
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}  
		return response;
	}
	@Override
	public FRMResponseDTO frmDecideRegistration(FRMRequestDTO requestDTO) {
		FRMResponseDTO response = new FRMResponseDTO();
		try {
			logger.info("registration api decide call");
			String urlPayload = getDecideRegistrationJson(requestDTO);
			response = getDecideResponse(urlPayload,requestDTO);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	private String getDecideRegistrationJson(FRMRequestDTO requestDTO) {
		JsonObjectBuilder payload = null;
		StringWriter jsnReqStr = new StringWriter();
		try {
			payload = getCommonJson(requestDTO);
			payload.add("msgBody", getRegistrationBody(requestDTO.getMsgBody()));
			JsonObject empObj = payload.build();
			JsonWriter jsonWtr = Json.createWriter(jsnReqStr);
			jsonWtr.writeObject(empObj);
			jsonWtr.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsnReqStr.toString();
	}

	private JsonObjectBuilder getRegistrationBody(MessageBodyDto msgBody) {
		JsonObjectBuilder mesgBoday = Json.createObjectBuilder();
		try {
			mesgBoday.add("host_id", "F");
			mesgBoday.add("channel", msgBody.getChannel());
			mesgBoday.add("mobile_no", msgBody.getMobile_no());
			mesgBoday.add("virtual_account_id", msgBody.getMobile_no());
			mesgBoday.add("event_id", msgBody.getEvent_id());
			mesgBoday.add("email_id", msgBody.getEmail_id());
			mesgBoday.add("pin_code", msgBody.getPin_code());
			mesgBoday.add("sys_time", msgBody.getSys_time());
			mesgBoday.add("device_id", msgBody.getDevice_id());
			mesgBoday.add("addr_network", msgBody.getAddr_network());
			mesgBoday.add("country", msgBody.getCountry());
			mesgBoday.add("succ_fail_flg", msgBody.getSucc_fail_flg());
			mesgBoday.add("account_id", msgBody.getAccount_id());
			mesgBoday.add("user_id", msgBody.getUser_id());
			mesgBoday.add("error_code", msgBody.getError_code());
			mesgBoday.add("error_desc", msgBody.getError_desc());
			mesgBoday.add("mfa_type", msgBody.getMfa_type());
			mesgBoday.add("cust_id", msgBody.getCust_id());
			mesgBoday.add("eventts", msgBody.getEventts());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mesgBoday;
	}
	
	//Below integration is not in use, it is made for individual call.
	@Override
	public FRMResponseDTO frmEnqueueRegistration(FRMRequestDTO requestDTO) {
		FRMResponseDTO response = new FRMResponseDTO();
//		try {
//			String urlPayload=  getDecideRegistrationJson(requestDTO);
//			Client client = Client.create();
//			WebResource resource = client.resource(FRMConstants.frmFtEnqueueURL+"?q=HOST&event_id="
//			+requestDTO.getEventId()+"&entity_id="+requestDTO.getEventId() 
//			+"&event_ts="+CommonUtil.getTimestamp(requestDTO.getMsgBody().getEventts())+"");	
//			System.out.println(resource.toString());
//			ClientResponse clientResponse = resource.accept(MediaType.APPLICATION_JSON_VALUE).type(MediaType.APPLICATION_JSON_VALUE)
//					.post(ClientResponse.class,urlPayload);
//			String frmResponse = clientResponse.getEntity(String.class);
//			if (frmResponse != null && !frmResponse.isEmpty()) {
//					response.setSuccess(true);
//			} else {
//				response.setSuccess(false);
//			}
//		} catch (Exception e) {
//			response.setSuccess(true);
//			e.printStackTrace();
//		}  
		return response;
	}
	@Override
	public FRMResponseDTO frmDecideLogin(FRMRequestDTO requestDTO) {
		FRMResponseDTO response = new FRMResponseDTO();
		try {
			logger.info("frm login api decide call");
			String urlPayload = getDecideLoginJson(requestDTO);
			response = getDecideResponse(urlPayload,requestDTO);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	// common for all enqueue api
	private boolean frmEnqueueResponse(FRMRequestDTO requestDTO, String urlPayload) {
		try {
			Client client = Client.create();
			WebResource resource = client.resource(FRMConstants.frmEnqueueURL+"?q=HOST&event_id="
			+requestDTO.getEventId()+"&entity_id="+requestDTO.getEventId() 
			+"&event_ts="+CommonUtil.getTimestamp(requestDTO.getMsgBody().getEventts())+"");	
			System.out.println(resource.toString());
			ClientResponse clientResponse = resource.accept(MediaType.APPLICATION_JSON_VALUE).type(MediaType.APPLICATION_JSON_VALUE)
					.post(ClientResponse.class,urlPayload);
			String frmResponse = clientResponse.getEntity(String.class);
			if (frmResponse != null && !frmResponse.isEmpty()) {
				if(enqueue.equalsIgnoreCase(frmResponse.toString())){
					return true;
				}
			} 
		} catch (Exception e) {
			e.printStackTrace();
		}  
		return false;
	}
	
	private String getDecideLoginJson(FRMRequestDTO requestDTO) {
		JsonObjectBuilder payload = null;
		StringWriter jsnReqStr = new StringWriter();
		try {
			payload = getCommonJson(requestDTO);
			payload.add("msgBody", getLoginBody(requestDTO.getMsgBody()));
			JsonObject empObj = payload.build();
			JsonWriter jsonWtr = Json.createWriter(jsnReqStr);
			jsonWtr.writeObject(empObj);
			jsonWtr.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsnReqStr.toString();
	}
	private JsonObjectBuilder getLoginBody(MessageBodyDto msgBody) {
		JsonObjectBuilder mesgBoday = Json.createObjectBuilder();
		try {
			mesgBoday.add("host_id", "F");
			mesgBoday.add("channel", msgBody.getChannel());
			mesgBoday.add("mobile_no", msgBody.getMobile_no());
			mesgBoday.add("corporate_id", msgBody.getCorporate_id());
			mesgBoday.add("lastLogints", msgBody.getLastLogints());
			mesgBoday.add("logints", msgBody.getLogints());
			mesgBoday.add("last_login_ip", msgBody.getLast_login_ip());
			mesgBoday.add("device_type", msgBody.getDevice_type());
			mesgBoday.add("user_type", msgBody.getUser_type());
			mesgBoday.add("sys_time", msgBody.getSys_time());
			mesgBoday.add("device_id", msgBody.getDevice_id());
			mesgBoday.add("addr_network", msgBody.getAddr_network());
			mesgBoday.add("IP_Country", msgBody.getCountry());
			mesgBoday.add("succ_fail_flg", msgBody.getSucc_fail_flg());
			mesgBoday.add("account_id", msgBody.getAccount_id());
			mesgBoday.add("user_id", msgBody.getUser_id());
			mesgBoday.add("error_code", msgBody.getError_code());
			mesgBoday.add("error_desc", msgBody.getError_desc());
			mesgBoday.add("mfa_type", msgBody.getMfa_type());
			mesgBoday.add("cust_id", msgBody.getCust_id());
			mesgBoday.add("eventts", msgBody.getEventts());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mesgBoday;
	}
	
	//Below integration is not in use, it is made for individual call.
	@Override
	public FRMResponseDTO frmEnqueueLogin(FRMRequestDTO requestDTO) {
		FRMResponseDTO response = new FRMResponseDTO();
//		try {
//			String urlPayload=  getDecideLoginJson(requestDTO);
//			Client client = Client.create();
//			WebResource resource = client.resource(FRMConstants.frmFtEnqueueURL+"?q=HOST&event_id="
//			+requestDTO.getEventId()+"&entity_id="+requestDTO.getEventId() 
//			+"&event_ts="+CommonUtil.getTimestamp(requestDTO.getMsgBody().getEventts())+"");	
//			System.out.println(resource.toString());
//			ClientResponse clientResponse = resource.accept(MediaType.APPLICATION_JSON_VALUE).type(MediaType.APPLICATION_JSON_VALUE)
//					.post(ClientResponse.class,urlPayload);
//			String frmResponse = clientResponse.getEntity(String.class);
//			if (frmResponse != null && !frmResponse.isEmpty()) {
//					response.setSuccess(false);
//			} else {
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}  
		return response;
	}
}
