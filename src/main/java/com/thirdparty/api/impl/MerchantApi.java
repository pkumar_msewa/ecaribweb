package com.thirdparty.api.impl;

import org.json.JSONException;

import com.payqwikweb.app.model.ResponseStatus;
import com.payqwikweb.app.model.UpiPayRequest;
import com.payqwikweb.app.utils.SecurityUtils;
import com.payqwikweb.model.web.Status;
import com.payqwikweb.util.APIUtils;
import com.payqwikweb.util.JSONParserUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import com.thirdparty.api.IMerchantApi;
import com.thirdparty.model.AuthenticateDTO;
import com.thirdparty.model.AuthenticationResponse;
import com.thirdparty.model.BescomAuthenthicationDTO;
import com.thirdparty.model.ChangePasswordDTO;
import com.thirdparty.model.CheckUpiStatus;
import com.thirdparty.model.ForgotPasswordDTO;
import com.thirdparty.model.LoginDTO;
import com.thirdparty.model.PaymentDTO;
import com.thirdparty.model.RegisterDTO;
import com.thirdparty.model.ResponseDTO;
import com.thirdparty.model.S2SRequest;
import com.thirdparty.model.S2SResponse;
import com.thirdparty.model.StatusCheckDTO;
import com.thirdparty.model.StatusResponse;
import com.thirdparty.model.VerifyDTO;
import com.thirdparty.util.MerchantConstants;
import com.upi.model.requet.UPIRedirect;
import com.upi.model.response.UPIRedirectResponse;
import com.upi.model.response.UPIResp;

public class MerchantApi implements IMerchantApi {

	@Override
	public AuthenticationResponse authenticateMerchant(AuthenticateDTO dto) {
		AuthenticationResponse resp = new AuthenticationResponse();
		Client client = Client.create();
		client.addFilter(new LoggingFilter(System.out));
		WebResource webResource = client.resource(MerchantConstants.AUTHENTICATE_URL);
		ClientResponse response = webResource.accept("application/json").type("application/json")
				.post(ClientResponse.class, dto.toJSON());
		String strResponse = response.getEntity(String.class);
		if (response.getStatus() == 200) {
			try {
				org.json.JSONObject json = new org.json.JSONObject(strResponse);
				final String code = JSONParserUtil.getString(json, "code");
				resp.setStatus(JSONParserUtil.getString(json, "status"));
				resp.setCode(code);
//				resp.setUserExist(JSONParserUtil.getBoolean(json,"userExist"));
				if (code.equalsIgnoreCase("S00")) {
					resp.setSuccess(true);
					String message = JSONParserUtil.getString(json, "message");
					resp.setMessage(message);
					org.json.JSONObject details = JSONParserUtil.getObject(json, "details");
					resp.setSuccessURL(JSONParserUtil.getString(details, "successURL"));
					resp.setFailureURL(JSONParserUtil.getString(details, "failureURL"));
					resp.setImage(JSONParserUtil.getString(details, "image"));
					resp.setMerchantId(JSONParserUtil.getLong(details, "merchantId"));
				} else {
					resp.setSuccess(false);
					resp.setMessage(JSONParserUtil.getString(json, "message"));
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		} else {

			// resp.setMessage(JSONParserUtil.getString(json,"message"));
		}
		return resp;
	}

	@Override
	public ResponseDTO authenticateUser(LoginDTO dto) {
		ResponseDTO resp = new ResponseDTO();
		Client client = Client.create();
		WebResource webResource = client.resource(MerchantConstants.AUTHENTICATE_USER);
		ClientResponse response = webResource.accept("application/json").type("application/json")
				.header("hash", "123456789").post(ClientResponse.class, dto.toJSON());
		String strResponse = response.getEntity(String.class);
		if (response.getStatus() == 200) {
			try {
				org.json.JSONObject json = new org.json.JSONObject(strResponse);
				final String code = JSONParserUtil.getString(json, "code");
				resp.setStatus(JSONParserUtil.getString(json, "status"));
				resp.setCode(code);
				if (code.equalsIgnoreCase("S00")) {
					String message = JSONParserUtil.getString(json, "message");
					resp.setMessage(message);
					org.json.JSONObject details = JSONParserUtil.getObject(json, "details");
					if (details != null) {
						final String sessionId = JSONParserUtil.getString(details, "sessionId");
						resp.setSessionId(sessionId);
						org.json.JSONObject accountDetail = JSONParserUtil.getObject(details, "accountDetail");
						if (accountDetail != null) {
							resp.setAmount(JSONParserUtil.getDouble(accountDetail, "balance"));
						}
						org.json.JSONObject userDetail = JSONParserUtil.getObject(details, "userDetail");
						if (userDetail != null) {
							resp.setDetails(JSONParserUtil.getString(userDetail, "username"));
							resp.setInfo(JSONParserUtil.getString(userDetail, "firstName") + " "
									+ JSONParserUtil.getString(userDetail, "lastName"));
						}
					}
				} else {
					resp.setMessage(JSONParserUtil.getString(json, "message"));
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		} else {

		}
		return resp;
	}

	@Override
	public ResponseDTO registerUser(RegisterDTO dto) {
		ResponseDTO result = new ResponseDTO();
		Client client = Client.create();
		WebResource webResource = client.resource(MerchantConstants.REGISTER_USER);
		ClientResponse response = webResource.accept("application/json").type("application/json")
				.header("hash", SecurityUtils.getHash("" + dto.toJSON())).post(ClientResponse.class, dto.toJSON());
		String strResponse = response.getEntity(String.class);
		if (response.getStatus() == 200) {
			try {
				org.json.JSONObject json = new org.json.JSONObject(strResponse);
				final String code = JSONParserUtil.getString(json, "code");
				result.setStatus(JSONParserUtil.getString(json, "status"));
				result.setCode(code);
				if (code.equalsIgnoreCase("S00")) {
					result.setMessage(JSONParserUtil.getString(json, "message"));
				} else {
					result.setMessage(JSONParserUtil.getString(json, "message"));
					if (code.equalsIgnoreCase("F00")) {
						org.json.JSONObject details = JSONParserUtil.getObject(json, "details");
						if (details != null) {
							boolean valid = JSONParserUtil.getBoolean(details, "valid");
							if (!valid) {
								result.setCode("F04");
								result.setMessage(JSONParserUtil.getString(details, "contactNo"));
								result.setDetails(JSONParserUtil.getString(details, "email"));
							}
						}
					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		return result;
	}

	@Override
	public ResponseDTO verifyOTP(VerifyDTO dto) {
		ResponseDTO result = new ResponseDTO();
		Client client = Client.create();
		WebResource webResource = client.resource(MerchantConstants.VERIFY_OTP);
		ClientResponse response = webResource.accept("application/json").type("application/json")
				.header("hash", SecurityUtils.getHash("" + dto.toJSON())).post(ClientResponse.class, dto.toJSON());
		String strResponse = response.getEntity(String.class);
		if (response.getStatus() == 200) {
			try {
				org.json.JSONObject json = new org.json.JSONObject(strResponse);
				result.setStatus(JSONParserUtil.getString(json, "status"));
				final String code = JSONParserUtil.getString(json, "code");
				result.setCode(code);
				result.setMessage(JSONParserUtil.getString(json, "details"));

			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		return result;
	}

	@Override
	public ResponseDTO forgotPassword(ForgotPasswordDTO dto) {
		ResponseDTO result = new ResponseDTO();
		Client client = Client.create();
		WebResource webResource = client.resource(MerchantConstants.FORGOT_PASSWORD);
		ClientResponse response = webResource.accept("application/json").type("application/json")
				.header("hash", SecurityUtils.getHash("" + dto.toJSON())).post(ClientResponse.class, dto.toJSON());
		String strResponse = response.getEntity(String.class);
		if (response.getStatus() == 200) {
			try {
				org.json.JSONObject json = new org.json.JSONObject(strResponse);
				result.setStatus(JSONParserUtil.getString(json, "status"));
				final String code = JSONParserUtil.getString(json, "code");
				result.setCode(code);
				result.setMessage(JSONParserUtil.getString(json, "details"));
			} catch (JSONException e) {
				e.printStackTrace();
				result.setStatus("Service Unavailable");
			}
		}
		return result;
	}

	@Override
	public ResponseDTO validateOTP(VerifyDTO dto) {
		ResponseDTO result = new ResponseDTO();
		Client client = Client.create();
		WebResource webResource = client.resource(MerchantConstants.VALIDATE_FORGOT_PASSWORD_OTP + "/" + dto.getOtp());
		ClientResponse response = webResource.accept("application/json").type("application/json")
				.header("hash", SecurityUtils.getHash("" + dto.toJSON())).post(ClientResponse.class, dto.toJSON());
		String strResponse = response.getEntity(String.class);
		if (response.getStatus() == 200) {
			try {
				org.json.JSONObject json = new org.json.JSONObject(strResponse);
				result.setStatus(JSONParserUtil.getString(json, "status"));
				final String code = JSONParserUtil.getString(json, "code");
				result.setCode(code);
				result.setMessage(JSONParserUtil.getString(json, "details"));
			} catch (JSONException e) {
				e.printStackTrace();
				result.setStatus("Service Unavailable");
			}
		}
		return result;
	}

	@Override
	public ResponseDTO validateUserOTP(VerifyDTO dto) {
		ResponseDTO result = new ResponseDTO();
		Client client = Client.create();
		WebResource webResource = client.resource(MerchantConstants.VALIDATE_USER_OTP + "/" + dto.getOtp());
		ClientResponse response = webResource.accept("application/json").type("application/json")
				.header("hash", SecurityUtils.getHash("" + dto.toJSON())).post(ClientResponse.class, dto.toJSON());
		String strResponse = response.getEntity(String.class);
		try {
			org.json.JSONObject json = new org.json.JSONObject(strResponse);
			final String code = JSONParserUtil.getString(json, "code");
			if (code.equalsIgnoreCase("S00")) {
				result.setStatus(JSONParserUtil.getString(json, "status"));
				result.setCode(code);
				result.setMessage(JSONParserUtil.getString(json, "details"));
				result.setSessionId(JSONParserUtil.getString(json, "sessionId"));
				result.setAmount(Double.parseDouble(JSONParserUtil.getString(json, "details3")));
				result.setInfo(JSONParserUtil.getString(json, "details2"));
				result.setDetails(JSONParserUtil.getString(json, "details4"));
			} else {
				result.setStatus(JSONParserUtil.getString(json, "status"));
				result.setCode(code);
				result.setMessage(JSONParserUtil.getString(json, "details"));
			}
		} catch (JSONException e) {
			e.printStackTrace();
			result.setStatus("Service Unavailable");
		}
		return result;
	}

	@Override
	public ResponseDTO changePassword(ChangePasswordDTO dto) {
		ResponseDTO result = new ResponseDTO();
		Client client = Client.create();
		WebResource webResource = client.resource(MerchantConstants.CHANGE_PASSWORD);
		ClientResponse response = webResource.accept("application/json").type("application/json")
				.header("hash", SecurityUtils.getHash("" + dto.toJSON())).post(ClientResponse.class, dto.toJSON());
		String strResponse = response.getEntity(String.class);
		if (response.getStatus() == 200) {
			try {
				org.json.JSONObject json = new org.json.JSONObject(strResponse);
				result.setStatus(JSONParserUtil.getString(json, "status"));
				final String code = JSONParserUtil.getString(json, "code");
				result.setCode(code);
				result.setMessage(JSONParserUtil.getString(json, "details"));
			} catch (JSONException e) {
				e.printStackTrace();
				result.setStatus("Service Unavailable");
			}
		}
		return result;
	}

	@Override
	public ResponseDTO processPayment(PaymentDTO dto) {
		ResponseDTO result = new ResponseDTO();
		Client client = Client.create();
		client.addFilter(new LoggingFilter(System.out));
		WebResource webResource = client.resource(MerchantConstants.PROCESS_PAYMENT);
		ClientResponse response = webResource.accept("application/json").type("application/json")
				.header("hash", SecurityUtils.getHash("" + dto.toJSON())).post(ClientResponse.class, dto.toJSON());
		String strResponse = response.getEntity(String.class);
		if (response.getStatus() == 200) {
			try {
				org.json.JSONObject json = new org.json.JSONObject(strResponse);
				result.setStatus(JSONParserUtil.getString(json, "status"));
				final String code = JSONParserUtil.getString(json, "code");
				result.setCode(code);
				result.setMessage(JSONParserUtil.getString(json, "details"));
				result.setTransactionId(json.getString("txnId"));
			} catch (JSONException e) {
				e.printStackTrace();
				result.setStatus("Service Unavailable");
			}
		}
		return result;
	}

	@Override
	public StatusResponse checkStatus(StatusCheckDTO dto) {
		StatusResponse result = new StatusResponse();
		Client client = Client.create();
		client.addFilter(new LoggingFilter(System.out));
		WebResource webResource = client.resource(MerchantConstants.STATUS_CHECK);
		ClientResponse response = webResource.accept("application/json").type("application/json")
				.header("hash", SecurityUtils.getHash("" + dto.toJSON())).post(ClientResponse.class, dto.toJSON());
		String strResponse = response.getEntity(String.class);
		if (response.getStatus() == 200) {
			try {
				org.json.JSONObject json = new org.json.JSONObject(strResponse);
				System.err.println(json);
				final String code = JSONParserUtil.getString(json, "code");
				result.setCode(code);
				result.setMessage(JSONParserUtil.getString(json, "message"));
				if (code.equalsIgnoreCase("S00")) {
					org.json.JSONObject details = JSONParserUtil.getObject(json, "details");
					result.setTransactionDate(JSONParserUtil.getString(details, "transactionDate"));
					result.setPaymentId(JSONParserUtil.getString(details, "paymentId"));
					result.setMerchantRefNo(JSONParserUtil.getString(details, "merchantRefNo"));
					result.setStatus(Status.valueOf(JSONParserUtil.getString(details, "status")));
					result.setAmount(JSONParserUtil.getString(details, "amount"));
				}
			} catch (JSONException e) {
				e.printStackTrace();
				result.setMessage("Service Unavailable");
			}
		}
		return result;
	}

	// TODO Merchant Payment by UPI
	@Override
	public AuthenticationResponse authenticateUpiMerchant(BescomAuthenthicationDTO dto) {
		AuthenticationResponse resp = new AuthenticationResponse();
		Client client = Client.create();
		client.addFilter(new LoggingFilter(System.out));
		WebResource webResource = client.resource(MerchantConstants.UPI_AUTHENTICATE_URL);
		ClientResponse response = webResource.accept("application/json").type("application/json")
				.post(ClientResponse.class, dto.toJSON());
		String strResponse = response.getEntity(String.class);
		if (response.getStatus() == 200) {
			try {
				org.json.JSONObject json = new org.json.JSONObject(strResponse);
				final String code = JSONParserUtil.getString(json, "code");
				resp.setStatus(JSONParserUtil.getString(json, "status"));
				resp.setCode(code);
				if (code.equalsIgnoreCase("S00")) {
					resp.setSuccess(true);
					String message = JSONParserUtil.getString(json, "message");
					resp.setMessage(message);
					org.json.JSONObject details = JSONParserUtil.getObject(json, "details");
					resp.setSuccessURL(JSONParserUtil.getString(details, "successURL"));
					resp.setFailureURL(JSONParserUtil.getString(details, "failureURL"));
					resp.setImage(JSONParserUtil.getString(details, "image"));
					resp.setMerchantId(JSONParserUtil.getLong(details, "merchantId"));
				} else {
					resp.setSuccess(false);
					resp.setMessage(JSONParserUtil.getString(json, "message"));
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		} else {

			// resp.setMessage(JSONParserUtil.getString(json,"message"));
		}
		return resp;
	}

	@Override
	public ResponseDTO upiMerchantPayment(UpiPayRequest dto) {
		ResponseDTO resp = new ResponseDTO();
		try {
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(MerchantConstants.UPI_PAYMENT_URL);
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.post(ClientResponse.class, dto.toJSON());
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						final String refId = (String) jobj.get("transactionRefNO");
						final String tdate = (String) jobj.get("transactionDate");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							resp.setSuccess(true);
							// org.json.JSONObject job =
							// JSONParserUtil.getObject(jobj, "details");

						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
						resp.setTdate(tdate);
						resp.setRefId(refId);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public UPIRedirectResponse upiRedirectPayment(UPIRedirect dto) {
		UPIRedirectResponse result = new UPIRedirectResponse();
		Client client = Client.create();
		client.addFilter(new LoggingFilter(System.out));
		WebResource webResource = client.resource(MerchantConstants.UPI_REDIRECT_URL);
		ClientResponse response = webResource.accept("application/json").type("application/json")
				.post(ClientResponse.class, dto.toJSON());
		String strResponse = response.getEntity(String.class);
		if (response.getStatus() == 200) {
			try {
				System.out.println("response is" + response.getStatus());
				org.json.JSONObject json = new org.json.JSONObject(strResponse);
				final String code = JSONParserUtil.getString(json, "code");
				if (code.equals("S00")) {
					UPIResp resp = new UPIResp();
					org.json.JSONObject details = new org.json.JSONObject(JSONParserUtil.getString(json, "details"));
					resp.setAmount(JSONParserUtil.getDouble(details, "amount"));
					resp.setMerchantRefNo(JSONParserUtil.getString(details, "merchantRefNo"));
					resp.setPaymentId(JSONParserUtil.getString(details, "paymentId"));
					resp.setTransactionDate(JSONParserUtil.getString(details, "transactionDate"));
					resp.setStatus(JSONParserUtil.getString(details, "status"));
					result.setDetails(resp);
				}
				result.setCode(code);
				result.setStatus(JSONParserUtil.getString(json, "status"));
				result.setMessage(JSONParserUtil.getString(json, "message"));
				// result.setTransactionId(json.getString("txnId"));
			} catch (JSONException e) {
				e.printStackTrace();
				result.setCode("");
				result.setStatus("Service Unavailable");
			}
		}
		return result;
	}

	@Override
	public StatusResponse checkUpiStatus(CheckUpiStatus dto) {
		StatusResponse result = new StatusResponse();
		Client client = Client.create();
//		client.addFilter(new LoggingFilter(System.out));
		WebResource webResource = client.resource(MerchantConstants.UPI_STATUS_URL);
		ClientResponse response = webResource.accept("application/json").type("application/json")
				.header("hash", SecurityUtils.getHash("" + dto.toJSON())).post(ClientResponse.class, dto.toJSON());
		String strResponse = response.getEntity(String.class);
		if (response.getStatus() == 200) {
			try {
				org.json.JSONObject json = new org.json.JSONObject(strResponse);
				final String code = JSONParserUtil.getString(json, "code");
				result.setCode(code);
				result.setMessage(JSONParserUtil.getString(json, "message"));
				if (code.equalsIgnoreCase("S00")) {
					org.json.JSONObject details = JSONParserUtil.getObject(json, "details");
					result.setTransactionDate(JSONParserUtil.getString(details, "transactionDate"));
					result.setPaymentId(JSONParserUtil.getString(details, "paymentId"));
					result.setMerchantRefNo(JSONParserUtil.getString(details, "merchantRefNo"));
					result.setStatus(Status.valueOf(JSONParserUtil.getString(details, "status")));
					result.setAmount(JSONParserUtil.getString(details, "amount"));
				}
			} catch (JSONException e) {
				e.printStackTrace();
				result.setMessage("Service Unavailable");
			}
		}
		return result;
	}

	@Override
	public String s2sCall(S2SRequest dto) {
		Client client = Client.create();
		 
		WebResource webResource = client.resource(MerchantConstants.BESCOM_S2S_CALL_BACK_URL);
		ClientResponse response = webResource.accept("application/json").type("application/json")
				.header("hash", SecurityUtils.getHash("" + dto.toJSON())).post(ClientResponse.class, dto.toJSON());
		String strResponse = response.getEntity(String.class);
		System.err.print("strResponse" + strResponse);
		return strResponse;
	}

	public static void main(String[] args) {
//		S2SRequest dto = ConvertUtil.convertS2SRequest("272868", "1515155504992", "6722", "Success", "S00", 15.0, 0.0,
//				"7416226162", "pavan_kamshetty@infosys.com", "2018-01-05 07:29:39", "8D8C86D48ED4E246F9B810AEB37CFD04");
//		Client client = Client.create();
//		 
//		WebResource webResource = client.resource(MerchantConstants.BESCOM_S2S_CALL_BACK_URL);
//		ClientResponse response = webResource.accept("application/json").type("application/json")
//				.header("hash", SecurityUtils.getHash("" + dto.toJSON())).post(ClientResponse.class, dto.toJSON());
//		String strResponse = response.getEntity(String.class);
//		System.err.print("strResponse" + strResponse);

	}

	@Override
	public StatusResponse savingS2Sresp(S2SRequest dto) {
		System.err.println("inside check status");
		StatusResponse result = new StatusResponse();
		Client client = Client.create();
		WebResource webResource = client.resource(MerchantConstants.SAVING_S2S_REQ_RESP);
		ClientResponse response = webResource.accept("application/json").type("application/json")
				.header("hash", SecurityUtils.getHash("" + dto.toJSON())).post(ClientResponse.class, dto.toJSON());
		String strResponse = response.getEntity(String.class);
//		System.err.print("strResponse" + strResponse);
		if (response.getStatus() == 200) {
			try {
				org.json.JSONObject json = new org.json.JSONObject(strResponse);
				System.err.println(json);
				final String code = JSONParserUtil.getString(json, "code");
				result.setCode(code);
				result.setMessage(JSONParserUtil.getString(json, "message"));
				/*
				 * if (code.equalsIgnoreCase("S00")) { org.json.JSONObject
				 * details = JSONParserUtil.getObject(json, "details");
				 * result.setTransactionDate(JSONParserUtil.getString(details,
				 * "transactionDate"));
				 * result.setPaymentId(JSONParserUtil.getString(details,
				 * "paymentId"));
				 * result.setMerchantRefNo(JSONParserUtil.getString(details,
				 * "merchantRefNo"));
				 * result.setStatus(Status.valueOf(JSONParserUtil.getString(
				 * details, "status")));
				 * result.setAmount(JSONParserUtil.getString(details,
				 * "amount")); }
				 */
			} catch (JSONException e) {
				e.printStackTrace();
				result.setMessage("Service Unavailable");
			}
		}
		return result;
	}

	@Override
	public S2SResponse besconS2SCall(S2SRequest dto) {
		S2SResponse result = new S2SResponse();
		try {
			Client client = Client.create();
			WebResource webResource = client.resource(dto.getS2sCallUrl());
			client.addFilter(new LoggingFilter(System.out));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("" + dto.toJSON())).post(ClientResponse.class, dto.toJSON());
			String strResponse = response.getEntity(String.class);
			System.err.print("strResponse ----------------  " + strResponse);
			if (strResponse != null) {
				org.json.JSONObject json = new org.json.JSONObject(strResponse);
				System.err.println(json);
				if (json != null) {
					org.json.JSONObject resp = JSONParserUtil.getObject(json, "Response");
					result.setReceiptId(resp.getString("ReceiptId"));
					result.setStatus(resp.getString("Status"));
					result.setTransactiondatetime(resp.getString("transactiondatetime"));
					result.setTransactionpostingdatetime(resp.getString("transactionpostingdatetime"));
					result.setWssTransactionID(resp.getString("WSSTransactionID"));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public S2SResponse besconRuralS2SCall(S2SRequest dto) {
		S2SResponse result = new S2SResponse();
		try {
			Client client = Client.create();
			MultivaluedMapImpl formData = new MultivaluedMapImpl();
			formData.add("msg", dto.getMsg());
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource("http://123.201.131.117:88/s2scall.aspx");
			ClientResponse response = resource.post(ClientResponse.class, formData);
			String strResponse = response.getEntity(String.class);
			System.err.print("strResponse ----------------  " + strResponse);
			if (strResponse != null) {
				org.json.JSONObject json = new org.json.JSONObject(strResponse);
				System.err.println(json);
				if (json != null) {
					org.json.JSONObject resp = JSONParserUtil.getObject(json, "Response");
					result.setReceiptId(resp.getString("ReceiptId"));
					result.setStatus(resp.getString("status"));
					result.setTransactiondatetime(resp.getString("transactiondatetime"));
					result.setTransactionpostingdatetime(resp.getString("transactionpostingdatetime"));
					result.setWssTransactionID(resp.getString("WSSTransactionID"));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
}
