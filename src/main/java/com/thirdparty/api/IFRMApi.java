package com.thirdparty.api;

import com.thirdparty.model.FRMRequestDTO;
import com.thirdparty.model.FRMResponseDTO;

public interface IFRMApi {

	FRMResponseDTO frmDecideFundTransfer(FRMRequestDTO requestDTO);

	FRMResponseDTO frmEnqueueFundTransfer(FRMRequestDTO requestDTO);

	FRMResponseDTO frmDecideRegistration(FRMRequestDTO requestDTO);

	FRMResponseDTO frmEnqueueRegistration(FRMRequestDTO requestDTO);

	FRMResponseDTO frmDecideLogin(FRMRequestDTO requestDTO);

	FRMResponseDTO frmEnqueueLogin(FRMRequestDTO requestDTO);

}
