package com.thirdparty.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.thirdparty.api.IFRMApi;
import com.thirdparty.model.FRMRequestDTO;
import com.thirdparty.model.FRMResponseDTO;

@RequestMapping("/frm")
@Controller
public class FRMController {

	private final IFRMApi frmApi;

	public FRMController(IFRMApi frmApi) {
		this.frmApi= frmApi;
	}
	
	@RequestMapping(value = "/decideFundTransfer", method = RequestMethod.POST,
			consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<FRMResponseDTO> decideFundTransfer(@RequestBody FRMRequestDTO requestDTO) {
		FRMResponseDTO response = new FRMResponseDTO();
		response = frmApi.frmDecideFundTransfer(requestDTO);
		return new ResponseEntity<FRMResponseDTO>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/enqueueFundTransfer", method = RequestMethod.POST,
			consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<FRMResponseDTO> enqueueFundTransfer(@RequestBody FRMRequestDTO requestDTO) {
		FRMResponseDTO response = new FRMResponseDTO();
		response = frmApi.frmEnqueueFundTransfer(requestDTO);
		return new ResponseEntity<FRMResponseDTO>(response, HttpStatus.OK);
	}

	@RequestMapping(value = "/decideRegistration", method = RequestMethod.POST,
			consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<FRMResponseDTO> decideRegistration(@RequestBody FRMRequestDTO requestDTO) {
		FRMResponseDTO response = new FRMResponseDTO();
		response = frmApi.frmDecideRegistration(requestDTO);
		return new ResponseEntity<FRMResponseDTO>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/enqueueRegistration", method = RequestMethod.POST,
			consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<FRMResponseDTO> enqueueRegistration(@RequestBody FRMRequestDTO requestDTO) {
		FRMResponseDTO response = new FRMResponseDTO();
		response = frmApi.frmEnqueueRegistration(requestDTO);
		return new ResponseEntity<FRMResponseDTO>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/decideLogin", method = RequestMethod.POST,
			consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<FRMResponseDTO> decideLogin(@RequestBody FRMRequestDTO requestDTO) {
		FRMResponseDTO response = new FRMResponseDTO();
		response = frmApi.frmDecideLogin(requestDTO);
		return new ResponseEntity<FRMResponseDTO>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/enqueueLogin", method = RequestMethod.POST,
			consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<FRMResponseDTO> enqueueLogin(@RequestBody FRMRequestDTO requestDTO) {
		FRMResponseDTO response = new FRMResponseDTO();
		response = frmApi.frmEnqueueLogin(requestDTO);
		return new ResponseEntity<FRMResponseDTO>(response, HttpStatus.OK);
	}
}
