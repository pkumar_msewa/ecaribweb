package com.thirdparty.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.payqwikweb.api.IAuthenticationApi;
import com.payqwikweb.app.api.ILoadMoneyApi;
import com.payqwikweb.app.api.ITravelBusApi;
import com.payqwikweb.app.api.ITravelFlightApi;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.ResponseStatus;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.busdto.BookTicketReq;
import com.payqwikweb.app.model.busdto.SaveSeatDetailsDTO;
import com.payqwikweb.app.model.flight.dto.PaymentType;
import com.payqwikweb.app.model.flight.request.FligthBookReq;
import com.payqwikweb.app.model.request.AirBookRQ;
import com.payqwikweb.app.model.request.CreateJsonRequestFlight;
import com.payqwikweb.app.model.response.EBSRedirectResponse;
import com.payqwikweb.app.model.response.LoadMoneyResponse;
import com.payqwikweb.app.model.response.RegistrationResponse;
import com.payqwikweb.model.app.request.LoadMoneyFlightRequest;
import com.payqwikweb.model.app.request.TravellerFlightDetails;
import com.payqwikweb.model.error.bus.BusRequestError;
import com.payqwikweb.model.web.Status;
import com.payqwikweb.util.APIUtils;
import com.payqwikweb.validation.bus.BusRequestValidation;
import com.thirdparty.api.ISDKPaymentApi;
import com.thirdparty.model.BusResponseDTO;
import com.thirdparty.model.MerchantRegisterDTO;
import com.thirdparty.model.ResponseDTO;

@Controller
@RequestMapping("/Api/{version}/{role}/{device}/{language}/sdk/api/payment")
public class SDKPaymentController {
	
	private final ISDKPaymentApi sdkPaymentApi;
	private final ILoadMoneyApi loadMoneyApi;
	private final ITravelFlightApi travelflightapi;
	private final ITravelBusApi travelBusApi;
	private final IAuthenticationApi authenticationApi;
	private final BusRequestValidation validation;

	public SDKPaymentController(ISDKPaymentApi sdkPaymentApi,ILoadMoneyApi loadMoneyApi, ITravelFlightApi travelflightapi,
			ITravelBusApi travelBusApi, IAuthenticationApi authenticationApi, BusRequestValidation validation) {
		this.sdkPaymentApi = sdkPaymentApi;
		this.loadMoneyApi = loadMoneyApi;
		this.travelflightapi = travelflightapi;
		this.travelBusApi = travelBusApi;
		this.authenticationApi = authenticationApi;
		this.validation = validation;
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/Flight/BookTicket", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<String> flightCheckOut(HttpSession session, @RequestBody AirBookRQ req) {
		try {
			JSONObject ticketdetails = CreateJsonRequestFlight.createMobilejsonForTicket(req);
			FligthBookReq flightBookReq = new FligthBookReq();
			flightBookReq.setSessionId(req.getSessionId());
			flightBookReq.setPaymentAmount(req.getPaymentDetails().getBookingAmount() + "");
			flightBookReq.setTicketDetails(ticketdetails.toString());
			flightBookReq.setPaymentMethod(req.getPaymentMethod());
			flightBookReq.setEmail(req.getEmailAddress());
			flightBookReq.setMobile(req.getMobileNumber());
			flightBookReq.setSource(req.getFlightSearchDetails().get(0).getOrigin());
			flightBookReq.setDestination(req.getFlightSearchDetails().get(0).getDestination());
			flightBookReq.setBaseFare(ticketdetails.getString("basicFare"));
			String paymentInitcode = null;
			String flightBookingInitiate = null;

			List<TravellerFlightDetails> travellerFlightDetails = req.getTravellerDetails();
			List<TravellerFlightDetails> travellerFlightDetailsReturn = new ArrayList<>();
			if (req.getBookSegments().size() == 2) {
				travellerFlightDetailsReturn.addAll(travellerFlightDetails);
			}
			travellerFlightDetailsReturn.addAll(travellerFlightDetails);

			if (req.getPaymentMethod().equalsIgnoreCase(PaymentType.Wallet.getKey())) {
				flightBookingInitiate = travelflightapi.flightBookingInitiate(flightBookReq,
						travellerFlightDetailsReturn);
				JSONObject objflightBookingInitiate = new JSONObject(flightBookingInitiate);
				String codeflightBookingInitiate = objflightBookingInitiate.getString("code");
				if (codeflightBookingInitiate.equalsIgnoreCase("S00")) {
					String transactionRefNo = objflightBookingInitiate.getJSONObject("details")
							.getString("transactionRefNo");
					req.setTransactionId(transactionRefNo);
				}
				paymentInitcode = codeflightBookingInitiate;
			} else if (req.getPaymentMethod().equalsIgnoreCase(PaymentType.EBS.getKey())) {
				if (req.getTransactionId() != null || !req.getTransactionId().isEmpty()) {
					paymentInitcode = ResponseStatus.SUCCESS.getValue();
				}

			} else if (req.getPaymentMethod().equalsIgnoreCase(PaymentType.VNet.getKey())) {

				if (req.getTransactionId() != null || !req.getTransactionId().isEmpty()) {
					paymentInitcode = ResponseStatus.SUCCESS.getValue();
				}
			}

			if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(paymentInitcode)) {
				req.setTravellerDetails(travellerFlightDetails);
				String airRePriceRQ = travelflightapi.AirBookRQForMobileConecting(req);

				JSONObject obj = new JSONObject(airRePriceRQ);
				String code = obj.getString("code");
				if (code.equalsIgnoreCase("S00")) {
					String transactionRefNomdex = obj.getJSONObject("details").getString("transactionRefNo");
					JSONArray tickets = obj.getJSONObject("details").getJSONObject("bookingDetail")
							.getJSONObject("pnrDetail").getJSONArray("tickets");
					String ticketNumber = "";
					for (int i = 0; i < tickets.length(); i++) {
						ticketNumber = tickets.getJSONObject(i).getString("ticketNumber");
						travellerFlightDetailsReturn.get(i).setTicketNo(ticketNumber);
					}
					String bookingRefId = obj.getJSONObject("details").getString("bookingRefId");
					JSONArray objccpnrs = obj.getJSONObject("details").getJSONObject("bookingDetail")
							.getJSONObject("pnrDetail").getJSONArray("pnrs");
					String pnrNo = objccpnrs.getJSONObject(0).getString("pnr");
					flightBookReq.setBookingRefId(bookingRefId);
					flightBookReq.setMdexTxnRefNo(transactionRefNomdex);
					flightBookReq.setStatus(Status.Booked.getValue());
					flightBookReq.setTxnRefno(req.getTransactionId());
					flightBookReq.setSuccess(true);
					flightBookReq.setFlightStatus(Status.Booked.getValue());
					flightBookReq.setPnrNo(pnrNo);
					String flightBookingSucess = null;
					if (req.getPaymentMethod().equalsIgnoreCase(PaymentType.Wallet.getKey())) {
						flightBookingSucess = travelflightapi.flightBookingSucess(flightBookReq,
								travellerFlightDetailsReturn);
					} else {
						flightBookingSucess = travelflightapi.flightPaymentGatewaySucess(flightBookReq,
								req.getTravellerDetails());
					}
					JSONObject flightBookingSucessobj = new JSONObject(flightBookingSucess);
					String flightBookingSucesscode = flightBookingSucessobj.getString("code");
					if (flightBookingSucesscode.equalsIgnoreCase("S00")) {
						return new ResponseEntity<String>("" + flightBookingSucess, HttpStatus.OK);
					} else {
						/*
						 * fligthBookReq.setSuccess(false);
						 * travelflightapi.flightBookingSucess(fligthBookReq,req
						 * .getTravellerDetails());
						 */
						return new ResponseEntity<String>("" + flightBookingSucess, HttpStatus.OK);
					}
				} else {
					flightBookReq.setTxnRefno(req.getTransactionId());
					flightBookReq.setSuccess(false);
					if (req.getPaymentMethod().equalsIgnoreCase(PaymentType.Wallet.getKey())) {
						travelflightapi.flightBookingSucess(flightBookReq, req.getTravellerDetails());
					} else {
						travelflightapi.flightPaymentGatewaySucess(flightBookReq, req.getTravellerDetails());
					}
					return new ResponseEntity<String>(airRePriceRQ, HttpStatus.OK);
				}
			} else {
				return new ResponseEntity<String>(flightBookingInitiate, HttpStatus.OK);
			}

		} catch (Exception e) {
			e.printStackTrace();

			return new ResponseEntity<String>("" + APIUtils.getFailedJSON().toString(), HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/Flight/InitiateLoadMoney", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<LoadMoneyResponse> initiateLoadMoney(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @ModelAttribute LoadMoneyFlightRequest dto,
			HttpServletRequest request, HttpServletResponse response, ModelMap modelMap, HttpSession session) {
		LoadMoneyResponse resp = new LoadMoneyResponse();
		if (version.equalsIgnoreCase("v1")) {
			if (role.equalsIgnoreCase("User")) {
				if (device.equalsIgnoreCase("Android") || device.equalsIgnoreCase("IOS")) {
					if (language.equalsIgnoreCase("EN")) {
						resp.setSuccess(false);
						resp = loadMoneyApi.SplitpaymentMoneyRequest(dto);
					} else {
						resp.setSuccess(false);
						resp.setMessage("Only EN locale available");
					}
				} else {
					resp.setSuccess(false);
					resp.setMessage("Unauthorized Device");
				}
			} else {
				resp.setSuccess(false);
				resp.setMessage("Unauthorized Role");
			}
		} else {
			resp.setSuccess(false);
			resp.setMessage("Invalid Version");
		}
		return new ResponseEntity<LoadMoneyResponse>(resp, HttpStatus.OK);
	}

	@RequestMapping(value = "/Flight/RedirectSDK", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<EBSRedirectResponse> redirectLoadMoneySDK(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @ModelAttribute EBSRedirectResponse redirectResponse,
			Model model) {
		EBSRedirectResponse newResponse = new EBSRedirectResponse();
		ResponseDTO result = null;
		if (version.equalsIgnoreCase("v1")) {
			if (role.equalsIgnoreCase("User")) {
				if (device.equalsIgnoreCase("Android") || device.equalsIgnoreCase("IOS")) {
					if (language.equalsIgnoreCase("EN")) {
						result = loadMoneyApi.verifyEBSTransaction(redirectResponse);
						if (result != null) {
							String code = result.getCode();
							if (code.equalsIgnoreCase("S00")) {
								redirectResponse.setSuccess(true);
								redirectResponse.setResponseCode("0");
							} else {
								redirectResponse.setSuccess(false);
								redirectResponse.setResponseCode("1");
							}
							newResponse = loadMoneyApi.processRedirectSDKSplitpaymentMoney(redirectResponse);
						}
					} else {
						newResponse.setSuccess(false);
						newResponse.setError("Only EN locale available");
					}
				} else {
					newResponse.setSuccess(false);
					newResponse.setError("Unauthorized Device");
				}
			} else {
				newResponse.setSuccess(false);
				newResponse.setError("Unauthorized Role");
			}
		} else {
			newResponse.setSuccess(false);
			newResponse.setError("Invalid Version");
		}
		return new ResponseEntity<EBSRedirectResponse>(newResponse, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/BookTicketUpdated", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<BusResponseDTO> bookTicketUpdated(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody BookTicketReq dto,
			@RequestHeader(value = "hash", required = false) String hash) {

		BusResponseDTO resp = new BusResponseDTO();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				String authority = authenticationApi.getAuthorityFromSession(dto.getSessionId(), Role.USER);
				if (authority != null) {
					try {
						BusRequestError busError = validation.bookTicketValUpdated(dto);
						if (busError.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {

							resp = travelBusApi.initPayment(dto);

							if (resp.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {

								dto.setVpqTxnId(resp.getTransactionRefNo());
								resp = travelBusApi.bookTicketUpdated(dto);

								if (!(ResponseStatus.SUCCESS.getKey().equalsIgnoreCase(resp.getMdexStatus()))) {
									resp.setMessage("Something went wrong. Please try later");
								} else if (ResponseStatus.SUCCESS.getKey().equalsIgnoreCase(resp.getMdexStatus())) {
									if (!(ResponseStatus.SUCCESS.getKey().equalsIgnoreCase(resp.getStatus()))) {
										resp.setMessage(
												"Your Ticket Is Booked.But Payment Not Successful.Please Contact Customer Care");
									}
								}
							} else {
								resp.setCode(ResponseStatus.FAILURE.getValue());
								resp.setStatus(ResponseStatus.FAILURE.getKey());
								// resp.setMessage("");
							}

						} else {
							resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
							resp.setStatus(ResponseStatus.BAD_REQUEST.getKey());
							resp.setMessage(busError.getMessage());
						}
						return new ResponseEntity<BusResponseDTO>(resp, HttpStatus.OK);

					} catch (Exception e) {
						System.out.println(e);
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
						return new ResponseEntity<BusResponseDTO>(resp, HttpStatus.OK);
					}
				} else {
					resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
					resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
					resp.setMessage("Session expired");
				}
			} else {
				resp.setCode("F00");
				resp.setMessage("Unknown device");
				resp.setStatus("FAILED");
				resp.setDetails(APIUtils.getFailedJSON().toString());
			}
		} else {
			resp.setCode("F00");
			resp.setMessage("Unauthorised access");
			resp.setStatus("FAILED");
			resp.setDetails(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<BusResponseDTO>(resp, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/saveSeatDetails", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<BusResponseDTO> saveSeatDetails(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody SaveSeatDetailsDTO dto,
			@RequestHeader(value = "hash", required = false) String hash) {

		BusResponseDTO resp=new BusResponseDTO();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					try {
						resp=sdkPaymentApi.saveSeatDetails(dto,device);
						return new ResponseEntity<BusResponseDTO>(resp, HttpStatus.OK);
					} catch (Exception e) {
						System.out.println(e);
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
						return new ResponseEntity<BusResponseDTO>(resp, HttpStatus.OK);
					}
			}
			else {
				resp.setCode("F00");
				resp.setMessage("Unknown device");
				resp.setStatus("FAILED");
				resp.setDetails(APIUtils.getFailedJSON().toString());
			}
		}else {
			resp.setCode("F00");
			resp.setMessage("Unauthorised access");
			resp.setStatus("FAILED");
			resp.setDetails(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<BusResponseDTO>(resp, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/MerchantRegistration", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<RegistrationResponse> processRegistration(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody MerchantRegisterDTO dto,
			@RequestHeader(value = "hash", required = false) String hash) {
		RegistrationResponse result = new RegistrationResponse();
		try{
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					result = sdkPaymentApi.register(dto,device);
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unknown device");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else{
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("Unauthorised access");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
	  } catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<RegistrationResponse>(result, HttpStatus.OK);

	}
	
	@RequestMapping(value = "/MerchantResendOTP", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<RegistrationResponse> processResendOTP(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody MerchantRegisterDTO dto,
			@RequestHeader(value = "hash", required = false) String hash) {
		RegistrationResponse result = new RegistrationResponse();
		try{
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					result = sdkPaymentApi.resendOTP(dto,device);
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unknown device");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else{
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("Unauthorised access");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
	  } catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<RegistrationResponse>(result, HttpStatus.OK);

	}

}
