package com.thirdparty.controller;

import com.payqwik.visa.util.QRRequestDTO;
import com.payqwik.visa.util.QRResponseDTO;
import com.payqwik.visa.util.VisaMerchantRequest;
import com.payqwik.visa.util.VisaModel;
import com.payqwikweb.app.api.IAdminApi;
import com.payqwikweb.app.model.response.AddMerchantResponse;
import com.payqwikweb.app.model.response.VisaSignUpResponse;
import com.payqwikweb.model.error.VisaMerchantError;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/M2P")
public class M2PController {

    private final IAdminApi appAdminApi;

    public M2PController(IAdminApi appAdminApi) {
        this.appAdminApi = appAdminApi;
    }

    @RequestMapping(value = { "/AddVisaMerchant" }, method = RequestMethod.POST,
            produces = {
                    MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
    ResponseEntity<VisaSignUpResponse> processVisaMerchantRegistration(@RequestBody VisaMerchantRequest merchant,
                                                                       HttpServletRequest request,
                                                                       HttpServletResponse response, HttpSession session, Model model){

               VisaSignUpResponse m2pResponse = appAdminApi.merchantSignOffAtM2P(merchant);
                return new ResponseEntity<>(m2pResponse,HttpStatus.OK) ;

    }


    @RequestMapping(value = { "/AddLiveVisaMerchant" }, method = RequestMethod.POST,
            produces = {MediaType.APPLICATION_JSON_VALUE })
    ResponseEntity<String> processLiveVisaMerchantRegistration(@RequestBody VisaModel visa,
                                                                       HttpServletRequest request,
                                                                       HttpServletResponse response, HttpSession session, Model model){
    	
    	String m2pResponse = appAdminApi.merchantSignOffAtM2P(visa.getRequest());
        return new ResponseEntity<>(m2pResponse,HttpStatus.OK);
    }


    @RequestMapping(value = { "/GetQRCode" }, method = RequestMethod.POST)
    ResponseEntity<QRResponseDTO> getQRCode(@RequestBody QRRequestDTO req,
                                           HttpServletRequest request, HttpServletResponse response, HttpSession session) {
        QRResponseDTO resp = appAdminApi.getQRResponseFromM2P(req);
        System.err.println("resp::::"+resp);
        resp.setCode("S00");
        resp.setMessage("successfully qr code generated..");
        resp.setEntityId(resp.getEntityId());
        resp.setQrCode(resp.getQrCode());
        return new ResponseEntity<QRResponseDTO>(resp,HttpStatus.OK);
    }

    @RequestMapping(value = { "/GetLiveQRCode" }, method = RequestMethod.POST)
    ResponseEntity<QRResponseDTO> getLiveQRCode(@RequestBody QRRequestDTO req,
                                           HttpServletRequest request, HttpServletResponse response, HttpSession session) {
        QRResponseDTO resp = appAdminApi.getLiveQRResponseFromM2P(req);
        System.err.println("resp::::"+resp);
        resp.setCode("S00");
        resp.setMessage("successfully qr code generated..");
        resp.setEntityId(resp.getEntityId());
        resp.setQrCode(resp.getQrCode());
        return new ResponseEntity<QRResponseDTO>(resp,HttpStatus.OK);
    }


}
