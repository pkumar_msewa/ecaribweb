package com.thirdparty.controller;

import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.gcm.model.CronNotificationDTO;
import com.gcm.utils.APIConstants;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;


@Controller
@RequestMapping("/Send")
public class SendNotificationController implements MessageSourceAware {

	private MessageSource messageSource;

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	@RequestMapping(value = "/Notification", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE },consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<String> sendNotification(@RequestBody CronNotificationDTO dto) {
		  try {
			  System.err.println("inside send notification");
				Client client = Client.create();
				client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(APIConstants.SEND_NOTIFICATION);
				ClientResponse clientResponse = webResource.accept("application/json").type("application/json").header("Authorization","key="+APIConstants.APP_KEY).post(ClientResponse.class, dto.toJSON());
			 String strResponse = clientResponse.getEntity(String.class);
			 System.err.println("response of gcm ::" + strResponse);
			if (clientResponse.getStatus() == 200) {
				String response = strResponse;
				return new ResponseEntity<String>(response, HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>("", HttpStatus.OK);
		}
		  return new ResponseEntity<String>("", HttpStatus.OK);
	}
}
