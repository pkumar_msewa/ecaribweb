package com.thirdparty.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.instantpay.api.IValidationApi;
import com.instantpay.model.request.Format;
import com.instantpay.model.request.Mode;
import com.instantpay.model.request.ValidationRequest;
import com.instantpay.util.InstantPayConstants;
import com.payqwikweb.thirdparty.request.BalanceRequest;
import com.payqwikweb.thirdparty.request.ServicesRequest;
import com.payqwikweb.thirdparty.request.StatusCheckRequest;
import com.payqwikweb.thirdparty.request.TransactionRequest;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.thirdparty.model.ResponseDTO;
import com.thirdparty.model.ValidationResponseDTO;

@Controller
@RequestMapping("/InstantPay")
public class InstantPayController implements MessageSourceAware {
	
	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private MessageSource messageSource;
	private final IValidationApi ivalidationApi;

	public InstantPayController(IValidationApi ivalidationApi) {
		this.ivalidationApi = ivalidationApi;
	}	
	
	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	
	
	}

	@RequestMapping(value = "/Transaction/Process", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<String> processTransaction(TransactionRequest dto) {
		try {
			String stringResponse = "";
			Client c = Client.create();
			c.addFilter(new LoggingFilter(System.out));
			WebResource resource = c.resource(InstantPayConstants.URL_TRANSACTION)
					.queryParam(InstantPayConstants.API_KEY_TOKEN, InstantPayConstants.getToken())
					.queryParam(InstantPayConstants.API_KEY_SPKEY, (dto.getSpKey() == null) ? "" : dto.getSpKey())
					.queryParam(InstantPayConstants.API_KEY_AGENTID, (dto.getAgentId() == null) ? "" : dto.getAgentId())
					.queryParam(InstantPayConstants.API_KEY_ACCOUNT, (dto.getAccount() == null) ? "" : dto.getAccount())
					.queryParam(InstantPayConstants.API_KEY_AMOUNT, (dto.getAmount() == null) ? "" : dto.getAmount())
					.queryParam(InstantPayConstants.API_KEY_OPTIONAL1,
							(dto.getOptional1() == null) ? "" : dto.getOptional1())
					.queryParam(InstantPayConstants.API_KEY_OPTIONAL2,
							(dto.getOptional2() == null) ? "" : dto.getOptional2())
					.queryParam(InstantPayConstants.API_KEY_OPTIONAL3,
							(dto.getOptional3() == null) ? "" : dto.getOptional3())
					.queryParam(InstantPayConstants.API_KEY_OPTIONAL4,
							(dto.getOptional4() == null) ? "" : dto.getOptional4())
					.queryParam(InstantPayConstants.API_KEY_OPTIONAL5,
							(dto.getOptional5() == null) ? "" : dto.getOptional5())
					.queryParam(InstantPayConstants.API_KEY_FORMAT, InstantPayConstants.REQUEST_FORMAT)
			        .queryParam(InstantPayConstants.API_KEY_OPTIONAL6,
					        (dto.getOptional6() == null) ? "" : dto.getOptional6())
			        .queryParam(InstantPayConstants.API_KEY_OPTIONAL7,
					        (dto.getOptional7() == null) ? "" : dto.getOptional7())
			        .queryParam(InstantPayConstants.API_KEY_OPTIONAL8,
					        (dto.getOptional8() == null) ? "" : dto.getOptional8())
			        .queryParam(InstantPayConstants.API_KEY_OPTIONAL9,
					        (dto.getOptional9() == null) ? "" : dto.getOptional9())
			        .queryParam(InstantPayConstants.API_KEY_BBPS_OUTLET_ID,
					        (dto.getOutletid() == null) ? "" : dto.getOutletid())
			        .queryParam(InstantPayConstants.API_KEY_PAYMENT_CHANNEL,
					        (dto.getPaymentchannel() == null) ? "" : dto.getPaymentchannel())
			        .queryParam(InstantPayConstants.API_KEY_PAYMENT_MODE,
					        (dto.getPaymentmode() == null) ? "" : dto.getPaymentmode())
			        .queryParam(InstantPayConstants.API_KEY_CUNSUMER_MOBILE,
					        (dto.getCustomermobile() == null) ? "" : dto.getCustomermobile());
			ClientResponse clientResponse = resource.get(ClientResponse.class);
			stringResponse = clientResponse.getEntity(String.class);
			logger.info("response from instantPay ::" + stringResponse);
			return new ResponseEntity<String>(stringResponse, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>("", HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/Transaction/VNet", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public String processTransaction() {
			String stringResponse = "";
		try {
			Client c = Client.create();
			c.addFilter(new LoggingFilter(System.out));
			WebResource resource = c.resource("http://219.65.65.171:9080/NASApp/BANA623WAR/BANKAWAY?Action.ShoppingMall.Login.Init=Y&BankId=029&MD=P&USER_LANG_ID=001&UserType=1&AppType=corporate");
			ClientResponse clientResponse = resource.get(ClientResponse.class);
			stringResponse = clientResponse.getEntity(String.class);

		} catch (Exception e) {
			e.printStackTrace();
//			return new ResponseEntity<String>("", HttpStatus.OK);
		}
		return stringResponse;
	}

	@RequestMapping(value = "/Validation/Process", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<String> processValidation(ValidationRequest dto) {
		try {
			String stringResponse = "";
			Client c = Client.create();
			c.addFilter(new LoggingFilter(System.out));
			WebResource resource = c.resource(InstantPayConstants.URL_TRANSACTION)
					.queryParam(InstantPayConstants.API_KEY_TOKEN, InstantPayConstants.getToken())
					.queryParam(InstantPayConstants.API_KEY_SPKEY, (dto.getSpKey() == null) ? "" : dto.getSpKey())
					.queryParam(InstantPayConstants.API_KEY_AGENTID, (dto.getAgentId() == null) ? "" : dto.getAgentId())
					.queryParam(InstantPayConstants.API_KEY_ACCOUNT, (dto.getAccount() == null) ? "" : dto.getAccount())
					.queryParam(InstantPayConstants.API_KEY_AMOUNT, (dto.getAmount() == null) ? "" : dto.getAmount())
					.queryParam(InstantPayConstants.API_KEY_OPTIONAL1,
							(dto.getOptional1() == null) ? "" : dto.getOptional1())
					.queryParam(InstantPayConstants.API_KEY_OPTIONAL2,
							(dto.getOptional2() == null) ? "" : dto.getOptional2())
					.queryParam(InstantPayConstants.API_KEY_OPTIONAL3,
							(dto.getOptional3() == null) ? "" : dto.getOptional3())
					.queryParam(InstantPayConstants.API_KEY_OPTIONAL4,
							(dto.getOptional4() == null) ? "" : dto.getOptional4())
					.queryParam(InstantPayConstants.API_KEY_OPTIONAL5,
							(dto.getOptional5() == null) ? "" : dto.getOptional5())
					.queryParam(InstantPayConstants.API_KEY_FORMAT, InstantPayConstants.REQUEST_FORMAT);
			ClientResponse clientResponse = resource.get(ClientResponse.class);
			stringResponse = clientResponse.getEntity(String.class);
			return new ResponseEntity<String>(stringResponse, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>("{}", HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/Services/Process", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<String> getServices(ServicesRequest dto, HttpServletRequest request, HttpServletResponse response,
			HttpSession session) {
		try {
			String stringResponse = "";
			Client c = Client.create();
			c.addFilter(new LoggingFilter(System.out));
			WebResource resource = c.resource(InstantPayConstants.URL_SERVICE)
					.queryParam(InstantPayConstants.API_KEY_TOKEN, InstantPayConstants.getToken())
					.queryParam(InstantPayConstants.API_KEY_TYPE, dto.getType())
					.queryParam(InstantPayConstants.API_KEY_FORMAT, InstantPayConstants.REQUEST_FORMAT);
			ClientResponse clientResponse = resource.get(ClientResponse.class);
			stringResponse = clientResponse.getEntity(String.class);
			return new ResponseEntity<String>(stringResponse, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>("{}", HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/StatusCheck/Process", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<String> checkStatus(StatusCheckRequest dto,HttpSession session) {
		try {
			String stringResponse = "";
			Client c = Client.create();
			c.addFilter(new LoggingFilter(System.out));
			WebResource resource = c.resource(InstantPayConstants.URL_STATUS)
					.queryParam(InstantPayConstants.API_KEY_TOKEN, InstantPayConstants.getToken())
					.queryParam(InstantPayConstants.API_KEY_AGENTID, dto.getAgentId())
					.queryParam(InstantPayConstants.API_KEY_FORMAT, InstantPayConstants.REQUEST_FORMAT);
			ClientResponse clientResponse = resource.get(ClientResponse.class);
			stringResponse = clientResponse.getEntity(String.class);
			return new ResponseEntity<String>(stringResponse, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>("{}", HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/Balance/Process", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<String> getBalance(BalanceRequest dto) {
		try {
			String stringResponse = "";
			Client c = Client.create();
			c.addFilter(new LoggingFilter(System.out));
			WebResource resource = c.resource(InstantPayConstants.URL_BALANCE)
					.queryParam(InstantPayConstants.API_KEY_TOKEN, InstantPayConstants.getToken())
					.queryParam(InstantPayConstants.API_KEY_FORMAT, InstantPayConstants.REQUEST_FORMAT);
			ClientResponse clientResponse = resource.get(ClientResponse.class);
			stringResponse = clientResponse.getEntity(String.class);
			return new ResponseEntity<String>(stringResponse, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>("{}", HttpStatus.OK);
		}
	}
	
	
	@RequestMapping(value = "/ValidateRequest/Process", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ValidationResponseDTO> getValidatedRequest(ValidationRequest dto) {
		ValidationResponseDTO response = new ValidationResponseDTO();
		try {
			dto.setMode(Mode.VALIDATE);
			dto.setAgentId(System.currentTimeMillis() + "");
			dto.setFormat(Format.JSON);
			dto.setToken(InstantPayConstants.getToken());
			dto.setSpKey(dto.getSpKey().substring(1));
			if (dto.getSpKey().equalsIgnoreCase("VATP")) {
				dto.setOptional5(InstantPayConstants.OUTLET_ID);
			}
			response = ivalidationApi.validateTransaction(dto);
			
			return new ResponseEntity<ValidationResponseDTO>(response, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ValidationResponseDTO>(response, HttpStatus.OK);
		}
	}

}
