package com.thirdparty.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.thirdparty.api.ISDKPaymentApi;
import com.thirdparty.model.PaymentDTO;
import com.thirdparty.model.SDKAuthenticationDTO;
import com.thirdparty.model.SDKAuthenticationResponse;
import com.thirdparty.model.SDKResponseDTO;
import com.thirdparty.model.VerifyDTO;
import com.thirdparty.model.error.CommonError;
import com.thirdparty.model.error.VerifyError;
import com.thirdparty.validation.MerchantValidation;
import com.thirdparty.validation.RegisterValidation;

@Controller
@RequestMapping("/sdk/api")
public class SDKRegistrationController {

	private final ISDKPaymentApi sdkPaymentApi;
	private final MerchantValidation merchantValidation;
	private final RegisterValidation registerValidation;

	public SDKRegistrationController(ISDKPaymentApi sdkPaymentApi, MerchantValidation merchantValidation,
			RegisterValidation registerValidation) {
		this.sdkPaymentApi = sdkPaymentApi;
		this.merchantValidation = merchantValidation;
		this.registerValidation = registerValidation;
	}

	@RequestMapping(value = "/authlogin", method = RequestMethod.POST)
	ResponseEntity<SDKAuthenticationResponse> authenticateMerchantAPI(@ModelAttribute SDKAuthenticationDTO dto,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) {
		CommonError error = merchantValidation.sdkCheckError(dto);
		SDKAuthenticationResponse apiResult = new SDKAuthenticationResponse();
		if (error.isValid()) {
			apiResult = sdkPaymentApi.authenticateMerchant(dto);
		}
		return new ResponseEntity<SDKAuthenticationResponse>(apiResult, HttpStatus.OK);
	}

	@RequestMapping(value = "/VerifyUserOTP", method = RequestMethod.POST)
	ResponseEntity<SDKResponseDTO> processVerifyOTP(@ModelAttribute VerifyDTO dto, ModelMap model,
			HttpSession session) {
		SDKResponseDTO result = new SDKResponseDTO();
		VerifyError error = registerValidation.checkError(dto);
		if (error.isValid()) {
			result = sdkPaymentApi.validateUserOTP(dto);
			if (result.getCode().equalsIgnoreCase("S00")) {
				PaymentDTO paymentDTO = new PaymentDTO();
				double netAmount = dto.getAmount();
				paymentDTO.setNetAmount(netAmount);
				paymentDTO.setWalletAmount(result.getAmount());
				if (netAmount <= result.getAmount()) {
					paymentDTO.setAmountToLoad(0);
					paymentDTO.setUseWallet(true);
				} else {
					paymentDTO.setAmountToLoad(Math.ceil(netAmount - result.getAmount()));
					paymentDTO.setUseWallet(false);
				}
				result.setPaymentDto(paymentDTO);
			}
		}
		return new ResponseEntity<SDKResponseDTO>(result, HttpStatus.OK);
	}

 }