package com.thirdparty.controller;

import java.io.BufferedReader;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.payqwikweb.app.api.ILoadMoneyApi;
import com.payqwikweb.app.api.ILogoutApi;
import com.payqwikweb.app.api.ITransactionApi;
import com.payqwikweb.app.api.IUserApi;
import com.payqwikweb.app.model.ResponseStatus;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.UpiPayRequest;
import com.payqwikweb.app.model.request.LoadMoneyRequest;
import com.payqwikweb.app.model.request.LogoutRequest;
import com.payqwikweb.app.model.request.TransactionRequest;
import com.payqwikweb.app.model.request.UserDetailsRequest;
import com.payqwikweb.app.model.response.EBSRedirectResponse;
import com.payqwikweb.app.model.response.LoadMoneyResponse;
import com.payqwikweb.app.model.response.UserDetailsResponse;
import com.payqwikweb.model.web.Status;
import com.payqwikweb.util.Authorities;
import com.payqwikweb.util.ConvertUtil;
import com.payqwikweb.util.JSONParserUtil;
import com.payqwikweb.util.ModelMapKey;
import com.payqwikweb.util.SecurityUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;
//import com.sun.org.apache.xpath.internal.operations.Mod;
import com.thirdparty.api.IMerchantApi;
import com.thirdparty.model.AuthenticateDTO;
import com.thirdparty.model.AuthenticationResponse;
import com.thirdparty.model.BescomAuthenthicationDTO;
import com.thirdparty.model.ChangePasswordDTO;
import com.thirdparty.model.CheckUpiStatus;
import com.thirdparty.model.ForgotPasswordDTO;
import com.thirdparty.model.LoginDTO;
import com.thirdparty.model.PaymentDTO;
import com.thirdparty.model.PaymentResponse;
import com.thirdparty.model.RegisterDTO;
import com.thirdparty.model.ResponseDTO;
import com.thirdparty.model.S2SRequest;
import com.thirdparty.model.S2SResponse;
import com.thirdparty.model.StatusCheckDTO;
import com.thirdparty.model.StatusResponse;
import com.thirdparty.model.VerifyDTO;
import com.thirdparty.model.error.ChangePasswordError;
import com.thirdparty.model.error.CommonError;
import com.thirdparty.model.error.ForgotPasswordError;
import com.thirdparty.model.error.LoginError;
import com.thirdparty.model.error.RegisterError;
import com.thirdparty.model.error.VerifyError;
import com.thirdparty.validation.LoginValidation;
import com.thirdparty.validation.MerchantValidation;
import com.thirdparty.validation.RegisterValidation;
import com.upi.model.requet.MerchantApprovedResponse;
import com.upi.model.requet.UPIRedirect;
import com.upi.model.response.MerchantMetaVPAResponse;
import com.upi.model.response.UPIRedirectResponse;

@Controller
@RequestMapping("/ws/api")
public class MerchantPaymentController {

	private final IUserApi userApi;
	private final IMerchantApi merchantApi;
	private final MerchantValidation merchantValidation;
	private final LoginValidation loginValidation;
	private final RegisterValidation registerValidation;
	private final ILoadMoneyApi loadMoneyApi;
	private final ILogoutApi logoutApi;
	private final ITransactionApi transactionApi;

	public MerchantPaymentController(IUserApi userApi, IMerchantApi merchantApi, MerchantValidation merchantValidation,
			LoginValidation loginValidation, RegisterValidation registerValidation, ILoadMoneyApi loadMoneyApi,
			ILogoutApi logoutApi, ITransactionApi transactionApi) {
		this.userApi = userApi;
		this.merchantApi = merchantApi;
		this.merchantValidation = merchantValidation;
		this.loginValidation = loginValidation;
		this.registerValidation = registerValidation;
		this.loadMoneyApi = loadMoneyApi;
		this.logoutApi = logoutApi;
		this.transactionApi = transactionApi;
	}

	@RequestMapping(value = { "/", "/getLoginPage" }, method = RequestMethod.GET)
	public String getLoginPageGET(Model model) {
		model.addAttribute("login", new LoginDTO());
		return "ThirdParty/Login";
	}

	// @RequestMapping(value={"/{page}"},method=RequestMethod.GET)
	// public String getPage(@PathVariable(value = "page") String page,Model
	// model){
	// PaymentDTO paymentDTO = new PaymentDTO();
	// LoginDTO loginDTO = new LoginDTO();
	// RegisterDTO registerDTO = new RegisterDTO();
	// VerifyDTO verifyDTO = new VerifyDTO();
	// ChangePasswordDTO changePasswordDTO = new ChangePasswordDTO();
	// model.addAttribute("changePassword",changePasswordDTO);
	// model.addAttribute("verifyOTP",verifyDTO);
	// model.addAttribute("signUp",registerDTO);
	// model.addAttribute("pgRequest",paymentDTO);
	// model.addAttribute("login",loginDTO);
	// return "ThirdParty/"+page;
	// }

	@RequestMapping(value = "/getErrorPage", method = RequestMethod.GET)
	public String getErrorPageGET() {
		return "ThirdParty/Error";
	}

	@RequestMapping(value = "/startTest", method = RequestMethod.GET)
	public String getTestGET() {
		return "ThirdParty/Test";
	}



    @RequestMapping(value = { "/auth", "/validate" }, method = RequestMethod.POST)
	public String authenticateMerchant(@ModelAttribute AuthenticateDTO dto, HttpServletRequest request,
			HttpSession session) {
		dto.setIpAddress(request.getRemoteAddr());
		CommonError error = merchantValidation.checkError(dto);
		session.setAttribute("transactionID", dto.getTransactionID());
		if (error.isValid()) {
			AuthenticationResponse result = merchantApi.authenticateMerchant(dto);
			if (result.getCode().equals("S00")) {
				session.setAttribute("image", result.getImage());
				session.setAttribute("successURL", result.getSuccessURL());
				session.setAttribute("failureURL", result.getFailureURL());
				session.setAttribute("id", result.getMerchantId());
				session.setAttribute("amount", dto.getAmount());
				session.setAttribute("extra", dto.getAdditionalInfo());
				return "redirect:/ws/api/Login";
			} else {
				return "redirect:/ws/api/FailedAuthentication";
			}
		} else {
			return "redirect:/ws/api/FailedAuthentication";
		}
	}
	
	/*@RequestMapping(value = { "/auth", "/validate" }, method = RequestMethod.POST)
	public String authenticateMerchant(@ModelAttribute AuthenticateDTO dto, HttpServletRequest request,
			HttpSession session,ModelMap map) {
		dto.setIpAddress(request.getRemoteAddr());
		CommonError error = merchantValidation.checkError(dto);
		session.setAttribute("transactionID", dto.getTransactionID());
		if (error.isValid()) {
			AuthenticationResponse result = merchantApi.authenticateMerchant(dto);
	     
			session.setAttribute("image", result.getImage());
			session.setAttribute("successURL", result.getSuccessURL());
			session.setAttribute("failureURL", result.getFailureURL());
			session.setAttribute("id", result.getMerchantId());
			session.setAttribute("amount", dto.getAmount());
			session.setAttribute("extra", dto.getAdditionalInfo());		
		 if(!result.isUserExist()||dto.getUsername().isEmpty())
		  {	
			System.err.println("the code is ::" + result.getCode());
			if (result.getCode().equals("S00")) {
				return "redirect:/ws/api/Login";
			} else {
				return "redirect:/ws/api/FailedAuthentication";
			}
		  }
		 else{
			 map.addAttribute("otpUsername",dto.getUsername());
			 return "ThirdParty/VerifyOtpNew";
		 }
		
		} else {
			return "redirect:/ws/api/FailedAuthentication";
		}
	}
*/
	@RequestMapping(value = { "/mauth", "/mvalidate" }, method = RequestMethod.POST)
	ResponseEntity<AuthenticationResponse> authenticateMerchantAPI(@ModelAttribute AuthenticateDTO dto,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) {
		CommonError error = merchantValidation.checkError(dto);
		AuthenticationResponse apiResult = new AuthenticationResponse();
		if (error.isValid()) {
			apiResult = merchantApi.authenticateMerchant(dto);
		}
		return new ResponseEntity<AuthenticationResponse>(apiResult, HttpStatus.OK);
	}

	@RequestMapping(value = "/FailedAuthentication", method = RequestMethod.GET)
	public String failedAuthentication(Model model) {
		return "ThirdParty/Error";
	}

	@RequestMapping(value = { "/Login" }, method = RequestMethod.GET)
	public String getLoginPage(@RequestParam(value = ModelMapKey.MESSAGE, required = false) String msg,
			ModelMap model) {
		LoginDTO dto = new LoginDTO();
		if (msg != null) {
			model.addAttribute(ModelMapKey.MESSAGE, msg);
		}
		model.addAttribute("login", dto);
		return "ThirdParty/Login";
	}

	@RequestMapping(value = { "/Login" }, method = RequestMethod.POST)
	public String validateLoginRequest(@ModelAttribute("login") LoginDTO dto, HttpSession session, Model model) {
		LoginError error = loginValidation.checkError(dto);
		double netAmount = Double.parseDouble((String) session.getAttribute("amount"));
		ResponseDTO result = null;
		if (error.isValid()) {
			String token = (String) session.getAttribute("token");
			dto.setToken(token);
			result = merchantApi.authenticateUser(dto);
			if (result.getCode().equalsIgnoreCase("S00")) {
				PaymentDTO paymentDTO = new PaymentDTO();
				session.setAttribute("sessionId", result.getSessionId());
				paymentDTO.setNetAmount(netAmount);
				paymentDTO.setWalletAmount(result.getAmount());
				paymentDTO.setUseWallet(true);
				if (netAmount <= result.getAmount()) {
					paymentDTO.setAmountToLoad(0);
				} else {
					paymentDTO.setAmountToLoad(Math.ceil(netAmount - result.getAmount()));
				}
				model.addAttribute("balance", result.getAmount());
				model.addAttribute("username", result.getDetails());
				model.addAttribute("fullName", result.getInfo());
				model.addAttribute("pgRequest", paymentDTO);
				return "ThirdParty/Dashboard";
			} else {
				model.addAttribute(ModelMapKey.MESSAGE, result.getMessage());
				return "ThirdParty/Login";
			}
		} else {
			model.addAttribute(ModelMapKey.ERROR, error);
			return "ThirdParty/Login";
		}
	}

	@RequestMapping(value = { "/Process" }, method = RequestMethod.POST)
	public String processPaymentPage(@ModelAttribute("pgRequest") PaymentDTO dto, HttpSession session, Model model) {
		double amountToLoad = dto.getAmountToLoad();
		String message = null;
		boolean valid = false;
		String sessionId = (String) session.getAttribute("sessionId");
		System.out.println("sessionId=="+sessionId);
		if (sessionId != null) {
			UserDetailsRequest userDetailsRequest = new UserDetailsRequest();
			userDetailsRequest.setSessionId(sessionId);
			UserDetailsResponse userDetailsResponse = userApi.getUserDetails(userDetailsRequest, Role.USER);
			if (userDetailsResponse != null) {
				TransactionRequest transactionDTO = new TransactionRequest();
				transactionDTO.setAmount(dto.getNetAmount());
				transactionDTO.setSessionId(sessionId);
				transactionDTO.setSenderUsername(userDetailsResponse.getUsername());
				transactionDTO.setTransactionRefNo("C");
				ResponseDTO result = transactionApi.validateTransaction(transactionDTO);
				if (result != null) {
					if (result.getCode().equalsIgnoreCase("S00")) {
						try {
							JSONObject json = new JSONObject(result.getDetails());
							valid = JSONParserUtil.getBoolean(json, "valid");
							if (!valid) {
								message = JSONParserUtil.getString(json, "message");
							}
						} catch (JSONException ex) {
							ex.printStackTrace();
						}
					}
				}
				String authority = userDetailsResponse.getAuthority();
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
					if (valid) {
						if (amountToLoad > 0) {
							LoadMoneyRequest loadMoneyRequest = new LoadMoneyRequest();
							loadMoneyRequest.setAmount("" + amountToLoad);
							loadMoneyRequest.setSessionId(sessionId);
							loadMoneyRequest.setReturnUrl("http://66.207.206.54:8089/ws/api/LoadMoney/Redirect");
							loadMoneyRequest.setAddress(userDetailsResponse.getAddress());
							loadMoneyRequest.setEmail(userDetailsResponse.getEmail());
							loadMoneyRequest.setPhone(userDetailsResponse.getContactNo());
							loadMoneyRequest.setName(
									userDetailsResponse.getFirstName() + " " + userDetailsResponse.getLastName());
							LoadMoneyResponse loadMoneyResponse = loadMoneyApi.loadMoneyRequest(loadMoneyRequest);
							model.addAttribute("loadmoney", loadMoneyResponse);
							return "User/Pay";
						} else {
							return "redirect:/ws/api/Process/Payment";
						}
					}
					model.addAttribute(ModelMapKey.MESSAGE, message);
				}
			}
			// set again payment dto
			PaymentDTO paymentDTO = dto;
			model.addAttribute("balance", dto.getWalletAmount());
			model.addAttribute("username", userDetailsResponse.getContactNo());
			model.addAttribute("fullName",
					userDetailsResponse.getFirstName() + " " + userDetailsResponse.getLastName());
			model.addAttribute("pgRequest", paymentDTO);
			return "ThirdParty/Dashboard";
		}
		return "redirect:/ws/api/getLoginPage";
	}

	@RequestMapping(value = "/LoadMoney/Redirect", method = RequestMethod.POST)
	public String redirectLoadMoney(HttpServletRequest request, Model model) {
		EBSRedirectResponse redirectResponse = loadMoneyApi.processRedirectResponse(request);
		if (redirectResponse.isSuccess() || redirectResponse.getResponseCode().equals("0")) {
			return "ThirdParty/Loading";
		} else {
			model.addAttribute("msg", "Transaction failed, Please try again later.");
		}
		model.addAttribute("loadmoneyResponse", redirectResponse);
		return "User/Loading";
	}

	@RequestMapping(value = "/success", method = RequestMethod.POST)
	public String successPayment(PaymentResponse dto, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, Model model) {
		System.err.println("dto is ::" + dto);
		int success = 1;
		if (success == 1) {
			model.addAttribute(ModelMapKey.MESSAGE, dto.getDescription());
		} else {
			model.addAttribute(ModelMapKey.MESSAGE, "Your payment is failed,Try again later");
		}
		return "ThirdParty/Success";
	}

	@RequestMapping(value = { "/Process/Payment" }, method = RequestMethod.GET)
	public String processPayment(HttpSession session, Model model) {
		String sessionId = (String) session.getAttribute("sessionId");
		String token = (String) session.getAttribute("token");
		long id = (long) session.getAttribute("id");
		double netAmount = Double.parseDouble((String) session.getAttribute("amount"));
		UserDetailsRequest userDetailsRequest = new UserDetailsRequest();
		userDetailsRequest.setSessionId(sessionId);
		UserDetailsResponse userDetailsResponse = userApi.getUserDetails(userDetailsRequest, Role.USER);
		String authority = userDetailsResponse.getAuthority();
		if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
			PaymentDTO paymentDTO = new PaymentDTO();
			paymentDTO.setSessionId(sessionId);
			paymentDTO.setToken(token);
			paymentDTO.setNetAmount(netAmount);
			paymentDTO.setId(id);
			paymentDTO.setTransactionID((String) session.getAttribute("transactionID"));
			ResponseDTO result = merchantApi.processPayment(paymentDTO);

			if (result.getCode().equalsIgnoreCase("S00")) {
				System.err.println("URL : " + session.getAttribute("successURL"));
				System.err.println("orderID : " +  paymentDTO.getTransactionID());
				System.err.println("success : " + 1);
				System.err.println("description : " + "Payamet done");
				System.err.println("additionalInfo : " + session.getAttribute("extra"));
				System.err.println("failure url : " + session.getAttribute("failureURL"));
				
				model.addAttribute("url", (String) session.getAttribute("successURL"));
				model.addAttribute("orderID", paymentDTO.getTransactionID());
				model.addAttribute("success", "1");
				model.addAttribute("description", "Payment Successfully Processed ...");
				model.addAttribute("additionalInfo", session.getAttribute("extra"));
			} else {
				model.addAttribute("url", (String) session.getAttribute("failureURL"));
			}
			model.addAttribute("method", (id == 7827031) ? "get" : "post");
			model.addAttribute(ModelMapKey.MESSAGE, result.getDetails());
			LogoutRequest logoutRequest = new LogoutRequest();
			logoutRequest.setSessionId(sessionId);
			logoutApi.logout(logoutRequest, Role.USER);
			return "ThirdParty/Receipt";
		} else {
			return "redirect:/ws/api/Process/Payment";
		}
	}

	@RequestMapping(value = { "/processMpay" }, method = RequestMethod.POST)
	ResponseEntity<ResponseDTO> processPayment(@ModelAttribute PaymentDTO dto) {
		ResponseDTO result = new ResponseDTO();
		UserDetailsRequest userDetailsRequest = new UserDetailsRequest();
		userDetailsRequest.setSessionId(dto.getSessionId());
		UserDetailsResponse userDetailsResponse = userApi.getUserDetails(userDetailsRequest, Role.USER);
		String authority = userDetailsResponse.getAuthority();
		if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
			result = merchantApi.processPayment(dto);
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = { "/Register", "/SignUp" }, method = RequestMethod.GET)
	public String getRegistrationPage(Model model) {
		RegisterDTO dto = new RegisterDTO();
		model.addAttribute("signUp", dto);
		return "ThirdParty/SignUp";
	}

	@RequestMapping(value = { "/Register", "/SignUp" }, method = RequestMethod.POST)
	public String validateRegistrationRequest(@ModelAttribute("signUp") RegisterDTO dto, ModelMap model) {
		RegisterError error = registerValidation.checkError(dto);
		if (error.isValid()) {
			ResponseDTO result = merchantApi.registerUser(dto);
			String code = result.getCode();
			if (code.equalsIgnoreCase("S00")) {
				model.addAttribute(ModelMapKey.MESSAGE, result.getMessage());
				model.addAttribute("contactNo", dto.getContactNo());
				VerifyDTO verify = new VerifyDTO();
				verify.setContactNo(dto.getContactNo());
				model.addAttribute("verifyOTP", verify);
				return "ThirdParty/VerifyMobile";
			} else if (code.equalsIgnoreCase("F04")) {
				error.setContactNo(result.getMessage());
				error.setEmail(result.getDetails());
			}
		}
		model.addAttribute("error", error);
		return "ThirdParty/SignUp";
	}

	@RequestMapping(value = "/VerifyMobile", method = RequestMethod.GET)
	public String getOTPPage(ModelMap model) {
		VerifyDTO dto = new VerifyDTO();
		dto.setContactNo((String) model.get("contactNo"));
		model.addAttribute("verifyOTP", dto);
		return "ThirdParty/VerifyMobile";
	}

	@RequestMapping(value = "/VerifyMobile", method = RequestMethod.POST)
	public String processOTPVerification(@ModelAttribute("verifyOTP") VerifyDTO dto, ModelMap model) {
		VerifyError error = registerValidation.checkError(dto);
		if (error.isValid()) {
			ResponseDTO result = merchantApi.verifyOTP(dto);
			String code = result.getCode();
			String message = result.getMessage();
			if (code.equalsIgnoreCase("S00")) {
				model.addAttribute(ModelMapKey.MESSAGE, message);
				return "redirect:/ws/api/Login";
			} else if (code.equalsIgnoreCase("F04")) {
				model.addAttribute(ModelMapKey.MESSAGE, message);
			}
		} else {
			model.addAttribute(ModelMapKey.ERROR, error);
		}
		return "ThirdParty/VerifyMobile";
	}

	@RequestMapping(value = "/ForgotPassword", method = RequestMethod.GET)
	public String getForgotPasswordPage(ModelMap model) {
		ForgotPasswordDTO dto = new ForgotPasswordDTO();
		model.addAttribute("forgotPassword", dto);
		return "ThirdParty/ForgotPassword";
	}

	@RequestMapping(value = "/ForgotPassword", method = RequestMethod.POST)
	public String processForgotPassword(@ModelAttribute("forgotPassword") ForgotPasswordDTO dto, ModelMap model) {
		ForgotPasswordError error = registerValidation.checkError(dto);
		if (error.isValid()) {
			ResponseDTO result = merchantApi.forgotPassword(dto);
			if (result.getCode().equalsIgnoreCase("S00")) {
				model.addAttribute(ModelMapKey.MESSAGE, result.getMessage());
				VerifyDTO verifyDTO = new VerifyDTO();
				verifyDTO.setContactNo(dto.getUsername());
				model.addAttribute("forgotPasswordOTP", verifyDTO);
				return "ThirdParty/VerifyOTP";
			} else {
				error.setUsername("Contact No. not exists");
			}
		}
		model.addAttribute("error", error);
		return "ThirdParty/ForgotPassword";
	}

	@RequestMapping(value = "/VerifyOTP", method = RequestMethod.POST)
	public String processForgotPasswordOTP(@ModelAttribute("forgotPasswordOTP") VerifyDTO dto, ModelMap model) {
		VerifyError error = registerValidation.checkError(dto);
		if (error.isValid()) {
			ResponseDTO result = merchantApi.validateOTP(dto);
			if (result.getCode().equalsIgnoreCase("S00")) {
				model.addAttribute(ModelMapKey.MESSAGE, result.getMessage());
				ChangePasswordDTO changePasswordDTO = new ChangePasswordDTO();
				changePasswordDTO.setUsername(dto.getContactNo());
				changePasswordDTO.setOtp(dto.getOtp());
				model.addAttribute("changePassword", changePasswordDTO);
				return "ThirdParty/ChangePassword";
			} else {
				error.setOtp(result.getMessage());
			}
		}
		model.addAttribute("error", error);
		return "ThirdParty/VerifyOTP";
	}

	@RequestMapping(value = "/ChangePassword", method = RequestMethod.POST)
	public String processChangePassword(@ModelAttribute("changePassword") ChangePasswordDTO dto, ModelMap model) {
		ChangePasswordError error = registerValidation.checkError(dto);
		if (error.isValid()) {
			ResponseDTO result = merchantApi.changePassword(dto);
			if (result.getCode().equalsIgnoreCase("S00")) {
				model.addAttribute(ModelMapKey.MESSAGE, result.getMessage());
				return "redirect:/ws/api/Login";
			}
		}
		model.addAttribute("error", error);
		return "ThirdParty/ChangePassword";
	}

	@RequestMapping(value = "/Status", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<StatusResponse> checkTransactionStatus(@RequestBody StatusCheckDTO dto) {
		System.err.println("inside status api");
		StatusResponse statusResponse = new StatusResponse();
		CommonError error = merchantValidation.checkError(dto);
		if (error.isValid()) {
			System.err.println("error is not present");
			statusResponse = merchantApi.checkStatus(dto);
		} else {
			statusResponse.setCode("F04");
			statusResponse.setMessage(error.getMessage());
		}
		return new ResponseEntity<StatusResponse>(statusResponse, HttpStatus.OK);
	}

	@RequestMapping(value = "/VerifyUserOTP", method = RequestMethod.POST)
	public String processVerifyOTP(@ModelAttribute("verifyOTP") VerifyDTO dto, ModelMap model,HttpSession session) {
		VerifyError error = registerValidation.checkError(dto);
		if (error.isValid()) {
			ResponseDTO result = merchantApi.validateUserOTP(dto);
			if (result.getCode().equalsIgnoreCase("S00")) {
				PaymentDTO paymentDTO = new PaymentDTO();
				session.setAttribute("sessionId", result.getSessionId());
				double netAmount = Double.parseDouble((String) session.getAttribute("amount"));
				paymentDTO.setNetAmount(netAmount);
				paymentDTO.setWalletAmount(result.getAmount());
				paymentDTO.setUseWallet(true);
				if (netAmount <= result.getAmount()) {
					paymentDTO.setAmountToLoad(0);
				} else {
					paymentDTO.setAmountToLoad(Math.ceil(netAmount - result.getAmount()));
				}
				model.addAttribute("balance", result.getAmount());
				model.addAttribute("username", result.getDetails());
				model.addAttribute("fullName", result.getInfo());
				model.addAttribute("pgRequest", paymentDTO);
				return "ThirdParty/Dashboard";
			} else {
				error.setOtp(result.getMessage());
			}
		}
		model.addAttribute("error", error);
		return "ThirdParty/VerifyOtpNew";
	}
	
	// TODO Merchant Payment by UPI
	@RequestMapping(value = { "/upiAuth", "/upiValidate" }, method = RequestMethod.POST)
	public String authenticateUPIMerchant(@ModelAttribute BescomAuthenthicationDTO dto, HttpServletRequest request,
			HttpSession session, ModelMap model) {
		dto.setIpAddress(request.getRemoteAddr());
		try{
			CommonError error = merchantValidation.checkBescomError(dto);
			session.setAttribute("transactionID", dto.getMerchanttxnid());
			if (error.isValid()) {
				dto.setAdditionalcharges(ConvertUtil.convertAdditionalAmount(dto.getBillamount()));
				AuthenticationResponse result = merchantApi.authenticateUpiMerchant(dto);
				System.err.println("the code is ::" + result.getCode());
				if (result.getCode().equals("S00")) {
					double totalAmount = dto.getBillamount() + dto.getAdditionalcharges();
					model.addAttribute("txnid",  dto.getMerchanttxnid());
					model.addAttribute("name",  dto.getFirstname());
					model.addAttribute("phone",  dto.getPhone());
					model.addAttribute("email",  dto.getEmail());
					model.addAttribute("amount",  dto.getBillamount());
					model.addAttribute("additionalcharges",  dto.getAdditionalcharges());
					model.addAttribute("total",  totalAmount);
					
					session.setAttribute("image", result.getImage());
					session.setAttribute("successURL", result.getSuccessURL());
					session.setAttribute("returnURL", result.getFailureURL());
					session.setAttribute("id", result.getMerchantId());
					session.setAttribute("amount", dto.getBillamount());
					session.setAttribute("additionalCharge", dto.getAdditionalcharges());
					session.setAttribute("extra", dto.getAdditionalInfo());
					session.setAttribute("name", dto.getFirstname());
					session.setAttribute("totalAmount", totalAmount);
					session.setAttribute("email", dto.getEmail());
					session.setAttribute("accountid", dto.getAccountid());
					session.setAttribute("phone", dto.getPhone());
					session.setAttribute("mhash", dto.getHash());
					return "ThirdParty/UpiPayment";
				} else {
					model.addAttribute("error", result.getMessage());
//					return "redirect:/ws/api/FailedUPIAuthentication";
					return "ThirdParty/UpiAuthFailed";
				}
			} else {
				model.addAttribute("error", error.getMessage());
//				return "redirect:/ws/api/FailedUPIAuthentication";
				return "ThirdParty/UpiAuthFailed";
			}
		}catch (Exception e){
			e.printStackTrace();
			model.addAttribute("error", "Service Unavailable");
//			return "redirect:/ws/api/FailedUPIAuthentication";
			return "ThirdParty/UpiAuthFailed";
		}
		
	}

	/*@RequestMapping(value = { "/UpiPayment" }, method = RequestMethod.GET)
	public String getUpiPaymentPage(@RequestParam(value = ModelMapKey.MESSAGE, required = false) String msg,
			ModelMap model, HttpServletRequest request, HttpServletResponse response) {
//		double originalAmount = Double.parseDouble(request.getParameter(""))
		model.addAttribute("name",  msg);
		model.addAttribute("phone",  msg);
		model.addAttribute("email",  msg);
		model.addAttribute("amount",  msg);
		model.addAttribute("additionalcharges",  msg);
		model.addAttribute("total",  msg);
		
		return "ThirdParty/UpiPayment";
	}*/

	@RequestMapping(value = "/FailedUPIAuthentication", method = RequestMethod.GET)
	public String failedUPiAuthentication(@RequestParam(value = "error", required = false) String msg, Model model, HttpServletRequest request, HttpServletResponse response) {
		System.err.println("Msgs " + request.getParameter("error"));
		model.addAttribute("error",  msg);
		return "ThirdParty/UpiAuthFailed";
	}

	/*@RequestMapping(value = { "/UpiPaymentProcess" }, method = RequestMethod.POST)
	public ResponseEntity<ResponseDTO> validateUpiPaymentRequest(@ModelAttribute("login") UpiPayRequest dto, HttpSession session,
			Model model) {
		ResponseDTO result = new ResponseDTO();
		try{
			LoginError error = loginValidation.checkUpiError(dto);
			if (error.isValid()) {
				double netAmount = (double) session.getAttribute("totalAmount");
				double additionalCharge = (double) session.getAttribute("additionalCharge");
				String txnId = (String) session.getAttribute("transactionID");
				String email = (String) session.getAttribute("email");
				long mid = (Long) session.getAttribute("id");
				System.err.println();
				dto.setAmount(netAmount);
				dto.setmTxnId(txnId);
				dto.setMid(mid);
				dto.setAdditionalCharge(additionalCharge);
				result = merchantApi.upiMerchantPayment(dto);
				result.setAmount(netAmount);
				result.setTransactionId(txnId);
				result.setEmail(email);
			} else {
				model.addAttribute(ModelMapKey.ERROR, error);
//				return "ThirdParty/UpiPayment";
				result.setMessage(error.getUsername());
			}
		}catch (Exception e){
			e.printStackTrace();
			result.setCode("F00");
			result.setMessage("Internal Server Error");
//			return "ThirdParty/UpiRedirectFailed";
		}
		
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}*/
	
	@RequestMapping(value = { "/UpiPaymentProcess" }, method = RequestMethod.POST)
	public String validateUpiPaymentRequest(@ModelAttribute("login") UpiPayRequest dto, HttpSession session,
			Model model) {
		String returnURL = (String) session.getAttribute("returnURL");
		try{
			LoginError error = loginValidation.checkUpiError(dto);
			if (error.isValid()) {
				double netAmount = (double) session.getAttribute("amount");
				double additionalCharge = (double) session.getAttribute("additionalCharge");
				String merchTxnId = (String) session.getAttribute("transactionID");
				
				
				long mid = (Long) session.getAttribute("id");
				String email = (String) session.getAttribute("email");
				System.err.println();
				ResponseDTO result = new ResponseDTO();
				dto.setAmount(netAmount);
				dto.setmTxnId(merchTxnId);
				dto.setMid(mid);
				dto.setAdditionalCharge(additionalCharge);
				result = merchantApi.upiMerchantPayment(dto);
				model.addAttribute("transactionDate", result.getTdate());
				model.addAttribute("paymentId",  merchTxnId);
				model.addAttribute("merchantRefNo", result.getRefId());
				model.addAttribute("amount", netAmount);
				model.addAttribute("email", email);
				model.addAttribute("mid", mid);
				String accountid = (String) session.getAttribute("accountid");
				model.addAttribute("accountid", accountid);
				session.setAttribute("transactionDate", result.getTdate());
				session.setAttribute("merchantRefNo", result.getRefId());
				System.err.println("Respose value : :  " + ResponseStatus.SUCCESS.getValue());
				if(result.getCode().equals(ResponseStatus.SUCCESS.getValue())){
//					session.setAttribute("amount", 0);
					model.addAttribute("netAmount", netAmount);
//					return "ThirdParty/UpiSuccessPage";
					return "ThirdParty/UpiCountDown";
				}else if(result.getCode().equals(ResponseStatus.FAILURE.getValue())){
					// TODO Need to failed the transaction and save the response from the BESCOM
					model.addAttribute("returnURL", returnURL);
					return "ThirdParty/UpiRedirectFailureAcknow";
//					return "ThirdParty/UpiFailedPage";
				}else if(result.getCode().equals(ResponseStatus.INTERNAL_SERVER_ERROR.getValue())){
//					return "ThirdParty/UpiRedirectFailureAcknow";
					// TODO Need to failed the transaction and save the response from the BESCOM
					model.addAttribute("returnURL", returnURL);
					return "ThirdParty/UpiRedirectFailureAcknow";
				}else{
					// TODO Need to failed the transaction and save the response from the BESCOM
					model.addAttribute("returnURL", returnURL);
					return "ThirdParty/UpiRedirectFailureAcknow";
				}
			} else {
				model.addAttribute(ModelMapKey.ERROR, error);
				model.addAttribute("returnURL", returnURL);
				return "ThirdParty/UpiPayment";
			}
		}catch (Exception e){
			e.printStackTrace();
			// TODO Need to failed the transaction and save the response from the BESCOM
			model.addAttribute("returnURL", returnURL);
			return "ThirdParty/UpiRedirectFailureAcknow";
		}
	}
	
	@RequestMapping(value = "/UpiSuccess", method = RequestMethod.GET)
	public String successUpiPayment(Model model) {
		return "ThirdParty/UpiCountDown";
//		return "ThirdParty/UpiSuccessPage";
	}
	
	@RequestMapping(value = "/UpiFailed", method = RequestMethod.GET)
	public String failedUpiPayment(Model model) {
		return "ThirdParty/UpiFailedPage";
	}
	
	@RequestMapping(value = "/UpiFailedPage", method = RequestMethod.GET)
	public String failedPayment(Model model) {
		return "ThirdParty/UpiRedirectFailed";
	}
	

	@RequestMapping(value = "/UpiRSuccess", method = RequestMethod.GET)
	public String successUpiPaymet(Model model) {
//		model.addAttribute("returnURL", "https://www.bescom.co.in/SCP/Myhome.aspx");
//		return "ThirdParty/UpiRedirectFailureAcknow";
		return "User/UpiSuccess";
	}
	
	
	
	/*@RequestMapping(value = "/UPIRedirect", method = RequestMethod.POST)
	public ResponseEntity<UPIRedirectResponse> redirectUPILoadMoney(UPIRedirect dto, Model model, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		System.err.println("INside UPI Redirect");
		System.err.println("Dto");
		MerchantApprovedResponse apprResp = new MerchantApprovedResponse();
		StringBuffer jb = new StringBuffer();
		String line = null;
		try {
			BufferedReader reader = request.getReader();
			while ((line = reader.readLine()) != null){
				jb.append(line);
				System.err.println(line);
			}
			String output = jb.toString();
			MerchantMetaVPAResponse resp = ConvertUtil.getUPIresponse(output);
			System.out.println("Ref No : : " + resp.getReferenceNo());
			if(resp.getCode().equals(ResponseStatus.SUCCESS.getValue())){
				dto.setReferenceId(resp.getReferenceNo());
				dto.setRespCode("0");
				apprResp.setOrgTxnId(resp.getOrgTxnId());
				apprResp.setOrgTxnRefId(resp.getReferenceNo());
				apprResp.setTxnId(resp.getTxnId());
				apprResp.setTimeStamp(System.currentTimeMillis()+"");
				System.err.println("UPI Redirect ************* ************** ******** *** *****  Success");
			}else{
				dto.setReferenceId(resp.getReferenceNo());
				dto.setRespCode("1");
				System.err.println("UPI Redirect ************* ************** ******** *** *****  Failed");
			}
		} catch (Exception e) {
			dto.setRespCode("1");
			System.out.println("UPI Redirect ************* ************** ******** *** *****  Exception");
	    }
		UPIRedirectResponse responseDTO = merchantApi.upiRedirectPayment(dto);
		if(responseDTO.getCode().equals(ResponseStatus.SUCCESS.getValue())){
			apprResp.setRespCode("000");
			apprResp.setRespDesc("APPROVED");
		}else{
			apprResp.setRespCode("111");
			apprResp.setRespDesc("DECLINED");
		}
		System.out.println("Approved Response : : : ----------  " + apprResp.getRequest());
		System.out.println("Approved Response Code : : : ----------  " + apprResp.getRespCode());
		System.out.println("Approved Response DESC : : : ----------  " + apprResp.getRespDesc());
		return new ResponseEntity<UPIRedirectResponse>(responseDTO, HttpStatus.OK);
	}*/
	
	
	@RequestMapping(value = "/UPIRedirect", method = RequestMethod.POST)
	public ResponseEntity<MerchantApprovedResponse> redirectUPILoadMoney(UPIRedirect dto, Model model, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		System.err.println("INside UPI Redirect");
		MerchantApprovedResponse apprResp = new MerchantApprovedResponse();
		StringBuffer jb = new StringBuffer();
		String line = null;
		try {
			BufferedReader reader = request.getReader();
			while ((line = reader.readLine()) != null){
				jb.append(line);
				System.err.println(line);
			}
			String output = jb.toString();
			MerchantMetaVPAResponse resp = ConvertUtil.getUPIresponse(output);
			if(resp.getCode().equals(ResponseStatus.SUCCESS.getValue())){
				dto.setReferenceId(resp.getTxnId());
				dto.setRespCode("0");
				apprResp.setOrgTxnId(resp.getOrgTxnId());
				apprResp.setOrgTxnRefId(resp.getReferenceNo());
				apprResp.setTxnId(resp.getTxnId());
				apprResp.setTimeStamp(System.currentTimeMillis()+"");
				System.err.println("UPI Redirect ************* ************** ******** *** *****  Success");
			}else{
				dto.setReferenceId(resp.getTxnId());
				dto.setRespCode("1");
				System.err.println("UPI Redirect ************* ************** ******** *** *****  Failed");
			}
		} catch (Exception e) {
			dto.setRespCode("1");
			System.out.println("UPI Redirect ************* ************** ******** *** *****  Exception");
	    }
		UPIRedirectResponse responseDTO = merchantApi.upiRedirectPayment(dto);
		if(responseDTO.getCode().equals(ResponseStatus.SUCCESS.getValue())){
			apprResp.setRespCode("000");
			apprResp.setRespDesc("APPROVED");
		}else{
			apprResp.setRespCode("111");
			apprResp.setRespDesc("DECLINED");
		}
		System.out.println("Approved Response : : : ----------  " + apprResp.getRequest());
		System.out.println("Approved Response Code : : : ----------  " + apprResp.getRespCode());
		System.out.println("Approved Response DESC : : : ----------  " + apprResp.getRespDesc());
		return new ResponseEntity<>(apprResp, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/UpiStatus", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<StatusResponse> checkUPITransactionStatus(@RequestBody CheckUpiStatus dto) {
		System.err.println("inside status api");
		StatusResponse statusResponse = new StatusResponse();
		CommonError error = merchantValidation.checkUPIStatusError(dto);
		if (error.isValid()) {
			System.err.println("error is not present");
			statusResponse = merchantApi.checkUpiStatus(dto);
		} else {
			statusResponse.setCode("F04");
			statusResponse.setMessage(error.getMessage());
		}
		return new ResponseEntity<StatusResponse>(statusResponse, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/UpiRedirectSuccess", method = RequestMethod.GET)
	public String redirectSuccessPage(Model model, HttpSession session) {
		String returnURL = (String) session.getAttribute("returnURL");
		String s2sCallUrl = (String) session.getAttribute("successURL");
		
		try{
			String merTxnId = (String) session.getAttribute("transactionID");
			String transactionDate = (String) session.getAttribute("transactionDate");
			String refId = (String) session.getAttribute("merchantRefNo");
			String email = (String) session.getAttribute("email");
			String phone = (String) session.getAttribute("phone");
			String mhash = (String) session.getAttribute("mhash");
			double additionalCharge = (double) session.getAttribute("additionalCharge");
			long mid = (Long) session.getAttribute("id");
			double netAmount = (double) session.getAttribute("amount");
			if(netAmount > 0){
				StatusResponse result = new StatusResponse();
				CheckUpiStatus dto = new CheckUpiStatus();
				dto.setMerchantId(mid);
				dto.setMerchantRefNo(merTxnId);
				String accountid = (String) session.getAttribute("accountid");
				model.addAttribute("accountid", accountid);
				model.addAttribute("transactionDate", transactionDate);
				model.addAttribute("paymentId",  merTxnId);
				model.addAttribute("merchantRefNo", refId);
				model.addAttribute("amount", netAmount);
				model.addAttribute("email", email);
				model.addAttribute("mid", mid);
				// TODO Call timer to get success
				boolean condition = true ;
				System.err.println("Condition time : :  " + condition);
				 while (condition){
					 result = merchantApi.checkUpiStatus(dto);
					 if(!result.getCode().equals("F11")){
							if(result.getStatus().equals(Status.Success)){
								condition = false;
								// TODO Redirect to success 
//								S2SRequest s2sReq  = ConvertUtil.convertS2SRequest(mid+"", merTxnId, refId, result.getStatus().getValue(), result.getCode(), netAmount,
//										additionalCharge, phone, email, transactionDate, mhash, s2sCallUrl);
//								String bResp = merchantApi.s2sCall(s2sReq);
								System.err.println("Completd");
								System.err.println("Redirecting the transaction and the merchant transaction ID is  : : :  : : :   " + merTxnId + " Merchant Id : " + mid + " Stauts : " + result.getStatus());
								session.setAttribute("totalAmount", 0);
								// TODO save the response which are coming from the BESCOM
//								S2SResponse resp = ConvertUtil.convertS2SResponse(mid+"", merTxnId, refId, result.getStatus().getValue(), s2sReq.toJSON().toString(), bResp);
//								StatusResponse callBck = merchantApi.savingS2Sresp(s2sReq);
//								System.out.println("Bescom call back details ----------------------------------- " + callBck.getStatus());
								model.addAttribute("returnURL", returnURL);
								return "ThirdParty/UpiRedirectSuccessAcknow";
							}else if(result.getStatus().equals(Status.Failed)){
								System.err.println("Redirecting the transaction and the merchant transaction ID is  : : :  : : :   " + merTxnId + " Merchant Id : " + mid);
								// TODO Redirect to failed
								model.addAttribute("returnURL", returnURL);
								return "ThirdParty/UpiRedirectFailureAcknow";
							}else{
								condition = true;
//								System.err.println("Calling apis");
							}
					 }else{
						System.err.println("Redirecting the transaction and the merchant transaction ID is  : : :  : : :   " + result.getMessage());
//						model.addAttribute("msg", result.getMessage());
						model.addAttribute("returnURL", returnURL);
						return "ThirdParty/UpiRedirectFailureAcknow";
					}
				}
			}else{
				// TODO while he tries the payments with the same amount
				model.addAttribute("returnURL", returnURL);
				return "ThirdParty/UpiResponseAfterPaymentSucess";
			}
		}catch (Exception e){
			//TODO Redirect to Failed
			e.printStackTrace();
			model.addAttribute("returnURL", returnURL);
			return "ThirdParty/UpiRedirectFailed";
		}
		model.addAttribute("returnURL", returnURL);
		return "ThirdParty/UpiRedirectFailed";
	}
	
	@RequestMapping(value = "/BescomS2SCall", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE },
			consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<S2SResponse> s2sCallTransaction(@RequestBody S2SRequest dto) {
		S2SResponse result = new S2SResponse();
		try {
			if(dto.isRural()){
				result = merchantApi.besconRuralS2SCall(dto);
			}else{
				result = merchantApi.besconS2SCall(dto);
			}
			return new ResponseEntity<S2SResponse>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<S2SResponse>(result, HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/UpiDemo", method = RequestMethod.GET)
	public String testUpiPayment(Model model) throws Exception {
		String	key = "272868"; // Arban
//		String	key = "272872"; // Rural
//		String	key = "7827031";
//		String	key = "272962"; // VIJ
		long	merchanttxnid = System.currentTimeMillis();
		String	billamount = "10.00";
		String	additionalcharges = "0.00";
		String	accountid = "100011001";
		String	firstname = "Pankaj Kumar";
		String	email = "pkumar@msewa.com" ;
		String	phone = "7022620747";
//		String	hash = SecurityUtil.md5("F947F49E37BCCAAAB774C1EE7C707F79|10.00|0.00|272962|"+merchanttxnid);
		String	hash = SecurityUtil.md5("E3CB82F39B9700A8AC52D143C2BF4733|10.00|0.00|272868|"+merchanttxnid); 
//		String	hash = SecurityUtil.md5("C88CCFC2BE86318594330A169F2E08C8|10.00|0.00|272872|"+merchanttxnid); // Rural
//		String	hash = SecurityUtil.md5("390096B48D59221A5BF357E9DE5B7D4B|10.00|0.00|7827031|"+merchanttxnid); // Rural
		model.addAttribute("key", key);
		model.addAttribute("merchanttxnid", merchanttxnid);
		model.addAttribute("billamount", billamount);
		model.addAttribute("additionalcharges", additionalcharges);
		model.addAttribute("accountid", accountid);
		model.addAttribute("firstname", firstname);
		model.addAttribute("email", email);
		model.addAttribute("phone", phone);
		model.addAttribute("hash", hash);
		System.err.println(hash);
		return "ThirdParty/Demo";
	}
	
/*	public static void main(String[] args) {
		try {
//			E3CB82F39B9700A8AC52D143C2BF4733|15.00|0.00|272868|6684
//			System.err.println("E3CB82F39B9700A8AC52D143C2BF4733" + "|" + 15.00 + "|" + 0.00 + "|" + 272868 + "|" + "6684");
//			System.err.println(SecurityUtil.md5("390096B48D59221A5BF357E9DE5B7D4B" + "|" + 1.00 + "|" + 0.00 + "|" + 7827031 + "|" + "10000052"));
//			System.err.println(SecurityUtil.md5("E3CB82F39B9700A8AC52D143C2BF4733" + "|" + 15.00 + "|" + 0.00 + "|" + 272868 + "|" + "6684"));
//			System.err.println(SecurityUtil.md5("E3CB82F39B9700A8AC52D143C2BF4733|1.00|0.00|272868|67264"));
//			System.err.println(SecurityUtil.md5("C88CCFC2BE86318594330A169F2E08C8|1.00|0.00|272872|67261"));
			System.err.println(SecurityUtil.md5("390096B48D59221A5BF357E9DE5B7D4B|1.00|0.00|7827031|10000080"));
//			390096B48D59221A5BF357E9DE5B7D4B|15.00|0.00|7827031|10000052
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}*/
	
	public static void main(String[] args) {
		try {
			String Status = "Success";
			String StatusCode = "100";
			String Key = "BESCOM";
			String Token = "VijayaBHIMUPI";
			String TransactionId = "1";
			String Amount = "2.00";
			String LocationCode = "5110101";
			String TransDate = "2018-01-30";
			String ConnectionId = "2";
			String EmailId = "dmrcute@gmail.com";
			String TransRefNo = "Dmr123456";
			String BillNo = "1111111";
			String Op1 = "";
			String Op2 = "";
			String Op3 = "";
			String Op4 = "";
			String Op5 = "";

			String ComputeHashValue = Key + "|" + Token + TransactionId + "|" + TransRefNo + "|" + ConnectionId + "|"
					+ Amount + "|" + LocationCode + "|" + BillNo + "|" + TransDate + "|" + EmailId	 + "|" + Op1 + "|"
					+ Op2 + "|" + Op3 + "|" + Op4 + "|" + Op5;
			
			String EncData = SecurityUtil.md5(ComputeHashValue);
			
			String Msg = Status + "|" + StatusCode + "|" + Key + "|" + Token + "|" + TransactionId + "|" + TransRefNo
					+ "|" + ConnectionId + "|" + Amount + "|" + LocationCode + "|" + BillNo + "|" + TransDate + "|"
					+ EmailId + "|" + EncData + "|" + Op1 + "|" + Op2 + "|" + Op3 + "|" + Op4 + "|" + Op5;
			
			
			System.out.println();
			System.out.println(Msg);
			S2SResponse result = new S2SResponse();

			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			MultivaluedMapImpl formData = new MultivaluedMapImpl();
			formData.add("msg", Msg);
			WebResource resource = client.resource("http://123.201.131.117:88/s2scall.aspx");
			ClientResponse response = resource.post(ClientResponse.class, formData);
			String strResponse = response.getEntity(String.class);
			System.err.print("strResponse ----------------  " + strResponse);
			if (strResponse != null) {
				org.json.JSONObject json = new org.json.JSONObject(strResponse);
				System.err.println(json);
				if (json != null) {
					org.json.JSONObject resp = JSONParserUtil.getObject(json, "Response");
					result.setReceiptId(resp.getString("ReceiptId"));
					result.setStatus(resp.getString("status"));
					result.setTransactiondatetime(resp.getString("transactiondatetime"));
					result.setTransactionpostingdatetime(resp.getString("transactionpostingdatetime"));
					result.setWssTransactionID(resp.getString("WSSTransactionID"));
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
