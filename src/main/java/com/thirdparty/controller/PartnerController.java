package com.thirdparty.controller;

import com.payqwikweb.app.api.IPartnerApi;
import com.payqwikweb.app.model.request.PDeviceUpdateDTO;
import com.payqwikweb.app.model.response.PDeviceUpdateResponse;
import org.json.JSONException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.xml.sax.SAXException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.ParserConfigurationException;

@Controller
@RequestMapping("/ws/api/partner")
public class PartnerController {

    private final IPartnerApi partnerApi;

    public PartnerController(IPartnerApi partnerApi) {
        this.partnerApi = partnerApi;
    }

    @RequestMapping(value = "/registerDevice", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE })
    ResponseEntity<PDeviceUpdateResponse> registerDevice(@RequestHeader(value="apiKey",required = false) String apiKey,@RequestBody PDeviceUpdateDTO dto, HttpServletRequest request, HttpServletResponse response,
                                                         HttpSession session) throws JSONException, ParserConfigurationException, SAXException {
        dto.setApiKey(apiKey);
        PDeviceUpdateResponse result  = partnerApi.saveDevice(dto);
        return new ResponseEntity<PDeviceUpdateResponse>(result, HttpStatus.OK);
    }
}
