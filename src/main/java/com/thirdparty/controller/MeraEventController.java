package com.thirdparty.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.payqwikweb.api.IAuthenticationApi;
import com.payqwikweb.app.api.IMeraEventsApi;
import com.payqwikweb.app.api.ITransactionApi;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.response.MeraEventsAttendeeFormRequest;
import com.payqwikweb.app.model.response.MeraEventsResponse;
import com.payqwikweb.util.APIUtils;
import com.payqwikweb.util.Authorities;

@Controller
@RequestMapping("/Events")

public class MeraEventController implements MessageSourceAware {
	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	private MessageSource messageSource;
	
	private final IAuthenticationApi authenticationApi;
	private final IMeraEventsApi meraEventsApi;
	public MeraEventController(IAuthenticationApi authenticationApi,IMeraEventsApi meraEventsApi) {
		this.authenticationApi = authenticationApi;
		this.meraEventsApi = meraEventsApi;
	}
	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	@RequestMapping(value = "/offlineBooking", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<String> offlineBooking(@RequestBody MeraEventsAttendeeFormRequest dto,
			@RequestHeader(value = "hash", required = false) String hash,
			 HttpServletRequest request,HttpServletResponse response, HttpSession session) {
		MeraEventsResponse result = new MeraEventsResponse();
		System.err.println("------------------------INSIDE OFFLINE BOOKING CONTROLLER-----------------------");
		String sessionId = dto.getSessionId();
		System.out.println("SESSION ID : " +sessionId);
				if (sessionId != null) {
					String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
					if (authority != null) {
						if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
							System.err.println("------------- ORDER ID ----------" +dto.getOrderId());
							dto.setSessionId(sessionId);
							System.out.println("Acess Token " +dto.getAccess_token());
							dto.setEmailEnable(true);
							dto.setSmsEnable(true);
							result = meraEventsApi.offlineBooking(dto);
							if (result.getCode().equalsIgnoreCase("S00")) {
								result.setCode("S00");
								result.setMessage("Event Booked Successfully");
								result.setSuccess(true);
								result.setStatus("Success");
								result.setResponse(result.getResponse());
							} else {
								result.setSuccess(false);
								result.setCode("F00");
								result.setMessage("Please try again later..");
								result.setStatus("Failure");
								result.setResponse(APIUtils.getFailedJSON().toString());
							}
						} else {
							result.setSuccess(false);
							result.setCode("F00");
							result.setMessage("User Authentication Failed");
							result.setStatus("Failure");
							result.setResponse(APIUtils.getFailedJSON().toString());
						}
					} else {
						result.setSuccess(false);
						result.setCode("F00");
						result.setMessage("User Authority null");
						result.setStatus("Failure");
						result.setResponse(APIUtils.getFailedJSON().toString());
					}
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Session null");
					result.setStatus("Failure");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
		return new ResponseEntity<String>(result.getResponse(), HttpStatus.OK);
	}

	
}
