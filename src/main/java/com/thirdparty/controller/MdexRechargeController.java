package com.thirdparty.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jettison.json.JSONObject;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.payqwikweb.app.metadatas.UrlMetadatas;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Language;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.Version;
import com.payqwikweb.app.utils.SecurityUtils;
import com.payqwikweb.model.web.TopupType;
import com.payqwikweb.thirdparty.request.TransactionRequest;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;

@Controller
@RequestMapping("/Recharge/Mdex")
public class MdexRechargeController implements MessageSourceAware {

	private MessageSource messageSource;

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	@RequestMapping(value = "/Prepaid", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<String> processPrepaidFromMdex(
			@RequestBody TransactionRequest dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response1) {
		try {
			System.out.println(dto.getAmount());
			String stringResponse = "";
			JSONObject payload = new JSONObject();
			payload.put("serviceProvider", dto.getServiceProvider());
			   payload.put("mobileNo", dto.getMobileNo());
			   payload.put("amount", dto.getAmount());
			   payload.put("transactionId", dto.getTransactionId());
			   payload.put("area", dto.getArea());
			if(dto.getAccountNumber()!=null && dto.getAccountNumber()!="") {
			   payload.put("accountNumber", dto.getAccountNumber());
			}
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			String url = "";
			if(TopupType.Prepaid.toString().equals(dto.getTopupType())) {
				url = UrlMetadatas.getMdexPrepaidRechargeUrl(Version.VERSION_1, Role.ClIENT, Device.ANDROID, Language.ENGLISH);
			}
			else if(TopupType.DataCard.toString().equals(dto.getTopupType())) {
				url = UrlMetadatas.getMdexDataCardRechargeUrl(Version.VERSION_1, Role.ClIENT, Device.ANDROID, Language.ENGLISH);
			}
			else if(TopupType.Dth.toString().equals(dto.getTopupType())) {
				payload.put("dthNo", dto.getDthNo());
				url = UrlMetadatas.getMdexDthRechargeUrl(Version.VERSION_1, Role.ClIENT, Device.ANDROID, Language.ENGLISH);
			}
			else if(TopupType.Postpaid.toString().equals(dto.getTopupType())) {
				url = UrlMetadatas.getMdexPostpaidRechargeUrl(Version.VERSION_1, Role.ClIENT, Device.ANDROID, Language.ENGLISH);
			}
			else if(TopupType.Landline.toString().equals(dto.getTopupType())) {
				payload.put("stdCode", dto.getStdCode());
				payload.put("landlineNumber", dto.getLandlineNumber());
				url = UrlMetadatas.getMdexLandlineRechargeUrl(Version.VERSION_1, Role.ClIENT, Device.ANDROID, Language.ENGLISH);
			}
			else if(TopupType.Electricity.toString().equals(dto.getTopupType())) {
				payload.put("cycleNumber", dto.getCycleNumber());
				payload.put("billingUnit", dto.getBillingUnit());
				payload.put("cityName", dto.getCityName());
				payload.put("processingCycle", dto.getProcessingCycle());
				url = UrlMetadatas.getMdexElectricityBillPayUrl(Version.VERSION_1, Role.ClIENT, Device.ANDROID, Language.ENGLISH);
			}
			if(!url.equalsIgnoreCase("")) {
			WebResource webResource = client.resource(url);
			ClientResponse response = webResource.accept("application/json").type("application/json")
				     .header("key", UrlMetadatas.MDEX_CLIENTKEY).header("token", UrlMetadatas.MDEX_CLIENTTOKEN)
				     .header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() == 200) {
				stringResponse = response.getEntity(String.class);
				return new ResponseEntity<String>(stringResponse, HttpStatus.OK);
			} else {
				return new ResponseEntity<String>(stringResponse, HttpStatus.OK);
			}
			}else {
				System.out.println("Not able to build url for mdex");
				return new ResponseEntity<String>("", HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>("", HttpStatus.OK);
		}
		
	}
	
	@RequestMapping(value = "/Status", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<String> mdexStatus(
			@RequestBody TransactionRequest dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response1) {
		JSONObject payload = new JSONObject();
		try {
			payload.put("transactionId", dto.getTransactionId());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(UrlMetadatas.getMdexStatusUrl(Version.VERSION_1, Role.ClIENT, Device.ANDROID, Language.ENGLISH));
			ClientResponse clientResponse = webResource.accept("application/json").type("application/json")
					.header("key", UrlMetadatas.MDEX_CLIENTKEY).header("token", UrlMetadatas.MDEX_CLIENTTOKEN)
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String stringResponse=""; 
			if (clientResponse.getStatus() == 200) {
					stringResponse = clientResponse.getEntity(String.class);
					System.err.println("response from mdex:::" + stringResponse);
					return new ResponseEntity<String>(stringResponse, HttpStatus.OK);
				} else {
					return new ResponseEntity<String>(stringResponse, HttpStatus.OK);
				}

			
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>("", HttpStatus.OK);
		}
		}

}
