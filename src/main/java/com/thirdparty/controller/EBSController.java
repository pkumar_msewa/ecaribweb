package com.thirdparty.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.ParserConfigurationException;

import org.json.JSONException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.xml.sax.SAXException;

import com.payqwikweb.api.constants.APIConstants;
import com.payqwikweb.app.api.ILoadMoneyApi;
import com.payqwikweb.app.model.response.EBSRedirectResponse;
import com.payqwikweb.app.model.response.VRedirectResponse;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import com.thirdparty.model.ResponseDTO;

@Controller
@RequestMapping("/ws/api/LoadMoney")
public class EBSController {

    private final ILoadMoneyApi loadMoneyApi;

    public EBSController(ILoadMoneyApi loadMoneyApi) {
        this.loadMoneyApi = loadMoneyApi;
    }

    @RequestMapping(value = "/{transactionRefNo}/Status", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
    ResponseEntity<ResponseDTO> checkEBSTransaction(@PathVariable("transactionRefNo") String transactionRefNo, HttpServletRequest request, HttpServletResponse response,
                                                    HttpSession session) throws JSONException, ParserConfigurationException, SAXException {
        EBSRedirectResponse ebsRedirectResponse = new EBSRedirectResponse();
        ebsRedirectResponse.setMerchantRefNo(transactionRefNo);
        ResponseDTO dto  = loadMoneyApi.verifyEBSTransaction(ebsRedirectResponse);
        return new ResponseEntity<ResponseDTO>(dto, HttpStatus.OK);
    }
    
    
    @RequestMapping(value = "/EbsStatus", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE })
    ResponseEntity<String> checkEBSTxns(@ModelAttribute EBSRedirectResponse dto, HttpServletRequest request, HttpServletResponse response,
                                                    HttpSession session) throws JSONException, ParserConfigurationException, SAXException {
    	try{
    		Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(com.payqwikweb.api.constants.APIConstants.EBS_VERIFICATION);
			MultivaluedMapImpl formData = new MultivaluedMapImpl();
			formData.add("Action", "statusByRef");
			formData.add("AccountID", "20696");
			formData.add("SecretKey", "6496e4db9ebf824ffe2269afee259447");
			formData.add("RefNo", dto.getMerchantRefNo());
			ClientResponse resp = webResource.post(ClientResponse.class, formData);
			String strResponse = resp.getEntity(String.class);
			return new ResponseEntity<String>(strResponse, HttpStatus.OK);
    	} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>("{}", HttpStatus.OK);
		}
    }
    
    @RequestMapping(value = "/VnetStatus", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE })
    ResponseEntity<String> checkVnetTransaction(@ModelAttribute VRedirectResponse dto, HttpServletRequest request, HttpServletResponse response,
                                                    HttpSession session) throws JSONException, ParserConfigurationException, SAXException {
    	try{
    		Client client = Client.create();
    		client.addFilter(new LoggingFilter(System.out));
    		MultivaluedMapImpl formData = new MultivaluedMapImpl();
    		formData.add("PID", APIConstants.VNET_PID);
    		formData.add("PRN", dto.getPRN());
    		formData.add("ITC", dto.getITC());
    		formData.add("AMT", dto.getAMT());
    		WebResource resource = client.resource(APIConstants.VNET_VERIFICATION_URL);
    		ClientResponse resp = resource.accept("application/x-www-form-urlencoded").post(ClientResponse.class, formData);
    		String strResponse = resp.getEntity(String.class);
    		System.err.println("string response ::" + strResponse);
    		return new ResponseEntity<String>(strResponse, HttpStatus.OK);
    	} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>("{}", HttpStatus.OK);
		}
    }
    
}
