package com.thirdparty.controller;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.payqwikweb.thirdparty.request.SMSRequest;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.thirdparty.model.SMSRunnable;
import com.thirdparty.model.SmsDTO;
import com.thirdparty.util.SmsUtil;

@Controller
@RequestMapping("/SendSMS")
public class SMSController {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
    public String response;

	@RequestMapping(value = "/Now/{mobileNumber}", method = RequestMethod.GET)
	ResponseEntity<String> sendSMSNow(@PathVariable(value = "mobileNumber") String mobileNumber) {
		final String destination = mobileNumber;
        SMSRunnable smsRunnable = new SMSRunnable();
		try {
            SMSRequest dto = new SMSRequest();
            dto.setDestination(destination);
            dto.setMessage("testing");
            dto.setDlr("1");
            dto.setUsername("msewaOTP");
            dto.setPassword("Hnfk5qzx");
            dto.setType("0");
            dto.setSource("VPYQWK");
            smsRunnable.setDto(dto);
            smsRunnable.run();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return new ResponseEntity<String>(smsRunnable.getResponse(), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST)
	ResponseEntity<String> sendSMS(final SMSRequest dto) {
		String response = "";
		try {

            SMSRunnable smsRunnable = new SMSRunnable();
            smsRunnable.setDto(dto);
            smsRunnable.run();
            response = smsRunnable.getResponse();
//			Thread t = new Thread(new Runnable() {
//				public void run() {
//					try {
//						URL sendUrl = new URL("https://sms6.routesms.com:8443/bulksms/bulksms?");
//
//						HostnameVerifier hostVerifier = new HostnameVerifier() {
//							@Override
//							public boolean verify(String hostname, SSLSession session) {
//								return true;
//							}
//						};
//
//						trustAllHttpsCertificates();
//						HttpURLConnection httpConnection = (HttpURLConnection) sendUrl.openConnection();
//						httpConnection.setRequestMethod("POST");
//						httpConnection.setDoInput(true);
//						httpConnection.setDoOutput(true);
//						httpConnection.setUseCaches(false);
//						DataOutputStream dataStreamToServer = new DataOutputStream(httpConnection.getOutputStream());
//						dataStreamToServer.writeBytes("username=" + URLEncoder.encode(dto.getUsername(), "UTF-8")
//								+ "&password=" + URLEncoder.encode(dto.getPassword(), "UTF-8") + "&type="
//								+ URLEncoder.encode(dto.getType(), "UTF-8") + "&dlr="
//								+ URLEncoder.encode(dto.getDlr(), "UTF-8") + "&destination="
//								+ URLEncoder.encode(dto.getDestination(), "UTF-8") + "&source="
//								+ URLEncoder.encode(dto.getDestination(), "UTF-8") + "&message="
//								+ URLEncoder.encode(dto.getMessage(), "UTF-8"));
//
//						dataStreamToServer.flush();
//						dataStreamToServer.close();
//						BufferedReader dataStreamFromUrl = new BufferedReader(
//								new InputStreamReader(httpConnection.getInputStream()));
//						String dataBuffer = "";
//    					while ((dataBuffer = dataStreamFromUrl.readLine()) != null) {
//							response += dataBuffer;
//						}
//						dataStreamFromUrl.close();
//                        setResponse(response);
//						System.err.print("sms response is :" + response);
//					} catch (Exception e) {
//						e.printStackTrace();
//					}
//
//				}
//			});
//			t.start();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<String>(response, HttpStatus.OK);
	}

	private static void trustAllHttpsCertificates() throws Exception {
		// Create a trust manager that does not validate certificate chains:
		javax.net.ssl.TrustManager[] trustAllCerts = new javax.net.ssl.TrustManager[1];
		javax.net.ssl.TrustManager tm = new miTM();
		trustAllCerts[0] = tm;
		javax.net.ssl.SSLContext sc = javax.net.ssl.SSLContext.getInstance("SSL");
		sc.init(null, trustAllCerts, null);
		javax.net.ssl.HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
	}

	public static class miTM implements javax.net.ssl.TrustManager, javax.net.ssl.X509TrustManager {
		public java.security.cert.X509Certificate[] getAcceptedIssuers() {
			return null;
		}

		public boolean isServerTrusted(java.security.cert.X509Certificate[] certs) {
			return true;
		}

		public boolean isClientTrusted(java.security.cert.X509Certificate[] certs) {
			return true;
		}

		public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType)
				throws java.security.cert.CertificateException {
			return;
		}

		public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType)
				throws java.security.cert.CertificateException {
			return;
		}
	}

	public void setResponse(String response){
        this.response = response;
    }

    public String getResponse(){
        return response;
    }
    
    @RequestMapping(value = "/Now", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<String> sendSms(@RequestBody SmsDTO dto) {
		try {
			String stringResponse = "";
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(SmsUtil.SMS_URL_LIVE)
					.queryParam("mobileno",dto.getMobileNumber())
			        .queryParam("message", dto.getMessage());
			ClientResponse clientResponse = webResource.get(ClientResponse.class);
			stringResponse = clientResponse.getEntity(String.class);
			if (clientResponse.getStatus() == 200) {
				String response = stringResponse;
				System.err.println("sms response is ::" + response);
				return new ResponseEntity<String>(response, HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>("", HttpStatus.OK);
		}
		return new ResponseEntity<String>(response, HttpStatus.OK);
    }
}
