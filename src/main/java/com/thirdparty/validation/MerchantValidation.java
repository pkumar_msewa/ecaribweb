package com.thirdparty.validation;

import com.payqwikweb.validation.CommonValidation;
import com.thirdparty.model.AuthenticateDTO;
import com.thirdparty.model.BescomAuthenthicationDTO;
import com.thirdparty.model.CheckUpiStatus;
import com.thirdparty.model.SDKAuthenticationDTO;
import com.thirdparty.model.StatusCheckDTO;
import com.thirdparty.model.error.CommonError;

public class MerchantValidation {

    public CommonError checkError(AuthenticateDTO dto){
        CommonError error = new CommonError();
        boolean valid = true;
        if(dto.getId() <=0){
            valid = false;
        }
        if(CommonValidation.isNull(dto.getAmount())){
            valid = false;
        }
        error.setValid(valid);
        return error;
    }
    
    public CommonError sdkCheckError(SDKAuthenticationDTO dto){
        CommonError error = new CommonError();
        boolean valid = true;
        if(dto.getId() <=0){
            valid = false;
        }
        if(CommonValidation.isNull(dto.getAmount())){
            valid = false;
        }
        error.setValid(valid);
        return error;
    }
    
    public CommonError checkBescomError(BescomAuthenthicationDTO dto){
        CommonError error = new CommonError();
        boolean valid = true;
        
        if(dto.getKey() <=0){
        	error.setMessage("Merchant ID is missing.");
            valid = false;
        }
        if(dto.getBillamount() <= 0){
        	error.setMessage("Bill Amount is missing.");
            valid = false;
        }
        if(dto.getAdditionalcharges() < 0){
        	error.setMessage("Additional Charge is missing.");
            valid = false;
        }
        
        if(CommonValidation.isNull(dto.getAccountid())){
        	error.setMessage("Account ID is missing.");
            valid = false;
        }
        
        if(CommonValidation.isNull(dto.getFirstname())){
        	error.setMessage("First Name is missing.");
            valid = false;
        }
        
        if(CommonValidation.isNull(dto.getEmail())){
        	error.setMessage("Email ID is missing.");
            valid = false;
        }else if (!(CommonValidation.isValidMail(dto.getEmail()))) {
			error.setMessage("Email ID is incorrect");
			valid = false;
		} 
        
        if(CommonValidation.isNull(dto.getPhone())){
        	error.setMessage("Phone Number is missing.");
            valid = false;
        }else if (!CommonValidation.checkLength10(dto.getPhone())) {
			error.setMessage("Phone number must be 10-digit");
			valid = false;
		} else if (!CommonValidation.isNumeric(dto.getPhone())) {
			error.setMessage("Phone number must be numeric");
			valid = false;
		}
        
        error.setValid(valid);
        return error;
    }

    public CommonError checkError(StatusCheckDTO dto){
        CommonError error = new CommonError();
        boolean valid = true;
        try{
            if(dto.getMerchantId() <= 0){
                valid = false;
                error.setMessage("Enter Merchant ID");
            }else if(CommonValidation.isNull(dto.getMerchantRefNo())){
                valid = false;
                error.setMessage("Enter Merchant Ref No");
            }else if(CommonValidation.isNull(dto.getSecretKey())){
                valid = false;
                error.setMessage("Enter Secret Key");
            }
        }catch(Exception e){
            valid = false;
        }
        error.setValid(valid);
        return error;
    }
    
    public CommonError checkUPIStatusError(CheckUpiStatus dto){
        CommonError error = new CommonError();
        boolean valid = true;
        try{
            if(dto.getMerchantId() <= 0){
                valid = false;
                error.setMessage("Enter Merchant ID");
            }else if(CommonValidation.isNull(dto.getMerchantRefNo())){
                valid = false;
                error.setMessage("Enter Merchant Ref No");
            }else if(CommonValidation.isNull(dto.getSecretKey())){
                valid = false;
                error.setMessage("Enter Secret Key");
            }else if(dto.getAmount() <= 0){
            	valid = false;
                error.setMessage("Enter valid Amount");
            }
        }catch(Exception e){
            valid = false;
        }
        error.setValid(valid);
        return error;
    }
}
