package com.thirdparty.util;

public enum EventStatus {
	
	Active(1),
	Inactive(2);

	private int numValue;
	EventStatus(int numValue) {
		this.numValue = numValue;
	}
	public int getNumValue() {
		return numValue;
	}
	public void setNumValue(int numValue) {
		this.numValue = numValue;
	}
}
