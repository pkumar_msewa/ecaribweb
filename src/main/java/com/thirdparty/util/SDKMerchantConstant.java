package com.thirdparty.util;

import com.payqwikweb.app.metadatas.UrlMetadatas;

public class SDKMerchantConstant {
	
	private static final String BASE_URL=UrlMetadatas.HOST+"/";
	   
    public static final String AUTHENTICATE_URL = BASE_URL+"SDK/Authenticate/MerchantAuth";
    public static final String VALIDATE_USER_OTP = BASE_URL+"SDK/Authenticate/VerifyOtp";
    public static final String AUTHENTICATE_USER = BASE_URL+"AuthenticateUser";
    public static final String REGISTER_USER = BASE_URL+"Register";
    public static final String VERIFY_OTP = BASE_URL+"Activate/Mobile";
    public static final String FORGOT_PASSWORD = BASE_URL+"ForgotPassword";
    public static final String VALIDATE_FORGOT_PASSWORD_OTP = BASE_URL+"Change";
    public static final String CHANGE_PASSWORD = BASE_URL+"RenewPassword";
    public static final String PROCESS_PAYMENT = BASE_URL+"Authenticate/Payment";
    public static final String STATUS_CHECK = BASE_URL+"Authenticate/StatusCheck";

}
