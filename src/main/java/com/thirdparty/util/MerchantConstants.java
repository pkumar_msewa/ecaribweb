package com.thirdparty.util;

import com.payqwikweb.app.metadatas.UrlMetadatas;

public class MerchantConstants {

	private static final String BASE_URL=UrlMetadatas.HOST+"/";
   
    public static final String AUTHENTICATE_URL = BASE_URL+"Authenticate/Merchant";
    public static final String AUTHENTICATE_USER = BASE_URL+"AuthenticateUser";
    public static final String REGISTER_USER = BASE_URL+"Register";
    public static final String VERIFY_OTP = BASE_URL+"Activate/Mobile";
    public static final String FORGOT_PASSWORD = BASE_URL+"ForgotPassword";
    public static final String VALIDATE_FORGOT_PASSWORD_OTP = BASE_URL+"Change";
    public static final String VALIDATE_USER_OTP = BASE_URL+"VerifyOtp";
    public static final String CHANGE_PASSWORD = BASE_URL+"RenewPassword";
    public static final String PROCESS_PAYMENT = BASE_URL+"Authenticate/Payment";
    public static final String STATUS_CHECK = BASE_URL+"Authenticate/StatusCheck";
    
    public static final String UPI_AUTHENTICATE_URL = BASE_URL+"Authenticate/MerchantUpiAuth";
    public static final String UPI_PAYMENT_URL = BASE_URL+"Authenticate/UpiMerchantPayment";
    public static final String UPI_REDIRECT_URL = BASE_URL+"Authenticate/RedirectMerchantUPI";
    public static final String UPI_STATUS_URL = BASE_URL+"Authenticate/StatusCheckUpi";
    public static final String BESCOM_MERCHANT_REFUND_REQUEST = BASE_URL+"Authenticate/BesomMerchantRefundRequest";
    public static final String BESCOM_S2S_CALL_BACK_URL = "http://www.Bescom.co.in:550/SCP/MyAccount/VijayaBankUPIResponceS2S.aspx";
    public static final String SAVING_S2S_REQ_RESP = BASE_URL+"Authenticate/SavingS2SUpdates";
    

}
