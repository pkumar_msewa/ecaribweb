package com.thirdparty.util;

import com.payqwikweb.app.metadatas.UrlMetadatas;

public class FRMConstants {
	private static String frmUrltest = "https://vb-dcn-frm-tst01.vbank.com:2504"; // Test Url
	private static String frmUrllive = "https://frmapp.vbank.com:2504";
	public static String frmDecideURL = getURL() + "/RDA/rest/rde-decide";
	public static String frmEnqueueURL = getURL() + "/mq/rest/cxq/load/put";
	private static String getURL() {
		return UrlMetadatas.PRODUCTION ? frmUrllive : frmUrltest;
	}
}
