package com.thirdparty.util;

import java.security.KeyManagementException;
import java.security.SecureRandom;
import java.security.Security;
import java.security.cert.CertificateException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.client.urlconnection.HTTPSProperties;

public class Ssltest {

	public static Client createClient() throws Exception{
		  Client restClient = null;
		        try {
		         Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());

		         // Create a trust manager that does not validate certificate chains 
		         TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
		             @Override
		    public void checkClientTrusted(java.security.cert.X509Certificate[] arg0, String arg1)
		      throws CertificateException {
		     // TODO Auto-generated method stub
		     
		    }

		    @Override
		    public void checkServerTrusted(java.security.cert.X509Certificate[] arg0, String arg1)
		      throws CertificateException {
		     // TODO Auto-generated method stub
		     
		    }

		    @Override
		    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
		     // TODO Auto-generated method stub
		     return null;
		    }
		         }};

		         // Ignore differences between given hostname and certificate hostname 
		         HostnameVerifier hv = new HostnameVerifier() {
		           public boolean verify(String hostname, SSLSession session) { return true; }
		         }; 
		         
		         ClientConfig config = new DefaultClientConfig();
		         SSLContext sc = SSLContext.getInstance("TLSv1.2"); //$NON-NLS-1$
		         sc.init(null, trustAllCerts, new SecureRandom());
		         HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
		         HttpsURLConnection.setDefaultHostnameVerifier(hv);
		         config.getProperties().put(HTTPSProperties.PROPERTY_HTTPS_PROPERTIES, new HTTPSProperties(hv, sc));
		         restClient = Client.create(config);
		         System.setProperty("javax.net.debug", "ssl");
		        } catch (KeyManagementException e) {
		         String errorMsg = "client initialization error: " //$NON-NLS-1$
		                 + e.getMessage();
		        System.err.println(errorMsg);
		         throw new Exception(errorMsg, e);
		     }
		  return restClient; 
		 }
}
