package com.thirdparty.model;

/**
 * @author Arshad
 *
 */
public class MessageBodyDto {
	private String host_id ;
	private String channel ;
	private String mobile_no ;
	private String payee_acct_id ;
	private String city_code ;
	private String tran_id ;
	private String payee_id ;
	private String payee_mob_no ;
	private String payee_code ;
	private String branch_id ;
	private String aadhar_no ;
	private String sys_time ;
	private String agent_code ;
	private String acct_name ;
	private String tran_rmks ;
	private String terminal_id ;
	private String payee_ifsc_code ;
	private String auth_type ;
	private String device_id ;
	private String tran_type ;
	private String vpa ;
	private String payee_nick_name ;
	private String ip_address ;
	private String mcc_id;
	private String country_code ;
	private String cust_card_id ;
	private String account_id ;
	private String user_id ;
	private String corporate_id ;
	private String payee_name ;
	private String tran_amt ;
	private String network_type ;
	private String cust_id ;
	private String tran_crncy_code ;
	private String eventts;
	private String virtual_account_id;
	private String event_id;
	private String email_id;
	private String pin_code;
	private String addr_network;
	private String country;
	private String succ_fail_flg;
	private String error_code;
	private String error_desc;	
	private String mfa_type;
	private String lastLogints;
	private String IP_Country;
	private String logints;
	private String user_type;
	private String last_login_ip;
	private String device_type;
	private String account_type;
	
	public String getAccount_type() {
		return account_type;
	}
	public void setAccount_type(String account_type) {
		this.account_type = account_type;
	}
	public String getIP_Country() {
		return IP_Country;
	}
	public void setIP_Country(String iP_Country) {
		IP_Country = iP_Country;
	}
	public String getLogints() {
		return logints;
	}
	public void setLogints(String logints) {
		this.logints = logints;
	}
	public String getUser_type() {
		return user_type;
	}
	public void setUser_type(String user_type) {
		this.user_type = user_type;
	}
	public String getLast_login_ip() {
		return last_login_ip;
	}
	public void setLast_login_ip(String last_login_ip) {
		this.last_login_ip = last_login_ip;
	}
	public String getDevice_type() {
		return device_type;
	}
	public void setDevice_type(String device_type) {
		this.device_type = device_type;
	}
	public String getLastLogints() {
		return lastLogints;
	}
	public void setLastLogints(String lastLogints) {
		this.lastLogints = lastLogints;
	}
	public String getVirtual_account_id() {
		return virtual_account_id;
	}
	public void setVirtual_account_id(String virtual_account_id) {
		this.virtual_account_id = virtual_account_id;
	}
	public String getEvent_id() {
		return event_id;
	}
	public void setEvent_id(String event_id) {
		this.event_id = event_id;
	}
	public String getEmail_id() {
		return email_id;
	}
	public void setEmail_id(String email_id) {
		this.email_id = email_id;
	}
	public String getPin_code() {
		return pin_code;
	}
	public void setPin_code(String pin_code) {
		this.pin_code = pin_code;
	}
	public String getAddr_network() {
		return addr_network;
	}
	public void setAddr_network(String addr_network) {
		this.addr_network = addr_network;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getSucc_fail_flg() {
		return succ_fail_flg;
	}
	public void setSucc_fail_flg(String succ_fail_flg) {
		this.succ_fail_flg = succ_fail_flg;
	}
	public String getError_code() {
		return error_code;
	}
	public void setError_code(String error_code) {
		this.error_code = error_code;
	}
	public String getError_desc() {
		return error_desc;
	}
	public void setError_desc(String error_desc) {
		this.error_desc = error_desc;
	}
	public String getMfa_type() {
		return mfa_type;
	}
	public void setMfa_type(String mfa_type) {
		this.mfa_type = mfa_type;
	}
	public String getMcc_id() {
		return mcc_id;
	}
	public void setMcc_id(String mcc_id) {
		this.mcc_id = mcc_id;
	}
	public String getHost_id() {
		return host_id;
	}
	public void setHost_id(String host_id) {
		this.host_id = host_id;
	}
	public String getChannel() {
		return channel;
	}
	public void setChannel(String channel) {
		this.channel = channel;
	}
	public String getMobile_no() {
		return mobile_no;
	}
	public void setMobile_no(String mobile_no) {
		this.mobile_no = mobile_no;
	}
	public String getPayee_acct_id() {
		return payee_acct_id;
	}
	public void setPayee_acct_id(String payee_acct_id) {
		this.payee_acct_id = payee_acct_id;
	}
	public String getCity_code() {
		return city_code;
	}
	public void setCity_code(String city_code) {
		this.city_code = city_code;
	}
	public String getTran_id() {
		return tran_id;
	}
	public void setTran_id(String tran_id) {
		this.tran_id = tran_id;
	}
	public String getPayee_id() {
		return payee_id;
	}
	public void setPayee_id(String payee_id) {
		this.payee_id = payee_id;
	}
	public String getPayee_mob_no() {
		return payee_mob_no;
	}
	public void setPayee_mob_no(String payee_mob_no) {
		this.payee_mob_no = payee_mob_no;
	}
	public String getPayee_code() {
		return payee_code;
	}
	public void setPayee_code(String payee_code) {
		this.payee_code = payee_code;
	}
	public String getBranch_id() {
		return branch_id;
	}
	public void setBranch_id(String branch_id) {
		this.branch_id = branch_id;
	}
	public String getAadhar_no() {
		return aadhar_no;
	}
	public void setAadhar_no(String aadhar_no) {
		this.aadhar_no = aadhar_no;
	}
	public String getSys_time() {
		return sys_time;
	}
	public void setSys_time(String sys_time) {
		this.sys_time = sys_time;
	}
	public String getAgent_code() {
		return agent_code;
	}
	public void setAgent_code(String agent_code) {
		this.agent_code = agent_code;
	}
	public String getAcct_name() {
		return acct_name;
	}
	public void setAcct_name(String acct_name) {
		this.acct_name = acct_name;
	}
	public String getTran_rmks() {
		return tran_rmks;
	}
	public void setTran_rmks(String tran_rmks) {
		this.tran_rmks = tran_rmks;
	}
	public String getTerminal_id() {
		return terminal_id;
	}
	public void setTerminal_id(String terminal_id) {
		this.terminal_id = terminal_id;
	}
	public String getPayee_ifsc_code() {
		return payee_ifsc_code;
	}
	public void setPayee_ifsc_code(String payee_ifsc_code) {
		this.payee_ifsc_code = payee_ifsc_code;
	}
	public String getAuth_type() {
		return auth_type;
	}
	public void setAuth_type(String auth_type) {
		this.auth_type = auth_type;
	}
	public String getDevice_id() {
		return device_id;
	}
	public void setDevice_id(String device_id) {
		this.device_id = device_id;
	}
	public String getTran_type() {
		return tran_type;
	}
	public void setTran_type(String tran_type) {
		this.tran_type = tran_type;
	}
	public String getVpa() {
		return vpa;
	}
	public void setVpa(String vpa) {
		this.vpa = vpa;
	}
	public String getPayee_nick_name() {
		return payee_nick_name;
	}
	public void setPayee_nick_name(String payee_nick_name) {
		this.payee_nick_name = payee_nick_name;
	}
	public String getIp_address() {
		return ip_address;
	}
	public void setIp_address(String ip_address) {
		this.ip_address = ip_address;
	}
	public String getCountry_code() {
		return country_code;
	}
	public void setCountry_code(String country_code) {
		this.country_code = country_code;
	}
	public String getCust_card_id() {
		return cust_card_id;
	}
	public void setCust_card_id(String cust_card_id) {
		this.cust_card_id = cust_card_id;
	}
	public String getAccount_id() {
		return account_id;
	}
	public void setAccount_id(String account_id) {
		this.account_id = account_id;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getCorporate_id() {
		return corporate_id;
	}
	public void setCorporate_id(String corporate_id) {
		this.corporate_id = corporate_id;
	}
	public String getPayee_name() {
		return payee_name;
	}
	public void setPayee_name(String payee_name) {
		this.payee_name = payee_name;
	}
	public String getTran_amt() {
		return tran_amt;
	}
	public void setTran_amt(String tran_amt) {
		this.tran_amt = tran_amt;
	}
	public String getNetwork_type() {
		return network_type;
	}
	public void setNetwork_type(String network_type) {
		this.network_type = network_type;
	}
	public String getCust_id() {
		return cust_id;
	}
	public void setCust_id(String cust_id) {
		this.cust_id = cust_id;
	}
	public String getTran_crncy_code() {
		return tran_crncy_code;
	}
	public void setTran_crncy_code(String tran_crncy_code) {
		this.tran_crncy_code = tran_crncy_code;
	}
	public String getEventts() {
		return eventts;
	}
	public void setEventts(String eventts) {
		this.eventts = eventts;
	}
	
	
}
