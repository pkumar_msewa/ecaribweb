package com.thirdparty.model;

public class S2SResponse {

	private String status;
	private String receiptId;
	private String wssTransactionID;
	private String transactiondatetime;
	private String transactionpostingdatetime;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getReceiptId() {
		return receiptId;
	}

	public void setReceiptId(String receiptId) {
		this.receiptId = receiptId;
	}

	public String getWssTransactionID() {
		return wssTransactionID;
	}

	public void setWssTransactionID(String wssTransactionID) {
		this.wssTransactionID = wssTransactionID;
	}

	public String getTransactiondatetime() {
		return transactiondatetime;
	}

	public void setTransactiondatetime(String transactiondatetime) {
		this.transactiondatetime = transactiondatetime;
	}

	public String getTransactionpostingdatetime() {
		return transactionpostingdatetime;
	}

	public void setTransactionpostingdatetime(String transactionpostingdatetime) {
		this.transactionpostingdatetime = transactionpostingdatetime;
	}
}
