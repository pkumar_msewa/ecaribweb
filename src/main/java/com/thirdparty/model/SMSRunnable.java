package com.thirdparty.model;


import com.payqwikweb.thirdparty.request.SMSRequest;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class SMSRunnable {
    private String response;

    private SMSRequest dto;

    public SMSRequest getDto() {
        return dto;
    }

    public void setDto(SMSRequest dto) {
        this.dto = dto;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public void run() {
        try {
            URL sendUrl = new URL("https://sms6.routesms.com:8443/bulksms/bulksms?");
            HostnameVerifier hostVerifier = new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };
            trustAllHttpsCertificates();
            HttpURLConnection httpConnection = (HttpURLConnection) sendUrl.openConnection();
            httpConnection.setRequestMethod("POST");
            httpConnection.setDoInput(true);
            httpConnection.setDoOutput(true);
            httpConnection.setUseCaches(false);
            DataOutputStream dataStreamToServer = new DataOutputStream(httpConnection.getOutputStream());
            dataStreamToServer.writeBytes("username=" + URLEncoder.encode(dto.getUsername(), "UTF-8")
                    + "&password=" + URLEncoder.encode(dto.getPassword(), "UTF-8") + "&type="
                    + URLEncoder.encode(dto.getType(), "UTF-8") + "&dlr="
                    + URLEncoder.encode(dto.getDlr(), "UTF-8") + "&destination="
                    + URLEncoder.encode(dto.getDestination(), "UTF-8") + "&source="
                    + URLEncoder.encode(dto.getSource(), "UTF-8") + "&message="
                    + URLEncoder.encode(dto.getMessage(), "UTF-8"));
            dataStreamToServer.flush();
            dataStreamToServer.close();
            BufferedReader dataStreamFromUrl = new BufferedReader(
                    new InputStreamReader(httpConnection.getInputStream()));
            String dataBuffer = "";
            while ((dataBuffer = dataStreamFromUrl.readLine()) != null) {
                response = dataBuffer;
            }
            dataStreamFromUrl.close();
            setResponse(response);
            System.err.print("sms response is :" + response);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private static void trustAllHttpsCertificates() throws Exception {
        // Create a trust manager that does not validate certificate chains:
        javax.net.ssl.TrustManager[] trustAllCerts = new javax.net.ssl.TrustManager[1];
        javax.net.ssl.TrustManager tm = new miTM();
        trustAllCerts[0] = tm;
        javax.net.ssl.SSLContext sc = javax.net.ssl.SSLContext.getInstance("SSL");
        sc.init(null, trustAllCerts, null);
        javax.net.ssl.HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
    }

    public static class miTM implements javax.net.ssl.TrustManager, javax.net.ssl.X509TrustManager {
        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
            return null;
        }

        public boolean isServerTrusted(java.security.cert.X509Certificate[] certs) {
            return true;
        }

        public boolean isClientTrusted(java.security.cert.X509Certificate[] certs) {
            return true;
        }

        public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType)
                throws java.security.cert.CertificateException {
            return;
        }

        public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType)
                throws java.security.cert.CertificateException {
            return;
        }
    }
}
