package com.thirdparty.model;

public class FRMRequestDTO {

	private String eventId;
	private String hostUserId;
	private String eventSubtype;
	private String eventName;
	private String eventType;
	private String source;
	private MessageBodyDto msgBody;
	
	public String getEventId() {
		return eventId;
	}
	public void setEventId(String eventId) {
		this.eventId = eventId;
	}
	public String getHostUserId() {
		return hostUserId;
	}
	public void setHostUserId(String hostUserId) {
		this.hostUserId = hostUserId;
	}
	public String getEventSubtype() {
		return eventSubtype;
	}
	public void setEventSubtype(String eventSubtype) {
		this.eventSubtype = eventSubtype;
	}
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public String getEventType() {
		return eventType;
	}
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public MessageBodyDto getMsgBody() {
		return msgBody;
	}
	public void setMsgBody(MessageBodyDto msgBody) {
		this.msgBody = msgBody;
	}
	
}
