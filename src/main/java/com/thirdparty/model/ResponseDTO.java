package com.thirdparty.model;

import java.util.List;
import java.util.Map;

import org.json.JSONObject;

import com.payqwikweb.app.model.request.UserAnalytics;

public class ResponseDTO {
    private String code;
    private String status;
    private String message;
    private String details;
    private String sessionId;
    private double amount;
    private Object info;
    private boolean success;
    private boolean valid ;
    private String transactionId;
    private String response;
    private JSONObject predictList;
    private String email;
    private String tdate;
    private String refId;
    private String token;
    private String merchantCredentials;
    Map<String, String> transvalue;
    
    
    public Map<String, String> getTransvalue() {
		return transvalue;
	}

	public String getTdate() {
		return tdate;
	}

	public void setTdate(String tdate) {
		this.tdate = tdate;
	}


	public String getRefId() {
		return refId;
	}


	public void setRefId(String refId) {
		this.refId = refId;
	}


	public void setTransvalue(Map<String, String> transvalue) {
		this.transvalue = transvalue;
	}
    private String ebsDate;
    private String paymentId;
    private String ebsStatus;
    
    public String getEbsStatus() {
		return ebsStatus;
	}

	public void setEbsStatus(String ebsStatus) {
		this.ebsStatus = ebsStatus;
	}

	public String getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}

	public String getEbsDate() {
		return ebsDate;
	}

	public void setEbsDate(String ebsDate) {
		this.ebsDate = ebsDate;
	}

	public long getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(long totalPages) {
		this.totalPages = totalPages;
	}

	private long totalPages;
    

	public JSONObject getPredictList() {
		return predictList;
	}

	public void setPredictList(JSONObject predictList) {
		this.predictList = predictList;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Object getInfo() {
        return info;
    }

    public void setInfo(Object info) {
        this.info = info;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getToken() {
		return token;
	}


	public void setToken(String token) {
		this.token = token;
	}


	public String getMerchantCredentials() {
		return merchantCredentials;
	}


	public void setMerchantCredentials(String merchantCredentials) {
		this.merchantCredentials = merchantCredentials;
	}


/*	@Override
	public String toString() {
		return "ResponseDTO [code=" + code + ", status=" + status + ", message=" + message + ", details=" + details
				+ ", sessionId=" + sessionId + ", amount=" + amount + ", info=" + info + ", success=" + success
				+ ", valid=" + valid + ", transactionId=" + transactionId + ", response=" + response + ", predictList="
				+ predictList + "]";*/
public JSONObject toJson(List<UserAnalytics>  dto){
	JSONObject obj =new JSONObject(dto);
	return obj;
	
}
}
