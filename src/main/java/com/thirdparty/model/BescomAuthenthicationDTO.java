package com.thirdparty.model;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

public class BescomAuthenthicationDTO extends AbstractDTO implements JSONWrapper{
    private String ipAddress;
    private long key;
    private String merchanttxnid;
    private double originalamount;
    private double billamount;
    private double additionalcharges;
    private String productinfo;
    private String firstname;	
    private String email;
    private String phone;
    private String lastname;
    private String address1;
    private String address2;
    private String city;
    private String state;
    private String country;
    private String zipcode;
    private String surl;
    private String furl;
    private String curl;
    private String custom_note;   
    private String hash;
    private String additionalInfo;
    private String accountid;
    private String paymenttype;
    private String transactiontype;
    private String subdivisioncode;
    private String consumerid;
    private String Freefield1;
    private String Freefield2;
    private String Freefield3;
    
	public String getFreefield1() {
		return Freefield1;
	}

	public void setFreefield1(String freefield1) {
		Freefield1 = freefield1;
	}

	public String getFreefield2() {
		return Freefield2;
	}

	public void setFreefield2(String freefield2) {
		Freefield2 = freefield2;
	}

	public String getFreefield3() {
		return Freefield3;
	}

	public void setFreefield3(String freefield3) {
		Freefield3 = freefield3;
	}

	public double getBillamount() {
		return billamount;
	}

	public void setBillamount(double billamount) {
		this.billamount = billamount;
	}

	public long getKey() {
		return key;
	}

	public void setKey(long key) {
		this.key = key;
	}

	public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getMerchanttxnid() {
		return merchanttxnid;
	}

	public void setMerchanttxnid(String merchanttxnid) {
		this.merchanttxnid = merchanttxnid;
	}

	public double getOriginalamount() {
		return originalamount;
	}

	public void setOriginalamount(double originalamount) {
		this.originalamount = originalamount;
	}

	public double getAdditionalcharges() {
		return additionalcharges;
	}

	public void setAdditionalcharges(double additionalcharges) {
		this.additionalcharges = additionalcharges;
	}

	public String getProductinfo() {
		return productinfo;
	}

	public void setProductinfo(String productinfo) {
		this.productinfo = productinfo;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getSurl() {
		return surl;
	}

	public void setSurl(String surl) {
		this.surl = surl;
	}

	public String getFurl() {
		return furl;
	}

	public void setFurl(String furl) {
		this.furl = furl;
	}

	public String getCurl() {
		return curl;
	}

	public void setCurl(String curl) {
		this.curl = curl;
	}

	public String getCustom_note() {
		return custom_note;
	}

	public void setCustom_note(String custom_note) {
		this.custom_note = custom_note;
	}

	public String getAccountid() {
		return accountid;
	}

	public void setAccountid(String accountid) {
		this.accountid = accountid;
	}

	public String getPaymenttype() {
		return paymenttype;
	}

	public void setPaymenttype(String paymenttype) {
		this.paymenttype = paymenttype;
	}

	public String getTransactiontype() {
		return transactiontype;
	}

	public void setTransactiontype(String transactiontype) {
		this.transactiontype = transactiontype;
	}

	public String getSubdivisioncode() {
		return subdivisioncode;
	}

	public void setSubdivisioncode(String subdivisioncode) {
		this.subdivisioncode = subdivisioncode;
	}

	public String getConsumerid() {
		return consumerid;
	}

	public void setConsumerid(String consumerid) {
		this.consumerid = consumerid;
	}

	@Override
    public JSONObject toJSON() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("transactionID",getMerchanttxnid());
            jsonObject.put("ipAddress",getIpAddress());
            jsonObject.put("id",getKey());
            jsonObject.put("hash",getHash());
            jsonObject.put("amount",getBillamount());
            jsonObject.put("additionalcharges",getAdditionalcharges());
            jsonObject.put("productinfo",getProductinfo());
            jsonObject.put("firstname",getFirstname());
            jsonObject.put("email",getEmail());
            jsonObject.put("phone",getPhone());
            jsonObject.put("lastname",getLastname());
            jsonObject.put("address1",getAddress1());
            jsonObject.put("address2",getAddress2());
            jsonObject.put("city",getCity());
            jsonObject.put("state",getState());
            jsonObject.put("country",getCountry());
            jsonObject.put("zipcode",getZipcode());
            jsonObject.put("surl",getSurl());
            jsonObject.put("furl",getFurl());
            jsonObject.put("curl",getCurl());
            jsonObject.put("custom_note",getCustom_note());
            jsonObject.put("accountid",getAccountid());
            jsonObject.put("paymenttype",getPaymenttype());
            jsonObject.put("transactiontype",getTransactiontype());
            jsonObject.put("subdivisioncode",getSubdivisioncode());
            jsonObject.put("consumerid",getConsumerid());
            jsonObject.put("locationCode",getFreefield1());
            jsonObject.put("billNo",getFreefield2());
            return jsonObject;
        } catch (JSONException e) {
            return null;
        }

    }

	
}
