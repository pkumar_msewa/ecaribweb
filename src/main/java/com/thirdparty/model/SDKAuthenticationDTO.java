package com.thirdparty.model;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

public class SDKAuthenticationDTO extends AbstractDTO implements JSONWrapper {
	private String ipAddress;
	private String amount;
	private String hash;
	private String additionalInfo;
	private String txnid;
	private String key;
	private String username;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getTxnid() {
		return txnid;
	}

	public void setTxnid(String txnid) {
		this.txnid = txnid;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getAdditionalInfo() {
		return additionalInfo;
	}

	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	@Override
	public JSONObject toJSON() {
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("transactionID", getTransactionID());
			jsonObject.put("ipAddress", getIpAddress());
			jsonObject.put("id", getId());
			jsonObject.put("hash", getHash());
			jsonObject.put("amount", getAmount());
			jsonObject.put("username",getUsername());
			return jsonObject;
		} catch (JSONException e) {
			return null;
		}

	}
}
