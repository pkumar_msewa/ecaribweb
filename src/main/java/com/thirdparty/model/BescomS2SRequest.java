package com.thirdparty.model;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

public class BescomS2SRequest implements JSONWrapper {

	private String key;
	private String merchanttxnid;
	private String pgtransactionid;
	private String status;
	private double billamount;
	private double additionalcharges;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getMerchanttxnid() {
		return merchanttxnid;
	}

	public void setMerchanttxnid(String merchanttxnid) {
		this.merchanttxnid = merchanttxnid;
	}

	public String getPgtransactionid() {
		return pgtransactionid;
	}

	public void setPgtransactionid(String pgtransactionid) {
		this.pgtransactionid = pgtransactionid;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public double getBillamount() {
		return billamount;
	}

	public void setBillamount(double billamount) {
		this.billamount = billamount;
	}

	public double getAdditionalcharges() {
		return additionalcharges;
	}

	public void setAdditionalcharges(double additionalcharges) {
		this.additionalcharges = additionalcharges;
	}

	@Override
	public JSONObject toJSON() {
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("key", getKey());
			jsonObject.put("merchanttxnid", getMerchanttxnid());
			jsonObject.put("pgtransactionid", getKey());
			jsonObject.put("status", getStatus());
			jsonObject.put("billamount", getBillamount());
			jsonObject.put("additionalcharges", getAdditionalcharges());
			return jsonObject;
		} catch (JSONException e) {
			return null;
		}
	}

}
