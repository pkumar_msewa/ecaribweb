package com.thirdparty.model;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.payqwikweb.util.ConvertUtil;

public class S2SRequest implements JSONWrapper {

	private String key;
	private String pgtransactionid;
	private String merchanttxnid;
	private String status;
	private String code;
	private String billamount;
	private String additionalcharges;
	private String phone;
	private String email;
	private String transactiondatetime;
	private String hash;
	private String s2sCallUrl;
	private String msg;
	private boolean rural;
	
	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public boolean isRural() {
		return rural;
	}

	public void setRural(boolean rural) {
		this.rural = rural;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getPgtransactionid() {
		return pgtransactionid;
	}

	public void setPgtransactionid(String pgtransactionid) {
		this.pgtransactionid = pgtransactionid;
	}

	public String getMerchanttxnid() {
		return merchanttxnid;
	}

	public void setMerchanttxnid(String merchanttxnid) {
		this.merchanttxnid = merchanttxnid;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getBillamount() { 
		return billamount;
	}

	public void setBillamount(String billamount) {
		this.billamount = billamount;
	}

	public String getAdditionalcharges() {
		return additionalcharges;
	}

	public void setAdditionalcharges(String additionalcharges) {
		this.additionalcharges = additionalcharges;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTransactiondatetime() {
		return transactiondatetime;
	}

	public void setTransactiondatetime(String transactiondatetime) {
		this.transactiondatetime = transactiondatetime;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public String getS2sCallUrl() {
		return s2sCallUrl;
	}

	public void setS2sCallUrl(String s2sCallUrl) {
		this.s2sCallUrl = s2sCallUrl;
	}

	@Override
	public JSONObject toJSON() {
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("key", getKey());
			jsonObject.put("pgtransactionid", getPgtransactionid());
			jsonObject.put("merchanttxnid", getMerchanttxnid());
			jsonObject.put("status", getStatus());
			jsonObject.put("code", getCode());
			jsonObject.put("billamount",  getBillamount());
			jsonObject.put("additionalcharges",  0);
//			jsonObject.put("additionalcharges",  getAdditionalcharges());
			jsonObject.put("phone", getPhone());
			jsonObject.put("email", getEmail());
			jsonObject.put("transactiondatetime", getTransactiondatetime());
			jsonObject.put("s2sCallUrl", getS2sCallUrl());
			
			jsonObject.put("hash", getHash());
			return jsonObject;
		} catch (JSONException e) {
			return null;
		}
	}
	
	public static void main(String[] args) {
//		S2SRequest dto = ConvertUtil.convertS2SRequest("", "", "", "", "", 15.0, 0.0, "", "", "", "");
//		System.err.println(dto.toJSON());
	}
}
