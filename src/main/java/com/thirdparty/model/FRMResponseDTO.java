package com.thirdparty.model;

public class FRMResponseDTO {

	private boolean success;
	private String code;
	private String message;
	private String decideResp;
	private boolean enqueueResp;
	
	public boolean isEnqueueResp() {
		return enqueueResp;
	}

	public void setEnqueueResp(boolean enqueueResp) {
		this.enqueueResp = enqueueResp;
	}

	public String getDecideResp() {
		return decideResp;
	}

	public void setDecideResp(String decideResp) {
		this.decideResp = decideResp;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
