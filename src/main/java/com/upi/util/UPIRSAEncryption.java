package com.upi.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.spec.RSAPublicKeySpec;

import javax.crypto.Cipher;

//import sun.misc.BASE64Encoder;


public class UPIRSAEncryption {
	private static String data = "VJBF99421A6C49D19B9754494EEF680M"; // Msg Id
	private static String mod = "20648977412592243540425694961280366521946406423458648114071207934068973358187481747224974249765790671676412859090527804475515140254950734961859876084728776369883210562867962351450817615137560859164075889744989299299599996234426340691218384857194467754927554316954616537863898860436172618933993441657027261024603761788877096618105501320413840616354797694872480211513488952418274882819855478133951553108866112467949648086238847076801708887736952913416125146237554773216177283569824818858787552298968437086022832076855218153852362009514872841082748488624165291418853168563370436398183762000945273606653786863570958713037";

	public static void main(String[] args) {
		try {
			UPIRSAEncryption rsa = new UPIRSAEncryption();
			PublicKey publicKey = rsa.getPublicKey();
			System.out.println(publicKey);
			System.out.println("------------------------------------------------------------------------");
//			
//			UPIRSAEncryption rsaObj = new UPIRSAEncryption();
//			String encryptedData = UPIRSAEncryption.encryptData(data, mod);
//			System.err.println(encryptedData);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String encryptData(String data, String mod) throws IOException {
		System.out.println("\n----------------ENCRYPTION STARTED------------");
		System.out.println();
		System.out.println("Data Before Encryption :" + data);
		byte[] dataToEncrypt = data.getBytes();
		byte[] encryptedData = null;
		String finalEncryptedData = "";
		try {
			String exp = "65537";
			BigInteger modulus = new BigInteger(mod);
			BigInteger exponent = new BigInteger(exp);
			RSAPublicKeySpec rsaPublicKeySpec = new RSAPublicKeySpec(modulus, exponent);
			KeyFactory fact = KeyFactory.getInstance("RSA");
			PublicKey publicKey = fact.generatePublic(rsaPublicKeySpec);
			Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
			cipher.init(Cipher.ENCRYPT_MODE, publicKey);
			encryptedData = cipher.doFinal(dataToEncrypt);
		//	finalEncryptedData = new BASE64Encoder().encode(encryptedData).toString();
			System.out.println("Encryted Data1: " + finalEncryptedData);
//			finalEncryptedData = finalEncryptedData.replaceAll("(\\r\\n)", "");
//			System.out.println("Encryted Data2: " + finalEncryptedData);
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println();
		System.out.println("----------------ENCRYPTION COMPLETED------------");
		return finalEncryptedData;
	}

	public PublicKey getPublicKey() {
		PublicKey publicKey = null;
		try {
			System.out.println("Certificate path : " + UPIConstants.CERTIFICATE_KEY_PATH);
			Certificate cert = getCertificate(UPIConstants.CERTIFICATE_KEY_PATH);
			publicKey = cert.getPublicKey();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return publicKey;
	}

	private Certificate getCertificate(String file) throws CertificateException, FileNotFoundException {
		CertificateFactory cf = CertificateFactory.getInstance("X.509");
		InputStream is = new FileInputStream(new File(file));
		InputStream caInput = new BufferedInputStream(is);
		Certificate ca;
		try {
			ca = cf.generateCertificate(caInput);
			return ca;
		} finally {
			try {
				caInput.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}