package com.upi.util;

import java.security.MessageDigest;
import java.util.Arrays;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.net.util.Base64;

public class DesEncryption {
	
	public static String desEncrypt(String data, String Key) throws Exception {
		final MessageDigest md = MessageDigest.getInstance("md5");
		final byte[] digestOfPassword = md.digest(Key.getBytes("utf-8"));
		final byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
		for (int j = 0, k = 16; j < 8;) {
			keyBytes[k++] = keyBytes[j++];
		}
		final SecretKey key = new SecretKeySpec(keyBytes, "DESede");
		final IvParameterSpec iv = new IvParameterSpec(new byte[8]);
		final Cipher cipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");
		cipher.init(Cipher.ENCRYPT_MODE, key, iv);
		final byte[] plainTextBytes = data.getBytes("utf-8");
		final byte[] cipherText = cipher.doFinal(plainTextBytes);
		return Base64.encodeBase64String(cipherText);
	}
	
	public static void main(String[] args) {
		String data = "VJBF99421A6C49D19B9754494EEF311M";
		String key = UPIConstants.KEK;
		try {
			String  desEncrypt =  DesEncryption.desEncrypt(data, key);
			System.out.println("DesEncrypt :: " + desEncrypt);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
