package com.upi.util;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;

public class UpiTest {
	
	public static String GenerateMerchantDEK = "<v:Envelope "
			 + " xmlns:i=\"http://www.w3.org/2001/XMLSchemainstance" +'"'
			 + " xmlns:d=\"http://www.w3.org/2001/XMLSchema" +'"'
			 + " xmlns:c=\"http://schemas.xmlsoap.org/soap/encoding/" +'"'
			 + " xmlns:v=\"http://schemas.xmlsoap.org/soap/envelope/\"> "
				+ "<v:Header/>"  
			   + "<v:Body>"  
			       + "<n0:GenerateMerchantDEK id=\"o0\" c:root=\"1"+'"'+ " xmlns:n0=\"http://com/fss/upi\">" 
			       		+ "<n0:req  i:type=\"n0:req\">"  
			                + "<n0:UPI i:type=\"n0:UPI\">"  
			       	
				                + "<TimeStamp i:type=\"d:string\">140116155016</TimeStamp>"
				                + "<MsgId i:type=\"d:string\">VJBF99421A6C49D19B9754494EEF222M</MsgId>"
				                
				                + "<DeviceID i:type=\"d:string\">V4FIIFEUCU5L79R8</DeviceID>" 
				                + "<Channel i:type=\"d:string\">07</Channel>"  
				                + "<AppVersion i:type=\"d:string\">1.0.0</AppVersion>" 
				                + "<PayerType i:type=\"d:string\">PERSON</PayerType>" 
				                + "<OrgId i:type=\"d:string\">400053</OrgId>" 
				                + "<BankId i:type=\"d:string\">454545</BankId>" 
				                + "<Remarks i:type=\"d:string\"/>"  
				                
				                + "<MobileNo i:type=\"d:string\">7795221595</MobileNo>" 
				                
				                + "<MerchantID i:type=\"d:string\">VIJBVPAYQWIK</MerchantID>"  
				                + "<SubMerchantID i:type=\"d:string\">VPAYQWIK01</SubMerchantID>" 
				                + "<TerminalID i:type=\"d:string\">VPAYQWIK01</TerminalID>" 
				                + "<MerchantCredentials i:type=\"d:string\"/>"  
			               + "</n0:UPI>"  
			               + "<reqData>"  
			                   + " <data i:type=\"d:string\">4519b33b2068eb49c7b2576520307cf343f8bd3c8a669f605ec64edab570a239</data>"  
			               + "</reqData>"
			           + "</n0:req>" 
			       + "</n0:GenerateMerchantDEK>"  
			   + "</v:Body>"  
			+ "</v:Envelope>";
	
	
	public static String MerchantTokenGen = "<v:Envelope "
			 + " xmlns:i=\"http://www.w3.org/2001/XMLSchemainstance" +'"'
			 + " xmlns:d=\"http://www.w3.org/2001/XMLSchema" +'"'
			 + " xmlns:c=\"http://schemas.xmlsoap.org/soap/encoding/" +'"'
			 + " xmlns:v=\"http://schemas.xmlsoap.org/soap/envelope/\"> "
			 + "<v:Header/>"  
			 + "<v:Body>"  
			+"<n0:MerchantTokenGen c:root=\"1\" id=\"o0" +'"'
			+ " xmlns:n0=\"http://com/fss/upi" +'"'+ " xmlns:c=\"http://schemas.xmlsoap.org/soap/encoding/\">"
					+ "<n0:req i:type=\"n0:req" +'"'+" xmlns:i=\"http://www.w3.org/1999/XMLSchema-instance\">"
					      + "<n0:UPI i:type=\"n0:UPI\">"
					      + "<n1:TokenCode i:type=\"d:string" +'"' +" xmlns:n1=\"java:com.fss.upi.req\">Dro8e647mo.stFBAs8ms</n1:TokenCode>"
					      + "<n2:PackageName i:type=\"d:string" +'"' +"xmlns:n2=\"java:com.fss.upi.req\">com.fss.vijmersdk</n2:PackageName>"
					      + "<n3:BankId i:type=\"d:string" +'"' +" xmlns:n3=\"java:com.fss.upi.req\">454545</n3:BankId>"
					      + "<n4:MerchantID i:type=\"d:string" +'"' +" xmlns:n4=\"java:com.fss.upi.req\">VIJBVPAYQWIK</n4:MerchantID>"
					      + "<n5:PayeeType i:type=\"d:string" +'"' +"  xmlns:n5=\"java:com.fss.upi.req\">ENTITY</n5:PayeeType>"
					      + "<n6:OrgId i:type=\"d:string" +'"' +" xmlns:n6=\"java:com.fss.upi.req\">400053</n6:OrgId>"
					      + "<n7:TerminalID i:type=\"d:string" +'"' +" xmlns:n7=\"java:com.fss.upi.req\">VPAYQWIK01</n7:TerminalID>"
					      + "<n8:PayerType i:type=\"d:string"+'"' +"  xmlns:n8=\"java:com.fss.upi.req\">PERSON</n8:PayerType>"
					      + "<n9:PayerCode i:type=\"d:string" +'"' +" xmlns:n9=\"java:com.fss.upi.req\">0000</n9:PayerCode>"
					      + "<n10:Channel i:type=\"d:string" +'"' +"  xmlns:n10=\"java:com.fss.upi.req\">07</n10:Channel>"
					      + "<n11:RefId i:type=\"d:string" +'"' +" xmlns:n11=\"java:com.fss.upi.req\">123456789020</n11:RefId>"
					      + "<n12:SdkVersion i:type=\"d:string" +'"' +"  xmlns:n12=\"java:com.fss.upi.req\">1.0.0</n12:SdkVersion>"
					      + "<n13:MsgId i:type=\"d:string" +'"' +"  xmlns:n13=\"java:com.fss.upi.req\">VJBF12354LKIOPU125478965422000744</n13:MsgId>"
					      + "<n14:TimeStamp i:type=\"d:string" +'"' +"  xmlns:n14=\"java:com.fss.upi.req\">140116155026</n14:TimeStamp>"
					      + "<n15:UserID i:type=\"d:string" +'"' +"  xmlns:n15=\"java:com.fss.upi.req\">UserID</n15:UserID>"
					      + "<n16:MobileNo i:type=\"d:string" +'"' +"  xmlns:n16=\"java:com.fss.upi.req\">7795221595</n16:MobileNo>"
					      + "<n17:SdkName i:type=\"d:string" +'"' +"  xmlns:n17=\"java:com.fss.upi.req\">VIJAYA BANK</n17:SdkName>"
					      + "<n18:Remarks i:type=\"d:string"+'"' +"  xmlns:n18=\"java:com.fss.upi.req\"/>"
					      + "<n19:PayeeCode i:type=\"d:string" +'"' +"  xmlns:n19=\"java:com.fss.upi.req\">5172</n19:PayeeCode>"
					      + "<n20:CurrencyType i:type=\"d:string" +'"' +"  xmlns:n20=\"java:com.fss.upi.req\">INR</n20:CurrencyType>"
					      
					      + "<n21:MerchantCredentials i:type=\"d:string" +'"' +"  xmlns:n21=\"java:com.fss.upi.req\">r1I3pqMMHW6pK6gQ8vYUngmTp/IGurgxiAbjo4FHKxY=</n21:MerchantCredentials>"
					     
					      + "<n22:AppVersion i:type=\"d:string" +'"' +"  xmlns:n22=\"java:com.fss.upi.req\">1.0</n22:AppVersion>"
					      + "<n23:DeviceID i:type=\"d:string" +'"' +"  xmlns:n23=\"java:com.fss.upi.req\">V4FIIFEUCU5L79R8</n23:DeviceID>"
					      + "<n24:UserPwd i:type=\"d:string" +'"' +" xmlns:n24=\"java:com.fss.upi.req\">Vijaya@12345</n24:UserPwd>"
					      + "<n25:SubMerchantID i:type=\"d:string"+'"' +"  xmlns:n25=\"java:com.fss.upi.req\"/>"
					      + " </n0:UPI>"
					  + " </n0:req>"
					+ "</n0:MerchantTokenGen>" 
					+ "</v:Body>"  
					+ "</v:Envelope>";

	 public static String MerchantValidateVPA = "<soapenv:Envelope"
			+" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/"+'"'
			+" xmlns:upi=\"http://com/fss/upi" +'"'
			+" xmlns:java=\"java:com.fss.upi.req\">"
			+" <soapenv:Header/>"
			+" <soapenv:Body>"
			+" <upi:MerchantValidateVPA>"
				+"<upi:req>"
					+"<java:UPI>"
						+"<java:Channel>07</java:Channel>"
						+"<java:MobileNo>7795221595</java:MobileNo>"
						+"<java:MsgId>UOBA557E639327E4499811FCE4CF3B116TN</java:MsgId>"
						+"<java:OrgId>400045</java:OrgId>"
						+"<java:BankId>504432</java:BankId>"
						+"<java:Remarks>?</java:Remarks>"
						+"<java:TimeStamp>1500387360</java:TimeStamp>"
						+"<java:DeviceID>V4FIIFEUCU5L79R8</java:DeviceID>"
						+"<java:PayerType>PERSON</java:PayerType>"
						+"<java:SubMerchantID></java:SubMerchantID>"
	                    +"<java:MerchantID>2merc</java:MerchantID>"
	                    +"<java:TerminalID>term123</java:TerminalID>"
	                    +"<java:MerchantCredentials>M6H8YQF+pvvE9GkKypWvoxAFZwSXejSjiuISEbEBG+Q75B2T7qqEbCzlEYy9VEkk</java:MerchantCredentials>"
	                    +"<java:AppVersion>1.0</java:AppVersion>"
	                    +"<java:RefId>705912889323</java:RefId>"
	                    +"<java:SdkVersion>1.0.0</java:SdkVersion>"
	                    +"<java:SdkName>UNION BANK OF INDIA</java:SdkName>"
	                    +"<java:PayeeCode>4216</java:PayeeCode>"
	                    +"<java:CurrencyType>INR</java:CurrencyType>"
	                    +"<java:PayerCode>0000</java:PayerCode>"
	                    +"<java:PayeeType>ENTITY</java:PayeeType>"
	                +"</java:UPI>"
	                +"<java:PayerVirAddr>prashant@msewa.com</java:PayerVirAddr>"
	                +"<java:PayerAccNo>656434864839880</java:PayerAccNo>"
	                +"<java:PayerCode>0000</java:PayerCode>"
	                +"<java:AddrType>VIR</java:AddrType>"
	                +"<java:PayeeVirAddr>jaikumar@vijb</java:PayeeVirAddr>"
	            +"</upi:req>"	
			+" </upi:MerchantValidateVPA>"
			+" </soapenv:Body>"
			+" </soapenv:Envelope>";
	 
	 public static String MerchantCollectMoney = "<soapenv:Envelope"
				+" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/"+'"'+" xmlns:upi=\"http://com/fss/upi" +'"'+" xmlns:java=\"java:com.fss.upi.req\">"
				+" <soapenv:Header/>"
				+" <soapenv:Body>"
				+" <upi:MerchantCollectMoney>"
					+"<req>"
						+"<Amount>10.00</Amount>"
						+"<Expdate>18-08-2016 17:38</Expdate>"
						+"<NickName></NickName>"
						+"<PayerVirAddr>mohamed@idfb</PayerVirAddr>"
						+"<PayeeVirAddr>suganyar@idfb</PayeeVirAddr>"
						+"<payeeTranserType>VIR</payeeTranserType>"
						+"<PayeeAccNo>10000234324</PayeeAccNo>"
						+"<GeoCode>12.8358977,80.2224368</GeoCode>"
						+"<DevLocation>INDIA</DevLocation>"
						+"<DevIp>V4FIIFEUCU5L79R8</DevIp>"
						+"<DevType>ANDROID</DevType>"
						+"<DevOs>ANDROID</DevOs>"
						+"<DevApp>com.fss.idfcpsp</DevApp>"
						+"<DevCapability>1000</DevCapability>"
					+"<DevId>WEB</DevId>"
					+"<PayerCode>00001</PayerCode>"
					+"<PayeeCode>0003</PayeeCode>"
					+"<CurrencyCode>INR</CurrencyCode>"
					+"<AddrType>ACCOUNT</AddrType>"
					+"<TransferMode >VIR</TransferMode>"
					+"<VirAddr >PREMILA</VirAddr>"
					+"<UPI>"
						+"<Channel>03</Channel>"
						+"<MobileNo>9841530835</MobileNo>"
						+"<MsgId>IDFE09E7A4E242FCAF6760DEE770BE2B461</MsgId>"
						+"<OrgId>400054</OrgId>"
						+"<BankId>401613</BankId>"
						+"<Remarks>test</Remarks>"
						+"<TimeStamp>140116155016</TimeStamp>"
						+"<DeviceID>IrisX8015D0031486</DeviceID>"
						+"<PayerType>Person</PayerType>"
						+"<AppVersion>1.0.52</AppVersion>"
						+"<MerchantID >12345</MerchantID>"
						+"<SubMerchantID >12345</SubMerchantID>"
						+"<TerminalID >01</TerminalID>"
						+"<MerchantCredentials>609780A033C2A45592EE565E1C8D3D15201C5A092FD015EB6F1CEE8AA7E14C0DC436152708CEFC2077841AC17B2C6552B0FDBD377D688CDC95B91846DC5CD2D51E98E06644542AFD00F984675A6929E2BC23444942D0CD5BE38D40864CF458526599028EAF6697F5AAD179DE51B28FC2B41ABE86DAAD0AA7D972188195CE6468B58A90CBC6CF6621B827F2FADCBF89253EC3D09C3876D1D2C7EE8583804D3BFF6CF72FA168996E9331CB2C6CE2E95E8E278AF97542637762DE1CBFF4B95CAD7C45D4A7452A7FC94AADBD62349DEDBC5B55EF5BED67B25D72EF302E81F92C1DD5910A949C87EA726133702B974CD3CDC2718AB120252BD2311BE2A0F83312D3E3</MerchantCredentials>"
					+"</UPI>"
					+"<RefId>705912889323</RefId>"
					+"<SdkVersion>1.0.0</SdkVersion>"
					+"<SdkName>UNION BANK OF INDIA</SdkName>"
					+"<PayeeCode>4216</PayeeCode>"
					+"<CurrencyType>INR</CurrencyType>"
					+"<PayerCode>0000</PayerCode>"
					+"<PayeeType>ENTITY</PayeeType>"
		            +"</req>"	
				+" </upi:MerchantCollectMoney>"
				+" </soapenv:Body>"
				+" </soapenv:Envelope>";
	 
	 public static void main(String[] args) throws Exception {
		 	Client restClient = UPIConstants.createClient();
			WebResource webResource = restClient.resource("https://vijayaupitest.fssnet.co.in/UPIMerchantService");
			restClient.addFilter(new LoggingFilter(System.out));
			ClientResponse resp = webResource.type("application/xml")
					.post(ClientResponse.class, UpiTest.MerchantTokenGen);
			String output = resp.getEntity(String.class);
			System.out.println("response: " + output);
			
//		 System.out.println(UpiTest.pp);pp
	}
}

