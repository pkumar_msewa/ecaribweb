package com.upi.util;

import java.security.KeyManagementException;
import java.security.SecureRandom;
import java.security.Security;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import com.payqwikweb.app.metadatas.UrlMetadatas;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.client.urlconnection.HTTPSProperties;

public class UPIBescomContstant {
	
	
//	public static final String KEK = getKek();
	public static final String MERCHANT_SERVICE_URL = getMerchantServiceurl();
//	public static final String MERCHANT_MOBILE_SERVICE_URL = getMerchantMobileServiceurl();
//	public static final String TRANSACTION_PWD = getTransactionPwd();
//	public static final String PAYEE_VIR_ADDR = getPayeeVirAddr();
//	public static final String PAYEE_MOBILE_NUM = getPayeeMobileNum();
//	public static final String MERCHANT_ID = getMerchantId();
//	public static final String VPA_TERMINAL_ID = getVpaTerminalId();
//	public static final String PLAN_DEK = getPlanDek();
//	public static final String PUBLIK_KEY = getPublicKey();
//	public static final String ORG_ID = getOrgId();
	
//  Live Credentials
	public static final String KEK_LIVE = "512811735867241930665414";//LIVE
	public static final String MERCHANT_SERVICE_URL_LIVE = "https://119.226.198.41/UPIMerchantWebService";
	public static final String TRANSACTION_PWD_LIVE = "2NRzBYWq";
	public static final String PAYEE_VIR_ADDR_LIVE = "bescom0001-01@vijb";
	public static final String PAYEE_MOBILE_NUM_LIVE = "7899986505";
	public static final String MERCHANT_ID_LIVE = "BESCOM0001";
	public static final String VPA_TERMINAL_ID_LIVE = "BESCOM000101";
	public static final String PLAN_DEK_LIVE = "NJ9JlJ3DXxiODlJvXES7Bet4gWBCUXId+CxahBP7GW8=";
	public static final String PUBLIK_KEY_LIVE = "18584734949748129348849857141620953954819484203832016646104433141815430957032192117783822264561583791580185935739301185152124796876993340309733339498682846995007548366727214935469941376925205875865062524360863866757996919721544442901654951783303666690089302449927804568258027064236249287847695772746455299589639930132157299166935013542678585901544669476819590371724082500261427629327033410512044984442683517405034006111321406104438549233219092449425200599492051824692485750924500922642283172887832569244032561943468918623650691766472899441889031313116837607310902224998915086797009215150706860576318749374800455247337";
	public static final String ORG_ID_LIVE = "159029";
	
//	Send Box Credentials
	public static final String MERCHANT_SERVICE_URL_TEST = "https://vijayaupitest.fssnet.co.in/UPIMerchantWebService";
	public static final String TRANSACTION_PWD_TEST = "0kzxOW3E";
	public static final String PAYEE_VIR_ADDR_TEST = "bescomuat01@vijb";	
	public static final String PAYEE_MOBILE_NUM_TEST = "7795221595";
	public static final String KEK_TEST = "239001455628013697380877";//TEST
	public static final String MERCHANT_ID_TEST = "BESCOMUAT";
	public static final String VPA_TERMINAL_ID_TEST = "bescomuat01@vijb";
	public static final String PLAN_DEK_TEST = "4O/m92UUowmaeR6EMjQqU2UmX90y686uyed7wGNtlXU=";
	public static final String PUBLIK_KEY_TEST = "20648977412592243540425694961280366521946406423458648114071207934068973358187481747224974249765790671676412859090527804475515140254950734961859876084728776369883210562867962351450817615137560859164075889744989299299599996234426340691218384857194467754927554316954616537863898860436172618933993441657027261024603761788877096618105501320413840616354797694872480211513488952418274882819855478133951553108866112467949648086238847076801708887736952913416125146237554773216177283569824818858787552298968437086022832076855218153852362009514872841082748488624165291418853168563370436398183762000945273606653786863570958713037";
	public static final String ORG_ID_TEST = "400053";
	
	public static final String MER_TNX_PASS = "Bescom@123";
	public static final String SUB_MERCHANT_ID = "BESCOMUAT01";
	public static final String TERMINAL_ID = "BESCOMUAT01";
	
	public static final String MERCHANT_CREDENTIALS = "RxsKxgh8Su2vPLieN6j3VlNjmehgMyYFhpCLskfMR7Y=";
	public static final String VIJAYA_VPA_EXT = "@vijb";
	public static final String VPA_DEVICE_ID = "WEB";
	public static final String VPA_CHENNEL = "08";
	public static final String DEVICE_ID = "V4FIIFEUCU5L79R8";
	public static final String CHENNEL = "08";
	public static final String APP_VERSION = "1.0.0";
	public static final String SDK_VERSION = "1.0.0";
	public static final String PAYER_TYPE = "PERSON";
	public static final String VPA_PAYER_TYPE = "ENTITY";
	
	public static final String BANK_ID = "454545";
	public static final String USER_ID = "UserID";
	public static final String TOKEN_CODE = "Dro8e647mo.stFBAs8ms";
	public static final String PACKGE_NAME = "com.fss.vijmersdk";
	public static final String PAYEE_TYPE = "ENTITY";
	public static final String VPA_PAYEE_TYPE = "PERSON";
	public static final String PAYER_CODE = "0000";
	public static final String PAYEE_CODE = "5172";
	public static final String SKD_NAME = "VIJAYA BANK";
	public static final String CURRENCY_TYPE = "INR";
	public static final String REQ_DATA = "4519b33b2068eb49c7b2576520307cf343f8bd3c8a669f605ec64edab570a239";

	public static final String VIR_ADDR = "VIRTUAL";
	public static final String PAYEE_TRANSFER_TYPE = "VIR";
	public static final String GEO_CODE = "11.011233,44.055677";
	public static final String DEV_LOCATION = "INDIA";
	public static final String DEV_IP = "100.100.100.100";
	public static final String DEV_TYPE = "WEB";
	public static final String DEV_OS = "WEB";
	public static final String DEV_ID = "WEB";
	public static final String DEVICE_ID_CM = "WEB";
	public static final String DEV_APP = "com.fss.vijb";
	public static final String DEV_CAPABILITY = "1000";
	public static final String CURRENCY_CODE = "INR";
	public static final String ADD_TYPE = "ACCOUNT";
	public static final String TRANFER_MODE = "VIR";
	
	public static final String REMARK = "Collect Request";
	public static final String STATUS_APP_VERSION = "1.0.45";
	public static final String STATUS_REMARK = "UPI";
	
	
//	Payer Virtual Address :  testvpa@vijb

//	public static final String UPI_DEVICE_ID = "";
//	public static final String UPI_DEVICE_ID = "";
//	public static final String UPI_DEVICE_ID = "";
//	public static final String UPI_DEVICE_ID = "";
	
//	public static final String CERTIFICATE_KEY_PATH = "D:/Projects/Work Spaces/TriPayWebWorkSpace/VpayQwik-Web/WebContent/Certificates/vijayaupitest_fssnet_co_in.crt";
	public static final String CERTIFICATE_KEY_PATH = "D:/Projects/Work Spaces/TriPayWebWorkSpace/VpayQwik-Web/WebContent/Certificates/vijayaupi_fssnet_co_in.crt";
//	public static final String CERTIFICATE_KEY_PATH = "D:/Projects/Work Spaces/TriPayWebWorkSpace/VpayQwik-Web/WebContent/Certificates/vijayaupitest_fssnet_co_in.crt";

	 public static Client createClient() throws Exception{
			Client restClient = null;
	        try {
		        Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());

		        // Create a trust manager that does not validate certificate chains 
		        TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {

					@Override
					public void checkServerTrusted(java.security.cert.X509Certificate[] arg0, String arg1)
							throws CertificateException {
						// TODO Auto-generated method stub
						
					}

					@Override
					public java.security.cert.X509Certificate[] getAcceptedIssuers() {
						// TODO Auto-generated method stub
						return null;
					}

					@Override
					public void checkClientTrusted(X509Certificate[] chain, String authType)
							throws CertificateException {
						// TODO Auto-generated method stub
						
					}
		        }};

		        // Ignore differences between given hostname and certificate hostname 
		        HostnameVerifier hv = new HostnameVerifier() {
		          public boolean verify(String hostname, SSLSession session) { return true; }
		        }; 
		        
		        ClientConfig config = new DefaultClientConfig();
		        SSLContext sc = SSLContext.getInstance("TLSv1.2"); //$NON-NLS-1$
		        sc.init(null, trustAllCerts, new SecureRandom());
		        sc.init(null, trustAllCerts, new SecureRandom());
		        HttpsURLConnection.setDefaultHostnameVerifier(hv);
		        
		        config.getProperties().put(HTTPSProperties.PROPERTY_HTTPS_PROPERTIES, new HTTPSProperties(hv, sc));
		        restClient = Client.create(config);
		        System.setProperty("javax.net.debug", "ssl");
	        } catch (KeyManagementException e) {
		        String errorMsg = "client initialization error: " //$NON-NLS-1$
		                + e.getMessage();
		       System.err.println(errorMsg);
		        throw new Exception(errorMsg, e);
		    }
			return restClient; 
	 }
	 
	 public static String getKek() {
			if (UrlMetadatas.PRODUCTION) {
				return KEK_LIVE;
			} else {
				return KEK_TEST;
			}
		}

		public static String getMerchantServiceurl() {
			if (UrlMetadatas.PRODUCTION) {
				return MERCHANT_SERVICE_URL_LIVE;
			} else {
				return MERCHANT_SERVICE_URL_TEST;
			}
		}
		
	/*	public static String getMerchantMobileServiceurl() {
			if (UrlMetadatas.PRODUCTION) {
				return MERCHANT_MOBILE_SERVICE_URL_LIVE;
			} else {
				return MERCHANT_MOBILE_SERVICE_URL_TEST;
			}
		}*/

		public static String getTransactionPwd() {
			if (UrlMetadatas.PRODUCTION) {
				return TRANSACTION_PWD_LIVE;
			} else {
				return TRANSACTION_PWD_TEST;
			}
		}

		public static String getPayeeVirAddr() {
			if (UrlMetadatas.PRODUCTION) {
				return PAYEE_VIR_ADDR_LIVE;
			} else {
				return PAYEE_VIR_ADDR_TEST;
			}
		}

		public static String getPayeeMobileNum() {
			if (UrlMetadatas.PRODUCTION) {
				return PAYEE_MOBILE_NUM_LIVE;
			} else {
				return PAYEE_MOBILE_NUM_TEST;
			}
		}

		public static String getMerchantId() {
			if (UrlMetadatas.PRODUCTION) {
				return MERCHANT_ID_LIVE;
			} else {
				return MERCHANT_ID_TEST;
			}
		}

		public static String getVpaTerminalId() {
			if (UrlMetadatas.PRODUCTION) {
				return VPA_TERMINAL_ID_LIVE;
			} else {
				return TERMINAL_ID;
			}
		}

		public static String getPlanDek() {
			if (UrlMetadatas.PRODUCTION) {
				return PLAN_DEK_LIVE;
			} else {
				return PLAN_DEK_TEST;
			}
		}

		public static String getPublicKey() {
			if (UrlMetadatas.PRODUCTION) {
				return PUBLIK_KEY_LIVE;
			} else {
				return PUBLIK_KEY_TEST;
			}
		}

		public static String getOrgId() {
			if (UrlMetadatas.PRODUCTION) {
				return ORG_ID_LIVE;
			} else {
				return ORG_ID_TEST;
			}
		}
}
