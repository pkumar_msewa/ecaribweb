package com.upi.api;

import com.upi.model.requet.GenerateDEKRequest;
import com.upi.model.requet.MerchantCollectMoneyInfoRequest;
import com.upi.model.requet.MerchantMetaInfoRequest;
import com.upi.model.requet.MerchantMetaInfoVPARequest;
import com.upi.model.requet.MerchantStatusMetaInfoRequest;
import com.upi.model.requet.MerchantTokenGenRequest;
import com.upi.model.requet.MerchantTokenValidRequest;
import com.upi.model.response.GenerateMerCredentialResp;
import com.upi.model.response.GenerateMerchantDEKResponse;
import com.upi.model.response.MerchantMetaVPAResponse;
import com.upi.model.response.MerchantTokenGenResponse;
import com.upi.model.response.MerchantTokenValidResponse;
import com.upi.model.response.MerchantTxnStatusResponse;
import com.upi.model.response.MerchantTxnStatusResponse;

public interface IUPIApi {

	GenerateMerchantDEKResponse getMerchantDEK(GenerateDEKRequest request);

	GenerateMerCredentialResp genMerchantCredential();

	MerchantTokenGenResponse genMerchantToken(MerchantTokenGenRequest request);

	MerchantTokenValidResponse validateMerchantToken(MerchantTokenValidRequest request);
	
	MerchantTokenValidResponse validateMerchantMetaDataInfo(MerchantMetaInfoRequest request);
	
	MerchantMetaVPAResponse validateMerchantMetaVPA(MerchantMetaInfoVPARequest request);
	
	MerchantMetaVPAResponse validateCollectMoney(MerchantCollectMoneyInfoRequest request);
	
	MerchantTxnStatusResponse maerchantTxnStatus(MerchantStatusMetaInfoRequest request);
	
	// TODO APis for Bescom Merchant
	
	GenerateMerchantDEKResponse getMerchantDEKBescom(GenerateDEKRequest request);

	GenerateMerCredentialResp genMerchantCredentialBescom();

	MerchantTokenGenResponse genMerchantTokenBescom(MerchantTokenGenRequest request);

	MerchantTokenValidResponse validateMerchantTokenBescom(MerchantTokenValidRequest request);
	
	MerchantTokenValidResponse validateMerchantMetaDataInfoBescom(MerchantMetaInfoRequest request);
	
	MerchantMetaVPAResponse validateMerchantMetaVPABescom(MerchantMetaInfoVPARequest request);
	
	MerchantMetaVPAResponse validateCollectMoneyBescom(MerchantCollectMoneyInfoRequest request);
	
	MerchantTxnStatusResponse maerchantTxnStatusBescom(MerchantStatusMetaInfoRequest request);
}
