package com.upi.api.impl;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.payqwikweb.app.model.ResponseStatus;
import com.payqwikweb.util.CommonUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.upi.api.IUPIApi;
import com.upi.model.requet.GenerateDEKRequest;
import com.upi.model.requet.MerchantCollectMoneyInfoRequest;
import com.upi.model.requet.MerchantMetaInfoRequest;
import com.upi.model.requet.MerchantMetaInfoVPARequest;
import com.upi.model.requet.MerchantStatusMetaInfoRequest;
import com.upi.model.requet.MerchantTokenGenRequest;
import com.upi.model.requet.MerchantTokenValidRequest;
import com.upi.model.response.GenerateMerCredentialResp;
import com.upi.model.response.GenerateMerchantDEKResponse;
import com.upi.model.response.MerchantMetaVPAResponse;
import com.upi.model.response.MerchantTokenGenResponse;
import com.upi.model.response.MerchantTokenValidResponse;
import com.upi.model.response.MerchantTxnStatusResponse;
import com.upi.util.AESAlgorithm;
import com.upi.util.DesEncryption;
import com.upi.util.UPIBescomContstant;
import com.upi.util.UPIConstants;


public class UPIApiImpl implements IUPIApi{

	/* (non-Javadoc)
	 * @see com.upi.api.IUPIApi#getMerchantDEK(com.upi.model.requet.GenerateDEKRequest)
	 */
	@Override
	public GenerateMerchantDEKResponse getMerchantDEK(GenerateDEKRequest request) {
		GenerateMerchantDEKResponse result = new GenerateMerchantDEKResponse();
		try{
			Client restClient = UPIConstants.createClient();
			WebResource webResource = restClient.resource(UPIConstants.MERCHANT_SERVICE_URL);
			restClient.addFilter(new LoggingFilter(System.out));
			ClientResponse resp = webResource.type("application/xml").post(ClientResponse.class, request.getRequest());
			String output = resp.getEntity(String.class);
			if (resp.getStatus() == 200) {
				if(resp.getStatus() != 401){
					DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
					DocumentBuilder builder = factory.newDocumentBuilder();
					InputStream inputStream = new ByteArrayInputStream(output.getBytes());
					org.w3c.dom.Document doc = builder.parse(inputStream);
					System.err.println("inside true");
					NodeList nodes = doc.getElementsByTagName("upi:return");
					System.out.println("node :: " + nodes.getLength());
					String respCode = "";
					String encryptedDEK = "";
					String tokenResp = "";
					for (int i = 0; i < nodes.getLength(); i++) {
						Element element = (Element) nodes.item(i);
						respCode = element.getElementsByTagName("java:ResCode").item(0).getFirstChild().getTextContent();
						
						tokenResp = element.getElementsByTagName("java:ResDesc").item(0).getFirstChild().getTextContent();
						encryptedDEK = element.getElementsByTagName("java:DEKKey").item(0).getFirstChild().getTextContent();
						System.err.println("ErrDesc  :: "+encryptedDEK);
					}
					if(respCode.equals("000")){
						result.setSuccess(true);
						result.setEncryptedDEK(encryptedDEK);
						result.setMessage(tokenResp);
						result.setStatus(ResponseStatus.SUCCESS);
					}else{	
						result.setSuccess(false);
						result.setEncryptedDEK(encryptedDEK);
						result.setMessage("Merchant DEK Generation Failed");
						result.setStatus(ResponseStatus.FAILURE);
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			result.setSuccess(false);
			result.setMessage("Internal Server Error");
			result.setStatus(ResponseStatus.FAILURE);
		}
		return result;
	}

	@Override
	public GenerateMerCredentialResp genMerchantCredential() {
		GenerateMerCredentialResp result = new GenerateMerCredentialResp();
		try{
			/*AESAlgorithm aesAlgorithm = new AESAlgorithm();
			String decryptedDEK = aesAlgorithm.decryptDEK(UPIConstants.PLAN_DEK, UPIConstants.KEK);
			System.out.println("DecryptedDEK:: " + decryptedDEK);
			String merchantCredentials = aesAlgorithm.generateMerchantCredential(decryptedDEK, CommonUtil.merchantRefNo() + "#" + UPIConstants.TRANSACTION_PWD);
			result.setSuccess(true);
			result.setMerchantCredentials(merchantCredentials);
			result.setMessage("Merchant Credentials");
			result.setStatus(ResponseStatus.SUCCESS);*/
		}catch(Exception e){
			e.printStackTrace();
			result.setSuccess(false);
			result.setMessage("Internal Server Error");
			result.setStatus(ResponseStatus.FAILURE);
		}
		return result;
	}

	@Override
	public MerchantTokenGenResponse genMerchantToken(MerchantTokenGenRequest request) {
		MerchantTokenGenResponse result = new MerchantTokenGenResponse();
		try{
			Client restClient = UPIConstants.createClient();
			System.err.println("SDK ::: :::  " + request.isSdk());
			String URL = null;
			if(request.isSdk()){
				URL = UPIConstants.MERCHANT_MOBILE_SERVICE_URL;
				System.out.println("Calling app application");
			}else{
				 URL = UPIConstants.MERCHANT_SERVICE_URL;
				 System.out.println("Calling web application");
			}
			WebResource webResource = restClient.resource(URL);
			restClient.addFilter(new LoggingFilter(System.out));
			ClientResponse resp = webResource.type("application/xml").post(ClientResponse.class, request.getRequest());
			String output = resp.getEntity(String.class);
			if (resp.getStatus() == 200) {
				if(resp.getStatus() != 401){
					DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
					DocumentBuilder builder = factory.newDocumentBuilder();
					InputStream inputStream = new ByteArrayInputStream(output.getBytes());
					org.w3c.dom.Document doc = builder.parse(inputStream);
					NodeList nodes = doc.getElementsByTagName("upi:return");
					System.out.println("node :: " + nodes.getLength());
					String respCode = "";
					String respDesc = "";
					String token = "";
					boolean valid = false;
					for (int i = 0; i < nodes.getLength(); i++) {
						Element element = (Element) nodes.item(i);
						respCode = element.getElementsByTagName("java:ResCode").item(0).getFirstChild().getTextContent();
						respDesc = element.getElementsByTagName("java:ResDesc").item(0).getFirstChild().getTextContent();
						System.err.println("respCode :: " + respCode);
						System.err.println("ResDesc :: " + respDesc);
						if(respCode.equals("000")){
							valid = true;
							token = element.getElementsByTagName("java:Token").item(0).getFirstChild().getTextContent();
						}
					}
					if(valid){
						System.err.println("1");
						result.setMessage(respDesc);
						result.setToken(token);
						result.setStatus(ResponseStatus.SUCCESS);
					}else{
						System.err.println("2");
						result.setMessage(respDesc);
						result.setStatus(ResponseStatus.FAILURE);
					}
				}else{
					System.err.println("2");
					result.setMessage("Service Unavailable");
					result.setStatus(ResponseStatus.FAILURE);
				}
			}else{
				System.err.println("2");
				result.setMessage("Service Unavailable");
				result.setStatus(ResponseStatus.FAILURE);
			}
		}catch(Exception e){
			e.printStackTrace();
			result.setMessage("Internal Server Error");
			result.setStatus(ResponseStatus.FAILURE);
			return result;
		}
		return result;
	}

	@Override
	public MerchantTokenValidResponse validateMerchantToken(MerchantTokenValidRequest request) {
		MerchantTokenValidResponse result = new MerchantTokenValidResponse();
		try{
			Client restClient = UPIConstants.createClient();
			WebResource webResource = restClient.resource(UPIConstants.MERCHANT_SERVICE_URL);
			restClient.addFilter(new LoggingFilter(System.out));
			ClientResponse resp = webResource.type("application/xml").post(ClientResponse.class, request.getRequest());
			String output = resp.getEntity(String.class);
			if (resp.getStatus() == 200) {
				if(resp.getStatus() != 401){
					DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
					DocumentBuilder builder = factory.newDocumentBuilder();
					InputStream inputStream = new ByteArrayInputStream(output.getBytes());
					org.w3c.dom.Document doc = builder.parse(inputStream);
					NodeList nodes = doc.getElementsByTagName("upi:return");
					System.out.println("node :: " + nodes.getLength());
					String respCode = "";
					String msgId = "";
					String respDesc = "";
					for (int i = 0; i < nodes.getLength(); i++) {
						Element element = (Element) nodes.item(i);
						respCode = element.getElementsByTagName("java:ResCode").item(0).getFirstChild().getTextContent();
						msgId = element.getElementsByTagName("java:MsgId").item(0).getFirstChild().getTextContent();
						respDesc = element.getElementsByTagName("java:ResDesc").item(0).getFirstChild().getTextContent();
					}
					if(respCode.equals("000")){
						result.setMsgId(msgId);
						result.setMessage(respDesc);
						result.setDesEncryptedKey(DesEncryption.desEncrypt(request.getRequest(), msgId));
						result.setStatus(ResponseStatus.SUCCESS);
					}else{
						result.setMessage("Merchant Token Validation Failed");
						result.setStatus(ResponseStatus.FAILURE);
					}
				}else{
					System.err.println("2");
					result.setMessage("Service Unavailable");
					result.setStatus(ResponseStatus.FAILURE);
				}
			}else{
				System.err.println("2");
				result.setMessage("Service Unavailable");
				result.setStatus(ResponseStatus.FAILURE);
			}
		}catch(Exception e){
			e.printStackTrace();
			result.setMessage("Internal Server Error");
			result.setStatus(ResponseStatus.FAILURE);
		}
		return result;
	}

	@Override
	public MerchantTokenValidResponse validateMerchantMetaDataInfo(MerchantMetaInfoRequest request) {
		MerchantTokenValidResponse result = new MerchantTokenValidResponse();
		try{
			Client restClient = UPIConstants.createClient();
			String URL = null;
			System.err.println("SDK ::: :::  " + request.isSdk());
			if(request.isSdk()){
				URL = UPIConstants.MERCHANT_MOBILE_SERVICE_URL;
				System.out.println("Calling app application");
			}else{
				 URL = UPIConstants.MERCHANT_SERVICE_URL;
				 System.out.println("Calling web application");
			}
			WebResource webResource = restClient.resource(URL);
			restClient.addFilter(new LoggingFilter(System.out));
			ClientResponse resp = webResource.type("application/xml").post(ClientResponse.class, request.getRequest());
			String output = resp.getEntity(String.class);
			if (resp.getStatus() == 200) {
				if(resp.getStatus() != 401){
					DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
					DocumentBuilder builder = factory.newDocumentBuilder();
					InputStream inputStream = new ByteArrayInputStream(output.getBytes());
					org.w3c.dom.Document doc = builder.parse(inputStream);
					NodeList nodes = doc.getElementsByTagName("upi:return");
					System.out.println("node :: " + nodes.getLength());
					String respCode = "";
					String msgId = "";
					String respDesc = "";
					for (int i = 0; i < nodes.getLength(); i++) {
						Element element = (Element) nodes.item(i);
						respCode = element.getElementsByTagName("java:ResCode").item(0).getFirstChild().getTextContent();
//						msgId = element.getElementsByTagName("java:MsgId").item(0).getFirstChild().getTextContent();
						respDesc = element.getElementsByTagName("java:ResDesc").item(0).getFirstChild().getTextContent();
					}
					if(respCode.equals("000")){
//						result.setMsgId(msgId);
						result.setMessage(respDesc);
//						result.setDesEncryptedKey(DesEncryption.desEncrypt(request.getRequest(), msgId));
						result.setStatus(ResponseStatus.SUCCESS);
					}else{
						result.setMessage("Merchant Token Validation Failed");
						result.setStatus(ResponseStatus.FAILURE);
					}
				}else{
					System.err.println("2");
					result.setMessage("Service Unavailable");
					result.setStatus(ResponseStatus.FAILURE);
				}
			}else{
				System.err.println("2");
				result.setMessage("Service Unavailable");
				result.setStatus(ResponseStatus.FAILURE);
			}
		}catch(Exception e){
			e.printStackTrace();
			result.setMessage("Internal Server Error");
			result.setStatus(ResponseStatus.FAILURE);
		}
		return result;
	}

	@Override
	public MerchantMetaVPAResponse validateMerchantMetaVPA(MerchantMetaInfoVPARequest request) {
		MerchantMetaVPAResponse result = new MerchantMetaVPAResponse();
		try{
			Client restClient = UPIConstants.createClient();
			WebResource webResource = restClient.resource(UPIConstants.MERCHANT_SERVICE_URL);
			restClient.addFilter(new LoggingFilter(System.out));
			ClientResponse resp = webResource.type("application/xml").post(ClientResponse.class, request.getRequest());
			String output = resp.getEntity(String.class);
			if (resp.getStatus() == 200) {
				if(resp.getStatus() != 401){
					DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
					DocumentBuilder builder = factory.newDocumentBuilder();
					InputStream inputStream = new ByteArrayInputStream(output.getBytes());
					org.w3c.dom.Document doc = builder.parse(inputStream);
					NodeList nodes = doc.getElementsByTagName("upi:return");
					System.out.println("node :: " + nodes.getLength());
					String respCode = "";
					String msgId = "";
					String respDesc = "";
					for (int i = 0; i < nodes.getLength(); i++) {
						Element element = (Element) nodes.item(i);
						respCode = element.getElementsByTagName("java:ResCode").item(0).getFirstChild().getTextContent();
//						msgId = element.getElementsByTagName("java:MsgId").item(0).getFirstChild().getTextContent();
						respDesc = element.getElementsByTagName("java:ResDesc").item(0).getFirstChild().getTextContent();
					}
					if(respCode.equals("000")){
//						result.setMsgId(msgId);
						result.setMessage(respDesc);
//						result.setDesEncryptedKey(DesEncryption.desEncrypt(request.getRequest(), msgId));
						result.setStatus(ResponseStatus.SUCCESS);
					}else{
						result.setMessage("Merchant Token Validation Failed");
						result.setStatus(ResponseStatus.FAILURE);
					}
				}else{
					System.err.println("2");
					result.setMessage("Service Unavailable");
					result.setStatus(ResponseStatus.FAILURE);
				}
			}else{
				System.err.println("2");
				result.setMessage("Service Unavailable");
				result.setStatus(ResponseStatus.FAILURE);
			}
		}catch(Exception e){
			e.printStackTrace();
			result.setMessage("Internal Server Error");
			result.setStatus(ResponseStatus.FAILURE);
		}
		return result;
	}

	@Override
	public MerchantMetaVPAResponse validateCollectMoney(MerchantCollectMoneyInfoRequest request) {
		MerchantMetaVPAResponse result = new MerchantMetaVPAResponse();
		try{
			Client restClient = UPIConstants.createClient();
			WebResource webResource = restClient.resource(UPIConstants.MERCHANT_SERVICE_URL);
			restClient.addFilter(new LoggingFilter(System.out));
			ClientResponse resp = webResource.type("application/xml").post(ClientResponse.class, request.getRequest());
			String output = resp.getEntity(String.class);
			if (resp.getStatus() == 200) {
				if(resp.getStatus() != 401){
					DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
					DocumentBuilder builder = factory.newDocumentBuilder();
					InputStream inputStream = new ByteArrayInputStream(output.getBytes());
					org.w3c.dom.Document doc = builder.parse(inputStream);
					NodeList nodes = doc.getElementsByTagName("upi:return");
					System.out.println("node :: " + nodes.getLength());
					String respCode = "";
					String msgId = "";
					String respDesc = "";
					String refNo = "";
					for (int i = 0; i < nodes.getLength(); i++) {
						Element element = (Element) nodes.item(i);
						respCode = element.getElementsByTagName("java:ResCode").item(0).getFirstChild().getTextContent();
//						msgId = element.getElementsByTagName("java:MsgId").item(0).getFirstChild().getTextContent();
						respDesc = element.getElementsByTagName("java:ResDesc").item(0).getFirstChild().getTextContent();
						if(respCode.equals("000")){
							refNo = element.getElementsByTagName("java:TxnRefId").item(0).getFirstChild().getTextContent();
						}
					}
					if(respCode.equals("000")){
						result.setMsgId(request.getMsgId());
						result.setMessage(respDesc);
						result.setReferenceNo(refNo);
//						result.setDesEncryptedKey(DesEncryption.desEncrypt(request.getRequest(), msgId));
						result.setStatus(ResponseStatus.SUCCESS);
					}else{
						result.setMessage("Merchant Collect Money Failed");
						result.setStatus(ResponseStatus.FAILURE);
						result.setMsgId(request.getMsgId());
						result.setMessage(respDesc);
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			result.setMessage("Internal Server Error");
			result.setStatus(ResponseStatus.FAILURE);
		}
		return result;
	}

	@Override
	public MerchantTxnStatusResponse maerchantTxnStatus(MerchantStatusMetaInfoRequest request) {
		MerchantTxnStatusResponse result = new MerchantTxnStatusResponse();
		try{
			Client restClient = UPIConstants.createClient();
			WebResource webResource = restClient.resource(UPIConstants.MERCHANT_SERVICE_URL);
			restClient.addFilter(new LoggingFilter(System.out));
			ClientResponse resp = webResource.type("application/xml").post(ClientResponse.class, request.getRequest());
			String output = resp.getEntity(String.class);
			if (resp.getStatus() == 200) {
				if(resp.getStatus() != 401){
					DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
					DocumentBuilder builder = factory.newDocumentBuilder();
					InputStream inputStream = new ByteArrayInputStream(output.getBytes());
					org.w3c.dom.Document doc = builder.parse(inputStream);
					NodeList nodes = doc.getElementsByTagName("upi:return");
					System.out.println("node :: " + nodes.getLength());
					String respCode = "";
					String msgId = "";
					String respDesc = "";
					String refNo = "";
					String upiStatus = "";
					String txnId = "";
					String txnType = "";
					String txnAmount = "";
					String remarks = "";
					for (int i = 0; i < nodes.getLength(); i++) {
						Element element = (Element) nodes.item(i);
						respCode = element.getElementsByTagName("java:ResCode").item(0).getFirstChild().getTextContent();
						msgId = element.getElementsByTagName("java:MsgId").item(0).getFirstChild().getTextContent();
						respDesc = element.getElementsByTagName("java:ResDesc").item(0).getFirstChild().getTextContent();
						if(respCode.equals("000")){
							upiStatus = element.getElementsByTagName("java:Status").item(0).getFirstChild().getTextContent();
							txnId = element.getElementsByTagName("java:TxnId").item(0).getFirstChild().getTextContent();
							refNo = element.getElementsByTagName("java:TxnRefId").item(0).getFirstChild().getTextContent();
							txnType = element.getElementsByTagName("java:TxnType").item(0).getFirstChild().getTextContent();
							txnAmount = element.getElementsByTagName("java:TxnAmount").item(0).getFirstChild().getTextContent();
							remarks = element.getElementsByTagName("java:TxnRemarks").item(0).getFirstChild().getTextContent();
//							refNo = element.getElementsByTagName("java:TxnRefId").item(0).getFirstChild().getTextContent();
						}
					}
					if(respCode.equals("000")){
						result.setMessage(respDesc);
						result.setMsgId(msgId);
						result.setUpiStatus(upiStatus);
						result.setTxnId(txnId);
						result.setTxnType(txnType);
						result.setTxnAmount(txnAmount);
						result.setRemarks(remarks);
						result.setReferenceNo(refNo);
						result.setStatus(ResponseStatus.SUCCESS);
					}else{
						result.setMessage("Merchant Collect Money Failed");
						result.setStatus(ResponseStatus.FAILURE);
						result.setMessage(respDesc);
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			result.setMessage("Internal Server Error");
			result.setStatus(ResponseStatus.FAILURE);
		}
		return result;
	}
	
	// TODO UPI Implementaion for BESCOM
	
	@Override
	public GenerateMerchantDEKResponse getMerchantDEKBescom(GenerateDEKRequest request) {
		GenerateMerchantDEKResponse result = new GenerateMerchantDEKResponse();
		try{
			Client restClient = UPIBescomContstant.createClient();
			WebResource webResource = restClient.resource(UPIBescomContstant.MERCHANT_SERVICE_URL);
			restClient.addFilter(new LoggingFilter(System.out));
			ClientResponse resp = webResource.type("application/xml").post(ClientResponse.class, request.getBescomUpiRequest());
			String output = resp.getEntity(String.class);
			if (resp.getStatus() == 200) {
				if(resp.getStatus() != 401){
					DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
					DocumentBuilder builder = factory.newDocumentBuilder();
					InputStream inputStream = new ByteArrayInputStream(output.getBytes());
					org.w3c.dom.Document doc = builder.parse(inputStream);
					System.err.println("inside true");
					NodeList nodes = doc.getElementsByTagName("upi:return");
					System.out.println("node :: " + nodes.getLength());
					String respCode = "";
					String encryptedDEK = "";
					String tokenResp = "";
					for (int i = 0; i < nodes.getLength(); i++) {
						Element element = (Element) nodes.item(i);
						respCode = element.getElementsByTagName("java:ResCode").item(0).getFirstChild().getTextContent();
						
						tokenResp = element.getElementsByTagName("java:ResDesc").item(0).getFirstChild().getTextContent();
						encryptedDEK = element.getElementsByTagName("java:DEKKey").item(0).getFirstChild().getTextContent();
						System.err.println("ErrDesc  :: "+encryptedDEK);
					}
					if(respCode.equals("000")){
						result.setSuccess(true);
						result.setEncryptedDEK(encryptedDEK);
						result.setMessage(tokenResp);
						result.setStatus(ResponseStatus.SUCCESS);
					}else{	
						result.setSuccess(false);
						result.setEncryptedDEK(encryptedDEK);
						result.setMessage("Merchant DEK Generation Failed");
						result.setStatus(ResponseStatus.FAILURE);
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			result.setSuccess(false);
			result.setMessage("Internal Server Error");
			result.setStatus(ResponseStatus.FAILURE);
		}
		return result;
	}

	@Override
	public GenerateMerCredentialResp genMerchantCredentialBescom() {
		GenerateMerCredentialResp result = new GenerateMerCredentialResp();
		try{
			AESAlgorithm aesAlgorithm = new AESAlgorithm();
//			String decryptedDEK = aesAlgorithm.decryptDEK(UPIBescomContstant.PLAN_DEK, UPIBescomContstant.KEK);
//			System.out.println("DecryptedDEK:: " + decryptedDEK);
//			String merchantCredentials = aesAlgorithm.generateMerchantCredential(decryptedDEK, CommonUtil.merchantRefNo() + "#" + UPIBescomContstant.TRANSACTION_PWD);
//			result.setSuccess(true);
//			result.setMerchantCredentials(merchantCredentials);
			result.setMessage("Merchant Credentials");
			result.setStatus(ResponseStatus.SUCCESS);
		}catch(Exception e){
			e.printStackTrace();
			result.setSuccess(false);
			result.setMessage("Internal Server Error");
			result.setStatus(ResponseStatus.FAILURE);
		}
		return result;
	}

	@Override
	public MerchantTokenGenResponse genMerchantTokenBescom(MerchantTokenGenRequest request) {
		MerchantTokenGenResponse result = new MerchantTokenGenResponse();
		try{
			Client restClient = UPIBescomContstant.createClient();
			WebResource webResource = restClient.resource(UPIBescomContstant.MERCHANT_SERVICE_URL);
			restClient.addFilter(new LoggingFilter(System.out));
			ClientResponse resp = webResource.type("application/xml").post(ClientResponse.class, request.getBescomUpiRequest());
			String output = resp.getEntity(String.class);
			if (resp.getStatus() == 200) {
				if(resp.getStatus() != 401){
					DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
					DocumentBuilder builder = factory.newDocumentBuilder();
					InputStream inputStream = new ByteArrayInputStream(output.getBytes());
					org.w3c.dom.Document doc = builder.parse(inputStream);
					NodeList nodes = doc.getElementsByTagName("upi:return");
					System.out.println("node :: " + nodes.getLength());
					String respCode = "";
					String respDesc = "";
					String token = "";
					boolean valid = false;
					for (int i = 0; i < nodes.getLength(); i++) {
						Element element = (Element) nodes.item(i);
						respCode = element.getElementsByTagName("java:ResCode").item(0).getFirstChild().getTextContent();
						respDesc = element.getElementsByTagName("java:ResDesc").item(0).getFirstChild().getTextContent();
						System.err.println("respCode :: " + respCode);
						System.err.println("ResDesc :: " + respDesc);
						if(respCode.equals("000")){
							valid = true;
							token = element.getElementsByTagName("java:Token").item(0).getFirstChild().getTextContent();
						}
					}
					if(valid){
						System.err.println("1");
						result.setMessage(respDesc);
						result.setToken(token);
						result.setStatus(ResponseStatus.SUCCESS);
					}else{
						System.err.println("2");
						result.setMessage(respDesc);
						result.setStatus(ResponseStatus.FAILURE);
					}
				}else{
					System.err.println("2");
					result.setMessage("Service Unavailable");
					result.setStatus(ResponseStatus.FAILURE);
				}
			}else{
				System.err.println("2");
				result.setMessage("Service Unavailable");
				result.setStatus(ResponseStatus.FAILURE);
			}
		}catch(Exception e){
			e.printStackTrace();
			result.setMessage("Internal Server Error");
			result.setStatus(ResponseStatus.FAILURE);
			return result;
		}
		return result;
	}

	@Override
	public MerchantTokenValidResponse validateMerchantTokenBescom(MerchantTokenValidRequest request) {
		MerchantTokenValidResponse result = new MerchantTokenValidResponse();
		try{
			Client restClient = UPIBescomContstant.createClient();
			WebResource webResource = restClient.resource(UPIBescomContstant.MERCHANT_SERVICE_URL);
			restClient.addFilter(new LoggingFilter(System.out));
			ClientResponse resp = webResource.type("application/xml").post(ClientResponse.class, request.getBescomUpiRequest());
			String output = resp.getEntity(String.class);
			if (resp.getStatus() == 200) {
				if(resp.getStatus() != 401){
					DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
					DocumentBuilder builder = factory.newDocumentBuilder();
					InputStream inputStream = new ByteArrayInputStream(output.getBytes());
					org.w3c.dom.Document doc = builder.parse(inputStream);
					NodeList nodes = doc.getElementsByTagName("upi:return");
					System.out.println("node :: " + nodes.getLength());
					String respCode = "";
					String msgId = "";
					String respDesc = "";
					for (int i = 0; i < nodes.getLength(); i++) {
						Element element = (Element) nodes.item(i);
						respCode = element.getElementsByTagName("java:ResCode").item(0).getFirstChild().getTextContent();
						msgId = element.getElementsByTagName("java:MsgId").item(0).getFirstChild().getTextContent();
						respDesc = element.getElementsByTagName("java:ResDesc").item(0).getFirstChild().getTextContent();
					}
					if(respCode.equals("000")){
						result.setMsgId(msgId);
						result.setMessage(respDesc);
						result.setDesEncryptedKey(DesEncryption.desEncrypt(request.getRequest(), msgId));
						result.setStatus(ResponseStatus.SUCCESS);
					}else{
						result.setMessage("Merchant Token Validation Failed");
						result.setStatus(ResponseStatus.FAILURE);
					}
				}else{
					System.err.println("2");
					result.setMessage("Service Unavailable");
					result.setStatus(ResponseStatus.FAILURE);
				}
			}else{
				System.err.println("2");
				result.setMessage("Service Unavailable");
				result.setStatus(ResponseStatus.FAILURE);
			}
		}catch(Exception e){
			e.printStackTrace();
			result.setMessage("Internal Server Error");
			result.setStatus(ResponseStatus.FAILURE);
		}
		return result;
	}

	@Override
	public MerchantTokenValidResponse validateMerchantMetaDataInfoBescom(MerchantMetaInfoRequest request) {
		MerchantTokenValidResponse result = new MerchantTokenValidResponse();
		try{
			Client restClient = UPIBescomContstant.createClient();
			WebResource webResource = restClient.resource(UPIBescomContstant.MERCHANT_SERVICE_URL);
			restClient.addFilter(new LoggingFilter(System.out));
			ClientResponse resp = webResource.type("application/xml").post(ClientResponse.class, request.getBescomUpiRequest());
			String output = resp.getEntity(String.class);
			if (resp.getStatus() == 200) {
				if(resp.getStatus() != 401){
					DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
					DocumentBuilder builder = factory.newDocumentBuilder();
					InputStream inputStream = new ByteArrayInputStream(output.getBytes());
					org.w3c.dom.Document doc = builder.parse(inputStream);
					NodeList nodes = doc.getElementsByTagName("upi:return");
					System.out.println("node :: " + nodes.getLength());
					String respCode = "";
					String msgId = "";
					String respDesc = "";
					for (int i = 0; i < nodes.getLength(); i++) {
						Element element = (Element) nodes.item(i);
						respCode = element.getElementsByTagName("java:ResCode").item(0).getFirstChild().getTextContent();
//						msgId = element.getElementsByTagName("java:MsgId").item(0).getFirstChild().getTextContent();
						respDesc = element.getElementsByTagName("java:ResDesc").item(0).getFirstChild().getTextContent();
					}
					if(respCode.equals("000")){
//						result.setMsgId(msgId);
						result.setMessage(respDesc);
//						result.setDesEncryptedKey(DesEncryption.desEncrypt(request.getRequest(), msgId));
						result.setStatus(ResponseStatus.SUCCESS);
					}else{
						result.setMessage("Merchant Token Validation Failed");
						result.setStatus(ResponseStatus.FAILURE);
					}
				}else{
					System.err.println("2");
					result.setMessage("Service Unavailable");
					result.setStatus(ResponseStatus.FAILURE);
				}
			}else{
				System.err.println("2");
				result.setMessage("Service Unavailable");
				result.setStatus(ResponseStatus.FAILURE);
			}
		}catch(Exception e){
			e.printStackTrace();
			result.setMessage("Internal Server Error");
			result.setStatus(ResponseStatus.FAILURE);
		}
		return result;
	}

	@Override
	public MerchantMetaVPAResponse validateMerchantMetaVPABescom(MerchantMetaInfoVPARequest request) {
		MerchantMetaVPAResponse result = new MerchantMetaVPAResponse();
		try{
			Client restClient = UPIBescomContstant.createClient();
			WebResource webResource = restClient.resource(UPIBescomContstant.MERCHANT_SERVICE_URL);
			restClient.addFilter(new LoggingFilter(System.out));
			ClientResponse resp = webResource.type("application/xml").post(ClientResponse.class, request.getBescomUpiRequest());
			String output = resp.getEntity(String.class);
			if (resp.getStatus() == 200) {
				if(resp.getStatus() != 401){
					DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
					DocumentBuilder builder = factory.newDocumentBuilder();
					InputStream inputStream = new ByteArrayInputStream(output.getBytes());
					org.w3c.dom.Document doc = builder.parse(inputStream);
					NodeList nodes = doc.getElementsByTagName("upi:return");
					System.out.println("node :: " + nodes.getLength());
					String respCode = "";
					String msgId = "";
					String respDesc = "";
					for (int i = 0; i < nodes.getLength(); i++) {
						Element element = (Element) nodes.item(i);
						respCode = element.getElementsByTagName("java:ResCode").item(0).getFirstChild().getTextContent();
//						msgId = element.getElementsByTagName("java:MsgId").item(0).getFirstChild().getTextContent();
						respDesc = element.getElementsByTagName("java:ResDesc").item(0).getFirstChild().getTextContent();
					}
					if(respCode.equals("000")){
//						result.setMsgId(msgId);
						result.setMessage(respDesc);
//						result.setDesEncryptedKey(DesEncryption.desEncrypt(request.getRequest(), msgId));
						result.setStatus(ResponseStatus.SUCCESS);
					}else{
						result.setMessage("Merchant Token Validation Failed");
						result.setStatus(ResponseStatus.FAILURE);
					}
				}else{
					System.err.println("2");
					result.setMessage("Service Unavailable");
					result.setStatus(ResponseStatus.FAILURE);
				}
			}else{
				System.err.println("2");
				result.setMessage("Service Unavailable");
				result.setStatus(ResponseStatus.FAILURE);
			}
		}catch(Exception e){
			e.printStackTrace();
			result.setMessage("Internal Server Error");
			result.setStatus(ResponseStatus.FAILURE);
		}
		return result;
	}

	@Override
	public MerchantMetaVPAResponse validateCollectMoneyBescom(MerchantCollectMoneyInfoRequest request) {
		MerchantMetaVPAResponse result = new MerchantMetaVPAResponse();
		try{
			Client restClient = UPIBescomContstant.createClient();
			WebResource webResource = restClient.resource(UPIBescomContstant.MERCHANT_SERVICE_URL);
			restClient.addFilter(new LoggingFilter(System.out));
			ClientResponse resp = webResource.type("application/xml").post(ClientResponse.class, request.getBescomUpiRequest());
			String output = resp.getEntity(String.class);
			if (resp.getStatus() == 200) {
				if(resp.getStatus() != 401){
					DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
					DocumentBuilder builder = factory.newDocumentBuilder();
					InputStream inputStream = new ByteArrayInputStream(output.getBytes());
					org.w3c.dom.Document doc = builder.parse(inputStream);
					NodeList nodes = doc.getElementsByTagName("upi:return");
					System.out.println("node :: " + nodes.getLength());
					String respCode = "";
					String msgId = "";
					String respDesc = "";
					String refNo = "";
					for (int i = 0; i < nodes.getLength(); i++) {
						Element element = (Element) nodes.item(i);
						respCode = element.getElementsByTagName("java:ResCode").item(0).getFirstChild().getTextContent();
//						msgId = element.getElementsByTagName("java:MsgId").item(0).getFirstChild().getTextContent();
						respDesc = element.getElementsByTagName("java:ResDesc").item(0).getFirstChild().getTextContent();
						if(respCode.equals("000")){
							refNo = element.getElementsByTagName("java:TxnRefId").item(0).getFirstChild().getTextContent();
						}
					}
					if(respCode.equals("000")){
						result.setMsgId(request.getMsgId());
						result.setMessage(respDesc);
						result.setReferenceNo(refNo);
//						result.setDesEncryptedKey(DesEncryption.desEncrypt(request.getRequest(), msgId));
						result.setStatus(ResponseStatus.SUCCESS);
					}else{
						result.setMessage("Merchant Collect Money Failed");
						result.setStatus(ResponseStatus.FAILURE);
						result.setMsgId(request.getMsgId());
						result.setMessage(respDesc);
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			result.setMessage("Internal Server Error");
			result.setStatus(ResponseStatus.FAILURE);
		}
		return result;
	}

	@Override
	public MerchantTxnStatusResponse maerchantTxnStatusBescom(MerchantStatusMetaInfoRequest request) {
		MerchantTxnStatusResponse result = new MerchantTxnStatusResponse();
		try{
			Client restClient = UPIBescomContstant.createClient();
			WebResource webResource = restClient.resource(UPIBescomContstant.MERCHANT_SERVICE_URL);
			restClient.addFilter(new LoggingFilter(System.out));
			ClientResponse resp = webResource.type("application/xml").post(ClientResponse.class, request.getBescomUpiRequest());
			String output = resp.getEntity(String.class);
			if (resp.getStatus() == 200) {
				if(resp.getStatus() != 401){
					DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
					DocumentBuilder builder = factory.newDocumentBuilder();
					InputStream inputStream = new ByteArrayInputStream(output.getBytes());
					org.w3c.dom.Document doc = builder.parse(inputStream);
					NodeList nodes = doc.getElementsByTagName("upi:return");
					System.out.println("node :: " + nodes.getLength());
					String respCode = "";
					String status = "";
					String respDesc = "";
					String refNo = "";
					for (int i = 0; i < nodes.getLength(); i++) {
						Element element = (Element) nodes.item(i);
						respCode = element.getElementsByTagName("java:ResCode").item(0).getFirstChild().getTextContent();
//						msgId = element.getElementsByTagName("java:MsgId").item(0).getFirstChild().getTextContent();
						respDesc = element.getElementsByTagName("java:ResDesc").item(0).getFirstChild().getTextContent();
						if(respCode.equals("000")){
							refNo = element.getElementsByTagName("java:TxnRefId").item(0).getFirstChild().getTextContent();
							status = element.getElementsByTagName("java:Status").item(0).getFirstChild().getTextContent();
						}
					}
					if(respCode.equals("000")){
						result.setMessage(respDesc);
						result.setReferenceNo(refNo);
						result.setUpiStatus(status);
//						result.setDesEncryptedKey(DesEncryption.desEncrypt(request.getRequest(), msgId));
						result.setStatus(ResponseStatus.SUCCESS);
					}else{
						result.setMessage("Merchant Collect Money Failed");
						result.setStatus(ResponseStatus.FAILURE);
						result.setMessage(respDesc);
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			result.setMessage("Internal Server Error");
			result.setStatus(ResponseStatus.FAILURE);
		}
		return result;
	}

}
