package com.upi.model.requet;

import javax.xml.soap.SOAPMessage;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.upi.util.DesEncryption;
import com.upi.util.UPIConstants;
import com.upi.util.UPIRSAEncryption;

public class MerchantMetaInfoVPARequest implements SOAPRequest {

	private String metaData;
	private String metaInfo;

	public String getMetaData() {
		return metaData;
	}

	public void setMetaData(String metaData) {
		this.metaData = metaData;
	}

	public String getMetaInfo() {
		return metaInfo;
	}

	public void setMetaInfo(String metaInfo) {
		this.metaInfo = metaInfo;
	}

	@Override
	public SOAPMessage buildRequest() {
		return null;
	}

	@Override
	public String getRequest() {
		String MerchantTokenValid = "<v:Envelope "
				 + " xmlns:i=\"http://www.w3.org/2001/XMLSchemainstance" +'"'
				 + " xmlns:d=\"http://www.w3.org/2001/XMLSchema" +'"'
				 + " xmlns:c=\"http://schemas.xmlsoap.org/soap/encoding/" +'"'
				 + " xmlns:v=\"http://schemas.xmlsoap.org/soap/envelope/\"> "
				 + "<v:Header/> "  
				 + "<v:Body> "  
					+"<MerchantValidateVPA c:root=\"1\" id=\"o0" +'"'+ " xmlns:n0=\"http://com/fss/upi\"> "
						+ "<req> "
							+ " <MetaData i:type=\"d:string\">"+getMetaData()+"</MetaData> "
							+ " <MetaInfo i:type=\"d:string\">"+getMetaInfo()+"</MetaInfo> "
						+ "</req> "
					+ "</MerchantValidateVPA>" 
				+ "</v:Body>"  
				+ "</v:Envelope>";	
		return MerchantTokenValid;
	}
	
	public static void main(String[] args) throws Exception {
		
		MerchantTokenValidRequest validMerchant = new MerchantTokenValidRequest();	
		validMerchant.setRefId("123456712364");
		validMerchant.setMsgId("VJBF99421A6C49D19B9754494EEF619M");
		validMerchant.setTimeStamp("140116155016");
		validMerchant.setMobileNo("7795221595");
		validMerchant.setMerchantCredentials("rkVbt8dY4a/IyfvClR85FeNxtv93Q4hu8hR/wbj1w4U=");
		validMerchant.setToken("6PLs8ZE60TFZjA/mTt+6mmtVCp3bke+g0nP2HIw96Es+wzJsu3l2RMyvAEi4jXD3");
		System.out.println("Request: " + validMerchant.getRequest());
		
		MerchantMetaInfoRequest dto = new MerchantMetaInfoRequest();	
		dto.setMetaData(DesEncryption.desEncrypt(validMerchant.getRequest(), validMerchant.getMsgId()));
		dto.setMetaInfo(UPIRSAEncryption.encryptData(validMerchant.getMsgId(), UPIConstants.PUBLIK_KEY));
		System.out.println("Request: " + dto.getRequest());
		Client restClient = UPIConstants.createClient();
		WebResource webResource = restClient.resource(UPIConstants.MERCHANT_SERVICE_URL);
		restClient.addFilter(new LoggingFilter(System.out));
		ClientResponse resp = webResource.type("application/xml").post(ClientResponse.class, dto.getRequest());
		String output = resp.getEntity(String.class);
		System.out.println("response: " + output);
	}

	@Override
	public String getBescomUpiRequest() {
		String MerchantTokenValid = "<v:Envelope "
				 + " xmlns:i=\"http://www.w3.org/2001/XMLSchemainstance" +'"'
				 + " xmlns:d=\"http://www.w3.org/2001/XMLSchema" +'"'
				 + " xmlns:c=\"http://schemas.xmlsoap.org/soap/encoding/" +'"'
				 + " xmlns:v=\"http://schemas.xmlsoap.org/soap/envelope/\"> "
				 + "<v:Header/> "  
				 + "<v:Body> "  
					+"<MerchantValidateVPA c:root=\"1\" id=\"o0" +'"'+ " xmlns:n0=\"http://com/fss/upi\"> "
						+ "<req> "
							+ " <MetaData i:type=\"d:string\">"+getMetaData()+"</MetaData> "
							+ " <MetaInfo i:type=\"d:string\">"+getMetaInfo()+"</MetaInfo> "
						+ "</req> "
					+ "</MerchantValidateVPA>" 
				+ "</v:Body>"  
				+ "</v:Envelope>";	
		return MerchantTokenValid;
	}

}
