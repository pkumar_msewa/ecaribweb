package com.upi.model.requet;

//import javax.xml.soap.SOAPMessage;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.upi.util.DesEncryption;
import com.upi.util.UPIBescomContstant;
import com.upi.util.UPIConstants;
import com.upi.util.UPIRSAEncryption;

public class VPARequest implements SOAPRequest {

	private String timeStamp;
	private String msgId;
	private String mobileNo;
	private String refId;
	private String merchantCredentials;
	private String payerVirAddr;
	private String payeeVirAddr;
	private String orgId;
	private String terminalId;
	private String merchantId;

	public String getPayeeVirAddr() {
		return payeeVirAddr;
	}

	public void setPayeeVirAddr(String payeeVirAddr) {
		this.payeeVirAddr = payeeVirAddr;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getMsgId() {
		return msgId;
	}

	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getRefId() {
		return refId;
	}

	public void setRefId(String refId) {
		this.refId = refId;
	}

	public String getMerchantCredentials() {
		return merchantCredentials;
	}

	public void setMerchantCredentials(String merchantCredentials) {
		this.merchantCredentials = merchantCredentials;
	}

	public String getPayerVirAddr() {
		return payerVirAddr;
	}

	public void setPayerVirAddr(String payerVirAddr) {
		this.payerVirAddr = payerVirAddr;
	}

	/*@Override
	public SOAPMessage buildRequest() {
		// TODO Auto-generated method stub
		return null;
	}*/

	@Override
	public String getRequest() {
		
		String MerchantValidateVPA = "<soapenv:Envelope"
				+" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/"+'"'
				+" xmlns:upi=\"http://com/fss/upi" +'"'
				+" xmlns:java=\"java:com.fss.upi.req\">"
				+" <soapenv:Header/>"
				+" <soapenv:Body>"
				+" <upi:MerchantValidateVPA xmlns:upi=\"http://com/fss/upi" +'"'+ " xmlns:java=\"java:com.fss.upi.req\"> "
					+"<upi:req>"
						+"<java:UPI>"
							+"<java:Channel>"+UPIConstants.VPA_CHENNEL+"</java:Channel>"
							+"<java:MobileNo>"+getMobileNo()+"</java:MobileNo>"
							+"<java:MsgId>"+getMsgId()+"</java:MsgId>"
							+"<java:OrgId>"+getOrgId()+"</java:OrgId>"
							+"<java:BankId>"+UPIConstants.BANK_ID+"</java:BankId>"
							+"<java:Remarks>?</java:Remarks>"
							+"<java:TimeStamp>"+getTimeStamp()+"</java:TimeStamp>"
							+"<java:DeviceID>"+UPIConstants.VPA_DEVICE_ID+"</java:DeviceID>"
							+"<java:PayerType>"+UPIConstants.PAYER_TYPE+"</java:PayerType>"
							+"<java:SubMerchantID></java:SubMerchantID>"
		                    +"<java:MerchantID>"+getMerchantId()+"</java:MerchantID>"
		                    +"<java:TerminalID>"+getTerminalId()+"</java:TerminalID>"
		                    +"<java:MerchantCredentials>"+getMerchantCredentials()+"</java:MerchantCredentials>"
		                    +"<java:AppVersion>"+UPIConstants.APP_VERSION+"</java:AppVersion>"
		                    +"<java:RefId>"+getRefId()+"</java:RefId>"
		                    +"<java:SdkVersion>"+UPIConstants.SDK_VERSION+"</java:SdkVersion>"
		                    +"<java:SdkName>"+UPIConstants.SKD_NAME+"</java:SdkName>"
		                    +"<java:PayeeCode>"+UPIConstants.PAYEE_CODE+"</java:PayeeCode>"
		                    +"<java:CurrencyType>"+UPIConstants.CURRENCY_TYPE+"</java:CurrencyType>"
		                    +"<java:PayerCode>"+UPIConstants.PAYER_CODE+"</java:PayerCode>"
		                    +"<java:PayeeType>"+UPIConstants.PAYEE_TYPE+"</java:PayeeType>"
		                +"</java:UPI>"
		                +"<java:PayerVirAddr>"+getPayeeVirAddr()+"</java:PayerVirAddr>"
		                +"<java:PayerAccNo></java:PayerAccNo>"
		                +"<java:PayerCode>"+UPIConstants.PAYER_CODE+"</java:PayerCode>"
		                +"<java:AddrType>"+UPIConstants.VIR_ADDR+"</java:AddrType>"
		                +"<java:PayeeVirAddr>"+getPayerVirAddr()+"</java:PayeeVirAddr>"
		            +"</upi:req>"	
				+" </upi:MerchantValidateVPA>"
				+" </soapenv:Body>"
				+" </soapenv:Envelope>";
		return MerchantValidateVPA;
	}

	/*public static void main(String[] args) throws Exception {
		VPARequest dto = new VPARequest();
		dto.setRefId("123456700369");
		dto.setMsgId("VJBF99421A6C49D19B9754494EEF009P");
		dto.setTimeStamp("140116155016");
		dto.setMobileNo("8050581012");
		dto.setMerchantCredentials("SKDsjVHudJQ5LxOeKaGuGDXEcHc9KQKqh/BkWwsv7ew=");
		dto.setPayerVirAddr("testvpa@vijb");
		 System.out.println("Request: " + dto.getRequest());
//		 Client restClient = UPIConstants.createClient();
//		 WebResource webResource = restClient.resource(UPIConstants.MERCHANT_SERVICE_URL);
//			restClient.addFilter(new LoggingFilter(System.out));
//			ClientResponse resp = webResource.type("application/xml").post(ClientResponse.class, dto.getRequest());
//			String output = resp.getEntity(String.class);
//			System.out.println("response: " + output);
		 
		 MerchantMetaInfoVPARequest merchantMeta = new MerchantMetaInfoVPARequest();	
			merchantMeta.setMetaData(DesEncryption.desEncrypt(dto.getRequest(), dto.getMsgId()));
			merchantMeta.setMetaInfo(UPIRSAEncryption.encryptData(dto.getMsgId(), UPIBescomContstant.PUBLIK_KEY));
			System.out.println("Request: " + merchantMeta.getRequest());
			Client restClient = UPIBescomContstant.createClient();
			WebResource webResource = restClient.resource(UPIBescomContstant.MERCHANT_SERVICE_URL);
			restClient.addFilter(new LoggingFilter(System.out));
			ClientResponse resp = webResource.type("application/xml").post(ClientResponse.class, merchantMeta.getRequest());
			String output = resp.getEntity(String.class);
			System.out.println("response: " + output);
	}*/

	@Override
	public String getBescomUpiRequest() {
		
		String MerchantValidateVPA = "<soapenv:Envelope"
				+" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/"+'"'
				+" xmlns:upi=\"http://com/fss/upi" +'"'
				+" xmlns:java=\"java:com.fss.upi.req\">"
				+" <soapenv:Header/>"
				+" <soapenv:Body>"
				+" <upi:MerchantValidateVPA xmlns:upi=\"http://com/fss/upi" +'"'+ " xmlns:java=\"java:com.fss.upi.req\"> "
					+"<upi:req>"
						+"<java:UPI>"
							+"<java:Channel>"+UPIBescomContstant.VPA_CHENNEL+"</java:Channel>"
							+"<java:MobileNo>"+getMobileNo()+"</java:MobileNo>"
							+"<java:MsgId>"+getMsgId()+"</java:MsgId>"
							+"<java:OrgId>"+getOrgId()+"</java:OrgId>"
							+"<java:BankId>"+UPIBescomContstant.BANK_ID+"</java:BankId>"
							+"<java:Remarks>?</java:Remarks>"
							+"<java:TimeStamp>"+getTimeStamp()+"</java:TimeStamp>"
							+"<java:DeviceID>"+UPIBescomContstant.VPA_DEVICE_ID+"</java:DeviceID>"
							+"<java:PayerType>"+UPIBescomContstant.PAYEE_TYPE+"</java:PayerType>"
							+"<java:SubMerchantID></java:SubMerchantID>"
		                    +"<java:MerchantID>"+getMerchantId()+"</java:MerchantID>"
		                    +"<java:TerminalID>"+getTerminalId()+"</java:TerminalID>"
		                    +"<java:MerchantCredentials>"+getMerchantCredentials()+"</java:MerchantCredentials>"
		                    +"<java:AppVersion>"+UPIBescomContstant.APP_VERSION+"</java:AppVersion>"
		                    +"<java:RefId>"+getRefId()+"</java:RefId>"
		                    +"<java:SdkVersion>"+UPIBescomContstant.SDK_VERSION+"</java:SdkVersion>"
		                    +"<java:SdkName>"+UPIBescomContstant.SKD_NAME+"</java:SdkName>"
		                    +"<java:PayeeCode>"+UPIBescomContstant.PAYEE_CODE+"</java:PayeeCode>"
		                    +"<java:CurrencyType>"+UPIBescomContstant.CURRENCY_TYPE+"</java:CurrencyType>"
		                    +"<java:PayerCode>"+UPIBescomContstant.PAYER_CODE+"</java:PayerCode>"
		                    +"<java:PayeeType>"+UPIBescomContstant.PAYEE_TYPE+"</java:PayeeType>"
		                +"</java:UPI>"
		                +"<java:PayerVirAddr>"+getPayeeVirAddr()+"</java:PayerVirAddr>"
		                +"<java:PayerAccNo></java:PayerAccNo>"
		                +"<java:PayerCode>"+UPIBescomContstant.PAYER_CODE+"</java:PayerCode>"
		                +"<java:AddrType>"+UPIBescomContstant.VIR_ADDR+"</java:AddrType>"
		                +"<java:PayeeVirAddr>"+getPayerVirAddr()+"</java:PayeeVirAddr>"
		            +"</upi:req>"	
				+" </upi:MerchantValidateVPA>"
				+" </soapenv:Body>"
				+" </soapenv:Envelope>";
		return MerchantValidateVPA;
	}
}
