package com.upi.model.requet;

import javax.xml.soap.SOAPMessage;

import com.payqwikweb.util.ConvertUtil;
import com.upi.util.UPIBescomContstant;
import com.upi.util.UPIConstants;

public class MerchantCollectMoneyRequest implements SOAPRequest{
	
	private String sessionId;
	private String timeStamp;
	private String msgId;
	private String mobileNo;
	private String refId;
	private String merchantCredentials;
	private String payerVirAddr;
	private double amount;
	private String expTime;
	private boolean sdk;
	private String deviceId;
	private String txnPwd;
	private String payeeVirAddr;
	private String payeeMobile;
	private String kek;
	private String merchantId;
	private String vpaTerminalId;
	private String planDek;
	private String publicKey;
	private String orgId;
	private String subMerchantId;
	private String terminalId;
	
	public boolean isSdk() {
		return sdk;
	}

	public void setSdk(boolean sdk) {
		this.sdk = sdk;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getExpTime() {
		return expTime;
	}

	public void setExpTime(String expTime) {
		this.expTime = expTime;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getMsgId() {
		return msgId;
	}

	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getRefId() {
		return refId;
	}

	public void setRefId(String refId) {
		this.refId = refId;
	}

	public String getMerchantCredentials() {
		return merchantCredentials;
	}

	public void setMerchantCredentials(String merchantCredentials) {
		this.merchantCredentials = merchantCredentials;
	}

	public String getPayerVirAddr() {
		return payerVirAddr;
	}

	public void setPayerVirAddr(String payerVirAddr) {
		this.payerVirAddr = payerVirAddr;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getTxnPwd() {
		return txnPwd;
	}

	public void setTxnPwd(String txnPwd) {
		this.txnPwd = txnPwd;
	}

	public String getPayeeVirAddr() {
		return payeeVirAddr;
	}

	public void setPayeeVirAddr(String payeeVirAddr) {
		this.payeeVirAddr = payeeVirAddr;
	}

	public String getPayeeMobile() {
		return payeeMobile;
	}

	public void setPayeeMobile(String payeeMobile) {
		this.payeeMobile = payeeMobile;
	}

	public String getKek() {
		return kek;
	}

	public void setKek(String kek) {
		this.kek = kek;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getVpaTerminalId() {
		return vpaTerminalId;
	}

	public void setVpaTerminalId(String vpaTerminalId) {
		this.vpaTerminalId = vpaTerminalId;
	}

	public String getPlanDek() {
		return planDek;
	}

	public void setPlanDek(String planDek) {
		this.planDek = planDek;
	}

	public String getPublicKey() {
		return publicKey;
	}

	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getSubMerchantId() {
		return subMerchantId;
	}

	public void setSubMerchantId(String subMerchantId) {
		this.subMerchantId = subMerchantId;
	}

	public String getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}

	@Override
	public SOAPMessage buildRequest() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getRequest() {
		String merchantCollectMoneyRequest = "<soapenv:Envelope"
				+" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/"+'"'
				+" xmlns:i=\"i"+'"'
				+" xmlns:upi=\"http://com/fss/upi" +'"'
				+" xmlns:java=\"java:com.fss.upi.req\">"
				+" <soapenv:Header/>"
				+" <soapenv:Body>"
				+" <upi:MerchantCollectMoney xmlns:upi=\"http://com/fss/upi" +'"'+ " xmlns:java=\"java:com.fss.upi.req\"> "
					+"<upi:req>"
						+"<java:Amount>"+getAmount()+"</java:Amount>"
						+"<java:Expdate>"+getExpTime()+"</java:Expdate>"
						+"<java:PayerVirAddr>"+getPayerVirAddr()+"</java:PayerVirAd"
								+ "dr>"
						+"<java:PayeeVirAddr>"+UPIConstants.PAYEE_VIR_ADDR+"</java:PayeeVirAddr>"
						+"<java:payeeTranserType>"+UPIConstants.PAYEE_TRANSFER_TYPE+"</java:payeeTranserType>"
						+"<java:PayeeAccNo></java:PayeeAccNo>"
						+"<java:GeoCode>"+UPIConstants.GEO_CODE+"</java:GeoCode>"
						+"<java:DevLocation>"+UPIConstants.DEV_LOCATION+"</java:DevLocation>"
						+"<java:DevIp>"+UPIConstants.DEV_IP+"</java:DevIp>"
						+"<java:DevIp>100.100.100.100</java:DevIp>"
						+"<java:DevType>WEB</java:DevType>"
						+"<java:DevOs>WEB</java:DevOs>"
						+"<java:DevApp>com.fss.vijb</java:DevApp>"
						+"<java:DevCapability>1000</java:DevCapability>"
						+"<java:DevId>WEB</java:DevId>"
						+"<java:DeviceID>WEB</java:DeviceID>"
						+"<java:CurrencyCode>INR</java:CurrencyCode>"
						+"<java:AddrType>ACCOUNT</java:AddrType>"
						+"<java:TransferMode i:type=\"d:string\">VIR</java:TransferMode>"
						+"<java:UPI>"
							+"<java:Channel>"+UPIConstants.VPA_CHENNEL+"</java:Channel>"
							+"<java:MobileNo>"+UPIConstants.PAYEE_MOBILE_NUM+"</java:MobileNo>"
							+"<java:MsgId>"+getMsgId()+"</java:MsgId>"
							+"<java:OrgId>"+UPIConstants.ORG_ID+"</java:OrgId>"
							+"<java:BankId>"+UPIConstants.BANK_ID+"</java:BankId>"
							+"<java:Remarks>"+UPIConstants.REMARK+"</java:Remarks>"
							+"<java:TimeStamp>"+getTimeStamp()+"</java:TimeStamp>"
							+"<java:DeviceID>"+UPIConstants.VPA_DEVICE_ID+"</java:DeviceID>"
							+"<java:PayerType>"+UPIConstants.PAYER_TYPE+"</java:PayerType>"
							+"<java:AppVersion>"+UPIConstants.APP_VERSION+"</java:AppVersion>"
							+"<java:MerchantID i:type=\"d:string\">"+UPIConstants.MERCHANT_ID+"</java:MerchantID>"
							+"<java:TerminalID i:type=\"d:string\">"+UPIConstants.TERMINAL_ID+"</java:TerminalID>"
							+"<java:MerchantCredentials i:type=\"d:string\">"+getMerchantCredentials()+"</java:MerchantCredentials>"
						+"</java:UPI>"	
						+"<java:RefId i:type=\"d:string\">"+getRefId()+"</java:RefId>"
						+"<java:SdkVersion i:type=\"d:string\">"+UPIConstants.SDK_VERSION+"</java:SdkVersion>"
						+"<java:SdkName i:type=\"d:string\">"+UPIConstants.SKD_NAME+"</java:SdkName>"
						+"<java:PayeeCode i:type=\"d:string\">"+UPIConstants.PAYEE_CODE+"</java:PayeeCode>"
						+"<java:CurrencyType i:type=\"d:string\">"+UPIConstants.CURRENCY_TYPE+"</java:CurrencyType>"
						+"<java:PayerCode i:type=\"d:string\">"+UPIConstants.PAYER_CODE+"</java:PayerCode>"
						+"<java:PayeeType i:type=\"d:string\">"+UPIConstants.PAYEE_TYPE+"</java:PayeeType>"
					+"</upi:req>"	
				+" </upi:MerchantCollectMoney>"
				+" </soapenv:Body>"
				+" </soapenv:Envelope>";
		return merchantCollectMoneyRequest;
	}
	public static void main(String[] args) throws Exception {
		/*MerchantCollectMoneyRequest dto = new MerchantCollectMoneyRequest();
		dto.setRefId("123456701369");
		dto.setMsgId("VJBF99421A6C49D19B9754494EEF019P");
		dto.setTimeStamp("140116155016");
		dto.setMerchantCredentials("vm0U7xw3gN45bFTB/7EqpNabiFbykIR0X0+Gn6ReTig=");
		
		dto.setMobileNo("8050581012");
		dto.setPayerVirAddr("testvpa@vijb");
		dto.setAmount(10);
		
		 System.out.println("Request: " + dto.getRequest());
//		 Client restClient = UPIConstants.createClient();
//		 WebResource webResource = restClient.resource(UPIConstants.MERCHANT_SERVICE_URL);
//			restClient.addFilter(new LoggingFilter(System.out));
//			ClientResponse resp = webResource.type("application/xml").post(ClientResponse.class, dto.getRequest());
//			String output = resp.getEntity(String.class);
//			System.out.println("response: " + output);
		 
		 MerchantMetaInfoVPARequest merchantMeta = new MerchantMetaInfoVPARequest();	
			merchantMeta.setMetaData(DesEncryption.desEncrypt(dto.getRequest(), dto.getMsgId()));
			merchantMeta.setMetaInfo(UPIRSAEncryption.encryptData(dto.getMsgId(), UPIConstants.PUBLIK_KEY));
			System.out.println("Request: " + merchantMeta.getRequest());
			Client restClient = UPIConstants.createClient();
			WebResource webResource = restClient.resource(UPIConstants.MERCHANT_SERVICE_URL);
			restClient.addFilter(new LoggingFilter(System.out));
			ClientResponse resp = webResource.type("application/xml").post(ClientResponse.class, merchantMeta.getRequest());
			String output = resp.getEntity(String.class);
			System.out.println("response: " + output);*/
		System.out.println(ConvertUtil.getUpiDate());
	}

	@Override
	public String getBescomUpiRequest() {
		String merchantCollectMoneyRequest = "<soapenv:Envelope"
				+" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/"+'"'
				+" xmlns:i=\"i"+'"'
				+" xmlns:upi=\"http://com/fss/upi" +'"'
				+" xmlns:java=\"java:com.fss.upi.req\">"
				+" <soapenv:Header/>"
				+" <soapenv:Body>"
				+" <upi:MerchantCollectMoney xmlns:upi=\"http://com/fss/upi" +'"'+ " xmlns:java=\"java:com.fss.upi.req\"> "
					+"<upi:req>"
						+"<java:Amount>"+getAmount()+"</java:Amount>"
						+"<java:Expdate>"+getExpTime()+"</java:Expdate>"
						+"<java:PayerVirAddr>"+getPayerVirAddr()+"</java:PayerVirAddr>"
						+"<java:PayeeVirAddr>"+getPayeeVirAddr()+"</java:PayeeVirAddr>"
						+"<java:payeeTranserType>"+UPIBescomContstant.PAYEE_TRANSFER_TYPE+"</java:payeeTranserType>"
						+"<java:PayeeAccNo></java:PayeeAccNo>"
						+"<java:GeoCode>"+UPIBescomContstant.GEO_CODE+"</java:GeoCode>"
						+"<java:DevLocation>"+UPIBescomContstant.DEV_LOCATION+"</java:DevLocation>"
						+"<java:DevIp>100.100.100.100</java:DevIp>"
						+"<java:DevType>WEB</java:DevType>"
						+"<java:DevOs>WEB</java:DevOs>"
						+"<java:DevApp>com.fss.vijb</java:DevApp>"
						+"<java:DevCapability>1000</java:DevCapability>"
						+"<java:DevId>WEB</java:DevId>"
						+"<java:DeviceID>WEB</java:DeviceID>"
						+"<java:CurrencyCode>INR</java:CurrencyCode>"
						+"<java:AddrType>ACCOUNT</java:AddrType>"
						+"<java:TransferMode i:type=\"d:string\">VIR</java:TransferMode>"
						+"<java:UPI>"
							+"<java:Channel>"+UPIBescomContstant.VPA_CHENNEL+"</java:Channel>"
							+"<java:MobileNo>"+getMobileNo()+"</java:MobileNo>"
							+"<java:MsgId>"+getMsgId()+"</java:MsgId>"
							+"<java:OrgId>"+getOrgId()+"</java:OrgId>"
							+"<java:BankId>"+UPIBescomContstant.BANK_ID+"</java:BankId>"
							+"<java:Remarks>"+UPIBescomContstant.REMARK+"</java:Remarks>"
							+"<java:TimeStamp>"+getTimeStamp()+"</java:TimeStamp>"
							+"<java:DeviceID>"+UPIBescomContstant.VPA_DEVICE_ID+"</java:DeviceID>"
							+"<java:PayerType>"+UPIBescomContstant.PAYER_TYPE+"</java:PayerType>"
							+"<java:AppVersion>"+UPIBescomContstant.APP_VERSION+"</java:AppVersion>"
							+"<java:MerchantID i:type=\"d:string\">"+getMerchantId()+"</java:MerchantID>"
							+"<java:TerminalID i:type=\"d:string\">"+getTerminalId()+"</java:TerminalID>"
							+"<java:MerchantCredentials i:type=\"d:string\">"+getMerchantCredentials()+"</java:MerchantCredentials>"
						+"</java:UPI>"	
						+"<java:RefId i:type=\"d:string\">"+getRefId()+"</java:RefId>"
						+"<java:SdkVersion i:type=\"d:string\">"+UPIBescomContstant.SDK_VERSION+"</java:SdkVersion>"
						+"<java:SdkName i:type=\"d:string\">"+UPIBescomContstant.SKD_NAME+"</java:SdkName>"
						+"<java:PayeeCode i:type=\"d:string\">"+UPIBescomContstant.PAYEE_CODE+"</java:PayeeCode>"
						+"<java:CurrencyType i:type=\"d:string\">"+UPIBescomContstant.CURRENCY_TYPE+"</java:CurrencyType>"
						+"<java:PayerCode i:type=\"d:string\">"+UPIBescomContstant.PAYER_CODE+"</java:PayerCode>"
						+"<java:PayeeType i:type=\"d:string\">"+UPIBescomContstant.PAYEE_TYPE+"</java:PayeeType>"
					+"</upi:req>"	
				+" </upi:MerchantCollectMoney>"
				+" </soapenv:Body>"
				+" </soapenv:Envelope>";
		return merchantCollectMoneyRequest;
	}
}
