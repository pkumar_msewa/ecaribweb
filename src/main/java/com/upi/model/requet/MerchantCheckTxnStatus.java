package com.upi.model.requet;

import javax.xml.soap.SOAPMessage;

import com.upi.util.UPIBescomContstant;
import com.upi.util.UPIConstants;

public class MerchantCheckTxnStatus implements SOAPRequest {

	private String timeStamp;
	private String msgId;
	private String mobileNo;
	private String refId;
	private String merchantCredentials;
	private String txnId;
	private String merchantId;
	private String orgId;
	private String terminalId;

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}

	public String getTxnId() {
		return txnId;
	}

	public void setTxnId(String txnId) {
		this.txnId = txnId;
	}

	public String getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getMsgId() {
		return msgId;
	}

	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getRefId() {
		return refId;
	}

	public void setRefId(String refId) {
		this.refId = refId;
	}

	public String getMerchantCredentials() {
		return merchantCredentials;
	}

	public void setMerchantCredentials(String merchantCredentials) {
		this.merchantCredentials = merchantCredentials;
	}

	@Override
	public SOAPMessage buildRequest() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getRequest() {
		String merchantCheckTxnStatus = "<soapenv:Envelope"
				+" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/"+'"'
				+" xmlns:upi=\"http://com/fss/upi" +'"'
				+" xmlns:java=\"java:com.fss.upi.req\">"
				+" <soapenv:Header/>"
				+" <soapenv:Body>"
				+" <upi:MerchantCheckTxnStatus> "
					+"<upi:req>"
						+"<java:UPI>"
							+"<java:Channel>"+UPIConstants.VPA_CHENNEL+"</java:Channel>"
							+"<java:MobileNo>"+getMobileNo()+"</java:MobileNo>"
							+"<java:MsgId>"+getMsgId()+"</java:MsgId>"
							+"<java:OrgId>"+UPIConstants.ORG_ID+"</java:OrgId>"
							+"<java:BankId>"+UPIConstants.BANK_ID+"</java:BankId>"
							+"<java:Remarks>?</java:Remarks>"
							+"<java:TimeStamp>"+getTimeStamp()+"</java:TimeStamp>"
							+"<java:DeviceID>"+UPIConstants.VPA_DEVICE_ID+"</java:DeviceID>"
							+"<java:PayerType>"+UPIConstants.VPA_PAYER_TYPE+"</java:PayerType>"
							+"<java:SubMerchantID></java:SubMerchantID>"
		                    +"<java:MerchantID>"+UPIConstants.MERCHANT_ID+"</java:MerchantID>"
		                    +"<java:TerminalID>"+UPIConstants.TERMINAL_ID+"</java:TerminalID>"
		                    +"<java:MerchantCredentials>"+getMerchantCredentials()+"</java:MerchantCredentials>"
		                    +"<java:AppVersion>"+UPIConstants.STATUS_APP_VERSION+"</java:AppVersion>"
		                    +"<java:SdkVersion>"+UPIConstants.SDK_VERSION+"</java:SdkVersion>"
		                    +"<java:SdkName>"+UPIConstants.SKD_NAME+"</java:SdkName>"
		                    +"<java:RefId>"+getRefId()+"</java:RefId>"
		                +"</java:UPI>"
		                +"<java:OrgTxnId>"+getTxnId()+"</java:OrgTxnId>"
		            +"</upi:req>"	
				+" </upi:MerchantCheckTxnStatus>"
				+" </soapenv:Body>"
				+" </soapenv:Envelope>";
		return merchantCheckTxnStatus;
	}

	@Override
	public String getBescomUpiRequest() {
		String merchantCheckTxnStatus = "<soapenv:Envelope"
				+" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/"+'"'
				+" xmlns:upi=\"http://com/fss/upi" +'"'
				+" xmlns:java=\"java:com.fss.upi.req\">"
				+" <soapenv:Header/>"
				+" <soapenv:Body>"
				+" <upi:MerchantCheckTxnStatus> "
					+"<upi:req>"
						+"<java:UPI>"
							+"<java:Channel>"+UPIBescomContstant.VPA_CHENNEL+"</java:Channel>"
							+"<java:MobileNo>"+getMobileNo()+"</java:MobileNo>"
							+"<java:MsgId>"+getMsgId()+"</java:MsgId>"
							+"<java:OrgId>"+getOrgId()+"</java:OrgId>"
							+"<java:BankId>"+UPIBescomContstant.BANK_ID+"</java:BankId>"
							+"<java:Remarks>?</java:Remarks>"
							+"<java:TimeStamp>"+getTimeStamp()+"</java:TimeStamp>"
							+"<java:DeviceID>"+UPIBescomContstant.VPA_DEVICE_ID+"</java:DeviceID>"
							+"<java:PayerType>"+UPIBescomContstant.VPA_PAYER_TYPE+"</java:PayerType>"
							+"<java:SubMerchantID></java:SubMerchantID>"
		                    +"<java:MerchantID>"+getMerchantId()+"</java:MerchantID>"
		                    +"<java:TerminalID>"+getTerminalId()+"</java:TerminalID>"
		                    +"<java:MerchantCredentials>"+getMerchantCredentials()+"</java:MerchantCredentials>"
		                    +"<java:AppVersion>"+UPIBescomContstant.STATUS_APP_VERSION+"</java:AppVersion>"
		                    +"<java:SdkVersion>"+UPIBescomContstant.SDK_VERSION+"</java:SdkVersion>"
		                    +"<java:SdkName>"+UPIBescomContstant.SKD_NAME+"</java:SdkName>"
		                    +"<java:RefId>"+getRefId()+"</java:RefId>"
		                +"</java:UPI>"
		                +"<java:OrgTxnId>"+getTxnId()+"</java:OrgTxnId>"
		            +"</upi:req>"	
				+" </upi:MerchantCheckTxnStatus>"
				+" </soapenv:Body>"
				+" </soapenv:Envelope>";
		return merchantCheckTxnStatus;
	}

}
