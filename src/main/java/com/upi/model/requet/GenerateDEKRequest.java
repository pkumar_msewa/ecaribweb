package com.upi.model.requet;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.payqwikweb.util.CommonUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.upi.util.UPIBescomContstant;
import com.upi.util.UPIConstants;

public class GenerateDEKRequest implements SOAPRequest {
	
	private String timeStamp;
	private String msgId;
	private String mobileNo;
	private String merchantId;
	private String orgId;
	private String terminalId;

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}

	public String getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getMsgId() {
		return msgId;
	}

	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	@Override
	public SOAPMessage buildRequest() {
		SOAPMessage soapMessage = CommonUtil.createSOAPMessage();
		if (soapMessage != null) {
			SOAPPart soapPart = soapMessage.getSOAPPart();
			try {
				   SOAPEnvelope envelope = soapPart.getEnvelope();
				   envelope.addNamespaceDeclaration("i","http://www.w3.org/2001/XMLSchemainstance");
				   envelope.addNamespaceDeclaration("d","http://www.w3.org/2001/XMLSchema");
				   envelope.addNamespaceDeclaration("c","http://schemas.xmlsoap.org/soap/encoding/");
				   envelope.setPrefix("v");
				   SOAPHeader header = envelope.getHeader();
				   header.setPrefix("v");
				   SOAPBody body = envelope.getBody();
				   body.setPrefix("v");

				   SOAPBodyElement element = body.addBodyElement(new QName("http://com/fss/upi","GenerateMerchantDEK","n0"));
//				   element.addNamespaceDeclaration("c","http://schemas.xmlsoap.org/soap/encoding/");
				   element.setAttribute("id","o0");
				   element.addAttribute(new QName("","root","c"),"1");
				   SOAPElement req = element.addChildElement(new QName("http://com/fss/upi","req","n0"));
//				   req.addNamespaceDeclaration("i","http://www.w3.org/1999/XMLSchema-instance");
				   req.addAttribute(new QName("","type","i"),"n0:req");	

				   SOAPElement upi = req.addChildElement(new QName("http://com/fss/upi","UPI","n0"));
				   upi.addAttribute(new QName("","type","i"),"n0:UPI");
				   //FOR timeStamp
				   SOAPElement timeStamp = upi.addChildElement(new QName("","TimeStamp",""));
				   timeStamp.addAttribute(new QName("","type","i"),"d:string");
				   timeStamp.addTextNode(getTimeStamp());
				   //FOR MsgId
				   SOAPElement MsgId = upi.addChildElement(new QName("","MsgId",""));
				   MsgId.addAttribute(new QName("","type","i"),"d:string");
				   MsgId.addTextNode(getMsgId());
				   //FOR DeviceID
				   SOAPElement deviceID = upi.addChildElement(new QName("","DeviceID",""));
				   deviceID.addAttribute(new QName("","type","i"),"d:string");
				   deviceID.addTextNode(UPIBescomContstant.VPA_DEVICE_ID);
				   //FOR Channel
				   SOAPElement channel = upi.addChildElement(new QName("","Channel",""));
				   channel.addAttribute(new QName("","type","i"),"d:string");
				   channel.addTextNode(UPIBescomContstant.VPA_CHENNEL);
				   //FOR AppVersion
				   SOAPElement appVersion = upi.addChildElement(new QName("","AppVersion",""));
				   appVersion.addAttribute(new QName("","type","i"),"d:string");
				   appVersion.addTextNode(UPIBescomContstant.APP_VERSION);
				   //FOR PayerType
				   SOAPElement payerType = upi.addChildElement(new QName("","PayerType",""));
				   payerType.addAttribute(new QName("","type","i"),"d:string");
				   payerType.addTextNode(UPIBescomContstant.PAYER_TYPE);
				   //FOR OrgId
				   SOAPElement orgId = upi.addChildElement(new QName("","OrgId",""));
				   orgId.addAttribute(new QName("","type","i"),"d:string");
//				   orgId.addTextNode(UPIBescomContstant.ORG_ID);
				   //FOR BankId
				   SOAPElement bankId = upi.addChildElement(new QName("","BankId",""));
				   bankId.addAttribute(new QName("","type","i"),"d:string");
				   bankId.addTextNode(UPIBescomContstant.BANK_ID);
				   //FOR Remarks
				   SOAPElement remarks = upi.addChildElement(new QName("","Remarks",""));
				   remarks.addAttribute(new QName("","type","i"),"d:string");
//				   remarks.addTextNode("Dro8e647mo.stFBAs8ms");
				   //FOR MobileNo
				   SOAPElement mobileNo = upi.addChildElement(new QName("","MobileNo",""));
				   mobileNo.addAttribute(new QName("","type","i"),"d:string");
				   mobileNo.addTextNode(getMobileNo());
				   //FOR MerchantID
				   SOAPElement merchantID = upi.addChildElement(new QName("","MerchantID",""));
				   merchantID.addAttribute(new QName("","type","i"),"d:string");
//				   merchantID.addTextNode(UPIBescomContstant.MERCHANT_ID);
				   //FOR SubMerchantID
				   SOAPElement subMerchantID = upi.addChildElement(new QName("","SubMerchantID",""));
				   subMerchantID.addAttribute(new QName("","type","i"),"d:string");
				   subMerchantID.addTextNode(UPIBescomContstant.SUB_MERCHANT_ID);
				   //FOR TerminalID
				   SOAPElement terminalID = upi.addChildElement(new QName("","TerminalID",""));
				   terminalID.addAttribute(new QName("","type","i"),"d:string");
//				   terminalID.addTextNode(UPIBescomContstant.TERMINAL_ID);
				   //FOR MerchantCredentials
				   SOAPElement merchantCredentials = upi.addChildElement(new QName("","MerchantCredentials",""));
				   merchantCredentials.addAttribute(new QName("","type","i"),"d:string");
				   //FOR reqData
				   SOAPElement reqData = req.addChildElement(new QName("","reqData",""));
				 //FOR data
				   SOAPElement data = reqData.addChildElement(new QName("","data",""));
				   data.addAttribute(new QName("","type","i"),"d:string");
				   data.addTextNode(UPIBescomContstant.REQ_DATA);

				   envelope.removeNamespaceDeclaration("SOAP-ENV");
				   soapMessage.saveChanges();
				   soapMessage.writeTo(System.err);
				
				System.err.println();
			} catch (SOAPException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return soapMessage;
	}

	@Override
	public String getRequest() {
		String generateMerchantDEK = "<v:Envelope "
				 + " xmlns:i=\"http://www.w3.org/2001/XMLSchemainstance" +'"'
				 + " xmlns:d=\"http://www.w3.org/2001/XMLSchema" +'"'
				 + " xmlns:c=\"http://schemas.xmlsoap.org/soap/encoding/" +'"'
				 + " xmlns:v=\"http://schemas.xmlsoap.org/soap/envelope/\"> "
					+ "<v:Header/>"  
				   + "<v:Body>"  
				       + "<n0:GenerateMerchantDEK id=\"o0\" c:root=\"1"+'"'+ " xmlns:n0=\"http://com/fss/upi\">" 
				       		+ "<n0:req i:type=\"n0:req\">"  
				                + "<n0:UPI i:type=\"n0:UPI\">"  
					                + "<TimeStamp i:type=\"d:string\">"+getTimeStamp()+"</TimeStamp>"
					                + "<MsgId i:type=\"d:string\">"+getMsgId()+"</MsgId>"
					                + "<DeviceID i:type=\"d:string\">"+UPIConstants.DEVICE_ID+"</DeviceID>" 
					                + "<Channel i:type=\"d:string\">"+UPIConstants.CHENNEL+"</Channel>"  
					                + "<AppVersion i:type=\"d:string\">"+UPIConstants.APP_VERSION+"</AppVersion>" 
					                + "<PayerType i:type=\"d:string\">"+UPIConstants.PAYER_TYPE+"</PayerType>" 
					                + "<OrgId i:type=\"d:string\">"+UPIConstants.ORG_ID+"</OrgId>" 
					                + "<BankId i:type=\"d:string\">"+UPIConstants.BANK_ID+"</BankId>" 
					                + "<Remarks i:type=\"d:string\"/>"  
					                + "<MobileNo i:type=\"d:string\">"+getMobileNo()+"</MobileNo>" 
					                + "<MerchantID i:type=\"d:string\">"+UPIConstants.MERCHANT_ID+"</MerchantID>"  
					                + "<SubMerchantID i:type=\"d:string\">"+UPIConstants.SUB_MERCHANT_ID+"</SubMerchantID>" 
					                + "<TerminalID i:type=\"d:string\">"+UPIConstants.TERMINAL_ID+"</TerminalID>" 
					                + "<MerchantCredentials i:type=\"d:string\"/>"  
				               + "</n0:UPI>"  
				               + "<reqData>"  
				                   + " <data i:type=\"d:string\">"+UPIConstants.REQ_DATA+"</data>"  
				               + "</reqData>"
				           + "</n0:req>" 
				       + "</n0:GenerateMerchantDEK>"  
				   + "</v:Body>"  
				+ "</v:Envelope>";
		return generateMerchantDEK;
	}
	
	@Override
	public String getBescomUpiRequest() {
		String generateMerchantDEK = "<v:Envelope "
				 + " xmlns:i=\"http://www.w3.org/2001/XMLSchemainstance" +'"'
				 + " xmlns:d=\"http://www.w3.org/2001/XMLSchema" +'"'
				 + " xmlns:c=\"http://schemas.xmlsoap.org/soap/encoding/" +'"'
				 + " xmlns:v=\"http://schemas.xmlsoap.org/soap/envelope/\"> "
					+ "<v:Header/>"  
				   + "<v:Body>"  
				       + "<n0:GenerateMerchantDEK id=\"o0\" c:root=\"1"+'"'+ " xmlns:n0=\"http://com/fss/upi\">" 
				       		+ "<n0:req i:type=\"n0:req\">"  
				                + "<n0:UPI i:type=\"n0:UPI\">"  
					                + "<TimeStamp i:type=\"d:string\">"+getTimeStamp()+"</TimeStamp>"
					                + "<MsgId i:type=\"d:string\">"+getMsgId()+"</MsgId>"
					                + "<DeviceID i:type=\"d:string\">"+UPIBescomContstant.DEVICE_ID+"</DeviceID>" 
					                + "<Channel i:type=\"d:string\">"+UPIBescomContstant.CHENNEL+"</Channel>"  
					                + "<AppVersion i:type=\"d:string\">"+UPIBescomContstant.APP_VERSION+"</AppVersion>" 
					                + "<PayerType i:type=\"d:string\">"+UPIBescomContstant.PAYER_TYPE+"</PayerType>" 
					                + "<OrgId i:type=\"d:string\">"+getOrgId()+"</OrgId>" 
					                + "<BankId i:type=\"d:string\">"+UPIBescomContstant.BANK_ID+"</BankId>" 
					                + "<Remarks i:type=\"d:string\"/>"  
					                + "<MobileNo i:type=\"d:string\">"+getMobileNo()+"</MobileNo>" 
					                + "<MerchantID i:type=\"d:string\">"+getMerchantId()+"</MerchantID>"  
					                + "<SubMerchantID i:type=\"d:string\">"+UPIBescomContstant.SUB_MERCHANT_ID+"</SubMerchantID>" 
					                + "<TerminalID i:type=\"d:string\">"+getTerminalId()+"</TerminalID>" 
					                + "<MerchantCredentials i:type=\"d:string\"/>"  
				               + "</n0:UPI>"  
				               + "<reqData>"  
				                   + " <data i:type=\"d:string\">"+UPIBescomContstant.REQ_DATA+"</data>"  
				               + "</reqData>"
				           + "</n0:req>" 
				       + "</n0:GenerateMerchantDEK>"  
				   + "</v:Body>"  
				+ "</v:Envelope>";
		return generateMerchantDEK;
	}
	
	public static void main(String[] args) throws Exception {
		 System.out.println();
		 GenerateDEKRequest dto = new GenerateDEKRequest();
		 dto.setMobileNo("8050581012");
		 dto.setTimeStamp("141161499011");
		 dto.setMsgId("VJBF99421A6C49D19B9754494EE129YY");
		 System.err.println(dto.getRequest());
//		 
//		 
//		 
		 SOAPConnection connection = CommonUtil.getNewConnection();
		 CommonUtil.doTrustToCertificates();
		 SOAPMessage resp = connection.call(dto.buildRequest(), UPIConstants.MERCHANT_SERVICE_URL);
		 resp.writeTo(System.err);
		 
		 /*System.out.println(dto.getRequest());
		  Client restClient = UPIBescomContstant.createClient();
			WebResource webResource = restClient.resource(UPIBescomContstant.MERCHANT_SERVICE_URL);
			restClient.addFilter(new LoggingFilter(System.out));
			ClientResponse resp = webResource.type("application/xml").post(ClientResponse.class, dto.getRequest());
			String output = resp.getEntity(String.class);
			
			if (resp.getStatus() == 200) {
				if(resp.getStatus() != 401){
					DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
					DocumentBuilder builder = factory.newDocumentBuilder();
					InputStream inputStream = new ByteArrayInputStream(output.getBytes());
					org.w3c.dom.Document doc = builder.parse(inputStream);
					System.err.println("inside true");
					NodeList nodes = doc.getElementsByTagName("upi:return");
					System.out.println("node :: " + nodes.getLength());
					for (int i = 0; i < nodes.getLength(); i++) {
						Element element = (Element) nodes.item(i);
						String errDesc = element.getElementsByTagName("java:DEKKey").item(0).getFirstChild().getTextContent();
						System.err.println("ErrDesc  :: "+errDesc);
					}
				}else{
					System.err.println("inside false "+ resp.getStatus());
				}
			}else{
				System.err.println("inside false "+ resp.getStatus());
			}*/
	
	}
}













