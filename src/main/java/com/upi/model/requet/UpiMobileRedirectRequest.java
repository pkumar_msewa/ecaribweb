package com.upi.model.requet;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.thirdparty.model.JSONWrapper;

public class UpiMobileRedirectRequest implements JSONWrapper{

	private String txnRefNo;
	private String refId;
	private String upiId;
	private String status;
	private String desc;
	private String txnsRespCode;

	public String getTxnsRespCode() {
		return txnsRespCode;
	}

	public void setTxnsRespCode(String txnsRespCode) {
		this.txnsRespCode = txnsRespCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getTxnRefNo() {
		return txnRefNo;
	}

	public void setTxnRefNo(String txnRefNo) {
		this.txnRefNo = txnRefNo;
	}

	public String getRefId() {
		return refId;
	}

	public void setRefId(String refId) {
		this.refId = refId;
	}

	public String getUpiId() {
		return upiId;
	}

	public void setUpiId(String upiId) {
		this.upiId = upiId;
	}

	 @Override
	    public JSONObject toJSON() {
	        JSONObject jsonObject = new JSONObject();
	        try {
	            jsonObject.put("txnRefNo",getTxnRefNo());
	            jsonObject.put("refId",getRefId());
	            jsonObject.put("upiId",getUpiId());
	            jsonObject.put("status",getStatus());
	            jsonObject.put("desc",getDesc());
	            jsonObject.put("txnsRespCode",getTxnsRespCode());
	            return jsonObject;
	        } catch (JSONException e) {
	            return null;
	        }

	    }
}
