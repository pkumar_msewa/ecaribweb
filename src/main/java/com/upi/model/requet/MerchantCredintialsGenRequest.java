package com.upi.model.requet;

public class MerchantCredintialsGenRequest {

	private String kek;
	private String plainDEK;
	private String transactionId;

	public String getKek() {
		return kek;
	}

	public void setKek(String kek) {
		this.kek = kek;
	}

	public String getPlainDEK() {
		return plainDEK;
	}

	public void setPlainDEK(String plainDEK) {
		this.plainDEK = plainDEK;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
}
