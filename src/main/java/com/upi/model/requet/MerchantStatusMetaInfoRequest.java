package com.upi.model.requet;

import javax.xml.soap.SOAPMessage;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.upi.util.DesEncryption;
import com.upi.util.UPIConstants;
import com.upi.util.UPIRSAEncryption;

public class MerchantStatusMetaInfoRequest implements SOAPRequest {

	private String metaData;
	private String metaInfo;

	public String getMetaData() {
		return metaData;
	}

	public void setMetaData(String metaData) {
		this.metaData = metaData;
	}

	public String getMetaInfo() {
		return metaInfo;
	}

	public void setMetaInfo(String metaInfo) {
		this.metaInfo = metaInfo;
	}

	@Override
	public SOAPMessage buildRequest() {
		return null;
	}

	@Override
	public String getRequest() {
		String MerchantCheckTxnStatus = "<v:Envelope "
				 + " xmlns:i=\"http://www.w3.org/2001/XMLSchemainstance" +'"'
				 + " xmlns:d=\"http://www.w3.org/2001/XMLSchema" +'"'
				 + " xmlns:c=\"http://schemas.xmlsoap.org/soap/encoding/" +'"'
				 + " xmlns:v=\"http://schemas.xmlsoap.org/soap/envelope/\"> "
				 + "<v:Header/> "  
				 + "<v:Body> "  
					+"<MerchantCheckTxnStatus c:root=\"1\" id=\"o0" +'"'+ " xmlns:n0=\"http://com/fss/upi\"> "
						+ "<req> "
							+ " <MetaData i:type=\"d:string\">"+getMetaData()+"</MetaData> "
							+ " <MetaInfo i:type=\"d:string\">"+getMetaInfo()+"</MetaInfo> "
						+ "</req> "
					+ "</MerchantCheckTxnStatus>" 
				+ "</v:Body>"  
				+ "</v:Envelope>";	
		return MerchantCheckTxnStatus;
	}
	
	/*public static void main(String[] args) throws Exception {
		
		MerchantCheckTxnStatus validMerchant = new MerchantCheckTxnStatus();	
		validMerchant.setRefId("123456000013");
		validMerchant.setMsgId("VJB95006157360957824479008239276796");
		validMerchant.setTimeStamp("140116155016");
		validMerchant.setMobileNo("8050581012");
		validMerchant.setTxnId("VJB57502128711039420999481936849927");
		validMerchant.setMerchantCredentials("w3GrgApb6P/8Y4vpj+18qOCCkDbmbZRixX19ZRozzUY=");
		System.err.println("Request: " + validMerchant.getRequest());
		
		MerchantStatusMetaInfoRequest dto = new MerchantStatusMetaInfoRequest();	
		dto.setMetaData(DesEncryption.desEncrypt(validMerchant.getRequest(), validMerchant.getMsgId()));
		dto.setMetaInfo(UPIRSAEncryption.encryptData(validMerchant.getMsgId(), UPIConstants.PUBLIK_KEY));
		System.out.println("Request: " + dto.getRequest());
		Client restClient = UPIConstants.createClient();
		WebResource webResource = restClient.resource(UPIConstants.MERCHANT_SERVICE_URL);
		restClient.addFilter(new LoggingFilter(System.out));
		ClientResponse resp = webResource.type("application/xml").post(ClientResponse.class, dto.getRequest());
		String output = resp.getEntity(String.class);
		System.out.println("response: " + output);
	}*/

	@Override
	public String getBescomUpiRequest() {
		String MerchantCheckTxnStatus = "<v:Envelope "
				 + " xmlns:i=\"http://www.w3.org/2001/XMLSchemainstance" +'"'
				 + " xmlns:d=\"http://www.w3.org/2001/XMLSchema" +'"'
				 + " xmlns:c=\"http://schemas.xmlsoap.org/soap/encoding/" +'"'
				 + " xmlns:v=\"http://schemas.xmlsoap.org/soap/envelope/\"> "
				 + "<v:Header/> "  
				 + "<v:Body> "  
					+"<MerchantCheckTxnStatus c:root=\"1\" id=\"o0" +'"'+ " xmlns:n0=\"http://com/fss/upi\"> "
						+ "<req> "
							+ " <MetaData i:type=\"d:string\">"+getMetaData()+"</MetaData> "
							+ " <MetaInfo i:type=\"d:string\">"+getMetaInfo()+"</MetaInfo> "
						+ "</req> "
					+ "</MerchantCheckTxnStatus>" 
				+ "</v:Body>"  
				+ "</v:Envelope>";	
		return MerchantCheckTxnStatus;
	}
}
