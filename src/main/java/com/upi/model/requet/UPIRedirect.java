package com.upi.model.requet;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.thirdparty.model.JSONWrapper;

public class UPIRedirect implements JSONWrapper {

	private String referenceId;
	private String respCode;
	private String orgRefId;

	public String getOrgRefId() {
		return orgRefId;
	}

	public void setOrgRefId(String orgRefId) {
		this.orgRefId = orgRefId;
	}

	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	public String getRespCode() {
		return respCode;
	}

	public void setRespCode(String respCode) {
		this.respCode = respCode;
	}

	@Override
	public JSONObject toJSON() {
		JSONObject json = new JSONObject();
		try {
			json.put("referenceId", getReferenceId());
			json.put("respCode", getRespCode());
			json.put("orgRefId", getOrgRefId());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return json;
	}
}
