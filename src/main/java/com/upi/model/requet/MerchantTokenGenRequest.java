package com.upi.model.requet;

import java.io.IOException;

import javax.xml.namespace.QName;
/*import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;*/

import com.payqwikweb.util.CommonUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.upi.util.AESAlgorithm;
import com.upi.util.UPIBescomContstant;
import com.upi.util.UPIConstants;

public class MerchantTokenGenRequest implements SOAPRequest{

	private String timeStamp;
	private String msgId;
	private String mobileNo;
	private String refId;
	private String merchantCredentials;
	private String deviceId;
	private String chennel;
	private boolean sdk;
	private String appVersion;
	private String txnPwd;
	private String merchantId;
	private String orgId;
	private String terminalId;

	public String getTxnPwd() {
		return txnPwd;
	}

	public void setTxnPwd(String txnPwd) {
		this.txnPwd = txnPwd;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}

	public String getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}

	public boolean isSdk() {
		return sdk;
	}

	public void setSdk(boolean sdk) {
		this.sdk = sdk;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getChennel() {
		return chennel;
	}

	public void setChennel(String chennel) {
		this.chennel = chennel;
	}

	public String getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getMsgId() {
		return msgId;
	}

	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getRefId() {
		return refId;
	}

	public void setRefId(String refId) {
		this.refId = refId;
	}

	public String getMerchantCredentials() {
		return merchantCredentials;
	}

	public void setMerchantCredentials(String merchantCredentials) {
		this.merchantCredentials = merchantCredentials;
	}

	/*@Override
	public SOAPMessage buildRequest() {
		SOAPMessage soapMessage = CommonUtil.createSOAPMessage();
		if (soapMessage != null) {
			SOAPPart soapPart = soapMessage.getSOAPPart();
			try {
				 SOAPEnvelope envelope = soapPart.getEnvelope();
				   envelope.addNamespaceDeclaration("i","http://www.w3.org/2001/XMLSchemainstance");
				   envelope.addNamespaceDeclaration("d","http://www.w3.org/2001/XMLSchema");
				   envelope.addNamespaceDeclaration("c","http://schemas.xmlsoap.org/soap/encoding/");
				   envelope.setPrefix("v");
				   SOAPHeader header = envelope.getHeader();
				   header.setPrefix("v");
				   SOAPBody body = envelope.getBody();
				   body.setPrefix("v");

				   SOAPBodyElement element = body.addBodyElement(new QName("http://com/fss/upi","MerchantTokenGen","n0"));
				   element.addNamespaceDeclaration("c","http://schemas.xmlsoap.org/soap/encoding/");
				   element.setAttribute("id","o0");
				   element.addAttribute(new QName("","root","c"),"1");
				   SOAPElement req = element.addChildElement(new QName("http://com/fss/upi","req","n0"));
				   req.addNamespaceDeclaration("i","http://www.w3.org/1999/XMLSchema-instance");
				   req.addAttribute(new QName("","type","i"),"n0:req");

				   SOAPElement upi = req.addChildElement(new QName("http://com/fss/upi","UPI","n0"));
				   upi.addAttribute(new QName("","type","i"),"n0:UPI");
				   //FOR n1
				   SOAPElement n1 = upi.addChildElement(new QName("java:com.fss.upi.req","TokenCode","n1"));
				   n1.addAttribute(new QName("","type","i"),"d:string");
				   n1.addTextNode(UPIBescomContstant.TOKEN_CODE);

				   //FOR n2
				   SOAPElement n2 = upi.addChildElement(new QName("java:com.fss.upi.req","PackageName","n2"));
				   n2.addAttribute(new QName("","type","i"),"d:string");
				   n2.addTextNode(UPIBescomContstant.PACKGE_NAME);

				   //FOR n3
				   SOAPElement n3 = upi.addChildElement(new QName("java:com.fss.upi.req","BankId","n3"));
				   n3.addAttribute(new QName("","type","i"),"d:string");
				   n3.addTextNode(UPIBescomContstant.BANK_ID);

				   //FOR n4
				   SOAPElement n4 = upi.addChildElement(new QName("java:com.fss.upi.req","MerchantID","n4"));
				   n4.addAttribute(new QName("","type","i"),"d:string");
				   n4.addTextNode("VIJBVPAYQWIK");

				   //FOR n5
				   SOAPElement n5 = upi.addChildElement(new QName("java:com.fss.upi.req","PayeeType","n5"));
				   n5.addAttribute(new QName("","type","i"),"d:string");
				   n5.addTextNode("ENTITY");

				   //FOR n6
				   SOAPElement n6 = upi.addChildElement(new QName("java:com.fss.upi.req","OrgId","n6"));
				   n6.addAttribute(new QName("","type","i"),"d:string");
//				   n6.addTextNode(UPIBescomContstant.ORG_ID);

				   //FOR n7
				   SOAPElement n7 = upi.addChildElement(new QName("java:com.fss.upi.req","TerminalID","n7"));
				   n7.addAttribute(new QName("","type","i"),"d:string");
//				   n7.addTextNode(UPIBescomContstant.TERMINAL_ID);

				   //FOR n8
				   SOAPElement n8 = upi.addChildElement(new QName("java:com.fss.upi.req","PayerType","n8"));
				   n8.addAttribute(new QName("","type","i"),"d:string");
				   n8.addTextNode(UPIBescomContstant.PAYER_TYPE);

				   //FOR n9
				   SOAPElement n9 = upi.addChildElement(new QName("java:com.fss.upi.req","PayerCode","n9"));
				   n9.addAttribute(new QName("","type","i"),"d:string");
				   n9.addTextNode(UPIBescomContstant.PAYER_CODE);

				   //FOR n10
				   SOAPElement n10 = upi.addChildElement(new QName("java:com.fss.upi.req","Channel","n10"));
				   n10.addAttribute(new QName("","type","i"),"d:string");
				   n10.addTextNode(UPIBescomContstant.CHENNEL);

				   //FOR n11
				   SOAPElement n11 = upi.addChildElement(new QName("java:com.fss.upi.req","RefId","n11"));
				   n11.addAttribute(new QName("","type","i"),"d:string");
				   n11.addTextNode(getRefId());

				   //FOR n12
				   SOAPElement n12 = upi.addChildElement(new QName("java:com.fss.upi.req","SdkVersion","n12"));
				   n12.addAttribute(new QName("","type","i"),"d:string");
				   n12.addTextNode(UPIBescomContstant.SDK_VERSION);


				   //FOR n13
				   SOAPElement n13 = upi.addChildElement(new QName("java:com.fss.upi.req","MsgId","n13"));
				   n13.addAttribute(new QName("","type","i"),"d:string");
				   n13.addTextNode(getMsgId());

				   //FOR n14
				   SOAPElement n14 = upi.addChildElement(new QName("java:com.fss.upi.req","TimeStamp","n14"));
				   n14.addAttribute(new QName("","type","i"),"d:string");
				   n14.addTextNode(getTimeStamp());

				   //FOR n15
				   SOAPElement n15 = upi.addChildElement(new QName("java:com.fss.upi.req","UserID","n15"));
				   n15.addAttribute(new QName("","type","i"),"d:string");
				   n15.addTextNode(UPIBescomContstant.USER_ID);

				   //FOR n16
				   SOAPElement n16 = upi.addChildElement(new QName("java:com.fss.upi.req","MobileNo","n16"));
				   n16.addAttribute(new QName("","type","i"),"d:string");
				   n16.addTextNode(getMobileNo());

				   //FOR n17
				   SOAPElement n17 = upi.addChildElement(new QName("java:com.fss.upi.req","SdkName","n17"));
				   n17.addAttribute(new QName("","type","i"),"d:string");
				   n17.addTextNode(UPIBescomContstant.SKD_NAME);

				   //FOR n18
				   SOAPElement n18 = upi.addChildElement(new QName("java:com.fss.upi.req","Remarks","n18"));
				   n18.addAttribute(new QName("","type","i"),"d:string");

				   //FOR n19
				   SOAPElement n19 = upi.addChildElement(new QName("java:com.fss.upi.req","PayeeCode","n19"));
				   n19.addAttribute(new QName("","type","i"),"d:string");
				   n19.addTextNode(UPIBescomContstant.PAYEE_CODE);


				   //FOR n20
				   SOAPElement n20 = upi.addChildElement(new QName("java:com.fss.upi.req","CurrencyType","n20"));
				   n20.addAttribute(new QName("","type","i"),"d:string");
				   n20.addTextNode(UPIBescomContstant.CURRENCY_TYPE);

				   //FOR n21
				   SOAPElement n21 = upi.addChildElement(new QName("java:com.fss.upi.req","MerchantCredentials","n21"));
				   n21.addAttribute(new QName("","type","i"),"d:string");
				   n21.addTextNode(getMerchantCredentials());

				   //FOR n22
				   SOAPElement n22 = upi.addChildElement(new QName("java:com.fss.upi.req","AppVersion","n22"));
				   n22.addAttribute(new QName("","type","i"),"d:string");
				   n22.addTextNode(UPIBescomContstant.APP_VERSION);

				   //FOR n23
				   SOAPElement n23 = upi.addChildElement(new QName("java:com.fss.upi.req","DeviceID","n23"));
				   n23.addAttribute(new QName("","type","i"),"d:string");
				   n23.addTextNode(UPIBescomContstant.DEVICE_ID);

				   //FOR n24
				   SOAPElement n24 = upi.addChildElement(new QName("java:com.fss.upi.req","UserPwd","n24"));
				   n24.addAttribute(new QName("","type","i"),"d:string");
				   n24.addTextNode(UPIBescomContstant.TRANSACTION_PWD_TEST);

				   //FOR n25
				   SOAPElement n25 = upi.addChildElement(new QName("java:com.fss.upi.req","SubMerchantID","n25"));
				   n25.addAttribute(new QName("","type","i"),"d:string");

				   envelope.removeNamespaceDeclaration("SOAP-ENV");
				   soapMessage.saveChanges();
				   soapMessage.writeTo(System.err);
			} catch (SOAPException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return soapMessage;
	}*/

	@Override
	public String getRequest() {
		
		String MerchantTokenGen = "<v:Envelope "
				 + " xmlns:i=\"http://www.w3.org/2001/XMLSchemainstance" +'"'
				 + " xmlns:d=\"http://www.w3.org/2001/XMLSchema" +'"'
				 + " xmlns:c=\"http://schemas.xmlsoap.org/soap/encoding/ " +'"'
				 + " xmlns:v=\"http://schemas.xmlsoap.org/soap/envelope/\"> "
				 + "<v:Header/> "  
				 + "<v:Body> " 
				+"<n0:MerchantTokenGen c:root=\"1\" id=\"o0" +'"'
				+ " xmlns:n0=\"http://com/fss/upi" +'"'+ " xmlns:c=\"http://schemas.xmlsoap.org/soap/encoding/\"> "
						+ "<n0:req i:type=\"n0:req" +'"'+" xmlns:i=\"http://www.w3.org/1999/XMLSchema-instance\"> "
						      + "<n0:UPI i:type=\"n0:UPI\">"
						      + "<n1:TokenCode i:type=\"d:string " +'"' +" xmlns:n1=\"java:com.fss.upi.req\">"+UPIConstants.TOKEN_CODE+"</n1:TokenCode> "
						      + "<n2:PackageName i:type=\"d:string " +'"' +" xmlns:n2=\"java:com.fss.upi.req\">"+UPIConstants.PACKGE_NAME+"</n2:PackageName> "
						      + "<n3:BankId i:type=\"d:string" +'"' +" xmlns:n3=\"java:com.fss.upi.req\">"+UPIConstants.BANK_ID+"</n3:BankId> "
						      + "<n4:MerchantID i:type=\"d:string" +'"' +" xmlns:n4=\"java:com.fss.upi.req\">"+UPIConstants.MERCHANT_ID+"</n4:MerchantID> "
						      + "<n5:PayeeType i:type=\"d:string" +'"' +"  xmlns:n5=\"java:com.fss.upi.req\">"+UPIConstants.PAYEE_TYPE+"</n5:PayeeType> "
						      + "<n6:OrgId i:type=\"d:string" +'"' +" xmlns:n6=\"java:com.fss.upi.req\">"+UPIConstants.ORG_ID+"</n6:OrgId> "
						      + "<n7:TerminalID i:type=\"d:string" +'"' +" xmlns:n7=\"java:com.fss.upi.req\">"+UPIConstants.TERMINAL_ID+"</n7:TerminalID> "
						      + "<n8:PayerType i:type=\"d:string"+'"' +"  xmlns:n8=\"java:com.fss.upi.req\">"+UPIConstants.PAYER_TYPE+"</n8:PayerType> "
						      + "<n9:PayerCode i:type=\"d:string" +'"' +" xmlns:n9=\"java:com.fss.upi.req\">"+UPIConstants.PAYER_CODE+"</n9:PayerCode> "
						      + "<n10:Channel i:type=\"d:string" +'"' +"  xmlns:n10=\"java:com.fss.upi.req\">"+getChennel()+"</n10:Channel> "
						      + "<n11:RefId i:type=\"d:string" +'"' +" xmlns:n11=\"java:com.fss.upi.req\">"+getRefId()+"</n11:RefId> "
						      + "<n12:SdkVersion i:type=\"d:string" +'"' +"  xmlns:n12=\"java:com.fss.upi.req\">"+UPIConstants.SDK_VERSION+"</n12:SdkVersion> "
						      + "<n13:MsgId i:type=\"d:string" +'"' +"  xmlns:n13=\"java:com.fss.upi.req\">"+getMsgId()+"</n13:MsgId> "
						      + "<n14:TimeStamp i:type=\"d:string" +'"' +"  xmlns:n14=\"java:com.fss.upi.req\">"+getTimeStamp()+"</n14:TimeStamp> "
						      + "<n15:UserID i:type=\"d:string" +'"' +"  xmlns:n15=\"java:com.fss.upi.req\">"+UPIConstants.USER_ID+"</n15:UserID> "
						      + "<n16:MobileNo i:type=\"d:string" +'"' +"  xmlns:n16=\"java:com.fss.upi.req\">"+getMobileNo()+"</n16:MobileNo> "
						      + "<n17:SdkName i:type=\"d:string" +'"' +"  xmlns:n17=\"java:com.fss.upi.req\">"+UPIConstants.SKD_NAME+"</n17:SdkName> "
						      + "<n18:Remarks i:type=\"d:string"+'"' +"  xmlns:n18=\"java:com.fss.upi.req\"/> "
						      + "<n19:PayeeCode i:type=\"d:string" +'"' +"  xmlns:n19=\"java:com.fss.upi.req\">"+UPIConstants.PAYEE_CODE+"</n19:PayeeCode> "
						      + "<n20:CurrencyType i:type=\"d:string" +'"' +"  xmlns:n20=\"java:com.fss.upi.req\">"+UPIConstants.CURRENCY_TYPE+"</n20:CurrencyType> "
						      + "<n21:MerchantCredentials i:type=\"d:string" +'"' +"  xmlns:n21=\"java:com.fss.upi.req\">"+getMerchantCredentials()+"</n21:MerchantCredentials> "
						      + "<n22:AppVersion i:type=\"d:string" +'"' +"  xmlns:n22=\"java:com.fss.upi.req\">"+getAppVersion()+"</n22:AppVersion>"
						      + "<n23:DeviceID i:type=\"d:string" +'"' +"  xmlns:n23=\"java:com.fss.upi.req\">"+getDeviceId()+"</n23:DeviceID>"
						      + "<n24:UserPwd i:type=\"d:string" +'"' +" xmlns:n24=\"java:com.fss.upi.req\">"+UPIConstants.TRANSACTION_PWD+"</n24:UserPwd>"
						      + "<n25:SubMerchantID i:type=\"d:string"+'"' +"  xmlns:n25=\"java:com.fss.upi.req\"/>"
						      + " </n0:UPI>"
						  + " </n0:req>"
						+ "</n0:MerchantTokenGen>" 
						+ "</v:Body>"  
						+ "</v:Envelope>";
		return MerchantTokenGen;
	}
	
	public static void main(String[] args) throws Exception {
		MerchantTokenGenRequest dto = new MerchantTokenGenRequest();
		AESAlgorithm aesAlgorithm = new AESAlgorithm();
		String refId = CommonUtil.merchantRefNo();
		System.err.println(" Ref ID : :  " + refId);
//		String decryptedDEK = aesAlgorithm.decryptDEK(UPIConstants.PLAN_DEK, UPIConstants.KEK);
		dto.setRefId(refId);
		dto.setMsgId(CommonUtil.generateVJB32digitToken());
		dto.setMobileNo(UPIConstants.PAYEE_MOBILE_NUM);
		dto.setTimeStamp(CommonUtil.merchantRefNo());
		dto.setChennel(UPIConstants.SDK_CHENNEL);
		dto.setDeviceId("");
		dto.setAppVersion(UPIConstants.SDK_APP_VERSION);
		
//		dto.setMerchantCredentials(aesAlgorithm.generateMerchantCredential(decryptedDEK, refId + "#" + UPIConstants.TRANSACTION_PWD));
		
		
		 System.out.println(dto.getRequest());
//		 System.err.println(dto.buildRequest());
		 
		 
		 
		 
		 Client restClient = UPIConstants.createClient();
		 WebResource webResource = restClient.resource(UPIConstants.MERCHANT_MOBILE_SERVICE_URL_TEST);
			restClient.addFilter(new LoggingFilter(System.out));
			ClientResponse resp = webResource.type("application/xml").post(ClientResponse.class, dto.getRequest());
			String output = resp.getEntity(String.class);
//			System.out.println("response: " + output);
		System.out.println("timestamp : " + CommonUtil.merchantRefNo());
		 
	}

	@Override
	public String getBescomUpiRequest() {
		
		String MerchantTokenGen = "<v:Envelope "
				 + " xmlns:i=\"http://www.w3.org/2001/XMLSchemainstance" +'"'
				 + " xmlns:d=\"http://www.w3.org/2001/XMLSchema" +'"'
				 + " xmlns:c=\"http://schemas.xmlsoap.org/soap/encoding/ " +'"'
				 + " xmlns:v=\"http://schemas.xmlsoap.org/soap/envelope/\"> "
				 + "<v:Header/> "  
				 + "<v:Body> "  
				+"<n0:MerchantTokenGen c:root=\"1\" id=\"o0" +'"'
				+ " xmlns:n0=\"http://com/fss/upi" +'"'+ " xmlns:c=\"http://schemas.xmlsoap.org/soap/encoding/\"> "
						+ "<n0:req i:type=\"n0:req" +'"'+" xmlns:i=\"http://www.w3.org/1999/XMLSchema-instance\"> "
						      + "<n0:UPI i:type=\"n0:UPI\">"
						      + "<n1:TokenCode i:type=\"d:string " +'"' +" xmlns:n1=\"java:com.fss.upi.req\">"+UPIBescomContstant.TOKEN_CODE+"</n1:TokenCode> "
						      + "<n2:PackageName i:type=\"d:string " +'"' +" xmlns:n2=\"java:com.fss.upi.req\">"+UPIBescomContstant.PACKGE_NAME+"</n2:PackageName> "
						      + "<n3:BankId i:type=\"d:string" +'"' +" xmlns:n3=\"java:com.fss.upi.req\">"+UPIBescomContstant.BANK_ID+"</n3:BankId> "
						      + "<n4:MerchantID i:type=\"d:string" +'"' +" xmlns:n4=\"java:com.fss.upi.req\">"+getMerchantId()+"</n4:MerchantID> "
						      + "<n5:PayeeType i:type=\"d:string" +'"' +"  xmlns:n5=\"java:com.fss.upi.req\">"+UPIBescomContstant.PAYEE_TYPE+"</n5:PayeeType> "
						      + "<n6:OrgId i:type=\"d:string" +'"' +" xmlns:n6=\"java:com.fss.upi.req\">"+getOrgId()+"</n6:OrgId> "
						      + "<n7:TerminalID i:type=\"d:string" +'"' +" xmlns:n7=\"java:com.fss.upi.req\">"+getTerminalId()+"</n7:TerminalID> "
						      + "<n8:PayerType i:type=\"d:string"+'"' +"  xmlns:n8=\"java:com.fss.upi.req\">"+UPIBescomContstant.PAYER_TYPE+"</n8:PayerType> "
						      + "<n9:PayerCode i:type=\"d:string" +'"' +" xmlns:n9=\"java:com.fss.upi.req\">"+UPIBescomContstant.PAYER_CODE+"</n9:PayerCode> "
						      + "<n10:Channel i:type=\"d:string" +'"' +"  xmlns:n10=\"java:com.fss.upi.req\">"+UPIBescomContstant.CHENNEL+"</n10:Channel> "
						      + "<n11:RefId i:type=\"d:string" +'"' +" xmlns:n11=\"java:com.fss.upi.req\">"+getRefId()+"</n11:RefId> "
						      + "<n12:SdkVersion i:type=\"d:string" +'"' +"  xmlns:n12=\"java:com.fss.upi.req\">"+UPIBescomContstant.SDK_VERSION+"</n12:SdkVersion> "
						      + "<n13:MsgId i:type=\"d:string" +'"' +"  xmlns:n13=\"java:com.fss.upi.req\">"+getMsgId()+"</n13:MsgId> "
						      + "<n14:TimeStamp i:type=\"d:string" +'"' +"  xmlns:n14=\"java:com.fss.upi.req\">"+getTimeStamp()+"</n14:TimeStamp> "
						      + "<n15:UserID i:type=\"d:string" +'"' +"  xmlns:n15=\"java:com.fss.upi.req\">"+UPIBescomContstant.USER_ID+"</n15:UserID> "
						      + "<n16:MobileNo i:type=\"d:string" +'"' +"  xmlns:n16=\"java:com.fss.upi.req\">"+getMobileNo()+"</n16:MobileNo> "
						      + "<n17:SdkName i:type=\"d:string" +'"' +"  xmlns:n17=\"java:com.fss.upi.req\">"+UPIBescomContstant.SKD_NAME+"</n17:SdkName> "
						      + "<n18:Remarks i:type=\"d:string"+'"' +"  xmlns:n18=\"java:com.fss.upi.req\"/> "
						      + "<n19:PayeeCode i:type=\"d:string" +'"' +"  xmlns:n19=\"java:com.fss.upi.req\">"+UPIBescomContstant.PAYEE_CODE+"</n19:PayeeCode> "
						      + "<n20:CurrencyType i:type=\"d:string" +'"' +"  xmlns:n20=\"java:com.fss.upi.req\">"+UPIBescomContstant.CURRENCY_TYPE+"</n20:CurrencyType> "
						      + "<n21:MerchantCredentials i:type=\"d:string" +'"' +"  xmlns:n21=\"java:com.fss.upi.req\">"+getMerchantCredentials()+"</n21:MerchantCredentials> "
						      + "<n22:AppVersion i:type=\"d:string" +'"' +"  xmlns:n22=\"java:com.fss.upi.req\">1.0</n22:AppVersion>"
						      + "<n23:DeviceID i:type=\"d:string" +'"' +"  xmlns:n23=\"java:com.fss.upi.req\">"+UPIBescomContstant.DEVICE_ID+"</n23:DeviceID>"
						      + "<n24:UserPwd i:type=\"d:string" +'"' +" xmlns:n24=\"java:com.fss.upi.req\">"+getTxnPwd()+"</n24:UserPwd>"
						      + "<n25:SubMerchantID i:type=\"d:string"+'"' +"  xmlns:n25=\"java:com.fss.upi.req\"/>"
						      + " </n0:UPI>"
						  + " </n0:req>"
						+ "</n0:MerchantTokenGen>" 
						+ "</v:Body>"  
						+ "</v:Envelope>";
		return MerchantTokenGen;
	}
	

}
