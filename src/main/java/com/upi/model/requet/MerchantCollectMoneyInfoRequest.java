package com.upi.model.requet;

import javax.xml.soap.SOAPMessage;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.upi.util.DesEncryption;
import com.upi.util.UPIConstants;
import com.upi.util.UPIRSAEncryption;

public class MerchantCollectMoneyInfoRequest implements SOAPRequest {

	private String metaData;
	private String metaInfo;
	private String msgId;

	public String getMsgId() {
		return msgId;
	}

	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

	public String getMetaData() {
		return metaData;
	}

	public void setMetaData(String metaData) {
		this.metaData = metaData;
	}

	public String getMetaInfo() {
		return metaInfo;
	}

	public void setMetaInfo(String metaInfo) {
		this.metaInfo = metaInfo;
	}

	@Override
	public SOAPMessage buildRequest() {
		return null;
	}

	@Override
	public String getRequest() {
		String MerchantTokenValid = "<v:Envelope "
				 + " xmlns:i=\"http://www.w3.org/2001/XMLSchemainstance" +'"'
				 + " xmlns:d=\"http://www.w3.org/2001/XMLSchema" +'"'
				 + " xmlns:c=\"http://schemas.xmlsoap.org/soap/encoding/" +'"'
				 + " xmlns:v=\"http://schemas.xmlsoap.org/soap/envelope/\"> "
				 + "<v:Header/> "  
				 + "<v:Body> "  
					+"<MerchantCollectMoney c:root=\"1\" id=\"o0" +'"'+ " xmlns:n0=\"http://com/fss/upi\"> "
						+ "<req> "
							+ " <MetaData i:type=\"d:string\">"+getMetaData()+"</MetaData> "
							+ " <MetaInfo i:type=\"d:string\">"+getMetaInfo()+"</MetaInfo> "
						+ "</req> "
					+ "</MerchantCollectMoney>" 
				+ "</v:Body>"  
				+ "</v:Envelope>";	
		return MerchantTokenValid;
	}
	
	public static void main(String[] args) throws Exception {
		
		MerchantCollectMoneyRequest validMerchant = new MerchantCollectMoneyRequest();
		validMerchant.setRefId("123456700002");
		validMerchant.setMsgId("VJBF99421A6C49D19B9754494EEF013P");
		validMerchant.setTimeStamp("140116155016");
		validMerchant.setMobileNo("8050581012");
		validMerchant.setMerchantCredentials("FPamA38dhZUslno7CG5+g+kHTDAdjiC16dH3ok0NruA=");
		validMerchant.setPayerVirAddr("testvpa@vijb");
		validMerchant.setAmount(10);
		 System.out.println("Request: " + validMerchant.getRequest());
		
		MerchantCollectMoneyInfoRequest dto = new MerchantCollectMoneyInfoRequest();
		dto.setMetaData(DesEncryption.desEncrypt(validMerchant.getRequest(), validMerchant.getMsgId()));
		dto.setMetaInfo(UPIRSAEncryption.encryptData(validMerchant.getMsgId(), UPIConstants.PUBLIK_KEY));
		System.out.println("Request: " + dto.getRequest());
		Client restClient = UPIConstants.createClient();
		WebResource webResource = restClient.resource(UPIConstants.MERCHANT_SERVICE_URL);
		restClient.addFilter(new LoggingFilter(System.out));
		ClientResponse resp = webResource.type("application/xml").post(ClientResponse.class, dto.getRequest());
		String output = resp.getEntity(String.class);
		System.out.println("response: " + output);
	}

	@Override
	public String getBescomUpiRequest() {
		String MerchantTokenValid = "<v:Envelope "
				 + " xmlns:i=\"http://www.w3.org/2001/XMLSchemainstance" +'"'
				 + " xmlns:d=\"http://www.w3.org/2001/XMLSchema" +'"'
				 + " xmlns:c=\"http://schemas.xmlsoap.org/soap/encoding/" +'"'
				 + " xmlns:v=\"http://schemas.xmlsoap.org/soap/envelope/\"> "
				 + "<v:Header/> "  
				 + "<v:Body> "  
					+"<MerchantCollectMoney c:root=\"1\" id=\"o0" +'"'+ " xmlns:n0=\"http://com/fss/upi\"> "
						+ "<req> "
							+ " <MetaData i:type=\"d:string\">"+getMetaData()+"</MetaData> "
							+ " <MetaInfo i:type=\"d:string\">"+getMetaInfo()+"</MetaInfo> "
						+ "</req> "
					+ "</MerchantCollectMoney>" 
				+ "</v:Body>"  
				+ "</v:Envelope>";	
		return MerchantTokenValid;
	}

}
