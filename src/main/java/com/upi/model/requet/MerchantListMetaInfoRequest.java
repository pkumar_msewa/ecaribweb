package com.upi.model.requet;

import javax.xml.soap.SOAPMessage;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.upi.util.DesEncryption;
import com.upi.util.UPIConstants;
import com.upi.util.UPIRSAEncryption;

public class MerchantListMetaInfoRequest implements SOAPRequest {

	private String metaData;
	private String metaInfo;

	public String getMetaData() {
		return metaData;
	}

	public void setMetaData(String metaData) {
		this.metaData = metaData;
	}

	public String getMetaInfo() {
		return metaInfo;
	}

	public void setMetaInfo(String metaInfo) {
		this.metaInfo = metaInfo;
	}

	@Override
	public SOAPMessage buildRequest() {
		return null;
	}

	@Override
	public String getRequest() {
		String MerchantCheckTxnStatus = "<v:Envelope "
				 + " xmlns:i=\"http://www.w3.org/2001/XMLSchemainstance" +'"'
				 + " xmlns:d=\"http://www.w3.org/2001/XMLSchema" +'"'
				 + " xmlns:c=\"http://schemas.xmlsoap.org/soap/encoding/" +'"'
				 + " xmlns:v=\"http://schemas.xmlsoap.org/soap/envelope/\"> "
				 + "<v:Header/> "  
				 + "<v:Body> "  
					+"<MerchantTxnList c:root=\"1\" id=\"o0" +'"'+ " xmlns:n0=\"http://com/fss/upi\"> "
						+ "<req> "
							+ " <MetaData i:type=\"d:string\">"+getMetaData()+"</MetaData> "
							+ " <MetaInfo i:type=\"d:string\">"+getMetaInfo()+"</MetaInfo> "
						+ "</req> "
					+ "</MerchantTxnList>" 
				+ "</v:Body>"  
				+ "</v:Envelope>";	
		return MerchantCheckTxnStatus;
	}
	
	public static void main(String[] args) throws Exception {
		
		MerchantTxnListRequest validMerchant = new MerchantTxnListRequest();	
		validMerchant.setRefId("123456000007");
		validMerchant.setMsgId("VJB17143176635889657992984375501407");
		validMerchant.setTimeStamp("140116155016");
		validMerchant.setMobileNo("8050581012");
//		validMerchant.setTxnId("VJB17143176635889657992984375501412");
		validMerchant.setMerchantCredentials("YNZYY0zDFYSKFgb6OTmi6iR+ty3cv3C4yvnb9wAamUc=");
		validMerchant.setCount("0");
		validMerchant.setStartDate("10-08-2017");
		validMerchant.setEndDate("23-08-2017");
		System.out.println("Request: " + validMerchant.getRequest());
		
		MerchantListMetaInfoRequest dto = new MerchantListMetaInfoRequest();	
		dto.setMetaData(DesEncryption.desEncrypt(validMerchant.getRequest(), validMerchant.getMsgId()));
		dto.setMetaInfo(UPIRSAEncryption.encryptData(validMerchant.getMsgId(), UPIConstants.PUBLIK_KEY));
		System.out.println("Request: " + dto.getRequest());
		Client restClient = UPIConstants.createClient();
		WebResource webResource = restClient.resource(UPIConstants.MERCHANT_SERVICE_URL);
		restClient.addFilter(new LoggingFilter(System.out));
		ClientResponse resp = webResource.type("application/xml").post(ClientResponse.class, dto.getRequest());
		String output = resp.getEntity(String.class);
		System.out.println("response: " + output);
	}

	@Override
	public String getBescomUpiRequest() {
		String MerchantCheckTxnStatus = "<v:Envelope "
				 + " xmlns:i=\"http://www.w3.org/2001/XMLSchemainstance" +'"'
				 + " xmlns:d=\"http://www.w3.org/2001/XMLSchema" +'"'
				 + " xmlns:c=\"http://schemas.xmlsoap.org/soap/encoding/" +'"'
				 + " xmlns:v=\"http://schemas.xmlsoap.org/soap/envelope/\"> "
				 + "<v:Header/> "  
				 + "<v:Body> "  
					+"<MerchantTxnList c:root=\"1\" id=\"o0" +'"'+ " xmlns:n0=\"http://com/fss/upi\"> "
						+ "<req> "
							+ " <MetaData i:type=\"d:string\">"+getMetaData()+"</MetaData> "
							+ " <MetaInfo i:type=\"d:string\">"+getMetaInfo()+"</MetaInfo> "
						+ "</req> "
					+ "</MerchantTxnList>" 
				+ "</v:Body>"  
				+ "</v:Envelope>";	
		return MerchantCheckTxnStatus;
	}

}
