package com.upi.model.requet;

import java.io.IOException;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

import com.payqwikweb.util.CommonUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.upi.util.UPIBescomContstant;
import com.upi.util.UPIConstants;

public class MerchantTokenValidRequest implements SOAPRequest{
	private String timeStamp;
	private String msgId;
	private String mobileNo;
	private String refId;
	private String merchantCredentials;
	private String token;
	private String deviceId;
	private String chennel;
	private boolean sdk;
	private String appVersion;
	private String txnPwd;
	private String merchantId;
	private String orgId;
	private String terminalId;

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getChennel() {
		return chennel;
	}

	public void setChennel(String chennel) {
		this.chennel = chennel;
	}

	public boolean isSdk() {
		return sdk;
	}

	public void setSdk(boolean sdk) {
		this.sdk = sdk;
	}

	public String getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}

	public String getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getMsgId() {
		return msgId;
	}

	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getRefId() {
		return refId;
	}

	public void setRefId(String refId) {
		this.refId = refId;
	}

	public String getMerchantCredentials() {
		return merchantCredentials;
	}

	public void setMerchantCredentials(String merchantCredentials) {
		this.merchantCredentials = merchantCredentials;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getTxnPwd() {
		return txnPwd;
	}

	public void setTxnPwd(String txnPwd) {
		this.txnPwd = txnPwd;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}

	@Override
	public SOAPMessage buildRequest() {
		SOAPMessage soapMessage = CommonUtil.createSOAPMessage();
		if (soapMessage != null) {
			SOAPPart soapPart = soapMessage.getSOAPPart();
			try {
				   SOAPEnvelope envelope = soapPart.getEnvelope();
				   envelope.addNamespaceDeclaration("i","http://www.w3.org/2001/XMLSchemainstance");
				   envelope.addNamespaceDeclaration("d","http://www.w3.org/2001/XMLSchema");
				   envelope.addNamespaceDeclaration("c","http://schemas.xmlsoap.org/soap/encoding/");
				   envelope.setPrefix("v");
				   SOAPHeader header = envelope.getHeader();
				   header.setPrefix("v");
				   SOAPBody body = envelope.getBody();
				   body.setPrefix("v");

				   SOAPBodyElement element = body.addBodyElement(new QName("http://com/fss/upi","MerchantTokenValid",""));
//				   element.addNamespaceDeclaration("c","http://schemas.xmlsoap.org/soap/encoding/");
				   element.setAttribute("id","o0");
				   element.addAttribute(new QName("","root","c"),"1");
				   SOAPElement req = element.addChildElement(new QName("","req",""));
//				   req.addNamespaceDeclaration("i","http://www.w3.org/1999/XMLSchema-instance");
				   req.addAttribute(new QName("","",""),"req");	

				   SOAPElement tokenCode = req.addChildElement(new QName("","TokenCode",""));
				   tokenCode.addAttribute(new QName("","type","i"),"TokenCode");
				   tokenCode.addTextNode(getTimeStamp());
				   
				   SOAPElement packageName = req.addChildElement(new QName("","PackageName",""));
				   packageName.addAttribute(new QName("","type","i"),"PackageName");
				   tokenCode.addTextNode(getTimeStamp());
				   
				   SOAPElement token = req.addChildElement(new QName("","Token",""));
				   token.addAttribute(new QName("","type","i"),"Token");
				   token.addTextNode(getTimeStamp());
				   
				   SOAPElement upi = req.addChildElement(new QName("","UPI",""));
				   upi.addAttribute(new QName("","",""),"UPI");
				   //FOR timeStamp
				   SOAPElement timeStamp = upi.addChildElement(new QName("","TimeStamp",""));
				   timeStamp.addAttribute(new QName("","type","i"),"d:string");
				   timeStamp.addTextNode(getTimeStamp());
				   //FOR MsgId
				   SOAPElement MsgId = upi.addChildElement(new QName("","MsgId",""));
				   MsgId.addAttribute(new QName("","type","i"),"d:string");
				   MsgId.addTextNode(getMsgId());
				   //FOR DeviceID
				   SOAPElement deviceID = upi.addChildElement(new QName("","DeviceID",""));
				   deviceID.addAttribute(new QName("","type","i"),"d:string");
				   deviceID.addTextNode(UPIBescomContstant.VPA_DEVICE_ID);
				   //FOR Channel
				   SOAPElement channel = upi.addChildElement(new QName("","Channel",""));
				   channel.addAttribute(new QName("","type","i"),"d:string");
				   channel.addTextNode(UPIBescomContstant.VPA_CHENNEL);
				   //FOR AppVersion
				   SOAPElement appVersion = upi.addChildElement(new QName("","AppVersion",""));
				   appVersion.addAttribute(new QName("","type","i"),"d:string");
				   appVersion.addTextNode(UPIBescomContstant.APP_VERSION);
				   //FOR PayerType
				   SOAPElement payerType = upi.addChildElement(new QName("","PayerType",""));
				   payerType.addAttribute(new QName("","type","i"),"d:string");
				   payerType.addTextNode(UPIBescomContstant.PAYER_TYPE);
				   //FOR OrgId
				   SOAPElement orgId = upi.addChildElement(new QName("","OrgId",""));
				   orgId.addAttribute(new QName("","type","i"),"d:string");
//				   orgId.addTextNode(UPIBescomContstant.ORG_ID);
				   //FOR BankId
				   SOAPElement bankId = upi.addChildElement(new QName("","BankId",""));
				   bankId.addAttribute(new QName("","type","i"),"d:string");
				   bankId.addTextNode(UPIBescomContstant.BANK_ID);
				   //FOR Remarks
				   SOAPElement remarks = upi.addChildElement(new QName("","Remarks",""));
				   remarks.addAttribute(new QName("","type","i"),"d:string");
//				   remarks.addTextNode("Dro8e647mo.stFBAs8ms");
				   //FOR MobileNo
				   SOAPElement mobileNo = upi.addChildElement(new QName("","MobileNo",""));
				   mobileNo.addAttribute(new QName("","type","i"),"d:string");
				   mobileNo.addTextNode(getMobileNo());
				   //FOR MerchantID
				   SOAPElement merchantID = upi.addChildElement(new QName("","MerchantID",""));
				   merchantID.addAttribute(new QName("","type","i"),"d:string");
//				   merchantID.addTextNode(UPIBescomContstant.MERCHANT_ID);
				   //FOR SubMerchantID
				   SOAPElement subMerchantID = upi.addChildElement(new QName("","SubMerchantID",""));
				   subMerchantID.addAttribute(new QName("","type","i"),"d:string");
				   subMerchantID.addTextNode(UPIBescomContstant.SUB_MERCHANT_ID);
				   //FOR TerminalID
				   SOAPElement terminalID = upi.addChildElement(new QName("","TerminalID",""));
				   terminalID.addAttribute(new QName("","type","i"),"d:string");
//				   terminalID.addTextNode(UPIBescomContstant.TERMINAL_ID);
				   //FOR MerchantCredentials
				   SOAPElement merchantCredentials = upi.addChildElement(new QName("","MerchantCredentials",""));
				   merchantCredentials.addAttribute(new QName("","type","i"),"d:string");
				   //FOR reqData
				   SOAPElement reqData = req.addChildElement(new QName("","reqData",""));
				 //FOR data
				   SOAPElement data = reqData.addChildElement(new QName("","data",""));
				   data.addAttribute(new QName("","type","i"),"d:string");
				   data.addTextNode(UPIBescomContstant.REQ_DATA);

				   envelope.removeNamespaceDeclaration("SOAP-ENV");
				   soapMessage.saveChanges();
				   soapMessage.writeTo(System.err);
				
				System.err.println();
			} catch (SOAPException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return soapMessage;
	}

	@Override
	public String getRequest() {
		
		String MerchantTokenValid = "<v:Envelope "
				 + " xmlns:i=\"http://www.w3.org/2001/XMLSchemainstance" +'"'
				 + " xmlns:d=\"http://www.w3.org/2001/XMLSchema" +'"'
				 + " xmlns:c=\"http://schemas.xmlsoap.org/soap/encoding/" +'"'
				 + " xmlns:v=\"http://schemas.xmlsoap.org/soap/envelope/\"> "
				 + "<v:Header/> "  
				 + "<v:Body> "  
				+"<n0:MerchantTokenValid c:root=\"1\" id=\"o0" +'"'+ " xmlns:n0=\"http://com/fss/upi\"> "
						+ "<req> "
						+ "<TokenCode i:type=\"d:string\">"+UPIConstants.TOKEN_CODE+"</TokenCode> "
						+ " <PackageName i:type=\"d:string\">"+UPIConstants.PACKGE_NAME+"</PackageName> "
						+ " <Token i:type=\"d:string\">"+getToken()+"</Token> "
						+ " <UPI> "
				           + " <BankId i:type=\"d:string\">"+UPIConstants.BANK_ID+"</BankId> "
				           + " <MerchantID i:type=\"d:string\">"+UPIConstants.MERCHANT_ID+"</MerchantID> "
				           + " <PayeeType i:type=\"d:string\">"+UPIConstants.PAYEE_TYPE+"</PayeeType> "
				           + " <OrgId i:type=\"d:string\">"+UPIConstants.ORG_ID+"</OrgId> "
				           + " <TerminalID i:type=\"d:string\">"+UPIConstants.TERMINAL_ID+"</TerminalID> "
				           + " <PayerType i:type=\"d:string\">"+UPIConstants.PAYER_TYPE+"</PayerType> "
				           + " <PayerCode i:type=\"d:string\">"+UPIConstants.PAYER_CODE+"</PayerCode> "
				           + " <Channel i:type=\"d:string\">"+getChennel()+"</Channel> "
				           + " <RefId i:type=\"d:string\">"+getRefId()+"</RefId> "
				           + " <SdkVersion i:type=\"d:string\">"+UPIConstants.SDK_VERSION+"</SdkVersion> "
				           + " <MsgId i:type=\"d:string\">"+getMsgId()+"</MsgId> "
				           + " <TimeStamp i:type=\"d:string\">"+getTimeStamp()+"</TimeStamp> "
				           + " <UserID i:type=\"d:string\">UserID</UserID> "
				           + " <MobileNo i:type=\"d:string\">"+UPIConstants.PAYEE_MOBILE_NUM+"</MobileNo> "
				           + " <SdkName i:type=\"d:string\">"+UPIConstants.SKD_NAME+"</SdkName> "
				           + " <Remarks i:type=\"d:string\"/> "
				           + " <PayeeCode i:type=\"d:string\">"+UPIConstants.PAYEE_CODE+"</PayeeCode> "
				           + " <CurrencyType i:type=\"d:string\">INR</CurrencyType> "
				           + " <MerchantCredentials i:type=\"d:string\">"+getMerchantCredentials()+"</MerchantCredentials> "
				           + " <AppVersion i:type=\"d:string\">"+getAppVersion()+"</AppVersion> "
				           + " <DeviceID i:type=\"d:string\">"+getDeviceId()+"</DeviceID> "
				           + " <UserPwd i:type=\"d:string\">"+UPIConstants.USER_PWD+"</UserPwd> "
				           + " <SubMerchantID i:type=\"d:string\"/> "
				          + "</UPI> "
				        + " </req> "
					+ "</n0:MerchantTokenValid>" 
					+ "</v:Body>"  
					+ "</v:Envelope>";
		return MerchantTokenValid;
	}
	
	/*public static void main(String[] args) throws Exception {
		MerchantTokenValidRequest dto = new MerchantTokenValidRequest();	
		dto.setRefId("123456712355");
		dto.setMsgId("VJBF99421A6C49D19B9754494EEF681M");
		dto.setTimeStamp("140116155016");
		dto.setMobileNo("7795221595");
		dto.setMerchantCredentials("qkdshc3X1yioSKmepYs52a6ClsVE0sEZ+7GVZcY9XH4=");
		dto.setToken("zSEoXE1y65mFn6Vxzf9saI9Fzlptdlgf2fs0SMRZJ2Q54Dry/XGG6h9ZI728Of5M");
		 System.out.println("Request: " + dto.getRequest());
		 
		 
		 Client restClient = UPIBescomContstant.createClient();
		 WebResource webResource = restClient.resource(UPIBescomContstant.MERCHANT_SERVICE_URL);
			restClient.addFilter(new LoggingFilter(System.out));
			ClientResponse resp = webResource.type("application/xml").post(ClientResponse.class, dto.getRequest());
			String output = resp.getEntity(String.class);
			System.out.println("response: " + output);
		 
//		 	DesEncryption des = new DesEncryption();
//			String data = dto.getRequest();
//			System.out.println(data);
//			String key = "VJBF99421A6C49D19B9754494EEF680M";
//			try {
//				String  desEncrypt =  des.desEncrypt(data, key);
//				System.out.println("DesEncrypt :: " + desEncrypt);
//			} catch (Exception e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		 
	}*/

	@Override
	public String getBescomUpiRequest() {
		
		String MerchantTokenValid = "<v:Envelope "
				 + " xmlns:i=\"http://www.w3.org/2001/XMLSchemainstance" +'"'
				 + " xmlns:d=\"http://www.w3.org/2001/XMLSchema" +'"'
				 + " xmlns:c=\"http://schemas.xmlsoap.org/soap/encoding/" +'"'
				 + " xmlns:v=\"http://schemas.xmlsoap.org/soap/envelope/\"> "
				 + "<v:Header/> "  
				 + "<v:Body> "  
				+"<n0:MerchantTokenValid c:root=\"1\" id=\"o0" +'"'+ " xmlns:n0=\"http://com/fss/upi\"> "
						+ "<req> "
						+ "<TokenCode i:type=\"d:string\">"+UPIBescomContstant.TOKEN_CODE+"</TokenCode> "
						+ " <PackageName i:type=\"d:string\">"+UPIBescomContstant.PACKGE_NAME+"</PackageName> "
						+ " <Token i:type=\"d:string\">"+getToken()+"</Token> "
						+ " <UPI> "
				           + " <BankId i:type=\"d:string\">"+UPIBescomContstant.BANK_ID+"</BankId> "
				           + " <MerchantID i:type=\"d:string\">"+getMerchantId()+"</MerchantID> "
				           + " <PayeeType i:type=\"d:string\">"+UPIBescomContstant.PAYEE_TYPE+"</PayeeType> "
				           + " <OrgId i:type=\"d:string\">"+getOrgId()+"</OrgId> "
				           + " <TerminalID i:type=\"d:string\">"+getTerminalId()+"</TerminalID> "
				           + " <PayerType i:type=\"d:string\">"+UPIBescomContstant.PAYER_TYPE+"</PayerType> "
				           + " <PayerCode i:type=\"d:string\">"+UPIBescomContstant.PAYER_CODE+"</PayerCode> "
				           + " <Channel i:type=\"d:string\">"+UPIBescomContstant.CHENNEL+"</Channel> "
				           + " <RefId i:type=\"d:string\">"+getRefId()+"</RefId> "
				           + " <SdkVersion i:type=\"d:string\">"+UPIBescomContstant.SDK_VERSION+"</SdkVersion> "
				           + " <MsgId i:type=\"d:string\">"+getMsgId()+"</MsgId> "
				           + " <TimeStamp i:type=\"d:string\">"+getTimeStamp()+"</TimeStamp> "
				           + " <UserID i:type=\"d:string\">UserID</UserID> "
				           + " <MobileNo i:type=\"d:string\">"+getMobileNo()+"</MobileNo> "
				           + " <SdkName i:type=\"d:string\">"+UPIBescomContstant.SKD_NAME+"</SdkName> "
				           + " <Remarks i:type=\"d:string\"/> "
				           + " <PayeeCode i:type=\"d:string\">"+UPIBescomContstant.PAYEE_CODE+"</PayeeCode> "
				           + " <CurrencyType i:type=\"d:string\">INR</CurrencyType> "
				           + " <MerchantCredentials i:type=\"d:string\">"+getMerchantCredentials()+"</MerchantCredentials> "
				           + " <AppVersion i:type=\"d:string\">"+UPIBescomContstant.APP_VERSION+"</AppVersion> "
				           + " <DeviceID i:type=\"d:string\">"+UPIBescomContstant.DEVICE_ID+"</DeviceID> "
				           + " <UserPwd i:type=\"d:string\">"+getTxnPwd()+"</UserPwd> "
				           + " <SubMerchantID i:type=\"d:string\"/> "
				          + "</UPI> "
				        + " </req> "
					+ "</n0:MerchantTokenValid>" 
					+ "</v:Body>"  
					+ "</v:Envelope>";
		return MerchantTokenValid;
	}
}
