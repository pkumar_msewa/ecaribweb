//@XmlSchema(
//	    elementFormDefault=XmlNsForm.QUALIFIED,
//	    namespace="http://www.example.com/FOO",
//	    xmlns={@XmlNs(prefix="bar", 
//	                  namespaceURI="http://www.example.com/BAR")}
//	)

package com.upi.model.requet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="java:Resp",namespace="java:com.fss.upi.rsp")
@XmlAccessorType(XmlAccessType.FIELD)
public class MerchantApprovedResponse{
	
	@XmlElement(name="java:OrgTxnId",namespace="java:com.fss.upi.rsp")
	private String orgTxnId;
	
	@XmlElement(name="java:TxnId",namespace="java:com.fss.upi.rsp")
	private String txnId;
	
	@XmlElement(name="java:OrgTxnRefId",namespace="java:com.fss.upi.rsp")
	private String orgTxnRefId;
	
	@XmlElement(name="java:ResCode",namespace="java:com.fss.upi.rsp")
	private String respCode;
	
	@XmlElement(name="java:ResDesc",namespace="java:com.fss.upi.rsp")
	private String respDesc;
	
	@XmlElement(name="java:TimeStamp",namespace="java:com.fss.upi.rsp")
	private String timeStamp;

	public String getRespCode() {
		return respCode;
	}

	public void setRespCode(String respCode) {
		this.respCode = respCode;
	}

	public String getRespDesc() {
		return respDesc;
	}

	public void setRespDesc(String respDesc) {
		this.respDesc = respDesc;
	}

	public String getOrgTxnId() {
		return orgTxnId;
	}

	public void setOrgTxnId(String orgTxnId) {
		this.orgTxnId = orgTxnId;
	}

	public String getTxnId() {
		return txnId;
	}

	public void setTxnId(String txnId) {
		this.txnId = txnId;
	}

	public String getOrgTxnRefId() {
		return orgTxnRefId;
	}

	public void setOrgTxnRefId(String orgTxnRefId) {
		this.orgTxnRefId = orgTxnRefId;
	}

	public String getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

//	@Override
//	public SOAPMessage buildRequest() {
//		SOAPMessage soapMessage = CommonUtil.createSOAPMessage();
//		return soapMessage;
//		
//	}

//	@Override
	public String getRequest() {
		String apprResp = "<java:Resp " +"xmlns:java=\"java:com.fss.upi.rsp\"> "
							+" <java:OrgTxnId>"+getOrgTxnId()+"</java:OrgTxnId>"
							+" <java:TxnId>"+getTxnId()+"</java:TxnId>"
							+" <java:OrgTxnRefId>"+getOrgTxnRefId()+"</java:OrgTxnRefId>"
							+" <java:ResCode>"+getRespCode()+"</java:ResCode>"
							+" <java:ResDesc>"+getRespDesc()+"</java:ResDesc>"
							+" <java:TimeStamp>"+System.currentTimeMillis()+"</java:TimeStamp>"
						+" </java:Resp>";
		
		return apprResp;
	}

}
