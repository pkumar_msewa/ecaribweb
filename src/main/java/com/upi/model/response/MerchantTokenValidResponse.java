package com.upi.model.response;

import com.payqwikweb.app.model.ResponseStatus;

public class MerchantTokenValidResponse {

	private String code;
	private String status;
	private String message;
	private String msgId;
	private String desEncryptedKey;
	private String token;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(ResponseStatus status) {
		this.status = status.getKey();
		this.code = status.getValue();
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMsgId() {
		return msgId;
	}

	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

	public String getDesEncryptedKey() {
		return desEncryptedKey;
	}

	public void setDesEncryptedKey(String desEncryptedKey) {
		this.desEncryptedKey = desEncryptedKey;
	}
}
