package com.upi.model.response;

import com.payqwikweb.app.model.ResponseStatus;

public class GenerateMerchantDEKResponse {
	private boolean success;
	
//	private String msgID;
	private String code;
	private String status;
	private String message;
	private String encryptedDEK;

	public String getEncryptedDEK() {
		return encryptedDEK;
	}

	public void setEncryptedDEK(String encryptedDEK) {
		this.encryptedDEK = encryptedDEK;
	}

	/*public String getMsgID() {
		return msgID;
	}

	public void setMsgID(String msgID) {
		this.msgID = msgID;
	}
*/
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(ResponseStatus status) {
		this.status = status.getKey();
		this.code = status.getValue();
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}
}
