package com.payqwikweb.test;

import java.io.StringWriter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author skargopolov
 */
public class PersonXMLDocument {
	public static void main(String[] args) {
		String personXMLStringValue = null;
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.newDocument();
			// Create Person root element
			Element PaymentRequest = doc.createElement("PaymentRequest");
			doc.appendChild(PaymentRequest);
			
			Element Transaction = doc.createElement("Transaction");
			PaymentRequest.appendChild(Transaction);
			
			// Create TranType Element
			Element TranType = doc.createElement("TranType");
			TranType.appendChild(doc.createTextNode("Sergey"));
			Transaction.appendChild(TranType);
			
			// Create CustomerRefNum Element
			Element CustomerRefNum = doc.createElement("CustomerRefNum");
			CustomerRefNum.appendChild(doc.createTextNode("Kargopolov"));
			Transaction.appendChild(CustomerRefNum);
			
			
			// Transform Document to XML String
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			StringWriter writer = new StringWriter();
			transformer.transform(new DOMSource(doc), new StreamResult(writer));
			// Get the String value of final xml document
			personXMLStringValue = writer.getBuffer().toString();
		} catch (ParserConfigurationException | TransformerException e) {
			e.printStackTrace();
		}
		System.out.println(personXMLStringValue);
	}
}
