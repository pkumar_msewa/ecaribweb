package com.payqwikweb.test;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.CDATASection;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.payqwikweb.util.CommonUtil;

public class CDataXmlReq {

	public static void main(String[] args) throws Exception {
		SOAPConnection connection = CommonUtil.getNewConnection();
		CommonUtil.doTrustToCertificates();
		SOAPMessage resp = connection.call(getSoapApi(), "https://ibluatapig.indusind.com/app/uat/IdirectPayService");
		resp.writeTo(System.err);
		// Parsing Response
//		<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Body><ProcessTxnInXmlResponse xmlns="http://tempuri.org/"><ProcessTxnInXmlResult>&lt;PaymentResponse>&lt;Transaction>&lt;IBLRefNo>AA01000000215581&lt;/IBLRefNo>&lt;CustomerRefNo>81658729&lt;/CustomerRefNo>&lt;Amount>100&lt;/Amount>&lt;StatusCode>R000&lt;/StatusCode>&lt;StatusDesc>Success&lt;/StatusDesc>&lt;/Transaction>&lt;/PaymentResponse></ProcessTxnInXmlResult></ProcessTxnInXmlResponse></s:Body></s:Envelope>IBLRefNO : : AA01000000215581
		SOAPBody body = resp.getSOAPBody();
		NodeList returnList = body.getElementsByTagName("ProcessTxnInXmlResult");
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		InputSource is = new InputSource(new StringReader(returnList.item(0).getTextContent()));
		org.w3c.dom.Document doc = builder.parse(is);
		NodeList nodeList = doc.getElementsByTagName("Transaction");
		 for (int i = 0; i < nodeList.getLength(); i++) {
			 Element element = (Element) nodeList.item(i);
				String iblRefNo = element.getElementsByTagName("IBLRefNo").item(0).getFirstChild().getTextContent();
				System.out.println("IBLRefNO : : " + iblRefNo);
				String customerRefNo = element.getElementsByTagName("CustomerRefNo").item(0).getFirstChild().getTextContent();
				System.out.println("CustomerRefNo : : " + customerRefNo);
				String amount = element.getElementsByTagName("Amount").item(0).getFirstChild().getTextContent();
				System.out.println("Amount : : " + amount);
				String statusCode = element.getElementsByTagName("StatusCode").item(0).getFirstChild().getTextContent();
				System.out.println("StatusCode : : " + statusCode);
				String statusDesc = element.getElementsByTagName("StatusDesc").item(0).getFirstChild().getTextContent();
				System.out.println("StatusDesc : : " + statusDesc);
		 }

	}

	private static SOAPMessage getSoapApi() {
		SOAPMessage soapMessage = CommonUtil.createSOAPMessage();
		if (soapMessage != null) {
			soapMessage.getMimeHeaders().addHeader("Content-Type", "text/xml");
			soapMessage.getMimeHeaders().addHeader("X-IBM-Client-Secret", "Q2sH4eN1iJ4fH5tM2vU5nB8bS2nE2rM4vT3iN6mE1hS3jX6vS2");
			soapMessage.getMimeHeaders().addHeader("X-IBM-Client-Id", "c4155000-0b24-46c4-82ea-9fdd93def051");
			soapMessage.getMimeHeaders().addHeader("SOAPAction", "http://tempuri.org/IPayService/ProcessTxnInXml");
			SOAPPart soapPart = soapMessage.getSOAPPart();
			try {
				SOAPEnvelope envelope = soapPart.getEnvelope();
				envelope.addNamespaceDeclaration("soapenv", "http://schemas.xmlsoap.org/soap/envelope");
				envelope.addNamespaceDeclaration("tem", "http://tempuri.org/");
				envelope.setPrefix("soapenv");
				SOAPHeader header = envelope.getHeader();
				header.setPrefix("soapenv");
				SOAPBody body = envelope.getBody();
				body.setPrefix("soapenv");
				SOAPBodyElement element = body.addBodyElement(new QName("", "ProcessTxnInXml", "tem"));
				// // FOR StrCustId
				SOAPElement strCustId = element.addChildElement(new QName("http://tempuri.org/", "strCustId", "tem"));
				strCustId.addTextNode("11255822");
				// //FOR StrInput
				SOAPElement strInputTxn = element
						.addChildElement(new QName("http://tempuri.org/", "strInputTxn", "tem"));
				// Build CDATA
				CDATASection cdata = strInputTxn.getOwnerDocument().createCDATASection(getXml());
				strInputTxn.appendChild(cdata);
				envelope.removeNamespaceDeclaration("SOAP-ENV");
				soapMessage.saveChanges();
				soapMessage.writeTo(System.err);
			} catch (SOAPException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return soapMessage;
	}

	private static String getXml() {
		String personXMLStringValue = null;
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder;
		try {
			dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.newDocument();
			Element rootElement = doc.createElement("PaymentRequest");
			doc.appendChild(rootElement);
			rootElement.appendChild(getEmployee(doc));
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			StringWriter writer = new StringWriter();
			transformer.transform(new DOMSource(doc), new StreamResult(writer));
			personXMLStringValue = writer.getBuffer().toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return personXMLStringValue;
	}

	private static Node getEmployee(Document doc) {
		Element employee = doc.createElement("Transaction");
		employee.appendChild(getEmployeeElements(doc, employee, "TranType", "IMPS"));
		employee.appendChild(getEmployeeElements(doc, employee, "CustomerRefNum", "81658729"));
		employee.appendChild(getEmployeeElements(doc, employee, "DebitAccount", "100002076734"));
		employee.appendChild(getEmployeeElements(doc, employee, "Amount", "100"));
		employee.appendChild(getEmployeeElements(doc, employee, "ValueDate", "21-03-2018"));
		employee.appendChild(getEmployeeElements(doc, employee, "BenName", "SURAJ"));
		employee.appendChild(getEmployeeElements(doc, employee, "BENE_ACNO", "22331122"));
		employee.appendChild(getEmployeeElements(doc, employee, "BENE_IFSC_CODE", "SBIN0001222"));
		employee.appendChild(getEmployeeElements(doc, employee, "BENE_BRANCH", ""));
		employee.appendChild(getEmployeeElements(doc, employee, "BENE_BANK", ""));
		employee.appendChild(getEmployeeElements(doc, employee, "Bene_MobileNo", ""));
		employee.appendChild(getEmployeeElements(doc, employee, "Bene_EmailId", ""));
		employee.appendChild(getEmployeeElements(doc, employee, "Bene_MMId", ""));
		employee.appendChild(getEmployeeElements(doc, employee, "Rem_Name", ""));
		employee.appendChild(getEmployeeElements(doc, employee, "Rem_MobileNo", "7506378035"));
		employee.appendChild(getEmployeeElements(doc, employee, "Rem_Address1", ""));
		employee.appendChild(getEmployeeElements(doc, employee, "Rem_Address2", ""));
		employee.appendChild(getEmployeeElements(doc, employee, "Rem_PinNo", "400059"));
		employee.appendChild(getEmployeeElements(doc, employee, "Rem_KYC_Status", "0"));
		employee.appendChild(getEmployeeElements(doc, employee, "CustomerId", "7506378035"));
		employee.appendChild(getEmployeeElements(doc, employee, "CustomerName", "AshishMore"));
		employee.appendChild(getEmployeeElements(doc, employee, "MakerId", "vijaytel"));
		employee.appendChild(getEmployeeElements(doc, employee, "CheckerId", "VIJAYISUP"));
		employee.appendChild(getEmployeeElements(doc, employee, "AgentID", "9401241829471214"));
		employee.appendChild(getEmployeeElements(doc, employee, "Reserve1", ""));
		employee.appendChild(getEmployeeElements(doc, employee, "Reserve2", ""));
		employee.appendChild(getEmployeeElements(doc, employee, "Reserve3", ""));
		employee.appendChild(getEmployeeElements(doc, employee, "Reserve4", ""));
		employee.appendChild(getEmployeeElements(doc, employee, "Reserve5", ""));
		return employee;
	}

	// utility method to create text node
	private static Node getEmployeeElements(Document doc, Element element, String name, String value) {
		Element node = doc.createElement(name);
		node.appendChild(doc.createTextNode(value));
		return node;
	}
}
