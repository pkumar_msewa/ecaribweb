package com.payqwikweb.test;


	import java.io.BufferedReader;
	import java.io.FileWriter;
	import java.io.IOException;
	import java.io.InputStreamReader;
	import java.net.URL;
	import java.net.URLConnection;
	import java.nio.charset.Charset;
	import java.text.ParseException;
	import java.util.regex.Matcher;
	import java.util.regex.Pattern;

	import org.json.JSONArray;
	import org.json.JSONException;
	import org.json.JSONObject;

	import com.csvreader.CsvWriter;




	public class telcosplans {

		
		private static final String FILE_PATH ="D:\\my fol\\mp.csv";
		
		public static void main(String[] args) throws ParseException,
				org.codehaus.jettison.json.JSONException, IOException {
			
			String [] circles={
				"Orissa","Andhra%20Pradesh","Assam","Bihar%20Jharkhand","Chennai","Gujarat","Haryana","Himachal%20Pradesh","Jammu%20Kashmir","Karnataka","Kerala","Kolkata","Madhya%20Pradesh%20Chhattisgarh","Maharashtra","North%20East","Rajasthan","Punjab","Tamil%20Nadu","West%20Bengal","UP%20East","UP%20West"};
					
			String [] operators={
					"Aircel","BSNL","Airtel","Idea","MTNL","Reliance%20GSM","MTS","Tata%20Docomo","Vodafone","Videocon","T24","Uninor","Reliance%20Mobile","Jio"};	
			//String [] operators={"Aircel"};
			
			for(int i=0;i<circles.length;i++){
				for(int j=0;j<operators.length;j++){
					String plansUrl = getPlansUrl(operators[j], circles[i]);
					StringBuilder sb = new StringBuilder();
					URLConnection urlConn = null;
					InputStreamReader in = null;
					try {
						URL url = new URL(plansUrl);
						urlConn = url.openConnection();
						if (urlConn != null)
							urlConn.setReadTimeout(6000 * 1000);
						if (urlConn != null && urlConn.getInputStream() != null) {
							in = new InputStreamReader(urlConn.getInputStream(),
									Charset.defaultCharset());
							BufferedReader bufferedReader = new BufferedReader(in);
							if (bufferedReader != null) {
								int cp;
								while ((cp = bufferedReader.read()) != -1) {
									sb.append((char) cp);
								}
								bufferedReader.close();
							}
						}
						in.close();
					} catch (Exception e) {
						throw new RuntimeException("Exception while calling URL:"+ plansUrl, e);
					}

					String responseString = sb.toString();
					System.err.println("string ::" + responseString);
					String jsonResponse = getJSONFromResponse(responseString);
					System.out.println("START :: " + jsonResponse + " END");
					writePlansToExcelFromJSONString(responseString);
				}
			}
		}

		public static String getPlansUrl(String operator, String circle) {
			
		
			
//		String url = "https://catalog.paytm.com/v1/g/recharge-plans/mobile/topup?operator="+ operator+ "&circle="+ circle+ "&type=mobile&description=1&page_count=1&items_per_page=500&sort_price=0&callback=angular.callbacks._h&channel=web&version=2";
	String url = "https://catalog.paytm.com/v1/g/recharge-plans/mobile/roaming?channel=web&child_site_id=1&site_id=1&version=1&operator="+operator+"&circle="+circle+"&type=mobile&description=1&page_count=1&items_per_page=30&sort_price=0";
		//	String url =https://catalog.paytm.com/v1/g/recharge-plans/mobile/exclusive-offer?channel=web&version=2&child_site_id=1&site_id=1&operator=Idea&type=mobile&description=1&page_count=1&items_per_page=30&circle=Madhya%20Pradesh%20Chhattisgarh
		return url;
		}

		public static String getJSONFromResponse(String response) {
			Matcher m = Pattern.compile("\\(([^)]+)\\)").matcher(response);
			while (m.find()) {
				return m.group(1);
			}
			return null;
		}

		public static void writePlansToExcelFromJSONString(String jsonString) throws IOException {
			// TODO JSON Parsing
			
			String actual= "";
			String desc = "";
			System.out.println("JSON :: " + jsonString + " END");
			try {
				JSONObject object = new JSONObject(jsonString);
				if (object != null) {
					JSONArray gridLayout = object.getJSONArray("grid_layout");
					if (gridLayout != null) {
						int gridLayoutLength = gridLayout.length();
						for (int i = 0; i < gridLayoutLength; i++) {
							JSONObject gridLayoutItem = (JSONObject) gridLayout
									.get(i);
							
							actual = gridLayoutItem.optString("actual_price");
							
							System.out.println(" acutal ----------------------> " + actual);
									
							
							if (gridLayoutItem != null) {
								JSONArray longRichDesc = gridLayoutItem
										.getJSONArray("long_rich_desc");
								
								int longRichDescLength = longRichDesc.length();
								for (int j = 0; j < longRichDescLength; j++) {
								
									
									JSONObject longRichDescItem = (JSONObject) longRichDesc.get(j);
									 desc = longRichDescItem.optString("description");
									if (longRichDescItem != null) {
										JSONObject attribute = longRichDescItem
												.getJSONObject("attributes");
										if (attribute != null) {
											
											String operator = attribute
													.getString("Operator");
											String circle = attribute
													.getString("Circle");
											String talktime = attribute
													.getString("Talktime");
											String validity = attribute
													.getString("Validity");
											String categoryName = attribute
													.getString("category_name");
											System.out.println(operator);
											System.out.println(circle);
											System.out.println(talktime);
											System.out.println(validity);
											System.out.println(categoryName);
											
											String citycode = "";
											
											String Mcode = "";
											String Ccode = "";
											
											String Ocode ="";
											
											//static String[] operators = { "Airtel","Aircel","BSNL","Idea","MTNL","MTS","Reliance%20CDMA","Reliance%20GSM","T24","Tata%20DOCOMO","Telenor","Videocon","Vodefone" };
													
											
											if(operator.equalsIgnoreCase("Reliance GSM")){
												
												Mcode = "RG";
												Ccode = "RBP";
												Ocode = "6";
											}
											if(operator.equalsIgnoreCase("Telenor")){
												Mcode = "UN";
												Ccode = "USP";
												Ocode = "13";
											}
											if(operator.equalsIgnoreCase("Reliance Mobile")){
												
												Mcode = "RM";
												Ccode = "RMP";
												
												Ocode = "18";
												
											}
											if(operator.equalsIgnoreCase("Tata Indicom")){
												
												Mcode = "TI";
												Ccode = "TIP";
												Ocode = "8";
												
											}
											
											
											if(operator.equalsIgnoreCase("Tata Docomo")){
												
												Mcode = "TD";
												Ccode = "TSP";
												
												Ocode = "88";
												
											}
											
											
											if(operator.equalsIgnoreCase("Airtel")){
												
												Mcode = "AT";
												Ccode = "ATP";
												
												Ocode = "2";
												
											}
												if(operator.equalsIgnoreCase("Aircel")){
												
												Mcode = "AC";
												Ccode = "ACP";
												Ocode = "1";
												
											}if(operator.equalsIgnoreCase("BSNL")){
												
												Mcode = "BS";
												Ccode = "BVP";
												Ocode = "3";
												
											}if(operator.equalsIgnoreCase("Idea")){
												
												Mcode = "ID";
												Ccode = "IDP";
												Ocode = "4";
												
											}if(operator.equalsIgnoreCase("MTNL")){
												
												Mcode = "MT";
												Ccode = "MMP";
												Ocode = "5";
												
											}if(operator.equalsIgnoreCase("MTS")){
												
												Mcode = "MS";
												Ccode = "MTP";
												Ocode = "7";
												
											}if(operator.equalsIgnoreCase("Videocon")){
												
												Mcode = "VC";
												Ccode = "VGP";
												Ocode = "11";
												
											}if(operator.equalsIgnoreCase("Vodafone")){
												
												Mcode = "VF";
												Ccode = "VFP";
												Ocode = "10";
												
											}if(operator.equalsIgnoreCase("T24")){
												
												Mcode = "T2";
												Ccode = "TVP";
												Ocode = "12";
												
											}
											if(operator.equalsIgnoreCase("Jio"))
											{
												Mcode="RJ";
												Ccode="RJP";
												Ocode="21";
											}

											
											
												if(circle.equalsIgnoreCase("Chennai")){
														citycode ="CH";
												
												}if(circle.equalsIgnoreCase("Karnataka")){
													citycode ="KN";
												}if(circle.equalsIgnoreCase("Andhra Pradesh")){
													citycode ="AP";
												}if(circle.equalsIgnoreCase("Assam")){
													citycode ="AS";
												
												}if(circle.equalsIgnoreCase("Haryana")){
													citycode ="HR";
												}if(circle.equalsIgnoreCase("Himachal Pradesh")){
													citycode ="HP";
												}if(circle.equalsIgnoreCase("Jammu Kashmir")){
													citycode ="JK";
												}if(circle.equalsIgnoreCase("Kerala")){
													citycode ="KL";
												}if(circle.equalsIgnoreCase("Kolkata")){
													citycode ="KO";
												}if(circle.equalsIgnoreCase("Madhya Pradesh Chhatisgarh")){
													citycode ="MP";
													
												}if(circle.equalsIgnoreCase("Maharashtra")){
													citycode ="MH";
												}if(circle.equalsIgnoreCase("Mumbai")){
													citycode ="MU";
												}if(circle.equalsIgnoreCase("Orissa")){
													citycode ="OR";
												}if(circle.equalsIgnoreCase("Punjab")){
													citycode ="PB";
												}if(circle.equalsIgnoreCase("Tamil Nadu")){
													citycode ="TN";
												}if(circle.equalsIgnoreCase("UP East")){
													citycode ="UE";
												}if(circle.equalsIgnoreCase("UP West")){
													citycode ="UW";
												}if(circle.equalsIgnoreCase("West Bengal")){
													citycode ="WB";
												}
												if(circle.equalsIgnoreCase("Rajasthan")){
													citycode ="RJ";
												}
												if(circle.equalsIgnoreCase("Bihar Jharkhand")){
													citycode ="BR";
												}
												if(circle.equalsIgnoreCase("North East")){
													citycode ="NE";
												}
												if(circle.equalsIgnoreCase("Gujarat")){
													citycode ="GJ";
												}
											
											
											
											
												CsvWriter csvOutput = new CsvWriter(new FileWriter(FILE_PATH, true),',');
												
												csvOutput.write(Mcode);
												csvOutput.write(circle.toUpperCase());
												csvOutput.write(categoryName);
												csvOutput.write(actual);
												csvOutput.write(desc + " of Rs."+talktime );
												csvOutput.write(citycode);
												csvOutput.write(validity);
												csvOutput.write("Null");
												csvOutput.write(Ocode);
												csvOutput.write(Ccode);
												csvOutput.endRecord();
												csvOutput.close();
																					}
									}
								}
							}
						}
					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

	}



