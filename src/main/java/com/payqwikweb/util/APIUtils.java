package com.payqwikweb.util;

import java.text.DateFormat;

import org.json.JSONException;
import org.json.JSONObject;

public class APIUtils {
	
	  public static void main(String args[] ) throws Exception {
	        String mili = "1499365800";
	        String date = convertMilisecToDate(Long.parseLong(mili)*1000);
	        System.out.println(date);
	    }
	    
	    private static String convertMilisecToDate(long milisec) {
	        return DateFormat.getDateInstance(DateFormat.SHORT).format(milisec);
        
        /* 
        Formats :
            DateFormat.SHORT
            DateFormat.MEDIUM
            DateFormat.LONG
            DateFormat.FULL
            DateFormat.DEFAULT
        */
    }

	public static final String HOST = "http://172.16.3.96:9037/";
	public static final String END_POINT = "validateVPayQwikUser";
	public static final String URL = HOST+END_POINT;
	public static final String CHANNEL_ID = "MOB";
	public static final String USERNAME = "vpayqwik";
	public static final String PASSWORD = "vijayavpayQwikvalidation";
	public static final String TRANSACTION_ID = "42144534";

	public static JSONObject getFailedJSON() {
		JSONObject object = new JSONObject();
		try {
			object.put("code", "F00");
			object.put("message", "Service unavailable");
			object.put("status", "FAILED");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return object;
	}

	public static JSONObject getCustomJSON(String code,String message){
		JSONObject json = new JSONObject();
		try{
			json.put("status","FAILED");
			json.put("code",code);
			json.put("message",message);
			json.put("details",message);
		}catch(JSONException ex){
			ex.printStackTrace();
		}
		return json;
	}
	
	
	//Mera Events URL 
public static final String BASE_URL = "https://stage.meraevents.com/";   // Staging
	public static final String BASE_URL1 = "https://dev.appsfly.io/executor/exec";   // Staging

//	public static final String BASE_URL = "https://www.meraevents.com/";	// Production
	
	//Staging ID and Secret Key
	public static final int ClientID = 328688312;							
	public static final String ClientSecret = "85CSJkcDVpHl0M7PVdXJ";
	
	//Production ID and secret key
//	public static final int ClientID = 407478312;
//	public static final String ClientSecret = "U1lmWo5A5At4APS8YkQQ";
	
	public static final String AUTH_CODE = "web/api/v1/authorize/authorizationCode";
	public static final String ACCESS_TOKEN = "web/api/v1/authorize/getAccessToken";
	public static final String EVENT_LIST = "web/api/v1/events";
	public static final String EVENT_DETAILS = "web/api/v1/event";
	public static final String TICKET_DETAILS = "web/api/v1/ticket";
	public static final String GALLERY_DETAILS = "web/api/v1/gallery";
	public static final String CATEGORIES = "web/api/v1/categories";
	public static final String CITIES = "web/api/v1/cities";
	public static final String TICKET_CALCULATION = "web/api/v1/event/ticketCalculation";
	public static final String INITIATE_BOOKING = "web/api/v1/booking";
	public static final String ATTENDEE_FORM = "web/api/v1/booking/getCustomFields";
	public static final String SAVE_ATTENDEE = "web/api/v1/booking/saveAttendeeData";
	public static final String OFFLINE_BOOKING = "web/api/v1/booking/offline";
	
	//Savaari URL
	
	public static final String BASE_URL_SAVAARI = "http://api.betasavaari.com/partner_api/public/";
	
	public static final String TOKEN_CODE =BASE_URL_SAVAARI+"auth/token";
	public static final String TRIPTYPE_CODE =BASE_URL_SAVAARI+"trip-types";
	public static final String SUB_TRIPTYPE_CODE =BASE_URL_SAVAARI+"sub-trip-types";
	public static final String SOURCE_CODE =BASE_URL_SAVAARI+"source-cities";
	public static final String DESTINATION_CODE =BASE_URL_SAVAARI+"destination-cities";
	public static final String AVAILABLE_CODE =BASE_URL_SAVAARI+"availabilities";
	public static final String LOCALITIES_CODE =BASE_URL_SAVAARI+"localities";
	public static final String CAR_TYPE_CODE =BASE_URL_SAVAARI+"car-types";
	public static final String BOOKING_CODE =BASE_URL_SAVAARI+"booking";
	public static final String BOOKING_DETAILS_CODE =BASE_URL_SAVAARI+"booking/get";
	public static final String CANCEL_DETAILS_CODE =BASE_URL_SAVAARI+"booking/cancel";
	public static final String API_KEY_APP_ID="appId";
	public static final String API_KEY_="apiKey";
	
	public static final String API_VALUE_APP_ID="a5c12a9ae1f6a9c8471f13215293f2b5";
	public static final String API_VALUE_API_KEY="a84730fe899be615fa037efeff632c15f4a2a99906de7f19cc75b0bb05bfc298";
	public static final String TOKEN="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpYXQiOjE0OTI1ODU5NjUsImp0aSI6IkpsZmxhNDJudlpCemkxZXJKYUNPOVN0ajBuTEUrXC9uSlluTDZCU0lkNDdrPSIsImlzcyI6InNhdmFhcmkiLCJuYmYiOjE0OTI1ODU5NjUsImV4cCI6MTQ5Mzg5NDkyNSwiZGF0YSI6eyJhcGlLZXkiOiJhODQ3MzBmZTg5OWJlNjE1ZmEwMzdlZmVmZjYzMmMxNWY0YTJhOTk5MDZkZTdmMTljYzc1YjBiYjA1YmZjMjk4IiwiYXBwSWQiOiJhNWMxMmE5YWUxZjZhOWM4NDcxZjEzMjE1MjkzZjJiNSJ9fQ.PYpZohNNZTZY3FpZV8TCNYXGXZt_KDLwoj8-JmqIIfi18YSlh47HhbroPtGlTOLDTdmWe7oT92XM5Auet7Xf1Q";
	
	//YupTV URL
	
	
	public static final String REGISTER_YUPTV = "http://119.81.201.166:8099/partner/api/v2/register/vpayqwik";
			
	public static final String DEACTIVATE_YUPTV = "http://119.81.201.166:8099/partner/api/v2/user/deactivate/request";
	
//	public static final String BASE_URL_YUPTV = "https://ext.yupptv.in/partner/api/v2/register/vpayqwik";
	
	public static final String APIKEY_YUPTV_KEY="APIKEY";
	public static final String API_KEY_YUPTV_SECRET="APISECRET";
	
	public static final String APIKEY_YUPTV_VALUE="VPAYQWIK";
	public static final String APISECRET_YUPTV_VALUE="befb821e72a6a771f2a7d4767c73cd2f";
	
	
}
