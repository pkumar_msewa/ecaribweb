package com.payqwikweb.util;

import java.util.ArrayList;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.json.JSONArray;

import com.payqwikweb.model.app.request.TKApplyCouponRequest;
import com.payqwikweb.model.app.request.TKCartDetailsRequest;
import com.payqwikweb.model.app.request.TKItemsRequest;
import com.payqwikweb.model.app.request.TKMenuPriceCalRequest;
import com.payqwikweb.model.app.request.TKPlaceOrderRequest;
import com.payqwikweb.model.app.request.TKTrainslistRequest;

public class TravelkhanaEncUtil {

	public static String getSessionIdDec(String decData) {

		String sessionId=null;

		try {
			JSONObject jObj=new JSONObject(decData);
			sessionId=jObj.getString("sessionId");
			return sessionId;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return sessionId;
	}

	public static TKTrainslistRequest getTrainNumber(String decData) {
		TKTrainslistRequest req = new TKTrainslistRequest();
		
		try{
			JSONObject jObj  = new JSONObject(decData);
			req.setSessionId(jObj.getString("sessionId"));
			req.setTrainNumber(jObj.getString("trainNumber"));
		}catch(JSONException e){
			e.printStackTrace();
		}
		return req;
	}

	public static TKTrainslistRequest getOutletInTime(String decData) {
	
		TKTrainslistRequest req = new TKTrainslistRequest();
		
		try{
			JSONObject jObj  = new JSONObject(decData);
			req.setSessionId(jObj.getString("sessionId"));
			req.setArrivalTime(jObj.getString("arrivalTime"));
			req.setDate(jObj.getString("date"));
			req.setStation(jObj.getString("station"));
			req.setTrainNumber(jObj.getString("trainNumber"));
		}catch(JSONException e){
			e.printStackTrace();
		}
		return req;
	}

	public static TKTrainslistRequest getMenuInTime(String decData) {
    TKTrainslistRequest req = new TKTrainslistRequest();
		
		try{
			JSONObject jObj  = new JSONObject(decData);
			req.setSessionId(jObj.getString("sessionId"));
			req.setOutletId(jObj.getString("outletId"));
			req.setArrivalTime(jObj.getString("arrivalTime"));
		}catch(JSONException e){
			e.printStackTrace();
		}
		return req;
	}

	public static TKTrainslistRequest getTrainRoutesMenu(String decData) {
		 TKTrainslistRequest req = new TKTrainslistRequest();
			
			try{
				JSONObject jObj  = new JSONObject(decData);
				req.setSessionId(jObj.getString("sessionId"));
				req.setDate(jObj.getString("date"));
				req.setStation(jObj.getString("station"));
				req.setTrainNumber(jObj.getString("trainNumber"));
			}catch(JSONException e){
				e.printStackTrace();
			}
			return req;
	}

	public static TKMenuPriceCalRequest getMenuPriceCalculation(String decData) {
		TKMenuPriceCalRequest req = new TKMenuPriceCalRequest();
		try{
			ArrayList<TKItemsRequest>orderMenu = new ArrayList<TKItemsRequest>();
			JSONObject jObj  = new JSONObject(decData);
			req.setSessionId(jObj.getString("sessionId"));
			req.setOrder_outlet_id(jObj.getString("order_outlet_id"));
			org.codehaus.jettison.json.JSONArray jArr = jObj.getJSONArray("orderMenu");
			for(int i=0;i<jArr.length();i++){
				TKItemsRequest itemReq = new TKItemsRequest();
				itemReq.setItemId(jArr.getJSONObject(i).getString("itemId"));
				itemReq.setQuantity(jArr.getJSONObject(i).getLong("quantity"));
				orderMenu.add(itemReq);
			}
			req.setOrderMenu(orderMenu);
		}catch(JSONException e){
			e.printStackTrace();
		}
		return req;
	}

	public static TKTrainslistRequest TrackUserOrder(String decData) {
		 TKTrainslistRequest req = new TKTrainslistRequest();
		try{
			JSONObject jObj  = new JSONObject(decData);
			req.setSessionId(jObj.getString("sessionId"));
			req.setOrderId(jObj.getLong("orderId"));
		}catch(JSONException e){
			e.printStackTrace();
		}
		return req;
	}
	
	public static TKPlaceOrderRequest ApiOrder(String decData) {
		TKPlaceOrderRequest req = new TKPlaceOrderRequest();
		try{
			JSONObject jObj  = new JSONObject(decData);
			req.setSessionId(jObj.getString("sessionId"));
			req.setCoach(jObj.getString("coach"));
			req.setCod(jObj.getString("cod"));
			req.setEta(jObj.getString("eta"));
			req.setContact_no(jObj.getString("contact_no"));
			req.setMail_id(jObj.getString("mail_id"));
			req.setStation_code(jObj.getString("station_code"));
			req.setSeat(jObj.getString("seat"));
			req.setDate(jObj.getString("date"));
			req.setName(jObj.getString("name"));
			req.setTotalCustomerPayable(jObj.getDouble("totalCustomerPayable"));
			req.setCustomer_comment(jObj.getString("customer_comment"));
			req.setPnr(jObj.getString("pnr"));
			req.setTrain_number(jObj.getString("train_number"));
			req.setOrder_outlet_id(jObj.getString("order_outlet_id"));
			
			ArrayList<TKItemsRequest> items = new ArrayList<>();
			org.codehaus.jettison.json.JSONArray jArr = jObj.getJSONArray("items");
			for(int i=0;i<jArr.length();i++){
				TKItemsRequest item= new TKItemsRequest();
				item.setItemId(jArr.getJSONObject(i).getString("itemId"));
				item.setQuantity(jArr.getJSONObject(i).getLong("quantity"));
				items.add(item);
			}
			req.setItemsDto(items);
			}catch(JSONException e){
			e.printStackTrace();
		}
		return req;
	}
	

	public static TKApplyCouponRequest applycoupon(String decData) {
		ArrayList<TKCartDetailsRequest>cartDetails = new ArrayList<>();
		TKApplyCouponRequest req = new TKApplyCouponRequest();
		try{
			JSONObject jObj  = new JSONObject(decData);
			req.setSessionId(jObj.getString("sessionId"));
			req.setMobileNumber(jObj.getString("mobileNumber"));
			req.setMailId(jObj.getString("mailId"));
			org.codehaus.jettison.json.JSONArray jArr = jObj.getJSONArray("cartDetails");
			for(int i=0;i<jArr.length();i++){
				TKCartDetailsRequest cartDetail = new TKCartDetailsRequest();
				cartDetail.setCouponCode(jArr.getJSONObject(i).getString("couponCode"));
				cartDetail.setOutletId(jArr.getJSONObject(i).getLong("outletId"));
				cartDetail.setTotalSP(jArr.getJSONObject(i).getDouble("totalSP"));
				cartDetails.add(cartDetail);
			}
			req.setCartDetails(cartDetails);
		}catch(JSONException e){
			e.printStackTrace();
		}
		return req;
	}
}
