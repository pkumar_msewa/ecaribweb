package com.payqwikweb.util;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;
import java.util.StringTokenizer;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.json.JSONObject;

import com.payqwikweb.model.app.request.TKApplyCouponRequest;
import com.payqwikweb.model.app.request.TKMenuPriceCalRequest;
import com.payqwikweb.model.app.request.TKPlaceOrderRequest;
import com.payqwikweb.model.app.request.TKTrainslistRequest;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.sun.net.ssl.internal.www.protocol.https.HttpAuthenticator;
import com.travelkhana.util.TravelkhanaUtil;

public class ClientPost {
	private static Client client;
	private static WebResource webResource;
	private static String pUrl;

	public ClientPost() {

		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				java.security.cert.X509Certificate[] chck = null;
				;
				return chck;
			}

			public void checkClientTrusted(
					java.security.cert.X509Certificate[] arg0, String arg1)
					throws java.security.cert.CertificateException {
				// TODO Auto-generated method stub

			}

			public void checkServerTrusted(
					java.security.cert.X509Certificate[] arg0, String arg1)
					throws java.security.cert.CertificateException {
				// TODO Auto-generated method stub

			}
		} };

		// Install the all-trusting trust manager
		try {
			System.setProperty("https.protocols", "TLSv1,TLSv1.1,TLSv1.2");
			SSLContext sc = SSLContext.getInstance("TLSv1.2");
			sc.init(null, trustAllCerts, new SecureRandom());
			HttpsURLConnection
					.setDefaultSSLSocketFactory(sc.getSocketFactory());
		} catch (Exception e) {
			;
		}

	
	}

	public static ClientResponse stationListByTrainNumber(TKTrainslistRequest stationlistreq) {
		client = new Client(); // TODO if you are not using proxy
		webResource = client.resource(TravelkhanaUtil.stationList+stationlistreq.getTrainNumber());
		ClientResponse clientResponse = webResource.header(TravelkhanaUtil.USER, TravelkhanaUtil.VALUE)
				.type(javax.ws.rs.core.MediaType.APPLICATION_JSON)
				.get(ClientResponse.class);
		return clientResponse;
	}
	
	public static ClientResponse trainRoutesMenulist(TKTrainslistRequest dto) {
		client = new Client(); // TODO if you are not using proxy
		
	//	client.addFilter(new HTTPBasicAuthFilter(TravelkhanaUtil.Travelkhana_Username,TravelkhanaUtil.Travelkhana_Password));
		webResource = client.resource(TravelkhanaUtil.trainRoutesMenu+dto.getStation()+"&date="+dto.getDate()+"&trainNo="+dto.getTrainNumber());
		/*HttpAuthenticator feature = HttpAuthenticationFeature.basicBuilder()
			    .nonPreemptive()
			    .credentials("user", "password")
			    .build();*/
		ClientResponse clientResponse = webResource.header(TravelkhanaUtil.USER, TravelkhanaUtil.VALUE)
				.type(javax.ws.rs.core.MediaType.APPLICATION_JSON)
				.get(ClientResponse.class);
		return clientResponse;
	}
	
	public static ClientResponse getMenulist(TKTrainslistRequest dto) {
		client = new Client(); // TODO if you are not using proxy
		webResource = client.resource(TravelkhanaUtil.menuInTime+dto.getOutletId()+"?"+"arrivalTime="+dto.getArrivalTime());
		ClientResponse clientResponse = webResource.header(TravelkhanaUtil.USER, TravelkhanaUtil.VALUE)
				.type(javax.ws.rs.core.MediaType.APPLICATION_JSON)
				.get(ClientResponse.class);
		return clientResponse;
	}
	
	public static ClientResponse getOuletMenulist(TKTrainslistRequest dto) {
		client = new Client();
		webResource = client.resource(TravelkhanaUtil.outletInTime+dto.getArrivalTime()+"&station="+dto.getStation()+"&date="+dto.getDate());
		ClientResponse clientResponse = webResource.header(TravelkhanaUtil.USER, TravelkhanaUtil.VALUE)
				.type(javax.ws.rs.core.MediaType.APPLICATION_JSON)
				.get(ClientResponse.class);
		return clientResponse;
	}
	
	public static ClientResponse getMenuPriceCal(TKMenuPriceCalRequest dto) {
		client = new Client(); 
		webResource = client.resource(TravelkhanaUtil.menuPriceCal);
		ClientResponse clientResponse = webResource.queryParam("qr1", "qr1")
				.queryParam("qr2", "qr2").header(TravelkhanaUtil.USER, TravelkhanaUtil.VALUE)
				.type(javax.ws.rs.core.MediaType.APPLICATION_JSON)
				.post(ClientResponse.class,dto.getJsonRequest2());
		return clientResponse;
	}
	
	public static ClientResponse trackUserOrder(TKTrainslistRequest dto) {
		client = new Client();
		webResource = client.resource(TravelkhanaUtil.trackOrder+dto.getOrderId());
		ClientResponse clientResponse = webResource.queryParam("qr1", "qr1")
				.queryParam("qr2", "qr2").header(TravelkhanaUtil.USER, TravelkhanaUtil.VALUE)
				.type(javax.ws.rs.core.MediaType.APPLICATION_JSON)
				.get(ClientResponse.class);
		return clientResponse;
	}
	
	public static ClientResponse placeOrder(TKPlaceOrderRequest dto) {
		client = new Client();
		webResource = client.resource(TravelkhanaUtil.placeOrder);
		ClientResponse clientResponse = webResource.queryParam("qr1", "qr1")
				.queryParam("qr2", "qr2").header(TravelkhanaUtil.USER, TravelkhanaUtil.VALUE)
				.type(javax.ws.rs.core.MediaType.APPLICATION_JSON)
				.post(ClientResponse.class,dto.getJsonRequest2());
		return clientResponse;
	}
	
	public static ClientResponse applycoupon(TKApplyCouponRequest dto) {
		client = new Client();
		webResource = client.resource(TravelkhanaUtil.placeOrder);
		ClientResponse clientResponse = webResource.queryParam("qr1", "qr1")
				.queryParam("qr2", "qr2").header(TravelkhanaUtil.USER, TravelkhanaUtil.VALUE)
				.type(javax.ws.rs.core.MediaType.APPLICATION_JSON)
				.post(ClientResponse.class,dto.getJsonRequest2());
		return clientResponse;
	}

	/*public static void main(String[] args) {
		ClientPost consumer = new ClientPost();
		String jsonStr = "";
		String resData = consumer.postData(jsonStr);
	}*/
}
