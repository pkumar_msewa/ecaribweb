package com.payqwikweb.util;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.payqwikweb.app.model.request.AirBookRQ;
import com.payqwikweb.app.model.request.Bonds;
import com.payqwikweb.app.model.request.BookFare;
import com.payqwikweb.app.model.request.BookSegment;
import com.payqwikweb.app.model.request.EngineID;
import com.payqwikweb.app.model.request.Fare;
import com.payqwikweb.app.model.request.FlightSearchDetails;
import com.payqwikweb.app.model.request.Legs;
import com.payqwikweb.app.model.request.PaxFares;
import com.payqwikweb.app.model.request.PaymentDetails;
import com.payqwikweb.app.model.request.TravelFlightRequest;
import com.payqwikweb.app.model.response.FlightPriceCheckRequest;
import com.payqwikweb.model.app.request.TravellerFlightDetails;

public class TravelFlightUtil {
	
	public static String getSessionIdDec(String decData) {

		String sessionId=null;

		try {
			JSONObject jObj=new JSONObject(decData);
			sessionId=jObj.getString("sessionId");
			return sessionId;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return sessionId;
	}
	
	public static TravelFlightRequest searchFlightDec(String decData) {

		TravelFlightRequest req= new TravelFlightRequest();

		try {
			JSONObject jObj=new JSONObject(decData);

			req.setOrigin(jObj.getString("origin"));
			req.setDestination(jObj.getString("destination"));
			req.setBeginDate(jObj.getString("beginDate"));
			req.setTripType(jObj.getString("tripType"));
			req.setCabin(jObj.getString("cabin"));
			req.setAdults(jObj.getString("adults"));
			req.setChilds(jObj.getString("childs"));
			req.setInfants(jObj.getString("infants"));
			
			if (jObj.has("endDate")) {
				req.setEndDate(jObj.getString("endDate"));
			}
			
			req.setSessionId(jObj.getString("sessionId"));

			return req;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return req;
	}


	public static FlightPriceCheckRequest airRePriceReqDec(String decData) {

		FlightPriceCheckRequest req= new FlightPriceCheckRequest();

		ArrayList<Fare> faresArrList=new ArrayList<Fare>();
		ArrayList<PaxFares> paxFaresArrList=new ArrayList<PaxFares>();
		ArrayList<BookFare> bookFaresArrList=new ArrayList<BookFare>();
		ArrayList<Legs> legsList=new ArrayList<Legs>();
		ArrayList<Bonds> bondsList=new ArrayList<Bonds>();
		
		try {
			JSONObject jObj=new JSONObject(decData);

			if (jObj.getString("bonds")!=null && !jObj.getString("bonds").isEmpty() && !jObj.getString("bonds").equalsIgnoreCase("null")) {

				JSONArray bondsArr=jObj.getJSONArray("bonds");

				for (int j = 0; j < bondsArr.length(); j++) {

					Bonds bond=new Bonds();

					JSONObject bondsObj=bondsArr.getJSONObject(j);

					if (bondsObj!=null) {
						
						if (bondsObj.getString("legs")!=null && !bondsObj.getString("legs").isEmpty() && !bondsObj.getString("legs").equalsIgnoreCase("null")) {

							JSONArray legsArr=bondsObj.getJSONArray("legs");

							for (int k = 0; k < legsArr.length(); k++) {
								Legs legs=new Legs();

								if (legsArr.getString(k)!=null && !legsArr.getString(k).isEmpty() && !legsArr.getString(k).equalsIgnoreCase("null")) {

									JSONObject legsObj=legsArr.getJSONObject(k);

									if (legsObj!=null) {
										
										legs.setAircraftCode(legsObj.getString("aircraftCode"));
										legs.setAircraftType(legsObj.getString("aircraftType"));
										legs.setAirlineName(legsObj.getString("airlineName"));
										legs.setArrivalDate(legsObj.getString("arrivalDate"));
										legs.setArrivalTerminal(legsObj.getString("arrivalTerminal"));
										legs.setArrivalTime(legsObj.getString("arrivalTime"));
										legs.setAvailableSeat(legsObj.getString("availableSeat"));
										legs.setBaggageUnit(legsObj.getString("baggageUnit"));
										legs.setBaggageWeight(legsObj.getString("baggageWeight"));
										legs.setBoundTypes(legsObj.getString("boundTypes"));
										legs.setCapacity(legsObj.getString("capacity"));
										legs.setCarrierCode(legsObj.getString("carrierCode"));
										legs.setCurrencyCode(legsObj.getString("currencyCode"));
										legs.setDepartureDate(legsObj.getString("departureDate"));
										legs.setDepartureTerminal(legsObj.getString("departureTerminal"));
										legs.setDepartureTime(legsObj.getString("departureTime"));
										legs.setDestination(legsObj.getString("destination"));
										legs.setDuration(legsObj.getString("duration"));
										legs.setFareBasisCode(legsObj.getString("fareBasisCode"));
										legs.setFareClassOfService(legsObj.getString("fareClassOfService"));
										legs.setFlightDesignator(legsObj.getString("flightDesignator"));
										legs.setFlightDetailRefKey(legsObj.getString("flightDetailRefKey"));
										legs.setFlightNumber(legsObj.getString("flightNumber"));
										legs.setFlightName(legsObj.getString("flightName"));
										legs.setGroup(legsObj.getString("group"));
										legs.setOrigin(legsObj.getString("origin"));
										legs.setProviderCode(legsObj.getString("providerCode"));
										legs.setSold(legsObj.getString("sold"));
										legs.setStatus(legsObj.getString("status"));
										legs.setConnecting(legsObj.getBoolean("connecting"));
										legs.setCabin(legsObj.getString("cabin"));

									}
								}	
								legsList.add(legs);
							}
						}
						
						bond.setBoundType(bondsObj.getString("boundType"));
						bond.setItineraryKey(bondsObj.getString("itineraryKey"));
						bond.setJourneyTime(bondsObj.getString("journeyTime"));
						bond.setLegs(legsList);
						bond.setAddOnDetail(bondsObj.getString("addOnDetail"));
						bond.setBaggageFare(bondsObj.getBoolean("baggageFare"));
						bond.setSsrFare(bondsObj.getBoolean("ssrFare"));
					}
					bondsList.add(bond);
				}
			}

			
			if (jObj.getString("fares")!=null && !jObj.getString("fares").isEmpty() && !jObj.getString("fares").equalsIgnoreCase("null")) {

				JSONArray faresArr=jObj.getJSONArray("fares");

				for (int i = 0; i < faresArr.length(); i++) {

					if (faresArr.getString(i)!=null && !faresArr.getString(i).equalsIgnoreCase("null")) {

						Fare dto=new Fare();

						JSONObject fareObj=faresArr.getJSONObject(i);

						if (fareObj!=null) {
							dto.setBasicFare(fareObj.getDouble("basicFare"));
							dto.setExchangeRate(fareObj.getDouble("exchangeRate"));
							dto.setTotalFareWithOutMarkUp(fareObj.getDouble("totalFareWithOutMarkUp"));
							dto.setTotalTaxWithOutMarkUp(fareObj.getDouble("totalTaxWithOutMarkUp"));

							if (fareObj.getString("paxFares")!=null && !fareObj.getString("paxFares").isEmpty() && !fareObj.getString("paxFares").equalsIgnoreCase("null")) {

								JSONArray paxFaresArr=fareObj.getJSONArray("paxFares");

								for (int j = 0; j < paxFaresArr.length(); j++) {

									PaxFares paxFares=new PaxFares();
									JSONObject paxFaresObj=paxFaresArr.getJSONObject(j);

									if (paxFaresObj!=null) {
										paxFares.setBaggageUnit(paxFaresObj.getString("baggageUnit"));
										paxFares.setBaggageWeight(paxFaresObj.getString("baggageWeight"));
										paxFares.setBaseTransactionAmount(paxFaresObj.getDouble("baseTransactionAmount"));
										paxFares.setBasicFare(paxFaresObj.getDouble("basicFare"));
										paxFares.setCancelPenalty(paxFaresObj.getDouble("cancelPenalty"));
										paxFares.setChangePenalty(paxFaresObj.getDouble("changePenalty"));
										paxFares.setEquivCurrencyCode(paxFaresObj.getString("equivCurrencyCode"));

										if (paxFaresObj.getString("bookFares")!=null && !paxFaresObj.getString("bookFares").isEmpty() && !paxFaresObj.getString("bookFares").equalsIgnoreCase("null")) {

											JSONArray bookFaresArr=	paxFaresObj.getJSONArray("bookFares");

											for (int k = 0; k < bookFaresArr.length(); k++) {

												BookFare bookFare=new BookFare();
												if (bookFaresArr.getString(k)!=null && !bookFaresArr.getString(k).isEmpty() && !bookFaresArr.getString(k).equalsIgnoreCase("null")) {

													JSONObject bookFaresObj=bookFaresArr.getJSONObject(k);

													if (bookFaresObj!=null) {
														bookFare.setAmount(bookFaresObj.getDouble("amount"));
														bookFare.setChargeCode(bookFaresObj.getString("chargeCode"));
														bookFare.setChargeType(bookFaresObj.getString("chargeType"));
													}
												}
												bookFaresArrList.add(bookFare);
											}	
										}

										paxFares.setBookFares(bookFaresArrList);
										paxFares.setFareBasisCode(paxFaresObj.getString("fareBasisCode"));
										paxFares.setFareInfoKey(paxFaresObj.getString("fareInfoKey"));
										paxFares.setFareInfoValue(paxFaresObj.getString("fareInfoValue"));
										paxFares.setMarkUP(paxFaresObj.getDouble("markUP"));
										paxFares.setPaxType(paxFaresObj.getString("paxType"));
										paxFares.setRefundable(paxFaresObj.getBoolean("refundable"));
										paxFares.setTotalFare(paxFaresObj.getDouble("totalFare"));
										paxFares.setTotalTax(paxFaresObj.getDouble("totalTax"));
										paxFares.setTransactionAmount(paxFaresObj.getDouble("transactionAmount"));
									}
									paxFaresArrList.add(paxFares);
								}
							}

							dto.setPaxFares(paxFaresArrList);
							faresArrList.add(dto);
						}
					}
				}
			}
			
			req.setBonds(bondsList);
			req.setFares(faresArrList);
			req.setBaggageFare(jObj.getBoolean("baggageFare"));
			req.setCache(jObj.getBoolean("cache"));
			req.setHoldBooking(jObj.getBoolean("holdBooking"));
			req.setInternational(jObj.getBoolean("international"));
			req.setRoundTrip(jObj.getBoolean("roundTrip"));
			req.setSpecial(jObj.getBoolean("special"));
			req.setSpecialId(jObj.getBoolean("specialId"));
			req.setFareRule(jObj.getString("fareRule"));
			req.setItineraryKey(jObj.getString("itineraryKey"));
			req.setJourneyIndex(jObj.getString("journeyIndex"));
			req.setNearByAirport(jObj.getBoolean("nearByAirport"));
			req.setSearchId(jObj.getString("searchId"));
			req.setOrigin(jObj.getString("origin"));
			req.setDestination(jObj.getString("destination"));
			req.setBeginDate(jObj.getString("beginDate"));
			req.setEndDate(jObj.getString("endDate"));
			req.setEngineID(jObj.getString("engineID"));
			req.setCabin(jObj.getString("cabin"));
			req.setTripType(jObj.getString("tripType"));
			req.setAdults(jObj.getString("adults"));
			req.setChilds(jObj.getString("childs"));
			req.setInfants(jObj.getString("infants"));
			req.setTraceId(jObj.getString("traceId"));
			req.setSessionId(jObj.getString("sessionId"));

			return req;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return req;
	}


	public static AirBookRQ bookTicketDec(String decData) {

		AirBookRQ req= new AirBookRQ();
		
		ArrayList<BookSegment> bookSegmentList=new ArrayList<BookSegment>();
		ArrayList<Bonds> bondsList=new ArrayList<Bonds>();
		ArrayList<Legs> legsList=new ArrayList<Legs>();
		ArrayList<PaxFares> paxFaresArrList=new ArrayList<PaxFares>();
		ArrayList<BookFare> bookFaresArrList=new ArrayList<BookFare>();
		ArrayList<EngineID> engineIDList=new ArrayList<EngineID>();
		PaymentDetails paymentDetails=new PaymentDetails();
		ArrayList<FlightSearchDetails> flightSearchDetailsList=new ArrayList<FlightSearchDetails>();
		List<TravellerFlightDetails> travellerDetails=new ArrayList<TravellerFlightDetails>();
		
		try {
			JSONObject jObj=new JSONObject(decData);

			req.setEmailAddress(jObj.getString("emailAddress"));
			req.setMobileNumber(jObj.getString("mobileNumber"));
			req.setVisatype(jObj.getString("visatype"));
			req.setTraceId(jObj.getString("traceId"));
			req.setEngineID(jObj.getString("engineID"));
			req.setAndroidBooking(jObj.getBoolean("androidBooking"));
			req.setDomestic(jObj.getBoolean("domestic"));
			req.setPaymentMethod(jObj.getString("paymentMethod"));


			if (jObj.getString("bookSegments")!=null && !jObj.getString("bookSegments").isEmpty() && !jObj.getString("bookSegments").equalsIgnoreCase("null")) {

				JSONArray bookSegmentsArr=jObj.getJSONArray("bookSegments");

				for (int i = 0; i < bookSegmentsArr.length(); i++) {
					
					BookSegment bookSegment=new BookSegment();
					
					if (bookSegmentsArr.getString(i)!=null && !bookSegmentsArr.getString(i).equalsIgnoreCase("null")) {

						JSONObject bookSegmentsObj=bookSegmentsArr.getJSONObject(i);

						if (bookSegmentsObj!=null) {

							if (bookSegmentsObj.getString("bonds")!=null && !bookSegmentsObj.getString("bonds").isEmpty() && !bookSegmentsObj.getString("bonds").equalsIgnoreCase("null")) {

								JSONArray bondsArr=bookSegmentsObj.getJSONArray("bonds");

								for (int j = 0; j < bondsArr.length(); j++) {

									Bonds bond=new Bonds();

									JSONObject bondsObj=bondsArr.getJSONObject(j);

									if (bondsObj!=null) {
										
										if (bondsObj.getString("legs")!=null && !bondsObj.getString("legs").isEmpty() && !bondsObj.getString("legs").equalsIgnoreCase("null")) {

											JSONArray legsArr=bondsObj.getJSONArray("legs");

											for (int k = 0; k < legsArr.length(); k++) {
												Legs legs=new Legs();

												if (legsArr.getString(k)!=null && !legsArr.getString(k).isEmpty() && !legsArr.getString(k).equalsIgnoreCase("null")) {

													JSONObject legsObj=legsArr.getJSONObject(k);

													if (legsObj!=null) {
														
														legs.setAircraftCode(legsObj.getString("aircraftCode"));
														legs.setAircraftType(legsObj.getString("aircraftType"));
														legs.setAirlineName(legsObj.getString("airlineName"));
														legs.setArrivalDate(legsObj.getString("arrivalDate"));
														legs.setArrivalTerminal(legsObj.getString("arrivalTerminal"));
														legs.setArrivalTime(legsObj.getString("arrivalTime"));
														legs.setAvailableSeat(legsObj.getString("availableSeat"));
														legs.setBaggageUnit(legsObj.getString("baggageUnit"));
														legs.setBaggageWeight(legsObj.getString("baggageWeight"));
														legs.setBoundTypes(legsObj.getString("boundTypes"));
														legs.setCapacity(legsObj.getString("capacity"));
														legs.setCarrierCode(legsObj.getString("carrierCode"));
														legs.setCurrencyCode(legsObj.getString("currencyCode"));
														legs.setDepartureDate(legsObj.getString("departureDate"));
														legs.setDepartureTerminal(legsObj.getString("departureTerminal"));
														legs.setDepartureTime(legsObj.getString("departureTime"));
														legs.setDestination(legsObj.getString("destination"));
														legs.setDuration(legsObj.getString("duration"));
														legs.setFareBasisCode(legsObj.getString("fareBasisCode"));
														legs.setFareClassOfService(legsObj.getString("fareClassOfService"));
														legs.setFlightDesignator(legsObj.getString("flightDesignator"));
														legs.setFlightDetailRefKey(legsObj.getString("flightDetailRefKey"));
														legs.setFlightNumber(legsObj.getString("flightNumber"));
														legs.setFlightName(legsObj.getString("flightName"));
														legs.setGroup(legsObj.getString("group"));
														legs.setOrigin(legsObj.getString("origin"));
														legs.setProviderCode(legsObj.getString("providerCode"));
														legs.setSold(legsObj.getString("sold"));
														legs.setStatus(legsObj.getString("status"));
														legs.setConnecting(legsObj.getBoolean("connecting"));
														legs.setCabin(legsObj.getString("cabin"));

													}
												}	
												
												legsList.add(legs);
											}
										}
										
										bond.setBoundType(bondsObj.getString("boundType"));
										bond.setItineraryKey(bondsObj.getString("itineraryKey"));
										bond.setJourneyTime(bondsObj.getString("journeyTime"));
										bond.setLegs(legsList);
										bond.setAddOnDetail(bondsObj.getString("addOnDetail"));
										bond.setBaggageFare(bondsObj.getBoolean("baggageFare"));
										bond.setSsrFare(bondsObj.getBoolean("ssrFare"));
									}
									bondsList.add(bond);
								}
								
								bookSegment.setBonds(bondsList);
							}

							if (bookSegmentsObj.getString("fares")!=null && !bookSegmentsObj.getString("fares").isEmpty() && !bookSegmentsObj.getString("fares").equalsIgnoreCase("null")) {

								JSONObject faresObj=bookSegmentsObj.getJSONObject("fares");
								Fare dto=new Fare();
								
								if (faresObj!=null) {

									if (faresObj.getString("paxFares")!=null && !faresObj.getString("paxFares").isEmpty() && !faresObj.getString("paxFares").equalsIgnoreCase("null")) {

										JSONArray paxFaresArr=faresObj.getJSONArray("paxFares");

										for (int j = 0; j < paxFaresArr.length(); j++) {

											PaxFares paxFares=new PaxFares();
											JSONObject paxFaresObj=paxFaresArr.getJSONObject(j);

											if (paxFaresObj!=null) {
												paxFares.setBaggageUnit(paxFaresObj.getString("baggageUnit"));
												paxFares.setBaggageWeight(paxFaresObj.getString("baggageWeight"));
												paxFares.setBaseTransactionAmount(paxFaresObj.getDouble("baseTransactionAmount"));
												paxFares.setBasicFare(paxFaresObj.getDouble("basicFare"));
												paxFares.setCancelPenalty(paxFaresObj.getDouble("cancelPenalty"));
												paxFares.setChangePenalty(paxFaresObj.getDouble("changePenalty"));
												paxFares.setEquivCurrencyCode(paxFaresObj.getString("equivCurrencyCode"));

												if (paxFaresObj.getString("bookFares")!=null && !paxFaresObj.getString("bookFares").isEmpty() && !paxFaresObj.getString("bookFares").equalsIgnoreCase("null")) {

													JSONArray bookFaresArr=	paxFaresObj.getJSONArray("bookFares");

													for (int k = 0; k < bookFaresArr.length(); k++) {

														BookFare bookFare=new BookFare();
														if (bookFaresArr.getString(k)!=null && !bookFaresArr.getString(k).isEmpty() && !bookFaresArr.getString(k).equalsIgnoreCase("null")) {

															JSONObject bookFaresObj=bookFaresArr.getJSONObject(k);

															if (bookFaresObj!=null) {
																bookFare.setAmount(bookFaresObj.getDouble("amount"));
																bookFare.setChargeCode(bookFaresObj.getString("chargeCode"));
																bookFare.setChargeType(bookFaresObj.getString("chargeType"));
															}
														}
														bookFaresArrList.add(bookFare);
													}	
												}

												paxFares.setBookFares(bookFaresArrList);
												paxFares.setFareBasisCode(paxFaresObj.getString("fareBasisCode"));
												paxFares.setFareInfoKey(paxFaresObj.getString("fareInfoKey"));
												paxFares.setFareInfoValue(paxFaresObj.getString("fareInfoValue"));
												paxFares.setMarkUP(paxFaresObj.getDouble("markUP"));
												paxFares.setPaxType(paxFaresObj.getString("paxType"));
												paxFares.setRefundable(paxFaresObj.getBoolean("refundable"));
												paxFares.setTotalFare(paxFaresObj.getDouble("totalFare"));
												paxFares.setTotalTax(paxFaresObj.getDouble("totalTax"));
												paxFares.setTransactionAmount(paxFaresObj.getDouble("transactionAmount"));
											}
											paxFaresArrList.add(paxFares);
										}
									}
									dto.setBasicFare(faresObj.getDouble("basicFare"));
									dto.setExchangeRate(faresObj.getDouble("exchangeRate"));
									dto.setTotalFareWithOutMarkUp(faresObj.getDouble("totalFareWithOutMarkUp"));
									dto.setTotalTaxWithOutMarkUp(faresObj.getDouble("totalTaxWithOutMarkUp"));
									dto.setPaxFares(paxFaresArrList);
								}
								bookSegment.setFares(dto);
							}
						}
						bookSegment.setBaggageFare(bookSegmentsObj.getBoolean("baggageFare"));
						bookSegment.setCache(bookSegmentsObj.getBoolean("cache"));
						bookSegment.setDeeplink(bookSegmentsObj.getString("deeplink"));
						bookSegment.setHoldBooking(bookSegmentsObj.getBoolean("holdBooking"));
						bookSegment.setInternational(bookSegmentsObj.getBoolean("international"));
						bookSegment.setRoundTrip(bookSegmentsObj.getBoolean("roundTrip"));
						bookSegment.setSpecial(bookSegmentsObj.getBoolean("special"));
						bookSegment.setSpecialId(bookSegmentsObj.getBoolean("specialId"));
						bookSegment.setEngineID(EngineID.getEnum(bookSegmentsObj.getString("engineID")));
						bookSegment.setFareRule(bookSegmentsObj.getString("fareRule"));
						bookSegment.setItineraryKey(bookSegmentsObj.getString("itineraryKey"));
						bookSegment.setJourneyIndex(bookSegmentsObj.getInt("journeyIndex"));
						bookSegment.setNearByAirport(bookSegmentsObj.getBoolean("nearByAirport"));
						bookSegment.setSearchId(bookSegmentsObj.getString("searchId"));
						bookSegment.setRemark(bookSegmentsObj.getString("remark"));
					}
					
					bookSegmentList.add(bookSegment);
				}
			}

			if (jObj.getString("engineIDList")!=null && !jObj.getString("engineIDList").isEmpty() && !jObj.getString("engineIDList").equalsIgnoreCase("null")) {
				
				JSONArray engineIDListArr=jObj.getJSONArray("engineIDList");
				
				for (int i = 0; i < engineIDListArr.length(); i++) {
					
					engineIDList.add(EngineID.getEnum(engineIDListArr.getString(i)));
				}
			}
			
			if(jObj.getString("paymentDetails")!=null && !jObj.getString("paymentDetails").isEmpty() && !jObj.getString("paymentDetails").equalsIgnoreCase("null"))
			{
				JSONObject paymentDetailsObj=jObj.getJSONObject("paymentDetails");
				
				if (paymentDetailsObj!=null) {
					paymentDetails.setBookingAmount(paymentDetailsObj.getDouble("bookingAmount"));
					paymentDetails.setBookingCurrencyCode(paymentDetailsObj.getString("bookingCurrencyCode"));
				}
			}
			
			if(jObj.getString("flightSearchDetails")!=null && !jObj.getString("flightSearchDetails").isEmpty() && !jObj.getString("flightSearchDetails").equalsIgnoreCase("null"))
			{
				JSONArray flightSearchDetailsArr=jObj.getJSONArray("flightSearchDetails");
				
				for (int i = 0; i < flightSearchDetailsArr.length(); i++) {
					
					FlightSearchDetails details=new FlightSearchDetails();
					JSONObject flightSearchDetailsObj=flightSearchDetailsArr.getJSONObject(i);
					
					if (flightSearchDetailsObj!=null) {
						details.setBeginDate(flightSearchDetailsObj.getString("beginDate"));
						details.setDestination(flightSearchDetailsObj.getString("destination"));
						details.setOrigin(flightSearchDetailsObj.getString("origin"));
						
						if(flightSearchDetailsObj.has("endDate"))
						{
							details.setEndDate(flightSearchDetailsObj.getString("endDate"));
						}
					}
					flightSearchDetailsList.add(details);
				}
			}
			
			if(jObj.getString("travellerDetails")!=null && !jObj.getString("travellerDetails").isEmpty() && !jObj.getString("travellerDetails").equalsIgnoreCase("null"))
			{
				JSONArray travellerDetailsArr=jObj.getJSONArray("travellerDetails");
				
				for (int i = 0; i < travellerDetailsArr.length(); i++) {
					
					TravellerFlightDetails details=new TravellerFlightDetails();
					JSONObject travellerDetailsObj=travellerDetailsArr.getJSONObject(i);
					
					if (travellerDetailsObj!=null) {
						
						details.setfName(travellerDetailsObj.getString("fName"));
						details.setlName(travellerDetailsObj.getString("lName"));
						details.setTitle(travellerDetailsObj.getString("title"));
						details.setGender(travellerDetailsObj.getString("gender"));
						details.setType(travellerDetailsObj.getString("type"));
						details.setDob(travellerDetailsObj.getString("dob"));
						details.setPassNo(travellerDetailsObj.getString("passNo"));
						details.setPassExpDate(travellerDetailsObj.getString("passExpDate"));
						
					}
					travellerDetails.add(details);
				}
			}
			
			req.setTravellerDetails(travellerDetails);
			req.setFlightSearchDetails(flightSearchDetailsList);
			req.setPaymentDetails(paymentDetails);
			req.setBookSegments(bookSegmentList);
			req.setEngineIDList(engineIDList);
			req.setSessionId(jObj.getString("sessionId"));

			return req;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return req;
	}

}
