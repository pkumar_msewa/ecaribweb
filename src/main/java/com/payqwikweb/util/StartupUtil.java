package com.payqwikweb.util;

import com.payqwikweb.app.metadatas.UrlMetadatas;

public class StartupUtil {

	// For Prajun Sir

//	 public static final String CSV_FILE =
//	 "E:/Softwares/Tomcat/apache-tomcat-7.0.57/webapps/PayQwik/WebContent/WEB-INF/startup/";

//	public static final String CSV_FILE = "E:/Softwares/Tomcat/apache-tomcat-7.0.57/webapps/PayQwik/WebContent/WEB-INF/startup/";

	// public static final String CSV_FILE =
	// "E:/Softwares/Tomcat/apache-tomcat-7.0.57/webapps/PayQwik/WebContent/WEB-INF/startup/";

	// For Vibhanshu
//	public static final String CSV_FILE = "C:/Users/vibhu/workspace/PayQwik/WebContent/WEB-INF/startup/";
	// public static final String
	// CSV_FILE="/usr/local/tomcat/ROOT/WebContent/WEB-INF/startup/";
	
	// For fgmtest server
//	 public static final String CSV_FILE =	"/usr/local/tomcat7/webapps/ROOT/WEB-INF/startup/";
//	public static final String CSV_FILE = "/usr/local/tomcat7/webapps/ROOT/WEB-INF/startup/";
	
//	public static final String CSV_FILE = "D:/PayQwik_Server/PayQwik/WebContent/WEB-INF/startup/";
	 
//	 public static final String CSV_FILE_REPORT_TEST = "D:\\OFFICE PROJECTS\\office project\\WEB\\WEB\\WebContent\\resources\\merchantReport\\";
	 
//	 public static final String CSV_FILE_REPORT_TEST = "D:\\OFFICE PROJECTS\\office project\\WEB\\WEB\\WebContent\\resources\\merchantReport\\";
	 
//	 public static final String CSV_FILE_TEST =  "D:\\Projects\\Work Spaces\\TriPayWebWorkSpace\\VpayQwik-Web\\WebContent\\resources\\refund\\";
	 
//	 public static final String CSV_FILE = "/usr/local/tomcat7/webapps/ROOT/resources/refund/";
	 
	 public static final String CSV_FILE_TEST = "/usr/local/tomcat/webapps/ROOT/resources/refund/";
	 
	 public static final String CSV_FILE_REPORT_TEST = "/usr/local/tomcat/webapps/ROOT/resources/merchantReport/";
	 
//	 public static final String CSV_FILE_REPORT_TEST = "D:\\OFFICE PROJECTS\\office project\\WEB\\WEB\\WebContent\\resources\\merchantReport\\";
	  
	 public static final String CSV_FILE_LIVE = "/usr/local/tomcat1/webapps/ROOT/resources/refund/";
	 
	 public static final String CSV_FILE_REPORT_LIVE = "/usr/local/tomcat1/webapps/ROOT/resources/merchantReport/";
	 
	 public static final String CSV_EXE_FORMAT = "csv";
	 
	 public static final String CSV_FILE = getCSVFile();
		
	 public static final String REPORT_CSV_FILE = getReportCSVFile();
		
		
	public static String getCSVFile() {
		return UrlMetadatas.PRODUCTION ? CSV_FILE_LIVE : CSV_FILE_TEST;
	}
		
	public static String getReportCSVFile() {
		return UrlMetadatas.PRODUCTION ? CSV_FILE_REPORT_LIVE : CSV_FILE_REPORT_TEST;
	}

}
