package com.payqwikweb.util;

import java.security.Key;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.lang.StringEscapeUtils;
import org.codehaus.jettison.json.JSONObject;

/*import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;*/

public class AES 
{
	private static String algorithm = "AES";
	private static byte[] keyValue=new byte[] 
			{ 'V', 'P', 'M', 'S', 'S', 'p', 'a', 'Y', 'M', 'e', 'N', 'T', 'S', 'K', 'e', 'y' };

	// Performs Encryption
	/*public static String encrypt(String plainText) throws Exception{
		Key key = generateKey();
		Cipher chiper = Cipher.getInstance(algorithm);
		chiper.init(Cipher.ENCRYPT_MODE, key);
		byte[] encVal = chiper.doFinal(plainText.getBytes());
	//	String encryptedValue = new BASE64Encoder().encode(encVal);
		return encryptedValue;
	}

	// Performs decryption
	public static String decrypt(String encrypted) throws Exception {
		String encryptedText=StringEscapeUtils.unescapeJava(encrypted);
		byte[] decordedValue = new BASE64Decoder().decodeBuffer(encryptedText);
		Key key = generateKey();
		Cipher chiper = Cipher.getInstance(algorithm);
		chiper.init(Cipher.DECRYPT_MODE, key);
		byte[] decValue = chiper.doFinal(decordedValue);
		String decryptedValue = new String(decValue);
		return decryptedValue;
	}*/

	//generateKey() is used to generate a secret key for AES algorithm
	private static Key generateKey() throws Exception {
		Key key = new SecretKeySpec(keyValue, algorithm);
		return key;
	}

	// performs encryption & decryption 

	public static void main(String[] args) throws Exception 
	{
//		String plainText="{\"sessionId\":\"FAB24C50A1438BF6F1055C1F7AE11343\",\"aadharNumber\":\"770811683877\"}";
		
		JSONObject payload= new JSONObject();
		payload.put("sessionId", "1sla4s55fh14tfk9tfrg6zf22");
		payload.put("aadharNumber", "770811683877");
		payload.put("otpreqId", "991521772819989");
		payload.put("key", "941485");
/*
		String encryptedText = AES.encrypt(payload.toString());
//		String decryptedText = AES.decrypt("DtH1iZ+uz2RUTLOm8sca8MS7xFIjWKbEHWHcyWbhZBFQ3RkrXPLDprfXwni4MLLtZOXeT7hZMhghP5vHOb4ILTqeNw2AIkwsSDDJWT9EZdY=");

		System.out.println("Encrypted Text is : "+encryptedText);
		System.err.println("Decrypted Text : " + AES.decrypt(encryptedText));*/
	}

}