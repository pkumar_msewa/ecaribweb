package com.payqwikweb.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.math.BigInteger;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.SignatureException;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;


public class SecurityUtil {


	private static final String KEY = "adkj@#$02#@adflkj)(*jlj@#$#@LKjasdjlkj<.,mo@#$@#kljlkdsu343";
	private static final String KEY_NEW = "AFRNDUCNBANKUPON";

	 public static void main(String[] args) throws Exception {
		System.err.println(md5("F947F49E37BCCAAAB774C1EE7C707F79|1|272962|999400101800018"));
		
		System.err.println(md5("F947F49E37BCCAAAB774C1EE7C707F79|1.00|0.00|272962|1234567891"));
	}

	public static boolean isValidAPIKey(String key) {
	 	return key.equals(KEY_NEW);
	}
	public static Key generateKey(String workingKey){
		byte []bytes = workingKey.getBytes();
		Key key = new SecretKeySpec(bytes,"AES");
		return key;
	}
	public static String getHash(String algorithm,String data) throws NoSuchAlgorithmException {
		MessageDigest messageDigest = MessageDigest.getInstance(algorithm);
		//convert the string data into bytes
		byte []bytes = data.getBytes();
		//update the bytes into object
		messageDigest.update(bytes);
		//calculte digest
		byte []digest = messageDigest.digest();
		//converting byte into integer
		BigInteger i = new BigInteger(1,digest);
		String result = i.toString(16).toUpperCase();
		return result;
	}

	public static KeyPair generateKeyPair(String algorithm) throws NoSuchAlgorithmException, NoSuchProviderException {
		KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(algorithm);
		SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG","SUN");
		keyPairGenerator.initialize(1024,secureRandom);
		return keyPairGenerator.generateKeyPair();
	}

	public static byte[] getSign(String data) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException, SignatureException {
		Signature signature = Signature.getInstance("SHA1withDSA");
		KeyPair keyPair = generateKeyPair("DSA");
		signature.initSign(keyPair.getPrivate());
		signature.update(data.getBytes());
		return signature.sign();
	}

	/**
	 * Encrypts and encodes the Object and IV for url inclusion
	 *
	 * @param obj
	 * @return
	 * @throws Exception
	 */
	public static String[] encryptObject(Object obj) throws Exception {
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		ObjectOutput out = new ObjectOutputStream(stream);
		try {
			out.writeObject(obj);
			byte[] serialized = stream.toByteArray();
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			byte[] iv = new byte[cipher.getBlockSize()];
			new SecureRandom().nextBytes(iv);
			IvParameterSpec ivSpec = new IvParameterSpec(iv);
			MessageDigest digest = MessageDigest.getInstance("SHA-256");
			digest.update(KEY.getBytes());
			byte[] key = new byte[16];
			System.arraycopy(digest.digest(), 0, key, 0, key.length);
			SecretKeySpec keySpec = new SecretKeySpec(key, "AES");
			cipher.init(Cipher.ENCRYPT_MODE, keySpec, ivSpec);
			byte[] encrypted = cipher.doFinal(serialized);
			byte[] base64Encoded = Base64.encodeBase64(encrypted);
			String base64String = new String(base64Encoded);
			String urlEncodedData = URLEncoder.encode(base64String, "UTF-8");
			byte[] base64IV = Base64.encodeBase64(iv);
			String base64IVString = new String(base64IV);
			String urlEncodedIV = URLEncoder.encode(base64IVString, "UTF-8");
			return new String[] { urlEncodedData, urlEncodedIV };
		} finally {
			stream.close();
			out.close();
		}
	}

	/**
	 * Decrypts the String and serializes the object
	 *
	 * @param base64Data
	 * @param base64IV
	 * @return
	 * @throws Exception
	 */
	public static Object decryptObject(String base64Data, String base64IV) throws Exception {
		// Decode the data
		byte[] encryptedData = Base64.decodeBase64(base64Data.getBytes());
		byte[] rawIV = Base64.decodeBase64(base64IV.getBytes());
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		IvParameterSpec ivSpec = new IvParameterSpec(rawIV);
		MessageDigest digest = MessageDigest.getInstance("SHA-256");
		digest.update(KEY.getBytes());
		byte[] key = new byte[16];
		System.arraycopy(digest.digest(), 0, key, 0, key.length);
		SecretKeySpec keySpec = new SecretKeySpec(key, "AES");
		cipher.init(Cipher.DECRYPT_MODE, keySpec, ivSpec);
		byte[] decrypted = cipher.doFinal(encryptedData);
		ByteArrayInputStream stream = new ByteArrayInputStream(decrypted);
		ObjectInput in = new ObjectInputStream(stream);
		Object obj = null;
		try {
			obj = in.readObject();
		} finally {
			stream.close();
			in.close();
		}
		return obj;
	}

	public static String md5(String str) throws Exception {

		MessageDigest m = MessageDigest.getInstance("MD5");

		byte[] data = str.getBytes();

		m.update(data, 0, data.length);

		BigInteger i = new BigInteger(1, m.digest());

		String hash = String.format("%1$032X", i);

		return hash;
	}


	public static boolean isHashMatches(Object obj, String hash) {
		boolean isValid = true;
		ObjectWriter ow = new ObjectMapper().writer();
		try {
			String json = ow.writeValueAsString(obj);
			System.err.println("JSON ::" + json);
			String encryptedHash = "";
			try {
				encryptedHash = md5(json);
				System.err.println("Calculated hash ::" + encryptedHash);
			} catch (Exception e) {
				System.err.println("Internal Server Error While Using MD5");
			}
			if (encryptedHash.equals(hash)) {
				System.err.println("Both hash are equal");
				isValid = true;
			} else {
				System.err.println("Hash are not equal ::" + hash);
			}
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return isValid;
	}
	
	public static byte[] decodeBase64(String encodedData) throws IOException {
		return java.util.Base64.getDecoder().decode(encodedData);
	}
}
