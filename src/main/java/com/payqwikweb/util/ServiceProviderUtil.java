package com.payqwikweb.util;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.payqwikweb.app.metadatas.Constants;
import com.payqwikweb.app.model.response.ServiceProviderResponse;


public class ServiceProviderUtil {
 
	public static ServiceProviderResponse getServiceProviders() throws JSONException {
		ServiceProviderResponse result = new ServiceProviderResponse();
		JSONArray array = new JSONArray();
		for (Map.Entry<String, String> entry : getElectricity().entrySet()) {
			JSONObject object1 = new JSONObject();
			object1.put("operatorCode", entry.getKey());
			object1.put("operatorName", entry.getValue());
			object1.put("accountNumberType", getElectri(entry.getKey()));
			object1.put("images", getElectricityImages(entry.getKey()));
			array.put(object1);
			// do something with key and/or tab
		}
		result.setDetail(array.toString());
		result.setSuccess(true);
		result.setCode("S00");
		return result;
	}
 
	public static HashMap<String, String> getElectricity() {
		LinkedHashMap<String, String> opItems = new LinkedHashMap<String, String>();
		opItems.put("VARE", "Ajmer Vidyut Vitran Nigam - RAJASTHAN");
		opItems.put("VAAE", "APDCL - ASSAM");
		opItems.put("VAEE", "APEPDCL - ANDHRA PRADESH");
		opItems.put("VASE", "APSPDCL - ANDHRA PRADESH");
		opItems.put("VBBE", "BESCOM - BENGALURU");
		opItems.put("VBME", "BEST Undertaking - MUMBAI");
		opItems.put("VBRE", "BSES Rajdhani - DELHI");
		opItems.put("VBYE", "BSES Yamuna - DELHI");
		opItems.put("VBPE", "BESL - BHARATPUR");
		opItems.put("VBKE", "BkESL - BIKANER");
		opItems.put("VCWE", "CESC - WEST BENGAL");
		opItems.put("VCCE", "CSEB - CHHATTISGARH");
		opItems.put("VDHE", "DHBVN - HARYANA");
		opItems.put("VDNE", "DNHPDCL - DADRA & NAGAR HAVELI");
		opItems.put("VDDE", "Daman and Diu Electricity");
		opItems.put("VDGE", "DGVCL - GUJARAT");
		opItems.put("VGKE", "GESCOM - KARNATAKA");
		opItems.put("VIPE", "India Power");
		opItems.put("VIBE", "India Power - BIHAR");
		opItems.put("VJRE", "Jaipur Vidyut Vitran Nigam - RAJASTHAN");
		opItems.put("VJUE", "Jamshedpur Utilities & Services (JUSCO)");
		opItems.put("VDRE", "Jodhpur Vidyut Vitran Nigam - RAJASTHAN");
		opItems.put("VKRE", "Kota Electricity Distribution - RAJASTHAN");
		opItems.put("VMME", "Madhya Kshetra Vitaran - MADHYA PRADESH");
		opItems.put("VMDE", "MSEDC - MAHARASHTRA");
		opItems.put("VMEE", "MEPDCL - MEGHALAYA");
		opItems.put("VMGE", "MGVCL - GUJARAT");
		opItems.put("VMBE", "Muzaffarpur Vidyut Vitran");
		opItems.put("VNUE", "Noida Power - NOIDA");
		opItems.put("VNBE", "NBPDCL - BIHAR");
		opItems.put("VNOE", "NESCO - ODISHA");
		opItems.put("VMPE", "Paschim Kshetra Vitaran - MADHYA PRADESH");
		opItems.put("VPGE", "PGVCL - GUJARAT");
		opItems.put("VREE", "Reliance Energy - MUMBAI");
		opItems.put("VSBE", "SBPDCL - BIHAR");
		opItems.put("VSNE", "SNDL Power - NAGPUR");
		opItems.put("VSOE", "SOUTHCO - ODISHA");
		opItems.put("VNDE", "Tata Power - DELHI");
		opItems.put("VTME", "Tata Power - MUMBAI");
		opItems.put("VTNE", "TNEB - TAMIL NADU");
		opItems.put("VTPE", "Torrent Power");
		opItems.put("VTTE", "TSECL - TRIPURA");
		opItems.put("VAJE", "TPADL - AJMER");
		opItems.put("VUGE", "UGVCL - GUJARAT");
		opItems.put("VUKE", "UPCL - UTTARAKHAND");
		opItems.put("VURE", "UPPCL (RURAL) - UTTAR PRADESH");
		opItems.put("VUPE", "UPPCL (URBAN) - UTTAR PRADESH");
		opItems.put("VWOE", "WESCO - ODISHA");
		opItems.put("VJBE", "JHARKHAND");

		return opItems;
	}

	public static String getElectri(String value) {
		LinkedHashMap<String, String> opItems = new LinkedHashMap<String, String>();

		opItems.put("VARE", "numeric");
		opItems.put("VAAE", "numeric");
		opItems.put("VAEE", "alphanumeric");
		opItems.put("VASE", "numeric");
		opItems.put("VBBE", "numeric");
		opItems.put("VBME", "alphanumeric");
		opItems.put("VBRE", "numeric");
		opItems.put("VBYE", "numeric");
		opItems.put("VBPE", "alphanumeric");
		opItems.put("VBKE", "numeric");
		opItems.put("VCWE", "numeric");
		opItems.put("VCCE", "numeric");
		opItems.put("VDHE", "alphanumeric");
		opItems.put("VDNE", "alphanumeric");
		opItems.put("VDDE", "numeric");
		opItems.put("VDGE", "numeric");
		opItems.put("VGKE", "alphanumeric");
		opItems.put("VIPE", "numeric");
		opItems.put("VIBE", "numeric");
		opItems.put("VJRE", "numeric");
		opItems.put("VJUE", "numeric");
		opItems.put("VDRE", "numeric");
		opItems.put("VKRE", "numeric");
		opItems.put("VMME", "numeric");
		opItems.put("VMDE", "numeric");
		opItems.put("VMEE", "numeric");
		opItems.put("VMGE", "numeric");
		opItems.put("VMBE", "numeric");
		opItems.put("VNUE", "numeric");
		opItems.put("VNBE", "numeric");
		opItems.put("VNOE", "numeric");
		opItems.put("VMPE", "numeric");
		opItems.put("VPGE", "numeric");
		opItems.put("VREE", "numeric");
		opItems.put("VSBE", "numeric");
		opItems.put("VSNE", "numeric");
		opItems.put("VSOE", "numeric");
		opItems.put("VNDE", "numeric");
		opItems.put("VTME", "numeric");
		opItems.put("VTNE", "alphanumeric");
		opItems.put("VTPE", "numeric");
		opItems.put("VTTE", "numeric");
		opItems.put("VAJE", "numeric");
		opItems.put("VUGE", "numeric");
		opItems.put("VUKE", "alphanumeric");
		opItems.put("VURE", "numeric");
		opItems.put("VUPE", "numeric");
		opItems.put("VWOE", "numeric");
		opItems.put("VJBE", "numeric");

		return opItems.get(value);
	}
    
	public static String getElectricityImages(String value) {
		LinkedHashMap<String, String> opItems = new LinkedHashMap<String, String>();
		opItems.put("VARE", Constants.getWebUrlForImages() + "/resources/images/electricity/ARE.png");
		opItems.put("VAAE", Constants.getWebUrlForImages() + "/resources/images/electricity/AAE.png");
		opItems.put("VAEE", Constants.getWebUrlForImages() + "/resources/images/electricity/AEE.png");
		opItems.put("VASE", Constants.getWebUrlForImages() + "/resources/images/electricity/ASE.png");
		opItems.put("VBBE", Constants.getWebUrlForImages() + "/resources/images/electricity/BBE.png");
		opItems.put("VBME", Constants.getWebUrlForImages() + "/resources/images/electricity/BME.png");
		opItems.put("VBRE", Constants.getWebUrlForImages() + "/resources/images/electricity/BRE.png");
		opItems.put("VBYE", Constants.getWebUrlForImages() + "/resources/images/electricity/BYE.png");
		opItems.put("VBPE", Constants.getWebUrlForImages() + "/resources/images/electricity/BPE.png");
		opItems.put("VBKE", Constants.getWebUrlForImages() + "/resources/images/electricity/BKE.png");
		opItems.put("VCWE", Constants.getWebUrlForImages() + "/resources/images/electricity/CWE.png");
		opItems.put("VCCE", Constants.getWebUrlForImages() + "/resources/images/electricity/CCE.png");
		opItems.put("VDHE", Constants.getWebUrlForImages() + "/resources/images/electricity/DHE.png");
		opItems.put("VDNE", Constants.getWebUrlForImages() + "/resources/images/electricity/DNE.png");
		opItems.put("VDDE", Constants.getWebUrlForImages() + "/resources/images/electricity/DDE.png");
		opItems.put("VDGE", Constants.getWebUrlForImages() + "/resources/images/electricity/DGE.png");
		opItems.put("VGKE", Constants.getWebUrlForImages() + "/resources/images/electricity/GKE.png");
		opItems.put("VIPE", Constants.getWebUrlForImages() + "/resources/images/electricity/IPE.png");
		opItems.put("VIBE", Constants.getWebUrlForImages() + "/resources/images/electricity/IBE.png");
		opItems.put("VJRE", Constants.getWebUrlForImages() + "/resources/images/electricity/JRE.png");
		opItems.put("VJUE", Constants.getWebUrlForImages() + "/resources/images/electricity/JUE.png");
		opItems.put("VDRE", Constants.getWebUrlForImages() + "/resources/images/electricity/DRE.png");
		opItems.put("VKRE", Constants.getWebUrlForImages() + "/resources/images/electricity/KRE.png");
		opItems.put("VMME", Constants.getWebUrlForImages() + "/resources/images/electricity/MME.png");
		opItems.put("VMDE", Constants.getWebUrlForImages() + "/resources/images/electricity/MDE.png");
		opItems.put("VMEE", Constants.getWebUrlForImages() + "/resources/images/electricity/MEE.png");
		opItems.put("VMGE", Constants.getWebUrlForImages() + "/resources/images/electricity/MGE.png");
		opItems.put("VMBE", Constants.getWebUrlForImages() + "/resources/images/electricity/MBE.png");
		opItems.put("VNUE", Constants.getWebUrlForImages() + "/resources/images/electricity/NUE.png");
		opItems.put("VNBE", Constants.getWebUrlForImages() + "/resources/images/electricity/NBE.png");
		opItems.put("VNOE", Constants.getWebUrlForImages() + "/resources/images/electricity/NOE.png");
		opItems.put("VMPE", Constants.getWebUrlForImages() + "/resources/images/electricity/MPE.png");
		opItems.put("VPGE", Constants.getWebUrlForImages() + "/resources/images/electricity/PGE.png");
		opItems.put("VREE", Constants.getWebUrlForImages() + "/resources/images/electricity/REE.png");
		opItems.put("VSBE", Constants.getWebUrlForImages() + "/resources/images/electricity/SBE.png");
		opItems.put("VSNE", Constants.getWebUrlForImages() + "/resources/images/electricity/SNE.png");
		opItems.put("VSOE", Constants.getWebUrlForImages() + "/resources/images/electricity/SOE.png");
		opItems.put("VNDE", Constants.getWebUrlForImages() + "/resources/images/electricity/NDE.png");
		opItems.put("VTME", Constants.getWebUrlForImages() + "/resources/images/electricity/TME.png");
		opItems.put("VTNE", Constants.getWebUrlForImages() + "/resources/images/electricity/TNE.png");
		opItems.put("VTPE", Constants.getWebUrlForImages() + "/resources/images/electricity/TPE.png");
		opItems.put("VTTE", Constants.getWebUrlForImages() + "/resources/images/electricity/TTE.png");
		opItems.put("VAJE", Constants.getWebUrlForImages() + "/resources/images/electricity/AJE.png");
		opItems.put("VUGE", Constants.getWebUrlForImages() + "/resources/images/electricity/UGE.png");
		opItems.put("VUKE", Constants.getWebUrlForImages() + "/resources/images/electricity/UKE.png");
		opItems.put("VURE", Constants.getWebUrlForImages() + "/resources/images/electricity/URE.png");
		opItems.put("VUPE", Constants.getWebUrlForImages() + "/resources/images/electricity/UPE.png");
		opItems.put("VWOE", Constants.getWebUrlForImages() + "/resources/images/electricity/WOE.png");

		return opItems.get(value);
	}
    
	public static ServiceProviderResponse getServiceProvidersForGas() throws JSONException {
		ServiceProviderResponse result = new ServiceProviderResponse();
		JSONArray array = new JSONArray();
		for (Map.Entry<String, String> entry : getGas().entrySet()) {
			JSONObject object1 = new JSONObject();
			object1.put("operatorCode", entry.getKey());
			object1.put("operatorName", entry.getValue());
			object1.put("accountNumberType", getGasValue(entry.getKey()));
			object1.put("accountNumberPattern", getGasPattern(entry.getKey()));
			object1.put("gasImages", getImages(entry.getKey()));
			array.put(object1);
		}
		result.setDetail(array.toString());
		result.setSuccess(true);
		result.setCode("S00");
		return result;
	}

	public static HashMap<String, String> getGas() {
		LinkedHashMap<String, String> opItems = new LinkedHashMap<String, String>();
		opItems.put("VADG", "Adani Gas - GUJARAT");
		opItems.put("VAHG", "Adani Gas - HARYANA");
		opItems.put("VGJG", "Gujarat Gas");
		opItems.put("VHCG", "Haryana City Gas");
		opItems.put("VIPG", "Indraprastha Gas");
		opItems.put("VMMG", "Mahanagar Gas");
		opItems.put("VSGG", "Sabarmati Gas");
		opItems.put("VSUG", "Siti Energy");
		opItems.put("VTNG", "Tripura Natural Gas");
		opItems.put("VUCG", "Unique Central Piped Gases");
		opItems.put("VVGG", "Vadodara Gas");
		return opItems;
	}

	public static String getGasValue(String value) {
		LinkedHashMap<String, String> opItems = new LinkedHashMap<String, String>();

		opItems.put("VADG", "numeric");
		opItems.put("VAHG", "numeric");
		opItems.put("VGJG", "numeric");
		opItems.put("VHCG", "numeric");
		opItems.put("VIPG", "numeric");
		opItems.put("VMMG", "numeric");
		opItems.put("VSGG", "numeric");
		opItems.put("VSUG", "numeric");
		opItems.put("VTNG", "numeric");
		opItems.put("VUCG", "alphanumeric");
		opItems.put("VVGG", "numeric");
		return opItems.get(value);
	}

	public static String getGasPattern(String value) {
		LinkedHashMap<String, String> opItems = new LinkedHashMap<String, String>();
		opItems.put("VADG", "Customer ID");
		opItems.put("VAHG", "Customer ID");
		opItems.put("VGJG", "Customer ID");
		opItems.put("VHCG", "CRN Number");
		opItems.put("VIPG", "BP Number");
		opItems.put("VMMG", "CA Number");
		opItems.put("VSGG", "Customer ID");
		opItems.put("VSUG", "ARN Number");
		opItems.put("VTNG", "Consumer Number");
		opItems.put("VUCG", "Customer No");
		opItems.put("VVGG", "Consumer Number");
		return opItems.get(value);
	}

	public static String getImages(String value) {
		LinkedHashMap<String, String> opItems = new LinkedHashMap<String, String>();
		opItems.put("VADG", Constants.getWebUrlForImages() + "/resources/images/gas/ADG.png");
		opItems.put("VAHG", Constants.getWebUrlForImages() + "/resources/images/gas/AHG.png");
		opItems.put("VGJG", Constants.getWebUrlForImages() + "/resources/images/gas/GJG.png");
		opItems.put("VHCG", Constants.getWebUrlForImages() + "/resources/images/gas/HCG.png");
		opItems.put("VIPG", Constants.getWebUrlForImages() + "/resources/images/gas/IPG.png");
		opItems.put("VMMG", Constants.getWebUrlForImages() + "/resources/images/gas/MMG.png");
		opItems.put("VSGG", Constants.getWebUrlForImages() + "/resources/images/gas/SGG.png");
		opItems.put("VSUG", Constants.getWebUrlForImages() + "/resources/images/gas/SUG.png");
		opItems.put("VTNG", Constants.getWebUrlForImages() + "/resources/images/gas/TNG.png");
		opItems.put("VUCG", Constants.getWebUrlForImages() + "/resources/images/gas/UCG.png");
		opItems.put("VVGG", Constants.getWebUrlForImages() + "/resources/images/gas/VGG.png");
		return opItems.get(value);
	}
    
	public static ServiceProviderResponse getServiceProvidersForDth() throws JSONException {
		ServiceProviderResponse result = new ServiceProviderResponse();
		JSONArray array = new JSONArray();
		for (Map.Entry<String, String> entry : getGas().entrySet()) {
			JSONObject object1 = new JSONObject();
			object1.put("operatorCode", entry.getKey());
			object1.put("operatorName", entry.getValue());
			object1.put("accountNumberType", getDthValue(entry.getKey()));
			object1.put("accountNumberPattern", getDthPattern(entry.getKey()));
			object1.put("gasImages", getDthImages(entry.getKey()));
			array.put(object1);
		}
		result.setDetail(array.toString());
		result.setSuccess(true);
		result.setCode("S00");
		return result;
	}

	public static HashMap<String, String> getDth() {
		LinkedHashMap<String, String> opItems = new LinkedHashMap<String, String>();
		opItems.put("VATV", "Airtel Digital TV");
		opItems.put("VDTV", "Dish TV");
		opItems.put("VRTV", "Reliance Digital TV");
		opItems.put("VSTV", "Sun Direct");
		opItems.put("VOTV", "Tata Sky - ONLINE");
		opItems.put("VVTV", "Videocon d2h");
		return opItems;
	}

	public static String getDthValue(String value) {
		LinkedHashMap<String, String> opItems = new LinkedHashMap<String, String>();

		opItems.put("VATV", "numeric");
		opItems.put("VDTV", "numeric");
		opItems.put("VRTV", "numeric");
		opItems.put("VSTV", "numeric");
		opItems.put("VOTV", "numeric");
		opItems.put("VVTV", "numeric");
		return opItems.get(value);
	}

	public static String getDthPattern(String value) {
		LinkedHashMap<String, String> opItems = new LinkedHashMap<String, String>();
		opItems.put("VATV", "Customer ID");
		opItems.put("VDTV", "Viewing Card Number");
		opItems.put("VRTV", "Smart Card Number");
		opItems.put("VSTV", "Smart Card Number");
		opItems.put("VOTV", "Subscriber ID");
		opItems.put("VVTV", "Customer ID");
		return opItems.get(value);
	}

	public static String getDthImages(String value) {
		LinkedHashMap<String, String> opItems = new LinkedHashMap<String, String>();
		opItems.put("VATV", Constants.getWebUrlForImages() + "/resources/images/dth/ATV.png");
		opItems.put("VDTV", Constants.getWebUrlForImages() + "/resources/images/dth/DTV.png");
		opItems.put("VRTV", Constants.getWebUrlForImages() + "/resources/images/dth/RTV.png");
		opItems.put("VSTV", Constants.getWebUrlForImages() + "/resources/images/dth/STV.png");
		opItems.put("VOTV", Constants.getWebUrlForImages() + "/resources/images/dth/OTV.png");
		opItems.put("VVTV", Constants.getWebUrlForImages() + "/resources/images/dth/VTV.png");
		return opItems.get(value);
	}
  
  
  public static ServiceProviderResponse getServiceProvidersForLandline() throws JSONException{
  	  ServiceProviderResponse result= new ServiceProviderResponse();
  	  JSONArray array=new JSONArray();
  	     for ( Map.Entry<String, String> entry : getGas().entrySet()) {
  	      JSONObject  object1=new JSONObject();
  	    object1.put("operatorCode", entry.getKey());
  	    object1.put("operatorName", entry.getValue());
  	    object1.put("landlineImages", getLandLineImages(entry.getKey()));
  	    array.put(object1);
  	   }
  	     result.setDetail(array.toString());
  	     result.setSuccess(true);
  	     result.setCode("S00");
  	     return result;
  	  } 
  
  
  public static HashMap<String, String> getLandLine() {
  LinkedHashMap<String, String> opItems = new LinkedHashMap<String, String>();
    opItems.put("VATL", "Airtel");
    opItems.put("VBGL", "BSNL");
    opItems.put("VMDL", "MTNL - Delhi");
    opItems.put("VMML", "MTNL - Mumbai");
    opItems.put("VTCL", "Tata Docomo");
    return opItems;
  }
  
  public static String  getLandLineImages(String value) {
      LinkedHashMap<String, String> opItems = new LinkedHashMap<String, String>();
      opItems.put("VATL", Constants.getWebUrlForImages()+"/resources/images/landline/ATL.png");
      opItems.put("VBGL", Constants.getWebUrlForImages()+"/resources/images/landline/BGL.png");
      opItems.put("VMDL", Constants.getWebUrlForImages()+"/resources/images/landline/MDL.png");
      opItems.put("VMML", Constants.getWebUrlForImages()+"/resources/images/landline/MML.png");
      opItems.put("VTCL", Constants.getWebUrlForImages()+"/resources/images/landline/TCL.png");
      return opItems.get(value);
  }
  
  
	public static ServiceProviderResponse getServiceProvidersForInsurance() throws JSONException {
		ServiceProviderResponse result = new ServiceProviderResponse();
		JSONArray array = new JSONArray();
		for (Map.Entry<String, String> entry : getGas().entrySet()) {
			JSONObject object1 = new JSONObject();
			object1.put("operatorCode", entry.getKey());
			object1.put("operatorName", entry.getValue());
			object1.put("accountNumberType", getInsuranceValue(entry.getKey()));
			object1.put("gasImages", getInsuranceImages(entry.getKey()));
			array.put(object1);
		}
		result.setDetail(array.toString());
		result.setSuccess(true);
		result.setCode("S00");
		return result;
	}

	public static HashMap<String, String> getInsurance() {
		LinkedHashMap<String, String> opItems = new LinkedHashMap<String, String>();
		opItems.put("VIPI", "ICICI Prudential Life Insurance");
		opItems.put("VTGI", "Tata AIA Life Insurance");
		opItems.put("VTAI", "Tata AIG General Insurance");

		return opItems;
	}

	public static String getInsuranceValue(String value) {
		LinkedHashMap<String, String> opItems = new LinkedHashMap<String, String>();

		opItems.put("VIPI", "numeric");
		opItems.put("VTGI", "alphanumeric");
		opItems.put("VTAI", "alphanumeric");
		return opItems.get(value);
	}

	public static String getInsuranceImages(String value) {
		LinkedHashMap<String, String> opItems = new LinkedHashMap<String, String>();
		opItems.put("VIPI", Constants.getWebUrlForImages() + "/resources/images/insurance/IPI.png");
		opItems.put("VTGI", Constants.getWebUrlForImages() + "/resources/images/insurance/TGI.png");
		opItems.put("VTAI", Constants.getWebUrlForImages() + "/resources/images/insurance/TAI.png");
		return opItems.get(value);
	}
    


}