package com.payqwikweb.util;

import com.payqwikweb.app.metadatas.UrlMetadatas;

public class TreatCardUtil {
	
	private static final String BASE_URL_TEST = "http://54.69.215.25:3000/api/v1/msewa/";// TEST URL
	private static final String BASE_URL_LIVE = "http://54.69.215.25:3000/api/v1/msewa/";// LIVE URL
    public static final String REGISTER_URL = getGciUrl()+"users";
   
    
     public static String getGciUrl(){
    	if(UrlMetadatas.PRODUCTION){
    		return BASE_URL_LIVE;
    	}else{
    		return BASE_URL_TEST;
    	}
    }

}
