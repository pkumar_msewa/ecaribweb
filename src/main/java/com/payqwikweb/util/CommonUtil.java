package com.payqwikweb.util;

import java.security.SecureRandom;
import java.security.Security;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.apache.commons.lang.RandomStringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.payqwikweb.app.model.Device;

public class CommonUtil {

	/**
	 * Random Number Generator for MObile OTP return 6 digit random number
	 * 
	 * @return
	 */
	public static SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	public static SimpleDateFormat formatter2 = new SimpleDateFormat("dd/MM/yyyy");
	public static SimpleDateFormat formatter3 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	public static SimpleDateFormat dateFormate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public static String startTime = " 00:00:00";
	public static String endTime = " 23:59:59";
	public static int YEAR = -365;
	public static int MONTH = -30;
	
	public static String generateSixDigitNumericString() {
		return "" + (int) (Math.random() * 1000000);
	}

	public static String generateNineDigitNumericString() {
		return "" + (int) (Math.random() * 1000000000);
	}

	public static String generateNDigitNumericString(long n) {
		double mul = Math.pow(10, n);
		long result = (long) (Math.random() * mul);
		String number = String.valueOf(result);
		if (number.length() != n) {
			return generateNDigitNumericString(n + 1);
		}
		return number;
	}

	public static int generateNDigitNumber(long n) {
		double mul = Math.pow(10, n);
		int result = (int) (Math.random() * mul);
		String number = String.valueOf(result);
		if (number.length() != n) {
			return generateNDigitNumber(n + 1);
		}
		return result;
	}
	
		public static Date getPreviousMonthDate() {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(new Date());
			calendar.add(calendar.DATE, -30);
			Date yesterDayDate = calendar.getTime();
			try {
				yesterDayDate = dateFormate.parse(formatter.format(yesterDayDate) + startTime);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			return yesterDayDate;
		}
		
		public static Date getTodayMonthDate() {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(new Date());
			Date yesterDayDate = calendar.getTime();
			try {
				yesterDayDate = dateFormate.parse(formatter.format(yesterDayDate) + endTime);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			return yesterDayDate;
		}

	public static void sleep(long timeInMS) {
		try {
			Thread.sleep(timeInMS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static String generateRandomCity() {
		String city = "";
		int choice = generateNDigitNumber(1);
		switch (choice) {
		case 1:
			city = "Jaipur";
			break;
		case 2:
			city = "Chennai";
			break;
		case 3:
			city = "Delhi";
			break;
		case 4:
			city = "Hyderabad";
			break;
		case 5:
			city = "Kolkata";
			break;
		case 6:
			city = "Mumbai";
			break;
		case 7:
			city = "Ahemdabad";
			break;
		case 8:
			city = "Bengaluru";
			break;
		case 9:
			city = "Pune";
			break;
		default:
			city = "Kanpur";
			break;
		}
		return city;
	}

	public static SOAPConnection getNewConnection() {
		SOAPConnection soapConnection = null;
		try {
			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
			soapConnection = soapConnectionFactory.createConnection();
		} catch (SOAPException e) {
			e.printStackTrace();
		}
		return soapConnection;
	}

	public static SOAPMessage createSOAPMessage() {
		SOAPMessage soapMessage = null;
		try {
			MessageFactory messageFactory = MessageFactory.newInstance();
			soapMessage = messageFactory.createMessage();
		} catch (SOAPException e) {
			e.printStackTrace();
		}
		return soapMessage;
	}

	public static String generateVJB32digitToken() {
		String random = RandomStringUtils.random(32, false, true);
		System.out.println("VJB" + random);
		return "VJB" + random;
	}

	public static String merchantRefNo() {
		String txnRef = System.currentTimeMillis() + "";
		txnRef = txnRef.substring(0, txnRef.length() - 1);
		return txnRef;
	}
	
	/**
	 * @author Prashant
	 * @param file
	 * @return
	 */
	public static boolean isValidCSVFormate(MultipartFile file){
		String[] fileExtension = file.getContentType().split("/");
		String[] formats = { "vnd.ms-excel", "csv" };
		for (String fmt : formats) {
			if (fileExtension[1].equals(fmt)) {
				return true;
			}
		}
		return false;
	}

	// public static void main(String... args){
	// System.err.print(generateRandomCity());
	// }

	/*
	 *
	 * Code from
	 * http://blog.hexican.com/2010/12/sending-soap-messages-through-https-using
	 * -saaj/
	 *
	 */
	static public void doTrustToCertificates() throws Exception {
		Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			public X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			public void checkServerTrusted(X509Certificate[] certs, String authType) throws CertificateException {
				return;
			}

			public void checkClientTrusted(X509Certificate[] certs, String authType) throws CertificateException {
				return;
			}
		} };

		SSLContext sc = SSLContext.getInstance("TLSv1.2");
		sc.init(null, trustAllCerts, new SecureRandom());
		HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
		HostnameVerifier hv = new HostnameVerifier() {
			public boolean verify(String urlHostName, SSLSession session) {
				if (!urlHostName.equalsIgnoreCase(session.getPeerHost())) {
					System.out.println("Warning: URL host '" + urlHostName + "' is different to SSLSession host '"
							+ session.getPeerHost() + "'.");
				}
				return true;
			}
		};
		HttpsURLConnection.setDefaultHostnameVerifier(hv);
		System.setProperty("javax.net.debug", "ssl");
	}

	public static String[] getDefaultDateRange() {
		String range[] = new String[2];
		Date today = new Date();
		Calendar cal = new GregorianCalendar();
		cal.setTime(today);
		cal.add(Calendar.DAY_OF_MONTH, -30);
		Date today30 = cal.getTime();
		range[0] = formatter.format(today30);
		range[1] = formatter.format(today);
		System.out.println(range[0] + ":" + range[1]);
		return range;
		
		
	}

	public static String formateSlashDateToDashDate(String strDate){
			try {
				if(strDate!=null && !strDate.isEmpty()){
					Date date=CommonUtil.formatter2.parse(strDate);
					return CommonUtil.formatter.format(date);					
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
			return null;
	}
	public static Long getTimestamp(String strDate){
		if(strDate!=null){
			try {
				Date date=formatter3.parse(strDate);
				return date.getTime();
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	public static boolean isNull(String str) {
		if (str == null || str.isEmpty() || str == "") {
			return true;
		} else {
			return false;
		}
	}
	
	public static Device getDevice(String osName) {
		if(osName!=null && !osName.isEmpty()){
			switch (osName.toUpperCase()) {
			case "ANDROID":
				return Device.ANDROID;
			case "IOS":
				return Device.IOS;
			default:
				return Device.WEBSITE;
			}			
		}
		return Device.WEBSITE;
	}
	
	public static String[] getDateRange(String daterange){
		String range[]=null;
		if(!isNull(daterange)){
			try{
				range=daterange.split("-");
				range[0]=formatter.format(new Date(Long.parseLong(range[0].trim())));
				range[1]=formatter.format(new Date(Long.parseLong(range[1].trim())));
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		return range;
	}
	
	public static void main(String[] args) {
		
//		String startDate=dateFormate.format(getPreviousMonthDate());
//		String endDate=	dateFormate.format(getTodayMonthDate());
//		System.err.println(startDate);
//		System.err.println(endDate);
		
	}
	
	
}
