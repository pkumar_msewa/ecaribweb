package com.payqwikweb.util;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.payqwik.visa.util.VisaResponseDTO;
import com.payqwikweb.app.model.MerchantDTO;
import com.payqwikweb.app.model.ResponseStatus;
import com.payqwikweb.app.model.request.Cabin;
import com.payqwikweb.app.model.request.EngineID;
import com.payqwikweb.app.model.request.PaxType;
import com.payqwikweb.app.model.request.VisaRequest;
import com.payqwikweb.app.model.response.EBSRedirectResponse;
import com.payqwikweb.app.model.response.VisaResponse;
import com.payqwikweb.model.mobile.UpiSdkCredentials;
import com.payqwikweb.model.web.WEBSRedirectResponse;
import com.thirdparty.model.S2SRequest;
import com.upi.model.requet.GenerateDEKRequest;
import com.upi.model.requet.MerchantCheckTxnStatus;
import com.upi.model.requet.MerchantCollectMoneyInfoRequest;
import com.upi.model.requet.MerchantCollectMoneyRequest;
import com.upi.model.requet.MerchantMetaInfoRequest;
import com.upi.model.requet.MerchantMetaInfoVPARequest;
import com.upi.model.requet.MerchantStatusMetaInfoRequest;
import com.upi.model.requet.MerchantTokenGenRequest;
import com.upi.model.requet.MerchantTokenValidRequest;
import com.upi.model.requet.VPARequest;
import com.upi.model.response.MerchantMetaVPAResponse;
import com.upi.util.AESAlgorithm;
import com.upi.util.UPIBescomContstant;
import com.upi.util.UPIConstants;
import com.upi.util.UPIRSAEncryption;

import javassist.runtime.DotClass;

public class ConvertUtil {

	private ConvertUtil() {

	}
	
	public static void main(String[] args) throws IOException {
		String x="2018-03-19";
		byte[] encodedBytes = Base64.getEncoder().encode(x.getBytes());
		String encodedString=ConvertUtil.encodeBase64(encodedBytes);
		System.err.println("encode ::" + encodedString);
		System.err.println("decode ::" + ConvertUtil.decodeBase64String(encodedString));
	}

	public static VisaResponseDTO getFromResponse(VisaResponse response) {
		VisaResponseDTO dto = new VisaResponseDTO();
		System.err.println("converting to dto");
		dto.setSuccess(response.isSuccess());
		dto.setCode(response.getCode());
		dto.setMessage(response.getMessage());
		dto.setResponse(response.getResponse());
		dto.setStatus(response.getStatus());
		return dto;
	}

	public static EBSRedirectResponse convertFromWEBS(WEBSRedirectResponse response) {
		EBSRedirectResponse redirectResponse = new EBSRedirectResponse();
		redirectResponse.setAccountId(response.getAccountId());
		redirectResponse.setAmount(response.getAmount());
		redirectResponse.setBillingAddress(response.getBillingAddress());
		redirectResponse.setBillingCity(response.getBillingCity());
		redirectResponse.setBillingCountry(response.getBillingCountry());
		redirectResponse.setBillingEmail(response.getBillingEmail());
		redirectResponse.setBillingName(response.getBillingName());
		redirectResponse.setBillingPostalCode(response.getBillingPostalCode());
		redirectResponse.setDateCreated(response.getDateCreated());
		redirectResponse.setMerchantRefNo(response.getMerchantRefNo());
		redirectResponse.setIsFlagged(response.getIsFlagged());
		redirectResponse.setMode(response.getMode());
		redirectResponse.setPaymentId(response.getPaymentId());
		redirectResponse.setDeliveryName(response.getDeliveryName());
		redirectResponse.setDeliveryAddress(response.getDeliveryAddress());
		redirectResponse.setDeliveryPhone(response.getDeliveryPhone());
		redirectResponse.setDeliveryPostalCode(response.getDeliveryPostalCode());
		return redirectResponse;
	}



	

	public static List<MerchantDTO> convertFromArray(JSONArray array) {
		List<MerchantDTO> dtoList = new ArrayList<>();
		try {
			if (array != null) {
				for (int i = 0; i < array.length(); i++) {
					JSONObject o = array.getJSONObject(i);
					dtoList.add(getFromJSON(o));
				}
			}
		} catch (Exception ex) {

		}
		return dtoList;
	}

	public static MerchantDTO getFromJSON(JSONObject o) {
		MerchantDTO dto = new MerchantDTO();
		try {
			if (o != null) {
				dto.setId(JSONParserUtil.getLong(o, "id"));
				dto.setName(JSONParserUtil.getString(o, "name"));
				dto.setImage(JSONParserUtil.getString(o, "image"));
				dto.setEmail(JSONParserUtil.getString(o, "email"));
				dto.setContactNo(JSONParserUtil.getString(o, "contactNo"));
			}
		} catch (NullPointerException ex) {
			ex.printStackTrace();
		}
		return dto;
	}

	
	public static boolean checkVisaRequest(VisaRequest request) {
		if (request.getRequest() != null)
			return true;
		return false;
	}

	public static String generateMerchantKey(String plainDEK) throws Exception {
		AESAlgorithm aesAlgorithm = new AESAlgorithm();
		String decryptedDEK = aesAlgorithm.decryptDEK(plainDEK, UPIConstants.KEK);
		System.out.println("DecryptedDEK:: " + decryptedDEK);
		String merchantCredentials = aesAlgorithm.generateMerchantCredential(decryptedDEK,
				System.currentTimeMillis() + "#" + UPIConstants.TRANSACTION_PWD);
		System.err.println("merchantCredentials " + merchantCredentials);
		return merchantCredentials;
	}

	public static GenerateDEKRequest generatDEKRequest() {
		GenerateDEKRequest dekRequest = new GenerateDEKRequest();
		dekRequest.setMobileNo(UPIConstants.PAYEE_MOBILE_NUM);
		dekRequest.setMsgId(CommonUtil.generateVJB32digitToken());
		dekRequest.setTimeStamp(CommonUtil.merchantRefNo());
		return dekRequest;
	}

	public static MerchantTokenGenRequest generateMerchantTokenRequest(MerchantCollectMoneyRequest dto) throws Exception {
		MerchantTokenGenRequest genRequest = new MerchantTokenGenRequest();
		String refId = CommonUtil.merchantRefNo();
		AESAlgorithm aesAlgorithm = new AESAlgorithm();
		if(dto.isSdk()){
			genRequest.setChennel(UPIConstants.SDK_CHENNEL);
			genRequest.setDeviceId(dto.getDeviceId());
			genRequest.setAppVersion(UPIConstants.SDK_APP_VERSION);
		}else{
			genRequest.setChennel(UPIConstants.CHENNEL);
			genRequest.setDeviceId(UPIConstants.DEVICE_ID);
			genRequest.setAppVersion(UPIConstants.APP_VERSION);
		}
		String decryptedDEK = aesAlgorithm.decryptDEK(UPIConstants.PLAN_DEK, UPIConstants.KEK);
		genRequest.setRefId(refId);
		genRequest.setTimeStamp(CommonUtil.merchantRefNo());
		genRequest.setMsgId(CommonUtil.generateVJB32digitToken());
		genRequest.setMerchantCredentials(
				aesAlgorithm.generateMerchantCredential(decryptedDEK, refId + "#" + UPIConstants.TRANSACTION_PWD));
		genRequest.setMobileNo(UPIConstants.PAYEE_MOBILE_NUM);
		genRequest.setSdk(dto.isSdk());
		return genRequest;
	}
	
	public static MerchantTokenValidRequest tokenValidRequest(String token, boolean sdk, String deviceId) throws Exception {
		MerchantTokenValidRequest tokenValidRequest = new MerchantTokenValidRequest();
		String refId = CommonUtil.merchantRefNo();
		AESAlgorithm aesAlgorithm = new AESAlgorithm();
		String decryptedDEK = aesAlgorithm.decryptDEK(UPIConstants.PLAN_DEK, UPIConstants.KEK);
		if(sdk){
			tokenValidRequest.setChennel(UPIConstants.SDK_CHENNEL);
			tokenValidRequest.setDeviceId(deviceId);
			tokenValidRequest.setAppVersion(UPIConstants.SDK_APP_VERSION);
		}else{
			tokenValidRequest.setChennel(UPIConstants.CHENNEL);
			tokenValidRequest.setDeviceId(UPIConstants.DEVICE_ID);
			tokenValidRequest.setAppVersion(UPIConstants.APP_VERSION);
		}
		tokenValidRequest.setMerchantCredentials(
				aesAlgorithm.generateMerchantCredential(decryptedDEK, refId + "#" + UPIConstants.TRANSACTION_PWD));
		tokenValidRequest.setMobileNo(UPIConstants.PAYEE_MOBILE_NUM);
		tokenValidRequest.setMsgId(CommonUtil.generateVJB32digitToken());
		tokenValidRequest.setRefId(refId);
		tokenValidRequest.setTimeStamp(System.currentTimeMillis() + "");
		System.err.println("Token : : " + token);
		tokenValidRequest.setToken(aesAlgorithm.generateMerchantCredential(decryptedDEK, token));
		System.out.println("validate token request:");
		System.out.println(tokenValidRequest.getRequest());
		return tokenValidRequest;
	}

	public static MerchantMetaInfoRequest merchantMetaInfoRequest(String msdId, String metaData, boolean sdk) throws IOException {
		MerchantMetaInfoRequest infoRequest = new MerchantMetaInfoRequest();
		infoRequest.setMetaData(metaData);
		infoRequest.setMetaInfo(UPIRSAEncryption.encryptData(msdId, UPIConstants.PUBLIK_KEY));
		infoRequest.setSdk(sdk);
		return infoRequest;
	}

	public static VPARequest vpaRequest(MerchantCollectMoneyRequest dto) throws Exception {
		VPARequest vpaRequest = new VPARequest();
		String refId = CommonUtil.merchantRefNo();
		AESAlgorithm aesAlgorithm = new AESAlgorithm();
		String decryptedDEK = aesAlgorithm.decryptDEK(UPIConstants.PLAN_DEK, UPIConstants.KEK);

		vpaRequest.setMerchantCredentials(
				aesAlgorithm.generateMerchantCredential(decryptedDEK, refId + "#" + UPIConstants.TRANSACTION_PWD));
		vpaRequest.setMobileNo(UPIConstants.PAYEE_MOBILE_NUM);
		vpaRequest.setMsgId(CommonUtil.generateVJB32digitToken());
		vpaRequest.setRefId(refId);
		vpaRequest.setTimeStamp(System.currentTimeMillis() + "");
		vpaRequest.setPayerVirAddr(dto.getPayerVirAddr());
		System.out.println("VPA Request :: :  " + vpaRequest.getRequest());
		return vpaRequest;
	}

	public static MerchantMetaInfoVPARequest validateMerchantVPARequest(String msdId, String metaData)
			throws IOException {
		MerchantMetaInfoVPARequest infoRequest = new MerchantMetaInfoVPARequest();
		infoRequest.setMetaData(metaData);
		infoRequest.setMetaInfo(UPIRSAEncryption.encryptData(msdId, UPIConstants.PUBLIK_KEY));
		return infoRequest;
	}

	public static MerchantCollectMoneyRequest collectMoneyRequest(MerchantCollectMoneyRequest dto) throws Exception {
		MerchantCollectMoneyRequest validMerchant = new MerchantCollectMoneyRequest();
//		String refId = CommonUtil.merchantRefNo();
		AESAlgorithm aesAlgorithm = new AESAlgorithm();
		String decryptedDEK = aesAlgorithm.decryptDEK(UPIConstants.PLAN_DEK, UPIConstants.KEK);

		validMerchant.setPayerVirAddr(dto.getPayerVirAddr());
		validMerchant.setAmount(dto.getAmount());
		validMerchant.setMerchantCredentials(
				aesAlgorithm.generateMerchantCredential(decryptedDEK, dto.getRefId() + "#" + UPIConstants.TRANSACTION_PWD));
		validMerchant.setMobileNo(UPIConstants.PAYEE_MOBILE_NUM);
		validMerchant.setMsgId(CommonUtil.generateVJB32digitToken());
		validMerchant.setRefId(dto.getRefId());
		validMerchant.setTimeStamp(System.currentTimeMillis() + "");
//		validMerchant.setExpTime(getUpiDate());
		validMerchant.setExpTime(getBescomUpiDate());
		System.err.println(validMerchant.getRequest());
		return validMerchant;
	}

	public static MerchantCollectMoneyInfoRequest validateMerchantCollectMoneyRequest(String msdId, String metaData)
			throws IOException {
		MerchantCollectMoneyInfoRequest infoRequest = new MerchantCollectMoneyInfoRequest();
		infoRequest.setMsgId(msdId);
		infoRequest.setMetaData(metaData);
		infoRequest.setMetaInfo(UPIRSAEncryption.encryptData(msdId, UPIConstants.PUBLIK_KEY));
		return infoRequest;
	}

	public static MerchantCheckTxnStatus merchantStatusRequest(String msgId) throws Exception {
		MerchantCheckTxnStatus validMerchant = new MerchantCheckTxnStatus();
		String refId = CommonUtil.merchantRefNo();
		AESAlgorithm aesAlgorithm = new AESAlgorithm();
		String decryptedDEK = aesAlgorithm.decryptDEK(UPIConstants.PLAN_DEK, UPIConstants.KEK);

		validMerchant.setMerchantCredentials(
				aesAlgorithm.generateMerchantCredential(decryptedDEK, refId + "#" + UPIConstants.TRANSACTION_PWD));
		validMerchant.setMobileNo(UPIConstants.PAYEE_MOBILE_NUM);
		validMerchant.setMsgId(CommonUtil.generateVJB32digitToken());
		validMerchant.setRefId(refId);
		validMerchant.setTimeStamp(System.currentTimeMillis() + "");
		validMerchant.setTxnId(msgId);
		System.err.println(validMerchant.getRequest());
		return validMerchant;
	}

	public static MerchantStatusMetaInfoRequest merchantTxnStatusRequest(String msdId, String metaData)
			throws IOException {
		System.out.println("Msd ID ************ ********************* " + msdId);
		MerchantStatusMetaInfoRequest infoRequest = new MerchantStatusMetaInfoRequest();
		infoRequest.setMetaData(metaData);
		infoRequest.setMetaInfo(UPIRSAEncryption.encryptData(msdId, UPIConstants.PUBLIK_KEY));
		return infoRequest;
	}
	
	
	// TODO BESCOMS Convert Util
	
	/*public static String generateBescomMerchantKey(String plainDEK) throws Exception {
		AESAlgorithm aesAlgorithm = new AESAlgorithm();
		String decryptedDEK = aesAlgorithm.decryptDEK(plainDEK, UPIBescomContstant.KEK);
		System.out.println("DecryptedDEK:: " + decryptedDEK);
		String merchantCredentials = aesAlgorithm.generateMerchantCredential(decryptedDEK,
				System.currentTimeMillis() + "#" + UPIBescomContstant.TRANSACTION_PWD);
		System.err.println("merchantCredentials " + merchantCredentials);
		return merchantCredentials;
	}*/

	public static GenerateDEKRequest generatBescomDEKRequest(String mobileNo) {
		GenerateDEKRequest dekRequest = new GenerateDEKRequest();
		dekRequest.setMobileNo(mobileNo);
		dekRequest.setMsgId(CommonUtil.generateVJB32digitToken());
		dekRequest.setTimeStamp(CommonUtil.merchantRefNo());
		return dekRequest;
	}

	public static MerchantTokenGenRequest generateBescomMerchantTokenRequest(MerchantCollectMoneyRequest dto) throws Exception {
		MerchantTokenGenRequest genRequest = new MerchantTokenGenRequest();
		String refId = CommonUtil.merchantRefNo();
		AESAlgorithm aesAlgorithm = new AESAlgorithm();
		String decryptedDEK = aesAlgorithm.decryptDEK(dto.getPlanDek(), dto.getKek());
		genRequest.setRefId(refId);
		genRequest.setTimeStamp(CommonUtil.merchantRefNo());
		genRequest.setMsgId(CommonUtil.generateVJB32digitToken());
		genRequest.setMerchantCredentials(aesAlgorithm.generateMerchantCredential(decryptedDEK, refId + "#" + dto.getTxnPwd()));
		genRequest.setMobileNo(dto.getPayeeMobile());
		genRequest.setMerchantId(dto.getMerchantId());
		genRequest.setTerminalId(dto.getTerminalId());
		genRequest.setOrgId(dto.getOrgId());
		genRequest.setTxnPwd(dto.getTxnPwd());
		System.out.println("MerchantTokenGenRequest  ::: ::: ::  " + genRequest.getBescomUpiRequest());
		return genRequest;
	}

	public static MerchantTokenValidRequest tokenValidBescomRequest(String token, MerchantCollectMoneyRequest dto) throws Exception {
		MerchantTokenValidRequest tokenValidRequest = new MerchantTokenValidRequest();
		String refId = CommonUtil.merchantRefNo();
		AESAlgorithm aesAlgorithm = new AESAlgorithm();
		String decryptedDEK = aesAlgorithm.decryptDEK(dto.getPlanDek(), dto.getKek());

		tokenValidRequest.setMerchantCredentials(aesAlgorithm.generateMerchantCredential(decryptedDEK, refId + "#" + dto.getTxnPwd()));
		tokenValidRequest.setMobileNo(dto.getPayeeMobile());
		tokenValidRequest.setMsgId(CommonUtil.generateVJB32digitToken());
		tokenValidRequest.setRefId(refId);
		tokenValidRequest.setTimeStamp(System.currentTimeMillis() + "");
		tokenValidRequest.setToken(aesAlgorithm.generateMerchantCredential(decryptedDEK, token));
		tokenValidRequest.setMerchantId(dto.getMerchantId());
		tokenValidRequest.setOrgId(dto.getOrgId());
		tokenValidRequest.setTxnPwd(dto.getTxnPwd());
		tokenValidRequest.setTerminalId(dto.getTerminalId());
		System.out.println("MerchantTokenValidRequest :: " + tokenValidRequest.getBescomUpiRequest());
		return tokenValidRequest;
	}

	public static MerchantMetaInfoRequest merchantMetaInfoBescomRequest(String msdId, String metaData, String publicKey) throws IOException {
		MerchantMetaInfoRequest infoRequest = new MerchantMetaInfoRequest();
		infoRequest.setMetaData(metaData);
		infoRequest.setMetaInfo(UPIRSAEncryption.encryptData(msdId, publicKey));
		return infoRequest;
	}

	public static VPARequest vpaBescomRequest(MerchantCollectMoneyRequest dto) throws Exception {
		VPARequest vpaRequest = new VPARequest();
		String refId = CommonUtil.merchantRefNo();
		AESAlgorithm aesAlgorithm = new AESAlgorithm();
		String decryptedDEK = aesAlgorithm.decryptDEK(dto.getPlanDek(), dto.getKek());

		vpaRequest.setMerchantCredentials(
				aesAlgorithm.generateMerchantCredential(decryptedDEK, refId + "#" + dto.getTxnPwd()));
		vpaRequest.setMobileNo(dto.getPayeeMobile());
		vpaRequest.setMsgId(CommonUtil.generateVJB32digitToken());
		vpaRequest.setRefId(refId);
		vpaRequest.setTimeStamp(System.currentTimeMillis() + "");
		vpaRequest.setPayerVirAddr(dto.getPayerVirAddr());
		vpaRequest.setTerminalId(dto.getTerminalId());
		vpaRequest.setMerchantId(dto.getMerchantId());
		vpaRequest.setOrgId(dto.getOrgId());
		vpaRequest.setPayeeVirAddr(dto.getPayeeVirAddr());
		System.out.println("VPA Request :: :  " + vpaRequest.getBescomUpiRequest());
		return vpaRequest;
	}

	public static MerchantMetaInfoVPARequest validateMerchantVPABescomRequest(String msdId, String metaData, String publicKey)
			throws IOException {
		MerchantMetaInfoVPARequest infoRequest = new MerchantMetaInfoVPARequest();
		infoRequest.setMetaData(metaData);
		infoRequest.setMetaInfo(UPIRSAEncryption.encryptData(msdId, publicKey));
		return infoRequest;
	}

	public static MerchantCollectMoneyRequest collectMoneyBescomRequest(MerchantCollectMoneyRequest dto) throws Exception {
		MerchantCollectMoneyRequest validMerchant = new MerchantCollectMoneyRequest();
		String refId = CommonUtil.merchantRefNo();
		AESAlgorithm aesAlgorithm = new AESAlgorithm();
		String decryptedDEK = aesAlgorithm.decryptDEK(dto.getPlanDek(), dto.getKek());

		validMerchant.setPayerVirAddr(dto.getPayerVirAddr());
		validMerchant.setAmount(dto.getAmount());
		validMerchant.setMerchantCredentials(aesAlgorithm.generateMerchantCredential(decryptedDEK, refId + "#" + dto.getTxnPwd()));
		validMerchant.setMobileNo(dto.getPayeeMobile());
		validMerchant.setMsgId(CommonUtil.generateVJB32digitToken());
		validMerchant.setRefId(refId);
		validMerchant.setTimeStamp(System.currentTimeMillis() + "");
		validMerchant.setExpTime(getBescomUpiDate());
		validMerchant.setPayeeVirAddr(dto.getPayeeVirAddr());
		validMerchant.setTerminalId(dto.getTerminalId());
		validMerchant.setMerchantId(dto.getMerchantId());
		validMerchant.setOrgId(dto.getOrgId());
		System.err.println(validMerchant.getBescomUpiRequest());
		return validMerchant;
	}

	public static MerchantCollectMoneyInfoRequest validateMerchantCollectMoneyBescomRequest(String msdId, String metaData, String publicKey)
			throws IOException {
		MerchantCollectMoneyInfoRequest infoRequest = new MerchantCollectMoneyInfoRequest();
		infoRequest.setMsgId(msdId);
		infoRequest.setMetaData(metaData);
		infoRequest.setMetaInfo(UPIRSAEncryption.encryptData(msdId, publicKey));
		return infoRequest;
	}

	public static MerchantCheckTxnStatus merchantStatusBescomRequest(String msgId, MerchantCollectMoneyRequest dto) throws Exception {
		MerchantCheckTxnStatus validMerchant = new MerchantCheckTxnStatus();
		String refId = CommonUtil.merchantRefNo();
		AESAlgorithm aesAlgorithm = new AESAlgorithm();
		String decryptedDEK = aesAlgorithm.decryptDEK(dto.getPlanDek(), dto.getKek());

		validMerchant.setMerchantCredentials(
				aesAlgorithm.generateMerchantCredential(decryptedDEK, refId + "#" + dto.getTxnPwd()));
		validMerchant.setMobileNo(dto.getPayeeMobile());
		validMerchant.setMsgId(CommonUtil.generateVJB32digitToken());
		validMerchant.setRefId(refId);
		validMerchant.setTimeStamp(System.currentTimeMillis() + "");
		validMerchant.setTxnId(msgId);
		validMerchant.setMerchantId(dto.getMerchantId());
		validMerchant.setTerminalId(dto.getTerminalId());
		validMerchant.setOrgId(dto.getOrgId());
		System.err.println(validMerchant.getBescomUpiRequest());
		return validMerchant;
	}

	public static MerchantStatusMetaInfoRequest merchantTxnStatusBescomRequest(String msdId, String metaData, String publicKey)
			throws IOException {
		System.out.println("Msd ID ************ ********************* " + msdId);
		MerchantStatusMetaInfoRequest infoRequest = new MerchantStatusMetaInfoRequest();
		infoRequest.setMetaData(metaData);
		infoRequest.setMetaInfo(UPIRSAEncryption.encryptData(msdId, publicKey));
		return infoRequest;
	}

	/**
	 * @return String
	 * @throws ParseException 
	 */
	
	public static String getUpiDate() throws ParseException {
		  DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		  Calendar calendar = Calendar.getInstance();
		  System.out.println("Date : : "+formatter.format(calendar.getTime()));
		  calendar.add(Calendar.MINUTE, 21);
		  System.out.println("Date : : "+formatter.format(calendar.getTime()));
		  return formatter.format(calendar.getTime());
		 }

	
	public static String getBescomUpiDate() throws ParseException {
		DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		DateFormat formatter2 = new SimpleDateFormat("dd-MM-yyyy ");
		String mmm = formatter.format(new Date()).toString();
		System.out.println(mmm);
		String ddd[] = mmm.split("-");
		int day = Integer.parseInt(ddd[0]);
		day = day + 1;
		String hhh[] = mmm.split(":");
		int mm = Integer.parseInt(hhh[1]);
		System.out.println();
		String hrs[] = hhh[0].split(" ");
		String org = formatter2.format(new Date()).toString();
		mm = mm + 3;
		 String orgTm = null;
		
		String yyy[] = ddd[2].split(" ");
		
		if(day>9){
			  orgTm = day +"-" + ddd[1] + "-" + yyy[0] + " " + hrs[1] + ":" + mm;
		}else{
			 orgTm = "0" + day +"-" + ddd[1] + "-" + yyy[0] + " " + hrs[1] + ":" + mm;
		}
		 System.out.println(orgTm);
		return orgTm;
	}
	
	
	
	public static MerchantMetaVPAResponse getUPIresponse(String output){
		MerchantMetaVPAResponse result = new MerchantMetaVPAResponse();
		try{
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			InputStream inputStream = new ByteArrayInputStream(output.getBytes());
			org.w3c.dom.Document doc = builder.parse(inputStream);
			NodeList nodes = doc.getElementsByTagName("upi:req");
			System.out.println("node :: " + nodes.getLength());
			String respCode = "";
			String respDesc = "";
			String refNo = "";
			String orgTxnId = "";
			String txnId = "";
			for (int i = 0; i < nodes.getLength(); i++) {
				Element element = (Element) nodes.item(i);
				respCode = element.getElementsByTagName("java:ResCode").item(0).getFirstChild().getTextContent();
//				msgId = element.getElementsByTagName("java:MsgId").item(0).getFirstChild().getTextContent();
				txnId = element.getElementsByTagName("java:TxnRefId").item(0).getFirstChild().getTextContent();
				refNo = element.getElementsByTagName("java:OrgTxnRefId").item(0).getFirstChild().getTextContent();
				respDesc = element.getElementsByTagName("java:ResDesc").item(0).getFirstChild().getTextContent();
				if(respCode.equals("000")){
					refNo = element.getElementsByTagName("java:OrgTxnRefId").item(0).getFirstChild().getTextContent();
					orgTxnId = element.getElementsByTagName("java:OrgTxnId").item(0).getFirstChild().getTextContent();
				}
			}
			if(respCode.equals("000")){
				if(respDesc.equals("Your transaction has been approved.")){
					result.setMessage(respDesc);
					result.setReferenceNo(refNo);
					result.setOrgTxnId(orgTxnId);
					result.setTxnId(txnId);
//					result.setDesEncryptedKey(DesEncryption.desEncrypt(request.getRequest(), msgId));
					result.setStatus(ResponseStatus.SUCCESS);
				}else{
					result.setMessage("The Collect Money request has been declined successfully.");
					result.setReferenceNo(refNo);
					result.setOrgTxnId(orgTxnId);
					result.setTxnId(txnId);
					result.setStatus(ResponseStatus.FAILURE);
					result.setMessage(respDesc);
				}
			}else{
				result.setMessage(respDesc);
				result.setReferenceNo(refNo);
				result.setOrgTxnId(orgTxnId);
				result.setTxnId(txnId);
				result.setStatus(ResponseStatus.FAILURE);
				result.setMessage(respDesc);
			}
		}catch(Exception e){
			e.printStackTrace();
			result.setMessage("Internal Server Error");
			result.setStatus(ResponseStatus.FAILURE);
		}
		return result;
	}

	public static String convertEnginId(String engineId){
		String eng = "";
		if(engineId.equals("0")){
			eng = EngineID.Indigo.name();
		}else if(engineId.equals("1")){
			eng = EngineID.Spicjet.name();
		}else if(engineId.equals("5")){
			eng = EngineID.GoAir.name();
		}else if(engineId.equals("7")){
			eng = EngineID.TravelPort.name();
		}else if(engineId.equals("10")){
			eng = EngineID.AirAsia.name();
		}else if(engineId.equals("6")){
			eng = EngineID.AirCosta.name();
		}else if(engineId.equals("11")){
			eng = EngineID.TravelPort.name();
		}
		return eng;
	}
	
	public static String convertCabin(String cabin){
		String eng = "";
		if(cabin.equalsIgnoreCase("Economy")){
			eng = Cabin.Economy.name();
		}else if(cabin.equalsIgnoreCase("RoundTrip")){
			eng = Cabin.RoundTrip.name();
		}else if(cabin.equalsIgnoreCase("Business")){
			eng = Cabin.Business.name();
		}
		return eng;
	}
	
	public static String convertPaxType(String paxType){
		String eng = "";
		if(paxType.equals("0")){
			eng = PaxType.ADT.name();
		}else if(paxType.equals("1")){
			eng = PaxType.CHD.name();
		}else if(paxType.equals("2")){
			eng = PaxType.INF.name();
		}
		return eng;
	}
	
	public static String removeLastString(String str){
		str = str.substring(0, str.length() - 1);
		System.out.println(str);
		return str;
	}
	
	public static S2SRequest convertS2SRequest(String key, String txnid, String refId, String status, String code, double billamount,
			double additionalcharges, String phone, String email, String transactiondatetime, String mhash, String s2sCallUrl){
		S2SRequest dto = new S2SRequest();
		dto.setKey(key);
		dto.setMerchanttxnid(txnid);
		dto.setPgtransactionid(refId);
		dto.setStatus(status);
		dto.setCode(code);
		dto.setBillamount(String.format("%.2f", billamount));
		dto.setAdditionalcharges(String.format("%.2f", additionalcharges));
		dto.setPhone(phone);
		dto.setEmail(email);
		dto.setTransactiondatetime(transactiondatetime);
		dto.setHash(mhash);
		dto.setS2sCallUrl(s2sCallUrl);
		return dto;
	}
	
	public static double convertAdditionalAmount(double addAmount){
		if(addAmount >=2000){
			return 6;
		}
		return 0;
	}
	
	public static String encodeBase64(byte[] data){
		return java.util.Base64.getEncoder().encodeToString(data);
	}

	public static String decodeBase64String(String encodedString) throws IOException{
		return new String(SecurityUtil.decodeBase64(encodedString), StandardCharsets.UTF_8);
	}
					
	public static UpiSdkCredentials getSDKCredentials(){
		UpiSdkCredentials sdk = new UpiSdkCredentials();
		sdk.setMerKek(UPIConstants.KEK);
		sdk.setMerchantVPA(UPIConstants.PAYEE_VIR_ADDR);
		sdk.setMerTxnCurrency(UPIConstants.CURRENCY_CODE);
		sdk.setMerTerminalId(UPIConstants.TERMINAL_ID);
		sdk.setMerDeviceInternet(UPIConstants.DEV_IP);
		sdk.setMerTransPassword(UPIConstants.TRANSACTION_PWD);
		sdk.setMerchantId(UPIConstants.MERCHANT_ID);
		sdk.setMerPayeeCode(UPIConstants.PAYEE_CODE);
		sdk.setMerPaymentType("Pay");
		sdk.setMerSubMerchantId(UPIConstants.SUB_MERCHANT_ID);
		sdk.setMerDek(UPIConstants.PLAN_DEK);
		return sdk;
	}
}
