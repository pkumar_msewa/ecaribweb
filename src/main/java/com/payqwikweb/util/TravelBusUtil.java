package com.payqwikweb.util;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.payqwikweb.app.model.busdto.BookTicketReq;
import com.payqwikweb.app.model.busdto.CancelPolicyListDTO;
import com.payqwikweb.app.model.busdto.GSTDetailDTO;
import com.payqwikweb.app.model.busdto.GetTransactionId;
import com.payqwikweb.app.model.busdto.SaveSeatDetailsDTO;
import com.payqwikweb.app.model.busdto.TravellerDetailsDTO;
import com.payqwikweb.app.model.request.bus.GetSeatDetails;
import com.payqwikweb.app.model.request.bus.ListOfAvailableTrips;

public class TravelBusUtil {
	
	
	public static String getSessionIdDec(String decData) {

		String sessionId=null;

		try {
			JSONObject jObj=new JSONObject(decData);
			sessionId=jObj.getString("sessionId");
			return sessionId;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return sessionId;
	}

	public static ListOfAvailableTrips getAllAvailableTripsDec(String decData) {

		ListOfAvailableTrips req= new ListOfAvailableTrips();

		try {
			JSONObject jObj=new JSONObject(decData);
			req.setSourceId(jObj.getString("sourceId"));
			req.setDestinationId(jObj.getString("destinationId"));
			req.setDate(jObj.getString("date"));
			req.setSessionId(jObj.getString("sessionId"));
			return req;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return req;
	}

	public static GetSeatDetails getSeatDetailsDec(String decData) {

		GetSeatDetails req= new GetSeatDetails();

		try {
			JSONObject jObj=new JSONObject(decData);
			req.setBusId(jObj.getString("busId"));
			req.setSeater(jObj.getBoolean("seater"));
			req.setSleeper(jObj.getBoolean("sleeper"));
			req.setJourneyDate(jObj.getString("journeyDate"));
			req.setEngineId(jObj.getInt("engineId"));
			req.setSessionId(jObj.getString("sessionId"));
			return req;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return req;
	}


	public static GetTransactionId getTxnIdDec(String decData) {

		GetTransactionId req= new GetTransactionId();

		List<TravellerDetailsDTO> travellers=new ArrayList<>();
		GSTDetailDTO gstDetails=new GSTDetailDTO();
		List<CancelPolicyListDTO> cancelPolicyList=new ArrayList<>();
		
		try {
			JSONObject jObj=new JSONObject(decData);

			req.setMobileNo(jObj.getString("mobileNo"));
			req.setEmailId(jObj.getString("emailId"));
			req.setIdProofId(jObj.getString("idProofId"));
			req.setIdProofNo(jObj.getString("idProofNo"));
			req.setAddress(jObj.getString("address"));
			req.setEngineId(jObj.getString("engineId"));
			req.setBusid(jObj.getString("busid"));
			req.setBusType(jObj.getString("busType"));
			req.setBoardId(jObj.getString("boardId"));
			req.setBoardLocation(jObj.getString("boardLocation"));
			req.setBoardName(jObj.getString("boardName"));
			req.setArrTime(jObj.getString("arrTime"));
			req.setDepTime(jObj.getString("depTime"));
			req.setTravelName(jObj.getString("travelName"));
			req.setSource(jObj.getString("source"));
			req.setDestination(jObj.getString("destination"));
			req.setSourceid(jObj.getString("sourceid"));
			req.setDestinationId(jObj.getString("destinationId"));
			req.setJourneyDate(jObj.getString("journeyDate"));
			req.setBoardpoint(jObj.getString("boardpoint"));
			req.setBoardprime(jObj.getString("boardprime"));
			req.setBoardTime(jObj.getString("boardTime"));
			req.setBoardlandmark(jObj.getString("boardlandmark"));
			req.setBoardContactNo(jObj.getString("boardContactNo"));
			req.setDropId(jObj.getString("dropId"));
			req.setDropName(jObj.getString("dropName"));
			req.setDroplocatoin(jObj.getString("droplocatoin"));
			req.setDropprime(jObj.getString("dropprime"));
			req.setDropTime(jObj.getString("dropTime"));
			req.setRouteId(jObj.getString("routeId"));
			req.setSeatDetail(jObj.getString("seatDetail"));
			req.setCouponCode(jObj.getString("couponCode"));
			req.setDiscount(jObj.getDouble("discount"));
			req.setCommission(jObj.getDouble("commission"));
			req.setMarkup(jObj.getDouble("markup"));
			req.setwLCode(jObj.getString("wLCode"));
			req.setIpAddress(jObj.getString("ipAddress"));
			req.setVersion(jObj.getString("version"));
			req.setAgentCode(jObj.getString("agentCode"));

			if(jObj.getString("travellers")!=null && !jObj.getString("travellers").equalsIgnoreCase("null"))
			{
				JSONArray travellersArr= jObj.getJSONArray("travellers");

				for (int i = 0; i < travellersArr.length(); i++) {

					if (travellersArr.getString(i)!=null && !travellersArr.getString(i).equalsIgnoreCase("null")) {
						TravellerDetailsDTO dto=new TravellerDetailsDTO();
						JSONObject travellersObj=travellersArr.getJSONObject(i);

						dto.setTitle(travellersObj.getString("title"));
						dto.setfName(travellersObj.getString("fName"));
						dto.setmName(travellersObj.getString("mName"));
						dto.setlName(travellersObj.getString("lName"));
						dto.setAge(travellersObj.getString("age"));
						dto.setGender(travellersObj.getString("gender"));
						dto.setSeatNo(travellersObj.getString("seatNo"));
						dto.setSeatType(travellersObj.getString("seatType"));
						dto.setFare(travellersObj.getString("fare"));
						dto.setSeatId(travellersObj.getString("seatId"));

						travellers.add(dto);
					}
				}
			}

			if(jObj.getString("gstDetails")!=null && !jObj.getString("gstDetails").equalsIgnoreCase("null"))
			{
				JSONObject gstDetailsObj= jObj.getJSONObject("gstDetails");

				gstDetails.setPhone(gstDetailsObj.getString("phone"));
				gstDetails.setEmail(gstDetailsObj.getString("email"));
				gstDetails.setCompanyName(gstDetailsObj.getString("companyName"));
				gstDetails.setAddress(gstDetailsObj.getString("address"));
				gstDetails.setGstNumber(gstDetailsObj.getString("gstNumber"));
			
			}

			if(jObj.getString("cancelPolicyList")!=null && !jObj.getString("cancelPolicyList").equalsIgnoreCase("null"))
			{
				JSONArray cancelPolicyListArr= jObj.getJSONArray("cancelPolicyList");

				for (int i = 0; i < cancelPolicyListArr.length(); i++) {

					if (cancelPolicyListArr.getString(i)!=null && !cancelPolicyListArr.getString(i).equalsIgnoreCase("null")) {
					
						CancelPolicyListDTO dto=new CancelPolicyListDTO();
						
						JSONObject cancelPolicyListObj=cancelPolicyListArr.getJSONObject(i);

						dto.setTimeFrom(cancelPolicyListObj.getDouble("timeFrom"));
						dto.setTimeTo(cancelPolicyListObj.getDouble("timeTo"));
						dto.setPercentageCharge(cancelPolicyListObj.getDouble("percentageCharge"));
						dto.setFlatCharge(cancelPolicyListObj.getDouble("flatCharge"));
						dto.setFlat(cancelPolicyListObj.getBoolean("flat"));
						dto.setSeatno(cancelPolicyListObj.getString("seatno"));
						
						cancelPolicyList.add(dto);
					}
				}
			}

			req.setTravellers(travellers);
			req.setGstDetails(gstDetails);
			req.setCancelPolicyList(cancelPolicyList);
			req.setSessionId(jObj.getString("sessionId"));
			req.setTripId(jObj.getString("tripId"));
			
			return req;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return req;
	}

	
	public static BookTicketReq bookTicketDec(String decData) {

		BookTicketReq req= new BookTicketReq();

		try {
			JSONObject jObj=new JSONObject(decData);
			req.setTransactionId(jObj.getString("transactionId"));
			req.setSeatHoldId(jObj.getString("seatHoldId"));
			req.setAmount(jObj.getDouble("amount"));
			req.setSessionId(jObj.getString("sessionId"));
			
			return req;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return req;
	}

	
	public static SaveSeatDetailsDTO saveSeatDetailsDTODec(String decData) {

		SaveSeatDetailsDTO req= new SaveSeatDetailsDTO();

		try {
			
			JSONObject jObj=new JSONObject(decData);
			req.setSessionId(jObj.getString("sessionId"));
			req.setBusType(jObj.getString("busType"));
			req.setTravelName(jObj.getString("travelName"));
			req.setJourneyDate(jObj.getString("journeyDate"));
			req.setSeatDetail(jObj.getString("seatDetail"));
			req.setTotalFare(jObj.getDouble("totalFare"));  
			req.setArrTime(jObj.getString("arrTime"));
			req.setDepTime(jObj.getString("depTime"));  
			req.setSource(jObj.getString("source"));
		    req.setDestination(jObj.getString("destination"));
		    req.setBoardTime(jObj.getString("boardTime"));
		    req.setBusid(jObj.getString("busid"));
		    req.setBoardId(jObj.getString("boardId"));
		    req.setBoardLocation(jObj.getString("boardLocation"));
		    req.setBoardContactNo(jObj.getString("boardContactNo"));
			
			return req;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return req;
	}

}
