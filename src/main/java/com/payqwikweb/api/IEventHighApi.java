package com.payqwikweb.api;

import com.payqwikweb.model.app.request.EventRequest;
import com.payqwikweb.model.app.response.EventHighResponse;

public interface IEventHighApi {
	
	EventHighResponse eventlist(EventRequest request);

}
