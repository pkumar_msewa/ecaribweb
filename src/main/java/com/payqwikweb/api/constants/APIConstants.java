package com.payqwikweb.api.constants;

public class APIConstants {

		public static final String EBS_VERIFICATION = "https://api.secure.ebs.in/api/1_0";
		public static final String DUMMY_HASH = "#(*@&(*#(#*&)@(&)$)#(&)@()@!()#&(#^($$(*$(*#(@*)(@*)(!)&#((#*(@*@)&##";
		public static final String EBS_STATUS="https://api.secure.ebs.in/api/status";
//		public static final String VNET_VERIFICATION_URL = "https://www.vijayabankonline.in:443/NASApp/BANA623WAR/BANKAWAY?Action.ShoppingMall.Login.Init=Y&BankId=029%20&MD=V&CRN=INR&CG=Y&USER_LANG_ID=001&UserType=1&AppType=corporate";
		public static final String VNET_VERIFICATION_URL = "https://172.16.7.34:443/NASApp/BANA623WAR/BANKAWAY?Action.ShoppingMall.Login.Init=Y&BankId=029%20&MD=V&CRN=INR&CG=Y&USER_LANG_ID=001&UserType=1&AppType=corporate";
		public static final String VNET_PID = "10420249";
	}

