package com.payqwikweb.api;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.codehaus.jettison.json.JSONObject;
import org.json.JSONArray;

import com.payqwikweb.app.model.request.TravelFlightRequest;
import com.payqwikweb.app.model.response.FlightResponse;

public interface IFlightApi {

	JSONObject getFlightSerachJson(TravelFlightRequest req);

	ArrayList<FlightResponse> getFlightListData(JSONArray jsonArray);

	Map<String, Object> getFlightMapData(JSONArray jsonArray);

	List<String> getPassengerList(String passenger);

	String getMinAmount(ArrayList<FlightResponse> oneWayFlights, ArrayList<FlightResponse> roundWayFlights);

	Object getMaxAmount(ArrayList<FlightResponse> oneWayFlights, ArrayList<FlightResponse> roundWayFlights);
	
}
