package com.payqwikweb.api;

import com.payqwikweb.model.app.request.AdlabsAccessTokenRequest;
import com.payqwikweb.model.app.request.AdlabsTicketRequest;
import com.payqwikweb.model.app.response.AdlabOrderCreateRequest;
import com.payqwikweb.model.app.response.AdlabsAccessTokenResponse;
import com.payqwikweb.model.app.response.AdlabsAmountInitateRequest;
import com.payqwikweb.model.app.response.AdlabsAmountInitateResponse;
import com.payqwikweb.model.app.response.AdlabsOrderCreateResponse;
import com.payqwikweb.model.app.response.AdlabsOrderVoucher;
import com.payqwikweb.model.app.response.AdlabsPaymentRequest;
import com.payqwikweb.model.app.response.AdlabsProceedResponse;
import com.payqwikweb.model.app.response.AdlabsTicketListResponse;
import com.payqwikweb.model.app.response.GCIOrderVoucher;
import com.payqwikweb.model.app.response.GCIProceedResponse;

public interface IAdlabsAPI {
	AdlabsAccessTokenResponse auth(AdlabsAccessTokenRequest request);
	AdlabsTicketListResponse ticketList(AdlabsTicketRequest request);
	AdlabsOrderCreateResponse ordercreate(AdlabOrderCreateRequest request);
	AdlabsOrderCreateResponse payment(AdlabsPaymentRequest request);
	AdlabsAmountInitateResponse initate(AdlabsAmountInitateRequest request);
	AdlabsProceedResponse sucees(AdlabsOrderVoucher request);
	
	
	
}
