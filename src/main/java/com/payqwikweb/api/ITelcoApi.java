package com.payqwikweb.api;

import com.payqwikweb.model.app.request.TelcoRequest;
import com.payqwikweb.model.app.response.TelcoReponse;

public interface ITelcoApi {

	TelcoReponse request(TelcoRequest req);
}
