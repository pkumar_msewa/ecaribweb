package com.payqwikweb.api.impl;

import org.codehaus.jettison.json.JSONObject;

import com.payqwikweb.api.IServiceCheckApi;
import com.payqwikweb.app.metadatas.UrlMetadatas;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Language;
import com.payqwikweb.app.model.ResponseStatus;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.Version;
import com.payqwikweb.app.model.response.bus.ResponseDTO;
import com.payqwikweb.app.utils.SecurityUtils;
import com.payqwikweb.model.web.Status;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;

public class ServiceCheckApi implements IServiceCheckApi {
	

	@Override
	public String checkServiceActive(String sessionId) {

		String service="";
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", sessionId);

			Client vpqClient = Client.create();
			vpqClient.addFilter(new LoggingFilter(System.out));
			WebResource vpqWebResource = vpqClient.resource(
					UrlMetadatas.checkBusService(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse vpqresponse = vpqWebResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = vpqresponse.getEntity(String.class);
			JSONObject jobj=new JSONObject(strResponse);
			final String code=jobj.getString("code");
			if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
				String strDetails=jobj.getString("details");
				service=strDetails;
			}
			
		} catch (Exception e) {
			System.out.println(e);
			
		}
		return service;
	}
	@Override
	public String agentCheckServiceActive(String sessionId) {

		String service="";
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", sessionId);

			Client vpqClient = Client.create();
			vpqClient.addFilter(new LoggingFilter(System.out));
			WebResource vpqWebResource = vpqClient.resource(
					UrlMetadatas.checkBusService(Version.VERSION_1, Role.AGENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse vpqresponse = vpqWebResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);

			String strResponse = vpqresponse.getEntity(String.class);
			System.out.println(strResponse);
			JSONObject jobj=new JSONObject(strResponse);
			final String code=jobj.getString("code");
			if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
				String strDetails=jobj.getString("details");
				System.err.println("");
				service=strDetails;
			}
			
		} catch (Exception e) {
			System.out.println(e);
			
		}
		return service;
	}
}
