package com.payqwikweb.api.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.payqwikweb.api.IGifCartAPI;
import com.payqwikweb.app.metadatas.UrlMetadatas;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Language;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.Version;
import com.payqwikweb.app.utils.SecurityUtils;
import com.payqwikweb.model.app.request.GCIAddCartRequest;
import com.payqwikweb.model.app.request.GCIAddOrderRequest;
import com.payqwikweb.model.app.request.GCIAmountInitateRequest;
import com.payqwikweb.model.app.request.GCIBrandStoreRequest;
import com.payqwikweb.model.app.request.GCIOrderVoucherRequest;
import com.payqwikweb.model.app.request.GCIProceedRequest;
import com.payqwikweb.model.app.request.GetBrandDenominationRequest;
import com.payqwikweb.model.app.response.GCIAddCartResponse;
import com.payqwikweb.model.app.response.GCIAddOrderResponse;
import com.payqwikweb.model.app.response.GCIAmountInitateResponse;
import com.payqwikweb.model.app.response.GCIBrandDenominationResponse;
import com.payqwikweb.model.app.response.GCIBrandStoreResponse;
import com.payqwikweb.model.app.response.GCIOrderVoucher;
import com.payqwikweb.model.app.response.GCIOrderVoucherResponse;
import com.payqwikweb.model.app.response.GCIProceedResponse;
import com.payqwikweb.util.APIUtils;
import com.payqwikweb.util.LogCat;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;

public class GifCartAPI implements IGifCartAPI {

	public String GetaccessToken() {
		String acessToken = "";
		try {
			Client client = Client.create();
			WebResource webResource = client.resource("http://interface.giftcardsindia.in/api/access-token/get?id=68ce6e0a66ae9b3b12d83f96658d827e&key=121b4b4ce66f14177905e36233a8dff13a951e305a81f395ccb049b35925e610");

			//"http://interface.giftcardsindia.in/api/access-token/get?id=68ce6e0a66ae9b3b12d83f96658d827e&key=121b4b4ce66f14177905e36233a8dff13a951e305a81f395ccb049b35925e610");
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.get(ClientResponse.class);
			String strResponse = response.getEntity(String.class);
			LogCat.print("Response : : " + strResponse);
			if (response.getStatus() != 200) {
				LogCat.print("Response :: " + response.getStatus());

			} else {

				JSONObject val = new JSONObject(strResponse);
				
				acessToken = val.getString("accessToken");
			}

		} catch (Exception e) {
			// TODO: handle exception
		}
		return acessToken;
	}

	@Override
	public String GetBrand(String acessToken) {
		String getbrands = "";
		try {

			Client client = Client.create();
			WebResource webResource = client
					.resource("http://interface.giftcardsindia.in/api/channel/brands?accessToken=" + acessToken);
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.get(ClientResponse.class);
			String strResponse = response.getEntity(String.class);
			LogCat.print("Response : : " + strResponse);
			if (response.getStatus() != 200) {
				LogCat.print("Response :: " + response.getStatus());

			} else {

				getbrands = strResponse;
			}

		} catch (Exception e) {
			// TODO: handle exception
		}
		return getbrands;
	}

	@Override
	public GCIBrandDenominationResponse denomation(GetBrandDenominationRequest request) {
		GCIBrandDenominationResponse denomation = new GCIBrandDenominationResponse();
		try {
			Client client = Client.create();
			WebResource webResource = client.resource("http://interface.giftcardsindia.in/api/brand/denominations?accessToken="
							+ request.getAccessToken() + "&hash=" + request.getHash() + "");
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.get(ClientResponse.class);
			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
			}
			String output = response.getEntity(String.class);
			System.err.println("RESPONSE:: " + output);
			denomation.setMessage(output);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return denomation;
	}

	@Override
	public GCIAddCartResponse addcart(GCIAddCartRequest request) {
		GCIAddCartResponse resp = new GCIAddCartResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("productType", request.getProductType());
			payload.put("quantity", "1");
			payload.put("amount", request.getAmount());
			payload.put("brandName", request.getBrandName());
			payload.put("sessioniId", request.getSessioniId());
			payload.put("brandHash", request.getBrandHash());
			payload.put("imagepath", request.getImagepath());
			payload.put("productId", request.getProductid());
			payload.put("skuId", request.getSkuId());

			Client client = Client.create();
			WebResource webResource = client
					.resource(UrlMetadatas.giftCartURL(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.header("hash", SecurityUtils.getHash(payload.toString()))
					.post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			System.err.println("ADD CART API RESPONSE:::::::::::" + strResponse);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");

			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						final Object details = (Object) jobj.get("details");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
		}
		return resp;
	}

	@Override
	public GCIBrandStoreResponse brandstore(GCIBrandStoreRequest request) {
		GCIBrandStoreResponse brandstore = new GCIBrandStoreResponse();

		try {

			Client client = Client.create();
			WebResource webResource = client
					.resource("http://interface.giftcardsindia.in/api/brand/denominations?accessToken="
							+ request.getAccessToken() + "&hash=" + request.getHash() + "");

			ClientResponse response = webResource.accept("application/json").type("application/json")
					.get(ClientResponse.class);

			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
			}

			String output = response.getEntity(String.class);

			brandstore.setMessage(output);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return brandstore;

	}

	@Override
	public GCIOrderVoucherResponse voucher(GCIOrderVoucherRequest request) {
		GCIOrderVoucherResponse voucher = new GCIOrderVoucherResponse();

		try {

			System.err.println("RECEPIET NO:::::::::::::"+request.getReceiptNo());
			System.err.println("ACCESS TOKEN :::::::::::::::::::"+request.getAccessToken());

			Client client = Client.create();
			WebResource webResource = client.resource("http://interface.giftcardsindia.in/api/order/get/"+ request.getReceiptNo()+ "?accessToken=" + request.getAccessToken() + "");

			ClientResponse response = webResource.accept("application/json").type("application/json")
					.get(ClientResponse.class);
			System.err.println("VOUCHER API RESPONSE::::::::::"+response);
			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
			}

			String output = response.getEntity(String.class);
			System.err.println("VOUCHER API RESPONSE::::::::::"+output);

			voucher.setMessage(output);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return voucher;

	}

	@Override
	public GCIAddCartResponse showcart(GCIAddCartRequest request) {
		GCIAddCartResponse resp = new GCIAddCartResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("productType", request.getProductType());
			payload.put("quantity", request.getQuantity());
			payload.put("amount", request.getAmount());
			payload.put("brandName", request.getBrandName());
			payload.put("denomination", request.getDenomination());
			payload.put("sessioniId", request.getSessioniId());
			payload.put("brandHash", request.getBrandHash());
			payload.put("imagepath", request.getImagepath());
			payload.put("productid", request.getProductid());
			payload.put("skuId", request.getSkuId());
			Client client = Client.create();
			WebResource webResource = client
					.resource(UrlMetadatas.cartSHOWURL(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.header("hash", SecurityUtils.getHash(payload.toString()))
					.post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						final Object details = (Object) jobj.get("details");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public GCIProceedResponse proceed(GCIProceedRequest request) {
		GCIProceedResponse resp = new GCIProceedResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessioniId", request.getSessionId());
			payload.put("skuId", request.getSkuId());
			payload.put("productId", request.getProductId());
			payload.put("amount", request.getAmount());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.itemRemoveURL(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.header("hash", SecurityUtils.getHash(payload.toString()))
					.post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						final Object details = (Object) jobj.get("details");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public GCIAddOrderResponse addOrder(GCIAddOrderRequest request) {
		GCIAddOrderResponse addOrder = new GCIAddOrderResponse();
		try {

			String today="";
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			Date date = new Date();
			today=(dateFormat.format(date));
			System.err.println("DATE:::::::::"+today);
			JSONObject jo = new JSONObject();
			jo.put("brandHash", request.getBrandHash());
			jo.put("denomination", request.getDenomination());
			jo.put("productType", request.getProductType());
			jo.put("quantity", "1");
			JSONArray ja = new JSONArray();
			ja.put(jo);
			MultivaluedMapImpl formData = new MultivaluedMapImpl();
			formData.add("orderDate",today);
			formData.add("billingName", request.getBillingName());
			formData.add("billingEmail", request.getBillingEmail());
			formData.add("billingAddressLine1", request.getBillingAddressLine1());
			formData.add("billingCity", request.getBillingCity());
			formData.add("billingCountry", request.getBillingCountry());
			formData.add("billingState", "Karanataka");
			formData.add("billingZip", "560068");
			formData.add("receiversName", "sonugupta");
			formData.add("receiversEmail", "sonuarahan@gmail.com");
			formData.add("shippingName", "rajeshgupta");
			formData.add("shippingEmail", "rajeshgupta@gmail.com");
			formData.add("shippingCity", "Bangalore");
			formData.add("shippingState", "Karanataka");
			formData.add("shippingCountry", "India");
			formData.add("shippingAddressLine1", "BangaloreBTM");
			formData.add("shippingZip", "560068");
			formData.add("clientOrderId", request.getClientOrderId());

			formData.add("products", ja);
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(
					"http://interface.giftcardsindia.in/api/order/add?accessToken=" + request.getAccessToken() + "");
			ClientResponse response = webResource.post(ClientResponse.class, formData);

			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
			}

			String output = response.getEntity(String.class);
			System.err.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@2OUTPUT FROM ADD ORDER API:::::::::::::::::::::::::::::::::: "+response.getStatus());
			//{"error":true,"message":"No voucher available "}
			System.err.println("OUTPUT FROM ADD ORDER API:::::::::::::::::::::::::::::::::: "+output);

			addOrder.setMessage(output);

			//addOrder.getError();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return addOrder;

	}

	@Override
	public GCIAmountInitateResponse initate(GCIAmountInitateRequest request) {
		GCIAmountInitateResponse resp = new GCIAmountInitateResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessioniId", request.getSessioniId());
			payload.put("amount", request.getAmount());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.paymentInitateURL(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.header("hash", SecurityUtils.getHash(payload.toString()))
					.post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			System.err.println("Response :: : " + strResponse);
			if (response.getStatus() != 200) {
				org.json.JSONObject jobj = new org.json.JSONObject(strResponse);


				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage(strResponse);
				//resp.setDetails(details);
				System.err.println("INITATE DETAIL ======+++"+resp.getDetails());
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						String txnId = jobj.getString("txnId");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							//org.json.JSONObject jobj = new org.json.JSONObject(strResponse);

							final String details = (String) jobj.get("details");
							System.err.println("+++++++++++++++=======DETAILS:::::::::::======="+details);
							resp.setDetails(details);
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setTxnId(txnId);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public GCIProceedResponse sucees(GCIOrderVoucher request) {
		GCIProceedResponse resp = new GCIProceedResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessioniId", request.getSessioniId());
			payload.put("brandName", request.getBrandName());
			payload.put("amount", request.getAmount());
			payload.put("voucherNumber", request.getVoucherNumber());
			payload.put("voucherPin", request.getVoucherPin());
			payload.put("expiryDate", request.getExpiryDate());
			payload.put("receiptno", request.getReceiptno());
			payload.put("transactionRefNo", request.getTransactionRefNo());
			payload.put("code", request.getCode());


			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.paymentsuccessURL(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.header("hash", SecurityUtils.getHash(payload.toString()))
					.post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						String txnId = jobj.getString("txnId");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {

							final String details = (String) jobj.get("details");
							System.err.println("+++++++++++++++=======DETAILS:: From SUCCESS:::::::::======="+details);
							resp.setDetails(details);
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setTxnId(txnId);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}
}