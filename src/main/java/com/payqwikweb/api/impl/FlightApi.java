package com.payqwikweb.api.impl;

import java.net.Inet4Address;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.payqwikweb.api.IFlightApi;
import com.payqwikweb.app.metadatas.UrlMetadatas;
import com.payqwikweb.app.model.EngineID;
import com.payqwikweb.app.model.flight.dto.FlightLegresArr;
import com.payqwikweb.app.model.request.TravelFlightRequest;
import com.payqwikweb.app.model.response.FlightResponse;

public class FlightApi implements IFlightApi {

	@SuppressWarnings("unchecked")
	@Override
	public org.codehaus.jettison.json.JSONObject getFlightSerachJson(TravelFlightRequest req) {
		org.codehaus.jettison.json.JSONObject payload = new org.codehaus.jettison.json.JSONObject();
		try {
			org.json.simple.JSONArray flightSearchDetails = new org.json.simple.JSONArray();
			org.codehaus.jettison.json.JSONObject flightSearchDetailsobj = new org.codehaus.jettison.json.JSONObject();
			org.codehaus.jettison.json.JSONObject roundtripDetailsobj = new org.codehaus.jettison.json.JSONObject();
			payload.put("clientIp", Inet4Address.getLocalHost().getHostAddress());
			payload.put("cllientKey", UrlMetadatas.MDEX_CLIENTKEY);
			payload.put("clientToken", UrlMetadatas.MDEX_CLIENTTOKEN);
			payload.put("clientApiName", "Flight Search");

			flightSearchDetailsobj.put("origin", req.getOrigin());
			flightSearchDetailsobj.put("destination", req.getDestination());
			flightSearchDetailsobj.put("beginDate",req.getBeginDate());

			flightSearchDetails.add(flightSearchDetailsobj);
			if (req.getTripType().equals("RoundTrip")) {
				roundtripDetailsobj.put("origin", req.getDestination());
				roundtripDetailsobj.put("destination", req.getOrigin());
				roundtripDetailsobj.put("beginDate",req.getEndDate());
				flightSearchDetails.add(roundtripDetailsobj);
			}

			ArrayList<String> objhasmap = new ArrayList<>();
			objhasmap.add("" + EngineID.Indigo);
			objhasmap.add("" + EngineID.AirAsia);
			objhasmap.add("" + EngineID.AirCosta);
			objhasmap.add("" + EngineID.GoAir);
			objhasmap.add("" + EngineID.Spicjet);
			objhasmap.add("" + EngineID.Trujet);
			objhasmap.add("" + EngineID.TravelPort);
			payload.put("engineIDs", objhasmap);
			payload.put("flightSearchDetails", flightSearchDetails);
			payload.put("tripType", req.getTripType());
			payload.put("cabin", req.getCabin());
			payload.put("adults", req.getAdults());
			payload.put("childs", req.getChilds());
			payload.put("infants", req.getInfants());
			payload.put("traceId", "AYTM00011111111110002");
				
		} catch (Exception e) {
			e.printStackTrace();
		}
		return payload;		
}

	@Override
	public ArrayList<FlightResponse> getFlightListData(JSONArray jsonArray) {
		return null;
	}

	@Override
	public Map<String, Object> getFlightMapData(JSONArray segmentArray) {
		Map<String, Object> flightMap = new HashMap<>();
		LinkedHashSet<String> flightNumbers = new LinkedHashSet<String>();
		LinkedHashSet<String> flightNames = new LinkedHashSet<String>();
		HashSet<String> noOfStopage = new HashSet<String>();
		try {
			ArrayList<FlightResponse> flightList = new ArrayList<FlightResponse>();
			if (segmentArray != null && segmentArray.length() > 0) {

				// for segment iteration
				for (int segment = 0; segment < segmentArray.length(); segment++) {
					JSONObject segmentObj = segmentArray.getJSONObject(segment);
					Iterator<?> jsonKey = segmentObj.keys();
					FlightResponse flightObj = new FlightResponse();
					while (jsonKey.hasNext()) {
						String segmentKey = (String) jsonKey.next();
						if (segmentObj.get(segmentKey) instanceof JSONArray) {
							flightObj.setItineraryKey(segmentArray.getJSONObject(segment).getString("itineraryKey"));
							flightObj.setSearchId(segmentArray.getJSONObject(segment).getString("searchId"));
							flightObj.setEngineID(segmentArray.getJSONObject(segment).getString("engineID"));
							// bond iteration for every segment
							if ("bonds".equals(segmentKey)) {
								JSONArray bondArray = segmentObj.getJSONArray(segmentKey);
								if (bondArray != null && bondArray.length() > 0) {
									flightObj = getFlightBondJson(flightObj, bondArray);
									flightMap.put("tripType", bondArray.length() >1 ? "internationalRoundWay":"OneWay");
									flightMap.put("journeyTime",bondArray.getJSONObject(0).getString("journeyTime") );
								}
							} else if ("fare".equals(segmentKey)) {
								JSONArray fareArray = segmentObj.getJSONArray(segmentKey);
								flightObj.setAmount(fareArray.getJSONObject(0).getString("totalFareWithOutMarkUp"));
							}
						}
					}
					noOfStopage.add(getStopsByLegs(flightObj.getFlightLegresArr()));
					flightNumbers.add(flightObj.getAirlineName());
					flightNames.add(flightObj.getFlightName());
					flightObj.setNumber(""+segment);
					flightList.add(flightObj);
				}
			}
			flightMap.put("flightList", flightList);
//			flightMap.put("flightNumber", getListByHashData(flightNumber));
			flightMap.put("noOfStopage", getListByHashData(noOfStopage));
			flightMap.put("flightNumber", getAirlineData(flightNumbers,flightNames));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return flightMap;
	}

	private List<FlightResponse> getAirlineData(LinkedHashSet<String> flightNumbers, LinkedHashSet<String> flightNames) {
		List<FlightResponse> airlineDetails = new ArrayList<>();
		try {
			Iterator<String> number = flightNumbers.iterator();
			Iterator<String> name = flightNames.iterator();
			while (number.hasNext()) {
				FlightResponse response = new FlightResponse();
				response.setAirlineName(number.next());
				response.setFlightName(name.next());
				airlineDetails.add(response);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return airlineDetails;
	}

	private List<FlightResponse> getListByHashData(HashSet<String> noOfStopage) {
		List<FlightResponse> stopsList = new ArrayList<>();
		try {
			if (noOfStopage != null) {
				Iterator<String> stops = noOfStopage.iterator();
				while (stops.hasNext()) {
					FlightResponse response = new FlightResponse();
					response.setStopage(stops.next());
//					response.setAirlineName(response.getStopage());
//					response.setFlightName(getFlightNameByAirline(response.getStopage()));
					stopsList.add(response);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return stopsList;
	}

//	private String getFlightNameByAirline(String airline) {
//		switch (airline) {
//		case"AI":
//			return "AirIndia";
//		case"I5":
//			return "AirAsia";
//		case"G8":
//			return "GoAir";
//		case"9W":
//			return "JetAirWays";
//		case"6E":
//			return "Indigo";
//		case"SG":
//			return "Spicejet";
//		case"UK":
//			return "Vistara";
//		default:
//			break;
//		}
//		return null;
//	}

	private String getStopsByLegs(List<FlightLegresArr> flightLegresArr) {
		if(flightLegresArr!=null){
			return getStopage(flightLegresArr.size());			
		}return null;
	}

	private FlightResponse getFlightBondJson(FlightResponse flightObj, JSONArray bondArray) {
		try {
			for (int bondVal = 0; bondVal < bondArray.length();) {
				flightObj.setJourneyTime(bondArray.getJSONObject(bondVal).getString("journeyTime"));
				flightObj.setStopage(getStopage(bondArray.getJSONObject(bondVal).getJSONArray("legs").length()));
				List<FlightLegresArr> flightLegs = getFlightLegsJson(
						bondArray.getJSONObject(bondVal).getJSONArray("legs"));
				if (!flightLegs.isEmpty()) {
					flightObj.setDepartureTime(flightLegs.get(0).getDepartureTime());
					flightObj.setFlightNumber(flightLegs.get(0).getFlightNumber());
					flightObj.setAirlineName(flightLegs.get(0).getAirlineName());
					flightObj.setFlightName(flightLegs.get(0).getFlightName());
					flightObj.setBaggageUnit(flightLegs.get(0).getBaggageUnit());
					flightObj.setBaggageWeight(flightLegs.get(0).getBaggageWeight());
					flightObj.setArrivalTime(flightLegs.size() > 1 ? flightLegs.get(flightLegs.size() - 1).getArrivalTime()
									: flightLegs.get(0).getArrivalTime());
				}
				if (bondArray.length() > 1) {
					flightObj = getInternationalRoundWayData(flightObj, bondArray.getJSONObject(1));
				}
				flightObj.setFlightLegresArr(flightLegs);
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flightObj;
	}

	private FlightResponse getInternationalRoundWayData(FlightResponse flightObj, JSONObject bondJson) {
		try {
			flightObj.setJourneyTimeReturn(bondJson.getString("journeyTime"));
			flightObj.setStopagereturn(getStopage(bondJson.getJSONArray("legs").length()));
			List<FlightLegresArr> flightLegs = getFlightLegsJson(bondJson.getJSONArray("legs"));
			if (!flightLegs.isEmpty()) {
				flightObj.setDepartureTimereturn(flightLegs.get(0).getDepartureTime());
				flightObj.setFlightNumberreturn(flightLegs.get(0).getFlightNumber());
				flightObj.setAirlineNamereturn(flightLegs.get(0).getAirlineName());
				flightObj.setArrivalTimereturn(flightLegs.size() > 1  ? flightLegs.get(flightLegs.size() - 1).getArrivalTime()
								: flightLegs.get(0).getArrivalTime());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flightObj;
	}

	private String getStopage(int stopageCount) {
		switch (stopageCount) {
		case 1:
			return "Non-Stop";
		case 2:
			return "1-Stop";
		default:
			return "2-Stop";
		}
	}

	private List<FlightLegresArr> getFlightLegsJson(JSONArray legArray) {
		List<FlightLegresArr> legsList = new ArrayList<>();
		try {
			if (legArray != null && legArray.length() > 0) {
				for (int leg = 0; leg < legArray.length(); leg++) {
					FlightLegresArr legData = new FlightLegresArr();
					legData.setAircraftType(legArray.getJSONObject(leg).getString("aircraftType"));
					legData.setAirlineName(legArray.getJSONObject(leg).getString("airlineName"));
					legData.setArrivalTime(legArray.getJSONObject(leg).getString("arrivalTime"));
					legData.setCabin(legArray.getJSONObject(leg).getString("cabin"));
					legData.setDepartureTime(legArray.getJSONObject(leg).getString("departureTime"));
					legData.setDestination(legArray.getJSONObject(leg).getString("destination"));
					legData.setDuration(legArray.getJSONObject(leg).getString("duration"));
					legData.setFlightNumber(legArray.getJSONObject(leg).getString("flightNumber"));
					legData.setOrigin(legArray.getJSONObject(leg).getString("origin"));
					legData.setBaggageUnit(legArray.getJSONObject(leg).getString("baggageUnit"));
					legData.setBaggageWeight(legArray.getJSONObject(leg).getString("baggageWeight"));
					legData.setFlightName(legArray.getJSONObject(leg).getString("flightName"));
					legsList.add(legData);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return legsList;
	}

	@Override
	public List<String> getPassengerList(String passenger) {
		if(passenger!=null && !passenger.isEmpty()){
			List<String> list = new ArrayList<>(Integer.parseInt(passenger));
			for (int i = 0; i < Integer.parseInt(passenger); i++) {
				 list.add(i + 1 + "");
			}
			return list;
		}
		return null;
	}

	@Override
	public String getMinAmount(ArrayList<FlightResponse> oneWayFlights,
			ArrayList<FlightResponse> roundWayFlights) {
		try {
			if (oneWayFlights != null && roundWayFlights != null && oneWayFlights.size()>0 && roundWayFlights.size()>0) {
				if (Double.parseDouble(oneWayFlights.get(0).getAmount()) < Double
						.parseDouble(roundWayFlights.get(0).getAmount()))
					return oneWayFlights.get(0).getAmount();
				return roundWayFlights.get(0).getAmount();
			} else if (oneWayFlights != null && oneWayFlights.size()>0 )
				return oneWayFlights.get(0).getAmount();			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Object getMaxAmount(ArrayList<FlightResponse> oneWayFlights, ArrayList<FlightResponse> roundWayFlights) {
		try {
			if (oneWayFlights != null && roundWayFlights != null && oneWayFlights.size()>1 && roundWayFlights.size()>1) {
				if (Double.parseDouble(
						oneWayFlights.get(oneWayFlights.size()-1).getAmount()) > Double
								.parseDouble(roundWayFlights.get(roundWayFlights.size()- 1 ).getAmount()))
					return oneWayFlights.get(oneWayFlights.size()- 1 ).getAmount();
				return roundWayFlights.get(roundWayFlights.size()- 1).getAmount();
			} else if (oneWayFlights != null && oneWayFlights.size()>1 )
				return oneWayFlights.get(oneWayFlights.size()-1 ).getAmount();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
