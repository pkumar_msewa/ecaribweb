package com.payqwikweb.api.impl;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jettison.json.JSONObject;

import com.payqwikweb.api.IDataConfigApi;
import com.payqwikweb.app.metadatas.UrlMetadatas;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Language;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.Version;
import com.payqwikweb.app.model.request.SessionDTO;
import com.payqwikweb.app.model.response.CommissionDTO;
import com.payqwikweb.app.model.response.ServiceListResponse;
import com.payqwikweb.app.model.response.ServicesDTO;
import com.payqwikweb.app.utils.SecurityUtils;
import com.payqwikweb.util.JSONParserUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class DataConfigApi implements IDataConfigApi {

	
	@Override
	public ServiceListResponse getServiceList(SessionDTO request) {
		ServiceListResponse resp = new ServiceListResponse();
		try {
			Client client = Client.create();
			JSONObject payload = new JSONObject();
			payload.put("sessionId",request.getSessionId());
			WebResource webResource = client.resource(UrlMetadatas.getDataConfigListUrl(Version.VERSION_1, Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456")).post(ClientResponse.class,payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if ("S00".equalsIgnoreCase(code)) {
							List<ServicesDTO> services = new ArrayList<>();
							resp.setSuccess(true);
							org.json.JSONArray details = JSONParserUtil.getArray(jobj,"details");
							if(details != null && details.length() > 0) {
								for (int i=0;i<details.length();i++){
									org.json.JSONObject json = details.getJSONObject(i);
									ServicesDTO dto = new ServicesDTO();
									dto.setConfigType(JSONParserUtil.getString(json,"configType"));
									dto.setDescription(JSONParserUtil.getString(json,"description"));
									dto.setStatus(JSONParserUtil.getString(json,"status"));
									dto.setToEmail(JSONParserUtil.getString(json,"toEmail"));
									dto.setCcEmail(JSONParserUtil.getString(json,"ccEmail"));
									dto.setBccEmail(JSONParserUtil.getString(json,"bccEmail"));
									dto.setMdex(JSONParserUtil.getString(json,"mdex"));
									dto.setCode(JSONParserUtil.getString(json, "code"));
									services.add(dto);
								}
								resp.setServiceList(services);
							}else {
								resp.setSuccess(false);
								resp.setMessage("Services Empty");
							}
						} else {
							resp.setSuccess(false);
							resp.setMessage("Please try Later");
						}
					}
				} else {
					resp.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
		}
		return resp;
	}
}