package com.payqwikweb.api;

import com.payqwikweb.model.app.response.TKMenuPriceCalDTO;
import com.payqwikweb.model.app.response.TKGetMenuListDTO;
import com.payqwikweb.model.app.response.TKTrainslistDTO;
import com.payqwikweb.model.app.response.TkApplyCouponDTO;
import com.payqwikweb.model.app.response.TKMenuListDTO;
import com.payqwikweb.model.app.response.TKOutletListDTO;
import com.payqwikweb.model.app.response.TKTrackUserOrderDTO;
import com.payqwikweb.model.app.response.TKTrainRouteDTO;
import com.payqwikweb.model.app.response.TKGetTrainRouteDTO;
import com.payqwikweb.model.app.response.TKOrderDTO;
import com.payqwikweb.model.app.response.TKMyOrderDetailsDTO;
import com.payqwikweb.model.app.response.TKOrderSuccessDTO;
import com.payqwikweb.model.app.response.TKPlaceOrderInitiateResp;

import org.codehaus.jettison.json.JSONException;

import com.payqwikweb.app.model.request.SessionDTO;
import com.payqwikweb.model.app.TravelkhanaStationlistRequest;
import com.payqwikweb.model.app.request.TKApplyCouponRequest;
import com.payqwikweb.model.app.request.TKMenuPriceCalRequest;
import com.payqwikweb.model.app.request.TKPlaceOrderRequest;
import com.payqwikweb.model.app.request.TKTrainslistRequest;

public interface ITravelkhanaApi {
	TKTrainslistDTO getTrainslist(TKTrainslistRequest trainslistreq);
	TKTrainslistDTO getStationlist(TKTrainslistRequest stationlistreq);
	TKMenuListDTO getMenulist(TKTrainslistRequest dto);
	TKTrainRouteDTO getTrainRoutesMenulist(TKTrainslistRequest dto);
	TKOutletListDTO getOuletMenulist(TKTrainslistRequest dto);
	TKMenuPriceCalDTO getMenuPriceCal(TKMenuPriceCalRequest dto);
	TKTrackUserOrderDTO trackUserOrder(TKTrainslistRequest dto);
	TKOrderDTO placeOrder(TKPlaceOrderRequest dto);
	TKPlaceOrderInitiateResp initate(TKPlaceOrderRequest objinit);
	TKOrderSuccessDTO SuccessOrder(TKPlaceOrderRequest dto);
	TkApplyCouponDTO applycoupon(TKApplyCouponRequest request);
	TKMyOrderDetailsDTO myOrder(SessionDTO dto);
	TKTrainslistDTO getTrainslistFromDB(TKTrainslistRequest request) throws JSONException;
	
}
