package com.payqwikweb.api;

import com.payqwikweb.app.model.request.SessionDTO;
import com.payqwikweb.app.model.response.ServiceListResponse;

public interface IDataConfigApi {
	
	
	ServiceListResponse getServiceList(SessionDTO request);
	
}
