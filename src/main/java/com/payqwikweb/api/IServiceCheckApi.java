package com.payqwikweb.api;

public interface IServiceCheckApi {

	String checkServiceActive(String sessionId);

	String agentCheckServiceActive(String sessionId);
	
}
