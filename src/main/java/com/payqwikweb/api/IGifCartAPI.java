package com.payqwikweb.api;

import com.payqwikweb.model.app.request.GCIAddCartRequest;
import com.payqwikweb.model.app.request.GCIAddOrderRequest;
import com.payqwikweb.model.app.request.GCIAmountInitateRequest;
import com.payqwikweb.model.app.request.GCIBrandStoreRequest;
import com.payqwikweb.model.app.request.GCIOrderVoucherRequest;
import com.payqwikweb.model.app.request.GCIProceedRequest;
import com.payqwikweb.model.app.request.GetBrandDenominationRequest;
import com.payqwikweb.model.app.response.GCIAddCartResponse;
import com.payqwikweb.model.app.response.GCIAddOrderResponse;
import com.payqwikweb.model.app.response.GCIAmountInitateResponse;
import com.payqwikweb.model.app.response.GCIBrandDenominationResponse;
import com.payqwikweb.model.app.response.GCIBrandStoreResponse;
import com.payqwikweb.model.app.response.GCIOrderVoucher;
import com.payqwikweb.model.app.response.GCIOrderVoucherResponse;
import com.payqwikweb.model.app.response.GCIProceedResponse;

public interface IGifCartAPI 
	{
		String GetaccessToken(); 

		String GetBrand(String acessToken);
		GCIBrandDenominationResponse denomation(GetBrandDenominationRequest request);
		GCIAddOrderResponse addOrder(GCIAddOrderRequest request);
		
		GCIAddCartResponse addcart(GCIAddCartRequest request);
		GCIBrandStoreResponse brandstore(GCIBrandStoreRequest request);
		GCIOrderVoucherResponse voucher(GCIOrderVoucherRequest request);
		GCIAddCartResponse showcart(GCIAddCartRequest request);
		GCIProceedResponse proceed(GCIProceedRequest request);
		GCIAmountInitateResponse initate(GCIAmountInitateRequest request);
		GCIProceedResponse sucees(GCIOrderVoucher request);


		
	}

