package com.payqwikweb.api;

import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.response.UserDetailsResponse;

public interface IAuthenticationApi {

	String getAuthorityFromSession(String session , Role role);

	UserDetailsResponse getUserDetailsFromSession(String session);
}
