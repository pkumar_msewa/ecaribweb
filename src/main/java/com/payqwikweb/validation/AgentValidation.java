package com.payqwikweb.validation;

import com.payqwikweb.model.app.request.AgentRegistrationRequest;
import com.payqwikweb.model.error.AgentError;

public class AgentValidation {

	public AgentError checkError(AgentRegistrationRequest dto) {
		AgentError error = new AgentError();
		boolean valid = true;
		String message = "";
		// String patternvalidation=�[A-Z][a-z]+( [A-Z][a-z]+)�;
		final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
				+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

		if (CommonValidation.isNull(dto.getFirstName())) {
			valid = false;
			message = "Please enter first Name";
		} else if (!CommonValidation.containsAlphabets(dto.getFirstName())) {
			valid = false;
			message = "Please enter valid first Name";
		} else if (!CommonValidation.checkLength25(dto.getFirstName())) {
			valid = false;
			message = "Please enter valid first Name";
		}

		if (CommonValidation.isNull(dto.getLastName())) {
			valid = false;
			message = "Please enter last Name";
		} else if (!CommonValidation.containsAlphabets(dto.getLastName())) {
			valid = false;
			message = "Please enter valid last Name";
		} else if (!CommonValidation.checkLength25(dto.getLastName())) {
			valid = false;
			message = "Please enter valid last Name";
		}
		
		if (CommonValidation.isNull(dto.getAgencyName())) {
			valid = false;
			message = "Please enter agency Name";
		} else if (!CommonValidation.containsAlphabetsWithSpaces(dto.getAgencyName())) {
			valid = false;
			message = "Please enter valid agency Name";
		} else if (!CommonValidation.checkLength25(dto.getAgencyName())) {
			valid = false;
			message = "Agency name should be 25 characters only";
		}

		if (CommonValidation.isNull(dto.getEmailAddress())) {
			valid = false;
			message = "Please enter email address";
		}
		else if (!dto.getEmailAddress().matches(EMAIL_PATTERN)) {
			valid = false;
			message = "Please enter valid email address";
		} else if (!CommonValidation.isValidMail(dto.getEmailAddress())) {
			valid = false;
			message = "Please enter valid email address";
		}

		if (CommonValidation.isNull(dto.getAddress())) {
			valid = false;
			message = "Please enter address";
		}
		/*
		 * else if(!CommonValidation.checkAddress(dto.getAddress1())){
		 * valid=false; message="Please enter valid address"; }
		 */

		if (CommonValidation.isNull(dto.getCity())) {
			valid = false;
			message = "Please enter city";
		}
		/*
		 * else if(!CommonValidation.containsAlphabets(dto.getCity())){
		 * valid=false; message="Please enter valid city"; }
		 */
		if (CommonValidation.isNull(dto.getState())) {
			valid = false;
			message = "Please enter state";
		}
		/*
		 * else if(!CommonValidation.containsAlphabets(dto.getState())){
		 * valid=false; message="Please enter valid state"; }
		 */

		if (CommonValidation.isNull(dto.getPinCode())) {
			valid = false;
			message = "Please enter pincode number";
		} else if (!CommonValidation.isNumeric(dto.getPinCode())) {
			valid = false;
			message = "Please enter valid pincode number";
		} else if (!CommonValidation.checkLength6(dto.getPinCode())) {
			valid = false;
			message = "Please enter 6 digit pincode number";
		}

		if (CommonValidation.isNull(dto.getPassword())) {
			valid = false;
			message = "Please enter password";
		} else if (!CommonValidation.checkLength6(dto.getPassword())) {
			valid = false;
			message = "Please enter minimum 6 digit password";
		}

		if (CommonValidation.isNull(dto.getPanNo())) {
			valid = false;
			message = "Please enter pancard number";
		} else if (CommonValidation.containsSpecialCharacter(dto.getPanNo())) {
			valid = false;
			message = "Please enter valid pancard number";
		} else if (!CommonValidation.checkLength10(dto.getPanNo())) {
			valid = false;
			message = "Please enter valid pancard number";
		}

		if (CommonValidation.isNull(dto.getAgentAccountName())) {
			valid = false;
			message = "Please enter account type";
		} else if (!CommonValidation.containsAlphabets(dto.getAgentAccountName())) {
			valid = false;
			message = "Please enter valid account type";
		}
		if (CommonValidation.isNull(dto.getAgentBankName())) {
			valid = false;
			message = "Please enter bank name";
		}
		/*
		 * else if(!CommonValidation.containsAlphabets(dto.getAgentBankName())){
		 * valid=false; message="Please enter valid bank name"; }
		 */

		if (CommonValidation.isNull(dto.getAgentAccountNumber())) {
			valid = false;
			message = "Please enter account number";
		} else if (!CommonValidation.isNumeric(dto.getAgentAccountNumber())) {
			valid = false;
			message = "Please enter valid account number";
		} else if (dto.getAgentAccountNumber().length() > 16) {
			valid = false;
			message = "Please enter valid account number";
		}

		if (CommonValidation.isNull(dto.getAgentbranchname())) {
			valid = false;
			message = "Please enter branch Name";
		} else if (!CommonValidation.containsAlphabets(dto.getAgentbranchname())) {
			valid = false;
			message = "Please enter valid branch Name";
		}

		if (CommonValidation.isNull(dto.getAgentBankIfscCode())) {
			valid = false;
			message = "Please enter bank location";
		}
		/*
		 * else
		 * if(!CommonValidation.containsAlphabets(dto.getAgentBankLocation())){
		 * valid=false; message="Please enter valid bank location"; }
		 */

		if (CommonValidation.isNull(dto.getMobileNo())) {
			valid = false;
			message = "Please enter mobile number";
		}
		else if (!CommonValidation.isNumeric(dto.getMobileNo())) {
			valid = false;
			message = "Please enter valid mobile number";
		} else if (!CommonValidation.checkLength10(dto.getMobileNo())) {
			valid = false;
			message = "";
		}

		if (CommonValidation.isNull(dto.getCountry())) {
			valid = false;
			message = "Please enter country";
		} else if (!CommonValidation.containsAlphabets(dto.getCountry())) {
			valid = false;
			message = "Please enter valid country";
		}

		/*
		 * else if(!CommonValidation.checkLength6(dto.getPinCode())){
		 * valid=false; }
		 */

		error.setValid(valid);
		error.setMessage(message);
		return error;

	}

	public AgentError checkPincode(AgentRegistrationRequest dto) {
		AgentError error = new AgentError();
		boolean valid = true;
		String message = "";
		if (CommonValidation.isNull(dto.getPinCode())) {
			valid = false;
			message = "Please enter pincode number";
		} else if (!CommonValidation.isNumeric(dto.getPinCode())) {
			valid = false;
			message = "Please enter valid pincode number";
		} else if (!CommonValidation.checkLength6(dto.getPinCode())) {
			valid = false;
			message = "Please enter 6 digit pincode number";
		}
		error.setMessage(message);
		error.setValid(valid);
		return error;
	}

}