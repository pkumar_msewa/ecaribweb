package com.payqwikweb.validation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.payqwik.visa.util.VisaMerchantRequest;
import com.payqwikweb.app.model.request.DRegistrationRequest;
import com.payqwikweb.app.model.request.MRegistrationRequest;
import com.payqwikweb.app.model.request.RegistrationRequest;
import com.payqwikweb.model.error.RegisterError;
import com.payqwikweb.model.error.VisaMerchantError;

public class RegisterValidation {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());


	public RegisterError checkError(RegistrationRequest registerRequest) {
		RegisterError error = new RegisterError();
		boolean valid = true;
		if (CommonValidation.isNull(registerRequest.getFirstName())) {
			error.setFirstName("Please enter First Name");
			valid = false;
		}
		
		/*if (CommonValidation.isNull(registerRequest.getLocationCode())) {
			error.setPincode("Please enter pincode");
			valid = false;
		}else if(!(CommonValidation.checkLength6(registerRequest.getLocationCode())&& CommonValidation.isNumeric(registerRequest.getLocationCode()))){
			error.setPincode("locationCode should be 6 digits long and should contain only digits");
			valid=false;
		}*/
		
		if (CommonValidation.isNull(registerRequest.getPassword())) {
			error.setPassword("Please enter valid password");
			valid = false;
		}else if(!CommonValidation.isValidPassword(registerRequest.getPassword())){
			valid = false;
		}

//		if (CommonValidation.isNull(registerRequest.getConfirmPassword())) {
//			error.setConfirmPassword("Please enter confirm Password");
//			valid = false;
//		}else if(!(registerRequest.getPassword().equals(registerRequest.getConfirmPassword()))){
//			valid = false;
//		}

		if (CommonValidation.isNull(registerRequest.getContactNo())) {
			error.setContactNo("Please enter your ContactNo");
			valid = false;
		} else if (!(CommonValidation.checkLength10(registerRequest.getContactNo()) && CommonValidation.isNumeric(registerRequest.getContactNo()) && CommonValidation.isValidMobileNumber(registerRequest.getContactNo()))) {
			error.setContactNo("mobile number must be 10-digit and should contain valid mobile number .");
			valid = false;
		}
		error.setValid(valid);
		return error;
	}

	public RegisterError checkMerchantRegistrationError(MRegistrationRequest request){

		RegisterError error  = new RegisterError();
		boolean valid =  true;
		if(CommonValidation.isNull(request.getFirstName())){
			error.setFirstName("Please enter First Name");
			valid = false;
		}

		if(CommonValidation.isNull(request.getEmail())){
			error.setEmail("Please enter email");
			valid = false;
		}else if(!CommonValidation.isValidMail(request.getEmail())){
			valid = false;
			error.setEmail("Enter valid email");
		}

		if(CommonValidation.isNull(request.getContactNo())){
			valid = false;
			error.setContactNo("Please enter Contact No");
		}else if(!CommonValidation.checkLength10(request.getContactNo())){
			valid = false;
			error.setContactNo("Enter valid contact no");
		}

		if(CommonValidation.isNull(request.getIpAddress())){
			valid = false;
			error.setIpAddress("Enter IP Address");
		}else if(!CommonValidation.isValidIP(request.getIpAddress())){
			valid = false;
			error.setIpAddress("Enter Valid IP Address");
		}

		if(CommonValidation.isNull(request.getSuccessURL())){
			valid = false;
			error.setSuccessURL("Enter Success URL for Payment");
		}else if(!CommonValidation.isValidURL(request.getSuccessURL())){
			valid = false;
			error.setSuccessURL("URL is not in valid format");
		}

		if(CommonValidation.isNull(request.getFailureURL())){
			valid = false;
			error.setFailureURL("Enter Failure URL for Payment");
		}else if(!CommonValidation.isValidURL(request.getFailureURL())){
			valid = false;
			error.setFailureURL("URL is not in valid format");
		}

		if(request.getMinAmount() <= 0){
			valid = false;
			error.setMinAmount("Enter Minimum Amount (> = 1)");
		}
		if(request.getMaxAmount() <= 0){
			valid = false;
			error.setMaxAmount("Enter Maximum Amount(> = 1)");
		}

		if(request.getValue() < 0){
			valid = false;
			error.setValue("Enter Charged Amount( >= 0)");
		}
		error.setValid(valid);
		return error;
	}
	
	public RegisterError checkDonateeRegistrationError(DRegistrationRequest request){

		RegisterError error  = new RegisterError();
		boolean valid =  true;
		if(CommonValidation.isNull(request.getFirstName())){
			error.setFirstName("Please enter First Name");
			valid = false;
		}

		if(CommonValidation.isNull(request.getEmail())){
			error.setEmail("Please enter email");
			valid = false;
		}else if(!CommonValidation.isValidMail(request.getEmail())){
			valid = false;
			error.setEmail("Enter valid email");
		}

		if(CommonValidation.isNull(request.getMobileNumber())){
			valid = false;
			error.setContactNo("Please enter Contact No");
		}else if(!CommonValidation.checkLength10(request.getMobileNumber())){
			valid = false;
			error.setContactNo("Enter valid contact no");
		}

		if(request.getValue() < 0){
			valid = false;
			error.setValue("Enter Charged Amount( >= 0)");
		}
		error.setValid(valid);
		return error;
	}
	
	public VisaMerchantError checkMSignupError(VisaMerchantRequest registerRequest) {
		VisaMerchantError error  = new VisaMerchantError();
		boolean valid = true;
		if (CommonValidation.isNull(registerRequest.getFirstName())) {
			error.setFirstName("Please enter Merchant Name");
			valid = false;
		}

		if (CommonValidation.isNull(registerRequest.getMobileNo())) {
			error.setMobileNo("Please enter your Contact No");
			valid = false;
		} else if (!CommonValidation.checkLength10(registerRequest.getMobileNo())) {
			error.setMobileNo("mobile number must be 10-digit");
			valid = false;
		}else if(!CommonValidation.isNumeric(registerRequest.getMobileNo())){
			valid =  false;
		}
		if(CommonValidation.isNull(registerRequest.getEmailAddress())){
			error.setEmailAddress("Please enter email");
			valid = false;
		}else if(!CommonValidation.isValidMail(registerRequest.getEmailAddress())){
			valid = false;
			error.setEmailAddress("Enter valid email");
		}
		
		if (CommonValidation.isNull(registerRequest.getPinCode())) {
			error.setPinCode("Please enter pincode");
			valid = false;
		}else if(!(CommonValidation.checkLength6(registerRequest.getPinCode())&& CommonValidation.isNumeric(registerRequest.getPinCode()))){
			error.setPinCode("locationCode should be 6 digits long and should contain only digits");
			valid=false;
		}
		if(CommonValidation.isNull(registerRequest.getAddress1())){
			error.setAddress1("Please enter address");
			valid = false;
		}
		if(CommonValidation.isNull(registerRequest.getCity())){
			error.setCity("Please enter city");
			valid = false;
		}
		if(CommonValidation.isNull(registerRequest.getState())){
			error.setState("Please enter State");
			valid = false;
		}
		if(CommonValidation.isNull(registerRequest.getCountry())){
			error.setCountry("Please enter country");
			valid = false;
		}
		error.setValid(valid);
		return error;
	}
	
	public VisaMerchantError checkMBankError(VisaMerchantRequest registerRequest) {
		VisaMerchantError error  = new VisaMerchantError();
		boolean valid = true;

		if(CommonValidation.isNull(registerRequest.getPanNo())) {
			error.setPanNo("Please enter your PAN Card no.");
			valid = false;
		}
		
		/*if(CommonValidation.isNull(registerRequest.getAadharNo())) {
			error.setAadharNo("Please enter your Aadhar Card No.");
			valid = false;
		}
*/
		if(CommonValidation.isNull(registerRequest.getMerchantAccountName())) {
			error.setMerchantAccountName("Please enter your Bank Account Number");
			valid = false;
		}
		
		if(CommonValidation.isNull(registerRequest.getMerchantAccountNumber())) {
			error.setMerchantAccountNumber("Please enter your Bank Account Number");
			valid = false;
		}
		
		if(CommonValidation.isNull(registerRequest.getMerchantBankName())) {
			error.setMerchantBankName("Please enter bank name");
			valid = false;
		}
		
//		if(CommonValidation.isNull(registerRequest.getMerchantBankLocation())) {
//			error.setMerchantBankLoction("Please enter branch name");
//			valid = false;
//		}
//
//		if(CommonValidation.isNull(registerRequest.getMerchantBankIfscCode())) {
//			error.setMerhantIfscCode("Please enter IFSC Code");
//			valid = false;
//		}

		error.setValid(valid);
		return error;
	}
	
	public VisaMerchantError checkVisaMerchantRegistrationError(VisaMerchantRequest request){

		VisaMerchantError error  = new VisaMerchantError();
		boolean valid =  true;
		if(CommonValidation.isNull(request.getFirstName())){
			error.setFirstName("Please enter First Name");
			valid = false;
		}
		
		if(CommonValidation.isNull(request.getLastName())){
			error.setLastName("Please enter Last Name");
			valid = false;
		}

		if(CommonValidation.isNull(request.getAddress1())){
			error.setAddress1("Please enter address");
			valid = false;
		}
		if(CommonValidation.isNull(request.getCity())){
			error.setCity("Please enter city");
			valid = false;
		}
		if(CommonValidation.isNull(request.getState())){
			error.setState("Please enter State");
			valid = false;
		}
		if(CommonValidation.isNull(request.getCountry())){
			error.setCountry("Please enter country");
			valid = false;
		}
		if(CommonValidation.isNull(request.getPinCode())){
			error.setPinCode("Please enter Pin Code");
			valid = false;
		}
		/*if(CommonValidation.isNull(request.getMerchantBusinessType())){
			error.setMerchantBusinessType("Please enter Merchant Business Type");
			valid = false;
		}*/
		if(CommonValidation.isNull(request.getLattitude())){
			error.setLatitude("Please enter Latitude");
			valid = false;
		}
		if(CommonValidation.isNull(request.getLongitude())){
			error.setLongitude("Please enter Longitude");
			valid = false;
		}
//		if(CommonValidation.isNull(request.getIsEnabled())){
//			error.setIsEnabled("Please enter IsEnabled");
//			valid = false;
//		}
		if(CommonValidation.isNull(request.getPanNo())){
			error.setPanNo("Please enter Pan No");
			valid = false;
		}
		/*if(CommonValidation.isNull(request.getAggregator())){
			error.setAggregator("Please enter Aggregator");
			valid = false;
		}*/
		if(CommonValidation.isNull(request.getMerchantAccountName())){
			error.setMerchantAccountName("Please enter Merchant Account Name");
			valid = false;
		}
		if(CommonValidation.isNull(request.getMerchantBankName())){
			error.setMerchantBankName("Please enter Merchant Bank Name");
			valid = false;
		}
		if(CommonValidation.isNull(request.getMerchantAccountNumber())){
			error.setMerchantAccountNumber("Please enter Merchant Account Number");
			valid = false;
		}
		if(CommonValidation.isNull(request.getMerchantBankIfscCode())){
			error.setMerhantIfscCode("Please enter Merhant Ifsc Code");
			valid = false;
		}
		if(CommonValidation.isNull(request.getMerchantBankLocation())){
			error.setMerchantBankLoction("Please enter Merchant Bank Loction");
			valid = false;
		}
		/*if(CommonValidation.isNull(request.getSettlementAccountName())){
			error.setSettlementAccountName("Please enter Merchant Account Name");
			valid = false;
		}
		if(CommonValidation.isNull(request.getSettlementBankName())){
			error.setSettlementBankName("Please enter Settlement Bank Name");
			valid = false;
		}
		if(CommonValidation.isNull(request.getSettlementAccountNumber())){
			error.setSettlementAccountNumber("Please enter Settlement Account Number");
			valid = false;
		}
		if(CommonValidation.isNull(request.getSettlementIfscCode())){
			error.setSettlementIfscCode("Please enter Settlement Ifsc Code");
			valid = false;
		}
		if(CommonValidation.isNull(request.getSettlementBankLocation())){
			error.setSettlementBankLoction("Please enter Settlement Bank Loction");
			valid = false;
		}
		*/
		if(CommonValidation.isNull(request.getEmailAddress())){
			error.setEmailAddress("Please enter email");
			valid = false;
		}else if(!CommonValidation.isValidMail(request.getEmailAddress())){
			valid = false;
			error.setEmailAddress("Enter valid email");
		}

		if(CommonValidation.isNull(request.getMobileNo())){
			valid = false;
			error.setMobileNo("Please enter Contact No");
		}else{
			valid=true;
		error.setValid(valid);
		}
		return error;
	}
	
	
	public RegisterError checkErrorAgenttoUser(RegistrationRequest registerRequest) {
		RegisterError error = new RegisterError();
		boolean valid = true;
		if (CommonValidation.isNull(registerRequest.getFirstName())) {
			error.setFirstName("Please enter First Name");
			error.setMessage("Please enter First Name");
			
			valid = false;
		}
		if (CommonValidation.isNull(registerRequest.getLastName())) {
			error.setFirstName("Please enter Last Name");
			error.setMessage("Please enter Last Name");
			valid = false;
		}
		if (CommonValidation.isNull(registerRequest.getLocationCode())) {
			error.setPincode("Please enter pincode");
			error.setMessage("Please enter pincode");
			valid = false;
		}else if(!(CommonValidation.checkLength6(registerRequest.getLocationCode())&& CommonValidation.isNumeric(registerRequest.getLocationCode()))){
			error.setPincode("locationCode should be 6 digits long and should contain only digits");
			error.setMessage("locationCode should be 6 digits long and should contain only digits");
			valid=false;
		}
		
		if (CommonValidation.isNull(registerRequest.getPassword())) {
			error.setPassword("Please enter valid password");
			error.setMessage("Please enter valid password");
			valid = false;
		}else if(!CommonValidation.isValidPassword(registerRequest.getPassword())){
			valid = false;
		}

//		if (CommonValidation.isNull(registerRequest.getConfirmPassword())) {
//			error.setConfirmPassword("Please enter confirm Password");
//			valid = false;
//		}else if(!(registerRequest.getPassword().equals(registerRequest.getConfirmPassword()))){
//			valid = false;
//		}

		if (CommonValidation.isNull(registerRequest.getContactNo())) {
			error.setContactNo("Please enter your ContactNo");
			error.setMessage("Please enter your ContactNo");
			valid = false;
		} else if (!CommonValidation.checkLength10(registerRequest.getContactNo())) {
			error.setContactNo("mobile number must be 10-digit");
			error.setMessage("mobile number must be 10-digit");
			valid = false;
		}else if(!CommonValidation.isNumeric(registerRequest.getContactNo())){
			valid =  false;
		}

		error.setValid(valid);
		return error;
	}
}



