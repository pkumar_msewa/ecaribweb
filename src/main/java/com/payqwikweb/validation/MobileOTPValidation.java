package com.payqwikweb.validation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.payqwikweb.app.model.request.MobileOTPRequest;
import com.payqwikweb.app.model.request.ResendMobileOTPRequest;
import com.payqwikweb.model.error.MobileOTPError;

public class MobileOTPValidation {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	public MobileOTPError checkError(MobileOTPRequest dto) {
		boolean valid = true;
		MobileOTPError error = new MobileOTPError();

		if (CommonValidation.isNull(dto.getKey())) {
			valid = false;
		} else if (!CommonValidation.isNumeric(dto.getKey())) {
			valid = false;
		}
		error.setValid(valid);
		return error;
	}
	
	public MobileOTPError checkError(ResendMobileOTPRequest dto) {
		boolean valid = true;
		MobileOTPError error = new MobileOTPError();

		if(CommonValidation.isNull(dto.getMobileNumber())){
			valid = false;
		}else if(!CommonValidation.isNumeric(dto.getMobileNumber())){
			valid = false;
		}else if(!CommonValidation.checkLength10(dto.getMobileNumber())){
			valid = false;
		}
		error.setValid(valid);
		return error;
	}

}
