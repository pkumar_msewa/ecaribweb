package com.payqwikweb.validation;

import com.payqwikweb.app.model.request.ChangeMPINRequest;
import com.payqwikweb.app.model.request.ForgotMpinRequest;
import com.payqwikweb.app.model.request.SetMPINRequest;
import com.payqwikweb.app.model.request.UserMaxLimitDto;
import com.payqwikweb.model.error.MpinError;
import com.payqwikweb.util.CommonUtil;
import com.thirdparty.model.ResponseDTO;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class MpinValidation {

	public MpinError checkNewMpinError(SetMPINRequest dto){
		MpinError error = new MpinError();
		boolean valid = true;
		if(CommonValidation.isNull(dto.getNewMpin())){
			error.setNewMpin("Please enter new MPIN");
			valid = false;
		}else if(!CommonValidation.checkLength4(dto.getNewMpin())){
			error.setNewMpin("MPIN must be 4 digits long");
			valid = false;
		}else if(!CommonValidation.isNumeric(dto.getNewMpin())){
			error.setNewMpin("MPIN must contain digits");
			valid = false;
		}
		
		if(CommonValidation.isNull(dto.getConfirmMpin())){
			error.setConfirmMpin("please re-enter MPIN");
			valid = false;
		}else if(!(dto.getNewMpin().equals(dto.getConfirmMpin()))){
			error.setConfirmMpin("MPIN mismatch");
			valid = false;
		}
		if(CommonValidation.isNull(dto.getUsername())){
			valid = false;
		}
		
		error.setValid(valid);
		return error;
	}
	
	public MpinError checkChangeMpinError(ChangeMPINRequest dto){
		MpinError error = new MpinError();
		boolean valid = true;
		
		if(dto.getOldMpin().equals(dto.getNewMpin())){
			error.setNewMpin("you can't repeat your previous MPIN.");
			valid = false;
		}else if(CommonValidation.isNull(dto.getNewMpin())){
			error.setNewMpin("Please enter new MPIN");
			valid = false;
		}else if(!CommonValidation.checkLength4(dto.getNewMpin())){
			error.setNewMpin("Please enter 4 characters valid MPIN");
			valid = false;
		}else if(!CommonValidation.isNumeric(dto.getNewMpin())){
			error.setNewMpin("MPIN must contain digits");
			valid = false;
		}
	
		if(CommonValidation.isNull(dto.getConfirmMpin())){
			error.setConfirmMpin("please re-enter MPIN");
			valid = false;
		}else if(!(dto.getNewMpin().equals(dto.getConfirmMpin()))){
			error.setConfirmMpin("MPIN mismatch");
			valid = false;
		}
		if(CommonValidation.isNull(dto.getOldMpin())){
			error.setOldMpin("please enter your old mpin");
			valid = false;
		}
		if(CommonValidation.isNull(dto.getUsername())){
			valid = false;
		}
		error.setValid(valid);
		return error;
	}

	public MpinError checkError(ForgotMpinRequest dto){
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		MpinError error = new MpinError();
		boolean valid = true;
		if(CommonValidation.isNull(dto.getPassword())){
			valid = false;
			error.setOldMpin("enter password");
		}
		if(CommonValidation.isNull(dto.getDateOfBirth())){
			valid = false;
			error.setNewMpin("enter date of birth");
		}else{
			try {
				dateFormat.parse(dto.getDateOfBirth());
			} catch (ParseException e) {
				valid = false;
				error.setNewMpin("enter date of birth in yyyy-mm-dd format");
			}
		}

		error.setValid(valid);
		return error;
	}

	public ResponseDTO validateMaxLimitRequest(UserMaxLimitDto dto) {
		boolean valid = true;
		ResponseDTO responseDTO= new ResponseDTO();
		try {
			if (CommonUtil.isNull(dto.getTransactionType())) {
				responseDTO.setMessage("Could not update max limit");
				return responseDTO;
			}
			if (CommonUtil.isNull(dto.getSessionId())) {
				responseDTO.setMessage("Invalid Session, Please login again");
				return responseDTO;
			}
			if("Banktransfer".equalsIgnoreCase(dto.getTransactionType())) {
				if(!CommonUtil.isNull(dto.getDailyAmountOfTxn()) 
						&& Double.parseDouble(dto.getDailyAmountOfTxn()) > 5000) {
					responseDTO.setMessage("Daily bank transfer amount could not be greater than 5000");
					return responseDTO;
				}
				if(!CommonUtil.isNull(dto.getMonthlyAmountOfTxn()) 
						&& Double.parseDouble(dto.getMonthlyAmountOfTxn()) > 15000) {
					responseDTO.setMessage("Monthly bank transfer amount could not be greater than 15000");
					return responseDTO;
				}
				if(!CommonUtil.isNull(dto.getMonthlyNoOfTxn()) 
						&& Double.parseDouble(dto.getMonthlyNoOfTxn()) > 3) {
					responseDTO.setMessage("Monthly number of bank transfer could not be greater than 3");
					return responseDTO;
				}
				if(!CommonUtil.isNull(dto.getDailyNoOfTxn()) 
						&& Double.parseDouble(dto.getDailyNoOfTxn()) > 1) {
					responseDTO.setMessage("Daily number of bank tranfer could not be greater than 1");
					return responseDTO;
				}
			}else {
				if(!CommonUtil.isNull(dto.getDailyAmountOfTxn()) 
						&& Double.parseDouble(dto.getDailyAmountOfTxn()) > 50000) {
					responseDTO.setMessage("Daily transaction amount could not be greater than 50000");
					return responseDTO;
				}
				if(!CommonUtil.isNull(dto.getMonthlyAmountOfTxn()) 
						&& Double.parseDouble(dto.getMonthlyAmountOfTxn()) > 50000) {
					responseDTO.setMessage("Monthly transaction amount could not be greater than 50000");
					return responseDTO;
				}
				if(!CommonUtil.isNull(dto.getMonthlyNoOfTxn()) 
						&& Double.parseDouble(dto.getMonthlyNoOfTxn()) > 5000) {
					responseDTO.setMessage("Monthly number of transaction could not be greater than 5000");
					return responseDTO;
				}
				if(!CommonUtil.isNull(dto.getDailyNoOfTxn()) 
						&& Double.parseDouble(dto.getDailyNoOfTxn()) > 5000) {
					responseDTO.setMessage("Daily number of transaction could not be greater than 5000");
					return responseDTO;
				}
			}
			responseDTO.setSuccess(valid);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return responseDTO;
	}



}
