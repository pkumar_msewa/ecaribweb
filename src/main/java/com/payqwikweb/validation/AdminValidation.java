package com.payqwikweb.validation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.payqwikweb.model.app.request.SpecificUserTransactionRequest;
import com.payqwikweb.model.app.request.TotalTransactionRequest;
import com.payqwikweb.model.app.request.TotalUsersRequest;
import com.payqwikweb.model.error.SpecificUserTransactionError;
import com.payqwikweb.model.error.TotalTransactionError;
import com.payqwikweb.model.error.TotalUsersError;
import com.payqwikweb.util.LogCat;

public class AdminValidation {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	

	public TotalUsersError checkError(TotalUsersRequest usersRequest) {
		TotalUsersError error = new TotalUsersError();
		boolean valid = true;
		if (CommonValidation.isNull(usersRequest.getPage())) {
			error.setPage("pages should not be null");
			valid = false;

		} else if (!CommonValidation.isNumeric(usersRequest.getPage())) {
			error.setPage("pages must be numeric ");
			valid = false;

		}
		if (CommonValidation.isNull(usersRequest.getSize())) {
			error.setPage("page size should not be null");
			valid = false;

		} else if (!CommonValidation.isNumeric(usersRequest.getPage())) {
			error.setPage(" page size must be numeric ");
			valid = false;
		}
		return error;
	}

	public TotalTransactionError checkError(TotalTransactionRequest totalTransactionRequest) {
		TotalTransactionError error = new TotalTransactionError();
		boolean valid = true;
		if (CommonValidation.isNull(totalTransactionRequest.getPage())) {
			error.setPage("pages should not be null");
			valid = false;

		} else if (!CommonValidation.isNumeric(totalTransactionRequest.getPage())) {
			error.setPage("pages must be numeric ");
			valid = false;

		}
		if (CommonValidation.isNull(totalTransactionRequest.getSize())) {
			error.setPage("page size should not be null");
			valid = false;

		} else if (!CommonValidation.isNumeric(totalTransactionRequest.getPage())) {
			error.setPage(" page size must be numeric ");
			valid = false;
		}
		return error;

		}

		
		public SpecificUserTransactionError checkError(SpecificUserTransactionRequest request) {
			SpecificUserTransactionError error = new SpecificUserTransactionError();
			boolean valid = true;
			if (CommonValidation.isNull(request.getPage())) {
				error.setPage("pages should not be null");
				valid = false;

			} else if (!CommonValidation.isNumeric(request.getPage())) {
				error.setPage("pages must be numeric ");
				valid = false;

			}
			if (CommonValidation.isNull(request.getSize())) {
				error.setPage("page size should not be null");
				valid = false;

			} else if (!CommonValidation.isNumeric(request.getPage())) {
				error.setPage(" page size must be numeric ");
				valid = false;

			}

		error.setValid(valid);
		return error;
	}

}
