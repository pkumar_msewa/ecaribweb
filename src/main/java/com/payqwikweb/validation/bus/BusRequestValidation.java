package com.payqwikweb.validation.bus;

import com.payqwikweb.app.model.ResponseStatus;
import com.payqwikweb.app.model.busdto.BookTicketReq;
import com.payqwikweb.app.model.busdto.GetTransactionId;
import com.payqwikweb.app.model.busdto.TravellerDetailsDTO;
import com.payqwikweb.app.model.request.bus.GetDestinationCity;
import com.payqwikweb.app.model.request.bus.GetSeatDetails;
import com.payqwikweb.app.model.request.bus.GetSourceCity;
import com.payqwikweb.app.model.request.bus.IsCancellableReq;
import com.payqwikweb.app.model.request.bus.ListOfAvailableTrips;
import com.payqwikweb.model.error.bus.BusRequestError;

public class BusRequestValidation {


	public BusRequestError getAllSourceCityVal(GetSourceCity request)
	{
		BusRequestError error=new BusRequestError();
		boolean valid=true;
		if (request.getSourceKey().length()<3) {
			error.setMessage("Please enter min 3 digit source key");
			error.setSuccess(ResponseStatus.FAILURE);
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}else {
			error.setSuccess(ResponseStatus.SUCCESS);
			error.setCode(ResponseStatus.SUCCESS.getValue());
		}
		return error;
	}

	public BusRequestError getAlldestinationCityVal(GetDestinationCity request)
	{
		BusRequestError error=new BusRequestError();
		boolean valid=true;
		if (request.getDestinationKey().length()<3) {
			error.setMessage("Please enter min 3 digit destination key");
			error.setSuccess(ResponseStatus.FAILURE);
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}if ((request.getSourceId()==null || request.getSourceId().isEmpty()) && valid==true) {
			error.setMessage("Please enter source id");
			error.setSuccess(ResponseStatus.FAILURE);
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}if (valid) {
			error.setSuccess(ResponseStatus.SUCCESS);
			error.setCode(ResponseStatus.SUCCESS.getValue());
		}
		return error;
	}



	public BusRequestError getAllAvailableTripsVal(ListOfAvailableTrips request)
	{
		BusRequestError error=new BusRequestError();
		boolean valid=true;
		if (request.getSourceId()==null || request.getSourceId().isEmpty()) {
			error.setMessage("Please enter source Id");
			error.setSuccess(ResponseStatus.FAILURE);
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		if ((request.getDestinationId()==null || request.getDestinationId().isEmpty()) && valid==true) {
			error.setMessage("Please enter destination Id");
			error.setSuccess(ResponseStatus.FAILURE);
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		if ((request.getDate()==null || request.getDate().isEmpty()) && valid==true) {
			error.setMessage("Please enter destination Id");
			error.setSuccess(ResponseStatus.FAILURE);
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		if(valid) {
			error.setSuccess(ResponseStatus.SUCCESS);
			error.setCode(ResponseStatus.SUCCESS.getValue());
		}
		return error;
	}


	public BusRequestError getSeatDetailsVal(GetSeatDetails request)
	{
		BusRequestError error=new BusRequestError();
		boolean valid=true;
		if (request.getBusId()==null || request.getBusId().isEmpty()) {
			error.setMessage("Please enter Bus Id");
			error.setSuccess(ResponseStatus.FAILURE);
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		if (request.getEngineId()<=0 && valid==true) {
			error.setMessage("Please enter engine Id");
			error.setSuccess(ResponseStatus.FAILURE);
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		if ((request.getJourneyDate()==null || request.getJourneyDate().isEmpty()) && valid==true) {
			error.setMessage("Please enter journey date");
			error.setSuccess(ResponseStatus.FAILURE);
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		if(valid) {
			error.setSuccess(ResponseStatus.SUCCESS);
			error.setCode(ResponseStatus.SUCCESS.getValue());
		}
		return error;
	}




		public BusRequestError getTransactionIdVal(GetTransactionId request)
	{
		BusRequestError error=new BusRequestError();
		boolean valid=true;
		if (request.getMobileNo()==null || request.getMobileNo().isEmpty()) {
			error.setMessage("Please enter mobile no");
			error.setSuccess(ResponseStatus.FAILURE);
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		if ((request.getEmailId()==null || request.getMobileNo().isEmpty())&& valid==true) {
			error.setMessage("Please enter email Id");
			error.setSuccess(ResponseStatus.FAILURE);
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		if ((request.getAddress()==null || request.getAddress().isEmpty()) && valid==true) {
			error.setMessage("Please enter address");
			error.setSuccess(ResponseStatus.FAILURE);
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		if ((request.getEngineId()==null || request.getEngineId().isEmpty()) && valid==true) {
			error.setMessage("Please enter engine id");
			error.setSuccess(ResponseStatus.FAILURE);
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		if ((request.getBusid()==null || request.getBusid().isEmpty()) && valid==true) {
			error.setMessage("Please enter bus id");
			error.setSuccess(ResponseStatus.FAILURE);
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		if ((request.getBusType()==null || request.getBusType().isEmpty()) && valid==true) {
			error.setMessage("Please enter bus type");
			error.setSuccess(ResponseStatus.FAILURE);
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		if ((request.getBoardId()==null || request.getBoardId().isEmpty()) && valid==true) {
			error.setMessage("Please enter board id");
			error.setSuccess(ResponseStatus.FAILURE);
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		if ((request.getBoardName()==null || request.getBoardName().isEmpty()) && valid==true) {
			error.setMessage("Please enter board name");
			error.setSuccess(ResponseStatus.FAILURE);
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		if ((request.getArrTime()==null || request.getArrTime().isEmpty()) && valid==true) {
			error.setMessage("Please enter arrival time");
			error.setSuccess(ResponseStatus.FAILURE);
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		if ((request.getDepTime()==null || request.getDepTime().isEmpty()) && valid==true) {
			error.setMessage("Please enter departure time");
			error.setSuccess(ResponseStatus.FAILURE);
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		if ((request.getTravelName()==null || request.getTravelName().isEmpty()) && valid==true) {
			error.setMessage("Please enter travel name");
			error.setSuccess(ResponseStatus.FAILURE);
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		if ((request.getSource()==null || request.getSource().isEmpty()) && valid==true) {
			error.setMessage("Please enter source");
			error.setSuccess(ResponseStatus.FAILURE);
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		if ((request.getDestination()==null || request.getDestination().isEmpty()) && valid==true) {
			error.setMessage("Please enter destination");
			error.setSuccess(ResponseStatus.FAILURE);
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		if ((request.getSourceid()==null || request.getSourceid().isEmpty()) && valid==true) {
			error.setMessage("Please enter source id");
			error.setSuccess(ResponseStatus.FAILURE);
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		if ((request.getDestinationId()==null || request.getDestinationId().isEmpty()) && valid==true) {
			error.setMessage("Please enter destination id");
			error.setSuccess(ResponseStatus.FAILURE);
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		if ((request.getJourneyDate()==null || request.getJourneyDate().isEmpty()) && valid==true) {
			error.setMessage("Please enter journey date");
			error.setSuccess(ResponseStatus.FAILURE);
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		if ((request.getBoardpoint()==null || request.getBoardpoint().isEmpty()) && valid==true) {
			error.setMessage("Please enter board point");
			error.setSuccess(ResponseStatus.FAILURE);
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}if ((request.getBoardprime()==null || request.getBoardprime().isEmpty()) && valid==true) {
			error.setMessage("Please enter board prime");
			error.setSuccess(ResponseStatus.FAILURE);
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		if ((request.getBoardTime()==null || request.getBoardTime().isEmpty()) && valid==true) {
			error.setMessage("Please enter board time");
			error.setSuccess(ResponseStatus.FAILURE);
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		if ((request.getDropId()==null || request.getDropId().isEmpty()) && valid==true) {
			error.setMessage("Please enter drop id");
			error.setSuccess(ResponseStatus.FAILURE);
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		if ((request.getDropName()==null || request.getDropName().isEmpty()) && valid==true) {
			error.setMessage("Please enter drop name");
			error.setSuccess(ResponseStatus.FAILURE);
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		if ((request.getDropprime()==null || request.getDropprime().isEmpty()) && valid==true) {
			error.setMessage("Please enter drop prime");
			error.setSuccess(ResponseStatus.FAILURE);
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		if ((request.getDropTime()==null || request.getDropTime().isEmpty()) && valid==true) {
			error.setMessage("Please enter drop time");
			error.setSuccess(ResponseStatus.FAILURE);
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		if ((request.getRouteId()==null || request.getRouteId().isEmpty()) && valid==true) {
			error.setMessage("Please enter route id");
			error.setSuccess(ResponseStatus.FAILURE);
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		if ((request.getSeatDetail()==null || request.getSeatDetail().isEmpty()) && valid==true) {
			error.setMessage("Please enter seat detail");
			error.setSuccess(ResponseStatus.FAILURE);
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		if ((request.getwLCode()==null || request.getwLCode().isEmpty()) && valid==true) {
			error.setMessage("Please enter wl code");
			error.setSuccess(ResponseStatus.FAILURE);
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		if ((request.getIpAddress()==null || request.getIpAddress().isEmpty()) && valid==true) {
			error.setMessage("Please enter ip address");
			error.setSuccess(ResponseStatus.FAILURE);
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}

		if (valid) {
			if (request.getTravellers()!=null|| !request.getTravellers().isEmpty()) {
			for (int i = 0; i < request.getTravellers().size(); i++) {
				TravellerDetailsDTO tdDTO=request.getTravellers().get(i);

				if ((tdDTO.getTitle()==null || tdDTO.getTitle().isEmpty()) && valid==true) {
					error.setMessage("Please enter title");
					error.setSuccess(ResponseStatus.FAILURE);
					error.setCode(ResponseStatus.FAILURE.getValue());
					valid=false;
				}
				if ((tdDTO.getfName()==null || tdDTO.getfName().isEmpty()) && valid==true) {
					error.setMessage("Please enter first name");
					error.setSuccess(ResponseStatus.FAILURE);
					error.setCode(ResponseStatus.FAILURE.getValue());
					valid=false;
				}
				if ((tdDTO.getlName()==null || tdDTO.getlName().isEmpty()) && valid==true) {
					error.setMessage("Please enter last name");
					error.setSuccess(ResponseStatus.FAILURE);
					error.setCode(ResponseStatus.FAILURE.getValue());
					valid=false;
				}
				if ((tdDTO.getAge()==null || tdDTO.getAge().isEmpty()) && valid==true) {
					error.setMessage("Please enter age");
					error.setSuccess(ResponseStatus.FAILURE);
					error.setCode(ResponseStatus.FAILURE.getValue());
					valid=false;
				}
				if ((tdDTO.getGender()==null || tdDTO.getGender().isEmpty()) && valid==true) {
					error.setMessage("Please enter gender");
					error.setSuccess(ResponseStatus.FAILURE);
					error.setCode(ResponseStatus.FAILURE.getValue());
					valid=false;
				}
				if ((tdDTO.getSeatNo()==null || tdDTO.getSeatNo().isEmpty()) && valid==true) {
					error.setMessage("Please enter seat no");
					error.setSuccess(ResponseStatus.FAILURE);
					error.setCode(ResponseStatus.FAILURE.getValue());
					valid=false;
				}
				if ((tdDTO.getSeatType()==null || tdDTO.getSeatType().isEmpty()) && valid==true) {
					error.setMessage("Please enter seat type");
					error.setSuccess(ResponseStatus.FAILURE);
					error.setCode(ResponseStatus.FAILURE.getValue());
					valid=false;
				}
				if ((tdDTO.getFare()==null || tdDTO.getFare().isEmpty()) && valid==true) {
					error.setMessage("Please enter fare");
					error.setSuccess(ResponseStatus.FAILURE);
					error.setCode(ResponseStatus.FAILURE.getValue());
					valid=false;
				}
				tdDTO=null;
			}
		}
			}
		
		if(valid) {
			error.setSuccess(ResponseStatus.SUCCESS);
			error.setCode(ResponseStatus.SUCCESS.getValue());
		}
		return error;
	}

	public BusRequestError bookTicketVal(BookTicketReq request)
	{
		BusRequestError error=new BusRequestError();
		boolean valid=true;
		if (request.getTransactionId()==null || request.getTransactionId().isEmpty()|| request.getTransactionId().equalsIgnoreCase("null")) {
			error.setMessage("Please enter transaction id");
			error.setSuccess(ResponseStatus.FAILURE);
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		if ((request.getMdexTxnId()==null || request.getMdexTxnId().isEmpty() ||request.getMdexTxnId().equalsIgnoreCase("null")) && valid==true) {
			error.setMessage("Please enter Mdex Transaction id");
			error.setSuccess(ResponseStatus.FAILURE);
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		if ((request.getVpqTxnId()==null || request.getVpqTxnId().isEmpty() ||request.getVpqTxnId().equalsIgnoreCase("null")) && valid==true) {
			error.setMessage("Please enter Vpq Transaction id");
			error.setSuccess(ResponseStatus.FAILURE);
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		if (valid) {
			error.setSuccess(ResponseStatus.SUCCESS);
			error.setCode(ResponseStatus.SUCCESS.getValue());
		}
		return error;
	}


	public BusRequestError isCancellableVal(IsCancellableReq request)
	{
		BusRequestError error=new BusRequestError();
		boolean valid=true;
		if (request.getBookId()==null || request.getBookId().isEmpty()) {
			error.setMessage("Please enter booking id");
			error.setSuccess(ResponseStatus.FAILURE);
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}if ((request.getCanceltype()==null || request.getCanceltype().isEmpty()) && valid==true) {
			error.setMessage("Please enter cancel type");
			error.setSuccess(ResponseStatus.FAILURE);
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		else {
			error.setSuccess(ResponseStatus.SUCCESS);
			error.setCode(ResponseStatus.SUCCESS.getValue());
		}
		return error;
	}

	
	public BusRequestError bookTicketValUpdated(BookTicketReq request)
	{
		BusRequestError error=new BusRequestError();
		boolean valid=true;
		if (request.getTransactionId()==null || request.getTransactionId().isEmpty()|| request.getTransactionId().equalsIgnoreCase("null")) {
			error.setMessage("Please enter transaction id");
			error.setSuccess(ResponseStatus.FAILURE);
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		else if ((request.getAmount()==0)){
			error.setMessage("Please enter valid amount");
			error.setSuccess(ResponseStatus.FAILURE);
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		else if ((request.getSeatHoldId()==null || request.getSeatHoldId().isEmpty()|| request.getSeatHoldId().equalsIgnoreCase("null"))){
			error.setMessage("Please enter seat hold id");
			error.setSuccess(ResponseStatus.FAILURE);
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}
		
		if (valid) {
			error.setSuccess(ResponseStatus.SUCCESS);
			error.setCode(ResponseStatus.SUCCESS.getValue());
		}
		return error;
	}

	
	public BusRequestError getDynamicFareDetails(String val)
	{
		BusRequestError error=new BusRequestError();
		boolean valid=true;
		if (val==null || val.isEmpty()) {
			error.setMessage("Please enter valid data");
			error.setSuccess(ResponseStatus.FAILURE);
			error.setCode(ResponseStatus.FAILURE.getValue());
			valid=false;
		}else {
			error.setSuccess(ResponseStatus.SUCCESS);
			error.setCode(ResponseStatus.SUCCESS.getValue());
		}
		return error;
	}
}
