package com.payqwikweb.validation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.payqwikweb.app.model.request.ASendMoneyBankRequest;
import com.payqwikweb.app.model.request.SendMoneyBankRequest;
import com.payqwikweb.app.model.request.SendMoneyMobileRequest;
import com.payqwikweb.model.error.SendMoneyBankError;
import com.payqwikweb.model.error.SendMoneyMobileError;
import com.payqwikweb.validation.CommonValidation;

public class SendMoneyValidation {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	
	public SendMoneyMobileError checkMobileError(SendMoneyMobileRequest mobile) {
		SendMoneyMobileError error = new SendMoneyMobileError();
		boolean valid = true;
		double amount = Double.parseDouble(mobile.getAmount());
		if (CommonValidation.isNull(mobile.getAmount())) {
			error.setAmount("Please enter amount");
			error.setMobileNumber("Please enter amount");
			valid = false;
		}else if (!CommonValidation.isNumeric(mobile.getAmount())) {
			error.setAmount("Enter valid amount in field");
			error.setMobileNumber("Enter valid amount in field");
			valid = false;
		}else if(amount < 10){
			error.setAmount("Amount must be greater than or equal to 10");
			error.setMobileNumber("Amount must be greater than or equal to 10");
			valid = false;
		}
		if (CommonValidation.isNull(mobile.getMobileNumber())) {
			error.setMobileNumber("Please enter mobile number");
			error.setMobileNumber("Please enter mobile number");
			valid = false;
		}else if (!CommonValidation.isNumeric(mobile.getMobileNumber())) {
			error.setMobileNumber("Enter valid mobile number");
			error.setMobileNumber("Enter valid mobile number");
			valid = false;
		}else if (!CommonValidation.checkLength10(mobile.getMobileNumber())) {
			error.setMobileNumber("Enter 10 digit mobile number");
			error.setMobileNumber("Enter 10 digit mobile number");
			valid = false;
		}else if(!CommonValidation.isValidMobile(mobile.getMobileNumber())){
			valid = false;
			error.setMobileNumber("Enter valid Mobile Number");
		}
		if(CommonValidation.isNull(mobile.getMessage())){
			valid = false;
			error.setMobileNumber("Please enter valid message");
		}
		error.setValid(valid);
		return error;
	}

	public SendMoneyBankError checkBankError(SendMoneyBankRequest bank) {
		SendMoneyBankError error = new SendMoneyBankError();
		boolean valid = true;

		if(CommonValidation.isNull(bank.getAccountName())){
			valid = false;
			error.setAccountName("Enter Account Name");
		}
		if(CommonValidation.isNull(bank.getAmount())){
			valid  = false;
			error.setAmount("Enter Amount");
		}else if(!CommonValidation.isNumeric(bank.getAmount())){
			valid = false;
			error.setAmount("Enter valid amount");
		}else {
			double amount = Double.parseDouble(bank.getAmount());
			if(amount < 500){
				valid = false;
				error.setAmount("Amount must be at least 500");
			}
		}
		if(CommonValidation.isNull(bank.getBankCode())){
			valid = false;
			error.setBankCode("Select Bank");
		}else if(!CommonValidation.containsAlphabets(bank.getBankCode())){
			valid = false;
			error.setBankCode("Not a valid bank code");
		}

		if(CommonValidation.isNull(bank.getIfscCode())){
			valid = false;
			error.setIfscCode("Enter IFSC Code");
		}

		if(Long.parseLong(bank.getAccountNumber()) < 99999){
			valid = false;
			error.setAccountNumber("Enter Valid Account Number");
		}
		error.setValid(valid);
		return error;

	}
	
	public SendMoneyBankError agentSendBankError(ASendMoneyBankRequest req){
		SendMoneyBankError error = new SendMoneyBankError();
		boolean valid=true;
		String message = "";
		if(CommonValidation.isNull(req.getAccountName())){
			valid=false;
			message="Enter receiver first name";
		}
		else if(!CommonValidation.containsAlphabets(req.getAccountName())){
			valid=false;
			message="Enter valid receiver first name";
		}
		
		if(CommonValidation.isNull(req.getReceiverLastName())){
			valid=false;
			message="Enter receiver last name";
		}
		else if(!CommonValidation.containsAlphabets(req.getReceiverLastName())){
			valid=false;
			message="Enter valid receiver last name";
		}
		
		if(CommonValidation.isNull(req.getSenderName())){
			valid=false;
			message="Enter sender first name";
		}
		else if(!CommonValidation.containsAlphabets(req.getSenderName())){
			valid=false;
			message="Enter valid sender first name";
		}
		
		if(CommonValidation.isNull(req.getSenderLastName())){
			valid=false;
			message="Enter sender last name";
		}
		else if(!CommonValidation.containsAlphabets(req.getSenderLastName())){
			valid=false;
			message="Enter valid sender last name";
		}
		
		if (CommonValidation.isNull(req.getSenderMobileNo())) {
			valid = false;
			message = "Please enter sender mobile number";
		}
		else if (!CommonValidation.isNumeric(req.getSenderMobileNo())) {
			valid = false;
			message = "Please enter sender mobile number";
		} else if (!CommonValidation.checkLength10(req.getSenderMobileNo())) {
			valid = false;
			message = "";
		}
		
		if (CommonValidation.isNull(req.getReceiverMobileNo())) {
			valid = false;
			message = "Please enter sender mobile number";
		}
		else if (!CommonValidation.isNumeric(req.getReceiverMobileNo())) {
			valid = false;
			message = "Please enter sender mobile number";
		} else if (!CommonValidation.checkLength10(req.getReceiverMobileNo())) {
			valid = false;
			message = "";
		}
		
		if (CommonValidation.isNull(req.getSenderEmailId())) {
			valid = false;
			message = "Please enter sender email address";
		}
		else if (!req.getSenderEmailId().matches(EMAIL_PATTERN)) {
			valid = false;
			message = "Please enter valid sender email address";
		} else if (!CommonValidation.isValidMail(req.getSenderEmailId())) {
			valid = false;
			message = "Please enter valid sender email address";
		}
		
		if(CommonValidation.isNull(req.getIdProofNo())){
			valid=false;
			message = "Please enter IdProof Number";
		}
		if(req.getIdProofImage()==null){
			valid=false;
			message = "Please enter IdProof Number";
		}
		
		if (CommonValidation.isNull(req.getReceiverEmailId())) {
			valid = false;
			message = "Please enter receiver email address";
		}
		else if (!req.getReceiverEmailId().matches(EMAIL_PATTERN)) {
			valid = false;
			message = "Please enter valid receiver email address";
		} else if (!CommonValidation.isValidMail(req.getReceiverEmailId())) {
			valid = false;
			message = "Please enter valid receiver email address";
		}
		
		if(CommonValidation.isNull(req.getAccountNumber())){
			valid=false;
			message="Enter accountNumber";
		}else if(!CommonValidation.isNumeric(req.getAccountNumber())){
			valid=false;
			message="Enter valid accountNumber";
		}else if(req.getAccountNumber().length()>16){
			valid=false;
			message="Enter valid max 16 digit length accountNumber";
		}else if(Long.parseLong(req.getAccountNumber()) < 99999){
			valid = false;
			message="Enter Valid Account Number";
		}
		
		if(CommonValidation.isNull(req.getAmount())){
			valid=false;
			message="Enter amount";
		}else if(!CommonValidation.isNumeric(req.getAmount())){
			valid=false;
			message="Enter valid amount";
		}else {
			double amount = Double.parseDouble(req.getAmount());
			if(amount < 500 || amount >5000){
				valid = false;
				message="Amount must be at least 500 and maxmimum 5000";
			}
		}
		
		if(CommonValidation.isNull(req.getBankName())){
			valid=false;
			message="Enter bankName";
		}
		error.setValid(valid);
		error.setMessage(message);
		return error;
	}
}
