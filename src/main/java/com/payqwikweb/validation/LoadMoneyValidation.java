package com.payqwikweb.validation;

import com.payqwikweb.app.model.request.LoadMoneyRequest;
import com.payqwikweb.model.app.request.LoadMoneyFlightRequest;
import com.payqwikweb.model.error.LoadMoneyError;
import com.thirdparty.model.error.CommonError;

public class LoadMoneyValidation {

	public LoadMoneyError checkError(LoadMoneyRequest request){
		boolean valid = true;
		double amount  = Double.parseDouble(request.getAmount());
		LoadMoneyError error = new LoadMoneyError();
		if(CommonValidation.isNull(request.getAmount())){
			error.setAmount("Enter amount");
			valid = false;
		}
		else if(!CommonValidation.isNumeric(request.getAmount())){
			error.setAmount("Please enter valid amount");
			valid = false;
		}
		else if(amount <= 0 || amount >= 10000) {
			error.setAmount("Amount must be between 1 to 10000");
			valid  = false;
		}
		error.setValid(valid);
		return error;
		
	}
	
	public LoadMoneyError checkUpiError(LoadMoneyRequest request){
		boolean valid = true;
		double amount  = Double.parseDouble(request.getAmount());
		LoadMoneyError error = new LoadMoneyError();
		if(CommonValidation.isNull(request.getAmount())){
			error.setAmount("Enter amount");
			valid = false;
		}
		else if(!CommonValidation.isNumeric(request.getAmount())){
			error.setAmount("Please enter valid amount");
			valid = false;
		}

		else if(amount <= 0 && amount >= 10000) {
			error.setAmount("Amount must be between 1 to 10000");
			valid  = false;
		}
		
		/*if(CommonValidation.isNull(request.getPayerVirAddr())){
			error.setAmount("Enter Virtual Payment Address");
			valid = false;
		}*/
		
		error.setValid(valid);
		return error;
		
	}
	
	public LoadMoneyError checkErrorFlight(LoadMoneyFlightRequest request){
		boolean valid = true;
		double amount  = Double.parseDouble(request.getAmount());
		LoadMoneyError error = new LoadMoneyError();
		if(CommonValidation.isNull(request.getAmount())){
			error.setAmount("Enter amount");
			valid = false;
		}
		else if(!CommonValidation.isNumeric(request.getAmount())){
			error.setAmount("Please enter valid amount");
			valid = false;
		}

		else if(amount >= 0 && amount >= 10000) {
			error.setAmount("Amount must be between 1 to 10000");
			valid  = false;
		}
		error.setValid(valid);
		return error;
		
	}
	
	public LoadMoneyError agentLoadMoney(LoadMoneyRequest request){
		boolean valid = true;
		double amount  = Double.parseDouble(request.getAmount());
		LoadMoneyError error = new LoadMoneyError();
		if(CommonValidation.isNull(request.getAmount())){
			error.setAmount("Enter amount");
			valid = false;
		}
		else if(!CommonValidation.isNumeric(request.getAmount())){
			error.setAmount("Please enter valid amount");
			valid = false;
		}
		else if(amount <= 0 || amount >= 100000) {
			error.setAmount("Amount must be between 10 to 100000");
			valid  = false;
		}
		error.setValid(valid);
		return error;
		
	}

	public CommonError checkLoadMoneyError(LoadMoneyRequest request){
		boolean valid = true;
		CommonError error = new CommonError();
		if(CommonValidation.isNull(request.getVoucherNumber())){
			error.setMessage("Please Enter Voucher Number");
			valid = false;
		}
		else if(!CommonValidation.is16DigitVoucher(request.getVoucherNumber())){
			error.setMessage("Please Enter 16 Digit Voucher Number");
			valid = false;
		}
		error.setValid(valid);
		return error;
	}
	
}
