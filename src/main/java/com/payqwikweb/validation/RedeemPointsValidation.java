package com.payqwikweb.validation;

import com.payqwikweb.app.model.request.RedeemPointsRequest;
import com.payqwikweb.app.model.request.RegistrationRequest;
import com.payqwikweb.model.error.RedeemPointsError;

public class RedeemPointsValidation {
    
	public RedeemPointsError checkPointsError(RedeemPointsRequest redeemPointsRequest) {
		RedeemPointsError error = new RedeemPointsError();
		boolean valid = true;
		if (CommonValidation.isNull(redeemPointsRequest.getPoints())) {
			error.setMessage("Please enter valid points number");
			valid = false;
		}
		else if(CommonValidation.alpha(redeemPointsRequest.getPoints())){
			error.setMessage("Please enter valid points number");
			valid=false;
		}
		else if(CommonValidation.isNegitive(redeemPointsRequest.getPoints())){
			error.setMessage("Please enter at least minimum 500 points");
			valid=false;
		}
		/*else if(CommonValidation.isFloat(redeemPointsRequest.getPoints())){
			error.setMessage("Please enter only Integer number not float number");
			valid=false;
		}*/
		error.setValid(valid);
	return error;
	}
} 
