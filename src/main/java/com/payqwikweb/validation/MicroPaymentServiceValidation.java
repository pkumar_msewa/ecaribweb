package com.payqwikweb.validation;

import com.payqwikweb.app.model.request.DirectPaymentRequest;
import com.payqwikweb.app.model.request.MicroPaymentInitiateRequest;
import com.payqwikweb.app.model.request.MicroPaymentRequest;

public class MicroPaymentServiceValidation {

	public static boolean validateTokenMicroPaymentRequest(MicroPaymentRequest request) {
		boolean flag = false;
		if (request.getConsumerSecretKey().trim() != null && request.getConsumerTokenKey().trim() != null
				&& request.getMobileNumber().trim() != null && request.getRetrivalReferenceNumber() != null && request.getRetrivalReferenceNumber().trim().length() != 0
				&& request.getSecretKey().trim() != null 
				&& request.getTokenKey().trim() != null && request.getTransactionRefNo().trim() != null)
			flag = true;
		return flag;
	}

	public static boolean validateMicroPaymentInitiateRequest(MicroPaymentInitiateRequest request) {
		boolean flag = false;
		if (request.getAuthCode().trim() != null && request.getConsumerSecretKey().trim() != null
				&& request.getConsumerTokenKey().trim() != null&& request.getMobileNumber().trim() != null && request.getSecretKey().trim() != null
				&& request.getTokenKey().trim() != null)
			flag = true;
		return flag;
	}

	public static boolean validateDirectPaymentGenerateToken(DirectPaymentRequest request) {
		boolean flag = false;
		if (request.getMobileNumber() != null)
			flag = true;
		return flag;
	}

	public static boolean validateDirectPaymentInitiateRequest(DirectPaymentRequest request) {
		boolean flag = false;
		if (request.getMobileNumber() != null && request.getConsumerSecretKey() != null
				&& request.getConsumerSecretKey() != null && request.getTransactionRefNo() != null
				&& request.getTokenKey() != null && request.getOtp() != null)
			flag = true;
		return flag;
	}

	public static boolean validateDirectPaymentUpdateTransaction(DirectPaymentRequest request) {
		boolean flag = false;
		if (request.getMobileNumber() != null && request.getConsumerSecretKey() != null
				&& request.getConsumerTokenKey() != null && request.getTokenKey() != null && request.getStatus() != null
				&& request.getRetrivalReferenceNumber() != null && request.getTransactionRefNo() != null)
			flag = true;
		return flag;
	}
}
