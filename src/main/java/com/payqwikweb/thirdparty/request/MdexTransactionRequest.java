package com.payqwikweb.thirdparty.request;

public class MdexTransactionRequest {

	private String mobileNo;
	private String serviceProvider;
	private String transactionId;
	private String area;
	private String topupType;
	private String dthNo;
	
	
		public String getDthNo() {
		return dthNo;
	}
	public void setDthNo(String dthNo) {
		this.dthNo = dthNo;
	}
		public String getTopupType() {
		return topupType;
	}
	public void setTopupType(String topupType) {
		this.topupType = topupType;
	}
		public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getServiceProvider() {
		return serviceProvider;
	}
	public void setServiceProvider(String serviceProvider) {
		this.serviceProvider = serviceProvider;
	}
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	
}
