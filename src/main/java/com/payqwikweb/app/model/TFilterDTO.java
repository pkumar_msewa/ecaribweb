package com.payqwikweb.app.model;

import com.payqwikweb.app.model.request.SessionDTO;
import com.payqwikweb.model.web.Status;

public class TFilterDTO extends SessionDTO {

    private String startDate;

    private String endDate;

    private Status status;

    private String serviceType;
    
    private String transactionRefNo;
    
    
    public String getTransactionRefNo() {
		return transactionRefNo;
	}

	public void setTransactionRefNo(String transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}

	public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }
}
