package com.payqwikweb.app.model;


import com.payqwikweb.app.model.request.SessionDTO;
import com.thirdparty.model.JSONWrapper;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

public class AccessDTO extends SessionDTO implements JSONWrapper{

    private boolean updateAuthUser;

    private boolean updateAuthAdmin;

    private boolean updateAuthMerchants;

    private boolean updateAuthAgents;

    private boolean sendGCM;

    private boolean sendSMS;

    private boolean sendMail;

    private boolean updateLimits;

    private boolean loadMoney;

    private boolean sendMoney;

    private boolean requestMoney;

    private boolean deductMoney;

    private String username;

    private String authority;


    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    public boolean isUpdateAuthUser() {
        return updateAuthUser;
    }

    public void setUpdateAuthUser(boolean updateAuthUser) {
        this.updateAuthUser = updateAuthUser;
    }

    public boolean isUpdateAuthAdmin() {
        return updateAuthAdmin;
    }

    public void setUpdateAuthAdmin(boolean updateAuthAdmin) {
        this.updateAuthAdmin = updateAuthAdmin;
    }

    public boolean isUpdateAuthMerchants() {
        return updateAuthMerchants;
    }

    public void setUpdateAuthMerchants(boolean updateAuthMerchants) {
        this.updateAuthMerchants = updateAuthMerchants;
    }

    public boolean isUpdateAuthAgents() {
        return updateAuthAgents;
    }

    public void setUpdateAuthAgents(boolean updateAuthAgents) {
        this.updateAuthAgents = updateAuthAgents;
    }

    public boolean isSendGCM() {
        return sendGCM;
    }

    public void setSendGCM(boolean sendGCM) {
        this.sendGCM = sendGCM;
    }

    public boolean isSendSMS() {
        return sendSMS;
    }

    public void setSendSMS(boolean sendSMS) {
        this.sendSMS = sendSMS;
    }

    public boolean isSendMail() {
        return sendMail;
    }

    public void setSendMail(boolean sendMail) {
        this.sendMail = sendMail;
    }

    public boolean isUpdateLimits() {
        return updateLimits;
    }

    public void setUpdateLimits(boolean updateLimits) {
        this.updateLimits = updateLimits;
    }

    public boolean isLoadMoney() {
        return loadMoney;
    }

    public void setLoadMoney(boolean loadMoney) {
        this.loadMoney = loadMoney;
    }

    public boolean isSendMoney() {
        return sendMoney;
    }

    public void setSendMoney(boolean sendMoney) {
        this.sendMoney = sendMoney;
    }

    public boolean isRequestMoney() {
        return requestMoney;
    }

    public void setRequestMoney(boolean requestMoney) {
        this.requestMoney = requestMoney;
    }

    public boolean isDeductMoney() {
        return deductMoney;
    }

    public void setDeductMoney(boolean deductMoney) {
        this.deductMoney = deductMoney;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    @Override
    public JSONObject toJSON() {
        JSONObject payload = new JSONObject();
        try{
            payload.put("sessionId",getSessionId());
            payload.put("username",getUsername());
            payload.put("deductMoney",isDeductMoney());
            payload.put("requestMoney",isRequestMoney());
            payload.put("sendMoney",isSendMoney());
            payload.put("loadMoney",isLoadMoney());
            payload.put("updateLimits",isUpdateLimits());
            payload.put("sendMail",isSendMail());
            payload.put("sendSMS",isSendSMS());
            payload.put("sendGCM",isSendGCM());
            payload.put("updateAuthAgents",isUpdateAuthAgents());
            payload.put("updateAuthUser",isUpdateAuthUser());
            payload.put("updateAuthMerchants",isUpdateAuthMerchants());
            payload.put("updateAuthAdmin",isUpdateAuthAdmin());
        }catch(JSONException ex){
            ex.printStackTrace();
        }
        return payload;
    }
}
