package com.payqwikweb.app.model;

public class NotificationsDTO {
    private String ipayBalance;

    public String getIpayBalance() {
        return ipayBalance;
    }

    public void setIpayBalance(String ipayBalance) {
        this.ipayBalance = ipayBalance;
    }
}
