package com.payqwikweb.app.model;

public class WoohooGiftCardDTO {
	
	private String firstName;
	private String lastName;
	private String emailId;
	private String contanctNo;
	private String orderId;
	private String status;
	private String transactionRefNo;
	private String cardName;
	private String expiry_date;
	private String cardnumber;
	private String card_price;
	private String pin_or_url;
	private String retrivalRefNo;
	private String description;
	private String userFirstName;
	private String userPhoneNumber;
	private String dateOfTrasnaction;
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getUserFirstName() {
		return userFirstName;
	}
	public void setUserFirstName(String userFirstName) {
		this.userFirstName = userFirstName;
	}
	public String getUserPhoneNumber() {
		return userPhoneNumber;
	}
	public void setUserPhoneNumber(String userPhoneNumber) {
		this.userPhoneNumber = userPhoneNumber;
	}
	public String getDateOfTrasnaction() {
		return dateOfTrasnaction;
	}
	public void setDateOfTrasnaction(String dateOfTrasnaction) {
		this.dateOfTrasnaction = dateOfTrasnaction;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getContanctNo() {
		return contanctNo;
	}
	public void setContanctNo(String contanctNo) {
		this.contanctNo = contanctNo;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTransactionRefNo() {
		return transactionRefNo;
	}
	public void setTransactionRefNo(String transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}
	public String getCardName() {
		return cardName;
	}
	public void setCardName(String cardName) {
		this.cardName = cardName;
	}
	public String getExpiry_date() {
		return expiry_date;
	}
	public void setExpiry_date(String expiry_date) {
		this.expiry_date = expiry_date;
	}
	public String getCardnumber() {
		return cardnumber;
	}
	public void setCardnumber(String cardnumber) {
		this.cardnumber = cardnumber;
	}
	public String getCard_price() {
		return card_price;
	}
	public void setCard_price(String card_price) {
		this.card_price = card_price;
	}
	public String getPin_or_url() {
		return pin_or_url;
	}
	public void setPin_or_url(String pin_or_url) {
		this.pin_or_url = pin_or_url;
	}
	public String getRetrivalRefNo() {
		return retrivalRefNo;
	}
	public void setRetrivalRefNo(String retrivalRefNo) {
		this.retrivalRefNo = retrivalRefNo;
	}

}
