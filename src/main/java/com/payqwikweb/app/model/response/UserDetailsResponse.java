package com.payqwikweb.app.model.response;

public class UserDetailsResponse {

	private boolean success;
	private String code;
	private String message;
	private String status;
	private String details;
	private String response;
	private String userDetail;
	private String accountDetail;
	private int points;
	private String username;
	private String firstName;
	private String middleName;
	private String lastName;
	private String address;
	private String contactNo;
	private String userType;
	private String authority;
	private String emailStatus;
	private String mobileStatus;
	private String email;
	private String image;
	private String sessionId;
	private double balance;
	private String accountType;
	private double dailyTransaction;
	private double monthlyTransaction;
	private long accountNumber;
	private String desc;
	private boolean flag;
	private String imageContent;
	private boolean hasRefer;
	private String totalRegisterUser;
	private double debitAmount;
	private String mdexToken;
	private String mdexKey;
	private boolean iplEnable;
	private String iplPrediction;
	private String iplSchedule;
	private String iplMyPrediction;
	private String mdexUsername;
	private String mdexPassword;
	private String mdexUrl;
	private boolean ebsEnable;
	private boolean vnetEnable;
	private boolean razorPayEnable;
	private String topupImage;

	public String getTopupImage() {
		return topupImage;
	}

	public void setTopupImage(String topupImage) {
		this.topupImage = topupImage;
	}

	public boolean isEbsEnable() {
		return ebsEnable;
	}

	public void setEbsEnable(boolean ebsEnable) {
		this.ebsEnable = ebsEnable;
	}

	public boolean isVnetEnable() {
		return vnetEnable;
	}

	public void setVnetEnable(boolean vnetEnable) {
		this.vnetEnable = vnetEnable;
	}

	public boolean isRazorPayEnable() {
		return razorPayEnable;
	}

	public void setRazorPayEnable(boolean razorPayEnable) {
		this.razorPayEnable = razorPayEnable;
	}

	public String getMdexUsername() {
		return mdexUsername;
	}

	public void setMdexUsername(String mdexUsername) {
		this.mdexUsername = mdexUsername;
	}

	public String getMdexPassword() {
		return mdexPassword;
	}

	public void setMdexPassword(String mdexPassword) {
		this.mdexPassword = mdexPassword;
	}

	public String getMdexUrl() {
		return mdexUrl;
	}

	public void setMdexUrl(String mdexUrl) {
		this.mdexUrl = mdexUrl;
	}

	public boolean isIplEnable() {
		return iplEnable;
	}

	public void setIplEnable(boolean iplEnable) {
		this.iplEnable = iplEnable;
	}

	public String getMdexToken() {
		return mdexToken;
	}

	public void setMdexToken(String mdexToken) {
		this.mdexToken = mdexToken;
	}

	public String getMdexKey() {
		return mdexKey;
	}

	public void setMdexKey(String mdexKey) {
		this.mdexKey = mdexKey;
	}

	public String getIplPrediction() {
		return iplPrediction;
	}

	public void setIplPrediction(String iplPrediction) {
		this.iplPrediction = iplPrediction;
	}

	public String getIplSchedule() {
		return iplSchedule;
	}

	public void setIplSchedule(String iplSchedule) {
		this.iplSchedule = iplSchedule;
	}

	public String getIplMyPrediction() {
		return iplMyPrediction;
	}

	public void setIplMyPrediction(String iplMyPrediction) {
		this.iplMyPrediction = iplMyPrediction;
	}

	public String getTotalRegisterUser() {
		return totalRegisterUser;
	}

	public void setTotalRegisterUser(String totalRegisterUser) {
		this.totalRegisterUser = totalRegisterUser;
	}

	public double getDebitAmount() {
		return debitAmount;
	}

	public void setDebitAmount(double debitAmount) {
		this.debitAmount = debitAmount;
	}

	public boolean isHasRefer() {
		return hasRefer;
	}

	public void setHasRefer(boolean hasRefer) {
		this.hasRefer = hasRefer;
	}

	public String getImageContent() {
		return imageContent;
	}

	public void setImageContent(String imageContent) {
		this.imageContent = imageContent;
	}

	public int getPoints() {
		return points;
	}

	public void setPoints(int points) {
		this.points = points;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public String getUserDetail() {
		return userDetail;
	}

	public void setUserDetail(String userDetail) {
		this.userDetail = userDetail;
	}

	public String getAccountDetail() {
		return accountDetail;
	}

	public void setAccountDetail(String accountDetail) {
		this.accountDetail = accountDetail;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getAuthority() {
		return authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}

	public String getEmailStatus() {
		return emailStatus;
	}

	public void setEmailStatus(String emailStatus) {
		this.emailStatus = emailStatus;
	}

	public String getMobileStatus() {
		return mobileStatus;
	}

	public void setMobileStatus(String mobileStatus) {
		this.mobileStatus = mobileStatus;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public double getDailyTransaction() {
		return dailyTransaction;
	}

	public void setDailyTransaction(double dailyTransaction) {
		this.dailyTransaction = dailyTransaction;
	}

	public double getMonthlyTransaction() {
		return monthlyTransaction;
	}

	public void setMonthlyTransaction(double monthlyTransaction) {
		this.monthlyTransaction = monthlyTransaction;
	}

	public long getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(long accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

}
