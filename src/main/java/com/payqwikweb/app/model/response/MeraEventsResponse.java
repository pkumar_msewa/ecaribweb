package com.payqwikweb.app.model.response;

import org.json.JSONArray;

public class MeraEventsResponse {

	private boolean success;
	private String code;
	private String message;
	private String status;
	private Object details;
	private String response;
	private JSONArray jsonArray;
	private String eventSignUpId;
	private double totalAmount;
	private String allresponse;
	private String apiStatus;

	private String balance;
	private String txnId;

	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}

	public String getTxnId() {
		return txnId;
	}

	public void setTxnId(String txnId) {
		this.txnId = txnId;
	}

	public String getApiStatus() {
		return apiStatus;
	}

	public void setApiStatus(String apiStatus) {
		this.apiStatus = apiStatus;
	}

	public String getAllresponse() {
		return allresponse;
	}

	public void setAllresponse(String allresponse) {
		this.allresponse = allresponse;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getEventSignUpId() {
		return eventSignUpId;
	}

	public void setEventSignUpId(String eventSignUpId) {
		this.eventSignUpId = eventSignUpId;
	}

	public JSONArray getJsonArray() {
		return jsonArray;
	}

	public void setJsonArray(JSONArray jsonArray) {
		this.jsonArray = jsonArray;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Object getDetails() {
		return details;
	}

	public void setDetails(Object details) {
		this.details = details;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

}
