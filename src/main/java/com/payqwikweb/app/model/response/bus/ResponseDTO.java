package com.payqwikweb.app.model.response.bus;

import java.util.List;

public class ResponseDTO {
	
	private String status;
	private String code;
	private String message;
	private Object details;
	private String splitAmount;
	private Object extraInfo;
	private List<String> seatNo;
	private double refAmt;
	private String transactionRefNo;
	private double cancellationCharges;
	
	private String vpayQwikBusBookingResp;
	private String mdexBusBookingResp;
	private String mdexStatus;
	private boolean isCancelable;
	private boolean success;
	
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getStatus() {
		return status;
	}
	public Object getDetails() {
		return details;
	}
	public void setDetails(Object details) {
		this.details = details;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getSplitAmount() {
		return splitAmount;
	}
	public void setSplitAmount(String splitAmount) {
		this.splitAmount = splitAmount;
	}
	public Object getExtraInfo() {
		return extraInfo;
	}
	public void setExtraInfo(Object extraInfo) {
		this.extraInfo = extraInfo;
	}
	public List<String> getSeatNo() {
		return seatNo;
	}
	public void setSeatNo(List<String> seatNo) {
		this.seatNo = seatNo;
	}
	public double getRefAmt() {
		return refAmt;
	}
	public void setRefAmt(double refAmt) {
		this.refAmt = refAmt;
	}
	public String getTransactionRefNo() {
		return transactionRefNo;
	}
	public void setTransactionRefNo(String transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}
	public double getCancellationCharges() {
		return cancellationCharges;
	}
	public void setCancellationCharges(double cancellationCharges) {
		this.cancellationCharges = cancellationCharges;
	}
	public String getVpayQwikBusBookingResp() {
		return vpayQwikBusBookingResp;
	}
	public void setVpayQwikBusBookingResp(String vpayQwikBusBookingResp) {
		this.vpayQwikBusBookingResp = vpayQwikBusBookingResp;
	}
	public String getMdexBusBookingResp() {
		return mdexBusBookingResp;
	}
	public void setMdexBusBookingResp(String mdexBusBookingResp) {
		this.mdexBusBookingResp = mdexBusBookingResp;
	}
	public String getMdexStatus() {
		return mdexStatus;
	}
	public void setMdexStatus(String mdexStatus) {
		this.mdexStatus = mdexStatus;
	}
	public boolean isCancelable() {
		return isCancelable;
	}
	public void setCancelable(boolean isCancelable) {
		this.isCancelable = isCancelable;
	}
	
}
