package com.payqwikweb.app.model.response;

import java.util.List;

import com.payqwikweb.app.model.request.BulkFileDTO;

public class BulkFileListResponse {

	private boolean success;
	private String message;
	List<BulkFileDTO> list;
	private String code;
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<BulkFileDTO> getList() {
		return list;
	}

	public void setList(List<BulkFileDTO> list) {
		this.list = list;
	}

}
