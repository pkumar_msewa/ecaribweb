package com.payqwikweb.app.model.response;

import com.payqwikweb.app.model.request.RefundDTO;
import com.payqwikweb.model.admin.TListDTO;

import java.util.List;

public class TListResponse {

    private boolean success;

    private String message;

    private String status;

    private List<TListDTO> list;
    
    private List<RefundDTO> refundList;
    
    public List<RefundDTO> getRefundList() {
		return refundList;
	}

	public void setRefundList(List<RefundDTO> refundList) {
		this.refundList = refundList;
	}

	public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public List<TListDTO> getList() {
        return list;
    }

    public void setList(List<TListDTO> list) {
        this.list = list;
    }
}
