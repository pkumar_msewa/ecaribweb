package com.payqwikweb.app.model.response;

import com.payqwikweb.app.model.AccessDTO;

public class UpdateAccessResponse {

    private boolean success;
    private String code;
    private AccessDTO details;
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public AccessDTO getDetails() {
        return details;
    }

    public void setDetails(AccessDTO details) {
        this.details = details;
    }
}
