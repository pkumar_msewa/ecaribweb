package com.payqwikweb.app.model.response;

import org.json.JSONObject;

import com.thirdparty.util.EventStatus;

public class MeraEventDetailsResponse extends MeraEventsResponse{

	private int id;
	private int ownerId;
	private String title;
	private String description;
	private String startDate;
	private String endDate;
	private EventStatus status;
	private String thumbnailPath;
	private String bannerPath;
	private JSONObject location;
	private String timeZone;
	private String timeZoneName;
	private String venueName;
	private String address1;
	private String address2;
	private String stateName;
	private String cityName;
	private String countryName;
	private String eventUrl;
	private String categoryName;
	private String subCategoryName;
	private String defaultThumbImage;
	private String defaultBannerImage;
	private String registrationType;
	
	
}
