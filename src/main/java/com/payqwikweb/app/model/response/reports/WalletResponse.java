package com.payqwikweb.app.model.response.reports;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.json.JSONArray;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WalletResponse {

	private boolean success;
	private String code;
	private String message;
	private String status;
	private String details;
	private String response;
	private List<Object> billdesk;
	private List<Object> ccavenu;
	private List<Object> userDetails;
	private JSONArray loadMoneyDetails;
	private JSONArray userListDetails;
	
	
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	public List<Object> getBilldesk() {
		return billdesk;
	}
	public void setBilldesk(List<Object> billdesk) {
		this.billdesk = billdesk;
	}
	public List<Object> getCcavenu() {
		return ccavenu;
	}
	public void setCcavenu(List<Object> ccavenu) {
		this.ccavenu = ccavenu;
	}
	public List<Object> getUserDetails() {
		return userDetails;
	}
	public void setUserDetails(List<Object> userDetails) {
		this.userDetails = userDetails;
	}
	public JSONArray getLoadMoneyDetails() {
		return loadMoneyDetails;
	}
	public void setLoadMoneyDetails(JSONArray loadMoneyDetails) {
		this.loadMoneyDetails = loadMoneyDetails;
	}
	public JSONArray getUserListDetails() {
		return userListDetails;
	}
	public void setUserListDetails(JSONArray userListDetails) {
		this.userListDetails = userListDetails;
	}
	
}