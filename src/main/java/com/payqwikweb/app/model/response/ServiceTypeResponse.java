package com.payqwikweb.app.model.response;

import com.payqwikweb.app.model.ServiceTypeDTO;
import com.payqwikweb.app.model.ServicesDTO;

import java.util.List;

public class ServiceTypeResponse {

    private List<ServiceTypeDTO> serviceDTOs;
    private String code;
    private boolean success;
    private List<ServicesDTO> servicesDTOs;
    private Object serviceList;
    private List<CommissionDTO> commList;
    
    public List<CommissionDTO> getCommList() {
		return commList;
	}

	public void setCommList(List<CommissionDTO> commList) {
		this.commList = commList;
	}

	public Object getServiceList() {
		return serviceList;
	}

	public void setServiceList(Object serviceList) {
		this.serviceList = serviceList;
	}

	public List<ServicesDTO> getServicesDTOs() {
        return servicesDTOs;
    }

    public void setServicesDTOs(List<ServicesDTO> servicesDTOs) {
        this.servicesDTOs = servicesDTOs;
    }

    public List<ServiceTypeDTO> getServiceDTOs() {
        return serviceDTOs;
    }

    public void setServiceDTOs(List<ServiceTypeDTO> serviceDTOs) {
        this.serviceDTOs = serviceDTOs;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
