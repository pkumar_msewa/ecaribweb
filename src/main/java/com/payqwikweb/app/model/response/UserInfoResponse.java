package com.payqwikweb.app.model.response;

import com.payqwikweb.app.model.UserAccountInfo;
import com.payqwikweb.app.model.UserBasicInfo;
import com.payqwikweb.app.model.UserLoginInfo;
import com.payqwikweb.app.model.UserTransactionsInfo;

import java.util.List;

public class UserInfoResponse {

    private boolean success;

    private String message;

    private UserBasicInfo basic;

    private List<UserTransactionsInfo> transactions;

    private List<UserLoginInfo> loginLogs;

    private UserAccountInfo account;

    private boolean online;

    private String totalCredit;

    private String totalDebit;
    
    private String code;
    
    public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getTotalCredit() {
        return totalCredit;
    }

    public void setTotalCredit(String totalCredit) {
        this.totalCredit = totalCredit;
    }

    public String getTotalDebit() {
        return totalDebit;
    }

    public void setTotalDebit(String totalDebit) {
        this.totalDebit = totalDebit;
    }

    public List<UserLoginInfo> getLoginLogs() {
        return loginLogs;
    }

    public void setLoginLogs(List<UserLoginInfo> loginLogs) {
        this.loginLogs = loginLogs;
    }

    public boolean isOnline() {
        return online;
    }

    public void setOnline(boolean online) {
        this.online = online;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UserBasicInfo getBasic() {
        return basic;
    }

    public void setBasic(UserBasicInfo basic) {
        this.basic = basic;
    }

    public List<UserTransactionsInfo> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<UserTransactionsInfo> transactions) {
        this.transactions = transactions;
    }

    public UserAccountInfo getAccount() {
        return account;
    }

    public void setAccount(UserAccountInfo account) {
        this.account = account;
    }
}
