package com.payqwikweb.app.model.response;

import com.payqwikweb.app.model.AccessDTO;

import java.util.List;

public class AccessListResponse {

    private boolean success;
    private String message;
    List<AccessDTO> list;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<AccessDTO> getList() {
        return list;
    }

    public void setList(List<AccessDTO> list) {
        this.list = list;
    }
}
