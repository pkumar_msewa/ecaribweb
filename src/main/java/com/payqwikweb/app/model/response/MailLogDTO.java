package com.payqwikweb.app.model.response;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class MailLogDTO {

    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private String executionTime;

    private String destination;

    private String sender;

    private String mailTemplate;

    private String status;

    public String getExecutionTime() {
        return executionTime;
    }

    private void setExecutionTime(String executionTime) {
        this.executionTime = executionTime;
    }

    public void setExecutionTime(long timeInMillis){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timeInMillis);
        setExecutionTime(sdf.format(calendar.getTime()));
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getMailTemplate() {
        return mailTemplate;
    }

    public void setMailTemplate(String mailTemplate) {
        this.mailTemplate = mailTemplate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
