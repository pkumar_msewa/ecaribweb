package com.payqwikweb.app.model.response.bus;

public class BookTicketResp {

	private String ticketPnr;
	private String operatorPnr;
	private boolean isTicket;
	private String message;
	
	public String getTicketPnr() {
		return ticketPnr;
	}
	public void setTicketPnr(String ticketPnr) {
		this.ticketPnr = ticketPnr;
	}
	public String getOperatorPnr() {
		return operatorPnr;
	}
	public void setOperatorPnr(String operatorPnr) {
		this.operatorPnr = operatorPnr;
	}
	public boolean isTicket() {
		return isTicket;
	}
	public void setTicket(boolean isTicket) {
		this.isTicket = isTicket;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
}
