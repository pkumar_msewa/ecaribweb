package com.payqwikweb.app.model.response;

import com.payqwikweb.app.model.request.OnePayRequest;

public class OneClickPayResponse {

	private boolean success;
	private String code;
	private String message;
	private String status;
	private String details;
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	public OnePayRequest getDto() {
		return dto;
	}
	public void setDto(OnePayRequest dto) {
		this.dto = dto;
	}
	private String response;
	private OnePayRequest dto;
	
}
