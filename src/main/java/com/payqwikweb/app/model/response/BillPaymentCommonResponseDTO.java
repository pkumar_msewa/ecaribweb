package com.payqwikweb.app.model.response;

import com.payqwikweb.app.model.request.BillPaymentCommonDTO;

public class BillPaymentCommonResponseDTO {
	
	private boolean success;
	private String code;
	private String message;
	private String status;
	private String details;
	private String response;
	private double remBalance;
	private String serviceCode;
	private String serviceName;
	private Object detail;
	private BillPaymentCommonDTO dto;
	
	
	public double getRemBalance() {
		return remBalance;
	}
	public void setRemBalance(double remBalance) {
		this.remBalance = remBalance;
	}
	public Object getDetail() {
		return detail;
	}
	public void setDetail(Object detail) {
		this.detail = detail;
	}
	public String getServiceCode() {
		return serviceCode;
	}
	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	public BillPaymentCommonDTO getDto() {
		return dto;
	}
	public void setDto(BillPaymentCommonDTO dto) {
		this.dto = dto;
	}

}
