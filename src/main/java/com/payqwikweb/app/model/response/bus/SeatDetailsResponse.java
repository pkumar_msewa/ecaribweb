package com.payqwikweb.app.model.response.bus;

public class SeatDetailsResponse {

	private String status;
	private String code;
	private String message;
	
	private boolean upperShow;
	private boolean lowerShow;
	private Object lower;
	private Object upper;
	private Object listBoardingPoints;
	private Object listDropingPoints;
	private int maxcolumn;
	private int maxrow;
	private double seatAcFare;
	private double seatNacFare;
	private double sleepAcFare;
	private double sleepNacFare;
	private double minFare;
	private double maxFare;
	private Object cancelPolicyList;

	
	public boolean isUpperShow() {
		return upperShow;
	}

	public void setUpperShow(boolean upperShow) {
		this.upperShow = upperShow;
	}

	public boolean isLowerShow() {
		return lowerShow;
	}

	public void setLowerShow(boolean lowerShow) {
		this.lowerShow = lowerShow;
	}

	public Object getLower() {
		return lower;
	}

	public void setLower(Object lower) {
		this.lower = lower;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getUpper() {
		return upper;
	}

	public void setUpper(Object upper) {
		this.upper = upper;
	}
	public Object getListBoardingPoints() {
		return listBoardingPoints;
	}
	public void setListBoardingPoints(Object listBoardingPoints) {
		this.listBoardingPoints = listBoardingPoints;
	}

	public Object getListDropingPoints() {
		return listDropingPoints;
	}

	public void setListDropingPoints(Object listDropingPoints) {
		this.listDropingPoints = listDropingPoints;
	}

	public Object getCancelPolicyList() {
		return cancelPolicyList;
	}

	public void setCancelPolicyList(Object cancelPolicyList) {
		this.cancelPolicyList = cancelPolicyList;
	}

	public int getMaxcolumn() {
		return maxcolumn;
	}

	public void setMaxcolumn(int maxcolumn) {
		this.maxcolumn = maxcolumn;
	}

	public int getMaxrow() {
		return maxrow;
	}

	public void setMaxrow(int maxrow) {
		this.maxrow = maxrow;
	}
	public double getSeatAcFare() {
		return seatAcFare;
	}

	public void setSeatAcFare(double seatAcFare) {
		this.seatAcFare = seatAcFare;
	}

	public double getSeatNacFare() {
		return seatNacFare;
	}

	public void setSeatNacFare(double seatNacFare) {
		this.seatNacFare = seatNacFare;
	}

	public double getSleepAcFare() {
		return sleepAcFare;
	}

	public void setSleepAcFare(double sleepAcFare) {
		this.sleepAcFare = sleepAcFare;
	}

	public double getSleepNacFare() {
		return sleepNacFare;
	}

	public void setSleepNacFare(double sleepNacFare) {
		this.sleepNacFare = sleepNacFare;
	}

	public double getMinFare() {
		return minFare;
	}

	public void setMinFare(double minFare) {
		this.minFare = minFare;
	}

	public double getMaxFare() {
		return maxFare;
	}

	public void setMaxFare(double maxFare) {
		this.maxFare = maxFare;
	}

	public void setMaxFare(int maxFare) {
		this.maxFare = maxFare;
	}
	
}
