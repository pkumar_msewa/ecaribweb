package com.payqwikweb.app.model.response.reports;

public class WalletLoadUserDetails {
	
	private String mobile;
	private String cName;
	private String cEmailId;
	private double amt;
	private String date;
	private String noOfLoad;
	private String walletCreated;
	
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getcName() {
		return cName;
	}
	public void setcName(String cName) {
		this.cName = cName;
	}
	public String getcEmailId() {
		return cEmailId;
	}
	public void setcEmailId(String cEmailId) {
		this.cEmailId = cEmailId;
	}
	public double getAmt() {
		return amt;
	}
	public void setAmt(double amt) {
		this.amt = amt;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getNoOfLoad() {
		return noOfLoad;
	}
	public void setNoOfLoad(String noOfLoad) {
		this.noOfLoad = noOfLoad;
	}
	public String getWalletCreated() {
		return walletCreated;
	}
	public void setWalletCreated(String walletCreated) {
		this.walletCreated = walletCreated;
	}
	
}
