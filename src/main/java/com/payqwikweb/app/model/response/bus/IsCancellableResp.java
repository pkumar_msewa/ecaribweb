package com.payqwikweb.app.model.response.bus;

public class IsCancellableResp {

	private String status;
	private String code;
	private String message;
	
	private String seatNo;
	private String remarks;
	private double refundAmount;
	private double cancellationCharges;
	private boolean isCancellable;
	
	public String getSeatNo() {
		return seatNo;
	}
	public void setSeatNo(String seatNo) {
		this.seatNo = seatNo;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public double getRefundAmount() {
		return refundAmount;
	}
	public void setRefundAmount(double refundAmount) {
		this.refundAmount = refundAmount;
	}
	public double getCancellationCharges() {
		return cancellationCharges;
	}
	public void setCancellationCharges(double cancellationCharges) {
		this.cancellationCharges = cancellationCharges;
	}
	public boolean isCancellable() {
		return isCancellable;
	}
	public void setCancellable(boolean isCancellable) {
		this.isCancellable = isCancellable;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

}
