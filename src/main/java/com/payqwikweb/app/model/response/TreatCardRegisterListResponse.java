package com.payqwikweb.app.model.response;

import org.json.JSONArray;

public class TreatCardRegisterListResponse {

	private boolean success;
	private String code;
	private String message;
	private String status;
	private String details;
	private JSONArray jsonArray;
	private long membershipCode;
	private String memberShipCodeExpire;
	private String username;
	private String response;
	private String emailId;
	private String firstName;
	private long count;
	private String created;
	
	public String getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public JSONArray getJsonArray() {
		return jsonArray;
	}

	public void setJsonArray(JSONArray jsonArray) {
		this.jsonArray = jsonArray;
	}

	public long getMembershipCode() {
		return membershipCode;
	}

	public void setMembershipCode(long membershipCode) {
		this.membershipCode = membershipCode;
	}

	public String getMemberShipCodeExpire() {
		return memberShipCodeExpire;
	}

	public void setMemberShipCodeExpire(String memberShipCodeExpire) {
		this.memberShipCodeExpire = memberShipCodeExpire;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

}
