package com.payqwikweb.app.model.response;

import com.payqwikweb.app.model.ResponseStatus;

public class MeraEventTicketCalculationResponse {

	private boolean success;
	private String code;
	private String message;
	private ResponseStatus status;
	private String details;
	private String response;
	private Object jsonArray;
	private String eventId;
	private double totalTicketAmount;
	private int totalTicketQuantity;
	private double totalCodeDiscount;
	private double totalBulkDiscount;
	private double totalTaxAmount;
	private double totalReferralDiscount;
	private double totalReferrerDiscount;
	private int roundofvalue;
	private double totalPurchaseAmount;
	private String currencyCode;
	private String discountCode;
	private String referralCode;
	private String pcode;
	private String promoterCode;
	private double referralDiscount;
	private double referrerDiscount;
	private String ticketId;
	private String ticketName;
	private String ticketType;
	private double ticketPrice;
	private int selectedQuantity;
	private double totalAmount;

	public String getEventId() {
		return eventId;
	}

	public void setEventId(String eventId) {
		this.eventId = eventId;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public ResponseStatus getStatus() {
		return status;
	}

	public void setStatus(ResponseStatus status) {
		this.status = status;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public Object getJsonArray() {
		return jsonArray;
	}

	public void setJsonArray(Object jsonArray) {
		this.jsonArray = jsonArray;
	}

	public double getTotalTicketAmount() {
		return totalTicketAmount;
	}

	public void setTotalTicketAmount(double totalTicketAmount) {
		this.totalTicketAmount = totalTicketAmount;
	}

	public int getTotalTicketQuantity() {
		return totalTicketQuantity;
	}

	public void setTotalTicketQuantity(int totalTicketQuantity) {
		this.totalTicketQuantity = totalTicketQuantity;
	}

	public double getTotalCodeDiscount() {
		return totalCodeDiscount;
	}

	public void setTotalCodeDiscount(double totalCodeDiscount) {
		this.totalCodeDiscount = totalCodeDiscount;
	}

	public double getTotalBulkDiscount() {
		return totalBulkDiscount;
	}

	public void setTotalBulkDiscount(double totalBulkDiscount) {
		this.totalBulkDiscount = totalBulkDiscount;
	}

	public double getTotalTaxAmount() {
		return totalTaxAmount;
	}

	public void setTotalTaxAmount(double totalTaxAmount) {
		this.totalTaxAmount = totalTaxAmount;
	}

	public double getTotalReferralDiscount() {
		return totalReferralDiscount;
	}

	public void setTotalReferralDiscount(double totalReferralDiscount) {
		this.totalReferralDiscount = totalReferralDiscount;
	}

	public double getTotalReferrerDiscount() {
		return totalReferrerDiscount;
	}

	public void setTotalReferrerDiscount(double totalReferrerDiscount) {
		this.totalReferrerDiscount = totalReferrerDiscount;
	}

	public int getRoundofvalue() {
		return roundofvalue;
	}

	public void setRoundofvalue(int roundofvalue) {
		this.roundofvalue = roundofvalue;
	}

	public double getTotalPurchaseAmount() {
		return totalPurchaseAmount;
	}

	public void setTotalPurchaseAmount(double totalPurchaseAmount) {
		this.totalPurchaseAmount = totalPurchaseAmount;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getDiscountCode() {
		return discountCode;
	}

	public void setDiscountCode(String discountCode) {
		this.discountCode = discountCode;
	}

	public String getReferralCode() {
		return referralCode;
	}

	public void setReferralCode(String referralCode) {
		this.referralCode = referralCode;
	}

	public String getPcode() {
		return pcode;
	}

	public void setPcode(String pcode) {
		this.pcode = pcode;
	}

	public String getPromoterCode() {
		return promoterCode;
	}

	public void setPromoterCode(String promoterCode) {
		this.promoterCode = promoterCode;
	}

	public double getReferralDiscount() {
		return referralDiscount;
	}

	public void setReferralDiscount(double referralDiscount) {
		this.referralDiscount = referralDiscount;
	}

	public double getReferrerDiscount() {
		return referrerDiscount;
	}

	public void setReferrerDiscount(double referrerDiscount) {
		this.referrerDiscount = referrerDiscount;
	}

	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

	public String getTicketName() {
		return ticketName;
	}

	public void setTicketName(String ticketName) {
		this.ticketName = ticketName;
	}

	public String getTicketType() {
		return ticketType;
	}

	public void setTicketType(String ticketType) {
		this.ticketType = ticketType;
	}

	public double getTicketPrice() {
		return ticketPrice;
	}

	public void setTicketPrice(double ticketPrice) {
		this.ticketPrice = ticketPrice;
	}

	public int getSelectedQuantity() {
		return selectedQuantity;
	}

	public void setSelectedQuantity(int selectedQuantity) {
		this.selectedQuantity = selectedQuantity;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

}
