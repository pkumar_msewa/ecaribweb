package com.payqwikweb.app.model.response;

import java.util.List;

public class MobileSearchResponse {

    private boolean success;
    private String message;
    private List<MobileSearchDTO> results;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<MobileSearchDTO> getSearchList() {
        return results;
    }

    public void setSearchList(List<MobileSearchDTO> results) {
        this.results= results;
    }
}
