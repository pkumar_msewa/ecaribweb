package com.payqwikweb.app.model.response;

public class AjaxResponse {
	private double totalLoadMoneyEBS;
	private double totalLoadMoneyVNet;
	private double pool;
	private double totalPayable;
	private double merchantPayable;
	private double totalCommission;
	private double bankAmount;
	private long totalTrans;
	private long totalUser;
	private double totalBankTransferCommission;
	private double promoBalance;
	private double iplBalance;
	private double mvisaTransaction;
	private double travelTransaction;
	private double totalLoadMoneyUPI;
	
	

	public double getIplBalance() {
		return iplBalance;
	}

	public void setIplBalance(double iplBalance) {
		this.iplBalance = iplBalance;
	}

	public double getMvisaTransaction() {
		return mvisaTransaction;
	}

	public void setMvisaTransaction(double mvisaTransaction) {
		this.mvisaTransaction = mvisaTransaction;
	}

	public double getTravelTransaction() {
		return travelTransaction;
	}

	public void setTravelTransaction(double travelTransaction) {
		this.travelTransaction = travelTransaction;
	}

	public double getPromoBalance() {
		return promoBalance;
	}

	public void setPromoBalance(double promoBalance) {
		this.promoBalance = promoBalance;
	}

	public double getTotalBankTransferCommission() {
		return totalBankTransferCommission;
	}

	public void setTotalBankTransferCommission(double totalBankTransferCommission) {
		this.totalBankTransferCommission = totalBankTransferCommission;
	}

	public double getTotalLoadMoneyEBS() {
		return totalLoadMoneyEBS;
	}

	public void setTotalLoadMoneyEBS(double totalLoadMoneyEBS) {
		this.totalLoadMoneyEBS = totalLoadMoneyEBS;
	}

	public double getTotalLoadMoneyVNet() {
		return totalLoadMoneyVNet;
	}

	public void setTotalLoadMoneyVNet(double totalLoadMoneyVNet) {
		this.totalLoadMoneyVNet = totalLoadMoneyVNet;
	}

	public double getPool() {
		return pool;
	}

	public void setPool(double pool) {
		this.pool = pool;
	}

	public double getTotalPayable() {
		return totalPayable;
	}

	public void setTotalPayable(double totalPayable) {
		this.totalPayable = totalPayable;
	}

	public double getMerchantPayable() {
		return merchantPayable;
	}

	public void setMerchantPayable(double merchantPayable) {
		this.merchantPayable = merchantPayable;
	}

	public double getTotalCommission() {
		return totalCommission;
	}

	public void setTotalCommission(double totalCommission) {
		this.totalCommission = totalCommission;
	}

	public double getBankAmount() {
		return bankAmount;
	}

	public void setBankAmount(double bankAmount) {
		this.bankAmount = bankAmount;
	}

	public long getTotalTrans() {
		return totalTrans;
	}

	public void setTotalTrans(long totalTrans) {
		this.totalTrans = totalTrans;
	}

	public long getTotalUser() {
		return totalUser;
	}

	public void setTotalUser(long totalUser) {
		this.totalUser = totalUser;
	}

	public double getTotalLoadMoneyUPI() {
		return totalLoadMoneyUPI;
	}

	public void setTotalLoadMoneyUPI(double totalLoadMoneyUPI) {
		this.totalLoadMoneyUPI = totalLoadMoneyUPI;
	}

	@Override
	public String toString() {
		return "AjaxResponse [totalLoadMoneyEBS=" + totalLoadMoneyEBS + ", totalLoadMoneyVNet=" + totalLoadMoneyVNet
				+ ", pool=" + pool + ", totalPayable=" + totalPayable + ", merchantPayable=" + merchantPayable
				+ ", totalCommission=" + totalCommission + ", bankAmount=" + bankAmount + ", totalTrans=" + totalTrans
				+ ", totalUser=" + totalUser + ", totalBankTransferCommission=" + totalBankTransferCommission
				+ ", promoBalance=" + promoBalance + ", mvisaTransaction=" + mvisaTransaction + ", travelTransaction="
				+ travelTransaction + ", totalLoadMoneyUPI=" + totalLoadMoneyUPI + "]";
	}
}
