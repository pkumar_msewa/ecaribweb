package com.payqwikweb.app.model.response.bus;


public class CancelTicketResp {

	private String status;
	private String code;
	private String message;
	
	private double refundAmount;
	private double cancellationCharges;
	private boolean cancelStatus;
	private boolean isCancelRequested;
	private boolean isRefunded;
	private String pnrNo;
	private String operatorPnr;
	/*private List<String> cancelSeatNo;*/
	private String cancelSeatNo;
	private String remarks;
	
	public double getRefundAmount() {
		return refundAmount;
	}
	public void setRefundAmount(double refundAmount) {
		this.refundAmount = refundAmount;
	}
	public double getCancellationCharges() {
		return cancellationCharges;
	}
	public void setCancellationCharges(double cancellationCharges) {
		this.cancellationCharges = cancellationCharges;
	}
	public boolean isCancelStatus() {
		return cancelStatus;
	}
	public void setCancelStatus(boolean cancelStatus) {
		this.cancelStatus = cancelStatus;
	}
	public boolean isCancelRequested() {
		return isCancelRequested;
	}
	public void setCancelRequested(boolean isCancelRequested) {
		this.isCancelRequested = isCancelRequested;
	}
	public boolean isRefunded() {
		return isRefunded;
	}
	public void setRefunded(boolean isRefunded) {
		this.isRefunded = isRefunded;
	}
	public String getPnrNo() {
		return pnrNo;
	}
	public void setPnrNo(String pnrNo) {
		this.pnrNo = pnrNo;
	}
	public String getOperatorPnr() {
		return operatorPnr;
	}
	public void setOperatorPnr(String operatorPnr) {
		this.operatorPnr = operatorPnr;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getCancelSeatNo() {
		return cancelSeatNo;
	}
	public void setCancelSeatNo(String cancelSeatNo) {
		this.cancelSeatNo = cancelSeatNo;
	}

	/*public List<String> getCancelSeatNo() {
		return cancelSeatNo;
	}
	public void setCancelSeatNo(List<String> cancelSeatNo) {
		this.cancelSeatNo = cancelSeatNo;
	}*/
}
