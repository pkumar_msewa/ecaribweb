package com.payqwikweb.app.model.response;

public class VisaSignUpResponse {

	private String code;
	private String mVisaId;
	private String entityId;
	private String rupayId;
	private boolean flag;
	private String customerId;
	private String message;
	
	public boolean isFlag() {
		return flag;
	}
	public void setFlag(boolean flag) {
		this.flag = flag;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getmVisaId() {
		return mVisaId;
	}
	public void setmVisaId(String mVisaId) {
		this.mVisaId = mVisaId;
	}
	public String getEntityId() {
		return entityId;
	}
	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getRupayId() {
		return rupayId;
	}
	public void setRupayId(String rupayId) {
		this.rupayId = rupayId;
	}
	
}
