package com.payqwikweb.app.model.response;

import com.payqwikweb.app.model.request.AdminRequest;

public class AllTransactionResponse extends AdminRequest{

	private boolean success;
	private String code;
	private String message;
	private String status;
	private String details;
	private String response;
	private long aRes;
	private long bRes;
	private long cRes;
	private long dRes;
	private long eRes;

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public long getaRes() {
		return aRes;
	}

	public void setaRes(long aRes) {
		this.aRes = aRes;
	}

	public long getbRes() {
		return bRes;
	}

	public void setbRes(long bRes) {
		this.bRes = bRes;
	}

	public long getcRes() {
		return cRes;
	}

	public void setcRes(long cRes) {
		this.cRes = cRes;
	}

	public long getdRes() {
		return dRes;
	}

	public void setdRes(long dRes) {
		this.dRes = dRes;
	}

	public long geteRes() {
		return eRes;
	}

	public void seteRes(long eRes) {
		this.eRes = eRes;
	}


}
