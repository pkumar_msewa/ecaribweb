package com.payqwikweb.app.model.response;

import org.json.JSONArray;

public class MeraEventCategoryListResponse extends MeraEventsResponse{

	private int categoryID;
	private String categoryName;
	private String themeColor;
	private String ticketSetting;
	private String defaultBannerPath;
	private String defaultThumbnailPath;
	private JSONArray jsonArray;
	private String value;
	private String iconPath;
	
	
	public JSONArray getJsonArray() {
		return jsonArray;
	}
	public void setJsonArray(JSONArray jsonArray) {
		this.jsonArray = jsonArray;
	}
	public int getCategoryID() {
		return categoryID;
	}
	public void setCategoryID(int categoryID) {
		this.categoryID = categoryID;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getThemeColor() {
		return themeColor;
	}
	public void setThemeColor(String themeColor) {
		this.themeColor = themeColor;
	}
	public String getTicketSetting() {
		return ticketSetting;
	}
	public void setTicketSetting(String ticketSetting) {
		this.ticketSetting = ticketSetting;
	}
	public String getDefaultBannerPath() {
		return defaultBannerPath;
	}
	public void setDefaultBannerPath(String defaultBannerPath) {
		this.defaultBannerPath = defaultBannerPath;
	}
	public String getDefaultThumbnailPath() {
		return defaultThumbnailPath;
	}
	public void setDefaultThumbnailPath(String defaultThumbnailPath) {
		this.defaultThumbnailPath = defaultThumbnailPath;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getIconPath() {
		return iconPath;
	}
	public void setIconPath(String iconPath) {
		this.iconPath = iconPath;
	}
	
}
