package com.payqwikweb.app.model.response;

public class ServiceProviderResponse {
	
	private String code;
	private boolean success;
	private String message;
	private Object detail;
	
	 public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Object getDetail() {
	  return detail;
	 }
	 public void setDetail(Object detail) {
	  this.detail = detail;
	 }

}
