package com.payqwikweb.app.model.response;

import java.util.List;

import com.payqwikweb.app.model.request.AadharDetailsDTO;

public class AadharServiceResponse {

	private boolean success;

	private String message;

	private String code;

	private List<AadharDetailsDTO> aadharList;
	
	public List<AadharDetailsDTO> getAadharList() {
		return aadharList;
	}

	public void setAadharList(List<AadharDetailsDTO> aadharList) {
		this.aadharList = aadharList;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

}
