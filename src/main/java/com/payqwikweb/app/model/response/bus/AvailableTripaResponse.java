package com.payqwikweb.app.model.response.bus;

import java.util.List;

public class AvailableTripaResponse {

	private String status;
	private String code;
	private String message;
	private Object details;
	private List<String> seatNo;


	public String getStatus() {
		return status;
	}
	public Object getDetails() {
		return details;
	}
	public void setDetails(Object details) {
		this.details = details;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<String> getSeatNo() {
		return seatNo;
	}
	public void setSeatNo(List<String> seatNo) {
		this.seatNo = seatNo;
	}


}
