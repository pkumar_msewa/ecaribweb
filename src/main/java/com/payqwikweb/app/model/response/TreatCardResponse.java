package com.payqwikweb.app.model.response;

public class TreatCardResponse {
	private String code;
	private String message;
	private String details;
	private boolean success;
	private String response;
	private String membershipCode;
	private String mebershipCodeExpire;
	
	public String getMembershipCode() {
		return membershipCode;
	}

	public void setMembershipCode(String membershipCode) {
		this.membershipCode = membershipCode;
	}

	public String getMebershipCodeExpire() {
		return mebershipCodeExpire;
	}

	public void setMebershipCodeExpire(String mebershipCodeExpire) {
		this.mebershipCodeExpire = mebershipCodeExpire;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

}
