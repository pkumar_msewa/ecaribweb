package com.payqwikweb.app.model.response.reports;

public class MISDTO {
	
	private long noOfWallet;
	private long totalWallet;
	private double closingBal;
	private double loadAmount;
	private double walletSpentAmt;
	
	public long getNoOfWallet() {
		return noOfWallet;
	}
	public void setNoOfWallet(long noOfWallet) {
		this.noOfWallet = noOfWallet;
	}
	public long getTotalWallet() {
		return totalWallet;
	}
	public void setTotalWallet(long totalWallet) {
		this.totalWallet = totalWallet;
	}
	public double getClosingBal() {
		return closingBal;
	}
	public void setClosingBal(double closingBal) {
		this.closingBal = closingBal;
	}
	public double getLoadAmount() {
		return loadAmount;
	}
	public void setLoadAmount(double loadAmount) {
		this.loadAmount = loadAmount;
	}
	public double getWalletSpentAmt() {
		return walletSpentAmt;
	}
	public void setWalletSpentAmt(double walletSpentAmt) {
		this.walletSpentAmt = walletSpentAmt;
	}
	
	
	
}
