package com.payqwikweb.app.model.response;

import java.util.List;

public class VersionListResponse {
	
	  private boolean success;

	    private String message;

	    private List<VersionDTO> versionList;

	    public boolean isSuccess() {
	        return success;
	    }

	    public void setSuccess(boolean success) {
	        this.success = success;
	    }

	    public String getMessage() {
	        return message;
	    }

	    public void setMessage(String message) {
	        this.message = message;
	    }

		public List<VersionDTO> getVersionList() {
			return versionList;
		}

		public void setVersionList(List<VersionDTO> versionList) {
			this.versionList = versionList;
		}



}
