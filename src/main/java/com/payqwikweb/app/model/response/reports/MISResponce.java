package com.payqwikweb.app.model.response.reports;

import java.util.List;

public class MISResponce {
	
	private boolean success;
	private String code;
	private String message;
	private String status;
	private String details;
	private String response;
	private Object additionalInfo;
	private List<Object> loadMoney;
	private List<Object> closing;
	private List<Object> created;
	private List<Object> spent;
	private List<Object> wallet;
	
	
	
	public List<Object> getLoadMoney() {
		return loadMoney;
	}
	public void setLoadMoney(List<Object> loadMoney) {
		this.loadMoney = loadMoney;
	}
	public List<Object> getClosing() {
		return closing;
	}
	public void setClosing(List<Object> closing) {
		this.closing = closing;
	}
	public List<Object> getCreated() {
		return created;
	}
	public void setCreated(List<Object> created) {
		this.created = created;
	}
	public List<Object> getSpent() {
		return spent;
	}
	public void setSpent(List<Object> spent) {
		this.spent = spent;
	}
	public List<Object> getWallet() {
		return wallet;
	}
	public void setWallet(List<Object> wallet) {
		this.wallet = wallet;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	public Object getAdditionalInfo() {
		return additionalInfo;
	}
	public void setAdditionalInfo(Object additionalInfo) {
		this.additionalInfo = additionalInfo;
	}
	
}
