package com.payqwikweb.app.model.response;

import java.util.Date;

import org.json.JSONObject;

import com.payqwikweb.app.model.ResponseStatus;
import com.thirdparty.util.EventStatus;

public class MeraEventsListResponse {

	private boolean success;
	private String code;
	private String message;
	private ResponseStatus status;
	private Object details;
	private String response;
	private Object jsonArray;

	private String eventId;
	private int ownerId;
	private String title;
	private String thumbImage;
	private String bannerImage;
	private String timeZone;
	private String startDate;
	private String endDate;
	private String address1;
	private String address2;
	private String venueName;
	private String cityName;
	private String stateName;
	private String countryName;
	private String eventUrl;
	private String categoryName;
	private String subCategoryName;
	private String defaultThumbImage;
	private String defaultBannerImage;
	private String registrationType;
	private boolean bookmarked;
	private boolean seatingLayout;
	private String latitude;
	private String longitude;
	private String booknowButtonValue;
	private int limitSingleTicketType;
	private int isMobileApiVisible;
	private int isStandardApiVisible;
	private int popularity;
	private int minTicketPrice;
	private int maxTicketPrice;
	private String currencyCode;
	private String eventMode;
	private int total;
	private boolean nextPage;
	private int eventStatus;
	private int pincode;
	private String timeZoneName;
	private String url;
	
	
	
	public String getEventId() {
		return eventId;
	}
	public void setEventId(String eventId) {
		this.eventId = eventId;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getTimeZoneName() {
		return timeZoneName;
	}
	public void setTimeZoneName(String timeZoneName) {
		this.timeZoneName = timeZoneName;
	}
	public int getPincode() {
		return pincode;
	}
	public void setPincode(int pincode) {
		this.pincode = pincode;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public ResponseStatus getStatus() {
		return status;
	}
	public void setStatus(ResponseStatus status) {
		this.status = status;
	}
	public Object getDetails() {
		return details;
	}
	public void setDetails(Object details) {
		this.details = details;
	}
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	public Object getJsonArray() {
		return jsonArray;
	}
	public void setJsonArray(Object jsonArray) {
		this.jsonArray = jsonArray;
	}
	
	public int getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(int ownerId) {
		this.ownerId = ownerId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getThumbImage() {
		return thumbImage;
	}
	public void setThumbImage(String thumbImage) {
		this.thumbImage = thumbImage;
	}
	public String getBannerImage() {
		return bannerImage;
	}
	public void setBannerImage(String bannerImage) {
		this.bannerImage = bannerImage;
	}
	public String getTimeZone() {
		return timeZone;
	}
	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}
	
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getVenueName() {
		return venueName;
	}
	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public String getStateName() {
		return stateName;
	}
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	public String getEventUrl() {
		return eventUrl;
	}
	public void setEventUrl(String eventUrl) {
		this.eventUrl = eventUrl;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getSubCategoryName() {
		return subCategoryName;
	}
	public void setSubCategoryName(String subCategoryName) {
		this.subCategoryName = subCategoryName;
	}
	public String getDefaultThumbImage() {
		return defaultThumbImage;
	}
	public void setDefaultThumbImage(String defaultThumbImage) {
		this.defaultThumbImage = defaultThumbImage;
	}
	public String getDefaultBannerImage() {
		return defaultBannerImage;
	}
	public void setDefaultBannerImage(String defaultBannerImage) {
		this.defaultBannerImage = defaultBannerImage;
	}
	public String getRegistrationType() {
		return registrationType;
	}
	public void setRegistrationType(String registrationType) {
		this.registrationType = registrationType;
	}
	public boolean isBookmarked() {
		return bookmarked;
	}
	public void setBookmarked(boolean bookmarked) {
		this.bookmarked = bookmarked;
	}
	public boolean isSeatingLayout() {
		return seatingLayout;
	}
	public void setSeatingLayout(boolean seatingLayout) {
		this.seatingLayout = seatingLayout;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getBooknowButtonValue() {
		return booknowButtonValue;
	}
	public void setBooknowButtonValue(String booknowButtonValue) {
		this.booknowButtonValue = booknowButtonValue;
	}
	public int getLimitSingleTicketType() {
		return limitSingleTicketType;
	}
	public void setLimitSingleTicketType(int limitSingleTicketType) {
		this.limitSingleTicketType = limitSingleTicketType;
	}
	public int getIsMobileApiVisible() {
		return isMobileApiVisible;
	}
	public void setIsMobileApiVisible(int isMobileApiVisible) {
		this.isMobileApiVisible = isMobileApiVisible;
	}
	public int getIsStandardApiVisible() {
		return isStandardApiVisible;
	}
	public void setIsStandardApiVisible(int isStandardApiVisible) {
		this.isStandardApiVisible = isStandardApiVisible;
	}
	public int getPopularity() {
		return popularity;
	}
	public void setPopularity(int popularity) {
		this.popularity = popularity;
	}
	public double getMinTicketPrice() {
		return minTicketPrice;
	}
	public int getMaxTicketPrice() {
		return maxTicketPrice;
	}
	public void setMaxTicketPrice(int maxTicketPrice) {
		this.maxTicketPrice = maxTicketPrice;
	}
	public void setMinTicketPrice(int minTicketPrice) {
		this.minTicketPrice = minTicketPrice;
	}
	public String getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	public String getEventMode() {
		return eventMode;
	}
	public void setEventMode(String eventMode) {
		this.eventMode = eventMode;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public boolean isNextPage() {
		return nextPage;
	}
	public void setNextPage(boolean nextPage) {
		this.nextPage = nextPage;
	}
	public int getEventStatus() {
		return eventStatus;
	}
	public void setEventStatus(int eventStatus) {
		this.eventStatus = eventStatus;
	}
	
	
}
