package com.payqwikweb.app.model.response;

import java.util.List;

public class GCMResponse {

    private boolean success;

    private String message;

    private long totalPages;

    List<String> gcmList;


    public long getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(long totalPages) {
        this.totalPages = totalPages;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<String> getGcmList() {
        return gcmList;
    }

    public void setGcmList(List<String> gcmList) {
        this.gcmList = gcmList;
    }
}
