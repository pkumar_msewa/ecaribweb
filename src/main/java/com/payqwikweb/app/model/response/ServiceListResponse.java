package com.payqwikweb.app.model.response;

import java.util.List;

public class ServiceListResponse {

    private boolean success;

    private String message;

    private List<ServicesDTO> serviceList;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ServicesDTO> getServiceList() {
        return serviceList;
    }

    public void setServiceList(List<ServicesDTO> serviceList) {
        this.serviceList = serviceList;
    }
}
