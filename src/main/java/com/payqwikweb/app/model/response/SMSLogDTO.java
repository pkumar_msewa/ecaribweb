package com.payqwikweb.app.model.response;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class SMSLogDTO {

    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private String executionTime;

    private String destination;

    private String message;

    private String sender;

    private String template;

    private String response;

    public String getExecutionTime() {
        return executionTime;
    }

    private void setExecutionTime(String executionTime) {
        this.executionTime = executionTime;
    }

    public void setExecutionTime(long timeInMillis){
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(timeInMillis);
            setExecutionTime(sdf.format(calendar.getTime()));
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
