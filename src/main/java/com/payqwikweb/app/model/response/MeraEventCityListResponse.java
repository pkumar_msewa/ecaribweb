package com.payqwikweb.app.model.response;

public class MeraEventCityListResponse extends MeraEventsResponse{

	private int cityID;
	private String cityName;
	private int order;
	private int countryId;
	private int specialCityStateId;
	private int aliasCityId;
	
	public int getCityID() {
		return cityID;
	}
	public void setCityID(int cityID) {
		this.cityID = cityID;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public int getOrder() {
		return order;
	}
	public void setOrder(int order) {
		this.order = order;
	}
	public int getCountryId() {
		return countryId;
	}
	public void setCountryId(int countryId) {
		this.countryId = countryId;
	}
	public int getSpecialCityStateId() {
		return specialCityStateId;
	}
	public void setSpecialCityStateId(int specialCityStateId) {
		this.specialCityStateId = specialCityStateId;
	}
	public int getAliasCityId() {
		return aliasCityId;
	}
	public void setAliasCityId(int aliasCityId) {
		this.aliasCityId = aliasCityId;
	}
	
}
