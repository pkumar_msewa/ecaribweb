package com.payqwikweb.app.model.response;

public class MeraEventsTicketDetailsResponse extends MeraEventsResponse{

	private int ticketId;
	private int eventId;
	private String ticketName;
	private String description;
	private double ticketPrice;
	private int quantity;
	private int minOrderQuantity;
	private int maxOrderQuantity;
	private String startDate;
	private String endDate;
	private int ticketStatus;
	private int totalSoldTickets;
	private String ticketType;
	private int order;
	private int displayStatus;
	private int currencyId;
	private int soldout;
	private String currencyCode;
	private String currencySymbol;
	private int upcomingTicket;
	private int pastTicket;
	private int totalTickets;
	private String i;
	
	
	public String getI() {
		return i;
	}
	public void setI(String i) {
		this.i = i;
	}
	public int getTotalTickets() {
		return totalTickets;
	}
	public void setTotalTickets(int totalTickets) {
		this.totalTickets = totalTickets;
	}
	public int getTicketId() {
		return ticketId;
	}
	public void setTicketId(int ticketId) {
		this.ticketId = ticketId;
	}
	public int getEventId() {
		return eventId;
	}
	public void setEventId(int eventId) {
		this.eventId = eventId;
	}
	public String getTicketName() {
		return ticketName;
	}
	public void setTicketName(String ticketName) {
		this.ticketName = ticketName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public double getTicketPrice() {
		return ticketPrice;
	}
	public void setTicketPrice(double ticketPrice) {
		this.ticketPrice = ticketPrice;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public int getMinOrderQuantity() {
		return minOrderQuantity;
	}
	public void setMinOrderQuantity(int minOrderQuantity) {
		this.minOrderQuantity = minOrderQuantity;
	}
	public int getMaxOrderQuantity() {
		return maxOrderQuantity;
	}
	public void setMaxOrderQuantity(int maxOrderQuantity) {
		this.maxOrderQuantity = maxOrderQuantity;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public int getTicketStatus() {
		return ticketStatus;
	}
	public void setTicketStatus(int ticketStatus) {
		this.ticketStatus = ticketStatus;
	}
	public int getTotalSoldTickets() {
		return totalSoldTickets;
	}
	public void setTotalSoldTickets(int totalSoldTickets) {
		this.totalSoldTickets = totalSoldTickets;
	}
	public String getTicketType() {
		return ticketType;
	}
	public void setTicketType(String ticketType) {
		this.ticketType = ticketType;
	}
	public int getOrder() {
		return order;
	}
	public void setOrder(int order) {
		this.order = order;
	}
	public int getDisplayStatus() {
		return displayStatus;
	}
	public void setDisplayStatus(int displayStatus) {
		this.displayStatus = displayStatus;
	}
	public int getCurrencyId() {
		return currencyId;
	}
	public void setCurrencyId(int currencyId) {
		this.currencyId = currencyId;
	}
	public int getSoldout() {
		return soldout;
	}
	public void setSoldout(int soldout) {
		this.soldout = soldout;
	}
	public String getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	public String getCurrencySymbol() {
		return currencySymbol;
	}
	public void setCurrencySymbol(String currencySymbol) {
		this.currencySymbol = currencySymbol;
	}
	public int getUpcomingTicket() {
		return upcomingTicket;
	}
	public void setUpcomingTicket(int upcomingTicket) {
		this.upcomingTicket = upcomingTicket;
	}
	public int getPastTicket() {
		return pastTicket;
	}
	public void setPastTicket(int pastTicket) {
		this.pastTicket = pastTicket;
	}
	
}
