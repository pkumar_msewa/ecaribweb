package com.payqwikweb.app.model.response;

public class EBSStatusResponseDTO {

	private String transactionRefNo;
	private String status;
	private String description;
	private String message;
	private String code;
	private boolean success;
	private double amount;
	private String errorCode;
	private String errorMessage;
	private String created;
	
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public String getTransactionRefNo() {
		return transactionRefNo;
	}
	public void setTransactionRefNo(String transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	@Override
	public String toString() {
		return "EBSStatusResponseDTO [transactionRefNo=" + transactionRefNo + ", status=" + status + ", description="
				+ description + ", message=" + message + ", code=" + code + ", success=" + success + ", amount="
				+ amount + ", errorCode=" + errorCode + ", errorMessage=" + errorMessage + "]";
	}
	
	
}
