package com.payqwikweb.app.model.response;

import java.util.List;

import com.payqwikweb.app.model.flight.dto.FlightLegresArr;

public class FlightResponse implements Comparable<FlightResponse> {

	private String aircraftType;
	private String arrivalTime;
	private String flightNumber;
	private String amount;
	private String cabin;
	private String destination;
	private String departureTime;
	private String duration;
	private String airlineName;
	private String origin;
	private String itineraryKey;
	private String searchId;
	private String number;
	private String baggageUnit;
	private String baggageWeight;
	private String aircraftTypereturn;
	private String arrivalTimereturn;
	private String flightNumberreturn;
	private String amountreturn;
	private String cabinreturn;
	private String destinationreturn;
	private String departureTimereturn;
	private String durationreturn;
	private String airlineNamereturn;
	private String originreturn;
	private String itineraryKeyreturn;
	private String searchIdreturn;
	private String numberreturn;
	private String baggageUnitreturn;
	private String baggageWeightreturn;
	private String engineID;
	private String engineIDreturn;
	private String stopage;
	private String stopagereturn;
	private String json;
	private String jsonpaxfare;
	private String journeyTime;
	private String journeyTimeReturn;
	private List<FlightLegresArr> flightLegresArr;
	private String flightName;
	private String flightNameReturn;

	public FlightResponse() {
	}

	public FlightResponse(String amount) {
		this.amount = amount;
	}

	public String getJsonpaxfare() {
		return jsonpaxfare;
	}

	public void setJsonpaxfare(String jsonpaxfare) {
		this.jsonpaxfare = jsonpaxfare;
	}

	public String getJson() {
		return json;
	}

	public void setJson(String json) {
		this.json = json;
	}

	public String getStopagereturn() {
		return stopagereturn;
	}

	public void setStopagereturn(String stopagereturn) {
		this.stopagereturn = stopagereturn;
	}

	public String getStopage() {
		return stopage;
	}

	public void setStopage(String stopage) {
		this.stopage = stopage;
	}

	public String getEngineIDreturn() {
		return engineIDreturn;
	}

	public void setEngineIDreturn(String engineIDreturn) {
		this.engineIDreturn = engineIDreturn;
	}

	public String getEngineID() {
		return engineID;
	}

	public void setEngineID(String engineID) {
		this.engineID = engineID;
	}

	public String getAircraftTypereturn() {
		return aircraftTypereturn;
	}

	public void setAircraftTypereturn(String aircraftTypereturn) {
		this.aircraftTypereturn = aircraftTypereturn;
	}

	public String getArrivalTimereturn() {
		return arrivalTimereturn;
	}

	public void setArrivalTimereturn(String arrivalTimereturn) {
		this.arrivalTimereturn = arrivalTimereturn;
	}

	public String getFlightNumberreturn() {
		return flightNumberreturn;
	}

	public void setFlightNumberreturn(String flightNumberreturn) {
		this.flightNumberreturn = flightNumberreturn;
	}

	public String getAmountreturn() {
		return amountreturn;
	}

	public void setAmountreturn(String amountreturn) {
		this.amountreturn = amountreturn;
	}

	public String getCabinreturn() {
		return cabinreturn;
	}

	public void setCabinreturn(String cabinreturn) {
		this.cabinreturn = cabinreturn;
	}

	public String getDestinationreturn() {
		return destinationreturn;
	}

	public void setDestinationreturn(String destinationreturn) {
		this.destinationreturn = destinationreturn;
	}

	public String getDepartureTimereturn() {
		return departureTimereturn;
	}

	public void setDepartureTimereturn(String departureTimereturn) {
		this.departureTimereturn = departureTimereturn;
	}

	public String getDurationreturn() {
		return durationreturn;
	}

	public void setDurationreturn(String durationreturn) {
		this.durationreturn = durationreturn;
	}

	public String getAirlineNamereturn() {
		return airlineNamereturn;
	}

	public void setAirlineNamereturn(String airlineNamereturn) {
		this.airlineNamereturn = airlineNamereturn;
	}

	public String getOriginreturn() {
		return originreturn;
	}

	public void setOriginreturn(String originreturn) {
		this.originreturn = originreturn;
	}

	public String getItineraryKeyreturn() {
		return itineraryKeyreturn;
	}

	public void setItineraryKeyreturn(String itineraryKeyreturn) {
		this.itineraryKeyreturn = itineraryKeyreturn;
	}

	public String getSearchIdreturn() {
		return searchIdreturn;
	}

	public void setSearchIdreturn(String searchIdreturn) {
		this.searchIdreturn = searchIdreturn;
	}

	public String getNumberreturn() {
		return numberreturn;
	}

	public void setNumberreturn(String numberreturn) {
		this.numberreturn = numberreturn;
	}

	public String getBaggageUnitreturn() {
		return baggageUnitreturn;
	}

	public void setBaggageUnitreturn(String baggageUnitreturn) {
		this.baggageUnitreturn = baggageUnitreturn;
	}

	public String getBaggageWeightreturn() {
		return baggageWeightreturn;
	}

	public void setBaggageWeightreturn(String baggageWeightreturn) {
		this.baggageWeightreturn = baggageWeightreturn;
	}

	public String getBaggageUnit() {
		return baggageUnit;
	}

	public void setBaggageUnit(String baggageUnit) {
		this.baggageUnit = baggageUnit;
	}

	public String getBaggageWeight() {
		return baggageWeight;
	}

	public void setBaggageWeight(String baggageWeight) {
		this.baggageWeight = baggageWeight;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getItineraryKey() {
		return itineraryKey;
	}

	public void setItineraryKey(String itineraryKey) {
		this.itineraryKey = itineraryKey;
	}

	public String getSearchId() {
		return searchId;
	}

	public void setSearchId(String searchId) {
		this.searchId = searchId;
	}

	public String getAircraftType() {
		return aircraftType;
	}

	public void setAircraftType(String aircraftType) {
		this.aircraftType = aircraftType;
	}

	public String getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getCabin() {
		return cabin;
	}

	public void setCabin(String cabin) {
		this.cabin = cabin;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getAirlineName() {
		return airlineName;
	}

	public void setAirlineName(String airlineName) {
		this.airlineName = airlineName;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public List<FlightLegresArr> getFlightLegresArr() {
		return flightLegresArr;
	}

	public void setFlightLegresArr(List<FlightLegresArr> flightLegresArr) {
		this.flightLegresArr = flightLegresArr;
	}

	public String getJourneyTime() {
		return journeyTime;
	}

	public void setJourneyTime(String journeyTime) {
		this.journeyTime = journeyTime;
	}

	public String getFlightName() {
		return flightName;
	}

	public void setFlightName(String flightName) {
		this.flightName = flightName;
	}

	public String getJourneyTimeReturn() {
		return journeyTimeReturn;
	}

	public void setJourneyTimeReturn(String journeyTimeReturn) {
		this.journeyTimeReturn = journeyTimeReturn;
	}

	public String getFlightNameReturn() {
		return flightNameReturn;
	}

	public void setFlightNameReturn(String flightNameReturn) {
		this.flightNameReturn = flightNameReturn;
	}

	@Override
	public int compareTo(FlightResponse o) {

		int val = 0;
		if (amount != null && !amount.isEmpty()) {
			val = (int) Double.parseDouble(this.amount) - (int) Double.parseDouble(o.amount);
		} else if (amountreturn != null && !amountreturn.isEmpty()) {
			val = (int) Double.parseDouble(this.amountreturn) - (int) Double.parseDouble(o.amountreturn);
		}
		return val;
	}

}
