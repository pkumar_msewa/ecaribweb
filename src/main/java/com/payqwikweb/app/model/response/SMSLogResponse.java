package com.payqwikweb.app.model.response;

import java.util.List;

public class SMSLogResponse {

    private boolean success;
    private String message;
    private List<SMSLogDTO> logList;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<SMSLogDTO> getLogList() {
        return logList;
    }

    public void setLogList(List<SMSLogDTO> logList) {
        this.logList = logList;
    }
}
