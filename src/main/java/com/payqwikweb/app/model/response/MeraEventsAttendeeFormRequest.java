package com.payqwikweb.app.model.response;

import com.payqwikweb.app.model.request.MeraEventsCommonRequest;

public class MeraEventsAttendeeFormRequest extends MeraEventsCommonRequest {

	private int ticketId;
	private String fields;
     private String eventId;
	private String ticketName;
	// private String fullName;
	// private String emailId;
	// private String mobileNo;
	private String fullName;
	private String emailId;
	private String mobileNo;
	private String FullName1;
	private String EmailId1;
	private String MobileNo1;
	private String FullName2;
	private String EmailId2;
	private String MobileNo2;
	private String FullName3;
	private String EmailId3;
	private String MobileNo3;
	private String FullName4;
	private String EmailId4;
	private String MobileNo4;
	private boolean isEmailEnable;
	private boolean isSmsEnable;
	private String transactionId;
	private String quantity;
	private String j;
	
	
	
	public String getEventId() {
		return eventId;
	}
	public void setEventId(String eventId) {
		this.eventId = eventId;
	}
	public int getTicketId() {
		return ticketId;
	}
	public void setTicketId(int ticketId) {
		this.ticketId = ticketId;
	}
	public String getFields() {
		return fields;
	}
	public void setFields(String fields) {
		this.fields = fields;
	}
	public String getTicketName() {
		return ticketName;
	}
	public void setTicketName(String ticketName) {
		this.ticketName = ticketName;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getFullName1() {
		return FullName1;
	}
	public void setFullName1(String fullName1) {
		FullName1 = fullName1;
	}
	public String getEmailId1() {
		return EmailId1;
	}
	public void setEmailId1(String emailId1) {
		EmailId1 = emailId1;
	}
	public String getMobileNo1() {
		return MobileNo1;
	}
	public void setMobileNo1(String mobileNo1) {
		MobileNo1 = mobileNo1;
	}
	public String getFullName2() {
		return FullName2;
	}
	public void setFullName2(String fullName2) {
		FullName2 = fullName2;
	}
	public String getEmailId2() {
		return EmailId2;
	}
	public void setEmailId2(String emailId2) {
		EmailId2 = emailId2;
	}
	public String getMobileNo2() {
		return MobileNo2;
	}
	public void setMobileNo2(String mobileNo2) {
		MobileNo2 = mobileNo2;
	}
	public String getFullName3() {
		return FullName3;
	}
	public void setFullName3(String fullName3) {
		FullName3 = fullName3;
	}
	public String getEmailId3() {
		return EmailId3;
	}
	public void setEmailId3(String emailId3) {
		EmailId3 = emailId3;
	}
	public String getMobileNo3() {
		return MobileNo3;
	}
	public void setMobileNo3(String mobileNo3) {
		MobileNo3 = mobileNo3;
	}
	public String getFullName4() {
		return FullName4;
	}
	public void setFullName4(String fullName4) {
		FullName4 = fullName4;
	}
	public String getEmailId4() {
		return EmailId4;
	}
	public void setEmailId4(String emailId4) {
		EmailId4 = emailId4;
	}
	public String getMobileNo4() {
		return MobileNo4;
	}
	public void setMobileNo4(String mobileNo4) {
		MobileNo4 = mobileNo4;
	}
	public boolean isEmailEnable() {
		return isEmailEnable;
	}
	public void setEmailEnable(boolean isEmailEnable) {
		this.isEmailEnable = isEmailEnable;
	}
	public boolean isSmsEnable() {
		return isSmsEnable;
	}
	public void setSmsEnable(boolean isSmsEnable) {
		this.isSmsEnable = isSmsEnable;
	}
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public String getJ() {
		return j;
	}
	public void setJ(String j) {
		this.j = j;
	}

	

	// private String orderId;
	// private String access_token;

}
