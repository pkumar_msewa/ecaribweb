package com.payqwikweb.app.model.response;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.gcm.model.NotificationDTO;
import com.payqwikweb.model.admin.TListDTO;
import com.vijaya.model.ReportRequest;

public class TransactionReportResponse {
	private String date;
	private long amount;
	private String description;
	private String status;
	private boolean debit;
	private String transactionRefNo;
	private String service;
	private long account_id;
	private String userName;
	private String email;
	private String firstName;
	private long balance;
	private String code;
	private String message;
	private boolean success;
	private JSONObject jsonObject;
	private JSONArray jsonArray;
	private String mvisaId;
	private String contactNo;
	private String authReferenceNo;
	private String retrivalReferenceNo;
	private List<TListDTO> list;
	private List<NEFTResponse> neftList;
	private List<NotificationDTO> gcmNotification;
	private List<ReportRequest> reportList;
	
	public List<ReportRequest> getReportList() {
		return reportList;
	}

	public void setReportList(List<ReportRequest> reportList) {
		this.reportList = reportList;
	}

	public List<NotificationDTO> getGcmNotification() {
		return gcmNotification;
	}

	public void setGcmNotification(List<NotificationDTO> gcmNotification) {
		this.gcmNotification = gcmNotification;
	}

	public List<NEFTResponse> getNeftList() {
		return neftList;
	}

	public void setNeftList(List<NEFTResponse> neftList) {
		this.neftList = neftList;
	}

	private String serviceName;

    public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	
	public List<TListDTO> getList() {
		return list;
	}

	public void setList(List<TListDTO> list) {
		this.list = list;
	}

	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public String getAuthReferenceNo() {
		return authReferenceNo;
	}

	public void setAuthReferenceNo(String authReferenceNo) {
		this.authReferenceNo = authReferenceNo;
	}

	public String getRetrivalReferenceNo() {
		return retrivalReferenceNo;
	}

	public void setRetrivalReferenceNo(String retrivalReferenceNo) {
		this.retrivalReferenceNo = retrivalReferenceNo;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public long getAmount() {
		return amount;
	}

	public void setAmount(long amount) {
		this.amount = amount;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public boolean isDebit() {
		return debit;
	}

	public void setDebit(boolean debit) {
		this.debit = debit;
	}

	public String getTransactionRefNo() {
		return transactionRefNo;
	}

	public void setTransactionRefNo(String transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public long getAccount_id() {
		return account_id;
	}

	public void setAccount_id(long account_id) {
		this.account_id = account_id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public long getBalance() {
		return balance;
	}

	public void setBalance(long balance) {
		this.balance = balance;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public JSONObject getJsonObject() {
		return jsonObject;
	}

	public void setJsonObject(JSONObject jsonObject) {
		this.jsonObject = jsonObject;
	}

	public JSONArray getJsonArray() {
		return jsonArray;
	}

	public void setJsonArray(JSONArray jsonArray) {
		this.jsonArray = jsonArray;
	}

	@Override
	public String toString() {
		return "TransactionReportResponse [date=" + date + ", amount=" + amount + ", description=" + description
				+ ", status=" + status + ", debit=" + debit + ", transactionRefNo=" + transactionRefNo + ", service="
				+ service + ", account_id=" + account_id + ", userName=" + userName + ", email=" + email
				+ ", firstName=" + firstName + ", balance=" + balance + ", code=" + code + ", message=" + message
				+ ", success=" + success + ", jsonObject=" + jsonObject + ", jsonArray=" + jsonArray + "]";
	}

	public String getMvisaId() {
		return mvisaId;
	}

	public void setMvisaId(String mvisaId) {
		this.mvisaId = mvisaId;
	}

}
