package com.payqwikweb.app.model.response;

import java.util.List;

public class MailLogResponse {

    private boolean success;

    private String message;

    private List<MailLogDTO> logList;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<MailLogDTO> getLogList() {
        return logList;
    }

    public void setLogList(List<MailLogDTO> logList) {
        this.logList = logList;
    }
}
