package com.payqwikweb.app.model;

import java.util.List;

public class Tickets {

	List<Oneway> oneway;
	List<Roundway> roundway;
	
	public List<Oneway> getOneway() {
		return oneway;
	}
	public void setOneway(List<Oneway> oneway) {
		this.oneway = oneway;
	}
	public List<Roundway> getRoundway() {
		return roundway;
	}
	public void setRoundway(List<Roundway> roundway) {
		this.roundway = roundway;
	}
	
}
