package com.payqwikweb.app.model.hotel.dto;

import java.util.List;

public class BookingRooms {

	private int count;
	private List<BookingRoom> room;

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public List<BookingRoom> getRoom() {
		return room;
	}

	public void setRoom(List<BookingRoom> room) {
		this.room = room;
	}

}
