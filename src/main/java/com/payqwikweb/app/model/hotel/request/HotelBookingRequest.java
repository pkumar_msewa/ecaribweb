package com.payqwikweb.app.model.hotel.request;



import com.payqwikweb.app.model.hotel.dto.AddressDetail;
import com.payqwikweb.app.model.hotel.dto.BookingRooms;
import com.payqwikweb.app.model.hotel.dto.GSTDetails;
import com.payqwikweb.app.model.hotel.dto.HotelReservationInfo;
import com.payqwikweb.app.model.request.SessionDTO;

public class HotelBookingRequest extends SessionDTO {

	private AddressDetail addressDetail;
	private String bookingID;
	private String cancellationPolicy;
	private String city;
	private String country;
	private String emtCommonID;
	private String engineID;
	private GSTDetails gstDetails;
	private String hotelChain;
	private HotelReservationInfo hotelReservationInfo;
	private String name;
	private String nights;
	private String tax;
	private String transactionid;
	private String uniqueID;
	private String chargeableRate;
	private String checkInDate;
	private String checkOutDate;
	private String hotelID;
	private String rateCode;
	private String rateKey;
	private String roomTypeCode;
	private String supplierType;
	private String location;
	private BookingRooms rooms;
	private String spKey;

	public String getSpKey() {
		return spKey;
	}

	public void setSpKey(String spKey) {
		this.spKey = spKey;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public AddressDetail getAddressDetail() {
		return addressDetail;
	}

	public void setAddressDetail(AddressDetail addressDetail) {
		this.addressDetail = addressDetail;
	}

	public String getBookingID() {
		return bookingID;
	}

	public void setBookingID(String bookingID) {
		this.bookingID = bookingID;
	}

	public String getCancellationPolicy() {
		return cancellationPolicy;
	}

	public void setCancellationPolicy(String cancellationPolicy) {
		this.cancellationPolicy = cancellationPolicy;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getEmtCommonID() {
		return emtCommonID;
	}

	public void setEmtCommonID(String emtCommonID) {
		this.emtCommonID = emtCommonID;
	}

	public String getEngineID() {
		return engineID;
	}

	public void setEngineID(String engineID) {
		this.engineID = engineID;
	}

	public GSTDetails getGstDetails() {
		return gstDetails;
	}

	public void setGstDetails(GSTDetails gstDetails) {
		this.gstDetails = gstDetails;
	}

	public String getHotelChain() {
		return hotelChain;
	}

	public void setHotelChain(String hotelChain) {
		this.hotelChain = hotelChain;
	}

	public HotelReservationInfo getHotelReservationInfo() {
		return hotelReservationInfo;
	}

	public void setHotelReservationInfo(HotelReservationInfo hotelReservationInfo) {
		this.hotelReservationInfo = hotelReservationInfo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNights() {
		return nights;
	}

	public void setNights(String nights) {
		this.nights = nights;
	}

	public String getTax() {
		return tax;
	}

	public void setTax(String tax) {
		this.tax = tax;
	}

	public String getTransactionid() {
		return transactionid;
	}

	public void setTransactionid(String transactionid) {
		this.transactionid = transactionid;
	}

	public String getUniqueID() {
		return uniqueID;
	}

	public void setUniqueID(String uniqueID) {
		this.uniqueID = uniqueID;
	}

	public String getChargeableRate() {
		return chargeableRate;
	}

	public void setChargeableRate(String chargeableRate) {
		this.chargeableRate = chargeableRate;
	}

	public String getCheckInDate() {
		return checkInDate;
	}

	public void setCheckInDate(String checkInDate) {
		this.checkInDate = checkInDate;
	}

	public String getCheckOutDate() {
		return checkOutDate;
	}

	public void setCheckOutDate(String checkOutDate) {
		this.checkOutDate = checkOutDate;
	}

	public String getHotelID() {
		return hotelID;
	}

	public void setHotelID(String hotelID) {
		this.hotelID = hotelID;
	}

	public String getRateCode() {
		return rateCode;
	}

	public void setRateCode(String rateCode) {
		this.rateCode = rateCode;
	}

	public String getRateKey() {
		return rateKey;
	}

	public void setRateKey(String rateKey) {
		this.rateKey = rateKey;
	}

	public String getRoomTypeCode() {
		return roomTypeCode;
	}

	public void setRoomTypeCode(String roomTypeCode) {
		this.roomTypeCode = roomTypeCode;
	}

	public String getSupplierType() {
		return supplierType;
	}

	public void setSupplierType(String supplierType) {
		this.supplierType = supplierType;
	}

	public BookingRooms getRooms() {
		return rooms;
	}

	public void setRooms(BookingRooms rooms) {
		this.rooms = rooms;
	}

}
