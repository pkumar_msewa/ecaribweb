package com.payqwikweb.app.model.hotel.dto;

import java.util.List;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

public class Child {

	private int numberOfChild;
	private List<String> childAge;

	public int getNumberOfChild() {
		return numberOfChild;
	}

	public void setNumberOfChild(int numberOfChild) {
		this.numberOfChild = numberOfChild;
	}

	public List<String> getChildAge() {
		return childAge;
	}

	public void setChildAge(List<String> childAge) {
		this.childAge = childAge;
	}
	
	public JSONObject getJson() throws JSONException
	{
		JSONObject payload=new JSONObject();
		JSONArray childAgeArr=new JSONArray();
		
		for (int i = 0; i < getChildAge().size(); i++) {
			childAgeArr.put(getChildAge().get(i));
		}
		
		payload.put("numberOfChild", getNumberOfChild());
		payload.put("childAge",childAgeArr);

		return payload;
	}

}
