package com.payqwikweb.app.model.hotel.dto;

import java.util.List;

public class BookingRoom {

	private String rateCode;
	private String rateKey;
	private String roomType;
	private String roomTypeCode;
	private String smokingPreference;
	private List<AdultDetails> adultDetails;
	private List<ChildDetails> childDetails;

	public String getRateCode() {
		return rateCode;
	}

	public void setRateCode(String rateCode) {
		this.rateCode = rateCode;
	}

	public String getRateKey() {
		return rateKey;
	}

	public void setRateKey(String rateKey) {
		this.rateKey = rateKey;
	}

	public String getRoomType() {
		return roomType;
	}

	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}

	public String getRoomTypeCode() {
		return roomTypeCode;
	}

	public void setRoomTypeCode(String roomTypeCode) {
		this.roomTypeCode = roomTypeCode;
	}

	public String getSmokingPreference() {
		return smokingPreference;
	}

	public void setSmokingPreference(String smokingPreference) {
		this.smokingPreference = smokingPreference;
	}

	public List<AdultDetails> getAdultDetails() {
		return adultDetails;
	}

	public void setAdultDetails(List<AdultDetails> adultDetails) {
		this.adultDetails = adultDetails;
	}

	public List<ChildDetails> getChildDetails() {
		return childDetails;
	}

	public void setChildDetails(List<ChildDetails> childDetails) {
		this.childDetails = childDetails;
	}
}
