package com.payqwikweb.app.model.hotel.dto;

import java.util.List;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;


public class Rooms {

	private String count;
	private List<Room> room;

	public String getCount() {
		return count;
	}

	public void setCount(String count) {
		this.count = count;
	}

	public List<Room> getRoom() {
		return room;
	}

	public void setRoom(List<Room> room) {
		this.room = room;
	}
	
	public JSONObject getJson() throws JSONException
	{
		JSONObject payload=new JSONObject();
		JSONArray roomArr=new JSONArray();
	
		payload.put("count", (getCount() == null) ? "" :getCount());
		
		for (int i = 0; i < getRoom().size(); i++) {
			roomArr.put(getRoom().get(i).getJson());
		}
		
		payload.put("room",roomArr);

		return payload;
	}

}
