package com.payqwikweb.app.model.hotel.dto;

public class GSTDetails {

	private String customerAddress;
	private String customerName;
	private String customerState;
	private String gstCity;
	private String gstCompanyAddres;
	private String gstCompanyEmailId;
	private String gstCompanyName;
	private String gstNumber;
	private String gstPhoneISD;
	private String gstPhoneNumber;
	private String gstPinCode;
	private String gstState;

	public String getCustomerAddress() {
		return customerAddress;
	}

	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerState() {
		return customerState;
	}

	public void setCustomerState(String customerState) {
		this.customerState = customerState;
	}

	public String getGstCity() {
		return gstCity;
	}

	public void setGstCity(String gstCity) {
		this.gstCity = gstCity;
	}

	public String getGstCompanyAddres() {
		return gstCompanyAddres;
	}

	public void setGstCompanyAddres(String gstCompanyAddres) {
		this.gstCompanyAddres = gstCompanyAddres;
	}

	public String getGstCompanyEmailId() {
		return gstCompanyEmailId;
	}

	public void setGstCompanyEmailId(String gstCompanyEmailId) {
		this.gstCompanyEmailId = gstCompanyEmailId;
	}

	public String getGstCompanyName() {
		return gstCompanyName;
	}

	public void setGstCompanyName(String gstCompanyName) {
		this.gstCompanyName = gstCompanyName;
	}

	public String getGstNumber() {
		return gstNumber;
	}

	public void setGstNumber(String gstNumber) {
		this.gstNumber = gstNumber;
	}

	public String getGstPhoneISD() {
		return gstPhoneISD;
	}

	public void setGstPhoneISD(String gstPhoneISD) {
		this.gstPhoneISD = gstPhoneISD;
	}

	public String getGstPhoneNumber() {
		return gstPhoneNumber;
	}

	public void setGstPhoneNumber(String gstPhoneNumber) {
		this.gstPhoneNumber = gstPhoneNumber;
	}

	public String getGstPinCode() {
		return gstPinCode;
	}

	public void setGstPinCode(String gstPinCode) {
		this.gstPinCode = gstPinCode;
	}

	public String getGstState() {
		return gstState;
	}

	public void setGstState(String gstState) {
		this.gstState = gstState;
	}
}
