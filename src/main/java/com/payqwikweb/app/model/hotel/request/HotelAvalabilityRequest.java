package com.payqwikweb.app.model.hotel.request;

import java.util.List;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.payqwikweb.app.metadatas.UrlMetadatas;
import com.payqwikweb.app.model.hotel.dto.HARooms;
import com.payqwikweb.app.model.request.SessionDTO;

public class HotelAvalabilityRequest  extends SessionDTO{

	private String bookingCode;
	private String checkInDate;
	private String checkOutDate;
	private String city;
	private String emtCommonID;
	private String engine;
	private String hotelChain;
	private String hotelID;
	private boolean includeDetails;
	private List<String> options;
	private String rateKey;
	private String roomTypeCode;
	private String rateCode;
	private HARooms rooms;
	private String country;
	private String stringToken;

	public String getBookingCode() {
		return bookingCode;
	}

	public void setBookingCode(String bookingCode) {
		this.bookingCode = bookingCode;
	}

	public String getCheckInDate() {
		return checkInDate;
	}

	public void setCheckInDate(String checkInDate) {
		this.checkInDate = checkInDate;
	}

	public String getCheckOutDate() {
		return checkOutDate;
	}

	public void setCheckOutDate(String checkOutDate) {
		this.checkOutDate = checkOutDate;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getEmtCommonID() {
		return emtCommonID;
	}

	public void setEmtCommonID(String emtCommonID) {
		this.emtCommonID = emtCommonID;
	}

	public String getEngine() {
		return engine;
	}

	public void setEngine(String engine) {
		this.engine = engine;
	}

	public String getHotelChain() {
		return hotelChain;
	}

	public void setHotelChain(String hotelChain) {
		this.hotelChain = hotelChain;
	}

	public String getHotelID() {
		return hotelID;
	}

	public void setHotelID(String hotelID) {
		this.hotelID = hotelID;
	}

	public boolean isIncludeDetails() {
		return includeDetails;
	}

	public void setIncludeDetails(boolean includeDetails) {
		this.includeDetails = includeDetails;
	}

	public List<String> getOptions() {
		return options;
	}

	public void setOptions(List<String> options) {
		this.options = options;
	}

	public String getRateKey() {
		return rateKey;
	}

	public void setRateKey(String rateKey) {
		this.rateKey = rateKey;
	}

	public String getRoomTypeCode() {
		return roomTypeCode;
	}

	public void setRoomTypeCode(String roomTypeCode) {
		this.roomTypeCode = roomTypeCode;
	}

	public String getRateCode() {
		return rateCode;
	}

	public void setRateCode(String rateCode) {
		this.rateCode = rateCode;
	}

	public HARooms getRooms() {
		return rooms;
	}

	public void setRooms(HARooms rooms) {
		this.rooms = rooms;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getStringToken() {
		return stringToken;
	}

	public void setStringToken(String stringToken) {
		this.stringToken = stringToken;
	}

	public JSONObject getJson() throws JSONException
	{
		JSONObject payload=new JSONObject();

		JSONArray optionArr=new JSONArray();
		
		for (int i = 0; i < getOptions().size(); i++) {
			
			optionArr.put(getOptions().get(i));
		}
		
		payload.put("clientIp", "49.204.86.246");
		payload.put("cllientKey", UrlMetadatas.MDEX_CLIENTKEY);
		payload.put("clientToken", UrlMetadatas.MDEX_CLIENTTOKEN);
		payload.put("clientApiName", "VPayQwik");

		payload.put("bookingCode",(getBookingCode()== null) ? "" :getBookingCode());
		payload.put("checkInDate", (getCheckInDate()== null) ? "" :getCheckInDate());
		payload.put("checkOutDate",(getCheckOutDate()== null) ? "" :getCheckOutDate());
		payload.put("city",(getCity()== null) ? "" :getCity());
		payload.put("emtCommonID",(getEmtCommonID()== null) ? "" :getEmtCommonID());
		payload.put("engine",(getEngine()== null) ? "" :getEngine());
		payload.put("hotelChain",(getHotelChain()== null) ? "" :getHotelChain());
		payload.put("hotelID",(getHotelID()== null) ? "" :getHotelID());
		payload.put("includeDetails",isIncludeDetails());
		payload.put("rateKey",(getRateKey()== null) ? "" :getRateKey());
		payload.put("roomTypeCode",(getRoomTypeCode()== null) ? "" :getRoomTypeCode());
		payload.put("rateCode",(getRateCode()== null) ? "" :getRateCode());	
		payload.put("rooms",(getRooms().getJson()== null) ? "" :getRooms().getJson());
		payload.put("country",(getCountry()== null) ? "" :getCountry());
		payload.put("stringToken",(getStringToken()== null) ? "" :getStringToken());
		payload.put("options",optionArr);
		
		System.err.println("PayLoad is:: "+payload);
		return payload;
	}
}
