package com.payqwikweb.app.model.hotel.dto;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;


public class HARoom {

	private int numberOfAdults;

	public int getNumberOfAdults() {
		return numberOfAdults;
	}

	public void setNumberOfAdults(int numberOfAdults) {
		this.numberOfAdults = numberOfAdults;
	}

	public JSONObject getJson() throws JSONException
	{
		JSONObject payload=new JSONObject();
		
		payload.put("numberOfAdults", getNumberOfAdults());
		
		return payload;
	}
	
	
}
