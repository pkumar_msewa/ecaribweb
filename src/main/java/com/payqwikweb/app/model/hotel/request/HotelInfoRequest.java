package com.payqwikweb.app.model.hotel.request;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.payqwikweb.app.metadatas.UrlMetadatas;
import com.payqwikweb.app.model.request.SessionDTO;

public class HotelInfoRequest extends SessionDTO{

	private String hotelID;
	private int engineId;
	private String emtCommonID;

	public String getHotelID() {
		return hotelID;
	}

	public void setHotelID(String hotelID) {
		this.hotelID = hotelID;
	}

	public int getEngineId() {
		return engineId;
	}

	public void setEngineId(int engineId) {
		this.engineId = engineId;
	}

	public String getEmtCommonID() {
		return emtCommonID;
	}

	public void setEmtCommonID(String emtCommonID) {
		this.emtCommonID = emtCommonID;
	}

	public JSONObject getJson() throws JSONException
	{
		JSONObject payload=new JSONObject();
		
		payload.put("clientIp", "49.204.86.246");
		payload.put("cllientKey", UrlMetadatas.MDEX_CLIENTKEY);
		payload.put("clientToken", UrlMetadatas.MDEX_CLIENTTOKEN);
		payload.put("clientApiName", "VPayQwik");
		
		payload.put("engineId", getEngineId());
		payload.put("emtCommonID", (getEmtCommonID()== null) ? "" :getEmtCommonID());
		payload.put("hotelID",(getHotelID()== null) ? "" :getHotelID());
		
		System.err.println("PayLoad is:: "+payload);
		return payload;
	}
}
