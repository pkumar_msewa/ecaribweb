package com.payqwikweb.app.model.hotel.request;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.payqwikweb.app.metadatas.UrlMetadatas;
import com.payqwikweb.app.model.hotel.dto.Rooms;
import com.payqwikweb.app.model.request.SessionDTO;

public class HotellistRequest extends SessionDTO {
	private String city;
	private String cityName;
	private String checkInDate;
	private String checkOutDate;
	private String country;
	private String currency;
	private int nights;
	private String engine;
	private Rooms rooms;

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getCheckInDate() {
		return checkInDate;
	}

	public void setCheckInDate(String checkInDate) {
		this.checkInDate = checkInDate;
	}

	public String getCheckOutDate() {
		return checkOutDate;
	}

	public void setCheckOutDate(String checkOutDate) {
		this.checkOutDate = checkOutDate;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public int getNights() {
		return nights;
	}

	public void setNights(int nights) {
		this.nights = nights;
	}

	public String getEngine() {
		return engine;
	}

	public void setEngine(String engine) {
		this.engine = engine;
	}

	public Rooms getRooms() {
		return rooms;
	}

	public void setRooms(Rooms rooms) {
		this.rooms = rooms;
	}

	

	public JSONObject getJson() throws JSONException
	{
		JSONObject payload=new JSONObject();
		
		payload.put("clientIp", "49.204.86.246");
		payload.put("cllientKey", UrlMetadatas.MDEX_CLIENTKEY);
		payload.put("clientToken", UrlMetadatas.MDEX_CLIENTTOKEN);
		payload.put("clientApiName", "VPayQwik");
		
		payload.put("city", (getCity() == null) ? "" :getCity());
		payload.put("cityName", (getCityName()== null) ? "" :getCityName());
		payload.put("checkInDate",(getCheckInDate()== null) ? "" :getCheckInDate());
		payload.put("checkOutDate",(getCheckOutDate()== null) ? "" :getCheckOutDate());
		payload.put("country",(getCountry()== null) ? "" :getCountry());
		payload.put("rooms",(getRooms().getJson()== null) ? "" :getRooms().getJson());
		payload.put("engine",(getEngine()== null) ? "" :getEngine());
		payload.put("currency",(getCurrency()== null) ? "" :getCurrency());
		payload.put("nights",getNights());
		
		System.err.println("PayLoad is:: "+payload);
		return payload;
	}

}
