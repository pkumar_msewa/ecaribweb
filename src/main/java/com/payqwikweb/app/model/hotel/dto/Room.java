package com.payqwikweb.app.model.hotel.dto;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

public class Room {

	private int numberOfAdults;
	private Child child;

	public int getNumberOfAdults() {
		return numberOfAdults;
	}

	public void setNumberOfAdults(int numberOfAdults) {
		this.numberOfAdults = numberOfAdults;
	}

	public Child getChild() {
		return child;
	}

	public void setChild(Child child) {
		this.child = child;
	}
	
	
	public JSONObject getJson() throws JSONException
	{
		JSONObject payload=new JSONObject();
	
		payload.put("numberOfAdults", getNumberOfAdults());
		payload.put("child", (getChild().getJson()== null) ? "" :getChild().getJson());

		return payload;
	}
}
