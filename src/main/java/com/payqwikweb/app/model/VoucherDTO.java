package com.payqwikweb.app.model;


public class VoucherDTO {

	private String voucherNo;
	private double amount;
	private String expiryDate;
	private String createdDate;
	private String expired;
	private String redeemed;
	
	
	public String getVoucherNo() {
		return voucherNo;
	}
	public void setVoucherNo(String voucherNo) {
		this.voucherNo = voucherNo;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	public String getExpired() {
		return expired;
	}
	public void setExpired(String expired) {
		this.expired = expired;
	}
	public String getRedeemed() {
		return redeemed;
	}
	public void setRedeemed(String redeemed) {
		this.redeemed = redeemed;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
}

