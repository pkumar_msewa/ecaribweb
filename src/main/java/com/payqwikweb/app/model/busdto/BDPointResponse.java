package com.payqwikweb.app.model.busdto;

import java.util.List;


public class BDPointResponse {
	
	private String totalSeats;
	private List<BDPointDTO> bdPoints;
	private List<AvailableTripsDTO> availableTripsDTOs;
	private List<DPPointsDTO> droppingPoints;
	private String minDeptTime;
	private String maxDeptTime;
	private String maxPrice;
	private String journeyDate;
	private String minPrice;
	private long totalTrips;
	private String Source;
	private long sourceId;
	private String destination;
	private long destinationId;
	private List<BusOperatorDTO> busOperator;
	private String sessionId;
	private String creationDate;
	private String sortDate;
	private double sortMaxDepTime;
	private double sortMinDepTime;
	private boolean isBusAvailable;
	private Object bdpointMap;
	private Object dppointMap;
	private Object details;
	private Object boardingFilter; 
	
	public String getTotalSeats() {
		return totalSeats;
	}

	public void setTotalSeats(String totalSeats) {
		this.totalSeats = totalSeats;
	}

	public List<BDPointDTO> getBdPoints() {
		return bdPoints;
	}

	public void setBdPoints(List<BDPointDTO> bdPoints) {
		this.bdPoints = bdPoints;
	}

	public List<AvailableTripsDTO> getAvailableTripsDTOs() {
		return availableTripsDTOs;
	}

	public void setAvailableTripsDTOs(List<AvailableTripsDTO> availableTripsDTOs) {
		this.availableTripsDTOs = availableTripsDTOs;
	}

	public List<DPPointsDTO> getDroppingPoints() {
		return droppingPoints;
	}

	public void setDroppingPoints(List<DPPointsDTO> droppingPoints) {
		this.droppingPoints = droppingPoints;
	}

	public String getMinDeptTime() {
		return minDeptTime;
	}

	public void setMinDeptTime(String minDeptTime) {
		this.minDeptTime = minDeptTime;
	}

	public String getMaxDeptTime() {
		return maxDeptTime;
	}

	public void setMaxDeptTime(String maxDeptTime) {
		this.maxDeptTime = maxDeptTime;
	}
	public String getMaxPrice() {
		return maxPrice;
	}

	public void setMaxPrice(String maxPrice) {
		this.maxPrice = maxPrice;
	}

	public String getMinPrice() {
		return minPrice;
	}

	public void setMinPrice(String minPrice) {
		this.minPrice = minPrice;
	}

	public String getJourneyDate() {
		return journeyDate;
	}

	public void setJourneyDate(String journeyDate) {
		this.journeyDate = journeyDate;
	}

	public long getTotalTrips() {
		return totalTrips;
	}

	public void setTotalTrips(long totalTrips) {
		this.totalTrips = totalTrips;
	}

	public String getSource() {
		return Source;
	}

	public void setSource(String source) {
		Source = source;
	}

	public long getSourceId() {
		return sourceId;
	}

	public void setSourceId(long sourceId) {
		this.sourceId = sourceId;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public long getDestinationId() {
		return destinationId;
	}

	public void setDestinationId(long destinationId) {
		this.destinationId = destinationId;
	}

	public List<BusOperatorDTO> getBusOperator() {
		return busOperator;
	}

	public void setBusOperator(List<BusOperatorDTO> busOperator) {
		this.busOperator = busOperator;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getSortDate() {
		return sortDate;
	}

	public void setSortDate(String sortDate) {
		this.sortDate = sortDate;
	}

	public double getSortMaxDepTime() {
		return sortMaxDepTime;
	}

	public void setSortMaxDepTime(double sortMaxDepTime) {
		this.sortMaxDepTime = sortMaxDepTime;
	}

	public double getSortMinDepTime() {
		return sortMinDepTime;
	}

	public void setSortMinDepTime(double sortMinDepTime) {
		this.sortMinDepTime = sortMinDepTime;
	}

	public boolean isBusAvailable() {
		return isBusAvailable;
	}

	public void setBusAvailable(boolean isBusAvailable) {
		this.isBusAvailable = isBusAvailable;
	}

	public Object getDetails() {
		return details;
	}

	public void setDetails(Object details) {
		this.details = details;
	}

	public Object getBdpointMap() {
		return bdpointMap;
	}

	public void setBdpointMap(Object bdpointMap) {
		this.bdpointMap = bdpointMap;
	}

	public Object getDppointMap() {
		return dppointMap;
	}

	public void setDppointMap(Object dppointMap) {
		this.dppointMap = dppointMap;
	}

	public Object getBoardingFilter() {
		return boardingFilter;
	}

	public void setBoardingFilter(Object boardingFilter) {
		this.boardingFilter = boardingFilter;
	}

}
