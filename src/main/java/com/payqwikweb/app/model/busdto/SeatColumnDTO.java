package com.payqwikweb.app.model.busdto;

public class SeatColumnDTO {
	
	private boolean available;
	private double baseFare;
	private String column;
	private String ladiesSeat;
	private String length;
	private int seatAvail;
	private String name;
	private String row;
	private String id;
	private String seatStyle;
	private String width;
	private String zIndex;
	private double fare;
	private String seatType;
	private String imageUrl;
	private String gender;
	private boolean isSleeper;
	private boolean isAc;
	private boolean upperShow;
	private boolean lowerShow;
	private int columnNo;
	private int rowNo;
	
	
	
	public boolean isAvailable() {
		return available;
	}
	public void setAvailable(boolean available) {
		this.available = available;
	}
	public double getBaseFare() {
		return baseFare;
	}
	public void setBaseFare(double baseFare) {
		this.baseFare = baseFare;
	}
	public String getColumn() {
		return column;
	}
	public void setColumn(String column) {
		this.column = column;
	}
	public String getLadiesSeat() {
		return ladiesSeat;
	}
	public void setLadiesSeat(String ladiesSeat) {
		this.ladiesSeat = ladiesSeat;
	}
	public String getLength() {
		return length;
	}
	public void setLength(String length) {
		this.length = length;
	}
	public int getSeatAvail() {
		return seatAvail;
	}
	public void setSeatAvail(int seatAvail) {
		this.seatAvail = seatAvail;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getRow() {
		return row;
	}
	public void setRow(String row) {
		this.row = row;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSeatStyle() {
		return seatStyle;
	}
	public void setSeatStyle(String seatStyle) {
		this.seatStyle = seatStyle;
	}
	public String getWidth() {
		return width;
	}
	public void setWidth(String width) {
		this.width = width;
	}
	public String getzIndex() {
		return zIndex;
	}
	public void setzIndex(String zIndex) {
		this.zIndex = zIndex;
	}
	public double getFare() {
		return fare;
	}
	public void setFare(double fare) {
		this.fare = fare;
	}
	public String getSeatType() {
		return seatType;
	}
	public void setSeatType(String seatType) {
		this.seatType = seatType;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public boolean isSleeper() {
		return isSleeper;
	}
	public void setSleeper(boolean isSleeper) {
		this.isSleeper = isSleeper;
	}
	public boolean isAc() {
		return isAc;
	}
	public void setAc(boolean isAc) {
		this.isAc = isAc;
	}
	public boolean isUpperShow() {
		return upperShow;
	}
	public void setUpperShow(boolean upperShow) {
		this.upperShow = upperShow;
	}
	public boolean isLowerShow() {
		return lowerShow;
	}
	public void setLowerShow(boolean lowerShow) {
		this.lowerShow = lowerShow;
	}
	public int getColumnNo() {
		return columnNo;
	}
	public void setColumnNo(int columnNo) {
		this.columnNo = columnNo;
	}
	public int getRowNo() {
		return rowNo;
	}
	public void setRowNo(int rowNo) {
		this.rowNo = rowNo;
	}
	
}
