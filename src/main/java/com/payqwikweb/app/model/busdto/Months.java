package com.payqwikweb.app.model.busdto;


public enum Months {

	January("1","January"), February("2","February"), March("3","March"), April (
			"4","April"), May("5","May"), June("6","June"), July("7","July"), August("8","August"),September("9","September"), October("10","October"),November("11","November"),December("12","December");

	private Months() {

	}
	
	private String key;

	private String value;

	private Months(String key,String value) {
		this.key=key;
		this.value = value;
	}
	
	public String getKey(){
		return key;
	}
	public String getValue() {
		return value;
	}

	public static Months getEnumByValue(String value) {
		if (value == null)
			throw new IllegalArgumentException();
		for (Months v : values())
			if (value.equalsIgnoreCase(v.getValue()))
				return v;
		throw new IllegalArgumentException();
	}
	
	public static Months getEnumByKey(String key) {
		if (key == null)
			throw new IllegalArgumentException();
		for (Months v : values())
			if (key.equalsIgnoreCase(v.getKey()))
				return v;
		throw new IllegalArgumentException();
	}

}
