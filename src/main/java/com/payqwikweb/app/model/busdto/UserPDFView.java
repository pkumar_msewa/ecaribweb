package com.payqwikweb.app.model.busdto;

import java.net.URL;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.web.servlet.view.document.AbstractPdfView;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.pdf.CMYKColor;
import com.lowagie.text.Chunk;
import com.lowagie.text.Image;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;

public class UserPDFView extends AbstractPdfView {

	@SuppressWarnings("unchecked")
	@Override
	protected void buildPdfDocument(java.util.Map<String, Object> model, com.lowagie.text.Document document,
			com.lowagie.text.pdf.PdfWriter arg2, HttpServletRequest arg3, HttpServletResponse arg4) throws Exception {

		Font black = FontFactory.getFont(FontFactory.HELVETICA, 16, Font.NORMAL, BaseColor.BLACK);
		Font redFont = FontFactory.getFont(FontFactory.COURIER, 12, Font.BOLD, new CMYKColor(0, 255, 0, 0));
		Font yellowFont = FontFactory.getFont(FontFactory.COURIER, 14, Font.BOLD, new CMYKColor(0, 0, 255, 0));
		Font red = FontFactory.getFont(FontFactory.COURIER, 14, Font.BOLD, BaseColor.RED);
		 
//		Document document = new Document();
		
		  Map<String,Object> userData = (Map<String,Object>) model.get("userDataTicket");
		  
		  if (userData!=null) {
	       Object busTicket=userData.get("busTicket");
		try
		{
//			PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(pdfUrlLive));
			
			document.open();

			 JSONObject jobj  =new JSONObject(busTicket.toString());
			 JSONArray jArr = jobj.getJSONArray("dto");
			String img="https://www.vpayqwik.com/resources/images/vijayalogo.png";
			Image image2 = Image.getInstance(new URL(img));
			//image2.setAbsolutePosition(1f, 1f);
			image2.setAlignment(Image.RIGHT);
			image2.scaleAbsolute(50, 50);

//			BusTicket busTicket= list.get(0).getBusTicketId();

			String origin=jobj.getString("source");
			String destination=jobj.getString("destination");
			String bk=jobj.getString("ticketPnr");
			String bkd=jobj.getString("created");
			String arr[]=bkd.split(" ");
			bkd=arr[0];
			if (bk==null || bk.equalsIgnoreCase("null")) {
				bk="NA";
			} 
					
					
			PdfPTable table = new PdfPTable(2);

			table.setWidthPercentage(100);
			table.addCell(getCell("E-TICKET", PdfPCell.ALIGN_LEFT));
			table.addCell(image2);
			document.add(table);

			PdfPTable table2 = new PdfPTable(2);
			table2.setWidthPercentage(100);
			table2.addCell(getCell("Ticket Pnr: "+bk, PdfPCell.ALIGN_LEFT));
			table2.addCell(getCell("Customer Care: +918025011300", PdfPCell.ALIGN_LEFT));
			document.add(table2);

			document.add(new Paragraph(" "));
			PdfPTable table3= new PdfPTable(1);
			table3.setWidthPercentage(100);
			table3.addCell(getCell("Booked on: "+bkd, PdfPCell.ALIGN_LEFT));
			document.add(table3);
			
			document.add(new Paragraph("----------------------------------------------------------------------------------------------------------------------------------"));
			
			document.add(new Paragraph("Bus Details:"));
			document.add(new Paragraph(" "));
//			document.add(new Paragraph(origin +" to "+destination));

			PdfPTable table4= new PdfPTable(2);
			table4.addCell(getCell(jobj.getString("busOperator"), PdfPCell.ALIGN_LEFT));
			table4.addCell(getCell("Journey Date: "+jobj.getString("journeyDate"), PdfPCell.ALIGN_RIGHT));
			document.add(table4);
			document.add(new Paragraph(" "));
//			document.add( Chunk.NEWLINE );
	
			document.add(new Paragraph("Boarding Point Details: "));
			document.add(new Paragraph(" "));
			
			PdfPTable table5= new PdfPTable(4);

			table5.addCell(getCell("Source", PdfPCell.ALIGN_LEFT));
			table5.addCell(getCell("Destination", PdfPCell.ALIGN_LEFT));
			table5.addCell(getCell("Boarding Point", PdfPCell.ALIGN_LEFT));
			table5.addCell(getCell("Boarding time", PdfPCell.ALIGN_LEFT));
			
			table5.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table5.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table5.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			table5.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
			
			table5.addCell(getCell(origin, PdfPCell.ALIGN_LEFT));
			table5.addCell(getCell(destination,PdfPCell.ALIGN_LEFT));
			table5.addCell(getCell(jobj.getString("boardingAddress"), PdfPCell.ALIGN_LEFT));
			table5.addCell(getCell(jobj.getString("boardTime"), PdfPCell.ALIGN_LEFT));

			document.add(table5);
			document.add(Chunk.NEWLINE);

			document.add(new Paragraph("Traveller Details: "));

//			document.add(Chunk.NEWLINE);

			document.add(new Paragraph(" "));
			
			PdfPTable table6= new PdfPTable(7);

			table6.addCell("SL No");
			table6.addCell("First Name");
			table6.addCell("Last Name");
			table6.addCell("Age");
			table6.addCell("Gender");
			table6.addCell("Seat No");
			table6.addCell("Fare");

			for (int i = 0; i < jArr.length(); i++) {

			   table6.addCell(i+1+"");
			   table6.addCell(jArr.getJSONObject(i).getString("fName"));
			   table6.addCell(jArr.getJSONObject(i).getString("lName"));
			   table6.addCell(jArr.getJSONObject(i).getString("age"));
			   table6.addCell(jArr.getJSONObject(i).getString("gender"));
			   table6.addCell(jArr.getJSONObject(i).getString("seatNo"));
			   table6.addCell(jArr.getJSONObject(i).getString("fare"));

			}

			document.add(table6);
			
			document.add(Chunk.NEWLINE);
			if (jobj.getString("status").equalsIgnoreCase("Booked")) {
				
				document.add(new Paragraph("Ticket Status: "+jobj.getString("status")));
			}else {
				document.add(new Paragraph("Ticket Status: "+"Not Booked"));
			}
			
			document.add(new Paragraph(" "));
			document.add(new Paragraph("Total Fare: "+jobj.getString("totalFare")));
			
			
			document.close();
//			writer.close();

		} catch (Exception e){
			e.printStackTrace();
		}
	}
	}
	
	public static PdfPCell getCell(String text, int alignment) {
		PdfPCell cell = new PdfPCell(new Phrase(text));
		cell.setPadding(0);
		cell.setHorizontalAlignment(alignment);
		cell.setBorder(PdfPCell.NO_BORDER);
//		cell.setBorderColor(r);
		return cell;
	 }
}
