package com.payqwikweb.app.model.busdto;

public class SaveSeatDetailsDTO {

	private String mobileNo;
	private String emailId;
	private String idProofId;
	private String idProofNo;
	private String sessionId;
	private String address;
	private String engineId;
	private String busid;
	private String busType;
	private String boardId;
	private String boardLocation;
	private String boardName;
	private String arrTime;
	private String depTime;
	private String travelName;
	private String source;
	private String destination;
	private String sourceid;
	private String destinationId;
	private String journeyDate;
	private String boardpoint;
	private String boardprime;
	private String boardTime;
	private String boardlandmark;
	private String boardContactNo;
	private String dropId;
	private String dropName;
	private String droplocatoin;
	private String dropprime;
	private String dropTime;
	private String routeId;
	private String seatDetail;
	private String couponCode;
	private double discount;
	private String agentCode;
	
	private double totalFare;
	private String data;
	
	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getIdProofId() {
		return idProofId;
	}

	public void setIdProofId(String idProofId) {
		this.idProofId = idProofId;
	}

	public String getIdProofNo() {
		return idProofNo;
	}

	public void setIdProofNo(String idProofNo) {
		this.idProofNo = idProofNo;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEngineId() {
		return engineId;
	}

	public void setEngineId(String engineId) {
		this.engineId = engineId;
	}

	public String getBusid() {
		return busid;
	}

	public void setBusid(String busid) {
		this.busid = busid;
	}

	public String getBusType() {
		return busType;
	}

	public void setBusType(String busType) {
		this.busType = busType;
	}

	public String getBoardId() {
		return boardId;
	}

	public void setBoardId(String boardId) {
		this.boardId = boardId;
	}

	public String getBoardLocation() {
		return boardLocation;
	}

	public void setBoardLocation(String boardLocation) {
		this.boardLocation = boardLocation;
	}

	public String getBoardName() {
		return boardName;
	}

	public void setBoardName(String boardName) {
		this.boardName = boardName;
	}

	public String getArrTime() {
		return arrTime;
	}

	public void setArrTime(String arrTime) {
		this.arrTime = arrTime;
	}

	public String getDepTime() {
		return depTime;
	}

	public void setDepTime(String depTime) {
		this.depTime = depTime;
	}

	public String getTravelName() {
		return travelName;
	}

	public void setTravelName(String travelName) {
		this.travelName = travelName;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getSourceid() {
		return sourceid;
	}

	public void setSourceid(String sourceid) {
		this.sourceid = sourceid;
	}

	public String getDestinationId() {
		return destinationId;
	}

	public void setDestinationId(String destinationId) {
		this.destinationId = destinationId;
	}

	public String getJourneyDate() {
		return journeyDate;
	}

	public void setJourneyDate(String journeyDate) {
		this.journeyDate = journeyDate;
	}

	public String getBoardpoint() {
		return boardpoint;
	}

	public void setBoardpoint(String boardpoint) {
		this.boardpoint = boardpoint;
	}

	public String getBoardprime() {
		return boardprime;
	}

	public void setBoardprime(String boardprime) {
		this.boardprime = boardprime;
	}

	public String getBoardTime() {
		return boardTime;
	}

	public void setBoardTime(String boardTime) {
		this.boardTime = boardTime;
	}

	public String getBoardlandmark() {
		return boardlandmark;
	}

	public void setBoardlandmark(String boardlandmark) {
		this.boardlandmark = boardlandmark;
	}

	public String getBoardContactNo() {
		return boardContactNo;
	}

	public void setBoardContactNo(String boardContactNo) {
		this.boardContactNo = boardContactNo;
	}

	public String getDropId() {
		return dropId;
	}

	public void setDropId(String dropId) {
		this.dropId = dropId;
	}

	public String getDropName() {
		return dropName;
	}

	public void setDropName(String dropName) {
		this.dropName = dropName;
	}

	public String getDroplocatoin() {
		return droplocatoin;
	}

	public void setDroplocatoin(String droplocatoin) {
		this.droplocatoin = droplocatoin;
	}

	public String getDropprime() {
		return dropprime;
	}

	public void setDropprime(String dropprime) {
		this.dropprime = dropprime;
	}

	public String getDropTime() {
		return dropTime;
	}

	public void setDropTime(String dropTime) {
		this.dropTime = dropTime;
	}

	public String getRouteId() {
		return routeId;
	}

	public void setRouteId(String routeId) {
		this.routeId = routeId;
	}

	public String getSeatDetail() {
		return seatDetail;
	}

	public void setSeatDetail(String seatDetail) {
		this.seatDetail = seatDetail;
	}

	public String getCouponCode() {
		return couponCode;
	}

	public void setCouponCode(String couponCode) {
		this.couponCode = couponCode;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public double getTotalFare() {
		return totalFare;
	}

	public void setTotalFare(double totalFare) {
		this.totalFare = totalFare;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	
}
