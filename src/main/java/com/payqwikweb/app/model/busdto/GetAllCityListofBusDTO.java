package com.payqwikweb.app.model.busdto;

public class GetAllCityListofBusDTO {

	private long cityId;
	private String cityName;
	
	
	public long getCityId() {
		return cityId;
	}
	public void setCityId(long cityId) {
		this.cityId = cityId;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
}
