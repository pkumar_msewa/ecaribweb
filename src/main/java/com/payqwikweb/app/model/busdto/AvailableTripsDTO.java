package com.payqwikweb.app.model.busdto;

import java.util.List;

import com.payqwikweb.app.model.response.bus.CancelPolicyListDTOResp;


public class AvailableTripsDTO implements Comparable<AvailableTripsDTO>{
	
	private boolean ac;
	private String arrivalTime;
	private String availableSeats;
	private String busType;
	private String bdlocation;
	private String busTypeId;
	private String cancellationPolicy;
	private String departureTime;
	private String doj;
	private String duration;
	private List<BDPointDTO> bdPointDTO;
	private List<DPPointsDTO> dpPointsDTO;
	private List<FareDetailDTO> fareDetailDTOs;
	private List<String> fare;
	
	private String travels;
	private String id;
	private String routeId;
	private String price;
	private boolean seater;
	private boolean sleeper;
	private String idProofRequired;
	private String liveTrackingAvailable;
	private boolean nonAC;
	private String operatorid;
	private String partialCancellationAllowed;
	private String tatkalTime;
	private String vehicleType;
	private String zeroCancellationTime;
	private String mTicketEnabled;
	private double sortDepTime;
	private long engineId;
	
	private List<CancelPolicyListDTOResp> cancelPolicyListDTO;
	private boolean isVolvo;
	private boolean isCancellable;
	private String status;
	private String totalSeat;
	private String departureDate;
	private String arrivalDate;
	private double discount;
	private double commission;
	private double markup;
	private double tds;
	private double stf;
	private String bpId;
	private String dpId;
	private boolean bpDpLayout;
	
	public String getBpId() {
		return bpId;
	}
	public void setBpId(String bpId) {
		this.bpId = bpId;
	}
	public String getDpId() {
		return dpId;
	}
	public void setDpId(String dpId) {
		this.dpId = dpId;
	}
	public boolean isBpDpLayout() {
		return bpDpLayout;
	}
	public void setBpDpLayout(boolean bpDpLayout) {
		this.bpDpLayout = bpDpLayout;
	}
	public boolean isAc() {
		return ac;
	}
	public void setAc(boolean ac) {
		this.ac = ac;
	}
	public String getArrivalTime() {
		return arrivalTime;
	}
	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}
	public String getAvailableSeats() {
		return availableSeats;
	}
	public void setAvailableSeats(String availableSeats) {
		this.availableSeats = availableSeats;
	}
	public String getBusType() {
		return busType;
	}
	public void setBusType(String busType) {
		this.busType = busType;
	}
	public String getBdlocation() {
		return bdlocation;
	}
	public void setBdlocation(String bdlocation) {
		this.bdlocation = bdlocation;
	}
	public String getBusTypeId() {
		return busTypeId;
	}
	public void setBusTypeId(String busTypeId) {
		this.busTypeId = busTypeId;
	}
	public String getCancellationPolicy() {
		return cancellationPolicy;
	}
	public void setCancellationPolicy(String cancellationPolicy) {
		this.cancellationPolicy = cancellationPolicy;
	}
	public String getDepartureTime() {
		return departureTime;
	}
	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}
	public String getDoj() {
		return doj;
	}
	public void setDoj(String doj) {
		this.doj = doj;
	}
	public String getDuration() {
		return duration;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}
	public List<BDPointDTO> getBdPointDTO() {
		return bdPointDTO;
	}
	public void setBdPointDTO(List<BDPointDTO> bdPointDTO) {
		this.bdPointDTO = bdPointDTO;
	}
	public List<DPPointsDTO> getDpPointsDTO() {
		return dpPointsDTO;
	}
	public void setDpPointsDTO(List<DPPointsDTO> dpPointsDTO) {
		this.dpPointsDTO = dpPointsDTO;
	}
	public List<FareDetailDTO> getFareDetailDTOs() {
		return fareDetailDTOs;
	}
	public void setFareDetailDTOs(List<FareDetailDTO> fareDetailDTOs) {
		this.fareDetailDTOs = fareDetailDTOs;
	}
	public List<String> getFare() {
		return fare;
	}
	public void setFare(List<String> fare) {
		this.fare = fare;
	}
	public String getTravels() {
		return travels;
	}
	public void setTravels(String travels) {
		this.travels = travels;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getRouteId() {
		return routeId;
	}
	public void setRouteId(String routeId) {
		this.routeId = routeId;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public boolean isSeater() {
		return seater;
	}
	public void setSeater(boolean seater) {
		this.seater = seater;
	}
	public boolean isSleeper() {
		return sleeper;
	}
	public void setSleeper(boolean sleeper) {
		this.sleeper = sleeper;
	}
	public String getIdProofRequired() {
		return idProofRequired;
	}
	public void setIdProofRequired(String idProofRequired) {
		this.idProofRequired = idProofRequired;
	}
	public String getLiveTrackingAvailable() {
		return liveTrackingAvailable;
	}
	public void setLiveTrackingAvailable(String liveTrackingAvailable) {
		this.liveTrackingAvailable = liveTrackingAvailable;
	}
	public boolean isNonAC() {
		return nonAC;
	}
	public void setNonAC(boolean nonAC) {
		this.nonAC = nonAC;
	}
	public String getOperatorid() {
		return operatorid;
	}
	public void setOperatorid(String operatorid) {
		this.operatorid = operatorid;
	}
	public String getPartialCancellationAllowed() {
		return partialCancellationAllowed;
	}
	public void setPartialCancellationAllowed(String partialCancellationAllowed) {
		this.partialCancellationAllowed = partialCancellationAllowed;
	}
	public String getTatkalTime() {
		return tatkalTime;
	}
	public void setTatkalTime(String tatkalTime) {
		this.tatkalTime = tatkalTime;
	}
	public String getVehicleType() {
		return vehicleType;
	}
	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}
	public String getZeroCancellationTime() {
		return zeroCancellationTime;
	}
	public void setZeroCancellationTime(String zeroCancellationTime) {
		this.zeroCancellationTime = zeroCancellationTime;
	}
	public String getmTicketEnabled() {
		return mTicketEnabled;
	}
	public void setmTicketEnabled(String mTicketEnabled) {
		this.mTicketEnabled = mTicketEnabled;
	}
	public double getSortDepTime() {
		return sortDepTime;
	}
	public void setSortDepTime(double sortDepTime) {
		this.sortDepTime = sortDepTime;
	}
	public long getEngineId() {
		return engineId;
	}
	public void setEngineId(long engineId) {
		this.engineId = engineId;
	}
	public List<CancelPolicyListDTOResp> getCancelPolicyListDTO() {
		return cancelPolicyListDTO;
	}
	public void setCancelPolicyListDTO(List<CancelPolicyListDTOResp> cancelPolicyListDTO) {
		this.cancelPolicyListDTO = cancelPolicyListDTO;
	}
	public boolean isVolvo() {
		return isVolvo;
	}
	public void setVolvo(boolean isVolvo) {
		this.isVolvo = isVolvo;
	}
	public boolean isCancellable() {
		return isCancellable;
	}
	public void setCancellable(boolean isCancellable) {
		this.isCancellable = isCancellable;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	public String getTotalSeat() {
		return totalSeat;
	}
	public void setTotalSeat(String totalSeat) {
		this.totalSeat = totalSeat;
	}
	public String getDepartureDate() {
		return departureDate;
	}
	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}
	public String getArrivalDate() {
		return arrivalDate;
	}
	public void setArrivalDate(String arrivalDate) {
		this.arrivalDate = arrivalDate;
	}
	public double getDiscount() {
		return discount;
	}
	public void setDiscount(double discount) {
		this.discount = discount;
	}
	public double getCommission() {
		return commission;
	}
	public void setCommission(double commission) {
		this.commission = commission;
	}
	public double getMarkup() {
		return markup;
	}
	public void setMarkup(double markup) {
		this.markup = markup;
	}
	public double getTds() {
		return tds;
	}
	public void setTds(double tds) {
		this.tds = tds;
	}
	public double getStf() {
		return stf;
	}
	public void setStf(double stf) {
		this.stf = stf;
	}
	
	@Override
	public int hashCode() {
		return Integer.parseInt(id);
	}
	
	@Override
	public boolean equals(Object obj) {
		
		AvailableTripsDTO tripsDTO=(AvailableTripsDTO)obj;
		return this.hashCode()==tripsDTO.hashCode();
	}
	
	
	@Override
	public int compareTo(AvailableTripsDTO o) {
		
		return Integer.parseInt(this.price)-Integer.parseInt(o.price);
	}
	
	
}
