package com.payqwikweb.app.model.busdto;

public class BDPointDTO {
	
	private String bdPoint;
	private String bdLongName;
	private String bdid;
	private String bdlocation;
	private String landmark;
	private String prime;
	private String time;
	private String contactNumber;
	
	
	public String getBdPoint() {
		return bdPoint;
	}
	public void setBdPoint(String bdPoint) {
		this.bdPoint = bdPoint;
	}
	public String getBdLongName() {
		return bdLongName;
	}
	public void setBdLongName(String bdLongName) {
		this.bdLongName = bdLongName;
	}
	public String getBdid() {
		return bdid;
	}
	public void setBdid(String bdid) {
		this.bdid = bdid;
	}
	public String getBdlocation() {
		return bdlocation;
	}
	public void setBdlocation(String bdlocation) {
		this.bdlocation = bdlocation;
	}
	public String getLandmark() {
		return landmark;
	}
	public void setLandmark(String landmark) {
		this.landmark = landmark;
	}
	public String getPrime() {
		return prime;
	}
	public void setPrime(String prime) {
		this.prime = prime;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getContactNumber() {
		return contactNumber;
	}
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

}
