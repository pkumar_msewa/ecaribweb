package com.payqwikweb.app.model.busdto;

public class BusPriceRecheckResp {

	private String seatNo;
	private String bookingFee;
	private String basicFare;
	private	String serviceFee;
	private	String totalFare;
	private	String serviceCharge;
	private	String serviceTax;
	private	String reservationFee;
	private	String concessionFee;
	private	String discount;
	private	String tollFee;
	private	String tollfeeRajasthan;
	private	String otherCharges;
	private	String leviesCharges;
	
	public String getSeatNo() {
		return seatNo;
	}
	public void setSeatNo(String seatNo) {
		this.seatNo = seatNo;
	}
	public String getBookingFee() {
		return bookingFee;
	}
	public void setBookingFee(String bookingFee) {
		this.bookingFee = bookingFee;
	}
	public String getBasicFare() {
		return basicFare;
	}
	public void setBasicFare(String basicFare) {
		this.basicFare = basicFare;
	}
	public String getServiceFee() {
		return serviceFee;
	}
	public void setServiceFee(String serviceFee) {
		this.serviceFee = serviceFee;
	}
	public String getTotalFare() {
		return totalFare;
	}
	public void setTotalFare(String totalFare) {
		this.totalFare = totalFare;
	}
	public String getServiceCharge() {
		return serviceCharge;
	}
	public void setServiceCharge(String serviceCharge) {
		this.serviceCharge = serviceCharge;
	}
	public String getServiceTax() {
		return serviceTax;
	}
	public void setServiceTax(String serviceTax) {
		this.serviceTax = serviceTax;
	}
	public String getReservationFee() {
		return reservationFee;
	}
	public void setReservationFee(String reservationFee) {
		this.reservationFee = reservationFee;
	}
	public String getConcessionFee() {
		return concessionFee;
	}
	public void setConcessionFee(String concessionFee) {
		this.concessionFee = concessionFee;
	}
	public String getDiscount() {
		return discount;
	}
	public void setDiscount(String discount) {
		this.discount = discount;
	}
	public String getTollFee() {
		return tollFee;
	}
	public void setTollFee(String tollFee) {
		this.tollFee = tollFee;
	}
	public String getTollfeeRajasthan() {
		return tollfeeRajasthan;
	}
	public void setTollfeeRajasthan(String tollfeeRajasthan) {
		this.tollfeeRajasthan = tollfeeRajasthan;
	}
	public String getOtherCharges() {
		return otherCharges;
	}
	public void setOtherCharges(String otherCharges) {
		this.otherCharges = otherCharges;
	}
	public String getLeviesCharges() {
		return leviesCharges;
	}
	public void setLeviesCharges(String leviesCharges) {
		this.leviesCharges = leviesCharges;
	}
}
