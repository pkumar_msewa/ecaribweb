package com.payqwikweb.app.model.busdto;

import java.util.List;

import com.payqwikweb.model.app.response.UserDetail;

public class BusTicketDTO {

	private String source;
	private String destination;
	private String journeyDate;
	private String arrTime;
	private double totalFare;
	private String transactionRefNo;
	private String status;
	private String emtTxnId;
	private List<TravellerDetailsDTO> dto;
	private UserDetail userDetail;
	private String operatorPnr;
	private String ticketPnr;
	private String txnStatus;
	private String txnDate;
	private String busOperator;
	private String boardingAddress;
	private String busType;
	private String emtTransactionScreenId;
	private double commsissionAmount;
	private String deptTime;
	private String boardTime;
	private String created;
	
	public double getCommsissionAmount() {
		return commsissionAmount;
	}
	public void setCommsissionAmount(double commsissionAmount) {
		this.commsissionAmount = commsissionAmount;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getJourneyDate() {
		return journeyDate;
	}
	public void setJourneyDate(String journeyDate) {
		this.journeyDate = journeyDate;
	}
	public String getArrTime() {
		return arrTime;
	}
	public void setArrTime(String arrTime) {
		this.arrTime = arrTime;
	}
	public double getTotalFare() {
		return totalFare;
	}
	public void setTotalFare(double totalFare) {
		this.totalFare = totalFare;
	}
	public String getTransactionRefNo() {
		return transactionRefNo;
	}
	public void setTransactionRefNo(String transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getEmtTxnId() {
		return emtTxnId;
	}
	public void setEmtTxnId(String emtTxnId) {
		this.emtTxnId = emtTxnId;
	}
	public List<TravellerDetailsDTO> getDto() {
		return dto;
	}
	public void setDto(List<TravellerDetailsDTO> dto) {
		this.dto = dto;
	}
	public UserDetail getUserDetail() {
		return userDetail;
	}
	public void setUserDetail(UserDetail userDetail) {
		this.userDetail = userDetail;
	}
	public String getOperatorPnr() {
		return operatorPnr;
	}
	public void setOperatorPnr(String operatorPnr) {
		this.operatorPnr = operatorPnr;
	}
	public String getTicketPnr() {
		return ticketPnr;
	}
	public void setTicketPnr(String ticketPnr) {
		this.ticketPnr = ticketPnr;
	}
	public String getTxnStatus() {
		return txnStatus;
	}
	public void setTxnStatus(String txnStatus) {
		this.txnStatus = txnStatus;
	}
	public String getTxnDate() {
		return txnDate;
	}
	public void setTxnDate(String txnDate) {
		this.txnDate = txnDate;
	}
	public String getBusOperator() {
		return busOperator;
	}
	public void setBusOperator(String busOperator) {
		this.busOperator = busOperator;
	}
	public String getBoardingAddress() {
		return boardingAddress;
	}
	public void setBoardingAddress(String boardingAddress) {
		this.boardingAddress = boardingAddress;
	}
	public String getBusType() {
		return busType;
	}
	public void setBusType(String busType) {
		this.busType = busType;
	}
	public String getEmtTransactionScreenId() {
		return emtTransactionScreenId;
	}
	public void setEmtTransactionScreenId(String emtTransactionScreenId) {
		this.emtTransactionScreenId = emtTransactionScreenId;
	}
	public String getDeptTime() {
		return deptTime;
	}
	public void setDeptTime(String deptTime) {
		this.deptTime = deptTime;
	}
	public String getBoardTime() {
		return boardTime;
	}
	public void setBoardTime(String boardTime) {
		this.boardTime = boardTime;
	}
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	
}
