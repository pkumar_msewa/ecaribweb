package com.payqwikweb.app.model.busdto;

public class FareDetailDTO {
	
	private String baseFare;
	private String markupFareAbsolute;
	private String markupFarePercentage;
	private String operatorServiceChargeAbsolute;
	private String operatorServiceChargePercentage;
	private String serviceTaxAbsolute;
	private String serviceTaxPercentage;
	private String totalFare;
	
	public String getBaseFare() {
		return baseFare;
	}
	public void setBaseFare(String baseFare) {
		this.baseFare = baseFare;
	}
	public String getMarkupFareAbsolute() {
		return markupFareAbsolute;
	}
	public void setMarkupFareAbsolute(String markupFareAbsolute) {
		this.markupFareAbsolute = markupFareAbsolute;
	}
	public String getMarkupFarePercentage() {
		return markupFarePercentage;
	}
	public void setMarkupFarePercentage(String markupFarePercentage) {
		this.markupFarePercentage = markupFarePercentage;
	}
	public String getOperatorServiceChargeAbsolute() {
		return operatorServiceChargeAbsolute;
	}
	public void setOperatorServiceChargeAbsolute(String operatorServiceChargeAbsolute) {
		this.operatorServiceChargeAbsolute = operatorServiceChargeAbsolute;
	}
	public String getOperatorServiceChargePercentage() {
		return operatorServiceChargePercentage;
	}
	public void setOperatorServiceChargePercentage(String operatorServiceChargePercentage) {
		this.operatorServiceChargePercentage = operatorServiceChargePercentage;
	}
	public String getServiceTaxAbsolute() {
		return serviceTaxAbsolute;
	}
	public void setServiceTaxAbsolute(String serviceTaxAbsolute) {
		this.serviceTaxAbsolute = serviceTaxAbsolute;
	}
	public String getServiceTaxPercentage() {
		return serviceTaxPercentage;
	}
	public void setServiceTaxPercentage(String serviceTaxPercentage) {
		this.serviceTaxPercentage = serviceTaxPercentage;
	}
	public String getTotalFare() {
		return totalFare;
	}
	public void setTotalFare(String totalFare) {
		this.totalFare = totalFare;
	}
	
}
