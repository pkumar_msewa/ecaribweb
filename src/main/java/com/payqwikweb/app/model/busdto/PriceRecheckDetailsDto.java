package com.payqwikweb.app.model.busdto;

public class PriceRecheckDetailsDto{

	private String seatHoldId;

	public String getSeatHoldId() {
		return seatHoldId;
	}
	public void setSeatHoldId(String seatHoldId) {
		this.seatHoldId = seatHoldId;
	}
}