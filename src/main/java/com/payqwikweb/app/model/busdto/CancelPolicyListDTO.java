package com.payqwikweb.app.model.busdto;


public class CancelPolicyListDTO 
{
	
	private double timeFrom; 
	private double timeTo;
	private double percentageCharge;
	private double flatCharge;
	private boolean isFlat;
	private String seatno;

	public double getTimeFrom() {
		return timeFrom;
	}
	public void setTimeFrom(double timeFrom) {
		this.timeFrom = timeFrom;
	}
	public double getTimeTo() {
		return timeTo;
	}
	public void setTimeTo(double timeTo) {
		this.timeTo = timeTo;
	}
	public double getPercentageCharge() {
		return percentageCharge;
	}
	public void setPercentageCharge(double percentageCharge) {
		this.percentageCharge = percentageCharge;
	}
	public double getFlatCharge() {
		return flatCharge;
	}
	public void setFlatCharge(double flatCharge) {
		this.flatCharge = flatCharge;
	}
	public boolean isFlat() {
		return isFlat;
	}
	public void setFlat(boolean isFlat) {
		this.isFlat = isFlat;
	}
	public String getSeatno() {
		return seatno;
	}
	public void setSeatno(String seatno) {
		this.seatno = seatno;
	}
}
