package com.payqwikweb.app.model.busdto;

import java.util.List;

public class LowerDTO {

	private List<SeatColumnDTO> firstColumn;
	private List<SeatColumnDTO> secondColumn;
	private List<SeatColumnDTO> thirdColumn;
	private List<SeatColumnDTO> fourthColumn;
	private List<SeatColumnDTO> fifthColumn;
	private List<SeatColumnDTO> sixthColumn;
	private List<SeatColumnDTO> seventhColumn;
	private List<SeatColumnDTO> eightColumn;
	private List<SeatColumnDTO> ninethColumn;
	
	public List<SeatColumnDTO> getFirstColumn() {
		return firstColumn;
	}

	public void setFirstColumn(List<SeatColumnDTO> firstColumn) {
		this.firstColumn = firstColumn;
	}

	public List<SeatColumnDTO> getSecondColumn() {
		return secondColumn;
	}

	public void setSecondColumn(List<SeatColumnDTO> secondColumn) {
		this.secondColumn = secondColumn;
	}

	public List<SeatColumnDTO> getThirdColumn() {
		return thirdColumn;
	}

	public void setThirdColumn(List<SeatColumnDTO> thirdColumn) {
		this.thirdColumn = thirdColumn;
	}

	public List<SeatColumnDTO> getFourthColumn() {
		return fourthColumn;
	}

	public void setFourthColumn(List<SeatColumnDTO> fourthColumn) {
		this.fourthColumn = fourthColumn;
	}

	public List<SeatColumnDTO> getFifthColumn() {
		return fifthColumn;
	}

	public void setFifthColumn(List<SeatColumnDTO> fifthColumn) {
		this.fifthColumn = fifthColumn;
	}

	public List<SeatColumnDTO> getSixthColumn() {
		return sixthColumn;
	}

	public void setSixthColumn(List<SeatColumnDTO> sixthColumn) {
		this.sixthColumn = sixthColumn;
	}

	public List<SeatColumnDTO> getSeventhColumn() {
		return seventhColumn;
	}

	public void setSeventhColumn(List<SeatColumnDTO> seventhColumn) {
		this.seventhColumn = seventhColumn;
	}

	public List<SeatColumnDTO> getEightColumn() {
		return eightColumn;
	}

	public void setEightColumn(List<SeatColumnDTO> eightColumn) {
		this.eightColumn = eightColumn;
	}

	public List<SeatColumnDTO> getNinethColumn() {
		return ninethColumn;
	}

	public void setNinethColumn(List<SeatColumnDTO> ninethColumn) {
		this.ninethColumn = ninethColumn;
	}
	
	
}
