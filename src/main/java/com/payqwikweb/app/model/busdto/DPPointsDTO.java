package com.payqwikweb.app.model.busdto;

public class DPPointsDTO {

	private String dpId;
	private String dpName;
	private String locatoin;
	private String prime;
	private String dpTime;
	private String contactNumber;
	
	
	public String getDpId() {
		return dpId;
	}
	public void setDpId(String dpId) {
		this.dpId = dpId;
	}
	public String getDpName() {
		return dpName;
	}
	public void setDpName(String dpName) {
		this.dpName = dpName;
	}
	public String getLocatoin() {
		return locatoin;
	}
	public void setLocatoin(String locatoin) {
		this.locatoin = locatoin;
	}
	public String getPrime() {
		return prime;
	}
	public void setPrime(String prime) {
		this.prime = prime;
	}
	public String getDpTime() {
		return dpTime;
	}
	public void setDpTime(String dpTime) {
		this.dpTime = dpTime;
	}
	public String getContactNumber() {
		return contactNumber;
	}
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}
}
