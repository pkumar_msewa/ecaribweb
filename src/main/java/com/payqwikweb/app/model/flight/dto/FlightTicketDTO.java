package com.payqwikweb.app.model.flight.dto;

import java.util.List;

import com.payqwikweb.model.app.request.TravellerFlightDetails;
import com.payqwikweb.model.app.response.UserDetail;

public class FlightTicketDTO {

	private String source;
	private String destination;
	private String journeyDate;
	private String arrTime;
	private double totalFare;
	private String transactionRefNo;
	private String status;
	private String mdexTxnRefNo;
	private List<TravellerFlightDetails> travellerDetails;
	private UserDetail userDetail;
	private String bookingRefId;
	private String txnStatus;
	private String txnDate;
	private String ticketDetails;
	private TicketObj oneWay;
	private TicketObj roundWay;
	private String tripType;
	private long flightTicketId;

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getJourneyDate() {
		return journeyDate;
	}

	public void setJourneyDate(String journeyDate) {
		this.journeyDate = journeyDate;
	}

	public String getArrTime() {
		return arrTime;
	}

	public void setArrTime(String arrTime) {
		this.arrTime = arrTime;
	}

	public double getTotalFare() {
		return totalFare;
	}

	public void setTotalFare(double totalFare) {
		this.totalFare = totalFare;
	}

	public String getTransactionRefNo() {
		return transactionRefNo;
	}

	public void setTransactionRefNo(String transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMdexTxnRefNo() {
		return mdexTxnRefNo;
	}

	public void setMdexTxnRefNo(String mdexTxnRefNo) {
		this.mdexTxnRefNo = mdexTxnRefNo;
	}

	public List<TravellerFlightDetails> getTravellerDetails() {
		return travellerDetails;
	}

	public void setTravellerDetails(List<TravellerFlightDetails> travellerDetails) {
		this.travellerDetails = travellerDetails;
	}

	public UserDetail getUserDetail() {
		return userDetail;
	}

	public void setUserDetail(UserDetail userDetail) {
		this.userDetail = userDetail;
	}

	public String getBookingRefId() {
		return bookingRefId;
	}

	public void setBookingRefId(String bookingRefId) {
		this.bookingRefId = bookingRefId;
	}

	public String getTxnStatus() {
		return txnStatus;
	}

	public void setTxnStatus(String txnStatus) {
		this.txnStatus = txnStatus;
	}

	public String getTxnDate() {
		return txnDate;
	}

	public void setTxnDate(String txnDate) {
		this.txnDate = txnDate;
	}

	public String getTicketDetails() {
		return ticketDetails;
	}

	public void setTicketDetails(String ticketDetails) {
		this.ticketDetails = ticketDetails;
	}

	public TicketObj getOneWay() {
		return oneWay;
	}

	public void setOneWay(TicketObj oneWay) {
		this.oneWay = oneWay;
	}

	public TicketObj getRoundWay() {
		return roundWay;
	}

	public void setRoundWay(TicketObj roundWay) {
		this.roundWay = roundWay;
	}

	public String getTripType() {
		return tripType;
	}

	public void setTripType(String tripType) {
		this.tripType = tripType;
	}

	public long getFlightTicketId() {
		return flightTicketId;
	}

	public void setFlightTicketId(long flightTicketId) {
		this.flightTicketId = flightTicketId;
	}

}
