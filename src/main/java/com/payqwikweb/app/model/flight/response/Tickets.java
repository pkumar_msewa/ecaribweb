package com.payqwikweb.app.model.flight.response;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown=true)
public class Tickets {

	@JsonProperty("Oneway")
	private List<FlightTicketDeatilsDTO> oneway;
	@JsonProperty("Roundway")
	private List<FlightTicketDeatilsDTO> roundway;
	
	public List<FlightTicketDeatilsDTO> getOneway() {
		return oneway;
	}
	public void setOneway(List<FlightTicketDeatilsDTO> oneway) {
		this.oneway = oneway;
	}
	public List<FlightTicketDeatilsDTO> getRoundway() {
		return roundway;
	}
	public void setRoundway(List<FlightTicketDeatilsDTO> roundway) {
		this.roundway = roundway;
	}
}
