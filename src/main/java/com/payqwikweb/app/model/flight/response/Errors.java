package com.payqwikweb.app.model.flight.response;

public class Errors {

	private String Code;
	private String Description;

	public String getCode() {
		return Code;
	}

	public void setCode(String code) {
		Code = code;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}
}
