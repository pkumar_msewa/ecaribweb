package com.payqwikweb.app.model.flight.response;

public class PayDTO {
	private String amount;
	private String loadAmt;
	private String code;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getLoadAmt() {
		return loadAmt;
	}

	public void setLoadAmt(String loadAmt) {
		this.loadAmt = loadAmt;
	}
}
