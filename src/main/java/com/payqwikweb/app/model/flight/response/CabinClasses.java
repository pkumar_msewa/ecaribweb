package com.payqwikweb.app.model.flight.response;

import java.util.ArrayList;

import com.payqwikweb.app.model.request.BookingClass;

public class CabinClasses {

	private ArrayList<BookingClass> bookingClass;
	private String CabinType;

	public ArrayList<BookingClass> getBookingClass() {
		return bookingClass;
	}

	public void setBookingClass(ArrayList<BookingClass> bookingClass) {
		this.bookingClass = bookingClass;
	}

	public String getCabinType() {
		return CabinType;
	}

	public void setCabinType(String cabinType) {
		CabinType = cabinType;
	}

}
