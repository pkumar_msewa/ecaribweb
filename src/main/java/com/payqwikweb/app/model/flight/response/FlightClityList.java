package com.payqwikweb.app.model.flight.response;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown=true)
public class FlightClityList {

	@JsonProperty("code")
	private String code;
	@JsonProperty("status")
	private String status;
	@JsonProperty("message")
	private String message;
	
	
	@JsonProperty("details")
	private List<AirLineNames> details;
	
	
	
	public void setCode(String code) {
		this.code = code;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<AirLineNames> getDetails() {
		return details;
	}
	public void setDetails(List<AirLineNames> details) {
		this.details = details;
	}
	public String getCode() {
		return code;
	}
	@Override
	public String toString() {
		return "{code=" + code + ", status=" + status + ", message=" + message + ", details="
				+ details + "}";
	}
	
}
