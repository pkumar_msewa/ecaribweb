package com.payqwikweb.app.model.flight.response;

public class Fares {

	private double Amount;
	private String ChargeCode;
	private String ChargeType;

	public double getAmount() {
		return Amount;
	}

	public void setAmount(double amount) {
		Amount = amount;
	}

	public String getChargeCode() {
		return ChargeCode;
	}

	public void setChargeCode(String chargeCode) {
		ChargeCode = chargeCode;
	}

	public String getChargeType() {
		return ChargeType;
	}

	public void setChargeType(String chargeType) {
		ChargeType = chargeType;
	}
}
