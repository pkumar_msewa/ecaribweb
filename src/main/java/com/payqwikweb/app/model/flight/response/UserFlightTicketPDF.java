package com.payqwikweb.app.model.flight.response;

import java.awt.Color;
import java.net.URL;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.web.servlet.view.document.AbstractPdfView;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Image;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;

public class UserFlightTicketPDF extends AbstractPdfView {

	@SuppressWarnings("unchecked")
	@Override
	protected void buildPdfDocument(java.util.Map<String, Object> model, Document document,
			com.lowagie.text.pdf.PdfWriter arg2, HttpServletRequest arg3, HttpServletResponse arg4) throws Exception {

		Font black = FontFactory.getFont(FontFactory.HELVETICA, 16, Font.NORMAL);
		
		FontFactory.getFont(FontFactory.HELVETICA, 12, Font.UNDERLINE, new Color(0, 0, 255));

		Map<String,Object> userData = (Map<String,Object>) model.get("userDataTicket");

		if (userData!=null) {
			Object busTicket=userData.get("busTicket");

			try
			{

				document.open();

				JSONObject jobj  =new JSONObject(busTicket.toString());
				JSONArray jArr = jobj.getJSONArray("travellerDetails");
				String img="https://www.vpayqwik.com/resources/images/vijayalogo.png";
				Image image2 = Image.getInstance(new URL(img));
				image2.setAlignment(Image.RIGHT);
				image2.scaleAbsolute(50, 50);

				JSONObject ticketsResp=jobj.getJSONObject("ticketsResp");
				JSONObject ticket=ticketsResp.getJSONObject("Tickets");
				JSONArray oneway=ticket.getJSONArray("Oneway");
				JSONArray roundway=ticket.getJSONArray("Roundway");
				String origin=oneway.getJSONObject(0).getString("origin");
				String destination=oneway.getJSONObject(oneway.length()-1).getString("destination");

				/*String origin=flightListRepository.getCityByCode(org);
				String destination=flightListRepository.getCityByCode(dest);*/

				String bk=jobj.getString("bookingRefNo");
				String bkd=jobj.getString("created");
				String arr[]=bkd.split(" ");
				bkd=arr[0];
				PdfPTable table = new PdfPTable(2);

				table.setWidthPercentage(100);
				table.addCell(getCell("E-TICKET", PdfPCell.ALIGN_LEFT));
				table.addCell(image2);
				document.add(table);

				PdfPTable table2 = new PdfPTable(2);
				table2.setWidthPercentage(100);
				table2.addCell(getCell("Booking Ref Id: "+bk, PdfPCell.ALIGN_LEFT));
				table2.addCell(getCell("Customer Care: +918025011300", PdfPCell.ALIGN_LEFT));
				document.add(table2);

				PdfPTable table3= new PdfPTable(1);
				table3.setWidthPercentage(100);
				table3.addCell(getCell("Booked on: "+bkd, PdfPCell.ALIGN_LEFT));
				document.add(table3);

				document.add(new Paragraph("----------------------------------------------------------------------------------------------------------------------------------"));
				document.add(new Paragraph("Flight Details :"));
				document.add(new Paragraph(origin +" to "+destination));

				PdfPTable table4= new PdfPTable(1);
				table4.addCell(getCell("Journey Date: "+oneway.getJSONObject(0).getString("departureDate"), PdfPCell.ALIGN_RIGHT));
				document.add(table4);
				document.add(new Paragraph(""));
				document.add( Chunk.NEWLINE );

				PdfPTable table5= new PdfPTable(2);


				for (int i = 0; i < oneway.length(); i++) {
					table5.addCell(getCell("Departure", PdfPCell.ALIGN_LEFT));
					table5.addCell(getCell("Arrival", PdfPCell.ALIGN_RIGHT));
					table5.addCell(getCell(oneway.getJSONObject(i).getString("origin"), PdfPCell.ALIGN_LEFT));
					table5.addCell(getCell(oneway.getJSONObject(i).getString("destination"),PdfPCell.ALIGN_RIGHT));
					table5.addCell(getCell(oneway.getJSONObject(i).getString("departureDate"), PdfPCell.ALIGN_LEFT));
					table5.addCell(getCell(oneway.getJSONObject(i).getString("departureDate"),PdfPCell.ALIGN_RIGHT));
					table5.addCell(getCell(oneway.getJSONObject(i).getString("departureTime"), PdfPCell.ALIGN_LEFT));
					table5.addCell(getCell(oneway.getJSONObject(i).getString("arrivalTime"), PdfPCell.ALIGN_RIGHT));
					table5.addCell(getCell(" ", PdfPCell.ALIGN_CENTER));
					table5.addCell(getCell(" ", PdfPCell.ALIGN_CENTER));
				}

				document.add(table5);
				document.add(Chunk.NEWLINE);
				if (roundway.length()>0) {
					document.add(new Paragraph("Return"));
					document.add(Chunk.NEWLINE);
				}

				PdfPTable table0= new PdfPTable(2);

				for (int i = 0; i < roundway.length(); i++) {
					table0.addCell(getCell(roundway.getJSONObject(i).getString("origin"), PdfPCell.ALIGN_LEFT));
					table0.addCell(getCell(roundway.getJSONObject(i).getString("destination"),PdfPCell.ALIGN_RIGHT));
					table0.addCell(getCell(roundway.getJSONObject(i).getString("departureDate"), PdfPCell.ALIGN_LEFT));
					table0.addCell(getCell(roundway.getJSONObject(i).getString("departureDate"),PdfPCell.ALIGN_RIGHT));
					table0.addCell(getCell(roundway.getJSONObject(i).getString("departureTime"), PdfPCell.ALIGN_LEFT));
					table0.addCell(getCell(roundway.getJSONObject(i).getString("arrivalTime"), PdfPCell.ALIGN_RIGHT));
					table0.addCell(getCell(" ", PdfPCell.ALIGN_CENTER));
					table0.addCell(getCell(" ", PdfPCell.ALIGN_CENTER));
				}

				document.add(table0);
				document.add(Chunk.NEWLINE);
				
				document.add(new Paragraph("----------------------------------------------------------------------------------------------------------------------------------"));

				document.add(new Paragraph("Traveller Details : "));

				document.add(Chunk.NEWLINE);

				PdfPTable table6= new PdfPTable(6);

				table6.addCell("SL No");
				table6.addCell("First Name");
				table6.addCell("Last Name");
				table6.addCell("Gender");
				table6.addCell("Pnr No");
				table6.addCell("Passenger Type");

				for (int i = 0; i < jArr.length(); i++) {

					table6.addCell(i+1+"");
					table6.addCell(jArr.getJSONObject(i).getString("fName"));
					table6.addCell(jArr.getJSONObject(i).getString("lName"));
					table6.addCell(jArr.getJSONObject(i).getString("gender"));
					table6.addCell(jArr.getJSONObject(i).getString("ticketNo"));
					table6.addCell(jArr.getJSONObject(i).getString("type"));
				}
				document.add(table6);
				document.close();

			} catch (Exception e){
				e.printStackTrace();
			}
		}
	}

	public static PdfPCell getCell(String text, int alignment) {
		PdfPCell cell = new PdfPCell(new Phrase(text));
		cell.setPadding(0);
		cell.setHorizontalAlignment(alignment);
		cell.setBorder(PdfPCell.NO_BORDER);
		return cell;
	}
}
