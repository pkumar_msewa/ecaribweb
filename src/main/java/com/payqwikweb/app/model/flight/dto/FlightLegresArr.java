package com.payqwikweb.app.model.flight.dto;

public class FlightLegresArr {

	private String aircraftType;
	private String airlineName;
	private String arrivalTime;
	private String cabin;
	private String departureTime;
	private String destination;
	private String duration;
	private String flightNumber;
	private String origin;
	private String baggageUnit;
	private String baggageWeight;
	private String returnFlight;
	private String flightName;
	
	public String getAircraftType() {
		return aircraftType;
	}
	public void setAircraftType(String aircraftType) {
		this.aircraftType = aircraftType;
	}
	public String getAirlineName() {
		return airlineName;
	}
	public void setAirlineName(String airlineName) {
		this.airlineName = airlineName;
	}
	public String getArrivalTime() {
		return arrivalTime;
	}
	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}
	public String getCabin() {
		return cabin;
	}
	public void setCabin(String cabin) {
		this.cabin = cabin;
	}
	public String getDepartureTime() {
		return departureTime;
	}
	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getDuration() {
		return duration;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}
	public String getFlightNumber() {
		return flightNumber;
	}
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}
	public String getOrigin() {
		return origin;
	}
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	public String getBaggageUnit() {
		return baggageUnit;
	}
	public void setBaggageUnit(String baggageUnit) {
		this.baggageUnit = baggageUnit;
	}
	public String getBaggageWeight() {
		return baggageWeight;
	}
	public void setBaggageWeight(String baggageWeight) {
		this.baggageWeight = baggageWeight;
	}
	public String getReturnFlight() {
		return returnFlight;
	}
	public void setReturnFlight(String returnFlight) {
		this.returnFlight = returnFlight;
	}
	public String getFlightName() {
		return flightName;
	}
	public void setFlightName(String flightName) {
		this.flightName = flightName;
	}
	
	
}








