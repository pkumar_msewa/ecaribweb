package com.payqwikweb.app.model.flight.response;

import java.util.List;

import com.payqwikweb.model.app.request.TravellerFlightDetails;

public class FlightTicketResp {

	private List<TravellerFlightDetails> travellerDetails;
	private TicketsResp ticketsResp;

	private String bookingRefNo;
	private String email;
	private String created;
	
	
	public List<TravellerFlightDetails> getTravellerDetails() {
		return travellerDetails;
	}
	public void setTravellerDetails(List<TravellerFlightDetails> travellerDetails) {
		this.travellerDetails = travellerDetails;
	}
	public TicketsResp getTicketsResp() {
		return ticketsResp;
	}
	public void setTicketsResp(TicketsResp ticketsResp) {
		this.ticketsResp = ticketsResp;
	}
	public String getBookingRefNo() {
		return bookingRefNo;
	}
	public void setBookingRefNo(String bookingRefNo) {
		this.bookingRefNo = bookingRefNo;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
}
