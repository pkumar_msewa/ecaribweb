package com.payqwikweb.app.model;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.thirdparty.model.JSONWrapper;

public class AddServiceTypeDTO implements JSONWrapper{

	private String name;

	private String description;
	
	private String sessionId;
	
	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	@Override
	public JSONObject toJSON() {
		JSONObject json = new JSONObject();
		try {
			json.put("name", getName());
			json.put("sessionId", getSessionId());
			json.put("description", getDescription());
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return json;
	}

}
