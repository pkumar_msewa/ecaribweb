package com.payqwikweb.app.model;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.thirdparty.model.JSONWrapper;

public class UpiPayRequest implements JSONWrapper{

	private double amount;
	private String merchantVpa;
	private long mid;
	private String mTxnId;
	private String ipAddress;
	private double additionalCharge;

	public double getAdditionalCharge() {
		return additionalCharge;
	}

	public void setAdditionalCharge(double additionalCharge) {
		this.additionalCharge = additionalCharge;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getMerchantVpa() {
		return merchantVpa;
	}

	public void setMerchantVpa(String merchantVpa) {
		this.merchantVpa = merchantVpa;
	}

	public long getMid() {
		return mid;
	}

	public void setMid(long mid) {
		this.mid = mid;
	}

	public String getmTxnId() {
		return mTxnId;
	}

	public void setmTxnId(String mTxnId) {
		this.mTxnId = mTxnId;
	}
	
	 @Override
	    public JSONObject toJSON() {
	        JSONObject jsonObject = new JSONObject();
	        try {
	            jsonObject.put("mTxnId",getmTxnId());
	            jsonObject.put("mid",getMid());
	            jsonObject.put("merchantVpa",getMerchantVpa());
	            jsonObject.put("amount",getAmount());
	            return jsonObject;
	        } catch (JSONException e) {
	            return null;
	        }

	    }
}
