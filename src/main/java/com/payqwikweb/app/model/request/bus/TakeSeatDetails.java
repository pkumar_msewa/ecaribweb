package com.payqwikweb.app.model.request.bus;

import com.payqwikweb.app.model.busdto.BDPointDTO;

public class TakeSeatDetails {

	private String busId;
	private String bdId;
	private String seats;
	private String seatsType;
	private String fares;
	private String totalFare;
	private String doj;
	private String busType;
	private String travelName;
	private String departureTime;
	private String arrivalTime;
	private String source;
	private String destination;
	private String duration;
	private String boardingInfo;
	private String sourceId;
	private String destinationId;
	private String routId;
	private String engineId;
	private String commission;
	private String markup;
	private BDPointDTO bdPointDTO;
	private String droppingInfo;
	
	public String getBdId() {
		return bdId;
	}
	public void setBdId(String bdId) {
		this.bdId = bdId;
	}
	public String getSeats() {
		return seats;
	}
	public void setSeats(String seats) {
		this.seats = seats;
	}
	public String getTotalFare() {
		return totalFare;
	}
	public void setTotalFare(String totalFare) {
		this.totalFare = totalFare;
	}
	public String getDoj() {
		return doj;
	}
	public void setDoj(String doj) {
		this.doj = doj;
	}
	public String getBusType() {
		return busType;
	}
	public void setBusType(String busType) {
		this.busType = busType;
	}
	public String getTravelName() {
		return travelName;
	}
	public void setTravelName(String travelName) {
		this.travelName = travelName;
	}
	public String getDepartureTime() {
		return departureTime;
	}
	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}
	public String getArrivalTime() {
		return arrivalTime;
	}
	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getDuration() {
		return duration;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}
	
	public String getBoardingInfo() {
		return boardingInfo;
	}
	public void setBoardingInfo(String boardingInfo) {
		this.boardingInfo = boardingInfo;
	}
	public String getSourceId() {
		return sourceId;
	}
	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}
	public String getDestinationId() {
		return destinationId;
	}
	public void setDestinationId(String destinationId) {
		this.destinationId = destinationId;
	}
	public String getRoutId() {
		return routId;
	}
	public void setRoutId(String routId) {
		this.routId = routId;
	}
	public String getCommission() {
		return commission;
	}
	public void setCommission(String commission) {
		this.commission = commission;
	}
	public String getMarkup() {
		return markup;
	}
	public void setMarkup(String markup) {
		this.markup = markup;
	}
	public BDPointDTO getBdPointDTO() {
		return bdPointDTO;
	}
	public void setBdPointDTO(BDPointDTO bdPointDTO) {
		this.bdPointDTO = bdPointDTO;
	}
	public String getDroppingInfo() {
		return droppingInfo;
	}
	public void setDroppingInfo(String droppingInfo) {
		this.droppingInfo = droppingInfo;
	}
	public String getBusId() {
		return busId;
	}
	public void setBusId(String busId) {
		this.busId = busId;
	}
	public String getEngineId() {
		return engineId;
	}
	public void setEngineId(String engineId) {
		this.engineId = engineId;
	}
	public String getSeatsType() {
		return seatsType;
	}
	public void setSeatsType(String seatsType) {
		this.seatsType = seatsType;
	}
	public String getFares() {
		return fares;
	}
	public void setFares(String fares) {
		this.fares = fares;
	}
	
}
