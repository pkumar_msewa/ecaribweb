package com.payqwikweb.app.model.request;


public class AddVoucherReq extends AdminRequest {

	private String sessionId;
	private long voucherQty;
	private double voucherAmount;
	private String expiryDate;
	
	
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public long getVoucherQty() {
		return voucherQty;
	}
	public void setVoucherQty(long voucherQty) {
		this.voucherQty = voucherQty;
	}
	public double getVoucherAmount() {
		return voucherAmount;
	}
	public void setVoucherAmount(double voucherAmount) {
		this.voucherAmount = voucherAmount;
	}
	public String getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
}
