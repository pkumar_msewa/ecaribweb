package com.payqwikweb.app.model.request;

import java.util.Map;

public class FetchMobileList {

	public String sessionId;
	public Map<String,String> mobileList;
	
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public Map<String, String> getMobileList() {
		return mobileList;
	}
	public void setMobileList(Map<String, String> mobileList) {
		this.mobileList = mobileList;
	}
	
}
