package com.payqwikweb.app.model.request;

import org.hibernate.validator.constraints.NotEmpty;

public class BulkMailRequest extends SessionDTO {

    @NotEmpty(message = "Enter subject of mail")
    private String subject;

    @NotEmpty(message = "Enter content of mail")
    private String content;

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
