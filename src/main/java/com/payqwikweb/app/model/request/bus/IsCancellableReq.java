package com.payqwikweb.app.model.request.bus;

import java.util.List;


public class IsCancellableReq 
{

	private String bookId;
	private String canceltype;
	private List<String> seatNo;
	private String ipAddress;
	private String sessionId;
	private String vPqTxnId;
	
	public String getBookId() {
		return bookId;
	}
	public void setBookId(String bookId) {
		this.bookId = bookId;
	}
	public String getCanceltype() {
		return canceltype;
	}
	public void setCanceltype(String canceltype) {
		this.canceltype = canceltype;
	}
	public List<String> getSeatNo() {
		return seatNo;
	}
	public void setSeatNo(List<String> seatNo) {
		this.seatNo = seatNo;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public String getvPqTxnId() {
		return vPqTxnId;
	}
	public void setvPqTxnId(String vPqTxnId) {
		this.vPqTxnId = vPqTxnId;
	}
	
}
