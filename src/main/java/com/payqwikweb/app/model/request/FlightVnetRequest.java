package com.payqwikweb.app.model.request;

import java.util.List;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.payqwikweb.model.app.request.TravellerFlightDetails;
import com.thirdparty.model.JSONWrapper;

	
	public class FlightVnetRequest extends SessionDTO implements JSONWrapper{
	    private String amount;
	    private String returnURL;
	    private final String serviceCode = "LMB";
	    private String paymentMode;
		private String ticketDetails;
		private List<TravellerFlightDetails> travellerDetails;
		
	    public String getAmount() {
	        return amount;
	    }

	    public void setAmount(String amount) {
	        this.amount = amount;
	    }

	    public String getReturnURL() {
	        return returnURL;
	    }

	    public void setReturnURL(String returnURL) {
	        this.returnURL = returnURL;
	    }

	    public String getServiceCode() {
	        return serviceCode;
	    }
	    public String getPaymentMode() {
			return paymentMode;
		}

		public void setPaymentMode(String paymentMode) {
			this.paymentMode = paymentMode;
		}

		public String getTicketDetails() {
			return ticketDetails;
		}

		public void setTicketDetails(String ticketDetails) {
			this.ticketDetails = ticketDetails;
		}

		public List<TravellerFlightDetails> getTravellerDetails() {
			return travellerDetails;
		}

		public void setTravellerDetails(List<TravellerFlightDetails> travellerDetails) {
			this.travellerDetails = travellerDetails;
		}

		@Override
	     public JSONObject toJSON() {
	         JSONObject json = new JSONObject();
	         JSONArray travellersDetails=new JSONArray();
	         try {
	             json.put("sessionId",getSessionId());
	             json.put("serviceCode",getServiceCode());
	             json.put("amount",getAmount());
	             json.put("returnURL",getReturnURL());
	             
	             if (getTravellerDetails()!=null) {
	             for (int j = 0; j < getTravellerDetails().size(); j++) {
	           JSONObject travellersDetail=new JSONObject();
	           travellersDetail.put("fName",(getTravellerDetails().get(j).getfName()== null) ? "" :getTravellerDetails().get(j).getfName());
	           travellersDetail.put("lName",(getTravellerDetails().get(j).getlName()== null) ? "" :getTravellerDetails().get(j).getlName());
	           travellersDetail.put("age",(getTravellerDetails().get(j).getAge()== null) ? "" :getTravellerDetails().get(j).getAge());
	           travellersDetail.put("gender",(getTravellerDetails().get(j).getGender()== null) ? "" :getTravellerDetails().get(j).getGender());
	           travellersDetail.put("fare",(getTravellerDetails().get(j).getFare()== null) ? "" :getTravellerDetails().get(j).getFare());
	           travellersDetail.put("travellerType",(getTravellerDetails().get(j).getType()== null) ? "" :getTravellerDetails().get(j).getType());
	           travellersDetail.put("ticketNo",(getTravellerDetails().get(j).getTicketNo()== null) ? "" :getTravellerDetails().get(j).getTicketNo());
	           travellersDetails.put(travellersDetail);
	          }
	             json.put("travellerDetails", travellersDetails);
	             
	         }
	             json.put("mode", getPaymentMode());
	             json.put("ticketDetails", getTicketDetails());
	             
	         }catch(JSONException ex){
	             ex.printStackTrace();
	         }
	             return json;
	     }


}
