package com.payqwikweb.app.model.request;

import java.util.Date;

public class EBSRefundRequest {
	
	private String tranRefNo;
	private String sessionId;
	private Date date;
	public String getTranRefNo() {
		return tranRefNo;
	}
	public void setTranRefNo(String tranRefNo) {
		this.tranRefNo = tranRefNo;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	@Override
	public String toString() {
		return "EBSRefundRequest [tranRefNo=" + tranRefNo + ", sessionId=" + sessionId + ", date=" + date + "]";
	}

}
