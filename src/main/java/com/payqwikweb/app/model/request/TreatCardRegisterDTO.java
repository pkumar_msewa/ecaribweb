package com.payqwikweb.app.model.request;

public class TreatCardRegisterDTO {

	private String membershipCode;
	private String membershipCodeExpire;
	private String username;

	public String getMembershipCode() {
		return membershipCode;
	}

	public void setMembershipCode(String membershipCode) {
		this.membershipCode = membershipCode;
	}

	public String getMembershipCodeExpire() {
		return membershipCodeExpire;
	}

	public void setMembershipCodeExpire(String membershipCodeExpire) {
		this.membershipCodeExpire = membershipCodeExpire;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

}
