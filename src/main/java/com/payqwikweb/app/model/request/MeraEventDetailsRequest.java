package com.payqwikweb.app.model.request;

public class MeraEventDetailsRequest extends MeraEventsCommonRequest {

	public String city;
	public String state;
	public String country;
	public String eventCategory;
	public String eventName;
	public String eventType;
	public String eventVanue;
	public String eventStartDate;
	public String eventEndDate;
	public String eventBookingDate;
	public String  noOfAttendees;
	public double totalAmount;
	public String ticketId;
	public String ticketType;
	public String ticketName;
	private String quantity;
	private String price;

	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getEventCategory() {
		return eventCategory;
	}

	public void setEventCategory(String eventCategory) {
		this.eventCategory = eventCategory;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getEventType() {
		return eventType;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	public String getEventVanue() {
		return eventVanue;
	}

	public void setEventVanue(String eventVanue) {
		this.eventVanue = eventVanue;
	}

	public String getEventStartDate() {
		return eventStartDate;
	}

	public void setEventStartDate(String eventStartDate) {
		this.eventStartDate = eventStartDate;
	}

	public String getEventEndDate() {
		return eventEndDate;
	}

	public void setEventEndDate(String eventEndDate) {
		this.eventEndDate = eventEndDate;
	}

	public String getEventBookingDate() {
		return eventBookingDate;
	}

	public void setEventBookingDate(String eventBookingDate) {
		this.eventBookingDate = eventBookingDate;
	}

	
	public String getNoOfAttendees() {
		return noOfAttendees;
	}

	public void setNoOfAttendees(String noOfAttendees) {
		this.noOfAttendees = noOfAttendees;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getTicketType() {
		return ticketType;
	}

	public void setTicketType(String ticketType) {
		this.ticketType = ticketType;
	}

	public String getTicketName() {
		return ticketName;
	}

	public void setTicketName(String ticketName) {
		this.ticketName = ticketName;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	
}
