package com.payqwikweb.app.model.request;

public class UserInfoRequest extends SessionDTO{
    private String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
