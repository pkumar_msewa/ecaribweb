package com.payqwikweb.app.model.request.bus;

public class GetSourceCity {

	private String sourceKey;
	private String sessionId;
	
	public String getSourceKey() {
		return sourceKey;
	}

	public void setSourceKey(String sourceKey) {
		this.sourceKey = sourceKey;
	}
	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
}
