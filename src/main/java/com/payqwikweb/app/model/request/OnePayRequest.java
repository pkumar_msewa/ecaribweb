package com.payqwikweb.app.model.request;


public class OnePayRequest extends SessionDTO{
    private String transactionRefNo;
    private String amount;
    private String splitAmount;
    

    public String getSplitAmount() {
		return splitAmount;
	}

	public void setSplitAmount(String splitAmount) {
		this.splitAmount = splitAmount;
	}

	public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTransactionRefNo() {
        return transactionRefNo;
    }

    public void setTransactionRefNo(String transactionRefNo) {
        this.transactionRefNo = transactionRefNo;
    }
}
