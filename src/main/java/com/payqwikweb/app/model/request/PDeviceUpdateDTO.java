package com.payqwikweb.app.model.request;

import com.thirdparty.model.JSONWrapper;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

public class PDeviceUpdateDTO implements JSONWrapper {

    private String apiKey;
    private String device;
    private String imei;
    private String model;

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    @Override
    public JSONObject toJSON() {
        JSONObject json = new JSONObject();
        try {
            json.put("apiKey",getApiKey());
            json.put("device",getDevice());
            json.put("imei",getImei());
            json.put("model",getModel());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json;
    }
}
