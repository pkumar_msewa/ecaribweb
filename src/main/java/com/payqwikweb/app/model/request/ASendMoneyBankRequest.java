package com.payqwikweb.app.model.request;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.web.multipart.MultipartFile;

import com.thirdparty.model.JSONWrapper;

public class ASendMoneyBankRequest extends SessionDTO implements JSONWrapper{

    private String bankCode;
    private String ifscCode;
    private String accountNumber;
    private String amount;
    private String bankName;
    
    private String senderName;
    private String senderLastName;
    private String senderEmailId;
    private String senderMobileNo;
    private String idProofNo;
    private MultipartFile idProofImage;
    private MultipartFile idProofBImage;
    private String description;
    private String accountName;
    private String receiverLastName;
    private String receiverEmailId;
    private String receiverMobileNo;
    private String encodedBytes;
    private String contentType;
    private String encodedBytes2;
    private String contentType2;
    
	public String getEncodedBytes2() {
		return encodedBytes2;
	}

	public void setEncodedBytes2(String encodedBytes2) {
		this.encodedBytes2 = encodedBytes2;
	}

	public String getContentType2() {
		return contentType2;
	}

	public void setContentType2(String contentType2) {
		this.contentType2 = contentType2;
	}

	public MultipartFile getIdProofBImage() {
		return idProofBImage;
	}

	public void setIdProofBImage(MultipartFile idProofBImage) {
		this.idProofBImage = idProofBImage;
	}

	public String getSenderLastName() {
		return senderLastName;
	}

	public void setSenderLastName(String senderLastName) {
		this.senderLastName = senderLastName;
	}

	public String getReceiverLastName() {
		return receiverLastName;
	}

	public void setReceiverLastName(String receiverLastName) {
		this.receiverLastName = receiverLastName;
	}

	public String getEncodedBytes() {
		return encodedBytes;
	}

	public void setEncodedBytes(String encodedBytes) {
		this.encodedBytes = encodedBytes;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public MultipartFile getIdProofImage() {
		return idProofImage;
	}

	public void setIdProofImage(MultipartFile idProofImage) {
		this.idProofImage = idProofImage;
	}

	public String getIdProofNo() {
		return idProofNo;
	}

	public void setIdProofNo(String idProofNo) {
		this.idProofNo = idProofNo;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSenderName() {
		return senderName;
	}

	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}

	public String getSenderEmailId() {
		return senderEmailId;
	}

	public void setSenderEmailId(String senderEmailId) {
		this.senderEmailId = senderEmailId;
	}

	public String getSenderMobileNo() {
		return senderMobileNo;
	}

	public void setSenderMobileNo(String senderMobileNo) {
		this.senderMobileNo = senderMobileNo;
	}


	/*public MultipartFile getIdProofImage() {
		return idProofImage;
	}

	public void setIdProofImage(MultipartFile idProofImage) {
		this.idProofImage = idProofImage;
	}*/

	public String getReceiverEmailId() {
		return receiverEmailId;
	}

	public void setReceiverEmailId(String receiverEmailId) {
		this.receiverEmailId = receiverEmailId;
	}

	public String getReceiverMobileNo() {
		return receiverMobileNo;
	}

	public void setReceiverMobileNo(String receiverMobileNo) {
		this.receiverMobileNo = receiverMobileNo;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getIfscCode() {
        return ifscCode;
    }

    public void setIfscCode(String ifscCode) {
        this.ifscCode = ifscCode;
    }

/*    public long getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(long accountNumber) {
        this.accountNumber = accountNumber;
    }*/

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }
    
    public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	
	@Override
    public JSONObject toJSON() {
        JSONObject json = new JSONObject();
        try{
            
        	json.put("sessionId",getSessionId());
            json.put("bankCode",getBankCode());
            json.put("ifscCode",getIfscCode());
            json.put("accountNumber",getAccountNumber());
            json.put("amount",getAmount());
            json.put("accountName",getAccountName());
            json.put("bankName", getBankName());
            
            json.put("senderName",getSenderName().trim());
            json.put("senderEmailId",getSenderEmailId());
            json.put("senderMobileNo",getSenderMobileNo());
            json.put("receiverEmailId",getReceiverEmailId());
            json.put("receiverMobileNo",getReceiverMobileNo());
            json.put("description",getDescription());
            json.put("idProofNo",getIdProofNo());
            json.put("encodedBytes",getEncodedBytes());
            json.put("contentType",getContentType());
            json.put("encodedBytes2",getEncodedBytes2());
            json.put("contentType2",getContentType2());
            
        }catch(JSONException ex){
            ex.printStackTrace();
            System.err.println("error ::" + ex.getMessage());
        }
        return json;
    }
}
