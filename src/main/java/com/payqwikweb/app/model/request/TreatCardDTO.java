package com.payqwikweb.app.model.request;

public class TreatCardDTO   {

	private String phoneNumber;
	private String emailId;
	private String first;
	private String last;
	private String sessionId;
	private String membershipCardValidityInDays;
	private String days;
	private String amount;
	
	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getDays() {
		return days;
	}

	public void setDays(String days) {
		this.days = days;
	}

	public String getMembershipCardValidityInDays() {
		return membershipCardValidityInDays;
	}

	public void setMembershipCardValidityInDays(String membershipCardValidityInDays) {
		this.membershipCardValidityInDays = membershipCardValidityInDays;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getFirst() {
		return first;
	}

	public void setFirst(String first) {
		this.first = first;
	}

	public String getLast() {
		return last;
	}

	public void setLast(String last) {
		this.last = last;
	}

	

}
