package com.payqwikweb.app.model.request;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Pattern;

public class SMSRequest extends SessionDTO{

    @NotEmpty(message = "Enter Content")
    private String content;

    @Pattern(regexp = "^[7-9]{1}[0-9]{9}$",message="Enter valid mobile number")
    private String mobile;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}
