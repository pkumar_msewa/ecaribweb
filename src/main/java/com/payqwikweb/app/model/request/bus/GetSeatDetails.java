package com.payqwikweb.app.model.request.bus;

public class GetSeatDetails {
	
	private String busId;
	private String journeyDate;
	private boolean seater;
	private boolean sleeper;
	private int engineId;
	private String sessionId;
	private String data;
	private boolean bpDpLayout;
	private String dpId;
	private String bpId;
	

	public boolean isBpDpLayout() {
		return bpDpLayout;
	}

	public void setBpDpLayout(boolean bpDpLayout) {
		this.bpDpLayout = bpDpLayout;
	}

	public String getDpId() {
		return dpId;
	}

	public void setDpId(String dpId) {
		this.dpId = dpId;
	}

	public String getBpId() {
		return bpId;
	}

	public void setBpId(String bpId) {
		this.bpId = bpId;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public String getBusId() {
		return busId;
	}
	public void setBusId(String busId) {
		this.busId = busId;
	}
	public String getJourneyDate() {
		return journeyDate;
	}
	public void setJourneyDate(String journeyDate) {
		this.journeyDate = journeyDate;
	}
	public boolean isSeater() {
		return seater;
	}
	public void setSeater(boolean seater) {
		this.seater = seater;
	}
	public boolean isSleeper() {
		return sleeper;
	}
	public void setSleeper(boolean sleeper) {
		this.sleeper = sleeper;
	}
	public int getEngineId() {
		return engineId;
	}
	public void setEngineId(int engineId) {
		this.engineId = engineId;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
}
