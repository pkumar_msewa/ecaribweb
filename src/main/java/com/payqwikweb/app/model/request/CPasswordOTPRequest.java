package com.payqwikweb.app.model.request;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Pattern;

public class CPasswordOTPRequest extends CPasswordRequest{

    @NotEmpty(message = "Please enter otp")
    @Pattern(regexp = "^[0-9]{5,6}$",message = "Enter valid otp")
    private String key;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
