package com.payqwikweb.app.model.request.reports;


import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.payqwikweb.app.model.request.SessionDTO;
import com.thirdparty.model.JSONWrapper;

public class MISDetailRequest extends SessionDTO implements JSONWrapper{

    private String bankCode;
    private String ifscCode;
    private String accountNumber;
    private String amount;
    private String accountName;
    private String benefAddress;
	private String emailId;
	private String benefMobileNo;
	private String transactionRefNo;
	private String bankRefNo;
	private String bankStatus;
	private String bankService;
    private String date;
    private String endDate;
//	private String benefName;
//	private String benefAccountNo;
//	private String benefIFSC;
//	private String transferAmount;
    
    
    public String getBankCode() {
        return bankCode;
    }
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getBankRefNo() {
		return bankRefNo;
	}

	public void setBankRefNo(String bankRefNo) {
		this.bankRefNo = bankRefNo;
	}

	public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getIfscCode() {
        return ifscCode;
    }

    public void setIfscCode(String ifscCode) {
        this.ifscCode = ifscCode;
    }

    public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }
    
    public String getBenefAddress() {
		return benefAddress;
	}

	public void setBenefAddress(String benefAddress) {
		this.benefAddress = benefAddress;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getBenefMobileNo() {
		return benefMobileNo;
	}

	public void setBenefMobileNo(String benefMobileNo) {
		this.benefMobileNo = benefMobileNo;
	}
	
	public String getTransactionRefNo() {
		return transactionRefNo;
	}

	public void setTransactionRefNo(String transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}

	public String getBankStatus() {
		return bankStatus;
	}

	public void setBankStatus(String bankStatus) {
		this.bankStatus = bankStatus;
	}

	public String getBankService() {
		return bankService;
	}

	public void setBankService(String bankService) {
		this.bankService = bankService;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
	@Override
    public JSONObject toJSON() {
        JSONObject json = new JSONObject();
        try{
            json.put("sessionId",getSessionId());
            json.put("bankCode",getBankCode());
            json.put("ifscCode",getIfscCode());
            json.put("accountNumber",getAccountNumber());
            json.put("amount",getAmount());
            json.put("accountName",getAccountName());
            json.put("benefAddress",getBenefAddress());
            json.put("transactionRefNo",getTransactionRefNo());
            json.put("benefMobileNo",getBenefMobileNo());
        }catch(JSONException ex){
            ex.printStackTrace();
        }
        return json;
    }
}
