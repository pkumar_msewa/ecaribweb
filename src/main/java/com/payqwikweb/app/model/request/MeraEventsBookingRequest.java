package com.payqwikweb.app.model.request;

import org.springframework.web.bind.annotation.RequestParam;

public class MeraEventsBookingRequest extends MeraEventsCommonRequest {

	private String eventId;
	private String ticketId;
	private int ticketID2;
	private int noOfTickets2;
	private int[] ticketArray;
	private int[] donateTicketArray;
	private String discountCode;
	private String referralCode;
	private String acode;
	private String noOfTickets;
	private String ticketPrice;
	

private String description;
private String type;
private String startDate;
private String endDate;


	public String city;
	public String state;
	public String country;
	public String eventCategory;
	public String eventName;
	public String eventType;
	public String eventVanue;
	public String eventStartDate;
	public String eventEndDate;
	public String eventBookingDate;
	public String  noOfAttendees;
	public double totalAmount;
	//public String ticketId;
	public String ticketType;
	public String ticketName;
	private String quantity;
	private String price;
	
	
	

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getEventCategory() {
		return eventCategory;
	}

	public void setEventCategory(String eventCategory) {
		this.eventCategory = eventCategory;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getEventType() {
		return eventType;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	public String getEventVanue() {
		return eventVanue;
	}

	public void setEventVanue(String eventVanue) {
		this.eventVanue = eventVanue;
	}

	public String getEventStartDate() {
		return eventStartDate;
	}

	public void setEventStartDate(String eventStartDate) {
		this.eventStartDate = eventStartDate;
	}

	public String getEventEndDate() {
		return eventEndDate;
	}

	public void setEventEndDate(String eventEndDate) {
		this.eventEndDate = eventEndDate;
	}

	public String getEventBookingDate() {
		return eventBookingDate;
	}

	public void setEventBookingDate(String eventBookingDate) {
		this.eventBookingDate = eventBookingDate;
	}

	public String getNoOfAttendees() {
		return noOfAttendees;
	}

	public void setNoOfAttendees(String noOfAttendees) {
		this.noOfAttendees = noOfAttendees;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getTicketType() {
		return ticketType;
	}

	public void setTicketType(String ticketType) {
		this.ticketType = ticketType;
	}

	public String getTicketName() {
		return ticketName;
	}

	public void setTicketName(String ticketName) {
		this.ticketName = ticketName;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getTicketPrice() {
		return ticketPrice;
	}

	public void setTicketPrice(String ticketPrice) {
		this.ticketPrice = ticketPrice;
	}

	public String getEventId() {
		return eventId;
	}

	public void setEventId(String eventId) {
		this.eventId = eventId;
	}

	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

	public int getTicketID2() {
		return ticketID2;
	}

	public void setTicketID2(int ticketID2) {
		this.ticketID2 = ticketID2;
	}

	public int getNoOfTickets2() {
		return noOfTickets2;
	}

	public void setNoOfTickets2(int noOfTickets2) {
		this.noOfTickets2 = noOfTickets2;
	}

	public String getNoOfTickets() {
		return noOfTickets;
	}

	public void setNoOfTickets(String noOfTickets) {
		this.noOfTickets = noOfTickets;
	}

	public int[] getTicketArray() {
		return ticketArray;
	}

	public void setTicketArray(int[] ticketArray) {
		this.ticketArray = ticketArray;
	}

	public int[] getDonateTicketArray() {
		return donateTicketArray;
	}

	public void setDonateTicketArray(int[] donateTicketArray) {
		this.donateTicketArray = donateTicketArray;
	}

	public String getDiscountCode() {
		return discountCode;
	}

	public void setDiscountCode(String discountCode) {
		this.discountCode = discountCode;
	}

	public String getReferralCode() {
		return referralCode;
	}

	public void setReferralCode(String referralCode) {
		this.referralCode = referralCode;
	}

	public String getAcode() {
		return acode;
	}

	public void setAcode(String acode) {
		this.acode = acode;
	}

}
