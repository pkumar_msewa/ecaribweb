package com.payqwikweb.app.model.request;

import java.util.Date;

import com.payqwikweb.app.model.UserStatus;

public class AllUserRequest extends AdminRequest {

	private String sessionId;
	private int page;
	private int size;
	private UserStatus status;
    private String startDate;
    private String endDate;
    private boolean isPageable;

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public UserStatus getStatus() {
		return status;
	}

	public void setStatus(UserStatus status) {
		this.status = status;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public boolean isPageable() {
		return isPageable;
	}

	public void setPageable(boolean isPageable) {
		this.isPageable = isPageable;
	}
	
	
}
