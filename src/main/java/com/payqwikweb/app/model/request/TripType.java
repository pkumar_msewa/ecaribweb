package com.payqwikweb.app.model.request;

public enum TripType 
{
	OneWay("OneWay"), RoundTrip("RoundTrip");

	 private final String value;

	 private TripType(String value) {
	  this.value = value;
	 }

	 @Override
	 public String toString() {
	  return value;
	 }

	 public String getValue() {
	  return value;
	 }

	 public static TripType getEnum(String value) {
	  if (value == null)
	   throw new IllegalArgumentException();
	  for (TripType v : values())
	   if (value.equalsIgnoreCase(v.getValue()))
	    return v;
	  throw new IllegalArgumentException();
	 }

}
