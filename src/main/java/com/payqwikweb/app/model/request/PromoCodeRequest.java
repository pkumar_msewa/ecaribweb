package com.payqwikweb.app.model.request;

import com.payqwikweb.app.model.response.CommissionDTO;
import com.thirdparty.model.JSONWrapper;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class PromoCodeRequest extends SessionDTO implements JSONWrapper {

	private String promoCode;
	private String terms;
	private String startDate;
	private String endDate;
	private double value;
	private boolean fixed;
	private String status;
	private String description;
	private ArrayList<String> services;
	private long serviceTypeId;
	private String cashBackValue;
	private String serviceId;
	private String promoCodeId;
	private List<CommissionDTO> serviceList;
	
	public List<CommissionDTO> getServiceList() {
		return serviceList;
	}

	public void setServiceList(List<CommissionDTO> serviceList) {
		this.serviceList = serviceList;
	}

	public String getPromoCodeId() {
		return promoCodeId;
	}

	public void setPromoCodeId(String promoCodeId) {
		this.promoCodeId = promoCodeId;
	}

	public String getCashBackValue() {
		return cashBackValue;
	}

	public void setCashBackValue(String cashBackValue) {
		this.cashBackValue = cashBackValue;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public long getServiceTypeId() {
		return serviceTypeId;
	}

	public boolean isFixed() {
		return fixed;
	}

	public void setFixed(boolean fixed) {
		this.fixed = fixed;
	}

	public void setServiceTypeId(long serviceTypeId) {
		this.serviceTypeId = serviceTypeId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setServices(ArrayList<String> services) {
		this.services = services;
	}

	public String getPromoCode() {
		return promoCode;
	}

	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}

	public String getTerms() {
		return terms;
	}

	public void setTerms(String terms) {
		this.terms = terms;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public ArrayList<String> getServices() {
		return services;
	}

	@Override
	public JSONObject toJSON() {
		JSONObject json = new JSONObject();
		try {
			json.put("sessionId", getSessionId());
			json.put("promoCode", getPromoCode());
			json.put("startDate", getStartDate());
			json.put("fixed", isFixed());
			json.put("endDate", getEndDate());
			json.put("value", getValue());
			json.put("description", getDescription());
			json.put("services", getServices());
			json.put("terms", getTerms());
			json.put("cashBackValue", getCashBackValue());
			json.put("promoCodeId", getPromoCodeId());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return json;
	}
}
