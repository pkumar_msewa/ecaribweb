package com.payqwikweb.app.model.request;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Pattern;

public class CPasswordRequest extends SessionDTO{

    @NotEmpty(message = "Enter current password")
    private String currentPassword;

    @NotEmpty(message = "Enter new password")
    @Pattern(regexp="^[0-9]{10}$",message = "Enter 10 digit numeric password")
    private String newPassword;

    private String confirmPassword;

    private boolean request;

    public String getCurrentPassword() {
        return currentPassword;
    }

    public void setCurrentPassword(String currentPassword) {
        this.currentPassword = currentPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public boolean isRequest() {
        return request;
    }

    public void setRequest(boolean request) {
        this.request = request;
    }
}
