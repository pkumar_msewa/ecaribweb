package com.payqwikweb.app.model.request;

import java.io.StringWriter;
import java.util.List;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonWriter;

import com.thirdparty.model.JSONRequest;

public class BulkFileUpload implements JSONRequest{
	private List<SendMoneyMobileDTO> data;
	private String sessionId;
	

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public List<SendMoneyMobileDTO> getData() {
		return data;
	}

	public void setData(List<SendMoneyMobileDTO> data) {
		this.data = data;
	}
	
	@Override
	public String getJsonRequest() {
		JsonObjectBuilder jsonBuilder = Json.createObjectBuilder();
		JsonArrayBuilder dtoList = Json.createArrayBuilder();
		JsonObjectBuilder mobileDTOs = Json.createObjectBuilder();
		for (int j = 0; j < getData().size(); j++) {
			mobileDTOs.add("mobileNumber", getData().get(j).getMobileNumber());
			mobileDTOs.add("amount", getData().get(j).getAmount());
			mobileDTOs.add("status", getData().get(j).getStatus());
			dtoList.add(mobileDTOs.build());
		}

		jsonBuilder.add("sessionId", getSessionId());
		jsonBuilder.add("mobileDTO", dtoList);

		JsonObject empObj = jsonBuilder.build();

		StringWriter jsnReqStr = new StringWriter();
		JsonWriter jsonWtr = Json.createWriter(jsnReqStr);
		jsonWtr.writeObject(empObj);
		jsonWtr.close();
		return jsnReqStr.toString();
	}

}
