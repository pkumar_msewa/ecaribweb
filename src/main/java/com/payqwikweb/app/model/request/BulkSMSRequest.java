package com.payqwikweb.app.model.request;

import org.hibernate.validator.constraints.NotEmpty;

public class BulkSMSRequest extends SessionDTO{

    @NotEmpty(message="Enter message content")
    private String content;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
