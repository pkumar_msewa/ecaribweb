package com.payqwikweb.app.model.request;

public class DirectPaymentRequest {

	private String tokenKey;
	private String mobileNumber;
	private double amount;
	private String consumerSecretKey;
	private String consumerTokenKey;
	private String status;
	private String retrivalReferenceNumber;
	private String transactionRefNo;
	private String otp;

	public String getTokenKey() {
		return tokenKey;
	}

	public void setTokenKey(String tokenKey) {
		this.tokenKey = tokenKey;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getConsumerSecretKey() {
		return consumerSecretKey;
	}

	public void setConsumerSecretKey(String consumerSecretKey) {
		this.consumerSecretKey = consumerSecretKey;
	}

	public String getConsumerTokenKey() {
		return consumerTokenKey;
	}

	public void setConsumerTokenKey(String consumerTokenKey) {
		this.consumerTokenKey = consumerTokenKey;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRetrivalReferenceNumber() {
		return retrivalReferenceNumber;
	}

	public void setRetrivalReferenceNumber(String retrivalReferenceNumber) {
		this.retrivalReferenceNumber = retrivalReferenceNumber;
	}

	public String getTransactionRefNo() {
		return transactionRefNo;
	}

	public void setTransactionRefNo(String transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

}
