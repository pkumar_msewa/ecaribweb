package com.payqwikweb.app.model.request;

public class UserMaxLimitDto extends SessionDTO {
	private String dailyNoOfTxn;
	private String dailyAmountOfTxn;
	private String monthlyNoOfTxn;
	private String monthlyAmountOfTxn;
	private String transactionType;
	private String mobileToken;
	
	public String getMobileToken() {
		return mobileToken;
	}
	public void setMobileToken(String mobileToken) {
		this.mobileToken = mobileToken;
	}
	public String getDailyNoOfTxn() {
		return dailyNoOfTxn;
	}
	public void setDailyNoOfTxn(String dailyNoOfTxn) {
		this.dailyNoOfTxn = dailyNoOfTxn;
	}
	public String getDailyAmountOfTxn() {
		return dailyAmountOfTxn;
	}
	public void setDailyAmountOfTxn(String dailyAmountOfTxn) {
		this.dailyAmountOfTxn = dailyAmountOfTxn;
	}
	public String getMonthlyNoOfTxn() {
		return monthlyNoOfTxn;
	}
	public void setMonthlyNoOfTxn(String monthlyNoOfTxn) {
		this.monthlyNoOfTxn = monthlyNoOfTxn;
	}
	public String getMonthlyAmountOfTxn() {
		return monthlyAmountOfTxn;
	}
	public void setMonthlyAmountOfTxn(String monthlyAmountOfTxn) {
		this.monthlyAmountOfTxn = monthlyAmountOfTxn;
	}
	public String getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
}
