package com.payqwikweb.app.model.request;

public class GraphDTO {

	private int year;
	private int month;
	private int date;
	private int day;
	private long aa;
	private long bb;
	private long cc;
	private long dd;
	private long ee;
	private long ff;
	private long gg;

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public long getAa() {
		return aa;
	}

	public void setAa(long aa) {
		this.aa = aa;
	}

	public long getBb() {
		return bb;
	}

	public void setBb(long bb) {
		this.bb = bb;
	}

	public long getCc() {
		return cc;
	}

	public void setCc(long cc) {
		this.cc = cc;
	}

	public long getDd() {
		return dd;
	}

	public void setDd(long dd) {
		this.dd = dd;
	}

	public long getEe() {
		return ee;
	}

	public void setEe(long ee) {
		this.ee = ee;
	}

	public long getFf() {
		return ff;
	}

	public void setFf(long ff) {
		this.ff = ff;
	}

	public long getGg() {
		return gg;
	}

	public void setGg(long gg) {
		this.gg = gg;
	}

	public int getDate() {
		return date;
	}

	public void setDate(int date) {
		this.date = date;
	}

}
