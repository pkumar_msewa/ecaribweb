package com.payqwikweb.app.model.request;

public class MicroPaymentRequest {

	private String sessionId;
	private String tokenKey;
	private String secretKey;
	private String mobileNumber;
	private double amount;
	private String consumerSecretKey;
	private String consumerTokenKey;
	private boolean success;
	private String retrivalReferenceNumber;
	private String transactionRefNo;

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getTokenKey() {
		return tokenKey;
	}

	public void setTokenKey(String tokenKey) {
		this.tokenKey = tokenKey;
	}

	public String getSecretKey() {
		return secretKey;
	}

	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getConsumerSecretKey() {
		return consumerSecretKey;
	}

	public void setConsumerSecretKey(String consumerSecretKey) {
		this.consumerSecretKey = consumerSecretKey;
	}

	public String getConsumerTokenKey() {
		return consumerTokenKey;
	}

	public void setConsumerTokenKey(String consumerTokenKey) {
		this.consumerTokenKey = consumerTokenKey;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getRetrivalReferenceNumber() {
		return retrivalReferenceNumber;
	}

	public void setRetrivalReferenceNumber(String retrivalReferenceNumber) {
		this.retrivalReferenceNumber = retrivalReferenceNumber;
	}

	public String getTransactionRefNo() {
		return transactionRefNo;
	}

	public void setTransactionRefNo(String transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}

}
