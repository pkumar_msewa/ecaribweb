package com.payqwikweb.app.model.request;

public class MobileSearchRequest extends SessionDTO{
    private String mobileSubString;

    public String getMobileSubString() {
        return mobileSubString;
    }

    public void setMobileSubString(String mobileSubString) {
        this.mobileSubString = mobileSubString;
    }
}
