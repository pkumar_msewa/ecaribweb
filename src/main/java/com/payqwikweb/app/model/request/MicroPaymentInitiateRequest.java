package com.payqwikweb.app.model.request;

public class MicroPaymentInitiateRequest {

	private String tokenKey;
	private String secretKey;
	private String mobileNumber;
	private double amount;
	private String consumerSecretKey;
	private String consumerTokenKey;
	private String authCode;

	public String getTokenKey() {
		return tokenKey;
	}

	public void setTokenKey(String tokenKey) {
		this.tokenKey = tokenKey;
	}

	public String getSecretKey() {
		return secretKey;
	}

	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getConsumerSecretKey() {
		return consumerSecretKey;
	}

	public void setConsumerSecretKey(String consumerSecretKey) {
		this.consumerSecretKey = consumerSecretKey;
	}

	public String getConsumerTokenKey() {
		return consumerTokenKey;
	}

	public void setConsumerTokenKey(String consumerTokenKey) {
		this.consumerTokenKey = consumerTokenKey;
	}

	public String getAuthCode() {
		return authCode;
	}

	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}
}
