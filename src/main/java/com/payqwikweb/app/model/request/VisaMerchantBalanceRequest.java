package com.payqwikweb.app.model.request;

public class VisaMerchantBalanceRequest {

	private String entityId;
	private String productId;
	private String yseId;
	private String balance;
	public String getEntityId() {
		return entityId;
	}
	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getYseId() {
		return yseId;
	}
	public void setYseId(String yseId) {
		this.yseId = yseId;
	}
	public String getBalance() {
		return balance;
	}
	public void setBalance(String balance) {
		this.balance = balance;
	}
	
	
}

