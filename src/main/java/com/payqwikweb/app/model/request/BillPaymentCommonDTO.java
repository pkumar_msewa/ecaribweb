package com.payqwikweb.app.model.request;

public class BillPaymentCommonDTO {
	
	private String sessionId;
	private String serviceProvider;
	private String accountNumber;
	private String policyNumber;
	private String cycleNumber;
	private String cityName;
	private String amount;
	private String policyDate;
	private String stdCode;
	private String landlineNumber;
	private String dthNo;
	private String splitAmount;
	private String billingUnit;
	private String processingCycle;
	private String address;
	private String billYear;
	private String billMonth;
	private String billerTransactionId;
	private String operatorName;
	
	
	public String getOperatorName() {
		return operatorName;
	}
	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}
	public String getBillingUnit() {
		return billingUnit;
	}
	public void setBillingUnit(String billingUnit) {
		this.billingUnit = billingUnit;
	}
	public String getSplitAmount() {
		return splitAmount;
	}
	public void setSplitAmount(String splitAmount) {
		this.splitAmount = splitAmount;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public String getServiceProvider() {
		return serviceProvider;
	}
	public void setServiceProvider(String serviceProvider) {
		this.serviceProvider = serviceProvider;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getPolicyNumber() {
		return policyNumber;
	}
	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}
	public String getCycleNumber() {
		return cycleNumber;
	}
	public void setCycleNumber(String cycleNumber) {
		this.cycleNumber = cycleNumber;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getPolicyDate() {
		return policyDate;
	}
	public void setPolicyDate(String policyDate) {
		this.policyDate = policyDate;
	}
	public String getStdCode() {
		return stdCode;
	}
	public void setStdCode(String stdCode) {
		this.stdCode = stdCode;
	}
	
	public String getLandlineNumber() {
		return landlineNumber;
	}
	public void setLandlineNumber(String landlineNumber) {
		this.landlineNumber = landlineNumber;
	}
	public String getDthNo() {
		return dthNo;
	}
	public void setDthNo(String dthNo) {
		this.dthNo = dthNo;
	}
	public String getProcessingCycle() {
		return processingCycle;
	}
	public void setProcessingCycle(String processingCycle) {
		this.processingCycle = processingCycle;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getBillYear() {
		return billYear;
	}
	public void setBillYear(String billYear) {
		this.billYear = billYear;
	}
	public String getBillMonth() {
		return billMonth;
	}
	public void setBillMonth(String billMonth) {
		this.billMonth = billMonth;
	}
	public String getBillerTransactionId() {
		return billerTransactionId;
	}
	public void setBillerTransactionId(String billerTransactionId) {
		this.billerTransactionId = billerTransactionId;
	}
	

}
