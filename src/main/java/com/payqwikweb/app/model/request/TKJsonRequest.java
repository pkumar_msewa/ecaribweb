package com.payqwikweb.app.model.request;

import org.codehaus.jettison.json.JSONException;

public interface TKJsonRequest {
	public String getJsonRequest() throws JSONException, org.json.JSONException;

	public String getJsonRequest2();
}
