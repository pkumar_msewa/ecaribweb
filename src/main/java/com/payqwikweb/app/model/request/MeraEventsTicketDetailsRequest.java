package com.payqwikweb.app.model.request;

public class MeraEventsTicketDetailsRequest extends MeraEventsCommonRequest{

	private String eventId;
	private String  ticketId;
	private int allTickets;
	
	
	public String getEventId() {
		return eventId;
	}
	public void setEventId(String eventId) {
		this.eventId = eventId;
	}
	public String getTicketId() {
		return ticketId;
	}
	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}
	public int getAllTickets() {
		return allTickets;
	}
	public void setAllTickets(int allTickets) {
		this.allTickets = allTickets;
	}
	
}
