package com.payqwikweb.app.model.request;

import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartRequest;

public class GCMRequest {
    private boolean genderM;
    private boolean genderF;
    private String regTo;
    private String regFrom;
    private boolean imageGCM;
    private String title;
    private MultipartFile gcmImage;
    private String message;
    private String UserName;
    private String NotificationType;
    

   

	
	public String getNotificationType() {
		return NotificationType;
	}

	public void setNotificationType(String notificationType) {
		NotificationType = notificationType;
	}

	public String getUserName() {
		return UserName;
	}

	public void setUserName(String userName) {
		UserName = userName;
	}

	public boolean isImageGCM() {
        return imageGCM;
    }

    public void setImageGCM(boolean imageGCM) {
        this.imageGCM = imageGCM;
    }

    public boolean isGenderM() {
        return genderM;
    }

    public void setGenderM(boolean genderM) {
        this.genderM = genderM;
    }

    public boolean isGenderF() {
        return genderF;
    }

    public void setGenderF(boolean genderF) {
        this.genderF = genderF;
    }

    public String getRegTo() {
        return regTo;
    }

    public void setRegTo(String regTo) {
        this.regTo = regTo;
    }

    public String getRegFrom() {
        return regFrom;
    }

    public void setRegFrom(String regFrom) {
        this.regFrom = regFrom;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public MultipartFile getGcmImage() {
        return gcmImage;
    }

    public void setGcmImage(MultipartFile gcmImage) {
        this.gcmImage = gcmImage;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
