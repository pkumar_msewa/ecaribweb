package com.payqwikweb.app.model.request;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.JsonObject;
import com.payqwikweb.model.admin.TListDTO;

public class AdminRequest {

	private int totalUser;
	private int onlineUser;
	private int todayTransaction;
	private int monthlyTransaction;
	private int totalMale;
	private int totalFemale;
	private String name;
	private String serviceStatus;
	private JSONObject jsonObject;
	private int count;
	private JSONArray jsonArray;
	 private List<AdminUserDetails> list;
	 

	public List<AdminUserDetails> getList() {
		return list;
	}

	public void setList(List<AdminUserDetails> list) {
		this.list = list;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	public String getServiceStatus() {
		return serviceStatus;
	}

	public void setServiceStatus(String serviceStatus) {
		this.serviceStatus = serviceStatus;
	}

	public int getTotalUser() {
		return totalUser;
	}

	public void setTotalUser(int totalUser) {
		this.totalUser = totalUser;
	}

	public int getOnlineUser() {
		return onlineUser;
	}

	public void setOnlineUser(int onlineUser) {
		this.onlineUser = onlineUser;
	}

	public int getTodayTransaction() {
		return todayTransaction;
	}

	public void setTodayTransaction(int todayTransaction) {
		this.todayTransaction = todayTransaction;
	}

	public int getMonthlyTransaction() {
		return monthlyTransaction;
	}

	public void setMonthlyTransaction(int monthlyTransaction) {
		this.monthlyTransaction = monthlyTransaction;
	}

	public JSONArray getJsonArray() {
		return jsonArray;
	}

	public void setJsonArray(JSONArray jsonArray) {
		this.jsonArray = jsonArray;
	}

	public int getTotalMale() {
		return totalMale;
	}

	public void setTotalMale(int totalMale) {
		this.totalMale = totalMale;
	}

	public int getTotalFemale() {
		return totalFemale;
	}

	public void setTotalFemale(int totalFemale) {
		this.totalFemale = totalFemale;
	}

	public JSONObject getJsonObject() {
		return jsonObject;
	}

	public void setJsonObject(JSONObject jsonObject) {
		this.jsonObject = jsonObject;
	}
	
}
