package com.payqwikweb.app.model.request;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

public class MailRequest extends SessionDTO{

    @NotEmpty
    @Email(message = "Enter valid mail id")
    private String destination;

    @NotEmpty(message="Please enter content")
    private String content;

    @NotEmpty(message="Please enter subject")
    private String subject;

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }
}
