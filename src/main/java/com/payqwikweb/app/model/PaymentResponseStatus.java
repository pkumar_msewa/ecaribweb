package com.payqwikweb.app.model;

public enum PaymentResponseStatus {

	SUFFICIENT_BALANCE("T00"),INSUFFICIENT_BALANCE("T01");
	
	private final String value;
	
	private PaymentResponseStatus(String value){
		this.value=value;
	}
	
	public static PaymentResponseStatus getEnum(String value) {
		if (value == null)
			throw new IllegalArgumentException();
		for (PaymentResponseStatus v : values())
			if (value.equalsIgnoreCase(v.getValue()))
				return v;
		throw new IllegalArgumentException();
	}

	public String getValue() {
		return value;
	}
	
	@Override
	public String toString() {
		return value;
	}
}
