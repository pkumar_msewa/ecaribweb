package com.payqwikweb.app.model.merchant;

public class MerchantKycDTO {

    private boolean vbankCustomer;
    private String accountNumber;
    private String mobileNumber;
    private String otp;


    public boolean isVbankCustomer() {
        return vbankCustomer;
    }

    public void setVbankCustomer(boolean vbankCustomer) {
        this.vbankCustomer = vbankCustomer;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }
}
