package com.payqwikweb.app.model.merchant;

import com.thirdparty.model.JSONWrapper;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

public class MerchantRegisterDTO implements JSONWrapper {

    private String firstName;

    private String lastName;

    private String email;

    private String mobileNumber;

    private String password;

    private String confirmPassword;

    private MerchantAddressDTO addressDTO;

    private MerchantBankDTO merchantBankDTO;

    private SettlementBankDTO settlementBankDTO;

    private MerchantUID uidDTO;

    private MerchantKycDTO kycDTO;


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public MerchantAddressDTO getAddressDTO() {
        return addressDTO;
    }

    public void setAddressDTO(MerchantAddressDTO addressDTO) {
        this.addressDTO = addressDTO;
    }

    public MerchantBankDTO getMerchantBankDTO() {
        return merchantBankDTO;
    }

    public void setMerchantBankDTO(MerchantBankDTO merchantBankDTO) {
        this.merchantBankDTO = merchantBankDTO;
    }

    public SettlementBankDTO getSettlementBankDTO() {
        return settlementBankDTO;
    }

    public void setSettlementBankDTO(SettlementBankDTO settlementBankDTO) {
        this.settlementBankDTO = settlementBankDTO;
    }

    public MerchantUID getUidDTO() {
        return uidDTO;
    }

    public void setUidDTO(MerchantUID uidDTO) {
        this.uidDTO = uidDTO;
    }

    public MerchantKycDTO getKycDTO() {
        return kycDTO;
    }

    public void setKycDTO(MerchantKycDTO kycDTO) {
        this.kycDTO = kycDTO;
    }

    @Override
    public JSONObject toJSON() {
        JSONObject payload = new JSONObject();
        try {
            payload.put("firstName",getFirstName());
            payload.put("lastName",getLastName());
            payload.put("email",getEmail());
            payload.put("mobileNumber",getMobileNumber());
            payload.put("password",getPassword());
            payload.put("confirmPassword",getConfirmPassword());
            MerchantAddressDTO address = getAddressDTO();
            if(address != null) {
                JSONObject addressDTO = new JSONObject();
                addressDTO.put("address", address.getAddress());
                addressDTO.put("city",address.getCity());
                addressDTO.put("state",address.getState());
                addressDTO.put("country",address.getCountry());
                addressDTO.put("pinCode",address.getPinCode());
                payload.put("addressDTO",addressDTO);
            }
            MerchantBankDTO bank = getMerchantBankDTO();
            if(bank != null) {
                JSONObject bankDTO = new JSONObject();
                bankDTO.put("accountName",bank.getAccountName());
                bankDTO.put("bankName",bank.getBankName());
                bankDTO.put("accountNumber",bank.getAccountNumber());
                bankDTO.put("ifscCode",bank.getIfscCode());
                bankDTO.put("location",bank.getLocation());
                payload.put("merchantBankDTO",bankDTO);
            }

            SettlementBankDTO settlementBank = getSettlementBankDTO();
            if(bank != null) {
                JSONObject settlementBankDTO = new JSONObject();
                settlementBankDTO.put("accountName",settlementBank.getAccountName());
                settlementBankDTO.put("bankName",settlementBank.getBankName());
                settlementBankDTO.put("accountNumber",settlementBank.getAccountNumber());
                settlementBankDTO.put("ifscCode",settlementBank.getIfscCode());
                settlementBankDTO.put("location",settlementBank.getLocation());
                payload.put("settlementBankDTO",settlementBankDTO);
            }

            MerchantUID uid = getUidDTO();
            if(uid != null){
                JSONObject uidDTO = new JSONObject();
                uidDTO.put("aadhaarNumber",uid.getAadhaarNumber());
                uidDTO.put("panNumber",uid.getPanNumber());
                payload.put("uidDTO",uidDTO);
            }
            MerchantKycDTO kyc = getKycDTO();
            if(kyc != null){
                JSONObject kycDTO = new JSONObject();
                kycDTO.put("vbankCustomer",kyc.isVbankCustomer());
                kycDTO.put("accountNumber",kyc.getAccountNumber());
                kycDTO.put("mobileNumber",kyc.getMobileNumber());
                kycDTO.put("otp",kyc.getOtp());
                payload.put("kycDTO",kycDTO);
            }
            System.err.println("payload::::::"+payload);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return payload;
    }
}
