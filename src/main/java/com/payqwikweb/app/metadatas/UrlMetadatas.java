package com.payqwikweb.app.metadatas;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Language;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.Version;

public class UrlMetadatas {

	protected final static Logger logger = LoggerFactory.getLogger(UrlMetadatas.class);

	// For test server please uncomment below two lines
	
	public static final String WEBURL = "http://localhost:8082";
	public static final String APPURL = "http://localhost:8081";
//	public static final String WEBURL = "http://23.101.178.130";
//  public static final String APPURL = "http://23.101.178.130"; 
	public static final boolean PRODUCTION = false;
	public static final String HOST = APPURL+"/ecaribapp";
	public static final String DOMAIN = HOST+"/Api"; 

	private static final String GET_VPAYQWIK_MAIN_COUNT="VpayQwikProjectInfo/ListCount";
	private static final String GET_ALL_USER_FROM_DB = "VpayQwikProjectInfo/userTransactions";
	private static final String GET_DEBIT_LIST_FROM_DB = "VpayQwikProjectInfo/DebitTransactions";
	private static final String GET_CREDIT_LIST_FROM_DB = "VpayQwikProjectInfo/CreditTransactions";
	private static final String GET_SERVICE_LIST_FROM_DB = "VpayQwikProjectInfo/ServiceList";
	private static final String GET_DATE_WISE_TRANS = "VpayQwikProjectInfo/DateWiseTrans";
	private static final String ADD_SUPERADMIN_LIST_PAYQWIK="VpayQwikProjectInfo/addSuperAdmin";
	private static final String GET_SUPERADMIN_LIST="VpayQwikProjectInfo/SuperAdminList";
	private static final String CHANGE_PASSWORD_SUPERADMIN="VpayQwikProjectInfo/ChPwdSuperAdmin";
	private static final String BLOCK_SUPERADMIN_PAYQWIK="VpayQwikProjectInfo/BlockSuperAdmin";
	// For Live server please uncommet below two lines

//	  public static final boolean PRODUCTION = true;
//	  public static final String BASE = "http://172.16.3.10";
//	  public static final String HOST = BASE+"";
//	  public static final String DOMAIN = HOST+"/Api";
//	  public static final String WEBURL = "https://www.vpayqwik.com";

	public static final String MDEX_CLIENTKEY = getMdexClientKey();
	public static final String MDEX_CLIENTTOKEN = getMdexClientToken();
	private static final String MDEX_TRAVELHOST = getMdexTravelHost();

	public static final String MDEX_CLIENTKEY_TEST = "5CF67A47D71A60F6759DDDC44C41D36E3B1E0D75";
	public static final String MDEX_CLIENTTOKEN_TEST = "MTUwMTA5MzU3NjIxNzM1NjAwMDAwMDAwMDAwMDU=";
	private static final String MDEX_TRAVELHOST_TEST = "http://demomdex.msewa.com";

	public static final String MDEX_CLIENTKEY_LIVE = "DDB6226DAFD21DB2FD8A8AE569A9B282D1731087";
	public static final String MDEX_CLIENTTOKEN_LIVE = "MTUwMzkyMjc3OTQ2MzM1NjAwMDAwMDAwMDAwMDY=";
	private static final String MDEX_TRAVELHOST_LIVE = "https://mdex.msewa.com";
	
	//IPL URL

	private static final String GET_USERPROFILE="VpayQwikProjectInfo/UserProfile";
	private static final String  GET_USERBLOCK="VpayQwikProjectInfo/blockUser";
	private static final String GET_USERUNBLOCK="VpayQwikProjectInfo/unBlockUser";
	private static final String  GET_USERKYC="VpayQwikProjectInfo/kycUser";
	private static final String GET_USERNONKYC="VpayQwikProjectInfo/nonKycUser";
	private static final String SEND_SUPERADMIN_MAIL="VpayQwikProjectInfo/SendMail";
	private static final String SEND_SUPERADMIN_BULKMAIL="VpayQwikProjectInfo/SendBulkMail";
	private static final String GET_USERLIST="VpayQwikProjectInfo/GetUserList";
	private static final String GET_USERLIST_FILTER="VpayQwikProjectInfo/GetUserListFilter";
	private static final String GET_GCM_NOTIFICATIONS="VpayQwikProjectInfo/GCMNotification";
	private static final String SEND_SUPERADMIN_SMS="VpayQwikProjectInfo/SendSms";
	private static final String SEND_SUPERADMIN_BULKSMS="VpayQwikProjectInfo/SendBulkSms";
	private static final String SAVE_AADHAR_DB="SaveAadhar";

	public static String getMdexTravelHost() {
		if (UrlMetadatas.PRODUCTION) {
			return MDEX_TRAVELHOST_LIVE;
		} else {
			return MDEX_TRAVELHOST_TEST;
		}
	}

	public static String getMdexClientToken() {
		if (UrlMetadatas.PRODUCTION) {
			return MDEX_CLIENTTOKEN_LIVE;
		} else {
			return MDEX_CLIENTTOKEN_TEST;
		}
	}

	public static String getMdexClientKey() {
		if (UrlMetadatas.PRODUCTION) {
			return MDEX_CLIENTKEY_LIVE;
		} else {
			return MDEX_CLIENTKEY_TEST;
		}
	}

	private static final String TravelDOMAIN = MDEX_TRAVELHOST + "/Api";
	private static final String DECRYPT_RESPONSE = "Merchant/decryptresponse";
	private static final String SEPARATOR = "/";
	private static final String ADD_MERCHANT = "Merchant/Save";
	private static final String ADD_OFFERS = "Offers/Save";
	// donatee
	private static final String ADD_DONATEE = "Donatee/Save";
	private static final String SETTLEMENT_REPORTS = "getReports";
	private static final String LOGIN = "Login";
	private static final String ONE_USER = "findByMobile";
	// private static final String LOGIN = "GetInfo/Basic";
	private static final String FORGET_PASSWORD = "ForgotPassword";
	private static final String CHANGE_PASSWORD_WITH_OTP = "RenewPassword";
	private static final String CHANGE_PASSWORD_WITH_Mob = "Agent/RenewPassword";
	private static final String RESET_PASSWORD_WITH_OTP = "ResetPasswordWithOtp";
	private static final String DEVICE_BINDING_WITH_OTP = "/Resend/DeviceBinding/OTP";
	private static final String ALL_TRANSACTION = "getTotalTransactions";
	private static final String ALL_TRANSACTION_FILTERED = "getTotalTransactionsFiltered";
	private static final String IMAGICA_TRANSACTION_FILTERED = "getImagicaTransactionsFiltered";
	private static final String DEBIT_TRANSACTION_FILTERED = "getDebitTransactionsFiltered";
	private static final String CREDIT_TRANSACTION_FILTERED = "getCreditTransactionsFiltered";
	private static final String NEWNIKKI = "findByDate1";
	private static final String ALL_USER = "getTotalUsers";
	private static final String UPI_TXN = "getUpiTxn";
	private static final String GCM_LIST= "gcmList";
	private static final String USER_TRANSACTION = "getUserTransactions";
	private static final String MERCHANT_TRANSACTIONS = "Merchant/GetTransactions";
	private static final String VPAYQWIK_MERCHANT_TRANSACTIONS = "Merchant/GetLast20VPayQwikTransactions";
	private static final String VPAYQWIK_MERCHANT_RECEIPTS = "Merchant/GetVPayQwikMerchantTransactions";
	private static final String BESCOM_MERCHANT_RECEIPTS = "Merchant/GetBescomMerchantTxns";
	private static final String BESCOM_CUST_DETAILS = "Authenticate/FetchCustDetails";
	private static final String BESCOM_MERCHANT_REFUND_REQUEST = "Authenticate/BesomMerchantRefundRequest";
	
	private static final String VPAYQWIK_MERCHANT_TRANSACTIONS_FILTERED = "Merchant/GetVPayQwikTransactionsFiltered";
	private static final String VPAYQWIK_MERCHANT_TRANSACTIONS_DEBIT = "Merchant/GetDebitVPayQwikTransactions";
	private static final String VPAYQWIK_MERCHANT_TRANSACTIONS_COMMISSION = "Merchant/GetVPayQwikMerchantCommision";
	private static final String EMAIL_LOG = "Report/Email";
	private static final String MESSAGE_LOG = "Report/SMS";
	private static final String BLOCK_USER = "BlockUser";
	private static final String UNBLOCK_USER = "UnblockUser";
	private static final String LOGOUT = "Logout";
	private static final String PREPAID_TOPUP = "MobileTopup/ProcessPrepaid";
	private static final String REGISTER = "Register";
	private static final String MOBILE_OTP = "Activate/Mobile";
	private static final String RESEND_MOBILE_OTP = "Resend/Mobile/OTP";
	private static final String RESEND_KYC_OTP = "Resend/KYC/OTP";
	private static final String VERIFY_EMAIL = "Activate/Email";
	private static final String BETWEEN_DATES = "Report/Transaction";
	private static final String ADMIN_DASHBOARD = "adminDashBoard";
	private static final String UPDATE_PASSWORD = "UpdatePassword/Process";
	private static final String USER_DETAILS = "GetUserDetails";
	private static final String EDIT_PROFILE = "EditProfile/Process";
	private static final String CHANGE_PASSWORD = "ChangePassword/Process";
	private static final String UPLOAD_PICTURE = "UploadPicture/Process";
	private static final String RECEIPTS = "GetReceipts";
	private static final String RECEIPTS_FILTER = "GetReceiptsFilter";
	private static final String SUCCESSFUL_TRANSACTIONS = "STransactions";
	private static final String SET_MPIN = "SetMpin";
	private static final String CHANGE_MPIN = "ChangeMpin";
	private static final String DELETE_MPIN = "ForgotMpin";
	private static final String VERIFY_MPIN = "VerifyMpin";
	private static final String ELECTRICITY_BILL = "BillPay/ProcessElectricity";
	private static final String GAS_BILL = "BillPay/ProcessGas";
	private static final String GET_SERVICE_TYPES = "GetServiceTypes";
	private static final String INSURANCE_BILL = "BillPay/ProcessInsurance";
	private static final String LANDLINE_BILL = "BillPay/ProcessLandline";
	private static final String DTH_BILL = "BillPay/ProcessDTH";
	private static final String OPERATOR_AND_CIRCLE = "Plans/GetOperatorsCircles";
	private static final String FORGOT_MPIN = "ForgotMpin";
	private static final String OPERATOR_AND_CIRCLE_FOR_MOB = "Plans/GetTelco";
	private static final String PLANS_FOR_MOBILE = "Plans/GetPlans";
	private static final String PAY_AT_STORE = "PayAtStore/Process";
	private static final String LOAD_MONEY = "LoadMoney/Process";
	private static final String LOAD_MONEY_WITH_VOUCHER = "LoadMoney/Voucher/Process";
	private static final String LOAD_MONEY_RAZORPAY = "LoadMoney/RazorPayProcess";
	private static final String LOAD_MONEY_RAZORPAY_SUCCESS = "LoadMoney/RazorPaySuccess";
	private static final String LOAD_MONEY_FAILED = "LoadMoney/FailedUpiTxns";
	
	private static final String GET_UPI_LOAD_MONEY = "LoadMoney/ProcessUPI";
	private static final String LOAD_MONEY_REDIRECT = "LoadMoney/Redirect";
	private static final String LOAD_MONEY_VNET = "LoadMoney/InitiateVNet";
	private static final String LOAD_MONEY_REDIRECT_VNET = "LoadMoney/RedirectVNet"; 
	private static final String LOAD_MONEY_REDIRECT_UPI = "LoadMoney/RedirectUPI";
	private static final String LOAD_MONEY_SDK_REDIRECT_UPI = "LoadMoney/SDKRedirectUPI";
	private static final String LOAD_MONEY_STATUS_AFTER_REDIRECT = "LoadMoney/UpiRedirectStatus";
	private static final String SEND_MONEY_MOBILE = "SendMoney/Mobile";
	private static final String BANK_INITIATE = "SendMoney/Bank/Initiate";
	private static final String BANK_SUCCESS = "SendMoney/Bank/Success";
	private static final String BANK_FAILURE = "SendMoney/Bank/Failure";
	private static final String MBANK_INITIATE = "SendMoney/MBankTransfer/Initiate";
	private static final String ABANK_INITIATE = "SendMoney/ABankTransfer/Initiate";
	// private static final String BANK_SUCCESS = "SendMoney/Bank/Success";
	// private static final String BANK_FAILURE = "SendMoney/Bank/Failure";
	private static final String LIST_STORE_API = "ListStoreApi";
	private static final String AUTHORITY = "Authenticate/SessionId";
	private static final String TRANSACTION_VALIDATION = "Validate/Transaction";
	private static final String VALIDATE_MERCHANT_TRX = "Validate/MTransaction";

	// private static final String INVITE_MOBILE_FRIEND = "Invite/Mobile";
	private static final String INVITE_MOBILE_FRIEND = "InviteFriends/ProcessByMobile";
	private static final String INVITE_EMAIL_FRIEND = "Invite/Email";
	private static final String USER_BY_ADMIN = "getTransactions";
	private static final String RESEND_EMAIL_OTP = "ReSendEmailOTP";
	private static final String CALL_BACK = "InstantPay/Callback";
	private static final String SAVE_PROMO_CODE = "PromoCode/Save";
	private static final String EDIT_PROMO_CODE = "PromoCode/Edit";
	private static final String LIST_PROMO_CODE = "PromoCode/List";
	private static final String REDEEM_PROMO = "RedeemProcess";
	private static final String KYC_REQUEST = "AccountUpdateRequest";
	private static final String KYC_OTP_VERIFICATION = "AccountUpdate";
	// private static final String KYC_REQUEST_WEB = "AccountUpdateRequestWeb";
	private static final String KYC_WEB_OTP_VERIFICATION = "AccountUpdateWeb";
	private static final String SINGLE_USER = "getTransactions";
	private static final String VALIDATE_VERSION = "AuthenticateVersion";
	private static final String UPDATE_VERSION = "UpdateVersion";
	private static final String LIST_VERSION = "Version/All";
	private static final String UPDATE_FAVOURITE = "UpdateFavourite";
	private static final String GET_SERVICES = "getServices";
	private static final String GET_TRANSACTION_DTO = "GetTransactionDTO";
	private static final String GET_ALL_BANKS = "listBanks";
	private static final String IFSC_BY_BANK = "listIFSC";
	private static final String OFFLINE_PAYMENT = "Merchant/RequestOffline";
	private static final String BASIC_DETAILS = "GetInfo/Basic";
	private static final String ACCOUNT_DETAILS = "GetInfo/Account";
	private static final String LOCATION_DETAILS = "GetInfo/Location";
	private static final String IMAGE_DETAILS = "GetInfo/Image";
	private static final String BALANCE_DETAILS = "GetInfo/Balance";
	private static final String LIMITS_DETAILS = "GetInfo/Limits";
	private static final String OFFLINE_OTP = "Merchant/ProcessOffline";
	public static String COUPONS = "http://localhost:8080/GetOffers/coupons";
	private static String CARROTFRY_COUPONS = "http://localhost:8080/CouponDist/Coupons/send";
	private static String NEFT_LIST = "BankTransferList";
	private static String VALUES_LIST = "GetValues";
	private static String MERCHANT_VALUES_LIST = "Authenticate/GetValues";
	
	private static String SHARE_POINTS = "SharePoints";
	private static String SERVICE_URL = "GetAllServices";
	private static String SERVICES_BY_ID = "GetAllServicesById";
	private static String GET_MERCHANTS = "getMerchants";
	private static String REFUND_AMOUNT = "RefundAmount";
	private static String PROMO_TRANSACTIONS = "PCTransactions";
	private static String PROMO_TRANSACTIONS_FILTERED = "PCTransactionsFiltered";
	private static String MERCHANT_CHPASSWORD = "Merchant/ChangePassword";
	private static String ADMIN_ALLTRANSACTIONS = "getTotalTransactionsByType";
	private static String MERCHANT_LIST = "Merchant/All";
	private static String REFUND_STATUS = "LoadMoney/RefundStatus";

	private static String PROMO_TRANSACTIONS_FILTERED_SUPERADMIN = "SuperAdmin/PCTransactionsFiltered";

	// visa
	private static final String LIST_VISA_MERCHANT = "listVisaMerchant";

	private static final String LIST_VISA_MERCHANT_FILTER = "listVisaMerchantFilter";
	// Travel Api URL:
	private static final String BUS_BOOKING = "SaveBookingDetails";
	private static final String BUS_BOOKING_RESPONSE = "BookBusResponse";
	private static String MERCHANT_NEFT_LIST = "/MBankTransferList";
	private static String AGENT_NEFT_LIST = "/ABankTransferList";
	private static String MERCHANT_TXLIST = "getMTransactions";
	// Events Api URL
	private static String SAVE_TOKEN = "saveAccessToken";
	private static String SAVE_EVENT_DETAILS = "SaveEventDetails";
	// VISA
	private static final String INITATED_REQUEST = "Mvisa/VisaProcess";
	private static final String GET_ACCOUNT_REQUEST = "Mvisa/AccountNumberRequest";
	private static final String GET_DECRYPT_REQUEST = "Mvisa/AccountNumberDecrypt";
	private static final String INITATED_RESPONSE = "Mvisa/ResponseProcess";
	// IPL
	// change password for Admin
	private static String ADMIN_CHANGEPASSWORD = "ChangePassword";

	private static final String SINGLE_TRANSACTION_POST = "getTransactions";
	private static String EVENT_PAYMENT = "initiateEventPayment";
	private static final String ADD_VISA_MERCHANT = "VisaMerchant";
	private static final String CHECK_VISA_MERCHANT = "CheckExistingVisaMerchant";
	// merchant signup
	private static final String GET_ENTITY_M2P = "Merchant/GetEntityId";
	private static final String MERCHANT_SIGNUP = "Merchant/SignUp";
	private static final String MERCHANT_ADD_BANK = "Merchant/addBankDetails";
	private static final String MERCHANT_MOBILE_OTP = "Merchant/ActivateMerchantMobile";
	private static final String RESEND_MERCHANT_MOBILE_OTP = "Merchant/Resend/MobileOtp";
	private static final String CHECK_EXISTING_MERCHANT = "Merchant/checkExistingMerchant";
	private static final String SIGNUP_VISA_MERCHANT = "Merchant/signUpVisaMerchant";
	private static final String SECURITY_QUESTIONS = "GetSecurityQuestions";
	// Super Admin URL
	private static final String SUPERADMIN_BETWEEN_DATES = "SuperAdmin/Report/Transaction";
	private static final String VALUES_LIST_SUPERADMIN = "SuperAdmin/GetValues";
	private static final String GET_SERVICE_TYPES_SUPERADMIN = "SuperAdmin/GetServiceTypes";
	private static final String GET_OPERATOR_SUPERADMIN = "SuperAdmin/GetOperatorList";
	private static final String SERVICE_URL_SUPERADMIN = "SuperAdmin/GetAllServices";
	private static final String ALL_TRANSACTION_SUPERADMIN = "SuperAdmin/getTotalTransactions";
	private static final String SETTLEMENT_REPORTS_SUPERADMIN = "SuperAdmin/GetReports";
	private static final String GET_MERCHANTS_SUPERADMIN = "getMerchants";
	private static final String REFUND_AMOUNT_SUPERADMIN = "SuperAdmin/RefundAmount";
	private static final String PROMO_TRANSACTIONS_SUPERADMIN = "SuperAdmin/PCTransactions";
	private static final String ALL_USER_SUPERADMIN = "SuperAdmin/GetTotalUser";
	private static final String MERCHANT_LIST_SUPERADMIN = "SuperAdmin/Merchant/All";
	private static final String USER_TRANSACTION_SUPERADMIN = "SuperAdmin/getUserTransactions";
	private static final String USER_BY_SUPERADMIN = "SuperAdmin/getTransactions";
	private static final String ADD_VISA_MERCHANT_SUPERADMIN = "SuperAdmin/VisaMerchant";
	private static final String CHECK_VISA_MERCHANT_SUPERADMIN = "SuperAdmin/CheckExistingVisaMerchant";
	private static final String EMAIL_LOG_SUPERADMIN = "SuperAdmin/Report/Email";
	private static final String MESSAGE_LOG_SUPERADMIN = "SuperAdmin/Report/SMS";
	private static final String BLOCK_USER_SUPERADMIN = "SuperAdmin/BlockUser";
	private static final String UNBLOCK_USER_SUPERADMIN = "SuperAdmin/UnblockUser";
	private static final String SUPERADMIN_ALLTRANSACTIONS = "SuperAdmin/getTotalTransactionsByType";
	private static final String SUPERADMIN_ADD_MERCHANT = "SuperAdmin/Merchant/Save";
	private static final String SUPERADMIN_NEFT_LIST = "SuperAdmin/BankTransferList";
	private static final String KYC_UPDATE = "SuperAdmin/GetKycUpdate";
	private static final String SUPERADMIN_ACCESS_LIST = "SuperAdmin/GetAccessList";
	private static final String SUPERADMIN_ACCESS_USERS = "SuperAdmin/GetAccessUsers";
	private static final String NON_KYC_UPDATE = "SuperAdmin/GetNonKycUpdate";
	private static final String SUPERADMIN_NEFT_LIST_FILTER = "SuperAdmin/BankTransferListFilter";

	private static final String SUPERADMIN_MERCHANT_NEFT_LIST = "SuperAdmin/MBankTransferList";
	private static final String SUPERADMIN_MERCHANT_TXLIST = "SuperAdmin/getMTransactions";
	private static final String SWITCH_STATUS = "SuperAdmin/GetServiceStatus";
	private static final String FP_STATUS_ACTIVE = "SuperAdmin/ServiceSwitchActive";
	private static final String FP_STATUS_INACTIVE = "SuperAdmin/ServiceSwitchInactive";
	private static final String SAVE_PROMO_CODE_SUPERADMIN = "SuperAdmin/PromoCode/Save";
	private static final String LIST_PROMO_CODE_SUPERADMIN = "SuperAdmin/PromoCode/List";

	private static final String USERLIST_BYTYPE_SUPERADMIN = "SuperAdmin/GetUsers/Now";
	private static final String USERLIST_BYTYPE_FILTER_SUPERADMIN = "SuperAdmin/GetUsers/Filter";

	private static final String TRANSACTION_LIST_SUPERADMIN = "SuperAdmin/GetTransaction/Now";
	private static final String TRANSACTION_LIST_FILTER_SUPERADMIN = "SuperAdmin/GetTransaction/Filter";
	private static final String SERVICES_BY_ID_SUPERADMIN = "SuperAdmin/GetAllServicesById";
	private static final String GET_COMMISSION = "SuperAdmin/getCommission";
	private static final String UPDATE_COMMISSION = "SuperAdmin/updateCommission";

	private static final String LOGS_NOW = "SuperAdmin/GetLogs/Now/";

	private static final String GET_ALL_SERVICES = "SuperAdmin/GetServices";
	
	private static final String GET_ALL_AADHAR_DETAILS = "SuperAdmin/GetAadharDetails";
	
	
	private static final String DATA_CONFIG_LIST= "SuperAdmin/dataConfigList";

	private static final String UPDATE_SERVICES = "SuperAdmin/UpdateService";
	
	private static final String UPDATE_MDEX = "SuperAdmin/UpdateMdexSwitch";

	private static final String SUPERADMIN_GETPOSSIBLITIES = "SuperAdmin/GetPossibilities";

	private static final String LOGS_FILTER = "SuperAdmin/GetLogs/Filter/";

	private static final String SUPERADMIN_FCM_ID = "SuperAdmin/GetGCMIDs";

	private static final String SUPERADMIN_SAVE_NOTIFICATION= "SuperAdmin/saveGCMIDs";

	private static final String SUPERADMIN_SMS = "SuperAdmin/SendSMS";

	private static final String SUPERADMIN_CHANGE_PASSWORD = "SuperAdmin/ChangePassword";

	private static final String LIST_ACCOUNTS_SUPERADMIN = "SuperAdmin/ListAccountTypes";
	private static final String UPDATE_ACCOUNTS_SUPERADMIN = "SuperAdmin/UpdateAccountType";
	private static final String SUPERADMIN_BULK_SMS = "SuperAdmin/SendBulkSMS";
	private static final String SUPERADMIN_EMAIL = "SuperAdmin/SendMail";
	private static final String SUPERADMIN_BULK_EMAIL = "SuperAdmin/SendBulkMail";
	private static final String SUPERADMIN_USER_INFO = "SuperAdmin/GetUserInfo";

	private static final String TRANSACTIONREPORT = "findByDate";
	private static final String TRANSACTIONREPORT_BY_PAGE = "DailyTransactionByPagination";
	private static final String WOOHOO_TRANSACTIONREPORT = "findWooHooTransactions";
	private static final String RECON_REPORT = "getReconcilingTxn";
	private static final String USERLIST_REPORT = "findUserByDate";
	private static final String FINDUSERBYMOBILE = "findByMobile";
	private static final String GIFT_CART = "GiftCart";
	private static final String CART_SHOW = "GiftCartshow";
	private static final String ITEM_REMOVE = "GiftCartDelet";
	private static final String PAYMENT_INITATE = "GiftCartOrderInitiate";
	private static final String PAYMENT_SUCCESS = "GiftCartOrderSuccess";
	private static final String LIST_ACCOUNT_TYPE = "ListAccountTypes";
	private static final String UPDATE_ACCOUNT_TYPE = "UpdateAccountType";
	/* Add Agent Url */
	private static final String ADD_AGENT = "AgentRegister";
	private static final String Get_City_State = "Agent/GetCityAndState";
	private static final String Get_Bank_List = "Agent/GetBankList";
	private static final String MobileOtp_AGENT = "Activate/AgentMobile";

	private static final String ADD_User_AGENT = "RegisterUsertoAgent";

	private static final String MobileOtp_User_AGENT = "Activate/AgentUserMobile";
	
	private static final String Resend_Mobile_OTP = "Agent/ResendOTP";
	private static final String SEND_MONEY_AGENT_MOBILE = "SendMoney/AgentMobile";

	private static final String SEND_MONEY_REQUEST_AGENT = "SendMoney/RequestAgentMoney";

	private static final String AllCreditlistAgent = "GetAllAgentCreditList";

	private static final String FORGET_AGENT_PASSWORD = "AgentForgotPassword";

	private static final String RESET_AGENT_PASSWORD = "AgentRenewPassword";

	private static final String AGNET_RESEND_MOBILE_OTP = "AgentResend/Mobile/OTP";

	// tx_id clickable
	private static final String TRANSACTIONID_CLICKABLE = "getUserFromTransactionId";
	private static final String NEFT_LIST_FILTERED = "BankTransferFilteredList";
	private static final String MERCHANT_NEFT_LIST_FILTERED = "MerchantBankTransferFilteredList";
	private static final String AGENT_NEFT_LIST_FILTERED = "AgentBankTransferFilteredList";
	
	private static final String MERCHANT_VERIFY_KYC = "MRegister/VerifyKYCRequest";
	private static final String MERCHANT_VERIFY_KYC_OTP = "MRegister/ValidateOTP";
	private static final String MERCHANT_GENERATE_PASSWORD = "MRegister/GeneratePassword";
	private static final String UPDATE_ACCESS = "SuperAdmin/UpdateAccess";
	private static final String EVENT_PAYMENT_INITATE = "initatePayment";

	// Savaari details

	private static final String SAVE_SAVAARI_TOKEN = "AccessToken";
	private static final String GET_SAVAARI_TOKEN = "Authenticate/Token";
	private static final String SAVE_SAVAARI_DETAILS = "SaveSavaariDetail";
	private static final String SUCCESS_SAVAARI_DETAILS = "BookSavaariResponse";
	private static final String INITIATE_CANCELLATION = "InitiateCancelation";
	private static final String CANCELL_TICKET = "cancelTicket";
	private static final String TICKET_DETAILS = "GetTicketDetails";

	private static final String ADMIN_FCM_ID = "GetGCMIDs";
	private static final String SEND_MONEY_DONATEE = "SendMoney/Donatee";
	private static final String AGENT_LIST = "Agent/All";
	private static final String FlightsDetailURL = "Flight/GetFlightDetailsForAdmin";

	// nikki transaction

	private static final String NIKKI_TOKEN = "GetNikkiToken";
	private static final String NIKKI_INTINATE_TRANSACTION = "IntinateNikkiTransaction";
	private static final String NIKKI_UPDATE_TRANSACTION = "UpdateNikkiTransaction";
	private static final String NIKKI_REFUND_TRANSACTION = "RefundNikkiTransaction";
	private static final String GET_SERVICE_TYPES_NEW = "SuperAdmin/ServiceTypes";
	private static final String GET_TRANSACTIONS_BY_SERVICE = "SuperAdmin/Transactions/ByService";
	private static final String GET_TRANSACTIONS_BY_SERVICE_BILLPAY = "SuperAdmin/Transactions/BillPayment";
	private static final String GET_STATUS_BY_TRANSACTION = "/Merchant/getTransactionStatus";
	private static final String SUPERADMIN_REFUND = "SuperAdmin/Refund/Transaction";
	private static final String SUPERADMIN_REFUND_BILLPAY = "SuperAdmin/Refund/BillPayTransaction";
	private static final String GET_REFUND_TRANSACTION = "SuperAdmin/getRefundMerchantRequest";
	private static final String SUPERADMIN_MERCHANT_REFUND = "SuperAdmin/RefundMerchantTransaction";

	// For Adlabs App Part URL Start

	private static final String PAYMENT_INITATE_ADLABS = "AdlabsOrderInitiate";
	private static final String PAYMENT_SUCCESS_ADLABS = "AdlabsOrderSuccess";

	private static final String PARTNER_DEVICE_REGISTRATION = "ws/api/partner/registerDevice";
	// For Adlabs App Part URL END

	private static final String GET_ANALYTICS = "GetAnalytics";

	private static final String GET_TRX_TIME = "LoadMoney/getTransactionTimeDiff";

	private static final String GET_UPDATE_RECEIPT = "FilteredBalanceOfUser";

	private static final String GET_UPDATE_RECEIPT_DATEWISE = "FilteredBalanceOfUserDateWise";

	private static final String GET_CREDIT_AND_DEBIT_RECEIPT = "TotalDebitAndCreditOfUser";

	private static final String GET_NOTIFICATIONS = "GetNotifications";

	private static final String USER_ANALYTICS = "GetUserAnalytics";

	// YUPTV URL

	private static final String SAVE_YUTPV_KEY_SECRET = "SaveRegister";

	private static final String CHECK_YUPTV_REGISTER = "CheckRegister";

	private static final String CHECK_EXIST_YUPTV_REGISTER = "ExistRegister";

	private static final String NEW_REGISTER = "URegisterUser";
	private static final String QWIKR_PAY_INITIATE = "QwikrPayOrderInitiate";
	private static final String QWIKR_PAY_SUCCESS = "QwikrPayOrderSuccess";

	// Imagica URL
	private static final String IMAGICA_AUTH = "Imagica/GetAuth";
	private static final String IMAGICA_PLACE_ORDER = "Imagica/PlaceOrder";
	private static final String IMAGICA_PAYMENT_PROCESS = "Imagica/ProcessPayment";
	private static final String IMAGICA_GET_LOCATION = "Imagica/LocationInfo";

	private static final String PARTNER_INSTALL_REPORTS = "getPartnerInstallReports";

	private static final String PARTNER_INSTALL_REPORTS_FILTER = "getPartnerInstallReportsFilter";

	private static final String NIKKI_TRANSACTION_FILTERED = "getNikkiTransactionsFiltered";

	public static String registerPartnerDevice() {
		String url = HOST + SEPARATOR + PARTNER_DEVICE_REGISTRATION;
		 
		return url;
	}

	private static final String EDIT_NAME = "EditName/Process";

	// versionCheck
	private static final String VERSION_CHECK = "CheckVersion";

	private static final String UPDATE_VERSIONS = "SuperAdmin/Update/Versions";

	private static final String GET_ALL_VERSIONS = "SuperAdmin/All/Versions";

	// Gci URL

	private static final String GCI_AUTH = "Gci/GetAccessToken";
	private static final String GCI_PAYMENT_INITATE = "Gci/ProcessGiftCard";

	// Travel Flight Controller URL

	private static final String Flight_SOURCE = "Flight/AirlineNameRQ";
	private static final String Flight_Search_API = "Flight/FlightSearch";
	private static final String Flight_AirRePriceRQ_API = "Flight/AirRePriceRQ";
	private static final String Flight_AirBookRQ_API = "Flight/AirBookRQ";
	private static final String LOAD_MONEY_Splitpayment = "Flight/ProcessFlightEBS";
	private static final String LOAD_MONEY_REDIRECT_Splitpayment = "Flight/RedirectFlightEBS";
	private static final String FLIGHT_ISCANCELABLE_TICKET = "Flight/FlightBookingDetail";

	// Travel Bus Controller URL

	private static final String BUS_GET_ALL_CITY_LIST = "Bus/GetAllCityList";
	private static final String BUS_GET_SOURCE_CITY_LIST = "Bus/GetSourceCityBySourceKey";
	private static final String BUS_GET_DESTINATION_CITY_LIST = "Bus/GetDestinationCity";
	private static final String BUS_GET_ALL_AVAILABLE_TRIPS = "Bus/GetAllAvailableTrips";
	private static final String BUS_GET_SEAT_DETAILS = "Bus/GetSeatDetails";
	private static final String BUS_GET_TXN_ID = "Bus/GetTransactionId";

	private static final String BUS_GET_BOOK_TICKET = "Bus/BookTicket";
	private static final String BUS_GET_ISCANCELABLE_TICKET = "Bus/IsCancellable";
	private static final String BUS_GET_CANCEL_TICKET = "Bus/CancelTicket";
	private static final String NEAR_BY_MERCHNAT_DETAILS = "/GetNearByMerchants";
	private static final String Flight_FlightBookingInitiate_API = "Flight/FlightBookingInitiate";
	private static final String Flight_FlightBookingSucees_API = "Flight/ProcessPayment";
	private static final String FLIGHT_PAYMENTGATEWAY_API = "Flight/SavePaymentGateWayDetails";
	private static final String BUS_BookingInitiate_API = "Bus/BusBookingInitiate";
	private static final String BUS_BookingSucees_API = "Bus/ProcessPayment";
	private static final String BUS_CANCEL_INIT_PAYMENT_API = "Bus/cancelInitPayment";
	private static final String BUS_CANCEL_BOOKED_TICKET_API = "Bus/cancelBookedTicketForAdmin";
	private static final String BUS_CRON_CHECK_API = "Bus/cronCheck";
	private static final String BUS_GET_ALL_CITY_LIST_FROM_DB = "Bus/getAllCityList";
	private static final String BUS_BOOK_TICKET_API = "Bus/getAllBookTickets";
	private static final String BUS_BOOK_TICKET_WEB_API = "Bus/getMyTicketsforWeb";
	private static final String BUS_BOOK_TICKET_TRAVELLER_WEB_API = "Bus/getSingleTicketTravellerDetails";
	private static final String BUS_DETAILS_FOR_ADMIN_API = "Bus/getBusDetailsForAdmin";
	private static final String BUS_BOOK_TICKET_TRAVELLER_ADMIN_API = "Bus/getTravellerDetailsForAdmin";
	private static final String BUS_CANCEL_MDEX_INIT_PAYMENT = "Bus/CancelInitTPayment";
	private static final String BUS_SERVICE_CHECK = "cehckService";
	private static final String BUS_BOOK_TICKET_PDF_API = "Bus/createBusTicketPdf";
	private static final String FIND_NEAR_BY_MERCHNAT_DETAILS = "findNearByMerchants";
	private static final String FIND_NEAR_BY_AGENT_DETAILS = "findNearByAgents";
	private static final String UPDATE_USER_MAX_LIMIT = "updateUserMaxLimit";
	private static final String GET_USER_MAX_LIMIT = "getUserMaxLimit";
	private static final String SEND_MAX_LIMIT_OTP = "sendMaxLimitOtp";
	private static final String VALIDATE_PASSWORD = "validatePassword";

	/*Change for price re check*/


	private static final String BUS_GET_TXN_ID_UPDATED = "Bus/GetTransactionIdUpdated";
	private static final String BUS_GET_BOOK_TICKET_UPDATED = "Bus/BookTicketUpdated";
	private static final String BUS_SAVEGETTXNID_API = "Bus/saveGetTxnId";
	private static final String BUS_BOOKTICKET_PAYMENT_API = "Bus/bookTicketPayment";
	private static final String BUS_PAYMENT_INIT_API = "Bus/BusPaymentInitiate";
	private static final String BUS_GET_DYNAMIC_FARE_DETAILS = "Bus/getDynamicFareDetails";



	private static final String BUS_SAVE_SEAT_DETAILS_API = "Bus/saveSeatDetails";
	private static final String BUS_SAVE_SEAT_DETAILS_SDK = "SDK/Payment/saveSDKSeatDetails";
	private static final String SDK_REGISTER = "SDK/Payment/MerchantRegistration";
	private static final String SDK_RESEND_OTP = "SDK/Payment/MerchantResendOTP";

	//	End Bus

	private static final String REFER_EARN="/Reward/ReferNearn";

	private static final String USER_LOCATION = "UserByLocationCode";
	private static final String Flight_VNETTXN = "Flight/getVnetTxnRefNo";

	private static final String Flight_FlightBookingInit_API = "Flight/FlightInitiate";

	// Hotel

	private static final String HOTEL_SEARCH_LIST = "Hotel/HotelSearchList";
	private static final String HOTEL_INFO = "Hotel/HotelInfo";
	private static final String HOTEL_AVAILABLITY = "Hotel/HotelAvailability";
	private static final String HOTEL_BOOK = "Hotel/HotelBooking";

	// RedeemPoints
	private static final String REDEEM_POINTS = "/RedeemPointProcess";

	private static final String Flight_BOOK_EBS_API = "Flight/SavePaymentGateWayDetails";

	private static final String LOAD_MONEY_VNET_FLIGHT = "Flight/InitiateVNetFlight";
	private static final String LOAD_MONEY_REDIRECT_VNET_FLIGHT = "Flight/RedirectVNetFlight";

	private static final String Flight_AirRePriceRQ_API_Connecting = "Flight/AirRePrice";
	private static final String GCM_ID_BY_USERNAME = "GetGCMIdByUserName";

	private static final String GCM_ID_BY_USERNAME_SUPERADMIN = "SuperAdmin/GetGCMIdByUserName";

	private static final String Flight_FlightBookingPaySucees_API = "Flight/FlightPayment";
	private static final String Flight_FlightBookingPaymentGatewaySucees_API = "Flight/PaymentGateWayDetailsSave";
	private static final String FLIGHT_CRON_CHECK_API = "Flight/cronCheck";
	private static final String FLIGHT_GET_CITY_FROM_DB_API = "Flight/getAirLineList";

	private static final String FLIGHT_COMPARE_CITY_API = "Flight/compareCountry";

	private static final String Flight_BOOK_TICKETS_API = "Flight/getMyFlightTickets";
	private static final String Flight_BOOK_TICKET_TRAVELLER_ADMIN_API = "Flight/getFlightTravellerDetailsForAdmin";


	// Travel Flight Controller URL

	/*private static final String Agent_Flight_SOURCE = "Flight/AirlineNameRQ";
	private static final String Agent_Flight_Search_API = "Flight/FlightSearch";
	private static final String Agent_Flight_AirRePriceRQ_API = "Flight/AirRePriceRQ";
	private static final String Agent_Flight_AirBookRQ_API = "Flight/AirBookRQ";*/
	private static final String Agent_LOAD_MONEY_Splitpayment = "Agent/Flight/ProcessFlightEBS";
	private static final String Agent_LOAD_MONEY_REDIRECT_Splitpayment = "Agent/Flight/RedirectFlightEBS";
	private static final String Agent_FLIGHT_GET_CITY_FROM_DB_API = "Agent/Flight/getAirLineList";
	private static final String Agent_Flight_BOOK_TICKET_TRAVELLER_ADMIN_API = "Agent/Flight/getFlightTravellerDetailsForAdmin";
	private static final String Agent_Flight_BOOK_EBS_API = "Agent/Flight/SavePaymentGateWayDetails";
	private static final String Agent_FlightsDetailURL = "Agent/Flight/GetFlightDetailsForAdmin";
	private static final String Agent_LOAD_MONEY_VNET_FLIGHT = "Agent/Flight/InitiateVNetFlight";
	private static final String Agent_LOAD_MONEY_REDIRECT_VNET_FLIGHT = "Agent/Flight/RedirectVNetFlight";

	private static final String Agent_Flight_FlightBookingInit_API = "Agent/Flight/FlightInitiate";
	private static final String AGENT_FLIGHT_INITIATE_SPLIT_API = "Agent/Flight/FlightInitiateSplit";
	private static final String Agent_Flight_FlightBookingPaySucees_API = "Agent/Flight/FlightPayment";
	private static final String Agent_Flight_FlightBookingPaymentGatewaySucees_API = "Agent/Flight/PaymentGateWayDetailsSave";
	private static final String Agent_Flight_BOOK_TICKETS_API = "Agent/Flight/getMyFlightTickets";
	private static final String Agent_FLIGHT_CRON_CHECK_API = "Agent/Flight/cronCheck";
	private static final String Agent_FLIGHT_COMPARE_CITY_API = "Agent/Flight/compareCountry";
	
	// Travelkhana
	private static final String PAYMENT_INITATE_TK = "Travelkhana/PlaceOrderInitiate";
	private static final String GET_TRAIN_LIST_FROM_DB = "Travelkhana/getTrainListFromDB";
	// private static final String PAYMENT_SUCCESS_TK =
	// "Travelkhana/PlaceOrderSuccess";
	private static final String My_Order = "Travelkhana/MyOrder";
	private static final String Order_DETAILS_FOR_ADMIN_API = "Travelkhana/getOrderDetailsForAdmin";
	private static final String Order_DETAILS_BY_DATE_FOR_ADMIN_API = "Travelkhana/getOrderDetailsByDateForAdmin";
	private static final String TreatCard_Details = "TreatCard/GetCardDetails";
	private static final String TREATCARD_PLANS = "TreatCard/GetTreatCardPlans";
	private static final String TREATCARD_PLANS_USER = "TreatCard/GetUserTreatCardPlans";
	private static final String UPDATE_TREATCARD_PLANS = "TreatCard/UpdateTreatCardPlans";
	private static final String UPDATE_TREATCARD_PAYMENT = "TreatCard/updateTreatCard";
	private static final String TREATCARD_REGISTER_LIST = "TreatCard/TreatCardRegisterList";
	private static final String TREATCARD_FILTERED_REGISTER_LIST = "TreatCard/TreatCardFilteredRegisterList";
	private static final String TREATCARD_TRANSACTION_LIST = "TreatCard/GetTreatCardTransaction";
	private static final String TREATCARD_FILTERED_TRANSACTION_LIST = "TreatCard/GetTreataCardTransaction/Filter";
	private static final String TREATCARD_COUNT_LIST = "TreatCard/TreatCardCountList";
	private static final String REFUND_MERCHANT = "Merchant/RefundMerchant";

	private static final String REFUND_TRANSACTION = "Merchant/RefundMerchantTransaction";

	private static final String FlightsDetailURLByDate = "Flight/GetFlightDetailsByDate";
	private static final String Agent_FlightsDetailURLByDate = "Agent/Flight/GetFlightDetailsByDate";
	private static final String BUS_DETAILS_FOR_ADMIN_API_BY_DATE = "Bus/getBusDetailsByDate";
	private static final String Agent_BUS_DETAILS_FOR_ADMIN_API_BY_DATE = "Agent/Bus/getBusDetailsByDate";
	// HouseJoy

	private static final String HOUSE_JOY_INITIATE = "HouseJoyInitiate";
	private static final String HOUSE_JOY_SUCCESS = "HouseJoySuccess";
	private static final String HOUSE_JOY_FAILURE = "HouseJoyCancel";
	private static final String HOUSE_JOYDetailURLByDate = "GetHouseJoyDetailsByDate";
	private static final String HOUSE_JOYDetailURL = "GetHouseJoyDetails";

	// Graph

	private static final String TRANSACTION_COUNT_MONTHWISE = "CountByMonthWise";
	private static final String USER_COUNT_MONTHWISE = "CountUserByMonthWise";
	private static final String TRANSACTION_COUNT_MONTHWISE_SINGLE_USER = "CountByMonthWiseForSingleUser";

	private static final String BULK_UPLOAD = "BulkUpload";
	
	private static final String BULK_UPLOAD_Merchant = "Merchant/BulkUpload";
	
	private static final String PREDICTANDWIN_UPLOAD = "PredictAndWinUpload";

	private static final String SUPERADMIN_BULKFILE_LIST = "SuperAdmin/GetBulkUploadRedords";

	private static final String SUPERADMIN_BULKFILE_LIST_RECORD = "SuperAdmin/SaveBulkUploadRedords";
	
	private static final String EDIT_PROMOCODE = "editPromoCode";
	
	private static final String EDIT_AgentList = "editAgentList";


	/*Admin Panel Backend Report*/

	private static final String WALLET_BALANCE = "getWalletBalance";
	private static String DAILY_VALUES_LIST = "GetTransactionReport";
	private static final String MISREPORT = "MISReport";
	private static final String WALLETLOADREPORT = "WalletLoadReport";
	private static final String WALLET_BALANCE_BY_DATE = "getWalletBalanceByDate";
	private static String ADMIN_REFUND_INSTANTPAY_TRANSACTIONS = "getRefundIpayList";

	// Agent Travel Bus Controller URL
	
		private static final String Agent_BUS_GET_ALL_CITY_LIST = "Agent/Bus/GetAllCityList";
		private static final String Agent_BUS_GET_SOURCE_CITY_LIST = "Agent/Bus/GetSourceCityBySourceKey";
		private static final String Agent_BUS_GET_DESTINATION_CITY_LIST = "Agent/Bus/GetDestinationCity";
//		private static final String Agent_BUS_GET_ALL_AVAILABLE_TRIPS = "Agent/Bus/GetAllAvailableTrips";
		private static final String Agent_BUS_GET_SEAT_DETAILS = "Agent/Bus/GetSeatDetails";
		private static final String Agent_BUS_GET_TXN_ID = "Agent/Bus/GetTransactionId";

		private static final String Agent_BUS_GET_BOOK_TICKET = "Agent/Bus/BookTicket";
		private static final String Agent_BUS_GET_ISCANCELABLE_TICKET = "Agent/Bus/IsCancellable";
		private static final String Agent_BUS_GET_CANCEL_TICKET = "Agent/Bus/CancelTicket";
//		private static final String NEAR_BY_MERCHNAT_DETAILS = "/GetNearByMerchants";
		private static final String Agent_Flight_FlightBookingInitiate_API = "Agent/Flight/FlightBookingInitiate";
		private static final String Agent_Flight_FlightBookingSucees_API = "Agent/Flight/ProcessPayment";
		private static final String Agent_FLIGHT_PAYMENTGATEWAY_API = "Agent/Flight/SavePaymentGateWayDetails";
		private static final String Agent_BUS_BookingInitiate_API = "Agent/Bus/BusBookingInitiate";
		private static final String Agent_BUS_BookingSucees_API = "Agent/Bus/ProcessPayment";
		private static final String Agent_BUS_CANCEL_INIT_PAYMENT_API = "Agent/Bus/cancelInitPayment";
		private static final String Agent_BUS_CANCEL_BOOKED_TICKET_API = "Agent/Bus/cancelBookedTicketForAdmin";
		private static final String Agent_BUS_CRON_CHECK_API = "Agent/Bus/cronCheck";
		private static final String Agent_BUS_GET_ALL_CITY_LIST_FROM_DB = "Agent/Bus/getAllCityList";
		private static final String Agent_BUS_BOOK_TICKET_API = "Agent/Bus/getAllBookTickets";
		private static final String Agent_BUS_BOOK_TICKET_WEB_API = "Agent/Bus/getMyTicketsforWeb";
		private static final String Agent_BUS_BOOK_TICKET_TRAVELLER_WEB_API = "Agent/Bus/getSingleTicketTravellerDetails";
		private static final String Agent_BUS_DETAILS_FOR_ADMIN_API = "Agent/Bus/getBusDetailsForAdmin";
		private static final String Agent_BUS_BOOK_TICKET_TRAVELLER_ADMIN_API = "Agent/Bus/getTravellerDetailsForAdmin";
	
		private static final String Agent_BUS_BOOK_TICKET_PDF_API = "Agent/Bus/createBusTicketPdf";
		/*Change for price re check*/

		private static final String Agent_BUS_SAVEGETTXNID_API = "Agent/Bus/saveGetTxnId";
		private static final String Agent_BUS_BOOKTICKET_PAYMENT_API = "Agent/Bus/bookTicketPayment";
		private static final String Agent_BUS_PAYMENT_INIT_API = "Agent/Bus/BusPaymentInitiate";

		private static final String Agent_BUS_SAVE_SEAT_DETAILS_API = "Agent/Bus/saveSeatDetails";
		
		private static final String GET_USEROTP_AADHAR="GetUserOTP";
		
		private static final String GET_USEROTP_AADHAR_VALIDATE="GetUserOTPValidate";
		
		private static final String SCHEDULE="schedule";
		
		private static final String PREDICTION="prediction";
		
		private static final String USER_PREDICTION="userPrediction";
		
		private static final String SAVE_CLAIM_DETAILS="saveClaimDetails";
		
		private static final String ADD_SERVICES = "SuperAdmin/AddServices";
		
		private static final String ADD_SERVICE_TYPE = "SuperAdmin/AddServiceType";
		
		private static final String VIJAYA_MERCHANT_REPORT = "Merchant/getMerchantReport";
		
		private static final String VIJAYA_MERCHANT_REPORT_FILTERED = "Merchant/getFilteredMerchantReport";

		private static final String ADD_VOUCHERS = "addVouchers";
		private static final String ALL_VOUCHERS = "getTotalVouchers";
		private static final String ALL_VOUCHERS_BY_DATE = "findVoucherByDate";
	
	public static String getLocationInfo(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + IMAGICA_GET_LOCATION;
		return url;
	}

	public static String processImagicaOrder(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + IMAGICA_PAYMENT_PROCESS;
		return url;
	}

	public static String placeImagicaOrder(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + IMAGICA_PLACE_ORDER;
		 
		return url;
	}

	public static String getImagicaCredentials(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + IMAGICA_AUTH;
		 
		return url;
	}
	//

	public static String getSuperadminRefund(Version version, Role role, Device device, Language language) {

		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + SUPERADMIN_REFUND;
		 
		return url;
	}

	public static String getSuperadminRefundBillPay(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + SUPERADMIN_REFUND_BILLPAY;
		 
		return url;
	}

	public static String getGetServiceTypesURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + GET_SERVICE_TYPES_NEW;
		 
		return url;
	}

	public static String getTransactionsFilteredURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + GET_TRANSACTIONS_BY_SERVICE;
		 
		return url;
	}

	public static String getTransactionsFilteredBillPayURL(Version version, Role role, Device device,
			Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + GET_TRANSACTIONS_BY_SERVICE_BILLPAY;
		 
		return url;
	}

	// paylo transaction
	private static final String PAYLO_TOKEN = "GetUserBalance";
	private static final String PAYLO_INTINATE_TRANSACTION = "InitiateTransation";
	private static final String PAYLO_UPDATE_TRANSACTION = "UpdateTransation";
	private static final String GET_CREDIT_ANAL = "GetAnalyticsCredit";

	// Paylo Transaction

	public static String getPayloToken(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + PAYLO_TOKEN;
		 
		return url;
	}

	public static String getInitiatePayloToken(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + PAYLO_INTINATE_TRANSACTION;
		 
		return url;
	}

	public static String getPayloUpdateTransaction(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + PAYLO_UPDATE_TRANSACTION;
		 
		return url;
	}

	// Paylo Transaction

	public static String getNikkiToken(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + NIKKI_TOKEN;
		 
		return url;
	}

	public static String getIntinateNikkiTransaction(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + NIKKI_INTINATE_TRANSACTION;
		 
		return url;
	}

	public static String getUpdateNikkiTransaction(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + NIKKI_UPDATE_TRANSACTION;
		 
		return url;
	}

	public static String getRefundNikkiTransaction(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + NIKKI_REFUND_TRANSACTION;
		 
		return url;
	}

	public static String getAllCreditlistAgent(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + AllCreditlistAgent;
		return url;
	}

	// send money to donatee

	public static String getSendMoneyDonatee(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + SEND_MONEY_DONATEE;
		return url;
	}

	public static String getSuperadminAccessList(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + SUPERADMIN_ACCESS_LIST;
		System.err.println(url);
		return url;
	}

	public static String getSuperadminAccessUsers(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + SUPERADMIN_ACCESS_USERS;
		System.err.println(url);
		return url;
	}

	public static String getUpdateAccessURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + UPDATE_ACCESS;
		System.err.println(url);
		return url;
	}

	public static String getVerifyKYCRequest(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + MERCHANT_VERIFY_KYC;
		return url;
	}

	public static String getVerifyKYCProcess(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + MERCHANT_VERIFY_KYC_OTP;
		return url;
	}

	public static String getGeneratePasswordRequest(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + MERCHANT_GENERATE_PASSWORD;
		return url;
	}

	private static final String UPDATE_MERCHANT = "Merchant/UpdateMerchant";
	private static final String UPDATE_QR = "Merchant/saveqr";

	public static String getListAccountTypeSuperAdminURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + LIST_ACCOUNTS_SUPERADMIN;
		return url;
	}

	public static String updateAccountTypeSuperAdminURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + UPDATE_ACCOUNTS_SUPERADMIN;
		return url;
	}

	public static String getSuperAdminChangePasswordUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + SUPERADMIN_CHANGE_PASSWORD;
		 
		return url;
	}

	public static String getNeftFilterUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + SUPERADMIN_NEFT_LIST_FILTER;
		 
		return url;
	}

	public static String getUserInfoUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + SUPERADMIN_USER_INFO;
		 
		return url;
	}

	public static String sendMail(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + SUPERADMIN_EMAIL;
		 
		return url;
	}

	public static String sendBulkMail(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + SUPERADMIN_BULK_EMAIL;
		 
		return url;
	}

	public static String sendSMS(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + SUPERADMIN_SMS;
		 
		return url;
	}

	public static String sendBulkSMS(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + SUPERADMIN_BULK_SMS;
		 
		return url;
	}

	public static String getPossiblities(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + SUPERADMIN_GETPOSSIBLITIES;
		 
		return url;
	}

	public static String updateServices(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + UPDATE_SERVICES;
		 
		return url;
	}

	public static String getGCMIDs(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + SUPERADMIN_FCM_ID;
		 
		return url;
	}

	public static String getAdminGCMIDs(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + ADMIN_FCM_ID;
		 
		return url;
	}

	public static String getSuperadminServicelistNow(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + GET_ALL_SERVICES;
		 
		return url;
	}

	public static String getSuperadminLogslistNow(Version version, Role role, Device device, Language language,
			String type) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + LOGS_NOW + type;
		 
		return url;
	}

	public static String getSuperadminLogslistFilter(Version version, Role role, Device device, Language language,
			String type) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + LOGS_FILTER + type;
		 
		return url;
	}

	public static String getSuperadminTransactionlistNow(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + TRANSACTION_LIST_SUPERADMIN;
		 
		return url;
	}

	public static String getSuperadminTransactionlistFilter(Version version, Role role, Device device,
			Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + TRANSACTION_LIST_FILTER_SUPERADMIN;
		 
		return url;
	}

	public static String getSuperadminUserlistNow(Version version, Role role, Device device, Language language,
			String userType) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + USERLIST_BYTYPE_SUPERADMIN
				+ SEPARATOR + userType;
		 
		return url;
	}

	public static String getSuperadminUserlistFilter(Version version, Role role, Device device, Language language,
			String userType) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + USERLIST_BYTYPE_FILTER_SUPERADMIN
				+ SEPARATOR + userType;
		return url;
	}

	public static String getSettlementsTransactionURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + SETTLEMENT_REPORTS;
		return url;
	}

	public static String getBasicDetailsURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + BASIC_DETAILS;
		return url;
	}

	public static String getAccountDetailsURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + ACCOUNT_DETAILS;
		return url;
	}

	public static String getBalanceDetailsURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + BALANCE_DETAILS;
		return url;
	}

	public static String getLocationDetailsURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + LOCATION_DETAILS;
		return url;
	}

	public static String getLimitDetailsURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + LIMITS_DETAILS;
		return url;
	}

	public static String getImageDetailsURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + IMAGE_DETAILS;
		return url;
	}

	public static String getSingleUserTransactionURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + SINGLE_TRANSACTION_POST;
		return url;
	}

	public static String getAllMerchantURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + MERCHANT_LIST;
		return url;
	}

	public static String getAllTransactionsAdminURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + ADMIN_ALLTRANSACTIONS;
		return url;
	}

	public static String renewMerchantPasswordURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + MERCHANT_CHPASSWORD;
		return url;
	}

	public static String getPromoTransactionsURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + PROMO_TRANSACTIONS;
		return url;
	}

	public static String getPromoTransactionsFilteredURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + PROMO_TRANSACTIONS_FILTERED;
		return url;
	}

	public static String getVpayqwikMerchantTransactionsURL(Version version, Role role, Device device,
			Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + VPAYQWIK_MERCHANT_TRANSACTIONS;
		return url;
	}

	public static String getVpayqwikMerchantReceiptURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + VPAYQWIK_MERCHANT_RECEIPTS;
		return url;
	}

	public static String getBescomMerchantReceiptURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + BESCOM_MERCHANT_RECEIPTS;
		return url;
	}
	
	public static String fetchCustDetailsURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + BESCOM_CUST_DETAILS;
		return url;
	}
	
	public static String getMerchantRefundRequestURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + BESCOM_MERCHANT_REFUND_REQUEST;
		return url;
	}
	
	public static String getVpayqwikMerchantTransactionsFilteredURL(Version version, Role role, Device device,
			Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR
				+ VPAYQWIK_MERCHANT_TRANSACTIONS_FILTERED;
		return url;
	}

	public static String getDebitVpayqwikMerchantTransactionsURL(Version version, Role role, Device device,
			Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR
				+ VPAYQWIK_MERCHANT_TRANSACTIONS_DEBIT;
		return url;
	}

	public static String getRefundAmountURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + REFUND_AMOUNT;
		return url;
	}

	public static String getMerchantsURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + GET_MERCHANTS;
		return url;
	}

	public static String getMerchantsTransactions(Version version, Role role, Device device, Language language,
			String username) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + username + SEPARATOR
				+ MERCHANT_TXLIST;
		return url;
	}

	public static String getServiceURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + SERVICE_URL;
		return url;
	}
	public static String getServicesById(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + SERVICES_BY_ID;
		return url;
	}
	
	public static String getServicesByIdSuperAdmin(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + SERVICES_BY_ID_SUPERADMIN;
		return url;
	}

	public static String getAllCommission(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + GET_COMMISSION;
		return url;
	}
	
	public static String updateCommission(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + UPDATE_COMMISSION;
		return url;
	}
	
	public static String getServiceTypesURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + GET_SERVICE_TYPES;
		return url;
	}

	public static String getSharePointsURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + SHARE_POINTS;
		return url;
	}

	public static String getValuesListURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + VALUES_LIST;
		return url;
	}

	public static String getMerchantValuesListURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + MERCHANT_VALUES_LIST;
		return url;
	}

	
	public static String getNeftListURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + NEFT_LIST;
		return url;
	}

	public static String getNeftListMerchantURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + MERCHANT_NEFT_LIST;
		return url;
	}
	public static String getNeftListAgentURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR +AGENT_NEFT_LIST;
		return url;
	}

	public static String getCarrotfryCouponsURL() {
		String url = CARROTFRY_COUPONS;
		return url;
	}

	public static String getAllBanksURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + GET_ALL_BANKS;
		return url;
	}

	public static String getIfscURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + IFSC_BY_BANK + SEPARATOR;
		return url;
	}

	public static String getOnePayTransactionURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + GET_TRANSACTION_DTO;
		return url;
	}

	public static String getMerchantTransactions(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + MERCHANT_TRANSACTIONS;
		return url;
	}

	public static String listAllVersions(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + LIST_VERSION;
		return url;
	}

	public static String validateVersion(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + VALIDATE_VERSION;
		return url;
	}

	public static String updateVersion(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + UPDATE_VERSION;
		return url;
	}

	public static String getAllServices(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + GET_SERVICES;
		return url;
	}

	public static String transactionCheck(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + TRANSACTION_VALIDATION;
		return url;
	}

	public static String merchantTransactionCheck(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + VALIDATE_MERCHANT_TRX;
		return url;
	}

	public static String initiateBankTransfer(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + BANK_INITIATE;
		return url;
	}
	public static String initiateABankTransfer(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + ABANK_INITIATE;
		return url;
	}

	public static String successBankTransfer(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + BANK_SUCCESS;
		return url;
	}

	public static String failureBankTransfer(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + BANK_FAILURE;
		return url;
	}

	public static String initiateMBankTransfer(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + MBANK_INITIATE;
		return url;
	}

	public static String getLoginUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + LOGIN;
		return url;
	}

	public static String getPayAtStoreUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + PAY_AT_STORE;
		return url;
	}

	public static String getLoadMoneyUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + LOAD_MONEY;
		return url;
	}
	
	public static String getLoadMoneyFailedUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + LOAD_MONEY_FAILED;
		return url;
	}

	public static String getLoadMoneyResponseUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + LOAD_MONEY_REDIRECT;
		return url;
	}

	public static String getLoadMoneyVNetUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + LOAD_MONEY_VNET;
		return url;
	}

	public static String getLoadMoneyVNetFlightUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + LOAD_MONEY_VNET_FLIGHT;
		return url;
	}

	public static String getLoadMoneyVNetResponseUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + LOAD_MONEY_REDIRECT_VNET;
		return url;
	}

	public static String getLoadMoneyStatusAfterRedirectResponseUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + LOAD_MONEY_STATUS_AFTER_REDIRECT;
		return url;
	}

	public static String getLoadMoneyUPIResponseUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + LOAD_MONEY_REDIRECT_UPI;
		return url;
	}
	
	public static String getLoadMoneySdkUPIResponseUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + LOAD_MONEY_SDK_REDIRECT_UPI;
		return url;
	}

	public static String getLoadMoneyVNetResponseFlightUrl(Version version, Role role, Device device,
			Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + LOAD_MONEY_REDIRECT_VNET_FLIGHT;
		return url;
	}

	public static String getSendMoneyMobileUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + SEND_MONEY_MOBILE;
		return url;
	}

	public static String getListStoreUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + LIST_STORE_API;
		return url;
	}

	public static String resetPasswordOtpUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + RESET_PASSWORD_WITH_OTP;
		return url;
	}

	public static String deviceBindingUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + DEVICE_BINDING_WITH_OTP;
		return url;
	}

	public static String getRegisterUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + REGISTER;
		return url;
	}

	public static String getMobileUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + MOBILE_OTP;
		return url;
	}

	public static String getResendMobileUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + RESEND_MOBILE_OTP;
		return url;
	}

	public static String getResendKycOtpUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + RESEND_KYC_OTP;
		return url;
	}

	public static String getVerifyEmailUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + VERIFY_EMAIL;
		return url;
	}

	public static String getAllTransactionUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + ALL_TRANSACTION;
		return url;
	}

	public static String getAllTransactionFilteredUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + ALL_TRANSACTION_FILTERED;
		return url;
	}

	public static String getAllUserUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + ALL_USER;
		return url;
	}
	
	public static String getUpiTxn(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + UPI_TXN;
		return url;
	}
	
	public static String getGcmList(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + GCM_LIST;
		return url;
	}

	public static String getUserTransactionUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + USER_TRANSACTION;
		return url;
	}

	public static String getMessageLogUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + MESSAGE_LOG;
		return url;
	}

	public static String getEmailLogUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + EMAIL_LOG;
		return url;
	}

	public static String blockUserUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + BLOCK_USER;
		return url;
	}

	public static String unblockUserUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + UNBLOCK_USER;
		return url;
	}

	public static String getLogoutUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + LOGOUT;
		return url;
	}

	public static String getPrePaidTopupUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + PREPAID_TOPUP;
		return url;
	}


	public static String getGetOperatorCircleUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + OPERATOR_AND_CIRCLE_FOR_MOB;
		return url;
	}

	public static String getUserDetailsUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + USER_DETAILS;
		return url;
	}

	public static String getEditProfileUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + EDIT_PROFILE;
		return url;
	}

	public static String getChangePasswordUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + CHANGE_PASSWORD;
		return url;
	}

	public static String getUpdatePasswordUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + UPDATE_PASSWORD;
		return url;
	}

	public static String getUploadPictureUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + UPLOAD_PICTURE;
		return url;
	}

	public static String getReceiptsUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + RECEIPTS;
		return url;
	}

	public static String getReceiptsFilterUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + RECEIPTS_FILTER;
		return url;
	}

	public static String getTransactionsUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + SUCCESSFUL_TRANSACTIONS;
		return url;
	}

	public static String getFavouriteUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + UPDATE_FAVOURITE;
		return url;
	}

	public static String getSetMPINUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + SET_MPIN;
		return url;
	}

	public static String getChangeMPINUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + CHANGE_MPIN;
		return url;
	}

	public static String getDeleteMPINUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + DELETE_MPIN;
		return url;
	}

	public static String getForgotMPINUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + FORGOT_MPIN;
		return url;
	}

	public static String getVerifyMPINUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + VERIFY_MPIN;
		return url;
	}

	public static String getElectricityBillUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + ELECTRICITY_BILL;
		return url;
	}

	public static String getGasBillUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + GAS_BILL;
		return url;
	}

	public static String getInsuranceBillUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + INSURANCE_BILL;
		return url;
	}

	public static String getLandlineBillUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + LANDLINE_BILL;
		return url;
	}

	public static String getDthBillUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + DTH_BILL;
		return url;
	}

	public static String getOperatorAndCircleUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + OPERATOR_AND_CIRCLE;
		return url;
	}

	public static String getOperatorAndCircleForMobUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + OPERATOR_AND_CIRCLE_FOR_MOB;
		return url;
	}

	public static String getBetweenDateTransactions(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + BETWEEN_DATES;
		return url;
	}

	public static String getAdminDashBoardVUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + ADMIN_DASHBOARD;
		return url;
	}

	public static String getAuthority(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + AUTHORITY;
		return url;
	}

	public static String getPlans(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + PLANS_FOR_MOBILE;
		return url;
	}

	public static String getBlockUser(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + BLOCK_USER;
		return url;
	}

	public static String getUserUnBlock(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + UNBLOCK_USER;
		return url;
	}

	public static String getInviteEmailFriendUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + INVITE_EMAIL_FRIEND;
		return url;
	}

	public static String getInviteMobileFriendUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + INVITE_MOBILE_FRIEND;
		return url;
	}

	public static String getUserDetailByAdminUrl(Version version, Role role, Device device, Language language,
			String usernumber) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + usernumber + SEPARATOR
				+ USER_BY_ADMIN;
		return url;
	}

	public static String reSendEmailOTPUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + RESEND_EMAIL_OTP;
		return url;
	}

	public static String getCallBackUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + CALL_BACK;
		return url;
	}

	public static String addMerchant(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + ADD_MERCHANT;
		return url;
	}

	public static String savePromoCode(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + SAVE_PROMO_CODE;
		return url;
	}
	
	public static String editPromoCode(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + EDIT_PROMO_CODE;
		return url;
	}

	public static String listPromoCode(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + LIST_PROMO_CODE;
		return url;
	}

	public static String redeemPromoCode(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + REDEEM_PROMO;
		return url;
	}

	public static String kycRequestUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + KYC_REQUEST;
		return url;
	}

	public static String kycOtpVerificationUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + KYC_OTP_VERIFICATION;
		return url;
	}

	public static String kycWebOtpVerificationUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + KYC_WEB_OTP_VERIFICATION;
		return url;
	}

	public static String getSingle(Version version, Role role, Device device, Language language, String number) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + number + SEPARATOR + SINGLE_USER;
		return url;
	}

	public static String offlinePaymentUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + OFFLINE_PAYMENT;
		return url;
	}

	public static String offlinePaymentOTP(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + OFFLINE_OTP;
		return url;
	}

	public static String busBookingUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + BUS_BOOKING;
		return url;
	}

	public static String busBookingSuccessResponseUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + BUS_BOOKING_RESPONSE;
		return url;
	}

	public static String saveAccessTokenURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + SAVE_TOKEN;
		 
		return url;
	}

	public static String saveTokenURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + SAVE_SAVAARI_TOKEN;
		 
		return url;
	}

	public static String getTokenURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + GET_SAVAARI_TOKEN;
		 
		return url;
	}

	public static String SaveBookingDetails(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + SAVE_SAVAARI_DETAILS;
		 
		return url;
	}

	public static String SuccessBookingDetails(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + SUCCESS_SAVAARI_DETAILS;
		 
		return url;
	}

	public static String getBookingId(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + INITIATE_CANCELLATION;
		 
		return url;
	}

	public static String saveEventDetailsURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + SAVE_EVENT_DETAILS;
		return url;
	}

	public static String visaInitatedRequestUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + INITATED_REQUEST;
		 
		return url;
	}

	public static String prepareGetAccountNumberRequest(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + GET_ACCOUNT_REQUEST;
		 
		return url;
	}

	public static String prepareGetAccNoDecryption(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + GET_DECRYPT_REQUEST;
		 
		return url;

	}

	public static String paymentInitateForMeraEventsURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + EVENT_PAYMENT_INITATE;
		 
		return url;
	}

	public static String visaInitatedResponseUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + INITATED_RESPONSE;
		 
		return url;
	}

	public static String eventPaymentURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + EVENT_PAYMENT;
		 
		return url;
	}

	public static String addVisaMerchant(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + MERCHANT_SIGNUP;
		System.err.println(url);
		return url;
	}

	public static String checkVisaMerchant(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + CHECK_VISA_MERCHANT;
		return url;
	}
	// for Admin password change

	public static String adminChangePassword(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + ADMIN_CHANGEPASSWORD;
		 
		return url;
	}

	public static String getMerchantSignUpUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + MERCHANT_SIGNUP;
		 
		return url;
	}

	public static String getMBankDetailsUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + MERCHANT_ADD_BANK;
		 
		return url;
	}

	public static String getMobileOTPUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + MERCHANT_MOBILE_OTP;
		return url;
	}

	public static String getResendMobileOTPUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + RESEND_MERCHANT_MOBILE_OTP;
		return url;
	}

	public static String getQuestionsUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + SECURITY_QUESTIONS;
		return url;
	}

	public static String checkExistingMerchant(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + CHECK_EXISTING_MERCHANT;
		return url;
	}

	public static String signUpVisaMerchant(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + SIGNUP_VISA_MERCHANT;
		return url;
	}

	// SUPER ADMIN URL

	public static String getValuesListOfSuperAdminURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + VALUES_LIST_SUPERADMIN;
		return url;
	}

	public static String getBetweenDateTransactionsOfSuperAdmin(Version version, Role role, Device device,
			Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + SUPERADMIN_BETWEEN_DATES;
		return url;
	}

	public static String getServiceTypesOfSuperAdminURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + GET_SERVICE_TYPES_SUPERADMIN;
		return url;
	}

	public static String getServiceOfSuperAdminURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + SERVICE_URL_SUPERADMIN;
		return url;
	}

	public static String getAllTransactionOfSuperAdminUrl(Version version, Role role, Device device,
			Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + ALL_TRANSACTION_SUPERADMIN;
		return url;
	}

	public static String getSettlementsTransactionOfSuperAdminURL(Version version, Role role, Device device,
			Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + SETTLEMENT_REPORTS_SUPERADMIN;
		return url;
	}

	public static String getPromoTransactionsOfSuperAdminURL(Version version, Role role, Device device,
			Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + PROMO_TRANSACTIONS_SUPERADMIN;
		return url;
	}

	public static String getAllUserOfSuperAdminUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + ALL_USER_SUPERADMIN;
		return url;
	}

	public static String getAllMerchantOfSuperAdminURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + MERCHANT_LIST_SUPERADMIN;
		return url;
	}

	public static String getUserTransactionOfSuperAdminUrl(Version version, Role role, Device device,
			Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + USER_TRANSACTION_SUPERADMIN;
		return url;
	}

	public static String getRefundAmountOfSuperAdminURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + REFUND_AMOUNT_SUPERADMIN;
		return url;
	}

	public static String getMessageLogOfSuperAdminUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + MESSAGE_LOG_SUPERADMIN;
		return url;
	}

	public static String getEmailLogOfSuperAdminUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + EMAIL_LOG_SUPERADMIN;
		return url;
	}

	public static String blockUserOfSuperAdminUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + BLOCK_USER_SUPERADMIN;
		return url;
	}

	public static String unblockUserOfSuperAdminUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + UNBLOCK_USER_SUPERADMIN;
		return url;
	}

	public static String getAllTransactionsOfSuperAdminURL(Version version, Role role, Device device,
			Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + SUPERADMIN_ALLTRANSACTIONS;
		return url;
	}

	public static String getUserDetailByOfSuperAdminUrl(Version version, Role role, Device device, Language language,
			String usernumber) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + USER_BY_SUPERADMIN;
		return url;
	}

	public static String getBlockUserOfSuperAdmin(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + GET_SERVICE_TYPES_SUPERADMIN;
		return url;
	}

	public static String addMerchantOfSuperAdmin(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + SUPERADMIN_ADD_MERCHANT;
		return url;
	}

	public static String getSingleOfSuperAdmin(Version version, Role role, Device device, Language language,
			String usernumber) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + USER_BY_SUPERADMIN;
		return url;
	}

	public static String getNeftListOfSuperAdminURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + SUPERADMIN_NEFT_LIST;
		return url;
	}

	public static String getNeftListMerchantOfSuperAdminURL(Version version, Role role, Device device,
			Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + SUPERADMIN_MERCHANT_NEFT_LIST;
		return url;
	}

	public static String getMerchantsTransactionsOfSuperAdmin(Version version, Role role, Device device,
			Language language, String username) {
		String MERCHANT_TXLIST = "SuperAdmin/"+username+"/getMTransactions";
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR 
				+ MERCHANT_TXLIST ;
		return url;
	}

	public static String addVisaMerchantOfSuperAdmin(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + ADD_VISA_MERCHANT_SUPERADMIN;
		return url;
	}

	public static String checkVisaMerchantOfSuperAdmin(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + CHECK_VISA_MERCHANT_SUPERADMIN;
		return url;
	}

	public static String getFPSwitchStatus(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + SWITCH_STATUS;
		 
		return url;
	}

	public static String getFPStatusActive(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + FP_STATUS_ACTIVE;
		 
		return url;
	}

	public static String getFPStatusInactive(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + FP_STATUS_INACTIVE;

		 
		return url;
	}

	public static String savePromoCodeOfSuperAdmin(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + SAVE_PROMO_CODE_SUPERADMIN;

		 
		return url;
	}

	public static String listPromoCodeOfSuperAdmin(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + LIST_PROMO_CODE_SUPERADMIN;

		 
		return url;
	}

	// Admin Search User
	public static String getUserByMobile(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + FINDUSERBYMOBILE;

		 
		return url;
	}

	public static String listVisaMerchant(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + LIST_VISA_MERCHANT;
		return url;
	}

	public static String listVisaMerchantFilter(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + LIST_VISA_MERCHANT_FILTER;
		return url;
	}

	public static String getsingleUserUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + ONE_USER;
		return url;
	}

	public static String listTransactionReport(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + TRANSACTIONREPORT;
		return url;
	}
	
	public static String listTransactionReportByPage(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + TRANSACTIONREPORT_BY_PAGE;
		return url;
	}

	public static String listWooHooTransactionReport(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + WOOHOO_TRANSACTIONREPORT;
		return url;
	}

	public static String listReconReport(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + RECON_REPORT;
		return url;
	}

	// For nikki new

	public static String listnikkiTransactionReport(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + NEWNIKKI;
		return url;
	}

	public static String listUserURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + USERLIST_REPORT;
		return url;
	}

	public static String giftCartURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + GIFT_CART;
		 
		return url;
	}

	public static String cartSHOWURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + CART_SHOW;
		 
		return url;
	}

	public static String itemRemoveURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + ITEM_REMOVE;
		 
		return url;
	}

	public static String paymentInitateURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + PAYMENT_INITATE;
		 
		return url;
	}

	public static String paymentsuccessURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + PAYMENT_SUCCESS;
		 
		return url;
	}

	public static String getListAccountTypeURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + LIST_ACCOUNT_TYPE;
		return url;
	}

	public static String updateAccountTypeURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + UPDATE_ACCOUNT_TYPE;
		return url;
	}

	public static String m2pGetEntityId(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + GET_ENTITY_M2P;
		return url;
	}

	public static String refundStatusURl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + REFUND_STATUS;
		return url;
	}

	public static String transactionIdClickable(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + TRANSACTIONID_CLICKABLE;
		return url;
	}

	public static String kycUpdateOfSuperAdminUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + KYC_UPDATE;
		return url;
	}

	public static String getChangePasswordWithOTPUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + CHANGE_PASSWORD_WITH_OTP;
		return url;
	}
	
	public static String getAgentPasswordUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + CHANGE_PASSWORD_WITH_Mob;
		return url;
	}

	public static String nonKycUpdateOfSuperAdminUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + NON_KYC_UPDATE;
		return url;
	}

	public static String getNeftListFilteredURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + NEFT_LIST_FILTERED;
		return url;
	}

	public static String updateMerchantM2P(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + UPDATE_MERCHANT;
		return url;
	}

	public static String updateQRcode(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + UPDATE_QR;
		return url;
	}

	public static String getPasswordOtpUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + FORGET_PASSWORD;
		return url;
	}

	public static String decryptResponse(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + DECRYPT_RESPONSE;
		return url;
	}

	/* Agnet Module */

	public static String addAgent(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + ADD_AGENT;
		return url;
	}
	
	public static String getCityAndState(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + Get_City_State;
		return url;
	}
	public static String getBankList(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + Get_Bank_List;
		return url;
	}

	public static String agentMobileOtpVerification(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + MobileOtp_AGENT;
		return url;
	}

	public static String getSendMoneyAgentMobileUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + SEND_MONEY_AGENT_MOBILE;
		return url;
	}

	public static String addUserAgent(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + ADD_User_AGENT;
		return url;
	}

	public static String getAgentPasswordOtpUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + FORGET_AGENT_PASSWORD;
		return url;
	}

	public static String resetAgentPasswordOtpUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + RESET_AGENT_PASSWORD;
		return url;
	}

	public static String getAgentResendMobileUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + AGNET_RESEND_MOBILE_OTP;
		return url;
	}

	public static String agentUserMobileOtpVerification(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + MobileOtp_User_AGENT;
		return url;
	}

	// for donatee
	public static String addDonatee(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + ADD_DONATEE;
		return url;
	}

	public static String getSendMoneyRequestAgentUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + SEND_MONEY_REQUEST_AGENT;
		return url;
	}

	public static String getStatusByTrx(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + GET_STATUS_BY_TRANSACTION;
		return url;
	}

	public static String cancelTicket(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + CANCELL_TICKET;
		 
		return url;
	}

	public static String getTicketDetails(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + TICKET_DETAILS;
		 
		return url;
	}

	public static String getAllAgentURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + AGENT_LIST;
		return url;
	}

	public static String getAllFlightsDetailURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + FlightsDetailURL;
		return url;
	}

	public static String paymentInitateURLAdlabs(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + PAYMENT_INITATE_ADLABS;
		 
		return url;
	}

	public static String paymentsuccessURLAdlabs(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + PAYMENT_SUCCESS_ADLABS;
		 
		return url;
	}

	public static String getAnalyticsServices(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + GET_ANALYTICS;
		 
		return url;
	}

	public static String getTransactionTimeDiff(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + GET_TRX_TIME;
		 
		return url;
	}

	public static String getUPILoadMoneyRequest(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + GET_UPI_LOAD_MONEY;
		 
		return url;
	}

	public static String updateReceiptUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + GET_UPDATE_RECEIPT;
		return url;
	}

	public static String updateReceiptInAjaxUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + GET_UPDATE_RECEIPT_DATEWISE;
		return url;
	}

	public static String userReceiptUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + GET_CREDIT_AND_DEBIT_RECEIPT;
		return url;
	}

	// YUPTV URL

	public static String saveKeyAndSecretURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + SAVE_YUTPV_KEY_SECRET;
		 
		return url;
	}

	public static String CheckRegisterURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + CHECK_YUPTV_REGISTER;
		 
		return url;
	}

	public static String existUserURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + CHECK_EXIST_YUPTV_REGISTER;
		 
		return url;
	}

	public static String getNotifications(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + GET_NOTIFICATIONS;
		 
		return url;
	}

	public static String getNewRegisterUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + NEW_REGISTER;
		return url;
	}

	public static String getUserAnalytics(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + USER_ANALYTICS;
		 
		return url;
	}

	public static String getUserAnalyticsCredits(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + GET_CREDIT_ANAL;
		 
		return url;
	}

	public static String getQwikrPayInitiatePayment(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + QWIKR_PAY_INITIATE;
		 
		return url;
	}

	public static String getQwikrPaySuccessPayment(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + QWIKR_PAY_SUCCESS;
		 
		return url;
	}

	public static String getEditNameUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + EDIT_NAME;
		return url;
	}

	public static String getMerchanttransactionCommision(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR
				+ VPAYQWIK_MERCHANT_TRANSACTIONS_COMMISSION;
		return url;
	}

	public static String getPartnerDetailsURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + PARTNER_INSTALL_REPORTS;
		return url;
	}

	public static String getPartnerDetailsFilterURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + PARTNER_INSTALL_REPORTS_FILTER;
		return url;
	}

	public static String checkVersion(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + VERSION_CHECK;
		return url;
	}

	public static String updateVersions(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + UPDATE_VERSIONS;
		 
		return url;
	}

	public static String getAllVersions(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + GET_ALL_VERSIONS;
		 
		return url;
	}

	// GCI URL
	public static String getGciCredentials(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + GCI_AUTH;
		 
		return url;
	}

	public static String getGciCardInitiateURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + GCI_PAYMENT_INITATE;
		 
		return url;
	}

	/* FOR Travel Flight */

	public static String getAirlineNames(Role role, Device device, Language language) {
		String url = TravelDOMAIN + SEPARATOR + role.getValue() + SEPARATOR + device.getValue() + SEPARATOR
				+ language.getValue() + SEPARATOR + Flight_SOURCE;
		 
		return url;
	}

	public static String getFlightSearchAPI(Role role, Device device, Language language) {
		String url = TravelDOMAIN + SEPARATOR + role.getValue() + SEPARATOR + device.getValue() + SEPARATOR
				+ language.getValue() + SEPARATOR + Flight_Search_API;
		 
		return url;
	}

	public static String getAirRePriceRQAPI(Role role, Device device, Language language) {
		String url = TravelDOMAIN + SEPARATOR + role.getValue() + SEPARATOR + device.getValue() + SEPARATOR
				+ language.getValue() + SEPARATOR + Flight_AirRePriceRQ_API;
		 
		return url;
	}

	public static String getAirBookRQ(Role role, Device device, Language language) {
		String url = TravelDOMAIN + SEPARATOR + role.getValue() + SEPARATOR + device.getValue() + SEPARATOR
				+ language.getValue() + SEPARATOR + Flight_AirBookRQ_API;
		return url;
	}

	public static String getFlightInitiateURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + Flight_FlightBookingInitiate_API;
		 
		return url;
	}

	public static String getFlightSucessURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + Flight_FlightBookingSucees_API;
		 
		return url;
	}

	public static String getFlightPaymentGatewaySucessURL(Version version, Role role, Device device,
			Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + FLIGHT_PAYMENTGATEWAY_API;
		 
		return url;
	}

	public static String getFlightDetailsForAdmin(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR
				+ Flight_BOOK_TICKET_TRAVELLER_ADMIN_API;
		 
		return url;
	}
	

	public static String isFlightCancelable(Role role, Device device, Language language) {
		String url = TravelDOMAIN + SEPARATOR + role.getValue() + SEPARATOR + device.getValue() + SEPARATOR
				+ language.getValue() + SEPARATOR + FLIGHT_ISCANCELABLE_TICKET;
		 
		return url;
	}

	// Refer n Earn

	public static String getRefernEarn(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + REFER_EARN;
		 
		return url;
	}

	public static String getUserByLocation(Version version, Role role, Device device, Language language,
			String locationCode) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + locationCode + SEPARATOR
				+ USER_LOCATION;
		return url;
	}

	/* For Bus */

	public static String checkBusService(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + BUS_SERVICE_CHECK;
		 
		return url;
	}

	public static String getBusCityList(Role role, Device device, Language language) {
		String url = TravelDOMAIN + SEPARATOR + role.getValue() + SEPARATOR + device.getValue() + SEPARATOR
				+ language.getValue() + SEPARATOR + BUS_GET_ALL_CITY_LIST;
		 
		return url;
	}

	public static String getBusSourceCityList(Role role, Device device, Language language) {
		String url = TravelDOMAIN + SEPARATOR + role.getValue() + SEPARATOR + device.getValue() + SEPARATOR
				+ language.getValue() + SEPARATOR + BUS_GET_SOURCE_CITY_LIST;
		 
		return url;
	}

	public static String getBusDestinationCityList(Role role, Device device, Language language) {
		String url = TravelDOMAIN + SEPARATOR + role.getValue() + SEPARATOR + device.getValue() + SEPARATOR
				+ language.getValue() + SEPARATOR + BUS_GET_DESTINATION_CITY_LIST;
		 
		return url;
	}

	public static String getAllAvailableTrip(Role role, Device device, Language language) {
		String url = TravelDOMAIN + SEPARATOR + role.getValue() + SEPARATOR + device.getValue() + SEPARATOR
				+ language.getValue() + SEPARATOR + BUS_GET_ALL_AVAILABLE_TRIPS;
		 
		return url;
	}

	public static String getseatDetails(Role role, Device device, Language language) {
		String url = TravelDOMAIN + SEPARATOR + role.getValue() + SEPARATOR + device.getValue() + SEPARATOR
				+ language.getValue() + SEPARATOR + BUS_GET_SEAT_DETAILS;
		 
		return url;
	}

	public static String getTxnId(Role role, Device device, Language language) {
		String url = TravelDOMAIN + SEPARATOR + role.getValue() + SEPARATOR + device.getValue() + SEPARATOR
				+ language.getValue() + SEPARATOR + BUS_GET_TXN_ID;
		 
		return url;
	}

	public static String getCancelInitMdexPayment(Role role, Device device, Language language) {
		String url = TravelDOMAIN + SEPARATOR + role.getValue() + SEPARATOR + device.getValue() + SEPARATOR
				+ language.getValue() + SEPARATOR + BUS_CANCEL_MDEX_INIT_PAYMENT;
		 
		return url;
	}

	public static String cronCheck(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + BUS_CRON_CHECK_API;
		 
		return url;
	}

	public static String getAllTicketByUser(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + BUS_BOOK_TICKET_API;
		 
		return url;
	}

	public static String getAllTicketByUserForWeb(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + BUS_BOOK_TICKET_WEB_API;
		 
		return url;
	}

	public static String getSingleTicketTravellerDetails(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + BUS_BOOK_TICKET_TRAVELLER_WEB_API;
		 
		return url;
	}

	public static String getTravellerDetailsForAdmin(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + BUS_BOOK_TICKET_TRAVELLER_ADMIN_API;
		 
		return url;
	}

	
	public static String createBusTicketPdfForBus(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + BUS_BOOK_TICKET_PDF_API;
		 
		return url;
	}

	public static String getBusDetailForAdminURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + BUS_DETAILS_FOR_ADMIN_API;
		return url;
	}

	public static String getAllCityListFormDB(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + BUS_GET_ALL_CITY_LIST_FROM_DB;
		 
		return url;
	}

	/* Change for price recheck */

	public static String getTxnIdUpdated(Role role, Device device, Language language) {
		String url = TravelDOMAIN + SEPARATOR + role.getValue() + SEPARATOR + device.getValue() + SEPARATOR
				+ language.getValue() + SEPARATOR + BUS_GET_TXN_ID_UPDATED;
		 
		return url;
	}

	public static String bookTicketUpdated(Role role, Device device, Language language) {
		String url = TravelDOMAIN + SEPARATOR + role.getValue() + SEPARATOR + device.getValue() + SEPARATOR
				+ language.getValue() + SEPARATOR + BUS_GET_BOOK_TICKET_UPDATED;
		 
		return url;
	}

	public static String saveTxnIdURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + BUS_SAVEGETTXNID_API;
		 
		return url;
	}

	public static String saveSeatDetailsURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + BUS_SAVE_SEAT_DETAILS_API;
		 
		return url;
	}

	public static String busPaymentURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + BUS_BOOKTICKET_PAYMENT_API;

		System.err.println("Url: " + url);
		 
		return url;
	}

	public static String busPaymentInit(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + BUS_PAYMENT_INIT_API;
		 
		return url;
	}

	public static String getDynamicFareDetails(Role role, Device device, Language language) {
		String url = TravelDOMAIN + SEPARATOR + role.getValue() + SEPARATOR + device.getValue() + SEPARATOR
				+ language.getValue() + SEPARATOR + BUS_GET_DYNAMIC_FARE_DETAILS;
		 
		return url;
	}

	/* Closed Change for price recheck */

	public static String getSplitpaymentUrlFlight(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + LOAD_MONEY_Splitpayment;
		return url;
	}

	public static String getSplitpaymentUrlFlightUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + LOAD_MONEY_REDIRECT_Splitpayment;
		return url;
	}

	public static String getBookTicket(Role role, Device device, Language language) {
		String url = TravelDOMAIN + SEPARATOR + role.getValue() + SEPARATOR + device.getValue() + SEPARATOR
				+ language.getValue() + SEPARATOR + BUS_GET_BOOK_TICKET;
		 
		return url;
	}

	public static String isCancelable(Role role, Device device, Language language) {
		String url = TravelDOMAIN + SEPARATOR + role.getValue() + SEPARATOR + device.getValue() + SEPARATOR
				+ language.getValue() + SEPARATOR + BUS_GET_ISCANCELABLE_TICKET;
		 
		return url;
	}

	public static String cancelTicket(Role role, Device device, Language language) {
		String url = TravelDOMAIN + SEPARATOR + role.getValue() + SEPARATOR + device.getValue() + SEPARATOR
				+ language.getValue() + SEPARATOR + BUS_GET_CANCEL_TICKET;
		 
		return url;
	}

	public static String getBusInitiateURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + BUS_BookingInitiate_API;
		 
		return url;
	}

	public static String getBusPaymentURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + BUS_BookingSucees_API;
		 
		return url;
	}

	public static String getCancelInitBusPaymentURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + BUS_CANCEL_INIT_PAYMENT_API;
		 
		return url;
	}

	public static String getCancelBusBookedTicketURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + BUS_CANCEL_BOOKED_TICKET_API;
		 
		System.err.println(url);
		return url;
	}

	public static String getCancelAgentBusBookedTicketURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + BUS_CANCEL_BOOKED_TICKET_API;
		 
		System.err.println(url);
		return url;
	}
	
	public static String getNearByMerchantsUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + NEAR_BY_MERCHNAT_DETAILS;
		return url;
	}

	public static String getRedeemPointsUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + REDEEM_POINTS;
		return url;
	}

	public static String getAirRePriceRQAPIConnecting(Role role, Device device, Language language) {
		String url = TravelDOMAIN + SEPARATOR + role.getValue() + SEPARATOR + device.getValue() + SEPARATOR
				+ language.getValue() + SEPARATOR + Flight_AirRePriceRQ_API_Connecting;
		 
		return url;
	}

	public static String getPayMentGateWayUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + Flight_BOOK_EBS_API;
		 
		return url;
	}

	public static String getVnetTxn(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + Flight_VNETTXN;
		 
		return url;
	}

	public static String getFlightInitURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + Flight_FlightBookingInit_API;
		 
		return url;
	}

	public static String getFlightPaymentSucessURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + Flight_FlightBookingPaySucees_API;
		 
		return url;
	}

	public static String getFlightPaymentGetwaySucessURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR
				+ Flight_FlightBookingPaymentGatewaySucees_API;
		 
		return url;
	}

	public static String getMyFlightTickets(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + Flight_BOOK_TICKETS_API;
		 
		return url;
	}

	// PlaceOrder Initiate
	public static String trainListFromDB(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + GET_TRAIN_LIST_FROM_DB;
		 
		return url;
	}
	public static String paymentInitateURLTK(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + PAYMENT_INITATE_TK;
		 
		return url;
	}

	// MyOrder
	public static String myOrderTK(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + My_Order;
		 
		return url;
	}

	/*
	 * //PlaceOrder Success public static String paymentSuccessURLTK(Version
	 * version, Role role, Device device, Language language) { String url =
	 * DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() +
	 * SEPARATOR + device.getValue() + SEPARATOR + language.getValue() +
	 * SEPARATOR + PAYMENT_SUCCESS_TK;   return url; }
	 */

	public static String flightCronCheck(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + FLIGHT_CRON_CHECK_API;
		 
		return url;
	}

	public static String getAirLineNamesFromDB(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + FLIGHT_GET_CITY_FROM_DB_API;
		 
		return url;
	}
	public static String aGetAirLineNamesFromDB(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + Agent_FLIGHT_GET_CITY_FROM_DB_API;
		 
		return url;
	}

	public static String getTreatCardDetails(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + TreatCard_Details;
		 
		return url;
	}

	public static String getNikkiTransactionFilteredUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + NIKKI_TRANSACTION_FILTERED;
		return url;
	}

	public static String getImagicaTransactionFilteredUrl(Version version, Role role, Device device,
			Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + IMAGICA_TRANSACTION_FILTERED;
		return url;
	}

	// for debit transaction

	public static String getDebitTransactionUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + DEBIT_TRANSACTION_FILTERED;
		return url;
	}

	// for credit

	public static String getCreditTransactionUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + CREDIT_TRANSACTION_FILTERED;
		return url;
	}

	// for single user gcm id

	public static String getAdminGCMIDByUserName(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + GCM_ID_BY_USERNAME;
		 
		return url;
	}

	public static String getTreatCardPlansUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + TREATCARD_PLANS;
		return url;
	}

	public static String getUserTreatCardPlansUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + TREATCARD_PLANS_USER;
		return url;
	}

	public static String updateTreatCardPlansUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + UPDATE_TREATCARD_PLANS;
		return url;
	}

	public static String updateTreatCardPaymentPlansUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + UPDATE_TREATCARD_PAYMENT;
		return url;
	}

	// refund
	public static String refundRequest(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + REFUND_MERCHANT;
		return url;
	}

	public static String refundTransactionRequest(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + REFUND_TRANSACTION;
		 
		return url;
	}

	public static String getBusDetailForAdminURLByDate(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + BUS_DETAILS_FOR_ADMIN_API_BY_DATE;
		return url;
	}
	public static String agentGetBusDetailForAdminURLByDate(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + Agent_BUS_DETAILS_FOR_ADMIN_API_BY_DATE;
		return url;
	}

	public static String getAllFlightsDetailURLByDate(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + FlightsDetailURLByDate;
		return url;
	}

	// Travelkhana Order list
	public static String getTKOrderDetailForAdminURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + Order_DETAILS_FOR_ADMIN_API;
		return url;
	}
	public static String getTKOrderDetailByDateForAdmin(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + Order_DETAILS_BY_DATE_FOR_ADMIN_API;
		return url;
	}


	public static String compareCity(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + FLIGHT_COMPARE_CITY_API;
		 
		return url;
	}

	// HouseJoy

	public static String getHouseJoyInitiatePayment(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + HOUSE_JOY_INITIATE;
		 
		return url;
	}

	public static String getHouseJoySuccessPayment(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + HOUSE_JOY_SUCCESS;
		 
		return url;
	}

	public static String getHouseJoyCancelPayment(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + HOUSE_JOY_FAILURE;
		 
		return url;
	}

	// Hotel

	public static String getHotelList(Role role, Device device, Language language) {
		String url = TravelDOMAIN + SEPARATOR + role.getValue() + SEPARATOR + device.getValue() + SEPARATOR
				+ language.getValue() + SEPARATOR + HOTEL_SEARCH_LIST;
		 
		return url;
	}

	public static String getHotelInfo(Role role, Device device, Language language) {
		String url = TravelDOMAIN + SEPARATOR + role.getValue() + SEPARATOR + device.getValue() + SEPARATOR
				+ language.getValue() + SEPARATOR + HOTEL_INFO;
		 
		return url;
	}

	public static String getHotelAvailablity(Role role, Device device, Language language) {
		String url = TravelDOMAIN + SEPARATOR + role.getValue() + SEPARATOR + device.getValue() + SEPARATOR
				+ language.getValue() + SEPARATOR + HOTEL_AVAILABLITY;
		 
		return url;
	}

	public static String getHotelBook(Role role, Device device, Language language) {
		String url = TravelDOMAIN + SEPARATOR + role.getValue() + SEPARATOR + device.getValue() + SEPARATOR
				+ language.getValue() + SEPARATOR + HOTEL_BOOK;
		 
		return url;
	}

	// HouseJoy

	public static String getAllHouseJoyDetailURLByDate(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + HOUSE_JOYDetailURLByDate;
		return url;
	}

	public static String getAllHouseJoyDetailURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + HOUSE_JOYDetailURL;
		return url;
	}

	// Graph

	public static String transactionCountByMonthWise(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + TRANSACTION_COUNT_MONTHWISE;
		return url;
	}

	public static String userCountByMonthWise(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + USER_COUNT_MONTHWISE;
		return url;
	}

	public static String getBulkUploadUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + BULK_UPLOAD;
		 
		return url;
	}

	public static String transactionCountByMonthWiseSingleUser(Version version, Role role, Device device,
			Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR
				+ TRANSACTION_COUNT_MONTHWISE_SINGLE_USER;
		return url;
	}

	public static String getAllUserCount(Version version, String role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role + SEPARATOR + device.getValue()
		+ SEPARATOR + language.getValue() + SEPARATOR + GET_ALL_USER_FROM_DB;
		 
		return url;
	}

	public static String getDebitList(Version version, String role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role + SEPARATOR + device.getValue()
		+ SEPARATOR + language.getValue() + SEPARATOR + GET_DEBIT_LIST_FROM_DB;
		 
		return url;
	}

	public static String getCreditList(Version version, String role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role + SEPARATOR + device.getValue()
		+ SEPARATOR + language.getValue() + SEPARATOR + GET_CREDIT_LIST_FROM_DB;
		 
		return url;
	}

	public static String getServiceList(Version version, String role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role + SEPARATOR + device.getValue()
		+ SEPARATOR + language.getValue() + SEPARATOR + GET_SERVICE_LIST_FROM_DB;
		 
		return url;
	}

	public static String getDateWiseTrans(Version version, String role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role + SEPARATOR + device.getValue()
		+ SEPARATOR + language.getValue() + SEPARATOR + GET_DATE_WISE_TRANS;
		 
		return url;
	}
	public static String getSuperAdminList(Version version, String role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR +GET_SUPERADMIN_LIST;
		 
		return url;
	}

	public static String changepasswordSuperAdmin(Version version, String role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR +CHANGE_PASSWORD_SUPERADMIN;
		 
		return url;
	}
	public static String addSuperAdmin(Version version, String role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role+ SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + ADD_SUPERADMIN_LIST_PAYQWIK;
		 
		return url;
	}
	public static String blockSuperAdmin(Version version, String role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role+ SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + BLOCK_SUPERADMIN_PAYQWIK;
		 
		return url;
	}

	public static String getBulkFileList(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + SUPERADMIN_BULKFILE_LIST;
		System.err.println(url);
		return url;
	}

	public static String uploadBulkFileList(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + SUPERADMIN_BULKFILE_LIST_RECORD;
		System.err.println(url);
		return url;
	}

	public static String VPayQwikListCount(Version version, String role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role + SEPARATOR + device.getValue()
		+ SEPARATOR + language.getValue() + SEPARATOR + GET_VPAYQWIK_MAIN_COUNT;
		 
		return url;
	}

	public static String getMerchantNeftListFilteredURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + MERCHANT_NEFT_LIST_FILTERED;
		return url;
	}
	public static String getAgentNeftListFilteredURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + AGENT_NEFT_LIST_FILTERED;
		return url;
	}

	// Woohoo
	private static final String GET_WOOHOO_CATEGORIES = "woohoo/category";
	private static final String GET_WOOHOO_PRODUCTS_BY_CATEGORIES = "woohoo/category";
	private static final String GET_WOOHOO_PRODUCTS_DESCRIPTION = "woohoo/product";
	private static final String GET_WOOHOO_PRODUCTS_STATUS = "woohoo/status/";
	private static final String GET_WOOHOO_PRODUCTS_RESEND = "woohoo/resend/";
	private static final String GET_WOOHOO_PRICE_RECHECK = "woohoo/orderhandling";
	private static final String PLACE_ORDER = "woohoo/spend";
	private static final String PROCES_PAYMENT = "Woohoo/PaymentResponse";
	private static final String INITIATE_PAYMENT = "Woohoo/InitiatePayment";
	private static final String INITIATE_PAYMENT_NEW= "Woohoo/WoohooProcessOrder";
	private static final String WOOHOO_GIFT_LIST= "FetchsAllCardDetails";
	private static final String GET_WOOHOO_PRODUCTS = "woohoo/productList";

	private static final String MDEX_PREPAID_RECHARGE = "ipay/Prepaid";
	private static final String MDEX_DATACARD_RECHARGE = "ipay/Datacard";
	private static final String MDEX_DTH_RECHARGE = "ipay/DTH";
	private static final String MDEX_POSTPAID_RECHARGE = "ipay/Postpaid";
	private static final String MDEX_LANDLINE_RECHARGE = "ipay/Landline";
	private static final String MDEX_ELECTRICITY_BILLPAY = "ipay/Electricity";
	private static final String MDEX_STATUS = "ipay/Status";
	
	

	
	
	private static final String Get_Pay_Amount = "payAmount";

	
	public static String getMdexPrepaidRechargeUrl(Version version, Role role, Device device, Language language) {
		String url = MDEX_TRAVELHOST + SEPARATOR +"ws"+SEPARATOR+"api"+SEPARATOR+ version.getValue() + SEPARATOR + role.getValue() + SEPARATOR + language.getValue() + SEPARATOR + MDEX_PREPAID_RECHARGE;
		 
		return url;
	}
	
	public static String getMdexDataCardRechargeUrl(Version version, Role role, Device device, Language language) {
		String url = MDEX_TRAVELHOST + SEPARATOR +"ws"+SEPARATOR+"api"+SEPARATOR+ version.getValue() + SEPARATOR + role.getValue() + SEPARATOR + language.getValue() + SEPARATOR + MDEX_DATACARD_RECHARGE;
		 
		return url;
	}
	
	public static String getMdexDthRechargeUrl(Version version, Role role, Device device, Language language) {
		String url = MDEX_TRAVELHOST + SEPARATOR +"ws"+SEPARATOR+"api"+SEPARATOR+ version.getValue() + SEPARATOR + role.getValue() + SEPARATOR + language.getValue() + SEPARATOR + MDEX_DTH_RECHARGE;
		 
		return url;
	}
	
	public static String getMdexPostpaidRechargeUrl(Version version, Role role, Device device, Language language) {
		String url = MDEX_TRAVELHOST + SEPARATOR +"ws"+SEPARATOR+"api"+SEPARATOR+ version.getValue() + SEPARATOR + role.getValue() + SEPARATOR + language.getValue() + SEPARATOR + MDEX_POSTPAID_RECHARGE;
		 
		return url;
	}
	
	public static String getMdexLandlineRechargeUrl(Version version, Role role, Device device, Language language) {
		String url = MDEX_TRAVELHOST + SEPARATOR +"ws"+SEPARATOR+"api"+SEPARATOR+ version.getValue() + SEPARATOR + role.getValue() + SEPARATOR + language.getValue() + SEPARATOR + MDEX_LANDLINE_RECHARGE;
		 
		return url;
	}
	
	public static String getMdexElectricityBillPayUrl(Version version, Role role, Device device, Language language) {
		String url = MDEX_TRAVELHOST + SEPARATOR +"ws"+SEPARATOR+"api"+SEPARATOR+ version.getValue() + SEPARATOR + role.getValue() + SEPARATOR + language.getValue() + SEPARATOR + MDEX_ELECTRICITY_BILLPAY;
		 
		return url;
	}
	
	public static String getMdexStatusUrl(Version version, Role role, Device device, Language language) {
		String url = MDEX_TRAVELHOST + SEPARATOR +"ws"+SEPARATOR+"api"+SEPARATOR+ version.getValue() + SEPARATOR + role.getValue() + SEPARATOR + language.getValue() + SEPARATOR + MDEX_STATUS;
		 
		return url;
	}
	
	public static String getCategories(Version version, Role role, Device device, Language language) {
		String url = TravelDOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + GET_WOOHOO_CATEGORIES;
		 
		return url;
	}

	public static String getProductsByCategories(Version version, Role role, Device device, Language language,
			String categoryId) {
		String url = TravelDOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + GET_WOOHOO_PRODUCTS_BY_CATEGORIES
				+ SEPARATOR + categoryId;
		 
		return url;
	}

	public static String getProductsDesciption(Version version, Role role, Device device, Language language,
			String productId) {
		String url = TravelDOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + GET_WOOHOO_PRODUCTS_DESCRIPTION
				+ SEPARATOR + productId;
		 
		return url;
	}

	public static String getStatusCheck(Version version, Role role, Device device, Language language,
			String productId) {
		String url = TravelDOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + GET_WOOHOO_PRODUCTS_STATUS
				+ productId;
		 
		return url;
	}
	
	
	public static String getResend(Version version, Role role, Device device, Language language,
			String productId) {
		String url = TravelDOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + GET_WOOHOO_PRODUCTS_RESEND
				+ productId;
		 
		return url;
	}
	
	
	public static String getPriceRecheck(Version version, Role role, Device device, Language language) {
		String url = TravelDOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + GET_WOOHOO_PRICE_RECHECK;
		 
		return url;
	}

	public static String processTransaction(Version version, Role role, Device device, Language language) {
		String url = TravelDOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + PLACE_ORDER;
		 
		return url;
	}

	public static String initiatePayment(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + INITIATE_PAYMENT;
		 
		return url;
	}
	
	public static String processOrder(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + INITIATE_PAYMENT_NEW;
		 
		return url;
	}

	public static String processPayment(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + PROCES_PAYMENT;
		 
		return url;
	}

	public static String getWoohooGiftCardListUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + WOOHOO_GIFT_LIST;
		return url;
	}


	public static String getRegisterListUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + TREATCARD_REGISTER_LIST;
		return url;
	}

	public static String getFilteredRegisterListUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + TREATCARD_FILTERED_REGISTER_LIST;
		return url;
	}

	public static String getTreatCardTransactionUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + TREATCARD_TRANSACTION_LIST;
		return url;
	}

	public static String getFilteredTreatCardTransactionUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + TREATCARD_FILTERED_TRANSACTION_LIST;
		return url;
	}

	public static String getTreatCardCountUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + TREATCARD_COUNT_LIST;
		return url;
	}

	public static String getProductsList(Version version, Role role, Device device, Language language) {
		String url = TravelDOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + GET_WOOHOO_PRODUCTS;
		return url;
	}


	/*Admin Panel Report*/

	public static String getWalletBalance(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + WALLET_BALANCE;
		return url;
	}

	public static String getDailyValuesListURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + DAILY_VALUES_LIST;
		return url;
	}

	public static String getMISReport(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + MISREPORT;
		 
		return url;
	}

	public static String getWalletLoadReport(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + WALLETLOADREPORT;
		return url;
	}
	public static String getWalletBalanceByDate(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + WALLET_BALANCE_BY_DATE;
		return url;
	}

	public static String getPromoTransactionsFilteredSuperAdminURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + PROMO_TRANSACTIONS_FILTERED_SUPERADMIN;
		return url;
	}

	public static String getAllIpayRefundTxnsAdminURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR
				+ ADMIN_REFUND_INSTANTPAY_TRANSACTIONS;
		 
		return url;
	}


	public static String getSuperAdminGCMIDByUserName(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + GCM_ID_BY_USERNAME_SUPERADMIN;
		return url;
	}

	public static String saveGCMIDs(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + SUPERADMIN_SAVE_NOTIFICATION;
		return url;
	}


	public static String getUserList(Version version, String role, Device device, Language language,String userType) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role+ SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + GET_USERLIST + SEPARATOR + userType ;
		return url;
	}
	public static String userProfile(Version version, String role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role+ SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + GET_USERPROFILE ;
		 
		return url;
	}
	public static String blockUserOfMasterAdminUrl(Version version, String role, Device device, Language language,String user) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role+ SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + GET_USERBLOCK + SEPARATOR + user;
		return url;
	}

	public static String unblockUserOfMasterAdminUrl(Version version, String role, Device device, Language language,String user) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role+ SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + GET_USERUNBLOCK+ SEPARATOR + user;
		 
		return url;
	}
	public static String kycMasterAdminUrl(Version version, String role, Device device, Language language,String user) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role+ SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + GET_USERKYC+ SEPARATOR + user;
		 
		return url;
	}

	public static String nonkycMasterAdminUrl(Version version, String role, Device device, Language language,String user) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role+ SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + GET_USERNONKYC+ SEPARATOR + user;
		 
		return url;
	}
	public static String getUserListFilter(Version version, String role, Device device, Language language,String userType) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role+ SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + GET_USERLIST_FILTER + SEPARATOR + userType ;
		return url;
	}

	public static String gcmnotification(Version version, String role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role+ SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + GET_GCM_NOTIFICATIONS;
		return url;
	}
	public static String sendsms(Version version, String role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role+ SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + SEND_SUPERADMIN_SMS;
		return url;
	}

	public static String sendbulksms(Version version, String role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role+ SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + SEND_SUPERADMIN_BULKSMS;
		return url;
	}
	public static String sendmail(Version version, String role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role+ SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + SEND_SUPERADMIN_MAIL;
		return url;
	}

	public static String sendbulkmail(Version version, String role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role+ SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + SEND_SUPERADMIN_BULKMAIL;
		return url;
	}
	
	public static String saveAadharUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + SAVE_AADHAR_DB;
		return url;
	}
	
	/* For Agent Bus */

/*	public static String agentCheckBusService(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + Agent_BUS_SERVICE_CHECK;
		 
		return url;
	}*/

	public static String agentGetBusCityList(Role role, Device device, Language language) {
		String url = TravelDOMAIN + SEPARATOR + role.getValue() + SEPARATOR + device.getValue() + SEPARATOR
				+ language.getValue() + SEPARATOR + BUS_GET_ALL_CITY_LIST;
		return url;
	}

	public static String agentGetBusSourceCityList(Role role, Device device, Language language) {
		String url = TravelDOMAIN + SEPARATOR + role.getValue() + SEPARATOR + device.getValue() + SEPARATOR
				+ language.getValue() + SEPARATOR +BUS_GET_SOURCE_CITY_LIST;
		return url;
	}

	public static String agentGetBusDestinationCityList(Role role, Device device, Language language) {
		String url = TravelDOMAIN + SEPARATOR + role.getValue() + SEPARATOR + device.getValue() + SEPARATOR
				+ language.getValue() + SEPARATOR + BUS_GET_DESTINATION_CITY_LIST;
		 
		return url;
	}

	public static String agentGetAllAvailableTrip(Role role, Device device, Language language) {
		String url = TravelDOMAIN + SEPARATOR + role.getValue() + SEPARATOR + device.getValue() + SEPARATOR
				+ language.getValue() + SEPARATOR +BUS_GET_ALL_AVAILABLE_TRIPS;
		 
		return url;
	}

	public static String agentGetseatDetails(Role role, Device device, Language language) {
		String url = TravelDOMAIN + SEPARATOR + role.getValue() + SEPARATOR + device.getValue() + SEPARATOR
				+ language.getValue() + SEPARATOR + BUS_GET_SEAT_DETAILS;
		 
		return url;
	}

	public static String agentGetTxnId(Role role, Device device, Language language) {
		String url = TravelDOMAIN + SEPARATOR + role.getValue() + SEPARATOR + device.getValue() + SEPARATOR
				+ language.getValue() + SEPARATOR +BUS_GET_TXN_ID;
		 
		return url;
	}

	public static String agentGetCancelInitMdexPayment(Role role, Device device, Language language) {
		String url = TravelDOMAIN + SEPARATOR + role.getValue() + SEPARATOR + device.getValue() + SEPARATOR
				+ language.getValue() + SEPARATOR + BUS_CANCEL_MDEX_INIT_PAYMENT;
		return url;
	}

	public static String agentCronCheck(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + Agent_BUS_CRON_CHECK_API;
		return url;
	}

	public static String agentGetAllTicketByUser(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + Agent_BUS_BOOK_TICKET_API;
		return url;
	}

	public static String agentGetAllTicketByUserForWeb(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + Agent_BUS_BOOK_TICKET_WEB_API;
		return url;
	}

	public static String agentGetSingleTicketTravellerDetails(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + Agent_BUS_BOOK_TICKET_TRAVELLER_WEB_API;
		return url;
	}

	public static String agentGetTravellerDetailsForAdmin(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + Agent_BUS_BOOK_TICKET_TRAVELLER_ADMIN_API;
		return url;
	}

	public static String agentCreateBusTicketPdfForBus(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + Agent_BUS_BOOK_TICKET_PDF_API;
		return url;
	}

	public static String agentGetBusDetailForAdminURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + Agent_BUS_DETAILS_FOR_ADMIN_API;
		return url;
	}

	public static String agentGetAllCityListFormDB(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + Agent_BUS_GET_ALL_CITY_LIST_FROM_DB;
		 
		return url;
	}

	public static String agentGetCancelInitBusPaymentURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + Agent_BUS_CANCEL_INIT_PAYMENT_API;
		 
		return url;
	}

	public static String agentGetCancelBusBookedTicketURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + Agent_BUS_CANCEL_BOOKED_TICKET_API;
		 
		System.err.println(url);
		return url;
	}
	
	/* Change for Agent flight */

	public static String agentSaveTxnIdURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + Agent_BUS_SAVEGETTXNID_API;
		return url;
	}

	public static String agentSaveSeatDetailsURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + Agent_BUS_SAVE_SEAT_DETAILS_API;
		return url;
	}

	public static String agentBusPaymentURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + Agent_BUS_BOOKTICKET_PAYMENT_API;
		return url;
	}

	public static String agentBusPaymentInit(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + Agent_BUS_PAYMENT_INIT_API;
		 
		return url;
	}
	
	public static String agentGetFlightInitiateURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + Agent_Flight_FlightBookingInitiate_API;
		 
		return url;
	}

	public static String agentGetFlightSucessURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + Agent_Flight_FlightBookingSucees_API;
		 
		return url;
	}

	public static String agentGetFlightPaymentGatewaySucessURL(Version version, Role role, Device device,
			Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + Agent_FLIGHT_PAYMENTGATEWAY_API;
		 
		return url;
	}

	public static String agentGetFlightDetailsForAdmin(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR
				+ Agent_Flight_BOOK_TICKET_TRAVELLER_ADMIN_API;
		return url;
	}
	
	public static String agentGetAllFlightsDetailURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + Agent_FlightsDetailURL;
		return url;
	}

	public static String agentGetSplitpaymentUrlFlight(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + Agent_LOAD_MONEY_Splitpayment;
		return url;
	}

	public static String agentGetSplitpaymentUrlFlightUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + Agent_LOAD_MONEY_REDIRECT_Splitpayment;
		return url;
	}
	
	public static String agentGetLoadMoneyVNetFlightUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR +  Agent_LOAD_MONEY_VNET_FLIGHT;
		return url;
	}
	public static String agentGetLoadMoneyVNetResponseFlightUrl(Version version, Role role, Device device,
			Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + Agent_LOAD_MONEY_REDIRECT_VNET_FLIGHT;
		return url;
	}
	public static String agentGetPayMentGateWayUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + Agent_Flight_BOOK_EBS_API;
		return url;
	}
	public static String agentGetFlightInitURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + Agent_Flight_FlightBookingInit_API;
		return url;
	}
	
	public static String agentGetFlightInitiateSplitURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + AGENT_FLIGHT_INITIATE_SPLIT_API;
		return url;
	}
	public static String agentGetFlightPaymentSucessURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + Agent_Flight_FlightBookingPaySucees_API;
		return url;
	}

	public static String agentGetFlightPaymentGetwaySucessURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR
				+ Agent_Flight_FlightBookingPaymentGatewaySucees_API;
		return url;
	}
	public static String agentGetMyFlightTickets(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + Agent_Flight_BOOK_TICKETS_API;
		return url;
	}
	public static String agentFlightCronCheck(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + Agent_FLIGHT_CRON_CHECK_API;
		return url;
	}
	public static String agentCompareCity(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + Agent_FLIGHT_COMPARE_CITY_API;
		return url;
	}
	public static String agentGetAllFlightsDetailURLByDate(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + Agent_FlightsDetailURLByDate;
		return url;
	}
	public static String getUserOTPUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + GET_USEROTP_AADHAR;
		return url;
	}
	
	public static String getUserOTPValidateUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + GET_USEROTP_AADHAR_VALIDATE;
		return url;
	}
	public static String agentResendMobileOtp(Version version, Role role, Device device, Language language) {
		String url= DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR 
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + Resend_Mobile_OTP;
		return url;
	}

	public static String getUserAmount(Version version, Role role, Device device, Language language) {
		String url= DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR 
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + Get_Pay_Amount;
		return url;
	}

	
	public static String getRefundTransactionsURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + GET_REFUND_TRANSACTION;
		 
		return url;
	}
	
	public static String getSuperadminMerchantRefund(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + SUPERADMIN_MERCHANT_REFUND;
		return url;
	}
	

	public static String addOffers(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + ADD_OFFERS;
return url;
	}
	
	public static String getDataConfigListUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + DATA_CONFIG_LIST;

		return url;
	}
	
	
	public static String updateMdexSwitch(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + UPDATE_MDEX;
		 
		return url;
	}
	
	public static String findNearByMerchantsUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + FIND_NEAR_BY_MERCHNAT_DETAILS;
		return url;
	}
	
	public static String findNearByAgentsUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + FIND_NEAR_BY_AGENT_DETAILS;
		return url;
	}
	
	public static String updateUserMaxLimit(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + UPDATE_USER_MAX_LIMIT;
		return url;
	}
	
	public static String getUserMaxLimit(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + GET_USER_MAX_LIMIT;
		return url;
	}
	
	public static String sendMaxLimitOtp(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + SEND_MAX_LIMIT_OTP;
		return url;
	}
	
	public static String validateUserPassword(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + VALIDATE_PASSWORD;
		return url;
	}
	
	
	public static String getAllMatchesUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + SCHEDULE;
		return url;
	}
	
	public static String predictionUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + PREDICTION;
		return url;
	}
	
	public static String userPredictionDataUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + USER_PREDICTION;
		return url;
	}
	
	public static String saveClaimDetails(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + SAVE_CLAIM_DETAILS;
		return url;
	}
	
	public static String getPredictAndWinUploadUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + PREDICTANDWIN_UPLOAD;
		 
		return url;
	}

	public static String getPromoCode(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + EDIT_PROMOCODE;
		 
		return url;
	}
	
	public static String addServices(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + ADD_SERVICES;
		return url;
	}
	
	public static String getOperatorOfSuperAdminURL(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + GET_OPERATOR_SUPERADMIN;
		return url;
	}
	
	public static String getAadharDetailsOfUsers(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + GET_ALL_AADHAR_DETAILS;
		 
		return url;
	}
	
	public static String addServiceType(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + ADD_SERVICE_TYPE;
		return url;
	}
	
	public static String getAgentDetails(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + EDIT_AgentList;
		 
		return url;
	}
	
	public static String getLoadMoneyRazorPayUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + LOAD_MONEY_RAZORPAY;
		return url;
	}
	
	public static String getSuccessLoadMoneyRazorPayUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + LOAD_MONEY_RAZORPAY_SUCCESS;
		return url;
	}
	
	public static String getMechantBulkUploadUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + BULK_UPLOAD_Merchant;
		 
		return url;
	}
	
	public static String getMerchantReport(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + VIJAYA_MERCHANT_REPORT;
		return url;
	}
	
	public static String getFilteredMerchantReport(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + VIJAYA_MERCHANT_REPORT_FILTERED;
		return url;
	}
	
	public static String saveSDKSeatDetailsURL(Version version, Role role, String device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device + SEPARATOR + language.getValue() + SEPARATOR + BUS_SAVE_SEAT_DETAILS_SDK;
		 
		return url;
	}
	
	public static String getSDKMerchantRegisterUrl(Version version, Role role, String device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device + SEPARATOR + language.getValue() + SEPARATOR + SDK_REGISTER;
		return url;
	}
	
	public static String getSDKMerchantResendOTPUrl(Version version, Role role, String device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device + SEPARATOR + language.getValue() + SEPARATOR + SDK_RESEND_OTP;
		return url;
	}

	public static String addVouchers(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + ADD_VOUCHERS;
		return url;
	}
	
	public static String getLoadMoneyByVoucherUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + LOAD_MONEY;
		return url;
	}
	
	public static String getAllVouchersUrl(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + ALL_VOUCHERS;
		return url;
	}
	public static String getAllVouchersByDate(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + ALL_VOUCHERS_BY_DATE;
		return url;
	}
	
	public static String getVoucherLoadMoney(Version version, Role role, Device device, Language language) {
		String url = DOMAIN + SEPARATOR + version.getValue() + SEPARATOR + role.getValue() + SEPARATOR
				+ device.getValue() + SEPARATOR + language.getValue() + SEPARATOR + LOAD_MONEY_WITH_VOUCHER;
		return url;
	}
	
}
