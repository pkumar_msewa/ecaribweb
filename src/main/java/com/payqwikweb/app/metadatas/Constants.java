package com.payqwikweb.app.metadatas;

public class Constants {

	public static final String MDEX_CLIENTKEY = getMdexClientKey();
	public static final String MDEX_CLIENTTOKEN = getMdexClientToken();
	private static final String MDEX_TRAVELHOST = getMdexTravelHost();

	public static final String MDEX_CLIENTKEY_TEST = "5CF67A47D71A60F6759DDDC44C41D36E3B1E0D75";
	public static final String MDEX_CLIENTTOKEN_TEST = "MTUwMTA5MzU3NjIxNzM1NjAwMDAwMDAwMDAwMDU=";
	private static final String MDEX_TRAVELHOST_TEST = "http://demomdex.msewa.com";

	public static final String MDEX_CLIENTKEY_LIVE = "DDB6226DAFD21DB2FD8A8AE569A9B282D1731087";
	public static final String MDEX_CLIENTTOKEN_LIVE = "MTUwMzkyMjc3OTQ2MzM1NjAwMDAwMDAwMDAwMDY=";
	private static final String MDEX_TRAVELHOST_LIVE = "https://mdex.msewa.com";

	public static final String URL_TEAM_LIST = MDEX_TRAVELHOST + "/Api/v1/Client/Android/en/ipl2018/schedule";
	public static final String URL_TEAM_PREDICT = MDEX_TRAVELHOST + "/Api/v1/Client/Android/en/ipl2018/predictmatch";
	public static final String URL_TEAM_MY_PREDICT = MDEX_TRAVELHOST+ "/Api/v1/Client/Android/en/ipl2018/mypredictions";
	public static final boolean IPL_ENABLE = false;

	// Mdex Url for Topup

	public static final String MDEX_USERNAME = "vpayqwik";
	public static final String MDEX_PASSWORD = "vpayqwik";
	public static final String MDEX_URL = getMdexTravelHost();
	public static final String TOPUP_IMAGEURL=getWebUrlForImages()+"/resources/images/topup/";
	
	public static final boolean EBS_ENABLE=true;
	public static final boolean VNET_ENABLE=true;
	public static final boolean RAZORPAY_ENABLE=true;
	
	private static final String LIVE_URL_IMAGES = "https://www.vpayqwik.com";
	private static final String TEST_URL_IMAGES = "http://66.207.206.54:8089";

	public static String getMdexTravelHost() {
		return(UrlMetadatas.PRODUCTION ? MDEX_TRAVELHOST_LIVE : MDEX_TRAVELHOST_TEST);
	}

	public static String getMdexClientToken() {
		return(UrlMetadatas.PRODUCTION ? MDEX_CLIENTTOKEN_LIVE : MDEX_CLIENTTOKEN_TEST);
	}

	public static String getMdexClientKey() {
		return(UrlMetadatas.PRODUCTION ? MDEX_CLIENTKEY_LIVE : MDEX_CLIENTKEY_TEST);
	}
	
	public static String getWebUrlForImages(){
    	return(UrlMetadatas.PRODUCTION ? LIVE_URL_IMAGES : TEST_URL_IMAGES);
	}

}
