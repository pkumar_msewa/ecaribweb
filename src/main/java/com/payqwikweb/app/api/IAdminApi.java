package com.payqwikweb.app.api;

import java.util.Date;
import java.util.List;

import com.payqwik.visa.util.QRRequestDTO;
import com.payqwik.visa.util.QRResponseDTO;
import com.payqwik.visa.util.VisaMerchantRequest;
import com.payqwik.visa.util.VisaMerchantTransaction;
import com.payqwikweb.app.model.AnalyticsResponse;
import com.payqwikweb.app.model.UserResponseDTO;
import com.payqwikweb.app.model.flight.dto.FlightCancelableResp;
import com.payqwikweb.app.model.flight.dto.GetFlightDetailsForAdmin;
import com.payqwikweb.app.model.flight.request.FligthcancelableReq;
import com.payqwikweb.app.model.flight.response.FlightResponseDTO;
import com.payqwikweb.app.model.merchant.MerchantRegisterDTO;
import com.payqwikweb.app.model.request.AccountTypeRequest;
import com.payqwikweb.app.model.request.AddVoucherReq;
import com.payqwikweb.app.model.request.AllTransactionRequest;
import com.payqwikweb.app.model.request.AllUserRequest;
import com.payqwikweb.app.model.request.BlockUnBlockUserRequest;
import com.payqwikweb.app.model.request.BlockUserRequest;
import com.payqwikweb.app.model.request.ChangePasswordRequest;
import com.payqwikweb.app.model.request.DRegistrationRequest;
import com.payqwikweb.app.model.request.DateDTO;
import com.payqwikweb.app.model.request.EmailLogRequest;
import com.payqwikweb.app.model.request.LoginRequest;
import com.payqwikweb.app.model.request.MRegistrationRequest;
import com.payqwikweb.app.model.request.MerchantTransactionRequest;
import com.payqwikweb.app.model.request.MessageLogRequest;
import com.payqwikweb.app.model.request.MobileOTPRequest;
import com.payqwikweb.app.model.request.PagingDTO;
import com.payqwikweb.app.model.request.PartnerDetailsRequest;
import com.payqwikweb.app.model.request.PromoCodeRequest;
import com.payqwikweb.app.model.request.ReceiptsRequest;
import com.payqwikweb.app.model.request.RefundDTO;
import com.payqwikweb.app.model.request.SessionDTO;
import com.payqwikweb.app.model.request.SingleUserRequest;
import com.payqwikweb.app.model.request.TransactionRequest;
import com.payqwikweb.app.model.request.TreatCardPlanRequest;
import com.payqwikweb.app.model.request.TreatCardRegisterList;
import com.payqwikweb.app.model.request.UserAnalytics;
import com.payqwikweb.app.model.request.UserResponse;
import com.payqwikweb.app.model.request.UserTransactionRequest;
import com.payqwikweb.app.model.request.bus.IsCancellableReq;
import com.payqwikweb.app.model.request.reports.MISDetailRequest;
import com.payqwikweb.app.model.response.AccountTypeResponse;
import com.payqwikweb.app.model.response.AddDonateeResponse;
import com.payqwikweb.app.model.response.AddMerchantResponse;
import com.payqwikweb.app.model.response.AddVoucherResponse;
import com.payqwikweb.app.model.response.AllTransactionResponse;
import com.payqwikweb.app.model.response.AllUserResponse;
import com.payqwikweb.app.model.response.BlockUnBlockUserResponse;
import com.payqwikweb.app.model.response.BlockUserResponse;
import com.payqwikweb.app.model.response.ChangePasswordResponse;
import com.payqwikweb.app.model.response.EmailLogResponse;
import com.payqwikweb.app.model.response.GCMResponse;
import com.payqwikweb.app.model.response.LoginResponse;
import com.payqwikweb.app.model.response.MessageLogResponse;
import com.payqwikweb.app.model.response.MobileOTPResponse;
import com.payqwikweb.app.model.response.NEFTResponse;
import com.payqwikweb.app.model.response.ReceiptsResponse;
import com.payqwikweb.app.model.response.RegistrationResponse;
import com.payqwikweb.app.model.response.ServiceTypeResponse;
import com.payqwikweb.app.model.response.TListResponse;
import com.payqwikweb.app.model.response.TransactionReportResponse;
import com.payqwikweb.app.model.response.TreatCardPlansResponse;
import com.payqwikweb.app.model.response.TreatCardRegisterListResponse;
import com.payqwikweb.app.model.response.UserTransactionResponse;
import com.payqwikweb.app.model.response.VisaSignUpResponse;
import com.payqwikweb.app.model.response.reports.MISResponce;
import com.payqwikweb.app.model.response.reports.WalletResponse;
import com.payqwikweb.model.app.request.AgentRegistrationRequest;
import com.payqwikweb.model.app.request.OffersRequest;
import com.payqwikweb.model.app.response.AddOffersResponse;
import com.payqwikweb.model.request.thirdpartyService.RequestRefund;
import com.payqwikweb.model.web.GiftCardDetailResponse;
import com.thirdparty.model.ResponseDTO;

public interface IAdminApi {

	LoginResponse login(LoginRequest request);

	ServiceTypeResponse getServiceType();

	ServiceTypeResponse getService();

	ResponseDTO verifyKYCRequest(MerchantRegisterDTO merchantRegisterDTO);

	ResponseDTO verifyKYCProcess(MerchantRegisterDTO merchantRegisterDTO);

	ResponseDTO generateMerchantPassword(MerchantRegisterDTO merchantRegisterDTO);

	AllTransactionResponse getAllTransaction(AllTransactionRequest request);

	AllTransactionResponse getSettlementTransactions(AllTransactionRequest request);

	AllTransactionResponse getPromoTransaction(AllTransactionRequest request);

	UserTransactionResponse getUserTransactionValues(SessionDTO dto);

	AllUserResponse getAllUser(AllUserRequest request);

	AllUserResponse getAllMerchants(AllUserRequest request);

	UserTransactionResponse getUserTransaction(UserTransactionRequest request);

	UserTransactionResponse refundLoadMoneyTransactions(RefundDTO dto);

	MessageLogResponse getMessageLog(MessageLogRequest request);

	EmailLogResponse getEmailLog(EmailLogRequest request);

	BlockUnBlockUserResponse blockUser(BlockUnBlockUserRequest request);

	BlockUnBlockUserResponse unblockUser(BlockUnBlockUserRequest request);

	AllTransactionResponse getDaily(AllTransactionRequest request);

	AllUserResponse getAllTransactions(AllTransactionRequest request);

	AllTransactionResponse getSingleUserTransaction(UserTransactionRequest request);

	BlockUserResponse userBlock(BlockUserRequest request);

	AddMerchantResponse addMerchant(MRegistrationRequest request);

	AllTransactionResponse getSingleUser(UserTransactionRequest request);

	List<NEFTResponse> getNEFTList(SessionDTO dto, boolean flag, Date date1, Date date2);

	List<NEFTResponse> getNEFTList(SessionDTO dto);

	ReceiptsResponse getSingleMerchantTransactionList(ReceiptsRequest request);

	ChangePasswordResponse getNewPassword(ChangePasswordRequest request);

	AddMerchantResponse addVisaMerchant(VisaMerchantRequest request);

	AddMerchantResponse checkVisaMerchant(VisaMerchantRequest request);

	// List<VisaMerchantRequest> allVisaMerchant(String sessionId);

	// List<VisaMerchantTransaction> getVisaMerchantTransaction(String request);

	VisaSignUpResponse merchantSignOffAtM2P(VisaMerchantRequest request);

	String merchantSignOffAtM2P(String request);

	VisaSignUpResponse merchantUpdateM2P(VisaMerchantRequest request);

	List<TransactionReportResponse> allVisaMerchant(String sessionId);

	List<VisaMerchantTransaction> getVisaMerchantTransaction(String request);

	AccountTypeResponse updateAccountType(AccountTypeRequest dto);

	AccountTypeResponse getListAccountType(SessionDTO dto);

	AllUserResponse getUserFromMobileNo(SingleUserRequest request);

	TransactionReportResponse getTransactionReport(DateDTO request);

	TransactionReportResponse getReconReport(DateDTO request);

	AllUserResponse getfilteredUserList(DateDTO request);

	QRResponseDTO getQRResponseFromM2P(QRRequestDTO request);

	QRResponseDTO getLiveQRResponseFromM2P(QRRequestDTO request);

	UserResponse transactionIdClickable(TransactionRequest dto);

	List<NEFTResponse> getNEFTListFiltered(DateDTO request, boolean flag);

	VisaSignUpResponse merchantUpdateQR(VisaMerchantRequest dto);

	QRResponseDTO getTransactions(QRRequestDTO request);

	String getBalanceFrom(String entityId);

	GCMResponse getGCMIds(PagingDTO dto);

	RegistrationResponse Agnetregister(AgentRegistrationRequest request);

	MobileOTPResponse agnetMobileOTP(MobileOTPRequest request);

	AddDonateeResponse addDonatee(DRegistrationRequest request);

	// Agent Get Api
	AllUserResponse getAllCreditAgent(AllUserRequest request);

	AllUserResponse getAllAgents(AllUserRequest request);

	List<AnalyticsResponse> getAnalytics(DateDTO request);

	List<AnalyticsResponse> getAnalyticsCredit(DateDTO request);

	List<UserAnalytics> getAllUsersAnalytics(DateDTO request);

	UserResponse transactionRefNo(TransactionRequest dto);

	AllTransactionResponse getPartnerDetails(AllTransactionRequest request);

	List<TransactionReportResponse> allVisaMerchantFilter(MerchantTransactionRequest req);

	AllTransactionResponse getPartnerDetailsFilter(PartnerDetailsRequest request);

	AllUserResponse getUserFromLocationCode(SingleUserRequest request);

	AllUserResponse getAllFlightsDetail(AllUserRequest request);

	FlightResponseDTO getSingleFlightTicketDetails(String sessionId, long flightId);

	FlightResponseDTO getFlightDetails(GetFlightDetailsForAdmin getFlightDetails);

	AllUserResponse getBusDetails(AllUserRequest request);

	com.payqwikweb.app.model.response.bus.ResponseDTO getSingleTicketTravellerDetails(String sessionId,
			String emtTxnId);

	com.payqwikweb.app.model.response.bus.ResponseDTO isCancellable(IsCancellableReq dto);

	com.payqwikweb.app.model.response.bus.ResponseDTO cancelBookedTicket(IsCancellableReq dto);

	AllTransactionResponse getNikkiTransaction(AllTransactionRequest request);

	AllTransactionResponse getImagicaTransaction(AllTransactionRequest request);

	AllTransactionResponse getDebitTransaction(AllTransactionRequest request);

	AllTransactionResponse getCreditTransaction(AllTransactionRequest request);

	GCMResponse getGCMIdByUserName(String userName, String sessionId);

	TreatCardPlansResponse getListPlans(SessionDTO dto);

	TreatCardPlansResponse updatePlans(TreatCardPlanRequest dto);

	AllUserResponse getAllFlightsDetailByDate(PagingDTO dto);

	AllUserResponse getBusDetailsByDate(PagingDTO dto);

	AllTransactionResponse getPromoTransactionFiltered(AllTransactionRequest request);

	AllUserResponse getTKOrderDetails(AllUserRequest request);
	
	AllUserResponse getHouseJoyByDate(PagingDTO dto);
	
	AllUserResponse getHouseJoy(PagingDTO dto);	
	
	FlightCancelableResp isCancellableFlight(FligthcancelableReq dto);
	
	UserResponseDTO findTotalUserByMonth(AllTransactionRequest request);

	ResponseDTO requestRefund(RequestRefund upload);

	ResponseDTO findTotalTransactionByMonth(AllTransactionRequest request);

	ResponseDTO findSingleUserTransaction(String sessionId, String username);

	List<NEFTResponse> getMerchantNEFTListFiltered(DateDTO request, boolean flag);
	

	com.payqwikweb.app.model.response.bus.ResponseDTO getSingleTicketPdf(String sessionId,String emtTxnId);

	TreatCardRegisterListResponse getRegisterList(SessionDTO dto);

	GiftCardDetailResponse getAllGiftCards(DateDTO dto);
	
	TreatCardRegisterListResponse getRegisterListFiltered(TreatCardRegisterList dto);

	TListResponse getTreatCardTransactionListWithFilter(AllTransactionRequest request);

	TListResponse getTreatCardTransactionList(AllTransactionRequest request);

	TreatCardRegisterListResponse getTreatCardCount(SessionDTO dto);
	
	AllUserResponse getWalletBalanceReport(AllUserRequest request);
	
	UserTransactionResponse getDailyTransactionReportValues(PagingDTO dto);
	
	MISResponce getMISreport(MISDetailRequest request);
	
	WalletResponse getWalletReport(MISDetailRequest request);
	
	AllUserResponse getWalletBalanceReportByDate(PagingDTO dto);

	AllUserResponse getAllIPayRefundTxn(AllTransactionRequest request);
	
	AllUserResponse getAllIPayRefundByDate(AllTransactionRequest request);
	

	AllTransactionResponse getWoohooTransactionReport(AllTransactionRequest transRequest);

	MobileOTPResponse agentResendOTP(MobileOTPRequest dto);

	TransactionReportResponse getUpiTransaction(AllUserRequest userRequest);
	
	AllUserResponse getAllAgentFlightsDetail(AllUserRequest request);

	AllUserResponse getAllAgentFlightsDetailByDate(PagingDTO request);

	FlightResponseDTO getSingleAgentFlightTicketDetails(String sessionId, long flightId);

	com.payqwikweb.app.model.response.bus.ResponseDTO getSingleAgentTicketPdf(String sessionId, String emtTxnNo);

	com.payqwikweb.app.model.response.bus.ResponseDTO getSingleAgentTicketTravellerDetails(String sessionId,
			String emtTxnNo);

	AllUserResponse getAgentBusDetailsByDate(PagingDTO dto);

	AllUserResponse getAgentBusDetails(AllUserRequest request);

	com.payqwikweb.app.model.response.bus.ResponseDTO cancelAgentBookedTicket(IsCancellableReq dto);

	RegistrationResponse changePassWithMobNo(ChangePasswordRequest dto);

	RegistrationResponse getCityAndState(AgentRegistrationRequest request);

	RegistrationResponse getBankList(String sessionId);

	RegistrationResponse getBankIfscCode(AgentRegistrationRequest request);

	AllUserResponse getTKOrderDetailsByDate(AllUserRequest request);

	List<NEFTResponse> getAgentNEFTListFiltered(DateDTO request, boolean flag);

	List<NEFTResponse> getAgentNEFTList(SessionDTO dto);

	AddOffersResponse addOffers(OffersRequest request);

	ServiceTypeResponse getServicesById(PromoCodeRequest req);

	TransactionReportResponse userFilteredNeftList(AllUserRequest userRequest);

	TransactionReportResponse getGcmList(AllUserRequest userRequest);
	
	ResponseDTO predictAndWinPayment(RequestRefund upload);

	PromoCodeRequest getPromoCode(String pId, String sessionCheck);

	TransactionReportResponse getTransactionReportByPage(AllUserRequest request);

	FlightResponseDTO getSingleAgentFlightTicketDetailsPdf(String sessionId, long flightId);

	AgentRegistrationRequest getAgentDetails(String aId, String sessionId);

	AllUserResponse getAllVoucher(AllUserRequest userRequest);

	AllUserResponse getfilteredVoucherList(DateDTO dto);

	AddVoucherResponse addVouchers(AddVoucherReq dto);

}