package com.payqwikweb.app.api;

import com.payqwikweb.app.model.request.MicroPaymentInitiateRequest;
import com.payqwikweb.app.model.request.MicroPaymentRequest;
import com.payqwikweb.app.model.response.MicroPaymentResponse;

public interface IMicroPaymentServiceApi {
	
	MicroPaymentResponse getNikkiToken(MicroPaymentRequest request);

	MicroPaymentResponse getIntinateNikkiTransaction(MicroPaymentInitiateRequest request);

	MicroPaymentResponse getUpdateNikkiTransaction(MicroPaymentRequest request);

	MicroPaymentResponse getRefundNikkiTransaction(MicroPaymentRequest request);
	
}
