package com.payqwikweb.app.api;

import com.payqwikweb.app.model.request.LoginRequest;
import com.payqwikweb.app.model.response.LoginResponse;

public interface IDonateeAPI {

	 public LoginResponse login(LoginRequest request);
}
