package com.payqwikweb.app.api;

import com.payqwikweb.app.model.request.MeraEventDetailsRequest;
import com.payqwikweb.app.model.request.MeraEventsBookingRequest;
import com.payqwikweb.app.model.request.MeraEventsCommonRequest;
import com.payqwikweb.app.model.request.MeraEventsListRequest;
import com.payqwikweb.app.model.response.MeraEventCategoryListResponse;
import com.payqwikweb.app.model.response.MeraEventCityListResponse;
import com.payqwikweb.app.model.response.MeraEventTicketCalculationResponse;
import com.payqwikweb.app.model.response.MeraEventsAttendeeFormRequest;
import com.payqwikweb.app.model.response.MeraEventsListResponse;
import com.payqwikweb.app.model.response.MeraEventsResponse;
import com.payqwikweb.app.model.response.MeraEventsTicketDetailsResponse;
import com.payqwikweb.controller.mobile.api.thirdparty.MeraEventAmountInitateRequest;

public interface IMeraEventsApi {

	//MeraEventsResponse getAuthorizationCode (MeraEventsCommonRequest request);
	
	//MeraEventsResponse getAccessToken(MeraEventsCommonRequest request);
	
	//MeraEventsResponse saveAccessToken(MeraEventsCommonRequest request);
	
	MeraEventCityListResponse getCities(MeraEventsCommonRequest request);
	
	MeraEventCategoryListResponse getEventCategory(MeraEventsCommonRequest request);
	
	MeraEventsListResponse getEventList(MeraEventsListRequest request);
	
	//MeraEventsListResponse filteredEventList(MeraEventsListRequest request);
	
	//MeraEventsListResponse filterEventsByCategory(MeraEventsListRequest request);
	
	//MeraEventsListResponse filterEventsByCity(MeraEventsListRequest request);
	
	MeraEventsListResponse getEventDetails(MeraEventsCommonRequest request);
	
	//MeraEventGalleryDetailsResponse getGalleryDetails(MeraEventsCommonRequest request);
	
	MeraEventsTicketDetailsResponse getEventTicketDetails(MeraEventsCommonRequest request);
	
	MeraEventTicketCalculationResponse calculateTotalAmount(MeraEventsBookingRequest request);
	
	MeraEventsResponse initiateBooking (MeraEventsBookingRequest request);
	
	MeraEventsResponse attendeeForm (MeraEventsCommonRequest request);
	
	MeraEventsResponse saveAttendeeDetails(MeraEventsAttendeeFormRequest request);
	
	MeraEventsResponse saveEventDetails(MeraEventDetailsRequest request);
	MeraEventsResponse eventbook (MeraEventsCommonRequest request);

	MeraEventsResponse eventPayment (MeraEventsCommonRequest request);
	
	MeraEventsResponse offlineBooking(MeraEventsAttendeeFormRequest request);
	MeraEventsResponse initate(MeraEventAmountInitateRequest request);


}
