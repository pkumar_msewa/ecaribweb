package com.payqwikweb.app.api;


import com.payqwikweb.app.model.busdto.BookTicketReq;
import com.payqwikweb.app.model.busdto.GetTransactionId;
import com.payqwikweb.app.model.busdto.PriceRecheckDetailsDto;
import com.payqwikweb.app.model.busdto.SaveSeatDetailsDTO;
import com.payqwikweb.app.model.request.bus.GetDestinationCity;
import com.payqwikweb.app.model.request.bus.GetSeatDetails;
import com.payqwikweb.app.model.request.bus.GetSourceCity;
import com.payqwikweb.app.model.request.bus.IsCancellableReq;
import com.payqwikweb.app.model.request.bus.ListOfAvailableTrips;
import com.payqwikweb.app.model.response.UserDetailsResponse;
import com.payqwikweb.app.model.response.bus.ResponseDTO;
import com.thirdparty.model.BusResponseDTO;

public interface ITravelBusApi {

	ResponseDTO getAllCityList(String sessionId);
	ResponseDTO getAllSourceCity(GetSourceCity city);
	ResponseDTO getAllDestinationCity(GetDestinationCity city);
	ResponseDTO getAllAvailableTrips(ListOfAvailableTrips city);
	ResponseDTO getSeatDetails(GetSeatDetails dto);
	ResponseDTO getTxnId(String session,GetTransactionId dto,UserDetailsResponse userDetailsResponse);
	ResponseDTO bookTicket(BookTicketReq dto);
	ResponseDTO isCancellable(IsCancellableReq dto);
	ResponseDTO cancelTicket(IsCancellableReq dto);
	ResponseDTO cancelInitPayment(BookTicketReq dto);
	ResponseDTO cronCheck(String session);
	ResponseDTO getTXnBySplitPayment(String session,GetTransactionId dto,String emtTxnId);
	ResponseDTO getAllBookTickets(String sessionId);
	ResponseDTO getAllMyBookTicketsForWeb(String sessionId);
	ResponseDTO getSingleTicketTravellerDetails(String sessionId,String emtTxnId);
	ResponseDTO getAllCityListByEMT(String sessionId);
	ResponseDTO getTxnIdUpdated(String session,GetTransactionId dto,UserDetailsResponse userDetailsResponse);
	BusResponseDTO bookTicketUpdated(BookTicketReq dto);
	BusResponseDTO initPayment(BookTicketReq dto);
	ResponseDTO getDynamicFareDetails(PriceRecheckDetailsDto dto);
	ResponseDTO getSingleTicketPdf(String sessionId,String emtTxnId);
	BusResponseDTO saveSeatDetails(String session,SaveSeatDetailsDTO dto);
	
}

