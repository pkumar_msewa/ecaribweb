package com.payqwikweb.app.api;

import com.payqwikweb.app.model.request.SavaariTokenRequest;
import com.payqwikweb.app.model.response.SavaariResponse;

public interface ISavaariApi {

	 SavaariResponse getAuthorizationCode(SavaariTokenRequest request);
	
	 SavaariResponse saveAccessToken(SavaariTokenRequest request);
	
	 SavaariResponse getTripType(SavaariTokenRequest request);
	 
	 SavaariResponse getSubTripType(SavaariTokenRequest request);
	 
	 SavaariResponse getSourceCity(SavaariTokenRequest request);
	 
	 SavaariResponse getDestinationCity(SavaariTokenRequest request);
	 
	 SavaariResponse getAvailbilities(SavaariTokenRequest request);
	 
	 SavaariResponse getlocalities(SavaariTokenRequest request);
	 
	 SavaariResponse getCarTypes(SavaariTokenRequest request);
	 
	 SavaariResponse getBooking(SavaariTokenRequest request);

	SavaariResponse getAccessToken(SavaariTokenRequest request);

	SavaariResponse saveSavaariUserBookingDetails(SavaariTokenRequest request);

	SavaariResponse getBookingDetails(SavaariTokenRequest request);

	SavaariResponse getSuccessTikcet(SavaariTokenRequest request);

//	SavaariResponse CancelTicket(SavaariTokenRequest request);

	SavaariResponse checkBookingId(SavaariTokenRequest request);

	SavaariResponse CancelDetails(SavaariTokenRequest request);

	SavaariResponse cancelTicket(SavaariTokenRequest request);

	SavaariResponse getTicketDetails(SavaariTokenRequest request);
}
