package com.payqwikweb.app.api;

import com.payqwikweb.app.model.request.CallBackRequest;
import com.payqwikweb.app.model.response.CallBackResponse;
import com.payqwikweb.app.model.response.IPayCallBack;

public interface ICallBackApi {

	CallBackResponse attempt(IPayCallBack request);

}
