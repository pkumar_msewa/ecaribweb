package com.payqwikweb.app.api;

import com.payqwikweb.app.model.request.ListStoreApiRequest;
import com.payqwikweb.app.model.request.OfflinePaymentRequest;
import com.payqwikweb.app.model.request.PayAtStoreRequest;
import com.payqwikweb.app.model.request.SendMoneyBankRequest;
import com.payqwikweb.app.model.request.SendMoneyMobileRequest;
import com.payqwikweb.app.model.response.ListStoreApiResponse;
import com.payqwikweb.app.model.response.OfflinePaymentResponse;
import com.payqwikweb.app.model.response.PayAtStoreResponse;
import com.payqwikweb.app.model.response.SendMoneyBankResponse;
import com.payqwikweb.app.model.response.SendMoneyMobileResponse;
import com.payqwikweb.model.app.request.SendMoneyDonateeRequest;
import com.payqwikweb.model.app.response.SendMoneyDonateeResponse;;

public interface ISendMoneyApi {

	SendMoneyMobileResponse sendMoneyMobileRequest(SendMoneyMobileRequest request);

	PayAtStoreResponse payAtStoreResponseRequest(PayAtStoreRequest request);

	ListStoreApiResponse listStoreResponseRequest(ListStoreApiRequest request);

	SendMoneyBankResponse sendMoneyBankRequest(SendMoneyBankRequest request);
	
	OfflinePaymentResponse offlinePayment(OfflinePaymentRequest request);
	
	OfflinePaymentResponse verifyOTP (OfflinePaymentRequest request);
	
	SendMoneyBankResponse sendMoneyToMBankRequest(SendMoneyBankRequest request);

	SendMoneyDonateeResponse sendMoneyToDonatee(SendMoneyDonateeRequest request);

}
