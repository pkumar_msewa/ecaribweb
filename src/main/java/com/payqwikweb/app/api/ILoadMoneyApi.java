package com.payqwikweb.app.api;

import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.payqwikweb.app.model.RazorPayResponse;
import com.payqwikweb.app.model.request.FlightVnetRequest;
import com.payqwikweb.app.model.request.LoadMoneyRequest;
import com.payqwikweb.app.model.request.RazorPayRequest;
import com.payqwikweb.app.model.request.SessionDTO;
import com.payqwikweb.app.model.request.VNetRequest;
import com.payqwikweb.app.model.response.EBSRedirectResponse;
import com.payqwikweb.app.model.response.EBSStatusResponseDTO;
import com.payqwikweb.app.model.response.LoadMoneyResponse;
import com.payqwikweb.app.model.response.TransactionReportResponse;
import com.payqwikweb.app.model.response.VNetResponse;
import com.payqwikweb.app.model.response.VRedirectResponse;
import com.payqwikweb.model.app.request.LoadMoneyFlightRequest;
import com.payqwikweb.model.app.response.VNetStatusResponse;
import com.payqwikweb.model.web.RefundStatusDTO;
import com.thirdparty.model.ResponseDTO;
import com.thirdparty.model.StatusResponse;
import com.thirdparty.model.UpiSdkResponseDTO;
import com.upi.model.requet.UPIRedirect;
import com.upi.model.requet.UpiMobileRedirectRequest;

public interface ILoadMoneyApi {
	
	LoadMoneyResponse loadMoneyRequest(LoadMoneyRequest request);
	
	UpiSdkResponseDTO loadMoneyUIPRequest(LoadMoneyRequest request);
	
	ResponseDTO loadMoneyUpiFailedRequest(LoadMoneyRequest request);

	EBSRedirectResponse processRedirectResponse(HttpServletRequest request);

	VNetResponse initiateVnetBanking(VNetRequest request);

	ResponseDTO handleRedirectRequest(VRedirectResponse dto);
	
	ResponseDTO handleUPIRedirectRequest(UPIRedirect dto);
	
	UpiSdkResponseDTO handleSdkUPIRedirectRequest(UpiMobileRedirectRequest dto);

	ResponseDTO verifyEBSTransaction(EBSRedirectResponse response);

	EBSRedirectResponse processRedirectSDK(EBSRedirectResponse response);
	//ResponseDTO refundEBSTransaction(EBSRedirectResponse response);
	
	EBSStatusResponseDTO crossCheckEBSStatus(String referenceNo) throws ParserConfigurationException, SAXException;
	
	public TransactionReportResponse getRefundStatus(RefundStatusDTO request);

	ResponseDTO getTransactionTym(SessionDTO request);
	
	LoadMoneyResponse SplitpaymentMoneyRequest(LoadMoneyFlightRequest request);
	
	EBSRedirectResponse processRedirectSDKSplitpaymentMoney(EBSRedirectResponse response);
	
	LoadMoneyResponse LoadMoneyFlightRequest(LoadMoneyFlightRequest request);

	VNetResponse initiateVnetBankingFlight(FlightVnetRequest request);

	ResponseDTO handleRedirectRequestFlight(VRedirectResponse dto);
	
	ResponseDTO verifyEBSTransactionForRecon(EBSRedirectResponse response);
	
	VNetStatusResponse verifyVNetTransaction(VRedirectResponse request);

	StatusResponse checkUpiStausAfterRedirectRequest(LoadMoneyRequest dto);

	VNetResponse agentInitiateVnetBankingFlight(FlightVnetRequest request);

	ResponseDTO agentHandleRedirectRequestFlight(VRedirectResponse dto);

	LoadMoneyResponse LoadMoneyAgentFlightRequest(LoadMoneyFlightRequest request);

	EBSRedirectResponse agentProcessRedirectSDK(EBSRedirectResponse redirectResponse);

	EBSRedirectResponse processAgentRedirectSDKSplitpaymentMoney(EBSRedirectResponse redirectResponse);

	LoadMoneyResponse AgentSplitpaymentMoneyRequest(LoadMoneyFlightRequest request);

	RazorPayResponse initiateRazorPayLoadMoney(RazorPayRequest request);

	RazorPayResponse verifyTransactionAtRazorPay(RazorPayRequest dto);

	RazorPayResponse successRazorPayLoadMoney(RazorPayRequest request, RazorPayResponse resp);

	LoadMoneyResponse loadMoneyUsingVoucher(LoadMoneyRequest request, String role);
	
}
