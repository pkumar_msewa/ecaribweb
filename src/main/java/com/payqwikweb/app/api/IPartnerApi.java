package com.payqwikweb.app.api;

import com.payqwikweb.app.model.request.PDeviceUpdateDTO;
import com.payqwikweb.app.model.response.PDeviceUpdateResponse;

public interface IPartnerApi {

    PDeviceUpdateResponse saveDevice(PDeviceUpdateDTO dto);
}
