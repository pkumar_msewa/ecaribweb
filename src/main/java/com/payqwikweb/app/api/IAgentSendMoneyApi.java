package com.payqwikweb.app.api;

import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.request.ASendMoneyBankRequest;
import com.payqwikweb.app.model.request.ListStoreApiRequest;
import com.payqwikweb.app.model.request.OfflinePaymentRequest;
import com.payqwikweb.app.model.request.PayAtStoreRequest;
import com.payqwikweb.app.model.request.SendMoneyBankRequest;
import com.payqwikweb.app.model.request.SendMoneyMobileRequest;
import com.payqwikweb.app.model.response.ListStoreApiResponse;
import com.payqwikweb.app.model.response.OfflinePaymentResponse;
import com.payqwikweb.app.model.response.PayAtStoreResponse;
import com.payqwikweb.app.model.response.SendMoneyBankResponse;
import com.payqwikweb.app.model.response.SendMoneyMobileResponse;

public interface IAgentSendMoneyApi {

	SendMoneyMobileResponse sendMoneyMobileRequest(SendMoneyMobileRequest request,Role role);

	PayAtStoreResponse payAtStoreResponseRequest(PayAtStoreRequest request,Role role);

	ListStoreApiResponse listStoreResponseRequest(ListStoreApiRequest request,Role role);

	SendMoneyBankResponse sendMoneyBankRequest(SendMoneyBankRequest request,Role role);
	
	OfflinePaymentResponse offlinePayment(OfflinePaymentRequest request,Role role);
	
	OfflinePaymentResponse verifyOTP (OfflinePaymentRequest request,Role role);
	
	SendMoneyBankResponse sendMoneyToMBankRequest(SendMoneyBankRequest request,Role role);
	
	SendMoneyMobileResponse sendMoneyRequestSuperAgent(SendMoneyMobileRequest request,Role role);

	SendMoneyBankResponse sendMoneyBankRequest(ASendMoneyBankRequest request);
}
