package com.payqwikweb.app.api;

import com.payqwikweb.app.model.response.HouseJoyResponse;
import com.payqwikweb.model.app.request.HouseJoyRequestDTO;
import com.thirdparty.model.ResponseDTO;

public interface IHouseJoyApi {

	HouseJoyResponse initializeHouseJoy(HouseJoyRequestDTO dto);
	HouseJoyResponse successHouseJoy(HouseJoyRequestDTO dto);
	HouseJoyResponse cancelHouseJoy(HouseJoyRequestDTO dto);
	
}
