package com.payqwikweb.app.api;

import com.payqwikweb.app.model.request.DirectPaymentRequest;
import com.payqwikweb.app.model.response.DirectPaymentResponse;

public interface IDirectPaymentServiceApi {

	DirectPaymentResponse getPayloToken(DirectPaymentRequest request);

	DirectPaymentResponse getIntinatePayloTransaction(DirectPaymentRequest request);

	DirectPaymentResponse getUpdatePayloTransaction(DirectPaymentRequest request);
}
