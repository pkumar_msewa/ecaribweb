package com.payqwikweb.app.api;

import com.payqwikweb.app.model.request.TreatCardDTO;
import com.payqwikweb.app.model.response.TreatCardResponse;

public interface ITreatCardApi {
	
	TreatCardResponse newRegister(TreatCardDTO reaquest);

}
