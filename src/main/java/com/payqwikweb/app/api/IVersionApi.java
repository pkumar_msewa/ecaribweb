package com.payqwikweb.app.api;

import com.payqwikweb.app.model.request.SessionDTO;
import com.payqwikweb.app.model.request.Utility;
import com.payqwikweb.app.model.request.VersionRequest;
import com.payqwikweb.app.model.response.VersionResponse;
import com.payqwikweb.model.app.request.VersionRequestDTO;
import com.payqwikweb.model.app.response.VersionResponseDTO;

public interface IVersionApi {

	VersionResponse listVersion(SessionDTO session);

	VersionResponse validateVersion(Utility dto);

	VersionResponse updateVersion(VersionRequest dto);

	VersionResponseDTO checkVersion(VersionRequestDTO dto);
}
