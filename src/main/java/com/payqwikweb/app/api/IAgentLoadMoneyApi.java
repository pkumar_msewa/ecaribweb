package com.payqwikweb.app.api;

import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.request.LoadMoneyRequest;
import com.payqwikweb.app.model.request.VNetRequest;
import com.payqwikweb.app.model.response.EBSRedirectResponse;
import com.payqwikweb.app.model.response.EBSStatusResponseDTO;
import com.payqwikweb.app.model.response.LoadMoneyResponse;
import com.payqwikweb.app.model.response.TransactionReportResponse;
import com.payqwikweb.app.model.response.VNetResponse;
import com.payqwikweb.app.model.response.VRedirectResponse;
import com.payqwikweb.model.web.RefundStatusDTO;
import com.thirdparty.model.ResponseDTO;

public interface IAgentLoadMoneyApi {

	LoadMoneyResponse loadMoneyRequest(LoadMoneyRequest request,Role role);

	EBSRedirectResponse processRedirectResponse(HttpServletRequest request,Role role);

	VNetResponse initiateVnetBanking(VNetRequest request,Role role);

	ResponseDTO handleRedirectRequest(VRedirectResponse dto,Role role);

	ResponseDTO verifyEBSTransaction(EBSRedirectResponse response,Role role);

	EBSRedirectResponse processRedirectSDK(EBSRedirectResponse response,Role role);
	//ResponseDTO refundEBSTransaction(EBSRedirectResponse response);
	
	EBSStatusResponseDTO crossCheckEBSStatus(String referenceNo,Role role) throws ParserConfigurationException, SAXException;
	
	public TransactionReportResponse getRefundStatus(RefundStatusDTO request,Role role);
}
