package com.payqwikweb.app.api;

import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.request.BillPaymentCommonDTO;
import com.payqwikweb.app.model.response.BillPaymentCommonResponseDTO;

public interface IAgentBillPaymentApi {
	BillPaymentCommonResponseDTO electricBill(BillPaymentCommonDTO request,Role role);

	BillPaymentCommonResponseDTO gasBill(BillPaymentCommonDTO request,Role role);

	BillPaymentCommonResponseDTO insuranceBill(BillPaymentCommonDTO request,Role role);

	BillPaymentCommonResponseDTO landline(BillPaymentCommonDTO request,Role role);

	BillPaymentCommonResponseDTO dthBill(BillPaymentCommonDTO request,Role role);

}
