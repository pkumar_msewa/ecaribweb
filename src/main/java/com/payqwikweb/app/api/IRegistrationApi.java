package com.payqwikweb.app.api;

import com.payqwik.visa.util.VisaMerchantRequest;
import com.payqwikweb.app.model.request.ChangePasswordRequest;
import com.payqwikweb.app.model.request.MobileOTPRequest;
import com.payqwikweb.app.model.request.RegistrationRequest;
import com.payqwikweb.app.model.request.ResendMobileOTPRequest;
import com.payqwikweb.app.model.request.VerifyEmailRequest;
import com.payqwikweb.app.model.response.AddMerchantResponse;
import com.payqwikweb.app.model.response.ChangePasswordResponse;
import com.payqwikweb.app.model.response.MobileOTPResponse;
import com.payqwikweb.app.model.response.QuestionResponse;
import com.payqwikweb.app.model.response.RegistrationResponse;
import com.payqwikweb.app.model.response.ResendMobileOTPResponse;
import com.payqwikweb.app.model.response.VerifyEmailResponse;
import com.payqwikweb.model.app.request.RegisterRequest;

public interface IRegistrationApi {

	RegistrationResponse register(RegistrationRequest request);

	MobileOTPResponse mobileOTP(MobileOTPRequest request);

	ResendMobileOTPResponse resendMobileOTP(ResendMobileOTPRequest request);

	VerifyEmailResponse verifyEmail(VerifyEmailRequest request);
	
	ChangePasswordResponse changePassword(ChangePasswordRequest cpr);

	RegistrationResponse registerMerchant(RegisterRequest request);
	
	RegistrationResponse merchantSignUp (VisaMerchantRequest request);
	
	AddMerchantResponse checkExistingVisaMerchant(VisaMerchantRequest request);
	
	AddMerchantResponse signUpVisaMerchant(VisaMerchantRequest request);
	
	RegistrationResponse addBankDetails(VisaMerchantRequest request);
	
	MobileOTPResponse merchantMobileOTP(MobileOTPRequest request);
	
	ResendMobileOTPResponse resendMerchantMobileOTP(ResendMobileOTPRequest request);
	
	QuestionResponse getQuestions();

	RegistrationResponse newRegister(RegistrationRequest request);
}
