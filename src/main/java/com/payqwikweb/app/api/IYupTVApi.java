package com.payqwikweb.app.api;

import com.payqwikweb.app.model.request.YupTVRequest;
import com.payqwikweb.app.model.response.YupTVResponse;

public interface IYupTVApi {


	YupTVResponse getRegister(YupTVRequest request);

	YupTVResponse getDeactivated(YupTVRequest request);

	YupTVResponse saveRegister(YupTVRequest request);

	YupTVResponse CheckRegister(YupTVRequest request);

	YupTVResponse existUser(YupTVRequest request);
}
