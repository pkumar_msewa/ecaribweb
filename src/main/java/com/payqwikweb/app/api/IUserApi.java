package com.payqwikweb.app.api;

import org.json.JSONException;

import com.gci.model.request.LoginDTO;
import com.payqwik.visa.util.QRRequestDTO;
import com.payqwik.visa.util.QRResponseDTO;
import com.payqwikweb.app.model.AccessDTO;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.request.BescomRefundRequest;
import com.payqwikweb.app.model.request.ChangeMPINRequest;
import com.payqwikweb.app.model.request.ChangePasswordRequest;
import com.payqwikweb.app.model.request.DeleteMPINRequest;
import com.payqwikweb.app.model.request.EBSRefundRequest;
import com.payqwikweb.app.model.request.EditProfileRequest;
import com.payqwikweb.app.model.request.FavouriteRequest;
import com.payqwikweb.app.model.request.FetchMobileList;
import com.payqwikweb.app.model.request.ForgotMpinRequest;
import com.payqwikweb.app.model.request.InviteFriendEmailRequest;
import com.payqwikweb.app.model.request.InviteFriendMobileRequest;
import com.payqwikweb.app.model.request.KycVerificationDTO;
import com.payqwikweb.app.model.request.NearByAgentsRequest;
import com.payqwikweb.app.model.request.NearByMerchantsRequest;
import com.payqwikweb.app.model.request.ReceiptsRequest;
import com.payqwikweb.app.model.request.RedeemCodeRequest;
import com.payqwikweb.app.model.request.SessionDTO;
import com.payqwikweb.app.model.request.SetMPINRequest;
import com.payqwikweb.app.model.request.TreatCardDTO;
import com.payqwikweb.app.model.request.UploadPictureRequest;
import com.payqwikweb.app.model.request.UserDetailsByAdminRequest;
import com.payqwikweb.app.model.request.UserDetailsRequest;
import com.payqwikweb.app.model.request.UserMaxLimitDto;
import com.payqwikweb.app.model.request.VerifyMPINRequest;
import com.payqwikweb.app.model.request.VisaRequest;
import com.payqwikweb.app.model.response.ChangePasswordResponse;
import com.payqwikweb.app.model.response.CustDetailsResp;
import com.payqwikweb.app.model.response.EBSRedirectResponse;
import com.payqwikweb.app.model.response.EditProfileResponse;
import com.payqwikweb.app.model.response.InviteFriendEmailResponse;
import com.payqwikweb.app.model.response.InviteFriendMobileResponse;
import com.payqwikweb.app.model.response.KycResponse;
import com.payqwikweb.app.model.response.MPINResponse;
import com.payqwikweb.app.model.response.MerchantResponse;
import com.payqwikweb.app.model.response.NearByAgentResponse;
import com.payqwikweb.app.model.response.NearBymerchantResponse;
import com.payqwikweb.app.model.response.NotificationsResponse;
import com.payqwikweb.app.model.response.ReceiptsResponse;
import com.payqwikweb.app.model.response.SharePointsResponse;
import com.payqwikweb.app.model.response.TreatCardPlansResponse;
import com.payqwikweb.app.model.response.TreatCardResponse;
import com.payqwikweb.app.model.response.UpdateAccessResponse;
import com.payqwikweb.app.model.response.UploadPictureResponse;
import com.payqwikweb.app.model.response.UserDetailsByAdminResponse;
import com.payqwikweb.app.model.response.UserDetailsResponse;
import com.payqwikweb.app.model.response.VisaResponse;
import com.payqwikweb.model.app.request.UpdateReceiptDTO;
import com.payqwikweb.model.app.response.AccountDTO;
import com.payqwikweb.model.app.response.BalanceDTO;
import com.payqwikweb.model.app.response.BasicDTO;
import com.payqwikweb.model.app.response.ImageDTO;
import com.payqwikweb.model.app.response.LimitDTO;
import com.payqwikweb.model.app.response.LocationDTO;
import com.payqwikweb.model.app.response.RedeemCodeResponse;
import com.payqwikweb.model.app.response.UpdateReceiptResponse;
import com.payqwikweb.model.web.GciAuthDTO;
import com.payqwikweb.model.web.ImagicaAuthDTO;
import com.payqwikweb.model.web.ImagicaOrderRequest;
import com.payqwikweb.model.web.ImagicaOrderResponse;
import com.payqwikweb.model.web.ImagicaPaymentRequest;
import com.payqwikweb.model.web.ImagicaPaymentResponse;
import com.payqwikweb.model.web.LocationRequest;
import com.payqwikweb.model.web.LocationResponse;
import com.payqwikweb.model.web.SharePointDTO;
import com.thirdparty.model.ResponseDTO;

public interface IUserApi  {


	UpdateAccessResponse updateAccessFromSuperAdmin(AccessDTO dto);

	BasicDTO getBasicDetails(UserDetailsRequest request,Role role);

	BalanceDTO getBalanceDetails(UserDetailsRequest request,Role role);

	AccountDTO getAccountDetails(UserDetailsRequest request, Role role);

	ImageDTO getImageDetails(UserDetailsRequest request, Role role);

	LimitDTO getLimitDetails(UserDetailsRequest request,Role role);

	LocationDTO getLocationDetails(UserDetailsRequest request,Role role);

	UserDetailsResponse getUserDetails(UserDetailsRequest request,Role role);
	
	UserDetailsResponse reSendEmailTop(UserDetailsRequest request);

	EditProfileResponse editProfile(EditProfileRequest request);

	MerchantResponse getAllMerchants(SessionDTO dto);

	ChangePasswordResponse changePassword(ChangePasswordRequest request);

	ChangePasswordResponse updateMerchantPassword(ChangePasswordRequest request);

	UploadPictureResponse uploadPicture(UploadPictureRequest request);

	ReceiptsResponse getReceipts(ReceiptsRequest request);

	ReceiptsResponse getMerchantReceipts(ReceiptsRequest request);

	ReceiptsResponse getSuccessfulTransactions(SessionDTO dto);

	ReceiptsResponse updateFavouriteTransaction(FavouriteRequest dto);

	MPINResponse setMPIN(SetMPINRequest request);

	MPINResponse changeMPIN(ChangeMPINRequest request);

	MPINResponse deleteMPIN(DeleteMPINRequest request);

	MPINResponse forgotMPIN(ForgotMpinRequest request);

	MPINResponse verifyMPIN(VerifyMPINRequest request);

	InviteFriendEmailResponse inviteEmailFriend(InviteFriendEmailRequest request);

	InviteFriendMobileResponse inviteMobileFriend(InviteFriendMobileRequest request);

	UserDetailsByAdminResponse userTransactionAndDetails(UserDetailsByAdminRequest request);
	
	RedeemCodeResponse redeemPromoCode(RedeemCodeRequest request);

	SharePointsResponse sharePoints(SharePointDTO dto);

	KycResponse kycVerification (KycVerificationDTO request);

	KycResponse kycOTPVerification (KycVerificationDTO request);
	
	KycResponse resendKycOTP (KycVerificationDTO request);
	
	VisaResponse visaTransactionInitinated(VisaRequest requests);
	
	VisaResponse prepareGetAccountNumberRequest(VisaRequest requests);
	
	VisaResponse prepareGetAccNoDecrption(VisaRequest req);

	EBSRedirectResponse getAllTransactionsRefund(EBSRefundRequest req);
	
	QRResponseDTO getEntityIdOfM2P(QRRequestDTO req);

	UpdateReceiptResponse userUpdateReceipts(UpdateReceiptDTO request);

	NotificationsResponse getNotifications(SessionDTO req) throws JSONException;

	ImagicaAuthDTO getImagicaAuth(SessionDTO dto);

	ImagicaOrderResponse placeImagicaOrder(ImagicaOrderRequest request);

	ImagicaPaymentResponse processPayment(ImagicaPaymentRequest request);

	LocationResponse getLocationByPin(LocationRequest request);

	UpdateReceiptResponse userReceipts(UpdateReceiptDTO request);

	ReceiptsResponse getReceiptsFilter(ReceiptsRequest request);
	//ReceiptsResponse getMerchantReceiptsFilter(ReceiptsRequest request);

	EditProfileResponse editName(EditProfileRequest request);

	UpdateReceiptResponse userUpdateReceiptsinAjax(UpdateReceiptDTO request);

	ReceiptsResponse getMerchantReceiptscommision(ReceiptsRequest request);

	ReceiptsResponse getMerchantReceiptsFiltered(ReceiptsRequest request);

	ReceiptsResponse getMerchantTransactions(ReceiptsRequest request);

	GciAuthDTO getGciAuth(SessionDTO dto);

	ResponseDTO getMobileListForRefer(FetchMobileList dto);

	NearBymerchantResponse getNearByMerchants(NearByMerchantsRequest request, Role role);

	ReceiptsResponse getDebitMerchantReceipts(ReceiptsRequest request);

	TreatCardResponse getTreatCardDetails(SessionDTO dto);

	TreatCardPlansResponse getListPlans(SessionDTO dto);

	TreatCardPlansResponse getUpdateTreatCard(TreatCardDTO dto);
	
	ReceiptsResponse getBescomMerchantTxns(ReceiptsRequest request);
	
	CustDetailsResp getCustDetails(BescomRefundRequest dto);
	
	ResponseDTO getBescomMerchantRefundReq(BescomRefundRequest dto);
	
	NearBymerchantResponse findNearByMerchants(NearByMerchantsRequest request, Role role);
	
	NearByAgentResponse findNearByAgents(NearByAgentsRequest request, Role role);

	ResponseDTO updateUserMaxLimit(UserMaxLimitDto dto);

	ResponseDTO getUserMaxLimit(UserMaxLimitDto dto);

	ResponseDTO sendMaxLimitOtp(UserMaxLimitDto dto);

	ResponseDTO validateUserPassword(LoginDTO dto);
}
