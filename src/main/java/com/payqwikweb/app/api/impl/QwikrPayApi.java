package com.payqwikweb.app.api.impl;

import org.codehaus.jettison.json.JSONObject;

import com.payqwikweb.app.api.IQwikrPayApi;
import com.payqwikweb.app.metadatas.UrlMetadatas;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Language;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.Version;
import com.payqwikweb.app.utils.SecurityUtils;
import com.payqwikweb.model.app.request.QwikrPayRequestDTO;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.thirdparty.model.ResponseDTO;

public class QwikrPayApi implements IQwikrPayApi{

	@Override
	public ResponseDTO initializeQwikrPay(QwikrPayRequestDTO dto) {
		
		ResponseDTO resp = new ResponseDTO();
		try {
			JSONObject payload = new JSONObject();
			payload.put("orderId",dto.getOrderId());
			payload.put("mobileNo", dto.getMobileNo());
			//payload.put("token", dto.getToken());
			payload.put("amount", dto.getAmount());
			payload.put("merchantUserName", dto.getMerchantUserName());
			System.out.println("\n \n payload from qwikrpay "+payload.toString());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getQwikrPayInitiatePayment(Version.VERSION_1, Role.USER,
					Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456"))
					.post(ClientResponse.class,payload);
			String strResponse = response.getEntity(String.class);
			System.err.println("str response for process ::::" + strResponse);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);

					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
							resp.setTransactionId(jobj.getString("txnId"));
							resp.setMessage(jobj.getString("message"));
							resp.setCode(code);
						}
						
					 else {
						resp.setSuccess(false);
						resp.setCode("F00");
						resp.setMessage(jobj.getString("message"));
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service Unavailable....");
				}
			}else{
				resp.setValid(false);
				resp.setCode("F00");
				resp.setMessage("no response from server");
			}
				}
			}
		 catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
		}
		return resp;
	}

	

	@Override
	public ResponseDTO successQwikrPay(QwikrPayRequestDTO dto) {
		
		ResponseDTO resp = new ResponseDTO();
		try {
			JSONObject payload = new JSONObject();
			payload.put("orderId",dto.getOrderId());
			payload.put("mobileNo", dto.getMobileNo());
			//payload.put("token", dto.getToken());
			payload.put("merchantUserName", dto.getMerchantUserName());
			payload.put("code", dto.getCode());
			payload.put("transactionRefNo", dto.getTransactionRefNo());
			System.out.println("inside success qwikrpay to app =="+payload.toString());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getQwikrPaySuccessPayment(Version.VERSION_1, Role.USER,
					Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456"))
					.post(ClientResponse.class,payload);
			String strResponse = response.getEntity(String.class);
			System.err.println("str response for process ::::" + strResponse);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);

					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
							resp.setTransactionId(jobj.getString("txnId"));
							resp.setMessage(jobj.getString("message"));
						}
						resp.setCode(code);
					} else {
						resp.setSuccess(false);
						resp.setCode("F00");
						resp.setMessage(jobj.getString("message"));
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("service Unavailable....");
				}
				resp.setDetails(strResponse);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
		}
		return resp;
	}

}
