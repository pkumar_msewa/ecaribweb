package com.payqwikweb.app.api.impl;

import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.payqwikweb.app.api.IRedeemPointsApi;
import com.payqwikweb.app.metadatas.UrlMetadatas;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Language;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.Version;
import com.payqwikweb.app.model.request.RedeemPointsRequest;
import com.payqwikweb.app.model.response.RedeemPointsResponse;
import com.payqwikweb.app.utils.SecurityUtils;
import com.payqwikweb.util.APIUtils;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class RedeemPointsApi implements IRedeemPointsApi {
	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Override
	public RedeemPointsResponse convertRedeemPointToCash(RedeemPointsRequest request) {

		RedeemPointsResponse resp = new RedeemPointsResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId",request.getSessionId());
			payload.put("points",request.getPoints());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getRedeemPointsUrl(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable...");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
         }
			else {
				String strResponse = response.getEntity(String.class);
				System.out.println("the response is......"+ strResponse);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						resp.setMessage(message);
						String errors = "";
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} 
						
						resp.setCode(code);
						resp.setStatus(status);
						resp.setResponse(strResponse);
					}
					} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}
}