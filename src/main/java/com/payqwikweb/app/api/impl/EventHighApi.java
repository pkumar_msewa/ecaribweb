package com.payqwikweb.app.api.impl;

import com.payqwikweb.api.IEventHighApi;
import com.payqwikweb.model.app.request.EventRequest;
import com.payqwikweb.model.app.response.EventHighResponse;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class EventHighApi implements IEventHighApi {

	@Override
	public EventHighResponse eventlist(EventRequest request) {

		EventHighResponse eventlist = new EventHighResponse();

		try {

			Client client = Client.create();
			WebResource webResource = client
					.resource("https://developer.eventshigh.com/events/"+request.getCity()+"?key=ev3nt5h1ghte5tK3y");

			ClientResponse response = webResource.accept("application/json").type("application/json")
					.get(ClientResponse.class);

			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
			}

			String output = response.getEntity(String.class);


			eventlist.setMessage(output);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return eventlist;

	}
}