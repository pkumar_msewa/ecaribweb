package com.payqwikweb.app.api.impl;

import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.payqwikweb.app.api.ITopupApi;
import com.payqwikweb.app.metadatas.UrlMetadatas;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Language;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.Version;
import com.payqwikweb.app.model.request.BrowsePlansRequest;
import com.payqwikweb.app.model.request.GetOperatorAndCircleForMobRequest;
import com.payqwikweb.app.model.request.GetOperatorAndCircleRequest;
import com.payqwikweb.app.model.request.RechargeRequestDTO;
import com.payqwikweb.app.model.response.BrowsePlansResponse;
import com.payqwikweb.app.model.response.GetOperatorAndCircleForMobResponse;
import com.payqwikweb.app.model.response.GetOperatorAndCircleResponse;
import com.payqwikweb.app.model.response.RechargeResponseDTO;
import com.payqwikweb.app.utils.SecurityUtils;
import com.payqwikweb.util.APIUtils;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;

public class TopupApi implements ITopupApi {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public RechargeResponseDTO prePaid(RechargeRequestDTO request) {
		RechargeResponseDTO resp = new RechargeResponseDTO();
		String status="";
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("topupType", request.getTopupType());
			payload.put("serviceProvider", request.getServiceProvider());
			payload.put("mobileNo", request.getMobileNo());
			payload.put("area", request.getArea());
			payload.put("amount", request.getAmount());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(
					UrlMetadatas.getPrePaidTopupUrl(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							 status = (String) jobj.get("status");
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					} 
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public GetOperatorAndCircleResponse operatorAndcircle(GetOperatorAndCircleRequest request) {
		GetOperatorAndCircleResponse resp = new GetOperatorAndCircleResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getOperatorAndCircleUrl(Version.VERSION_1, Role.USER,
					Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					} 
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}

		return resp;
	}

	@Override
	public GetOperatorAndCircleForMobResponse operatorAndcircleForMob(GetOperatorAndCircleForMobRequest request) {
		GetOperatorAndCircleForMobResponse resp = new GetOperatorAndCircleForMobResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", (request.getSessionId() == null) ? "" : request.getSessionId());
			payload.put("mobileNumber", request.getMobileNumber());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getOperatorAndCircleForMobUrl(Version.VERSION_1,
					Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						resp.setSuccess(true);
						resp.setCode("S00");
						resp.setStatus("SUCCESS");
						resp.setMessage("Getting operator and circle for mobile");
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}

		return resp;
	}

	@Override
	public BrowsePlansResponse getPlansForMobile(BrowsePlansRequest request) {
		BrowsePlansResponse resp = new BrowsePlansResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", (request.getSessionId() == null) ? "" : request.getSessionId());
			payload.put("circleCode", request.getCircleCode());
			payload.put("operatorCode", request.getOperatorCode());
			Client client = Client.create();
			WebResource webResource = client
					.resource(UrlMetadatas.getPlans(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						resp.setSuccess(true);
						resp.setCode("S00");
						resp.setStatus("SUCCESS");
						resp.setMessage("Getting Plans for mobile");
						resp.setResponse(strResponse);
					}

				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}

		return resp;
	}

}
