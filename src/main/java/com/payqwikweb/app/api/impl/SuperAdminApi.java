package com.payqwikweb.app.api.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gcm.model.CronNotificationDTO;
import com.payqwik.visa.util.VisaContants;
import com.payqwik.visa.util.VisaMerchantRequest;
import com.payqwikweb.app.api.ISuperAdminApi;
import com.payqwikweb.app.metadatas.UrlMetadatas;
import com.payqwikweb.app.model.AccessDTO;
import com.payqwikweb.app.model.AddServiceTypeDTO;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Language;
import com.payqwikweb.app.model.ResponseStatus;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.ServiceTypeDTO;
import com.payqwikweb.app.model.TFilterDTO;
import com.payqwikweb.app.model.UserAccountInfo;
import com.payqwikweb.app.model.UserBasicInfo;
import com.payqwikweb.app.model.UserListDTO;
import com.payqwikweb.app.model.UserLoginInfo;
import com.payqwikweb.app.model.UserTransactionsInfo;
import com.payqwikweb.app.model.Version;
import com.payqwikweb.app.model.busdto.BusTicketDTO;
import com.payqwikweb.app.model.busdto.TravellerDetailsDTO;
import com.payqwikweb.app.model.flight.dto.FlightCancelableResp;
import com.payqwikweb.app.model.flight.dto.GetFlightDetailsForAdmin;
import com.payqwikweb.app.model.flight.request.FligthcancelableReq;
import com.payqwikweb.app.model.flight.response.FlightResponseDTO;
import com.payqwikweb.app.model.flight.response.FlightTicketResp;
import com.payqwikweb.app.model.flight.response.TicketsResp;
import com.payqwikweb.app.model.request.AadharDetailsDTO;
import com.payqwikweb.app.model.request.AccountTypeRequest;
import com.payqwikweb.app.model.request.AdminUserDetails;
import com.payqwikweb.app.model.request.AllTransactionRequest;
import com.payqwikweb.app.model.request.AllUserRequest;
import com.payqwikweb.app.model.request.BlockUnBlockUserRequest;
import com.payqwikweb.app.model.request.BlockUserRequest;
import com.payqwikweb.app.model.request.BulkFileDTO;
import com.payqwikweb.app.model.request.BulkFileUpload;
import com.payqwikweb.app.model.request.BulkMailRequest;
import com.payqwikweb.app.model.request.BulkSMSRequest;
import com.payqwikweb.app.model.request.CPasswordOTPRequest;
import com.payqwikweb.app.model.request.CPasswordRequest;
import com.payqwikweb.app.model.request.EmailLogRequest;
import com.payqwikweb.app.model.request.LoginRequest;
import com.payqwikweb.app.model.request.MRegistrationRequest;
import com.payqwikweb.app.model.request.MailRequest;
import com.payqwikweb.app.model.request.MessageLogRequest;
import com.payqwikweb.app.model.request.MobileSearchRequest;
import com.payqwikweb.app.model.request.PagingDTO;
import com.payqwikweb.app.model.request.PromoCodeRequest;
import com.payqwikweb.app.model.request.ReceiptsRequest;
import com.payqwikweb.app.model.request.RefundDTO;
import com.payqwikweb.app.model.request.SMSRequest;
import com.payqwikweb.app.model.request.ServiceRequest;
import com.payqwikweb.app.model.request.ServicesRequest;
import com.payqwikweb.app.model.request.SessionDTO;
import com.payqwikweb.app.model.request.TreatCardPlanRequest;
import com.payqwikweb.app.model.request.TreatCardRegisterList;
import com.payqwikweb.app.model.request.UserInfoRequest;
import com.payqwikweb.app.model.request.UserTransactionRequest;
import com.payqwikweb.app.model.request.bus.IsCancellableReq;
import com.payqwikweb.app.model.response.AadharServiceResponse;
import com.payqwikweb.app.model.response.AccessListResponse;
import com.payqwikweb.app.model.response.AccountTypeResponse;
import com.payqwikweb.app.model.response.AddMerchantResponse;
import com.payqwikweb.app.model.response.AllTransactionResponse;
import com.payqwikweb.app.model.response.AllUserResponse;
import com.payqwikweb.app.model.response.BlockUnBlockUserResponse;
import com.payqwikweb.app.model.response.BlockUserResponse;
import com.payqwikweb.app.model.response.BulkFileListResponse;
import com.payqwikweb.app.model.response.CPasswordResponse;
import com.payqwikweb.app.model.response.CommissionDTO;
import com.payqwikweb.app.model.response.EmailLogResponse;
import com.payqwikweb.app.model.response.GCMResponse;
import com.payqwikweb.app.model.response.LoginResponse;
import com.payqwikweb.app.model.response.MailLogDTO;
import com.payqwikweb.app.model.response.MailLogResponse;
import com.payqwikweb.app.model.response.MailResponse;
import com.payqwikweb.app.model.response.MessageLogResponse;
import com.payqwikweb.app.model.response.MobileSearchDTO;
import com.payqwikweb.app.model.response.MobileSearchResponse;
import com.payqwikweb.app.model.response.NEFTResponse;
import com.payqwikweb.app.model.response.ReceiptsResponse;
import com.payqwikweb.app.model.response.SMSLogDTO;
import com.payqwikweb.app.model.response.SMSLogResponse;
import com.payqwikweb.app.model.response.SMSResponse;
import com.payqwikweb.app.model.response.ServiceListDTO;
import com.payqwikweb.app.model.response.ServiceListResponse;
import com.payqwikweb.app.model.response.ServiceTypeResponse;
import com.payqwikweb.app.model.response.ServicesDTO;
import com.payqwikweb.app.model.response.ServicesResponse;
import com.payqwikweb.app.model.response.TListResponse;
import com.payqwikweb.app.model.response.TreatCardPlansResponse;
import com.payqwikweb.app.model.response.TreatCardRegisterListResponse;
import com.payqwikweb.app.model.response.UserInfoResponse;
import com.payqwikweb.app.model.response.UserTransactionResponse;
import com.payqwikweb.app.model.response.VersionDTO;
import com.payqwikweb.app.model.response.VersionListResponse;
import com.payqwikweb.app.model.response.VisaSignUpResponse;
import com.payqwikweb.app.model.response.bus.CancelTicketResp;
import com.payqwikweb.app.model.response.bus.IsCancellableResp;
import com.payqwikweb.app.model.response.bus.ResponseDTO;
import com.payqwikweb.app.utils.SecurityUtils;
import com.payqwikweb.model.admin.FilterDTO;
import com.payqwikweb.model.admin.TListDTO;
import com.payqwikweb.model.app.request.AddServicesDTO;
import com.payqwikweb.model.app.request.TravellerFlightDetails;
import com.payqwikweb.model.app.request.UserListRequest;
import com.payqwikweb.model.app.request.VersionRequest;
import com.payqwikweb.model.app.response.TransactionUserResponse;
import com.payqwikweb.model.app.response.UserListResponse;
import com.payqwikweb.util.APIUtils;
import com.payqwikweb.util.JSONParserUtil;
import com.payqwikweb.util.LogCat;
import com.payqwikweb.validation.CommonValidation;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;

import sun.util.logging.resources.logging;

public class SuperAdminApi implements ISuperAdminApi {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	private SimpleDateFormat simformat = new SimpleDateFormat("dd-MM-yyyy");

	@Override
	public ServiceListDTO getServiceTypeList(SessionDTO dto) {
		ServiceListDTO resp = new ServiceListDTO();
		try {
			Client client = Client.create();
			JSONObject payload = new JSONObject();
			payload.put("sessionId", dto.getSessionId());
			WebResource webResource = client.resource(UrlMetadatas.getGetServiceTypesURL(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456")).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
							org.json.JSONArray serviceArray = JSONParserUtil.getArray(jobj, "details");
							Map<String, String> map = new HashMap<>();
							if (serviceArray != null) {
								for (int i = 0; i < serviceArray.length(); i++) {
									org.json.JSONObject json = serviceArray.getJSONObject(i);
									map.put(JSONParserUtil.getString(json, "name"),
											JSONParserUtil.getString(json, "description"));
								}
							}
							resp.setServiceMap(map);
						} else {
							resp.setSuccess(false);
						}
					} else {
						resp.setSuccess(false);
					}
				} else {
					resp.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
		}
		return resp;
	}

	@Override
	public TListResponse getFilteredTransactionList(TFilterDTO request) {
		TListResponse resp = new TListResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("startDate", request.getStartDate());
			payload.put("endDate", request.getEndDate());
			payload.put("status", request.getStatus());
			payload.put("serviceType", request.getServiceType());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getTransactionsFilteredURL(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456")).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (code.equalsIgnoreCase("S00")) {
							List<TListDTO> transactions = new ArrayList<>();
							resp.setSuccess(true);
							org.json.JSONArray details = JSONParserUtil.getArray(jobj, "details");
							if (details != null && details.length() > 0) {
								for (int i = 0; i < details.length(); i++) {
									org.json.JSONObject json = details.getJSONObject(i);
									TListDTO dto = new TListDTO();
									dto.setUsername(JSONParserUtil.getString(json, "username"));
									dto.setEmail(JSONParserUtil.getString(json, "email"));
									dto.setContactNo(JSONParserUtil.getString(json, "contactNo"));
									dto.setTransactionRefNo(JSONParserUtil.getString(json, "transactionRefNo"));
									dto.setCurrentBalance(JSONParserUtil.getString(json, "currentBalance"));
									dto.setDateOfTransaction(JSONParserUtil.getString(json, "dateOfTransaction"));
									dto.setDebit(JSONParserUtil.getString(json, "debit"));
									dto.setAmount(JSONParserUtil.getString(json, "amount"));
									dto.setStatus(JSONParserUtil.getString(json, "status"));
									dto.setDescription(JSONParserUtil.getString(json, "description"));
									transactions.add(dto);
								}
								resp.setList(transactions);
							} else {
								resp.setSuccess(false);
								resp.setMessage("Transaction List Empty");
							}
						} else {
							resp.setSuccess(false);
							resp.setMessage("Please try Later");
						}
					} else {
						resp.setSuccess(false);
					}
				} else {
					resp.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
		}
		return resp;

	}

	@Override
	public TListResponse getFilteredTransactionListOfBillPay(TFilterDTO request) {
		TListResponse resp = new TListResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("startDate", request.getStartDate());
			payload.put("endDate", request.getEndDate());
			payload.put("status", request.getStatus());
			payload.put("serviceType", request.getServiceType());
			payload.put("transactionRefNo", request.getTransactionRefNo());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getTransactionsFilteredBillPayURL(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456")).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (code.equalsIgnoreCase("S00")) {
							List<TListDTO> transactions = new ArrayList<>();
							resp.setSuccess(true);
							org.json.JSONArray details = JSONParserUtil.getArray(jobj, "details");
							if (details != null && details.length() > 0) {
								for (int i = 0; i < details.length(); i++) {
									org.json.JSONObject json = details.getJSONObject(i);
									TListDTO dto = new TListDTO();
									dto.setUsername(JSONParserUtil.getString(json, "username"));
									dto.setEmail(JSONParserUtil.getString(json, "email"));
									dto.setContactNo(JSONParserUtil.getString(json, "contactNo"));
									dto.setTransactionRefNo(JSONParserUtil.getString(json, "transactionRefNo"));
									dto.setCurrentBalance(JSONParserUtil.getString(json, "currentBalance"));
									dto.setDateOfTransaction(JSONParserUtil.getString(json, "dateOfTransaction"));
									dto.setDebit(JSONParserUtil.getString(json, "debit"));
									dto.setAmount(JSONParserUtil.getString(json, "amount"));
									dto.setStatus(JSONParserUtil.getString(json, "status"));
									dto.setDescription(JSONParserUtil.getString(json, "description"));
									transactions.add(dto);
								}
								resp.setList(transactions);
							} else {
								resp.setSuccess(false);
								resp.setMessage("Transaction List Empty");
							}
						} else {
							resp.setSuccess(false);
							resp.setMessage("Please try Later");
						}
					} else {
						resp.setSuccess(false);
					}
				} else {
					resp.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
		}
		return resp;

	}

	@Override
	public TransactionUserResponse refundTransaction(RefundDTO dto) {
		TransactionUserResponse resp = new TransactionUserResponse();
		try {
			Client client = Client.create();
			JSONObject payload = new JSONObject();
			payload.put("sessionId", dto.getSessionId());
			payload.put("transactionRefNo", dto.getTransactionRefNo());
			WebResource webResource = client.resource(UrlMetadatas.getSuperadminRefund(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456")).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						final String message = jobj.getString("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setMessage(message);
						resp.setCode(code);
					}
				} else {
					resp.setSuccess(false);
					resp.setMessage("strResponse is null");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setMessage("Exception Occurred");
		}

		return resp;
	}

	@Override
	public TransactionUserResponse refundTransactionBillpay(RefundDTO dto) {
		TransactionUserResponse resp = new TransactionUserResponse();
		try {
			Client client = Client.create();
			JSONObject payload = new JSONObject();
			payload.put("sessionId", dto.getSessionId());
			payload.put("transactionRefNo", dto.getTransactionRefNo());
			WebResource webResource = client.resource(UrlMetadatas.getSuperadminRefundBillPay(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456")).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						final String message = jobj.getString("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setMessage(message);
					} else {
						resp.setSuccess(false);
						resp.setMessage("Response cannot be parsed");
					}
				} else {
					resp.setSuccess(false);
					resp.setMessage("strResponse is null");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setMessage("Exception Occurred");
		}
		return resp;
	}

	@Override
	public MobileSearchResponse getAccessUserList(SessionDTO request) {
		MobileSearchResponse resp = new MobileSearchResponse();
		try {
			Client client = Client.create();
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			WebResource webResource = client.resource(UrlMetadatas.getSuperadminAccessUsers(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456")).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (code.equalsIgnoreCase("S00")) {
							List<MobileSearchDTO> list = new ArrayList<>();
							resp.setSuccess(true);
							org.json.JSONArray details = JSONParserUtil.getArray(jobj, "details");
							if (details != null) {
								for (int i = 0; i < details.length(); i++) {
									org.json.JSONObject json = details.getJSONObject(i);
									MobileSearchDTO dto = new MobileSearchDTO();
									dto.setUrl(JSONParserUtil.getString(json, "mobileNumber"));
									dto.setSuggestion(JSONParserUtil.getString(json, "firstName"));
									list.add(dto);
								}
							}
							resp.setSearchList(list);
						} else {
							resp.setSuccess(false);
						}
					} else {
						resp.setSuccess(false);
					}
				} else {
					resp.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
		}
		return resp;
	}

	@Override
	public AccessListResponse getAccessList(SessionDTO request) {
		AccessListResponse resp = new AccessListResponse();
		try {
			Client client = Client.create();
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			WebResource webResource = client.resource(UrlMetadatas.getSuperadminAccessList(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456")).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (code.equalsIgnoreCase("S00")) {
							List<AccessDTO> list = new ArrayList<>();
							resp.setSuccess(true);
							org.json.JSONArray details = JSONParserUtil.getArray(jobj, "details");
							if (details != null) {
								for (int i = 0; i < details.length(); i++) {
									org.json.JSONObject json = details.getJSONObject(i);
									AccessDTO result = new AccessDTO();
									result.setUpdateAuthMerchants(JSONParserUtil.getBoolean(json, "updateAuthUser"));
									result.setUpdateAuthAdmin(JSONParserUtil.getBoolean(json, "updateAuthAdmin"));
									result.setUpdateAuthUser(JSONParserUtil.getBoolean(json, "updateAuthUser"));
									result.setUpdateAuthAgents(JSONParserUtil.getBoolean(json, "updateAuthAgents"));
									result.setSendGCM(JSONParserUtil.getBoolean(json, "sendGCM"));
									result.setSendSMS(JSONParserUtil.getBoolean(json, "sendSMS"));
									result.setSendMail(JSONParserUtil.getBoolean(json, "sendMail"));
									result.setUpdateLimits(JSONParserUtil.getBoolean(json, "updateLimits"));
									result.setLoadMoney(JSONParserUtil.getBoolean(json, "loadMoney"));
									result.setRequestMoney(JSONParserUtil.getBoolean(json, "requestMoney"));
									result.setSendMoney(JSONParserUtil.getBoolean(json, "sendMoney"));
									result.setDeductMoney(JSONParserUtil.getBoolean(json, "deductMoney"));
									result.setUsername(JSONParserUtil.getString(json, "username"));
									result.setAuthority(JSONParserUtil.getString(json, "authority"));
									list.add(result);
								}
							}
							resp.setList(list);
						} else {
							resp.setSuccess(false);
						}
					} else {
						resp.setSuccess(false);
					}
				} else {
					resp.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
		}
		return resp;
	}

	@Override
	public LoginResponse login(LoginRequest request) {
		LoginResponse resp = new LoginResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("username", request.getUsername());
			payload.put("password", request.getPassword());
			payload.put("ipAddress", request.getIpAddress());
			payload.put("validate", request.isValidate());
			payload.put("mobileToken", request.getMobileToken());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getLoginUrl(Version.VERSION_1, Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);

			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				logger.info("Response 1" + response.getStatus());
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							final String sessionId = (String) jobj.getJSONObject("details").get("sessionId");
							resp.setSuccess(true);
							resp.setSessionId(sessionId);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public CPasswordResponse requestChangePassword(CPasswordRequest request) {
		CPasswordResponse resp = new CPasswordResponse();
		try {
			Client client = Client.create();
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("password", request.getCurrentPassword());
			payload.put("newPassword", request.getNewPassword());
			payload.put("confirmPassword", request.getNewPassword());
			payload.put("request", true);
			WebResource webResource = client.resource(UrlMetadatas.getSuperAdminChangePasswordUrl(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456")).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						resp.setCode(code);
						if (code.equalsIgnoreCase("S00")) {
							resp.setMessage(JSONParserUtil.getString(jobj, "message"));
							resp.setSuccess(true);
						} else if (code.equalsIgnoreCase("F04")) {
							resp.setSuccess(false);
							org.json.JSONObject details = jobj.getJSONObject("details");
							if (details != null) {
								resp.setPassword(
										(details.getString("password") == null) ? "" : details.getString("password"));
							}
						} else {
							resp.setSuccess(false);
							resp.setMessage("Please try Later");
						}
					} else {
						resp.setSuccess(false);
					}
				} else {
					resp.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
		}
		return resp;
	}

	@Override
	public CPasswordResponse processChangePassword(CPasswordOTPRequest request) {
		CPasswordResponse resp = new CPasswordResponse();
		try {
			Client client = Client.create();
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("password", request.getCurrentPassword());
			payload.put("newPassword", request.getNewPassword());
			payload.put("confirmPassword", request.getNewPassword());
			payload.put("key", request.getKey());
			payload.put("request", false);
			WebResource webResource = client.resource(UrlMetadatas.getSuperAdminChangePasswordUrl(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456")).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						resp.setCode(code);
						if (code.equalsIgnoreCase("S00")) {
							resp.setMessage(JSONParserUtil.getString(jobj, "message"));
							resp.setSuccess(true);
						} else if (code.equalsIgnoreCase("F04")) {
							resp.setSuccess(false);
							org.json.JSONObject details = jobj.getJSONObject("details");
							if (details != null) {
								resp.setMessage((details.getString("key") == null) ? "" : details.getString("key"));
							}
						} else {
							resp.setSuccess(false);
							resp.setMessage("Please try Later");
						}
					} else {
						resp.setSuccess(false);
					}
				} else {
					resp.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
		}
		return resp;
	}

	@Override
	public UserInfoResponse getUserInfo(UserInfoRequest request) {
		UserInfoResponse resp = new UserInfoResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("username", request.getUsername());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getUserInfoUrl(Version.VERSION_1, Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setMessage("Service unavailable");
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
							org.json.JSONObject details = JSONParserUtil.getObject(jobj, "details");
							if (details != null) {
								org.json.JSONArray loginLogs = JSONParserUtil.getArray(details, "loginLogs");
								org.json.JSONArray transactions = JSONParserUtil.getArray(details, "transactions");
								org.json.JSONObject user = JSONParserUtil.getObject(details, "user");
								org.json.JSONObject account = JSONParserUtil.getObject(details, "account");
								List<UserLoginInfo> logs = new ArrayList<>();
								if (loginLogs != null) {
									for (int j = 0; j < loginLogs.length(); j++) {
										org.json.JSONObject loginObj = loginLogs.getJSONObject(j);
										UserLoginInfo login = new UserLoginInfo();
										login.setLoginTime(JSONParserUtil.getString(loginObj, "loginTime"));
										login.setDeviceId(JSONParserUtil.getString(loginObj, "deviceId"));
										login.setStatus(JSONParserUtil.getString(loginObj, "status"));
										logs.add(login);
									}
								}
								List<UserTransactionsInfo> transactionList = new ArrayList<>();
								if (transactions != null) {
									for (int i = 0; i < transactions.length(); i++) {
										org.json.JSONObject transaction = transactions.getJSONObject(i);
										UserTransactionsInfo dto = new UserTransactionsInfo();
										dto.setDate(JSONParserUtil.getString(transaction, "date"));
										dto.setAmount(JSONParserUtil.getString(transaction, "amount"));
										dto.setCurrentBalance(JSONParserUtil.getString(transaction, "currentBalance"));
										dto.setDebit(JSONParserUtil.getBoolean(transaction, "debit"));
										dto.setFavourite(JSONParserUtil.getBoolean(transaction, "favourite"));
										dto.setCommissionIdentifier(
												JSONParserUtil.getString(transaction, "commissionIdentifier"));
										dto.setDescription(JSONParserUtil.getString(transaction, "description"));
										dto.setTransactionRefNo(
												JSONParserUtil.getString(transaction, "transactionRefNo"));
										dto.setStatus(JSONParserUtil.getString(transaction, "status"));
										transactionList.add(dto);
									}
								}
								UserBasicInfo basic = new UserBasicInfo();
								if (user != null) {
									basic.setRegistrationDate(JSONParserUtil.getString(user, "registrationDate"));
									basic.setAuthority(JSONParserUtil.getString(user, "authority"));
									basic.setEmailStatus(JSONParserUtil.getString(user, "emailStatus"));
									basic.setMobileStatus(JSONParserUtil.getString(user, "mobileStatus"));
									basic.setMobile(JSONParserUtil.getString(user, "mobile"));
									basic.setEmail(JSONParserUtil.getString(user, "email"));
									basic.setName(JSONParserUtil.getString(user, "name"));
									basic.setGender(JSONParserUtil.getString(user, "gender"));
									basic.setDateOfBirth(JSONParserUtil.getString(user, "dateOfBirth"));
									basic.setImage(JSONParserUtil.getString(user, "image"));
									basic.setCircleName(JSONParserUtil.getString(user, "circleName"));
									basic.setPinCode(JSONParserUtil.getString(user, "pinCode"));
									basic.setStateName(JSONParserUtil.getString(user, "stateName"));
									basic.setUsername(JSONParserUtil.getString(user, "username"));
								}
								UserAccountInfo ainfo = new UserAccountInfo();
								if (account != null) {
									ainfo.setBalance(JSONParserUtil.getString(account, "balance"));
									ainfo.setAccountNumber(JSONParserUtil.getString(account, "accountNumber"));
									ainfo.setDescription(JSONParserUtil.getString(account, "description"));
									ainfo.setPoints(JSONParserUtil.getString(account, "points"));
									ainfo.setBranchCode(JSONParserUtil.getString(account, "branchCode"));
									ainfo.setVijayaAccountNo(JSONParserUtil.getString(account, "vijayaAccountNo"));
									ainfo.setTransactionLimit(JSONParserUtil.getString(account, "transactionLimit"));
									ainfo.setBalanceLimit(JSONParserUtil.getString(account, "balanceLimit"));
									ainfo.setName(JSONParserUtil.getString(account, "name"));
									ainfo.setCode(JSONParserUtil.getString(account, "code"));
									ainfo.setDailyLimit(JSONParserUtil.getString(account, "dailyLimit"));
									ainfo.setMonthlyLimit(JSONParserUtil.getString(account, "monthlyLimit"));
									ainfo.setLinkedAccountName(JSONParserUtil.getString(account, "linkedAccountName"));
									ainfo.setLinkedAccountMobile(
											JSONParserUtil.getString(account, "linkedAccountMobile"));
								}
								resp.setLoginLogs(logs);
								resp.setOnline(JSONParserUtil.getBoolean(details, "online"));
								resp.setTotalCredit(JSONParserUtil.getString(details, "totalCredit"));
								resp.setTotalDebit(JSONParserUtil.getString(details, "totalDebit"));
								resp.setTransactions(transactionList);
								resp.setBasic(basic);
								resp.setAccount(ainfo);
							}
						} else {
							resp.setSuccess(false);
						}
						resp.setMessage(message);
						resp.setCode(code);
					} else {
						resp.setSuccess(false);
						resp.setMessage("Service unavailable");
					}
				} else {
					resp.setSuccess(false);
					resp.setMessage("Service unavailable");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setMessage("Service unavailable");
		}
		return resp;
	}

	@Override
	public ServiceTypeResponse getServiceType() {
		ServiceTypeResponse resp = new ServiceTypeResponse();
		try {
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getServiceTypesOfSuperAdminURL(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456")).get(ClientResponse.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (code.equalsIgnoreCase("S00")) {
							List<ServiceTypeDTO> serviceTypeDTOs = new ArrayList<>();
							resp.setSuccess(true);
							org.json.JSONArray details = JSONParserUtil.getArray(jobj, "details");
							if (details != null && details.length() > 0) {
								for (int i = 0; i < details.length(); i++) {
									ServiceTypeDTO s = new ServiceTypeDTO();
									org.json.JSONObject temp = (org.json.JSONObject) details.get(i);
									s.setId(JSONParserUtil.getLong(temp, "id"));
									s.setName(JSONParserUtil.getString(temp, "name"));
									s.setDescription(JSONParserUtil.getString(temp, "description"));
									serviceTypeDTOs.add(s);
								}
								resp.setServiceDTOs(serviceTypeDTOs);
							}
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
					} else {
						resp.setSuccess(false);
						resp.setCode("F00");
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
		}
		return resp;
	}

	/*
	 * @Override public ServiceTypeResponse getService() { ServiceTypeResponse
	 * resp = new ServiceTypeResponse(); try { Client client = Client.create();
	 * WebResource webResource = client.resource(
	 * UrlMetadatas.getServiceOfSuperAdminURL(Version.VERSION_1,
	 * Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH)); ClientResponse
	 * response =
	 * webResource.accept("application/json").type("application/json")
	 * .header("hash",
	 * SecurityUtils.getHash("123456")).get(ClientResponse.class); String
	 * strResponse = response.getEntity(String.class); if (response.getStatus()
	 * != 200) { resp.setSuccess(false); resp.setCode("F00"); } else { if
	 * (strResponse != null) { org.json.JSONObject jobj = new
	 * org.json.JSONObject(strResponse); if (jobj != null) { final String code =
	 * (String) jobj.get("code"); if (code.equalsIgnoreCase("S00")) {
	 * List<com.payqwikweb.app.model.ServicesDTO> serviceTypeDTOs = new
	 * ArrayList<>(); resp.setSuccess(true); org.json.JSONArray details =
	 * JSONParserUtil.getArray(jobj,"details"); if(details != null &&
	 * details.length() > 0) { for (int i=0;i<details.length();i++){
	 * com.payqwikweb.app.model.ServicesDTO s = new
	 * com.payqwikweb.app.model.ServicesDTO(); org.json.JSONObject temp =
	 * (org.json.JSONObject) details.get(i);
	 * s.setCode(JSONParserUtil.getString(temp,"code"));
	 * s.setDescription(JSONParserUtil.getString(temp,"description"));
	 * serviceTypeDTOs.add(s); } resp.setServicesDTOs(serviceTypeDTOs); } } else
	 * { resp.setSuccess(false); } resp.setCode(code); } else {
	 * resp.setSuccess(false); resp.setCode("F00"); } } else {
	 * resp.setSuccess(false); resp.setCode("F00"); } } } catch (Exception e) {
	 * e.printStackTrace(); resp.setSuccess(false); resp.setCode("F00"); }
	 * return resp; }
	 */

	@Override
	public GCMResponse getGCMIds(PagingDTO dto) {
		GCMResponse resp = new GCMResponse();
		try {
			Client client = Client.create();
			JSONObject payload = new JSONObject();
			payload.put("sessionId", dto.getSessionId());
			payload.put("page", dto.getPage());
			payload.put("size", dto.getSize());
			WebResource webResource = client.resource(
					UrlMetadatas.getGCMIDs(Version.VERSION_1, Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456")).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (code.equalsIgnoreCase("S00")) {
							List<String> gcmIds = new ArrayList<>();
							resp.setSuccess(true);
							org.json.JSONObject details = JSONParserUtil.getObject(jobj, "details");
							if (details != null) {
								long totalPages = JSONParserUtil.getLong(details, "totalPages");
								org.json.JSONArray content = JSONParserUtil.getArray(details, "content");
								if (content != null) {
									for (int i = 0; i < content.length(); i++) {
										gcmIds.add(content.getString(i));
									}
									resp.setGcmList(gcmIds);
									resp.setTotalPages(totalPages);
								}
							}
						} else {
							resp.setSuccess(false);
						}
					} else {
						resp.setSuccess(false);
					}
				} else {
					resp.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
		}
		return resp;
	}

	@Override
	public GCMResponse saveGCMIds(CronNotificationDTO dto) {
		GCMResponse resp = new GCMResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", dto.getSessionId());
			payload.put("message", dto.getMessage());
			payload.put("title", dto.getTitle());
			payload.put("image", dto.getImage());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.saveGCMIDs(Version.VERSION_1, Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456")).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setMessage(message);
					}
				} else {
					resp.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
		}
		return resp;
	}

	@Override
	public GCMResponse getGCMIdByUserName(String userName, String sessionId) {
		GCMResponse resp = new GCMResponse();
		try {
			Client client = Client.create();
			JSONObject payload = new JSONObject();
			payload.put("sessionId", sessionId);
			payload.put("username", userName);
			WebResource webResource = client.resource(UrlMetadatas.getAdminGCMIDByUserName(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456")).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (code.equalsIgnoreCase("S00")) {
							List<String> gcmIds = new ArrayList<>();
							resp.setSuccess(true);
							String details = JSONParserUtil.getString(jobj, "details");
							if (!CommonValidation.isNull(details)) {
								gcmIds.add(details);
								resp.setGcmList(gcmIds);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
		}
		return resp;
	}

	@Override
	public SMSResponse sendSingleSMS(SMSRequest dto) {
		SMSResponse resp = new SMSResponse();
		try {
			Client client = Client.create();
			JSONObject payload = new JSONObject();
			payload.put("sessionId", dto.getSessionId());
			payload.put("content", dto.getContent());
			payload.put("mobile", dto.getMobile());
			WebResource webResource = client.resource(
					UrlMetadatas.sendSMS(Version.VERSION_1, Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456")).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
							resp.setMessage(JSONParserUtil.getString(jobj, "message"));
						} else {
							resp.setSuccess(false);
							resp.setMessage(JSONParserUtil.getString(jobj, "message"));
						}
					} else {
						resp.setSuccess(false);
						resp.setMessage(JSONParserUtil.getString(jobj, "message"));
					}
				} else {
					resp.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
		}
		return resp;

	}

	@Override
	public SMSResponse sendBulkSMS(BulkSMSRequest dto) {
		SMSResponse resp = new SMSResponse();
		try {
			Client client = Client.create();
			JSONObject payload = new JSONObject();
			payload.put("sessionId", dto.getSessionId());
			payload.put("content", dto.getContent());
			WebResource webResource = client.resource(
					UrlMetadatas.sendBulkSMS(Version.VERSION_1, Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456")).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
							resp.setMessage(JSONParserUtil.getString(jobj, "message"));
						} else {
							resp.setSuccess(false);
							resp.setMessage(JSONParserUtil.getString(jobj, "message"));
						}
					} else {
						resp.setSuccess(false);
						resp.setMessage(JSONParserUtil.getString(jobj, "message"));
					}
				} else {
					resp.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
		}
		return resp;
	}

	@Override
	public MailResponse sendSingleMail(MailRequest dto) {
		MailResponse mailResponse = new MailResponse();
		try {
			Client client = Client.create();
			JSONObject payload = new JSONObject();
			payload.put("sessionId", dto.getSessionId());
			payload.put("content", dto.getContent());
			payload.put("destination", dto.getDestination());
			payload.put("subject", dto.getSubject());
			WebResource webResource = client.resource(
					UrlMetadatas.sendMail(Version.VERSION_1, Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456")).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				mailResponse.setSuccess(false);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (code.equalsIgnoreCase("S00")) {
							mailResponse.setSuccess(true);
							mailResponse.setMessage(JSONParserUtil.getString(jobj, "message"));
						} else {
							mailResponse.setSuccess(false);
							mailResponse.setMessage(JSONParserUtil.getString(jobj, "message"));
						}
					} else {
						mailResponse.setSuccess(false);
						mailResponse.setMessage(JSONParserUtil.getString(jobj, "message"));
					}
				} else {
					mailResponse.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			mailResponse.setSuccess(false);
		}
		return mailResponse;
	}

	@Override
	public MailResponse sendBulkMail(BulkMailRequest dto) {
		MailResponse mailResponse = new MailResponse();
		try {
			Client client = Client.create();
			JSONObject payload = new JSONObject();
			payload.put("sessionId", dto.getSessionId());
			payload.put("content", dto.getContent());
			payload.put("subject", dto.getSubject());
			WebResource webResource = client.resource(
					UrlMetadatas.sendBulkMail(Version.VERSION_1, Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456")).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				mailResponse.setSuccess(false);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (code.equalsIgnoreCase("S00")) {
							mailResponse.setSuccess(true);
							mailResponse.setMessage(JSONParserUtil.getString(jobj, "message"));
						} else {
							mailResponse.setSuccess(false);
							mailResponse.setMessage(JSONParserUtil.getString(jobj, "message"));
						}
					} else {
						mailResponse.setSuccess(false);
						mailResponse.setMessage(JSONParserUtil.getString(jobj, "message"));
					}
				} else {
					mailResponse.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			mailResponse.setSuccess(false);
		}
		return mailResponse;
	}

	@Override
	public MobileSearchResponse getPossiblities(MobileSearchRequest request) {
		MobileSearchResponse resp = new MobileSearchResponse();
		try {
			Client client = Client.create();
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("mobileSubString", request.getMobileSubString());
			WebResource webResource = client.resource(
					UrlMetadatas.getPossiblities(Version.VERSION_1, Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456")).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (code.equalsIgnoreCase("S00")) {
							List<MobileSearchDTO> list = new ArrayList<>();
							resp.setSuccess(true);
							org.json.JSONArray details = JSONParserUtil.getArray(jobj, "details");
							if (details != null) {
								for (int i = 0; i < details.length(); i++) {
									org.json.JSONObject json = details.getJSONObject(i);
									MobileSearchDTO dto = new MobileSearchDTO();
									dto.setUrl(JSONParserUtil.getString(json, "mobileNumber"));
									dto.setSuggestion(JSONParserUtil.getString(json, "firstName"));
									list.add(dto);
								}
							}
							resp.setSearchList(list);
						} else {
							resp.setSuccess(false);
						}
					} else {
						resp.setSuccess(false);
					}
				} else {
					resp.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
		}
		return resp;
	}

	@Override
	public UserListResponse getUsersList(UserListRequest request, String type) {
		UserListResponse resp = new UserListResponse();
		try {
			Client client = Client.create();
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			WebResource webResource = client.resource(UrlMetadatas.getSuperadminUserlistNow(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH, type));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456")).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (code.equalsIgnoreCase("S00")) {
							List<UserListDTO> users = new ArrayList<>();
							resp.setSuccess(true);
							org.json.JSONArray details = JSONParserUtil.getArray(jobj, "details");
							if (details != null && details.length() > 0) {
								for (int i = 0; i < details.length(); i++) {
									org.json.JSONObject json = details.getJSONObject(i);
									UserListDTO dto = new UserListDTO();
									dto.setAccountType(JSONParserUtil.getString(json, "accountType"));
									dto.setCircleName(JSONParserUtil.getString(json, "circleName"));
									dto.setAuthority(JSONParserUtil.getString(json, "authority"));
									dto.setBalance(JSONParserUtil.getString(json, "balance"));
									dto.setDateOfBirth(JSONParserUtil.getString(json, "dateOfBirth"));
									dto.setEmail(JSONParserUtil.getString(json, "email"));
									dto.setMobile(JSONParserUtil.getString(json, "mobile"));
									dto.setGender(JSONParserUtil.getString(json, "gender"));
									dto.setName(JSONParserUtil.getString(json, "name"));
									dto.setPinCode(JSONParserUtil.getString(json, "pinCode"));
									dto.setPoints(JSONParserUtil.getString(json, "points"));
									dto.setRegistrationDate(JSONParserUtil.getString(json, "registrationDate"));
									dto.setVbankAccountNumber(JSONParserUtil.getString(json, "vbankAccountNumber"));
									users.add(dto);
								}
								resp.setUserList(users);
							} else {
								resp.setSuccess(false);
								resp.setMessage("User List Empty");
							}
						} else {
							resp.setSuccess(false);
							resp.setMessage("Please try Later");
						}
					} else {
						resp.setSuccess(false);
					}
				} else {
					resp.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
		}
		return resp;
	}

	@Override
	public UserListResponse getUsersListWithFilter(UserListRequest request, String type) {
		UserListResponse resp = new UserListResponse();
		try {
			Client client = Client.create();
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("startDate", request.getStartDate());
			payload.put("endDate", request.getEndDate());
			WebResource webResource = client.resource(UrlMetadatas.getSuperadminUserlistFilter(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH, type));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456")).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (code.equalsIgnoreCase("S00")) {
							List<UserListDTO> users = new ArrayList<>();
							resp.setSuccess(true);
							org.json.JSONArray details = JSONParserUtil.getArray(jobj, "details");
							if (details != null && details.length() > 0) {
								for (int i = 0; i < details.length(); i++) {
									org.json.JSONObject json = details.getJSONObject(i);
									UserListDTO dto = new UserListDTO();
									dto.setAccountType(JSONParserUtil.getString(json, "accountType"));
									dto.setCircleName(JSONParserUtil.getString(json, "circleName"));
									dto.setAuthority(JSONParserUtil.getString(json, "authority"));
									dto.setBalance(JSONParserUtil.getString(json, "balance"));
									dto.setDateOfBirth(JSONParserUtil.getString(json, "dateOfBirth"));
									dto.setEmail(JSONParserUtil.getString(json, "email"));
									dto.setMobile(JSONParserUtil.getString(json, "mobile"));
									dto.setGender(JSONParserUtil.getString(json, "gender"));
									dto.setName(JSONParserUtil.getString(json, "name"));
									dto.setPinCode(JSONParserUtil.getString(json, "pinCode"));
									dto.setPoints(JSONParserUtil.getString(json, "points"));
									dto.setRegistrationDate(JSONParserUtil.getString(json, "registrationDate"));
									dto.setVbankAccountNumber(JSONParserUtil.getString(json, "vbankAccountNumber"));
									users.add(dto);
								}
								resp.setUserList(users);
							} else {
								resp.setSuccess(false);
								resp.setMessage("User List Empty");
							}
						} else {
							resp.setSuccess(false);
							resp.setMessage("Please try Later");
						}
					} else {
						resp.setSuccess(false);
					}
				} else {
					resp.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
		}
		return resp;
	}

	@Override
	public TListResponse getTransactionList(AllTransactionRequest request) {
		TListResponse resp = new TListResponse();
		try {
			Client client = Client.create();
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			WebResource webResource = client.resource(UrlMetadatas.getSuperadminTransactionlistNow(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456")).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (code.equalsIgnoreCase("S00")) {
							List<TListDTO> transactions = new ArrayList<>();
							resp.setSuccess(true);
							org.json.JSONArray details = JSONParserUtil.getArray(jobj, "details");
							if (details != null && details.length() > 0) {
								for (int i = 0; i < details.length(); i++) {
									org.json.JSONObject json = details.getJSONObject(i);
									TListDTO dto = new TListDTO();
									dto.setUsername(JSONParserUtil.getString(json, "username"));
									dto.setEmail(JSONParserUtil.getString(json, "email"));
									dto.setContactNo(JSONParserUtil.getString(json, "contactNo"));
									dto.setTransactionRefNo(JSONParserUtil.getString(json, "transactionRefNo"));
									dto.setCurrentBalance(JSONParserUtil.getString(json, "currentBalance"));
									dto.setDateOfTransaction(JSONParserUtil.getString(json, "dateOfTransaction"));
									dto.setDebit(JSONParserUtil.getString(json, "debit"));
									dto.setAmount(JSONParserUtil.getString(json, "amount"));
									dto.setStatus(JSONParserUtil.getString(json, "status"));
									dto.setDescription(JSONParserUtil.getString(json, "description"));
									transactions.add(dto);
								}
								resp.setList(transactions);
							} else {
								resp.setSuccess(false);
								resp.setMessage("Transaction List Empty");
							}
						} else {
							resp.setSuccess(false);
							resp.setMessage("Please try Later");
						}
					} else {
						resp.setSuccess(false);
					}
				} else {
					resp.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
		}
		return resp;
	}

	@Override
	public SMSLogResponse getSMSLogs(AllTransactionRequest request) {
		SMSLogResponse resp = new SMSLogResponse();
		try {
			Client client = Client.create();
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			WebResource webResource = client.resource(UrlMetadatas.getSuperadminLogslistNow(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH, "SMS"));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456")).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (code.equalsIgnoreCase("S00")) {
							List<SMSLogDTO> logs = new ArrayList<>();
							resp.setSuccess(true);
							org.json.JSONArray details = JSONParserUtil.getArray(jobj, "details");
							if (details != null && details.length() > 0) {
								for (int i = 0; i < details.length(); i++) {
									org.json.JSONObject json = details.getJSONObject(i);
									SMSLogDTO dto = new SMSLogDTO();
									dto.setExecutionTime(JSONParserUtil.getLong(json, "created"));
									dto.setMessage(JSONParserUtil.getString(json, "message"));
									dto.setDestination(JSONParserUtil.getString(json, "destination"));
									dto.setResponse(JSONParserUtil.getString(json, "response"));
									dto.setSender(JSONParserUtil.getString(json, "sender"));
									dto.setTemplate(JSONParserUtil.getString(json, "template"));
									logs.add(dto);
								}
								resp.setLogList(logs);
							} else {
								resp.setSuccess(false);
								resp.setMessage("Logs Empty");
							}
						} else {
							resp.setSuccess(false);
							resp.setMessage("Please try Later");
						}
					} else {
						resp.setSuccess(false);
					}
				} else {
					resp.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
		}
		return resp;
	}

	@Override
	public SMSLogResponse getSMSLogsFilter(AllTransactionRequest request) {
		SMSLogResponse resp = new SMSLogResponse();
		try {
			Client client = Client.create();
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("startDate", request.getStartDate());
			payload.put("endDate", request.getEndDate());
			WebResource webResource = client.resource(UrlMetadatas.getSuperadminLogslistFilter(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH, "SMS"));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456")).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (code.equalsIgnoreCase("S00")) {
							List<SMSLogDTO> logs = new ArrayList<>();
							resp.setSuccess(true);
							org.json.JSONArray details = JSONParserUtil.getArray(jobj, "details");
							if (details != null && details.length() > 0) {
								for (int i = 0; i < details.length(); i++) {
									org.json.JSONObject json = details.getJSONObject(i);
									SMSLogDTO dto = new SMSLogDTO();
									dto.setExecutionTime(JSONParserUtil.getLong(json, "created"));
									dto.setMessage(JSONParserUtil.getString(json, "message"));
									dto.setDestination(JSONParserUtil.getString(json, "destination"));
									dto.setResponse(JSONParserUtil.getString(json, "response"));
									dto.setSender(JSONParserUtil.getString(json, "sender"));
									dto.setTemplate(JSONParserUtil.getString(json, "template"));
									logs.add(dto);
								}
								resp.setLogList(logs);
							} else {
								resp.setSuccess(false);
								resp.setMessage("Logs Empty");
							}
						} else {
							resp.setSuccess(false);
							resp.setMessage("Please try Later");
						}
					} else {
						resp.setSuccess(false);
					}
				} else {
					resp.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
		}
		return resp;
	}

	@Override
	public MailLogResponse getMailLogs(AllTransactionRequest request) {
		MailLogResponse resp = new MailLogResponse();
		try {
			Client client = Client.create();
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			WebResource webResource = client.resource(UrlMetadatas.getSuperadminLogslistNow(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH, "EMAIL"));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456")).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (code.equalsIgnoreCase("S00")) {
							List<MailLogDTO> logs = new ArrayList<>();
							resp.setSuccess(true);
							org.json.JSONArray details = JSONParserUtil.getArray(jobj, "details");
							if (details != null && details.length() > 0) {
								for (int i = 0; i < details.length(); i++) {
									org.json.JSONObject json = details.getJSONObject(i);
									MailLogDTO dto = new MailLogDTO();
									dto.setExecutionTime(JSONParserUtil.getLong(json, "created"));
									dto.setDestination(JSONParserUtil.getString(json, "destination"));
									dto.setStatus(JSONParserUtil.getString(json, "status"));
									dto.setSender(JSONParserUtil.getString(json, "sender"));
									dto.setMailTemplate(JSONParserUtil.getString(json, "mailTemplate"));
									logs.add(dto);
								}
								resp.setLogList(logs);
							} else {
								resp.setSuccess(false);
								resp.setMessage("Logs Empty");
							}
						} else {
							resp.setSuccess(false);
							resp.setMessage("Please try Later");
						}
					} else {
						resp.setSuccess(false);
					}
				} else {
					resp.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
		}
		return resp;
	}

	@Override
	public MailLogResponse getMailLogsFilter(AllTransactionRequest request) {
		MailLogResponse resp = new MailLogResponse();
		try {
			Client client = Client.create();
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("startDate", request.getStartDate());
			payload.put("endDate", request.getEndDate());
			WebResource webResource = client.resource(UrlMetadatas.getSuperadminLogslistFilter(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH, "EMAIL"));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456")).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (code.equalsIgnoreCase("S00")) {
							List<MailLogDTO> logs = new ArrayList<>();
							resp.setSuccess(true);
							org.json.JSONArray details = JSONParserUtil.getArray(jobj, "details");
							if (details != null && details.length() > 0) {
								for (int i = 0; i < details.length(); i++) {
									org.json.JSONObject json = details.getJSONObject(i);
									MailLogDTO dto = new MailLogDTO();
									dto.setExecutionTime(JSONParserUtil.getLong(json, "created"));
									dto.setDestination(JSONParserUtil.getString(json, "destination"));
									dto.setStatus(JSONParserUtil.getString(json, "status"));
									dto.setSender(JSONParserUtil.getString(json, "sender"));
									dto.setMailTemplate(JSONParserUtil.getString(json, "mailTemplate"));
									logs.add(dto);
								}
								resp.setLogList(logs);
							} else {
								resp.setSuccess(false);
								resp.setMessage("Logs Empty");
							}
						} else {
							resp.setSuccess(false);
							resp.setMessage("Please try Later");
						}
					} else {
						resp.setSuccess(false);
					}
				} else {
					resp.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
		}
		return resp;
	}

	@Override
	public ServiceListResponse getServiceList(SessionDTO request) {
		ServiceListResponse resp = new ServiceListResponse();
		try {
			Client client = Client.create();
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			WebResource webResource = client.resource(UrlMetadatas.getSuperadminServicelistNow(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456")).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (code.equalsIgnoreCase("S00")) {
							List<ServicesDTO> services = new ArrayList<>();
							resp.setSuccess(true);
							org.json.JSONArray details = JSONParserUtil.getArray(jobj, "details");
							if (details != null && details.length() > 0) {
								for (int i = 0; i < details.length(); i++) {
									org.json.JSONObject json = details.getJSONObject(i);
									ServicesDTO dto = new ServicesDTO();
									dto.setName(JSONParserUtil.getString(json, "name"));
									dto.setCode(JSONParserUtil.getString(json, "code"));
									dto.setDescription(JSONParserUtil.getString(json, "description"));
									dto.setMaxAmount(JSONParserUtil.getString(json, "maxAmount"));
									dto.setMinAmount(JSONParserUtil.getString(json, "minAmount"));
									dto.setTypeDescription(JSONParserUtil.getString(json, "typeDescription"));
									dto.setTypeName(JSONParserUtil.getString(json, "typeName"));
									dto.setStatus(JSONParserUtil.getString(json, "status"));
									List<CommissionDTO> commissions = new ArrayList<>();
									org.json.JSONArray commission = JSONParserUtil.getArray(json, "list");
									if (commission != null) {
										for (int j = 0; j < commission.length(); j++) {
											org.json.JSONObject commissionObj = commission.getJSONObject(j);
											CommissionDTO commissionDTO = new CommissionDTO();
											commissionDTO
													.setMaxAmount(JSONParserUtil.getString(commissionObj, "maxAmount"));
											commissionDTO
													.setMinAmount(JSONParserUtil.getString(commissionObj, "minAmount"));
											commissionDTO.setFixed(JSONParserUtil.getString(commissionObj, "fixed"));
											commissionDTO.setIdentifier(
													JSONParserUtil.getString(commissionObj, "identifier"));
											commissionDTO.setValue(JSONParserUtil.getString(commissionObj, "value"));
											commissions.add(commissionDTO);
										}
									}
									dto.setCommissionList(commissions);
									services.add(dto);
								}
								resp.setServiceList(services);
							} else {
								resp.setSuccess(false);
								resp.setMessage("Services Empty");
							}
						} else {
							resp.setSuccess(false);
							resp.setMessage("Please try Later");
						}
					} else {
						resp.setSuccess(false);
					}
				} else {
					resp.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
		}
		return resp;
	}

	@Override
	public ServicesDTO updateServiceList(ServiceRequest request) {
		ServicesDTO resp = new ServicesDTO();
		try {
			Client client = Client.create();
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("code", request.getCode());
			payload.put("status", request.getStatus());
			WebResource webResource = client.resource(
					UrlMetadatas.updateServices(Version.VERSION_1, Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456")).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						final String message = jobj.getString("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
							org.json.JSONObject details = jobj.getJSONObject("details");
							if (details != null) {
								org.json.JSONObject service = details.getJSONObject("service");
								if (service != null) {
									resp.setSuccess(true);
									resp.setCode(service.getString("code"));
									resp.setStatus(service.getString("status"));
								}
							}
						} else {
							resp.setSuccess(false);
							resp.setMessage(message);
						}
					} else {
						resp.setSuccess(false);
						resp.setMessage("Please try Later");
					}
				} else {
					resp.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
		}
		return resp;
	}

	@Override
	public TListResponse getTransactionListWithFilter(AllTransactionRequest request) {
		TListResponse resp = new TListResponse();
		try {
			Client client = Client.create();
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("startDate", request.getStartDate());
			payload.put("endDate", request.getEndDate());
			WebResource webResource = client.resource(UrlMetadatas.getSuperadminTransactionlistFilter(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456")).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (code.equalsIgnoreCase("S00")) {
							List<TListDTO> transactions = new ArrayList<>();
							resp.setSuccess(true);
							org.json.JSONArray details = JSONParserUtil.getArray(jobj, "details");
							if (details != null && details.length() > 0) {
								for (int i = 0; i < details.length(); i++) {
									org.json.JSONObject json = details.getJSONObject(i);
									TListDTO dto = new TListDTO();
									dto.setUsername(JSONParserUtil.getString(json, "username"));
									dto.setEmail(JSONParserUtil.getString(json, "email"));
									dto.setContactNo(JSONParserUtil.getString(json, "contactNo"));
									dto.setTransactionRefNo(JSONParserUtil.getString(json, "transactionRefNo"));
									dto.setCurrentBalance(JSONParserUtil.getString(json, "currentBalance"));
									dto.setDateOfTransaction(JSONParserUtil.getString(json, "dateOfTransaction"));
									dto.setDebit(JSONParserUtil.getString(json, "debit"));
									dto.setAmount(JSONParserUtil.getString(json, "amount"));
									dto.setStatus(JSONParserUtil.getString(json, "status"));
									dto.setDescription(JSONParserUtil.getString(json, "description"));
									transactions.add(dto);
								}
								resp.setList(transactions);
							} else {
								resp.setSuccess(false);
								resp.setMessage("Transaction List Empty");
							}
						} else {
							resp.setSuccess(false);
							resp.setMessage("Please try Later");
						}
					} else {
						resp.setSuccess(false);
					}
				} else {
					resp.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
		}
		return resp;
	}

	@Override
	public AllTransactionResponse getAllTransaction(AllTransactionRequest request) {
		AllTransactionResponse resp = new AllTransactionResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("page", request.getPage());
			payload.put("size", request.getSize());
			payload.put("start", request.getStartDate());
			payload.put("end", request.getEndDate());
			payload.put("userStatus", "");
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getAllTransactionOfSuperAdminUrl(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						final org.json.JSONObject totalTtransaction = (org.json.JSONObject) jobj
								.getJSONObject("details");
						org.json.JSONArray totalTransactionArray = null;
						if (totalTtransaction != null) {
							totalTransactionArray = totalTtransaction.getJSONArray("content");
						}
						JSONArray jsonArray = new JSONArray();
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
						resp.setMonthlyTransaction(totalTtransaction.length());
						resp.setJsonArray(totalTransactionArray);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public AllTransactionResponse getSettlementTransactions(AllTransactionRequest request) {
		AllTransactionResponse resp = new AllTransactionResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("startDate", request.getStartDate());
			payload.put("endDate", request.getEndDate());
			payload.put("reportType", request.getReportType().toUpperCase());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getSettlementsTransactionOfSuperAdminURL(
					Version.VERSION_1, Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						org.json.JSONArray totalTransactionArray = null;
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
							totalTransactionArray = jobj.getJSONArray("details");
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
						resp.setJsonArray(totalTransactionArray);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public AllTransactionResponse getPromoTransaction(AllTransactionRequest request) {
		AllTransactionResponse resp = new AllTransactionResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getPromoTransactionsOfSuperAdminURL(
					Version.VERSION_1, Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (code.equalsIgnoreCase("S00")) {
							List<AdminUserDetails> userList = new ArrayList<AdminUserDetails>();
							resp.setSuccess(true);
							org.json.JSONArray details = JSONParserUtil.getArray(jobj, "details");
							if (details != null && details.length() > 0) {
								for (int i = 0; i < details.length(); i++) {
									org.json.JSONObject t = details.getJSONObject(i);
									AdminUserDetails userDetails = new AdminUserDetails();
									long milliseconds = JSONParserUtil.getLong(t, "date");
									Calendar calendar = Calendar.getInstance();
									calendar.setTimeInMillis(milliseconds);
									userDetails.setDateOfTransaction(simformat.format(calendar.getTime()));
									userDetails.setTransactionRefNo(JSONParserUtil.getString(t, "transactionRefNo"));
									userDetails.setCurrentBalance(
											String.valueOf(JSONParserUtil.getDouble(t, "currentBalance")));
									userDetails.setContactNo(JSONParserUtil.getString(t, "contactNo"));
									userDetails.setAmount(String.valueOf(JSONParserUtil.getDouble(t, "amount")));
									userDetails.setFirstName(JSONParserUtil.getString(t, "description"));
									userDetails.setEmail(JSONParserUtil.getString(t, "email"));
									userDetails.setCurrentBalance(JSONParserUtil.getString(t, "currentBalance"));
									userDetails.setStatus(JSONParserUtil.getString(t, "status"));
									userList.add(userDetails);
								}
								resp.setList(userList);
							} else {
								resp.setSuccess(false);
								resp.setMessage("Transaction List Empty");
							}
						} else {
							resp.setSuccess(false);
							resp.setMessage("Please try Later");
						}
					} else {
						resp.setSuccess(false);
					}
				} else {
					resp.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
		}
		return resp;
	}

	@Override
	public UserTransactionResponse getUserTransactionValues(SessionDTO dto) {
		UserTransactionResponse resp = new UserTransactionResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", dto.getSessionId());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getValuesListOfSuperAdminURL(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public AllUserResponse getAllUser(AllUserRequest request) {
		AllUserResponse resp = new AllUserResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("page", request.getPage());
			payload.put("size", request.getSize());
			payload.put("userStatus", request.getStatus().getValue());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getAllUserOfSuperAdminUrl(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						final org.json.JSONObject details = jobj.getJSONObject("details");
						final org.json.JSONArray totalUser = (org.json.JSONArray) jobj.getJSONObject("details")
								.getJSONArray("content");
						long numberOfElements = details.getLong("numberOfElements");
						boolean firstPage = details.getBoolean("firstPage");
						boolean lastPage = details.getBoolean("lastPage");
						long size = details.getLong("size");
						long totalPages = details.getLong("totalPages");
						resp.setTotalPages(totalPages);
						resp.setSize(size);
						resp.setNumberOfElements(numberOfElements);
						resp.setFirstPage(firstPage);
						resp.setLastPage(lastPage);
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
						resp.setTotalUser(totalUser.length());
						resp.setJsonArray(totalUser);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public AllUserResponse getAllMerchants(AllUserRequest request) {
		AllUserResponse resp = new AllUserResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("page", request.getPage());
			payload.put("size", request.getSize());
			payload.put("userStatus", request.getStatus().getValue());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getAllMerchantOfSuperAdminURL(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							final org.json.JSONArray details = jobj.getJSONArray("details");
							resp.setJsonArray(details);
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public UserTransactionResponse getUserTransaction(UserTransactionRequest request) {
		UserTransactionResponse resp = new UserTransactionResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("page", request.getPage());
			payload.put("size", request.getSize());
			payload.put("username", request.getUsername());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getUserTransactionOfSuperAdminUrl(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public UserTransactionResponse refundLoadMoneyTransactions(RefundDTO dto) {
		UserTransactionResponse resp = new UserTransactionResponse();
		try {
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getRefundAmountOfSuperAdminURL(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(dto.toJSON().toString()))
					.post(ClientResponse.class, dto.toJSON());
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public MessageLogResponse getMessageLog(MessageLogRequest request) {
		MessageLogResponse resp = new MessageLogResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("start", request.getStartDate());
			payload.put("end", request.getEndDate());
			payload.put("sessionId", request.getSessionId());
			payload.put("username", "");
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getMessageLogOfSuperAdminUrl(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						final org.json.JSONArray totalTtransaction = (org.json.JSONArray) jobj.getJSONArray("details");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
						resp.setJsonArray(totalTtransaction);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public EmailLogResponse getEmailLog(EmailLogRequest request) {
		EmailLogResponse resp = new EmailLogResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("start", request.getStartDate());
			payload.put("end", request.getEndDate());
			payload.put("username", "");
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getEmailLogOfSuperAdminUrl(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						final org.json.JSONArray totalTtransaction = (org.json.JSONArray) jobj.getJSONArray("details");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
						resp.setJsonArray(totalTtransaction);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public BlockUnBlockUserResponse blockUser(BlockUnBlockUserRequest request) {
		BlockUnBlockUserResponse resp = new BlockUnBlockUserResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("username", request.getUsername());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.blockUserOfSuperAdminUrl(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public BlockUnBlockUserResponse unblockUser(BlockUnBlockUserRequest request) {
		BlockUnBlockUserResponse resp = new BlockUnBlockUserResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("username", request.getUsername());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.unblockUserOfSuperAdminUrl(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public BlockUnBlockUserResponse kycUpdate(BlockUnBlockUserRequest request) {
		BlockUnBlockUserResponse resp = new BlockUnBlockUserResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("username", request.getUsername());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.kycUpdateOfSuperAdminUrl(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public BlockUnBlockUserResponse NonKycUpdate(BlockUnBlockUserRequest request) {
		BlockUnBlockUserResponse resp = new BlockUnBlockUserResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("username", request.getUsername());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.nonKycUpdateOfSuperAdminUrl(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");

						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public AllTransactionResponse getDaily(AllTransactionRequest request) {

		AllTransactionResponse resp = new AllTransactionResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("start", request.getStartDate());
			payload.put("end", request.getEndDate());
			payload.put("username", "");
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getBetweenDateTransactionsOfSuperAdmin(
					Version.VERSION_1, Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						final org.json.JSONArray totalTtransaction = (org.json.JSONArray) jobj.getJSONArray("details");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
						resp.setMonthlyTransaction(totalTtransaction.length());
						resp.setJsonArray(totalTtransaction);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public AllUserResponse getAllTransactions(AllTransactionRequest request) {

		AllUserResponse resp = new AllUserResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("page", request.getPage());
			payload.put("size", request.getSize());
			payload.put("reportType", "ALL");
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getAllTransactionsOfSuperAdminURL(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						final org.json.JSONObject details = jobj.getJSONObject("details");
						final org.json.JSONArray totalUser = (org.json.JSONArray) jobj.getJSONObject("details")
								.getJSONArray("content");
						long numberOfElements = details.getLong("numberOfElements");
						boolean firstPage = details.getBoolean("firstPage");
						boolean lastPage = details.getBoolean("lastPage");
						long size = details.getLong("size");
						long totalPages = details.getLong("totalPages");
						resp.setTotalPages(totalPages);
						resp.setSize(size);
						resp.setNumberOfElements(numberOfElements);
						resp.setFirstPage(firstPage);
						resp.setLastPage(lastPage);
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
						resp.setTotalUser(totalUser.length());
						resp.setJsonArray(totalUser);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public AllTransactionResponse getSingleUserTransaction(UserTransactionRequest request) {
		AllTransactionResponse resp = new AllTransactionResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("page", 0);
			payload.put("size", 100);
			payload.put("userStatus", "");
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getUserDetailByOfSuperAdminUrl(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH, request.getUsername()));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						final org.json.JSONArray totalTtransaction = (org.json.JSONArray) jobj.getJSONObject("details")
								.getJSONArray("content");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
						resp.setJsonArray(totalTtransaction);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public BlockUserResponse userBlock(BlockUserRequest request) {
		BlockUserResponse resp = new BlockUserResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getAdminSessionId());
			payload.put("username", request.getUsername());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getBlockUserOfSuperAdmin(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public AddMerchantResponse addMerchant(MRegistrationRequest request) {
		AddMerchantResponse response = new AddMerchantResponse();
		try {
			/*
			 * private String successURL; private String failureURL; private
			 * String ipAddress; private String serviceName; private String
			 * image; private boolean fixed; private double maxAmount; private
			 * double minAmount; private double value; private boolean store;
			 * private boolean paymentGateway;
			 */
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("contactNo", request.getContactNo());
			payload.put("firstName", request.getFirstName());
			payload.put("lastName", " ");
			payload.put("email", request.getEmail());
			payload.put("ipAddress", request.getIpAddress());
			payload.put("successURL", request.getSuccessURL());
			payload.put("failureURL", request.getFailureURL());
			payload.put("serviceName", "Merchant Payment");
			payload.put("image", request.getImage());
			payload.put("fixed", request.isFixed());
			payload.put("paymentGateway", request.isPaymentGateway());
			payload.put("store", request.isStore());
			payload.put("minAmount", request.getMinAmount());
			payload.put("maxAmount", request.getMaxAmount());
			payload.put("value", request.getValue());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.addMerchantOfSuperAdmin(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse resp = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = resp.getEntity(String.class);
			LogCat.print("string response is :: " + strResponse);
			if (resp.getStatus() != 200) {
				response.setSuccess(false);
				response.setCode("F00");
				response.setMessage("Service unavailable");
				response.setStatus("FAILED");
				response.setDetails(APIUtils.getFailedJSON().toString());
			} else {

				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							response.setSuccess(true);
							final String details = (String) jobj.get("details");
							response.setDetails(details);
						} else {
							response.setSuccess(false);
						}
						response.setCode(code);
						response.setStatus(status);
						response.setMessage(message);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setSuccess(false);
			response.setCode("F00");
			response.setMessage("Service unavailable");
			response.setStatus("FAILED");
			response.setDetails(APIUtils.getFailedJSON().toString());
		}
		return response;
	}

	@Override
	public AllTransactionResponse getSingleUser(UserTransactionRequest request) {
		AllTransactionResponse resp = new AllTransactionResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("page", 0);
			payload.put("size", 100000);
			payload.put("userStatus", "");
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getSingleOfSuperAdmin(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH, request.getUsername()));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						final org.json.JSONArray totalTtransaction = (org.json.JSONArray) jobj.getJSONObject("details")
								.getJSONArray("content");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
						resp.setJsonArray(totalTtransaction);
					} else {
						resp.setSuccess(false);
						resp.setCode("F00");
						resp.setMessage("Service unavailable");
						resp.setStatus("FAILED");
						resp.setResponse(APIUtils.getFailedJSON().toString());
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public List<NEFTResponse> getNEFTList(SessionDTO dto, boolean flag, Date date1, Date date2) {
		List<NEFTResponse> neftResponses = new ArrayList<>();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", dto.getSessionId());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getNeftListOfSuperAdminURL(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {

			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							final org.json.JSONArray list = jobj.getJSONArray("details");
							if (flag == false) {
								for (int i = 0; i < list.length(); i++) {
									NEFTResponse neft = new NEFTResponse();
									org.json.JSONObject json = list.getJSONObject(i);
									neft.setName(JSONParserUtil.getString(json, "name"));
									neft.setMobileNo(JSONParserUtil.getString(json, "mobileNumber"));
									neft.setEmail(JSONParserUtil.getString(json, "email"));
									neft.setTransactionDate(JSONParserUtil.getString(json, "transactionDate"));
									neft.setTransactionID(JSONParserUtil.getString(json, "transactionID"));
									neft.setBankName(JSONParserUtil.getString(json, "bankName"));
									neft.setIfscCode(JSONParserUtil.getString(json, "ifscCode"));
									neft.setAccountNumber(JSONParserUtil.getString(json, "beneficiaryAccountNumber"));
									neft.setAccountName(JSONParserUtil.getString(json, "beneficiaryAccountName"));
									neft.setUserAccount(JSONParserUtil.getString(json, "virtualAccount"));
									neft.setBankAccount(JSONParserUtil.getString(json, "bankVirtualAccount"));
									neft.setAmount(JSONParserUtil.getString(json, "amount"));
									neft.setStatus(JSONParserUtil.getString(json, "status"));
									neftResponses.add(neft);
								}
							} else {
								for (int i = 0; i < list.length(); i++) {
									org.json.JSONObject json = list.getJSONObject(i);
									String d = JSONParserUtil.getString(json, "transactionDate");
									Date d3 = sdf.parse(d);
									Calendar calender = Calendar.getInstance();
									calender.setTime(d3);
									long m = calender.getTimeInMillis();
									calender.setTimeInMillis(m);
									Date txdate = calender.getTime();
									if (txdate.after(date1) && txdate.before(date2)) {
										NEFTResponse neft = new NEFTResponse();
										neft.setName(JSONParserUtil.getString(json, "name"));
										neft.setMobileNo(JSONParserUtil.getString(json, "mobileNumber"));
										neft.setEmail(JSONParserUtil.getString(json, "email"));
										neft.setTransactionDate(JSONParserUtil.getString(json, "transactionDate"));
										neft.setTransactionID(JSONParserUtil.getString(json, "transactionID"));
										neft.setBankName(JSONParserUtil.getString(json, "bankName"));
										neft.setIfscCode(JSONParserUtil.getString(json, "ifscCode"));
										neft.setAccountNumber(
												JSONParserUtil.getString(json, "beneficiaryAccountNumber"));
										neft.setAccountName(JSONParserUtil.getString(json, "beneficiaryAccountName"));
										neft.setUserAccount(JSONParserUtil.getString(json, "virtualAccount"));
										neft.setBankAccount(JSONParserUtil.getString(json, "bankVirtualAccount"));
										neft.setAmount(JSONParserUtil.getString(json, "amount"));
										neft.setStatus(JSONParserUtil.getString(json, "status"));
										neftResponses.add(neft);
									}
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();

		}
		return neftResponses;

	}

	@Override
	public List<NEFTResponse> getUserNEFTList(SessionDTO dto) {
		List<NEFTResponse> neftResponses = new ArrayList<>();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", dto.getSessionId());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getNeftListOfSuperAdminURL(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {

			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							final org.json.JSONArray list = jobj.getJSONArray("details");
							for (int i = 0; i < list.length(); i++) {
								NEFTResponse neft = new NEFTResponse();
								org.json.JSONObject json = list.getJSONObject(i);
								neft.setName(JSONParserUtil.getString(json, "name"));
								neft.setMobileNo(JSONParserUtil.getString(json, "mobileNumber"));
								neft.setEmail(JSONParserUtil.getString(json, "email"));
								neft.setTransactionDate(JSONParserUtil.getString(json, "transactionDate"));
								neft.setTransactionID(JSONParserUtil.getString(json, "transactionID"));
								neft.setBankName(JSONParserUtil.getString(json, "bankName"));
								neft.setIfscCode(JSONParserUtil.getString(json, "ifscCode"));
								neft.setAccountNumber(JSONParserUtil.getString(json, "beneficiaryAccountNumber"));
								neft.setAccountName(JSONParserUtil.getString(json, "beneficiaryAccountName"));
								neft.setUserAccount(JSONParserUtil.getString(json, "virtualAccount"));
								neft.setBankAccount(JSONParserUtil.getString(json, "bankVirtualAccount"));
								neft.setAmount(JSONParserUtil.getString(json, "amount"));
								neft.setStatus(JSONParserUtil.getString(json, "status"));
								neftResponses.add(neft);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();

		}
		return neftResponses;
	}

	@Override
	public List<NEFTResponse> getUserNEFTListFilter(FilterDTO dto) {
		List<NEFTResponse> neftResponses = new ArrayList<>();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", dto.getSessionId());
			payload.put("startDate", dto.getStartDate());
			payload.put("endDate", dto.getEndDate());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getNeftFilterUrl(Version.VERSION_1, Role.SUPERADMIN,
					Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {

			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							final org.json.JSONArray list = jobj.getJSONArray("details");
							for (int i = 0; i < list.length(); i++) {
								NEFTResponse neft = new NEFTResponse();
								org.json.JSONObject json = list.getJSONObject(i);
								neft.setName(JSONParserUtil.getString(json, "name"));
								neft.setMobileNo(JSONParserUtil.getString(json, "mobileNumber"));
								neft.setEmail(JSONParserUtil.getString(json, "email"));
								neft.setTransactionDate(JSONParserUtil.getString(json, "transactionDate"));
								neft.setTransactionID(JSONParserUtil.getString(json, "transactionID"));
								neft.setBankName(JSONParserUtil.getString(json, "bankName"));
								neft.setIfscCode(JSONParserUtil.getString(json, "ifscCode"));
								neft.setAccountNumber(JSONParserUtil.getString(json, "beneficiaryAccountNumber"));
								neft.setAccountName(JSONParserUtil.getString(json, "beneficiaryAccountName"));
								neft.setUserAccount(JSONParserUtil.getString(json, "virtualAccount"));
								neft.setBankAccount(JSONParserUtil.getString(json, "bankVirtualAccount"));
								neft.setAmount(JSONParserUtil.getString(json, "amount"));
								neft.setStatus(JSONParserUtil.getString(json, "status"));
								neftResponses.add(neft);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();

		}
		return neftResponses;
	}

	@Override
	public List<NEFTResponse> getNEFTList(SessionDTO dto) {
		List<NEFTResponse> neftResponses = new ArrayList<>();
		JSONObject payload = new JSONObject();
		try {
			payload.put("sessionId", dto.getSessionId());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getNeftListMerchantOfSuperAdminURL(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							final org.json.JSONArray list = jobj.getJSONArray("details");
							for (int i = 0; i < list.length(); i++) {
								NEFTResponse neft = new NEFTResponse();
								org.json.JSONObject json = list.getJSONObject(i);
								neft.setName(JSONParserUtil.getString(json, "name"));
								neft.setMobileNo(JSONParserUtil.getString(json, "mobileNumber"));
								neft.setEmail(JSONParserUtil.getString(json, "email"));
								neft.setTransactionDate(JSONParserUtil.getString(json, "transactionDate"));
								neft.setTransactionID(JSONParserUtil.getString(json, "transactionID"));
								neft.setBankName(JSONParserUtil.getString(json, "bankName"));
								neft.setIfscCode(JSONParserUtil.getString(json, "ifscCode"));
								neft.setAccountNumber(JSONParserUtil.getString(json, "beneficiaryAccountNumber"));
								neft.setAccountName(JSONParserUtil.getString(json, "beneficiaryAccountName"));
								neft.setUserAccount(JSONParserUtil.getString(json, "virtualAccount"));
								neft.setBankAccount(JSONParserUtil.getString(json, "bankVirtualAccount"));
								neft.setAmount(JSONParserUtil.getString(json, "amount"));
								neft.setStatus(JSONParserUtil.getString(json, "status"));
								neftResponses.add(neft);
							}
						}
					}
				}
			}
		} catch (org.codehaus.jettison.json.JSONException | JSONException e) {
			e.printStackTrace();
		}
		return neftResponses;

	}

	@Override
	public ReceiptsResponse getSingleMerchantTransactionList(ReceiptsRequest request) {
		ReceiptsResponse resp = new ReceiptsResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("page", request.getPage());
			payload.put("size", request.getSize());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getMerchantsTransactionsOfSuperAdmin(
					Version.VERSION_1, Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH, request.getUsername()));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						// final org.json.JSONArray totalTtransaction =
						// (org.json.JSONArray)
						// jobj.getJSONObject("details").getJSONArray("content");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public AddMerchantResponse addVisaMerchant(VisaMerchantRequest request) {
		AddMerchantResponse response = new AddMerchantResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("firstName", request.getFirstName());
			payload.put("lastName", request.getLastName());
			payload.put("emailAddress", request.getEmailAddress());
			payload.put("address1", request.getAddress1());
			payload.put("address2", request.getAddress2());
			payload.put("city", request.getCity());
			payload.put("state", "Merchant Payment");
			payload.put("country", request.getCountry());
			payload.put("pinCode", request.getPanNo());
			payload.put("merchantBusinessType", request.getMerchantBusinessType());
			// payload.put("lattitude", request.getLattitude());
			payload.put("longitude", request.getLongitude());
			payload.put("isEnabled", request.getIsEnabled());
			payload.put("panNo", request.getPanNo());
			payload.put("mobileNo", request.getMobileNo());
			payload.put("aggregator", request.getAggregator());

			payload.put("merchantAccountName", request.getMerchantAccountName());
			payload.put("merchantBankName", request.getMerchantBankName());
			payload.put("merchantAccountNumber", request.getMerchantAccountNumber());
			// payload.put("merhantIfscCode",
			// request.getMerchantBankIfscCode());
			// payload.put("merchantBankLoction",
			// request.getMerchantBankLocation());

			payload.put("settlementAccountName", request.getSettlementAccountName());
			payload.put("settlementBankName", request.getSettlementBankName());
			payload.put("settlementAccountNumber", request.getSettlementAccountNumber());
			payload.put("settlementIfscCode", request.getSettlementIfscCode());
			// payload.put("settlementBankLoction",
			// request.getSettlementBankLocation());

			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.addVisaMerchantOfSuperAdmin(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse resp = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (resp.getStatus() != 200) {
				response.setSuccess(false);
				response.setCode("F00");
				response.setMessage("Service unavailable");
				response.setStatus("FAILED");
				response.setDetails(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = resp.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							response.setSuccess(true);
							final String details = (String) jobj.get("details");
							response.setDetails(details);
						} else {
							response.setSuccess(false);
						}
						response.setCode(code);
						response.setStatus(status);
						response.setMessage(message);
					} else {
						response.setSuccess(false);
						response.setCode("F00");
						response.setMessage("Service unavailable");
						response.setStatus("FAILED");
						response.setDetails(APIUtils.getFailedJSON().toString());
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setSuccess(false);
			response.setCode("F00");
			response.setMessage("Service unavailable");
			response.setStatus("FAILED");
			response.setDetails(APIUtils.getFailedJSON().toString());
		}
		return response;
	}

	@Override
	public AddMerchantResponse checkVisaMerchant(VisaMerchantRequest request) {
		AddMerchantResponse response = new AddMerchantResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("emailAddress", request.getEmailAddress());

			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.checkVisaMerchantOfSuperAdmin(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse resp = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (resp.getStatus() != 200) {
				response.setSuccess(false);
				response.setCode("F00");
				response.setMessage("Service unavailable");
				response.setStatus("FAILED");
				response.setDetails(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = resp.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							response.setSuccess(true);
							final String details = (String) jobj.get("details");
							response.setDetails(details);
						} else {
							response.setSuccess(false);
						}
						response.setCode(code);
						response.setStatus(status);
						response.setMessage(message);
					} else {
						response.setSuccess(false);
						response.setCode("F00");
						response.setMessage("Service unavailable");
						response.setStatus("FAILED");
						response.setDetails(APIUtils.getFailedJSON().toString());
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setSuccess(false);
			response.setCode("F00");
			response.setMessage("Service unavailable");
			response.setStatus("FAILED");
			response.setDetails(APIUtils.getFailedJSON().toString());
		}
		return response;
	}

	@Override
	public VisaSignUpResponse merchantSignOffAtM2P(VisaMerchantRequest request) {
		VisaSignUpResponse resp = new VisaSignUpResponse();
		try {
			Client client = Client.create();
			WebResource webResource = client.resource(VisaContants.SIGN_UP_URL);
			JSONObject payload = new JSONObject();
			JSONObject merchant = new JSONObject();
			JSONObject merchantBankDetail = new JSONObject();
			JSONObject settlementBankDetail = new JSONObject();

			merchant.put("firstName", request.getFirstName());
			merchant.put("lastName", request.getLastName());
			merchant.put("emailAddress", request.getEmailAddress());
			merchant.put("address1", request.getAddress1());
			merchant.put("address2", request.getAddress2());
			merchant.put("city", request.getCity());
			merchant.put("state", request.getState());
			merchant.put("pinCode", request.getPinCode());
			merchant.put("merchantCategory", request.getMerchantBusinessType());
			// merchant.put("latitude", request.getLattitude());
			merchant.put("longitude", request.getLongitude());
			merchant.put("isEnabled", request.getIsEnabled());
			merchant.put("panNo", request.getPanNo());
			merchant.put("mobileNo", request.getMobileNo());
			merchant.put("aggregator", request.getAggregator());

			merchantBankDetail.put("accName", request.getMerchantAccountName());
			merchantBankDetail.put("bankName", request.getMerchantBankName());
			merchantBankDetail.put("bankAccNo", request.getMerchantAccountNumber());
			// merchantBankDetail.put("bankIfscCode",
			// request.getMerchantBankIfscCode());
			// merchantBankDetail.put("bankLocation",
			// request.getMerchantBankLocation());

			settlementBankDetail.put("accName", request.getSettlementBankName());
			settlementBankDetail.put("bankName", request.getSettlementBankName());
			settlementBankDetail.put("bankAccNo", request.getSettlementAccountName());
			settlementBankDetail.put("bankIfscCode", request.getSettlementIfscCode());
			// settlementBankDetail.put("bankLocation",
			// request.getSettlementBankLocation());

			payload.put("merchant", merchant);
			payload.put("merchantBankDetail", merchantBankDetail);
			payload.put("settlementBankDetail", settlementBankDetail);
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("Content-Type", "application/json").header("Authorization", "Basic YWRtaW46YWRtaW4=")
					.post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setFlag(false);
				resp.setCode("F04");
				resp.setMessage("Server Eroor M2P");
			} else {
				String strResponse = response.getEntity(String.class);

				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						org.json.JSONObject obj = jobj.getJSONObject("result");
						String entityId = (String) obj.get("customerId");
						String mVisaId = (String) obj.get("mvisaId");
						resp.setCode("S00");
						resp.setEntityId(entityId);
						resp.setmVisaId(mVisaId);
						resp.setFlag(true);
					}

				} else {
					resp.setMessage("Error from M2P . Service Down");
					resp.setCode("F00");
					resp.setFlag(false);
				}
			}

		} catch (Exception e) {
		}
		return resp;
	}

	@Override
	public List<ServicesResponse> getServiceStatus(ServicesRequest request) {

		List<ServicesResponse> resp = new ArrayList<>();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			logger.info("Payload " + payload);

			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getFPSwitchStatus(Version.VERSION_1, Role.SUPERADMIN,
					Device.WEBSITE, Language.ENGLISH));

			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");

						if (code.equalsIgnoreCase("S00")) {
							final org.json.JSONArray list = jobj.getJSONArray("details");
							for (int i = 0; i < list.length(); i++) {
								ServicesResponse service = new ServicesResponse();
								org.json.JSONObject json = list.getJSONObject(i);
								service.setName(JSONParserUtil.getString(json, "name"));
								service.setServiceStatus(JSONParserUtil.getString(json, "status"));
								resp.add(service);
							}
						}
					}
				}
			}
		} catch (org.codehaus.jettison.json.JSONException | JSONException e) {
			e.printStackTrace();
		}
		return resp;
	}

	@Override
	public ServicesResponse checkServiceStatus(ServicesRequest request) {
		ServicesResponse response = new ServicesResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("name", request.getName());
			payload.put("status", request.getStatus());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getFPStatusActive(Version.VERSION_1, Role.SUPERADMIN,
					Device.WEBSITE, Language.ENGLISH));
			ClientResponse resp = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (resp.getStatus() != 200) {
				response.setSuccess(false);
				response.setCode("F00");
				response.setMessage("Service unavailable");
				response.setStatus("FAILED");
				response.setDetails(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = resp.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							response.setSuccess(true);
							org.json.JSONObject details = (org.json.JSONObject) jobj.get("details");
						} else {
							response.setSuccess(false);
						}
						response.setCode(code);
						response.setStatus(status);
						response.setMessage(message);
					} else {
						response.setSuccess(false);
						response.setCode("F00");
						response.setMessage("Service unavailable");
						response.setStatus("FAILED");
						response.setDetails(APIUtils.getFailedJSON().toString());
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setSuccess(false);
			response.setCode("F00");
			response.setMessage("Service unavailable");
			response.setStatus("FAILED");
			response.setDetails(APIUtils.getFailedJSON().toString());
		}
		return response;
	}

	@Override
	public AccountTypeResponse getListAccountType(SessionDTO dto) {
		AccountTypeResponse resp = new AccountTypeResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", dto.getSessionId());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getListAccountTypeSuperAdminURL(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						final org.json.JSONArray arr = jobj.getJSONArray("details");
						if (code.equalsIgnoreCase("S00")) {

							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setJsonArray(arr);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public AccountTypeResponse updateAccountType(AccountTypeRequest dto) {
		AccountTypeResponse resp = new AccountTypeResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", dto.getSessionId());
			payload.put("accountType", dto.getCode());
			payload.put("monthlyLimit", dto.getMonthlyLimit());
			payload.put("balanceLimit", dto.getBalanceLimit());
			payload.put("dailyLimit", dto.getDailyLimit());
			payload.put("transactionLimit", dto.getTransactionLimit());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.updateAccountTypeSuperAdminURL(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							final org.json.JSONArray arr = jobj.getJSONArray("details");
							resp.setJsonArray(arr);
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public VersionListResponse getVersionList(SessionDTO request) {
		VersionListResponse resp = new VersionListResponse();
		try {
			Client client = Client.create();
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			WebResource webResource = client.resource(
					UrlMetadatas.getAllVersions(Version.VERSION_1, Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456")).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (code.equalsIgnoreCase("S00")) {
							List<VersionDTO> services = new ArrayList<>();
							resp.setSuccess(true);
							org.json.JSONArray details = JSONParserUtil.getArray(jobj, "details");
							if (details != null && details.length() > 0) {
								for (int i = 0; i < details.length(); i++) {
									org.json.JSONObject json = details.getJSONObject(i);
									VersionDTO dto = new VersionDTO();
									dto.setStatus(JSONParserUtil.getString(json, "status"));
									dto.setVersion(JSONParserUtil.getString(json, "version"));
									DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");
									long milliSeconds = Long.parseLong(JSONParserUtil.getString(json, "created"));
									Calendar calendar = Calendar.getInstance();
									calendar.setTimeInMillis(milliSeconds);
									dto.setCreated(formatter.format(calendar.getTime()));
									services.add(dto);
								}
								resp.setVersionList(services);
							} else {
								resp.setSuccess(false);
								resp.setMessage("Versions Empty");
							}
						} else {
							resp.setSuccess(false);
							resp.setMessage("Please try Later");
						}
					} else {
						resp.setSuccess(false);
					}
				} else {
					resp.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
		}
		return resp;
	}

	@Override
	public VersionDTO updateVersionList(VersionRequest request) {
		VersionDTO resp = new VersionDTO();
		try {
			Client client = Client.create();
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("version", request.getVersion());
			payload.put("status", request.getStatus());
			WebResource webResource = client.resource(
					UrlMetadatas.updateVersions(Version.VERSION_1, Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456")).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						final String message = jobj.getString("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
							org.json.JSONObject details = jobj.getJSONObject("details");
							if (details != null) {
								org.json.JSONObject service = details.getJSONObject("service");
								if (service != null) {
									resp.setSuccess(true);
									resp.setVersion(service.getString("version"));
									resp.setStatus(service.getString("status"));
								}
							}
						} else {
							resp.setSuccess(false);
							resp.setMessage(message);
						}
					} else {
						resp.setSuccess(false);
						resp.setMessage("Please try Later");
					}
				} else {
					resp.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
		}
		return resp;
	}

	@Override
	public BulkFileListResponse getBulkFile(SessionDTO request) {
		BulkFileListResponse resp = new BulkFileListResponse();
		try {
			Client client = Client.create();
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			WebResource webResource = client.resource(
					UrlMetadatas.getBulkFileList(Version.VERSION_1, Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456")).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (code.equalsIgnoreCase("S00")) {
							List<BulkFileDTO> list = new ArrayList<>();
							resp.setSuccess(true);
							org.json.JSONArray details = JSONParserUtil.getArray(jobj, "details");
							if (details != null) {
								for (int i = 0; i < details.length(); i++) {
									org.json.JSONObject json = details.getJSONObject(i);
									BulkFileDTO result = new BulkFileDTO();
									result.setMobileNumber(JSONParserUtil.getString(json, "mobileNumber"));
									result.setAmount(JSONParserUtil.getString(json, "amount"));
									result.setStatus(JSONParserUtil.getString(json, "status"));
									list.add(result);
								}
							}
							resp.setList(list);
						} else {
							resp.setSuccess(false);
						}
					}
				} else {
					resp.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
		}
		return resp;
	}

	@Override
	public BulkFileListResponse uploadBulkFile(BulkFileUpload request) {
		BulkFileListResponse resp = new BulkFileListResponse();
		try {
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.uploadBulkFileList(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456"))
					.post(ClientResponse.class, request.getJsonRequest());
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (code.equalsIgnoreCase("S00")) {
							List<BulkFileDTO> list = new ArrayList<>();
							resp.setSuccess(true);
							resp.setCode(code);
							org.json.JSONArray details = JSONParserUtil.getArray(jobj, "details");
							if (details != null) {
								for (int i = 0; i < details.length(); i++) {
									org.json.JSONObject json = details.getJSONObject(i);
									BulkFileDTO result = new BulkFileDTO();
									result.setStatus(JSONParserUtil.getString(json, "status"));
									list.add(result);
								}
							}
							resp.setList(list);
						} else {
							resp.setSuccess(false);
						}
					}
				} else {
					resp.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
		}
		return resp;
	}

	@Override
	public TreatCardPlansResponse getListPlans(SessionDTO dto) {
		TreatCardPlansResponse resp = new TreatCardPlansResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", dto.getSessionId());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getTreatCardPlansUrl(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						org.json.JSONArray arr = null;
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
							arr = jobj.getJSONArray("plans");
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setJsonArray(arr);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public TreatCardPlansResponse updatePlans(TreatCardPlanRequest dto) {
		TreatCardPlansResponse resp = new TreatCardPlansResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", dto.getSessionId());
			payload.put("days", dto.getDays());
			payload.put("amount", dto.getAmount());
			payload.put("status", dto.getStatus());
			payload.put("id", dto.getPlanId());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.updateTreatCardPlansUrl(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							final org.json.JSONArray arr = jobj.getJSONArray("details");
							resp.setJsonArray(arr);
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public TreatCardRegisterListResponse getRegisterList(SessionDTO dto) {
		TreatCardRegisterListResponse resp = new TreatCardRegisterListResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", dto.getSessionId());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getRegisterListUrl(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						org.json.JSONArray arr = null;
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
							arr = jobj.getJSONArray("details");
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setJsonArray(arr);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public TreatCardRegisterListResponse getRegisterListFiltered(TreatCardRegisterList dto) {
		TreatCardRegisterListResponse resp = new TreatCardRegisterListResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", dto.getSessionId());
			payload.put("fromDate", dto.getFromDate());
			payload.put("toDate", dto.getToDate());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getFilteredRegisterListUrl(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						org.json.JSONArray arr = null;
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
							arr = jobj.getJSONArray("details");
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setJsonArray(arr);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public TListResponse getTreatCardTransactionList(AllTransactionRequest request) {
		TListResponse resp = new TListResponse();
		try {
			Client client = Client.create();
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			WebResource webResource = client.resource(UrlMetadatas.getTreatCardTransactionUrl(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456")).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (code.equalsIgnoreCase("S00")) {
							List<TListDTO> transactions = new ArrayList<>();
							resp.setSuccess(true);
							org.json.JSONArray details = JSONParserUtil.getArray(jobj, "details");
							if (details != null && details.length() > 0) {
								for (int i = 0; i < details.length(); i++) {
									org.json.JSONObject json = details.getJSONObject(i);
									TListDTO dto = new TListDTO();
									dto.setUsername(JSONParserUtil.getString(json, "username"));
									dto.setEmail(JSONParserUtil.getString(json, "email"));
									dto.setContactNo(JSONParserUtil.getString(json, "contactNo"));
									dto.setTransactionRefNo(JSONParserUtil.getString(json, "transactionRefNo"));
									dto.setCurrentBalance(JSONParserUtil.getString(json, "currentBalance"));
									dto.setDateOfTransaction(JSONParserUtil.getString(json, "dateOfTransaction"));
									dto.setDebit(JSONParserUtil.getString(json, "debit"));
									dto.setAmount(JSONParserUtil.getString(json, "amount"));
									dto.setStatus(JSONParserUtil.getString(json, "status"));
									dto.setDescription(JSONParserUtil.getString(json, "description"));
									transactions.add(dto);
								}
								resp.setList(transactions);
							} else {
								resp.setSuccess(false);
								resp.setMessage("Transaction List Empty");
							}
						} else {
							resp.setSuccess(false);
							resp.setMessage("Please try Later");
						}
					} else {
						resp.setSuccess(false);
					}
				} else {
					resp.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
		}
		return resp;
	}

	@Override
	public TListResponse getTreatCardTransactionListWithFilter(AllTransactionRequest request) {
		TListResponse resp = new TListResponse();
		try {
			Client client = Client.create();
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("startDate", request.getStartDate());
			payload.put("endDate", request.getEndDate());
			WebResource webResource = client.resource(UrlMetadatas.getFilteredTreatCardTransactionUrl(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456")).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (code.equalsIgnoreCase("S00")) {
							List<TListDTO> transactions = new ArrayList<>();
							resp.setSuccess(true);
							org.json.JSONArray details = JSONParserUtil.getArray(jobj, "details");
							if (details != null && details.length() > 0) {
								for (int i = 0; i < details.length(); i++) {
									org.json.JSONObject json = details.getJSONObject(i);
									TListDTO dto = new TListDTO();
									dto.setUsername(JSONParserUtil.getString(json, "username"));
									dto.setEmail(JSONParserUtil.getString(json, "email"));
									dto.setContactNo(JSONParserUtil.getString(json, "contactNo"));
									dto.setTransactionRefNo(JSONParserUtil.getString(json, "transactionRefNo"));
									dto.setCurrentBalance(JSONParserUtil.getString(json, "currentBalance"));
									dto.setDateOfTransaction(JSONParserUtil.getString(json, "dateOfTransaction"));
									dto.setDebit(JSONParserUtil.getString(json, "debit"));
									dto.setAmount(JSONParserUtil.getString(json, "amount"));
									dto.setStatus(JSONParserUtil.getString(json, "status"));
									dto.setDescription(JSONParserUtil.getString(json, "description"));
									transactions.add(dto);
								}
								resp.setList(transactions);
							} else {
								resp.setSuccess(false);
								resp.setMessage("Transaction List Empty");
							}
						} else {
							resp.setSuccess(false);
							resp.setMessage("Please try Later");
						}
					} else {
						resp.setSuccess(false);
					}
				} else {
					resp.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
		}
		return resp;
	}

	@Override
	public TreatCardRegisterListResponse getTreatCardCount(SessionDTO dto) {
		TreatCardRegisterListResponse resp = new TreatCardRegisterListResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", dto.getSessionId());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getTreatCardCountUrl(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						org.json.JSONArray arr = null;
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
							arr = jobj.getJSONArray("details");
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setJsonArray(arr);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public AllTransactionResponse getPromoTransactionFiltered(AllTransactionRequest request) {
		AllTransactionResponse resp = new AllTransactionResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("fromDate", request.getStartDate());
			payload.put("toDate", request.getEndDate());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getPromoTransactionsFilteredSuperAdminURL(
					Version.VERSION_1, Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (code.equalsIgnoreCase("S00")) {
							List<AdminUserDetails> userList = new ArrayList<AdminUserDetails>();
							resp.setSuccess(true);
							org.json.JSONArray details = JSONParserUtil.getArray(jobj, "details");
							if (details != null && details.length() > 0) {
								for (int i = 0; i < details.length(); i++) {
									org.json.JSONObject t = details.getJSONObject(i);
									AdminUserDetails userDetails = new AdminUserDetails();
									long milliseconds = JSONParserUtil.getLong(t, "date");
									Calendar calendar = Calendar.getInstance();
									calendar.setTimeInMillis(milliseconds);
									userDetails.setDateOfTransaction(simformat.format(calendar.getTime()));
									userDetails.setTransactionRefNo(JSONParserUtil.getString(t, "transactionRefNo"));
									userDetails.setCurrentBalance(
											String.valueOf(JSONParserUtil.getDouble(t, "currentBalance")));
									userDetails.setContactNo(JSONParserUtil.getString(t, "contactNo"));
									userDetails.setAmount(String.valueOf(JSONParserUtil.getDouble(t, "amount")));
									userDetails.setFirstName(JSONParserUtil.getString(t, "description"));
									userDetails.setEmail(JSONParserUtil.getString(t, "email"));
									userDetails.setStatus(JSONParserUtil.getString(t, "status"));
									userList.add(userDetails);
								}
								resp.setList(userList);
							} else {
								resp.setSuccess(false);
								resp.setMessage("Transaction List Empty");
							}
						} else {
							resp.setSuccess(false);
							resp.setMessage("Please try Later");
						}
					} else {
						resp.setSuccess(false);
					}
				} else {
					resp.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
		}
		return resp;
	}

	@Override
	public AllUserResponse getAllFlightsDetail(AllUserRequest request) {
		AllUserResponse resp = new AllUserResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			/*
			 * payload.put("page", request.getPage()); payload.put("size",
			 * request.getSize()); payload.put("userStatus",
			 * request.getStatus().getValue());
			 */
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getAllFlightsDetailURL(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {

					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							final org.json.JSONArray details = jobj.getJSONArray("details");
							resp.setJsonArray(details);
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);

						resp.setMessage(message);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public FlightResponseDTO getSingleFlightTicketDetails(String sessionId, long flightId) {

		FlightResponseDTO resp = new FlightResponseDTO();
		List<TravellerFlightDetails> travellerFlightDetails = new ArrayList<>();
		FlightTicketResp flightTicketResp = new FlightTicketResp();

		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", sessionId);
			payload.put("flightId", flightId);
			Client vpqClient = Client.create();
			WebResource vpqWebResource = vpqClient.resource(UrlMetadatas.getFlightDetailsForAdmin(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse vpqresponse = vpqWebResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = vpqresponse.getEntity(String.class);
			org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
			final String code = jobj.getString("code");
			final String status = jobj.getString("code");
			final String message = jobj.getString("code");
			if (code.equalsIgnoreCase("S00")) {
				final org.json.JSONArray details = jobj.getJSONArray("details");
				for (int i = 0; i < details.length(); i++) {
					org.json.JSONObject detail = details.getJSONObject(i);
					TravellerFlightDetails tDetails = new TravellerFlightDetails();

					tDetails.setfName(detail.getString("fName"));
					tDetails.setlName(detail.getString("lName"));
					tDetails.setGender(detail.getString("gender"));
					tDetails.setType(detail.getString("type"));
					tDetails.setTicketNo(detail.getString("ticketNo"));

					travellerFlightDetails.add(tDetails);
				}

				if (details.length() > 0) {

					org.json.JSONObject detail = details.getJSONObject(0);

					if (detail != null) {
						String flString = detail.getString("flightTicket");
						if (flString != null) {
							org.json.JSONObject flDetails = detail.getJSONObject("flightTicket");

							String ticketsStr = flDetails.getString("ticketDetails");
							if (!ticketsStr.equalsIgnoreCase("null") && !ticketsStr.equalsIgnoreCase("nullnull")) {
								TicketsResp ticketDeatilsDTO = new TicketsResp();

								ObjectMapper mapper = new ObjectMapper();
								ticketDeatilsDTO = mapper.readValue(ticketsStr, TicketsResp.class);

								flightTicketResp.setTicketsResp(ticketDeatilsDTO);
							}
							flightTicketResp.setBookingRefNo(flDetails.getString("bookingRefId"));
							flightTicketResp.setEmail(flDetails.getString("email"));

							Date date = new Date(flDetails.getLong("created"));

							String dateStr = simformat.format(date);

							flightTicketResp.setCreated(dateStr);
						}

						flightTicketResp.setTravellerDetails(travellerFlightDetails);
					}

				}

			}

			resp.setCode(code);
			resp.setStatus(status);
			resp.setMessage(message);
			resp.setDetails(flightTicketResp);
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			resp.setMessage("Service Unavailable");
			resp.setDetails(null);
			resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
		}
		return resp;

	}

	@Override
	public com.payqwikweb.app.model.response.bus.ResponseDTO getSingleTicketTravellerDetails(String sessionId,
			String emtTxnNo) {

		com.payqwikweb.app.model.response.bus.ResponseDTO resp = new com.payqwikweb.app.model.response.bus.ResponseDTO();

		List<TravellerDetailsDTO> list = new ArrayList<>();
		List<String> seatNo = new ArrayList<>();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", sessionId);
			payload.put("bookingTxnId", emtTxnNo);
			Client vpqClient = Client.create();
			WebResource vpqWebResource = vpqClient.resource(UrlMetadatas.getTravellerDetailsForAdmin(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse vpqresponse = vpqWebResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = vpqresponse.getEntity(String.class);
			org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
			final String code = jobj.getString("code");
			if (code.equalsIgnoreCase("S00")) {
				org.json.JSONArray details = jobj.getJSONArray("details");
				for (int i = 0; i < details.length(); i++) {
					TravellerDetailsDTO dto = new TravellerDetailsDTO();
					dto.setfName((String) details.getJSONObject(i).get("fName"));
					dto.setlName((String) details.getJSONObject(i).get("lName"));
					dto.setGender((String) details.getJSONObject(i).get("gender"));
					dto.setSeatNo((String) details.getJSONObject(i).get("seatNo"));
					dto.setFare((String) details.getJSONObject(i).get("fare"));
					dto.setAge((details.getJSONObject(i).getInt("age")) + "");
					list.add(dto);
					seatNo.add((String) details.getJSONObject(i).get("seatNo"));
				}
				if (details.length() >= 0) {
					BusTicketDTO busTicketDTO = new BusTicketDTO();
					String source = (String) details.getJSONObject(0).getJSONObject("busTicketId").get("source");
					String status = (String) details.getJSONObject(0).getJSONObject("busTicketId").get("status");
					String destination = (String) details.getJSONObject(0).getJSONObject("busTicketId")
							.get("destination");
					String journeyDate = (String) details.getJSONObject(0).getJSONObject("busTicketId")
							.get("journeyDate");
					String operatorPnr = details.getJSONObject(0).getJSONObject("busTicketId").getString("operatorPnr");
					String busOperator = (String) details.getJSONObject(0).getJSONObject("busTicketId")
							.getString("busOperator");
					String ticketPnr = details.getJSONObject(0).getJSONObject("busTicketId").getString("ticketPnr");
					;
					double totalFare = (double) details.getJSONObject(0).getJSONObject("busTicketId")
							.getDouble("totalFare");
					String emtTxnId = (String) details.getJSONObject(0).getJSONObject("busTicketId").get("emtTxnId");
					String boardingAddress = (String) details.getJSONObject(0).getJSONObject("busTicketId")
							.get("boardingAddress");
					String arrTime = (String) details.getJSONObject(0).getJSONObject("busTicketId").get("arrTime");
					String emtTransactionScreenId = (String) details.getJSONObject(0).getJSONObject("busTicketId")
							.get("emtTransactionScreenId");
					String vPqTxnRefNo = "NA";
					if (details.getJSONObject(0).getJSONObject("busTicketId").getString("transaction") != null
							&& !details.getJSONObject(0).getJSONObject("busTicketId").getString("transaction").isEmpty()
							&& !details.getJSONObject(0).getJSONObject("busTicketId").getString("transaction")
									.equalsIgnoreCase("Null")) {
						vPqTxnRefNo = details.getJSONObject(0).getJSONObject("busTicketId").getJSONObject("transaction")
								.getString("transactionRefNo");
					}

					String deptTime = (String) details.getJSONObject(0).getJSONObject("busTicketId").get("deptTime");

					busTicketDTO.setDeptTime(deptTime);
					busTicketDTO.setSource(source);
					busTicketDTO.setDestination(destination);
					busTicketDTO.setJourneyDate(journeyDate);
					busTicketDTO.setOperatorPnr(operatorPnr);
					busTicketDTO.setBusOperator(busOperator);
					busTicketDTO.setTicketPnr(ticketPnr);
					busTicketDTO.setTotalFare(totalFare);
					busTicketDTO.setEmtTxnId(emtTxnId);
					busTicketDTO.setBoardingAddress(boardingAddress);
					busTicketDTO.setArrTime(arrTime);
					busTicketDTO.setEmtTransactionScreenId(emtTransactionScreenId);
					busTicketDTO.setTransactionRefNo(vPqTxnRefNo);
					busTicketDTO.setStatus(status);
					resp.setExtraInfo(busTicketDTO);

				}
			}

			resp.setSeatNo(seatNo);
			resp.setCode(code);
			resp.setMessage("Get All Book Tickets");
			resp.setDetails(list);
			resp.setStatus(ResponseStatus.SUCCESS.getKey());

		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			resp.setMessage("Service Unavailable");
			resp.setDetails(null);
			resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
		}
		return resp;

	}

	@Override
	public com.payqwikweb.app.model.response.bus.ResponseDTO isCancellable(IsCancellableReq dto) {

		com.payqwikweb.app.model.response.bus.ResponseDTO resp = new com.payqwikweb.app.model.response.bus.ResponseDTO();
		IsCancellableResp result = new IsCancellableResp();
		try {

			JSONObject payload = new JSONObject();
			payload.put("clientIp", "49.204.86.246");
			payload.put("cllientKey", UrlMetadatas.MDEX_CLIENTKEY);
			payload.put("clientToken", UrlMetadatas.MDEX_CLIENTTOKEN);
			payload.put("clientApiName", "VPayQwik");
			payload.put("bookId", dto.getBookId());
			payload.put("canceltype", "1");
			payload.put("seatNo", dto.getSeatNo());
			payload.put("ipAddress", dto.getIpAddress());

			Client client = Client.create();
			WebResource webResource = client
					.resource(UrlMetadatas.isCancelable(Role.ClIENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setCode("F00");
				resp.setMessage("Service Unavailable");
				resp.setStatus(ResponseStatus.FAILURE.getValue());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {

					JSONObject jObj = new JSONObject(strResponse);

					final boolean isCancellable = jObj.getBoolean("cancellable");
					final double refundAmount = jObj.getDouble("refundAmount");
					final double cancellationCharges = jObj.getDouble("cancellationCharges");
					final String seatNo = jObj.getString("seatNo");

					if (isCancellable) {
						resp.setStatus(ResponseStatus.SUCCESS.getKey());
						resp.setCode(ResponseStatus.SUCCESS.getValue());
					} else {
						resp.setStatus(ResponseStatus.FAILURE.getKey());
						resp.setCode(ResponseStatus.FAILURE.getValue());
					}
					result.setMessage("Get Cancellation Details");
					result.setCancellable(isCancellable);
					result.setCancellationCharges(cancellationCharges);
					result.setRefundAmount(refundAmount);
					result.setSeatNo(seatNo);
					resp.setDetails(result);
				} else {
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus(ResponseStatus.FAILURE.getValue());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus(ResponseStatus.FAILURE.getValue());
		}

		return resp;

	}

	@Override
	public com.payqwikweb.app.model.response.bus.ResponseDTO cancelBookedTicket(IsCancellableReq dto) {

		com.payqwikweb.app.model.response.bus.ResponseDTO resp = new com.payqwikweb.app.model.response.bus.ResponseDTO();

		CancelTicketResp result = new CancelTicketResp();
		try {

			JSONObject payload = new JSONObject();
			payload.put("clientIp", "49.204.86.246");
			payload.put("cllientKey", UrlMetadatas.MDEX_CLIENTKEY);
			payload.put("clientToken", UrlMetadatas.MDEX_CLIENTTOKEN);
			payload.put("clientApiName", "VPayQwik");
			payload.put("bookId", dto.getBookId());
			payload.put("seatNo", dto.getSeatNo());
			payload.put("ipAddress", dto.getIpAddress());
			payload.put("canceltype", "1");

			Client client = Client.create();
			WebResource webResource = client
					.resource(UrlMetadatas.cancelTicket(Role.ClIENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);

			if (response.getStatus() != 200) {
				resp.setCode("F00");
				resp.setMessage("Service Unavailable");
				resp.setStatus(ResponseStatus.FAILURE.getValue());

			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					JSONObject jObj = new JSONObject(strResponse);
					if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(jObj.getString("code"))) {
						final boolean isCancelRequested = jObj.getBoolean("cancelRequested");
						final double refundAmount = jObj.getDouble("refundAmount");
						final double cancellationCharges = jObj.getDouble("cancellationCharges");
						final boolean cancelStatus = jObj.getBoolean("cancelStatus");
						final String pnrNo = jObj.getString("pnrNo");
						final String operatorPnr = jObj.getString("operatorPnr");
						final String remarks = jObj.getString("remarks");

						if (isCancelRequested) {

							resp.setStatus(ResponseStatus.SUCCESS.getKey());
							resp.setCode(ResponseStatus.SUCCESS.getValue());

							String txnRefNo = dto.getvPqTxnId();
							String a[] = txnRefNo.split("D");

							JSONObject vpayload = new JSONObject();

							vpayload.put("sessionId", dto.getSessionId());
							vpayload.put("refundedAmount", refundAmount);
							vpayload.put("pQTxnId", a[0] + "");

							Client vpqClient = Client.create();
							WebResource vpqWebResource = vpqClient.resource(UrlMetadatas.getCancelBusBookedTicketURL(
									Version.VERSION_1, Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
							ClientResponse vpqresponse = vpqWebResource.accept("application/json")
									.type("application/json").header("hash", SecurityUtils.getHash(vpayload.toString()))
									.post(ClientResponse.class, vpayload);

							String vpqstrResponse = vpqresponse.getEntity(String.class);

							if (vpqstrResponse != null) {
								JSONObject vJObj = new JSONObject(vpqstrResponse);
								String status = vJObj.getString("status");
								String code = vJObj.getString("code");
								String vMessage = vJObj.getString("message");

								if (code.equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
									resp.setStatus(status);
									resp.setCode(code);
									resp.setMessage(vMessage);

									resp.setDetails(result);
								} else {
									resp.setStatus(status);
									resp.setCode(code);
									resp.setMessage(vMessage);
								}
							}

						} else {
							resp.setStatus(ResponseStatus.FAILURE.getKey());
							resp.setCode(ResponseStatus.FAILURE.getValue());
						}
						resp.setMessage("Get Cancellation Details");
						result.setCancellationCharges(cancellationCharges);
						result.setRefundAmount(refundAmount);
						result.setCancelRequested(isCancelRequested);
						result.setCancelStatus(cancelStatus);
						result.setPnrNo(pnrNo);

						/*
						 * final String
						 * cancelSeatNo=jObj.getString("cancelSeatNo"); if
						 * (cancelSeatNo!=null) {
						 * org.codehaus.jettison.json.JSONArray
						 * cancelSeats=jObj.getJSONArray("cancelSeatNo"); for
						 * (int i = 0; i < cancelSeats.length(); i++) {
						 * cList.add((String)cancelSeats.get(i)); } }
						 */

						// result.setCancelSeatNo(cancelSeatNo);

						result.setOperatorPnr(operatorPnr);
						result.setRemarks(remarks);
						resp.setDetails(result);
					} else {
						resp.setCode("F00");
						resp.setMessage("Ticket Not Canceled");
						resp.setStatus(ResponseStatus.FAILURE.getValue());
					}
				} else {
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus(ResponseStatus.FAILURE.getValue());
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus(ResponseStatus.FAILURE.getValue());
		}

		return resp;
	}

	@Override
	public AllUserResponse getBusDetails(AllUserRequest request) {
		AllUserResponse resp = new AllUserResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			/*
			 * payload.put("page", request.getPage()); payload.put("size",
			 * request.getSize()); payload.put("userStatus",
			 * request.getStatus().getValue());
			 */
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getBusDetailForAdminURL(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);

						resp.setMessage(message);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public AllUserResponse getAllFlightsDetailByDate(PagingDTO request) {
		AllUserResponse resp = new AllUserResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("fromDate", request.getStartDate());
			payload.put("toDate", request.getEndDate());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getAllFlightsDetailURLByDate(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							final org.json.JSONArray details = jobj.getJSONArray("details");
							resp.setJsonArray(details);
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);

						resp.setMessage(message);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public FlightCancelableResp isCancellableFlight(FligthcancelableReq dto) {

		FlightCancelableResp resp = new FlightCancelableResp();

		try {

			JSONObject payload = new JSONObject();
			payload.put("clientIp", "49.204.86.246");
			payload.put("cllientKey", UrlMetadatas.MDEX_CLIENTKEY);
			payload.put("clientToken", UrlMetadatas.MDEX_CLIENTTOKEN);
			payload.put("clientApiName", "VPayQwik");
			payload.put("subUserId", dto.getSubUserId());
			payload.put("transactionScreenId", dto.getTransactionScreenId());

			Client client = Client.create();
			WebResource webResource = client
					.resource(UrlMetadatas.isFlightCancelable(Role.ClIENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setCode("F00");
				resp.setMessage("Service Unavailable");
				resp.setStatus(ResponseStatus.FAILURE.getValue());

			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {

					JSONObject jObj = new JSONObject(strResponse);

					/*
					 * final boolean
					 * isCancellable=jObj.getBoolean("cancellable"); final
					 * double refundAmount=jObj.getDouble("refundAmount"); final
					 * double
					 * cancellationCharges=jObj.getDouble("cancellationCharges")
					 * ; final String seatNo=jObj.getString("seatNo");
					 */

					String code = jObj.getString("code");

					if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
						resp.setStatus(ResponseStatus.SUCCESS.getKey());
						resp.setCode(ResponseStatus.SUCCESS.getValue());
						resp.setMessage("Flight cancelabel response");
					} else {
						resp.setStatus(ResponseStatus.FAILURE.getKey());
						resp.setCode(ResponseStatus.FAILURE.getValue());
					}
					resp.setMessage("Get Cancellation Details");

					resp.setDetails(strResponse);
				} else {
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus(ResponseStatus.FAILURE.getValue());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return resp;
	}

	@Override
	public FlightResponseDTO getFlightDetails(GetFlightDetailsForAdmin getFlightDetails) {

		return null;
	}

	@Override
	public AllUserResponse getBusDetailsByDate(PagingDTO dto) {
		AllUserResponse resp = new AllUserResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", dto.getSessionId());
			payload.put("fromDate", dto.getStartDate());
			payload.put("toDate", dto.getEndDate());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getBusDetailForAdminURLByDate(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);

						resp.setMessage(message);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public com.payqwikweb.app.model.response.bus.ResponseDTO getSingleTicketPdf(String sessionId, String emtTxnNo) {

		com.payqwikweb.app.model.response.bus.ResponseDTO resp = new com.payqwikweb.app.model.response.bus.ResponseDTO();

		List<TravellerDetailsDTO> list = new ArrayList<>();
		List<String> seatNo = new ArrayList<>();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", sessionId);
			payload.put("bookingTxnId", emtTxnNo);
			Client vpqClient = Client.create();
			WebResource vpqWebResource = vpqClient.resource(UrlMetadatas.getTravellerDetailsForAdmin(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse vpqresponse = vpqWebResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = vpqresponse.getEntity(String.class);
			org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
			final String code = jobj.getString("code");
			if (code.equalsIgnoreCase("S00")) {
				org.json.JSONArray details = jobj.getJSONArray("details");
				for (int i = 0; i < details.length(); i++) {
					TravellerDetailsDTO dto = new TravellerDetailsDTO();
					dto.setfName((String) details.getJSONObject(i).get("fName"));
					dto.setlName((String) details.getJSONObject(i).get("lName"));
					dto.setGender((String) details.getJSONObject(i).get("gender"));
					dto.setSeatNo((String) details.getJSONObject(i).get("seatNo"));
					dto.setFare((String) details.getJSONObject(i).get("fare"));
					dto.setAge((details.getJSONObject(i).getInt("age")) + "");
					list.add(dto);
					seatNo.add((String) details.getJSONObject(i).get("seatNo"));
				}
				if (details.length() >= 0) {
					BusTicketDTO busTicketDTO = new BusTicketDTO();
					String source = (String) details.getJSONObject(0).getJSONObject("busTicketId").get("source");
					String status = (String) details.getJSONObject(0).getJSONObject("busTicketId").get("status");
					String destination = (String) details.getJSONObject(0).getJSONObject("busTicketId")
							.get("destination");
					String journeyDate = (String) details.getJSONObject(0).getJSONObject("busTicketId")
							.get("journeyDate");
					String operatorPnr = details.getJSONObject(0).getJSONObject("busTicketId").getString("operatorPnr");
					String busOperator = (String) details.getJSONObject(0).getJSONObject("busTicketId")
							.getString("busOperator");
					String ticketPnr = details.getJSONObject(0).getJSONObject("busTicketId").getString("ticketPnr");
					;
					double totalFare = (double) details.getJSONObject(0).getJSONObject("busTicketId")
							.getDouble("totalFare");
					String emtTxnId = (String) details.getJSONObject(0).getJSONObject("busTicketId").get("emtTxnId");
					String boardingAddress = (String) details.getJSONObject(0).getJSONObject("busTicketId")
							.get("boardingAddress");
					String arrTime = (String) details.getJSONObject(0).getJSONObject("busTicketId").get("arrTime");
					String emtTransactionScreenId = (String) details.getJSONObject(0).getJSONObject("busTicketId")
							.get("emtTransactionScreenId");
					String vPqTxnRefNo = "NA";
					if (details.getJSONObject(0).getJSONObject("busTicketId").getString("transaction") != null
							&& !details.getJSONObject(0).getJSONObject("busTicketId").getString("transaction").isEmpty()
							&& !details.getJSONObject(0).getJSONObject("busTicketId").getString("transaction")
									.equalsIgnoreCase("Null")) {
						vPqTxnRefNo = details.getJSONObject(0).getJSONObject("busTicketId").getJSONObject("transaction")
								.getString("transactionRefNo");
					}

					String deptTime = (String) details.getJSONObject(0).getJSONObject("busTicketId").get("deptTime");
					String boardTime = details.getJSONObject(0).getJSONObject("busTicketId").getString("boardTime");
					long created = details.getJSONObject(0).getJSONObject("busTicketId").getLong("created");

					Date date = new Date(created);

					String dateStr = simformat.format(date);

					busTicketDTO.setCreated(dateStr);
					busTicketDTO.setBoardTime(boardTime);
					busTicketDTO.setDeptTime(deptTime);
					busTicketDTO.setSource(source);
					busTicketDTO.setDestination(destination);
					busTicketDTO.setJourneyDate(journeyDate);
					busTicketDTO.setOperatorPnr(operatorPnr);
					busTicketDTO.setBusOperator(busOperator);
					busTicketDTO.setTicketPnr(ticketPnr);
					busTicketDTO.setTotalFare(totalFare);
					busTicketDTO.setEmtTxnId(emtTxnId);
					busTicketDTO.setBoardingAddress(boardingAddress);
					busTicketDTO.setArrTime(arrTime);
					busTicketDTO.setEmtTransactionScreenId(emtTransactionScreenId);
					busTicketDTO.setTransactionRefNo(vPqTxnRefNo);
					busTicketDTO.setStatus(status);
					busTicketDTO.setDto(list);
					resp.setExtraInfo(busTicketDTO);

				}
			}

			resp.setSeatNo(seatNo);
			resp.setCode(code);
			resp.setMessage("Get All Book Tickets");
			resp.setDetails(list);
			resp.setStatus(ResponseStatus.SUCCESS.getKey());

		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			resp.setMessage("Service Unavailable");
			resp.setDetails(null);
			resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
		}
		return resp;

	}

	@Override
	public TListResponse getRefundMerchantTransactionList(RefundDTO request) {
		TListResponse resp = new TListResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getRefundTransactionsURL(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456")).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (code.equalsIgnoreCase("S00")) {
							List<RefundDTO> transactions = new ArrayList<>();
							resp.setSuccess(true);
							org.json.JSONArray details = JSONParserUtil.getArray(jobj, "details");
							if (details != null && details.length() > 0) {
								for (int i = 0; i < details.length(); i++) {
									org.json.JSONObject json = details.getJSONObject(i);
									RefundDTO dto = new RefundDTO();
									dto.setUsername(JSONParserUtil.getString(json, "username"));
									dto.setTransactionRefNo(JSONParserUtil.getString(json, "transactionRefNo"));
									dto.setDateOfTrasnaction(JSONParserUtil.getString(json, "created"));
									dto.setNetAmount(JSONParserUtil.getDouble(json, "netAmount"));
									dto.setRefundAmount(JSONParserUtil.getString(json, "refundAmount"));
									dto.setOrderId(JSONParserUtil.getString(json, "orderId"));
									dto.setStatus(JSONParserUtil.getString(json, "status"));
									transactions.add(dto);
								}
								resp.setRefundList(transactions);
							} else {
								resp.setSuccess(false);
								resp.setMessage("Transaction List Empty");
							}
						} else {
							resp.setSuccess(false);
							resp.setMessage("Please try Later");
						}
					} else {
						resp.setSuccess(false);
					}
				} else {
					resp.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
		}
		return resp;

	}

	@Override
	public TransactionUserResponse refundMerchantTransaction(RefundDTO dto) {
		TransactionUserResponse resp = new TransactionUserResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", dto.getSessionId());
			payload.put("transactionRefNo", dto.getTransactionRefNo());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getSuperadminMerchantRefund(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456")).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						final String message = jobj.getString("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setMessage(message);
						resp.setCode(code);
					}
				} else {
					resp.setSuccess(false);
					resp.setMessage("strResponse is null");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setMessage("Exception Occurred");
		}

		return resp;
	}

	@Override
	public ServicesDTO updateMdexSwitch(ServicesDTO request) {
		ServicesDTO resp = new ServicesDTO();
		try {
			Client client = Client.create();
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("code", request.getCode());
			payload.put("status", request.getStatus());
			WebResource webResource = client.resource(UrlMetadatas.updateMdexSwitch(Version.VERSION_1, Role.SUPERADMIN,
					Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456")).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						final String message = jobj.getString("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
							org.json.JSONObject details = jobj.getJSONObject("details");
							if (details != null) {
								resp.setSuccess(true);
								resp.setCode(details.getString("code"));
								resp.setStatus(details.getString("status"));
							}
						} else {
							resp.setSuccess(false);
							resp.setMessage(message);
						}
					}
				} else {
					resp.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
		}
		return resp;
	}

	@Override
	public ServiceTypeResponse getService() {
		ServiceTypeResponse resp = new ServiceTypeResponse();
		try {
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getServiceURL(Version.VERSION_1, Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456")).get(ClientResponse.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							List<com.payqwikweb.app.model.ServicesDTO> serviceTypeDTOs = new ArrayList<>();
							resp.setSuccess(true);
							org.json.JSONArray details = JSONParserUtil.getArray(jobj, "details");
							if (details != null && details.length() > 0) {
								for (int i = 0; i < details.length(); i++) {
									com.payqwikweb.app.model.ServicesDTO s = new com.payqwikweb.app.model.ServicesDTO();
									org.json.JSONObject temp = (org.json.JSONObject) details.get(i);
									s.setCode(JSONParserUtil.getString(temp, "code"));
									s.setDescription(JSONParserUtil.getString(temp, "description"));
									serviceTypeDTOs.add(s);
								}
								resp.setServicesDTOs(serviceTypeDTOs);
							}
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
		}
		return resp;
	}

	@Override
	public ServiceTypeResponse getServicesById(PromoCodeRequest req) {
		ServiceTypeResponse resp = new ServiceTypeResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("serviceTypeId", req.getServiceId());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getServicesByIdSuperAdmin(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456")).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if ("S00".equalsIgnoreCase(code)) {
							List<com.payqwikweb.app.model.ServicesDTO> serviceTypeDTOs = new ArrayList<>();
							resp.setSuccess(true);
							org.json.JSONArray details = JSONParserUtil.getArray(jobj, "details");
							if (details != null && details.length() > 0) {
								for (int i = 0; i < details.length(); i++) {
									com.payqwikweb.app.model.ServicesDTO s = new com.payqwikweb.app.model.ServicesDTO();
									org.json.JSONObject temp = (org.json.JSONObject) details.get(i);
									s.setCode(JSONParserUtil.getString(temp, "code"));
									s.setDescription(JSONParserUtil.getString(temp, "description"));
									serviceTypeDTOs.add(s);
								}
								resp.setServicesDTOs(serviceTypeDTOs);
							}
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
		}
		return resp;
	}

	@Override
	public ServiceTypeResponse getAllCommission(ServiceRequest service) {
		ServiceTypeResponse comResponse = new ServiceTypeResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("code", service.getCode());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getAllCommission(Version.VERSION_1, Role.SUPERADMIN,
					Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456")).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (strResponse != null) {
				org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
				if (jobj != null) {
					final String code = (String) jobj.get("code");
					if ("S00".equalsIgnoreCase(code)) {
						org.json.JSONArray details = JSONParserUtil.getArray(jobj, "details");
						if (details != null && details.length() > 0) {
							List<CommissionDTO> comList = new ArrayList<>();
							for (int i = 0; i < details.length(); i++) {
								CommissionDTO dto = new CommissionDTO();
								org.json.JSONObject obj = details.getJSONObject(i);
								dto.setFixed(obj.getString("fixed"));
								dto.setValue(obj.getString("value"));
								dto.setType(obj.getString("type"));
								dto.setIdentifier(obj.getString("identifier"));
								dto.setMaxAmount(obj.getString("maxAmount"));
								dto.setMinAmount(obj.getString("minAmount"));
								dto.setServiceId(obj.getString("serviceId"));
								dto.setServiceName(obj.getString("serviceName"));
								comList.add(dto);
							}
							comResponse.setCode(code);
							comResponse.setSuccess(true);
							comResponse.setCommList(comList);
							return comResponse;
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return comResponse;
	}

	@Override
	public ResponseDTO updateCommission(CommissionDTO comDto) {
		ResponseDTO comResponse = new ResponseDTO();
		try {
			JSONObject payload = new JSONObject();
			payload.put("minAmount", comDto.getMinAmount());
			payload.put("maxAmount", comDto.getMaxAmount());
			payload.put("fixed", comDto.getFixed());
			payload.put("value", comDto.getValue());
			payload.put("type", comDto.getType());
			payload.put("serviceId", comDto.getServiceId());
			payload.put("sessionId", comDto.getSessionId());
			payload.put("code", comDto.getCode());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.updateCommission(Version.VERSION_1, Role.SUPERADMIN,
					Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456")).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (strResponse != null) {
				org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
				if (jobj != null) {
					final String code = (String) jobj.get("code");
					if ("S00".equalsIgnoreCase(code)) {
						comResponse.setMessage("Commission updated successfully");
					} else {
						comResponse.setMessage((String) jobj.get("message"));
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return comResponse;
	}

	@Override
	public ResponseDTO AddServices(AddServicesDTO dto) {
		ResponseDTO resp = new ResponseDTO();
		try {
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(
					UrlMetadatas.addServices(Version.VERSION_1, Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456")).post(ClientResponse.class, dto.toJSON());
			String strResponse = response.getEntity(String.class);
			logger.info("response  ::" + strResponse);
			if (strResponse != null) {
				org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
				if (jobj != null) {
					final String code = (String) jobj.get("code");
					final String message = jobj.getString("message");
					if (code.equalsIgnoreCase("S00")) {
						resp.setSuccess(true);
						resp.setCode(code);
						resp.setMessage(message);
					} else {
						resp.setSuccess(false);
						resp.setCode(code);
						resp.setMessage(message);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}
	
	@Override
	public ServiceTypeResponse getOperatorList() {
		ServiceTypeResponse resp = new ServiceTypeResponse();
		try {
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getOperatorOfSuperAdminURL(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456")).get(ClientResponse.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (code.equalsIgnoreCase("S00")) {
							List<ServiceTypeDTO> serviceTypeDTOs = new ArrayList<>();
							resp.setSuccess(true);
							org.json.JSONArray details = JSONParserUtil.getArray(jobj, "details");
							if (details != null && details.length() > 0) {
								for (int i = 0; i < details.length(); i++) {
									ServiceTypeDTO s = new ServiceTypeDTO();
									org.json.JSONObject temp = (org.json.JSONObject) details.get(i);
									s.setName(JSONParserUtil.getString(temp, "name"));
									serviceTypeDTOs.add(s);
								}
								resp.setServiceDTOs(serviceTypeDTOs);
							}
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
					} 
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
		}
		return resp;
	}
	
	@Override
	public AadharServiceResponse getAadharDetails(SessionDTO request) {
		AadharServiceResponse resp = new AadharServiceResponse();
		try {
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			WebResource webResource = client.resource(UrlMetadatas.getAadharDetailsOfUsers(Version.VERSION_1,
					Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456")).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (code.equalsIgnoreCase("S00")) {
							List<AadharDetailsDTO> aadharDTOs = new ArrayList<>();
							resp.setSuccess(true);
							org.json.JSONArray details = JSONParserUtil.getArray(jobj, "details");
							if (details != null && details.length() > 0) {
								for (int i = 0; i < details.length(); i++) {
									AadharDetailsDTO s = new AadharDetailsDTO();
									org.json.JSONObject temp = (org.json.JSONObject) details.get(i);
									s.setAadharName(JSONParserUtil.getString(temp, "aadharName"));
									s.setAadharAddress(JSONParserUtil.getString(temp, "aadharAddress"));
									s.setMobileNumber(JSONParserUtil.getString(temp, "mobileNumber"));
									s.setAadharNumber(JSONParserUtil.getString(temp, "aadharNumber"));
									s.setDateOfUpdate(JSONParserUtil.getString(temp, "dateOfUpdate"));
									aadharDTOs.add(s);
								}
								resp.setAadharList(aadharDTOs);
							}
						} else {
							resp.setSuccess(false);
							resp.setCode(code);
						}
						resp.setCode(code);
					} 
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
		}
		return resp;
	}
	
	
	@Override
	public ResponseDTO AddServiceType(AddServiceTypeDTO dto) {
		ResponseDTO resp = new ResponseDTO();
		try {
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(
					UrlMetadatas.addServiceType(Version.VERSION_1, Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456")).post(ClientResponse.class, dto.toJSON());
			String strResponse = response.getEntity(String.class);
			logger.info("response  ::" + strResponse);
			if (strResponse != null) {
				org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
				if (jobj != null) {
					final String code = (String) jobj.get("code");
					final String message = jobj.getString("message");
					if (code.equalsIgnoreCase("S00")) {
						resp.setSuccess(true);
						resp.setCode(code);
						resp.setMessage(message);
					} else {
						resp.setSuccess(false);
						resp.setCode(code);
						resp.setMessage(message);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}
}
