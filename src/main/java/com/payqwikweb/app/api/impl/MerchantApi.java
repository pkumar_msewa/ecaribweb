package com.payqwikweb.app.api.impl;

import java.text.SimpleDateFormat;

import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.payqwikweb.app.api.IMerchantApi;
import com.payqwikweb.app.metadatas.UrlMetadatas;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Language;
import com.payqwikweb.app.model.ResponseStatus;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.Version;
import com.payqwikweb.app.model.request.AllTransactionRequest;
import com.payqwikweb.app.model.request.LoginRequest;
import com.payqwikweb.app.model.request.RefundDTO;
import com.payqwikweb.app.model.request.SessionDTO;
import com.payqwikweb.app.model.request.StatusDTO;
import com.payqwikweb.app.model.response.AllTransactionResponse;
import com.payqwikweb.app.model.response.LoginResponse;
import com.payqwikweb.app.model.response.UserTransactionResponse;
import com.payqwikweb.app.utils.SecurityUtils;
import com.payqwikweb.model.app.response.TransactionUserResponse;
import com.payqwikweb.util.APIUtils;
import com.payqwikweb.util.JSONParserUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.thirdparty.model.ResponseDTO;

public class MerchantApi implements IMerchantApi{

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    
    

   

	@Override
    public LoginResponse login(LoginRequest request) {
        LoginResponse resp = new LoginResponse();
        try {
            JSONObject payload = new JSONObject();
            payload.put("username", request.getUsername());
            payload.put("password", request.getPassword());
            payload.put("ipAddress", request.getIpAddress());
            Client client = Client.create();
            WebResource webResource = client.resource(
                    UrlMetadatas.getLoginUrl(Version.VERSION_1, Role.MERCHANT, Device.WEBSITE, Language.ENGLISH));
            ClientResponse response = webResource.accept("application/json").type("application/json")
                    .header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
            String strResponse = response.getEntity(String.class);
            if (response.getStatus() != 200) {
                resp.setSuccess(false);
                resp.setCode("F00");
                resp.setMessage("Service unavailable");
                resp.setStatus("FAILED");
                resp.setResponse(APIUtils.getFailedJSON().toString());
            } else {
                if (strResponse != null) {
                	System.err.println("response ::" + strResponse);
                    org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
                    if (jobj != null) {
                        final String status = (String) jobj.get("status");
                        final String code = (String) jobj.get("code");
                        final String message = (String) jobj.get("message");
                        if (code.equalsIgnoreCase("S00")) {
                            resp.setSuccess(true);
                            final org.json.JSONObject details = JSONParserUtil.getObject(jobj,"details");
                            final org.json.JSONObject accountDetail = JSONParserUtil.getObject(details,"accountDetail");
                            final org.json.JSONObject accountType = JSONParserUtil.getObject(accountDetail,"accountType");
                            final org.json.JSONObject userDetail = JSONParserUtil.getObject(details,"userDetail");
                            final String sessionId = JSONParserUtil.getString(details,"sessionId");
                            final double balance = JSONParserUtil.getDouble(accountDetail,"balance");
                            final long accountNumber = JSONParserUtil.getLong(accountDetail,"accountNumber");
                            final String typeName = JSONParserUtil.getString(accountType,"name");
                            final String typeDescription = JSONParserUtil.getString(accountType,"description");
                            final double monthlyLimit = JSONParserUtil.getDouble(accountType,"monthlyLimit");
                            final double dailyLimit = JSONParserUtil.getDouble(accountType,"dailyLimit");
                            final String username = JSONParserUtil.getString(userDetail,"username");
                            final String firstName = JSONParserUtil.getString(userDetail,"firstName");
                            final String lastName = JSONParserUtil.getString(userDetail,"lastName");
                            final String contactNo = JSONParserUtil.getString(userDetail,"contactNo");
                            final String email = JSONParserUtil.getString(userDetail,"email");
                            final String image = JSONParserUtil.getString(userDetail,"image");
                            resp.setBalance(balance);
                            resp.setSessionId(sessionId);
                            resp.setAccountNumber(accountNumber);
                            resp.setAccountType(typeName);
                            resp.setDailyTransaction(dailyLimit);
                            resp.setMonthlyTransaction(monthlyLimit);
                            resp.setFirstName(firstName);
                            resp.setLastName(lastName);
                            resp.setEmail(email);
                            resp.setUsername(username);
                            resp.setContactNo(contactNo);
                            resp.setImage(image);
                        } else {
                            resp.setSuccess(false);
                        }
                        resp.setCode(code);
                        resp.setStatus(status);
                        resp.setMessage(message);
                        resp.setResponse(strResponse);
                    } else {
                        resp.setSuccess(false);
                        resp.setCode("F00");
                        resp.setMessage("Service unavailable");
                        resp.setStatus("FAILED");
                        resp.setResponse(APIUtils.getFailedJSON().toString());
                    }
                } else {
                    resp.setSuccess(false);
                    resp.setCode("F00");
                    resp.setMessage("Service unavailable");
                    resp.setStatus("FAILED");
                    resp.setResponse(APIUtils.getFailedJSON().toString());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            resp.setSuccess(false);
            resp.setCode("F00");
            resp.setMessage("Service unavailable");
            resp.setStatus("FAILED");
            resp.setResponse(APIUtils.getFailedJSON().toString());
        }
        return resp;
    }

    @Override
    public AllTransactionResponse getAllTransaction(AllTransactionRequest request) {
        AllTransactionResponse resp = new AllTransactionResponse();
        try {
            JSONObject payload = new JSONObject();
            payload.put("sessionId", request.getSessionId());
            Client client = Client.create();
            WebResource webResource = client.resource(
                    UrlMetadatas.getMerchantTransactions(Version.VERSION_1, Role.MERCHANT, Device.WEBSITE, Language.ENGLISH));
            ClientResponse response = webResource.accept("application/json").type("application/json")
                    .header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
            String strResponse = response.getEntity(String.class);
            if (response.getStatus() != 200) {
                resp.setSuccess(false);
                resp.setCode("F00");
                resp.setMessage("Service unavailable");
                resp.setStatus("FAILED");
                resp.setResponse(APIUtils.getFailedJSON().toString());
            } else {
                if (strResponse != null) {
                    org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
                    if (jobj != null) {
                        final String status = (String) jobj.get("status");
                        final String code = (String) jobj.get("code");
                        final String message = (String) jobj.get("message");
                        if(code.equalsIgnoreCase("S00")) {
                            final org.json.JSONArray totalTtransactions = (org.json.JSONArray) jobj
                                    .getJSONArray("details");
//                            resp.setJsonArray(totalTtransactions);
                            resp.setSuccess(true);
                        } else {
                            resp.setSuccess(false);
                        }
                        resp.setCode(code);
                        resp.setStatus(status);
                        resp.setMessage(message);
                        resp.setResponse(strResponse);
                    } else {
                        resp.setSuccess(false);
                        resp.setCode("F00");
                        resp.setMessage("Service unavailable");
                        resp.setStatus("FAILED");
                        resp.setResponse(APIUtils.getFailedJSON().toString());
                    }
                } else {
                    resp.setSuccess(false);
                    resp.setCode("F00");
                    resp.setMessage("Service unavailable");
                    resp.setStatus("FAILED");
                    resp.setResponse(APIUtils.getFailedJSON().toString());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            resp.setSuccess(false);
            resp.setCode("F00");
            resp.setMessage("Service unavailable");
            resp.setStatus("FAILED");
            resp.setResponse(APIUtils.getFailedJSON().toString());
        }
        return resp;
    }

    @Override
    public ResponseDTO getStatus(StatusDTO request) {
        ResponseDTO resp = new ResponseDTO();
        try {
            JSONObject payload = new JSONObject();
            payload.put("transactionRefNo", request.getTransactionRefNo());
            Client client = Client.create();
            WebResource webResource = client.resource(
                    UrlMetadatas.getStatusByTrx(Version.VERSION_1, Role.MERCHANT, Device.WEBSITE, Language.ENGLISH));
            ClientResponse response = webResource.accept("application/json").type("application/json")
                    .header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
            String strResponse = response.getEntity(String.class);
            if (response.getStatus() != 200) {
                resp.setSuccess(false);
                resp.setCode("F00");
                resp.setMessage("Service unavailable");
                resp.setStatus("FAILED");
                resp.setResponse(APIUtils.getFailedJSON().toString());
            } else {
                if (strResponse != null) {
                    org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
                    if (jobj != null) {
                      resp.setStatus(jobj.getString("status"));
                      resp.setCode(jobj.getString("code"));
                      resp.setMessage(jobj.getString("message"));
                      resp.setTransactionId(jobj.getString("txnId"));
                    } else {
                        resp.setSuccess(false);
                        resp.setCode("F00");
                        resp.setMessage("Service unavailable");
                        resp.setStatus("FAILED");
                        //resp.setResponse(APIUtils.getFailedJSON().toString());
                    }
                } else {
                    resp.setSuccess(false);
                    resp.setCode("F00");
                    resp.setMessage("Service unavailable");
                    resp.setStatus("FAILED");
                    //resp.setResponse(APIUtils.getFailedJSON().toString());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            resp.setSuccess(false);
            resp.setCode("F00");
            resp.setMessage("Service unavailable");
            resp.setStatus("FAILED");
           // resp.setResponse(APIUtils.getFailedJSON().toString());
        }
        return resp;
    }

    @Override
    public ResponseDTO requestRefund(String sessionId,String path) {
        ResponseDTO resp = new ResponseDTO();
        try {
            JSONObject payload = new JSONObject();
            payload.put("sessionId", sessionId);
            payload.put("path", path);
            Client client = Client.create();
            WebResource webResource = client.resource(
                    UrlMetadatas.refundRequest(Version.VERSION_1, Role.MERCHANT, Device.WEBSITE, Language.ENGLISH));
            ClientResponse response = webResource.accept("application/json").type("application/json")
                    .header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
            String strResponse = response.getEntity(String.class);
            if (response.getStatus() != 200) {
                resp.setSuccess(false);
                resp.setCode("F00");
                resp.setMessage("Service unavailable");
                resp.setStatus("FAILED");
                resp.setResponse(APIUtils.getFailedJSON().toString());
            } else {
                if (strResponse != null) {
                    org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
                    if (jobj != null) {
                      resp.setStatus(jobj.getString("status"));
                      resp.setCode(jobj.getString("code"));
                      resp.setMessage(jobj.getString("message"));
                    } else {
                        resp.setSuccess(false);
                        resp.setCode("F00");
                        resp.setMessage("Service unavailable");
                        resp.setStatus("FAILED");
                        //resp.setResponse(APIUtils.getFailedJSON().toString());
                    }
                } else {
                    resp.setSuccess(false);
                    resp.setCode("F00");
                    resp.setMessage("Service unavailable");
                    resp.setStatus("FAILED");
                    //resp.setResponse(APIUtils.getFailedJSON().toString());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            resp.setSuccess(false);
            resp.setCode("F00");
            resp.setMessage("Service unavailable");
            resp.setStatus("FAILED");
           // resp.setResponse(APIUtils.getFailedJSON().toString());
        }
        return resp;
    }
    
	@Override
	public TransactionUserResponse requestRefundTransaction(RefundDTO dto) {
		TransactionUserResponse resp = new TransactionUserResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId",dto.getSessionId());
			payload.put("transactionRefNo",dto.getTransactionRefNo());
			payload.put("netAmount",dto.getNetAmount());
			payload.put("orderId",dto.getOrderId());
			payload.put("refundAmount",dto.getRefundAmount());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.refundTransactionRequest(Version.VERSION_1, Role.MERCHANT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456")).post(ClientResponse.class,payload);
			String strResponse = response.getEntity(String.class);
			System.err.println(strResponse);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						final String message = jobj.getString("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
					resp.setCode(code);
					resp.setMessage(message);
					} else {
						resp.setSuccess(false);
						resp.setMessage("Response cannot be parsed");
					}
				} else {
					resp.setSuccess(false);
					resp.setMessage("strResponse is null");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setMessage("Exception Occurred");
		}
		return resp;
	}
	
	@Override
	public UserTransactionResponse getMerchatTransactionValues(SessionDTO dto) {
		UserTransactionResponse resp = new UserTransactionResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", dto.getSessionId());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getMerchantValuesListURL(Version.VERSION_1, Role.MERCHANT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

}
