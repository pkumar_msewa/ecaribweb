package com.payqwikweb.app.api.impl;
import com.payqwikweb.app.api.IPartnerApi;
import com.payqwikweb.app.metadatas.UrlMetadatas;
import com.payqwikweb.app.model.request.PDeviceUpdateDTO;
import com.payqwikweb.app.model.response.PDeviceUpdateResponse;
import com.payqwikweb.util.JSONParserUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import org.json.JSONObject;

public class PartnerApi implements IPartnerApi{

    @Override
    public PDeviceUpdateResponse saveDevice(PDeviceUpdateDTO dto) {
      PDeviceUpdateResponse response = new PDeviceUpdateResponse();
        try {
            String stringResponse = "";
            WebResource resource = Client.create().resource(UrlMetadatas.registerPartnerDevice()) ;
            ClientResponse clientResponse = resource.post(ClientResponse.class,dto.toJSON());
            stringResponse = clientResponse.getEntity(String.class);
            System.err.println("response::"+stringResponse);
            if (clientResponse.getStatus() == 200) {
                JSONObject o = new JSONObject(stringResponse);
                String code = JSONParserUtil.getString(o,"code");
                response.setMessage(JSONParserUtil.getString(o,"message"));
                if(code.equalsIgnoreCase("S00")) {
                    response.setSuccess(true);
                } else {
                    response.setSuccess(false);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }
}
