package com.payqwikweb.app.api.impl;

import javax.ws.rs.core.MediaType;

import org.codehaus.jettison.json.JSONObject;
import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.payqwikweb.app.api.IMeraEventsApi;
import com.payqwikweb.app.metadatas.UrlMetadatas;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Language;
import com.payqwikweb.app.model.ResponseStatus;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.Version;
import com.payqwikweb.app.model.request.MeraEventDetailsRequest;
import com.payqwikweb.app.model.request.MeraEventsBookingRequest;
import com.payqwikweb.app.model.request.MeraEventsCommonRequest;
import com.payqwikweb.app.model.request.MeraEventsListRequest;
import com.payqwikweb.app.model.response.MeraEventCategoryListResponse;
import com.payqwikweb.app.model.response.MeraEventCityListResponse;
import com.payqwikweb.app.model.response.MeraEventTicketCalculationResponse;
import com.payqwikweb.app.model.response.MeraEventsAttendeeFormRequest;
import com.payqwikweb.app.model.response.MeraEventsListResponse;
import com.payqwikweb.app.model.response.MeraEventsResponse;
import com.payqwikweb.app.model.response.MeraEventsTicketDetailsResponse;
import com.payqwikweb.app.utils.SecurityUtils;
import com.payqwikweb.controller.mobile.api.thirdparty.MeraEventAmountInitateRequest;
import com.payqwikweb.util.APIUtils;
import com.payqwikweb.util.LogCat;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import com.thirdparty.util.Ssltest;

public class MeraEventsApi implements IMeraEventsApi{

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	/*@Override
	public  MeraEventsResponse getAuthorizationCode(MeraEventsCommonRequest request) {
		MeraEventsResponse resp = new MeraEventsResponse();
		try {
			Client client = Client.create();
			WebResource webResource = client.resource(APIUtils.BASE_URL+APIUtils.AUTH_CODE+"?clientId="+request.getClientId());
			ClientResponse response = webResource.accept("application/json").type("application/json").get(ClientResponse.class);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("Failure");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					JSONObject jsonObj = new JSONObject(strResponse);
					if (jsonObj != null) {
					String authorizationCode = jsonObj.getJSONObject("response").getString("authorizationCode");
					resp.setSuccess(true);
					resp.setCode("S00");
					resp.setMessage("Client Authenticated");
					resp.setStatus("Success");
					resp.setResponse(authorizationCode);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("Failure");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}

	@Override
	public MeraEventsResponse getAccessToken(MeraEventsCommonRequest request) {
		MeraEventsResponse resp = new MeraEventsResponse();
		try {
			MultivaluedMapImpl formData = new MultivaluedMapImpl();
			formData.add("clientId", request.getClientId());
			formData.add("clientSecret", request.getClientSecret());
			formData.add("authorizationCode", request.getAuthorizationCode());
			Client client = Client.create();
			WebResource webResource = client.resource(APIUtils.BASE_URL+APIUtils.ACCESS_TOKEN);
			ClientResponse response = webResource.accept("application/json").post(ClientResponse.class,formData);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("Failure");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					JSONObject jsonObj = new JSONObject(strResponse);
					if (jsonObj != null) {
					String accessToken = jsonObj.getJSONObject("response").getString("accessToken") ;
					resp.setSuccess(true);
					resp.setCode("S00");
					resp.setMessage("Access token generated");
					resp.setStatus("Success");
					resp.setResponse(accessToken);
					} 
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("Failure");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}

	@Override
	public MeraEventsResponse saveAccessToken(MeraEventsCommonRequest request) {
		MeraEventsResponse resp = new MeraEventsResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("accessToken", request.getAccess_token());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.saveAccessTokenURL(Version.VERSION_1, Role.USER,
					Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type(MediaType.APPLICATION_JSON)
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				System.err.print(strResponse);
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						final String details = (String)jobj.get("details");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(details);
					} 
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("Failure");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("Failure");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}*/

	
	@Override
	public MeraEventCityListResponse getCities(MeraEventsCommonRequest request) {
		MeraEventCityListResponse resp = new MeraEventCityListResponse();
		JSONObject payload = new JSONObject();
		JSONObject data = new JSONObject();
			
			try {
				payload.put("intent","cityList");
				payload.put("data", data);
			//Client client = Client.create();
				Client client=Ssltest.createClient(); 
			WebResource webResource = client.resource("https://dev.appsfly.io/executor/exec");
			ClientResponse response = webResource.accept("application/json")
					.header("Content-Type", "application/json")
					.header("X-UUID", "generic")
					.header("X-Association-Key", "b98babec-e9cd-4227-8fc1-3ee28e07d2ab")
					.header("X-Product-Id", "58c7d3c3862dc10011305ade")
					.header("X-Module-Id", "58d39e6726d9720011a6ff20")
					.post(ClientResponse.class,payload.toString());
			String strResponse = response.getEntity(String.class);
			System.err.println("getCities API RESPONSE::::::::::::"+strResponse);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("Failure");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					JSONArray ja = new JSONArray(strResponse);

					if (ja != null) {
					resp.setSuccess(true);
					resp.setCode("S00");
					resp.setMessage("City List : ");
					resp.setStatus("Success");
					resp.setResponse(strResponse);
					} 
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("Failure");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}
		
	@Override
	public MeraEventCategoryListResponse getEventCategory(MeraEventsCommonRequest request) {
		MeraEventCategoryListResponse resp = new MeraEventCategoryListResponse();
		JSONObject payload = new JSONObject();
		JSONObject data = new JSONObject();
			
			try {
				payload.put("intent","categoriesList");
				payload.put("data", data);
				Client client=Ssltest.createClient(); 
			WebResource webResource = client.resource("https://dev.appsfly.io/executor/exec");
			ClientResponse response = webResource.accept("application/json")
					.header("Content-Type", "application/json")
					.header("X-UUID", "generic")
					.header("X-Association-Key", "b98babec-e9cd-4227-8fc1-3ee28e07d2ab")
					.header("X-Product-Id", "58c7d3c3862dc10011305ade")
					.header("X-Module-Id", "58d39e6726d9720011a6ff20")
					.post(ClientResponse.class,payload.toString());
			String strResponse = response.getEntity(String.class);
			
			System.err.println("SELECT CATEGEORY API ::::::::RESPONSE"+strResponse);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("Failure");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				
				
				if (strResponse != null) {
					JSONArray ja = new JSONArray(strResponse);

					if (ja != null) {
					resp.setSuccess(true);
					resp.setCode("S00");
					resp.setMessage("Category List : ");
					resp.setStatus("Success");
					resp.setResponse(strResponse);
				
					} 
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("Failure");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}

	@Override
	public MeraEventsListResponse getEventList(MeraEventsListRequest request) {
		MeraEventsListResponse resp = new MeraEventsListResponse();
		JSONObject payload = new JSONObject();
		JSONObject payload1 = new JSONObject();
			
			try {
				payload.put("intent","sync");
				payload1.put("cityId", request.getCityId());
				payload1.put("categoryId",request.getCategoryId());
				payload.put("data",payload1);
				Client client=Ssltest.createClient(); 
			WebResource webResource = client.resource("https://dev.appsfly.io/executor/exec");
			ClientResponse response = webResource.accept("application/json")
					.header("Content-Type", "application/json")
					.header("X-UUID", "generic")
					.header("X-Association-Key", "b98babec-e9cd-4227-8fc1-3ee28e07d2ab")
					.header("X-Product-Id", "58c7d3c3862dc10011305ade")
					.header("X-Module-Id", "58d39e6726d9720011a6ff20")
					.post(ClientResponse.class,payload.toString());
			String strResponse = response.getEntity(String.class);
			System.out.println("EVENT LIST RESPONSE : : "+strResponse);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus(ResponseStatus.FAILURE);
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jsonObj = new org.json.JSONObject(strResponse);
					if (jsonObj != null) {
					org.json.JSONArray eventList = jsonObj.getJSONArray("eventList");
					resp.setSuccess(true);
					resp.setCode("S00");
					resp.setStatus(ResponseStatus.SUCCESS);
					resp.setJsonArray(eventList);
					} 
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus(ResponseStatus.FAILURE);
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}
	

	@Override
	public MeraEventsListResponse getEventDetails(MeraEventsCommonRequest request) {
		MeraEventsListResponse resp = new MeraEventsListResponse();
		JSONObject payload = new JSONObject();
		JSONObject payload1 = new JSONObject();
			
			try {
				payload.put("intent","eventDetails");
				payload1.put("event_id", request.getEventId());
				payload.put("data",payload1);
				Client client=Ssltest.createClient(); 
			WebResource webResource = client.resource("https://dev.appsfly.io/executor/exec");
			ClientResponse response = webResource.accept("application/json")
					.header("Content-Type", "application/json")
					.header("X-UUID", "generic")
					.header("X-Association-Key", "b98babec-e9cd-4227-8fc1-3ee28e07d2ab")
					.header("X-Product-Id", "58c7d3c3862dc10011305ade")
					.header("X-Module-Id", "58d39e6726d9720011a6ff20")
					.post(ClientResponse.class,payload.toString());
			String strResponse = response.getEntity(String.class);
			
			System.err.println("STR RESPONSE from event detail api::::::::::::"+strResponse);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus(ResponseStatus.FAILURE);
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					JSONObject jsonObj = new JSONObject(strResponse);
					System.err.println("EVENT DETAILS :"+strResponse);
					if (jsonObj != null) {
					resp.setSuccess(true);
					resp.setCode("S00");
					resp.setStatus(ResponseStatus.SUCCESS);
					resp.setResponse(strResponse);
					} 
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus(ResponseStatus.FAILURE);
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}

	
	@Override
	public MeraEventsTicketDetailsResponse getEventTicketDetails(MeraEventsCommonRequest request) {
		MeraEventsTicketDetailsResponse resp = new MeraEventsTicketDetailsResponse();
		JSONObject payload = new JSONObject();
		JSONObject payload1 = new JSONObject();
			
			try {
				payload.put("intent","ticketDetails");
				payload1.put("event_id", request.getEventId());
				payload.put("data",payload1);
				Client client=Ssltest.createClient(); 
			WebResource webResource = client.resource("https://dev.appsfly.io/executor/exec");
			ClientResponse response = webResource.accept("application/json")
					.header("Content-Type", "application/json")
					.header("X-UUID", "generic")
					.header("X-Association-Key", "b98babec-e9cd-4227-8fc1-3ee28e07d2ab")
					.header("X-Product-Id", "58c7d3c3862dc10011305ade")
					.header("X-Module-Id", "58d39e6726d9720011a6ff20")
					.post(ClientResponse.class,payload.toString());
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("Failure");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				
				if (strResponse != null) {
					JSONArray ja = new JSONArray(strResponse);
					if (ja != null) {
					resp.setSuccess(true);
					resp.setCode("S00");
					resp.setMessage("TicketList : ");
					resp.setStatus("Success");
					resp.setResponse(strResponse);
					} else {
						resp.setSuccess(false);
						resp.setCode("F00");
						resp.setMessage("Service unavailable");
						resp.setStatus("Failure");
						resp.setResponse(APIUtils.getFailedJSON().toString());
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("Failure");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}

	@Override
	public MeraEventTicketCalculationResponse calculateTotalAmount(MeraEventsBookingRequest request) {
		MeraEventTicketCalculationResponse resp = new MeraEventTicketCalculationResponse();
		JSONObject payload = new JSONObject();
		JSONObject payload1 = new JSONObject();
			
			try {
				payload.put("intent","ticketCalculation");
				payload1.put("event_id", request.getEventId());
				//payload1.put("categoryId",request.getCategoryId());
				payload.put("data",payload1);
				Client client=Ssltest.createClient(); 
			WebResource webResource = client.resource("https://dev.appsfly.io/executor/exec");
			ClientResponse response = webResource.accept("application/json")
					.header("Content-Type", "application/json")
					.header("X-UUID", "generic")
					.header("X-Association-Key", "b98babec-e9cd-4227-8fc1-3ee28e07d2ab")
					.header("X-Product-Id", "58c7d3c3862dc10011305ade")
					.header("X-Module-Id", "58d39e6726d9720011a6ff20")
					.post(ClientResponse.class,payload.toString());
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus(ResponseStatus.FAILURE);
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					JSONObject jsonObj = new JSONObject(strResponse);
					if (jsonObj != null) {
					resp.setSuccess(true);
					resp.setCode("S00");
					resp.setStatus(ResponseStatus.SUCCESS);
					resp.setResponse(strResponse);
					} else {
						resp.setSuccess(false);
						resp.setCode("F00");
						resp.setMessage("Service unavailable");
						resp.setStatus(ResponseStatus.FAILURE);
						resp.setResponse(APIUtils.getFailedJSON().toString());
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus(ResponseStatus.FAILURE);
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}
	
	
	@Override
	public MeraEventsResponse initiateBooking(MeraEventsBookingRequest request) {
		MeraEventsResponse resp = new MeraEventsResponse();
		
		
		try {
			JSONObject payload = new JSONObject();
			JSONObject payloadData = new JSONObject();
			JSONObject payloadSubmit = new JSONObject();
			
				
			payloadSubmit.put(request.getTicketId(),request.getNoOfTickets());
			payloadData.put("tickets", payloadSubmit);
			payloadData.put("event_id",request.getEventId());
			payload.put("data",payloadData);
			payload.put("intent","registrationFields");
				System.err.println("error::"+payload.toString());
			
		//try {
			/*MultivaluedMapImpl formData = new MultivaluedMapImpl();
			formData.add("eventId", request.getEventId());
			formData.add("ticketArray["+request.getTicketId()+"]", request.getNoOfTickets());
		//	formData.add("ticketArray["+request.getTicketID2()+"]", request.getNoOfTickets2());
*/			System.err.println("PAYLOAD DATA INSIDE INTIATE BOOK API API"+payload.toString());
          Client client=Ssltest.createClient(); 
           WebResource webResource = client.resource("https://dev.appsfly.io/executor/exec");
            ClientResponse response = webResource.accept("application/json")
		.header("Content-Type", "application/json")
		.header("X-UUID", "generic")
		.header("X-Association-Key", "b98babec-e9cd-4227-8fc1-3ee28e07d2ab")
		.header("X-Product-Id", "58c7d3c3862dc10011305ade")
		.header("X-Module-Id", "58d39e6726d9720011a6ff20")
		.post(ClientResponse.class,payload.toString());
			String strResponse = response.getEntity(String.class);
			LogCat.print("Response : : " +strResponse);
			System.err.println("STR RESPONSE: INITATE BOOKING API :::::::::::::::::"+strResponse);
			if (response.getStatus() != 200) {
				LogCat.print("Response :: " + response.getStatus());
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("Failure");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jsonObj = new org.json.JSONObject(strResponse);
					if (jsonObj != null) {
					org.json.JSONArray customList = jsonObj.getJSONArray("customFields");
					resp.setSuccess(true);
					resp.setCode("S00");
					resp.setJsonArray(customList);
					} 
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("Failure");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}


	@Override
	public MeraEventsResponse attendeeForm(MeraEventsCommonRequest request) {
		MeraEventsResponse resp = new MeraEventsResponse();
		try {
			MultivaluedMapImpl formData = new MultivaluedMapImpl();
			formData.add("orderId", request.getOrderId());
			Client client = Client.create();
			WebResource webResource = client.resource(APIUtils.BASE_URL+APIUtils.ATTENDEE_FORM
					+"?access_token="+request.getAccess_token());
			ClientResponse response = webResource.accept("application/json").post(ClientResponse.class,formData);
			String strResponse = response.getEntity(String.class);
			LogCat.print("Response : : " +strResponse);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("Failure");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jsonObj = new org.json.JSONObject(strResponse);
					System.out.println("????????????????"+strResponse);
					if (jsonObj != null) {
						JSONArray customFields = jsonObj.getJSONObject("response").getJSONArray("customFields");
					resp.setSuccess(true);
					resp.setCode("S00");
					resp.setStatus("");
					resp.setAllresponse(strResponse);
					resp.setJsonArray(customFields);
					} else {
						resp.setSuccess(false);
						resp.setCode("F00");
						resp.setMessage("Service unavailable");
						resp.setStatus("Failure");
						resp.setResponse(APIUtils.getFailedJSON().toString());
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("Failure");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}

	@Override
	public MeraEventsResponse saveAttendeeDetails(MeraEventsAttendeeFormRequest request) {
		MeraEventsResponse resp = new MeraEventsResponse();
		
		
		System.out.println("--------------INSIDE SAVE ATTENDEE API--------------");
		try {
			JSONObject payload = new JSONObject();
			JSONObject payloadData = new JSONObject();
			JSONObject payloadSubmit = new JSONObject();

			if(request.getQuantity().equals("1"))
			{
				payloadSubmit.put("FullName1",request.getFullName1());
				payloadSubmit.put("EmailId1",request.getEmailId1());
				payloadSubmit.put("MobileNo1",request.getMobileNo1());
				payloadData.put("submit", payloadSubmit);
				payloadData.put("event_id",request.getEventId());
				payload.put("data",payloadData);
				payload.put("intent","attendeeRegistration");
				System.err.println("error::"+payload.toString());
			}
			else if(request.getQuantity().equals("2"))
			{
				payloadSubmit.put("FullName1",request.getFullName1());
				payloadSubmit.put("MobileNo1",request.getMobileNo1());
				payloadSubmit.put("EmailId1",request.getEmailId1());
				payloadSubmit.put("FullName2",request.getFullName2());
				payloadSubmit.put("MobileNo2",request.getMobileNo2());
				payloadSubmit.put("EmailId2",request.getEmailId2());
				payloadData.put("submit", payloadSubmit);
				payloadData.put("event_id",request.getEventId());
				payload.put("data",payloadData);
				payload.put("intent","attendeeRegistration");
			}
			

			else if(request.getQuantity().equals("3"))
			{
				payloadSubmit.put("FullName1",request.getFullName1());
				payloadSubmit.put("MobileNo1",request.getMobileNo1());
				payloadSubmit.put("EmailId1",request.getEmailId1());
				payloadSubmit.put("FullName2",request.getFullName2());
				payloadSubmit.put("MobileNo2",request.getMobileNo2());
				payloadSubmit.put("EmailId2",request.getEmailId2());
				payloadSubmit.put("FullName3",request.getFullName3());
				payloadSubmit.put("MobileNo3",request.getMobileNo3());
				payloadSubmit.put("EmailId3",request.getEmailId3());
				payloadData.put("submit", payloadSubmit);
				payloadData.put("event_id",request.getEventId());
				payload.put("data",payloadData);
				payload.put("intent","attendeeRegistration");
			}
			else if(request.getQuantity().equals("4"))
			{
				payloadSubmit.put("FullName1",request.getFullName1());
				payloadSubmit.put("MobileNo1",request.getMobileNo1());
				payloadSubmit.put("EmailId1",request.getEmailId1());
				payloadSubmit.put("FullName2",request.getFullName2());
				payloadSubmit.put("MobileNo2",request.getMobileNo2());
				payloadSubmit.put("EmailId2",request.getEmailId2());
				payloadSubmit.put("FullName3",request.getFullName3());
				payloadSubmit.put("MobileNo3",request.getMobileNo3());
				payloadSubmit.put("EmailId3",request.getEmailId3());
				payloadSubmit.put("FullName4",request.getFullName4());
				payloadSubmit.put("MobileNo4",request.getMobileNo4());
				payloadSubmit.put("EmailId4",request.getEmailId4());
				payloadData.put("submit", payloadSubmit);
				payloadData.put("event_id",request.getEventId());
				payload.put("data",payloadData);
				payload.put("intent","attendeeRegistration");
			
			}
			System.err.println("PAYLOAD DATA INSIDE SAVE ATTENDE API"+payload.toString());
			Client client=Ssltest.createClient(); 
			WebResource webResource = client.resource("https://dev.appsfly.io/executor/exec");
			ClientResponse response = webResource.accept("application/json")
					.header("Content-Type", "application/json")
					.header("X-UUID", "generic")
					.header("X-Association-Key", "b98babec-e9cd-4227-8fc1-3ee28e07d2ab")
					.header("X-Product-Id", "58c7d3c3862dc10011305ade")
					.header("X-Module-Id", "58d39e6726d9720011a6ff20")
					.post(ClientResponse.class,payload.toString());
			String strResponse = response.getEntity(String.class);
			System.err.println("Response  from save Attendee API : : " +strResponse);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("Failure");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					JSONObject jsonObj = new JSONObject(strResponse);
					if (jsonObj != null) {
					resp.setSuccess(true);
					resp.setCode("S00");
					resp.setStatus("Success");
					resp.setEventSignUpId(jsonObj.getJSONObject("response").getString("eventSignupId"));
					resp.setTotalAmount(jsonObj.getJSONObject("response").getDouble("totalPurchaseAmount"));
					resp.setResponse(strResponse);
					} 
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("Failure");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
			} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}

	
	@Override
	public MeraEventsResponse saveEventDetails(MeraEventDetailsRequest request) {
		MeraEventsResponse resp = new MeraEventsResponse();
		System.err.println("---------------INSIDE SAVE EVENT DETAILS API--------------");
		JSONObject payload = new JSONObject();
		try {
			payload.put("sessionId", request.getSessionId());
			payload.put("orderId", request.getOrderId());
			payload.put("eventSignUpId", request.getEventSignUpId());
			payload.put("city", request.getCity());
			payload.put("state", request.getState());
			payload.put("country", request.getCountry());
			payload.put("category", request.getEventCategory());
			payload.put("eventId", request.getEventId());
			payload.put("eventName", request.getEventName());
			payload.put("eventType", request.getEventType());
			payload.put("startDate", request.getEventStartDate());
			payload.put("endDate", request.getEventEndDate());
			payload.put("eventVanue", request.getEventVanue());
			payload.put("noOfAttendees", request.getNoOfAttendees());
			payload.put("netAmount", request.getTotalAmount());
			payload.put("ticketId", request.getTicketId());
			payload.put("ticketType", request.getTicketType());
			payload.put("ticketName", request.getTicketName());
			payload.put("quantity", request.getQuantity());
			payload.put("price", ""+request.getPrice());
			logger.info("Payload: " +payload.toString());
			/*orderId :null
			eventSignUpId :799089
			city :Bengaluru
			state :Karnataka
			country :India
			category :Entertainment
			eventId :104241
			eventName :MEvent Reg Image
			eventVanue :Bengaluru
			noOfAttendees :0
			netAmount :105.9
			ticketId :0
			ticketType :null
			ticketName :null
			quantity :0
			price :0.0
			*/
			
			System.err.println("Payload INSIDE save event detail in app : " +payload.toString());
			logger.info("URL " + UrlMetadatas.saveEventDetailsURL(Version.VERSION_1, Role.USER, Device.WEBSITE,
					Language.ENGLISH));
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.saveEventDetailsURL(Version.VERSION_1, Role.USER,
					Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type(MediaType.APPLICATION_JSON)
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			System.err.println("STR RESPONSE::::FDFFDLWD"+strResponse);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					} else {
						resp.setSuccess(false);
						resp.setCode("F00");
						resp.setMessage("Service unavailable");
						resp.setStatus("Failure");
						resp.setResponse(APIUtils.getFailedJSON().toString());
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("Failure");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("Failure");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}
	
	@Override
	public MeraEventsResponse eventPayment(MeraEventsCommonRequest request) {
		MeraEventsResponse resp = new MeraEventsResponse();
		System.err.println("--------------INSIDE EVENT PAYMENT API-------------");
		System.err.println("TRANSACTION ID"+request.getTransaction_id());
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("orderId", request.getOrderId());
			payload.put("accessToken", request.getAccess_token());
			payload.put("eventSignUpId", request.getEventSignUpId());
			payload.put("netAmount", request.getNetAmount());
			payload.put("serviceCode", "EVNT");
			payload.put("status", request.getStatus());
			payload.put("transactionRefNo", request.getTransaction_id());
			logger.info("Payload: " +payload.toString());
			
			logger.info("URL " + UrlMetadatas.eventPaymentURL(Version.VERSION_1, Role.USER, Device.WEBSITE,
					Language.ENGLISH));
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.eventPaymentURL(Version.VERSION_1, Role.USER,
					Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			System.err.println("STR RESPONSE FROM PAYMENT API FROM APP "+strResponse);
			if (response.getStatus() != 200) {
				System.err.print(strResponse);
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					System.err.println("-------------------------------------------- RESPONSE AFTER PAYMENT IS DONE ----------------------------------------");
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					} 
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("Failure");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("Failure");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public MeraEventsResponse offlineBooking(MeraEventsAttendeeFormRequest request) {
		MeraEventsResponse resp = new MeraEventsResponse();
		try {
			System.err.println("----------------------- INSIDE OFFLINE BOOKING API -------------------------");
			MultivaluedMapImpl formData = new MultivaluedMapImpl();
			formData.add("orderId", request.getOrderId());
			//formData.add("isEmailEnable", request.isEmailEnable());
			//formData.add("isSmsEnable", request.isSmsEnable());
			//formData.add("transactionId", request.getTransactionId());
			Client client = Client.create();
			WebResource webResource = client.resource(APIUtils.BASE_URL+APIUtils.OFFLINE_BOOKING
					+"?access_token="+request.getAccess_token());
			ClientResponse response = webResource.accept("application/json").post(ClientResponse.class,formData);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("Failure");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					JSONObject jsonObj = new JSONObject(strResponse);
					System.err.println("-------------------------------RESPONSE FROM THE THIRD PARTY OFFLINE BOOKING API ---------------------------" +strResponse);
					if (jsonObj != null) {
						int statusCode = (int) jsonObj.get("statusCode");
						boolean bookingStatus = jsonObj.getBoolean("status");
						if(bookingStatus == true) {
							resp.setSuccess(true);
							resp.setCode("S00");
							resp.setStatus("Success");
							resp.setResponse(strResponse);
						}
					} else {
						resp.setSuccess(false);
						resp.setCode("F00");
						resp.setMessage("Service unavailable");
						resp.setStatus("Failure");
						resp.setResponse(APIUtils.getFailedJSON().toString());
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("Failure");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}

	@Override
	public MeraEventsResponse eventbook(MeraEventsCommonRequest request) {
		
		MeraEventsResponse resp = new MeraEventsResponse();
		JSONObject payload = new JSONObject();
		JSONObject payload1 = new JSONObject();
			
			try {
				payload.put("intent","bookTicket");
				payload1.put("transaction_id",request.getTransaction_id());
				payload.put("data",payload1);
				Client client=Ssltest.createClient(); 
			WebResource webResource = client.resource("https://dev.appsfly.io/executor/exec");
			ClientResponse response = webResource.accept("application/json")
					.header("Content-Type", "application/json")
					.header("X-UUID", "generic")
					.header("X-Association-Key", "b98babec-e9cd-4227-8fc1-3ee28e07d2ab")
					.header("X-Product-Id", "58c7d3c3862dc10011305ade")
					.header("X-Module-Id", "58d39e6726d9720011a6ff20")
					.post(ClientResponse.class,payload.toString());
			
			System.err.println("PAYLOAD  eventbook API ::++++++++========="+payload.toString());
			String strResponse = response.getEntity(String.class);
			System.err.println("EVENT BOOK API RESPONSE : : "+strResponse);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				//resp.setStatus(ResponseStatus.FAILURE);
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jsonObj = new org.json.JSONObject(strResponse);
					if (jsonObj != null) {
						String apiStatus = jsonObj.getString("status");					
						resp.setSuccess(true);
					resp.setCode("S00");
					//resp.setStatus(ResponseStatus.SUCCESS);
					resp.setResponse(apiStatus);
					} 
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					//resp.setStatus(ResponseStatus.FAILURE);
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}

	@Override
	public MeraEventsResponse initate(MeraEventAmountInitateRequest request) {
		MeraEventsResponse resp = new MeraEventsResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessioniId());
			payload.put("netAmount", request.getNetAmount());
			payload.put("serviceCode", "EVNT");
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.paymentInitateForMeraEventsURL(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.header("hash", SecurityUtils.getHash(payload.toString()))
					.post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			System.err.println("Response :: : " + strResponse);
			if (response.getStatus() != 200) {
				org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage(strResponse);
				//resp.setDetails(details);
				System.err.println("INITATE DETAIL ======+++"+resp.getDetails());
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						String txnId = jobj.getString("txnId");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							//org.json.JSONObject jobj = new org.json.JSONObject(strResponse);

							final String details = (String) jobj.get("details");
						     System.err.println("+++++++++++++++=======DETAILS:::::::::::======="+details);
						   resp.setDetails(details);
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setTxnId(txnId);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}
}
