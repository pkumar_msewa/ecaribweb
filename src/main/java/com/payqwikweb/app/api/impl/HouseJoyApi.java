package com.payqwikweb.app.api.impl;

import org.codehaus.jettison.json.JSONObject;

import com.payqwikweb.app.api.IHouseJoyApi;
import com.payqwikweb.app.metadatas.UrlMetadatas;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Language;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.Version;
import com.payqwikweb.app.model.response.HouseJoyResponse;
import com.payqwikweb.app.utils.SecurityUtils;
import com.payqwikweb.model.app.request.HouseJoyRequestDTO;
import com.payqwikweb.model.web.Status;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class HouseJoyApi implements IHouseJoyApi{

	@Override
	public HouseJoyResponse initializeHouseJoy(HouseJoyRequestDTO dto) {
		HouseJoyResponse resp = new HouseJoyResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("serviceName",dto.getServiceName());
			
			payload.put("mobileNo", dto.getMobileNo());
			
			payload.put("sessionId", dto.getSessionId());	
			
			payload.put("amount", dto.getAmount());
			payload.put("merchantUserName", dto.getMerchantUserName());
			
			System.out.println("\n \n payload of HouseJoy "+payload.toString());
			Client client = Client.create();
			
			WebResource webResource = client.resource(UrlMetadatas.getHouseJoyInitiatePayment(Version.VERSION_1, Role.USER,
					Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456"))
					.post(ClientResponse.class,payload);
			String strResponse = response.getEntity(String.class);
			System.err.println("str response for process ::::" + strResponse);
			if (response.getStatus() != 200) {
				resp.setStatus(Status.Failed.getValue());
				resp.setCode("F00");
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);

					final String code = (String) jobj.get("code");
					if (code.equalsIgnoreCase("S00")) {
						resp.setStatus(Status.Success.getValue());
						resp.setTransactionId(jobj.getString("txnId"));
						resp.setMessage(jobj.getString("message"));
						resp.setCode(code);
					}

					else {
						resp.setStatus(Status.Failed.getValue());
						resp.setCode("F00");
						resp.setMessage(jobj.getString("message"));
					}
				}else{
					resp.setStatus(Status.Failed.getValue());
					resp.setCode("F00");
					resp.setMessage("no response from server");
				}
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			resp.setStatus(Status.Failed.getValue());
			resp.setCode("F00");
		}
		return resp;
	}



	@Override
	public HouseJoyResponse successHouseJoy(HouseJoyRequestDTO dto) {
		HouseJoyResponse resp = new HouseJoyResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("jobId",dto.getJobId());
			payload.put("mobileNo", dto.getMobileNo());
			payload.put("merchantUserName", dto.getMerchantUserName());
			payload.put("code", "S00");
			payload.put("transactionRefNo", dto.getTransactionRefNo());
			payload.put("amount", dto.getAmount());
			payload.put("paymentOffline",dto.isPaymentOffline());
			payload.put("mobileNo", dto.getMobileNo());
			payload.put("hjtxnRefNo",dto.getHjtxnRefNo());
			payload.put("customerName", dto.getCustomerName());
			payload.put("address",dto.getAddress());
			payload.put("date", dto.getDate());
			payload.put("mobile", dto.getMobile());
			payload.put("time", dto.getTime());
			payload.put("serviceName",dto.getServiceName());	
			payload.put("amount", dto.getAmount());
			payload.put("sessionId", dto.getSessionId());
			
			System.out.println("inside success HouseJoy to app == "+payload.toString());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getHouseJoySuccessPayment(Version.VERSION_1, Role.USER,
					Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456"))
					.post(ClientResponse.class,payload);
			String strResponse = response.getEntity(String.class);
			System.err.println("str response for process ::::" + strResponse);
			if (response.getStatus() != 200) {
				resp.setStatus(Status.Failed.getValue());
				resp.setCode("F00");
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);

					final String code = (String) jobj.get("code");
					if (code.equalsIgnoreCase("S00")) {
						resp.setStatus(Status.Success.getValue());
						resp.setTransactionId(jobj.getString("txnId"));
						resp.setMessage(jobj.getString("message"));
						resp.setCode(code);
					}
					else {
						resp.setCode(code);
						resp.setStatus(Status.Failed.getValue());
						resp.setMessage(jobj.getString("message"));
					}
					
				} else {
					resp.setStatus(Status.Failed.getValue());
					resp.setCode("F00");
					resp.setMessage("service Unavailable....");
				}
				resp.setDetails(strResponse);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setStatus(Status.Failed.getValue());
			resp.setCode("F00");
		}
		return resp;
	}

	@Override
	public HouseJoyResponse cancelHouseJoy(HouseJoyRequestDTO dto) {
		HouseJoyResponse resp = new HouseJoyResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("transactionRefNo",dto.getTransactionRefNo());
			
			System.out.println("\n \n payload of HouseJoy: "+payload.toString());
			Client client = Client.create();
			
			WebResource webResource = client.resource(UrlMetadatas.getHouseJoyCancelPayment(Version.VERSION_1, Role.USER,
					Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456"))
					.post(ClientResponse.class,payload);
			String strResponse = response.getEntity(String.class);
			System.err.println("str response for process ::::" + strResponse);
			if (response.getStatus() != 200) {
				resp.setStatus(Status.Failed.getValue());
				resp.setCode("F00");
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);

					final String code = (String) jobj.get("code");
					if (code.equalsIgnoreCase("S00")) {
						resp.setStatus(Status.Success.getValue());
						resp.setTransactionId(jobj.getString("txnId"));
						resp.setMessage(jobj.getString("message"));
						resp.setCode(code);
					}

					else {
						resp.setStatus(Status.Failed.getValue());
						resp.setCode("F00");
						resp.setMessage(jobj.getString("message"));
					}
				}else{
					resp.setStatus(Status.Failed.getValue());
					resp.setCode("F00");
					resp.setMessage("no response from server");
				}
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			resp.setStatus(Status.Failed.getValue());
			resp.setCode("F00");
		}
		return resp;
	}

}
