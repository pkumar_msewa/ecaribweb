package com.payqwikweb.app.api.impl;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.codehaus.jettison.json.JSONObject;
import org.json.JSONArray;
import org.springframework.web.multipart.MultipartFile;

import com.payqwikweb.app.api.IAgentApi;
import com.payqwikweb.app.metadatas.UrlMetadatas;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Language;
import com.payqwikweb.app.model.ResponseStatus;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.Version;
import com.payqwikweb.app.model.request.ChangePasswordRequest;
import com.payqwikweb.app.model.request.ChangePasswordWithOtpRequest;
import com.payqwikweb.app.model.request.EditProfileRequest;
import com.payqwikweb.app.model.request.ForgetPasswordUserRequest;
import com.payqwikweb.app.model.request.LoginRequest;
import com.payqwikweb.app.model.request.MobileOTPRequest;
import com.payqwikweb.app.model.request.ReceiptsRequest;
import com.payqwikweb.app.model.request.RegistrationRequest;
import com.payqwikweb.app.model.request.ResendMobileOTPRequest;
import com.payqwikweb.app.model.request.UploadPictureRequest;
import com.payqwikweb.app.model.request.UserDetailsRequest;
import com.payqwikweb.app.model.response.BanksDto;
import com.payqwikweb.app.model.response.ChangePasswordResponse;
import com.payqwikweb.app.model.response.ChangePasswordWithOtpResponse;
import com.payqwikweb.app.model.response.EditProfileResponse;
import com.payqwikweb.app.model.response.ForgetPasswordUserResponse;
import com.payqwikweb.app.model.response.LoginResponse;
import com.payqwikweb.app.model.response.MobileOTPResponse;
import com.payqwikweb.app.model.response.ReceiptsResponse;
import com.payqwikweb.app.model.response.RegistrationResponse;
import com.payqwikweb.app.model.response.ResendMobileOTPResponse;
import com.payqwikweb.app.model.response.UploadPictureResponse;
import com.payqwikweb.app.model.response.UserDetailsResponse;
import com.payqwikweb.app.utils.SecurityUtils;
import com.payqwikweb.model.app.request.AgentRegistrationRequest;
import com.payqwikweb.model.web.Status;
import com.payqwikweb.model.web.UserType;
import com.payqwikweb.util.APIUtils;
import com.payqwikweb.util.LogCat;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;




public class AgentApi implements IAgentApi {

	private Object nullObject = null;

	@Override
	public EditProfileResponse editProfile(EditProfileRequest request, Role role) {
		EditProfileResponse resp = new EditProfileResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("address", request.getAddress());
			payload.put("firstName", request.getFirstName());
			payload.put("lastName", request.getLastName());
			payload.put("email", request.getEmail());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(
					UrlMetadatas.getEditProfileUrl(Version.VERSION_1, role, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						 String details=null;
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
							 details = (String) jobj.get("details");
							 resp.setCode(code);
						} else {
							resp.setSuccess(false);
							org.json.JSONObject detail= jobj.getJSONObject(details);
							resp.setCode(code);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setDetails(details);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setDetails(" ");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setDetails(" ");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public UploadPictureResponse uploadPicture(UploadPictureRequest request, Role role) {
		UploadPictureResponse resp = new UploadPictureResponse();
		try {
			MultipartFile imageFile = request.getProfilePicture();
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("encodedBytes", Base64.getEncoder().encodeToString(imageFile.getBytes()));
			payload.put("contentType", imageFile.getContentType());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getUploadPictureUrl(Version.VERSION_1, role, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setDetails(" ");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setDetails(" ");
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setDetails(" ");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setDetails(" ");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public ChangePasswordResponse changePassword(ChangePasswordRequest request, Role role) {
		ChangePasswordResponse resp = new ChangePasswordResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("password", request.getPassword());
			payload.put("username", request.getUsername());
			payload.put("newPassword", request.getNewPassword());
			payload.put("confirmPassword", request.getConfirmPassword());
			payload.put("key", request.getUsername());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getUpdatePasswordUrl(Version.VERSION_1, role, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				LogCat.print("Response :: " + response.getStatus());
				String strResponse = response.getEntity(String.class);
				LogCat.print("ChangePassword Response :: " + strResponse);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					} else {
						resp.setSuccess(false);
						resp.setCode("F00");
						resp.setMessage("Service unavailable");
						resp.setStatus("FAILED");
						resp.setResponse(APIUtils.getFailedJSON().toString());
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public ReceiptsResponse getReceipts(ReceiptsRequest request, Role role) {
		// TODO Auto-generated method stub
		ReceiptsResponse resp = new ReceiptsResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("page", request.getPage());
			payload.put("size", request.getSize());
			payload.put("userStatus", "");
			Client client = Client.create();
			WebResource webResource = client
					.resource(UrlMetadatas.getReceiptsUrl(Version.VERSION_1, role, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				System.err.println("response ::" + strResponse);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							final JSONArray receiptsArray = jobj.getJSONObject("details").getJSONArray("content");
							resp.setAdditionalInfo(receiptsArray);
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					} else {
						resp.setSuccess(false);
						resp.setCode("F00");
						resp.setMessage("Service unavailable");
						resp.setStatus("FAILED");
						resp.setResponse(APIUtils.getFailedJSON().toString());
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public LoginResponse login(LoginRequest request, Role role) {
		// TODO Auto-generated method stub
		LoginResponse resp = new LoginResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("username", "A" + request.getUsername());
			payload.put("password", request.getPassword());
			payload.put("ipAddress", request.getIpAddress());
			payload.put("registrationId", request.getRegistrationId());
			payload.put("validate", request.isValidate());
			payload.put("mobileToken", request.getMobileToken());
			Client client = Client.create();
			WebResource webResource = client
					.resource(UrlMetadatas.getLoginUrl(Version.VERSION_1, role, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						// final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							final double balance = (double) jobj.getJSONObject("details").getJSONObject("accountDetail")
									.get("balance");
							final long accountNumber = (long) jobj.getJSONObject("details")
									.getJSONObject("accountDetail").get("accountNumber");
							final String accountTypeDescription = (String) jobj.getJSONObject("details")
									.getJSONObject("accountDetail").getJSONObject("accountType").get("description");
							final String accountTypeName = (String) jobj.getJSONObject("details")
									.getJSONObject("accountDetail").getJSONObject("accountType").get("name");
							final double accountTypeMonthlyLimit = (double) jobj.getJSONObject("details")
									.getJSONObject("accountDetail").getJSONObject("accountType").get("monthlyLimit");
							final double accountTypeDailyLimit = (double) jobj.getJSONObject("details")
									.getJSONObject("accountDetail").getJSONObject("accountType").get("dailyLimit");
							final double accountTypeBalanceLimit = (double) jobj.getJSONObject("details")
									.getJSONObject("accountDetail").getJSONObject("accountType").get("balanceLimit");
							final String username = (String) jobj.getJSONObject("details").getJSONObject("userDetail")
									.get("username");
							final String firstName = (String) jobj.getJSONObject("details").getJSONObject("userDetail")
									.get("firstName");
							final String middleName = (String) jobj.getJSONObject("details").getJSONObject("userDetail")
									.get("middleName");
							final String lastName = (String) jobj.getJSONObject("details").getJSONObject("userDetail")
									.get("lastName");
							final String address = (String) jobj.getJSONObject("details").getJSONObject("userDetail")
									.get("address");
							final String contactNo = (String) jobj.getJSONObject("details").getJSONObject("userDetail")
									.get("contactNo");
							final String userType = (String) jobj.getJSONObject("details").getJSONObject("userDetail")
									.get("userType");
							final String authority = (String) jobj.getJSONObject("details").getJSONObject("userDetail")
									.get("authority");
							final String emailStatus = (String) jobj.getJSONObject("details")
									.getJSONObject("userDetail").get("emailStatus");
							final String mobileStatus = (String) jobj.getJSONObject("details")
									.getJSONObject("userDetail").get("mobileStatus");
							final String email = (String) jobj.getJSONObject("details").getJSONObject("userDetail")
									.get("email");
							final String image = (String) jobj.getJSONObject("details").getJSONObject("userDetail")
									.get("image");
							final String sessionId = (String) jobj.getJSONObject("details").get("sessionId");
							final String totaluserregister = (String) jobj.getJSONObject("details").get("numberOfRegisterUser");
							final double totaldebitamount = (double) jobj.getJSONObject("details").get("totalDebitTransaction");

							resp.setTotalRegisterUser(""+totaluserregister);
							resp.setDebitAmount(totaldebitamount);
							resp.setAddress(address);
							resp.setAuthority(authority);
							resp.setContactNo(contactNo);
							resp.setEmail(email);
							resp.setEmailStatus(emailStatus);
							resp.setFirstName(firstName);
							resp.setImage(image);
							resp.setLastName(lastName);
							resp.setMiddleName(middleName);
							resp.setMobileStatus(mobileStatus);
							resp.setSessionId(sessionId);
							resp.setUserType(userType);
							resp.setCode(code);
							resp.setAccountType(accountTypeDescription);
							resp.setDailyTransaction(accountTypeDailyLimit);
							resp.setMonthlyTransaction(accountTypeMonthlyLimit);
							resp.setUserType(accountTypeName);
							resp.setBalance(balance);
							resp.setAccountNumber(accountNumber);
							resp.setSuccess(true);
							resp.setUsername(username);
							resp.setMessage(message);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						// resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					} else {
						resp.setSuccess(false);
						resp.setCode("F00");
						resp.setMessage("Service unavailable");
						resp.setStatus("FAILED");
						resp.setResponse(APIUtils.getFailedJSON().toString());
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;

	}

	@Override
	public RegistrationResponse SaveUserToAgent(RegistrationRequest request, Role role) {
		// TODO Auto-generated method stub
		RegistrationResponse response = new RegistrationResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("username", request.getContactNo());
			payload.put("password", request.getPassword());
			payload.put("confirmPassword", request.getConfirmPassword());
			payload.put("userType", UserType.User);
			payload.put("authority", "");
			payload.put("status", Status.Inactive);
			payload.put("address", "");
			payload.put("locationCode", request.getLocationCode());
			payload.put("contactNo", request.getContactNo());
			payload.put("firstName", request.getFirstName());
			payload.put("middleName", "");
			payload.put("lastName", request.getLastName());
			payload.put("locationCode", request.getLocationCode());
			payload.put("gender", request.getGender());
			payload.put("dateOfBirth", request.getDateOfBirth());
			payload.put("email", request.getEmail());
			payload.put("vbankCustomer", request.isVbankCustomer());
			payload.put("accountNumber", request.getAccountNumber());
			payload.put("secQuestionCode", request.getSecQuestionCode());
			payload.put("secAnswer", request.getSecAnswer());


			Client client = Client.create();
            client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client
					.resource(UrlMetadatas.addUserAgent(Version.VERSION_1, role, Device.WEBSITE, Language.ENGLISH));
			ClientResponse resp = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (resp.getStatus() != 200) {
				response.setSuccess(false);
				response.setCode("F00");
				response.setMessage("Service unavailable");
				response.setStatus("FAILED");
				response.setDetails(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = resp.getEntity(String.class);
				System.err.println("response ::" + strResponse);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							response.setSuccess(true);
							final String details = (String) jobj.get("details");
							response.setDetails(details);
						} else {
							response.setSuccess(false);
						}
						response.setCode(code);
						response.setStatus(status);
						response.setMessage(message);
					} else {
						response.setSuccess(false);
						response.setCode("F00");
						response.setMessage("Service unavailable");
						response.setStatus("FAILED");
						response.setDetails(APIUtils.getFailedJSON().toString());
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setSuccess(false);
			response.setCode("F00");
			response.setMessage("Service unavailable");
			response.setStatus("FAILED");
			response.setDetails(APIUtils.getFailedJSON().toString());
		}
		return response;
	}

	@Override
	public MobileOTPResponse SaveUsertoAgentMobileOTP(MobileOTPRequest request, Role role) {
		// TODO Auto-generated method stub
		MobileOTPResponse resp = new MobileOTPResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("mobileNumber", request.getMobileNumber());
			payload.put("key", request.getKey());

			System.err.println(payload.toString());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.agentUserMobileOtpVerification(Version.VERSION_1,
					Role.AGENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			System.err.println(strResponse);

			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {

				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						final Object details = (String) jobj.get("details");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setDetails(details);
						resp.setResponse(strResponse);
					} else {
						resp.setSuccess(false);
						resp.setCode("F00");
						resp.setMessage("Service unavailable");
						resp.setStatus("FAILED");
						resp.setResponse(APIUtils.getFailedJSON().toString());
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());

		}
		return resp;
	}

	public ForgetPasswordUserResponse forgetPasswordUserRequest(ForgetPasswordUserRequest request, Role role) {

		ForgetPasswordUserResponse resp = new ForgetPasswordUserResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", "");
			payload.put("username", request.getUsername());

			System.err.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@" + payload.toString());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getAgentPasswordOtpUrl(Version.VERSION_1, role, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);

			System.err.println("@@@@@@@@@@@@@@@@@@@" + strResponse);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {

				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						final Object details = (Object) jobj.get("details");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setDetails(details);
						resp.setResponse(strResponse);
					} else {
						resp.setSuccess(false);
						resp.setCode("F00");
						resp.setMessage("Service unavailable");
						resp.setDetails(null);
						resp.setStatus("FAILED");
						resp.setResponse(APIUtils.getFailedJSON().toString());
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public ChangePasswordWithOtpResponse changePasswordWithOtpRequest(ChangePasswordWithOtpRequest request, Role role) {
		ChangePasswordWithOtpResponse resp = new ChangePasswordWithOtpResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", "");
			payload.put("password", "");
			payload.put("username", request.getUsername());
			payload.put("newPassword", request.getNewPassword());
			payload.put("confirmPassword", request.getConfirmPassword());
			payload.put("key", request.getKey());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.resetAgentPasswordOtpUrl(Version.VERSION_1, role, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						final Object details = (Object) jobj.get("details");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setDetails(details);
						resp.setResponse(strResponse);
					} else {
						resp.setSuccess(false);
						resp.setCode("F00");
						resp.setMessage("Service unavailable");
						resp.setStatus("FAILED");
						resp.setResponse(APIUtils.getFailedJSON().toString());
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public ResendMobileOTPResponse resendUsertoAgentMobileOTP(ResendMobileOTPRequest request, Role role) {
		// TODO Auto-generated method stub
		ResendMobileOTPResponse resp = new ResendMobileOTPResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId",request.getSessionId());
			payload.put("mobileNumber", request.getMobileNumber());
			payload.put("key", "");
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getAgentResendMobileUrl(Version.VERSION_1, role, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);

			System.err.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%" + strResponse);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						final Object details = (Object) jobj.get("details");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
//						resp.setDetails(details);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public UserDetailsResponse getAgentDetails(UserDetailsRequest request, Role role) {
		// TODO Auto-generated method stub
		UserDetailsResponse resp = new UserDetailsResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getUserDetailsUrl(Version.VERSION_1, role, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
							final double balance = (double) jobj.getJSONObject("details").getJSONObject("accountDetail")
									.get("balance");
							final int points = (int) jobj.getJSONObject("details").getJSONObject("accountDetail")
									.get("points");
							final long accountNumber = (long) jobj.getJSONObject("details")
									.getJSONObject("accountDetail").get("accountNumber");
							final String accountTypeDescription = (String) jobj.getJSONObject("details")
									.getJSONObject("accountDetail").getJSONObject("accountType").get("description");
							final String accountTypeName = (String) jobj.getJSONObject("details")
									.getJSONObject("accountDetail").getJSONObject("accountType").get("name");
							final double accountTypeMonthlyLimit = (double) jobj.getJSONObject("details")
									.getJSONObject("accountDetail").getJSONObject("accountType").get("monthlyLimit");
							final double accountTypeDailyLimit = (double) jobj.getJSONObject("details")
									.getJSONObject("accountDetail").getJSONObject("accountType").get("dailyLimit");
							final double accountTypeBalanceLimit = (double) jobj.getJSONObject("details")
									.getJSONObject("accountDetail").getJSONObject("accountType").get("balanceLimit");
							final String username = (String) jobj.getJSONObject("details").getJSONObject("userDetail")
									.get("username");
							final String firstName = (String) jobj.getJSONObject("details").getJSONObject("userDetail")
									.get("firstName");
							final String middleName = (String) jobj.getJSONObject("details").getJSONObject("userDetail")
									.get("middleName");
							final String lastName = (String) jobj.getJSONObject("details").getJSONObject("userDetail")
									.get("lastName");
							final String address = (String) jobj.getJSONObject("details").getJSONObject("userDetail")
									.get("address");
							final String contactNo = (String) jobj.getJSONObject("details").getJSONObject("userDetail")
									.get("contactNo");
							final String userType = (String) jobj.getJSONObject("details").getJSONObject("userDetail")
									.get("userType");
							final String authority = (String) jobj.getJSONObject("details").getJSONObject("userDetail")
									.get("authority");
							final String emailStatus = (String) jobj.getJSONObject("details")
									.getJSONObject("userDetail").get("emailStatus");
							final String mobileStatus = (String) jobj.getJSONObject("details")
									.getJSONObject("userDetail").get("mobileStatus");
							final String email = (String) jobj.getJSONObject("details").getJSONObject("userDetail")
									.get("email");
							final String image = (String) jobj.getJSONObject("details").getJSONObject("userDetail")
									.get("image");
							final String imageContent = jobj.getJSONObject("details").getJSONObject("userDetail")
									.getString("encodedImage");

							final String totaluserregister = (String) jobj.getJSONObject("details").get("numberofregisteruser");

							final double totaldebitamount = (double) jobj.getJSONObject("details").get("totaldebittransaction");

							resp.setTotalRegisterUser(""+totaluserregister);
							resp.setDebitAmount(totaldebitamount);
							resp.setAddress(address);
							resp.setAuthority(authority);
							resp.setContactNo(contactNo);
							resp.setEmail(email);
							resp.setEmailStatus(emailStatus);
							resp.setFirstName(firstName);
							resp.setImage(image);
							resp.setLastName(lastName);
							resp.setMiddleName(middleName);
							resp.setMobileStatus(mobileStatus);
							resp.setUserType(userType);
							resp.setCode(code);
							resp.setAccountType(accountTypeDescription);
							resp.setDailyTransaction(accountTypeDailyLimit);
							resp.setMonthlyTransaction(accountTypeMonthlyLimit);
							resp.setUserType(accountTypeName);
							resp.setBalance(balance);
							resp.setPoints(points);
							resp.setAccountNumber(accountNumber);
							resp.setImageContent(imageContent);
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					} 
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public RegistrationResponse getBankList(String sessionId) {
		RegistrationResponse response = new RegistrationResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", sessionId);
			Client client = Client.create();

			WebResource webResource = client
					.resource(UrlMetadatas.getBankList(Version.VERSION_1, Role.AGENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse resp = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (resp.getStatus() != 200) {
				response.setSuccess(false);
				response.setCode(ResponseStatus.FAILURE.getValue());
				response.setMessage("Service unavailable");
				response.setStatus("FAILED");
				response.setDetails(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = resp.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							response.setSuccess(true);
							org.json.JSONArray jArr = jobj.getJSONArray("details");
							List<BanksDto>list = new ArrayList<>();
							for(int i=0;i<jArr.length();i++){
								BanksDto dto = new BanksDto();
								final String bankName = jArr.getJSONObject(i).getString("name");
								/*final String bankCode = jArr.getJSONObject(i).getString("code");
								dto.setBankCode(bankCode);*/
								dto.setBankName(bankName);
								list.add(dto);
							}
							response.setDetails2(list);
						} else {
							response.setSuccess(false);
						}
						response.setCode(code);
						response.setStatus(status);
						response.setMessage(message);
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			response.setSuccess(false);
			response.setCode(ResponseStatus.FAILURE.getValue());
			response.setMessage("Service unavailable");
			response.setStatus("FAILED");
			response.setDetails(APIUtils.getFailedJSON().toString());
		}
		return response;
	}

}
