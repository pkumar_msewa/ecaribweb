package com.payqwikweb.app.api.impl;

import com.payqwikweb.app.model.request.SendMoneyBankRequest;
import com.payqwikweb.app.model.response.SendMoneyBankResponse;
import com.payqwikweb.util.JSONParserUtil;
import com.payqwikweb.util.SecurityUtil;
import com.payqwikweb.util.StartupUtil;

import org.codehaus.jettison.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aadhar.model.AadharOTPValidateDTO;
import com.aadhar.model.AadharOTPValidateResponse;
import com.aadhar.util.AadharUtil;
import com.payqwikweb.app.api.ISendMoneyApi;
import com.payqwikweb.app.metadatas.Test;
import com.payqwikweb.app.metadatas.UrlMetadatas;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Language;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.Version;
import com.payqwikweb.app.model.request.ListStoreApiRequest;
import com.payqwikweb.app.model.request.OfflinePaymentRequest;
import com.payqwikweb.app.model.request.PayAtStoreRequest;
import com.payqwikweb.app.model.request.SendMoneyMobileRequest;
import com.payqwikweb.app.model.response.ListStoreApiResponse;
import com.payqwikweb.app.model.response.OfflinePaymentResponse;
import com.payqwikweb.app.model.response.PayAtStoreResponse;
import com.payqwikweb.app.model.response.SendMoneyMobileResponse;
import com.payqwikweb.app.utils.SecurityUtils;
import com.payqwikweb.model.app.request.SendMoneyDonateeRequest;
import com.payqwikweb.model.app.response.SendMoneyDonateeResponse;
import com.payqwikweb.util.APIUtils;
import com.payqwikweb.util.LogCat;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.LoggingFilter;

public class SendMoneyApi implements ISendMoneyApi {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public PayAtStoreResponse payAtStoreResponseRequest(PayAtStoreRequest request) {

		PayAtStoreResponse resp = new PayAtStoreResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("netAmount", request.getNetAmount());
			payload.put("id", request.getId());
//			payload.put("remarks", request.getRemarks());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getPayAtStoreUrl(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					} 
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("response is null");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;

	}

	@Override
	public SendMoneyMobileResponse sendMoneyMobileRequest(SendMoneyMobileRequest request) {
		SendMoneyMobileResponse resp = new SendMoneyMobileResponse();
		String message = "";
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("mobileNumber", request.getMobileNumber());
			payload.put("amount", request.getAmount());
			payload.put("message", request.getMessage());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(
					UrlMetadatas.getSendMoneyMobileUrl(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
							message = jobj.getString("message");
							resp.setMessage(message);
						} else if (code.equalsIgnoreCase("F04")) {
							resp.setSuccess(false);
							org.json.JSONObject detail = jobj.getJSONObject("details");
							String errorMessage = JSONParserUtil.getString(detail, "message");
							resp.setMessage(errorMessage);
						} else {
							message = jobj.getString("message");
							resp.setSuccess(false);
							resp.setMessage(message);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	// send money to donatee

	@Override
	public SendMoneyDonateeResponse sendMoneyToDonatee(SendMoneyDonateeRequest request) {
		SendMoneyDonateeResponse resp = new SendMoneyDonateeResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("donatee", request.getUsername());
			payload.put("amount", request.getAmount());
			payload.put("message", request.getMessage());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getSendMoneyDonatee(Version.VERSION_1, Role.USER, Device.ANDROID, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					} 
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public ListStoreApiResponse listStoreResponseRequest(ListStoreApiRequest request) {
		ListStoreApiResponse resp = new ListStoreApiResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getListStoreUrl(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					} 
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public SendMoneyBankResponse sendMoneyBankRequest(SendMoneyBankRequest request) {
		SendMoneyBankResponse response = new SendMoneyBankResponse();
		try {
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.initiateBankTransfer(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse clientResponse = webResource.accept("application/json")
					.header("hash", SecurityUtils.getHash(request.toString())).type("application/json")
					.post(ClientResponse.class, request.toJSON());
			String strResponse = clientResponse.getEntity(String.class);
			if (clientResponse.getStatus() != 200) {
				response.setCode("F00");
				response.setSuccess(false);
				response.setStatus("FAILED");
				response.setMessage("Response Status from server is ::" + clientResponse.getStatus());
			} else {
				if (strResponse != null) {
					org.json.JSONObject json = new org.json.JSONObject(strResponse);
					final String code = JSONParserUtil.getString(json, "code");
					final String message = JSONParserUtil.getString(json, "message");
					response.setCode(code);
					response.setMessage(message);
					if (code.equalsIgnoreCase("S00")) {
						response.setSuccess(true);
					} else {
						response.setSuccess(false);
					}
				} else {
					response.setSuccess(false);
					response.setCode("F00");
					response.setMessage("Service unavailable");
					response.setStatus("FAILED");
					response.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setSuccess(false);
			response.setCode("F00");
			response.setMessage("Service Unavailable");
			response.setStatus("FAILED");
			response.setResponse(APIUtils.getFailedJSON().toString());
		}
		return response;
	}

	@Override
	public OfflinePaymentResponse offlinePayment(OfflinePaymentRequest request) {
		OfflinePaymentResponse resp = new OfflinePaymentResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("mobileNumber", request.getMobileNumber());
			payload.put("amount", request.getAmount());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.offlinePaymentUrl(Version.VERSION_1, Role.MERCHANT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				LogCat.print("RESPONSE :: " + strResponse);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					} 
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public OfflinePaymentResponse verifyOTP(OfflinePaymentRequest request) {
		OfflinePaymentResponse resp = new OfflinePaymentResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("otp", request.getOtp());
			payload.put("amount", request.getAmount());
			payload.put("mobileNumber", request.getMobileNumber());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.offlinePaymentOTP(Version.VERSION_1, Role.MERCHANT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					} 
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public SendMoneyBankResponse sendMoneyToMBankRequest(SendMoneyBankRequest request) {
		SendMoneyBankResponse response = new SendMoneyBankResponse();
		try {
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.initiateMBankTransfer(Version.VERSION_1,
					Role.MERCHANT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse clientResponse = webResource.accept("application/json")
					.header("hash", SecurityUtils.getHash(request.toString())).type("application/json")
					.post(ClientResponse.class, request.toJSON());
			if (clientResponse.getStatus() != 200) {
				response.setCode("F00");
				response.setSuccess(false);
				response.setStatus("FAILED");
				response.setMessage("Response Status from server is ::" + clientResponse.getStatus());
			} else {
				String strResponse = clientResponse.getEntity(String.class);
				org.json.JSONObject json = new org.json.JSONObject(strResponse);
				final String code = JSONParserUtil.getString(json, "code");
				final String message = JSONParserUtil.getString(json, "message");
				response.setCode(code);
				response.setMessage(message);
				if (code.equalsIgnoreCase("S00")) {
					response.setSuccess(true);
				} else {
					response.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setSuccess(false);
			response.setCode("F00");
			response.setMessage("Service Unavailable");
			response.setStatus("FAILED");
			response.setResponse(APIUtils.getFailedJSON().toString());
		}
		return response;
	}

}
