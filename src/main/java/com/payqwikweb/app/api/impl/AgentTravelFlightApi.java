package com.payqwikweb.app.api.impl;

import java.net.Inet4Address;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringEscapeUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.payqwikweb.app.api.IATravelFlightApi;
import com.payqwikweb.app.api.ITravelFlightApi;
import com.payqwikweb.app.metadatas.UrlMetadatas;
import com.payqwikweb.app.model.Cabin;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.EngineID;
import com.payqwikweb.app.model.FlightPaymentGateWayDTO;
import com.payqwikweb.app.model.Language;
import com.payqwikweb.app.model.ResponseStatus;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.Version;
import com.payqwikweb.app.model.busdto.BusCityListDTO;
import com.payqwikweb.app.model.flight.dto.FlightTicketDTO;
import com.payqwikweb.app.model.flight.dto.FlightTicketMobileDTO;
import com.payqwikweb.app.model.flight.dto.TicketObj;
import com.payqwikweb.app.model.flight.dto.TravellerDetails;
import com.payqwikweb.app.model.flight.request.FligthBookReq;
import com.payqwikweb.app.model.flight.response.AirLineNames;
import com.payqwikweb.app.model.flight.response.Errors;
import com.payqwikweb.app.model.flight.response.FlightClityList;
import com.payqwikweb.app.model.flight.response.FlightPriceReCheckResponse;
import com.payqwikweb.app.model.flight.response.FlightResponseDTO;
import com.payqwikweb.app.model.flight.response.FlightSearchResponse;
import com.payqwikweb.app.model.flight.response.JourneyDetail;
import com.payqwikweb.app.model.flight.response.Journeys;
import com.payqwikweb.app.model.request.AirPriceCheckRequest;
import com.payqwikweb.app.model.request.Bonds;
import com.payqwikweb.app.model.request.BookFare;
import com.payqwikweb.app.model.request.BookingClass;
import com.payqwikweb.app.model.request.CabinClasses;
import com.payqwikweb.app.model.request.CreateJsonRequestFlight;
import com.payqwikweb.app.model.request.Fare;
import com.payqwikweb.app.model.request.FlightAvailability;
import com.payqwikweb.app.model.request.FlightSearchDetails;
import com.payqwikweb.app.model.request.Legs;
import com.payqwikweb.app.model.request.MobileFlightBookRequest;
import com.payqwikweb.app.model.request.PaxFares;
import com.payqwikweb.app.model.request.SSRDetails;
import com.payqwikweb.app.model.request.Segment;
import com.payqwikweb.app.model.request.TravelFlightRequest;
import com.payqwikweb.app.model.response.FlightPriceCheckRequest;
import com.payqwikweb.app.utils.SecurityUtils;
import com.payqwikweb.model.app.request.FlightBookRequest;
import com.payqwikweb.model.app.request.MobileRoundwayFlightBookRequest;
import com.payqwikweb.model.app.request.ReturnFlightBookRequest;
import com.payqwikweb.model.app.request.TravellerFlightDetails;
import com.payqwikweb.util.ConvertUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;

public class AgentTravelFlightApi implements IATravelFlightApi {

	private SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

	@Override
	public String getAirlineNames() {
		try {
			JSONObject payload = new JSONObject();
			payload.put("clientIp", Inet4Address.getLocalHost().getHostAddress());
			payload.put("cllientKey", UrlMetadatas.MDEX_CLIENTKEY);
			payload.put("clientToken", UrlMetadatas.MDEX_CLIENTTOKEN);
			payload.put("clientApiName", "Flight Name Request");
			// System.out.println(payload.toString());
			Client client = Client.create();
			// client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client
					.resource(UrlMetadatas.getAirlineNames(Role.ClIENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);

			String strResponse = response.getEntity(String.class);
			// System.out.println(strResponse);
			System.err.println("strResponse: "+strResponse);
			return "" + strResponse;
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
			return "";
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public String getFlightSearchAPI(TravelFlightRequest req) {

		try {
			org.json.simple.JSONArray flightSearchDetails = new org.json.simple.JSONArray();


			JSONObject flightSearchDetailsobj = new JSONObject();
			JSONObject roundtripDetailsobj = new JSONObject();

			JSONObject payload = new JSONObject();
			payload.put("clientIp", Inet4Address.getLocalHost().getHostAddress());
			payload.put("cllientKey", UrlMetadatas.MDEX_CLIENTKEY);
			payload.put("clientToken", UrlMetadatas.MDEX_CLIENTTOKEN);
			payload.put("clientApiName", "Flight Search");

			flightSearchDetailsobj.put("origin", req.getOrigin());
			flightSearchDetailsobj.put("destination", req.getDestination());
			flightSearchDetailsobj.put("beginDate", req.getBeginDate());

			flightSearchDetails.add(flightSearchDetailsobj);
			if (req.getTripType().equals("RoundTrip")) {
				roundtripDetailsobj.put("origin", req.getDestination());
				roundtripDetailsobj.put("destination", req.getOrigin());
				roundtripDetailsobj.put("beginDate", req.getEndDate());

				flightSearchDetails.add(roundtripDetailsobj);
			}

			ArrayList<String> objhasmap = new ArrayList<>();
			objhasmap.add("" + EngineID.Indigo);
			objhasmap.add("" + EngineID.AirAsia);
			objhasmap.add("" + EngineID.AirCosta);
			objhasmap.add("" + EngineID.GoAir);
			objhasmap.add("" + EngineID.Spicjet);
			objhasmap.add("" + EngineID.Trujet);
			objhasmap.add("" + EngineID.TravelPort);

			payload.put("engineIDs", objhasmap);

			payload.put("flightSearchDetails", flightSearchDetails);
			payload.put("tripType", req.getTripType());
			payload.put("cabin", req.getCabin());
			payload.put("adults", req.getAdults());
			payload.put("childs", req.getChilds());
			payload.put("infants", req.getInfants());
			payload.put("traceId", "AYTM00011111111110002");

			System.out.println(payload.toString());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client
					.resource(UrlMetadatas.getFlightSearchAPI(Role.ClIENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);

			String strResponse = response.getEntity(String.class);
			System.err.println(strResponse);
			return "" + strResponse;
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}
		return "";
	}


	@Override
	public String AirRePriceRQ(FlightPriceCheckRequest req) {
		try {
			JSONObject payload = new JSONObject();
			payload.put("clientIp", Inet4Address.getLocalHost().getHostAddress());
			payload.put("cllientKey", UrlMetadatas.MDEX_CLIENTKEY);
			payload.put("clientToken", UrlMetadatas.MDEX_CLIENTTOKEN);
			payload.put("clientApiName", "Flight Price Re-Check");
			payload.put("adults", req.getAdults());
			payload.put("cabin", Cabin.Economy);
			payload.put("childs", req.getChilds());
			// ArrayList<String> objhasmap = new ArrayList<>();
			// objhasmap.add("" + EngineID.Indigo);
			// objhasmap.add("" + EngineID.AirAsia);
			// objhasmap.add("" + EngineID.AirCosta);
			// objhasmap.add("" + EngineID.GoAir);
			// objhasmap.add("" + EngineID.Spicjet);
			// objhasmap.add("" + EngineID.Trujet);
			payload.put("engineID", req.getEngineID());
			payload.put("traceId", req.getTraceId());
			payload.put("origin", req.getOrigin());
			payload.put("destination", req.getDestination());
			payload.put("beginDate", req.getBeginDate());
			payload.put("infants", req.getInfants());
			payload.put("tripType", req.getTripType());
			payload.put("bondType", req.getBondType());
			payload.put("boundType", req.getBoundType());
			payload.put("isBaggageFare", false);
			payload.put("isSSR", false);
			payload.put("itineraryKey", req.getItineraryKey());
			payload.put("journeyTime", req.getJourneyTime());
			payload.put("airlineName", req.getAirlineName());
			payload.put("amount", Double.parseDouble(req.getAmount()));
			payload.put("arrivalDate", req.getArrivalDate());
			payload.put("arrivalTerminal", req.getArrivalTerminal());
			payload.put("arrivalTime", req.getArrivalTime());
			payload.put("baggageUnit", req.getBaggageUnit());
			payload.put("baggageWeight", req.getBaggageWeight());
			payload.put("capacity", req.getCapacity());
			payload.put("carrierCode", req.getCarrierCode());
			payload.put("currencyCode", req.getCurrencyCode());
			payload.put("departureDate", req.getDepartureDate());
			payload.put("departureTerminal", req.getDepartureTerminal());
			payload.put("departureTime", req.getDepartureTime());
			payload.put("duration", req.getDuration());
			payload.put("fareClassOfService", req.getFareClassOfService());
			payload.put("flightName", req.getFlightName());
			payload.put("flightNumber", req.getFlightNumber());
			payload.put("isConnecting", false);
			payload.put("sold", req.getSold());
			payload.put("status", req.getStatus());
			payload.put("basicFare", req.getBasicFare());
			payload.put("exchangeRate", req.getExchangeRate());
			payload.put("baseTransactionAmount", req.getBaseTransactionAmount());
			payload.put("cancelPenalty", req.getCancelPenalty());
			payload.put("changePenalty", req.getChangePenalty());
			payload.put("chargeCode", req.getChargeCode());
			payload.put("chargeType", req.getChargeType());
			payload.put("markUP", req.getMarkUP());
			payload.put("paxType", 0);
			payload.put("refundable", true);
			payload.put("totalFare", req.getTotalFare());
			payload.put("totalTax", req.getTotalTax());
			payload.put("transactionAmount", req.getTransactionAmount());
			payload.put("totalFareWithOutMarkUp", req.getTotalFareWithOutMarkUp());
			payload.put("totalTaxWithOutMarkUp", req.getTotalTaxWithOutMarkUp());
			payload.put("fareRule", req.getFareRule());
			payload.put("isCache", false);
			payload.put("isHoldBooking", false);
			payload.put("isInternational", false);
			payload.put("isRoundTrip", false);
			payload.put("isSpecial", true);
			payload.put("isSpecialId", false);
			payload.put("journeyIndex", req.getJourneyIndex());
			payload.put("nearByAirport", false);
			payload.put("searchId", req.getSearchId());
			System.out.println(payload.toString());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client
					.resource(UrlMetadatas.getAirRePriceRQAPI(Role.ClIENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);

			String strResponse = response.getEntity(String.class);
			System.out.println(strResponse);
			return "" + strResponse;
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e);
			e.printStackTrace();
		}
		return "";

	}

	@Override
	public String AirBookRQ(FlightBookRequest req, String source, HttpSession session) {
		try {

			JSONObject payload = CreateJsonRequestFlight.createjson(req, source, session);
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client
					.resource(UrlMetadatas.getAirBookRQ(Role.ClIENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);

			String strResponse = response.getEntity(String.class);
			System.out.println(strResponse);
			return "" + strResponse;
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e);

		}
		return "";
	}

	@Override
	public String AirBookRQreturn(ReturnFlightBookRequest req, String source, String destination, HttpSession session) {
		try {

			JSONObject payload = CreateJsonRequestFlight.createjsonreturn(req, source, destination, session);
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client
					.resource(UrlMetadatas.getAirBookRQ(Role.ClIENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);

			String strResponse = response.getEntity(String.class);
			System.out.println(strResponse);
			return "" + strResponse;
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}
		return "";
	}

	@Override
	public String AirBookRQForMobile(MobileFlightBookRequest req) {
		// TODO Auto-generated method stub
		try {

			JSONObject payload = CreateJsonRequestFlight.createjsonForMobile(req);
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client
					.resource(UrlMetadatas.getAirBookRQ(Role.ClIENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);

			String strResponse = response.getEntity(String.class);
			System.out.println(strResponse);
			return "" + strResponse;
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}
		return "";
	}

	@Override
	public String AirBookreturnRQForMobile(MobileRoundwayFlightBookRequest req) {
		// TODO Auto-generated method stub
		try {

			JSONObject payload = CreateJsonRequestFlight.createjsonForReturnMobile(req);
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client
					.resource(UrlMetadatas.getAirBookRQ(Role.ClIENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);

			String strResponse = response.getEntity(String.class);
			System.out.println(strResponse);
			return "" + strResponse;
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}
		return "";
	}

	@Override
	public String FlightBookingInitiate(String sessionId, String firstName, String paymentAmount, String ticketDetails,
			String paymentmethod) {
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", sessionId);
			payload.put("firstName", firstName);
			payload.put("ticketDetails", ticketDetails);
			payload.put("paymentAmount", paymentAmount);
			payload.put("paymentmethod", paymentmethod);

			System.out.println(payload.toString());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(
					UrlMetadatas.agentGetFlightInitiateURL(Version.VERSION_1, Role.AGENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);

			String strResponse = response.getEntity(String.class);
			System.err.println(strResponse);
			return "" + strResponse;
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
			return "";
		}
	}

	@Override
	public String FlightBookingSucess(String sessionId, String ticketNumber, String firstName, String bookingRefId,
			String transactionRefNomdex, String paymentAmount, String ticketDetails, String statusflight,
			String paymentstatus, String paymentmethod, String transactionRefno, boolean isSuccess,String email,String mobile) {
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", sessionId);
			payload.put("ticketNumber", ticketNumber);
			payload.put("firstName", firstName);
			payload.put("bookingRefId", bookingRefId);
			payload.put("transactionRefNomdex", transactionRefNomdex);
			payload.put("paymentAmount", paymentAmount);
			payload.put("ticketDetails", ticketDetails);
			payload.put("statusflight", statusflight);
			payload.put("paymentstatus", paymentstatus);
			payload.put("paymentmethod", paymentmethod);
			payload.put("transactionRefno", transactionRefno);
			payload.put("success", isSuccess);
			payload.put("email", email);
			payload.put("mobile", mobile);
			// System.out.println(payload.toString());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(
					UrlMetadatas.agentGetFlightSucessURL(Version.VERSION_1, Role.AGENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);

			String strResponse = response.getEntity(String.class);
			// System.out.println(strResponse);
			return "" + strResponse;
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
			return "";
		}
	}

	@Override
	public String FlightPaymentGatewaySucess(String sessionId, String ticketNumber, String firstName,
			String bookingRefId, String transactionRefNomdex, String paymentAmount, String ticketDetails,
			String statusflight, String paymentstatus, String paymentmethod, String transactionRefno, boolean success,String email,String mobile) {
		try {
			JSONObject payload = new JSONObject();

			payload.put("sessionId", sessionId);
			payload.put("ticketNumber", ticketNumber);
			payload.put("firstName", firstName);
			payload.put("bookingRefId", bookingRefId);
			payload.put("transactionRefNomdex", transactionRefNomdex);
			payload.put("paymentAmount", paymentAmount);
			payload.put("ticketDetails", ticketDetails);
			payload.put("statusflight", statusflight);
			payload.put("paymentstatus", paymentstatus);
			payload.put("paymentmethod", paymentmethod);
			payload.put("transactionRefno", transactionRefno);
			payload.put("success", success);
			payload.put("email", email);
			payload.put("mobile", mobile);

			System.err.println("TxnRefNo:: "+transactionRefno);

			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(
					UrlMetadatas.agentGetPayMentGateWayUrl(Version.VERSION_1, Role.AGENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);

			String strResponse = response.getEntity(String.class);
			// System.out.println(strResponse);
			return "" + strResponse;
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
			return "";
		}
	}

	@Override
	public String AirBookRQwithPaymentGatway(FlightBookRequest req, String source, HttpSession session) {

		System.err.println("first name:: "+req.getFirstName());
		JSONObject payload = CreateJsonRequestFlight.createjson(req, source, session);

		System.err.println("Split Json: "+payload);

		return "" + payload;
	}

	@Override
	public String FlightBookPaymentGatway(String json) {
		try {

			JSONObject payload = new JSONObject(json);
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client
					.resource(UrlMetadatas.getAirBookRQ(Role.ClIENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);

			String strResponse = response.getEntity(String.class);
			System.out.println(strResponse);
			return "" + strResponse;
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e);
		}
		return null;
	}

	@Override
	public String AirBookRQreturnwithPaymentGatway(ReturnFlightBookRequest req, String source, String destination,
			HttpSession session) {
		JSONObject payload = CreateJsonRequestFlight.createjsonreturn(req, source, destination, session);
		return "" + payload;
	}

	@Override
	public String AirRePriceRQConnecting(FlightPriceCheckRequest req, String source, String destination,
			HttpSession session) {
		try {


			JSONObject payload = new JSONObject();
			JSONArray flightSearchDetailsArr=new JSONArray();
			JSONObject flightSearchDetailsObj=new JSONObject();

			FlightAvailability flightAvailability = new FlightAvailability();

			FlightSearchDetails flightSearchDetails = new FlightSearchDetails();

			flightSearchDetails.setBeginDate("" + session.getAttribute("beginDate"));
			flightSearchDetails.setDestination("" + session.getAttribute("destination"));
			flightSearchDetails.setEndDate("" + session.getAttribute("endDate"));
			flightSearchDetails.setOrigin("" + session.getAttribute("source"));

			JSONObject obj = new JSONObject(source);

			System.err.println("JSon: "+obj.toString());

			JSONArray journeys = obj.getJSONObject("details").getJSONArray("journeys");
			JSONArray segmentsArr=null;
			if (req.getOnewayflight().equals("Firstfligt")) {

				segmentsArr = journeys.getJSONObject(0).getJSONArray("segments");
				flightSearchDetailsObj.put("beginDate", "" + session.getAttribute("beginDate"));
				flightSearchDetailsObj.put("origin", ""+ session.getAttribute("source"));
				flightSearchDetailsObj.put("destination", ""+ session.getAttribute("destination"));
			}
			else if (req.getOnewayflight().equals("Secondflight")) {
				segmentsArr = journeys.getJSONObject(1).getJSONArray("segments");
				flightSearchDetailsObj.put("beginDate", ""+ session.getAttribute("endDate"));
				flightSearchDetailsObj.put("destination", ""+ session.getAttribute("source"));
				flightSearchDetailsObj.put("origin", ""+ session.getAttribute("destination"));
			}

			flightSearchDetailsArr.put(flightSearchDetailsObj);

			JSONObject segmentsObj=segmentsArr.getJSONObject(Integer.parseInt(req.getColNo())-1);
			System.err.println("segmentsObj:: "+(Integer.parseInt(req.getColNo())-1));
			segmentsObj.getString("engineID");

			ArrayList<String> engineIds = new ArrayList<>();
			engineIds.add(req.getEngineID());

			flightAvailability.setEngineIDs(engineIds);
			flightAvailability.setCabin(req.getCabin());
			flightAvailability.setTripType(""+session.getAttribute("triptype"));
			flightAvailability.setAdults(""+session.getAttribute("adults"));
			flightAvailability.setChilds("" + session.getAttribute("childs"));
			flightAvailability.setInfants("" + session.getAttribute("infants"));
			flightAvailability.setTraceId("AYTM00011111111110001");
			ArrayList<FlightSearchDetails> flightSearchArray = new ArrayList<>();
			flightSearchArray.add(flightSearchDetails);
			flightAvailability.setFlightSearchDetails(flightSearchArray);


			JSONArray engineIDsArr=new JSONArray();
			engineIDsArr.put(segmentsObj.getString("engineID"));

			//flightSearchDetailsObj.put("endDate", ""+ session.getAttribute("endDate"));

			JSONObject flightAvailabilityObj=new JSONObject();
			flightAvailabilityObj.put("traceId", "AYTM00011111111110001");
			flightAvailabilityObj.put("adults", ""+session.getAttribute("adults"));
			flightAvailabilityObj.put("childs", ""+session.getAttribute("childs"));
			flightAvailabilityObj.put("infants", ""+session.getAttribute("infants"));
			flightAvailabilityObj.put("cabin", req.getCabin());
			flightAvailabilityObj.put("engineIDs", engineIDsArr);
			flightAvailabilityObj.put("flightSearchDetails", flightSearchDetailsArr);
			flightAvailabilityObj.put("tripType", ""+session.getAttribute("triptype"));

			JSONArray segments=new JSONArray();

			JSONArray bondsArr=segmentsObj.getJSONArray("bonds");

			if (bondsArr.length()==2) {

				JSONObject flightSearchDetailsreturnObj=new JSONObject();
				flightSearchDetailsreturnObj.put("beginDate", ""+ session.getAttribute("endDate"));
				flightSearchDetailsreturnObj.put("destination", ""+ session.getAttribute("source"));
				flightSearchDetailsreturnObj.put("origin", ""+ session.getAttribute("destination"));
				flightSearchDetailsArr.put(flightSearchDetailsreturnObj);
			}


			for (int i = 0; i < bondsArr.length(); i++) {
				JSONArray legsArr=bondsArr.getJSONObject(i).getJSONArray("legs");

				for (int j = 0; j < legsArr.length(); j++) {
					JSONObject legsobj=legsArr.getJSONObject(j);
					legsobj.remove("cabinClasses");
					legsobj.remove("ssrDetails");
					legsobj.remove("isConnecting");
					legsobj.put("boundTypes", legsobj.getString("boundType"));
					legsobj.remove("boundType");
					legsobj.remove("amount");
					legsobj.remove("remarks");
					legsobj.remove("numberOfStops");
				}
				JSONObject bondsObj=bondsArr.getJSONObject(i);
				System.err.println(bondsObj.getString("specialServices"));

				bondsObj.put("baggageFare", bondsObj.getBoolean("isBaggageFare"));
				bondsObj.put("ssrFare", bondsObj.getBoolean("isSSR"));

				bondsObj.remove("specialServices");
				bondsObj.remove("isBaggageFare");
				bondsObj.remove("isSSR");
			}

			JSONArray faresArr=segmentsObj.getJSONArray("fare");

			for (int i = 0; i < faresArr.length(); i++) {
				JSONArray paxFares=faresArr.getJSONObject(i).getJSONArray("paxFares");
				for (int j = 0; j < paxFares.length(); j++) {
					JSONObject paxFaresobj=paxFares.getJSONObject(j);

					paxFaresobj.put("bookFares", paxFaresobj.getJSONArray("fares"));
					paxFaresobj.remove("fares");

				}
			}

			segmentsObj.put("fares", faresArr);

			segmentsObj.remove("fare");
			segmentsObj.remove("bondType");
			segmentsObj.remove("remark");
			segmentsObj.remove("deeplink");

			segmentsObj.put("baggageFare", segmentsObj.getString("isBaggageFare"));
			segmentsObj.put("cache", segmentsObj.getString("isCache"));
			segmentsObj.put("holdBooking", segmentsObj.getString("isHoldBooking"));
			segmentsObj.put("international", segmentsObj.getString("isInternational"));
			segmentsObj.put("roundTrip", segmentsObj.getString("isRoundTrip"));
			segmentsObj.put("special", segmentsObj.getString("isSpecial"));
			segmentsObj.put("specialId", segmentsObj.getString("isSpecialId"));

			segmentsObj.remove("isBaggageFare");
			segmentsObj.remove("isCache");
			segmentsObj.remove("isHoldBooking");
			segmentsObj.remove("isInternational");
			segmentsObj.remove("isRoundTrip");
			segmentsObj.remove("isSpecial");
			segmentsObj.remove("isSpecialId");


			segments.put(segmentsObj);

			payload.put("clientIp", "192.170.1.116");
			payload.put("cllientKey", UrlMetadatas.MDEX_CLIENTKEY);
			payload.put("clientToken", UrlMetadatas.MDEX_CLIENTTOKEN);
			payload.put("clientApiName", "Flight Price Re-Check");
			payload.put("segments", segments);
			payload.put("flightAvailability", flightAvailabilityObj);


			System.err.println("Payload:: "+ payload.toString());


			//		##############################################	//


			//JSONObject payload = CreateJsonRequestFlight.createjsonpricerecheckApi(req, source, destination, session);

			System.out.println(payload.toString());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client
					.resource(UrlMetadatas.getAirRePriceRQAPIConnecting(Role.ClIENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);

			String strResponse = response.getEntity(String.class);
			System.err.println(strResponse);
			return "" + strResponse;
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}
		return "";

	}

	@Override
	public String mobileAirRePriceRQConnecting(FlightPriceCheckRequest req) {
		try {

			JSONObject payload = new JSONObject();

			FlightAvailability flightAvailability = new FlightAvailability();

			FlightSearchDetails flightSearchDetails = new FlightSearchDetails();

			flightSearchDetails.setBeginDate(req.getBeginDate());
			flightSearchDetails.setDestination(req.getDestination());
			flightSearchDetails.setEndDate(req.getEndDate());
			flightSearchDetails.setOrigin(req.getOrigin());

			ArrayList<String> engineIds = new ArrayList<>();
			engineIds.add(req.getEngineID());

			flightAvailability.setEngineIDs(engineIds);
			flightAvailability.setCabin(req.getCabin());
			flightAvailability.setTripType(req.getTripType());
			flightAvailability.setAdults(req.getAdults());
			flightAvailability.setChilds(req.getChilds());
			flightAvailability.setInfants(req.getInfants());
			flightAvailability.setTraceId(req.getTraceId());
			ArrayList<FlightSearchDetails> flightSearchArray = new ArrayList<>();
			flightSearchArray.add(flightSearchDetails);
			flightAvailability.setFlightSearchDetails(flightSearchArray);

			Segment segment = new Segment();

			//segment.setBondType(req.getBondType());
			segment.setBaggageFare(req.isBaggageFare());
			segment.setDeeplink("");
			segment.setEngineID(req.getEngineID());
			segment.setFares(req.getFares());
			segment.setFareIndicator("0");
			segment.setFareRule(req.getFareRule());
			segment.setCache(req.isCache());
			segment.setHoldBooking(req.isHoldBooking());
			segment.setInternational(req.isInternational());
			segment.setRoundTrip(req.isRoundTrip());
			segment.setSpecial(req.isSpecial());
			segment.setSpecialId(req.isSpecialId());
			segment.setItineraryKey(req.getItineraryKey());
			segment.setMemoryCreationTime(new Date().toString());
			segment.setJourneyIndex(Integer.parseInt(req.getJourneyIndex()));
			segment.setNearByAirport(req.isNearByAirport());
			segment.setRemark("");
			segment.setSearchId(req.getSearchId());

			/*
			 * Bonds bonds=new Bonds();
			 * 
			 * bonds.setBaggageFare(req.isBaggageFare());
			 * bonds.setSsrFare(false);
			 * bonds.setItineraryKey(req.getItineraryKey());
			 * bonds.setJourneyTime(req.getJourneyTime());
			 * bonds.setLegs(req.getLegs());
			 */

			segment.setBonds(req.getBonds());

			ArrayList<Segment> segments = new ArrayList<>();

			segments.add(segment);
			AirPriceCheckRequest airPrice = new AirPriceCheckRequest();

			airPrice.setClientApiName("Flight Search");
			airPrice.setClientIp(Inet4Address.getLocalHost().getHostAddress());
			airPrice.setClientToken(UrlMetadatas.MDEX_CLIENTTOKEN);
			airPrice.setCllientKey(UrlMetadatas.MDEX_CLIENTKEY);
			airPrice.setFlightAvailability(flightAvailability);
			airPrice.setSegments(segments);

			// String jsonObject=airPrice.getJsonRequest();

			// System.err.println(jsonObject);

			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client
					.resource(UrlMetadatas.getAirRePriceRQAPIConnecting(Role.ClIENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString()))
					.post(ClientResponse.class, airPrice.getJsonRequest());

			String strResponse = response.getEntity(String.class);
			System.out.println(strResponse);
			return "" + strResponse;
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e);

		}
		return "";
	}

	@Override
	public String AirRePriceRQConnectinginternational(org.json.JSONObject payload1) {
		// TODO Auto-generated method stub
		try {

			JSONObject payload = new JSONObject(payload1.toString());
			System.err.println(payload);
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client
					.resource(UrlMetadatas.getAirRePriceRQAPIConnecting(Role.ClIENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);

			String strResponse = response.getEntity(String.class);
			System.out.println(strResponse);
			return "" + strResponse;
		} catch (Exception e) {
			System.out.println(e);
			//
		}
		return "";
	}

	@Override
	public String AirBookRQForMobileConecting(com.payqwikweb.app.model.request.AirBookRQ req) {
		try {

			String payload = req.getJsonRequest();
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client
					.resource(UrlMetadatas.getAirBookRQ(Role.ClIENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);

			String strResponse = response.getEntity(String.class);
			System.err.println(strResponse);
			return "" + strResponse;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}


	@Override
	public String AirBookRQForPaymentGateWayMobile(FlightPaymentGateWayDTO req) {
		try{

			JSONObject payload = CreateJsonRequestFlight.createjsonForPaymentGateWayMobile(req);
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client
					.resource(UrlMetadatas.getAirBookRQ(Role.ClIENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);

			String strResponse = response.getEntity(String.class);
			System.out.println(strResponse);
			return "" + strResponse;
		} catch (Exception e) {
			System.out.println(e);
			//
		}
		return "";


	}


	@Override
	public String AirBookRQForPaymentGateWayMobileConecting(FlightPaymentGateWayDTO req) {

		try {

			String payload = req.getJsonRequest();
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client
					.resource(UrlMetadatas.getAirBookRQ(Role.ClIENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);

			String strResponse = response.getEntity(String.class);
			System.out.println(strResponse);
			return "" + strResponse;
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e);
			//
		}
		return "";
	}


	@Override
	public String AirBookreturnRQForPaymentGateWayMobile(FlightPaymentGateWayDTO req) {

		try {

			JSONObject payload = CreateJsonRequestFlight.createjsonForPaymentGateWayReturnMobile(req);
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client
					.resource(UrlMetadatas.getAirBookRQ(Role.ClIENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);

			String strResponse = response.getEntity(String.class);
			System.out.println(strResponse);
			return "" + strResponse;
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}
		return "";
	}

	@Override
	public String getTxnRefNo(String sessionId) {

		JSONObject payload = new JSONObject();
		try{
			payload.put("sessionId", sessionId);

			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(
					UrlMetadatas.agentGetFlightSucessURL(Version.VERSION_1, Role.AGENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);

			String strResponse = response.getEntity(String.class);
			// System.out.println(strResponse);
			return "" + strResponse;
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
			return "";
		}
	}





	/* Code Done By Rohit End */


	/*##############################################################################################################*/

	/* Fresh Code Started By Subir */



	@Override
	public FlightClityList getAirLineNames(String sessionId) {
		FlightClityList resp=new FlightClityList();
		try {

			JSONObject payload = new JSONObject();
			payload.put("sessionId", sessionId);
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.aGetAirLineNamesFromDB(Version.VERSION_1, Role.AGENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", "12345").post(ClientResponse.class, payload);

			String strResponse = response.getEntity(String.class);
			//System.err.println("strResponse: "+strResponse);
			//JSONObject jobj=new JSONObject(strResponse);
			ObjectMapper mapper = new ObjectMapper();
			resp = mapper.readValue(strResponse, FlightClityList.class);
			//final String code=jobj.getString("code");
			//final String status=jobj.getString("status");
			//final String message=jobj.getString("message");
					
			//if (code != null && code.equalsIgnoreCase("S00")) {
			//String strDetails=jobj.getString("details");
			//if (strDetails!=null) {
			//List<AirLineNames> airports = new ArrayList<>();
			//JSONArray cList=jobj.getJSONArray("details");
			//for (int i = 0; i < cList.length(); i++) {
			//JSONObject temp = cList.getJSONObject(i);
			//airports.add(new AirLineNames(temp.getString("cityCode"), temp.getString("cityName"), temp.getString("airportName")));
			//}
						
			//resp.setDetails(airports);
			
			//}
			//resp.setCode(code);
			//resp.setMessage(message);
			//resp.setStatus(status);
			//}else{
			//resp.setCode(code);
			//resp.setMessage(message);
			//resp.setStatus(status);
			//}

		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			resp.setMessage("Service Unavailable");
			resp.setDetails(null);
			resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
		}
		return resp;
	}


	@SuppressWarnings("unchecked")
	@Override
	public FlightResponseDTO searchFlight(TravelFlightRequest req) {

		FlightResponseDTO result=new FlightResponseDTO();

		try {
			
			boolean val=compareCountry(req);
			
			org.json.simple.JSONArray flightSearchDetails = new org.json.simple.JSONArray();

			org.json.JSONArray engineIDs = new org.json.JSONArray();

			JSONObject flightSearchDetailsobj = new JSONObject();
			JSONObject roundtripDetailsobj = new JSONObject();

			JSONObject engineIDsobj = new JSONObject();
			JSONObject payload = new JSONObject();
			payload.put("clientIp", Inet4Address.getLocalHost().getHostAddress());
			payload.put("cllientKey", UrlMetadatas.MDEX_CLIENTKEY);
			payload.put("clientToken", UrlMetadatas.MDEX_CLIENTTOKEN);
			payload.put("clientApiName", "Flight Search");

			flightSearchDetailsobj.put("origin", req.getOrigin());
			flightSearchDetailsobj.put("destination", req.getDestination());
			flightSearchDetailsobj.put("beginDate", req.getBeginDate());

			flightSearchDetails.add(flightSearchDetailsobj);
			if (req.getTripType().equals("RoundTrip")) {
				roundtripDetailsobj.put("origin", req.getDestination());
				roundtripDetailsobj.put("destination", req.getOrigin());
				roundtripDetailsobj.put("beginDate", req.getEndDate());

				flightSearchDetails.add(roundtripDetailsobj);
			}

			ArrayList<String> objhasmap = new ArrayList<>();
			objhasmap.add("" + EngineID.Indigo);
			objhasmap.add("" + EngineID.AirAsia);
			objhasmap.add("" + EngineID.AirCosta);
			objhasmap.add("" + EngineID.GoAir);
			objhasmap.add("" + EngineID.Spicjet);
			objhasmap.add("" + EngineID.Trujet);
			objhasmap.add("" + EngineID.TravelPort);

			payload.put("engineIDs", objhasmap);

			payload.put("flightSearchDetails", flightSearchDetails);
			payload.put("tripType", req.getTripType());
			payload.put("cabin", req.getCabin());
			payload.put("adults", req.getAdults());
			payload.put("childs", req.getChilds());
			payload.put("infants", req.getInfants());
			payload.put("traceId", "AYTM00011111111110002");

			System.out.println(payload.toString());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client
					.resource(UrlMetadatas.getFlightSearchAPI(Role.ClIENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);

			String strResponse = response.getEntity(String.class);
			System.out.println(strResponse);
			if (response.getStatus() != 200) {
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setMessage("Service unavailable");
				result.setDetails("Service unavailable");
			} else {
				FlightSearchResponse searchResponse = new FlightSearchResponse();
				JSONObject jobjres = new JSONObject(strResponse);
				JSONObject jobj=jobjres.getJSONObject("details");
				if (jobj != null) {
					String errStr = jobj.getString("errors");
					//System.err.println("err Str : " + errStr);
					if (!errStr.equals("null")) {
						JSONObject errObj = jobj.getJSONObject("errors");
						Errors errCls = new Errors();
						errCls.setCode(errObj.getString("code"));
						errCls.setDescription(errObj.getString("description"));
						searchResponse.setErrors(errCls);
						result.setStatus(ResponseStatus.FAILURE.getKey());
						result.setMessage(errObj.getString("description"));
						result.setDetails(searchResponse);
					} else {
						String JourneysStr = jobj.getString("journeys");
						//System.out.println("Journeys  " + JourneysStr);
						if(JourneysStr.equals("null")){
							result.setStatus(ResponseStatus.FAILURE.getKey());
							result.setMessage("Flight Search Details are not Found");
							result.setDetails(searchResponse);
							return result;
						}else{
							ArrayList<Journeys> journeysList = new ArrayList<>();
							JSONArray Journeys = jobj.getJSONArray("journeys");
							for (int i = 0; i < Journeys.length(); i++) {
								Journeys journeysCls = new Journeys();
								JSONObject job = Journeys.getJSONObject(i);
								if (job != null) {
									String jrnyStr = job.getString("journeyDetail");
									if (!jrnyStr.equals("null")) {
										JSONObject jrnyD = job.getJSONObject("journeyDetail");
										//System.err.println("journeyDetails : " + jrnyD);
										if (jrnyD != null) {
											JourneyDetail journeyDetail = new JourneyDetail();
											journeyDetail.setOrigin(jrnyD.getString("origin"));
											journeyDetail.setDestination(jrnyD.getString("destination"));
											journeyDetail.setBeginDate(jrnyD.getString("beginDate"));
											journeyDetail.setEndDate(jrnyD.getString("endDate"));
											
											journeyDetail.setEngineID(engineId(jrnyD.getString("engineID")));
											journeyDetail.setCabin(ConvertUtil.convertCabin(jrnyD.getString("cabin")));
											journeyDetail.setCurrencyCode(jrnyD.getString("currencyCode"));
											journeysCls.setJourneyDetail(journeyDetail);
										}
									}
									ArrayList<Segment> segList = new ArrayList<>();
									JSONArray segments = job.getJSONArray("segments");
									for (int j = 0; j < segments.length(); j++) {
										ArrayList<Bonds> bondList = new ArrayList<>();
										ArrayList<Fare> fareList = new ArrayList<>();
										Segment segCls = new Segment();
										job = segments.getJSONObject(j);
										segCls.setBondType(job.getString("bondType"));
										segCls.setDeeplink(job.getString("deeplink"));
										segCls.setEngineID(job.getString("engineID"));
										segCls.setFareRule(job.getString("fareRule"));
										segCls.setBaggageFare(job.getBoolean("isBaggageFare"));
										segCls.setCache(job.getBoolean("isCache"));
										segCls.setHoldBooking(job.getBoolean("isHoldBooking"));
//										segCls.setInternational(job.getBoolean("isInternational"));
										segCls.setInternational(val);
										segCls.setRoundTrip(job.getBoolean("isRoundTrip"));
										segCls.setSpecial(job.getBoolean("isSpecial"));
										segCls.setSpecialId(job.getBoolean("isSpecialId"));
										segCls.setItineraryKey(job.getString("itineraryKey"));
										segCls.setJourneyIndex(job.getInt("journeyIndex"));
										segCls.setNearByAirport(job.getBoolean("nearByAirport"));
										segCls.setRemark(job.getString("remark"));
										segCls.setSearchId(job.getString("searchId"));
										segCls.setBonds(bondList);
										segCls.setFares(fareList);
										segList.add(segCls);
										JSONArray bonds = job.getJSONArray("bonds");
										JSONArray fareArr = job.getJSONArray("fare");
										for (int k = 0; k < bonds.length(); k++) {
											Bonds bondCls = new Bonds();
											job = bonds.getJSONObject(k);
											bondCls.setBoundType(job.getString("boundType"));
											bondCls.setItineraryKey(job.getString("itineraryKey"));
											bondCls.setBaggageFare(job.getBoolean("isBaggageFare"));
											bondCls.setSsrFare(job.getBoolean("isSSR"));
											bondCls.setJourneyTime(job.getString("journeyTime"));
											ArrayList<Legs> legsList = new ArrayList<>();
											JSONArray legs = job.getJSONArray("legs");
											for (int l = 0; l < legs.length(); l++) {
												Legs legsCls = new Legs();
												job = legs.getJSONObject(l);
												legsCls.setAircraftCode(job.getString("aircraftCode"));
												legsCls.setAircraftType(job.getString("aircraftType"));
												legsCls.setAirlineName(job.getString("airlineName"));
												//legsCls.setAmount(job.getDouble("Amount"));
												legsCls.setArrivalDate(job.getString("arrivalDate"));
												legsCls.setArrivalTerminal(job.getString("arrivalTerminal"));
												legsCls.setArrivalTime(job.getString("arrivalTime"));
												legsCls.setAvailableSeat(job.getString("availableSeat"));
												legsCls.setBaggageUnit(job.getString("baggageUnit"));
												legsCls.setBaggageWeight(job.getString("baggageWeight"));
												legsCls.setBoundTypes(job.getString("boundType"));
												legsCls.setCabin(ConvertUtil.convertCabin(job.getString("cabin")));
												legsCls.setCapacity(job.getString("capacity"));
												legsCls.setCarrierCode(job.getString("carrierCode"));
												legsCls.setCurrencyCode(job.getString("currencyCode"));
												legsCls.setDepartureDate(job.getString("departureDate"));
												legsCls.setDepartureTerminal(job.getString("departureTerminal"));
												legsCls.setDepartureTime(job.getString("departureTime"));
												legsCls.setDestination(job.getString("destination"));
												legsCls.setDuration(job.getString("duration"));
												legsCls.setFareBasisCode(job.getString("fareBasisCode"));
												legsCls.setFareClassOfService(job.getString("fareClassOfService"));
												legsCls.setFlightDesignator(job.getString("flightDesignator"));
												legsCls.setFlightDetailRefKey(job.getString("flightDetailRefKey"));
												legsCls.setFlightNumber(job.getString("flightNumber"));
												legsCls.setFlightName(job.getString("flightName"));
												legsCls.setGroup(job.getString("group"));
												legsCls.setConnecting(job.getBoolean("isConnecting"));
												legsCls.setNumberOfStops(job.getString("numberOfStops"));
												legsCls.setOrigin(job.getString("origin"));
												legsCls.setProviderCode(job.getString("providerCode"));
												legsCls.setRemarks(job.getString("remarks"));
												legsCls.setSold(job.getInt("sold")+"");
												legsCls.setStatus(job.getString("status"));
												String cbnStr = job.getString("cabinClasses");
												ArrayList<CabinClasses> cbnList = new ArrayList<>();
												if(!cbnStr.equals("null")){
													JSONArray cabinClasses = job.getJSONArray("cabinClasses");
													for (int m = 0; m < cabinClasses.length(); m++) {
														CabinClasses cbnCls = new CabinClasses();
														job = cabinClasses.getJSONObject(m);
														cbnCls.setCabinType(job.getString("cabinType"));
														ArrayList<BookingClass> bookingList = new ArrayList<>();
														String bkStr = job.getString("bookingClass");
														if(!bkStr.equals("null")){
															JSONArray bookingClasses = job.getJSONArray("bookingClass");
															for (int n = 0; n < bookingClasses.length(); n++) {
																BookingClass bookingCls = new BookingClass();
																job = bookingClasses.getJSONObject(n);
																bookingCls.setBClass(job.getString("bClass"));
																bookingCls.setCount(job.getInt("count")+"");
																bookingList.add(bookingCls);
															}
														}
														cbnCls.setBookingClasses(bookingList);
														cbnList.add(cbnCls);
													}
												}
												legsCls.setCabinClasses(cbnList);
												ArrayList<SSRDetails> ssrList = new ArrayList<>();
												String ssrStr = job.getString("ssrDetails");
												if(!ssrStr.equals("null")){
													JSONArray ssrDetails = job.getJSONArray("ssrDetails");
													for (int m = 0; m < ssrDetails.length(); m++) {
														SSRDetails ssrCls = new SSRDetails();
														job = ssrDetails.getJSONObject(m);
														ssrCls.setSSRAmount(job.getDouble("amount"));
														ssrCls.setSSRDetails(job.getString("detail"));
														ssrCls.setSSRCode(job.getString("ssrCode"));
														ssrCls.setSSRType(job.getString("ssrType"));
														ssrList.add(ssrCls);
													}
												}
												legsCls.setSsrDetails(ssrList);
												legsList.add(legsCls);
											}
											bondCls.setLegs(legsList);
											bondList.add(bondCls);
										}

										for (int k = 0; k < fareArr.length(); k++) {
											JSONObject fare=fareArr.getJSONObject(k);										
											Fare fareCls = new Fare();
											fareCls.setBasicFare(fare.getDouble("basicFare"));
											fareCls.setExchangeRate(fare.getDouble("exchangeRate"));
											fareCls.setTotalFareWithOutMarkUp(fare.getDouble("totalFareWithOutMarkUp"));
											fareCls.setTotalTaxWithOutMarkUp(fare.getDouble("totalTaxWithOutMarkUp"));
											fareList.add(fareCls);
											JSONArray paxFares = fare.getJSONArray("paxFares");
											ArrayList<PaxFares> paxFaresList = new ArrayList<>();
											for (int l = 0; l < paxFares.length(); l++) {
												job = paxFares.getJSONObject(l);
												JSONArray fares = job.getJSONArray("fares");
												ArrayList<BookFare> faresList = new ArrayList<>();
												PaxFares paxFaresCls = new PaxFares();
												paxFaresCls.setBaggageUnit(job.getString("baggageUnit"));
												paxFaresCls.setBaggageWeight(job.getString("baggageWeight"));
												paxFaresCls.setBaseTransactionAmount(job.getDouble("baseTransactionAmount"));
												paxFaresCls.setBasicFare(job.getDouble("basicFare"));
												paxFaresCls.setCancelPenalty(job.getDouble("cancelPenalty"));
												paxFaresCls.setChangePenalty(job.getDouble("changePenalty"));
												paxFaresCls.setEquivCurrencyCode(job.getString("equivCurrencyCode"));
												paxFaresCls.setFareBasisCode(job.getString("fareBasisCode"));
												paxFaresCls.setFareInfoKey(job.getString("fareInfoKey"));
												paxFaresCls.setFareInfoValue(job.getString("fareInfoValue"));
												paxFaresCls.setMarkUP(job.getDouble("markUP"));
												paxFaresCls.setPaxType(ConvertUtil.convertPaxType(job.getString("paxType")));
												paxFaresCls.setRefundable(job.getBoolean("refundable"));
												paxFaresCls.setTotalFare(job.getDouble("totalFare"));
												paxFaresCls.setTotalTax(job.getDouble("totalTax"));
												paxFaresCls.setPaxType(job.getString("paxType"));
												paxFaresCls.setTransactionAmount(job.getDouble("transactionAmount"));
												for (int l2 = 0; l2 < fares.length(); l2++) {
													job = fares.getJSONObject(l2);
													BookFare faresCls = new BookFare();
													faresCls.setAmount(job.getDouble("amount"));
													faresCls.setChargeCode(job.getString("chargeCode"));
													faresCls.setChargeType(job.getString("chargeType"));
													faresList.add(faresCls);
												}
												paxFaresCls.setBookFares(faresList);
												paxFaresList.add(paxFaresCls);
											}
											fareCls.setPaxFares(paxFaresList);
										}
									}
									journeysCls.setSegments(segList);
									journeysList.add(journeysCls);
								}

							}
							searchResponse.setJourneys(journeysList);
							result.setCode(ResponseStatus.SUCCESS.getValue());
							result.setStatus(ResponseStatus.SUCCESS.getKey());
							result.setMessage("Flight Search Response");
							result.setDetails(searchResponse);
							return result;
						}
					}

				}

			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setCode(ResponseStatus.FAILURE.getValue());
			result.setStatus(ResponseStatus.FAILURE.getKey());
			result.setMessage("Service unavailable");
			result.setDetails("Service unavailable");
		}
		return result;
	}



	@Override
	public FlightResponseDTO airRePriceRQ(FlightPriceCheckRequest req) {

		FlightResponseDTO result=new FlightResponseDTO();

		try {

			JSONObject payload = new JSONObject();

			FlightAvailability flightAvailability = new FlightAvailability();

			FlightSearchDetails flightSearchDetails = new FlightSearchDetails();

			flightSearchDetails.setBeginDate(req.getBeginDate());
			flightSearchDetails.setDestination(req.getDestination());
			flightSearchDetails.setEndDate(req.getEndDate());
			flightSearchDetails.setOrigin(req.getOrigin());

			ArrayList<String> engineIds = new ArrayList<>();
			engineIds.add(req.getEngineID());

			flightAvailability.setEngineIDs(engineIds);
			flightAvailability.setCabin(req.getCabin());
			flightAvailability.setTripType(req.getTripType());
			flightAvailability.setAdults(req.getAdults());
			flightAvailability.setChilds(req.getChilds());
			flightAvailability.setInfants(req.getInfants());
			flightAvailability.setTraceId(req.getTraceId());
			ArrayList<FlightSearchDetails> flightSearchArray = new ArrayList<>();
			flightSearchArray.add(flightSearchDetails);


			Segment segment = new Segment();

			//segment.setBondType(req.getBondType());
			segment.setBaggageFare(req.isBaggageFare());
			segment.setDeeplink("");
			segment.setEngineID(req.getEngineID());
			segment.setFares(req.getFares());
			segment.setFareIndicator("0");
			segment.setFareRule(req.getFareRule());
			segment.setCache(req.isCache());
			segment.setHoldBooking(req.isHoldBooking());
			segment.setInternational(req.isInternational());
			segment.setRoundTrip(req.isRoundTrip());
			segment.setSpecial(req.isSpecial());
			segment.setSpecialId(req.isSpecialId());
			segment.setItineraryKey(req.getItineraryKey());
			segment.setMemoryCreationTime(new Date().toString());
			segment.setJourneyIndex(Integer.parseInt(req.getJourneyIndex()));
			segment.setNearByAirport(req.isNearByAirport());
			segment.setRemark("");
			segment.setSearchId(req.getSearchId());
			segment.setBonds(req.getBonds());

			if (req.getBonds().size()==2) {
				FlightSearchDetails flightSearchDetailsreturn = new FlightSearchDetails();

				flightSearchDetailsreturn.setBeginDate(req.getEndDate());
				flightSearchDetailsreturn.setDestination(req.getOrigin());
				flightSearchDetailsreturn.setOrigin(req.getDestination());
				flightSearchArray.add(flightSearchDetailsreturn);
			}

			flightAvailability.setFlightSearchDetails(flightSearchArray);

			ArrayList<Segment> segments = new ArrayList<>();

			segments.add(segment);
			AirPriceCheckRequest airPrice = new AirPriceCheckRequest();

			airPrice.setClientApiName("Flight Price Recheck");
			airPrice.setClientIp(Inet4Address.getLocalHost().getHostAddress());
			airPrice.setClientToken(UrlMetadatas.MDEX_CLIENTTOKEN);
			airPrice.setCllientKey(UrlMetadatas.MDEX_CLIENTKEY);
			airPrice.setFlightAvailability(flightAvailability);
			airPrice.setSegments(segments);

			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client
					.resource(UrlMetadatas.getAirRePriceRQAPIConnecting(Role.ClIENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString()))
					.post(ClientResponse.class, airPrice.getJsonRequest());

			String strResponse = response.getEntity(String.class);
			System.out.println(strResponse);

			if (response.getStatus() != 200) {
				result.setCode(ResponseStatus.FAILURE.getValue());
				result.setStatus(ResponseStatus.FAILURE.getKey());
				result.setMessage("Service unavailable");
				result.setDetails("Service unavailable");

			} else {
				FlightPriceReCheckResponse reCheckResponse =  new FlightPriceReCheckResponse();
				JSONObject jobjres = new JSONObject(strResponse);
				JSONObject jobj=jobjres.getJSONObject("details");
				if (jobj != null) {
					String errStr = jobj.getString("errors");
					System.err.println("err Str : " + errStr);
					if (!errStr.equals("null")) {
						JSONObject errObj = jobj.getJSONObject("errors");
						Errors errCls = new Errors();
						errCls.setCode(errObj.getString("code"));
						errCls.setDescription(errObj.getString("description"));
						reCheckResponse.setErrors(errCls);
						result.setCode(ResponseStatus.FAILURE.getValue());
						result.setStatus(ResponseStatus.FAILURE.getKey());
						result.setMessage(errObj.getString("description"));
						result.setDetails(reCheckResponse);
					} else {
						ArrayList<Journeys> journeysList = new ArrayList<>();
						JSONArray Journeys = jobj.getJSONArray("journeys");
						for (int i = 0; i < Journeys.length(); i++) {
							Journeys journeysCls = new Journeys();
							JSONObject job = Journeys.getJSONObject(i);
							if (job != null) {
								String jrnyStr = job.getString("journeyDetail");
								if (!jrnyStr.equals("null")) {
									JSONObject jrnyD = job.getJSONObject("journeyDetail");
									System.err.println("journeyDetails : " + jrnyD);
									if (jrnyD != null) {
										JourneyDetail journeyDetail = new JourneyDetail();
										journeyDetail.setOrigin(jrnyD.getString("origin"));
										journeyDetail.setDestination(jrnyD.getString("destination"));
										journeyDetail.setBeginDate(jrnyD.getString("beginDate"));
										journeyDetail.setEndDate(jrnyD.getString("endDate"));
										journeyDetail.setEngineID(jrnyD.getString("engineID"));
										journeyDetail.setCabin(jrnyD.getString("cabin"));
										journeyDetail.setCurrencyCode(jrnyD.getString("currencyCode"));
										journeysCls.setJourneyDetail(journeyDetail);
									}
								}
								ArrayList<Segment> segList = new ArrayList<>();
								JSONArray segmentsArr = job.getJSONArray("segments");
								if(segmentsArr.length() != 0){
									for (int j = 0; j < segmentsArr.length(); j++) {
										ArrayList<Bonds> bondList = new ArrayList<>();
										ArrayList<Fare> fareList = new ArrayList<>();
										Segment segCls = new Segment();
										job = segmentsArr.getJSONObject(j);
										segCls.setBondType(job.getString("bondType"));
										segCls.setDeeplink(job.getString("deeplink"));
										segCls.setEngineID(job.getString("engineID"));
										segCls.setFareRule(job.getString("fareRule"));
										segCls.setBaggageFare(job.getBoolean("isBaggageFare"));
										segCls.setCache(job.getBoolean("isCache"));
										segCls.setHoldBooking(job.getBoolean("isHoldBooking"));
										segCls.setInternational(job.getBoolean("isInternational"));
										segCls.setRoundTrip(job.getBoolean("isRoundTrip"));
										segCls.setSpecial(job.getBoolean("isSpecial"));
										segCls.setSpecialId(job.getBoolean("isSpecialId"));
										segCls.setItineraryKey(job.getString("itineraryKey"));
										segCls.setJourneyIndex(job.getInt("journeyIndex"));
										segCls.setNearByAirport(job.getBoolean("nearByAirport"));
										segCls.setRemark(job.getString("remark"));
										segCls.setSearchId(job.getString("searchId"));
										segCls.setBonds(bondList);
										segCls.setFares(fareList);
										segList.add(segCls);
										JSONArray bonds = job.getJSONArray("bonds");
										//										JSONObject fare = job.getJSONObject("fare");
										JSONArray fareArr = job.getJSONArray("fare");
										//										shjkjsk
										for (int k = 0; k < bonds.length(); k++) {
											Bonds bondCls = new Bonds();
											job = bonds.getJSONObject(k);
											bondCls.setBoundType(job.getString("boundType"));
											bondCls.setItineraryKey(job.getString("itineraryKey"));
											bondCls.setBaggageFare(job.getBoolean("isBaggageFare"));
											bondCls.setSsrFare(job.getBoolean("isSSR"));
											bondCls.setJourneyTime(job.getString("journeyTime"));
											ArrayList<Legs> legsList = new ArrayList<>();
											JSONArray legs = job.getJSONArray("legs");
											for (int l = 0; l < legs.length(); l++) {
												Legs legsCls = new Legs();
												job = legs.getJSONObject(l);
												legsCls.setAircraftCode(job.getString("aircraftCode"));
												legsCls.setAircraftType(job.getString("aircraftType"));
												legsCls.setAirlineName(job.getString("airlineName"));
												//legsCls.setAmount(job.getDouble("Amount"));
												legsCls.setArrivalDate(job.getString("arrivalDate"));
												legsCls.setArrivalTerminal(job.getString("arrivalTerminal"));
												legsCls.setArrivalTime(job.getString("arrivalTime"));
												legsCls.setAvailableSeat(job.getString("availableSeat"));
												legsCls.setBaggageUnit(job.getString("baggageUnit"));
												legsCls.setBaggageWeight(job.getString("baggageWeight"));
												legsCls.setBoundTypes(job.getString("boundType"));
												legsCls.setCabin(job.getString("cabin"));
												legsCls.setCapacity(job.getString("capacity"));
												legsCls.setCarrierCode(job.getString("carrierCode"));
												legsCls.setCurrencyCode(job.getString("currencyCode"));
												legsCls.setDepartureDate(job.getString("departureDate"));
												legsCls.setDepartureTerminal(job.getString("departureTerminal"));
												legsCls.setDepartureTime(job.getString("departureTime"));
												legsCls.setDestination(job.getString("destination"));
												legsCls.setDuration(job.getString("duration"));
												legsCls.setFareBasisCode(job.getString("fareBasisCode"));
												legsCls.setFareClassOfService(job.getString("fareClassOfService"));
												legsCls.setFlightDesignator(job.getString("flightDesignator"));
												legsCls.setFlightDetailRefKey(job.getString("flightDetailRefKey"));
												legsCls.setFlightNumber(job.getString("flightNumber"));
												legsCls.setGroup(job.getString("group"));
												legsCls.setConnecting(job.getBoolean("isConnecting"));
												legsCls.setNumberOfStops(job.getString("numberOfStops"));
												legsCls.setOrigin(job.getString("origin"));
												legsCls.setProviderCode(job.getString("providerCode"));
												legsCls.setRemarks(job.getString("remarks"));
												legsCls.setSold(job.getInt("sold")+"");
												legsCls.setStatus(job.getString("status"));
												String cbnStr = job.getString("cabinClasses");
												ArrayList<CabinClasses> cbnList = new ArrayList<>();
												if(!cbnStr.equals("null")){
													JSONArray cabinClasses = job.getJSONArray("cabinClasses");
													for (int m = 0; m < cabinClasses.length(); m++) {
														CabinClasses cbnCls = new CabinClasses();
														job = cabinClasses.getJSONObject(m);
														cbnCls.setCabinType(job.getString("cabinType"));
														ArrayList<BookingClass> bookingList = new ArrayList<>();
														String bkStr = job.getString("bookingClass");
														if(!bkStr.equals("null")){
															JSONArray bookingClasses = job.getJSONArray("bookingClass");
															for (int n = 0; n < bookingClasses.length(); n++) {
																BookingClass bookingCls = new BookingClass();
																job = bookingClasses.getJSONObject(n);
																bookingCls.setBClass(job.getString("bClass"));
																bookingCls.setCount(job.getInt("count")+"");
																bookingList.add(bookingCls);
															}
														}
														cbnCls.setBookingClasses(bookingList);
														cbnList.add(cbnCls);
													}
												}
												legsCls.setCabinClasses(cbnList);
												ArrayList<SSRDetails> ssrList = new ArrayList<>();
												String ssrStr = job.getString("ssrDetails");
												if(!ssrStr.equals("null")){
													JSONArray ssrDetails = job.getJSONArray("ssrDetails");
													for (int m = 0; m < ssrDetails.length(); m++) {
														SSRDetails ssrCls = new SSRDetails();
														job = ssrDetails.getJSONObject(m);
														ssrCls.setSSRAmount(job.getDouble("amount"));
														ssrCls.setSSRDetails(job.getString("detail"));
														ssrCls.setSSRCode(job.getString("ssrcode"));
														ssrCls.setSSRType(job.getString("ssrtype"));
														ssrList.add(ssrCls);
													}
												}
												legsCls.setSsrDetails(ssrList);
												legsList.add(legsCls);
											}
											bondCls.setLegs(legsList);
											bondList.add(bondCls);
										}


										for (int k = 0; k < fareArr.length(); k++) {
											JSONObject fare=fareArr.getJSONObject(k);										
											Fare fareCls = new Fare();
											fareCls.setBasicFare(fare.getDouble("basicFare"));
											fareCls.setExchangeRate(fare.getDouble("exchangeRate"));
											fareCls.setTotalFareWithOutMarkUp(fare.getDouble("totalFareWithOutMarkUp"));
											fareCls.setTotalTaxWithOutMarkUp(fare.getDouble("totalTaxWithOutMarkUp"));
											fareList.add(fareCls);
											JSONArray paxFares = fare.getJSONArray("paxFares");
											ArrayList<PaxFares> paxFaresList = new ArrayList<>();
											for (int l = 0; l < paxFares.length(); l++) {
												job = paxFares.getJSONObject(l);
												JSONArray fares = job.getJSONArray("fares");
												ArrayList<BookFare> faresList = new ArrayList<>();
												PaxFares paxFaresCls = new PaxFares();
												paxFaresCls.setBaggageUnit(job.getString("baggageUnit"));
												paxFaresCls.setBaggageWeight(job.getString("baggageWeight"));
												paxFaresCls.setBaseTransactionAmount(job.getDouble("baseTransactionAmount"));
												paxFaresCls.setBasicFare(job.getDouble("basicFare"));
												paxFaresCls.setCancelPenalty(job.getDouble("cancelPenalty"));
												paxFaresCls.setChangePenalty(job.getDouble("changePenalty"));
												paxFaresCls.setEquivCurrencyCode(job.getString("equivCurrencyCode"));
												paxFaresCls.setFareBasisCode(job.getString("fareBasisCode"));
												paxFaresCls.setFareInfoKey(job.getString("fareInfoKey"));
												paxFaresCls.setFareInfoValue(job.getString("fareInfoValue"));
												paxFaresCls.setMarkUP(job.getDouble("markUP"));
												paxFaresCls.setPaxType((job.getString("paxType")));
												paxFaresCls.setRefundable(job.getBoolean("refundable"));
												paxFaresCls.setTotalFare(job.getDouble("totalFare"));
												paxFaresCls.setTotalTax(job.getDouble("totalTax"));
												paxFaresCls.setTransactionAmount(job.getDouble("transactionAmount"));
												for (int l2 = 0; l2 < fares.length(); l2++) {
													job = fares.getJSONObject(l2);
													BookFare faresCls = new BookFare();
													faresCls.setAmount(job.getDouble("amount"));
													faresCls.setChargeCode(job.getString("chargeCode"));
													faresCls.setChargeType(job.getString("chargeType"));
													faresList.add(faresCls);
												}
												paxFaresCls.setBookFares(faresList);
												paxFaresList.add(paxFaresCls);
											}
											fareCls.setPaxFares(paxFaresList);
										}
									}
									journeysCls.setSegments(segList);
									journeysList.add(journeysCls);
								}else{
									reCheckResponse.setJourneys(journeysList);
									result.setStatus(ResponseStatus.FAILURE.getKey());
									result.setMessage("Flight Details Not Found");
									result.setDetails(reCheckResponse);
									System.err.println("Details not found");
									return result;
								}

							}

						}
						reCheckResponse.setJourneys(journeysList);
						reCheckResponse.setTraceId(jobj.getString("traceId"));
						result.setCode(ResponseStatus.SUCCESS.getValue());
						result.setStatus(ResponseStatus.SUCCESS.getKey());
						result.setMessage("Flight Price Re-Check Response");
						result.setDetails(reCheckResponse);
						return result;
					}

				}

			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			result.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
			result.setMessage("Internal Server Error");
			result.setDetails("Internal Server Error");
		}
		return result;
	}
	
	@Override
	public String flightBookingInitiate(FligthBookReq fligthBookReq,List<TravellerFlightDetails> details) {
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", fligthBookReq.getSessionId());
			payload.put("ticketDetails", fligthBookReq.getTicketDetails());
			payload.put("paymentAmount", fligthBookReq.getPaymentAmount());
			payload.put("convenienceFee", fligthBookReq.getConvenienceFee());
			payload.put("baseFare", fligthBookReq.getBaseFare());
			payload.put("paymentMethod", fligthBookReq.getPaymentMethod());
			payload.put("source", fligthBookReq.getSource());
			payload.put("destination", fligthBookReq.getDestination());

			JSONArray travellersDetails=new JSONArray();
			for (int j = 0; j < details.size(); j++) {
				JSONObject travellersDetail=new JSONObject();
				travellersDetail.put("fName",(details.get(j).getfName()== null) ? "" :details.get(j).getfName());
				travellersDetail.put("lName",(details.get(j).getlName()== null) ? "" :details.get(j).getlName());
				travellersDetail.put("age",(details.get(j).getDob()== null) ? "" :details.get(j).getDob());
				travellersDetail.put("gender",(details.get(j).getGender()== null) ? "" :details.get(j).getGender());
				travellersDetail.put("fare",(details.get(j).getFare()== null) ? "" :details.get(j).getFare());
				travellersDetail.put("travellerType",(details.get(j).getType()== null) ? "" :details.get(j).getType());
				travellersDetail.put("ticketNo",(details.get(j).getTicketNo()== null) ? "" :details.get(j).getTicketNo());
				travellersDetails.put(travellersDetail);
			}
			payload.put("travellerDetails", travellersDetails);

			System.out.println(payload.toString());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(
					UrlMetadatas.agentGetFlightInitURL(Version.VERSION_1, Role.AGENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);

			String strResponse = response.getEntity(String.class);
			System.err.println(strResponse);
			return "" + strResponse;
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
			return "";
		}
	}
	
	
	@Override
	public String flightBookingInitiateForSplitPayment(FligthBookReq fligthBookReq,List<TravellerFlightDetails> details) {
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", fligthBookReq.getSessionId());
			payload.put("ticketDetails", fligthBookReq.getTicketDetails());
			payload.put("paymentAmount", fligthBookReq.getPaymentAmount());
			payload.put("paymentMethod", fligthBookReq.getPaymentMethod());
			payload.put("source", fligthBookReq.getSource());
			payload.put("destination", fligthBookReq.getDestination());

			JSONArray travellersDetails=new JSONArray();
			for (int j = 0; j < details.size(); j++) {
				JSONObject travellersDetail=new JSONObject();
				travellersDetail.put("fName",(details.get(j).getfName()== null) ? "" :details.get(j).getfName());
				travellersDetail.put("lName",(details.get(j).getlName()== null) ? "" :details.get(j).getlName());
				travellersDetail.put("age",(details.get(j).getAge()== null) ? "" :details.get(j).getAge());
				travellersDetail.put("gender",(details.get(j).getGender()== null) ? "" :details.get(j).getGender());
				travellersDetail.put("fare",(details.get(j).getFare()== null) ? "" :details.get(j).getFare());
				travellersDetail.put("travellerType",(details.get(j).getType()== null) ? "" :details.get(j).getType());
				travellersDetail.put("ticketNo",(details.get(j).getTicketNo()== null) ? "" :details.get(j).getTicketNo());
				travellersDetails.put(travellersDetail);
			}
			payload.put("travellerDetails", travellersDetails);

			System.out.println(payload.toString());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(
					UrlMetadatas.agentGetFlightInitiateSplitURL(Version.VERSION_1, Role.AGENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);

			String strResponse = response.getEntity(String.class);
			System.err.println(strResponse);
			return "" + strResponse;
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
			return "";
		}
	}


	@Override
	public String flightBookingSucess(FligthBookReq req,List<TravellerFlightDetails> details) {

		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", req.getSessionId());
			payload.put("bookingRefId", req.getBookingRefId());
			payload.put("mdexTxnRefNo", req.getMdexTxnRefNo());
			payload.put("paymentAmount", req.getPaymentAmount());
			payload.put("ticketDetails", req.getTicketDetails());
			payload.put("flightStatus", req.getFlightStatus());
			payload.put("paymentStatus", req.getPaymentStatus());
			payload.put("paymentMethod", req.getPaymentMethod());
			payload.put("txnRefno", req.getTxnRefno());
			payload.put("status", req.isSuccess());
			payload.put("email", req.getEmail());
			payload.put("mobile", req.getMobile());
			payload.put("success", req.isSuccess());
			payload.put("pnrNo", req.getPnrNo());
			
			JSONArray travellersDetails=new JSONArray();
			for (int j = 0; j < details.size(); j++) {
				JSONObject travellersDetail=new JSONObject();
				travellersDetail.put("fName",(details.get(j).getfName()== null) ? "" :details.get(j).getfName());
				travellersDetail.put("lName",(details.get(j).getlName()== null) ? "" :details.get(j).getlName());
				travellersDetail.put("age",(details.get(j).getDob()== null) ? "" :details.get(j).getDob());
				travellersDetail.put("gender",(details.get(j).getGender()== null) ? "" :details.get(j).getGender());
				travellersDetail.put("fare",(details.get(j).getFare()== null) ? "" :details.get(j).getFare());
				travellersDetail.put("travellerType",(details.get(j).getType()== null) ? "" :details.get(j).getType());
				travellersDetail.put("ticketNo",(details.get(j).getTicketNo()== null) ? "" :details.get(j).getTicketNo());
				travellersDetails.put(travellersDetail);
			}
			payload.put("travellerDetails", travellersDetails);

			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(
					UrlMetadatas.agentGetFlightPaymentSucessURL(Version.VERSION_1, Role.AGENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);

			String strResponse = response.getEntity(String.class);
			return "" + strResponse;
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
			return "";
		}
	}



	@Override
	public String flightPaymentGatewaySucess(FligthBookReq req, List<TravellerFlightDetails> details) {

		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", req.getSessionId());
			payload.put("bookingRefId", req.getBookingRefId());
			payload.put("mdexTxnRefNo", req.getMdexTxnRefNo());
			payload.put("paymentAmount", req.getPaymentAmount());
			payload.put("ticketDetails", req.getTicketDetails());
			payload.put("flightStatus", req.getFlightStatus());
			payload.put("paymentStatus", req.getPaymentStatus());
			payload.put("paymentMethod", req.getPaymentMethod());
			payload.put("merchantRefNo", req.getTxnRefno());
			payload.put("status", req.isSuccess());
			payload.put("email", req.getEmail());
			payload.put("mobile", req.getMobile());
			payload.put("success", req.isSuccess());
			payload.put("pnrNo", req.getPnrNo());
			
			JSONArray travellersDetails=new JSONArray();
			for (int j = 0; j < details.size(); j++) {
				JSONObject travellersDetail=new JSONObject();
				travellersDetail.put("fName",(details.get(j).getfName()== null) ? "" :details.get(j).getfName());
				travellersDetail.put("lName",(details.get(j).getlName()== null) ? "" :details.get(j).getlName());
				travellersDetail.put("age",(details.get(j).getDob()== null) ? "" :details.get(j).getDob());
				travellersDetail.put("gender",(details.get(j).getGender()== null) ? "" :details.get(j).getGender());
				travellersDetail.put("fare",(details.get(j).getFare()== null) ? "" :details.get(j).getFare());
				travellersDetail.put("travellerType",(details.get(j).getType()== null) ? "" :details.get(j).getType());
				travellersDetail.put("ticketNo",(details.get(j).getTicketNo()== null) ? "" :details.get(j).getTicketNo());
				travellersDetails.put(travellersDetail);
			}
			payload.put("travellerDetails", travellersDetails);

			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(
					UrlMetadatas.agentGetFlightPaymentGetwaySucessURL(Version.VERSION_1, Role.AGENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);

			String strResponse = response.getEntity(String.class);
			return "" + strResponse;
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
			return "";
		}
	}


	@Override
	public FlightResponseDTO getAllBookTickets(String sessionId) {
		FlightResponseDTO resp=new FlightResponseDTO();

		List<FlightTicketDTO> list=new ArrayList<>();
		List<TicketObj> onewayList=new ArrayList<>();
		List<TicketObj> roundwayList=new ArrayList<>();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", sessionId);

			Client vpqClient = Client.create();
			vpqClient.addFilter(new LoggingFilter(System.out));
			WebResource vpqWebResource = vpqClient.resource(
					UrlMetadatas.agentGetMyFlightTickets(Version.VERSION_1, Role.AGENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse vpqresponse = vpqWebResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = vpqresponse.getEntity(String.class);
			System.err.println("response ::" + strResponse);
			JSONObject jobj=new JSONObject(strResponse);
			final String code=jobj.getString("code");
			if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
				JSONArray detls=jobj.getJSONArray("details");
				for (int i = 0; i < detls.length(); i++) {
					JSONObject jObj=detls.getJSONObject(i).getJSONObject("flightTicket");

					if (jObj!=null) {
						JSONObject txn=jObj.getJSONObject("transaction");
						FlightTicketDTO dto=new FlightTicketDTO();
						String source=null;
						String destination=null;
						String journeyDate=null;
						String arrTime=null;
						String tripType=null;
						double totalFare=(double)jObj.getDouble("paymentAmount");
						String transactionRefNo=txn.getString("transactionRefNo");
						String status=jObj.getString("flightStatus");
						String mdexTxnRefNo=jObj.getString("mdexTxnRefNo");
						String bookingRefId=jObj.getString("bookingRefId");
						String ticketDetails=jObj.getString("ticketDetails");
						String txnStatus=txn.getString("status");
						long date=jObj.getLong("created");
						Date d=new Date(date);
						String txnDate=dateFormat.format(d);
						String unescape = StringEscapeUtils.unescapeJava(ticketDetails);
						if (!unescape.equalsIgnoreCase("null") && !unescape.equalsIgnoreCase("nullnull")) {
							JSONObject tickets=new JSONObject(unescape);
							JSONObject ticket=tickets.getJSONObject("Tickets");
							JSONArray oneway=ticket.getJSONArray("Oneway");
							JSONArray roundway=ticket.getJSONArray("Roundway");
							if (oneway.length()>0) {
								for (int j = 0; j < oneway.length(); j++) {
									TicketObj ticketObj=new TicketObj(); 
									String onewayObjStr=oneway.get(j).toString();
									JSONObject onewayObj=new JSONObject(onewayObjStr);
									ticketObj.setOrigin(onewayObj.getString("origin"));
									ticketObj.setDestination(onewayObj.getString("destination"));
									ticketObj.setArrivalDate(onewayObj.getString("arrivalDate"));
									ticketObj.setDepartureDate(onewayObj.getString("departureDate"));
									ticketObj.setJourneyTime(onewayObj.getString("journeyTime"));
									onewayList.add(ticketObj);
								}
								TicketObj fsTicketObj=onewayList.get(0);
								TicketObj lsTicketObj=onewayList.get(onewayList.size()-1);
								source=fsTicketObj.getOrigin();
								destination=lsTicketObj.getDestination();
								journeyDate=fsTicketObj.getDepartureDate();
								arrTime=lsTicketObj.getArrivalTime();
							}

							if (roundway.length()>0) {
								for (int j = 0; j < roundway.length(); j++) {
									TicketObj ticketObj=new TicketObj(); 
									String onewayObjStr=roundway.get(j).toString();
									JSONObject onewayObj=new JSONObject(onewayObjStr);
									ticketObj.setOrigin(onewayObj.getString("origin"));
									ticketObj.setDestination(onewayObj.getString("destination"));
									ticketObj.setArrivalDate(onewayObj.getString("arrivalDate"));
									ticketObj.setDepartureDate(onewayObj.getString("departureDate"));
									ticketObj.setJourneyTime(onewayObj.getString("journeyTime"));
									tripType="Roundway";
									roundwayList.add(ticketObj);
								}
								TicketObj fsTicketObj=roundwayList.get(0);
								TicketObj lsTicketObj=roundwayList.get(roundwayList.size()-1);
								source=fsTicketObj.getOrigin();
								destination=lsTicketObj.getDestination();
								journeyDate=fsTicketObj.getDepartureDate();
								arrTime=lsTicketObj.getArrivalTime();

							}

							dto.setArrTime(arrTime);
							dto.setDestination(destination);
							dto.setSource(source);
							dto.setJourneyDate(journeyDate);
							dto.setTotalFare(totalFare);
							dto.setTransactionRefNo(transactionRefNo);
							dto.setStatus(status);
							dto.setMdexTxnRefNo(mdexTxnRefNo);
							dto.setBookingRefId(bookingRefId);
							dto.setTicketDetails(ticketDetails);
							dto.setTxnStatus(txnStatus);
							dto.setTxnDate(txnDate);
							dto.setTripType(tripType);
							dto.setFlightTicketId(detls.getJSONObject(i).getJSONObject("flightTicket").getLong("id"));
							list.add(dto);
						}

					}

				}
				resp.setDetails(list);
				resp.setCode(code);
				resp.setMessage("Get All Book Tickets");
				resp.setStatus(ResponseStatus.SUCCESS.getKey());
			}
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
			resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			resp.setMessage("Service Unavailable");
			resp.setDetails(null);
			resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
		}
		return resp;
	}


	@Override
	public FlightResponseDTO getAllBookTicketsForMobile(String sessionId) {
		FlightResponseDTO resp=new FlightResponseDTO();

		List<FlightTicketMobileDTO> dtos=new ArrayList<>();

		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", sessionId);

			Client vpqClient = Client.create();
			vpqClient.addFilter(new LoggingFilter(System.out));
			WebResource vpqWebResource = vpqClient.resource(
					UrlMetadatas.agentGetMyFlightTickets(Version.VERSION_1, Role.AGENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse vpqresponse = vpqWebResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);

			String strResponse = vpqresponse.getEntity(String.class);
			System.err.println(strResponse);
			JSONObject jobj=new JSONObject(strResponse);
			final String code=jobj.getString("code");

			if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
				String details=jobj.getString("details");

				JSONArray detailsArr =new JSONArray(details);

				for (int i = 0; i <detailsArr.length(); i++) {

					JSONObject detail1=detailsArr.getJSONObject(i);
					List<TicketObj> onewayList=new ArrayList<>();
					List<TicketObj> roundwayList=new ArrayList<>();

					JSONArray travellerDetails=detail1.getJSONArray("travellerDetails");
					List<TravellerDetails> tDetailsList=new ArrayList<>();
					for (int j = 0; j < travellerDetails.length(); j++) {
						TravellerDetails tdDetails=new TravellerDetails();
						JSONObject single=travellerDetails.getJSONObject(j);

						tdDetails.setfName(single.getString("fName"));
						tdDetails.setlName(single.getString("lName"));
						tdDetails.setGender(single.getString("gender"));
						tdDetails.setTicketNo(single.getString("ticketNo"));
						tdDetails.setTravellerType(single.getString("type"));
						tDetailsList.add(tdDetails);
					}

					JSONObject flightTicket=detail1.getJSONObject("flightTicket");

					FlightTicketMobileDTO fDto=new FlightTicketMobileDTO();

					String source=null;
					String destination=null;
					String journeyDate=null;
					String arrTime=null;
					String deptTime=null;
					fDto.setBookingRefId(flightTicket.getString("bookingRefId"));
					fDto.setMdexTxnRefNo(flightTicket.getString("mdexTxnRefNo"));
					fDto.setPaymentStatus(flightTicket.getString("paymentStatus"));
					fDto.setFlightStatus(flightTicket.getString("flightStatus"));
					fDto.setTotalFare(flightTicket.getString("paymentAmount"));
					fDto.setPaymentMethod(flightTicket.getString("paymentMethod"));
					fDto.setTravellerDetails(tDetailsList);
					String txn=flightTicket.getString("transaction");
					if (txn!=null) {
						if (!txn.equalsIgnoreCase("null")) {
							JSONObject transaction=new JSONObject(txn);
							String txnRefNo=transaction.getString("transactionRefNo");
							fDto.setTransactionRefNo(txnRefNo);
						}
					}

					JSONObject tickets=null;
					String ticketsStr=flightTicket.getString("ticketDetails");
					if (!ticketsStr.equalsIgnoreCase("null") && !ticketsStr.equalsIgnoreCase("nullnull")) {
						tickets=new JSONObject(ticketsStr);

						JSONObject ticket=tickets.getJSONObject("Tickets");
						JSONArray oneway=ticket.getJSONArray("Oneway");
						JSONArray roundway=ticket.getJSONArray("Roundway");

						if (oneway.length()>0) {

							for (int j = 0; j < oneway.length(); j++) {

								TicketObj ticketObj=new TicketObj(); 

								String onewayObjStr=oneway.get(j).toString();
								String onewayObjStrs=oneway.get(j).toString();

								JSONObject onewayObj=new JSONObject(onewayObjStr);
								JSONObject onewayObjls=new JSONObject(onewayObjStrs);

								ticketObj.setOrigin(onewayObj.getString("origin"));
								ticketObj.setArrivalDate(onewayObj.getString("arrivalDate"));
								ticketObj.setDepartureDate(onewayObj.getString("departureDate"));
								ticketObj.setArrivalTime(onewayObj.getString("arrivalTime"));
								ticketObj.setDestination(onewayObjls.getString("destination"));
								ticketObj.setDepartureTime(onewayObjls.getString("departureTime"));
								ticketObj.setJourneyTime(onewayObj.getString("journeyTime"));
								ticketObj.setCabin(onewayObj.getString("cabin"));
								ticketObj.setAirlineName(onewayObj.getString("airlineName"));
								ticketObj.setDuration(onewayObj.getString("duration"));
								ticketObj.setFlightNumber(onewayObj.getString("flightNumber"));
								onewayList.add(ticketObj);
							}
							TicketObj fsTicketObj=onewayList.get(0);
							TicketObj lsTicketObj=onewayList.get(onewayList.size()-1);
							source=fsTicketObj.getOrigin();
							destination=lsTicketObj.getDestination();
							journeyDate=fsTicketObj.getDepartureDate();
							arrTime=lsTicketObj.getArrivalTime();
							deptTime=lsTicketObj.getDepartureTime();
						}
						fDto.setSource(source);
						fDto.setDestination(destination);
						fDto.setArrTime(arrTime);
						fDto.setJourneyDate(journeyDate);
						fDto.setDeptTime(deptTime);
						
						if (roundway.length()>0) {
							for (int j = 0; j < roundway.length(); j++) {

								TicketObj ticketObj=new TicketObj(); 

								String roundwayObjStr=roundway.get(j).toString();
								String roundwayObjStrs=roundway.get(j).toString();
								JSONObject roundwayObj=new JSONObject(roundwayObjStr);
								JSONObject roundwayObjls=new JSONObject(roundwayObjStrs);
								
								ticketObj.setOrigin(roundwayObj.getString("origin"));
								ticketObj.setDestination(roundwayObjls.getString("destination"));
								ticketObj.setArrivalDate(roundwayObjls.getString("arrivalDate"));
								ticketObj.setDepartureDate(roundwayObj.getString("departureDate"));
								ticketObj.setArrivalTime(roundwayObjls.getString("arrivalTime"));
								ticketObj.setDepartureTime(roundwayObjls.getString("departureTime"));
								ticketObj.setJourneyTime(roundwayObjls.getString("journeyTime"));
								ticketObj.setCabin(roundwayObjls.getString("cabin"));
								ticketObj.setAirlineName(roundwayObjls.getString("airlineName"));
								ticketObj.setDuration(roundwayObjls.getString("duration"));
								ticketObj.setFlightNumber(roundwayObjls.getString("flightNumber"));
								roundwayList.add(ticketObj);
								TicketObj fsTicketObj=roundwayList.get(0);
								TicketObj lsTicketObj=roundwayList.get(roundwayList.size()-1);
								source=fsTicketObj.getOrigin();
								destination=lsTicketObj.getDestination();
								journeyDate=fsTicketObj.getDepartureDate();
								arrTime=lsTicketObj.getArrivalTime();
								deptTime=lsTicketObj.getDepartureTime();
							}
						}

						fDto.setSourceReturn(source);
						fDto.setDestinationReturn(destination);
						fDto.setArrTimeReturn(arrTime);
						fDto.setJourneyDateReturn(journeyDate);
						fDto.setDeptTimeReturn(deptTime);
						fDto.setOneway(onewayList);
						fDto.setRoundway(roundwayList);
						dtos.add(fDto);
					}

					resp.setDetails(dtos);
				}
			}
			resp.setCode(code);
			resp.setMessage("Get All Book Tickets");
			resp.setStatus(ResponseStatus.SUCCESS.getKey());

		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
			resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			resp.setMessage("Service Unavailable");
			resp.setDetails(null);
			resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
		}
		return resp;
	}


	@Override
	public FlightResponseDTO saveAirLineNamesInDb() {

		FlightResponseDTO resp=new FlightResponseDTO();
		List<AirLineNames>airLineNames=new ArrayList<>(); 
		try {
			JSONObject payload = new JSONObject();
			payload.put("clientIp", Inet4Address.getLocalHost().getHostAddress());
			payload.put("cllientKey", UrlMetadatas.MDEX_CLIENTKEY);
			payload.put("clientToken", UrlMetadatas.MDEX_CLIENTTOKEN);
			payload.put("clientApiName", "Flight Name Request");
			Client client = Client.create();
			WebResource webResource = client
					.resource(UrlMetadatas.getAirlineNames(Role.ClIENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);

			String strResponse = response.getEntity(String.class);
			//System.err.println("strResponse: "+strResponse);
			JSONObject jobj=new JSONObject(strResponse);
			final String code=jobj.getString("code");
			final String status=jobj.getString("status");
			final String message=jobj.getString("message");

			if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
				String strDetails=jobj.getString("details");
				if (strDetails!=null) {
					String airportName=null;
					String cityCode=null;
					String cityName=null;
					String country=null;
					JSONArray cList=jobj.getJSONArray("details");
					for (int i = 0; i < cList.length(); i++) {
						airportName=cList.getJSONObject(i).getString("airportName");
						cityCode=cList.getJSONObject(i).getString("cityCode");
						cityName=cList.getJSONObject(i).getString("cityName");
						country=cList.getJSONObject(i).getString("country");
						AirLineNames  dto=new AirLineNames();
						dto.setAirportName(airportName);
						dto.setCityCode(cityCode);
						dto.setCityName(cityName);
						dto.setCountry(country);
						airLineNames.add(dto);
					}

					resp.setDetails(airLineNames);

				}
				resp.setCode(code);
				resp.setMessage(message);
				resp.setStatus(status);
			}else{
				resp.setCode(code);
				resp.setMessage(message);
				resp.setStatus(status);
			}

		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			resp.setMessage("Service Unavailable");
			resp.setDetails(null);
			resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
		}
		return resp;
	}

	@Override
	public FlightResponseDTO cronCheck(String session) {

		FlightResponseDTO resp=new FlightResponseDTO();

		List<BusCityListDTO> list=new ArrayList<>();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", session);

			Client vpqClient = Client.create();
			vpqClient.addFilter(new LoggingFilter(System.out));
			WebResource vpqWebResource = vpqClient.resource(
					UrlMetadatas.agentFlightCronCheck(Version.VERSION_1, Role.AGENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse vpqresponse = vpqWebResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);


			String strResponse = vpqresponse.getEntity(String.class);
			System.out.println(strResponse);
			JSONObject jobj=new JSONObject(strResponse);
			final String code=jobj.getString("code");
			if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
				String strDetails=jobj.getString("details");
				if (strDetails!=null) {
					JSONArray cList=jobj.getJSONArray("details");

				}
				resp.setCode(code);
				resp.setMessage("Get All City List");
				resp.setDetails(list);
				resp.setStatus(ResponseStatus.SUCCESS.getKey());
			}
		} catch (Exception e) {
			System.out.println(e);
			resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			resp.setMessage("Service Unavailable");
			resp.setDetails(null);
			resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
		}
		return resp;
	}

	
	private String engineId(String engineId)
	{
		String val=engineId;
		
		if (engineId.equalsIgnoreCase("AirAsia")) {
			val="AirAsia";
		}
		else if (engineId.equalsIgnoreCase("Airindia")) {
			val="Airindia";
		}
		else if (engineId.equalsIgnoreCase("Spicjet")) {
			val="Spicjet";
		}
		else if (engineId.equalsIgnoreCase("Jetairways")) {
			val="Jetairways";
		}
		else if (engineId.equalsIgnoreCase("Indigo")) {
			val="Indigo";
		}
		else if (engineId.equalsIgnoreCase("GoAir")) {
			val="GoAir";
		}
		return val;
		
	}
	
		
	
	@Override
	public boolean compareCountry(TravelFlightRequest req)
	{
		boolean resp=false;
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", req.getSessionId());
			payload.put("org", req.getOrigin());
			payload.put("dest", req.getDestination());
			
			Client vpqClient = Client.create();
			vpqClient.addFilter(new LoggingFilter(System.out));
			WebResource vpqWebResource = vpqClient.resource(
					UrlMetadatas.agentCompareCity(Version.VERSION_1, Role.AGENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse vpqresponse = vpqWebResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);


			String strResponse = vpqresponse.getEntity(String.class);
			System.out.println(strResponse);
			JSONObject jobj=new JSONObject(strResponse);
			final String code=jobj.getString("code");
			if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
				String strDetails=jobj.getString("details");
				if (strDetails!=null) {
//					JSONArray cList=jobj.getJSONArray("details");
					resp=jobj.getBoolean("details");
				}
				
			}
		} catch (Exception e) {
			System.out.println(e);
			
		}
		return resp;
		
	}
	
	@Override
	public String getUserPayAmount(String sessionId,String grandTotal) {
		String amount="";
		 try{
			    JSONObject payload = new JSONObject();
			    payload.put("sessionId",sessionId);
			 
			    Client vpqClient = Client.create();
				vpqClient.addFilter(new LoggingFilter(System.out));
				WebResource vpqWebResource = vpqClient.resource(
						UrlMetadatas.getUserAmount(Version.VERSION_1, Role.AGENT, Device.WEBSITE, Language.ENGLISH));
				System.out.println("vpqWebResource=="+vpqWebResource);
				ClientResponse vpqresponse = vpqWebResource.accept("application/json").type("application/json")
						.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);

				String strResponse = vpqresponse.getEntity(String.class);
				System.out.println(strResponse);
				JSONObject jobj=new JSONObject(strResponse);
				amount = jobj.getString("details");
				
		}catch(Exception e){
			System.out.println(e);
		}
		return amount;
	}

}


