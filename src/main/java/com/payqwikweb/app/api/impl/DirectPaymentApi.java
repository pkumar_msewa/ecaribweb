package com.payqwikweb.app.api.impl;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.payqwikweb.app.api.IDirectPaymentServiceApi;
import com.payqwikweb.app.metadatas.UrlMetadatas;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Language;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.Version;
import com.payqwikweb.app.model.request.DirectPaymentRequest;
import com.payqwikweb.app.model.response.DirectPaymentResponse;
import com.payqwikweb.app.utils.SecurityUtils;
import com.payqwikweb.util.APIUtils;
import com.payqwikweb.util.LogCat;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;

public class DirectPaymentApi implements IDirectPaymentServiceApi {

	@Override
	public DirectPaymentResponse getPayloToken(DirectPaymentRequest request) {
		DirectPaymentResponse resp = new DirectPaymentResponse();
		JSONObject payload = new JSONObject();
		try {
			payload.put("userName", request.getMobileNumber());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(
					UrlMetadatas.getPayloToken(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));

			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				String strResponse = response.getEntity(String.class);
				System.err.println(strResponse);
				LogCat.print("RESPONSE :: " + strResponse);

				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setDetails(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				LogCat.print("RESPONSE :: " + strResponse);
				System.out.println("RESPONSE :: " + strResponse);
				if (strResponse != null) {
					org.json.JSONObject jobj;
					try {
						jobj = new org.json.JSONObject(strResponse);
						if (jobj != null) {
							final String status = (String) jobj.get("status");
							final String code = (String) jobj.get("code");
							final String message = (String) jobj.get("message");
							if(code.equalsIgnoreCase("S00")) {
								final Object details = (String) jobj.get("details");
								final String transactionRefNo = (String) jobj.get("txnId");
								final double balance = (double) jobj.getDouble("balance");
								resp.setDetails(details);
								resp.setTxnId(transactionRefNo);
								resp.setBalance(balance);
							}
							resp.setCode(code);
							resp.setMessage(message);
							resp.setStatus(status);
						}
					} catch (org.json.JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return resp;
	}

	@Override
	public DirectPaymentResponse getIntinatePayloTransaction(DirectPaymentRequest request) {
		DirectPaymentResponse resp = new DirectPaymentResponse();
		JSONObject payload = new JSONObject();
		try {
			payload.put("userName", request.getMobileNumber());
			payload.put("consumerTokenKey", request.getConsumerTokenKey());
			payload.put("consumerSecretKey", request.getConsumerSecretKey());
			payload.put("amount", request.getAmount());
			payload.put("transactionRefNo", request.getTransactionRefNo());
			payload.put("consumerTokenKey", request.getConsumerTokenKey());
			payload.put("tokenKey", request.getTokenKey());
			payload.put("otp", request.getOtp());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getInitiatePayloToken(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				String strResponse = response.getEntity(String.class);
				System.err.println(strResponse);
				LogCat.print("RESPONSE :: " + strResponse);

				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setDetails(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				LogCat.print("RESPONSE :: " + strResponse);
				System.out.println("RESPONSE :: " + strResponse);
				if (strResponse != null) {
					org.json.JSONObject jobj;
					try {
						jobj = new org.json.JSONObject(strResponse);
						String txnId = null;
						if (jobj != null) {
							final String status = (String) jobj.get("status");
							final String code = (String) jobj.get("code");
							final String message = (String) jobj.get("message");
							if(code.equalsIgnoreCase("S00")) {
								final Object details = (String) jobj.get("details");
								final String error = (String) jobj.get("error");
								resp.setDetails(details);
								resp.setError(error);
									if ((String) jobj.get("txnId") != null) {
										txnId = (String) jobj.get("txnId");
									}
								resp.setTxnId(txnId);
							}
							resp.setCode(code);
							resp.setMessage(message);
							resp.setStatus(status);
						}
					} catch (org.json.JSONException e) {
						e.printStackTrace();
					} catch (Exception e) {

					}

				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}

	@Override
	public DirectPaymentResponse getUpdatePayloTransaction(DirectPaymentRequest request) {
		DirectPaymentResponse resp = new DirectPaymentResponse();
		JSONObject payload = new JSONObject();
		try {
			payload.put("userName", request.getMobileNumber());
			payload.put("consumerTokenKey", request.getConsumerTokenKey());
			payload.put("consumerSecretKey", request.getConsumerSecretKey());
			payload.put("transactionRefNo", request.getTransactionRefNo());
			payload.put("consumerTokenKey", request.getConsumerTokenKey());
			payload.put("tokenKey", request.getTokenKey());
			payload.put("status", request.getStatus());
			payload.put("retrivalReferenceNo", request.getRetrivalReferenceNumber());

			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getPayloUpdateTransaction(Version.VERSION_1,
					Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				String strResponse = response.getEntity(String.class);
				System.err.println(strResponse);
				LogCat.print("RESPONSE :: " + strResponse);

				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setDetails(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				LogCat.print("RESPONSE :: " + strResponse);
				System.out.println("RESPONSE :: " + strResponse);
				if (strResponse != null) {
					org.json.JSONObject jobj;
					try {
						jobj = new org.json.JSONObject(strResponse);
						String txnId = null;
						if (jobj != null) {
							final String status = (String) jobj.get("status");
							final String code = (String) jobj.get("code");
							final String message = (String) jobj.get("message");
							if(code.equalsIgnoreCase("S00")) {
								final Object details = (String) jobj.get("details");
								final String error = (String) jobj.get("error");
								if ((String) jobj.get("txnId") != null) {
									txnId = (String) jobj.get("txnId");
								}

								resp.setDetails(details);
								resp.setError(error);
								resp.setTxnId(txnId);
							}
								resp.setMessage(message);
								resp.setStatus(status);
								resp.setCode(code);
						}
					} catch (org.json.JSONException e) {
						e.printStackTrace();
					} catch (Exception e) {

					}

				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}

}
