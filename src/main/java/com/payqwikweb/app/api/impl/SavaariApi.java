package com.payqwikweb.app.api.impl;

import java.util.List;

import org.codehaus.jettison.json.JSONObject;
import org.json.JSONArray;

import com.payqwikweb.app.api.ISavaariApi;
import com.payqwikweb.app.metadatas.UrlMetadatas;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Language;
import com.payqwikweb.app.model.MerchantDTO;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.Version;
import com.payqwikweb.app.model.request.SavaariTokenRequest;
import com.payqwikweb.app.model.request.SessionDTO;
import com.payqwikweb.app.model.response.MerchantResponse;
import com.payqwikweb.app.model.response.SavaariResponse;
import com.payqwikweb.app.utils.SecurityUtils;
import com.payqwikweb.util.APIUtils;
import com.payqwikweb.util.ConvertUtil;
import com.payqwikweb.util.JSONParserUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;

public class SavaariApi implements ISavaariApi {

	@Override
	public SavaariResponse getAuthorizationCode(SavaariTokenRequest request) {
		SavaariResponse resp = new SavaariResponse();

		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(APIUtils.TOKEN_CODE)
					.queryParam(APIUtils.API_KEY_APP_ID, request.getAppId())
					.queryParam(APIUtils.API_KEY_, request.getApiKey());
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.get(ClientResponse.class);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("Failure");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jsonObj = new org.json.JSONObject(strResponse);
					if (jsonObj != null) {
						String status = jsonObj.getString("status");
						if (status.equalsIgnoreCase("success")) {
							String token = jsonObj.getJSONObject("data").getString("token");
							resp.setSuccess(true);
							resp.setCode("S00");
							resp.setMessage("Client Authenticated");
							resp.setStatus(status);
							resp.setToken(token);
						}
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("Failure");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}

	@Override
	public SavaariResponse saveAccessToken(SavaariTokenRequest request) {
		SavaariResponse resp = new SavaariResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("accessToken", request.getAccessToken());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(
					UrlMetadatas.saveTokenURL(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				System.err.print(strResponse);
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						final String details = (String) jobj.get("details");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(details);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("Failure");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("Failure");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public SavaariResponse getAccessToken(SavaariTokenRequest request) {
		SavaariResponse resp = new SavaariResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client
					.resource(UrlMetadatas.getTokenURL(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				System.err.print(strResponse);
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						final String details = (String) jobj.get("details");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(details);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("Failure");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("Failure");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public SavaariResponse getTripType(SavaariTokenRequest request) {
		SavaariResponse resp = new SavaariResponse();

		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(APIUtils.TRIPTYPE_CODE).queryParam("token",
					request.getAccessToken());
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.get(ClientResponse.class);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setStatus("Failure");
				resp.setResponse(strResponse);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jsonObj = new org.json.JSONObject(strResponse);
					if (jsonObj != null) {
						String status = jsonObj.getString("status");
						if (status.equalsIgnoreCase("success")) {
							org.json.JSONObject obj = jsonObj.getJSONObject("data");
							resp.setSuccess(true);
							resp.setCode("S00");
							resp.setMessage("trip Types");
							resp.setStatus(status);
							resp.setResponse(strResponse);
						}
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("Failure");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}

	@Override
	public SavaariResponse getSubTripType(SavaariTokenRequest request) {
		SavaariResponse resp = new SavaariResponse();

		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("tripType", request.getTripType());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(APIUtils.SUB_TRIPTYPE_CODE)
					.queryParam("token", request.getAccessToken()).queryParam("tripType", request.getTripType());
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.get(ClientResponse.class);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setStatus("Failure");
				resp.setResponse(strResponse);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jsonObj = new org.json.JSONObject(strResponse);
					if (jsonObj != null) {
						String status = jsonObj.getString("status");
						if (status.equalsIgnoreCase("success")) {
							org.json.JSONObject obj = jsonObj.getJSONObject("data");
							resp.setSuccess(true);
							resp.setCode("S00");
							resp.setMessage("Sub Trip Types");
							resp.setStatus(status);
							resp.setResponse(strResponse);
						}
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("Failure");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}

	@Override
	public SavaariResponse getSourceCity(SavaariTokenRequest request) {
		SavaariResponse resp = new SavaariResponse();

		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("tripType", request.getTripType());
			payload.put("subTripType", request.getSubTripType());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(APIUtils.SOURCE_CODE)
					.queryParam("token", request.getAccessToken()).queryParam("tripType", request.getTripType())
					.queryParam("subTripType", request.getSubTripType());
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.get(ClientResponse.class);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setStatus("Failure");
				resp.setResponse(strResponse);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jsonObj = new org.json.JSONObject(strResponse);
					if (jsonObj != null) {
						String status = jsonObj.getString("status");
						if (status.equalsIgnoreCase("success")) {
							resp.setSuccess(true);
							resp.setCode("S00");
							resp.setMessage("Source Cities");
							resp.setStatus(status);
							resp.setResponse(strResponse);
						}
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("Failure");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}

	@Override
	public SavaariResponse getDestinationCity(SavaariTokenRequest request) {
		SavaariResponse resp = new SavaariResponse();

		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("tripType", request.getTripType());
			payload.put("subTripType", request.getSubTripType());
			payload.put("sourceCity", request.getSourceCity());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(APIUtils.DESTINATION_CODE)
					.queryParam("token", request.getAccessToken()).queryParam("tripType", request.getTripType())
					.queryParam("subTripType", request.getSubTripType())
					.queryParam("sourceCity", request.getSourceCity());
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.get(ClientResponse.class);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setStatus("Failure");
				resp.setResponse(strResponse);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jsonObj = new org.json.JSONObject(strResponse);
					if (jsonObj != null) {
						String status = jsonObj.getString("status");
						if (status.equalsIgnoreCase("success")) {
							resp.setSuccess(true);
							resp.setCode("S00");
							resp.setMessage("Destination Cities");
							resp.setStatus(status);
							resp.setResponse(strResponse);
						}
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("Failure");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}

	@Override
	public SavaariResponse getAvailbilities(SavaariTokenRequest request) {
		SavaariResponse resp = new SavaariResponse();

		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("tripType", request.getTripType());
			payload.put("subTripType", request.getSubTripType());
			payload.put("sourceCity", request.getSourceCity());
			payload.put("destinationCity", request.getDestinationCity());
			payload.put("duration", request.getDuration());
			payload.put("pickUpDateTime", request.getPickUpDateTime());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(APIUtils.AVAILABLE_CODE)
					.queryParam("token", request.getAccessToken()).queryParam("tripType", request.getTripType())
					.queryParam("subTripType", request.getSubTripType())
					.queryParam("sourceCity", request.getSourceCity())
					.queryParam("destinationCity",
							(request.getDestinationCity() == null) ? "" : request.getDestinationCity())
					.queryParam("pickupDateTime", request.getPickUpDateTime())
					.queryParam("duration", request.getDuration());
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.get(ClientResponse.class);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setStatus("Failure");
				resp.setResponse(strResponse);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jsonObj = new org.json.JSONObject(strResponse);
					if (jsonObj != null) {
						String status = jsonObj.getString("status");
						if (status.equalsIgnoreCase("success")) {
							resp.setSuccess(true);
							resp.setCode("S00");
							resp.setMessage("Availabilities Of Cars");
							resp.setStatus(status);
							resp.setResponse(strResponse);
						}
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("Failure");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}

	@Override
	public SavaariResponse getlocalities(SavaariTokenRequest request) {
		SavaariResponse resp = new SavaariResponse();

		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("sourceCity", request.getSourceCity());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(APIUtils.LOCALITIES_CODE)
					.queryParam("token", request.getAccessToken()).queryParam("sourceCity", request.getSourceCity());
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.get(ClientResponse.class);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setStatus("Failure");
				resp.setResponse(strResponse);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jsonObj = new org.json.JSONObject(strResponse);
					if (jsonObj != null) {
						String status = jsonObj.getString("status");
						if (status.equalsIgnoreCase("success")) {
							resp.setSuccess(true);
							resp.setCode("S00");
							resp.setMessage("Locality of Source City");
							resp.setStatus(status);
							resp.setResponse(strResponse);
						}
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("Failure");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}

	@Override
	public SavaariResponse getCarTypes(SavaariTokenRequest request) {
		SavaariResponse resp = new SavaariResponse();

		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("sourceCity", request.getSourceCity());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(APIUtils.CAR_TYPE_CODE)
					.queryParam("token", request.getAccessToken()).queryParam("sourceCity", request.getSourceCity());
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.get(ClientResponse.class);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setStatus("Failure");
				resp.setResponse(strResponse);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jsonObj = new org.json.JSONObject(strResponse);
					if (jsonObj != null) {
						String status = jsonObj.getString("status");
						if (status.equalsIgnoreCase("success")) {
							resp.setSuccess(true);
							resp.setCode("S00");
							resp.setMessage("Types Of Cars");
							resp.setStatus(status);
							resp.setResponse(strResponse);
						}
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("Failure");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}

	@Override
	public SavaariResponse saveSavaariUserBookingDetails(SavaariTokenRequest request) {
		SavaariResponse resp = new SavaariResponse();

		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("sourceCity", request.getSourceCity());
			payload.put("destinationCity", request.getDestinationCity());
			payload.put("tripType", request.getTripType());
			payload.put("subTripType", request.getSubTripType());
			payload.put("pickUpDateTime", request.getPickUpDateTime());
			payload.put("duration", request.getDuration());
			payload.put("pickupAddress", request.getPickupAddress());
			payload.put("localityId", request.getLocalityId());
			payload.put("customerName", request.getCustomerName());
			payload.put("customerEmail", request.getCustomerEmail());
			payload.put("customerMobile", request.getCustomerMobile());
			payload.put("carType", request.getCarType());
			payload.put("prePayment", request.getPrePayment());
			payload.put("transactionRefNo", request.getTransactionRefNo());
			// payload.put("couponCode", request.getCouponCode());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(
					UrlMetadatas.SaveBookingDetails(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setStatus("Failure");
				resp.setResponse(strResponse);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						final String details = (String) jobj.get("details");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(details);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("Failure");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}

	@Override
	public SavaariResponse getBooking(SavaariTokenRequest request) {
		SavaariResponse resp = new SavaariResponse();

		try {
			// JSONObject payload = new JSONObject();
			MultivaluedMapImpl formData = new MultivaluedMapImpl();
			// payload.put("token", request.getAccessToken());
			formData.add("sourceCity", request.getSourceCity());
			formData.add("destinationCity", (request.getDestinationCity() == null) ? "" : request.getDestinationCity());
			formData.add("tripType", request.getTripType());
			formData.add("subTripType", request.getSubTripType());
			formData.add("pickupDateTime", request.getPickUpDateTime());
			formData.add("duration", request.getDuration());
			formData.add("pickupAddress", request.getPickupAddress());
			formData.add("localityId", request.getLocalityId());
			formData.add("customerName", request.getCustomerName());
			formData.add("customerEmail", request.getCustomerEmail());
			formData.add("customerMobile", request.getCustomerMobile());
			formData.add("carType", request.getCarType());
			formData.add("prePayment", request.getPrePayment());
			formData.add("couponCode", (request.getCouponCode() == null) ? "" : request.getCouponCode());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(APIUtils.BOOKING_CODE).queryParam("token",
					request.getAccessToken());
			ClientResponse response = webResource.post(ClientResponse.class, formData);
			String strResponse = response.getEntity(String.class);

			if (response.getStatus() == 201) {
				if (strResponse != null) {
					org.json.JSONObject jsonObj = new org.json.JSONObject(strResponse);
					if (jsonObj != null) {
						String status = jsonObj.getString("status");
						if (status.equalsIgnoreCase("success")) {
							org.json.JSONObject obj = jsonObj.getJSONObject("data");
							String bookingId = obj.getString("bookingId");
							resp.setSuccess(true);
							resp.setCode("S00");
							resp.setMessage("Booking of Car");
							resp.setStatus(status);
							resp.setResponse(strResponse);
							resp.setBookingId(bookingId);
						}
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("Failure");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setStatus("Failure");
				resp.setResponse(strResponse);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}

	@Override
	public SavaariResponse getBookingDetails(SavaariTokenRequest request) {
		SavaariResponse resp = new SavaariResponse();

		try {
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(APIUtils.BOOKING_DETAILS_CODE).queryParam("bookingId",
					request.getBookingId());
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.get(ClientResponse.class);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setStatus("Failure");
				resp.setResponse(strResponse);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jsonObj = new org.json.JSONObject(strResponse);
					if (jsonObj != null) {
						String status = jsonObj.getString("status");
						if (status.equalsIgnoreCase("success")) {
							resp.setSuccess(true);
							resp.setCode("S00");
							resp.setMessage("Booking of Car");
							resp.setStatus(status);
							resp.setResponse(strResponse);
						}
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("Failure");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}

	@Override
	public SavaariResponse getSuccessTikcet(SavaariTokenRequest request) {
		SavaariResponse resp = new SavaariResponse();

		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("bookingId", request.getBookingId());
			payload.put("sourceCity", request.getSourceCity());
			payload.put("carType", request.getCarType());
			payload.put("carName", request.getCarName());
			payload.put("tripType", request.getTripType());
			payload.put("localityId", request.getLocality());
			payload.put("pickUpDateTime", request.getPickupDate());
			payload.put("pickUpLocation", request.getPickupLocation());
			payload.put("returnDate", request.getReturnDate());
			payload.put("reservationId", request.getReservationId());
			payload.put("prePayment", request.getPrePayment());
			payload.put("transactionRefNo", request.getTransactionRefNo());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(
					UrlMetadatas.SuccessBookingDetails(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setStatus("Failure");
				resp.setResponse(strResponse);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						final String details = (String) jobj.get("details");
						final double balance =  (double) jobj.get("balance");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setDetails(details);
						resp.setBalance(balance);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("Failure");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}
	@Override
	public SavaariResponse CancelDetails(SavaariTokenRequest request) {
		SavaariResponse resp = new SavaariResponse();

		try {
			Client client = Client.create();
			MultivaluedMapImpl formData = new MultivaluedMapImpl();
			formData.add("bookingId", request.getBookingId());
			formData.add("cancellationReason", request.getCancelReason());
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(APIUtils.CANCEL_DETAILS_CODE)
					.queryParam("token",request.getAccessToken());
			ClientResponse response = webResource.accept("application/json")
					.post(ClientResponse.class,formData);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setStatus("Failure");
				resp.setResponse(strResponse);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jsonObj = new org.json.JSONObject(strResponse);
					if (jsonObj != null) {
						String status = jsonObj.getString("status");
						if (status.equalsIgnoreCase("success")) {
							resp.setSuccess(true);
							resp.setCode("S00");
							resp.setMessage("Cancellation Of a Car");
							resp.setStatus(status);
							resp.setResponse(strResponse);
						}
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("Failure");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}
	
	@Override
	public SavaariResponse checkBookingId(SavaariTokenRequest request) {
		SavaariResponse resp = new SavaariResponse();

		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("bookingId", request.getBookingId());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(
					UrlMetadatas.getBookingId(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setStatus("Failure");
				resp.setResponse(strResponse);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						final String details = (String) jobj.get("details");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setDetails(details);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("Failure");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}
	
	@Override
	public SavaariResponse cancelTicket(SavaariTokenRequest request) {
		SavaariResponse resp = new SavaariResponse();

		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("bookingId", request.getBookingId());
//			payload.put("status", request.getStatus());
			payload.put("prePayment", "1590");
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(
					UrlMetadatas.cancelTicket(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setStatus("Failure");
				resp.setResponse(strResponse);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						final String details = (String) jobj.get("details");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setDetails(details);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("Failure");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}
	
	@Override
	public SavaariResponse getTicketDetails(SavaariTokenRequest dto) {
		SavaariResponse resp = new SavaariResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", dto.getSessionId());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getTicketDetails(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
							resp.setCode(code);
							resp.setMessage(message);
							resp.setResponse(strResponse);
						} else {
							resp.setSuccess(false);
						}
					} else {
						resp.setSuccess(false);
						resp.setCode("F00");
						resp.setMessage("Service unavailable");
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
		}
		return resp;
	}
}
