package com.payqwikweb.app.api.impl;

import java.io.StringReader;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.MultivaluedMap;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import com.gcm.utils.APIConstants;
import com.payqwikweb.app.model.request.VNetRequest;
import com.payqwikweb.app.model.response.VNetResponse;
import com.payqwikweb.app.model.response.VRedirectResponse;
import com.thirdparty.model.ResponseDTO;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.payqwikweb.app.api.IAgentLoadMoneyApi;
import com.payqwikweb.app.api.ILoadMoneyApi;
import com.payqwikweb.app.metadatas.UrlMetadatas;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Language;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.Version;
import com.payqwikweb.app.model.request.LoadMoneyRequest;
import com.payqwikweb.app.model.response.EBSRedirectResponse;
import com.payqwikweb.app.model.response.EBSStatusResponseDTO;
import com.payqwikweb.app.model.response.LoadMoneyResponse;
import com.payqwikweb.app.model.response.TransactionReportResponse;
import com.payqwikweb.app.utils.SecurityUtils;
import com.payqwikweb.model.web.RefundStatusDTO;
import com.payqwikweb.util.APIUtils;
import com.payqwikweb.util.JSONParserUtil;
import com.payqwikweb.util.LogCat;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class AgentLoadMoneyApi implements IAgentLoadMoneyApi {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());


	public LoadMoneyResponse loadMoneyRequest(LoadMoneyRequest request,Role role) {
		LoadMoneyResponse resp = new LoadMoneyResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("channel", request.getChannel());
			payload.put("account_id", request.getAccountId());
			payload.put("reference_no", request.getReferenceNo());
			payload.put("amount", request.getAmount());
			payload.put("mode", request.getMode());
			payload.put("currency", request.getCurrency());
			payload.put("description", request.getDescription());
			payload.put("return_url", request.getReturnUrl());
			payload.put("name", request.getName());
			payload.put("address", request.getAddress());
			payload.put("city", request.getCity());
			payload.put("state", request.getState());
			payload.put("country", request.getCountry());
			payload.put("postal_code", request.getPostalCode());
			payload.put("phone", request.getPhone());
			payload.put("email", request.getEmail());
			payload.put("ship_name", request.getShipName());
			payload.put("ship_address", request.getShipAddress());
			payload.put("ship_city", request.getShipCity());
			payload.put("ship_state", request.getShipState());
			payload.put("ship_country", request.getShipCountry());
			payload.put("ship_postal_code", request.getShipPostalCode());
			payload.put("ship_phone", request.getShipPhone());
			payload.put("payment_mode", request.getPaymentMode());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getLoadMoneyUrl(Version.VERSION_1, role, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				String strResponse = response.getEntity(String.class);
				org.json.JSONObject jObj = new org.json.JSONObject(strResponse);
				final String code = JSONParserUtil.getString(jObj, "code");
				if (code.equalsIgnoreCase("S00")) {
					org.json.JSONObject details = JSONParserUtil.getObject(jObj, "details");
					if (details != null) {
						/*resp.setAccountId(JSONParserUtil.getString(details, "account_id"));
						resp.setAddress(JSONParserUtil.getString(details, "address"));
						resp.setAmount(JSONParserUtil.getString(details, "amount"));
						resp.setChannel(JSONParserUtil.getString(details, "channel"));
						resp.setCity(JSONParserUtil.getString(details, "city"));
						resp.setCountry(JSONParserUtil.getString(details, "country"));
						resp.setCurrency(JSONParserUtil.getString(details, "currency"));
						resp.setDescription(JSONParserUtil.getString(details, "description"));
						resp.setEmail(JSONParserUtil.getString(details, "email"));
						resp.setMode(JSONParserUtil.getString(details, "mode"));
						resp.setName(JSONParserUtil.getString(details, "name"));
						resp.setPhone(JSONParserUtil.getString(details, "phone"));
						resp.setPostalCode(JSONParserUtil.getString(details, "postal_code"));
						resp.setReferenceNo(JSONParserUtil.getString(details, "reference_no"));
						resp.setReturnUrl(JSONParserUtil.getString(details, "return_url"));
						resp.setSecureHash(JSONParserUtil.getString(details, "secure_hash"));
						resp.setShipAddress(JSONParserUtil.getString(details, "ship_address"));
						resp.setShipCity(JSONParserUtil.getString(details, "ship_city"));
						resp.setShipCountry(JSONParserUtil.getString(details, "ship_country"));
						resp.setShipName(JSONParserUtil.getString(details, "ship_name"));
						resp.setShipPhone(JSONParserUtil.getString(details, "ship_phone"));
						resp.setShipPostalCode(JSONParserUtil.getString(details, "ship_postal_code"));
						resp.setShipState(JSONParserUtil.getString(details, "ship_state"));
						resp.setState(JSONParserUtil.getString(details, "state"));*/
						resp.setSuccess(true);
					} else {
						resp.setSuccess(false);
					}
				} else if (code.equalsIgnoreCase("F00")) {
					org.json.JSONObject failureDetails = JSONParserUtil.getObject(jObj, "details");
					if (failureDetails != null) {
					//	resp.setDescription(JSONParserUtil.getString(failureDetails, "message"));
					}
				} else {
					resp.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
		}
		return resp;

	}

	@Override
	public EBSRedirectResponse processRedirectResponse(HttpServletRequest request,Role role) {
		MultivaluedMapImpl formData = new MultivaluedMapImpl();
		EBSRedirectResponse redirectResponse = new EBSRedirectResponse();
		Enumeration paramNames = request.getParameterNames();
		while (paramNames.hasMoreElements()) {
			String paramName = (String) paramNames.nextElement();
			String paramValue = request.getParameter(paramName);
			if (paramName.equalsIgnoreCase("responseCode") && paramValue.equalsIgnoreCase("0")) {
				redirectResponse.setSuccess(true);
			} else if (paramName.equalsIgnoreCase("responseCode") && paramValue.equalsIgnoreCase("1")) {
				redirectResponse.setSuccess(false);
			}
			formData.add(paramName, paramValue);
		}
		try {
			Client client = Client.create();
			WebResource resource = client.resource(UrlMetadatas.getLoadMoneyResponseUrl(Version.VERSION_1, Role.USER,
					Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = resource.accept("application/json").post(ClientResponse.class, formData);
			String strResponse = response.getEntity(String.class);
			System.err.println("string response ::" + strResponse);
			if (response.getStatus() == 200) {
				org.json.JSONObject jObj = new org.json.JSONObject(strResponse);
				final String code = JSONParserUtil.getString(jObj, "code");
				if (code.equalsIgnoreCase("S00")) {
					redirectResponse.setSuccess(true);
					org.json.JSONObject details = JSONParserUtil.getObject(jObj, "details");
					redirectResponse.setDetails(String.valueOf(details));
				} else {
					redirectResponse.setResponseCode("1");
					redirectResponse.setSuccess(false);
				}
			} else {
				redirectResponse.setResponseCode("1");
				redirectResponse.setSuccess(false);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return redirectResponse;
	}

	@Override
	public VNetResponse initiateVnetBanking(VNetRequest request,Role role) {
		VNetResponse resp = new VNetResponse();
		try {
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getLoadMoneyVNetUrl(Version.VERSION_1, role, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(request.toJSON().toString()))
					.post(ClientResponse.class, request.toJSON());
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				String strResponse = response.getEntity(String.class);
				org.json.JSONObject jObj = new org.json.JSONObject(strResponse);
				final String code = JSONParserUtil.getString(jObj, "code");
				if (code.equalsIgnoreCase("S00")) {
					org.json.JSONObject details = JSONParserUtil.getObject(jObj, "details");
					if (details != null) {
						resp.setMerchantName(JSONParserUtil.getString(details, "merchantName"));
						resp.setReturnUrl(JSONParserUtil.getString(details, "returnURL"));
						resp.setItc(JSONParserUtil.getString(details, "itc"));
						resp.setAmount(JSONParserUtil.getString(details, "amount"));
						resp.setCrn(JSONParserUtil.getString(details, "crnNo"));
						resp.setPrn(JSONParserUtil.getString(details, "prnNo"));
						resp.setPid(JSONParserUtil.getString(details, "pid"));
						resp.setMid(JSONParserUtil.getString(details, "mid"));
						resp.setSuccess(true);
					} else {
						resp.setSuccess(false);
					}
				} else if (code.equalsIgnoreCase("F00")) {
					org.json.JSONObject failureDetails = JSONParserUtil.getObject(jObj, "details");
					if (failureDetails != null) {
						resp.setMessage(JSONParserUtil.getString(failureDetails, "message"));
					}
				} else {
					resp.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
		}
		return resp;

	}

	@Override
	public ResponseDTO handleRedirectRequest(VRedirectResponse dto,Role role) {
		ResponseDTO result = new ResponseDTO();
		try {
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getLoadMoneyVNetResponseUrl(Version.VERSION_1,
					role, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(dto.toJSON().toString()))
					.post(ClientResponse.class, dto.toJSON());
			if (response.getStatus() != 200) {
				result.setSuccess(false);
			} else {
				String strResponse = response.getEntity(String.class);
				org.json.JSONObject jObj = new org.json.JSONObject(strResponse);
				final String code = JSONParserUtil.getString(jObj, "code");
				if (code.equalsIgnoreCase("S00")) {
					result.setSuccess(true);
					result.setMessage(JSONParserUtil.getString(jObj, "message"));
					result.setCode(JSONParserUtil.getString(jObj, "code"));
				} else {
					result.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
		}
		return result;
	}

	@Override
	public ResponseDTO verifyEBSTransaction(EBSRedirectResponse resp,Role role) {
		final ResponseDTO result = new ResponseDTO();
		try {
			Client client = Client.create();
			WebResource webResource = client.resource(com.payqwikweb.api.constants.APIConstants.EBS_VERIFICATION);
			MultivaluedMapImpl formData = new MultivaluedMapImpl();
			formData.add("Action", "statusByRef");
			formData.add("AccountID", "20696");
			formData.add("SecretKey", "6496e4db9ebf824ffe2269afee259447");
			formData.add("RefNo", resp.getMerchantRefNo());
			ClientResponse response = webResource.post(ClientResponse.class, formData);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				result.setSuccess(false);
			} else {
				SAXParserFactory factory = SAXParserFactory.newInstance();
				SAXParser saxParser = factory.newSAXParser();
				DefaultHandler handler = new DefaultHandler() {
					boolean output = false;

					public void startElement(String uri, String localName, String qName, Attributes attributes)
							throws SAXException {
						if (qName.equalsIgnoreCase("output")) {
							output = true;
						}
						if (output) {
							String status = attributes.getValue("status");
							String error = attributes.getValue("error");
							String transactionType = attributes.getValue("transactionType");
							if (error == null) {
								if (transactionType.equalsIgnoreCase("Authorized")) {
									if (status.equalsIgnoreCase("Processed")) {
										result.setSuccess(true);
										result.setCode("S00");
									}
								} else {
									result.setSuccess(false);
									result.setCode("F00");
									result.setMessage("Transaction is " + transactionType);
								}
							} else {
								result.setSuccess(false);
								result.setCode("F00");
								result.setMessage(error);
							}
						}
					}
				};
				saxParser.parse(new InputSource(new StringReader(strResponse)), handler);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
		}
		return result;
	}

	@Override
	public EBSRedirectResponse processRedirectSDK(EBSRedirectResponse redirectResponse,Role role) {
		try {
			MultivaluedMapImpl formData = new MultivaluedMapImpl();
			formData.add("responseCode", redirectResponse.getResponseCode());
			formData.add("merchantRefNo", redirectResponse.getMerchantRefNo());
			Client client = Client.create();
			WebResource resource = client.resource(UrlMetadatas.getLoadMoneyResponseUrl(Version.VERSION_1, role,
					Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = resource.accept("application/json").post(ClientResponse.class, formData);
			String strResponse = response.getEntity(String.class);
			System.err.println(strResponse);
			if (response.getStatus() == 200) {
				org.json.JSONObject jObj = new org.json.JSONObject(strResponse);
				final String code = JSONParserUtil.getString(jObj, "code");
				if (code.equalsIgnoreCase("S00")) {
					redirectResponse.setSuccess(true);
					org.json.JSONObject details = JSONParserUtil.getObject(jObj, "details");
					redirectResponse.setDetails(String.valueOf(details));
					redirectResponse.setSessionId(JSONParserUtil.getString(jObj, "sessionId"));
				} else {
					redirectResponse.setResponseCode("1");
					redirectResponse.setSuccess(false);
				}
			} else {
				redirectResponse.setResponseCode("1");
				redirectResponse.setSuccess(false);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return redirectResponse;
	}

	// in test phase
	/*
	 * @Override public ResponseDTO refundEBSTransaction(EBSRedirectResponse
	 * resp) { ResponseDTO result = new ResponseDTO(); try { Client client =
	 * Client.create(); client.addFilter(new LoggingFilter(System.out));
	 * WebResource webResource =
	 * client.resource(com.payqwikweb.api.constants.APIConstants.URL.EBS_STATUS)
	 * ;
	 * 
	 * MultivaluedMapImpl formData = new MultivaluedMapImpl();
	 * formData.add("Action","status"); formData.add("AccountID","20696");
	 * formData.add("SecretKey","6496e4db9ebf824ffe2269afee259447");
	 * formData.add("RefNo",resp.getMerchantRefNo()); ClientResponse response =
	 * webResource.post(ClientResponse.class, formData); String strResponse =
	 * response.getEntity(String.class); if (response.getStatus() != 200) {
	 * logger.info("RESPONSE  :: " + strResponse); result.setSuccess(false); }
	 * else { logger.info("RESPONSE  :: " + strResponse); SAXParserFactory
	 * factory = SAXParserFactory.newInstance(); SAXParser saxParser =
	 * factory.newSAXParser(); DefaultHandler handler = new DefaultHandler(){
	 * 
	 * boolean output = false;
	 * 
	 * public void startElement(String uri, String localName,String qName,
	 * Attributes attributes) throws SAXException {
	 * 
	 * System.out.println("Start Element :" + qName);
	 * 
	 * if (qName.equalsIgnoreCase("output")) { output = true; } if(output){
	 * System.out.println(attributes.getValue("paymentId"));
	 * System.out.println(attributes.getValue("status")); String status =
	 * attributes.getValue("status"); String error =
	 * attributes.getValue("error"); String
	 * tranxId=attributes.getValue("referenceNo"); String
	 * description=attributes.getValue("description"); String transactionType =
	 * attributes.getValue("transactionType"); if(error == null) {
	 * if(status.equalsIgnoreCase("Captured") &&
	 * status.equalsIgnoreCase("Authorized")) { result.setSuccess(true);
	 * result.setCode("S00"); } } else { result.setSuccess(false);
	 * result.setCode("F00"); result.setMessage("Transaction is " +
	 * transactionType); } }else { result.setSuccess(false);
	 * result.setCode("F00"); //result.setMessage(error); } }
	 * 
	 * };
	 * 
	 * saxParser.parse(new InputSource(new StringReader(strResponse)),handler);
	 * 
	 * 
	 * 
	 * logger.info("Load money response ::" + strResponse); } } catch (Exception
	 * e) { e.printStackTrace(); result.setSuccess(false); }
	 * 
	 * return result; }
	 */
	@Override
	public EBSStatusResponseDTO crossCheckEBSStatus(String referenceNo,Role role) 
			throws ParserConfigurationException, SAXException {
		EBSStatusResponseDTO statusResponse = new EBSStatusResponseDTO();
		try {
		Client client = Client.create();
		WebResource webResource = client.resource(com.payqwikweb.api.constants.APIConstants.EBS_STATUS);
		MultivaluedMapImpl formData = new MultivaluedMapImpl();
		formData.add("Action", "status");
		formData.add("AccountID", "20696");
		formData.add("SecretKey", "6496e4db9ebf824ffe2269afee259447");
		formData.add("RefNo", referenceNo);
		ClientResponse response = webResource.post(ClientResponse.class, formData);
		String strResponse = response.getEntity(String.class);
		if (response.getStatus() != 200) {
			statusResponse.setSuccess(false);
		} else {
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser saxParser = factory.newSAXParser();
			DefaultHandler handler = new DefaultHandler() {
				boolean output = false;

				public void startElement(String uri, String localName, String qName, Attributes attributes)
						throws SAXException {
					
					if (qName.equalsIgnoreCase("output")) {
						output = true;
					}
					if(output){
						String status = attributes.getValue("status");
						String error = attributes.getValue("error");
						String errorCode=attributes.getValue("errorCode");
						if(error==null) {
						double amount=Double.parseDouble(attributes.getValue("amount"));
						String tranxId=attributes.getValue("referenceNo");
						String description=attributes.getValue("description");
						String dateCreated=attributes.getValue("dateCreated");
								if (status.equalsIgnoreCase("Captured")|| status.equalsIgnoreCase("Incompleted") || status.equalsIgnoreCase("AuthFailed")) {
									statusResponse.setSuccess(true);
									statusResponse.setCreated(dateCreated);
									statusResponse.setTransactionRefNo(tranxId);
									statusResponse.setAmount(amount);
									statusResponse.setStatus(status);
									statusResponse.setDescription(description);
									statusResponse.setCode("S00");
								   }
								 }else if(error!=null && errorCode.equalsIgnoreCase("4")){
									   statusResponse.setErrorMessage(error);
									   statusResponse.setErrorCode(errorCode);
								   }
							   }
							} 
				
			};
			saxParser.parse(new InputSource(new StringReader(strResponse)), handler);

		}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return statusResponse;
	}

	@Override
	public TransactionReportResponse getRefundStatus(RefundStatusDTO request,Role role) {
		TransactionReportResponse resp = new TransactionReportResponse();

		try {
			JSONObject payload = new JSONObject();
			payload.put("from", request.getFrom());
			payload.put("to", request.getTo());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.refundStatusURl(Version.VERSION_1, Role.USER, Device.ANDROID, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				// resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				// resp.setStatus("FAILED");
				// resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						// final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						final org.json.JSONArray details = jobj.getJSONArray("details");

						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}

						resp.setCode(code);
						// resp.setStatus(status);
						resp.setMessage(message);
						resp.setJsonArray(details);
					} else {
						resp.setSuccess(false);
						resp.setCode("F00");
						resp.setMessage("Service unavailable");
						resp.setStatus("FAILED");
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
		}
		return resp;
	}
}
