package com.payqwikweb.app.api.impl;

import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.payqwik.visa.util.VisaMerchantRequest;
import com.payqwikweb.app.api.IRegistrationApi;
import com.payqwikweb.app.metadatas.UrlMetadatas;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Language;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.Version;
import com.payqwikweb.app.model.request.ChangePasswordRequest;
import com.payqwikweb.app.model.request.MobileOTPRequest;
import com.payqwikweb.app.model.request.RegistrationRequest;
import com.payqwikweb.app.model.request.ResendMobileOTPRequest;
import com.payqwikweb.app.model.request.VerifyEmailRequest;
import com.payqwikweb.app.model.response.AddMerchantResponse;
import com.payqwikweb.app.model.response.ChangePasswordResponse;
import com.payqwikweb.app.model.response.MobileOTPResponse;
import com.payqwikweb.app.model.response.QuestionResponse;
import com.payqwikweb.app.model.response.RegistrationResponse;
import com.payqwikweb.app.model.response.ResendMobileOTPResponse;
import com.payqwikweb.app.model.response.VerifyEmailResponse;
import com.payqwikweb.app.utils.SecurityUtils;
import com.payqwikweb.model.app.request.RegisterRequest;
import com.payqwikweb.model.web.Status;
import com.payqwikweb.model.web.UserType;
import com.payqwikweb.util.APIUtils;
import com.payqwikweb.util.CommonUtil;
import com.payqwikweb.util.JSONParserUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;


public class RegistrationApi implements IRegistrationApi {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Override
	public RegistrationResponse register(RegistrationRequest request) {

		RegistrationResponse resp = new RegistrationResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", "");
			payload.put("username", request.getContactNo());
			payload.put("password", request.getPassword());
			payload.put("confirmPassword", request.getConfirmPassword());
			payload.put("userType", UserType.User);
			payload.put("authority", "");
			payload.put("status", Status.Inactive);
			payload.put("address", "");
			payload.put("locationCode", request.getLocationCode());
			payload.put("contactNo", request.getContactNo());
			payload.put("firstName", request.getFirstName());
			payload.put("middleName", "");
			payload.put("lastName", request.getLastName());
			payload.put("locationCode",request.getLocationCode());
			payload.put("gender", request.getGender());
			payload.put("dateOfBirth", request.getDateOfBirth());
			payload.put("email", request.getEmail());
			payload.put("vbankCustomer",request.isVbankCustomer());
			payload.put("accountNumber",request.getAccountNumber());
			payload.put("secQuestionCode",request.getSecQuestionCode());
			payload.put("secAnswer",request.getSecAnswer());
			payload.put("brand",request.getBrand());
			payload.put("model",request.getModel());
			payload.put("imeiNo",request.getImeiNo());
			payload.put("fingerPrint", request.getFingerPrint());
			
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(
					UrlMetadatas.getRegisterUrl(Version.VERSION_1, Role.USER, CommonUtil.getDevice(request.getDevice()), Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable...");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						resp.setMessage(message);
						String errors = "";
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							org.json.JSONObject details = JSONParserUtil.getObject(jobj,"details");
							if(details != null){
								boolean valid = JSONParserUtil.getBoolean(details,"valid");
								if(!valid){
									final String contactNo = JSONParserUtil.getString(details,"contactNo");
									final String email = JSONParserUtil.getString(details,"email");
									final String pinCode = JSONParserUtil.getString(details,"locationCode");
									if(!contactNo.equals("null")){
										errors = errors+"|"+contactNo;
										resp.setMessage(contactNo);
									}
									if(!email.equals("null")){
										errors = errors+"|"+email;
										resp.setMessage(email);
									}
									if(!pinCode.equals("null")) {
										errors = errors +"|"+pinCode;
										resp.setMessage(pinCode);
									}
								}
							}
							resp.setSuccess(false);
							resp.setDetails(errors);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setResponse(strResponse);
					}
					} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public MobileOTPResponse mobileOTP(MobileOTPRequest request) {

		MobileOTPResponse resp = new MobileOTPResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", "");
			payload.put("mobileNumber", request.getMobileNumber());
			payload.put("key", request.getKey());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getMobileUrl(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						final Object details = (String) jobj.get("details");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setDetails(details);
						resp.setResponse(strResponse);
					} else {
						resp.setSuccess(false);
						resp.setCode("F00");
						resp.setMessage("Service unavailable");
						resp.setStatus("FAILED");
						resp.setResponse(APIUtils.getFailedJSON().toString());
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());

		}
		return resp;
	}

	@Override
	public ResendMobileOTPResponse resendMobileOTP(ResendMobileOTPRequest request) {
		ResendMobileOTPResponse resp = new ResendMobileOTPResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", "");
			payload.put("mobileNumber", request.getMobileNumber());
			payload.put("key", "");
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getResendMobileUrl(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						final Object details =(Object)jobj.get("details");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setDetails(details);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public VerifyEmailResponse verifyEmail(VerifyEmailRequest request) {
		VerifyEmailResponse resp = new VerifyEmailResponse();
		try {
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getVerifyEmailUrl(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH)+"/"+request.getKey());
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(request.getKey())).post(ClientResponse.class);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						final Object details = (Object) jobj.get("details");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setDetails(details);
						resp.setResponse(strResponse);
					} 
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public ChangePasswordResponse changePassword(ChangePasswordRequest request) {
		ChangePasswordResponse resp = new ChangePasswordResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("oldPassword", request.getOldPassword());
			payload.put("newPassword", request.getNewPassword());
			payload.put("confirmPassword", request.getConfirmPassword());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getChangePasswordUrl(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					} else {
						resp.setSuccess(false);
						resp.setCode("F00");
						resp.setMessage("Service unavailable");
						resp.setStatus("FAILED");
						resp.setResponse(APIUtils.getFailedJSON().toString());
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public RegistrationResponse registerMerchant(RegisterRequest request) {
		RegistrationResponse resp = new RegistrationResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("username", request.getContactNo());
			payload.put("password", "");
			payload.put("confirmPassword", "");
			payload.put("userType", "");
			payload.put("authority", "");
			payload.put("status", "");
			payload.put("address", "");
			payload.put("contactNo", request.getContactNo());
			payload.put("firstName", request.getFirstName());
			payload.put("middleName", "");
			payload.put("lastName", request.getLastName());
			payload.put("locationCode", "");
			payload.put("gender", "");
			payload.put("dateOfBirth", "");
			payload.put("email", request.getEmail());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getRegisterUrl(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public RegistrationResponse merchantSignUp(VisaMerchantRequest request) {
		RegistrationResponse resp = new RegistrationResponse();
		try {
			JSONObject payload = new JSONObject();
//			payload.put("merchantName", request.getMerchantName());
			payload.put("firstName", request.getFirstName());
			payload.put("lastName",request.getLastName());
			payload.put("password", request.getPassword());
			payload.put("confirmPassword", request.getPassword());
			payload.put("emailAddress", request.getEmailAddress());
			payload.put("mobileNo", request.getMobileNo());
			payload.put("username", request.getEmailAddress());
			payload.put("userType", UserType.Merchant);
			payload.put("authority", "");
			payload.put("status", Status.Inactive);
			payload.put("address1", request.getAddress1());
			payload.put("pinCode", request.getPinCode());
			payload.put("panNo", request.getPanNo());
			payload.put("aadharNo", request.getAadharNo());
	//		payload.put("vbankCustomer",request.isVbankCustomer());
			
			payload.put("accountName", request.getMerchantAccountName());
			payload.put("bankAccountNo",request.getMerchantAccountNumber());
			payload.put("bankName", request.getMerchantBankName());
			payload.put("branchName", request.getMerchantBankLocation());
			payload.put("ifscCode", request.getMerchantBankIfscCode());
			/*
			payload.put("settlementAccountName", request.getSettlementAccountName());
			payload.put("settlementAccountNo", request.getSettlementAccountNumber());
			payload.put("settlementBankName", request.getSettlementBankName());
			payload.put("settlementBranchName", request.getSettlementBankLoction());
			payload.put("settlementIfscCode", request.getSettlementIfscCode());*/
			System.err.println("PAYLOAD : :" +payload.toString());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getMerchantSignUpUrl(Version.VERSION_1, Role.MERCHANT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				System.err.println("inside false case");
				String strResponse = response.getEntity(String.class);
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						resp.setMessage(message);
						String errors = "";
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							org.json.JSONObject details = JSONParserUtil.getObject(jobj,"details");
							if(details != null){
								boolean valid = JSONParserUtil.getBoolean(details,"valid");
								if(!valid){
									final String contactNo = JSONParserUtil.getString(details,"contactNo");
									final String email = JSONParserUtil.getString(details,"email");
									final String pinCode = JSONParserUtil.getString(details,"locationCode");
									if(!contactNo.equals("null")){
										errors = errors+"|"+contactNo;
										resp.setMessage(contactNo);
									}
									if(!email.equals("null")){
										errors = errors+"|"+email;
										resp.setMessage(email);
									}
									if(!pinCode.equals("null")) {
										errors = errors +"|"+pinCode;
										resp.setMessage(pinCode);
									}
								}
							}
							resp.setSuccess(false);
							resp.setDetails(errors);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setResponse(strResponse);
					}
					} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}
	
	@Override
	public AddMerchantResponse checkExistingVisaMerchant(VisaMerchantRequest request) {
		AddMerchantResponse response = new AddMerchantResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("emailAddress", request.getEmailAddress());
			System.out.println("PayLoad :: " + payload.toString());

			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.checkExistingMerchant(Version.VERSION_1, Role.MERCHANT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse resp = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (resp.getStatus() != 200) {
				response.setSuccess(false);
				response.setCode("F00");
				response.setMessage("Service unavailable");
				response.setStatus("FAILED");
				response.setDetails(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = resp.getEntity(String.class);
				System.err.println("Response :: " + strResponse);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							response.setSuccess(true);
							final String details = (String) jobj.get("details");
							response.setDetails(details);
						} else {
							response.setSuccess(false);
						}
						response.setCode(code);
						response.setStatus(status);
						response.setMessage(message);
					} else {
						response.setSuccess(false);
						response.setCode("F00");
						response.setMessage("Service unavailable");
						response.setStatus("FAILED");
						response.setDetails(APIUtils.getFailedJSON().toString());
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setSuccess(false);
			response.setCode("F00");
			response.setMessage("Service unavailable");
			response.setStatus("FAILED");
			response.setDetails(APIUtils.getFailedJSON().toString());
		}
		return response;
	}
	
	@Override
	public AddMerchantResponse signUpVisaMerchant(VisaMerchantRequest request) {
		AddMerchantResponse response = new AddMerchantResponse();
		try {
			JSONObject payload = new JSONObject();
//			payload.putOpt("merchantName", request.getMerchantName());
			payload.put("firstName", request.getFirstName());
			payload.put("lastName", request.getLastName());
			payload.put("emailAddress", request.getEmailAddress());
			payload.put("address1", request.getAddress1());
			payload.put("address2", request.getAddress2());
			payload.put("city", request.getCity());
			payload.put("state", "Merchant Payment");
			payload.put("country", request.getCountry());
			payload.put("pinCode", request.getPanNo());
			payload.put("merchantBusinessType", request.getMerchantBusinessType());
//			payload.put("lattitude", request.getLattitude());
			payload.put("longitude", request.getLongitude());
			payload.put("isEnabled", request.getIsEnabled());
			payload.put("panNo", request.getPanNo());
			payload.put("mobileNo", request.getMobileNo());
			payload.put("aggregator", request.getAggregator());
			//payload.put("entityId", request.getEntityId());
			//payload.put("mVisaId", request.getMvisaId());
			
			payload.put("merchantAccountName", request.getMerchantAccountName());
			payload.put("merchantBankName", request.getMerchantBankName());
			payload.put("merchantAccountNumber", request.getMerchantAccountNumber());
//			payload.put("merhantIfscCode", request.getMerchantBankIfscCode());
//			payload.put("merchantBankLoction", request.getMerchantBankLocation());

			payload.put("settlementAccountName", "vpayqwik");
			payload.put("settlementBankName", "Vijaya");
			payload.put("settlementAccountNumber", "5461612151");
			payload.put("settlementIfscCode","VIJB0039");
			payload.put("settlementBankLoction", "bangalore");

			System.err.println("PayLoad :: " + payload.toString());

			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.signUpVisaMerchant(Version.VERSION_1, Role.MERCHANT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse resp = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (resp.getStatus() != 200) {
				response.setSuccess(false);
				response.setCode("F00");
				response.setMessage("Service unavailable");
				response.setStatus("FAILED");
				response.setDetails(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = resp.getEntity(String.class);
				System.err.println("Response ::: " + strResponse);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							response.setSuccess(true);
							final String details = (String) jobj.get("details");
							response.setDetails(details);
						} else {
							response.setSuccess(false);
						}
						response.setCode(code);
						response.setStatus(status);
						response.setMessage(message);
					} else {
						response.setSuccess(false);
						response.setCode("F00");
						response.setMessage("Service unavailable");
						response.setStatus("FAILED");
						response.setDetails(APIUtils.getFailedJSON().toString());
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setSuccess(false);
			response.setCode("F00");
			response.setMessage("Service unavailable");
			response.setStatus("FAILED");
			response.setDetails(APIUtils.getFailedJSON().toString());
		}
		return response;
	}

	@Override
	public RegistrationResponse addBankDetails(VisaMerchantRequest request) {
		RegistrationResponse resp = new RegistrationResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("firstName", request.getFirstName());
			payload.put("middleName", "");
			payload.put("lastName", "");
			payload.put("password", request.getPassword());
			payload.put("confirmPassword", request.getPassword());
			payload.put("email", request.getEmailAddress());
			payload.put("contactNo", request.getMobileNo());
			payload.put("username", request.getEmailAddress());
			payload.put("userType", UserType.Merchant);
			payload.put("authority", "");
			payload.put("status", Status.Inactive);
			payload.put("address", request.getAddress1());
			payload.put("locationCode", request.getPinCode());
	//		payload.put("panCardNo", request.getPanNo());
	//		payload.put("aadharNo", request.getAadharNo());
	//		payload.put("vbankCustomer",request.isVbankCustomer());
	//		payload.put("accountName", request.getMerchantAccountName());
	//		payload.put("bankAccountNo",request.getMerchantAccountNumber());
	//		payload.put("bankName", request.getMerchantBankName());
	//		payload.put("branchName", request.getMerchantBankLoction());
	//		payload.put("ifscCode", request.getMerhantIfscCode());
			System.err.println("PAYLOAD : :" +payload.toString());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getMBankDetailsUrl(Version.VERSION_1, Role.MERCHANT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				String strResponse = response.getEntity(String.class);
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						resp.setMessage(message);
						String errors = "";
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							org.json.JSONObject details = JSONParserUtil.getObject(jobj,"details");
							if(details != null){
								boolean valid = JSONParserUtil.getBoolean(details,"valid");
								if(!valid){
									final String contactNo = JSONParserUtil.getString(details,"contactNo");
									final String email = JSONParserUtil.getString(details,"email");
									final String pinCode = JSONParserUtil.getString(details,"locationCode");
									if(!contactNo.equals("null")){
										errors = errors+"|"+contactNo;
										resp.setMessage(contactNo);
									}
									if(!email.equals("null")){
										errors = errors+"|"+email;
										resp.setMessage(email);
									}
									if(!pinCode.equals("null")) {
										errors = errors +"|"+pinCode;
										resp.setMessage(pinCode);
									}
								}
							}
							resp.setSuccess(false);
							resp.setDetails(errors);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setResponse(strResponse);
					}
					} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}
	
	@Override
	public MobileOTPResponse merchantMobileOTP(MobileOTPRequest request) {
		MobileOTPResponse resp = new MobileOTPResponse();
		try {
			JSONObject payload = new JSONObject();
			//payload.put("sessionId", "");
			payload.put("mobileNumber", request.getMobileNumber());
			payload.put("key", request.getKey());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getMobileOTPUrl(Version.VERSION_1, Role.MERCHANT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						final Object details = (String) jobj.get("details");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setDetails(details);
						resp.setResponse(strResponse);
					} else {
						resp.setSuccess(false);
						resp.setCode("F00");
						resp.setMessage("Service unavailable");
						resp.setStatus("FAILED");
						resp.setResponse(APIUtils.getFailedJSON().toString());
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());

		}
		return resp;
	}
	
	@Override
	public ResendMobileOTPResponse resendMerchantMobileOTP(ResendMobileOTPRequest request) {
		ResendMobileOTPResponse resp = new ResendMobileOTPResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", "");
			payload.put("mobileNumber", request.getMobileNumber());
			payload.put("key", "");
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getResendMobileOTPUrl(Version.VERSION_1, Role.MERCHANT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						final Object details = (Object) jobj.get("details");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setDetails(details);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public QuestionResponse getQuestions() {
		QuestionResponse resp=new QuestionResponse();
		try {
			String strResponse = "";
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getQuestionsUrl(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json").get(ClientResponse.class);
			System.err.println("URL :: " + UrlMetadatas.getQuestionsUrl(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
	
			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
			}
			strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMsg("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMsg(message);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMsg("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMsg("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}

		return resp;

	}
	
	@Override
	public RegistrationResponse newRegister(RegistrationRequest request) {

		RegistrationResponse resp = new RegistrationResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("username", request.getContactNo());
			payload.put("password", request.getPassword());
			payload.put("userType", UserType.User);
			payload.put("authority", "");
			payload.put("status", Status.Inactive);
			payload.put("address", "");
			payload.put("locationCode", request.getLocationCode());
			payload.put("contactNo", request.getContactNo());
			payload.put("name", request.getFirstName());
			payload.put("middleName", "");
			payload.put("lastName", request.getLastName());
			payload.put("locationCode",request.getLocationCode());
			payload.put("gender", request.getGender());
			payload.put("dateOfBirth", request.getDateOfBirth());
			payload.put("email", request.getEmail());
			payload.put("vbankCustomer",false);
			payload.put("mpin",request.getMpin());
			
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(
					UrlMetadatas.getNewRegisterUrl(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("API_KEY", "AFRNDUCNBANKUPON").post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					System.err.println("Response ::" + strResponse);
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						resp.setMessage(message);
						String errors = "";
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							org.json.JSONObject details = JSONParserUtil.getObject(jobj,"details");
							if(details != null){
								boolean valid = JSONParserUtil.getBoolean(details,"valid");
								if(!valid){
									final String contactNo = JSONParserUtil.getString(details,"contactNo");
									final String email = JSONParserUtil.getString(details,"email");
									final String pinCode = JSONParserUtil.getString(details,"locationCode");
									if(!contactNo.equals("null")){
										errors = errors+"|"+contactNo;
										resp.setMessage(contactNo);
									}
									if(!email.equals("null")){
										errors = errors+"|"+email;
										resp.setMessage(email);
									}
									if(!pinCode.equals("null")) {
										errors = errors +"|"+pinCode;
										resp.setMessage(pinCode);
									}
								}
							}
							resp.setSuccess(false);
							resp.setDetails(errors);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setResponse(strResponse);
					}
					} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	

	

	
}
