package com.payqwikweb.app.api.impl;

import javax.servlet.http.HttpSession;

import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.payqwikweb.app.api.ILoginApi;
import com.payqwikweb.app.metadatas.Constants;
import com.payqwikweb.app.metadatas.UrlMetadatas;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Language;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.Version;
import com.payqwikweb.app.model.request.ChangePasswordWithOtpRequest;
import com.payqwikweb.app.model.request.ForgetPasswordUserRequest;
import com.payqwikweb.app.model.request.LoginRequest;
import com.payqwikweb.app.model.request.ResendForgotPasswordOtpRequest;
import com.payqwikweb.app.model.response.ChangePasswordWithOtpResponse;
import com.payqwikweb.app.model.response.ForgetPasswordUserResponse;
import com.payqwikweb.app.model.response.LoginResponse;
import com.payqwikweb.app.model.response.ResendForgotPasswordOtpResponse;
import com.payqwikweb.app.utils.SecurityUtils;
import com.payqwikweb.util.APIUtils;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.xml.bind.v2.runtime.reflect.opt.Const;

public class LoginApi implements ILoginApi {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public LoginResponse login(LoginRequest request, Role role) {
		LoginResponse resp = new LoginResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("username", request.getUsername());
			payload.put("password", request.getPassword());
			payload.put("ipAddress", request.getIpAddress());	
			payload.put("registrationId", request.getRegistrationId());
			payload.put("validate",request.isValidate());
			payload.put("mobileToken",request.getMobileToken());
			Client client = Client.create();
			WebResource webResource = client
					.resource(UrlMetadatas.getLoginUrl(Version.VERSION_1, role, getDevice(request.getOsName()), Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						// final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						final boolean hasrefer=(boolean) jobj.get("hasRefer");
						if (code.equalsIgnoreCase("S00")) {
							final double balance = (double) jobj.getJSONObject("details").getJSONObject("accountDetail")
									.get("balance");
							final long accountNumber = (long) jobj.getJSONObject("details")
									.getJSONObject("accountDetail").get("accountNumber");
							final String accountTypeDescription = (String) jobj.getJSONObject("details")
									.getJSONObject("accountDetail").getJSONObject("accountType").get("description");
							final String accountTypeName = (String) jobj.getJSONObject("details")
									.getJSONObject("accountDetail").getJSONObject("accountType").get("name");
							final double accountTypeMonthlyLimit = (double) jobj.getJSONObject("details")
									.getJSONObject("accountDetail").getJSONObject("accountType").get("monthlyLimit");
							final double accountTypeDailyLimit = (double) jobj.getJSONObject("details")
									.getJSONObject("accountDetail").getJSONObject("accountType").get("dailyLimit");
							final double accountTypeBalanceLimit = (double) jobj.getJSONObject("details")
									.getJSONObject("accountDetail").getJSONObject("accountType").get("balanceLimit");
							final String username = (String) jobj.getJSONObject("details").getJSONObject("userDetail")
									.get("username");
							final String firstName = (String) jobj.getJSONObject("details").getJSONObject("userDetail")
									.get("firstName");
							final String middleName = (String) jobj.getJSONObject("details").getJSONObject("userDetail")
									.get("middleName");
							final String lastName = (String) jobj.getJSONObject("details").getJSONObject("userDetail")
									.get("lastName");
							final String address = (String) jobj.getJSONObject("details").getJSONObject("userDetail")
									.get("address");
							final String contactNo = (String) jobj.getJSONObject("details").getJSONObject("userDetail")
									.get("contactNo");
							final String userType = (String) jobj.getJSONObject("details").getJSONObject("userDetail")
									.get("userType");
							final String authority = (String) jobj.getJSONObject("details").getJSONObject("userDetail")
									.get("authority");
							final String emailStatus = (String) jobj.getJSONObject("details")
									.getJSONObject("userDetail").get("emailStatus");
							final String mobileStatus = (String) jobj.getJSONObject("details")
									.getJSONObject("userDetail").get("mobileStatus");
							final String email = (String) jobj.getJSONObject("details").getJSONObject("userDetail")
									.get("email");
							final String image = (String) jobj.getJSONObject("details").getJSONObject("userDetail")
									.get("image");
							final String sessionId = (String) jobj.getJSONObject("details").get("sessionId");
							resp.setAddress(address);
							resp.setAuthority(authority);
							resp.setContactNo(contactNo);
							resp.setEmail(email);
							resp.setEmailStatus(emailStatus);
							resp.setFirstName(firstName);
							resp.setImage(image);
							resp.setLastName(lastName);
							resp.setMiddleName(middleName);
							resp.setMobileStatus(mobileStatus);
							resp.setSessionId(sessionId);
							resp.setUserType(userType);
							resp.setCode(code);
							resp.setAccountType(accountTypeDescription);
							resp.setDailyTransaction(accountTypeDailyLimit);
							resp.setMonthlyTransaction(accountTypeMonthlyLimit);
							resp.setUserType(accountTypeName);
							resp.setBalance(balance);
							resp.setAccountNumber(accountNumber);
							resp.setSuccess(true);
							resp.setUsername(username);
							resp.setMessage(message);
							resp.setHasRefer(hasrefer);
							resp.setMdexKey(UrlMetadatas.MDEX_CLIENTKEY);
							resp.setMdexToken(UrlMetadatas.MDEX_CLIENTTOKEN);
							resp.setIplEnable(Constants.IPL_ENABLE);
							resp.setIplMyPrediction(Constants.URL_TEAM_MY_PREDICT);
							resp.setIplPrediction(Constants.URL_TEAM_PREDICT);
							resp.setIplSchedule(Constants.URL_TEAM_LIST);
							resp.setMdexUsername(Constants.MDEX_USERNAME);
							resp.setMdexPassword(Constants.MDEX_PASSWORD);
							resp.setMdexUrl(Constants.MDEX_URL);
							resp.setEbsEnable(Constants.EBS_ENABLE);
							resp.setVnetEnable(Constants.VNET_ENABLE);
							resp.setRazorPayEnable(Constants.RAZORPAY_ENABLE);
							resp.setTopupImage(Constants.TOPUP_IMAGEURL);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						// resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					} 
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
//			e.printStackTrace();
//			resp.setSuccess(false);
//			resp.setCode("F00");
//			resp.setMessage("Service unavailable");
//			resp.setStatus("FAILED");
//			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;

	}
	
	private Device getDevice(String osName) {
		if(osName!=null && !osName.isEmpty()){
			switch (osName.toUpperCase()) {
			case "ANDROID":
				return Device.ANDROID;
			case "IOS":
				return Device.IOS;
			default:
				return Device.WEBSITE;
			}			
		}
		return Device.WEBSITE;
	}

	@Override
	public LoginResponse loginNew(LoginRequest request, Role role, HttpSession session) {
		LoginResponse resp = new LoginResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("username", request.getUsername());
			payload.put("password", request.getPassword());
			payload.put("ipAddress", request.getIpAddress());
			payload.put("registrationId", request.getRegistrationId());
			payload.put("validate", request.isValidate());
			payload.put("mobileToken", request.getMobileToken());
            
			Client client = Client.create();
			WebResource webResource = client
					.resource(UrlMetadatas.getLoginUrl(Version.VERSION_1, role, Device.WEBSITE, Language.ENGLISH));

			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);

			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						// final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							
							final double balance = (double) jobj.getJSONObject("details").getJSONObject("accountDetail")
									.get("balance");
							final long accountNumber = (long) jobj.getJSONObject("details")
									.getJSONObject("accountDetail").get("accountNumber");
							final String accountTypeDescription = (String) jobj.getJSONObject("details")
									.getJSONObject("accountDetail").getJSONObject("accountType").get("description");
							final String accountTypeName = (String) jobj.getJSONObject("details")
									.getJSONObject("accountDetail").getJSONObject("accountType").get("name");

							final double accountTypeMonthlyLimit = (double) jobj.getJSONObject("details")
									.getJSONObject("accountDetail").getJSONObject("accountType").get("monthlyLimit");
							final double accountTypeDailyLimit = (double) jobj.getJSONObject("details")
									.getJSONObject("accountDetail").getJSONObject("accountType").get("dailyLimit");
							final double accountTypeBalanceLimit = (double) jobj.getJSONObject("details")
									.getJSONObject("accountDetail").getJSONObject("accountType").get("balanceLimit");

							final String username = (String) jobj.getJSONObject("details").getJSONObject("userDetail")
									.get("username");
							final String firstName = (String) jobj.getJSONObject("details").getJSONObject("userDetail")
									.get("firstName");
							final String middleName = (String) jobj.getJSONObject("details").getJSONObject("userDetail")
									.get("middleName");
							final String lastName = (String) jobj.getJSONObject("details").getJSONObject("userDetail")
									.get("lastName");
							final String address = (String) jobj.getJSONObject("details").getJSONObject("userDetail")
									.get("address");
							final String contactNo = (String) jobj.getJSONObject("details").getJSONObject("userDetail")
									.get("contactNo");
							final String userType = (String) jobj.getJSONObject("details").getJSONObject("userDetail")
									.get("userType");
							final String authority = (String) jobj.getJSONObject("details").getJSONObject("userDetail")
									.get("authority");
							final String emailStatus = (String) jobj.getJSONObject("details")
									.getJSONObject("userDetail").get("emailStatus");
							final String mobileStatus = (String) jobj.getJSONObject("details")
									.getJSONObject("userDetail").get("mobileStatus");
							final String email = (String) jobj.getJSONObject("details").getJSONObject("userDetail")
									.get("email");
							final String image = (String) jobj.getJSONObject("details").getJSONObject("userDetail")
									.get("image");
							final String sessionId = (String) jobj.getJSONObject("details").get("sessionId");

							/*resp.setAddress(address);
							resp.setAuthority(authority);
							resp.setContactNo(contactNo);
							resp.setEmail(email);
							resp.setEmailStatus(emailStatus);
							resp.setFirstName(firstName);
							resp.setImage(image);
							resp.setLastName(lastName);
							resp.setMiddleName(middleName);
							resp.setMobileStatus(mobileStatus);
							resp.setSessionId(sessionId);
							resp.setUserType(userType);*/
							resp.setCode(code);
							/*resp.setAccountType(accountTypeDescription);
							resp.setDailyTransaction(accountTypeDailyLimit);
							resp.setMonthlyTransaction(accountTypeMonthlyLimit);
							resp.setUserType(accountTypeName);
							resp.setBalance(balance);
							resp.setAccountNumber(accountNumber);*/
							resp.setSuccess(true);
//							resp.setUsername(username);
							resp.setMessage(message);
							session.setAttribute("sessionId", sessionId);
							session.setAttribute("balance", balance);
							session.setAttribute("firstName", firstName);
							session.setAttribute("userDetails", resp);
							session.setAttribute("username", username);
							session.setAttribute("emailId", email);
							session.setAttribute("accountId", accountNumber);
							session.setAttribute("dailyTransaction", accountTypeDailyLimit);
							session.setAttribute("monthlyTransaction", accountTypeMonthlyLimit);
							
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						// resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					} else {
						resp.setSuccess(false);
						resp.setCode("F00");
						resp.setMessage("Service unavailable");
						resp.setStatus("FAILED");
						resp.setResponse(APIUtils.getFailedJSON().toString());
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;

	}

	public ForgetPasswordUserResponse forgetPasswordUserRequest(ForgetPasswordUserRequest request) {

		ForgetPasswordUserResponse resp = new ForgetPasswordUserResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", "");
			payload.put("username", request.getUsername());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getPasswordOtpUrl(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						final Object details = (Object) jobj.get("details");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setDetails(details);
						resp.setResponse(strResponse);
					} 
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public ChangePasswordWithOtpResponse changePasswordWithOtpRequest(ChangePasswordWithOtpRequest request) {
		ChangePasswordWithOtpResponse resp = new ChangePasswordWithOtpResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", "");
			payload.put("password", "");
			payload.put("username", request.getUsername());
			payload.put("newPassword", request.getNewPassword());
			payload.put("confirmPassword", request.getConfirmPassword());
			payload.put("key", request.getKey());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getChangePasswordWithOTPUrl(Version.VERSION_1,
					Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
//						resp.setDetails(details);
						resp.setResponse(strResponse);
					} else {
						resp.setSuccess(false);
						resp.setCode("F00");
						resp.setMessage("Service unavailable");
						resp.setStatus("FAILED");
						resp.setResponse(APIUtils.getFailedJSON().toString());
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public ResendForgotPasswordOtpResponse resendForgotPasswordOtpRequest(ResendForgotPasswordOtpRequest request) {
		ResendForgotPasswordOtpResponse resp = new ResendForgotPasswordOtpResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("mobileNumber", request.getMobileNumber());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.resetPasswordOtpUrl(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					} 
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}
	
	@Override
	public ResendForgotPasswordOtpResponse resendDeviceBindingOtpRequest(ResendForgotPasswordOtpRequest request) {
		ResendForgotPasswordOtpResponse resp = new ResendForgotPasswordOtpResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("mobileNumber", request.getMobileNumber());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.deviceBindingUrl(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					} else {
						resp.setSuccess(false);
						resp.setCode("F00");
						resp.setMessage("Service unavailable");
						resp.setStatus("FAILED");
						resp.setResponse(APIUtils.getFailedJSON().toString());
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

}
