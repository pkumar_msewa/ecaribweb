package com.payqwikweb.app.api.impl;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jettison.json.JSONObject;

import com.payqwikweb.app.api.IHotelApi;
import com.payqwikweb.app.metadatas.UrlMetadatas;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Language;
import com.payqwikweb.app.model.ResponseStatus;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.busdto.BusCityListDTO;
import com.payqwikweb.app.model.hotel.request.HotelAvalabilityRequest;
import com.payqwikweb.app.model.hotel.request.HotelBookingRequest;
import com.payqwikweb.app.model.hotel.request.HotelInfoRequest;
import com.payqwikweb.app.model.hotel.request.HotellistRequest;
import com.payqwikweb.app.model.hotel.response.HotelResponseDTO;
import com.payqwikweb.app.utils.SecurityUtils;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class HotelApi implements IHotelApi{

	@Override
	public HotelResponseDTO getAllCityList(String sessionId) {
		HotelResponseDTO resp=new HotelResponseDTO();

		List<BusCityListDTO> list=new ArrayList<>();
		try {
			JSONObject payload = new JSONObject();
			payload.put("clientIp", "49.204.86.246");
			payload.put("cllientKey", UrlMetadatas.MDEX_CLIENTKEY);
			payload.put("clientToken", UrlMetadatas.MDEX_CLIENTTOKEN);
			payload.put("clientApiName", "VPayQwik");
//			payload.put("sourceKey",req.getSourceKey());
			Client client = Client.create();
			WebResource webResource = client
					.resource(UrlMetadatas.getBusSourceCityList(Role.ClIENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);

			String strResponse = response.getEntity(String.class);
			System.out.println(strResponse);
		/*	JSONObject jobj=new JSONObject(strResponse);
			final String code=jobj.getString("code");
			if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
				String strDetails=jobj.getString("details");
				if (strDetails!=null) {
					JSONArray cList=jobj.getJSONArray("details");

					for (int i = 0; i < cList.length(); i++) {
						BusCityListDTO dto=new BusCityListDTO();
						dto.setCityId(cList.getJSONObject(i).getLong("cityId"));
						dto.setCityName(cList.getJSONObject(i).getString("cityName"));
						list.add(dto);
					}
				}
				resp.setCode(code);
				resp.setMessage(jobj.getString("message"));
				resp.setDetails(list);
				resp.setStatus(ResponseStatus.SUCCESS.getKey());
			}*/
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			resp.setMessage("Service Unavailable");
			resp.setDetails(null);
			resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
		}
		return resp;
	}

	
	
	@Override
	public HotelResponseDTO getavailableHotels(HotellistRequest request) {
		HotelResponseDTO resp=new HotelResponseDTO();

		try {

			Client client = Client.create();
			WebResource webResource = client
					.resource(UrlMetadatas.getHotelList(Role.ClIENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(request.toString())).post(ClientResponse.class, request.getJson());

			String strResponse = response.getEntity(String.class);
			System.out.println(strResponse);
			JSONObject jobj=new JSONObject(strResponse);
			final String code=jobj.getString("code");
			if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
				String strDetails=jobj.getString("details");
				/*if (strDetails!=null) {
					JSONArray cList=jobj.getJSONArray("details");

					for (int i = 0; i < cList.length(); i++) {
						BusCityListDTO dto=new BusCityListDTO();
						dto.setCityId(cList.getJSONObject(i).getLong("cityId"));
						dto.setCityName(cList.getJSONObject(i).getString("cityName"));
						list.add(dto);
					}
				}*/
				resp.setCode(code);
				resp.setMessage(jobj.getString("message"));
				resp.setDetails(strDetails);
				resp.setStatus(ResponseStatus.SUCCESS.getKey());
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			resp.setMessage("Service Unavailable");
			resp.setDetails(null);
			resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
		}
		return resp;
	}

	
	@Override
	public HotelResponseDTO gethotelInfo(HotelInfoRequest request) {
		HotelResponseDTO resp=new HotelResponseDTO();

		try {

			Client client = Client.create();
			WebResource webResource = client
					.resource(UrlMetadatas.getHotelInfo(Role.ClIENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(request.toString())).post(ClientResponse.class, request.getJson());

			String strResponse = response.getEntity(String.class);
			System.out.println(strResponse);
			JSONObject jobj=new JSONObject(strResponse);
			final String code=jobj.getString("code");
			if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
				String strDetails=jobj.getString("details");
				/*if (strDetails!=null) {
					JSONArray cList=jobj.getJSONArray("details");

					for (int i = 0; i < cList.length(); i++) {
						BusCityListDTO dto=new BusCityListDTO();
						dto.setCityId(cList.getJSONObject(i).getLong("cityId"));
						dto.setCityName(cList.getJSONObject(i).getString("cityName"));
						list.add(dto);
					}
				}*/
				resp.setCode(code);
				resp.setMessage(jobj.getString("message"));
				
				/*org.json.JSONObject jsonObject=new org.json.JSONObject(strDetails);
				org.json.simple.JSONArray deatis =new org.json.simple.JSONArray();
				deatis.add(jsonObject);
				StringWriter stringWriter=new StringWriter();
				stringWriter.write(strDetails);
				deatis.writeJSONString(stringWriter);
			    Object object=deatis;
				System.out.println("jsoopp"+object);*/
				
				resp.setDetails(strDetails);
				resp.setStatus(ResponseStatus.SUCCESS.getKey());
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			resp.setMessage("Service Unavailable");
			resp.setDetails(null);
			resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
		}
		return resp;
	}
	
	
	@Override
	public HotelResponseDTO gethotelAvailableity(HotelAvalabilityRequest request) {

		HotelResponseDTO resp=new HotelResponseDTO();

		try {
			Client client = Client.create();
			WebResource webResource = client
					.resource(UrlMetadatas.getHotelAvailablity(Role.ClIENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(request.toString())).post(ClientResponse.class, request.getJson());

			String strResponse = response.getEntity(String.class);
			System.out.println(strResponse);
			JSONObject jobj=new JSONObject(strResponse);
			final String code=jobj.getString("code");
			if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
				String strDetails=jobj.getString("details");
				/*if (strDetails!=null) {
					JSONArray cList=jobj.getJSONArray("details");

					for (int i = 0; i < cList.length(); i++) {
						BusCityListDTO dto=new BusCityListDTO();
						dto.setCityId(cList.getJSONObject(i).getLong("cityId"));
						dto.setCityName(cList.getJSONObject(i).getString("cityName"));
						list.add(dto);
					}
				}*/
				resp.setCode(code);
				resp.setMessage(jobj.getString("message"));
			/*	org.json.JSONObject  deatis=new org.json.JSONObject(strDetails);
				StringWriter stringWriter=new StringWriter();
				stringWriter.write(deatis.toString());
				deatis.write(stringWriter);*/
				resp.setDetails(strDetails);
				resp.setStatus(ResponseStatus.SUCCESS.getKey());
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			resp.setMessage("Service Unavailable");
			resp.setDetails(null);
			resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
		}
		return resp;
	}
	
	@Override
	public HotelResponseDTO gethotelBook(HotelBookingRequest request) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	
}
