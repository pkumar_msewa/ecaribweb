package com.payqwikweb.app.api.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import com.gcm.model.NotificationDTO;
import com.instantpay.api.IStatusCheckApi;
import com.instantpay.model.request.StatusCheckRequest;
import com.instantpay.model.response.StatusCheckResponse;
import com.instantpay.util.InstantPayConstants;
import com.payqwik.visa.util.QRRequestDTO;
import com.payqwik.visa.util.QRResponseDTO;
import com.payqwik.visa.util.VisaContants;
import com.payqwik.visa.util.VisaMerchantRequest;
import com.payqwik.visa.util.VisaMerchantTransaction;
import com.payqwikweb.app.api.IAdminApi;
import com.payqwikweb.app.api.ILoadMoneyApi;
import com.payqwikweb.app.metadatas.UrlMetadatas;
import com.payqwikweb.app.model.AnalyticsResponse;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Language;
import com.payqwikweb.app.model.ResponseStatus;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.ServiceTypeDTO;
import com.payqwikweb.app.model.ServicesDTO;
import com.payqwikweb.app.model.UserResponseDTO;
import com.payqwikweb.app.model.Version;
import com.payqwikweb.app.model.WoohooGiftCardDTO;
import com.payqwikweb.app.model.busdto.BusTicketDTO;
import com.payqwikweb.app.model.busdto.TravellerDetailsDTO;
import com.payqwikweb.app.model.flight.dto.FlightCancelableResp;
import com.payqwikweb.app.model.flight.dto.GetFlightDetailsForAdmin;
import com.payqwikweb.app.model.flight.request.FligthcancelableReq;
import com.payqwikweb.app.model.flight.response.FlightResponseDTO;
import com.payqwikweb.app.model.flight.response.FlightTicketResp;
import com.payqwikweb.app.model.flight.response.TicketsResp;
import com.payqwikweb.app.model.merchant.MerchantRegisterDTO;
import com.payqwikweb.app.model.request.AccountTypeRequest;
import com.payqwikweb.app.model.request.AddVoucherReq;
import com.payqwikweb.app.model.request.AdminUserDetails;
import com.payqwikweb.app.model.request.AllTransactionRequest;
import com.payqwikweb.app.model.request.AllUserRequest;
import com.payqwikweb.app.model.request.BlockUnBlockUserRequest;
import com.payqwikweb.app.model.request.BlockUserRequest;
import com.payqwikweb.app.model.request.ChangePasswordRequest;
import com.payqwikweb.app.model.request.DRegistrationRequest;
import com.payqwikweb.app.model.request.DateDTO;
import com.payqwikweb.app.model.request.EmailLogRequest;
import com.payqwikweb.app.model.request.LoginRequest;
import com.payqwikweb.app.model.request.MRegistrationRequest;
import com.payqwikweb.app.model.request.MerchantTransactionRequest;
import com.payqwikweb.app.model.request.MessageLogRequest;
import com.payqwikweb.app.model.request.MobileOTPRequest;
import com.payqwikweb.app.model.request.PagingDTO;
import com.payqwikweb.app.model.request.PartnerDetailsRequest;
import com.payqwikweb.app.model.request.PromoCodeRequest;
import com.payqwikweb.app.model.request.ReceiptsRequest;
import com.payqwikweb.app.model.request.RefundDTO;
import com.payqwikweb.app.model.request.SessionDTO;
import com.payqwikweb.app.model.request.SingleUserRequest;
import com.payqwikweb.app.model.request.TransactionRequest;
import com.payqwikweb.app.model.request.TreatCardPlanRequest;
import com.payqwikweb.app.model.request.TreatCardRegisterList;
import com.payqwikweb.app.model.request.UserAnalytics;
import com.payqwikweb.app.model.request.UserResponse;
import com.payqwikweb.app.model.request.UserTransactionRequest;
import com.payqwikweb.app.model.request.bus.IsCancellableReq;
import com.payqwikweb.app.model.request.reports.MISDetailRequest;
import com.payqwikweb.app.model.response.AccountTypeResponse;
import com.payqwikweb.app.model.response.AddDonateeResponse;
import com.payqwikweb.app.model.response.AddMerchantResponse;
import com.payqwikweb.app.model.response.AddVoucherResponse;
import com.payqwikweb.app.model.response.AllTransactionResponse;
import com.payqwikweb.app.model.response.AllUserResponse;
import com.payqwikweb.app.model.response.BanksDto;
import com.payqwikweb.app.model.response.BlockUnBlockUserResponse;
import com.payqwikweb.app.model.response.BlockUserResponse;
import com.payqwikweb.app.model.response.ChangePasswordResponse;
import com.payqwikweb.app.model.response.CommissionDTO;
import com.payqwikweb.app.model.response.EBSRedirectResponse;
import com.payqwikweb.app.model.response.EmailLogResponse;
import com.payqwikweb.app.model.response.GCMResponse;
import com.payqwikweb.app.model.response.LoginResponse;
import com.payqwikweb.app.model.response.MessageLogResponse;
import com.payqwikweb.app.model.response.MobileOTPResponse;
import com.payqwikweb.app.model.response.NEFTResponse;
import com.payqwikweb.app.model.response.PredictListResponse;
import com.payqwikweb.app.model.response.ReceiptsResponse;
import com.payqwikweb.app.model.response.RegistrationResponse;
import com.payqwikweb.app.model.response.ServiceTypeResponse;
import com.payqwikweb.app.model.response.TListResponse;
import com.payqwikweb.app.model.response.TransactionReportResponse;
import com.payqwikweb.app.model.response.TreatCardPlansResponse;
import com.payqwikweb.app.model.response.TreatCardRegisterListResponse;
import com.payqwikweb.app.model.response.UserTransactionResponse;
import com.payqwikweb.app.model.response.VRedirectResponse;
import com.payqwikweb.app.model.response.VisaSignUpResponse;
import com.payqwikweb.app.model.response.bus.CancelTicketResp;
import com.payqwikweb.app.model.response.bus.IsCancellableResp;
import com.payqwikweb.app.model.response.reports.MISResponce;
import com.payqwikweb.app.model.response.reports.WalletResponse;
import com.payqwikweb.app.utils.SecurityUtils;
import com.payqwikweb.model.admin.TListDTO;
import com.payqwikweb.model.app.request.AgentRegistrationRequest;
import com.payqwikweb.model.app.request.OffersRequest;
import com.payqwikweb.model.app.request.TravellerFlightDetails;
import com.payqwikweb.model.app.response.AddOffersResponse;
import com.payqwikweb.model.app.response.PaginationDTO;
import com.payqwikweb.model.app.response.TransactionM2PDTO;
import com.payqwikweb.model.app.response.VNetStatusResponse;
import com.payqwikweb.model.request.thirdpartyService.RequestRefund;
import com.payqwikweb.model.web.GiftCardDetailResponse;
import com.payqwikweb.util.APIUtils;
import com.payqwikweb.util.ConvertUtil;
import com.payqwikweb.util.JSONParserUtil;
import com.payqwikweb.validation.CommonValidation;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.thirdparty.model.ResponseDTO;

public class AdminApi implements IAdminApi {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	private SimpleDateFormat simformat = new SimpleDateFormat("dd-MM-yyyy");

	private final ILoadMoneyApi loadMoneyApi;
	private final IStatusCheckApi statusCheckApi;

	public AdminApi(ILoadMoneyApi loadMoneyApi, IStatusCheckApi statusCheckApi) {
		this.loadMoneyApi = loadMoneyApi;
		this.statusCheckApi = statusCheckApi;
	}

	@Override
	public LoginResponse login(LoginRequest request) {
		LoginResponse resp = new LoginResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("username", request.getUsername());
			payload.put("password", request.getPassword());
			payload.put("ipAddress", request.getIpAddress());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getLoginUrl(Version.VERSION_1, Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							final String sessionId = (String) jobj.getJSONObject("details").get("sessionId");
							resp.setSuccess(true);
							resp.setSessionId(sessionId);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public ServiceTypeResponse getServiceType() {
		ServiceTypeResponse resp = new ServiceTypeResponse();
		try {
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getServiceTypesURL(Version.VERSION_1, Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456")).get(ClientResponse.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							List<ServiceTypeDTO> serviceTypeDTOs = new ArrayList<>();
							resp.setSuccess(true);
							org.json.JSONArray details = JSONParserUtil.getArray(jobj, "details");
							if (details != null && details.length() > 0) {
								for (int i = 0; i < details.length(); i++) {
									ServiceTypeDTO s = new ServiceTypeDTO();
									org.json.JSONObject temp = (org.json.JSONObject) details.get(i);
									s.setId(JSONParserUtil.getLong(temp, "id"));
									s.setName(JSONParserUtil.getString(temp, "name"));
									s.setDescription(JSONParserUtil.getString(temp, "description"));
									serviceTypeDTOs.add(s);
								}
								resp.setServiceDTOs(serviceTypeDTOs);
							}
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
		}
		return resp;
	}

	@Override
	public ServiceTypeResponse getService() {
		ServiceTypeResponse resp = new ServiceTypeResponse();
		try {
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getServiceURL(Version.VERSION_1, Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456")).get(ClientResponse.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							List<ServicesDTO> serviceTypeDTOs = new ArrayList<>();
							resp.setSuccess(true);
							org.json.JSONArray details = JSONParserUtil.getArray(jobj, "details");
							if (details != null && details.length() > 0) {
								for (int i = 0; i < details.length(); i++) {
									ServicesDTO s = new ServicesDTO();
									org.json.JSONObject temp = (org.json.JSONObject) details.get(i);
									s.setCode(JSONParserUtil.getString(temp, "code"));
									s.setDescription(JSONParserUtil.getString(temp, "description"));
									serviceTypeDTOs.add(s);
								}
								resp.setServicesDTOs(serviceTypeDTOs);
							}
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
		}
		return resp;
	}

	@Override
	public ServiceTypeResponse getServicesById(PromoCodeRequest req) {
		ServiceTypeResponse resp = new ServiceTypeResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("serviceTypeId", req.getServiceId());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getServicesById(Version.VERSION_1, Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456")).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							List<ServicesDTO> serviceTypeDTOs = new ArrayList<>();
							resp.setSuccess(true);
							org.json.JSONArray details = JSONParserUtil.getArray(jobj, "details");
							if (details != null && details.length() > 0) {
								for (int i = 0; i < details.length(); i++) {
									ServicesDTO s = new ServicesDTO();
									org.json.JSONObject temp = (org.json.JSONObject) details.get(i);
									s.setCode(JSONParserUtil.getString(temp, "code"));
									s.setDescription(JSONParserUtil.getString(temp, "description"));
									serviceTypeDTOs.add(s);
								}
								resp.setServicesDTOs(serviceTypeDTOs);
							}
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
		}
		return resp;
	}

	@Override
	public ResponseDTO verifyKYCRequest(MerchantRegisterDTO merchantRegisterDTO) {
		ResponseDTO resp = new ResponseDTO();
		try {
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getVerifyKYCRequest(Version.VERSION_1, Role.MERCHANT,
					Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456"))
					.post(ClientResponse.class, merchantRegisterDTO.toJSON());
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);

					if (jobj != null) {
						final String code = (String) jobj.get("code");
						final String message = JSONParserUtil.getString(jobj, "message");
						resp.setCode(code);
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							resp.setSuccess(true);
							resp.setMessage(message);
						} else if (code.equalsIgnoreCase("R01")) {
							resp.setSuccess(false);
							resp.setCode(code);
							resp.setMessage("You're not a Vijaya Bank Merchant");
						} else {
							resp.setSuccess(false);
							resp.setCode(code);
							resp.setMessage(message);
						}
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
				}

				resp.setDetails(strResponse);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
		}
		return resp;
	}

	@Override
	public ResponseDTO verifyKYCProcess(MerchantRegisterDTO merchantRegisterDTO) {
		ResponseDTO resp = new ResponseDTO();
		try {
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getVerifyKYCProcess(Version.VERSION_1, Role.MERCHANT,
					Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456"))
					.post(ClientResponse.class, merchantRegisterDTO.toJSON());
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							resp.setSuccess(true);
						}
						resp.setCode(code);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
				}
				resp.setDetails(strResponse);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
		}
		return resp;
	}

	@Override
	public ResponseDTO generateMerchantPassword(MerchantRegisterDTO merchantRegisterDTO) {
		ResponseDTO resp = new ResponseDTO();
		try {
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getGeneratePasswordRequest(Version.VERSION_1,
					Role.MERCHANT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456"))
					.post(ClientResponse.class, merchantRegisterDTO.toJSON());
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);

					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							resp.setSuccess(true);
						}
						resp.setCode(code);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
				}
				resp.setDetails(strResponse);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
		}
		return resp;
	}

	@Override
	public AllTransactionResponse getAllTransaction(AllTransactionRequest request) {
		AllTransactionResponse resp = new AllTransactionResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("page", request.getPage());
			payload.put("size", request.getSize());
			payload.put("fromDate", request.getStartDate());
			payload.put("toDate", request.getEndDate());
			payload.put("reportType", request.getReportType());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getAllTransactionFilteredUrl(Version.VERSION_1,
					Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						org.json.JSONArray totalTransactionArray = null;
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							resp.setSuccess(true);
							totalTransactionArray = jobj.getJSONArray("details");
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
						resp.setJsonArray(totalTransactionArray);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public AllTransactionResponse getSettlementTransactions(AllTransactionRequest request) {
		AllTransactionResponse resp = new AllTransactionResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("startDate", request.getStartDate());
			payload.put("endDate", request.getEndDate());
			payload.put("reportType", request.getReportType().toUpperCase());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getSettlementsTransactionURL(Version.VERSION_1,
					Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						org.json.JSONArray totalTransactionArray = null;
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							resp.setSuccess(true);
							totalTransactionArray = jobj.getJSONArray("details");
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
						resp.setJsonArray(totalTransactionArray);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public AllTransactionResponse getPromoTransaction(AllTransactionRequest request) {
		AllTransactionResponse resp = new AllTransactionResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getPromoTransactionsURL(Version.VERSION_1,
					Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (code.equalsIgnoreCase("S00")) {
							List<AdminUserDetails> userList = new ArrayList<AdminUserDetails>();
							resp.setSuccess(true);
							org.json.JSONArray details = JSONParserUtil.getArray(jobj, "details");
							if (details != null && details.length() > 0) {
								for (int i = 0; i < details.length(); i++) {
									org.json.JSONObject t = details.getJSONObject(i);
									AdminUserDetails userDetails = new AdminUserDetails();
									long milliseconds = JSONParserUtil.getLong(t, "date");
									Calendar calendar = Calendar.getInstance();
									calendar.setTimeInMillis(milliseconds);
									userDetails.setDateOfTransaction(simformat.format(calendar.getTime()));
									userDetails.setTransactionRefNo(JSONParserUtil.getString(t, "transactionRefNo"));
									userDetails.setCurrentBalance(
											String.valueOf(JSONParserUtil.getDouble(t, "currentBalance")));
									userDetails.setContactNo(JSONParserUtil.getString(t, "contactNo"));
									userDetails.setAmount(String.valueOf(JSONParserUtil.getDouble(t, "amount")));
									userDetails.setFirstName(JSONParserUtil.getString(t, "description"));
									userDetails.setEmail(JSONParserUtil.getString(t, "email"));
									userDetails.setCurrentBalance(JSONParserUtil.getString(t, "currentBalance"));
									userDetails.setStatus(JSONParserUtil.getString(t, "status"));
									userList.add(userDetails);
								}
								resp.setList(userList);
							} else {
								resp.setSuccess(false);
								resp.setMessage("Transaction List Empty");
							}
						} else {
							resp.setSuccess(false);
							resp.setMessage("Please try Later");
						}
					} else {
						resp.setSuccess(false);
					}
				} else {
					resp.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
		}
		return resp;
	}

	@Override
	public AllTransactionResponse getPromoTransactionFiltered(AllTransactionRequest request) {
		AllTransactionResponse resp = new AllTransactionResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("fromDate", request.getStartDate());
			payload.put("toDate", request.getEndDate());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getPromoTransactionsFilteredURL(Version.VERSION_1,
					Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (code.equalsIgnoreCase("S00")) {
							List<AdminUserDetails> userList = new ArrayList<AdminUserDetails>();
							resp.setSuccess(true);
							org.json.JSONArray details = JSONParserUtil.getArray(jobj, "details");
							if (details != null && details.length() > 0) {
								for (int i = 0; i < details.length(); i++) {
									org.json.JSONObject t = details.getJSONObject(i);
									AdminUserDetails userDetails = new AdminUserDetails();
									long milliseconds = JSONParserUtil.getLong(t, "date");
									Calendar calendar = Calendar.getInstance();
									calendar.setTimeInMillis(milliseconds);
									userDetails.setDateOfTransaction(simformat.format(calendar.getTime()));
									userDetails.setTransactionRefNo(JSONParserUtil.getString(t, "transactionRefNo"));
									userDetails.setCurrentBalance(
											String.valueOf(JSONParserUtil.getDouble(t, "currentBalance")));
									userDetails.setContactNo(JSONParserUtil.getString(t, "contactNo"));
									userDetails.setAmount(String.valueOf(JSONParserUtil.getDouble(t, "amount")));
									userDetails.setFirstName(JSONParserUtil.getString(t, "description"));
									userDetails.setEmail(JSONParserUtil.getString(t, "email"));
									userDetails.setStatus(JSONParserUtil.getString(t, "status"));
									userList.add(userDetails);
								}
								resp.setList(userList);
							} else {
								resp.setSuccess(false);
								resp.setMessage("Transaction List Empty");
							}
						} else {
							resp.setSuccess(false);
							resp.setMessage("Please try Later");
						}
					} else {
						resp.setSuccess(false);
					}
				} else {
					resp.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
		}
		return resp;
	}

	@Override
	public UserTransactionResponse getUserTransactionValues(SessionDTO dto) {
		UserTransactionResponse resp = new UserTransactionResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", dto.getSessionId());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getValuesListURL(Version.VERSION_1, Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public AllUserResponse getAllUser(AllUserRequest request) {
		AllUserResponse resp = new AllUserResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("page", request.getPage());
			payload.put("size", request.getSize());
			payload.put("userStatus", request.getStatus().getValue());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getAllUserUrl(Version.VERSION_1, Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						final org.json.JSONObject details = jobj.getJSONObject("details");
						final org.json.JSONArray totalUser = (org.json.JSONArray) jobj.getJSONObject("details")
								.getJSONArray("content");
						long numberOfElements = details.getLong("numberOfElements");
						boolean firstPage = details.getBoolean("firstPage");
						boolean lastPage = details.getBoolean("lastPage");
						long size = details.getLong("size");
						long totalPages = details.getLong("totalPages");
						resp.setTotalPages(totalPages);
						resp.setSize(size);
						resp.setNumberOfElements(numberOfElements);
						resp.setFirstPage(firstPage);
						resp.setLastPage(lastPage);
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
						resp.setTotalUser(totalUser.length());
						resp.setJsonArray(totalUser);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public AllUserResponse getAllMerchants(AllUserRequest request) {
		AllUserResponse resp = new AllUserResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("page", request.getPage());
			payload.put("size", request.getSize());
			payload.put("userStatus", request.getStatus().getValue());
			payload.put("toDate", request.getEndDate());
			payload.put("fromDate", request.getStartDate());

			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getAllMerchantURL(Version.VERSION_1, Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							final org.json.JSONArray details = jobj.getJSONArray("details");
							resp.setJsonArray(details);
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public UserTransactionResponse getUserTransaction(UserTransactionRequest request) {
		UserTransactionResponse resp = new UserTransactionResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("page", request.getPage());
			payload.put("size", request.getSize());
			payload.put("username", request.getUsername());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getUserTransactionUrl(Version.VERSION_1, Role.ADMIN,
					Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public UserTransactionResponse refundLoadMoneyTransactions(RefundDTO dto) {
		UserTransactionResponse resp = new UserTransactionResponse();
		try {
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getRefundAmountURL(Version.VERSION_1, Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(dto.toJSON().toString()))
					.post(ClientResponse.class, dto.toJSON());
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public MessageLogResponse getMessageLog(MessageLogRequest request) {
		MessageLogResponse resp = new MessageLogResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("start", request.getStartDate());
			payload.put("end", request.getEndDate());
			payload.put("sessionId", request.getSessionId());
			payload.put("username", "");
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getMessageLogUrl(Version.VERSION_1, Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						final org.json.JSONArray totalTtransaction = (org.json.JSONArray) jobj.getJSONArray("details");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
						resp.setJsonArray(totalTtransaction);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public EmailLogResponse getEmailLog(EmailLogRequest request) {
		EmailLogResponse resp = new EmailLogResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("start", request.getStartDate());
			payload.put("end", request.getEndDate());
			payload.put("username", "");
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getEmailLogUrl(Version.VERSION_1, Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						final org.json.JSONArray totalTtransaction = (org.json.JSONArray) jobj.getJSONArray("details");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
						resp.setJsonArray(totalTtransaction);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public BlockUnBlockUserResponse blockUser(BlockUnBlockUserRequest request) {
		BlockUnBlockUserResponse resp = new BlockUnBlockUserResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("username", request.getUsername());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.blockUserUrl(Version.VERSION_1, Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public BlockUnBlockUserResponse unblockUser(BlockUnBlockUserRequest request) {
		BlockUnBlockUserResponse resp = new BlockUnBlockUserResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("username", request.getUsername());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.unblockUserUrl(Version.VERSION_1, Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public AllTransactionResponse getDaily(AllTransactionRequest request) {

		AllTransactionResponse resp = new AllTransactionResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getAdminDashBoardVUrl(Version.VERSION_1, Role.ADMIN,
					Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							org.json.JSONObject details = JSONParserUtil.getObject(jobj, "details");
							resp.setaRes(JSONParserUtil.getLong(details, "aRes"));
							resp.setbRes(JSONParserUtil.getLong(details, "bRes"));
							resp.setcRes(JSONParserUtil.getLong(details, "cRes"));
							resp.setdRes(JSONParserUtil.getLong(details, "dRes"));
							resp.seteRes(JSONParserUtil.getLong(details, "eRes"));
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public AllUserResponse getAllTransactions(AllTransactionRequest request) {

		AllUserResponse resp = new AllUserResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("page", request.getPage());
			payload.put("size", request.getSize());
			payload.put("reportType", "ALL");
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getAllTransactionsAdminURL(Version.VERSION_1,
					Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						final org.json.JSONObject details = jobj.getJSONObject("details");
						final org.json.JSONArray totalUser = (org.json.JSONArray) jobj.getJSONObject("details")
								.getJSONArray("content");
						long numberOfElements = details.getLong("numberOfElements");
						boolean firstPage = details.getBoolean("firstPage");
						boolean lastPage = details.getBoolean("lastPage");
						long size = details.getLong("size");
						long totalPages = details.getLong("totalPages");
						resp.setTotalPages(totalPages);
						resp.setSize(size);
						resp.setNumberOfElements(numberOfElements);
						resp.setFirstPage(firstPage);
						resp.setLastPage(lastPage);
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
						resp.setTotalUser(totalUser.length());
						resp.setJsonArray(totalUser);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public AllTransactionResponse getSingleUserTransaction(UserTransactionRequest request) {
		AllTransactionResponse resp = new AllTransactionResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("page", 0);
			payload.put("size", 100);
			payload.put("userStatus", "");
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getUserDetailByAdminUrl(Version.VERSION_1,
					Role.ADMIN, Device.WEBSITE, Language.ENGLISH, request.getUsername()));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						final org.json.JSONArray totalTtransaction = (org.json.JSONArray) jobj.getJSONObject("details")
								.getJSONArray("content");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
						resp.setJsonArray(totalTtransaction);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public BlockUserResponse userBlock(BlockUserRequest request) {
		BlockUserResponse resp = new BlockUserResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getAdminSessionId());
			payload.put("username", request.getUsername());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getBlockUser(Version.VERSION_1, Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public AddMerchantResponse addMerchant(MRegistrationRequest request) {
		AddMerchantResponse response = new AddMerchantResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("contactNo", request.getContactNo());
			payload.put("firstName", request.getFirstName());
			payload.put("lastName", " ");
			payload.put("email", request.getEmail());
			payload.put("ipAddress", request.getIpAddress());
			payload.put("successURL", request.getSuccessURL());
			payload.put("failureURL", request.getFailureURL());
			payload.put("serviceName", "Merchant Payment");
			payload.put("image", request.getImage());
			payload.put("fixed", request.isFixed());
			payload.put("paymentGateway", request.isPaymentGateway());
			payload.put("store", request.isStore());
			payload.put("minAmount", request.getMinAmount());
			payload.put("maxAmount", request.getMaxAmount());
			payload.put("value", request.getValue());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.addMerchant(Version.VERSION_1, Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse resp = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (resp.getStatus() != 200) {
				response.setSuccess(false);
				response.setCode(ResponseStatus.FAILURE.getValue());
				response.setMessage("Service unavailable");
				response.setStatus("FAILED");
				response.setDetails(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = resp.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							response.setSuccess(true);
							final String details = (String) jobj.get("details");
							response.setDetails(details);
						} else {
							response.setSuccess(false);
						}
						response.setCode(code);
						response.setStatus(status);
						response.setMessage(message);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setSuccess(false);
			response.setCode(ResponseStatus.FAILURE.getValue());
			response.setMessage("Service unavailable");
			response.setStatus("FAILED");
			response.setDetails(APIUtils.getFailedJSON().toString());
		}
		return response;
	}

	@Override
	public AddDonateeResponse addDonatee(DRegistrationRequest request) {
		AddDonateeResponse response = new AddDonateeResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("mobileNumber", request.getMobileNumber());
			payload.put("firstName", request.getFirstName());
			payload.put("lastName", request.getLastName());
			payload.put("email", request.getEmail());
			payload.put("serviceName", "Donation Payment");
			payload.put("image", request.getImage());
			payload.put("fixed", request.isFixed());
			payload.put("value", request.getValue());
			Client client = Client.create();
			WebResource webResource = client
					.resource(UrlMetadatas.addDonatee(Version.VERSION_1, Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse resp = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (resp.getStatus() != 200) {
				response.setSuccess(false);
				response.setCode(ResponseStatus.FAILURE.getValue());
				response.setMessage("Service unavailable");
				response.setStatus("FAILED");
				response.setDetails(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = resp.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = jobj.getString("status");
						final String code = jobj.getString("code");
						final String message = jobj.getString("message");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							response.setSuccess(true);
							// final String details = (String)
							// jobj.get("details");
							// response.setDetails(details);
							response.setMessage(message);
						} else {
							response.setSuccess(false);
						}
						response.setCode(code);
						response.setStatus(status);
						response.setMessage(message);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setSuccess(false);
			response.setCode(ResponseStatus.FAILURE.getValue());
			response.setMessage("Service unavailable");
			response.setStatus("FAILED");
			// response.setDetails(APIUtils.getFailedJSON().toString());
		}
		return response;
	}

	@Override
	public AllTransactionResponse getSingleUser(UserTransactionRequest request) {
		AllTransactionResponse resp = new AllTransactionResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("page", 0);
			payload.put("size", 100000);
			payload.put("userStatus", "");
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getSingle(Version.VERSION_1, Role.ADMIN,
					Device.WEBSITE, Language.ENGLISH, request.getUsername()));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						final org.json.JSONArray totalTtransaction = (org.json.JSONArray) jobj.getJSONObject("details")
								.getJSONArray("content");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
						resp.setJsonArray(totalTtransaction);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public List<NEFTResponse> getNEFTList(SessionDTO dto, boolean flag, Date date1, Date date2) {
		List<NEFTResponse> neftResponses = new ArrayList<>();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", dto.getSessionId());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getNeftListURL(Version.VERSION_1, Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {

			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							final org.json.JSONArray list = jobj.getJSONArray("details");
							if (flag == false) {
								for (int i = 0; i < list.length(); i++) {
									NEFTResponse neft = new NEFTResponse();
									org.json.JSONObject json = list.getJSONObject(i);
									neft.setName(JSONParserUtil.getString(json, "name"));
									neft.setMobileNo(JSONParserUtil.getString(json, "mobileNumber"));
									neft.setEmail(JSONParserUtil.getString(json, "email"));
									neft.setTransactionDate(JSONParserUtil.getString(json, "transactionDate"));
									neft.setTransactionID(JSONParserUtil.getString(json, "transactionID"));
									neft.setBankName(JSONParserUtil.getString(json, "bankName"));
									neft.setIfscCode(JSONParserUtil.getString(json, "ifscCode"));
									neft.setAccountNumber(JSONParserUtil.getString(json, "beneficiaryAccountNumber"));
									neft.setAccountName(JSONParserUtil.getString(json, "beneficiaryAccountName"));
									neft.setUserAccount(JSONParserUtil.getString(json, "virtualAccount"));
									neft.setBankAccount(JSONParserUtil.getString(json, "bankVirtualAccount"));
									neft.setAmount(JSONParserUtil.getString(json, "amount"));
									neft.setStatus(JSONParserUtil.getString(json, "status"));
									neftResponses.add(neft);
								}
							} else {
								for (int i = 0; i < list.length(); i++) {
									org.json.JSONObject json = list.getJSONObject(i);
									String d = JSONParserUtil.getString(json, "transactionDate");
									Date d3 = sdf.parse(d);
									Calendar calender = Calendar.getInstance();
									calender.setTime(d3);
									long m = calender.getTimeInMillis();
									calender.setTimeInMillis(m);
									Date txdate = calender.getTime();
									if (txdate.after(date1) && txdate.before(date2)) {
										NEFTResponse neft = new NEFTResponse();
										neft.setName(JSONParserUtil.getString(json, "name"));
										neft.setMobileNo(JSONParserUtil.getString(json, "mobileNumber"));
										neft.setEmail(JSONParserUtil.getString(json, "email"));
										neft.setTransactionDate(JSONParserUtil.getString(json, "transactionDate"));
										neft.setTransactionID(JSONParserUtil.getString(json, "transactionID"));
										neft.setBankName(JSONParserUtil.getString(json, "bankName"));
										neft.setIfscCode(JSONParserUtil.getString(json, "ifscCode"));
										neft.setAccountNumber(
												JSONParserUtil.getString(json, "beneficiaryAccountNumber"));
										neft.setAccountName(JSONParserUtil.getString(json, "beneficiaryAccountName"));
										neft.setUserAccount(JSONParserUtil.getString(json, "virtualAccount"));
										neft.setBankAccount(JSONParserUtil.getString(json, "bankVirtualAccount"));
										neft.setAmount(JSONParserUtil.getString(json, "amount"));
										neft.setStatus(JSONParserUtil.getString(json, "status"));
										neftResponses.add(neft);
									}
								}
							}
						}
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();

		}
		return neftResponses;

	}

	@Override
	public List<NEFTResponse> getNEFTList(SessionDTO dto) {
		List<NEFTResponse> neftResponses = new ArrayList<>();
		JSONObject payload = new JSONObject();
		try {
			payload.put("sessionId", dto.getSessionId());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getNeftListMerchantURL(Version.VERSION_1, Role.ADMIN,
					Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {

			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							final org.json.JSONArray list = jobj.getJSONArray("details");
							for (int i = 0; i < list.length(); i++) {
								NEFTResponse neft = new NEFTResponse();
								org.json.JSONObject json = list.getJSONObject(i);
								neft.setName(JSONParserUtil.getString(json, "name"));
								neft.setMobileNo(JSONParserUtil.getString(json, "mobileNumber"));
								neft.setEmail(JSONParserUtil.getString(json, "email"));
								neft.setTransactionDate(JSONParserUtil.getString(json, "transactionDate"));
								neft.setTransactionID(JSONParserUtil.getString(json, "transactionID"));
								neft.setBankName(JSONParserUtil.getString(json, "bankName"));
								neft.setIfscCode(JSONParserUtil.getString(json, "ifscCode"));
								neft.setAccountNumber(JSONParserUtil.getString(json, "beneficiaryAccountNumber"));
								neft.setAccountName(JSONParserUtil.getString(json, "beneficiaryAccountName"));
								neft.setUserAccount(JSONParserUtil.getString(json, "virtualAccount"));
								neft.setBankAccount(JSONParserUtil.getString(json, "bankVirtualAccount"));
								neft.setAmount(JSONParserUtil.getString(json, "amount"));
								neft.setStatus(JSONParserUtil.getString(json, "status"));
								neftResponses.add(neft);
							}
						}
					}
				}
			}
		} catch (org.codehaus.jettison.json.JSONException | JSONException e) {
			e.printStackTrace();
		}
		return neftResponses;

	}

	@Override
	public List<NEFTResponse> getNEFTListFiltered(DateDTO request, boolean flag) {
		List<NEFTResponse> neftResponses = new ArrayList<>();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("fromDate", request.getFromDate());
			payload.put("toDate", request.getToDate());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getNeftListFilteredURL(Version.VERSION_1, Role.ADMIN,
					Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {

			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							final org.json.JSONArray list = jobj.getJSONArray("details");
							if (flag == false) {
								for (int i = 0; i < list.length(); i++) {
									NEFTResponse neft = new NEFTResponse();
									org.json.JSONObject json = list.getJSONObject(i);
									neft.setName(JSONParserUtil.getString(json, "name"));
									neft.setMobileNo(JSONParserUtil.getString(json, "mobileNumber"));
									neft.setEmail(JSONParserUtil.getString(json, "email"));
									neft.setTransactionDate(JSONParserUtil.getString(json, "transactionDate"));
									neft.setTransactionID(JSONParserUtil.getString(json, "transactionID"));
									neft.setBankName(JSONParserUtil.getString(json, "bankName"));
									neft.setIfscCode(JSONParserUtil.getString(json, "ifscCode"));
									neft.setAccountNumber(JSONParserUtil.getString(json, "beneficiaryAccountNumber"));
									neft.setAccountName(JSONParserUtil.getString(json, "beneficiaryAccountName"));
									neft.setUserAccount(JSONParserUtil.getString(json, "virtualAccount"));
									neft.setBankAccount(JSONParserUtil.getString(json, "bankVirtualAccount"));
									neft.setAmount(JSONParserUtil.getString(json, "amount"));
									neft.setStatus(JSONParserUtil.getString(json, "status"));
									neftResponses.add(neft);
								}
							} else {
								for (int i = 0; i < list.length(); i++) {
									org.json.JSONObject json = list.getJSONObject(i);
									NEFTResponse neft = new NEFTResponse();
									neft.setName(JSONParserUtil.getString(json, "name"));
									neft.setMobileNo(JSONParserUtil.getString(json, "mobileNumber"));
									neft.setEmail(JSONParserUtil.getString(json, "email"));
									neft.setTransactionDate(JSONParserUtil.getString(json, "transactionDate"));
									neft.setTransactionID(JSONParserUtil.getString(json, "transactionID"));
									neft.setBankName(JSONParserUtil.getString(json, "bankName"));
									neft.setIfscCode(JSONParserUtil.getString(json, "ifscCode"));
									neft.setAccountNumber(JSONParserUtil.getString(json, "beneficiaryAccountNumber"));
									neft.setAccountName(JSONParserUtil.getString(json, "beneficiaryAccountName"));
									neft.setUserAccount(JSONParserUtil.getString(json, "virtualAccount"));
									neft.setBankAccount(JSONParserUtil.getString(json, "bankVirtualAccount"));
									neft.setAmount(JSONParserUtil.getString(json, "amount"));
									neft.setStatus(JSONParserUtil.getString(json, "status"));
									neftResponses.add(neft);
								}
							}
						}
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();

		}
		return neftResponses;
	}

	@Override
	public List<NEFTResponse> getAgentNEFTList(SessionDTO dto) {
		List<NEFTResponse> neftResponses = new ArrayList<>();
		JSONObject payload = new JSONObject();
		try {
			payload.put("sessionId", dto.getSessionId());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getNeftListAgentURL(Version.VERSION_1, Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {

			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							final org.json.JSONArray list = jobj.getJSONArray("details");
							for (int i = 0; i < list.length(); i++) {
								NEFTResponse neft = new NEFTResponse();
								org.json.JSONObject json = list.getJSONObject(i);
								neft.setName(JSONParserUtil.getString(json, "name"));
								neft.setMobileNo(JSONParserUtil.getString(json, "mobileNumber"));
								neft.setEmail(JSONParserUtil.getString(json, "email"));
								neft.setTransactionDate(JSONParserUtil.getString(json, "transactionDate"));
								neft.setTransactionID(JSONParserUtil.getString(json, "transactionID"));
								neft.setBankName(JSONParserUtil.getString(json, "bankName"));
								neft.setIfscCode(JSONParserUtil.getString(json, "ifscCode"));
								neft.setAccountNumber(JSONParserUtil.getString(json, "beneficiaryAccountNumber"));
								neft.setAccountName(JSONParserUtil.getString(json, "beneficiaryAccountName"));
								neft.setUserAccount(JSONParserUtil.getString(json, "virtualAccount"));
								neft.setBankAccount(JSONParserUtil.getString(json, "bankVirtualAccount"));
								neft.setAmount(JSONParserUtil.getString(json, "amount"));
								neft.setStatus(JSONParserUtil.getString(json, "status"));

								neft.setImage(JSONParserUtil.getString(json, "image"));
								neft.setImageContent(JSONParserUtil.getString(json, "imageContent"));
								neft.setSenderEmailId(JSONParserUtil.getString(json, "senderEmailId"));
								neft.setSenderMobileNo(JSONParserUtil.getString(json, "senderMobileNo"));
								neft.setSenderName(JSONParserUtil.getString(json, "senderName"));
								neft.setReceiverEmailId(JSONParserUtil.getString(json, "receiverEmailId"));
								neft.setReceiverMobileNo(JSONParserUtil.getString(json, "receiverMobileNo"));
								String desc = JSONParserUtil.getString(json, "description");
								neft.setIdProofNo(JSONParserUtil.getString(json, "idProofNo"));
								if (desc.length() > 0) {
									neft.setDescription(JSONParserUtil.getString(json, "description"));
								} else {
									neft.setDescription("NA");
								}

								neftResponses.add(neft);
							}
						}
					}
				}
			}
		} catch (org.codehaus.jettison.json.JSONException | JSONException e) {
			e.printStackTrace();
		}
		return neftResponses;

	}

	@Override
	public List<NEFTResponse> getAgentNEFTListFiltered(DateDTO request, boolean flag) {
		List<NEFTResponse> neftResponses = new ArrayList<>();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("fromDate", request.getFromDate());
			payload.put("toDate", request.getToDate());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getAgentNeftListFilteredURL(Version.VERSION_1,
					Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {

			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							final org.json.JSONArray list = jobj.getJSONArray("details");
							if (flag == false) {
								for (int i = 0; i < list.length(); i++) {
									NEFTResponse neft = new NEFTResponse();
									org.json.JSONObject json = list.getJSONObject(i);
									neft.setName(JSONParserUtil.getString(json, "name"));
									neft.setMobileNo(JSONParserUtil.getString(json, "mobileNumber"));
									neft.setEmail(JSONParserUtil.getString(json, "email"));
									neft.setTransactionDate(JSONParserUtil.getString(json, "transactionDate"));
									neft.setTransactionID(JSONParserUtil.getString(json, "transactionID"));
									neft.setBankName(JSONParserUtil.getString(json, "bankName"));
									neft.setIfscCode(JSONParserUtil.getString(json, "ifscCode"));
									neft.setAccountNumber(JSONParserUtil.getString(json, "beneficiaryAccountNumber"));
									neft.setAccountName(JSONParserUtil.getString(json, "beneficiaryAccountName"));
									neft.setUserAccount(JSONParserUtil.getString(json, "virtualAccount"));
									neft.setBankAccount(JSONParserUtil.getString(json, "bankVirtualAccount"));
									neft.setAmount(JSONParserUtil.getString(json, "amount"));
									neft.setStatus(JSONParserUtil.getString(json, "status"));

									neft.setImage(JSONParserUtil.getString(json, "image"));
									neft.setImageContent(JSONParserUtil.getString(json, "imageContent"));
									neft.setSenderEmailId(JSONParserUtil.getString(json, "senderEmailId"));
									neft.setSenderMobileNo(JSONParserUtil.getString(json, "senderMobileNo"));
									neft.setSenderName(JSONParserUtil.getString(json, "senderName"));
									neft.setReceiverEmailId(JSONParserUtil.getString(json, "receiverEmailId"));
									neft.setReceiverMobileNo(JSONParserUtil.getString(json, "receiverMobileNo"));
									String desc = JSONParserUtil.getString(json, "description");
									System.out.println("description==" + desc.length());
									neft.setIdProofNo(JSONParserUtil.getString(json, "idProofNo"));
									if (desc.length() > 0) {
										neft.setDescription(JSONParserUtil.getString(json, "description"));
									} else {
										neft.setDescription("NA");
									}

									neftResponses.add(neft);
								}
							} else {
								for (int i = 0; i < list.length(); i++) {
									org.json.JSONObject json = list.getJSONObject(i);
									NEFTResponse neft = new NEFTResponse();
									neft.setName(JSONParserUtil.getString(json, "name"));
									neft.setMobileNo(JSONParserUtil.getString(json, "mobileNumber"));
									neft.setEmail(JSONParserUtil.getString(json, "email"));
									neft.setTransactionDate(JSONParserUtil.getString(json, "transactionDate"));
									neft.setTransactionID(JSONParserUtil.getString(json, "transactionID"));
									neft.setBankName(JSONParserUtil.getString(json, "bankName"));
									neft.setIfscCode(JSONParserUtil.getString(json, "ifscCode"));
									neft.setAccountNumber(JSONParserUtil.getString(json, "beneficiaryAccountNumber"));
									neft.setAccountName(JSONParserUtil.getString(json, "beneficiaryAccountName"));
									neft.setUserAccount(JSONParserUtil.getString(json, "virtualAccount"));
									neft.setBankAccount(JSONParserUtil.getString(json, "bankVirtualAccount"));
									neft.setAmount(JSONParserUtil.getString(json, "amount"));
									neft.setStatus(JSONParserUtil.getString(json, "status"));

									neft.setImage(JSONParserUtil.getString(json, "image"));
									neft.setImageContent(JSONParserUtil.getString(json, "imageContent"));
									neft.setSenderEmailId(JSONParserUtil.getString(json, "senderEmailId"));
									neft.setSenderMobileNo(JSONParserUtil.getString(json, "senderMobileNo"));
									neft.setSenderName(JSONParserUtil.getString(json, "senderName"));
									neft.setReceiverEmailId(JSONParserUtil.getString(json, "receiverEmailId"));
									neft.setReceiverMobileNo(JSONParserUtil.getString(json, "receiverMobileNo"));
									String desc = JSONParserUtil.getString(json, "description");
									System.out.println("description==" + desc.length());
									neft.setIdProofNo(JSONParserUtil.getString(json, "idProofNo"));
									if (desc.length() > 0) {
										neft.setDescription(JSONParserUtil.getString(json, "description"));
									} else {
										neft.setDescription("NA");
									}

									neftResponses.add(neft);
								}
							}
						}
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();

		}
		return neftResponses;

	}

	@Override
	public ReceiptsResponse getSingleMerchantTransactionList(ReceiptsRequest request) {
		ReceiptsResponse resp = new ReceiptsResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("page", request.getPage());
			payload.put("size", request.getSize());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getMerchantsTransactions(Version.VERSION_1,
					Role.ADMIN, Device.WEBSITE, Language.ENGLISH, request.getUsername()));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						// final org.json.JSONArray totalTtransaction =
						// (org.json.JSONArray)
						// jobj.getJSONObject("details").getJSONArray("content");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	// @Override
	// public VisaSignUpResponse merchantSignOffAtM2P(VisaMerchantRequest
	// request) {
	// VisaSignUpResponse resp = new VisaSignUpResponse();
	// try {
	// Client client = Client.create();
	// WebResource webResource = client.resource(VisaContants.SIGN_UP_URL);
	// JSONObject payload = new JSONObject();
	// JSONObject merchant = new JSONObject();
	// JSONObject merchantBankDetail = new JSONObject();
	// JSONObject settlementBankDetail = new JSONObject();
	//
	// merchant.put("firstName", request.getFirstName());
	// merchant.put("lastName", request.getLastName());
	// merchant.put("emailAddress", request.getEmailAddress());
	// merchant.put("address1", request.getAddress1());
	// merchant.put("address2", request.getAddress2());
	// merchant.put("city", request.getCity());
	// merchant.put("state", request.getState());
	// merchant.put("pinCode", request.getPinCode());
	// merchant.put("merchantCategory", request.getMerchantBusinessType());
	// merchant.put("latitude", request.getLatitude());
	// merchant.put("longitude", request.getLongitude());
	// merchant.put("isEnabled", request.getIsEnabled());
	// merchant.put("panNo", request.getPanNo());
	// merchant.put("mobileNo", request.getMobileNo());
	// merchant.put("aggregator", request.getAggregator());
	//
	// merchantBankDetail.put("accName", request.getMerchantAccountName());
	// merchantBankDetail.put("bankName", request.getMerchantBankName());
	// merchantBankDetail.put("bankAccNo", request.getMerchantAccountNumber());
	// merchantBankDetail.put("bankIfscCode", request.getMerhantIfscCode());
	// merchantBankDetail.put("bankLocation", request.getMerchantBankLoction());
	//
	// settlementBankDetail.put("accName", request.getSettlementBankName());
	// settlementBankDetail.put("bankName", request.getSettlementBankName());
	// settlementBankDetail.put("bankAccNo",
	// request.getSettlementAccountName());
	// settlementBankDetail.put("bankIfscCode",
	// request.getSettlementIfscCode());
	// settlementBankDetail.put("bankLocation",
	// request.getSettlementBankLoction());
	//
	// payload.put("merchant", merchant);
	// payload.put("merchantBankDetail", merchantBankDetail);
	// payload.put("settlementBankDetail", settlementBankDetail);
	// System.err.println(payload.toString());
	// ClientResponse response =
	// webResource.accept("application/json").type("application/json")
	// .header("Content-Type", "application/json").header("Authorization",
	// "Basic YWRtaW46YWRtaW4=")
	// .post(ClientResponse.class, payload);
	// System.err.println("RESPONSE FROM MVISA: : "
	// +response.getEntity(String.class));
	// if (response.getStatus() != 200) {
	// resp.setFlag(false);
	// resp.setCode("F04");
	// resp.setMessage("Server Eroor M2P");
	// } else {
	// String strResponse = response.getEntity(String.class);
	//
	// if (strResponse != null) {
	// org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
	// if (jobj != null) {
	// org.json.JSONObject obj = jobj.getJSONObject("result");
	// String entityId = (String) obj.get("customerId");
	// String mVisaId = (String) obj.get("mvisaId");
	// resp.setCode("S00");
	// resp.setEntityId(entityId);
	// resp.setmVisaId(mVisaId);
	// resp.setFlag(true);
	// }
	//
	// } else {
	// resp.setMessage("Error from M2P . Service Down");
	// resp.setCode(ResponseStatus.FAILURE.getValue());
	// resp.setFlag(false);
	// }
	// }
	//
	// } catch (Exception e) {
	// }
	// return resp;
	// }

	public ChangePasswordResponse getNewPassword(ChangePasswordRequest request) {
		ChangePasswordResponse resp = new ChangePasswordResponse();
		JSONObject payload = new JSONObject();
		try {
			payload.put("sessionId", request.getSessionId());
			payload.put("username", request.getUsername());
			payload.put("password", request.getOldPassword());
			payload.put("newPassword", request.getNewPassword());
			payload.put("confirmPassword", request.getConfirmPassword());

			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.adminChangePassword(Version.VERSION_1, Role.ADMIN, Device.WEBSITE, Language.ENGLISH));

			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				logger.info("Response :: " + response.getStatus());
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				logger.info("Response : " + response.getStatus());
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					logger.info("Response Data " + strResponse.toString());
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						resp.setStatus((String) jobj.get("status"));
						resp.setMessage((String) jobj.get("message"));
						resp.setDetails((String) jobj.get("details"));
					}

				}
			}

		} catch (org.codehaus.jettison.json.JSONException | JSONException e) {
			e.printStackTrace();
		}
		return resp;
	}

	// <<<<copy>>>>>>
	@Override
	public AddMerchantResponse addVisaMerchant(VisaMerchantRequest request) {
		AddMerchantResponse response = new AddMerchantResponse();
		try {
			JSONObject payload = new JSONObject();
			// payload.put("sessionId", request.getSessionId());
			payload.put("firstName", request.getFirstName());
			payload.put("lastName", request.getLastName());
			payload.put("emailAddress", request.getEmailAddress());
			payload.put("password", request.getPassword());
			payload.put("address1", request.getAddress1());
			// payload.put("address2", request.getAddress2());
			payload.put("city", request.getCity());
			// payload.put("state", "Merchant Payment");
			payload.put("country", request.getCountry());
			payload.put("pinCode", request.getPinCode());
			// payload.put("merchantBusinessType",
			// request.getMerchantBusinessType());
			payload.put("lattitude", request.getLattitude());
			payload.put("longitude", request.getLongitude());
			// payload.put("isEnabled", request.getIsEnabled());
			payload.put("panNo", request.getPanNo());
			payload.put("mobileNo", request.getMobileNo());
			payload.put("aggregator", request.getAggregator());
			payload.put("customerId", request.getCustomerId());
			payload.put("mvisaId", request.getMvisaId());
			payload.put("aadharNo", request.getAadharNo());

			payload.put("merchantAccountName", request.getMerchantAccountName());
			payload.put("merchantBankName", request.getMerchantBankName());
			payload.put("merchantAccountNumber", request.getMerchantAccountNumber());
			payload.put("merchantBankIfscCode", request.getMerchantBankIfscCode());
			payload.put("merchantBankLocation", request.getMerchantBankLocation());

			/*
			 * payload.put("settlementAccountName",
			 * request.getSettlementAccountName());
			 * payload.put("settlementBankName",
			 * request.getSettlementBankName());
			 * payload.put("settlementAccountNumber",
			 * request.getSettlementAccountNumber());
			 * payload.put("settlementIfscCode",
			 * request.getSettlementIfscCode());
			 * payload.put("settlementBankLoction",
			 * request.getSettlementBankLocation());
			 */
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.addVisaMerchant(Version.VERSION_1, Role.MERCHANT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse resp = webResource.accept("application/json").type("application/json")
					.post(ClientResponse.class, payload);
			if (resp.getStatus() != 200) {
				response.setSuccess(false);
				response.setCode(ResponseStatus.FAILURE.getValue());
				response.setMessage("Service unavailable");
				response.setStatus("FAILED");
				response.setDetails(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = resp.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							response.setSuccess(true);
							final String details = jobj.getString("details");
							response.setDetails(details);
						} else {
							response.setSuccess(false);
						}
						response.setCode(code);
						response.setStatus(status);
						response.setMessage(message);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setSuccess(false);
			response.setCode(ResponseStatus.FAILURE.getValue());
			response.setMessage("Service unavailable");
			response.setStatus("FAILED");
			response.setDetails(APIUtils.getFailedJSON().toString());
		}
		return response;
	}

	@Override
	public AddMerchantResponse checkVisaMerchant(VisaMerchantRequest request) {
		AddMerchantResponse response = new AddMerchantResponse();
		try {
			JSONObject payload = new JSONObject();
			// payload.put("sessionId", request.getSessionId());
			payload.put("emailAddress", request.getMobileNo());

			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.checkVisaMerchant(Version.VERSION_1, Role.MERCHANT, Device.ANDROID, Language.ENGLISH));
			ClientResponse resp = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (resp.getStatus() != 200) {
				response.setSuccess(false);
				response.setCode(ResponseStatus.FAILURE.getValue());
				response.setMessage("Service unavailable");
				response.setStatus("FAILED");
				response.setDetails(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = resp.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							response.setSuccess(true);
							final String details = (String) jobj.get("details");
							response.setDetails(details);
						} else {
							response.setSuccess(false);
						}
						response.setCode(code);
						response.setStatus(status);
						response.setMessage(message);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setSuccess(false);
			response.setCode(ResponseStatus.FAILURE.getValue());
			response.setMessage("Service unavailable");
			response.setStatus("FAILED");
			response.setDetails(APIUtils.getFailedJSON().toString());
		}
		return response;
	}

	@Override
	public List<TransactionReportResponse> allVisaMerchant(String sessionId) {
		List<TransactionReportResponse> response = new ArrayList<>();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", sessionId);
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.listVisaMerchant(Version.VERSION_1, Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse resp = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (resp.getStatus() != 200) {
			} else {
				String strResponse = resp.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							org.json.JSONArray list = jobj.getJSONArray("details");
							for (int i = 0; i < list.length(); i++) {
								TransactionReportResponse txreport = new TransactionReportResponse();
								org.json.JSONObject obj = (org.json.JSONObject) list.getJSONObject(i);
								txreport.setTransactionRefNo(obj.getString("transactionRefNo"));
								txreport.setAmount((long) (obj.getDouble("amount")));
								Long dateInLong = obj.getLong("dateOfTransaction");
								Calendar calender = Calendar.getInstance();
								calender.setTimeInMillis(dateInLong);

								txreport.setDate(simformat.format(calender.getTime()));
								txreport.setDescription(obj.getString("description"));
								txreport.setStatus(obj.getString("status"));
								txreport.setUserName(obj.getString("username"));
								txreport.setEmail(obj.getString("email"));
								txreport.setContactNo(obj.getString("contactNo"));
								String retrivaLRefNo = obj.getString("retrivalReferenceNo");
								if (retrivaLRefNo.equalsIgnoreCase("null")) {
									txreport.setRetrivalReferenceNo("NA");
								} else {
									txreport.setRetrivalReferenceNo(retrivaLRefNo);
								}
								response.add(txreport);
							}
						} else {
						}

					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	@Override
	public List<TransactionReportResponse> allVisaMerchantFilter(MerchantTransactionRequest req) {
		List<TransactionReportResponse> response = new ArrayList<>();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", req.getSessionId());
			payload.put("fromDate", req.getFromDate());
			payload.put("toDate", req.getToDate());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.listVisaMerchantFilter(Version.VERSION_1, Role.ADMIN,
					Device.WEBSITE, Language.ENGLISH));
			ClientResponse resp = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (resp.getStatus() != 200) {

			} else {
				String strResponse = resp.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							org.json.JSONArray list = jobj.getJSONArray("details");
							for (int i = 0; i < list.length(); i++) {
								TransactionReportResponse txreport = new TransactionReportResponse();
								org.json.JSONObject obj = (org.json.JSONObject) list.getJSONObject(i);
								txreport.setTransactionRefNo(obj.getString("transactionRefNo"));
								txreport.setAmount((long) (obj.getDouble("amount")));
								Long dateInLong = obj.getLong("dateOfTransaction");
								Calendar calender = Calendar.getInstance();
								calender.setTimeInMillis(dateInLong);
								txreport.setDate(simformat.format(calender.getTime()));
								txreport.setDescription(obj.getString("description"));
								txreport.setStatus(obj.getString("status"));
								txreport.setUserName(obj.getString("username"));
								txreport.setEmail(obj.getString("email"));
								txreport.setContactNo(obj.getString("contactNo"));
								String retrivaLRefNo = obj.getString("retrivalReferenceNo");
								if (retrivaLRefNo.equalsIgnoreCase("null")) {
									txreport.setRetrivalReferenceNo("NA");
								} else {
									txreport.setRetrivalReferenceNo(retrivaLRefNo);
								}
								response.add(txreport);
							}
						} else {
						}

					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	@Override
	public List<VisaMerchantTransaction> getVisaMerchantTransaction(String request) {
		List<VisaMerchantTransaction> resp = new ArrayList<VisaMerchantTransaction>();
		try {
			Client client = Client.create();
			WebResource webResource = client
					.resource("http://54.169.216.42:8080/Yappay/txn-manager/fetch/success/entity/" + request
							+ "?pageNumber=0&pageSize=1000");
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("Content-Type", "application/json").header("TENANT", "M2P").get(ClientResponse.class);
			if (response.getStatus() != 200) {

			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					org.json.JSONArray arr = jobj.getJSONArray("result");
					for (int i = 0; i < arr.length(); i++) {
						VisaMerchantTransaction transaction = new VisaMerchantTransaction();
						org.json.JSONObject ob = arr.getJSONObject(i).getJSONObject("transaction");
						transaction.setAmount(ob.getString("amount"));
						transaction.setBalance(ob.getString("balance"));
						transaction.setBeneficiaryId(ob.getString("beneficiaryId"));
						transaction.setBeneficiaryName(ob.getString("beneficiaryName"));
						transaction.setBeneficiaryType(ob.getString("beneficiaryType"));
						transaction.setBeneficiaryWallet(ob.getString("beneficiaryWallet"));
						transaction.setBusinessId(ob.getString("businessId"));
						transaction.setDescription(ob.getString("description"));
						transaction.setExternalTransactionId(ob.getString("externalTransactionId"));
						transaction.setOtherPartyId(ob.getString("otherPartyId"));
						transaction.setOtherPartyName(ob.getString("otherPartyName"));
						transaction.setStatus(ob.getString("status"));
						transaction.setTime(ob.getString("time"));
						transaction.setTransactionStatus(ob.getString("transactionStatus"));
						transaction.setTransactionType(ob.getString("transactionType"));
						transaction.setTxnOrigin(ob.getString("txnOrigin"));
						transaction.setTxRef(ob.getString("txRef"));
						transaction.setType(ob.getString("type"));

						resp.add(transaction);
					}

				}
			}

		} catch (Exception e) {
		}
		return resp;
	}

	@Override
	public VisaSignUpResponse merchantSignOffAtM2P(VisaMerchantRequest request) {
		VisaSignUpResponse resp = new VisaSignUpResponse();
		try {
			Client client = Client.create();
			WebResource webResource = client.resource(VisaContants.SIGN_UP_LIVE);
			JSONObject payload = new JSONObject();
			JSONObject merchant = new JSONObject();
			JSONObject merchantBankDetail = new JSONObject();
			JSONObject settlementBankDetail = new JSONObject();
			merchant.put("merchantName", "VIJAYA_MERCHANT");
			merchant.put("firstName", request.getFirstName());
			merchant.put("lastName", request.getLastName());
			merchant.put("emailAddress", request.getEmailAddress());
			merchant.put("address1", request.getAddress1());
			merchant.put("address2", request.getAddress1());
			merchant.put("city", request.getCity());
			merchant.put("state", request.getState());
			merchant.put("country", "India");
			merchant.put("pinCode", request.getPinCode());
			merchant.put("merchantCategory", 1);
			merchant.put("lattitude", Double.parseDouble(request.getLattitude()));
			merchant.put("longitude", Double.parseDouble(request.getLongitude()));
			// merchant.put("isEnabled", request.getIsEnabled())
			merchant.put("panNo", request.getPanNo());
			merchant.put("mobileNo", request.getMobileNo());
			merchant.put("aggregator", "M2P");
			merchantBankDetail.put("accName", request.getMerchantAccountName());
			merchantBankDetail.put("bankName", request.getMerchantBankName());
			merchantBankDetail.put("bankAccNo", request.getMerchantAccountNumber());
			merchantBankDetail.put("bankIfscCode", request.getMerchantBankIfscCode());
			merchantBankDetail.put("bankLocation", request.getMerchantBankLocation());
			settlementBankDetail.put("accName", request.getMerchantAccountName());
			settlementBankDetail.put("bankName", request.getMerchantBankName());
			settlementBankDetail.put("bankAccNo", request.getMerchantAccountNumber());
			settlementBankDetail.put("bankIfscCode", request.getMerchantBankIfscCode());
			settlementBankDetail.put("bankLocation", request.getMerchantBankLocation());
			payload.put("merchant", merchant);
			payload.put("merchantBankDetail", merchantBankDetail);
			payload.put("settlementBankDetail", settlementBankDetail);
			/*
			 * ObjectMapper mapper = new ObjectMapper(); String req1 =
			 * mapper.writeValueAsString(payload); SecurityTest sec = new
			 * SecurityTest(); String enc = sec.encodeRequest(req1,
			 * "9876543211234562", "VIJAYA"); System.out.println("red  "+enc);
			 */

			// client.addFilter(System.out);
			ClientResponse response = webResource.header("Content-Type", "application/json")
					.header("Authorization", "Basic YWRtaW46YWRtaW4=").header("TENANT", "VIJAYA_MERCHANT")
					.post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setFlag(false);
				resp.setCode("F04");
				resp.setMessage("Server Eroor M2P");
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						org.json.JSONObject obj = jobj.getJSONObject("result");
						String customerId = (String) obj.get("customerId");
						String mVisaId = (String) obj.get("mvisaId");
						String rupayId = obj.getString("rupayId");
						resp.setCode("S00");
						resp.setCustomerId(customerId);
						resp.setmVisaId(mVisaId);
						resp.setRupayId(rupayId);
						resp.setFlag(true);
					}

				} else {
					resp.setMessage("Error from M2P . Service Down");
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setFlag(false);
				}
			}

		} catch (Exception e) {
		}
		return resp;
	}

	@Override
	public String merchantSignOffAtM2P(String request) {
		String strResponse = "";
		try {
			Client client = Client.create();
			WebResource webResource = client.resource(VisaContants.SIGN_UP_LIVE);
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("Authorization", "Basic YWRtaW46YWRtaW4=").header("TENANT", "VIJAYA_MERCHANT")
					.post(ClientResponse.class, request);
			strResponse = response.getEntity(String.class);
		} catch (Exception e) {
		}

		return strResponse;
	}

	@Override
	public AllUserResponse getUserFromMobileNo(SingleUserRequest request) {
		AllUserResponse resp = new AllUserResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("username", request.getUsername());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getsingleUserUrl(Version.VERSION_1, Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						final org.json.JSONObject details = jobj.getJSONObject("details");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
						resp.setJsonObject(details);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public AllUserResponse getfilteredUserList(DateDTO request) {
		AllUserResponse resp = new AllUserResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("fromDate", request.getFromDate());
			payload.put("toDate", request.getToDate());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.listUserURL(Version.VERSION_1, Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						final org.json.JSONArray details = jobj.getJSONArray("details");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
						resp.setJsonArray(details);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	public TransactionReportResponse getTransactionReport(DateDTO request) {
		TransactionReportResponse resp = new TransactionReportResponse();
		try {
			Client client = Client.create();
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("startDate", request.getFromDate());
			payload.put("endDate", request.getToDate());
			payload.put("transactionType", request.getTransactionType());
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(UrlMetadatas.listTransactionReport(Version.VERSION_1, Role.ADMIN,
					Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456")).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (code.equalsIgnoreCase("S00")) {
							List<TListDTO> transactions = new ArrayList<>();
							resp.setSuccess(true);
							org.json.JSONArray details = JSONParserUtil.getArray(jobj, "details");
							if (details != null && details.length() > 0) {
								for (int i = 0; i < details.length(); i++) {
									org.json.JSONObject json = details.getJSONObject(i);
									TListDTO dto = new TListDTO();
									dto.setUsername(JSONParserUtil.getString(json, "username"));
									dto.setEmail(JSONParserUtil.getString(json, "email"));
									dto.setContactNo(JSONParserUtil.getString(json, "contactNo"));
									dto.setTransactionRefNo(JSONParserUtil.getString(json, "transactionRefNo"));
									dto.setCurrentBalance(JSONParserUtil.getString(json, "currentBalance"));
									dto.setDateOfTransaction(JSONParserUtil.getString(json, "dateOfTransaction"));
									dto.setDebit(JSONParserUtil.getString(json, "debit"));
									dto.setAmount(JSONParserUtil.getString(json, "amount"));
									dto.setStatus(JSONParserUtil.getString(json, "status"));
									dto.setDescription(JSONParserUtil.getString(json, "description"));
									transactions.add(dto);
								}
								resp.setList(transactions);
							} else {
								resp.setSuccess(false);
								resp.setMessage("Transaction List Empty");
							}
						} else {
							resp.setSuccess(false);
							resp.setMessage("Please try Later");
						}
					} else {
						resp.setSuccess(false);
					}
				} else {
					resp.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
		}
		return resp;
	}
	@Override
	public TransactionReportResponse getTransactionReportByPage(AllUserRequest request) {
		TransactionReportResponse resp = new TransactionReportResponse();
		try {
			Client client = Client.create();
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("page", request.getPage());
			payload.put("size", request.getSize());
			payload.put("pageable", request.isPageable());
			payload.put("fromDate", request.getStartDate());
			payload.put("toDate", request.getEndDate());
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(UrlMetadatas.listTransactionReportByPage(Version.VERSION_1, Role.ADMIN,
					Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456")).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (code.equalsIgnoreCase("S00")) {
							List<TListDTO> transactions = new ArrayList<>();
							resp.setSuccess(true);
							org.json.JSONArray details = JSONParserUtil.getArray(jobj, "details");
							if (details != null && details.length() > 0) {
								for (int i = 0; i < details.length(); i++) {
									org.json.JSONObject json = details.getJSONObject(i);
									TListDTO dto = new TListDTO();
									dto.setUsername(JSONParserUtil.getString(json, "username"));
									dto.setEmail(JSONParserUtil.getString(json, "email"));
									dto.setContactNo(JSONParserUtil.getString(json, "contactNo"));
									dto.setTransactionRefNo(JSONParserUtil.getString(json, "transactionRefNo"));
									dto.setCurrentBalance(JSONParserUtil.getString(json, "currentBalance"));
									dto.setDateOfTransaction(JSONParserUtil.getString(json, "dateOfTransaction"));
									dto.setDebit(JSONParserUtil.getString(json, "debit"));
									dto.setAmount(JSONParserUtil.getString(json, "amount"));
									dto.setStatus(JSONParserUtil.getString(json, "status"));
									dto.setDescription(JSONParserUtil.getString(json, "description"));
									transactions.add(dto);
								}
								resp.setList(transactions);
								resp.setAmount(JSONParserUtil.getLong(jobj, "count"));
							} else {
								resp.setSuccess(false);
								resp.setMessage("Transaction List Empty");
							}
						} else {
							resp.setSuccess(false);
							resp.setMessage("Please try Later");
						}
					} else {
						resp.setSuccess(false);
					}
				} else {
					resp.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
		}
		return resp;
	}

	public TransactionReportResponse getReconReport(DateDTO request) {
		TransactionReportResponse resp = new TransactionReportResponse();

		try {
			Client client = Client.create();
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("startDate", request.getFromDate());
			payload.put("endDate", request.getToDate());
			payload.put("type", request.getServiceName());
			payload.put("status", request.getServiceStatus());
			WebResource webResource = client.resource(
					UrlMetadatas.listReconReport(Version.VERSION_1, Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456")).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (code.equalsIgnoreCase("S00")) {
							List<TListDTO> transactions = new ArrayList<>();
							resp.setSuccess(true);
							org.json.JSONArray details = JSONParserUtil.getArray(jobj, "details");
							if (details != null && details.length() > 0) {
								if (request.getServiceName().equals("IPay")) {
									resp.setServiceName("IPay ");
									for (int i = 0; i < details.length(); i++) {
										org.json.JSONObject json = details.getJSONObject(i);
										String txnRef = ConvertUtil
												.removeLastString(JSONParserUtil.getString(json, "transactionRefNo"));
										StatusCheckRequest req = new StatusCheckRequest();
										req.setToken(InstantPayConstants.getToken());
										req.setAgentId(txnRef);

										StatusCheckResponse result = statusCheckApi.request(req);
										TListDTO dto = new TListDTO();
										dto.setUsername(JSONParserUtil.getString(json, "username"));
										dto.setEmail(JSONParserUtil.getString(json, "email"));
										dto.setContactNo(JSONParserUtil.getString(json, "contactNo"));
										dto.setTransactionRefNo(JSONParserUtil.getString(json, "transactionRefNo"));
										dto.setCurrentBalance(JSONParserUtil.getString(json, "currentBalance"));
										dto.setDateOfTransaction(JSONParserUtil.getString(json, "dateOfTransaction"));
										dto.setDebit(JSONParserUtil.getString(json, "debit"));
										dto.setAmount(JSONParserUtil.getString(json, "amount"));
										dto.setStatus(JSONParserUtil.getString(json, "status"));
										dto.setDescription(JSONParserUtil.getString(json, "description"));
										dto.setDateTime(result.getStatusCheck().getDatetime());
										dto.setOrgId(result.getStatusCheck().getOprId());
										dto.setServiceStatus(result.getStatusCheck().getStatus());
										transactions.add(dto);
									}
								} else if (request.getServiceName().equals("EBS")) {
									resp.setServiceName("EBS ");
									for (int i = 0; i < details.length(); i++) {
										org.json.JSONObject json = details.getJSONObject(i);
										String txnRef = ConvertUtil
												.removeLastString(JSONParserUtil.getString(json, "transactionRefNo"));
										EBSRedirectResponse ebsRedirectResponse = new EBSRedirectResponse();
										ebsRedirectResponse.setMerchantRefNo(txnRef);
										ResponseDTO result = loadMoneyApi
												.verifyEBSTransactionForRecon(ebsRedirectResponse);
										if (result.isValid()) {
											TListDTO dto = new TListDTO();
											dto.setUsername(JSONParserUtil.getString(json, "username"));
											dto.setEmail(JSONParserUtil.getString(json, "email"));
											dto.setContactNo(JSONParserUtil.getString(json, "contactNo"));
											dto.setTransactionRefNo(JSONParserUtil.getString(json, "transactionRefNo"));
											dto.setCurrentBalance(JSONParserUtil.getString(json, "currentBalance"));
											dto.setDateOfTransaction(
													JSONParserUtil.getString(json, "dateOfTransaction"));
											dto.setDebit(JSONParserUtil.getString(json, "debit"));
											dto.setAmount(JSONParserUtil.getString(json, "amount"));
											dto.setStatus(JSONParserUtil.getString(json, "status"));
											dto.setDescription(JSONParserUtil.getString(json, "description"));
											dto.setDateTime(result.getEbsDate());
											dto.setOrgId(result.getPaymentId());
											dto.setServiceStatus(result.getEbsStatus());
											transactions.add(dto);
										}
									}
								} else if (request.getServiceName().equals("VNET")) {
									resp.setServiceName("VNET ");
									for (int i = 0; i < details.length(); i++) {
										org.json.JSONObject json = details.getJSONObject(i);
										String txnRef = ConvertUtil
												.removeLastString(JSONParserUtil.getString(json, "transactionRefNo"));
										VRedirectResponse req = new VRedirectResponse();
										req.setPRN(txnRef);
										req.setITC(txnRef);
										req.setAMT(JSONParserUtil.getString(json, "amount"));
										VNetStatusResponse result = loadMoneyApi.verifyVNetTransaction(req);
										TListDTO dto = new TListDTO();
										dto.setUsername(JSONParserUtil.getString(json, "username"));
										dto.setEmail(JSONParserUtil.getString(json, "email"));
										dto.setContactNo(JSONParserUtil.getString(json, "contactNo"));
										dto.setTransactionRefNo(JSONParserUtil.getString(json, "transactionRefNo"));
										dto.setCurrentBalance(JSONParserUtil.getString(json, "currentBalance"));
										dto.setDateOfTransaction(JSONParserUtil.getString(json, "dateOfTransaction"));
										dto.setDebit(JSONParserUtil.getString(json, "debit"));
										dto.setAmount(JSONParserUtil.getString(json, "amount"));
										dto.setStatus(JSONParserUtil.getString(json, "status"));
										dto.setDescription(JSONParserUtil.getString(json, "description"));
										dto.setServiceStatus(result.getStatus());
										transactions.add(dto);
									}
								}
								resp.setList(transactions);
							} else {
								resp.setSuccess(false);
								resp.setMessage("Transaction List Empty");
							}
						} else {
							resp.setSuccess(false);
							resp.setMessage("Please try Later");
						}
					} else {
						resp.setSuccess(false);
					}
				} else {
					resp.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
		}
		return resp;
	}

	@Override
	public QRResponseDTO getQRResponseFromM2P(QRRequestDTO request) {
		QRResponseDTO resp = new QRResponseDTO();
		String entityId = null;
		QRResponseDTO dto = new UserApi().getEntityIdOfM2P(request);
		entityId = dto.getEntityId();
		Client client = Client.create();
		WebResource webResource = client.resource(VisaContants.GET_LIVE_QR + entityId);
		ClientResponse response = webResource.accept("application/json").type("application/json")
				.header("Authorization", "Basic YWRtaW46YWRtaW4=").header("TENANT", "VIJAYA_MERCHANT")
				.get(ClientResponse.class);
		String strResponse = response.getEntity(String.class);
		if (response.getStatus() != 200) {
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("sorry please again later..");
		} else {
			if (strResponse != null) {
				org.json.JSONObject jobj = null;
				try {
					jobj = new org.json.JSONObject(strResponse);
				} catch (JSONException e) {
					e.printStackTrace();
				}
				if (jobj != null) {
					String qrCode = null;
					try {
						org.json.JSONObject result = jobj.getJSONObject("result");
						if (result != null) {
							qrCode = result.getString("qrCode");
							resp.setQrCode(qrCode);
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
					resp.setCode("S00");
					resp.setSuccess(true);
					resp.setMessage("QR generated successfully");
					resp.setQrCode(qrCode);
				}

			}

		}
		return resp;
	}

	@Override
	public QRResponseDTO getLiveQRResponseFromM2P(QRRequestDTO request) {
		QRResponseDTO resp = new QRResponseDTO();
		Client client = Client.create();
		WebResource webResource = client.resource(VisaContants.GET_LIVE_QR + request.getUsername());
		ClientResponse response = webResource.accept("application/json").type("application/json")
				.header("Authorization", "Basic YWRtaW46YWRtaW4=").header("TENANT", "VIJAYA_MERCHANT")
				.get(ClientResponse.class);
		String strResponse = response.getEntity(String.class);
		if (response.getStatus() != 200) {
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("sorry please again later..");
		} else {
			if (strResponse != null) {
				org.json.JSONObject jobj = null;
				try {
					jobj = new org.json.JSONObject(strResponse);
				} catch (JSONException e) {
					e.printStackTrace();
				}
				if (jobj != null) {
					String qrCode = null;
					try {
						org.json.JSONObject result = jobj.getJSONObject("result");
						if (result != null) {
							qrCode = result.getString("qrCode");
							resp.setQrCode(qrCode);
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
					resp.setCode(ResponseStatus.SUCCESS.getValue());
					resp.setSuccess(true);
					resp.setMessage("QR generated successfully");
					resp.setQrCode(qrCode);
				}

			}

		}
		return resp;
	}

	@Override
	public AccountTypeResponse getListAccountType(SessionDTO dto) {
		AccountTypeResponse resp = new AccountTypeResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", dto.getSessionId());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getListAccountTypeURL(Version.VERSION_1, Role.ADMIN,
					Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						final org.json.JSONArray arr = jobj.getJSONArray("details");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {

							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setJsonArray(arr);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public AccountTypeResponse updateAccountType(AccountTypeRequest dto) {
		AccountTypeResponse resp = new AccountTypeResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", dto.getSessionId());
			payload.put("accountType", dto.getCode());
			payload.put("monthlyLimit", dto.getMonthlyLimit());
			payload.put("balanceLimit", dto.getBalanceLimit());
			payload.put("dailyLimit", dto.getDailyLimit());
			payload.put("transactionLimit", dto.getTransactionLimit());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.updateAccountTypeURL(Version.VERSION_1, Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						final org.json.JSONArray arr = jobj.getJSONArray("details");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
						resp.setJsonArray(arr);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	// transactionId clickable
	@Override
	public UserResponse transactionIdClickable(TransactionRequest dto) {
		UserResponse resp = new UserResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", dto.getSessionId());
			payload.put("transactionRefNo", dto.getTransactionRefNo());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.transactionIdClickable(Version.VERSION_1, Role.ADMIN,
					Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {

						resp.setCode(jobj.getString("code"));
						resp.setStatus(jobj.getString("status"));
						resp.setAccountType(jobj.getString("accountType"));
						resp.setAuthority(jobj.getString("authority"));
						resp.setCreated(jobj.getString("created"));
						resp.setBalance(jobj.getDouble("balance"));
						resp.setContactNo(jobj.getString("contactNo"));
						resp.setDateOfBirth(jobj.getString("dateOfBirth"));
						resp.setEmail(jobj.getString("email"));
						resp.setFirstName(jobj.getString("firstName"));
					}
				} else {
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
		}
		return resp;
	}

	@Override
	public VisaSignUpResponse merchantUpdateM2P(VisaMerchantRequest dto) {

		VisaSignUpResponse resp = new VisaSignUpResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("customerId", dto.getCustomerId());
			payload.put("mvisaId", dto.getMvisaId());
			payload.put("userName", dto.getUserName());
			payload.put("rupayId", dto.getRupayId());

			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.updateMerchantM2P(Version.VERSION_1, Role.MERCHANT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						resp.setCode(jobj.getString("code"));
						resp.setMessage("successfully updated");
					}
				} else {
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
		}
		return resp;
	}

	@Override
	public VisaSignUpResponse merchantUpdateQR(VisaMerchantRequest dto) {

		VisaSignUpResponse resp = new VisaSignUpResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("qrcode", dto.getQrcode());
			payload.put("userName", dto.getUserName());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.updateQRcode(Version.VERSION_1, Role.MERCHANT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						resp.setCode(jobj.getString("code"));
						resp.setMessage("successfully updated");
					}
				} else {
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
		}
		return resp;
	}

	public VisaSignUpResponse getM2PMerchantBalance(VisaMerchantRequest dto) {

		VisaSignUpResponse resp = new VisaSignUpResponse();
		try {
			Client client = Client.create();
			WebResource webResource = client.resource(
					"m2p.sit.yappay.in:9000/Yappay/business-entity-manager/fetchbalance/" + dto.getCustomerId());
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.get(ClientResponse.class);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						org.json.JSONArray jarray = jobj.getJSONArray("result");
						for (int i = 0; i < jarray.length(); i++) {
							String balance = jarray.getJSONObject(i).getString("balance");
						}
						resp.setCode(jobj.getString("code"));
						resp.setMessage("successfully updated");
					}
				} else {
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
		}
		return resp;
	}

	@Override
	public GCMResponse getGCMIds(PagingDTO dto) {
		GCMResponse resp = new GCMResponse();
		try {
			Client client = Client.create();
			JSONObject payload = new JSONObject();
			payload.put("sessionId", dto.getSessionId());
			payload.put("page", dto.getPage());
			payload.put("size", dto.getSize());
			WebResource webResource = client.resource(
					UrlMetadatas.getAdminGCMIDs(Version.VERSION_1, Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456")).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							List<String> gcmIds = new ArrayList<>();
							resp.setSuccess(true);
							org.json.JSONObject details = JSONParserUtil.getObject(jobj, "details");
							if (details != null) {
								long totalPages = JSONParserUtil.getLong(details, "totalPages");
								org.json.JSONArray content = JSONParserUtil.getArray(details, "content");
								if (content != null) {
									for (int i = 0; i < content.length(); i++) {
										gcmIds.add(content.getString(i));
									}
									resp.setGcmList(gcmIds);
									resp.setTotalPages(totalPages);
								}
							}
						} else {
							resp.setSuccess(false);
						}
					}
				} else {
					resp.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
		}
		return resp;
	}

	@Override
	public QRResponseDTO getTransactions(QRRequestDTO request) {

		QRResponseDTO resp = new QRResponseDTO();

		PaginationDTO page = new PaginationDTO();
		Client client = Client.create();
		WebResource webResource = client.resource(VisaContants.GET_TRANSACTION_LIST_URL + request.getCustomerId());
		ClientResponse response = webResource.accept("application/json").type("application/json")
				.header("Authorization", "Basic YWRtaW46YWRtaW4=").header("TENANT", "VIJAYA_M2P")
				.get(ClientResponse.class);
		String strResponse = response.getEntity(String.class);
		if (response.getStatus() != 200) {
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("sorry please again later..");
			// resp.setStatus("FAILED");
			// resp.setResponse(APIUtils.getFailedJSON().toString());
		} else {
			if (strResponse != null) {
				org.json.JSONObject jobj = null;
				try {
					jobj = new org.json.JSONObject(strResponse);
				} catch (JSONException e) {
					e.printStackTrace();
				}
				if (jobj != null) {
					try {
						org.json.JSONArray jarray = jobj.getJSONArray("result");
						List<TransactionM2PDTO> list = new ArrayList<>();
						for (int i = 0; i < jarray.length(); i++) {
							org.json.JSONObject obj1 = jarray.getJSONObject(i);
							org.json.JSONObject transaction = obj1.getJSONObject("transaction");
							if (transaction != null) {
								TransactionM2PDTO dto1 = new TransactionM2PDTO();
								dto1.setBalance(JSONParserUtil.getDouble(transaction, "balance"));
								dto1.setBeneficiaryName(JSONParserUtil.getString(transaction, "beneficiaryName"));
								dto1.setName(JSONParserUtil.getString(transaction, "beneficiaryName"));
								dto1.setTime(simformat.format(JSONParserUtil.getLong(transaction, "time")));
								dto1.setTxRef(String.valueOf(JSONParserUtil.getLong(transaction, "txRef")));
								dto1.setAmount(JSONParserUtil.getString(transaction, "amount"));
								dto1.setStatus(JSONParserUtil.getString(transaction, "transactionStatus"));
								list.add(dto1);
							}

						}
						int pageSize = jobj.getJSONObject("pagination").getInt("pageSize");
						int pageNo = jobj.getJSONObject("pagination").getInt("pageNo");
						int totalPages = jobj.getJSONObject("pagination").getInt("totalPages");
						int totalElements = jobj.getJSONObject("pagination").getInt("totalElements");
						page.setPageSize(pageSize);
						page.setPageNo(pageNo);
						page.setTotalPages(totalPages);
						page.setTotalElements(totalElements);
						resp.setCode("S00");
						resp.setMessage("Transaction Details");
						resp.setDto(list);
						resp.setPagination(page);
						resp.setSuccess(true);
					} catch (JSONException e) {
						e.printStackTrace();
						resp.setSuccess(false);
						resp.setCode(ResponseStatus.FAILURE.getValue());
						resp.setMessage("Exception Occurred");
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Exception Occurred");
				}
			} else {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Exception Occurred");
			}
		}
		return resp;
	}

	@Override
	public String getBalanceFrom(String entityId) {
		String stringResponse = "";
		try {
			Client client = Client.create();
			WebResource webResource = client.resource(VisaContants.FETCH_BALANCE + entityId);
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("Authorization", "Basic YWRtaW46YWRtaW4=").header("TENANT", "VIJAYA_M2P")
					.get(ClientResponse.class);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() == 200) {
				stringResponse = strResponse;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return stringResponse;
	}

	@Override
	public RegistrationResponse Agnetregister(AgentRegistrationRequest request) {
		RegistrationResponse response = new RegistrationResponse();
		try {
			MultipartFile imageFile = request.getPanCardImg();
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("firstName", request.getFirstName());
			payload.put("lastName", request.getLastName());
			payload.put("agencyName", request.getAgencyName());
			payload.put("emailAddress", request.getEmailAddress());
			payload.put("address1", request.getAddress());
			payload.put("city", request.getCity());
			payload.put("state", request.getState());
			payload.put("country", request.getCountry());
			payload.put("pinCode", request.getPinCode());
			payload.put("password", request.getPassword());
			payload.put("userName", request.getMobileNo());
			payload.put("panNo", request.getPanNo());
			payload.put("encodedBytes", Base64.getEncoder().encodeToString(imageFile.getBytes()));
			payload.put("contentType", imageFile.getContentType());
			payload.put("mobileNo", request.getMobileNo());
			payload.put("agentAccountName", request.getAgentAccountName());
			payload.put("agentBankName", request.getAgentBankName());
			payload.put("agentAccountNumber", request.getAgentAccountNumber());
			payload.put("agentBankIfscCode", request.getAgentBankIfscCode());
			payload.put("agentBranchName", request.getAgentbranchname());
			payload.put("userType", "Agent");
			Client client = Client.create();
            client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client
					.resource(UrlMetadatas.addAgent(Version.VERSION_1, Role.AGENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse resp = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (resp.getStatus() != 200) {
				response.setSuccess(false);
				response.setCode(ResponseStatus.FAILURE.getValue());
				response.setMessage("Service unavailable");
				response.setStatus("FAILED");
				response.setDetails(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = resp.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							response.setSuccess(true);
							final String details = (String) jobj.get("details");
							response.setDetails(details);
						} else {
							response.setSuccess(false);
						}
						response.setCode(code);
						response.setStatus(status);
						response.setMessage(message);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setSuccess(false);
			response.setCode(ResponseStatus.FAILURE.getValue());
			response.setMessage("Service unavailable");
			response.setStatus("FAILED");
			response.setDetails(APIUtils.getFailedJSON().toString());
		}
		return response;
	}

	@Override
	public RegistrationResponse getCityAndState(AgentRegistrationRequest request) {
		RegistrationResponse response = new RegistrationResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("pinCode", request.getPinCode());
			Client client = Client.create();

			WebResource webResource = client.resource(
					UrlMetadatas.getCityAndState(Version.VERSION_1, Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse resp = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (resp.getStatus() != 200) {
				response.setSuccess(false);
				response.setCode(ResponseStatus.FAILURE.getValue());
				response.setMessage("Service unavailable");
				response.setStatus("FAILED");
				response.setDetails(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = resp.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							response.setSuccess(true);
							final String details = (String) jobj.get("details");
							final String details2 = (String) jobj.get("details2");
							response.setDetails(details);
							response.setResponse(details2);
						} else {
							response.setSuccess(false);
						}
						response.setCode(code);
						response.setStatus(status);
						response.setMessage(message);
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			response.setSuccess(false);
			response.setCode(ResponseStatus.FAILURE.getValue());
			response.setMessage("Service unavailable");
			response.setStatus("FAILED");
			response.setDetails(APIUtils.getFailedJSON().toString());
		}
		return response;
	}

	@Override
	public RegistrationResponse getBankList(String sessionId) {
		RegistrationResponse response = new RegistrationResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", sessionId);
			Client client = Client.create();

			WebResource webResource = client.resource(
					UrlMetadatas.getBankList(Version.VERSION_1, Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse resp = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (resp.getStatus() != 200) {
				response.setSuccess(false);
				response.setCode(ResponseStatus.FAILURE.getValue());
				response.setMessage("Service unavailable");
				response.setStatus("FAILED");
				response.setDetails(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = resp.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							response.setSuccess(true);
							org.json.JSONArray jArr = jobj.getJSONArray("details");
							List<BanksDto> list = new ArrayList<>();
							for (int i = 0; i < jArr.length(); i++) {
								BanksDto dto = new BanksDto();
								final String bankName = jArr.getJSONObject(i).getString("name");
								/*
								 * final String bankCode =
								 * jArr.getJSONObject(i).getString("code");
								 * dto.setBankCode(bankCode);
								 */
								dto.setBankName(bankName);
								list.add(dto);
							}
							response.setDetails2(list);
						} else {
							response.setSuccess(false);
						}
						response.setCode(code);
						response.setStatus(status);
						response.setMessage(message);
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			response.setSuccess(false);
			response.setCode(ResponseStatus.FAILURE.getValue());
			response.setMessage("Service unavailable");
			response.setStatus("FAILED");
			response.setDetails(APIUtils.getFailedJSON().toString());
		}
		return response;
	}

	@Override
	public RegistrationResponse getBankIfscCode(AgentRegistrationRequest request) {
		RegistrationResponse response = new RegistrationResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("agentBankName", request.getAgentBankName());
			Client client = Client.create();

			WebResource webResource = client.resource(
					UrlMetadatas.getBankList(Version.VERSION_1, Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse resp = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (resp.getStatus() != 200) {
				response.setSuccess(false);
				response.setCode(ResponseStatus.FAILURE.getValue());
				response.setMessage("Service unavailable");
				response.setStatus("FAILED");
				response.setDetails(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = resp.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							response.setSuccess(true);
							org.json.JSONArray jArr = jobj.getJSONArray("details");
							List<BanksDto> list = new ArrayList<>();
							for (int i = 0; i < jArr.length(); i++) {
								BanksDto dto = new BanksDto();
								final String bankCode = jArr.getJSONObject(i).getString("code");
								dto.setBankCode(bankCode);
								list.add(dto);
							}
							response.setDetails2(list);
						} else {
							response.setSuccess(false);
						}
						response.setCode(code);
						response.setStatus(status);
						response.setMessage(message);
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			response.setSuccess(false);
			response.setCode(ResponseStatus.FAILURE.getValue());
			response.setMessage("Service unavailable");
			response.setStatus("FAILED");
			response.setDetails(APIUtils.getFailedJSON().toString());
		}
		return response;
	}

	@Override
	public MobileOTPResponse agnetMobileOTP(MobileOTPRequest request) {
		MobileOTPResponse resp = new MobileOTPResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("mobileNumber", request.getMobileNumber());
			payload.put("key", request.getKey());

			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.agentMobileOtpVerification(Version.VERSION_1,
					Role.AGENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);

			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {

				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						final Object details = (String) jobj.get("details");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setDetails(details);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());

		}
		return resp;

	}

	@Override
	public List<AnalyticsResponse> getAnalytics(DateDTO request) {
		List<AnalyticsResponse> list = new ArrayList<>();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("fromDate", request.getToDate());
			payload.put("toDate", request.getFromDate());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getAnalyticsServices(Version.VERSION_1, Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				// resp.setCode(ResponseStatus.FAILURE.getValue());
				// resp.setMessage("Service unavailable");
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						jobj.getString("code");
						jobj.getString("message");
						org.json.JSONArray jsonArray = jobj.getJSONArray("details");
						if (jsonArray != null) {
							for (int i = 0; i < jsonArray.length(); i++) {
								org.json.JSONObject obj = jsonArray.getJSONObject(i);
								if (obj != null) {
									AnalyticsResponse resp = new AnalyticsResponse();
									resp.setCount(obj.getLong("count"));
									resp.setAmount(obj.getDouble("amount"));
									resp.setServiceName(obj.getString("serviceName"));
									// resp.setDebit(obj.getBoolean("debit"));
									list.add(resp);

								}
							}
						}
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();

		}
		return list;
	}

	@Override
	public AllUserResponse getAllCreditAgent(AllUserRequest request) {
		AllUserResponse resp = new AllUserResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("page", request.getPage());
			payload.put("size", request.getSize());
			payload.put("userStatus", request.getStatus().getValue());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getAllCreditlistAgent(Version.VERSION_1, Role.ADMIN,
					Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						final org.json.JSONObject details = jobj.getJSONObject("details");
						final org.json.JSONArray totalUser = (org.json.JSONArray) jobj.getJSONObject("details")
								.getJSONArray("content");
						long numberOfElements = details.getLong("numberOfElements");
						boolean firstPage = details.getBoolean("firstPage");
						boolean lastPage = details.getBoolean("lastPage");
						long size = details.getLong("size");
						long totalPages = details.getLong("totalPages");
						resp.setTotalPages(totalPages);
						resp.setSize(size);
						resp.setNumberOfElements(numberOfElements);
						resp.setFirstPage(firstPage);
						resp.setLastPage(lastPage);
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
						resp.setTotalUser(totalUser.length());
						resp.setJsonArray(totalUser);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public List<UserAnalytics> getAllUsersAnalytics(DateDTO request) {
		AllUserResponse resp = new AllUserResponse();
		List<UserAnalytics> list = new ArrayList<>();
		try {
			JSONObject payload = new JSONObject();
			payload.put("serviceName", request.getServiceName());
			payload.put("to", request.getToDate());
			payload.put("from", request.getFromDate());
			payload.put("sessionId", request.getSessionId());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getUserAnalytics(Version.VERSION_1, Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = jobj.getString("code");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							final org.json.JSONArray details = jobj.getJSONArray("details");
							for (int i = 0; i < details.length(); i++) {
								org.json.JSONObject jobject = details.getJSONObject(i);
								UserAnalytics analytics = new UserAnalytics();
								analytics.setUsername(jobject.getString("username"));
								analytics.setCreated(jobject.getString("created"));
								analytics.setTransactionRefNo(jobject.getString("transactionRefNo"));
								analytics.setAmount(jobject.getString("amount"));
								analytics.setStatus(jobject.getString("status"));
								analytics.setDescription(jobject.getString("description"));
								list.add(analytics);
							}
							resp.setDetailResponse(list);
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(ResponseStatus.FAILURE.getValue());
						resp.setStatus("Failed");
						resp.setMessage("Unexpected response");
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return list;
	}

	@Override
	public AllUserResponse getAllAgents(AllUserRequest request) {
		AllUserResponse resp = new AllUserResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("page", request.getPage());
			payload.put("size", request.getSize());
			payload.put("userStatus", request.getStatus().getValue());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getAllAgentURL(Version.VERSION_1, Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			System.out.println("strResponse===" + strResponse);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							final org.json.JSONArray details = jobj.getJSONArray("details");
							resp.setJsonArray(details);
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public UserResponse transactionRefNo(TransactionRequest dto) {
		UserResponse resp = new UserResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", dto.getSessionId());
			payload.put("transactionRefNo", dto.getTransactionRefNo());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.transactionIdClickable(Version.VERSION_1, Role.ADMIN,
					Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						resp.setCode(jobj.getString("code"));
						resp.setStatus(jobj.getString("status"));
						resp.setAccountType(jobj.getString("accountType"));
						resp.setAuthority(jobj.getString("authority"));
						resp.setCreated(jobj.getString("created"));
						resp.setBalance(jobj.getDouble("balance"));
						resp.setContactNo(jobj.getString("contactNo"));
						resp.setDateOfBirth(jobj.getString("dateOfBirth"));
						resp.setEmail(jobj.getString("email"));
						resp.setFirstName(jobj.getString("firstName"));
					}
				} else {
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
		}
		return resp;
	}

	@Override
	public List<AnalyticsResponse> getAnalyticsCredit(DateDTO request) {

		List<AnalyticsResponse> list = new ArrayList<>();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("fromDate", request.getToDate());
			payload.put("toDate", request.getFromDate());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getUserAnalyticsCredits(Version.VERSION_1,
					Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				// resp.setCode(ResponseStatus.FAILURE.getValue());
				// resp.setMessage("Service unavailable");
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						jobj.getString("code");
						jobj.getString("message");
						org.json.JSONArray jsonArray = jobj.getJSONArray("details");
						if (jsonArray != null) {
							for (int i = 0; i < jsonArray.length(); i++) {
								org.json.JSONObject obj = jsonArray.getJSONObject(i);
								if (obj != null) {
									AnalyticsResponse resp = new AnalyticsResponse();
									resp.setCount(obj.getLong("count"));
									resp.setAmount(obj.getDouble("amount"));
									resp.setServiceName(obj.getString("serviceName"));
									// resp.setDebit(obj.getBoolean("debit"));
									list.add(resp);

								}
							}
						}
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();

		}
		return list;

	}

	@Override
	public AllTransactionResponse getPartnerDetails(AllTransactionRequest request) {
		AllTransactionResponse resp = new AllTransactionResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("partnerName", request.getPartnerName().toUpperCase());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getPartnerDetailsURL(Version.VERSION_1, Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						org.json.JSONArray totalTransactionArray = null;
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							resp.setSuccess(true);
							totalTransactionArray = jobj.getJSONArray("details");
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
						resp.setJsonArray(totalTransactionArray);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public AllTransactionResponse getPartnerDetailsFilter(PartnerDetailsRequest request) {
		AllTransactionResponse resp = new AllTransactionResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("partnerName", request.getPartnerName().toUpperCase());
			payload.put("startDate", request.getFromDate());
			payload.put("endDate", request.getEndDate());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getPartnerDetailsFilterURL(Version.VERSION_1,
					Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						org.json.JSONArray totalTransactionArray = null;
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							resp.setSuccess(true);
							totalTransactionArray = jobj.getJSONArray("details");
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
						resp.setJsonArray(totalTransactionArray);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public AllUserResponse getUserFromLocationCode(SingleUserRequest request) {
		AllUserResponse resp = new AllUserResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("fromDate", request.getFromDate());
			payload.put("toDate", request.getToDate());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getUserByLocation(Version.VERSION_1, Role.ADMIN,
					Device.WEBSITE, Language.ENGLISH, request.getLocationCode()));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						final org.json.JSONObject details = jobj.getJSONObject("details");
						org.json.JSONArray array = details.getJSONArray("UserByLocationCode");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
						resp.setJsonArray(array);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public AllUserResponse getAllAgentFlightsDetail(AllUserRequest request) {
		AllUserResponse resp = new AllUserResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.agentGetAllFlightsDetailURL(Version.VERSION_1,
					Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							final org.json.JSONArray details = jobj.getJSONArray("details");
							resp.setJsonArray(details);
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);

						resp.setMessage(message);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public AllUserResponse getAllFlightsDetail(AllUserRequest request) {
		AllUserResponse resp = new AllUserResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			/*
			 * payload.put("page", request.getPage()); payload.put("size",
			 * request.getSize()); payload.put("userStatus",
			 * request.getStatus().getValue());
			 */
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getAllFlightsDetailURL(Version.VERSION_1, Role.ADMIN,
					Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							final org.json.JSONArray details = jobj.getJSONArray("details");
							resp.setJsonArray(details);
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);

						resp.setMessage(message);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public FlightResponseDTO getSingleFlightTicketDetails(String sessionId, long flightId) {

		FlightResponseDTO resp = new FlightResponseDTO();
		List<TravellerFlightDetails> travellerFlightDetails = new ArrayList<>();
		FlightTicketResp flightTicketResp = new FlightTicketResp();

		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", sessionId);
			payload.put("flightId", flightId);
			Client vpqClient = Client.create();
			WebResource vpqWebResource = vpqClient.resource(UrlMetadatas.getFlightDetailsForAdmin(Version.VERSION_1,
					Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse vpqresponse = vpqWebResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = vpqresponse.getEntity(String.class);
			org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
			final String code = jobj.getString("code");
			final String status = jobj.getString("code");
			final String message = jobj.getString("code");
			if (code.equalsIgnoreCase("S00")) {
				final org.json.JSONArray details = jobj.getJSONArray("details");
				final org.json.JSONObject details2 = jobj.getJSONObject("details2");
				System.out.println("details2==" + details2);
				for (int i = 0; i < details.length(); i++) {
					org.json.JSONObject detail = details.getJSONObject(i);
					TravellerFlightDetails tDetails = new TravellerFlightDetails();

					tDetails.setfName(detail.getString("fName"));
					tDetails.setlName(detail.getString("lName"));
					tDetails.setGender(detail.getString("gender"));
					tDetails.setType(detail.getString("type"));
					tDetails.setTicketNo(detail.getString("ticketNo"));

					travellerFlightDetails.add(tDetails);
				}

				if (details.length() > 0) {

					org.json.JSONObject detail = details.getJSONObject(0);

					if (detail != null) {
						String flString = detail.getString("flightTicket");
						if (flString != null) {
							org.json.JSONObject flDetails = detail.getJSONObject("flightTicket");

							String ticketsStr = flDetails.getString("ticketDetails");
							if (!ticketsStr.equalsIgnoreCase("null") && !ticketsStr.equalsIgnoreCase("nullnull")) {
								TicketsResp ticketDeatilsDTO = new TicketsResp();

								ObjectMapper mapper = new ObjectMapper();
								ticketDeatilsDTO = mapper.readValue(ticketsStr, TicketsResp.class);

								System.err.println("ticketDeatilsDTO:: " + ticketDeatilsDTO);
								if (details2 != null) {
									org.json.JSONArray job = details2.getJSONObject("Tickets").getJSONArray("Oneway");
									System.out.println("job==" + job);
									for (int i = 0; i < job.length(); i++) {
										ticketDeatilsDTO.getTickets().getOneway().get(i)
												.setDestinationName(job.getJSONObject(i).getString("destinationName"));
										ticketDeatilsDTO.getTickets().getOneway().get(i)
												.setSourceName(job.getJSONObject(i).getString("sourceName"));
									}

									org.json.JSONArray job2 = details2.getJSONObject("Tickets")
											.getJSONArray("Roundway");
									System.out.println("job==" + job2);
									for (int i = 0; i < job2.length(); i++) {
										ticketDeatilsDTO.getTickets().getRoundway().get(i)
												.setDestinationName(job2.getJSONObject(i).getString("destinationName"));
										ticketDeatilsDTO.getTickets().getRoundway().get(i)
												.setSourceName(job2.getJSONObject(i).getString("sourceName"));
									}
								}

								flightTicketResp.setTicketsResp(ticketDeatilsDTO);
							}
							flightTicketResp.setBookingRefNo(flDetails.getString("bookingRefId"));
							flightTicketResp.setEmail(flDetails.getString("email"));

							Date date = new Date(flDetails.getLong("created"));

							String dateStr = simformat.format(date);

							flightTicketResp.setCreated(dateStr);
						}

						flightTicketResp.setTravellerDetails(travellerFlightDetails);
					}

				}

			}

			resp.setCode(code);
			resp.setStatus(status);
			resp.setMessage(message);
			resp.setDetails(flightTicketResp);
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			resp.setMessage("Service Unavailable");
			resp.setDetails(null);
			resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
		}
		return resp;

	}

	@Override
	public FlightResponseDTO getSingleAgentFlightTicketDetails(String sessionId, long flightId) {
		FlightResponseDTO resp = new FlightResponseDTO();
		List<TravellerFlightDetails> travellerFlightDetails = new ArrayList<>();
		FlightTicketResp flightTicketResp = new FlightTicketResp();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", sessionId);
			payload.put("flightId", flightId);
			Client vpqClient = Client.create();
			vpqClient.addFilter(new LoggingFilter(System.out));
			WebResource vpqWebResource = vpqClient.resource(UrlMetadatas
					.agentGetFlightDetailsForAdmin(Version.VERSION_1, Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse vpqresponse = vpqWebResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = vpqresponse.getEntity(String.class);
			org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
			final String code = jobj.getString("code");
			final String status = jobj.getString("code");
			final String message = jobj.getString("code");
			if (code.equalsIgnoreCase("S00")) {
				final org.json.JSONArray details = jobj.getJSONArray("details");
				final org.json.JSONObject details2 = jobj.getJSONObject("details2");
				System.out.println("details2==" + details2);
				for (int i = 0; i < details.length(); i++) {
					org.json.JSONObject detail = details.getJSONObject(i);
					TravellerFlightDetails tDetails = new TravellerFlightDetails();
					tDetails.setfName(detail.getString("fName"));
					tDetails.setlName(detail.getString("lName"));
					tDetails.setGender(detail.getString("gender"));
					tDetails.setType(detail.getString("type"));
					tDetails.setTicketNo(detail.getString("ticketNo"));
					travellerFlightDetails.add(tDetails);
				}
				if (details.length() > 0) {
					org.json.JSONObject detail = details.getJSONObject(0);
					if (detail != null) {
						String flString = detail.getString("flightTicket");
						if (flString != null) {
							org.json.JSONObject flDetails = detail.getJSONObject("flightTicket");
							String ticketsStr = flDetails.getString("ticketDetails");
							if (!ticketsStr.equalsIgnoreCase("null") && !ticketsStr.equalsIgnoreCase("nullnull")) {
								TicketsResp ticketDeatilsDTO = new TicketsResp();
								ObjectMapper mapper = new ObjectMapper();
								ticketDeatilsDTO = mapper.readValue(ticketsStr, TicketsResp.class);
								System.err.println("ticketDeatilsDTO:: " + ticketDeatilsDTO);
								if (details2 != null) {
									org.json.JSONArray job = details2.getJSONObject("Tickets").getJSONArray("Oneway");
									System.out.println("job==" + job);
									for (int i = 0; i < job.length(); i++) {
										ticketDeatilsDTO.getTickets().getOneway().get(i)
												.setDestinationName(job.getJSONObject(i).getString("destinationName"));
										ticketDeatilsDTO.getTickets().getOneway().get(i)
												.setSourceName(job.getJSONObject(i).getString("sourceName"));
									}

									org.json.JSONArray job2 = details2.getJSONObject("Tickets")
											.getJSONArray("Roundway");
									System.out.println("job==" + job2);
									for (int i = 0; i < job2.length(); i++) {
										ticketDeatilsDTO.getTickets().getRoundway().get(i)
												.setDestinationName(job2.getJSONObject(i).getString("destinationName"));
										ticketDeatilsDTO.getTickets().getRoundway().get(i)
												.setSourceName(job2.getJSONObject(i).getString("sourceName"));
									}
								}

								flightTicketResp.setTicketsResp(ticketDeatilsDTO);
							}
							flightTicketResp.setBookingRefNo(flDetails.getString("bookingRefId"));
							flightTicketResp.setEmail(flDetails.getString("email"));

							Date date = new Date(flDetails.getLong("created"));

							String dateStr = simformat.format(date);

							flightTicketResp.setCreated(dateStr);
						}

						flightTicketResp.setTravellerDetails(travellerFlightDetails);
					}

				}

			}

			resp.setCode(code);
			resp.setStatus(status);
			resp.setMessage(message);
			resp.setDetails(flightTicketResp);
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			resp.setMessage("Service Unavailable");
			resp.setDetails(null);
			resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
		}
		return resp;

	}
	
	
	@Override
	public FlightResponseDTO getSingleAgentFlightTicketDetailsPdf(String sessionId, long flightId) {
		FlightResponseDTO resp = new FlightResponseDTO();
		List<TravellerFlightDetails> travellerFlightDetails = new ArrayList<>();
		FlightTicketResp flightTicketResp = new FlightTicketResp();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", sessionId);
			payload.put("flightId", flightId);
			Client vpqClient = Client.create();
			vpqClient.addFilter(new LoggingFilter(System.out));
			WebResource vpqWebResource = vpqClient.resource(UrlMetadatas
					.agentGetFlightDetailsForAdmin(Version.VERSION_1, Role.AGENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse vpqresponse = vpqWebResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = vpqresponse.getEntity(String.class);
			org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
			final String code = jobj.getString("code");
			final String status = jobj.getString("code");
			final String message = jobj.getString("code");
			if (code.equalsIgnoreCase("S00")) {
				final org.json.JSONArray details = jobj.getJSONArray("details");
				final org.json.JSONObject details2 = jobj.getJSONObject("details2");
				System.out.println("details2==" + details2);
				for (int i = 0; i < details.length(); i++) {
					org.json.JSONObject detail = details.getJSONObject(i);
					TravellerFlightDetails tDetails = new TravellerFlightDetails();
					tDetails.setfName(detail.getString("fName"));
					tDetails.setlName(detail.getString("lName"));
					tDetails.setGender(detail.getString("gender"));
					tDetails.setType(detail.getString("type"));
					tDetails.setTicketNo(detail.getString("ticketNo"));
					travellerFlightDetails.add(tDetails);
				}
				if (details.length() > 0) {
					org.json.JSONObject detail = details.getJSONObject(0);
					if (detail != null) {
						String flString = detail.getString("flightTicket");
						if (flString != null) {
							org.json.JSONObject flDetails = detail.getJSONObject("flightTicket");
							String ticketsStr = flDetails.getString("ticketDetails");
							if (!ticketsStr.equalsIgnoreCase("null") && !ticketsStr.equalsIgnoreCase("nullnull")) {
								TicketsResp ticketDeatilsDTO = new TicketsResp();
								ObjectMapper mapper = new ObjectMapper();
								ticketDeatilsDTO = mapper.readValue(ticketsStr, TicketsResp.class);
								System.err.println("ticketDeatilsDTO:: " + ticketDeatilsDTO);
								if (details2 != null) {
									org.json.JSONArray job = details2.getJSONObject("Tickets").getJSONArray("Oneway");
									System.out.println("job==" + job);
									for (int i = 0; i < job.length(); i++) {
										ticketDeatilsDTO.getTickets().getOneway().get(i)
												.setDestinationName(job.getJSONObject(i).getString("destinationName"));
										ticketDeatilsDTO.getTickets().getOneway().get(i)
												.setSourceName(job.getJSONObject(i).getString("sourceName"));
									}

									org.json.JSONArray job2 = details2.getJSONObject("Tickets")
											.getJSONArray("Roundway");
									System.out.println("job==" + job2);
									for (int i = 0; i < job2.length(); i++) {
										ticketDeatilsDTO.getTickets().getRoundway().get(i)
												.setDestinationName(job2.getJSONObject(i).getString("destinationName"));
										ticketDeatilsDTO.getTickets().getRoundway().get(i)
												.setSourceName(job2.getJSONObject(i).getString("sourceName"));
									}
								}

								flightTicketResp.setTicketsResp(ticketDeatilsDTO);
							}
							flightTicketResp.setBookingRefNo(flDetails.getString("bookingRefId"));
							flightTicketResp.setEmail(flDetails.getString("email"));

							Date date = new Date(flDetails.getLong("created"));

							String dateStr = simformat.format(date);

							flightTicketResp.setCreated(dateStr);
						}

						flightTicketResp.setTravellerDetails(travellerFlightDetails);
					}

				}

			}

			resp.setCode(code);
			resp.setStatus(status);
			resp.setMessage(message);
			resp.setDetails(flightTicketResp);
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			resp.setMessage("Service Unavailable");
			resp.setDetails(null);
			resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
		}
		return resp;

	}

	@Override
	public AllUserResponse getTKOrderDetails(AllUserRequest request) {
		AllUserResponse resp = new AllUserResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("page", request.getPage());
			payload.put("size", request.getSize());
			payload.put("userStatus", request.getStatus().getValue());
			payload.put("pageable", request.isPageable());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getTKOrderDetailForAdminURL(Version.VERSION_1,
					Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			System.out.println("strResponse==" + strResponse);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);

						resp.setMessage(message);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public AllUserResponse getTKOrderDetailsByDate(AllUserRequest request) {
		AllUserResponse resp = new AllUserResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			/*
			 * payload.put("page", request.getPage()); payload.put("size",
			 * request.getSize()); payload.put("userStatus",
			 * request.getStatus().getValue());
			 */
			payload.put("fromDate", request.getStartDate());
			payload.put("toDate", request.getEndDate());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getTKOrderDetailByDateForAdmin(Version.VERSION_1,
					Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);

						resp.setMessage(message);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	/*
	 * @Override >>>>>>> origin/k_6.1 public AllUserResponse
	 * getTKOrderDetailsByDate(AllUserRequest request) { AllUserResponse resp =
	 * new AllUserResponse(); try { JSONObject payload = new JSONObject();
	 * payload.put("sessionId", request.getSessionId()); payload.put("page",
	 * request.getPage()); payload.put("size", request.getSize());
	 * payload.put("userStatus", request.getStatus().getValue());
	 * payload.put("fromDate",request.getStartDate());
	 * payload.put("toDate",request.getEndDate()); Client client =
	 * Client.create(); WebResource webResource =
	 * client.resource(UrlMetadatas.getTKOrderDetailByDateForAdmin(Version.
	 * VERSION_1, Role.ADMIN, Device.WEBSITE, Language.ENGLISH)); ClientResponse
	 * response =
	 * webResource.accept("application/json").type("application/json")
	 * .header("hash",
	 * SecurityUtils.getHash(payload.toString())).post(ClientResponse.class,
	 * payload); String strResponse = response.getEntity(String.class); if
	 * (response.getStatus() != 200) { resp.setSuccess(false);
	 * resp.setCode(ResponseStatus.FAILURE.getValue()); resp.setMessage(
	 * "Service unavailable"); resp.setStatus("FAILED");
	 * resp.setResponse(APIUtils.getFailedJSON().toString()); } else{ if
	 * (strResponse != null) { org.json.JSONObject jobj = new
	 * org.json.JSONObject(strResponse); if (jobj != null) { final String code =
	 * (String) jobj.get("code"); final String message = (String)
	 * jobj.get("message"); if (code.equalsIgnoreCase("S00")) {
	 * resp.setSuccess(true); } else { resp.setSuccess(false); }
	 * resp.setCode(code);
	 * 
	 * resp.setMessage(message); resp.setResponse(strResponse); } } else {
	 * resp.setSuccess(false); resp.setCode(ResponseStatus.FAILURE.getValue());
	 * resp.setMessage("Service unavailable"); resp.setStatus("FAILED");
	 * resp.setResponse(APIUtils.getFailedJSON().toString()); } } } catch
	 * (Exception e) { e.printStackTrace(); resp.setSuccess(false);
	 * resp.setCode(ResponseStatus.FAILURE.getValue()); resp.setMessage(
	 * "Service unavailable"); resp.setStatus("FAILED");
	 * resp.setResponse(APIUtils.getFailedJSON().toString()); } return resp; }
	 */

	@Override
	public com.payqwikweb.app.model.response.bus.ResponseDTO getSingleTicketTravellerDetails(String sessionId,
			String emtTxnNo) {

		com.payqwikweb.app.model.response.bus.ResponseDTO resp = new com.payqwikweb.app.model.response.bus.ResponseDTO();

		List<TravellerDetailsDTO> list = new ArrayList<>();
		List<String> seatNo = new ArrayList<>();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", sessionId);
			payload.put("bookingTxnId", emtTxnNo);
			Client vpqClient = Client.create();
			WebResource vpqWebResource = vpqClient.resource(UrlMetadatas.getTravellerDetailsForAdmin(Version.VERSION_1,
					Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse vpqresponse = vpqWebResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = vpqresponse.getEntity(String.class);
			org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
			final String code = jobj.getString("code");
			if (code.equalsIgnoreCase("S00")) {
				org.json.JSONArray details = jobj.getJSONArray("details");
				for (int i = 0; i < details.length(); i++) {
					TravellerDetailsDTO dto = new TravellerDetailsDTO();
					dto.setfName((String) details.getJSONObject(i).get("fName"));
					dto.setlName((String) details.getJSONObject(i).get("lName"));
					dto.setGender((String) details.getJSONObject(i).get("gender"));
					dto.setSeatNo((String) details.getJSONObject(i).get("seatNo"));
					dto.setFare((String) details.getJSONObject(i).get("fare"));
					dto.setAge((details.getJSONObject(i).getInt("age")) + "");
					list.add(dto);
					seatNo.add((String) details.getJSONObject(i).get("seatNo"));
				}
				if (details.length() >= 0) {
					BusTicketDTO busTicketDTO = new BusTicketDTO();
					String source = (String) details.getJSONObject(0).getJSONObject("busTicketId").get("source");
					String status = (String) details.getJSONObject(0).getJSONObject("busTicketId").get("status");
					String destination = (String) details.getJSONObject(0).getJSONObject("busTicketId")
							.get("destination");
					String journeyDate = (String) details.getJSONObject(0).getJSONObject("busTicketId")
							.get("journeyDate");
					String operatorPnr = details.getJSONObject(0).getJSONObject("busTicketId").getString("operatorPnr");
					String busOperator = (String) details.getJSONObject(0).getJSONObject("busTicketId")
							.getString("busOperator");
					String ticketPnr = details.getJSONObject(0).getJSONObject("busTicketId").getString("ticketPnr");
					;
					double totalFare = (double) details.getJSONObject(0).getJSONObject("busTicketId")
							.getDouble("totalFare");
					String emtTxnId = (String) details.getJSONObject(0).getJSONObject("busTicketId").get("emtTxnId");
					String boardingAddress = (String) details.getJSONObject(0).getJSONObject("busTicketId")
							.get("boardingAddress");
					String arrTime = (String) details.getJSONObject(0).getJSONObject("busTicketId").get("arrTime");
					String emtTransactionScreenId = (String) details.getJSONObject(0).getJSONObject("busTicketId")
							.get("emtTransactionScreenId");
					String vPqTxnRefNo = "NA";
					if (details.getJSONObject(0).getJSONObject("busTicketId").getString("transaction") != null
							&& !details.getJSONObject(0).getJSONObject("busTicketId").getString("transaction").isEmpty()
							&& !details.getJSONObject(0).getJSONObject("busTicketId").getString("transaction")
									.equalsIgnoreCase("Null")) {
						vPqTxnRefNo = details.getJSONObject(0).getJSONObject("busTicketId").getJSONObject("transaction")
								.getString("transactionRefNo");
					}

					String deptTime = (String) details.getJSONObject(0).getJSONObject("busTicketId").get("deptTime");

					busTicketDTO.setDeptTime(deptTime);
					busTicketDTO.setSource(source);
					busTicketDTO.setDestination(destination);
					busTicketDTO.setJourneyDate(journeyDate);
					busTicketDTO.setOperatorPnr(operatorPnr);
					busTicketDTO.setBusOperator(busOperator);
					busTicketDTO.setTicketPnr(ticketPnr);
					busTicketDTO.setTotalFare(totalFare);
					busTicketDTO.setEmtTxnId(emtTxnId);
					busTicketDTO.setBoardingAddress(boardingAddress);
					busTicketDTO.setArrTime(arrTime);
					busTicketDTO.setEmtTransactionScreenId(emtTransactionScreenId);
					busTicketDTO.setTransactionRefNo(vPqTxnRefNo);
					busTicketDTO.setStatus(status);
					resp.setExtraInfo(busTicketDTO);

				}
			}

			resp.setSeatNo(seatNo);
			resp.setCode(code);
			resp.setMessage("Get All Book Tickets");
			resp.setDetails(list);
			resp.setStatus(ResponseStatus.SUCCESS.getKey());

		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			resp.setMessage("Service Unavailable");
			resp.setDetails(null);
			resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
		}
		return resp;

	}

	@Override
	public com.payqwikweb.app.model.response.bus.ResponseDTO getSingleAgentTicketTravellerDetails(String sessionId,
			String emtTxnNo) {

		com.payqwikweb.app.model.response.bus.ResponseDTO resp = new com.payqwikweb.app.model.response.bus.ResponseDTO();

		List<TravellerDetailsDTO> list = new ArrayList<>();
		List<String> seatNo = new ArrayList<>();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", sessionId);
			payload.put("bookingTxnId", emtTxnNo);
			Client vpqClient = Client.create();
			WebResource vpqWebResource = vpqClient.resource(UrlMetadatas
					.agentGetTravellerDetailsForAdmin(Version.VERSION_1, Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse vpqresponse = vpqWebResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = vpqresponse.getEntity(String.class);
			org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
			final String code = jobj.getString("code");
			if (code.equalsIgnoreCase("S00")) {
				org.json.JSONArray details = jobj.getJSONArray("details");
				for (int i = 0; i < details.length(); i++) {
					TravellerDetailsDTO dto = new TravellerDetailsDTO();
					dto.setfName((String) details.getJSONObject(i).get("fName"));
					dto.setlName((String) details.getJSONObject(i).get("lName"));
					dto.setGender((String) details.getJSONObject(i).get("gender"));
					dto.setSeatNo((String) details.getJSONObject(i).get("seatNo"));
					dto.setFare((String) details.getJSONObject(i).get("fare"));
					dto.setAge((details.getJSONObject(i).getInt("age")) + "");
					list.add(dto);
					seatNo.add((String) details.getJSONObject(i).get("seatNo"));
				}
				if (details.length() >= 0) {
					BusTicketDTO busTicketDTO = new BusTicketDTO();
					String source = (String) details.getJSONObject(0).getJSONObject("busTicketId").get("source");
					String status = (String) details.getJSONObject(0).getJSONObject("busTicketId").get("status");
					String destination = (String) details.getJSONObject(0).getJSONObject("busTicketId")
							.get("destination");
					String journeyDate = (String) details.getJSONObject(0).getJSONObject("busTicketId")
							.get("journeyDate");
					String operatorPnr = details.getJSONObject(0).getJSONObject("busTicketId").getString("operatorPnr");
					String busOperator = (String) details.getJSONObject(0).getJSONObject("busTicketId")
							.getString("busOperator");
					String ticketPnr = details.getJSONObject(0).getJSONObject("busTicketId").getString("ticketPnr");
					;
					double totalFare = (double) details.getJSONObject(0).getJSONObject("busTicketId")
							.getDouble("totalFare");
					String emtTxnId = (String) details.getJSONObject(0).getJSONObject("busTicketId").get("emtTxnId");
					String boardingAddress = (String) details.getJSONObject(0).getJSONObject("busTicketId")
							.get("boardingAddress");
					String arrTime = (String) details.getJSONObject(0).getJSONObject("busTicketId").get("arrTime");
					String emtTransactionScreenId = (String) details.getJSONObject(0).getJSONObject("busTicketId")
							.get("emtTransactionScreenId");
					String vPqTxnRefNo = "NA";
					if (details.getJSONObject(0).getJSONObject("busTicketId").getString("transaction") != null
							&& !details.getJSONObject(0).getJSONObject("busTicketId").getString("transaction").isEmpty()
							&& !details.getJSONObject(0).getJSONObject("busTicketId").getString("transaction")
									.equalsIgnoreCase("Null")) {
						vPqTxnRefNo = details.getJSONObject(0).getJSONObject("busTicketId").getJSONObject("transaction")
								.getString("transactionRefNo");
					}

					String deptTime = (String) details.getJSONObject(0).getJSONObject("busTicketId").get("deptTime");

					busTicketDTO.setDeptTime(deptTime);
					busTicketDTO.setSource(source);
					busTicketDTO.setDestination(destination);
					busTicketDTO.setJourneyDate(journeyDate);
					busTicketDTO.setOperatorPnr(operatorPnr);
					busTicketDTO.setBusOperator(busOperator);
					busTicketDTO.setTicketPnr(ticketPnr);
					busTicketDTO.setTotalFare(totalFare);
					busTicketDTO.setEmtTxnId(emtTxnId);
					busTicketDTO.setBoardingAddress(boardingAddress);
					busTicketDTO.setArrTime(arrTime);
					busTicketDTO.setEmtTransactionScreenId(emtTransactionScreenId);
					busTicketDTO.setTransactionRefNo(vPqTxnRefNo);
					busTicketDTO.setStatus(status);
					resp.setExtraInfo(busTicketDTO);

				}
			}

			resp.setSeatNo(seatNo);
			resp.setCode(code);
			resp.setMessage("Get All Book Tickets");
			resp.setDetails(list);
			resp.setStatus(ResponseStatus.SUCCESS.getKey());

		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			resp.setMessage("Service Unavailable");
			resp.setDetails(null);
			resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
		}
		return resp;

	}

	@Override
	public com.payqwikweb.app.model.response.bus.ResponseDTO isCancellable(IsCancellableReq dto) {

		com.payqwikweb.app.model.response.bus.ResponseDTO resp = new com.payqwikweb.app.model.response.bus.ResponseDTO();
		IsCancellableResp result = new IsCancellableResp();
		try {

			JSONObject payload = new JSONObject();
			payload.put("clientIp", "49.204.86.246");
			payload.put("cllientKey", UrlMetadatas.MDEX_CLIENTKEY);
			payload.put("clientToken", UrlMetadatas.MDEX_CLIENTTOKEN);
			payload.put("clientApiName", "VPayQwik");
			payload.put("bookId", dto.getBookId());
			payload.put("canceltype", "1");
			payload.put("seatNo", dto.getSeatNo());
			payload.put("ipAddress", dto.getIpAddress());
			Client client = Client.create();
			WebResource webResource = client
					.resource(UrlMetadatas.isCancelable(Role.ClIENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setCode("F00");
				resp.setMessage("Service Unavailable");
				resp.setStatus(ResponseStatus.FAILURE.getValue());

			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					JSONObject jObj = new JSONObject(strResponse);
					final boolean isCancellable = jObj.getBoolean("cancellable");
					final double refundAmount = jObj.getDouble("refundAmount");
					final double cancellationCharges = jObj.getDouble("cancellationCharges");
					final String seatNo = jObj.getString("seatNo");

					if (isCancellable) {
						resp.setStatus(ResponseStatus.SUCCESS.getKey());
						resp.setCode(ResponseStatus.SUCCESS.getValue());
					} else {
						resp.setStatus(ResponseStatus.FAILURE.getKey());
						resp.setCode(ResponseStatus.FAILURE.getValue());
					}
					result.setMessage("Get Cancellation Details");
					result.setCancellable(isCancellable);
					result.setCancellationCharges(cancellationCharges);
					result.setRefundAmount(refundAmount);
					result.setSeatNo(seatNo);
					resp.setDetails(result);
				} else {
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus(ResponseStatus.FAILURE.getValue());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus(ResponseStatus.FAILURE.getValue());
		}

		return resp;

	}

	@Override
	public com.payqwikweb.app.model.response.bus.ResponseDTO cancelBookedTicket(IsCancellableReq dto) {
		com.payqwikweb.app.model.response.bus.ResponseDTO resp = new com.payqwikweb.app.model.response.bus.ResponseDTO();
		CancelTicketResp result = new CancelTicketResp();
		try {
			JSONObject payload = new JSONObject();
			payload.put("clientIp", "49.204.86.246");
			payload.put("cllientKey", UrlMetadatas.MDEX_CLIENTKEY);
			payload.put("clientToken", UrlMetadatas.MDEX_CLIENTTOKEN);
			payload.put("clientApiName", "VPayQwik");
			payload.put("bookId", dto.getBookId());
			payload.put("seatNo", dto.getSeatNo());
			payload.put("ipAddress", dto.getIpAddress());
			payload.put("canceltype", "1");

			Client client = Client.create();
			WebResource webResource = client
					.resource(UrlMetadatas.cancelTicket(Role.ClIENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setCode("F00");
				resp.setMessage("Service Unavailable");
				resp.setStatus(ResponseStatus.FAILURE.getValue());

			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					JSONObject jObj = new JSONObject(strResponse);
					if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(jObj.getString("code"))) {
						final boolean isCancelRequested = jObj.getBoolean("cancelRequested");
						final double refundAmount = jObj.getDouble("refundAmount");
						final double cancellationCharges = jObj.getDouble("cancellationCharges");
						final boolean cancelStatus = jObj.getBoolean("cancelStatus");
						final String pnrNo = jObj.getString("pnrNo");
						final String operatorPnr = jObj.getString("operatorPnr");
						final String remarks = jObj.getString("remarks");
						if (isCancelRequested) {
							resp.setStatus(ResponseStatus.SUCCESS.getKey());
							resp.setCode(ResponseStatus.SUCCESS.getValue());

							String txnRefNo = dto.getvPqTxnId();
							String a[] = txnRefNo.split("D");

							JSONObject vpayload = new JSONObject();

							vpayload.put("sessionId", dto.getSessionId());
							vpayload.put("refundedAmount", refundAmount);
							vpayload.put("pQTxnId", a[0] + "");

							Client vpqClient = Client.create();
							WebResource vpqWebResource = vpqClient.resource(UrlMetadatas.getCancelBusBookedTicketURL(
									Version.VERSION_1, Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
							ClientResponse vpqresponse = vpqWebResource.accept("application/json")
									.type("application/json").header("hash", SecurityUtils.getHash(vpayload.toString()))
									.post(ClientResponse.class, vpayload);

							String vpqstrResponse = vpqresponse.getEntity(String.class);

							if (vpqstrResponse != null) {
								JSONObject vJObj = new JSONObject(vpqstrResponse);
								String status = vJObj.getString("status");
								String code = vJObj.getString("code");
								String vMessage = vJObj.getString("message");

								if (code.equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
									resp.setStatus(status);
									resp.setCode(code);
									resp.setMessage(vMessage);

									resp.setDetails(result);
								} else {
									resp.setStatus(status);
									resp.setCode(code);
									resp.setMessage(vMessage);
								}
							}
						} else {
							resp.setStatus(ResponseStatus.FAILURE.getKey());
							resp.setCode(ResponseStatus.FAILURE.getValue());
						}
						resp.setMessage("Get Cancellation Details");
						result.setCancellationCharges(cancellationCharges);
						result.setRefundAmount(refundAmount);
						result.setCancelRequested(isCancelRequested);
						result.setCancelStatus(cancelStatus);
						result.setPnrNo(pnrNo);

						/*
						 * final String
						 * cancelSeatNo=jObj.getString("cancelSeatNo"); if
						 * (cancelSeatNo!=null) {
						 * org.codehaus.jettison.json.JSONArray
						 * cancelSeats=jObj.getJSONArray("cancelSeatNo"); for
						 * (int i = 0; i < cancelSeats.length(); i++) {
						 * cList.add((String)cancelSeats.get(i)); } }
						 */

						// result.setCancelSeatNo(cancelSeatNo);

						result.setOperatorPnr(operatorPnr);
						result.setRemarks(remarks);
						resp.setDetails(result);
					} else {
						resp.setCode("F00");
						resp.setMessage("Ticket Not Canceled");
						resp.setStatus(ResponseStatus.FAILURE.getValue());
					}
				} else {
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus(ResponseStatus.FAILURE.getValue());
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus(ResponseStatus.FAILURE.getValue());
		}

		return resp;
	}

	@Override
	public com.payqwikweb.app.model.response.bus.ResponseDTO cancelAgentBookedTicket(IsCancellableReq dto) {
		com.payqwikweb.app.model.response.bus.ResponseDTO resp = new com.payqwikweb.app.model.response.bus.ResponseDTO();
		CancelTicketResp result = new CancelTicketResp();
		try {
			JSONObject payload = new JSONObject();
			payload.put("clientIp", "49.204.86.246");
			payload.put("cllientKey", UrlMetadatas.MDEX_CLIENTKEY);
			payload.put("clientToken", UrlMetadatas.MDEX_CLIENTTOKEN);
			payload.put("clientApiName", "VPayQwik");
			payload.put("bookId", dto.getBookId());
			payload.put("seatNo", dto.getSeatNo());
			payload.put("ipAddress", dto.getIpAddress());
			payload.put("canceltype", "1");

			Client client = Client.create();
			WebResource webResource = client
					.resource(UrlMetadatas.cancelTicket(Role.ClIENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setCode("F00");
				resp.setMessage("Service Unavailable");
				resp.setStatus(ResponseStatus.FAILURE.getValue());

			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					JSONObject jObj = new JSONObject(strResponse);
					if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(jObj.getString("code"))) {
						final boolean isCancelRequested = jObj.getBoolean("cancelRequested");
						final double refundAmount = jObj.getDouble("refundAmount");
						final double cancellationCharges = jObj.getDouble("cancellationCharges");
						final boolean cancelStatus = jObj.getBoolean("cancelStatus");
						final String pnrNo = jObj.getString("pnrNo");
						final String operatorPnr = jObj.getString("operatorPnr");
						final String remarks = jObj.getString("remarks");
						if (isCancelRequested) {
							resp.setStatus(ResponseStatus.SUCCESS.getKey());
							resp.setCode(ResponseStatus.SUCCESS.getValue());

							String txnRefNo = dto.getvPqTxnId();
							String a[] = txnRefNo.split("D");

							JSONObject vpayload = new JSONObject();

							vpayload.put("sessionId", dto.getSessionId());
							vpayload.put("refundedAmount", refundAmount);
							vpayload.put("pQTxnId", a[0] + "");

							Client vpqClient = Client.create();
							WebResource vpqWebResource = vpqClient
									.resource(UrlMetadatas.agentGetCancelBusBookedTicketURL(Version.VERSION_1,
											Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
							ClientResponse vpqresponse = vpqWebResource.accept("application/json")
									.type("application/json").header("hash", SecurityUtils.getHash(vpayload.toString()))
									.post(ClientResponse.class, vpayload);

							String vpqstrResponse = vpqresponse.getEntity(String.class);

							if (vpqstrResponse != null) {
								JSONObject vJObj = new JSONObject(vpqstrResponse);
								String status = vJObj.getString("status");
								String code = vJObj.getString("code");
								String vMessage = vJObj.getString("message");

								if (code.equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
									resp.setStatus(status);
									resp.setCode(code);
									resp.setMessage(vMessage);

									resp.setDetails(result);
								} else {
									resp.setStatus(status);
									resp.setCode(code);
									resp.setMessage(vMessage);
								}
							}
						} else {
							resp.setStatus(ResponseStatus.FAILURE.getKey());
							resp.setCode(ResponseStatus.FAILURE.getValue());
						}
						resp.setMessage("Get Cancellation Details");
						result.setCancellationCharges(cancellationCharges);
						result.setRefundAmount(refundAmount);
						result.setCancelRequested(isCancelRequested);
						result.setCancelStatus(cancelStatus);
						result.setPnrNo(pnrNo);

						/*
						 * final String
						 * cancelSeatNo=jObj.getString("cancelSeatNo"); if
						 * (cancelSeatNo!=null) {
						 * org.codehaus.jettison.json.JSONArray
						 * cancelSeats=jObj.getJSONArray("cancelSeatNo"); for
						 * (int i = 0; i < cancelSeats.length(); i++) {
						 * cList.add((String)cancelSeats.get(i)); } }
						 */

						// result.setCancelSeatNo(cancelSeatNo);

						result.setOperatorPnr(operatorPnr);
						result.setRemarks(remarks);
						resp.setDetails(result);
					} else {
						resp.setCode("F00");
						resp.setMessage("Ticket Not Canceled");
						resp.setStatus(ResponseStatus.FAILURE.getValue());
					}
				} else {
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus(ResponseStatus.FAILURE.getValue());
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus(ResponseStatus.FAILURE.getValue());
		}

		return resp;
	}

	// for imagica

	@Override
	public AllTransactionResponse getImagicaTransaction(AllTransactionRequest request) {
		AllTransactionResponse resp = new AllTransactionResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("page", request.getPage());
			payload.put("size", request.getSize());
			payload.put("fromDate", request.getStartDate());
			payload.put("toDate", request.getEndDate());
			// payload.put("reportType", request.getReportType());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getImagicaTransactionFilteredUrl(Version.VERSION_1,
					Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						org.json.JSONArray totalTransactionArray = null;
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							resp.setSuccess(true);
							totalTransactionArray = jobj.getJSONArray("details");
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
						resp.setJsonArray(totalTransactionArray);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	// for debit transaction

	@Override
	public AllTransactionResponse getDebitTransaction(AllTransactionRequest request) {
		AllTransactionResponse resp = new AllTransactionResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("page", request.getPage());
			payload.put("size", request.getSize());
			payload.put("fromDate", request.getStartDate());
			payload.put("toDate", request.getEndDate());
			// payload.put("reportType", request.getReportType());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getDebitTransactionUrl(Version.VERSION_1, Role.ADMIN,
					Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						org.json.JSONArray totalTransactionArray = null;
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							resp.setSuccess(true);
							totalTransactionArray = jobj.getJSONArray("details");
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
						resp.setJsonArray(totalTransactionArray);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	// for credit transaction

	@Override
	public AllTransactionResponse getCreditTransaction(AllTransactionRequest request) {
		AllTransactionResponse resp = new AllTransactionResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("page", request.getPage());
			payload.put("size", request.getSize());
			payload.put("fromDate", request.getStartDate());
			payload.put("toDate", request.getEndDate());
			// payload.put("reportType", request.getReportType());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getCreditTransactionUrl(Version.VERSION_1,
					Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						org.json.JSONArray totalTransactionArray = null;
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							resp.setSuccess(true);
							totalTransactionArray = jobj.getJSONArray("details");
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
						resp.setJsonArray(totalTransactionArray);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	// for single user

	@Override
	public GCMResponse getGCMIdByUserName(String userName, String sessionId) {
		GCMResponse resp = new GCMResponse();
		try {
			Client client = Client.create();
			JSONObject payload = new JSONObject();
			payload.put("sessionId", sessionId);
			payload.put("username", userName);
			WebResource webResource = client.resource(UrlMetadatas.getAdminGCMIDByUserName(Version.VERSION_1,
					Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456")).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (code.equalsIgnoreCase("S00")) {
							List<String> gcmIds = new ArrayList<>();
							resp.setSuccess(true);
							String details = JSONParserUtil.getString(jobj, "details");
							if (!CommonValidation.isNull(details)) {
								gcmIds.add(details);
								resp.setGcmList(gcmIds);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
		}
		return resp;
	}

	@Override
	public FlightResponseDTO getFlightDetails(GetFlightDetailsForAdmin getFlightDetails) {

		return null;
	}

	// For nikki chat Transaction

	@Override
	public AllTransactionResponse getNikkiTransaction(AllTransactionRequest request) {
		AllTransactionResponse resp = new AllTransactionResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("page", request.getPage());
			payload.put("size", request.getSize());
			payload.put("fromDate", request.getStartDate());
			payload.put("toDate", request.getEndDate());
			// payload.put("reportType", request.getReportType());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getNikkiTransactionFilteredUrl(Version.VERSION_1,
					Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						org.json.JSONArray totalTransactionArray = null;
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							resp.setSuccess(true);
							totalTransactionArray = jobj.getJSONArray("details");
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
						resp.setJsonArray(totalTransactionArray);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public TreatCardPlansResponse getListPlans(SessionDTO dto) {
		TreatCardPlansResponse resp = new TreatCardPlansResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", dto.getSessionId());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getTreatCardPlansUrl(Version.VERSION_1, Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						org.json.JSONArray arr = null;
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
							arr = jobj.getJSONArray("plans");
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setJsonArray(arr);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public TreatCardPlansResponse updatePlans(TreatCardPlanRequest dto) {
		TreatCardPlansResponse resp = new TreatCardPlansResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", dto.getSessionId());
			payload.put("days", dto.getDays());
			payload.put("amount", dto.getAmount());
			payload.put("status", dto.getStatus());
			payload.put("id", dto.getPlanId());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.updateTreatCardPlansUrl(Version.VERSION_1,
					Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							final org.json.JSONArray arr = jobj.getJSONArray("details");
							resp.setJsonArray(arr);
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public AllUserResponse getBusDetails(AllUserRequest request) {
		AllUserResponse resp = new AllUserResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			/*
			 * payload.put("page", request.getPage()); payload.put("size",
			 * request.getSize()); payload.put("userStatus",
			 * request.getStatus().getValue());
			 */
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getBusDetailForAdminURL(Version.VERSION_1,
					Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);

						resp.setMessage(message);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public AllUserResponse getAgentBusDetails(AllUserRequest request) {
		AllUserResponse resp = new AllUserResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			/*
			 * payload.put("page", request.getPage()); payload.put("size",
			 * request.getSize()); payload.put("userStatus",
			 * request.getStatus().getValue());
			 */
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.agentGetBusDetailForAdminURL(Version.VERSION_1,
					Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);

						resp.setMessage(message);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public AllUserResponse getAllFlightsDetailByDate(PagingDTO request) {
		AllUserResponse resp = new AllUserResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("fromDate", request.getStartDate());
			payload.put("toDate", request.getEndDate());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getAllFlightsDetailURLByDate(Version.VERSION_1,
					Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							final org.json.JSONArray details = jobj.getJSONArray("details");
							resp.setJsonArray(details);
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);

						resp.setMessage(message);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public AllUserResponse getAllAgentFlightsDetailByDate(PagingDTO request) {
		AllUserResponse resp = new AllUserResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("fromDate", request.getStartDate());
			payload.put("toDate", request.getEndDate());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.agentGetAllFlightsDetailURLByDate(Version.VERSION_1,
					Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							final org.json.JSONArray details = jobj.getJSONArray("details");
							resp.setJsonArray(details);
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);

						resp.setMessage(message);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}
	// HouseJoy

	@Override
	public AllUserResponse getHouseJoyByDate(PagingDTO dto) {
		AllUserResponse resp = new AllUserResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", dto.getSessionId());
			payload.put("fromDate", dto.getStartDate());
			payload.put("toDate", dto.getEndDate());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getAllFlightsDetailURLByDate(Version.VERSION_1,
					Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							final org.json.JSONArray details = jobj.getJSONArray("details");
							resp.setJsonArray(details);
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);

						resp.setMessage(message);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public AllUserResponse getHouseJoy(PagingDTO dto) {
		AllUserResponse resp = new AllUserResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", dto.getSessionId());
			payload.put("fromDate", dto.getStartDate());
			payload.put("toDate", dto.getEndDate());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getAllHouseJoyDetailURL(Version.VERSION_1,
					Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							final org.json.JSONArray details = jobj.getJSONArray("details");
							resp.setJsonArray(details);
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);

						resp.setMessage(message);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public FlightCancelableResp isCancellableFlight(FligthcancelableReq dto) {

		FlightCancelableResp resp = new FlightCancelableResp();

		try {

			JSONObject payload = new JSONObject();
			payload.put("clientIp", "49.204.86.246");
			payload.put("cllientKey", UrlMetadatas.MDEX_CLIENTKEY);
			payload.put("clientToken", UrlMetadatas.MDEX_CLIENTTOKEN);
			payload.put("clientApiName", "VPayQwik");
			payload.put("subUserId", dto.getSubUserId());
			payload.put("transactionScreenId", dto.getTransactionScreenId());

			Client client = Client.create();
			WebResource webResource = client
					.resource(UrlMetadatas.isFlightCancelable(Role.ClIENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setCode("F00");
				resp.setMessage("Service Unavailable");
				resp.setStatus(ResponseStatus.FAILURE.getValue());

			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					JSONObject jObj = new JSONObject(strResponse);
					/*
					 * final boolean
					 * isCancellable=jObj.getBoolean("cancellable"); final
					 * double refundAmount=jObj.getDouble("refundAmount"); final
					 * double
					 * cancellationCharges=jObj.getDouble("cancellationCharges")
					 * ; final String seatNo=jObj.getString("seatNo");
					 */

					String code = jObj.getString("code");

					if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
						resp.setStatus(ResponseStatus.SUCCESS.getKey());
						resp.setCode(ResponseStatus.SUCCESS.getValue());
						resp.setMessage("Flight cancelabel response");
					} else {
						resp.setStatus(ResponseStatus.FAILURE.getKey());
						resp.setCode(ResponseStatus.FAILURE.getValue());
					}
					resp.setMessage("Get Cancellation Details");

					resp.setDetails(strResponse);
				} else {
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus(ResponseStatus.FAILURE.getValue());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public ResponseDTO requestRefund(RequestRefund upload) {
		ResponseDTO resp = new ResponseDTO();
		try {
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getBulkUploadUrl(Version.VERSION_1, Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(upload.toString()))
					.post(ClientResponse.class, upload.getJsonRequest());
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						resp.setStatus(jobj.getString("status"));
						resp.setCode(jobj.getString("code"));
						resp.setMessage(jobj.getString("message"));
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					// resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			// resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public List<NEFTResponse> getMerchantNEFTListFiltered(DateDTO request, boolean flag) {
		List<NEFTResponse> neftResponses = new ArrayList<>();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("fromDate", request.getFromDate());
			payload.put("toDate", request.getToDate());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getMerchantNeftListFilteredURL(Version.VERSION_1,
					Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {

			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							final org.json.JSONArray list = jobj.getJSONArray("details");
							for (int i = 0; i < list.length(); i++) {
								NEFTResponse neft = new NEFTResponse();
								org.json.JSONObject json = list.getJSONObject(i);
								neft.setName(JSONParserUtil.getString(json, "name"));
								neft.setMobileNo(JSONParserUtil.getString(json, "mobileNumber"));
								neft.setEmail(JSONParserUtil.getString(json, "email"));
								neft.setTransactionDate(JSONParserUtil.getString(json, "transactionDate"));
								neft.setTransactionID(JSONParserUtil.getString(json, "transactionID"));
								neft.setBankName(JSONParserUtil.getString(json, "bankName"));
								neft.setIfscCode(JSONParserUtil.getString(json, "ifscCode"));
								neft.setAccountNumber(JSONParserUtil.getString(json, "beneficiaryAccountNumber"));
								neft.setAccountName(JSONParserUtil.getString(json, "beneficiaryAccountName"));
								neft.setUserAccount(JSONParserUtil.getString(json, "virtualAccount"));
								neft.setBankAccount(JSONParserUtil.getString(json, "bankVirtualAccount"));
								neft.setAmount(JSONParserUtil.getString(json, "amount"));
								neft.setStatus(JSONParserUtil.getString(json, "status"));
								neftResponses.add(neft);
							}
						}
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();

		}
		return neftResponses;

	}

	@Override
	public com.payqwikweb.app.model.response.bus.ResponseDTO getSingleTicketPdf(String sessionId, String emtTxnNo) {

		com.payqwikweb.app.model.response.bus.ResponseDTO resp = new com.payqwikweb.app.model.response.bus.ResponseDTO();

		List<TravellerDetailsDTO> list = new ArrayList<>();
		List<String> seatNo = new ArrayList<>();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", sessionId);
			payload.put("bookingTxnId", emtTxnNo);
			Client vpqClient = Client.create();
			WebResource vpqWebResource = vpqClient.resource(UrlMetadatas.getTravellerDetailsForAdmin(Version.VERSION_1,
					Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse vpqresponse = vpqWebResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = vpqresponse.getEntity(String.class);
			org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
			final String code = jobj.getString("code");
			if (code.equalsIgnoreCase("S00")) {
				org.json.JSONArray details = jobj.getJSONArray("details");
				for (int i = 0; i < details.length(); i++) {
					TravellerDetailsDTO dto = new TravellerDetailsDTO();
					dto.setfName((String) details.getJSONObject(i).get("fName"));
					dto.setlName((String) details.getJSONObject(i).get("lName"));
					dto.setGender((String) details.getJSONObject(i).get("gender"));
					dto.setSeatNo((String) details.getJSONObject(i).get("seatNo"));
					dto.setFare((String) details.getJSONObject(i).get("fare"));
					dto.setAge((details.getJSONObject(i).getInt("age")) + "");
					list.add(dto);
					seatNo.add((String) details.getJSONObject(i).get("seatNo"));
				}
				if (details.length() >= 0) {
					BusTicketDTO busTicketDTO = new BusTicketDTO();
					String source = (String) details.getJSONObject(0).getJSONObject("busTicketId").get("source");
					String status = (String) details.getJSONObject(0).getJSONObject("busTicketId").get("status");
					String destination = (String) details.getJSONObject(0).getJSONObject("busTicketId")
							.get("destination");
					String journeyDate = (String) details.getJSONObject(0).getJSONObject("busTicketId")
							.get("journeyDate");
					String operatorPnr = details.getJSONObject(0).getJSONObject("busTicketId").getString("operatorPnr");
					String busOperator = (String) details.getJSONObject(0).getJSONObject("busTicketId")
							.getString("busOperator");
					String ticketPnr = details.getJSONObject(0).getJSONObject("busTicketId").getString("ticketPnr");
					;
					double totalFare = (double) details.getJSONObject(0).getJSONObject("busTicketId")
							.getDouble("totalFare");
					String emtTxnId = (String) details.getJSONObject(0).getJSONObject("busTicketId").get("emtTxnId");
					String boardingAddress = (String) details.getJSONObject(0).getJSONObject("busTicketId")
							.get("boardingAddress");
					String arrTime = (String) details.getJSONObject(0).getJSONObject("busTicketId").get("arrTime");
					String emtTransactionScreenId = (String) details.getJSONObject(0).getJSONObject("busTicketId")
							.get("emtTransactionScreenId");
					String vPqTxnRefNo = "NA";
					if (details.getJSONObject(0).getJSONObject("busTicketId").getString("transaction") != null
							&& !details.getJSONObject(0).getJSONObject("busTicketId").getString("transaction").isEmpty()
							&& !details.getJSONObject(0).getJSONObject("busTicketId").getString("transaction")
									.equalsIgnoreCase("Null")) {
						vPqTxnRefNo = details.getJSONObject(0).getJSONObject("busTicketId").getJSONObject("transaction")
								.getString("transactionRefNo");
					}

					String deptTime = (String) details.getJSONObject(0).getJSONObject("busTicketId").get("deptTime");
					String boardTime = details.getJSONObject(0).getJSONObject("busTicketId").getString("boardTime");
					long created = details.getJSONObject(0).getJSONObject("busTicketId").getLong("created");

					Date date = new Date(created);

					String dateStr = simformat.format(date);
					busTicketDTO.setCreated(dateStr);
					busTicketDTO.setBoardTime(boardTime);
					busTicketDTO.setDeptTime(deptTime);
					busTicketDTO.setSource(source);
					busTicketDTO.setDestination(destination);
					busTicketDTO.setJourneyDate(journeyDate);
					busTicketDTO.setOperatorPnr(operatorPnr);
					busTicketDTO.setBusOperator(busOperator);
					busTicketDTO.setTicketPnr(ticketPnr);
					busTicketDTO.setTotalFare(totalFare);
					busTicketDTO.setEmtTxnId(emtTxnId);
					busTicketDTO.setBoardingAddress(boardingAddress);
					busTicketDTO.setArrTime(arrTime);
					busTicketDTO.setEmtTransactionScreenId(emtTransactionScreenId);
					busTicketDTO.setTransactionRefNo(vPqTxnRefNo);
					busTicketDTO.setStatus(status);
					busTicketDTO.setDto(list);
					resp.setExtraInfo(busTicketDTO);

				}
			}

			resp.setSeatNo(seatNo);
			resp.setCode(code);
			resp.setMessage("Get All Book Tickets");
			resp.setDetails(list);
			resp.setStatus(ResponseStatus.SUCCESS.getKey());

		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			resp.setMessage("Service Unavailable");
			resp.setDetails(null);
			resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
		}
		return resp;

	}

	@Override
	public com.payqwikweb.app.model.response.bus.ResponseDTO getSingleAgentTicketPdf(String sessionId,
			String emtTxnNo) {

		com.payqwikweb.app.model.response.bus.ResponseDTO resp = new com.payqwikweb.app.model.response.bus.ResponseDTO();

		List<TravellerDetailsDTO> list = new ArrayList<>();
		List<String> seatNo = new ArrayList<>();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", sessionId);
			payload.put("bookingTxnId", emtTxnNo);
			Client vpqClient = Client.create();
			WebResource vpqWebResource = vpqClient.resource(UrlMetadatas
					.agentGetTravellerDetailsForAdmin(Version.VERSION_1, Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse vpqresponse = vpqWebResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = vpqresponse.getEntity(String.class);
			org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
			final String code = jobj.getString("code");
			if (code.equalsIgnoreCase("S00")) {
				org.json.JSONArray details = jobj.getJSONArray("details");
				for (int i = 0; i < details.length(); i++) {
					TravellerDetailsDTO dto = new TravellerDetailsDTO();
					dto.setfName((String) details.getJSONObject(i).get("fName"));
					dto.setlName((String) details.getJSONObject(i).get("lName"));
					dto.setGender((String) details.getJSONObject(i).get("gender"));
					dto.setSeatNo((String) details.getJSONObject(i).get("seatNo"));
					dto.setFare((String) details.getJSONObject(i).get("fare"));
					dto.setAge((details.getJSONObject(i).getInt("age")) + "");
					list.add(dto);
					seatNo.add((String) details.getJSONObject(i).get("seatNo"));
				}
				if (details.length() >= 0) {
					BusTicketDTO busTicketDTO = new BusTicketDTO();
					String source = (String) details.getJSONObject(0).getJSONObject("busTicketId").get("source");
					String status = (String) details.getJSONObject(0).getJSONObject("busTicketId").get("status");
					String destination = (String) details.getJSONObject(0).getJSONObject("busTicketId")
							.get("destination");
					String journeyDate = (String) details.getJSONObject(0).getJSONObject("busTicketId")
							.get("journeyDate");
					String operatorPnr = details.getJSONObject(0).getJSONObject("busTicketId").getString("operatorPnr");
					String busOperator = (String) details.getJSONObject(0).getJSONObject("busTicketId")
							.getString("busOperator");
					String ticketPnr = details.getJSONObject(0).getJSONObject("busTicketId").getString("ticketPnr");
					;
					double totalFare = (double) details.getJSONObject(0).getJSONObject("busTicketId")
							.getDouble("totalFare");
					String emtTxnId = (String) details.getJSONObject(0).getJSONObject("busTicketId").get("emtTxnId");
					String boardingAddress = (String) details.getJSONObject(0).getJSONObject("busTicketId")
							.get("boardingAddress");
					String arrTime = (String) details.getJSONObject(0).getJSONObject("busTicketId").get("arrTime");
					String emtTransactionScreenId = (String) details.getJSONObject(0).getJSONObject("busTicketId")
							.get("emtTransactionScreenId");
					String vPqTxnRefNo = "NA";
					if (details.getJSONObject(0).getJSONObject("busTicketId").getString("transaction") != null
							&& !details.getJSONObject(0).getJSONObject("busTicketId").getString("transaction").isEmpty()
							&& !details.getJSONObject(0).getJSONObject("busTicketId").getString("transaction")
									.equalsIgnoreCase("Null")) {
						vPqTxnRefNo = details.getJSONObject(0).getJSONObject("busTicketId").getJSONObject("transaction")
								.getString("transactionRefNo");
					}

					String deptTime = (String) details.getJSONObject(0).getJSONObject("busTicketId").get("deptTime");
					String boardTime = details.getJSONObject(0).getJSONObject("busTicketId").getString("boardTime");
					long created = details.getJSONObject(0).getJSONObject("busTicketId").getLong("created");

					Date date = new Date(created);

					String dateStr = simformat.format(date);
					busTicketDTO.setCreated(dateStr);
					busTicketDTO.setBoardTime(boardTime);
					busTicketDTO.setDeptTime(deptTime);
					busTicketDTO.setSource(source);
					busTicketDTO.setDestination(destination);
					busTicketDTO.setJourneyDate(journeyDate);
					busTicketDTO.setOperatorPnr(operatorPnr);
					busTicketDTO.setBusOperator(busOperator);
					busTicketDTO.setTicketPnr(ticketPnr);
					busTicketDTO.setTotalFare(totalFare);
					busTicketDTO.setEmtTxnId(emtTxnId);
					busTicketDTO.setBoardingAddress(boardingAddress);
					busTicketDTO.setArrTime(arrTime);
					busTicketDTO.setEmtTransactionScreenId(emtTransactionScreenId);
					busTicketDTO.setTransactionRefNo(vPqTxnRefNo);
					busTicketDTO.setStatus(status);
					busTicketDTO.setDto(list);
					resp.setExtraInfo(busTicketDTO);

				}
			}

			resp.setSeatNo(seatNo);
			resp.setCode(code);
			resp.setMessage("Get All Book Tickets");
			resp.setDetails(list);
			resp.setStatus(ResponseStatus.SUCCESS.getKey());

		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			resp.setMessage("Service Unavailable");
			resp.setDetails(null);
			resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
		}
		return resp;

	}

	/* For RBI reports */

	@Override
	public ResponseDTO findTotalTransactionByMonth(AllTransactionRequest request) {
		ResponseDTO resp = new ResponseDTO();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.transactionCountByMonthWise(Version.VERSION_1,
					Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
			} else {
				if (strResponse != null) {
					JSONObject jobj = new JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						JSONObject details = jobj.getJSONObject("details");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							resp.setSuccess(true);
							Map<String, String> values = new LinkedHashMap<String, String>();
							Iterator<?> keys = details.keys();
							while (keys.hasNext()) {
								String key = (String) keys.next();
								String value = details.getString(key);
								values.put(key, value);
							}
							resp.setTransvalue(values);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setMessage(message);
						resp.setInfo(details);
						return resp;
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
		}
		return resp;
	}

	@Override
	public UserResponseDTO findTotalUserByMonth(AllTransactionRequest request) {
		UserResponseDTO resp = new UserResponseDTO();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.userCountByMonthWise(Version.VERSION_1, Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
			} else {
				if (strResponse != null) {
					JSONObject jobj = new JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						JSONObject details = jobj.getJSONObject("details");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							resp.setSuccess(true);
							Map<String, String> values = new LinkedHashMap<String, String>();
							Iterator<?> keys = details.keys();
							while (keys.hasNext()) {
								String key = (String) keys.next();
								String value = details.getString(key);
								values.put(key, value);
							}
							resp.setTransvalue(values);
						} else {
							resp.setSuccess(false);
						}

						resp.setCode(code);
						// resp.setStatus(status);
						resp.setMessage(message);
						// resp.setJsonArray(details);
						resp.setInfo(details);
						return resp;

						// resp.setResponse(strResponse);
						// resp.setJsonObject(details);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					// resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			// resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public AllUserResponse getWalletBalanceReport(AllUserRequest request) {
		AllUserResponse resp = new AllUserResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("page", request.getPage());
			payload.put("size", request.getSize());
			payload.put("userStatus", request.getStatus().getValue());

			logger.info("Payload " + payload);

			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getWalletBalance(Version.VERSION_1, Role.ADMIN, Device.WEBSITE, Language.ENGLISH));

			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			logger.info("URL "
					+ UrlMetadatas.getWalletBalance(Version.VERSION_1, Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					final String status = (String) jobj.get("status");
					final String code = (String) jobj.get("code");
					final String message = (String) jobj.get("message");
					final org.json.JSONObject details = jobj.getJSONObject("details");
					final org.json.JSONArray totalUser = (org.json.JSONArray) jobj.getJSONObject("details")
							.getJSONArray("content");
					long numberOfElements = details.getLong("numberOfElements");
					boolean firstPage = details.getBoolean("firstPage");
					boolean lastPage = details.getBoolean("lastPage");
					long size = details.getLong("size");
					long totalPages = details.getLong("totalPages");
					resp.setTotalPages(totalPages);
					resp.setSize(size);
					resp.setNumberOfElements(numberOfElements);
					resp.setFirstPage(firstPage);
					resp.setLastPage(lastPage);
					if (code.equalsIgnoreCase("S00")) {
						resp.setSuccess(true);
					} else {
						resp.setSuccess(false);
					}
					resp.setCode(code);
					resp.setStatus(status);
					resp.setMessage(message);
					resp.setResponse(strResponse);
					resp.setTotalUser(totalUser.length());
					resp.setJsonArray(totalUser);
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public UserTransactionResponse getDailyTransactionReportValues(PagingDTO dto) {
		UserTransactionResponse resp = new UserTransactionResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", dto.getSessionId());
			payload.put("startDate", dto.getStartDate());

			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getDailyValuesListURL(Version.VERSION_1, Role.ADMIN,
					Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					final String status = (String) jobj.get("status");
					final String code = (String) jobj.get("code");
					final String message = (String) jobj.get("message");
					if (code.equalsIgnoreCase("S00")) {
						resp.setSuccess(true);
					} else {
						resp.setSuccess(false);
					}
					resp.setCode(code);
					resp.setStatus(status);
					resp.setMessage(message);
					resp.setResponse(strResponse);
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public MISResponce getMISreport(MISDetailRequest request) {
		MISResponce resp = new MISResponce();

		List<Object> loadMoneyList = new ArrayList<Object>();
		List<Object> spentList = new ArrayList<Object>();
		List<Object> closingList = new ArrayList<Object>();
		List<Object> createdList = new ArrayList<Object>();
		List<Object> walletList = new ArrayList<Object>();

		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());

			Client client = Client.create();

			WebResource webResource = client.resource(
					UrlMetadatas.getMISReport(Version.VERSION_1, Role.ADMIN, Device.WEBSITE, Language.ENGLISH));

			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);

			if (response.getStatus() != 200) {
				logger.info("Response :: " + response.getStatus());
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				logger.info("Response : " + response.getStatus());
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					final String status = (String) jobj.get("status");
					final String code = (String) jobj.get("code");
					final String message = (String) jobj.get("message");

					if (code.equalsIgnoreCase("S00")) {
						org.json.JSONObject json = JSONParserUtil.getObject(jobj, "details");
						org.json.JSONArray closing = json.getJSONArray("closing");
						for (int i = 0; i < closing.length(); i++) {
							closingList.add(closing.get(i));
						}
						org.json.JSONArray wallet = json.getJSONArray("wallet");
						for (int i = 0; i < wallet.length(); i++) {
							walletList.add(wallet.get(i));
						}
						org.json.JSONArray created = json.getJSONArray("created");
						for (int i = 0; i < created.length(); i++) {
							createdList.add(created.get(i));
						}
						org.json.JSONArray spent = json.getJSONArray("spent");
						for (int i = 0; i < spent.length(); i++) {
							spentList.add(spent.get(i));
						}
						org.json.JSONArray loadMoney = json.getJSONArray("loadMoney");
						for (int i = 0; i < loadMoney.length(); i++) {
							loadMoneyList.add(loadMoney.get(i));
						}
						resp.setAdditionalInfo(json);
						resp.setClosing(closingList);
						resp.setLoadMoney(loadMoneyList);
						resp.setCreated(createdList);
						resp.setWallet(walletList);
						resp.setSpent(spentList);
						resp.setSuccess(true);

					} else {
						resp.setSuccess(false);
					}
					resp.setCode(code);
					resp.setStatus(status);
					resp.setMessage(message);
					resp.setResponse(strResponse);
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public WalletResponse getWalletReport(MISDetailRequest request) {

		WalletResponse resp = new WalletResponse();

		List<Object> billdeskList = new ArrayList<Object>();
		List<Object> ccavenuList = new ArrayList<Object>();
		List<Object> user = new ArrayList<Object>();

		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("date", request.getDate());
			payload.put("endDate", request.getEndDate());
			Client client = Client.create();

			WebResource webResource = client.resource(
					UrlMetadatas.getWalletLoadReport(Version.VERSION_1, Role.ADMIN, Device.WEBSITE, Language.ENGLISH));

			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);

			if (response.getStatus() != 200) {
				// logger.info("Response :: " + response.getStatus());
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				// logger.info("Response : " + response.getStatus());
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					final String status = (String) jobj.get("status");
					final String code = (String) jobj.get("code");
					final String message = (String) jobj.get("message");
					if (code.equalsIgnoreCase("S00")) {
						org.json.JSONArray details = jobj.getJSONArray("details");
						org.json.JSONArray userList = jobj.getJSONArray("userList");

						resp.setLoadMoneyDetails(details);
						resp.setUserListDetails(userList);
						resp.setBilldesk(billdeskList);
						resp.setCcavenu(ccavenuList);
						resp.setUserDetails(user);
						resp.setSuccess(true);
					} else {
						resp.setSuccess(false);
					}
					resp.setCode(code);
					resp.setStatus(status);
					resp.setMessage(message);
					resp.setResponse(strResponse);
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public AllUserResponse getWalletBalanceReportByDate(PagingDTO request) {
		AllUserResponse resp = new AllUserResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("page", request.getPage());
			payload.put("size", request.getSize());
			payload.put("endDate", request.getEndDate());
			payload.put("startDate", request.getStartDate());

			logger.info("Payload " + payload);

			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getWalletBalanceByDate(Version.VERSION_1, Role.ADMIN,
					Device.WEBSITE, Language.ENGLISH));

			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			logger.info("URL " + UrlMetadatas.getWalletBalanceByDate(Version.VERSION_1, Role.ADMIN, Device.WEBSITE,
					Language.ENGLISH));
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					final String status = (String) jobj.get("status");
					final String code = (String) jobj.get("code");
					final String message = (String) jobj.get("message");
					final org.json.JSONObject details = jobj.getJSONObject("details");
					final org.json.JSONArray totalUser = (org.json.JSONArray) jobj.getJSONObject("details")
							.getJSONArray("content");
					long numberOfElements = details.getLong("numberOfElements");
					boolean firstPage = details.getBoolean("firstPage");
					boolean lastPage = details.getBoolean("lastPage");
					long size = details.getLong("size");
					long totalPages = details.getLong("totalPages");
					resp.setTotalPages(totalPages);
					resp.setSize(size);
					resp.setNumberOfElements(numberOfElements);
					resp.setFirstPage(firstPage);
					resp.setLastPage(lastPage);
					if (code.equalsIgnoreCase("S00")) {
						resp.setSuccess(true);
					} else {
						resp.setSuccess(false);
					}
					resp.setCode(code);
					resp.setStatus(status);
					resp.setMessage(message);
					resp.setResponse(strResponse);
					resp.setTotalUser(totalUser.length());
					resp.setJsonArray(totalUser);
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public AllUserResponse getBusDetailsByDate(PagingDTO dto) {
		AllUserResponse resp = new AllUserResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", dto.getSessionId());
			payload.put("fromDate", dto.getStartDate());
			payload.put("toDate", dto.getEndDate());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getBusDetailForAdminURLByDate(Version.VERSION_1,
					Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);

						resp.setMessage(message);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public AllUserResponse getAgentBusDetailsByDate(PagingDTO dto) {
		AllUserResponse resp = new AllUserResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", dto.getSessionId());
			payload.put("fromDate", dto.getStartDate());
			payload.put("toDate", dto.getEndDate());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.agentGetBusDetailForAdminURLByDate(Version.VERSION_1,
					Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);

						resp.setMessage(message);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public ResponseDTO findSingleUserTransaction(String sessionId, String username) {
		ResponseDTO resp = new ResponseDTO();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", sessionId);
			payload.put("username", username);
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.transactionCountByMonthWiseSingleUser(
					Version.VERSION_1, Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
			} else {
				if (strResponse != null) {
					JSONObject jobj = new JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						JSONObject details = jobj.getJSONObject("details");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
							Map<String, String> values = new LinkedHashMap<String, String>();
							Iterator<?> keys = details.keys();
							while (keys.hasNext()) {
								String key = (String) keys.next();
								String value = details.getString(key);
								values.put(key, value);
							}
							resp.setTransvalue(values);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setMessage(message);
						resp.setInfo(details);
						return resp;
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
		}
		return resp;
	}

	@Override
	public TreatCardRegisterListResponse getRegisterList(SessionDTO dto) {
		TreatCardRegisterListResponse resp = new TreatCardRegisterListResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", dto.getSessionId());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getRegisterListUrl(Version.VERSION_1, Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						org.json.JSONArray arr = null;
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
							arr = jobj.getJSONArray("details");
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setJsonArray(arr);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public TreatCardRegisterListResponse getRegisterListFiltered(TreatCardRegisterList dto) {
		TreatCardRegisterListResponse resp = new TreatCardRegisterListResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", dto.getSessionId());
			payload.put("fromDate", dto.getFromDate());
			payload.put("toDate", dto.getToDate());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getFilteredRegisterListUrl(Version.VERSION_1,
					Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						org.json.JSONArray arr = null;
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
							arr = jobj.getJSONArray("details");
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setJsonArray(arr);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public TListResponse getTreatCardTransactionList(AllTransactionRequest request) {
		TListResponse resp = new TListResponse();
		try {
			Client client = Client.create();
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			WebResource webResource = client.resource(UrlMetadatas.getTreatCardTransactionUrl(Version.VERSION_1,
					Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456")).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (code.equalsIgnoreCase("S00")) {
							List<TListDTO> transactions = new ArrayList<>();
							resp.setSuccess(true);
							org.json.JSONArray details = JSONParserUtil.getArray(jobj, "details");
							if (details != null && details.length() > 0) {
								for (int i = 0; i < details.length(); i++) {
									org.json.JSONObject json = details.getJSONObject(i);
									TListDTO dto = new TListDTO();
									dto.setUsername(JSONParserUtil.getString(json, "username"));
									dto.setEmail(JSONParserUtil.getString(json, "email"));
									dto.setContactNo(JSONParserUtil.getString(json, "contactNo"));
									dto.setTransactionRefNo(JSONParserUtil.getString(json, "transactionRefNo"));
									dto.setCurrentBalance(JSONParserUtil.getString(json, "currentBalance"));
									dto.setDateOfTransaction(JSONParserUtil.getString(json, "dateOfTransaction"));
									dto.setDebit(JSONParserUtil.getString(json, "debit"));
									dto.setAmount(JSONParserUtil.getString(json, "amount"));
									dto.setStatus(JSONParserUtil.getString(json, "status"));
									dto.setDescription(JSONParserUtil.getString(json, "description"));
									transactions.add(dto);
								}
								resp.setList(transactions);
							} else {
								resp.setSuccess(false);
								resp.setMessage("Transaction List Empty");
							}
						} else {
							resp.setSuccess(false);
							resp.setMessage("Please try Later");
						}
					} else {
						resp.setSuccess(false);
					}
				} else {
					resp.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
		}
		return resp;
	}

	@Override
	public TListResponse getTreatCardTransactionListWithFilter(AllTransactionRequest request) {
		TListResponse resp = new TListResponse();
		try {
			Client client = Client.create();
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("startDate", request.getStartDate());
			payload.put("endDate", request.getEndDate());
			WebResource webResource = client.resource(UrlMetadatas.getFilteredTreatCardTransactionUrl(Version.VERSION_1,
					Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash("123456")).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (code.equalsIgnoreCase("S00")) {
							List<TListDTO> transactions = new ArrayList<>();
							resp.setSuccess(true);
							org.json.JSONArray details = JSONParserUtil.getArray(jobj, "details");
							if (details != null && details.length() > 0) {
								for (int i = 0; i < details.length(); i++) {
									org.json.JSONObject json = details.getJSONObject(i);
									TListDTO dto = new TListDTO();
									dto.setUsername(JSONParserUtil.getString(json, "username"));
									dto.setEmail(JSONParserUtil.getString(json, "email"));
									dto.setContactNo(JSONParserUtil.getString(json, "contactNo"));
									dto.setTransactionRefNo(JSONParserUtil.getString(json, "transactionRefNo"));
									dto.setCurrentBalance(JSONParserUtil.getString(json, "currentBalance"));
									dto.setDateOfTransaction(JSONParserUtil.getString(json, "dateOfTransaction"));
									dto.setDebit(JSONParserUtil.getString(json, "debit"));
									dto.setAmount(JSONParserUtil.getString(json, "amount"));
									dto.setStatus(JSONParserUtil.getString(json, "status"));
									dto.setDescription(JSONParserUtil.getString(json, "description"));
									transactions.add(dto);
								}
								resp.setList(transactions);
							} else {
								resp.setSuccess(false);
								resp.setMessage("Transaction List Empty");
							}
						} else {
							resp.setSuccess(false);
							resp.setMessage("Please try Later");
						}
					} else {
						resp.setSuccess(false);
					}
				} else {
					resp.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
		}
		return resp;
	}

	@Override
	public TreatCardRegisterListResponse getTreatCardCount(SessionDTO dto) {
		TreatCardRegisterListResponse resp = new TreatCardRegisterListResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", dto.getSessionId());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getTreatCardCountUrl(Version.VERSION_1, Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						org.json.JSONArray arr = null;
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
							arr = jobj.getJSONArray("details");
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setJsonArray(arr);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public AllUserResponse getAllIPayRefundTxn(AllTransactionRequest request) {

		AllUserResponse resp = new AllUserResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("page", request.getPage());
			payload.put("size", request.getSize());
			payload.put("reportType", "ALL");

			logger.info("Payload " + payload);

			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getAllIpayRefundTxnsAdminURL(Version.VERSION_1,
					Role.ADMIN, Device.WEBSITE, Language.ENGLISH));

			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			logger.info("URL "
					+ UrlMetadatas.getAllUserUrl(Version.VERSION_1, Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {

				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					final String status = (String) jobj.get("status");
					final String code = (String) jobj.get("code");
					final String message = (String) jobj.get("message");
					final org.json.JSONObject details = jobj.getJSONObject("details");
					final org.json.JSONArray totalUser = (org.json.JSONArray) jobj.getJSONObject("details")
							.getJSONArray("content");
					long numberOfElements = details.getLong("numberOfElements");
					boolean firstPage = details.getBoolean("firstPage");
					boolean lastPage = details.getBoolean("lastPage");
					long size = details.getLong("size");
					long totalPages = details.getLong("totalPages");
					resp.setTotalPages(totalPages);
					resp.setSize(size);
					resp.setNumberOfElements(numberOfElements);
					resp.setFirstPage(firstPage);
					resp.setLastPage(lastPage);
					if (code.equalsIgnoreCase("S00")) {
						resp.setSuccess(true);
					} else {
						resp.setSuccess(false);
					}
					resp.setCode(code);
					resp.setStatus(status);
					resp.setMessage(message);
					resp.setResponse(strResponse);
					resp.setTotalUser(totalUser.length());
					resp.setJsonArray(totalUser);
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public AllUserResponse getAllIPayRefundByDate(AllTransactionRequest request) {

		AllUserResponse resp = new AllUserResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("page", request.getPage());
			payload.put("size", request.getSize());
			payload.put("startDate", request.getStartDate());
			payload.put("endDate", request.getEndDate());
			payload.put("reportType", "ALL");
			logger.info("Payload " + payload);

			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getAllIpayRefundTxnsAdminURL(Version.VERSION_1,
					Role.ADMIN, Device.WEBSITE, Language.ENGLISH));

			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			logger.info("URL "
					+ UrlMetadatas.getAllUserUrl(Version.VERSION_1, Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {

				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					final String status = (String) jobj.get("status");
					final String code = (String) jobj.get("code");
					final String message = (String) jobj.get("message");
					final org.json.JSONObject details = jobj.getJSONObject("details");
					final org.json.JSONArray totalUser = (org.json.JSONArray) jobj.getJSONObject("details")
							.getJSONArray("content");
					long numberOfElements = details.getLong("numberOfElements");
					boolean firstPage = details.getBoolean("firstPage");
					boolean lastPage = details.getBoolean("lastPage");
					long size = details.getLong("size");
					long totalPages = details.getLong("totalPages");
					resp.setTotalPages(totalPages);
					resp.setSize(size);
					resp.setNumberOfElements(numberOfElements);
					resp.setFirstPage(firstPage);
					resp.setLastPage(lastPage);
					if (code.equalsIgnoreCase("S00")) {
						resp.setSuccess(true);
					} else {
						resp.setSuccess(false);
					}
					resp.setCode(code);
					resp.setStatus(status);
					resp.setMessage(message);
					resp.setResponse(strResponse);
					resp.setTotalUser(totalUser.length());
					resp.setJsonArray(totalUser);
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public GiftCardDetailResponse getAllGiftCards(DateDTO dto) {
		GiftCardDetailResponse resp = new GiftCardDetailResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", dto.getSessionId());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getWoohooGiftCardListUrl(Version.VERSION_1,
					Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
			} else {
				if (strResponse != null) {
					JSONObject jobj = new JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						JSONArray totalTransactionArray = null;
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							totalTransactionArray = jobj.getJSONArray("details");
							List<WoohooGiftCardDTO> cs = new ArrayList<>();
							for (int i = 0; i < totalTransactionArray.length(); i++) {
								WoohooGiftCardDTO card = new WoohooGiftCardDTO();
								JSONObject cObj = totalTransactionArray.getJSONObject(i);
								card.setCard_price(cObj.getString("card_price"));
								card.setCardName(cObj.getString("cardName"));
								card.setCardnumber(cObj.getString("cardnumber"));
								card.setContanctNo(cObj.getString("contanctNo"));
								card.setEmailId(cObj.getString("emailId"));
								card.setExpiry_date(cObj.getString("expiry_date"));
								card.setOrderId(cObj.getString("orderId"));
								card.setRetrivalRefNo(cObj.getString("retrivalRefNo"));
								card.setTransactionRefNo(cObj.getString("transactionRefNo"));
								card.setStatus(cObj.getString("status"));
								card.setPin_or_url(cObj.getString("pin_or_url"));
								card.setUserFirstName(cObj.getString("userFirstName"));
								card.setDateOfTrasnaction(cObj.getString("dateOfTrasnaction"));
								card.setDescription(cObj.getString("description"));
								card.setUserPhoneNumber(cObj.getString("userPhoneNumber"));
								cs.add(card);
							}
							resp.setGiftCards(cs);
							resp.setSuccess(true);
						} else {
							resp.setCode(code);
							resp.setMessage(message);
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setMessage(message);
					}
				} else {
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
		}
		return resp;
	}

	@Override
	public AllTransactionResponse getWoohooTransactionReport(AllTransactionRequest request) {
		AllTransactionResponse resp = new AllTransactionResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("page", request.getPage());
			payload.put("size", request.getSize());
			payload.put("fromDate", request.getStartDate());
			payload.put("toDate", request.getEndDate());
			// payload.put("reportType", request.getReportType());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.listWooHooTransactionReport(Version.VERSION_1,
					Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						org.json.JSONArray totalTransactionArray = null;
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							resp.setSuccess(true);
							totalTransactionArray = jobj.getJSONArray("details");
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
						resp.setJsonArray(totalTransactionArray);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public MobileOTPResponse agentResendOTP(MobileOTPRequest dto) {
		MobileOTPResponse resp = new MobileOTPResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", dto.getSessionId());
			payload.put("mobileNumber", dto.getMobileNumber());

			System.err.println(payload.toString());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.agentResendMobileOtp(Version.VERSION_1, Role.AGENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			System.err.println(strResponse);

			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {

				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						final Object details = (String) jobj.get("details");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setDetails(details);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());

		}
		return resp;
	}

	@Override
	public TransactionReportResponse getUpiTransaction(AllUserRequest userRequest) {
		TransactionReportResponse resp = new TransactionReportResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", userRequest.getSessionId());
			payload.put("page", userRequest.getPage());
			payload.put("size", userRequest.getSize());
			payload.put("pageable", userRequest.isPageable());
			payload.put("fromDate", userRequest.getStartDate());
			payload.put("toDate", userRequest.getEndDate());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client
					.resource(UrlMetadatas.getUpiTxn(Version.VERSION_1, Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				// resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (code.equalsIgnoreCase("S00") && jobj.getString("details") != null
								&& jobj.getString("details").contains("[")) {
							List<TListDTO> transactions = new ArrayList<>();
							resp.setSuccess(true);
							org.json.JSONArray details = JSONParserUtil.getArray(jobj, "details");
							if (details != null && details.length() > 0) {
								for (int i = 0; i < details.length(); i++) {
									org.json.JSONObject json = details.getJSONObject(i);
									TListDTO dto = new TListDTO();
									dto.setUsername(JSONParserUtil.getString(json, "username"));
									dto.setEmail(JSONParserUtil.getString(json, "email"));
									dto.setContactNo(JSONParserUtil.getString(json, "contactNo"));
									dto.setTransactionRefNo(JSONParserUtil.getString(json, "transactionRefNo"));
									dto.setCurrentBalance(JSONParserUtil.getString(json, "currentBalance"));
									dto.setDateOfTransaction(JSONParserUtil.getString(json, "dateOfTransaction"));
									dto.setDebit(JSONParserUtil.getString(json, "debit"));
									dto.setAmount(JSONParserUtil.getString(json, "amount"));
									dto.setStatus(JSONParserUtil.getString(json, "status"));
									dto.setDescription(JSONParserUtil.getString(json, "description"));
									transactions.add(dto);
								}
								resp.setAmount(jobj.getLong("count"));
								resp.setList(transactions);
							} else {
								resp.setSuccess(false);
								resp.setMessage("Transaction List Empty");
							}
						} else {
							resp.setSuccess(false);
							resp.setMessage("Please try Later");
						}
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					// resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			// resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	public RegistrationResponse changePassWithMobNo(ChangePasswordRequest request) {

		RegistrationResponse resp = new RegistrationResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", "");
			payload.put("username", request.getUsername());
			payload.put("newPassword", request.getNewPassword());
			payload.put("confirmPassword", request.getConfirmPassword());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getAgentPasswordUrl(Version.VERSION_1, Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						final Object details = (Object) jobj.get("details");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					} else {
						resp.setSuccess(false);
						resp.setCode("F00");
						resp.setMessage("Service unavailable");
						resp.setDetails(null);
						resp.setStatus("FAILED");
						resp.setResponse(APIUtils.getFailedJSON().toString());
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public AddOffersResponse addOffers(OffersRequest request) {
		AddOffersResponse response = new AddOffersResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("offers", request.getOffers());
			payload.put("activeOffer", request.getActiveOffer());
			payload.put("services", request.getServices());
			Client client = Client.create();
			WebResource webResource = client
					.resource(UrlMetadatas.addOffers(Version.VERSION_1, Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse resp = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (resp.getStatus() != 200) {
				response.setSuccess(false);
				response.setCode(ResponseStatus.FAILURE.getValue());
				response.setMessage("Service unavailable");
				response.setStatus("FAILED");
				response.setDetails(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = resp.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
							response.setSuccess(true);
							final String details = (String) jobj.get("details");
							response.setDetails(details);
						} else {
							response.setSuccess(false);
						}
						response.setCode(code);
						response.setStatus(status);
						response.setMessage(message);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setSuccess(false);
			response.setCode(ResponseStatus.FAILURE.getValue());
			response.setMessage("Service unavailable");
			response.setStatus("FAILED");
			response.setDetails(APIUtils.getFailedJSON().toString());
		}
		return response;
	}

	@Override
	public TransactionReportResponse userFilteredNeftList(AllUserRequest userRequest) {
		TransactionReportResponse resp = new TransactionReportResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", userRequest.getSessionId());
			payload.put("page", userRequest.getPage());
			payload.put("size", userRequest.getSize());
			payload.put("pageable", userRequest.isPageable());
			payload.put("fromDate", userRequest.getStartDate());
			payload.put("toDate", userRequest.getEndDate());
			Client client = Client.create();
			WebResource webResource = client
					.resource(UrlMetadatas.getUpiTxn(Version.VERSION_1, Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				// resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (code.equalsIgnoreCase("S00") && jobj.getString("details") != null
								&& jobj.getString("details").contains("[")) {
							List<NEFTResponse> transactions = new ArrayList<>();
							resp.setSuccess(true);
							org.json.JSONArray details = JSONParserUtil.getArray(jobj, "details");
							if (details != null && details.length() > 0) {
								for (int i = 0; i < details.length(); i++) {
									org.json.JSONObject json = details.getJSONObject(i);
									NEFTResponse neft = new NEFTResponse();
									neft.setName(JSONParserUtil.getString(json, "name"));
									neft.setMobileNo(JSONParserUtil.getString(json, "mobileNumber"));
									neft.setEmail(JSONParserUtil.getString(json, "email"));
									neft.setTransactionDate(JSONParserUtil.getString(json, "transactionDate"));
									neft.setTransactionID(JSONParserUtil.getString(json, "transactionID"));
									neft.setBankName(JSONParserUtil.getString(json, "bankName"));
									neft.setIfscCode(JSONParserUtil.getString(json, "ifscCode"));
									neft.setAccountNumber(JSONParserUtil.getString(json, "beneficiaryAccountNumber"));
									neft.setAccountName(JSONParserUtil.getString(json, "beneficiaryAccountName"));
									neft.setUserAccount(JSONParserUtil.getString(json, "virtualAccount"));
									neft.setBankAccount(JSONParserUtil.getString(json, "bankVirtualAccount"));
									neft.setAmount(JSONParserUtil.getString(json, "amount"));
									neft.setStatus(JSONParserUtil.getString(json, "status"));
									transactions.add(neft);
								}
								resp.setAmount(jobj.getLong("count"));
								resp.setNeftList(transactions);
							} else {
								resp.setSuccess(false);
								resp.setMessage("Transaction List Empty");
							}
						} else {
							resp.setSuccess(false);
							resp.setMessage("Please try Later");
						}
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					// resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			// resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public ResponseDTO predictAndWinPayment(RequestRefund upload) {
		ResponseDTO resp = new ResponseDTO();
		try {
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getPredictAndWinUploadUrl(Version.VERSION_1,
					Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(upload.toString()))
					.post(ClientResponse.class, upload.getJsonRequest());
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						resp.setStatus(jobj.getString("status"));
						resp.setCode(jobj.getString("code"));
						resp.setMessage(jobj.getString("message"));
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					// resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			// resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public TransactionReportResponse getGcmList(AllUserRequest userRequest) {
		TransactionReportResponse resp = new TransactionReportResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("page", userRequest.getPage());
			payload.put("size", userRequest.getSize());
			payload.put("pageable", userRequest.isPageable());
			Client client = Client.create();
			WebResource webResource = client
					.resource(UrlMetadatas.getGcmList(Version.VERSION_1, Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode(ResponseStatus.FAILURE.getValue());
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				// resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if (code.equalsIgnoreCase("S00") && jobj.getString("details") != null
								&& jobj.getString("details").contains("[")) {
							List<NotificationDTO> notificationDTOs = new ArrayList<>();
							resp.setSuccess(true);
							org.json.JSONArray details = JSONParserUtil.getArray(jobj, "details");
							if (details != null && details.length() > 0) {
								for (int i = 0; i < details.length(); i++) {
									org.json.JSONObject json = details.getJSONObject(i);
									NotificationDTO dto = new NotificationDTO();
									dto.setMessage(JSONParserUtil.getString(json, "message"));
									dto.setTitle(JSONParserUtil.getString(json, "title"));
									dto.setDeliveredCount(JSONParserUtil.getLong(json, "deliveredCount"));
									dto.setStatus(JSONParserUtil.getString(json, "status"));
									dto.setImage(JSONParserUtil.getString(json, "image"));
									dto.setCreatedDate(JSONParserUtil.getString(json, "createdDate"));
									notificationDTOs.add(dto);
								}
								resp.setAmount(jobj.getLong("count")); 
								resp.setGcmNotification(notificationDTOs);
							} else {
								resp.setSuccess(false);
								resp.setMessage("Transaction List Empty");
							}
						} else {
							resp.setSuccess(false);
							resp.setMessage("Please try Later");
						}
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					// resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			// resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public PromoCodeRequest getPromoCode(String pId,String sessionId) {
		PromoCodeRequest resp= new PromoCodeRequest();
		try {
			JSONObject payload = new JSONObject();
			payload.put("promoCodeId", pId);
			payload.put("sessionId", sessionId);
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client
					.resource(UrlMetadatas.getPromoCode(Version.VERSION_1, Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			logger.info("promocode response:....."+strResponse);
			if (strResponse != null && !strResponse.isEmpty()) {
				JSONObject jsonObj = new JSONObject(strResponse);
				if (jsonObj != null) {
					String code = jsonObj.getString("code");
					if (code.equalsIgnoreCase("S00")) {
						JSONObject jobj= jsonObj.getJSONObject("details");
						resp.setPromoCodeId(jobj.getString("promoCodeId"));
						resp.setPromoCode(jobj.getString("promoCode"));
						resp.setTerms(jobj.getString("terms"));
						resp.setStartDate(jobj.getString("startDate"));
						resp.setEndDate(jobj.getString("endDate"));
						resp.setDescription(jobj.getString("description"));
						resp.setFixed(jobj.getBoolean("fixed"));
						resp.setValue(jobj.getDouble("value"));
						resp.setCashBackValue(jobj.getString("cashBackValue"));
						resp.setServiceTypeId(jobj.getLong("serviceTypeId"));
						try {
							JSONArray array = jobj.getJSONArray("serviceList");
							if (array != null && array.length() > 0) {
								List<CommissionDTO> serviceList= getServiceIds(array);
								resp.setServiceList(serviceList);
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return resp;
	}

	private List<CommissionDTO> getServiceIds(JSONArray array) {
		List<CommissionDTO> list = new ArrayList<>();
		for (int i = 0; i < array.length(); i++) {
			try {
				JSONObject obj = array.getJSONObject(i);
				CommissionDTO dto = new CommissionDTO();
				dto.setCode(obj.getString("code"));
				dto.setServiceName(obj.getString("serviceName"));
				list.add(dto);
			} catch (org.codehaus.jettison.json.JSONException e) {
				e.printStackTrace();
			}
		}
		return list;
	}
	
	
	@Override
	public AgentRegistrationRequest getAgentDetails(String aId,String sessionId) {
		AgentRegistrationRequest resp= new AgentRegistrationRequest();
		try {
			JSONObject payload = new JSONObject();
			payload.put("agentId", aId);
			payload.put("sessionId", sessionId);
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client
					.resource(UrlMetadatas.getAgentDetails(Version.VERSION_1, Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (strResponse != null && !strResponse.isEmpty()) {
				JSONObject jsonObj = new JSONObject(strResponse);
				if (jsonObj != null) {
					String code = jsonObj.getString("code");
					if (code.equalsIgnoreCase("S00")) {
						JSONObject jobj= jsonObj.getJSONObject("details");
					/*	resp.setPromoCodeId(jobj.getString("promoCodeId"));
						resp.setPromoCode(jobj.getString("promoCode"));
						resp.setTerms(jobj.getString("terms"));
						resp.setStartDate(jobj.getString("startDate"));
						resp.setEndDate(jobj.getString("endDate"));
						resp.setDescription(jobj.getString("description"));
						resp.setFixed(jobj.getBoolean("fixed"));
						resp.setValue(jobj.getDouble("value"));
						resp.setCashBackValue(jobj.getString("cashBackValue"));
						resp.setServiceTypeId(jobj.getLong("serviceTypeId"));*/
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return resp;
	}
	@Override
	public AddVoucherResponse addVouchers(AddVoucherReq request) {
		
		AddVoucherResponse resp = new AddVoucherResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("voucherQty", request.getVoucherQty());
			payload.put("voucherAmount", request.getVoucherAmount());
			payload.put("expiryDate", request.getExpiryDate());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.addVouchers(Version.VERSION_1, Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
							final int totalVoucher=jobj.getInt("details");
							resp.setResponse(totalVoucher+"");
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						
//						resp.setDetails(totalVoucher.);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}
	
	@Override
	public AllUserResponse getAllVoucher(AllUserRequest request) {
		AllUserResponse resp = new AllUserResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("page", request.getPage());
			payload.put("size", request.getSize());
			payload.put("userStatus", request.getStatus().getValue());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getAllVouchersUrl(Version.VERSION_1, Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						final org.json.JSONObject details = jobj.getJSONObject("details");
						final org.json.JSONArray totalUser = (org.json.JSONArray) jobj.getJSONObject("details")
								.getJSONArray("content");
						long numberOfElements = details.getLong("numberOfElements");
						boolean firstPage = details.getBoolean("firstPage");
						boolean lastPage = details.getBoolean("lastPage");
						long size = details.getLong("size");
						long totalPages = details.getLong("totalPages");
						resp.setTotalPages(totalPages);
						resp.setSize(size);
						resp.setNumberOfElements(numberOfElements);
						resp.setFirstPage(firstPage);
						resp.setLastPage(lastPage);
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
						resp.setTotalUser(totalUser.length());
						resp.setJsonArray(totalUser);
					} 
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}
	
	@Override
	public AllUserResponse getfilteredVoucherList(DateDTO request) {
		
		AllUserResponse resp = new AllUserResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("fromDate", request.getFromDate());
			payload.put("toDate", request.getToDate());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getAllVouchersByDate(Version.VERSION_1, Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				System.err.println("response ::" + strResponse);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						final org.json.JSONArray details = jobj.getJSONArray("details");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
						resp.setJsonArray(details);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}
}
