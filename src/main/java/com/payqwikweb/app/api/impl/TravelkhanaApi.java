package com.payqwikweb.app.api.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.json.JSONArray;

import com.payqwikweb.api.ITravelkhanaApi;
import com.payqwikweb.app.metadatas.UrlMetadatas;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Language;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.Version;
import com.payqwikweb.app.model.request.SessionDTO;
import com.payqwikweb.app.utils.SecurityUtils;
import com.payqwikweb.model.app.request.TKApplyCouponRequest;
import com.payqwikweb.model.app.request.TKMenuPriceCalRequest;
import com.payqwikweb.model.app.request.TKPlaceOrderRequest;
import com.payqwikweb.model.app.request.TKTrainslistRequest;
import com.payqwikweb.model.app.request.UserOrderInfoDTO;
import com.payqwikweb.model.app.request.UserOrderMenuDTO;
import com.payqwikweb.model.app.response.MenuDTO;
import com.payqwikweb.model.app.response.MenuPriceCalDTO;
import com.payqwikweb.model.app.response.OutletTypeDTO;
import com.payqwikweb.model.app.response.StationDTO;
import com.payqwikweb.model.app.response.StationsDTO;
import com.payqwikweb.model.app.response.TKCartDetailsDTO;
import com.payqwikweb.model.app.response.TKGetMenuListDTO;
import com.payqwikweb.model.app.response.TKGetOutletListDTO;
import com.payqwikweb.model.app.response.TKGetTrainRouteDTO;
import com.payqwikweb.model.app.response.TKMenuListDTO;
import com.payqwikweb.model.app.response.TKMenuPriceCalDTO;
import com.payqwikweb.model.app.response.TKMyOrderDetailsDTO;
import com.payqwikweb.model.app.response.TKOrderDTO;
import com.payqwikweb.model.app.response.TKOrderDetailsDTO;
import com.payqwikweb.model.app.response.TKOrderSuccessDTO;
import com.payqwikweb.model.app.response.TKOutletListDTO;
import com.payqwikweb.model.app.response.TKPassengerInfoDTO;
import com.payqwikweb.model.app.response.TKPlaceOrderInitiateResp;
import com.payqwikweb.model.app.response.TKTrackUserOrderDTO;
import com.payqwikweb.model.app.response.TKTrainRouteDTO;
import com.payqwikweb.model.app.response.TKTrainslistDTO;
import com.payqwikweb.model.app.response.TKUserOrderDTO;
import com.payqwikweb.model.app.response.TkApplyCouponDTO;
import com.payqwikweb.model.app.response.TkTrackMenuDTO;
import com.payqwikweb.model.app.response.TrainDTO;
import com.payqwikweb.model.web.Status;
import com.payqwikweb.util.APIUtils;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.travelkhana.util.TravelkhanaUtil;

public class TravelkhanaApi implements ITravelkhanaApi{
	private SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
	@Override
	public TKTrainslistDTO getTrainslist(TKTrainslistRequest request){
		TKTrainslistDTO resp = new TKTrainslistDTO();
		JSONObject payload = new JSONObject();
		try {
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
//			client.addFilter(new HTTPBasicAuthFilter(TravelkhanaUtil.Travelkhana_Username,TravelkhanaUtil.Travelkhana_Password));
//			WebResource res=client.resource("https://stage.travelkhana.com/api/static/trainsList");
			WebResource res=client.resource(TravelkhanaUtil.trainList);
			ClientResponse response=res.header(TravelkhanaUtil.USER, TravelkhanaUtil.VALUE).get(ClientResponse.class);
	//		WebResource res=client.resource(UrlMetadatas.TRAVELKHANA_URL);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus(Status.Failed);
			} else {
				if (strResponse != null) {
					ArrayList<TrainDTO>list = new ArrayList<TrainDTO>();
					JSONArray  jsonArray = new JSONArray(strResponse);
						  if (jsonArray != null) {
							       for (int i = 0; i < jsonArray.length(); i++) {
							    	   System.out.println(jsonArray.get(i));
							    	   String trainsList[]=jsonArray.get(i).toString().split("/");
							    	   TrainDTO trainlist = new TrainDTO();
							    	    if(trainsList[0].contains("-Slip") || trainsList[0].contains("-Slip2")){
							    	    	String trains[]=trainsList[0].split("-");
							    	    	trainsList[0]=trains[0];
							    	     }
							    	    if(!trainsList[0].equalsIgnoreCase("225XX")){
							    	   trainlist.setTrainNumber(trainsList[0]);
							    	   trainlist.setTrainName(trainsList[1]);
							    	   list.add(trainlist);
							     }
							}
					  }
						resp.setSuccess(true);
						resp.setListOfTrain(list);
						   
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus(Status.Failed);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}
	
	@Override
	public TKTrainslistDTO getTrainslistFromDB(TKTrainslistRequest request) throws JSONException {
		TKTrainslistDTO resp = new TKTrainslistDTO();
		JSONObject payload = new JSONObject();
		payload.put("sessionId",request.getSessionId());
		
		try {
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.trainListFromDB(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.header("hash", SecurityUtils.getHash(payload.toString())).type("application/json")
					.post(ClientResponse.class,payload.toString());
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus(Status.Failed);
			} else {
				if (strResponse != null) {
					ArrayList<TrainDTO>list = new ArrayList<TrainDTO>();
					System.out.println("strResponse=="+strResponse);
					JSONObject  jsonObj = new JSONObject(strResponse);
						  if (jsonObj != null) {
							  
							  org.codehaus.jettison.json.JSONArray jArr = jsonObj.getJSONArray("details");
							       for (int i = 0; i < jArr.length(); i++) {
							    	   JSONObject jobj = jArr.getJSONObject(i);
							    	   TrainDTO trainlist = new TrainDTO();
							    	   String trainNumber = jobj.getString("trainNumber");
							    	   String trainName = jobj.getString("trainName");
							    	    if(trainNumber.contains("-Slip") || trainNumber.contains("-Slip2")){
							    	    	String trains[]=trainNumber.split("-");
							    	    	trainNumber=trains[0];
							    	     }
							    	    if(!trainNumber.equalsIgnoreCase("225XX")){
							    	        trainlist.setTrainNumber(trainNumber);
							    	        trainlist.setTrainName(trainName);
							    	        list.add(trainlist);
							    	    }
							   }
					   }
						resp.setSuccess(true);
						resp.setListOfTrain(list);
						   
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus(Status.Failed);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}

	@Override
	public TKTrainslistDTO getStationlist(TKTrainslistRequest stationlistreq){
	   
		TKTrainslistDTO resp = new TKTrainslistDTO();
		JSONObject payload = new JSONObject();
		try {
			
			payload.put("trainNumber",stationlistreq.getTrainNumber());
			
			System.err.println("payload for stationlist" + payload.toString() + " trainNumber ++++++++" +stationlistreq.getTrainNumber());
			Client client = Client.create();
			   client.addFilter(new LoggingFilter(System.out));
			   WebResource res=client.resource(TravelkhanaUtil.stationList+stationlistreq.getTrainNumber());
			   ClientResponse response=res.header(TravelkhanaUtil.USER, TravelkhanaUtil.VALUE).get(ClientResponse.class);
			   String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus(Status.Failed);
			} else {
				if (strResponse != null) {
				  ArrayList<StationDTO>list = new ArrayList<StationDTO>();
				  JSONObject jsonObject1 = new JSONObject(strResponse);
					   if (jsonObject1 != null) {
						   Iterator<?> keys = jsonObject1.keys();
						   String key = null;
						   while( keys.hasNext()){
							    key = (String)keys.next();
							 JSONObject jsonObject2 = new JSONObject(jsonObject1.get(key).toString());
					    	 String station = jsonObject2.getString("station");
					    	 StationDTO trainlist = new StationDTO();
					    	 trainlist.setStation(station);
					         list.add(trainlist);
						  }
					   }	
				    	resp.setStations(list);
				    	resp.setSuccess(true);
					}
				   else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus(Status.Failed);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
    }

	@Override
	public TKMenuListDTO getMenulist(TKTrainslistRequest dto) {
		

		TKMenuListDTO resp = new TKMenuListDTO();
		JSONObject payload = new JSONObject();
		try {

			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			client.addFilter(new HTTPBasicAuthFilter(TravelkhanaUtil.Travelkhana_Username,TravelkhanaUtil.Travelkhana_Password));
//			WebResource res=client.resource("https://stage.travelkhana.com/api/orderBooking/menuInTime/"+dto.getOutletId()+"?"+"arrivalTime="+dto.getArrivalTime());
			WebResource res=client.resource(TravelkhanaUtil.menuInTime+dto.getOutletId()+"?"+"arrivalTime="+dto.getArrivalTime());
			ClientResponse response=res.header(TravelkhanaUtil.USER, TravelkhanaUtil.VALUE)
				.type("application/json").get(ClientResponse.class);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus(Status.Failed);
			} else {
				if (strResponse != null) {
					
				ArrayList<TKGetMenuListDTO>listOfMenuobject=new ArrayList<TKGetMenuListDTO>();
					JSONArray  jsonArray = new JSONArray(strResponse);
						  if (jsonArray != null) {
							       for (int i = 0; i < jsonArray.length(); i++) {
							    	   
							    	   TKGetMenuListDTO getMenuList = new TKGetMenuListDTO();
							    	   long categoryId = jsonArray.getJSONObject(i).getLong("categoryId");
							    	   String categoryName = jsonArray.getJSONObject(i).getString("categoryName");
							    	   getMenuList.setCategoryId(categoryId);
							    	   getMenuList.setCategoryName(categoryName);
							    	   
							    	   ArrayList<MenuDTO>listOfMenuDTOobject=new ArrayList<MenuDTO>();
							    	   JSONArray jsonArray2 = new JSONArray(jsonArray.getJSONObject(i).getString("menu"));
							      if (jsonArray2 != null) {
									for (int j = 0; j < jsonArray2.length(); j++) {
									    	   
							    	   double serviceTax = jsonArray2.getJSONObject(j).getDouble("serviceTax");
							    	   String itemName = jsonArray2.getJSONObject(j).getString("itemName");
							    	   int webDisplay = jsonArray2.getJSONObject(j).getInt("webDisplay");
							    	   double vat = jsonArray2.getJSONObject(j).getDouble("vat");
							    	   String image = jsonArray2.getJSONObject(j).getString("image");
							    	   double customerPayable = jsonArray2.getJSONObject(j).getDouble("customerPayable");
							    	   long itemId = jsonArray2.getJSONObject(j).getLong("itemId");
							    	   boolean isActive = jsonArray2.getJSONObject(j).getBoolean("isActive");
							    	   String menuTag = jsonArray2.getJSONObject(j).getString("menuTag");
							    	   double price = jsonArray2.getJSONObject(j).getDouble("price");
							    	   String desc = jsonArray2.getJSONObject(j).getString("description");
							    	   String closingTime = jsonArray2.getJSONObject(j).getString("closingTime");
							    	   String openingTime = jsonArray2.getJSONObject(j).getString("openingTime");
							    	   double basePrice = jsonArray2.getJSONObject(j).getDouble("basePrice");
							   // 	   String additionalTags[] = jsonArray2.getJSONObject(j).getString("additionalTags");
							    	  
							    	   MenuDTO menudto = new MenuDTO();
							    	   menudto.setActive(isActive);
							    	   menudto.setBasePrice(basePrice);
							    	   menudto.setClosingTime(closingTime);
							    	   menudto.setOpeningTime(openingTime);
							    	   menudto.setCustomerPayable(customerPayable);
							    	   menudto.setDescription(desc);
							    	   menudto.setImage(image);
							    	   menudto.setItemId(Long.toString(itemId));
							    	   menudto.setItemName(itemName);
							    	   menudto.setPrice(price);
							    	   menudto.setServiceTax(serviceTax);
							    	   menudto.setVat(vat);
							    	   menudto.setWebDisplay(webDisplay);
							    	   menudto.setMenuTag(menuTag);
							    //	   menudto.setAdditionalTags(additionalTags);
							    	   listOfMenuDTOobject.add(menudto);
							    	   
									 }
							      }
							       getMenuList.setListOfMenuDTOobject(listOfMenuDTOobject);
							       listOfMenuobject.add(getMenuList); 
							 }
							         
					  }
				    	resp.setListOfMenuobject(listOfMenuobject);
				    	resp.setSuccess(true);
				 }
				   else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus(Status.Failed);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}
	
	@Override
	public TKTrainRouteDTO getTrainRoutesMenulist(TKTrainslistRequest dto) {
		TKTrainRouteDTO resp = new TKTrainRouteDTO();
		JSONObject payload = new JSONObject();
		try {
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			client.addFilter(new HTTPBasicAuthFilter(TravelkhanaUtil.Travelkhana_Username,TravelkhanaUtil.Travelkhana_Password));
//			WebResource res=client.resource("https://stage.travelkhana.com/api/orderBooking/trainRoutesNew?station="+dto.getStation()+"&date="+dto.getDate()+"&trainNo="+dto.getTrainNumber());
			WebResource res=client.resource(TravelkhanaUtil.trainRoutesMenu+dto.getStation()+"&date="+dto.getDate()+"&trainNo="+dto.getTrainNumber());
			ClientResponse clientResp=res.header(TravelkhanaUtil.USER, TravelkhanaUtil.VALUE)
				.type("application/json").get(ClientResponse.class);
			String strResponse = clientResp.getEntity(String.class);
		    JSONObject jsonObject = new JSONObject(strResponse);
		  if(clientResp.getStatus()!=406){
			if (clientResp.getStatus()!= 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus(Status.Failed);
			} else {
				if (strResponse != null) {
					String status =jsonObject.getString("status");
		          if(!status.equalsIgnoreCase("false")){	
		        
					ArrayList<TKTrainRouteDTO>trainRouteObject=new ArrayList<TKTrainRouteDTO>();
					ArrayList<StationsDTO>stationsobject=new ArrayList<StationsDTO>();
					ArrayList<TKGetMenuListDTO>menuobject=new ArrayList<TKGetMenuListDTO>();
					ArrayList<TKGetTrainRouteDTO>MenuStationObject=new ArrayList<TKGetTrainRouteDTO>();
				
					  TKGetTrainRouteDTO trainRouteStationsMenu = new TKGetTrainRouteDTO();
					  if (jsonObject != null) {	  
						 JSONObject jsonObject2 = jsonObject.getJSONObject("data");
					       if (jsonObject2 != null) {  	
					    	 boolean availability = jsonObject2.getBoolean("availability");
					    	  if(availability){					    	  
					    	    long trainNumber = jsonObject2.getInt("train");
							    String trainName = jsonObject2.getString("trainName");
							    String date = jsonObject2.getString("date");
							    trainRouteStationsMenu.setTrainNumber(trainNumber);
							    trainRouteStationsMenu.setTrainName(trainName);
							    trainRouteStationsMenu.setDate(date);
							    trainRouteStationsMenu.setAvailability(availability);
				
					    	  org.codehaus.jettison.json.JSONArray jsonArray3 = jsonObject2.getJSONArray("stations");
					    	  if (jsonArray3 != null) {
					    		  for (int i = 0; i < jsonArray3.length(); i++) {
					    			 
					    			  int arrivalDayIncrement =  jsonArray3.getJSONObject(i).getInt("arrivalDayIncrement");
					    			  String stationCode = jsonArray3.getJSONObject(i).getString("stationCode");
					    			  String arrivalTime = jsonArray3.getJSONObject(i).getString("arrivalTime");
					    			  int seqNo = jsonArray3.getJSONObject(i).getInt("seqNo"); 
					    			  String dateAtStation = jsonArray3.getJSONObject(i).getString("dateAtStation");
					    			  String departureTime = jsonArray3.getJSONObject(i).getString("departureTime");	  
					    			  String delay = jsonArray3.getJSONObject(i).getString("delay");
					    			  boolean foodAvailablity = jsonArray3.getJSONObject(i).getBoolean("foodAvailablity");
					    			  String halt = jsonArray3.getJSONObject(i).getString("foodAvailablity");
					    			  String stationName = jsonArray3.getJSONObject(i).getString("stationName");
					    			  
					    			  StationsDTO stationDto = new StationsDTO();
					    			  stationDto.setArrivalDayIncrement(arrivalDayIncrement);
					    			  stationDto.setArrivalTime(arrivalTime);
					    			  stationDto.setDateAtStation(dateAtStation);
					    			  stationDto.setDelay(delay);
					    			  stationDto.setDepartureTime(departureTime);
					    			  stationDto.setFoodAvailablity(foodAvailablity);
					    			  stationDto.setHalt(halt);		  
					    			  stationDto.setSeqNo(seqNo);
					    			  stationDto.setStationCode(stationCode);
					    			  stationDto.setStationName(stationName);
					    			  
					    			  ArrayList<OutletTypeDTO>foodTypeDto = new ArrayList<OutletTypeDTO>();
					    	/*		  org.codehaus.jettison.json.JSONArray jsonArray4 = jsonArray3.getJSONObject(i).getJSONArray("foodType");
					    			  	if (jsonArray4 != null) {
					    			  		for (int j = 0; j < jsonArray4.length(); j++) {
					    			  				
					    			  				String tagType = jsonArray4.getJSONObject(j).getString("tagType");
					    			  				String tagSubType = jsonArray4.getJSONObject(j).getString("tagSubType");
					    			  				String tagName = jsonArray4.getJSONObject(j).getString("tagName");
					    			  				long tagId = jsonArray4.getJSONObject(j).getLong("tagId"); 
					    			  				
					    			  				OutletTypeDTO outletTypeDto = new OutletTypeDTO();
					    			  				outletTypeDto.setTagId(tagId);
					    			  				outletTypeDto.setTagName(tagName);
					    			  				outletTypeDto.setTagSubType(tagSubType);
					    			  				outletTypeDto.setTagType(tagType);
					    			  				
					    			  				foodTypeDto.add(outletTypeDto);
					    			  	     }
					    			  	}*/
					    		//	  	stationDto.setOutletTypeDTO(foodTypeDto);
					    			  	stationsobject.add(stationDto);
					    		   }
					    	  }
					    	  long outletId=0;
					    	  org.codehaus.jettison.json.JSONArray jsonArray5 = jsonObject2.getJSONArray("vendor");
					    	  if (jsonArray5 != null) {
					    		  for (int i = 0; i < jsonArray5.length(); i++) {
					    			   if(jsonArray5.getJSONObject(i).getBoolean("showMenu")){
					    				    outletId = jsonArray5.getJSONObject(i).getLong("outletId");
					    				    break;
					    			   }
					    		   }
					    	  }
					    	  trainRouteStationsMenu.setOutletId(outletId);
					    	  
					    	  org.codehaus.jettison.json.JSONArray jsonArray = jsonObject2.getJSONArray("categoriesMenu");
					    	  if (jsonArray != null) {
					    		  for (int i = 0; i < jsonArray.length(); i++) {
					    			  
					    			   TKGetMenuListDTO getMenuList = new TKGetMenuListDTO();
					    			   long categoryId = jsonArray.getJSONObject(i).getLong("categoryId");
							    	   String categoryName = jsonArray.getJSONObject(i).getString("categoryName");
							    	   getMenuList.setCategoryId(categoryId);
							    	   getMenuList.setCategoryName(categoryName);
							    	   
							    	   ArrayList<MenuDTO>listOfMenuDTOobject=new ArrayList<MenuDTO>();
							    	   JSONArray jsonArray2 = new JSONArray(jsonArray.getJSONObject(i).getString("menu"));
							        if (jsonArray2 != null) {
									  for (int j = 0; j < jsonArray2.length(); j++) {
									    	   
							    	   double serviceTax = jsonArray2.getJSONObject(j).getDouble("serviceTax");
							    	   String itemName = jsonArray2.getJSONObject(j).getString("itemName");
							    	   int webDisplay = jsonArray2.getJSONObject(j).getInt("webDisplay");
							    	   double vat = jsonArray2.getJSONObject(j).getDouble("vat");
							    	   String image = jsonArray2.getJSONObject(j).getString("image");
							    	   double customerPayable = jsonArray2.getJSONObject(j).getDouble("customerPayable");
							    	   long itemId = jsonArray2.getJSONObject(j).getLong("itemId");
							    	   boolean isActive = jsonArray2.getJSONObject(j).getBoolean("isActive");
							    	   String menuTag = jsonArray2.getJSONObject(j).getString("menuTag");
							    	   double price = jsonArray2.getJSONObject(j).getDouble("price");
							    	   String desc = jsonArray2.getJSONObject(j).getString("description");
							    	   String closingTime = jsonArray2.getJSONObject(j).getString("closingTime");
							    	   String openingTime = jsonArray2.getJSONObject(j).getString("openingTime");
							    	   double basePrice = jsonArray2.getJSONObject(j).getDouble("basePrice");
							   // 	   String additionalTags[] = jsonArray2.getJSONObject(j).getString("additionalTags");
							    	   
							    	   MenuDTO menudto = new MenuDTO();
							    	   menudto.setActive(isActive);
							    	   menudto.setBasePrice(basePrice);
							    	   menudto.setClosingTime(closingTime);
							    	   menudto.setOpeningTime(openingTime);
							    	   menudto.setCustomerPayable(customerPayable);
							    	   menudto.setDescription(desc);
							    	   menudto.setImage(image);
							    	   menudto.setItemId(Long.toString(itemId));
							    	   menudto.setItemName(itemName);
							    	   menudto.setPrice(price);
							    	   menudto.setServiceTax(serviceTax);
							    	   menudto.setVat(vat);
							    	   menudto.setWebDisplay(webDisplay);
							    	   menudto.setMenuTag(menuTag);
							    //	   menudto.setAdditionalTags(additionalTags);
							    	   listOfMenuDTOobject.add(menudto);
							    	   
									  }
							         } 
							           getMenuList.setListOfMenuDTOobject(listOfMenuDTOobject);
							           menuobject.add(getMenuList); 
					    		    }
					    	     }
					    	       trainRouteStationsMenu.setCategoriesMenu(menuobject);
					    	       trainRouteStationsMenu.setStationsDTO(stationsobject);
					    	       resp.setTrainRoutesListResponse(trainRouteStationsMenu);
							        resp.setSuccess(true);
							        resp.setCode("S00");
					             }
					       else{
					    	   String message = jsonObject.getString("message");
					    	   resp.setSuccess(false);
							   resp.setMessage(message);
							   System.out.println(message);
							   resp.setCode("N00");
					         }
					     }
					       else{
					    	   String message = jsonObject.getString("message");
					    	   resp.setSuccess(false);
							   resp.setMessage(message);
							   resp.setCode("F00");
							   System.out.println(message);
					       }
					  }
					  else{
					  resp.setSuccess(false);
						resp.setCode("F00");
						resp.setMessage("Wrong Response from Travelkhana");
						resp.setStatus(Status.Failed);
					}
					 
				 }
				  else{
			    	   String message = jsonObject.getString("message");
			    	   resp.setSuccess(false);
					   resp.setMessage(message);
					   resp.setCode("F00");
					   System.out.println(message);
			        }
				}
				   else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus(Status.Failed);
				}
			}
			 
		  }else{
			  String message = jsonObject.getString("Message");
			  resp.setSuccess(false);
			  resp.setMessage(message);
			  resp.setCode("F00");
		  }
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}

	@Override
	public TKOutletListDTO getOuletMenulist(TKTrainslistRequest dto) {
	

		TKOutletListDTO resp = new TKOutletListDTO();
		JSONObject payload = new JSONObject();
		try {

			System.err.println("payload for menulist" + payload.toString() + "Station--" + dto.getStation() + "arrivalTime" + dto.getArrivalTime()+"Date"+dto.getDate() );
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			client.addFilter(new HTTPBasicAuthFilter(TravelkhanaUtil.Travelkhana_Username,TravelkhanaUtil.Travelkhana_Password));
//			WebResource res=client.resource("https://stage.travelkhana.com/api/orderBooking/outletInTime?arrivalTime="+dto.getArrivalTime()+"&station="+dto.getStation()+"&date="+dto.getDate());
			WebResource res=client.resource(TravelkhanaUtil.outletInTime+dto.getArrivalTime()+"&station="+dto.getStation()+"&date="+dto.getDate());
			ClientResponse response=res.header(TravelkhanaUtil.USER, TravelkhanaUtil.VALUE)
				.type("application/json").get(ClientResponse.class);
			String strResponse = response.getEntity(String.class);
			System.err.println("Menu list API RESPONSE : : " + strResponse);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus(Status.Failed);
			} else {
				if (strResponse != null) {
					
		//		ArrayList<TravelkhanaVendorDTO>listOfvendorobject=new ArrayList<TravelkhanaVendorDTO>();
				ArrayList<TKGetMenuListDTO>Menuobject=new ArrayList<TKGetMenuListDTO>();
				TKGetOutletListDTO outletListResponse = new TKGetOutletListDTO();
			//	ArrayList<TKOutletListResponse>MenuAndVendor=new ArrayList<TKOutletListResponse>();
				JSONObject jsonObject = new JSONObject(strResponse);
				boolean status = jsonObject.getBoolean("status");
				String message = jsonObject.getString("message");
				if(status){
				    if (jsonObject != null) {	  
					  JSONObject jsonObject2 = jsonObject.getJSONObject("data");
				      if (jsonObject2 != null) {
			//	        for (int i = 0; i < jArray.length(); i++) {	  Not parsing vendor info 
				    	  long outletId=0;
				    	  org.codehaus.jettison.json.JSONArray jsonArray5 = jsonObject2.getJSONArray("vendor");
				    	  if (jsonArray5 != null) {
				    		  for (int i = 0; i < jsonArray5.length(); i++) {
				    			   if(jsonArray5.getJSONObject(i).getBoolean("showMenu")){
				    				    outletId = jsonArray5.getJSONObject(i).getLong("outletId");
				    				    break;
				    			   }
				    		   }
				    	  }
				    	  outletListResponse.setOutletId(outletId);
				    	  org.codehaus.jettison.json.JSONArray jsonArray = jsonObject2.getJSONArray("categoriesMenu");
				    	  if (jsonArray != null) {
				    		  for (int i = 0; i < jsonArray.length(); i++) {
				    			   
				    			   TKGetMenuListDTO getMenuList = new TKGetMenuListDTO();
						    	   Long categoryId = jsonArray.getJSONObject(i).getLong("categoryId");
						    	   String categoryName = jsonArray.getJSONObject(i).getString("categoryName");
						    	   getMenuList.setCategoryId(categoryId);
						    	   getMenuList.setCategoryName(categoryName);
						    	   
						    	   ArrayList<MenuDTO>listOfMenuDTOobject=new ArrayList<MenuDTO>();
						    	   JSONArray jsonArray2 = new JSONArray(jsonArray.getJSONObject(i).getString("menu"));
						        if (jsonArray2 != null) {
								  for (int j = 0; j < jsonArray2.length(); j++) {
								    	   
						    	   double serviceTax = jsonArray2.getJSONObject(j).getDouble("serviceTax");
						    	   String itemName = jsonArray2.getJSONObject(j).getString("itemName");
						    	   int webDisplay = jsonArray2.getJSONObject(j).getInt("webDisplay");
						    	   double vat = jsonArray2.getJSONObject(j).getDouble("vat");
						    	   String image = jsonArray2.getJSONObject(j).getString("image");
						    	   double customerPayable = jsonArray2.getJSONObject(j).getDouble("customerPayable");
						    	   long itemId = jsonArray2.getJSONObject(j).getLong("itemId");
						    	   boolean isActive = jsonArray2.getJSONObject(j).getBoolean("isActive");
						    	   String menuTag = jsonArray2.getJSONObject(j).getString("menuTag");
						    	   double price = jsonArray2.getJSONObject(j).getDouble("price");
						    	   String desc = jsonArray2.getJSONObject(j).getString("description");
						    	   String closingTime = jsonArray2.getJSONObject(j).getString("closingTime");
						    	   String openingTime = jsonArray2.getJSONObject(j).getString("openingTime");
						    	   double basePrice = jsonArray2.getJSONObject(j).getDouble("basePrice");
						   // 	   String additionalTags[] = jsonArray2.getJSONObject(j).getString("additionalTags");
						    	  
						    	   MenuDTO menudto = new MenuDTO();
						    	   menudto.setActive(isActive);
						    	   menudto.setBasePrice(basePrice);
						    	   menudto.setClosingTime(closingTime);
						    	   menudto.setOpeningTime(openingTime);
						    	   menudto.setCustomerPayable(customerPayable);
						    	   menudto.setDescription(desc);
						    	   menudto.setImage(image);
						    	   menudto.setItemId(Long.toString(itemId));
						    	   menudto.setItemName(itemName);
						    	   menudto.setPrice(price);
						    	   menudto.setServiceTax(serviceTax);
						    	   menudto.setVat(vat);
						    	   menudto.setWebDisplay(webDisplay);
						    	   menudto.setMenuTag(menuTag);
						    //	   menudto.setAdditionalTags(additionalTags);
						    	   listOfMenuDTOobject.add(menudto);
								  }
						         } 
						           getMenuList.setListOfMenuDTOobject(listOfMenuDTOobject);
						           Menuobject.add(getMenuList); 
				    		  }
				    	   }
				    	  outletListResponse.setCategoriesMenu(Menuobject);
				      	  }
				             resp.setListOfOuletobject(outletListResponse);
				             resp.setSuccess(true);
				        }
					}
				   else{
					   resp.setMessage(message);
					   resp.setSuccess(false);
				   }
				  		
				 }
				   else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus(Status.Failed);
				   } 
		        }
		    }
		    catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}
	
	@Override
	public TKMenuPriceCalDTO getMenuPriceCal(TKMenuPriceCalRequest dto){
		TKMenuPriceCalDTO resp = new TKMenuPriceCalDTO();
		try {
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			client.addFilter(new HTTPBasicAuthFilter(TravelkhanaUtil.Travelkhana_Username,TravelkhanaUtil.Travelkhana_Password));
	//		WebResource res=client.resource("https://stage.travelkhana.com/api/orderBooking/menuPriceCalculation");
			WebResource res=client.resource(TravelkhanaUtil.menuPriceCal);
			ClientResponse response=res.header(TravelkhanaUtil.USER, TravelkhanaUtil.VALUE)
					 				.type("application/json").post(ClientResponse.class,dto.getJsonRequest2());
	//		WebResource res=client.resource(UrlMetadatas.TRAVELKHANA_URL);
			String strResponse = response.getEntity(String.class);
			System.err.println("Trains list API RESPONSE : : " + strResponse);
			
			 if(response.getStatus()!=406){
			   if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus(Status.Failed);
			   } else {
				if (strResponse != null) {
					MenuPriceCalDTO menuPriceCal = new MenuPriceCalDTO();
					JSONObject jObj = new JSONObject(strResponse);
					  if(jObj!=null){
						  JSONObject jObj2 = jObj.getJSONObject("userOrderInfo");
						    if(jObj2!=null){
						        double totalCapped = jObj2.getDouble("totalCapped");
//						        double totalCustomerPayable = jObj2.getDouble("totalCustomerPayable");
						        double totalCustomerPayableCapped = jObj2.getDouble("totalCustomerPayableCapped");
						        double totalBasePrice = jObj2.getDouble("totalBasePrice");
//						        double taxes = jObj2.getDouble("taxes");
						        double discount = jObj2.getDouble("discount");
						        double delivery_cost = jObj2.getDouble("delivery_cost");
						        String station_code = jObj2.getString("station_code");
						        String voucher_code = jObj2.getString("voucher_code");
						        String order_outlet_id = jObj2.getString("order_outlet_id");
						        String train_no = jObj2.getString("train_no");
						        String date = jObj2.getString("date");
						        String cod = jObj2.getString("cod");
						     
						    	UserOrderInfoDTO userOrderInfo = new UserOrderInfoDTO();
						    	userOrderInfo.setCod(cod);
						    	userOrderInfo.setDate(date);
						    	userOrderInfo.setDelivery_cost(delivery_cost);
						    	userOrderInfo.setDiscount(discount);
						    	userOrderInfo.setOrder_outlet_id(order_outlet_id);
						    	userOrderInfo.setStation_code(station_code);
						    	userOrderInfo.setTotalBasePrice(totalBasePrice);
						    	userOrderInfo.setTotalCapped(totalCapped);
						    	userOrderInfo.setTotalCustomerPayableCapped(totalCustomerPayableCapped);
						    	userOrderInfo.setTotalCustomerPayable(totalCustomerPayableCapped);
						    	userOrderInfo.setTrain_no(train_no);
						    	userOrderInfo.setVoucher_code(voucher_code);
						    	
						    	menuPriceCal.setUserOrderInfo(userOrderInfo);
						   }
						    
						    org.codehaus.jettison.json.JSONArray jArray = jObj.getJSONArray("userOrderMenu");
						    ArrayList<UserOrderMenuDTO> userOrderMenuObj =new ArrayList<UserOrderMenuDTO>();
						    for(int i=0;i<jArray.length();i++){
						    	double itemBasePrice = jArray.getJSONObject(i).getDouble("itemBasePrice");
						    	long quantity = jArray.getJSONObject(i).getLong("quantity");
						    	String image = jArray.getJSONObject(i).getString("image");
						    	String item_name = jArray.getJSONObject(i).getString("item_name");
						    	long itemId = jArray.getJSONObject(i).getLong("itemId");
						    	UserOrderMenuDTO userOrderMenu = new UserOrderMenuDTO();
						    	userOrderMenu.setImage(image);
						    	userOrderMenu.setItem_name(item_name);
						    	userOrderMenu.setItemBasePrice(itemBasePrice);
						    	userOrderMenu.setItemId(Long.toString(itemId));
						    	userOrderMenu.setQuantity(quantity);
						    	
						    	userOrderMenuObj.add(userOrderMenu);
						    }
						     menuPriceCal.setUserOrderMenu(userOrderMenuObj);
						     double minAmountThreshold = jObj.getDouble("minAmountThreshold");
						     double outletminorderAmount = jObj.getDouble("outletminorderAmount");
						     menuPriceCal.setMinAmountThreshold(minAmountThreshold);
						     menuPriceCal.setOutletminorderAmount(outletminorderAmount);
					  }
					  resp.setSuccess(true);
					  resp.setMessage("Total Menu Price");
					 resp.setMenuPriceCalDTO(menuPriceCal);		
					 
				  } else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus(Status.Failed);
				}
			  }
		    }
			 else{
				  JSONObject jObj = new JSONObject(strResponse);	//Fail resp
				  String message = jObj.getString("Message");
				  System.out.println(message);
				  resp.setSuccess(false);
				  resp.setMessage(message);
			  }
		} catch (Exception e) {
			e.printStackTrace();
			
		}
		return resp;
	}
	
	@Override
	public TKTrackUserOrderDTO trackUserOrder(TKTrainslistRequest dto){
		TKTrackUserOrderDTO resp = new TKTrackUserOrderDTO();
		JSONObject payload = new JSONObject();
		try {

			System.err.println("payload for TrackOrder --OrderId--" + dto.getOrderId());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			client.addFilter(new HTTPBasicAuthFilter(TravelkhanaUtil.Travelkhana_Username,TravelkhanaUtil.Travelkhana_Password));
			WebResource res=client.resource(TravelkhanaUtil.trackOrder+dto.getOrderId());
			ClientResponse response=res.header(TravelkhanaUtil.USER, TravelkhanaUtil.VALUE)
				.type("application/json").get(ClientResponse.class);
			String strResponse = response.getEntity(String.class);
		  if(response.getStatus()!=406){
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus(Status.Failed);
			} else {
				if (strResponse != null) {
					ArrayList<TKUserOrderDTO>tKUserOrderObj = new ArrayList<TKUserOrderDTO>();
				    JSONArray jArray = new JSONArray(strResponse);
					for(int i=0;i<jArray.length();i++){
						long userOrderId = jArray.getJSONObject(i).getLong("userOrderId");
						String name = jArray.getJSONObject(i).getString("name");
						long contactNo = jArray.getJSONObject(i).getLong("contactNo");
						String coach = jArray.getJSONObject(i).getString("coach");
						String seat = jArray.getJSONObject(i).getString("seat");
						Long pnr = jArray.getJSONObject(i).getLong("pnr");
						String customerComment = jArray.getJSONObject(i).getString("customerComment");
						double totalAmount = jArray.getJSONObject(i).getDouble("totalAmount");
					//	String bookedDateTime = jArray.getJSONObject(i).getString("bookedDateTime");
						long orderDateDiff = jArray.getJSONObject(i).getLong("orderDateDiff");
						
						double advanceAmount = jArray.getJSONObject(i).getDouble("advanceAmount");
						double discount =jArray.getJSONObject(i).getDouble("discount");
						String  delivery_cost =jArray.getJSONObject(i).getString("delivery_cost");
						
				 //		String isBulk = jArray.getJSONObject(i).getString("isBulk");
						double walletDiscount = jArray.getJSONObject(i).getDouble("walletDiscount");
						String uId = jArray.getJSONObject(i).getString("uId");
						
						long trainNo = jArray.getJSONObject(i).getLong("trainNo");
						String stationName =jArray.getJSONObject(i).getString("stationName");
						long orderOutletId =jArray.getJSONObject(i).getLong("orderOutletId");
						String date = jArray.getJSONObject(i).getString("date");
						String trainName =jArray.getJSONObject(i).getString("trainName");
						String outletName = jArray.getJSONObject(i).getString("outletName");
						String stationCode = jArray.getJSONObject(i).getString("stationCode");
						String sta = jArray.getJSONObject(i).getString("sta");
						String eta = jArray.getJSONObject(i).getString("eta");
						String mailId = jArray.getJSONObject(i).getString("mailId");
						String ata = jArray.getJSONObject(i).getString("ata");
						String voucherCode = jArray.getJSONObject(i).getString("voucherCode");
						String status = jArray.getJSONObject(i).getString("status");
						double payable_amount = jArray.getJSONObject(i).getDouble("payable_amount");
						
						TKUserOrderDTO userOrderDTO= new TKUserOrderDTO();
						userOrderDTO.setAdvanceAmount(advanceAmount);
						userOrderDTO.setAta(ata);
				//		userOrderDTO.setBookedDateTime(bookedDateTime);
						userOrderDTO.setCoach(coach);
						userOrderDTO.setContactNo(contactNo);
						userOrderDTO.setCustomerComment(customerComment);
						userOrderDTO.setDate(date);
						userOrderDTO.setDelivery_cost(delivery_cost);
						userOrderDTO.setDiscount(discount);
						userOrderDTO.setEta(eta);
				//		userOrderDTO.setIsBulk(isBulk);
						userOrderDTO.setOrderDateDiff(orderDateDiff);
						userOrderDTO.setMailId(mailId);
						userOrderDTO.setName(name);
						userOrderDTO.setOrderOutletId(orderOutletId);
						userOrderDTO.setOutletName(outletName);
						userOrderDTO.setPayable_amount(payable_amount);
						userOrderDTO.setPnr(pnr);
						userOrderDTO.setSeat(seat);
						userOrderDTO.setSta(sta);
						userOrderDTO.setStationCode(stationCode);
						userOrderDTO.setStationName(stationName);
						userOrderDTO.setStatus(status);
						userOrderDTO.setTotalAmount(totalAmount);
						userOrderDTO.setTrainName(trainName);
						userOrderDTO.setTrainNo(trainNo);
						userOrderDTO.setUserOrderId(userOrderId);
						userOrderDTO.setVoucherCode(voucherCode);
						userOrderDTO.setuId(uId);
						
						//Menu Objects
						ArrayList<TkTrackMenuDTO>menuobj=new ArrayList<TkTrackMenuDTO>();
						JSONArray jArray2 = jArray.getJSONObject(i).getJSONArray("menu");
						for(int j=0;j<jArray2.length();j++){
							String itemName = jArray2.getJSONObject(j).getString("itemName");
							long quantity = jArray2.getJSONObject(j).getLong("quantity");
							double rate = jArray2.getJSONObject(j).getDouble("rate");
							
							TkTrackMenuDTO trackMenuDTO = new TkTrackMenuDTO();
							trackMenuDTO.setItemName(itemName);
							trackMenuDTO.setQuantity(quantity);
							trackMenuDTO.setRate(rate);
							
							 menuobj.add(trackMenuDTO);
						}
						userOrderDTO.setTkTrackMenuDTO(menuobj);
						tKUserOrderObj.add(userOrderDTO);
					}
					   resp.settKUserOrderDTO(tKUserOrderObj); //Success resp
					   resp.setSuccess(true);
				  }
				   else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus(Status.Failed);
				   } 
		        }
		  }  
		  else{
			  JSONObject jObj = new JSONObject(strResponse);	//Fail resp
			  String message = jObj.getString("Message");
			  System.out.println(message);
			  resp.setSuccess(false);
			  resp.setMessage(message);
		  }
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}
	
	@Override
	public TKPlaceOrderInitiateResp initate(TKPlaceOrderRequest dto) {
		TKPlaceOrderInitiateResp resp = new TKPlaceOrderInitiateResp();
		try {
			JSONObject payload = new JSONObject();
			
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.paymentInitateURLTK(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.header("hash", SecurityUtils.getHash(payload.toString())).type("application/json")
					.post(ClientResponse.class,dto.getJsonRequest().toString());
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage(strResponse);
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
							long userOrderId = jobj.getLong("userOrderId");
				    	     resp.setUserOrderId(userOrderId);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message); 
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public TKOrderDTO placeOrder(TKPlaceOrderRequest dto){
		TKOrderDTO resp = new TKOrderDTO();
		try {
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			client.addFilter(new HTTPBasicAuthFilter(TravelkhanaUtil.Travelkhana_Username,TravelkhanaUtil.Travelkhana_Password));
			WebResource res=client.resource(TravelkhanaUtil.placeOrder);
			ClientResponse response=res.header(TravelkhanaUtil.USER, TravelkhanaUtil.VALUE)
					.type("application/json").post(ClientResponse.class,dto.getJsonRequest2());
			String strResponse = response.getEntity(String.class);
	    if(response.getStatus()!=406){
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus(Status.Failed);
			} else {
				if (strResponse != null) {
					JSONObject jObj = new JSONObject(strResponse);
					 if(jObj!=null)
					 {
						 boolean status = jObj.getBoolean("Status");
						 	if(status){
					    	     JSONObject jObj2 = jObj.getJSONObject("Data");
					    	     long userOrderId = jObj2.getLong("userOrderId");
					    	     resp.setUserOrderId(userOrderId);
					    	     resp.setSuccess(true);
					    	     resp.setCode("S00");
					    	     resp.setMessage("Order placed successfully");
						 	}
						 	else{
						 			String message = jObj.getString("Message");
						 			resp.setSuccess(false);
						 			resp.setCode("F00");
						 			resp.setMessage(message);
						 	    }
					 }
					
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus(Status.Failed);
				}
			}
		}
		 else{
			  JSONObject jObj = new JSONObject(strResponse);	//Fail resp client
			  String message = jObj.getString("Message");
			  System.out.println(message);
			  resp.setSuccess(false);
			  resp.setCode("F00");
			  resp.setMessage(message);
		  }
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}
	@Override
	public TkApplyCouponDTO applycoupon(TKApplyCouponRequest request){
		TkApplyCouponDTO resp = new TkApplyCouponDTO();
		try {
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			client.addFilter(new HTTPBasicAuthFilter(TravelkhanaUtil.Travelkhana_Username,TravelkhanaUtil.Travelkhana_Password));
			WebResource res=client.resource(TravelkhanaUtil.applyCoupon+request.getCartDetails().get(0).getCouponCode());
			ClientResponse response=res.header(TravelkhanaUtil.USER, TravelkhanaUtil.VALUE).post(ClientResponse.class,request.getJsonRequest2());
			String strResponse = response.getEntity(String.class);
		 if(response.getStatus()!=406||response.getStatus()!=404){
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus(Status.Failed);
			} else {
				if (strResponse != null) {
					 JSONObject jobj = new JSONObject(strResponse);
					 //CartDetails
					 org.codehaus.jettison.json.JSONArray jArray  = jobj.getJSONArray("cartDetailBean");
					 ArrayList<TKCartDetailsDTO>cartDtoObj  = new ArrayList<TKCartDetailsDTO>();
					 for(int i=0;i<jArray.length();i++){
						 TKCartDetailsDTO cartDto = new TKCartDetailsDTO();
						 cartDto.setCouponCode(jArray.getJSONObject(i).getString("couponCode"));
						 cartDto.setTotalSP(jArray.getJSONObject(i).getDouble("totalSP"));
						 cartDto.setDiscount(jArray.getJSONObject(i).getDouble("discount"));
						 cartDto.setOutletId(jArray.getJSONObject(i).getLong("outletId"));
						 cartDto.setDiscountFlag(jArray.getJSONObject(i).getBoolean("discountFlag"));
						 cartDto.setDiscountMessage(jArray.getJSONObject(i).getString("discountMessage"));
						 cartDto.setCappingAmount(jArray.getJSONObject(i).getDouble("cappingAmount"));
						 cartDto.setCashbackAmount(jArray.getJSONObject(i).getDouble("cashbackAmount"));
						 cartDto.setCashbackFlag(jArray.getJSONObject(i).getBoolean("cashbackFlag"));
						 cartDto.setDeliveryCost(jArray.getJSONObject(i).getDouble("deliveryCost"));
						 cartDto.setMenuItems(jArray.getJSONObject(i).getString("menuItems"));
						 cartDto.setMinAmountThreshold(jArray.getJSONObject(i).getDouble("minAmountThreshold"));
						 cartDto.setOrderDate(jArray.getJSONObject(i).getString("orderDate"));
						 cartDto.setSequenceNumber(jArray.getJSONObject(i).getInt("sequenceNumber"));
						 cartDto.setServiceTax(jArray.getJSONObject(i).getDouble("serviceTax"));
						 cartDto.setTotalCP(jArray.getJSONObject(i).getDouble("totalCP"));
						 cartDto.setTotalPayableAmount(jArray.getJSONObject(i).getDouble("totalPayableAmount"));
						 cartDto.setTrainNumber(jArray.getJSONObject(i).getLong("trainNumber"));
						 cartDtoObj.add(cartDto);
					 }
					 boolean discountFlag = jArray.getJSONObject(0).getBoolean("discountFlag");
					 String discountMessage = jArray.getJSONObject(0).getString("discountMessage");
					 
					 TKPassengerInfoDTO passengerInfo = new TKPassengerInfoDTO();
					 JSONObject jobj3 = jobj.getJSONObject("passengerInfoBean");
					 String passengerName = jobj3.getString("passengerName");
					 String mobileNumber = jobj3.getString("mobileNumber");
					 String mailId = jobj3.getString("mailId");
					 String channel = jobj3.getString("channel");
					 String coach = jobj3.getString("coach");
					 String seat = jobj3.getString("seat");
					 String pnr = jobj3.getString("pnr");
					 String deviceId = jobj3.getString("deviceId");
					 
					 passengerInfo.setChannel(channel);
					 passengerInfo.setCoach(coach);
					 passengerInfo.setDeviceId(deviceId);
					 passengerInfo.setMailId(mailId);
					 passengerInfo.setMobileNumber(mobileNumber);
					 passengerInfo.setPassengerName(passengerName);
					 passengerInfo.setPnr(pnr);
					 passengerInfo.setSeat(seat);
					 
					 resp.setCartDetails(cartDtoObj);
				 	 resp.setPassengerInfo(passengerInfo);
				 	 
					 if(discountFlag){
					 	 resp.setSuccess(true);
					  } 
					 else{
						   resp.setSuccess(false);
					 }
					 resp.setMessage(discountMessage);
					 	 
				   }
					
				 else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus(Status.Failed);
				}
			}
		   } else{
				  JSONObject jObj = new JSONObject(strResponse);	//Fail resp client
				  String message = jObj.getString("Message");
				  System.out.println(message);
				  System.out.println("In fail client rep");
				  resp.setSuccess(false);
				  resp.setCode("F00");
				  resp.setMessage(message);
			  }
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}

	@Override
	public TKOrderSuccessDTO SuccessOrder(TKPlaceOrderRequest dto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TKMyOrderDetailsDTO myOrder(SessionDTO dto) {
		TKMyOrderDetailsDTO resp = new TKMyOrderDetailsDTO();
	  try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId",dto.getSessionId());
			System.err.println("payload for MyOrder+++++++++++++++++"+dto.getSessionId());
			
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.myOrderTK(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.header("hash", SecurityUtils.getHash(payload.toString())).type("application/json")
					.post(ClientResponse.class,payload.toString());
			String strResponse = response.getEntity(String.class);
			System.err.println("Response :: : " + strResponse);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage(strResponse);
				resp.setStatus(Status.Failed);
			} else {
				if (strResponse != null) {
					 JSONObject jobj = new JSONObject(strResponse);
					 boolean status = jobj.getBoolean("success");
					 String message = jobj.getString("message");
					 	if(status){
					// 	  JSONObject jobj2 = new JSONObject(jobj.getString("orderDetail"));
					 	  JSONArray jArr = new JSONArray(jobj.getString("orderDetail"));
				   		  ArrayList<TKOrderDetailsDTO>list = new ArrayList<TKOrderDetailsDTO>();
	
				   		 if (jArr != null) {
				   			for (int i = 0; i < jArr.length(); i++) {
				   			  TKOrderDetailsDTO orderDetail = new TKOrderDetailsDTO();
				   			  long id = jArr.getJSONObject(i).getLong("id");
				   			  String date = jArr.getJSONObject(i).getString("date");
				   			  String order_outlet_id = jArr.getJSONObject(i).getString("order_outlet_id");
				   			  String mail_id=jArr.getJSONObject(i).getString("mail_id");
				   			  System.out.println(date+"---"+order_outlet_id+"--"+mail_id+"-");			    	   
		//		   			  String created = jArr.getJSONObject(i).getString("created");
				   			  String train_number = jArr.getJSONObject(i).getString("train_number");
				   			  String orderStatus = jArr.getJSONObject(i).getString("orderStatus");
				   			  String station_code = jArr.getJSONObject(i).getString("station_code");
				   			  String transactionRefNo = jArr.getJSONObject(i).getString("transactionRefNo");
				   			  String userOrderId = jArr.getJSONObject(i).getString("userOrderId");
				   			  double totalCustomerPayable = jArr.getJSONObject(i).getDouble("totalCustomerPayable");
				   			  String seat = jArr.getJSONObject(i).getString("seat");
				   			  String eta = jArr.getJSONObject(i).getString("eta");
				   			  String pnr = jArr.getJSONObject(i).getString("pnr");
				   			  String coach = jArr.getJSONObject(i).getString("coach");
				   			  String contact_no = jArr.getJSONObject(i).getString("contact_no");
				   			  String name = jArr.getJSONObject(i).getString("name");
				   			  
				   			  long txnDate = jArr.getJSONObject(i).getLong("created");
				   			  Date date2=new Date(txnDate); 
				   			  String txnDate2=dateFormat.format(date2);
				   			  orderDetail.setTxnDate(txnDate2);
				   			  
				   			  orderDetail.setCoach(coach);
				   			  orderDetail.setContact_no(contact_no);
		//		   			  orderDetail.setTxnDate(created);
				   			  orderDetail.setOrderDate(date);
				   			  orderDetail.setEta(eta);
				   			  orderDetail.setMail_id(mail_id);
				   			  orderDetail.setName(name);
				   			  orderDetail.setOrder_outlet_id(order_outlet_id);
				   			  orderDetail.setOrderStatus(orderStatus);
				   			  orderDetail.setPnr(pnr);
				   			  orderDetail.setSeat(seat);
				   			  orderDetail.setStation_code(station_code);
				   			  orderDetail.setTrain_number(train_number);
				   			  orderDetail.setTransactionRefNo(transactionRefNo);
				   			  orderDetail.setUserOrderId(userOrderId);
				   			  orderDetail.setTotalCustomerPayable(totalCustomerPayable);
				   			  list.add(orderDetail);
				   		     }
				         }
				   			 resp.setSuccess(true);
				   			 resp.setOrderDetails(list);
				   		  } else {
				   					resp.setSuccess(false);
				   					resp.setCode("F00");
				   					resp.setStatus(Status.Failed);
				   		  		}
					 			resp.setMessage(message);
					    }
					 	else{
					 		resp.setSuccess(false);
					 		resp.setStatus(Status.Failed);
					 	 }
		
			   }
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus(Status.Failed);
		}
		return resp;
	}

}
