package com.payqwikweb.app.api.impl;

import java.util.Base64;

import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import com.payqwikweb.app.api.IAgentSendMoneyApi;
import com.payqwikweb.app.metadatas.UrlMetadatas;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Language;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.Version;
import com.payqwikweb.app.model.request.ASendMoneyBankRequest;
import com.payqwikweb.app.model.request.ListStoreApiRequest;
import com.payqwikweb.app.model.request.OfflinePaymentRequest;
import com.payqwikweb.app.model.request.PayAtStoreRequest;
import com.payqwikweb.app.model.request.SendMoneyBankRequest;
import com.payqwikweb.app.model.request.SendMoneyMobileRequest;
import com.payqwikweb.app.model.response.ListStoreApiResponse;
import com.payqwikweb.app.model.response.OfflinePaymentResponse;
import com.payqwikweb.app.model.response.PayAtStoreResponse;
import com.payqwikweb.app.model.response.SendMoneyBankResponse;
import com.payqwikweb.app.model.response.SendMoneyMobileResponse;
import com.payqwikweb.app.utils.SecurityUtils;
import com.payqwikweb.util.APIUtils;
import com.payqwikweb.util.JSONParserUtil;
import com.payqwikweb.util.LogCat;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;

public class AgentSendMoneyApi implements IAgentSendMoneyApi {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Override
	public PayAtStoreResponse payAtStoreResponseRequest(PayAtStoreRequest request,Role role) {

		PayAtStoreResponse resp = new PayAtStoreResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("netAmount",request.getNetAmount());
			payload.put("id",request.getId());
			Client client = Client.create();
			WebResource webResource = client
					.resource(UrlMetadatas.getPayAtStoreUrl(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					} else {
						resp.setSuccess(false);
						resp.setCode("F00");
						resp.setMessage("Service unavailable");
						resp.setStatus("FAILED");
						resp.setResponse(APIUtils.getFailedJSON().toString());
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;

	}

	@Override
	public SendMoneyMobileResponse sendMoneyMobileRequest(SendMoneyMobileRequest request,Role role) {
		SendMoneyMobileResponse resp = new SendMoneyMobileResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("mobileNumber", request.getMobileNumber());
			payload.put("amount", request.getAmount());
			payload.put("message",request.getMessage());
			System.err.println(payload);
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getSendMoneyAgentMobileUrl(Version.VERSION_1, role, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
				String strResponse = response.getEntity(String.class);
			System.err.println(strResponse);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					} else {
						resp.setSuccess(false);
						resp.setCode("F00");
						resp.setMessage("Service unavailable");
						resp.setStatus("FAILED");
						resp.setResponse(APIUtils.getFailedJSON().toString());
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public SendMoneyBankResponse sendMoneyBankRequest(ASendMoneyBankRequest request) {
		SendMoneyBankResponse response = new SendMoneyBankResponse();
		try {
			MultipartFile imageFile = request.getIdProofImage();
			MultipartFile imageFile2 = request.getIdProofBImage();
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("encodedBytes", Base64.getEncoder().encodeToString(imageFile.getBytes()));
			payload.put("contentType", imageFile.getContentType());
			payload.put("encodedBytes2", Base64.getEncoder().encodeToString(imageFile2.getBytes()));
			payload.put("contentType2", imageFile2.getContentType());
			
			request.setContentType(imageFile.getContentType());
			request.setEncodedBytes(Base64.getEncoder().encodeToString(imageFile.getBytes()));
			request.setContentType2(imageFile2.getContentType());
			request.setEncodedBytes2(Base64.getEncoder().encodeToString(imageFile2.getBytes()));
			
			request.setSenderName(request.getSenderName()+" "+request.getSenderLastName());
			request.setAccountName(request.getAccountName()+" "+request.getReceiverLastName());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(
					UrlMetadatas.initiateABankTransfer(Version.VERSION_1, Role.AGENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse clientResponse = webResource.accept("application/json")
					.header("hash", SecurityUtils.getHash(request.toString())).type("application/json")
					.post(ClientResponse.class, request.toJSON());
			String strResponse = clientResponse.getEntity(String.class);
			if (clientResponse.getStatus() != 200) {
				response.setCode("F00");
				response.setSuccess(false);
				response.setStatus("FAILED");
				response.setMessage("Response Status from server is ::" + clientResponse.getStatus());
			} else {
				System.err.println("RESPONSE :: " + strResponse);
				if (strResponse != null) {
					org.json.JSONObject json = new org.json.JSONObject(strResponse);
					final String code = JSONParserUtil.getString(json, "code");
					final String message = JSONParserUtil.getString(json, "message");
					response.setCode(code);
					response.setMessage(message);
					if (code.equalsIgnoreCase("S00")) {
						response.setSuccess(true);
					} else {
						response.setSuccess(false);
					}
				} else {
					response.setSuccess(false);
					response.setCode("F00");
					response.setMessage("Service unavailable");
					response.setStatus("FAILED");
					response.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setSuccess(false);
			response.setCode("F00");
			response.setMessage("Service Unavailable");
			response.setStatus("FAILED");
			response.setResponse(APIUtils.getFailedJSON().toString());
		}
		return response;
	}
	@Override
	public ListStoreApiResponse listStoreResponseRequest(ListStoreApiRequest request,Role role) {
		ListStoreApiResponse resp = new ListStoreApiResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getListStoreUrl(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					} else {
						resp.setSuccess(false);
						resp.setCode("F00");
						resp.setMessage("Service unavailable");
						resp.setStatus("FAILED");
						resp.setResponse(APIUtils.getFailedJSON().toString());
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public SendMoneyBankResponse sendMoneyBankRequest(SendMoneyBankRequest request,Role role) {
		SendMoneyBankResponse response  = new SendMoneyBankResponse();
		try{
            Client  client = Client.create();
            WebResource webResource = client.resource(UrlMetadatas.initiateBankTransfer(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
            ClientResponse clientResponse = webResource.accept("application/json").header("hash", SecurityUtils.getHash(request.toString())).type("application/json").post(ClientResponse.class,request.toJSON());
            if(clientResponse.getStatus() != 200){
                response.setCode("F00");
                response.setSuccess(false);
                response.setStatus("FAILED");
                response.setMessage("Response Status from server is ::"+clientResponse.getStatus());
            }else {
				String strResponse = clientResponse.getEntity(String.class);
				org.json.JSONObject json = new org.json.JSONObject(strResponse);
				final String code = JSONParserUtil.getString(json,"code");
				final String message  =  JSONParserUtil.getString(json,"message");
                response.setCode(code);
                response.setMessage(message);
				if(code.equalsIgnoreCase("S00")){
				    response.setSuccess(true);
				}else {
                    response.setSuccess(false);
                }
			}
		}catch(Exception e){
            e.printStackTrace();
            response.setSuccess(false);
            response.setCode("F00");
            response.setMessage("Service Unavailable");
            response.setStatus("FAILED");
            response.setResponse(APIUtils.getFailedJSON().toString());
        }
        return response;
	}

	@Override
	public OfflinePaymentResponse offlinePayment(OfflinePaymentRequest request,Role role) {
		OfflinePaymentResponse resp  = new OfflinePaymentResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("mobileNumber", request.getMobileNumber());
			payload.put("amount", request.getAmount());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.offlinePaymentUrl(Version.VERSION_1, Role.MERCHANT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
				String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				LogCat.print("RESPONSE :: " + strResponse);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					} else {
						resp.setSuccess(false);
						resp.setCode("F00");
						resp.setMessage("Service unavailable");
						resp.setStatus("FAILED");
						resp.setResponse(APIUtils.getFailedJSON().toString());
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public OfflinePaymentResponse verifyOTP(OfflinePaymentRequest request,Role role) {
		OfflinePaymentResponse resp  = new OfflinePaymentResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("otp", request.getOtp());
			payload.put("amount",request.getAmount());
			payload.put("mobileNumber",request.getMobileNumber());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.offlinePaymentOTP(Version.VERSION_1, Role.MERCHANT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
				String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					} else {
						resp.setSuccess(false);
						resp.setCode("F00");
						resp.setMessage("Service unavailable");
						resp.setStatus("FAILED");
						resp.setResponse(APIUtils.getFailedJSON().toString());
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}
	
	
	@Override
	public SendMoneyBankResponse sendMoneyToMBankRequest(SendMoneyBankRequest request,Role role) {
		SendMoneyBankResponse response  = new SendMoneyBankResponse();
        try{
            Client  client = Client.create();
            WebResource webResource = client.resource(UrlMetadatas.initiateMBankTransfer(Version.VERSION_1, Role.MERCHANT, Device.WEBSITE, Language.ENGLISH));
            ClientResponse clientResponse = webResource.accept("application/json").header("hash", SecurityUtils.getHash(request.toString())).type("application/json").post(ClientResponse.class,request.toJSON());
            if(clientResponse.getStatus() != 200){
                response.setCode("F00");
                response.setSuccess(false);
                response.setStatus("FAILED");
                response.setMessage("Response Status from server is ::"+clientResponse.getStatus());
            }else {
				String strResponse = clientResponse.getEntity(String.class);
				org.json.JSONObject json = new org.json.JSONObject(strResponse);
				final String code = JSONParserUtil.getString(json,"code");
				final String message  =  JSONParserUtil.getString(json,"message");
                response.setCode(code);
                response.setMessage(message);
				if(code.equalsIgnoreCase("S00")){
				    response.setSuccess(true);
				}else {
                    response.setSuccess(false);
                }
			}
		}catch(Exception e){
            e.printStackTrace();
            response.setSuccess(false);
            response.setCode("F00");
            response.setMessage("Service Unavailable");
            response.setStatus("FAILED");
            response.setResponse(APIUtils.getFailedJSON().toString());
        }
        return response;
	}

	@Override
	public SendMoneyMobileResponse sendMoneyRequestSuperAgent(SendMoneyMobileRequest request, Role role) {
		// TODO Auto-generated method stub
		SendMoneyMobileResponse resp = new SendMoneyMobileResponse();
		
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("mobileNumber", request.getMobileNumber());
			payload.put("amount", request.getAmount());
			payload.put("message",request.getMessage());
			System.err.println(payload);
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getSendMoneyRequestAgentUrl(Version.VERSION_1, role, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
				String strResponse = response.getEntity(String.class);
			System.err.println(strResponse);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					} else {
						resp.setSuccess(false);
						resp.setCode("F00");
						resp.setMessage("Service unavailable");
						resp.setStatus("FAILED");
						resp.setResponse(APIUtils.getFailedJSON().toString());
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

}