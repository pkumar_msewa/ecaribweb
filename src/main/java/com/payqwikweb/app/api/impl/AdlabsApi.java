package com.payqwikweb.app.api.impl;

import javax.ws.rs.core.MediaType;

import org.codehaus.jettison.json.JSONObject;

import com.payqwikweb.api.IAdlabsAPI;
import com.payqwikweb.app.metadatas.UrlMetadatas;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Language;
import com.payqwikweb.app.model.ResponseStatus;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.Version;
import com.payqwikweb.app.utils.SecurityUtils;
import com.payqwikweb.model.app.request.AdlabsAccessTokenRequest;
import com.payqwikweb.model.app.request.AdlabsTicketRequest;
import com.payqwikweb.model.app.response.AdlabOrderCreateRequest;
import com.payqwikweb.model.app.response.AdlabsAccessTokenResponse;
import com.payqwikweb.model.app.response.AdlabsAmountInitateRequest;
import com.payqwikweb.model.app.response.AdlabsAmountInitateResponse;
import com.payqwikweb.model.app.response.AdlabsOrderCreateResponse;
import com.payqwikweb.model.app.response.AdlabsOrderVoucher;
import com.payqwikweb.model.app.response.AdlabsPaymentRequest;
import com.payqwikweb.model.app.response.AdlabsProceedResponse;
import com.payqwikweb.model.app.response.AdlabsTicketListResponse;
import com.payqwikweb.model.app.response.GCIAmountInitateResponse;
import com.payqwikweb.model.app.response.GCIProceedResponse;
import com.payqwikweb.util.APIUtils;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;

public class AdlabsApi implements IAdlabsAPI {

	@Override
	public AdlabsAccessTokenResponse auth(AdlabsAccessTokenRequest request) {
		AdlabsAccessTokenResponse resp = new AdlabsAccessTokenResponse();
		JSONObject payload = new JSONObject();
		try {
			payload.put("username", "sonuarahan");
			payload.put("password", "12345");
			Client client = Client.create();
			WebResource webResource = client.resource("https://beta-imagica.adlabsimagica.com/b2w/user/login.json");
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.post(ClientResponse.class, payload.toString());
			String strResponse = response.getEntity(String.class);
			System.err.println("Access Token API RESPONSE : : " + strResponse);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus(ResponseStatus.FAILURE);
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jsonObj = new org.json.JSONObject(strResponse);
					if (jsonObj != null) {
						//org.json.JSONArray ticketList = jsonObj.getJSONArray("package");
						resp.setSuccess(true);
						resp.setCode("S00");
						resp.setStatus(ResponseStatus.SUCCESS);
						//resp.setJsonArray(strResponse);
						resp.setMessage(strResponse);
						resp.setDetails(strResponse);
						System.err.println("STR RESPONSE:: For AUTH :::::::"+strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus(ResponseStatus.FAILURE);
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}

	@Override
	public AdlabsTicketListResponse ticketList(AdlabsTicketRequest request) {

		AdlabsTicketListResponse resp = new AdlabsTicketListResponse();
		JSONObject payload = new JSONObject();
		try {
			payload.put("adult", "1");
			payload.put("child", "1");
			payload.put("sc", "1");
			payload.put("college", "1");
			payload.put("total_day", "1");
			payload.put("date_visit",request.getDate_departure());
			payload.put("date_departure",request.getDate_departure());
			payload.put("version", "new");
			payload.put("destination",request.getDestination());

			System.err.println("payload for ticket" + payload.toString() + " token ++++++++" + request.getAcessToken());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client
					.resource("https://beta-imagica.adlabsimagica.com/b2w_services/booknow-suggestions/search.json");
			ClientResponse response = webResource.header("x-csrf-token", request.getAcessToken()).header("cookie",request.getSession_name()).type(MediaType.APPLICATION_JSON)
					.post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			System.err.println("Search Ticket  LIST RESPONSE : : " + strResponse);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus(ResponseStatus.FAILURE);
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jsonObj = new org.json.JSONObject(strResponse);
					if (jsonObj != null) {
						//org.json.JSONArray ticketList = jsonObj.getJSONArray("package");
						resp.setSuccess(true);
						resp.setCode("S00");
						resp.setStatus(ResponseStatus.SUCCESS);
						resp.setJsonArray(strResponse);
						resp.setMessage(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus(ResponseStatus.FAILURE);
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}

	@Override
	public AdlabsOrderCreateResponse ordercreate(AdlabOrderCreateRequest request) {
		AdlabsOrderCreateResponse resp = new AdlabsOrderCreateResponse();
		try {
			MultivaluedMapImpl formData = new MultivaluedMapImpl();
			formData.add("total_day","1");
			formData.add("field_visit_date",request.getField_visit_date());
			formData.add("field_departure_date",request.getField_departure_date());
			formData.add("field_evt_res_business_unit",request.getField_evt_res_business_unit());
			formData.add("commerce_product[0][id]", request.getProduct_id());
			formData.add("commerce_product[0][quantity]", request.getQuantity());
			System.err.println("payload for Order Create " + formData.toString() + " token ++++++++" + request.getAccessToken());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client
					.resource("https://beta-imagica.adlabsimagica.com/b2w_services/book-order.json");
			ClientResponse response = webResource.header("x-csrf-token", request.getAccessToken()).header("cookie",request.getSession_name())
					.post(ClientResponse.class, formData);
			String strResponse = response.getEntity(String.class);
			System.err.println("Order Create  Ticket  LIST RESPONSE : : " + strResponse);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				//resp.setStatus(ResponseStatus.FAILURE);
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jsonObj = new org.json.JSONObject(strResponse);
					if (jsonObj != null) {
						//org.json.JSONArray ticketList = jsonObj.getJSONArray("package");
						resp.setSuccess(true);
						resp.setCode("S00");
						//resp.setStatus(ResponseStatus.SUCCESS);
						resp.setJsonArray(strResponse);
						resp.setMessage(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					//resp.setStatus(ResponseStatus.FAILURE);
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		//return resp;
		return resp;
	}

	@Override
	public AdlabsOrderCreateResponse payment(AdlabsPaymentRequest request) {
		AdlabsOrderCreateResponse resp = new AdlabsOrderCreateResponse();
		try {
			MultivaluedMapImpl formData = new MultivaluedMapImpl();
			formData.add("payment_data[uid]",request.getUid());
			formData.add("payment_data[amount]",request.getAmount());
			formData.add("payment_data[order_id]",request.getOrder_id());
			formData.add("payment_data[payment_method]","wallet");
			formData.add("payment_data[transaction_id]", request.getTransaction_id());
			formData.add("payment_data[status]", "success");
			formData.add("payment_data[currency_code]","INR");
			formData.add("profile_data[uid]",request.getUid());
			formData.add("profile_data[name_title]","Mrs.");
			formData.add("profile_data[first_name]",request.getFirst_name());
			formData.add("profile_data[last_name]", request.getLast_name());
			formData.add("profile_data[email_id]", request.getEmail_id());
			formData.add("profile_data[dob]","15-02-1992");
			formData.add("profile_data[mob_num]",request.getMob_num());
			formData.add("profile_data[clickfor_infant]","0");
			formData.add("profile_data[num_infant]","0");
			formData.add("profile_data[address]", request.getAddress());
			formData.add("profile_data[postal_code]","560068");
			formData.add("profile_data[country]","IN");
			formData.add("profile_data[state]","Karnataka");
			formData.add("profile_data[city]",request.getCity());
			formData.add("profile_data[order_id]",request.getOrder_id());
			 
			
			/*formData.add("payment_data[uid]",request.getUid());
			formData.add("payment_data[amount]",request.getAmount());
			formData.add("payment_data[order_id]",request.getOrder_id());
			formData.add("payment_data[payment_method]","wallet");
			formData.add("payment_data[transaction_id]", request.getTransaction_id());
			formData.add("payment_data[status]", "success");
			formData.add("payment_data[currency_code]", "INR");
			formData.add("profile_data[uid]",request.getUid());
			formData.add("profile_data[name_title]","Mrs.");
			formData.add("profile_data[first_name]","sonu");
			formData.add("profile_data[last_name]", "gupta");
			formData.add("profile_data[email_id]","sonuarahan@gmail.com");
			formData.add("profile_data[dob]","15-02-1992");
			formData.add("profile_data[mob_num]","9066340634");
			formData.add("profile_data[clickfor_infant]","0");
			formData.add("profile_data[num_infant]","0");
			formData.add("profile_data[address]","Bangalore");
			formData.add("profile_data[postal_code]","560068");
			formData.add("profile_data[country]","IN");
			formData.add("profile_data[state]","UttarPradesh");
			formData.add("profile_data[city]","Kanpur");
			formData.add("profile_data[order_id]",request.getOrder_id());*/
			
			
			

			System.err.println("payload for Payment Adlabs " + formData.toString() + " token ++++++++" + request.getAccessToken());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client
					.resource("https://beta-imagica.adlabsimagica.com/b2w_services/payment-process/payment.json");
			ClientResponse response = webResource.header("x-csrf-token", request.getAccessToken()).header("cookie",request.getSession_name())
					.post(ClientResponse.class, formData);
			String strResponse = response.getEntity(String.class);
			System.err.println("Payment adlabs  Ticket  LIST RESPONSE : : " + strResponse);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				//resp.setStatus(ResponseStatus.FAILURE);
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jsonObj = new org.json.JSONObject(strResponse);
					if (jsonObj != null) {
						//org.json.JSONArray ticketList = jsonObj.getJSONArray("package");
						resp.setSuccess(true);
						resp.setCode("S00");
						//resp.setStatus(ResponseStatus.SUCCESS);
						resp.setJsonArray(strResponse);
						resp.setMessage(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					//resp.setStatus(ResponseStatus.FAILURE);
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		//return resp;
		return resp;
	}

	@Override
	public AdlabsAmountInitateResponse initate(AdlabsAmountInitateRequest request) {
		AdlabsAmountInitateResponse resp = new AdlabsAmountInitateResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessioniId", request.getSessioniId());
			payload.put("amount", request.getAmount());
			payload.put("serviceCode", "ADLABS");
			
			
			System.err.println("payload for Initiate amount for adlab+++++++++++++++++"+payload.toString());
			
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.paymentInitateURLAdlabs(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.header("hash", SecurityUtils.getHash(payload.toString()))
					.post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			System.err.println("Response :: : " + strResponse);
			if (response.getStatus() != 200) {
				org.json.JSONObject jobj = new org.json.JSONObject(strResponse);

				
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage(strResponse);
				//resp.setDetails(details);
				System.err.println("INITATE DETAIL ======+++"+resp.getDetails());
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						String txnId = jobj.getString("txnId");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							//org.json.JSONObject jobj = new org.json.JSONObject(strResponse);

							final String details = (String) jobj.get("details");
						     System.err.println("+++++++++++++++=======DETAILS:::::::::::======="+details);
						   resp.setDetails(details);
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setTxnId(txnId);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public AdlabsProceedResponse sucees(AdlabsOrderVoucher request) {
		AdlabsProceedResponse resp = new AdlabsProceedResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessioniId", request.getSessioniId());
			payload.put("transactionRefNo", request.getTransactionRefNo());
			payload.put("code", request.getCode());
			payload.put("success_profile", request.getSuccess_profile());
			payload.put("success_payment", request.getSuccess_payment());
			payload.put("amount", request.getAmount());
			
			
			System.err.println("Payload for Amount Success ++++++++++++++++++++"+payload.toString());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.paymentsuccessURLAdlabs(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.header("hash", SecurityUtils.getHash(payload.toString()))
					.post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			
			System.err.println("STR RESPONSE::::::::::::::::+++++++++++++++++++++"+strResponse);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						String txnId = jobj.getString("txnId");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							
							final String details = (String) jobj.get("details");
						     System.err.println("+++++++++++++++=======DETAILS:: From SUCCESS:::::::::======="+details);
						   resp.setDetails(details);
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setTxnId(txnId);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}
}
	/*public static AdlabsTicketListResponse ticketLista(AdlabsTicketRequest request) {

		AdlabsTicketListResponse resp = new AdlabsTicketListResponse();
		JSONObject payload = new JSONObject();
		try {
			payload.put("adult", "1");
			payload.put("child", "1");
			payload.put("sc", "1");
			payload.put("college", "1");
			payload.put("total_day", "1");
			payload.put("date_visit", "04-05-2017");
			payload.put("date_departure", "04-05-2017");
			payload.put("version", "new");
			payload.put("destination", "Theme Park");
			System.err.println("payload for ticket" + payload.toString() + " token ++++++++" + request.getAcessToken());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client
					.resource("https://beta-imagica.adlabsimagica.com/b2w_services/booknow-suggestions/search.json");
			ClientResponse response = webResource.header("x-csrf-token", request.getAcessToken()).header("cookie", "SSESS1234ab32b74cc679f4f3658aaf5f7cd4=upH_8-EzEipDPcqyMyBsAvKa5JnTqhylagKVCPvNYyU").type(MediaType.APPLICATION_JSON)
					.post(ClientResponse.class, payload);
			
			
			
			String strResponse = response.getEntity(String.class);
			System.err.println("strresponse"+strResponse);
			System.err.println("Search Ticket  LIST RESPONSE : : " + response.getStatus());
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus(ResponseStatus.FAILURE);
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jsonObj = new org.json.JSONObject(strResponse);
					if (jsonObj != null) {
						org.json.JSONArray ticketList = jsonObj.getJSONArray("package");
						resp.setSuccess(true);
						resp.setCode("S00");
						resp.setStatus(ResponseStatus.SUCCESS);
						resp.setJsonArray(ticketList);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus(ResponseStatus.FAILURE);
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}

	public static void main(String... a) {
		AdlabsTicketRequest request = new AdlabsTicketRequest();
		request.setAcessToken("fFmumLL2cYjuRylhgvSwvmCd-8zM67a5mbKSAXdgeWU");
		ticketLista(request);
	}*/
