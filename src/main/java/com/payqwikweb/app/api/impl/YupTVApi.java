package com.payqwikweb.app.api.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.apache.commons.codec.binary.Base64;
import org.codehaus.jettison.json.JSONObject;

import com.payqwikweb.app.api.IYupTVApi;
import com.payqwikweb.app.metadatas.UrlMetadatas;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Language;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.Version;
import com.payqwikweb.app.model.request.YupTVRequest;
import com.payqwikweb.app.model.response.YupTVResponse;
import com.payqwikweb.app.utils.SecurityUtils;
import com.payqwikweb.util.APIUtils;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;

public class YupTVApi implements IYupTVApi {

	@Override
	public YupTVResponse getRegister(YupTVRequest request) {
		YupTVResponse resp=new YupTVResponse();
		String str="VPAYQWIK:befb821e72a6a771f2a7d4767c73cd2f";
		String data=encrpyt(str);
		String authorization="BASIC "+data;
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("firstname", request.getFirstName());
			payload.put("lastname", request.getLastName());
			payload.put("mobile", request.getMobile());
			payload.put("unique_id", request.getUnique_id());
			payload.put("region", request.getRegion());
			payload.put("email_id", request.getEmail());
			System.err.println("payload ::"+payload.toString());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(APIUtils.REGISTER_YUPTV);
			ClientResponse response = webResource.accept("application/json").header("Content-Type", "application/json")
					.header("Authorization", authorization).post(ClientResponse.class,payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				org.json.JSONObject jsonObj = new org.json.JSONObject(strResponse);
				if (jsonObj != null) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage(jsonObj.getString("message"));
				resp.setStatus("Failure");
				resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				if (strResponse != null) {
					System.err.println("response ::" + strResponse);
					org.json.JSONObject jsonObj = new org.json.JSONObject(strResponse);
					if (jsonObj != null) {
						String status = jsonObj.getString("status");
						String token = jsonObj.getString("token");
						String message = jsonObj.getString("message");
						String userId = jsonObj.getString("userId");
						String expiry = jsonObj.getString("expiry");
						String partnerId = jsonObj.getString("partnerId");
					        String date = convertMilisecToDate(Long.parseLong(expiry)*1000);
						if (status.equalsIgnoreCase("1")) {
							resp.setSuccess(true);
							resp.setCode("S01");
							resp.setMessage(message);
							resp.setStatus(status);
							resp.setToken(token);
							resp.setExpiry(date);
							resp.setPartnerId(partnerId);
							resp.setUserId(userId);
						}else{
							if((status.equalsIgnoreCase("0"))){
							resp.setSuccess(false);
							resp.setCode("S00");
							resp.setMessage(message);
							resp.setStatus(status);
							resp.setToken(token);
							resp.setExpiry(date);
							resp.setPartnerId(partnerId);
							resp.setUserId(userId);
							}
						}
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("Failure");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}
	@Override
	public YupTVResponse CheckRegister(YupTVRequest request) {
		YupTVResponse resp=new YupTVResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("unique_id", request.getUnique_id());
			payload.put("amount", request.getAmount());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(
					UrlMetadatas.CheckRegisterURL(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("Failure");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					System.err.println("response ::" + strResponse);
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if(jobj.getString("txnId")!=null){
						final String txnId = (String) jobj.get("txnId");
						resp.setTxnId(txnId);
						}
						final String details = (String) jobj.get("details");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
							resp.setCode(code);
							resp.setStatus(status);
							resp.setMessage(message);
							resp.setResponse(strResponse);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
						resp.setDetails(details);
						
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("Failure");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}
	@Override
	public YupTVResponse saveRegister(YupTVRequest request) {
		YupTVResponse resp=new YupTVResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("partnerId", request.getPartnerId());
			payload.put("status", request.getStatus());
			payload.put("expiryDate", request.getExpiryDate());
			payload.put("token", request.getToken());
			payload.put("userId", request.getUserId());
			payload.put("message", request.getMessage());
			payload.put("unique_id", request.getUnique_id());
			payload.put("transactionRefNo", request.getTransactionRefNo());
			payload.put("amount", request.getAmount());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(
					UrlMetadatas.saveKeyAndSecretURL(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("Failure");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					System.err.println("response ::" + strResponse);
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						final String txnId = (String) jobj.get("txnId");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
						resp.setTxnId(txnId);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("Failure");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}
	@Override
	public YupTVResponse getDeactivated(YupTVRequest request) {
		YupTVResponse resp=new YupTVResponse();
		String str="VPAYQWIK:befb821e72a6a771f2a7d4767c73cd2f";
		String data=encrpyt(str);
		String authorization="BASIC "+data;
		try {
			JSONObject payload = new JSONObject();
			payload.put("account_no", request.getUnique_id());
			System.err.println("payload ::"+payload.toString());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(APIUtils.DEACTIVATE_YUPTV);
			ClientResponse response = webResource.accept("application/json").header("Content-Type", "application/json")
					.header("Authorization", authorization).post(ClientResponse.class,payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				System.err.println("response :::" + strResponse);
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("Failure");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					System.err.println("response ::" + strResponse);
					org.json.JSONObject jsonObj = new org.json.JSONObject(strResponse);
					if (jsonObj != null) {
						String status = jsonObj.getString("status");
						String message = jsonObj.getString("message");
						if (status.equalsIgnoreCase("true")) {
							resp.setSuccess(true);
							resp.setCode("S00");
							resp.setMessage(message);
							resp.setStatus(status);
						}else{
							resp.setSuccess(false);
							resp.setCode("F00");
							resp.setMessage(message);
							resp.setStatus(status);
						}
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("Failure");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}
	
	@Override
	public YupTVResponse existUser(YupTVRequest request) {
		YupTVResponse resp=new YupTVResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("partnerId", request.getPartnerId());
			payload.put("status", request.getStatus());
			payload.put("expiryDate", request.getExpiryDate());
			payload.put("token", request.getToken());
			payload.put("userId", request.getUserId());
			payload.put("message", request.getMessage());
			payload.put("unique_id", request.getUnique_id());
			payload.put("transactionRefNo", request.getTransactionRefNo());
			payload.put("amount", request.getAmount());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(
					UrlMetadatas.existUserURL(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("Failure");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					System.err.println("response ::" + strResponse);
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
//						final String txnId = (String) jobj.get("txnId");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
						resp.setPartnerId(request.getPartnerId());
						resp.setUserId(request.getUserId());
						resp.setToken(request.getToken());
						resp.setExpiry(request.getExpiryDate());
//						resp.setTxnId(txnId);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("Failure");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}
	
	public static String encrpyt(String str){
		byte[] encoded = Base64.encodeBase64(str.getBytes()); 
		String data=new String(encoded);
		return data;
	}
	 private static String convertMilisecToDate(long milisec) {
		 return new SimpleDateFormat("yyyy-MM-dd").format(milisec);
	 }
}
