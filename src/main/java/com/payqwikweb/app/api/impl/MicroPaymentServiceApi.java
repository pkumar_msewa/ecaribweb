package com.payqwikweb.app.api.impl;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.payqwikweb.app.api.IMicroPaymentServiceApi;
import com.payqwikweb.app.metadatas.UrlMetadatas;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Language;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.Version;
import com.payqwikweb.app.model.request.MicroPaymentInitiateRequest;
import com.payqwikweb.app.model.request.MicroPaymentRequest;
import com.payqwikweb.app.model.response.MicroPaymentResponse;
import com.payqwikweb.app.utils.SecurityUtils;
import com.payqwikweb.util.APIUtils;
import com.payqwikweb.util.LogCat;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;

public class MicroPaymentServiceApi implements IMicroPaymentServiceApi {

	@Override
	public MicroPaymentResponse getNikkiToken(MicroPaymentRequest request) {
		MicroPaymentResponse resp = new MicroPaymentResponse();
		JSONObject payload = new JSONObject();
		try {
			payload.put("sessionId", request.getSessionId());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getNikkiToken(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));

			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				String strResponse = response.getEntity(String.class);
				System.err.println(strResponse);
				LogCat.print("RESPONSE :: " + strResponse);

				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setDetails(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				System.out.println("RESPONSE :: " + strResponse);
				if (strResponse != null) {
					org.json.JSONObject jobj;
					try {
						jobj = new org.json.JSONObject(strResponse);
						if (jobj != null) {
							final String status = (String) jobj.get("status");
							final String code = (String) jobj.get("code");
							final String message = (String) jobj.get("message");
							final Object details = (String) jobj.get("details");
							resp.setCode(code);
							resp.setDetails(details);
							resp.setMessage(message);
							resp.setStatus(status);
						}
					} catch (org.json.JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return resp;
	}

	@Override
	public MicroPaymentResponse getIntinateNikkiTransaction(MicroPaymentInitiateRequest request) {
		MicroPaymentResponse resp = new MicroPaymentResponse();
		JSONObject payload = new JSONObject();
		try {
			payload.put("tokenKey", request.getTokenKey());
			payload.put("secretKey", request.getSecretKey());
			payload.put("mobileNumber", request.getMobileNumber());
			payload.put("amount", request.getAmount());
			payload.put("consumerSecretKey", request.getConsumerSecretKey());
			payload.put("consumerTokenKey", request.getConsumerTokenKey());
			payload.put("authCode", request.getAuthCode());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(UrlMetadatas.getIntinateNikkiTransaction(Version.VERSION_1,
					Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
			} else {
				String strResponse = response.getEntity(String.class);
				String txnId = null;
				String details=null;
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
							txnId = (String) jobj.get("txnId");
							details = (String) jobj.get("details");
						} else {
							resp.setSuccess(false);
						}
						resp.setDetails(details);
						resp.setTxnId(txnId);
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
		}
		return resp;
	}

	
	@Override
	public MicroPaymentResponse getUpdateNikkiTransaction(MicroPaymentRequest request) {

		MicroPaymentResponse resp = new MicroPaymentResponse();
		JSONObject payload = new JSONObject();
		try {
			payload.put("tokenKey", request.getTokenKey());
			payload.put("secretKey", request.getSecretKey());
			payload.put("mobileNumber", request.getMobileNumber());
			payload.put("amount", request.getAmount());
			payload.put("consumerSecretKey", request.getConsumerSecretKey());
			payload.put("consumerTokenKey", request.getConsumerTokenKey());
			payload.put("retrivalReferenceNumber", request.getRetrivalReferenceNumber());
			payload.put("transactionRefNo", request.getTransactionRefNo());
			payload.put("status", request.isSuccess());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(UrlMetadatas.getUpdateNikkiTransaction(Version.VERSION_1,
					Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
			} else {
				String strResponse = response.getEntity(String.class);
				String details=null;
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
							details = (String) jobj.getString("details");
						} else {
							resp.setSuccess(false);
						}
						resp.setDetails(details);
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
		}
		return resp;
	}

	
	
	@Override
	public MicroPaymentResponse getRefundNikkiTransaction(MicroPaymentRequest request) {
		MicroPaymentResponse resp = new MicroPaymentResponse();
		JSONObject payload = new JSONObject();
		try {
			payload.put("consumerSecretKey", request.getConsumerSecretKey());
			payload.put("consumerTokenKey", request.getConsumerTokenKey());
			payload.put("transactionRefNo", request.getTransactionRefNo());
			payload.put("retrivalReferenceNumber", request.getRetrivalReferenceNumber());
			payload.put("status", request.isSuccess());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(UrlMetadatas.getRefundNikkiTransaction(Version.VERSION_1,
					Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
			} else {
				String strResponse = response.getEntity(String.class);
				String txnId = null;
				String details=null;
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
							details = (String) jobj.get("details");
						} else {
							resp.setSuccess(false);
						}
						resp.setDetails(details);
						resp.setTxnId(txnId);
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
		}
		return resp;
	}


}
