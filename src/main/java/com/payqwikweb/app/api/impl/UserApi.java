package com.payqwikweb.app.api.impl;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jettison.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import com.gci.model.request.LoginDTO;
import com.letsManage.model.response.ResponseStatus;
import com.payqwik.visa.util.QRRequestDTO;
import com.payqwik.visa.util.QRResponseDTO;
import com.payqwikweb.app.api.IUserApi;
import com.payqwikweb.app.metadatas.Constants;
import com.payqwikweb.app.metadatas.UrlMetadatas;
import com.payqwikweb.app.model.AccessDTO;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Language;
import com.payqwikweb.app.model.MerchantDTO;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.Version;
import com.payqwikweb.app.model.request.BescomRefundRequest;
import com.payqwikweb.app.model.request.ChangeMPINRequest;
import com.payqwikweb.app.model.request.ChangePasswordRequest;
import com.payqwikweb.app.model.request.DeleteMPINRequest;
import com.payqwikweb.app.model.request.EBSRefundRequest;
import com.payqwikweb.app.model.request.EditProfileRequest;
import com.payqwikweb.app.model.request.FavouriteRequest;
import com.payqwikweb.app.model.request.FetchMobileList;
import com.payqwikweb.app.model.request.ForgotMpinRequest;
import com.payqwikweb.app.model.request.InviteFriendEmailRequest;
import com.payqwikweb.app.model.request.InviteFriendMobileRequest;
import com.payqwikweb.app.model.request.KycVerificationDTO;
import com.payqwikweb.app.model.request.NearByAgentsRequest;
import com.payqwikweb.app.model.request.NearByMerchantsRequest;
import com.payqwikweb.app.model.request.NotificationsGCMDTO;
import com.payqwikweb.app.model.request.ReceiptsRequest;
import com.payqwikweb.app.model.request.RedeemCodeRequest;
import com.payqwikweb.app.model.request.SessionDTO;
import com.payqwikweb.app.model.request.SetMPINRequest;
import com.payqwikweb.app.model.request.TreatCardDTO;
import com.payqwikweb.app.model.request.UploadPictureRequest;
import com.payqwikweb.app.model.request.UserDetailsByAdminRequest;
import com.payqwikweb.app.model.request.UserDetailsRequest;
import com.payqwikweb.app.model.request.UserMaxLimitDto;
import com.payqwikweb.app.model.request.VerifyMPINRequest;
import com.payqwikweb.app.model.request.VisaRequest;
import com.payqwikweb.app.model.response.ChangePasswordResponse;
import com.payqwikweb.app.model.response.CustDetailsResp;
import com.payqwikweb.app.model.response.EBSRedirectResponse;
import com.payqwikweb.app.model.response.EditProfileResponse;
import com.payqwikweb.app.model.response.InviteFriendEmailResponse;
import com.payqwikweb.app.model.response.InviteFriendMobileResponse;
import com.payqwikweb.app.model.response.KycResponse;
import com.payqwikweb.app.model.response.MPINResponse;
import com.payqwikweb.app.model.response.MerchantResponse;
import com.payqwikweb.app.model.response.NearByAgentResponse;
import com.payqwikweb.app.model.response.NearBymerchantResponse;
import com.payqwikweb.app.model.response.NotificationsResponse;
import com.payqwikweb.app.model.response.ReceiptsResponse;
import com.payqwikweb.app.model.response.SharePointsResponse;
import com.payqwikweb.app.model.response.TreatCardPlansResponse;
import com.payqwikweb.app.model.response.TreatCardResponse;
import com.payqwikweb.app.model.response.UpdateAccessResponse;
import com.payqwikweb.app.model.response.UploadPictureResponse;
import com.payqwikweb.app.model.response.UserDetailsByAdminResponse;
import com.payqwikweb.app.model.response.UserDetailsResponse;
import com.payqwikweb.app.model.response.VisaResponse;
import com.payqwikweb.app.utils.SecurityUtils;
import com.payqwikweb.model.app.request.UpdateReceiptDTO;
import com.payqwikweb.model.app.response.AccountDTO;
import com.payqwikweb.model.app.response.BalanceDTO;
import com.payqwikweb.model.app.response.BasicDTO;
import com.payqwikweb.model.app.response.ImageDTO;
import com.payqwikweb.model.app.response.LimitDTO;
import com.payqwikweb.model.app.response.LocationDTO;
import com.payqwikweb.model.app.response.RedeemCodeResponse;
import com.payqwikweb.model.app.response.UpdateReceiptResponse;
import com.payqwikweb.model.web.GciAuthDTO;
import com.payqwikweb.model.web.ImagicaAuthDTO;
import com.payqwikweb.model.web.ImagicaOrderRequest;
import com.payqwikweb.model.web.ImagicaOrderResponse;
import com.payqwikweb.model.web.ImagicaPaymentRequest;
import com.payqwikweb.model.web.ImagicaPaymentResponse;
import com.payqwikweb.model.web.LocationRequest;
import com.payqwikweb.model.web.LocationResponse;
import com.payqwikweb.model.web.SharePointDTO;
import com.payqwikweb.model.web.Status;
import com.payqwikweb.util.APIUtils;
import com.payqwikweb.util.ConvertUtil;
import com.payqwikweb.util.JSONParserUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.thirdparty.model.ResponseDTO;
import com.thirdparty.util.MerchantConstants;

public class UserApi implements IUserApi {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	private Object nullObject = null;

	@Override
	public UpdateAccessResponse updateAccessFromSuperAdmin(AccessDTO dto) {
		UpdateAccessResponse resp = new UpdateAccessResponse();
		try {
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getUpdateAccessURL(Version.VERSION_1, Role.SUPERADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(dto.toJSON().toString())).post(ClientResponse.class, dto.toJSON());
				String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setSuccess(false);
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if(code.equalsIgnoreCase("S00")) {
							org.json.JSONObject details = JSONParserUtil.getObject(jobj,"details");
							if(details != null) {
								resp.setSuccess(true);
								AccessDTO result = new AccessDTO();
								result.setUpdateAuthMerchants(JSONParserUtil.getBoolean(details,"updateAuthUser"));
								result.setUpdateAuthAdmin(JSONParserUtil.getBoolean(details,"updateAuthAdmin"));
								result.setUpdateAuthUser(JSONParserUtil.getBoolean(details,"updateAuthUser"));
								result.setUpdateAuthAgents(JSONParserUtil.getBoolean(details,"updateAuthAgents"));
								result.setSendGCM(JSONParserUtil.getBoolean(details,"sendGCM"));
								result.setSendSMS(JSONParserUtil.getBoolean(details,"sendSMS"));
								result.setSendMail(JSONParserUtil.getBoolean(details,"sendMail"));
								result.setUpdateLimits(JSONParserUtil.getBoolean(details,"updateLimits"));
								result.setLoadMoney(JSONParserUtil.getBoolean(details,"loadMoney"));
								result.setRequestMoney(JSONParserUtil.getBoolean(details,"requestMoney"));
								result.setSendMoney(JSONParserUtil.getBoolean(details,"sendMoney"));
								result.setDeductMoney(JSONParserUtil.getBoolean(details,"deductMoney"));
								result.setUsername(JSONParserUtil.getString(details,"username"));
								resp.setDetails(result);
							}
						}
						resp.setCode(code);
						resp.setMessage(JSONParserUtil.getString(jobj,"message"));
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setSuccess(false);
		}

		return resp;
	}

	@Override
	public BasicDTO getBasicDetails(UserDetailsRequest request, Role role) {
		BasicDTO resp = new BasicDTO();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getBasicDetailsURL(Version.VERSION_1, role, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if(code.equalsIgnoreCase("S00")) {
							org.json.JSONObject details = JSONParserUtil.getObject(jobj,"details");
							if(details != null) {
								resp.setAddress(JSONParserUtil.getString(details,"address"));
								resp.setContactNo(JSONParserUtil.getString(details,"contactNo"));
								resp.setDateOfBirth(JSONParserUtil.getString(details,"dateOfBirth"));
								resp.setEmail(JSONParserUtil.getString(details,"email"));
								resp.setGender(JSONParserUtil.getString(details,"gender"));
								resp.setEmailStatus(Status.valueOf(JSONParserUtil.getString(details,"emailStatus")));
								resp.setMobileStatus(Status.valueOf(JSONParserUtil.getString(details,"mobileStatus")));
								resp.setName(JSONParserUtil.getString(details,"name"));
							}
						}
						resp.setCode(code);
						resp.setStatus(JSONParserUtil.getString(jobj,"status"));
						resp.setMessage(JSONParserUtil.getString(jobj,"message"));
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
		}
		return resp;
	}


	@Override
	public BalanceDTO getBalanceDetails(UserDetailsRequest request, Role role) {
		BalanceDTO resp = new BalanceDTO();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getBalanceDetailsURL(Version.VERSION_1, role, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if(code.equalsIgnoreCase("S00")) {
							resp.setBalance(JSONParserUtil.getDouble(jobj,"details"));
						}
						resp.setCode(code);
						resp.setStatus(JSONParserUtil.getString(jobj,"status"));
						resp.setMessage(JSONParserUtil.getString(jobj,"message"));
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
		}
		return resp;
	}

	@Override
	public AccountDTO getAccountDetails(UserDetailsRequest request, Role role) {
		AccountDTO resp = new AccountDTO();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getAccountDetailsURL(Version.VERSION_1, role, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if(code.equalsIgnoreCase("S00")) {
							org.json.JSONObject details = JSONParserUtil.getObject(jobj,"details");
							resp.setAccountNumber(JSONParserUtil.getLong(details,"accountNumber"));
							resp.setAccountType(JSONParserUtil.getString(details,"accountType"));
							resp.setRewardPoints(JSONParserUtil.getLong(details,"rewardPoints"));
						}
						resp.setCode(code);
						resp.setStatus(JSONParserUtil.getString(jobj,"status"));
						resp.setMessage(JSONParserUtil.getString(jobj,"message"));
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
		}
		return resp;
	}

	@Override
	public ImageDTO getImageDetails(UserDetailsRequest request, Role role) {
		ImageDTO resp = new ImageDTO();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getImageDetailsURL(Version.VERSION_1, role, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if(code.equalsIgnoreCase("S00")) {
							resp.setImageUrl(JSONParserUtil.getString(jobj,"details"));
						}
						resp.setCode(code);
						resp.setStatus(JSONParserUtil.getString(jobj,"status"));
						resp.setMessage(JSONParserUtil.getString(jobj,"message"));
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
		}
		return resp;
	}

	@Override
	public LimitDTO getLimitDetails(UserDetailsRequest request, Role role) {
		LimitDTO resp = new LimitDTO();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getLimitDetailsURL(Version.VERSION_1, role, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if(code.equalsIgnoreCase("S00")) {
							org.json.JSONObject details = JSONParserUtil.getObject(jobj,"details");
							if(details != null) {
								resp.setBalanceLimit(JSONParserUtil.getDouble(details,"balanceLimit"));
								resp.setCreditConsumed(JSONParserUtil.getDouble(details,"creditConsumed"));
								resp.setDailyLimit(JSONParserUtil.getDouble(details,"dailyLimit"));
								resp.setDebitConsumed(JSONParserUtil.getDouble(details,"debitConsumed"));
								resp.setMonthlyLimit(JSONParserUtil.getDouble(details,"monthlyLimit"));
							}
						}
						resp.setCode(code);
						resp.setStatus(JSONParserUtil.getString(jobj,"status"));
						resp.setMessage(JSONParserUtil.getString(jobj,"message"));
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
		}
		return resp;
	}

	@Override
	public LocationDTO getLocationDetails(UserDetailsRequest request, Role role) {
		LocationDTO resp = new LocationDTO();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getLocationDetailsURL(Version.VERSION_1, role, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String code = (String) jobj.get("code");
						if(code.equalsIgnoreCase("S00")) {
							org.json.JSONObject details = JSONParserUtil.getObject(jobj,"details");
							if(details != null) {
								resp.setCircleName(JSONParserUtil.getString(details, "circleName"));
								resp.setLocality(JSONParserUtil.getString(details, "locality"));
								resp.setDistrictName(JSONParserUtil.getString(details,"districtName"));
								resp.setPinCode(JSONParserUtil.getString(details,"pinCode"));
								resp.setRegionName(JSONParserUtil.getString(details,"regionName"));
							}
						}
						resp.setCode(code);
						resp.setStatus(JSONParserUtil.getString(jobj,"status"));
						resp.setMessage(JSONParserUtil.getString(jobj,"message"));
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
		}
		return resp;
	}

	@Override
	public UserDetailsResponse getUserDetails(UserDetailsRequest request, Role role) {
		UserDetailsResponse resp = new UserDetailsResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getUserDetailsUrl(Version.VERSION_1, role, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						final boolean hasRefer=(boolean)jobj.get("hasRefer");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
							final double balance = (double) jobj.getJSONObject("details").getJSONObject("accountDetail")
									.get("balance");
							final int points = (int) jobj.getJSONObject("details").getJSONObject("accountDetail")
									.get("points");
							final long accountNumber = (long) jobj.getJSONObject("details")
									.getJSONObject("accountDetail").get("accountNumber");
							final String accountTypeDescription = (String) jobj.getJSONObject("details")
									.getJSONObject("accountDetail").getJSONObject("accountType").get("description");
							final String accountTypeName = (String) jobj.getJSONObject("details")
									.getJSONObject("accountDetail").getJSONObject("accountType").get("name");
							final double accountTypeMonthlyLimit = (double) jobj.getJSONObject("details")
									.getJSONObject("accountDetail").getJSONObject("accountType").get("monthlyLimit");
							final double accountTypeDailyLimit = (double) jobj.getJSONObject("details")
									.getJSONObject("accountDetail").getJSONObject("accountType").get("dailyLimit");
							/*final double accountTypeBalanceLimit = (double) jobj.getJSONObject("details")
									.getJSONObject("accountDetail").getJSONObject("accountType").get("balanceLimit");
							final String username = (String) jobj.getJSONObject("details").getJSONObject("userDetail")
									.get("username");*/
							final String firstName = (String) jobj.getJSONObject("details").getJSONObject("userDetail")
									.get("firstName");
							final String middleName = (String) jobj.getJSONObject("details").getJSONObject("userDetail")
									.get("middleName");
							final String lastName = (String) jobj.getJSONObject("details").getJSONObject("userDetail")
									.get("lastName");
							final String address = (String) jobj.getJSONObject("details").getJSONObject("userDetail")
									.get("address");
							final String contactNo = (String) jobj.getJSONObject("details").getJSONObject("userDetail")
									.get("contactNo");
							final String userType = (String) jobj.getJSONObject("details").getJSONObject("userDetail")
									.get("userType");
							final String authority = (String) jobj.getJSONObject("details").getJSONObject("userDetail")
									.get("authority");
							final String emailStatus = (String) jobj.getJSONObject("details")
									.getJSONObject("userDetail").get("emailStatus");
							final String mobileStatus = (String) jobj.getJSONObject("details")
									.getJSONObject("userDetail").get("mobileStatus");
							final String email = (String) jobj.getJSONObject("details").getJSONObject("userDetail")
									.get("email");
							final String image = (String) jobj.getJSONObject("details").getJSONObject("userDetail")
									.get("image");
							final String imageContent  =  jobj.getJSONObject("details").getJSONObject("userDetail").getString("encodedImage");
							resp.setAddress(address);
							resp.setAuthority(authority);
							resp.setContactNo(contactNo);
							resp.setEmail(email);
							resp.setEmailStatus(emailStatus);
							resp.setFirstName(firstName);
							resp.setImage(image);
							resp.setLastName(lastName);
							resp.setMiddleName(middleName);
							resp.setMobileStatus(mobileStatus);
							resp.setUserType(userType);
							resp.setHasRefer(hasRefer);
							resp.setCode(code);
							resp.setAccountType(accountTypeDescription);
							resp.setDailyTransaction(accountTypeDailyLimit);
							resp.setMonthlyTransaction(accountTypeMonthlyLimit);
							resp.setUserType(accountTypeName);
							DecimalFormat twoDForm = new DecimalFormat("#.##");
							resp.setBalance(Double.valueOf(twoDForm.format(balance)));
							resp.setPoints(points);
							resp.setAccountNumber(accountNumber);
							resp.setImageContent(imageContent);
							resp.setSuccess(true);
							resp.setMdexKey(UrlMetadatas.MDEX_CLIENTKEY);
							resp.setMdexToken(UrlMetadatas.MDEX_CLIENTTOKEN);
							resp.setIplEnable(Constants.IPL_ENABLE);
							resp.setIplMyPrediction(Constants.URL_TEAM_MY_PREDICT);
							resp.setIplPrediction(Constants.URL_TEAM_PREDICT);
							resp.setIplSchedule(Constants.URL_TEAM_LIST);
							resp.setMdexUsername(Constants.MDEX_USERNAME);
							resp.setMdexPassword(Constants.MDEX_PASSWORD);
							resp.setMdexUrl(Constants.MDEX_URL);
							resp.setEbsEnable(Constants.EBS_ENABLE);
							resp.setVnetEnable(Constants.VNET_ENABLE);
							resp.setRazorPayEnable(Constants.RAZORPAY_ENABLE);
							resp.setTopupImage(Constants.TOPUP_IMAGEURL);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					} 
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public EditProfileResponse editProfile(EditProfileRequest request) {
		EditProfileResponse resp = new EditProfileResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("username", nullObject);
			payload.put("password", nullObject);
			payload.put("confirmPassword", nullObject);
			payload.put("userType", nullObject);
			payload.put("authority", nullObject);
			payload.put("status", nullObject);
			payload.put("address", request.getAddress());
			payload.put("contactNo", nullObject);
			payload.put("firstName", request.getFirstName());
			payload.put("middleName", "");
			payload.put("lastName", request.getLastName());
			payload.put("locationCode", nullObject);
			payload.put("gender", request.getGender());
			payload.put("dateOfBirth", nullObject);
			payload.put("email", request.getEmail());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getEditProfileUrl(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setDetails(" ");
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setDetails(" ");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setDetails(" ");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public MerchantResponse getAllMerchants(SessionDTO dto) {
		MerchantResponse resp = new MerchantResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", dto.getSessionId());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getMerchantsURL(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
//						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
							JSONArray array = JSONParserUtil.getArray(jobj, "details");
							List<MerchantDTO> merchantList = ConvertUtil.convertFromArray(array);
							resp.setMerchantList(merchantList);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setMessage(message);
					} 
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
		}
		return resp;
	}

	@Override
	public ChangePasswordResponse changePassword(ChangePasswordRequest request) {
		ChangePasswordResponse resp = new ChangePasswordResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("password", request.getPassword());
			payload.put("username", request.getUsername());
			payload.put("newPassword", request.getNewPassword());
			payload.put("confirmPassword", request.getConfirmPassword());
			payload.put("key", request.getUsername());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getUpdatePasswordUrl(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					} 
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public ChangePasswordResponse updateMerchantPassword(ChangePasswordRequest request) {
		ChangePasswordResponse resp = new ChangePasswordResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("password", request.getOldPassword());
			payload.put("username", request.getUsername());
			payload.put("newPassword", request.getNewPassword());
			payload.put("confirmPassword", request.getConfirmPassword());
			payload.put("key", request.getUsername());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.renewMerchantPasswordURL(Version.VERSION_1,
					Role.MERCHANT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					} 
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public UploadPictureResponse uploadPicture(UploadPictureRequest request) {
		UploadPictureResponse resp = new UploadPictureResponse();
		try {
			MultipartFile imageFile = request.getProfilePicture();
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("encodedBytes", Base64.getEncoder().encodeToString(imageFile.getBytes()));
			payload.put("contentType", imageFile.getContentType());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getUploadPictureUrl(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setDetails(" ");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setDetails(" ");
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setDetails(" ");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setDetails(" ");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public ReceiptsResponse getReceipts(ReceiptsRequest request) {
		ReceiptsResponse resp = new ReceiptsResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("page", request.getPage());
			payload.put("size", request.getSize());
			payload.put("userStatus", "");
			Client client = Client.create();
			
			WebResource webResource = client.resource(
					UrlMetadatas.getReceiptsUrl(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
//							final JSONArray receiptsArray = jobj.getJSONObject("details").getJSONArray("content");
//							resp.setAdditionalInfo(receiptsArray);
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					} 
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public ReceiptsResponse getReceiptsFilter(ReceiptsRequest request) {
		ReceiptsResponse resp = new ReceiptsResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("fromDate", request.getFromDate());
			payload.put("toDate", request.getToDate());
			payload.put("page", request.getPage());
			payload.put("size", request.getSize());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getReceiptsFilterUrl(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
//							final JSONArray receiptsArray = jobj.getJSONObject("details").getJSONArray("content");
//							resp.setAdditionalInfo(receiptsArray);
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}
	
	@Override
	public ReceiptsResponse getMerchantTransactions(ReceiptsRequest request) {
		ReceiptsResponse resp = new ReceiptsResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("page", request.getPage());
			payload.put("size", request.getSize());
			payload.put("userStatus", "");
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getVpayqwikMerchantReceiptURL(Version.VERSION_1,
					Role.MERCHANT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					} 
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}
	
	
	@Override
	public ReceiptsResponse getBescomMerchantTxns(ReceiptsRequest request) {
		ReceiptsResponse resp = new ReceiptsResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("page", request.getPage());
			payload.put("size", request.getSize());
			payload.put("userStatus", "");
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(UrlMetadatas.getBescomMerchantReceiptURL(Version.VERSION_1,
					Role.MERCHANT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					} 
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	
	@Override
	public ReceiptsResponse getMerchantReceipts(ReceiptsRequest request) {
		ReceiptsResponse resp = new ReceiptsResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getVpayqwikMerchantTransactionsURL(Version.VERSION_1,
					Role.MERCHANT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					} 
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	
	@Override
	public ReceiptsResponse getMerchantReceiptsFiltered(ReceiptsRequest request) {
		ReceiptsResponse resp = new ReceiptsResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("page", request.getPage());
			payload.put("size", request.getSize());
			payload.put("fromDate", request.getFromDate());
			payload.put("toDate", request.getToDate());
			payload.put("userStatus", "");
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getVpayqwikMerchantTransactionsFilteredURL(Version.VERSION_1,
					Role.MERCHANT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					} 
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}
	
	
	@Override
	public ReceiptsResponse getDebitMerchantReceipts(ReceiptsRequest request) {
		ReceiptsResponse resp = new ReceiptsResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("page", request.getPage());
			payload.put("size", request.getSize());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getDebitVpayqwikMerchantTransactionsURL(Version.VERSION_1,
					Role.MERCHANT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					} 
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public ReceiptsResponse getSuccessfulTransactions(SessionDTO dto) {
		ReceiptsResponse resp = new ReceiptsResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", dto.getSessionId());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getTransactionsUrl(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					} 
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public ReceiptsResponse updateFavouriteTransaction(FavouriteRequest dto) {
		ReceiptsResponse resp = new ReceiptsResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", dto.getSessionId());
			payload.put("favourite", dto.isFavourite());
			payload.put("transactionRefNo", dto.getTransactionRefNo());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getFavouriteUrl(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					} 
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public MPINResponse setMPIN(SetMPINRequest request) {
		MPINResponse resp = new MPINResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("username", request.getUsername());
			payload.put("newMpin", request.getNewMpin());
			payload.put("confirmMpin", request.getConfirmMpin());
			payload.put("dateOfBirth", request.getDateOfBirth());

			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getSetMPINUrl(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));

			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);

			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					} 
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public MPINResponse changeMPIN(ChangeMPINRequest request) {
		MPINResponse resp = new MPINResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("username", request.getUsername());
			payload.put("oldMpin", request.getOldMpin());
			payload.put("newMpin", request.getNewMpin());
			payload.put("confirmMpin", request.getConfirmMpin());

			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getChangeMPINUrl(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));

			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);

			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					} 
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public MPINResponse deleteMPIN(DeleteMPINRequest request) {
		MPINResponse resp = new MPINResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getDeleteMPINUrl(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					} 
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public MPINResponse forgotMPIN(ForgotMpinRequest request) {
		MPINResponse resp = new MPINResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("password", request.getPassword());
			payload.put("dateOfBirth", request.getDateOfBirth());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getForgotMPINUrl(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));

			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
							if (code.equalsIgnoreCase("F04")) {
								org.json.JSONObject json = JSONParserUtil.getObject(jobj, "details");
//								final boolean valid = JSONParserUtil.getBoolean(json, "valid");
								final String password = JSONParserUtil.getString(json, "password");
								final String dateOfBirth = JSONParserUtil.getString(json, "dateOfBirth");
								if (password != null) {
									resp.setResponse(password);
								}
								if (dateOfBirth != null) {
									resp.setDetails(dateOfBirth);
								}
							}
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
					} 
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public MPINResponse verifyMPIN(VerifyMPINRequest request) {
		MPINResponse resp = new MPINResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("username", request.getUsername());
			payload.put("oldMpin", request.getNewMpin());
			payload.put("newMpin", "");
			payload.put("confirmMpin", "");
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getVerifyMPINUrl(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));

			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);

			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						final boolean hasRefer=(boolean) jobj.get("hasRefer");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setHasRefer(hasRefer);
						resp.setResponse(strResponse);
					} 
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public InviteFriendEmailResponse inviteEmailFriend(InviteFriendEmailRequest request) {
		InviteFriendEmailResponse resp = new InviteFriendEmailResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("receiversName", request.getReceiversName());
			payload.put("message", (request.getMessage() == null) ? "" : request.getMessage());
			payload.put("email", request.getEmail());
			payload.put("mobileNo", (request.getMobileNo() == null) ? "" : request.getMobileNo());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getInviteEmailFriendUrl(Version.VERSION_1, Role.USER,
					Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					} 
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public InviteFriendMobileResponse inviteMobileFriend(InviteFriendMobileRequest request) {
		InviteFriendMobileResponse resp = new InviteFriendMobileResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("receiversName", request.getReceiversName());
//			payload.put("message", "You are invited to download VPayQwik app");
			payload.put("email", "");
			payload.put("mobileNo", request.getMobileNo());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getInviteMobileFriendUrl(Version.VERSION_1,
					Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					} 
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}


	@Override
	public UserDetailsByAdminResponse userTransactionAndDetails(UserDetailsByAdminRequest request) {

		UserDetailsByAdminResponse resp = new UserDetailsByAdminResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("page", 0);
			payload.put("size", 100);
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getUserDetailByAdminUrl(Version.VERSION_1,
					Role.ADMIN, Device.WEBSITE, Language.ENGLISH, request.getUsername()));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);

					} 
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;

	}

	@Override
	public UserDetailsResponse reSendEmailTop(UserDetailsRequest request) {
		UserDetailsResponse resp = new UserDetailsResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.reSendEmailOTPUrl(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
                        if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					} 
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public RedeemCodeResponse redeemPromoCode(RedeemCodeRequest request) {
		RedeemCodeResponse resp = new RedeemCodeResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("promoCode", request.getPromoCode());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.redeemPromoCode(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					} 
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public SharePointsResponse sharePoints(SharePointDTO dto) {
		SharePointsResponse resp = new SharePointsResponse();
		try {
            Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getSharePointsURL(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(dto.toJSON().toString()))
					.post(ClientResponse.class, dto.toJSON());
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					} 
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public KycResponse kycVerification(KycVerificationDTO request) {

		KycResponse resp = new KycResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("mobileNumber", request.getMobileNumber());
			payload.put("accountNumber", request.getAccountNumber());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.kycRequestUrl(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						// resp.setResponse(strResponse);
					} 
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public KycResponse kycOTPVerification(KycVerificationDTO request) {

		KycResponse resp = new KycResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("otp", request.getOtp());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.kycOtpVerificationUrl(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						// resp.setResponse(strResponse);
					} 
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	

	@Override
	public KycResponse resendKycOTP(KycVerificationDTO request) {

		KycResponse resp = new KycResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("mobileNumber", request.getMobileNumber());
			payload.put("key", "");
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getResendKycOtpUrl(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
//						final Object details = (Object) jobj.get("details");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());

		}
		return resp;
	}

	@Override
	public VisaResponse visaTransactionInitinated(VisaRequest request) {
		VisaResponse resp = new VisaResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", "session");
//			payload.put("request", URLEncoder.encode(request.getRequest(), "UTF-8"));
			payload.put("request", request.getRequest());
			payload.put("amount", 10);
			payload.put("merchantName", "mer");
			payload.put("merchantId", "mer");
			payload.put("merchantAddress", "asa");
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.visaInitatedRequestUrl(Version.VERSION_1, Role.USER, request.getDevice(), Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					 String txnId =null;
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status =jobj.getString("status");
						final String code = jobj.getString("code");
						final String message =  jobj.getString("message");
						final Object details = (Object) jobj.get("details");
						 
						if (code.equalsIgnoreCase("S00")) {
							txnId = (String) jobj.get("txnId");
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
						resp.setDetails(details);
						resp.setTxnId(txnId);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());

		}
		return resp;
	}

	public EBSRedirectResponse getAllTransactionsRefund(EBSRefundRequest req) {
		return null;
	}

	@Override
	public VisaResponse prepareGetAccountNumberRequest(VisaRequest request) {
		VisaResponse resp = new VisaResponse();
		try {
			JSONObject payload = new JSONObject();
//			payload.put("request", request.getMerchantName());
//			payload.put("data", request.getMerchantId());
//			payload.put("object", request.getMerchantId());
//			payload.put("txRef", request.getMerchantId());
//			payload.put("transactionStatus", request.getMerchantId());
//			payload.put("externalTransactionId", request.getMerchantId());
//			payload.put("amount", request.getMerchantId());
//			payload.put("merchantName", request.getMerchantId());
//			payload.put("merchantId", request.getMerchantId());
//			payload.put("merchantAddress", request.getMerchantAddress());
//			payload.put("additionalData", request.getMerchantAddress());
			payload.put("sessionId", request.getSessionId());
			
			Client client = Client.create();
			System.out.println("URL : "+UrlMetadatas.prepareGetAccountNumberRequest(Version.VERSION_1, Role.USER, Device.ANDROID, Language.ENGLISH));
			WebResource webResource = client.resource(
					UrlMetadatas.prepareGetAccountNumberRequest(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			System.out.println("Response Code :  "+response.getStatus());
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			}
			else{
				System.out.println("respoinse :: "+strResponse);
				org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
				resp.setMessage((String)jobj.getString("message"));
				resp.setCode("S00");
			}
	}catch(Exception e){
		e.printStackTrace();
		}
		
		return resp;

	}

	@Override
	public VisaResponse prepareGetAccNoDecrption(VisaRequest encrypted) {
		VisaResponse resp = new VisaResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("object", encrypted.getRequest());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.prepareGetAccNoDecryption(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			System.out.println("Send App server to decrypt data "+response.getStatus() +"AND RESPONSE "+strResponse.toString());
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			}
			else{
				org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
				resp.setMessage((String)jobj.getString("details"));
				resp.setCode("S00");
				
			}
	}catch(Exception e){
		e.printStackTrace();
		}
		
		return resp;

	}


	@Override
	public QRResponseDTO getEntityIdOfM2P(QRRequestDTO req){
		
		QRResponseDTO resp=new QRResponseDTO();
		
		JSONObject payload = new JSONObject();
		try {
			payload.put("username", req.getUsername());
		} catch (org.codehaus.jettison.json.JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Client client = Client.create();
		WebResource webResource = client.resource(
				UrlMetadatas.m2pGetEntityId(Version.VERSION_1, Role.MERCHANT, Device.ANDROID, Language.ENGLISH));
		ClientResponse response = webResource.accept("application/json").type("application/json").post(ClientResponse.class, payload);
		String strResponse = response.getEntity(String.class);
		if (response.getStatus() != 200) {
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
		} else {
			if (strResponse != null) {
				org.json.JSONObject jobj = null;
				try {
					jobj = new org.json.JSONObject(strResponse);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				String entityId = null;
				try {
					entityId = jobj.getString("details");
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				resp.setEntityId(entityId);
				resp.setCode("S00");
				resp.setMessage("ENtity Id suc-sexfully fetched");
				resp.setSuccess(true);
				
		
	}
			else {
				resp.setCode("F00");
				resp.setMessage("Entity doesn't exist..");
				resp.setSuccess(false);
				
			}
		}
		return resp;
	}
	
	@Override
	public UpdateReceiptResponse userUpdateReceipts(UpdateReceiptDTO request) {

		UpdateReceiptResponse resp = new UpdateReceiptResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("fromDate", request.getFromDate());
			payload.put("toDate", request.getToDate());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.updateReceiptUrl(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						
//						final double credit = (double) jobj.getJSONObject("details").get("credit");
//						final double debit = (double) jobj.getJSONObject("details").get("debit");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
//						resp.setDebit(debit);
//						resp.setCredit(credit);
					    resp.setResponse(strResponse);
					} 
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}
	
	
	@Override
	public UpdateReceiptResponse userUpdateReceiptsinAjax(UpdateReceiptDTO request) {

		UpdateReceiptResponse resp = new UpdateReceiptResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("fromDate", request.getFromDate());
			payload.put("toDate", request.getToDate());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.updateReceiptInAjaxUrl(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						
						final double credit = (double) jobj.getJSONObject("details").get("credit");
						final double debit = (double) jobj.getJSONObject("details").get("debit");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setDebit(debit);
						resp.setCredit(credit);
					    resp.setResponse(strResponse);
					} 
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}
	
	@Override
	public NotificationsResponse getNotifications(SessionDTO req) throws JSONException {
	
	NotificationsResponse resp=new NotificationsResponse();
		List<NotificationsGCMDTO> list=new ArrayList<>();
		JSONObject payload = new JSONObject();
		try {
			payload.put("sessionId", req.getSessionId());
		} catch (org.codehaus.jettison.json.JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Client client = Client.create();
		//System.out.println("URL : "+UrlMetadatas.visaInitatedRequestUrl(Version.VERSION_1, Role.USER, Device.ANDROID, Language.ENGLISH));
		WebResource webResource = client.resource(
				UrlMetadatas.getNotifications(Version.VERSION_1, Role.USER, Device.ANDROID, Language.ENGLISH));
		ClientResponse response = webResource.accept("application/json").type("application/json").post(ClientResponse.class, payload);
		String strResponse = response.getEntity(String.class);
		if (response.getStatus() != 200) {
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
		} else {
			if (strResponse != null) {
				org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
				String message=	jobj.getString("message");
				String code=	jobj.getString("code");
				JSONArray details = jobj.getJSONArray("details");
			if(details!=null){
				for(int i=0;i<details.length();i++){
					org.json.JSONObject jsonObj = details.getJSONObject(i);
					ObjectMapper mapper = new ObjectMapper();
				    NotificationsGCMDTO usr = null;
					try {
						usr = mapper.readValue(jsonObj.toString(), NotificationsGCMDTO.class);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}      
				    list.add(usr);
				}
				resp.setDetails(list);
			}
				resp.setCode(code);
				resp.setMessage(message);
				
		
	}
			else {
				resp.setCode("F00");
				resp.setMessage("operation failed");
				
			}
		}
		return resp;
	}

	@Override
	public ImagicaAuthDTO getImagicaAuth(SessionDTO dto) {
		ImagicaAuthDTO resp = new ImagicaAuthDTO();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", dto.getSessionId());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getImagicaCredentials(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setMessage("Service unavailable");
			} else{
				org.json.JSONObject json = new org.json.JSONObject(strResponse);
				if(json != null) {
					String code = JSONParserUtil.getString(json,"code");
					String message = JSONParserUtil.getString(json,"message");
					if(code.equalsIgnoreCase("S00")) {
							org.json.JSONObject details = JSONParserUtil.getObject(json,"details");
							if(details != null) {
								resp.setCookie(JSONParserUtil.getString(details,"cookie"));
								resp.setToken(JSONParserUtil.getString(details,"token"));
								resp.setSuccess(true);
							}
					}
					resp.setMessage(message);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return resp;
	}

	@Override
	public ImagicaOrderResponse placeImagicaOrder(ImagicaOrderRequest request) {
		ImagicaOrderResponse resp = new ImagicaOrderResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("uid", request.getUid());
			payload.put("orderId", request.getOrderId());
			payload.put("totalAmount", request.getTotalAmount());
			payload.put("baseAmount", request.getBaseAmount());
			payload.put("serviceTax", request.getServiceTax());
			payload.put("sbCess", request.getSbCess());
			payload.put("kkCess", request.getKkCess());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.placeImagicaOrder(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setMessage("Service unavailable");
			} else {
				org.json.JSONObject json = new org.json.JSONObject(strResponse);
				if (json != null) {
					String code = JSONParserUtil.getString(json, "code");
					String message = JSONParserUtil.getString(json, "message");
					if (code.equalsIgnoreCase("S00")) {
						org.json.JSONObject details = JSONParserUtil.getObject(json, "details");
						if (details != null) {
							resp.setUid(JSONParserUtil.getString(details, "uid"));
							resp.setOrderId(JSONParserUtil.getString(details, "orderId"));
							resp.setAmount(JSONParserUtil.getString(details, "amount"));
							resp.setTransactionId(JSONParserUtil.getString(details, "transactionId"));
							resp.setSuccess(true);
							resp.setCode("S00");

						}
						resp.setMessage(message);
						resp.setCode(code);
					} else {
						resp.setMessage(message);
						resp.setCode(code);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode("F00");
		}

		return resp;
	}

	@Override
	public ImagicaPaymentResponse processPayment(ImagicaPaymentRequest request) {
		ImagicaPaymentResponse resp = new ImagicaPaymentResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("profileId",request.getProfileId());
			payload.put("transactionRefNo",request.getTransctionId());
			payload.put("success",request.isSuccess());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.processImagicaOrder(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setMessage("Service unavailable");
			} else {
				org.json.JSONObject json = new org.json.JSONObject(strResponse);
				
				if(json != null) {
					String code = JSONParserUtil.getString(json,"code");
					String message = JSONParserUtil.getString(json,"message");
					resp.setMessage(message);
					if(code.equalsIgnoreCase("S00")) {
						resp.setSuccess(true);
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}

		return resp;
	}

	@Override
	public LocationResponse getLocationByPin(LocationRequest request) {
		LocationResponse resp = new LocationResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("pincode", request.getPincode());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getLocationInfo(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setMessage("Service unavailable");
			} else {
				org.json.JSONObject json = new org.json.JSONObject(strResponse);
				if (json != null) {
					String code = JSONParserUtil.getString(json, "code");
					String message = JSONParserUtil.getString(json, "message");
					resp.setMessage(message);
					if (code.equalsIgnoreCase("S00")) {
						resp.setSuccess(true);
						org.json.JSONObject details = JSONParserUtil.getObject(json, "details");
						if (details != null) {
							resp.setSuccess(true);
							resp.setCity(JSONParserUtil.getString(details, "districtName"));
							resp.setState(JSONParserUtil.getString(details, "circleName"));
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}

	public UpdateReceiptResponse userReceipts(UpdateReceiptDTO request) {
		UpdateReceiptResponse resp = new UpdateReceiptResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.userReceiptUrl(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						
						final double credit = (double) jobj.getJSONObject("details").get("credit");
						final double debit = (double) jobj.getJSONObject("details").get("debit");
								
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setDebit(debit);
						resp.setCredit(credit);
					    resp.setResponse(strResponse);
					} 
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}
	
	/*@Override
	public ReceiptsResponse getMerchantReceiptsFilter(ReceiptsRequest request) {
		ReceiptsResponse resp = new ReceiptsResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("page", request.getPage());
			payload.put("size", request.getSize());
			payload.put("toDate", request.getToDate());
			payload.put("fromDate", request.getFromDate());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getVpayqwikMerchantTransactionsURL(Version.VERSION_1,
					Role.MERCHANT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				System.err.println("the response is::"+strResponse);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					} else {
						resp.setSuccess(false);
						resp.setCode("F00");
						resp.setMessage("Service unavailable");
						resp.setStatus("FAILED");
						resp.setResponse(APIUtils.getFailedJSON().toString());
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}*/
	
	/*public ReceiptsResponse getReceiptsMerchantOld(ReceiptsRequest request) {
		ReceiptsResponse resp = new ReceiptsResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("page", request.getPage());
			payload.put("size", request.getSize());
			payload.put("userStatus", "");
			Client client = Client.create();
			
			System.err.println(payload.toString());
			WebResource webResource = client.resource(
					UrlMetadatas.getReceiptsUrl(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				System.err.println("response is::"+strResponse);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							final JSONArray receiptsArray = jobj.getJSONObject("details").getJSONArray("content");
							for(int i=0;i<receiptsArray.length();i++){
								ReceiptsResponse resps=new ReceiptsResponse();
								org.json.JSONObject jobject=receiptsArray.getJSONObject(i);
								
							
							}
							resp.setAdditionalInfo(receiptsArray);
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					} else {
						resp.setSuccess(false);
						resp.setCode("F00");
						resp.setMessage("Service unavailable");
						resp.setStatus("FAILED");
						resp.setResponse(APIUtils.getFailedJSON().toString());
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}
*/
	
	@Override
	public EditProfileResponse editName(EditProfileRequest request) {
		EditProfileResponse resp = new EditProfileResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("firstName", request.getFirstName());
			payload.put("lastName", request.getLastName());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getEditNameUrl(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						final String details = (String) jobj.get("details");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setDetails(details);
						resp.setResponse(strResponse);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setDetails(" ");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setDetails(" ");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public ReceiptsResponse getMerchantReceiptscommision(ReceiptsRequest request) {
		ReceiptsResponse resp = new ReceiptsResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("page", request.getPage());
			payload.put("size", request.getSize());
			payload.put("userStatus", "");
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getMerchanttransactionCommision(Version.VERSION_1,
					Role.MERCHANT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					} 
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}
	
	
	@Override
	public GciAuthDTO getGciAuth(SessionDTO dto) {
		GciAuthDTO resp = new GciAuthDTO();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", dto.getSessionId());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getGciCredentials(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setMessage("Service unavailable");
			} else{
				org.json.JSONObject json = new org.json.JSONObject(strResponse);
				if(json != null) {
					String code = JSONParserUtil.getString(json,"code");
					String message = JSONParserUtil.getString(json,"message");
					if(code.equalsIgnoreCase("S00")) {
							org.json.JSONObject details = JSONParserUtil.getObject(json,"details");
							if(details != null) {
								resp.setToken(JSONParserUtil.getString(details,"token"));
								resp.setSuccess(true);
							}
					}
					resp.setMessage(message);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return resp;
	}


	@Override
	public ResponseDTO getMobileListForRefer(FetchMobileList dto) {
		ResponseDTO resp = new ResponseDTO();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", dto.getSessionId());
			payload.put("mobileList", dto.getMobileList());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getRefernEarn(Version.VERSION_1, Role.USER, Device.ANDROID, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setMessage("Service unavailable");
			} else{
				org.json.JSONObject json = new org.json.JSONObject(strResponse);
				if(json != null) {
					String code = JSONParserUtil.getString(json,"code");
					String message = JSONParserUtil.getString(json,"message");
					 String status=JSONParserUtil.getString(json,"status");
					if(code.equalsIgnoreCase("S00")) {
								resp.setSuccess(true);
								resp.setMessage(message);
								resp.setStatus(status);
								resp.setCode(code);
							}else{
								resp.setSuccess(false);
								resp.setMessage(message);
								resp.setStatus(status);
								resp.setCode(code);
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return resp;
	}
	
	
	@Override
	public NearBymerchantResponse getNearByMerchants(NearByMerchantsRequest request, Role role) {
		NearBymerchantResponse resp = new NearBymerchantResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("latitude", request.getLatitude());
			payload.put("longitude", request.getLongitude());
			payload.put("radius", (request.getRadius() == 0) ? "" : request.getRadius());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getNearByMerchantsUrl(Version.VERSION_1, role, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
					} 
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}
	
	
	@Override
	public TreatCardResponse getTreatCardDetails(SessionDTO dto) {
		TreatCardResponse resp = new TreatCardResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", dto.getSessionId());
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.getTreatCardDetails(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setMessage("Service unavailable");
			} else{
				org.json.JSONObject json = new org.json.JSONObject(strResponse);
				if(json != null) {
					String code = JSONParserUtil.getString(json,"code");
					String message = JSONParserUtil.getString(json,"message");
					String details = JSONParserUtil.getString(json,"details");
					String membershipCode = String.valueOf(JSONParserUtil.getLong(json,"membershipCode"));
					String membershipCodeExpire = String.valueOf(JSONParserUtil.getDate(json,"memberShipCodeExpire"));
					if(code.equalsIgnoreCase("S00")) {
						resp.setSuccess(true);
							}
					resp.setCode(code);
					resp.setResponse(strResponse);
					resp.setMessage(message);
					resp.setDetails(details);
					resp.setMembershipCode(membershipCode);
					resp.setMebershipCodeExpire(membershipCodeExpire);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return resp;
	}
	
	@Override
	public TreatCardPlansResponse getListPlans(SessionDTO dto) {
		TreatCardPlansResponse resp = new TreatCardPlansResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", dto.getSessionId());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getUserTreatCardPlansUrl(Version.VERSION_1, Role.USER,Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				System.err.print(strResponse);
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						 org.json.JSONArray arr=null;
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
						resp.setJsonArray(arr);
					} 
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}
	
	
	@Override
	public TreatCardPlansResponse getUpdateTreatCard(TreatCardDTO dto) {
		TreatCardPlansResponse resp = new TreatCardPlansResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", dto.getSessionId());
			payload.put("days", dto.getDays());
			payload.put("amount", dto.getAmount());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.updateTreatCardPaymentPlansUrl(Version.VERSION_1, Role.USER,Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				System.err.print(strResponse);
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				if (strResponse != null) {
					System.err.println("response ::" + strResponse);
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						 org.json.JSONArray arr=null;
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setResponse(strResponse);
						resp.setJsonArray(arr);
					} 
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public CustDetailsResp getCustDetails(BescomRefundRequest request) {
		CustDetailsResp resp = new CustDetailsResp();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("transactionRefNo", request.getTid());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.fetchCustDetailsURL(Version.VERSION_1,
					Role.MERCHANT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
			} else {
				String strResponse = response.getEntity(String.class);
				System.err.println("Response : : " + strResponse);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
							org.json.JSONObject details = JSONParserUtil.getObject(jobj, "details");
							resp.setVpa(JSONParserUtil.getString(details, "vpa"));
							resp.setEmail(JSONParserUtil.getString(details, "email"));
							resp.setPhone(JSONParserUtil.getString(details, "mobile"));
						} else {
							resp.setSuccess(false);
						}
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
					} 
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
		}
		return resp;
	}

	@Override
	public ResponseDTO getBescomMerchantRefundReq(BescomRefundRequest dto) {
		ResponseDTO resp = new ResponseDTO();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", dto.getSessionId());
			payload.put("transactionRefNo", dto.getTid());
			payload.put("amount", dto.getAmount());
			Client client = Client.create();
			WebResource webResource = client.resource(MerchantConstants.BESCOM_MERCHANT_REFUND_REQUEST);
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setMessage("Service unavailable");
			} else{
				org.json.JSONObject json = new org.json.JSONObject(strResponse);
				if(json != null) {
					String code = JSONParserUtil.getString(json,"code");
					String message = JSONParserUtil.getString(json,"message");
					 String status=JSONParserUtil.getString(json,"status");
					if(code.equalsIgnoreCase("S00")) {
								resp.setSuccess(true);
								resp.setMessage(message);
								resp.setStatus(status);
								resp.setCode(code);
							}else{
								resp.setSuccess(false);
								resp.setMessage(message);
								resp.setStatus(status);
								resp.setCode(code);
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return resp;
	}

	@Override
	public NearBymerchantResponse findNearByMerchants(NearByMerchantsRequest request, Role role) {
		NearBymerchantResponse resp = new NearBymerchantResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("latitude", request.getLatitude());
			payload.put("longitude", request.getLongitude());
			payload.put("radius", (request.getRadius() == 0) ? "" : request.getRadius());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(
					UrlMetadatas.findNearByMerchantsUrl(Version.VERSION_1, role, Device.ANDROID, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
				resp.setStatus("FAILED");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			} else {
				String strResponse = response.getEntity(String.class);
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					final String status = (String) jobj.get("status");
					final String code = (String) jobj.get("code");
					final String message = (String) jobj.get("message");
					if (code.equalsIgnoreCase("S00")) {
						resp.setSuccess(true);
					} else {
						resp.setSuccess(false);
					}
					resp.setCode(code);
					resp.setStatus(status);
					resp.setMessage(message);
					resp.setResponse(strResponse);
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return resp;
	}

	@Override
	public NearByAgentResponse findNearByAgents(NearByAgentsRequest request, Role role) {
		NearByAgentResponse nearByAgentResponse = new NearByAgentResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("latitude", request.getLatitude());
			payload.put("longitude", request.getLongitude());
			payload.put("radius", (request.getRadius() == 0) ? "" : request.getRadius());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(
					UrlMetadatas.findNearByAgentsUrl(Version.VERSION_1, role, Device.ANDROID, Language.ENGLISH));
			ClientResponse clientResponse = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (clientResponse.getStatus() != 200) {
				logger.info("error");
			} else {
				String response = clientResponse.getEntity(String.class);
				logger.info("response "+response);
				if (response != null && !response.isEmpty()) {
					org.json.JSONObject jobj = new org.json.JSONObject(response);
					final String status = (String) jobj.get("status");
					final String code = (String) jobj.get("code");
					final String message = (String) jobj.get("message");
					if (code.equalsIgnoreCase(ResponseStatus.SUCCESS.toString())) {
						nearByAgentResponse.setSuccess(true);
					} else {
						nearByAgentResponse.setSuccess(false);
					}
					nearByAgentResponse.setCode(code);
					nearByAgentResponse.setStatus(status);
					nearByAgentResponse.setMessage(message);
					nearByAgentResponse.setResponse(response);
				} else {
					nearByAgentResponse.setSuccess(false);
					nearByAgentResponse.setCode(ResponseStatus.FAILURE.toString());
					nearByAgentResponse.setMessage("Service unavailable");
					nearByAgentResponse.setStatus("FAILED");
					nearByAgentResponse.setResponse(APIUtils.getFailedJSON().toString());
				}
				
			}
			
		} catch (Exception e) {
			logger.error(e.getMessage());
			nearByAgentResponse.setSuccess(false);
			nearByAgentResponse.setCode(ResponseStatus.FAILURE.toString());
			nearByAgentResponse.setMessage("Service unavailable");
			nearByAgentResponse.setStatus("FAILED");
			nearByAgentResponse.setResponse(APIUtils.getFailedJSON().toString());
		}
		return nearByAgentResponse;
	}

	@Override
	public ResponseDTO updateUserMaxLimit(UserMaxLimitDto request) {
		ResponseDTO responseDTO = new ResponseDTO();
		try {
			JSONObject payload = new JSONObject();
			payload.put("dailyNoOfTxn", request.getDailyNoOfTxn());
			payload.put("dailyAmountOfTxn", request.getDailyAmountOfTxn());
			payload.put("monthlyNoOfTxn", request.getMonthlyNoOfTxn());
			payload.put("monthlyAmountOfTxn", request.getMonthlyAmountOfTxn());
			payload.put("transactionType", request.getTransactionType());
			payload.put("mobileToken", request.getMobileToken());	
			payload.put("sessionId", request.getSessionId());
			
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(
					UrlMetadatas.updateUserMaxLimit(Version.VERSION_1, Role.USER, Device.ANDROID, Language.ENGLISH));
			ClientResponse clientResponse = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (clientResponse.getStatus() != 200) {
				logger.info("error");
			} else {
				String response = clientResponse.getEntity(String.class);
				JSONObject jsonObject= new JSONObject(response);
				responseDTO.setCode(jsonObject.getString("code"));
				responseDTO.setMessage(jsonObject.getString("message"));
				logger.info("response "+response);
				}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		return responseDTO;
	}

	@Override
	public ResponseDTO getUserMaxLimit(UserMaxLimitDto request) {
		ResponseDTO responseDTO = new ResponseDTO();
		try {
			JSONObject payload = new JSONObject();
			payload.put("transactionType", request.getTransactionType());
			payload.put("sessionId", request.getSessionId());
			
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(
					UrlMetadatas.getUserMaxLimit(Version.VERSION_1, Role.USER, Device.ANDROID, Language.ENGLISH));
			ClientResponse clientResponse = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (clientResponse.getStatus() != 200) {
				logger.info("error");
			} else {
				String response = clientResponse.getEntity(String.class);
				JSONObject jsonObject= new JSONObject(response);
				responseDTO.setCode(jsonObject.getString("code"));
				responseDTO.setMessage(jsonObject.getString("message"));
				responseDTO.setDetails(jsonObject.getString("details"));
				logger.info("response "+response);
				}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		return responseDTO;
	}
	@Override
	public ResponseDTO sendMaxLimitOtp(UserMaxLimitDto request) {
		ResponseDTO responseDTO = new ResponseDTO();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(
					UrlMetadatas.sendMaxLimitOtp(Version.VERSION_1, Role.USER, Device.ANDROID, Language.ENGLISH));
			ClientResponse clientResponse = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (clientResponse.getStatus() != 200) {
				logger.info("error");
			} else {
				String response = clientResponse.getEntity(String.class);
				JSONObject jsonObject= new JSONObject(response);
				responseDTO.setCode(jsonObject.getString("code"));
				responseDTO.setMessage(jsonObject.getString("message"));
				responseDTO.setDetails(jsonObject.getString("details"));
				logger.info("response "+response);
				}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		return responseDTO;
	}
	
	@Override
	public ResponseDTO validateUserPassword(LoginDTO request) {
		ResponseDTO responseDTO = new ResponseDTO();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("password", request.getPassword());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(
					UrlMetadatas.validateUserPassword(Version.VERSION_1, Role.USER, Device.ANDROID, Language.ENGLISH));
			ClientResponse clientResponse = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (clientResponse.getStatus() != 200) {
				logger.info("error");
			} else {
				String response = clientResponse.getEntity(String.class);
				JSONObject jsonObject = new JSONObject(response);
				responseDTO.setCode(jsonObject.getString("code"));
				responseDTO.setMessage(jsonObject.getString("message"));
				logger.info("response " + response);
			}
		} catch (Exception e) {
			responseDTO.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			responseDTO.setMessage(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		return responseDTO;
	}
}