package com.payqwikweb.app.api.impl;

import java.io.StringReader;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.payqwikweb.api.constants.APIConstants;
import com.payqwikweb.app.api.ILoadMoneyApi;
import com.payqwikweb.app.metadatas.UrlMetadatas;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Language;
import com.payqwikweb.app.model.RazorPayResponse;
import com.payqwikweb.app.model.ResponseStatus;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.Version;
import com.payqwikweb.app.model.request.FlightVnetRequest;
import com.payqwikweb.app.model.request.LoadMoneyRequest;
import com.payqwikweb.app.model.request.RazorPayRequest;
import com.payqwikweb.app.model.request.SessionDTO;
import com.payqwikweb.app.model.request.VNetRequest;
import com.payqwikweb.app.model.response.EBSRedirectResponse;
import com.payqwikweb.app.model.response.EBSStatusResponseDTO;
import com.payqwikweb.app.model.response.LoadMoneyResponse;
import com.payqwikweb.app.model.response.TransactionReportResponse;
import com.payqwikweb.app.model.response.VNetResponse;
import com.payqwikweb.app.model.response.VRedirectResponse;
import com.payqwikweb.app.utils.SecurityUtils;
import com.payqwikweb.model.app.request.LoadMoneyFlightRequest;
import com.payqwikweb.model.app.request.TravellerFlightDetails;
import com.payqwikweb.model.app.response.VNetStatusResponse;
import com.payqwikweb.model.web.RefundStatusDTO;
import com.payqwikweb.model.web.Status;
import com.payqwikweb.util.JSONParserUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import com.thirdparty.model.ResponseDTO;
import com.thirdparty.model.StatusResponse;
import com.thirdparty.model.UpiSdkResponseDTO;
import com.upi.model.requet.UPIRedirect;
import com.upi.model.requet.UpiMobileRedirectRequest;
import com.upi.util.UPIConstants;

public class LoadMoneyApi implements ILoadMoneyApi {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public LoadMoneyResponse loadMoneyRequest(LoadMoneyRequest request) {
		LoadMoneyResponse resp = new LoadMoneyResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("channel", request.getChannel());
			payload.put("account_id", request.getAccountId());
			payload.put("reference_no", request.getReferenceNo());
			payload.put("amount", request.getAmount());
			payload.put("mode", request.getMode());
			payload.put("currency", request.getCurrency());
			payload.put("description", request.getDescription());
			payload.put("return_url", request.getReturnUrl());
			payload.put("name", request.getName());
			payload.put("address", request.getAddress());
			payload.put("city", request.getCity());
			payload.put("state", request.getState());
			payload.put("country", request.getCountry());
			payload.put("postal_code", request.getPostalCode());
			payload.put("phone", request.getPhone());
			payload.put("email", request.getEmail());
			payload.put("ship_name", request.getShipName());
			payload.put("ship_address", request.getShipAddress());
			payload.put("ship_city", request.getShipCity());
			payload.put("ship_state", request.getShipState());
			payload.put("ship_country", request.getShipCountry());
			payload.put("ship_postal_code", request.getShipPostalCode());
			payload.put("ship_phone", request.getShipPhone());
			payload.put("payment_mode", request.getPaymentMode());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(
					UrlMetadatas.getLoadMoneyUrl(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				String strResponse = response.getEntity(String.class);
				org.json.JSONObject jObj = new org.json.JSONObject(strResponse);
				final String code = JSONParserUtil.getString(jObj, "code");
				if (code.equalsIgnoreCase("S00")) {
					org.json.JSONObject details = JSONParserUtil.getObject(jObj, "details");
					if (details != null) {
						/*resp.setAccountId(JSONParserUtil.getString(details, "account_id"));
						resp.setAddress(JSONParserUtil.getString(details, "address"));
						resp.setAmount(JSONParserUtil.getString(details, "amount"));
						resp.setChannel(JSONParserUtil.getString(details, "channel"));
						resp.setCity(JSONParserUtil.getString(details, "city"));
						resp.setCountry(JSONParserUtil.getString(details, "country"));
						resp.setCurrency(JSONParserUtil.getString(details, "currency"));
						resp.setDescription(JSONParserUtil.getString(details, "description"));
						resp.setEmail(JSONParserUtil.getString(details, "email"));
						resp.setMode(JSONParserUtil.getString(details, "mode"));
						resp.setName(JSONParserUtil.getString(details, "name"));
						resp.setPhone(JSONParserUtil.getString(details, "phone"));
						resp.setPostalCode(JSONParserUtil.getString(details, "postal_code"));
						resp.setReferenceNo(JSONParserUtil.getString(details, "reference_no"));
						resp.setReturnUrl(JSONParserUtil.getString(details, "return_url"));
						resp.setSecureHash(JSONParserUtil.getString(details, "secure_hash"));
						resp.setShipAddress(JSONParserUtil.getString(details, "ship_address"));
						resp.setShipCity(JSONParserUtil.getString(details, "ship_city"));
						resp.setShipCountry(JSONParserUtil.getString(details, "ship_country"));
						resp.setShipName(JSONParserUtil.getString(details, "ship_name"));
						resp.setShipPhone(JSONParserUtil.getString(details, "ship_phone"));
						resp.setShipPostalCode(JSONParserUtil.getString(details, "ship_postal_code"));
						resp.setShipState(JSONParserUtil.getString(details, "ship_state"));
						resp.setState(JSONParserUtil.getString(details, "state"));*/
						resp.setSuccess(true);
					} else {
						resp.setSuccess(false);
					}
				} else if (code.equalsIgnoreCase("F00")) {
					org.json.JSONObject failureDetails = JSONParserUtil.getObject(jObj, "details");
					if (failureDetails != null) {
					//	resp.setDescription(JSONParserUtil.getString(failureDetails, "message"));
					}
				}
				 else if (code.equalsIgnoreCase("F04")) {
						org.json.JSONObject failureDetails = JSONParserUtil.getObject(jObj, "details");
						if (failureDetails != null) {
					//		resp.setDescription(JSONParserUtil.getString(failureDetails, "message"));
						}
					}else {
					resp.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
		}
		return resp;

	}
	
	@Override
	public ResponseDTO loadMoneyUpiFailedRequest(LoadMoneyRequest request) {
		ResponseDTO resp = new ResponseDTO();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("transactionRefNo", request.getReferenceNo());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(
					UrlMetadatas.getLoadMoneyFailedUrl(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				String strResponse = response.getEntity(String.class);
				org.json.JSONObject jObj = new org.json.JSONObject(strResponse);
				final String code = JSONParserUtil.getString(jObj, "code");
				final String message = JSONParserUtil.getString(jObj, "message");
				if (code.equalsIgnoreCase("S00")) {
					resp.setSuccess(true);
					resp.setMessage(message);
				} else if (code.equalsIgnoreCase("F00")) {
					resp.setSuccess(false);
					resp.setMessage(message);
				} else {
					resp.setSuccess(false);
					resp.setMessage(message);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
		}
		return resp;
	}

	@Override
	public EBSRedirectResponse processRedirectResponse(HttpServletRequest request) {
		MultivaluedMapImpl formData = new MultivaluedMapImpl();
		EBSRedirectResponse redirectResponse = new EBSRedirectResponse();
		Enumeration paramNames = request.getParameterNames();
		while (paramNames.hasMoreElements()) {
			String paramName = (String) paramNames.nextElement();
			String paramValue = request.getParameter(paramName);
			if (paramName.equalsIgnoreCase("responseCode") && paramValue.equalsIgnoreCase("0")) {
				redirectResponse.setSuccess(true);
			} else if (paramName.equalsIgnoreCase("responseCode") && paramValue.equalsIgnoreCase("1")) {
				redirectResponse.setSuccess(false);
			}
			formData.add(paramName, paramValue);
		}
		try {
			Client client = Client.create();
			WebResource resource = client.resource(UrlMetadatas.getLoadMoneyResponseUrl(Version.VERSION_1, Role.USER,
					Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = resource.accept("application/json").post(ClientResponse.class, formData);
			String strResponse = response.getEntity(String.class);
			System.err.println("string response ::" + strResponse);
			if (response.getStatus() == 200) {
				org.json.JSONObject jObj = new org.json.JSONObject(strResponse);
				final String code = JSONParserUtil.getString(jObj, "code");
				if (code.equalsIgnoreCase("S00")) {
					redirectResponse.setSuccess(true);
					org.json.JSONObject details = JSONParserUtil.getObject(jObj, "details");
					redirectResponse.setDetails(String.valueOf(details));
				} else {
					redirectResponse.setResponseCode("1");
					redirectResponse.setSuccess(false);
				}
			} else {
				redirectResponse.setResponseCode("1");
				redirectResponse.setSuccess(false);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return redirectResponse;
	}

	@Override
	public VNetResponse initiateVnetBanking(VNetRequest request) {
		VNetResponse resp = new VNetResponse();
		try {
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(
					UrlMetadatas.getLoadMoneyVNetUrl(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(request.toJSON().toString()))
					.post(ClientResponse.class, request.toJSON());
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				String strResponse = response.getEntity(String.class);
				org.json.JSONObject jObj = new org.json.JSONObject(strResponse);
				final String code = JSONParserUtil.getString(jObj, "code");
				if (code.equalsIgnoreCase("S00")) {
					org.json.JSONObject details = JSONParserUtil.getObject(jObj, "details");
					if (details != null) {
						resp.setMerchantName(JSONParserUtil.getString(details, "merchantName"));
						resp.setReturnUrl(JSONParserUtil.getString(details, "returnURL"));
						resp.setItc(JSONParserUtil.getString(details, "itc"));
						resp.setAmount(JSONParserUtil.getString(details, "amount"));
						resp.setCrn(JSONParserUtil.getString(details, "crnNo"));
						resp.setPrn(JSONParserUtil.getString(details, "prnNo"));
						resp.setPid(JSONParserUtil.getString(details, "pid"));
						resp.setMid(JSONParserUtil.getString(details, "mid"));
						resp.setSuccess(true);
					} else {
						resp.setSuccess(false);
					}
				} else if (code.equalsIgnoreCase("F00")) {
					org.json.JSONObject failureDetails = JSONParserUtil.getObject(jObj, "details");
					if (failureDetails != null) {
						resp.setMessage(JSONParserUtil.getString(failureDetails, "message"));
					}
				} else {
					resp.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
		}
		return resp;

	}
	
	
	@Override
	public VNetResponse initiateVnetBankingFlight(FlightVnetRequest request) {
		VNetResponse resp = new VNetResponse();
		try {
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(
					UrlMetadatas.getLoadMoneyVNetFlightUrl(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(request.toJSON().toString()))
					.post(ClientResponse.class, request.toJSON());
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				String strResponse = response.getEntity(String.class);
				org.json.JSONObject jObj = new org.json.JSONObject(strResponse);
				final String code = JSONParserUtil.getString(jObj, "code");
				if (code.equalsIgnoreCase("S00")) {
					org.json.JSONObject details = JSONParserUtil.getObject(jObj, "details");
					if (details != null) {
						resp.setMerchantName(JSONParserUtil.getString(details, "merchantName"));
						resp.setReturnUrl(JSONParserUtil.getString(details, "returnURL"));
						resp.setItc(JSONParserUtil.getString(details, "itc"));
						resp.setAmount(JSONParserUtil.getString(details, "amount"));
						resp.setCrn(JSONParserUtil.getString(details, "crnNo"));
						resp.setPrn(JSONParserUtil.getString(details, "prnNo"));
						resp.setPid(JSONParserUtil.getString(details, "pid"));
						resp.setMid(JSONParserUtil.getString(details, "mid"));
						resp.setSuccess(true);
						resp.setTransactionRefNo(JSONParserUtil.getString(details, "transactionRefNo"));
					} else {
						resp.setSuccess(false);
					}
				} else if (code.equalsIgnoreCase("F00")) {
					org.json.JSONObject failureDetails = JSONParserUtil.getObject(jObj, "details");
					if (failureDetails != null) {
						resp.setMessage(JSONParserUtil.getString(failureDetails, "message"));
					}
				} else {
					resp.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
		}
		return resp;

	}
	@Override
	public VNetResponse agentInitiateVnetBankingFlight(FlightVnetRequest request) {
		VNetResponse resp = new VNetResponse();
		try {
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(
					UrlMetadatas.agentGetLoadMoneyVNetFlightUrl(Version.VERSION_1, Role.AGENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(request.toJSON().toString()))
					.post(ClientResponse.class, request.toJSON());
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				String strResponse = response.getEntity(String.class);
				org.json.JSONObject jObj = new org.json.JSONObject(strResponse);
				final String code = JSONParserUtil.getString(jObj, "code");
				if (code.equalsIgnoreCase("S00")) {
					org.json.JSONObject details = JSONParserUtil.getObject(jObj, "details");
					if (details != null) {
						resp.setMerchantName(JSONParserUtil.getString(details, "merchantName"));
						resp.setReturnUrl(JSONParserUtil.getString(details, "returnURL"));
						resp.setItc(JSONParserUtil.getString(details, "itc"));
						resp.setAmount(JSONParserUtil.getString(details, "amount"));
						resp.setCrn(JSONParserUtil.getString(details, "crnNo"));
						resp.setPrn(JSONParserUtil.getString(details, "prnNo"));
						resp.setPid(JSONParserUtil.getString(details, "pid"));
						resp.setMid(JSONParserUtil.getString(details, "mid"));
						resp.setSuccess(true);
						resp.setTransactionRefNo(JSONParserUtil.getString(details, "transactionRefNo"));
					} else {
						resp.setSuccess(false);
					}
				} else if (code.equalsIgnoreCase("F00")) {
					org.json.JSONObject failureDetails = JSONParserUtil.getObject(jObj, "details");
					if (failureDetails != null) {
						resp.setMessage(JSONParserUtil.getString(failureDetails, "message"));
					}
				} else {
					resp.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
		}
		return resp;

	}

	@Override
	public ResponseDTO handleRedirectRequest(VRedirectResponse dto) {
		ResponseDTO result = new ResponseDTO();
		try {
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(UrlMetadatas.getLoadMoneyVNetResponseUrl(Version.VERSION_1,
					Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(dto.toJSON().toString()))
					.post(ClientResponse.class, dto.toJSON());
			if (response.getStatus() != 200) {
				result.setSuccess(false);
			} else {
				String strResponse = response.getEntity(String.class);
				org.json.JSONObject jObj = new org.json.JSONObject(strResponse);
				final String code = JSONParserUtil.getString(jObj, "code");
				if (code.equalsIgnoreCase("S00")) {
					result.setSuccess(true);
					result.setMessage(JSONParserUtil.getString(jObj, "message"));
					result.setCode(JSONParserUtil.getString(jObj, "code"));
				} else {
					result.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
		}
		return result;
	}
	
	@Override
	public ResponseDTO handleRedirectRequestFlight(VRedirectResponse dto) {
		ResponseDTO result = new ResponseDTO();
		try {
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(UrlMetadatas.getLoadMoneyVNetResponseFlightUrl(Version.VERSION_1,
					Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(dto.toJSON().toString()))
					.post(ClientResponse.class, dto.toJSON());
			if (response.getStatus() != 200) {
				result.setSuccess(false);
			} else {
				String strResponse = response.getEntity(String.class);
				org.json.JSONObject jObj = new org.json.JSONObject(strResponse);
				final String code = JSONParserUtil.getString(jObj, "code");
				if (code.equalsIgnoreCase("S00")) {
					result.setSuccess(true);
					result.setMessage(JSONParserUtil.getString(jObj, "message"));
					result.setCode(JSONParserUtil.getString(jObj, "code"));
					result.setTransactionId(JSONParserUtil.getString(jObj, "txnId"));
				} else {
					result.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
		}
		return result;
	}
	@Override
	public ResponseDTO agentHandleRedirectRequestFlight(VRedirectResponse dto) {
		ResponseDTO result = new ResponseDTO();
		try {
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(UrlMetadatas.agentGetLoadMoneyVNetResponseFlightUrl(Version.VERSION_1,
					Role.AGENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(dto.toJSON().toString()))
					.post(ClientResponse.class, dto.toJSON());
			if (response.getStatus() != 200) {
				result.setSuccess(false);
			} else {
				String strResponse = response.getEntity(String.class);
				org.json.JSONObject jObj = new org.json.JSONObject(strResponse);
				final String code = JSONParserUtil.getString(jObj, "code");
				if (code.equalsIgnoreCase("S00")) {
					result.setSuccess(true);
					result.setMessage(JSONParserUtil.getString(jObj, "message"));
					result.setCode(JSONParserUtil.getString(jObj, "code"));
					result.setTransactionId(JSONParserUtil.getString(jObj, "txnId"));
				} else {
					result.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
		}
		return result;
	}

	@Override
	public ResponseDTO verifyEBSTransaction(EBSRedirectResponse resp) {
		final ResponseDTO result = new ResponseDTO();
		try {
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(com.payqwikweb.api.constants.APIConstants.EBS_VERIFICATION);
			MultivaluedMapImpl formData = new MultivaluedMapImpl();
			formData.add("Action", "statusByRef");
			formData.add("AccountID", "20696");
			formData.add("SecretKey", "6496e4db9ebf824ffe2269afee259447");
			formData.add("RefNo", resp.getMerchantRefNo());
			ClientResponse response = webResource.post(ClientResponse.class, formData);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				result.setSuccess(false);
			} else {
				SAXParserFactory factory = SAXParserFactory.newInstance();
				SAXParser saxParser = factory.newSAXParser();
				DefaultHandler handler = new DefaultHandler() {
					boolean output = false;

					public void startElement(String uri, String localName, String qName, Attributes attributes)
							throws SAXException {
						if (qName.equalsIgnoreCase("output")) {
							output = true;
						}
						if (output) {
							String status = attributes.getValue("status");
							String error = attributes.getValue("error");
							String transactionType = attributes.getValue("transactionType");
							if (error == null) {
								if (transactionType.equalsIgnoreCase("Authorized")) {
									if (status.equalsIgnoreCase("Processed")) {
										result.setSuccess(true);
										result.setCode("S00");
									}
								} else {
									result.setSuccess(false);
									result.setCode("F00");
									result.setMessage("Transaction is " + transactionType);
								}
							} else {
								result.setSuccess(false);
								result.setCode("F00");
								result.setMessage(error);
							}
						}
					}
				};
				saxParser.parse(new InputSource(new StringReader(strResponse)), handler);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
		}
		return result;
	}
	
	@Override
	public ResponseDTO verifyEBSTransactionForRecon(EBSRedirectResponse resp) {
		final ResponseDTO result = new ResponseDTO();
		try {
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(com.payqwikweb.api.constants.APIConstants.EBS_VERIFICATION);
			MultivaluedMapImpl formData = new MultivaluedMapImpl();
			formData.add("Action", "statusByRef");
			formData.add("AccountID", "20696");
			formData.add("SecretKey", "6496e4db9ebf824ffe2269afee259447");
			formData.add("RefNo", resp.getMerchantRefNo());
			ClientResponse response = webResource.post(ClientResponse.class, formData);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				result.setSuccess(false);
			} else {
				SAXParserFactory factory = SAXParserFactory.newInstance();
				SAXParser saxParser = factory.newSAXParser();
				DefaultHandler handler = new DefaultHandler() {
					boolean output = false;

					public void startElement(String uri, String localName, String qName, Attributes attributes)
							throws SAXException {
						if (qName.equalsIgnoreCase("output")) {
							output = true;
						}
						if (output) {
							String status = attributes.getValue("status");
							String paymentId = attributes.getValue("paymentId");
							String dateTime = attributes.getValue("dateTime");
							String error = attributes.getValue("error");
							String transactionType = attributes.getValue("transactionType");
							if (error == null) {
								if (transactionType.equalsIgnoreCase("Authorized")) {
									if (status.equalsIgnoreCase("Processed")) {
										result.setSuccess(true);
										result.setEbsStatus(status);
										result.setCode("S00");
										result.setPaymentId(paymentId);
										result.setEbsDate(dateTime);
										result.setValid(true);
									}
								} else  {
									if(transactionType.equalsIgnoreCase("AuthFailed")){
										result.setEbsStatus("Failed");
									}else{
										result.setEbsStatus(transactionType);
									}
									result.setValid(true);
									result.setPaymentId(paymentId);
									result.setEbsDate(dateTime);
									result.setSuccess(false);
									result.setCode("F00");
									result.setMessage("Transaction is " + transactionType);
								}
							} else {
								result.setValid(false);
								result.setSuccess(false);
								result.setCode("F00");
								result.setMessage(error);
							}
						}
					}
				};
				saxParser.parse(new InputSource(new StringReader(strResponse)), handler);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
		}
		return result;
	}

	@Override
	public EBSRedirectResponse processRedirectSDK(EBSRedirectResponse redirectResponse) {
		try {
			MultivaluedMapImpl formData = new MultivaluedMapImpl();
			formData.add("responseCode", redirectResponse.getResponseCode());
			formData.add("merchantRefNo", redirectResponse.getMerchantRefNo());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(UrlMetadatas.getLoadMoneyResponseUrl(Version.VERSION_1, Role.USER,
					Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = resource.accept("application/json").post(ClientResponse.class, formData);
			String strResponse = response.getEntity(String.class);
			System.err.println(strResponse);
			if (response.getStatus() == 200) {
				org.json.JSONObject jObj = new org.json.JSONObject(strResponse);
				final String code = JSONParserUtil.getString(jObj, "code");
				final String message = JSONParserUtil.getString(jObj, "message");
				if (code.equalsIgnoreCase("S00")) {
					redirectResponse.setSuccess(true);
					org.json.JSONObject details = JSONParserUtil.getObject(jObj, "details");
					redirectResponse.setDetails(String.valueOf(details));
					redirectResponse.setSessionId(JSONParserUtil.getString(jObj, "sessionId"));
				} else {
					redirectResponse.setResponseCode("1");
					redirectResponse.setCode(code);
					redirectResponse.setSuccess(false);
					redirectResponse.setMessage(message);
				}
			} else {
				redirectResponse.setResponseCode("1");
				redirectResponse.setSuccess(false);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return redirectResponse;
	}
	@Override
	public EBSRedirectResponse agentProcessRedirectSDK(EBSRedirectResponse redirectResponse) {
		try {
			MultivaluedMapImpl formData = new MultivaluedMapImpl();
			formData.add("responseCode", redirectResponse.getResponseCode());
			formData.add("merchantRefNo", redirectResponse.getMerchantRefNo());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(UrlMetadatas.getLoadMoneyResponseUrl(Version.VERSION_1, Role.AGENT,
					Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = resource.accept("application/json").post(ClientResponse.class, formData);
			String strResponse = response.getEntity(String.class);
			System.err.println(strResponse);
			if (response.getStatus() == 200) {
				org.json.JSONObject jObj = new org.json.JSONObject(strResponse);
				final String code = JSONParserUtil.getString(jObj, "code");
				if (code.equalsIgnoreCase("S00")) {
					redirectResponse.setSuccess(true);
					org.json.JSONObject details = JSONParserUtil.getObject(jObj, "details");
					redirectResponse.setDetails(String.valueOf(details));
				} else {
					redirectResponse.setResponseCode("1");
					redirectResponse.setSuccess(false);
				}
			} else {
				redirectResponse.setResponseCode("1");
				redirectResponse.setSuccess(false);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return redirectResponse;
	}

	// in test phase
	/*
	 * @Override public ResponseDTO refundEBSTransaction(EBSRedirectResponse
	 * resp) { ResponseDTO result = new ResponseDTO(); try { Client client =
	 * Client.create(); client.addFilter(new LoggingFilter(System.out));
	 * WebResource webResource =
	 * client.resource(com.payqwikweb.api.constants.APIConstants.URL.EBS_STATUS)
	 * ;
	 * 
	 * MultivaluedMapImpl formData = new MultivaluedMapImpl();
	 * formData.add("Action","status"); formData.add("AccountID","20696");
	 * formData.add("SecretKey","6496e4db9ebf824ffe2269afee259447");
	 * formData.add("RefNo",resp.getMerchantRefNo()); ClientResponse response =
	 * webResource.post(ClientResponse.class, formData); String strResponse =
	 * response.getEntity(String.class); if (response.getStatus() != 200) {
	 * logger.info("RESPONSE  :: " + strResponse); result.setSuccess(false); }
	 * else { logger.info("RESPONSE  :: " + strResponse); SAXParserFactory
	 * factory = SAXParserFactory.newInstance(); SAXParser saxParser =
	 * factory.newSAXParser(); DefaultHandler handler = new DefaultHandler(){
	 * 
	 * boolean output = false;
	 * 
	 * public void startElement(String uri, String localName,String qName,
	 * Attributes attributes) throws SAXException {
	 * 
	 * System.out.println("Start Element :" + qName);
	 * 
	 * if (qName.equalsIgnoreCase("output")) { output = true; } if(output){
	 * System.out.println(attributes.getValue("paymentId"));
	 * System.out.println(attributes.getValue("status")); String status =
	 * attributes.getValue("status"); String error =
	 * attributes.getValue("error"); String
	 * tranxId=attributes.getValue("referenceNo"); String
	 * description=attributes.getValue("description"); String transactionType =
	 * attributes.getValue("transactionType"); if(error == null) {
	 * if(status.equalsIgnoreCase("Captured") &&
	 * status.equalsIgnoreCase("Authorized")) { result.setSuccess(true);
	 * result.setCode("S00"); } } else { result.setSuccess(false);
	 * result.setCode("F00"); result.setMessage("Transaction is " +
	 * transactionType); } }else { result.setSuccess(false);
	 * result.setCode("F00"); //result.setMessage(error); } }
	 * 
	 * };
	 * 
	 * saxParser.parse(new InputSource(new StringReader(strResponse)),handler);
	 * 
	 * 
	 * 
	 * logger.info("Load money response ::" + strResponse); } } catch (Exception
	 * e) { e.printStackTrace(); result.setSuccess(false); }
	 * 
	 * return result; }
	 */
	@Override
	public EBSStatusResponseDTO crossCheckEBSStatus(String referenceNo)
			throws ParserConfigurationException, SAXException {
		EBSStatusResponseDTO statusResponse = new EBSStatusResponseDTO();
		try {
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(com.payqwikweb.api.constants.APIConstants.EBS_STATUS);
			MultivaluedMapImpl formData = new MultivaluedMapImpl();
			formData.add("Action", "status");
			formData.add("AccountID", "20696");
			formData.add("SecretKey", "6496e4db9ebf824ffe2269afee259447");
			formData.add("RefNo", referenceNo);
			ClientResponse response = webResource.post(ClientResponse.class, formData);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				statusResponse.setSuccess(false);
			} else {
				SAXParserFactory factory = SAXParserFactory.newInstance();
				SAXParser saxParser = factory.newSAXParser();
				DefaultHandler handler = new DefaultHandler() {
					boolean output = false;

					public void startElement(String uri, String localName, String qName, Attributes attributes)
							throws SAXException {

						if (qName.equalsIgnoreCase("output")) {
							output = true;
						}
						if (output) {
							String status = attributes.getValue("status");
							String error = attributes.getValue("error");
							String errorCode = attributes.getValue("errorCode");
							if (error == null) {
								double amount = Double.parseDouble(attributes.getValue("amount"));
								String tranxId = attributes.getValue("referenceNo");
								String description = attributes.getValue("description");
								String dateCreated = attributes.getValue("dateCreated");
								if (status.equalsIgnoreCase("Captured") || status.equalsIgnoreCase("Incompleted")
										|| status.equalsIgnoreCase("AuthFailed")) {
									statusResponse.setSuccess(true);
									statusResponse.setCreated(dateCreated);
									statusResponse.setTransactionRefNo(tranxId);
									statusResponse.setAmount(amount);
									statusResponse.setStatus(status);
									statusResponse.setDescription(description);
									statusResponse.setCode("S00");
								}
							} else if (error != null && errorCode.equalsIgnoreCase("4")) {
								statusResponse.setErrorMessage(error);
								statusResponse.setErrorCode(errorCode);
							}
						}
					}

				};
				saxParser.parse(new InputSource(new StringReader(strResponse)), handler);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return statusResponse;
	}

	@Override
	public TransactionReportResponse getRefundStatus(RefundStatusDTO request) {
		TransactionReportResponse resp = new TransactionReportResponse();

		try { 
			JSONObject payload = new JSONObject();
			
			payload.put("from", request.getFrom());
			payload.put("to", request.getTo());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(
					UrlMetadatas.refundStatusURl(Version.VERSION_1, Role.USER, Device.ANDROID, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						// final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						final org.json.JSONArray details = jobj.getJSONArray("details");

						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(true);
						} else {
							resp.setSuccess(false);
						}

						resp.setCode(code);
						// resp.setStatus(status);
						resp.setMessage(message);
						resp.setJsonArray(details);
					} else {
						resp.setSuccess(false);
						resp.setCode("F00");
						resp.setMessage("Service unavailable");
						resp.setStatus("FAILED");
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
		}
		return resp;
	}

	@Override
	public ResponseDTO getTransactionTym(SessionDTO request) {
		ResponseDTO resp = new ResponseDTO();

		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.getTransactionTimeDiff(Version.VERSION_1, Role.USER,
					Device.ANDROID, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						// final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						final boolean valid = jobj.getBoolean("valid");

						if (code.equalsIgnoreCase("S00")) {
							resp.setSuccess(valid);
						} else {
							resp.setSuccess(valid);
						}

						resp.setCode(code);
						// resp.setStatus(status);
						resp.setMessage(message);
					} else {
						resp.setSuccess(false);
						resp.setCode("F00");
						resp.setMessage("Service unavailable");
						resp.setStatus("FAILED");
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
		}
		return resp;
	}

	@Override
	public LoadMoneyResponse SplitpaymentMoneyRequest(LoadMoneyFlightRequest request) {
		// TODO Auto-generated method stub
		LoadMoneyResponse resp = new LoadMoneyResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("channel", request.getChannel());
			payload.put("account_id", request.getAccountId());
			payload.put("reference_no", request.getReferenceNo());
			payload.put("amount", request.getAmount());
			payload.put("mode", request.getMode());
			payload.put("currency", request.getCurrency());
			payload.put("description", request.getDescription());
			payload.put("return_url", request.getReturnUrl());
			payload.put("name", request.getName());
			payload.put("address", request.getAddress());
			payload.put("city", request.getCity());
			payload.put("state", request.getState());
			payload.put("country", request.getCountry());
			payload.put("postal_code", request.getPostalCode());
			payload.put("phone", request.getPhone());
			payload.put("email", request.getEmail());
			payload.put("ship_name", request.getShipName());
			payload.put("ship_address", request.getShipAddress());
			payload.put("ship_city", request.getShipCity());
			payload.put("ship_state", request.getShipState());
			payload.put("ship_country", request.getShipCountry());
			payload.put("ship_postal_code", request.getShipPostalCode());
			payload.put("ship_phone", request.getShipPhone());
			payload.put("payment_mode", request.getPaymentMode());
			payload.put("baseFare", request.getBaseFare());
			
			payload.put("ticketDetails", request.getTicketDetails());
			List<TravellerFlightDetails> tdetails=request.getTravellerDetails();
			
			JSONArray travellersDetails=new JSONArray();
			if (request.getTravellerDetails()!=null) {
			
			for (int j = 0; j < request.getTravellerDetails().size(); j++) {
				JSONObject travellersDetail=new JSONObject();
				travellersDetail.put("fName",(tdetails.get(j).getfName()== null) ? "" :tdetails.get(j).getfName());
				travellersDetail.put("lName",(tdetails.get(j).getlName()== null) ? "" :tdetails.get(j).getlName());
				travellersDetail.put("age",(tdetails.get(j).getAge()== null) ? "" :tdetails.get(j).getAge());
				travellersDetail.put("gender",(tdetails.get(j).getGender()== null) ? "" :tdetails.get(j).getGender());
				travellersDetail.put("fare",(tdetails.get(j).getFare()== null) ? "" :tdetails.get(j).getFare());
				travellersDetail.put("travellerType",(tdetails.get(j).getType()== null) ? "" :tdetails.get(j).getType());
				travellersDetail.put("ticketNo",(tdetails.get(j).getTicketNo()== null) ? "" :tdetails.get(j).getTicketNo());
				travellersDetails.put(travellersDetail);
			}
			
			payload.put("travellerDetails", travellersDetails);
			}
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(UrlMetadatas.getSplitpaymentUrlFlight(Version.VERSION_1,
					Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				String strResponse = response.getEntity(String.class);
				org.json.JSONObject jObj = new org.json.JSONObject(strResponse);
				final String code = JSONParserUtil.getString(jObj, "code");
				if (code.equalsIgnoreCase("S00")) {
					org.json.JSONObject details = JSONParserUtil.getObject(jObj, "details");
					if (details != null) {
						/*resp.setAccountId(JSONParserUtil.getString(details, "account_id"));
						resp.setAddress(JSONParserUtil.getString(details, "address"));
						resp.setAmount(JSONParserUtil.getString(details, "amount"));
						resp.setChannel(JSONParserUtil.getString(details, "channel"));
						resp.setCity(JSONParserUtil.getString(details, "city"));
						resp.setCountry(JSONParserUtil.getString(details, "country"));
						resp.setCurrency(JSONParserUtil.getString(details, "currency"));
						resp.setDescription(JSONParserUtil.getString(details, "description"));
						resp.setEmail(JSONParserUtil.getString(details, "email"));
						resp.setMode(JSONParserUtil.getString(details, "mode"));
						resp.setName(JSONParserUtil.getString(details, "name"));
						resp.setPhone(JSONParserUtil.getString(details, "phone"));
						resp.setPostalCode(JSONParserUtil.getString(details, "postal_code"));
						resp.setReferenceNo(JSONParserUtil.getString(details, "reference_no"));
						resp.setReturnUrl(JSONParserUtil.getString(details, "return_url"));
						resp.setSecureHash(JSONParserUtil.getString(details, "secure_hash"));
						resp.setShipAddress(JSONParserUtil.getString(details, "ship_address"));
						resp.setShipCity(JSONParserUtil.getString(details, "ship_city"));
						resp.setShipCountry(JSONParserUtil.getString(details, "ship_country"));
						resp.setShipName(JSONParserUtil.getString(details, "ship_name"));
						resp.setShipPhone(JSONParserUtil.getString(details, "ship_phone"));
						resp.setShipPostalCode(JSONParserUtil.getString(details, "ship_postal_code"));
						resp.setShipState(JSONParserUtil.getString(details, "ship_state"));
						resp.setState(JSONParserUtil.getString(details, "state"));*/
						resp.setSuccess(true);
					} else {
						resp.setSuccess(false);
					}
				} else if (code.equalsIgnoreCase("F00")) {
					org.json.JSONObject failureDetails = JSONParserUtil.getObject(jObj, "details");
					if (failureDetails != null) {
					//	resp.setDescription(JSONParserUtil.getString(failureDetails, "message"));
					}
				} else if (code.equalsIgnoreCase("F03")){
					resp.setSuccess(false);
					resp.setMessage(JSONParserUtil.getString(jObj, "message"));
					resp.setStatus(JSONParserUtil.getString(jObj, "status"));
				}else{
					resp.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
		}
		return resp;

	}

	@Override
	public LoadMoneyResponse AgentSplitpaymentMoneyRequest(LoadMoneyFlightRequest request) {
		// TODO Auto-generated method stub
		LoadMoneyResponse resp = new LoadMoneyResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("channel", request.getChannel());
			payload.put("account_id", request.getAccountId());
			payload.put("reference_no", request.getReferenceNo());
			payload.put("amount", request.getAmount());
			payload.put("mode", request.getMode());
			payload.put("currency", request.getCurrency());
			payload.put("description", request.getDescription());
			payload.put("return_url", request.getReturnUrl());
			payload.put("name", request.getName());
			payload.put("address", request.getAddress());
			payload.put("city", request.getCity());
			payload.put("state", request.getState());
			payload.put("country", request.getCountry());
			payload.put("postal_code", request.getPostalCode());
			payload.put("phone", request.getPhone());
			payload.put("email", request.getEmail());
			payload.put("ship_name", request.getShipName());
			payload.put("ship_address", request.getShipAddress());
			payload.put("ship_city", request.getShipCity());
			payload.put("ship_state", request.getShipState());
			payload.put("ship_country", request.getShipCountry());
			payload.put("ship_postal_code", request.getShipPostalCode());
			payload.put("ship_phone", request.getShipPhone());
			payload.put("payment_mode", request.getPaymentMode());
			
			payload.put("ticketDetails", request.getTicketDetails());
			List<TravellerFlightDetails> tdetails=request.getTravellerDetails();
			
			JSONArray travellersDetails=new JSONArray();
			if (request.getTravellerDetails()!=null) {
			
			for (int j = 0; j < request.getTravellerDetails().size(); j++) {
				JSONObject travellersDetail=new JSONObject();
				travellersDetail.put("fName",(tdetails.get(j).getfName()== null) ? "" :tdetails.get(j).getfName());
				travellersDetail.put("lName",(tdetails.get(j).getlName()== null) ? "" :tdetails.get(j).getlName());
				travellersDetail.put("age",(tdetails.get(j).getAge()== null) ? "" :tdetails.get(j).getAge());
				travellersDetail.put("gender",(tdetails.get(j).getGender()== null) ? "" :tdetails.get(j).getGender());
				travellersDetail.put("fare",(tdetails.get(j).getFare()== null) ? "" :tdetails.get(j).getFare());
				travellersDetail.put("travellerType",(tdetails.get(j).getType()== null) ? "" :tdetails.get(j).getType());
				travellersDetail.put("ticketNo",(tdetails.get(j).getTicketNo()== null) ? "" :tdetails.get(j).getTicketNo());
				travellersDetails.put(travellersDetail);
			}
			
			payload.put("travellerDetails", travellersDetails);
			}
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(UrlMetadatas.agentGetSplitpaymentUrlFlight(Version.VERSION_1,
					Role.AGENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				String strResponse = response.getEntity(String.class);
				org.json.JSONObject jObj = new org.json.JSONObject(strResponse);
				final String code = JSONParserUtil.getString(jObj, "code");
				if (code.equalsIgnoreCase("S00")) {
					org.json.JSONObject details = JSONParserUtil.getObject(jObj, "details");
					if (details != null) {
						/*resp.setAccountId(JSONParserUtil.getString(details, "account_id"));
						resp.setAddress(JSONParserUtil.getString(details, "address"));
						resp.setAmount(JSONParserUtil.getString(details, "amount"));
						resp.setChannel(JSONParserUtil.getString(details, "channel"));
						resp.setCity(JSONParserUtil.getString(details, "city"));
						resp.setCountry(JSONParserUtil.getString(details, "country"));
						resp.setCurrency(JSONParserUtil.getString(details, "currency"));
						resp.setDescription(JSONParserUtil.getString(details, "description"));
						resp.setEmail(JSONParserUtil.getString(details, "email"));
						resp.setMode(JSONParserUtil.getString(details, "mode"));
						resp.setName(JSONParserUtil.getString(details, "name"));
						resp.setPhone(JSONParserUtil.getString(details, "phone"));
						resp.setPostalCode(JSONParserUtil.getString(details, "postal_code"));
						resp.setReferenceNo(JSONParserUtil.getString(details, "reference_no"));
						resp.setReturnUrl(JSONParserUtil.getString(details, "return_url"));
						resp.setSecureHash(JSONParserUtil.getString(details, "secure_hash"));
						resp.setShipAddress(JSONParserUtil.getString(details, "ship_address"));
						resp.setShipCity(JSONParserUtil.getString(details, "ship_city"));
						resp.setShipCountry(JSONParserUtil.getString(details, "ship_country"));
						resp.setShipName(JSONParserUtil.getString(details, "ship_name"));
						resp.setShipPhone(JSONParserUtil.getString(details, "ship_phone"));
						resp.setShipPostalCode(JSONParserUtil.getString(details, "ship_postal_code"));
						resp.setShipState(JSONParserUtil.getString(details, "ship_state"));
						resp.setState(JSONParserUtil.getString(details, "state"));*/
						resp.setSuccess(true);
					} else {
						resp.setSuccess(false);
					}
				} else if (code.equalsIgnoreCase("F00")) {
					org.json.JSONObject failureDetails = JSONParserUtil.getObject(jObj, "details");
					if (failureDetails != null) {
						//resp.setDescription(JSONParserUtil.getString(failureDetails, "message"));
					}
				} else if (code.equalsIgnoreCase("F03")){
					resp.setSuccess(false);
					resp.setMessage(JSONParserUtil.getString(jObj, "message"));
					resp.setStatus(JSONParserUtil.getString(jObj, "status"));
				}else{
					resp.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
		}
		return resp;

	}
	@Override
	public EBSRedirectResponse processRedirectSDKSplitpaymentMoney(EBSRedirectResponse redirectResponse) {
		try {
			MultivaluedMapImpl formData = new MultivaluedMapImpl();
			formData.add("responseCode", redirectResponse.getResponseCode());
			formData.add("merchantRefNo", redirectResponse.getMerchantRefNo());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(UrlMetadatas.getSplitpaymentUrlFlightUrl(Version.VERSION_1,
					Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = resource.accept("application/json").post(ClientResponse.class, formData);
			String strResponse = response.getEntity(String.class);
			System.err.println("response ::" +strResponse);
			if (response.getStatus() == 200) {
				org.json.JSONObject jObj = new org.json.JSONObject(strResponse);
				final String code = JSONParserUtil.getString(jObj, "code");
				if (code.equalsIgnoreCase("S00")) {
					redirectResponse.setSuccess(true);
					org.json.JSONObject details = JSONParserUtil.getObject(jObj, "details");
					redirectResponse.setDetails(String.valueOf(details));
				} else {
					redirectResponse.setResponseCode("1");
					redirectResponse.setSuccess(false);
				}
			} else {
				redirectResponse.setResponseCode("1");
				redirectResponse.setSuccess(false);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return redirectResponse;
	}

	@Override
	public EBSRedirectResponse processAgentRedirectSDKSplitpaymentMoney(EBSRedirectResponse redirectResponse) {
		try {
			MultivaluedMapImpl formData = new MultivaluedMapImpl();
			formData.add("responseCode", redirectResponse.getResponseCode());
			formData.add("merchantRefNo", redirectResponse.getMerchantRefNo());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource resource = client.resource(UrlMetadatas.getSplitpaymentUrlFlightUrl(Version.VERSION_1,
					Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = resource.accept("application/json").post(ClientResponse.class, formData);
			String strResponse = response.getEntity(String.class);
			System.err.println("response ::" +strResponse);
			if (response.getStatus() == 200) {
				org.json.JSONObject jObj = new org.json.JSONObject(strResponse);
				final String code = JSONParserUtil.getString(jObj, "code");
				if (code.equalsIgnoreCase("S00")) {
					redirectResponse.setSuccess(true);
					org.json.JSONObject details = JSONParserUtil.getObject(jObj, "details");
					redirectResponse.setDetails(String.valueOf(details));
				} else {
					redirectResponse.setResponseCode("1");
					redirectResponse.setSuccess(false);
				}
			} else {
				redirectResponse.setResponseCode("1");
				redirectResponse.setSuccess(false);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return redirectResponse;
	}

	@Override
	public LoadMoneyResponse LoadMoneyFlightRequest(com.payqwikweb.model.app.request.LoadMoneyFlightRequest request) {
		// TODO Auto-generated method stub
		LoadMoneyResponse resp = new LoadMoneyResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("channel", request.getChannel());
			payload.put("account_id", request.getAccountId());
			payload.put("reference_no", request.getReferenceNo());
			payload.put("amount", request.getAmount());
			payload.put("mode", request.getMode());
			payload.put("currency", request.getCurrency());
			payload.put("description", request.getDescription());
			payload.put("return_url", request.getReturnUrl());
			payload.put("name", request.getName());
			payload.put("address", request.getAddress());
			payload.put("city", request.getCity());
			payload.put("state", request.getState());
			payload.put("country", request.getCountry());
			payload.put("postal_code", request.getPostalCode());
			payload.put("phone", request.getPhone());
			payload.put("email", request.getEmail());
			payload.put("ship_name", request.getShipName());
			payload.put("ship_address", request.getShipAddress());
			payload.put("ship_city", request.getShipCity());
			payload.put("ship_state", request.getShipState());
			payload.put("ship_country", request.getShipCountry());
			payload.put("ship_postal_code", request.getShipPostalCode());
			payload.put("ship_phone", request.getShipPhone());
			payload.put("payment_mode", request.getPaymentMode());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(
					UrlMetadatas.getLoadMoneyUrl(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				String strResponse = response.getEntity(String.class);
				org.json.JSONObject jObj = new org.json.JSONObject(strResponse);
				final String code = JSONParserUtil.getString(jObj, "code");
				if (code.equalsIgnoreCase("S00")) {
					org.json.JSONObject details = JSONParserUtil.getObject(jObj, "details");
					if (details != null) {
						/*resp.setAccountId(JSONParserUtil.getString(details, "account_id"));
						resp.setAddress(JSONParserUtil.getString(details, "address"));
						resp.setAmount(JSONParserUtil.getString(details, "amount"));
						resp.setChannel(JSONParserUtil.getString(details, "channel"));
						resp.setCity(JSONParserUtil.getString(details, "city"));
						resp.setCountry(JSONParserUtil.getString(details, "country"));
						resp.setCurrency(JSONParserUtil.getString(details, "currency"));
						resp.setDescription(JSONParserUtil.getString(details, "description"));
						resp.setEmail(JSONParserUtil.getString(details, "email"));
						resp.setMode(JSONParserUtil.getString(details, "mode"));
						resp.setName(JSONParserUtil.getString(details, "name"));
						resp.setPhone(JSONParserUtil.getString(details, "phone"));
						resp.setPostalCode(JSONParserUtil.getString(details, "postal_code"));
						resp.setReferenceNo(JSONParserUtil.getString(details, "reference_no"));
						resp.setReturnUrl(JSONParserUtil.getString(details, "return_url"));
						resp.setSecureHash(JSONParserUtil.getString(details, "secure_hash"));
						resp.setShipAddress(JSONParserUtil.getString(details, "ship_address"));
						resp.setShipCity(JSONParserUtil.getString(details, "ship_city"));
						resp.setShipCountry(JSONParserUtil.getString(details, "ship_country"));
						resp.setShipName(JSONParserUtil.getString(details, "ship_name"));
						resp.setShipPhone(JSONParserUtil.getString(details, "ship_phone"));
						resp.setShipPostalCode(JSONParserUtil.getString(details, "ship_postal_code"));
						resp.setShipState(JSONParserUtil.getString(details, "ship_state"));
						resp.setState(JSONParserUtil.getString(details, "state"));*/
						resp.setSuccess(true);
					} else {
						resp.setSuccess(false);
					}
				} else if (code.equalsIgnoreCase("F00")) {
					org.json.JSONObject failureDetails = JSONParserUtil.getObject(jObj, "details");
					if (failureDetails != null) {
						//resp.setDescription(JSONParserUtil.getString(failureDetails, "message"));
					}
				} else {
					resp.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}

	@Override
	public LoadMoneyResponse LoadMoneyAgentFlightRequest(com.payqwikweb.model.app.request.LoadMoneyFlightRequest request) {
		// TODO Auto-generated method stub
		LoadMoneyResponse resp = new LoadMoneyResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("channel", request.getChannel());
			payload.put("account_id", request.getAccountId());
			payload.put("reference_no", request.getReferenceNo());
			payload.put("amount", request.getAmount());
			payload.put("mode", request.getMode());
			payload.put("currency", request.getCurrency());
			payload.put("description", request.getDescription());
			payload.put("return_url", request.getReturnUrl());
			payload.put("name", request.getName());
			payload.put("address", request.getAddress());
			payload.put("city", request.getCity());
			payload.put("state", request.getState());
			payload.put("country", request.getCountry());
			payload.put("postal_code", request.getPostalCode());
			payload.put("phone", request.getPhone());
			payload.put("email", request.getEmail());
			payload.put("ship_name", request.getShipName());
			payload.put("ship_address", request.getShipAddress());
			payload.put("ship_city", request.getShipCity());
			payload.put("ship_state", request.getShipState());
			payload.put("ship_country", request.getShipCountry());
			payload.put("ship_postal_code", request.getShipPostalCode());
			payload.put("ship_phone", request.getShipPhone());
			payload.put("payment_mode", request.getPaymentMode());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(
					UrlMetadatas.getLoadMoneyUrl(Version.VERSION_1, Role.AGENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				String strResponse = response.getEntity(String.class);
				org.json.JSONObject jObj = new org.json.JSONObject(strResponse);
				final String code = JSONParserUtil.getString(jObj, "code");
				if (code.equalsIgnoreCase("S00")) {
					org.json.JSONObject details = JSONParserUtil.getObject(jObj, "details");
					if (details != null) {
						/*resp.setAccountId(JSONParserUtil.getString(details, "account_id"));
						resp.setAddress(JSONParserUtil.getString(details, "address"));
						resp.setAmount(JSONParserUtil.getString(details, "amount"));
						resp.setChannel(JSONParserUtil.getString(details, "channel"));
						resp.setCity(JSONParserUtil.getString(details, "city"));
						resp.setCountry(JSONParserUtil.getString(details, "country"));
						resp.setCurrency(JSONParserUtil.getString(details, "currency"));
						resp.setDescription(JSONParserUtil.getString(details, "description"));
						resp.setEmail(JSONParserUtil.getString(details, "email"));
						resp.setMode(JSONParserUtil.getString(details, "mode"));
						resp.setName(JSONParserUtil.getString(details, "name"));
						resp.setPhone(JSONParserUtil.getString(details, "phone"));
						resp.setPostalCode(JSONParserUtil.getString(details, "postal_code"));
						resp.setReferenceNo(JSONParserUtil.getString(details, "reference_no"));
						resp.setReturnUrl(JSONParserUtil.getString(details, "return_url"));
						resp.setSecureHash(JSONParserUtil.getString(details, "secure_hash"));
						resp.setShipAddress(JSONParserUtil.getString(details, "ship_address"));
						resp.setShipCity(JSONParserUtil.getString(details, "ship_city"));
						resp.setShipCountry(JSONParserUtil.getString(details, "ship_country"));
						resp.setShipName(JSONParserUtil.getString(details, "ship_name"));
						resp.setShipPhone(JSONParserUtil.getString(details, "ship_phone"));
						resp.setShipPostalCode(JSONParserUtil.getString(details, "ship_postal_code"));
						resp.setShipState(JSONParserUtil.getString(details, "ship_state"));
						resp.setState(JSONParserUtil.getString(details, "state"));*/
						resp.setSuccess(true);
					} else {
						resp.setSuccess(false);
					}
				} else if (code.equalsIgnoreCase("F00")) {
					org.json.JSONObject failureDetails = JSONParserUtil.getObject(jObj, "details");
					if (failureDetails != null) {
					//	resp.setDescription(JSONParserUtil.getString(failureDetails, "message"));
					}
				} else {
					resp.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}

	public UpiSdkResponseDTO loadMoneyUIPRequest(LoadMoneyRequest request) {
		UpiSdkResponseDTO resp = new UpiSdkResponseDTO();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("amount", request.getAmount());
			payload.put("payerVirAddr", request.getPayerVirAddr());
			payload.put("expTime", request.getExpDate());
			payload.put("merchantVpa", UPIConstants.PAYEE_VIR_ADDR);
			payload.put("sdk", request.isSdk());
			payload.put("deviceId", request.getDeviceId());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(UrlMetadatas.getUPILoadMoneyRequest(Version.VERSION_1, Role.USER,
					Device.ANDROID, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.post(ClientResponse.class, payload);
			String strResponse = response.getEntity(String.class);
			System.err.println("Response : : " + strResponse);
			if (response.getStatus() != 200) {
				resp.setCode("F00");
				resp.setMessage("Service unavailable");
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						String txnId = null;
						if(code.equals("S00")){
							txnId = (String) jobj.get("txnId");
//							String mToken = (String) jobj.getJSONObject("details").getString("merchantToken");
//							resp.setToken(mToken);
						}else if(code.equals("F00")){
							txnId = (String) jobj.get("txnId");
						}
						resp.setSuccess(true);
						resp.setCode(code);
						resp.setStatus(status);
						resp.setMessage(message);
						resp.setTransactionId(txnId);
					} else {
						resp.setSuccess(false);
						resp.setCode("F00");
						resp.setMessage("Service unavailable");
						resp.setStatus("FAILED");
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus("FAILED");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus("FAILED");
		}
		return resp;
	}

	@Override
	public ResponseDTO handleUPIRedirectRequest(UPIRedirect dto) {
		ResponseDTO result = new ResponseDTO();
		try {
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(UrlMetadatas.getLoadMoneyUPIResponseUrl(Version.VERSION_1,
					Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(dto.toJSON().toString()))
					.post(ClientResponse.class, dto.toJSON());
			if (response.getStatus() != 200) {
				result.setSuccess(false);
			} else {
				String strResponse = response.getEntity(String.class);
				org.json.JSONObject jObj = new org.json.JSONObject(strResponse);
				final String code = JSONParserUtil.getString(jObj, "code");
				if (code.equalsIgnoreCase("S00")) {
					result.setSuccess(true);
					result.setMessage(JSONParserUtil.getString(jObj, "message"));
					result.setCode(JSONParserUtil.getString(jObj, "code"));
				} else {
					result.setSuccess(false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
		}
		return result;
	}
	
	@Override
	public UpiSdkResponseDTO handleSdkUPIRedirectRequest(UpiMobileRedirectRequest dto) {
		UpiSdkResponseDTO result = new UpiSdkResponseDTO();
		try {
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(UrlMetadatas.getLoadMoneySdkUPIResponseUrl(Version.VERSION_1,
					Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(dto.toJSON().toString()))
					.post(ClientResponse.class, dto.toJSON());
			String strResponse = response.getEntity(String.class);
			System.err.println("Response : : " + strResponse);
			if (response.getStatus() != 200) {
				result.setCode("F00");
				result.setMessage("Service unavailable");
			} else {
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						final String status = (String) jobj.get("status");
						final String code = (String) jobj.get("code");
						final String message = (String) jobj.get("message");
						result.setSuccess(true);
						result.setCode(code);
						result.setStatus(status);
						result.setMessage(message);
					} else {
						result.setSuccess(false);
						result.setCode("F00");
						result.setMessage("Service unavailable");
						result.setStatus("FAILED");
					}
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Service unavailable");
					result.setStatus("FAILED");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
		}
		return result;
	}

	@Override
	public VNetStatusResponse verifyVNetTransaction(VRedirectResponse request) {
		VNetStatusResponse result = new VNetStatusResponse();
		try{
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			MultivaluedMapImpl formData = new MultivaluedMapImpl();
			formData.add("PID", APIConstants.VNET_PID);
			formData.add("BID", request.getBID());
			formData.add("PRN", request.getPRN());
			formData.add("ITC", request.getPRN());
			formData.add("AMT", request.getAmount());
			WebResource resource = client.resource(APIConstants.VNET_VERIFICATION_URL);
			ClientResponse response = resource.accept("application/x-www-form-urlencoded").post(ClientResponse.class, formData);
			String strResponse = response.getEntity(String.class);
			System.err.println("string response ::" + strResponse);
			if(strResponse != null){
				Document html = Jsoup.parse(strResponse);
				String h4 = html.body().getElementsByTag("h4").text();
				if(h4.equals("Your Payment is Successful")){
					System.err.println("TRUE");
					result.setSuccess(true);
					result.setMessage(h4);
					result.setStatus("Success");
					System.out.println("Afte parsing, Heading : " + h4);
				}else{
					result.setSuccess(false);
					result.setMessage(h4);
					result.setStatus("Failed");
					System.err.println("FALSE");
					System.out.println("Afte parsing, Heading : " + h4);
				}
			}
		}catch (Exception e){
			e.printStackTrace();
			result.setSuccess(false);
		}
		return result;
	}

	@Override
	public StatusResponse checkUpiStausAfterRedirectRequest(LoadMoneyRequest dto) {
		StatusResponse result = new StatusResponse();
	try {
		Client client = Client.create();
//		client.addFilter(new LoggingFilter(System.out));
		JSONObject payload = new JSONObject();
		payload.put("transactionRefNo", dto.getReferenceNo());
		WebResource webResource = client.resource(UrlMetadatas.getLoadMoneyStatusAfterRedirectResponseUrl(Version.VERSION_1,
				Role.USER, Device.WEBSITE, Language.ENGLISH));
		ClientResponse response = webResource.accept("application/json").type("application/json")
				.post(ClientResponse.class, payload);
		if (response.getStatus() == 200) {
			String strResponse = response.getEntity(String.class);
			System.err.println("strResponse :  :  " + strResponse);
			org.json.JSONObject jObj = new org.json.JSONObject(strResponse);
			final String code = JSONParserUtil.getString(jObj, "code");
			result.setMessage(JSONParserUtil.getString(jObj, "message"));
			result.setCode(JSONParserUtil.getString(jObj, "code"));
			if (code.equalsIgnoreCase("S00")) {
				org.json.JSONObject details = JSONParserUtil.getObject(jObj, "details");
				result.setTransactionDate(JSONParserUtil.getString(details, "transactionDate"));
				result.setPaymentId(JSONParserUtil.getString(details, "paymentId"));
				result.setMerchantRefNo(JSONParserUtil.getString(details, "merchantRefNo"));
				result.setStatus(Status.valueOf(JSONParserUtil.getString(details, "status")));
				result.setAmount(JSONParserUtil.getString(details, "amount"));
			} 
		} 
	} catch (Exception e) {
		e.printStackTrace();
	}
	return result;
	}
	
	
	@Override
	public RazorPayResponse initiateRazorPayLoadMoney(RazorPayRequest request) {
		RazorPayResponse resp = new RazorPayResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("amount", request.getAmount());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(
					UrlMetadatas.getLoadMoneyRazorPayUrl(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				String strResponse = response.getEntity(String.class);
				org.json.JSONObject jObj = new org.json.JSONObject(strResponse);
				final String code = JSONParserUtil.getString(jObj, "code");
				if (code.equalsIgnoreCase("S00")) {
					org.json.JSONObject details = JSONParserUtil.getObject(jObj, "details");
					if (details != null) {
						resp.setSuccess(true);
						resp.setCode(code);
						resp.setMessage(JSONParserUtil.getString(jObj, "message"));
						resp.setStatus(JSONParserUtil.getString(jObj, "status"));
						resp.setReferenceNo(JSONParserUtil.getString(details, "transactionId"));
					} else {
						resp.setSuccess(false);
						resp.setMessage("Please try again later.");
					}
				} else if (code.equalsIgnoreCase("F00")) {
					org.json.JSONObject failureDetails = JSONParserUtil.getObject(jObj, "details");
					if (failureDetails != null) {
						resp.setMessage(JSONParserUtil.getString(failureDetails, "message"));
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(code);
					resp.setStatus(JSONParserUtil.getString(jObj, "status"));
					resp.setMessage(JSONParserUtil.getString(jObj, "message"));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
		}
		return resp;

	}
	
	@Override
	public RazorPayResponse successRazorPayLoadMoney(RazorPayRequest request,RazorPayResponse razorpayResp) {
		RazorPayResponse resp = new RazorPayResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("amount", request.getAmount());
			payload.put("referenceId", razorpayResp.getId());
			payload.put("transactionId", request.getTransactionId());
			payload.put("success", razorpayResp.isSuccess());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(
					UrlMetadatas.getSuccessLoadMoneyRazorPayUrl(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				String strResponse = response.getEntity(String.class);
				org.json.JSONObject jObj = new org.json.JSONObject(strResponse);
				final String code = JSONParserUtil.getString(jObj, "code");
				if (code.equalsIgnoreCase("S00")) {
					org.json.JSONObject details = JSONParserUtil.getObject(jObj, "details");
					if (details != null) {
						resp.setSuccess(true);
						resp.setCode(JSONParserUtil.getString(jObj, "code"));
						resp.setMessage(JSONParserUtil.getString(jObj, "message"));
						resp.setStatus(JSONParserUtil.getString(jObj, "status"));
						resp.setReferenceNo(JSONParserUtil.getString(details, "transactionId"));
					} else {
						resp.setSuccess(false);
					}
				} else if (code.equalsIgnoreCase("F00")) {
					org.json.JSONObject failureDetails = JSONParserUtil.getObject(jObj, "details");
					if (failureDetails != null) {
						resp.setMessage(JSONParserUtil.getString(failureDetails, "message"));
					}
				} else {
					resp.setSuccess(false);
					resp.setCode(code);
					resp.setStatus(JSONParserUtil.getString(jObj, "status"));
					resp.setMessage(JSONParserUtil.getString(jObj, "message"));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
		}
		return resp;

	}
	
	@Override
	public RazorPayResponse verifyTransactionAtRazorPay(RazorPayRequest dto) {
		RazorPayResponse resp = new RazorPayResponse();
//		try {
/*			double amt =dto.getAmount();
			double validAmount = amt * 100;
			RazorpayClient razorpayClient = new RazorpayClient(ClientDetails.getRazorPayUsername(),
					ClientDetails.getRazorPayPassword());
			org.json.JSONObject options = new org.json.JSONObject();
			options.put("amount", validAmount);
			Client.create().addFilter(new LoggingFilter(System.out));
			Payment strResponse = razorpayClient.Payments.capture(dto.getRazorpayPaymentId(), options);
			JSONObject jObj = new JSONObject(strResponse.toString());
			System.err.println("response ::" + strResponse.toString());
			final String message = jObj.getString("description");
			final String status = jObj.getString("status");
			if ("captured".equalsIgnoreCase(status)) {
				resp.setId(jObj.getString("id"));
				resp.setCaptured(jObj.getBoolean("captured"));
				resp.setSuccess(true);
			} else {
				resp.setMessage(message);
				resp.setId(jObj.getString("id"));
				resp.setCaptured(jObj.getBoolean("captured"));
				resp.setSuccess(false);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
		}*/
		return resp;
	}
	
	@Override
	public LoadMoneyResponse loadMoneyUsingVoucher(LoadMoneyRequest request,String role) {
		LoadMoneyResponse resp = new LoadMoneyResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionId());
			payload.put("voucherNumber", request.getVoucherNumber());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(
					UrlMetadatas.getVoucherLoadMoney(Version.VERSION_1,Role.getEnum(role), Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				resp.setSuccess(false);
			} else {
				String strResponse = response.getEntity(String.class);
				org.json.JSONObject jObj = new org.json.JSONObject(strResponse);
				final String code = JSONParserUtil.getString(jObj, "code");
				final String message = JSONParserUtil.getString(jObj, "message");
				if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
					resp.setStatus(Status.Success.getValue());
					resp.setSuccess(true);
					resp.setCode(code);
				} else {
					resp.setStatus(Status.Failed.getValue());
					resp.setCode(code);
					resp.setSuccess(false);
				}
				resp.setMessage(message);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F04");
			resp.setMessage("Service Unavailable");
		}
		return resp;
	}

}