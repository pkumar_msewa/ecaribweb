package com.payqwikweb.app.api.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.payqwikweb.app.api.IATravelBusApi;
import com.payqwikweb.app.api.ILoadMoneyApi;
import com.payqwikweb.app.api.ITravelBusApi;
import com.payqwikweb.app.metadatas.UrlMetadatas;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Language;
import com.payqwikweb.app.model.ResponseStatus;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.Version;
import com.payqwikweb.app.model.busdto.AvailableTripsDTO;
import com.payqwikweb.app.model.busdto.BDPointDTO;
import com.payqwikweb.app.model.busdto.BDPointResponse;
import com.payqwikweb.app.model.busdto.BookTicketReq;
import com.payqwikweb.app.model.busdto.BusCityListDTO;
import com.payqwikweb.app.model.busdto.BusOperatorDTO;
import com.payqwikweb.app.model.busdto.BusPriceRecheckResp;
import com.payqwikweb.app.model.busdto.BusTicketDTO;
import com.payqwikweb.app.model.busdto.DPPointsDTO;
import com.payqwikweb.app.model.busdto.FareDetailDTO;
import com.payqwikweb.app.model.busdto.GetTransactionId;
import com.payqwikweb.app.model.busdto.LowerDTO;
import com.payqwikweb.app.model.busdto.PriceRecheckDetailsDto;
import com.payqwikweb.app.model.busdto.SaveSeatDetailsDTO;
import com.payqwikweb.app.model.busdto.SeatColumnDTO;
import com.payqwikweb.app.model.busdto.TravellerDetailsDTO;
import com.payqwikweb.app.model.request.bus.GetDestinationCity;
import com.payqwikweb.app.model.request.bus.GetSeatDetails;
import com.payqwikweb.app.model.request.bus.GetSourceCity;
import com.payqwikweb.app.model.request.bus.IsCancellableReq;
import com.payqwikweb.app.model.request.bus.ListOfAvailableTrips;
import com.payqwikweb.app.model.response.UserDetailsResponse;
import com.payqwikweb.app.model.response.bus.BookTicketResp;
import com.payqwikweb.app.model.response.bus.CancelPolicyListDTOResp;
import com.payqwikweb.app.model.response.bus.CancelTicketResp;
import com.payqwikweb.app.model.response.bus.IsCancellableResp;
import com.payqwikweb.app.model.response.bus.ResponseDTO;
import com.payqwikweb.app.model.response.bus.SeatDetailsResponse;
import com.payqwikweb.app.model.response.bus.TranasctionIdResponse;
import com.payqwikweb.app.utils.SecurityUtils;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;

public class AgentTravelBusApi implements IATravelBusApi{

	private final ILoadMoneyApi loadMoneyApi;
	private SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");

	public AgentTravelBusApi(ILoadMoneyApi loadMoneyApi) {
		this.loadMoneyApi=loadMoneyApi;
	}

	@Override
	public ResponseDTO getAllCityList(String sessionId) {
		ResponseDTO resp=new ResponseDTO();

		int size=0;
		List<BusCityListDTO> list=new ArrayList<>();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", sessionId);

			Client vpqClient = Client.create();
			vpqClient.addFilter(new LoggingFilter(System.out));
			WebResource vpqWebResource = vpqClient.resource(
					UrlMetadatas.agentGetAllCityListFormDB(Version.VERSION_1,Role.AGENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse vpqresponse = vpqWebResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);

			String strResponse = vpqresponse.getEntity(String.class);
			System.out.println(strResponse);
			JSONObject jobj=new JSONObject(strResponse);
			final String code=jobj.getString("code");
			if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
				String strDetails=jobj.getString("details");
				if (strDetails!=null) {
					JSONArray cList=jobj.getJSONArray("details");

					long bCid=0;

					for (int i = 0; i < cList.length(); i++) {
						BusCityListDTO dto=new BusCityListDTO();

						dto.setCityId(cList.getJSONObject(i).getLong("cityId"));
						dto.setCityName(cList.getJSONObject(i).getString("cityName"));

						if (cList.getJSONObject(i).getString("cityName").equalsIgnoreCase("Bangalore")) {
							bCid=cList.getJSONObject(i).getLong("cityId");
						}else if (cList.getJSONObject(i).getString("cityName").equalsIgnoreCase("Bangaluru")) {
							dto.setCityId(bCid);
						}else if(cList.getJSONObject(i).getString("cityName").equalsIgnoreCase("Bengaluru")) {
							dto.setCityId(bCid);
						}
						list.add(dto);
					}
				}
				resp.setCode(code);
				resp.setMessage("Get All City List");
				resp.setDetails(list);
				resp.setStatus(ResponseStatus.SUCCESS.getKey());
			}
		} catch (Exception e) {
			System.out.println(e);
			resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			resp.setMessage("Service Unavailable");
			resp.setDetails(null);
			resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
		}
		return resp;
	}


	@Override
	public ResponseDTO getAllSourceCity(GetSourceCity req) {
		ResponseDTO resp=new ResponseDTO();

		List<BusCityListDTO> list=new ArrayList<>();
		try {
			JSONObject payload = new JSONObject();
			payload.put("clientIp", "49.204.86.246");
			payload.put("cllientKey", UrlMetadatas.MDEX_CLIENTKEY);
			payload.put("clientToken", UrlMetadatas.MDEX_CLIENTTOKEN);
			payload.put("clientApiName", "VPayQwik");
			payload.put("sourceKey",req.getSourceKey());
			Client client = Client.create();
			WebResource webResource = client
					.resource(UrlMetadatas.agentGetBusSourceCityList(Role.ClIENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);

			String strResponse = response.getEntity(String.class);
			System.out.println(strResponse);
			JSONObject jobj=new JSONObject(strResponse);
			final String code=jobj.getString("code");
			if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
				String strDetails=jobj.getString("details");
				if (strDetails!=null) {
					JSONArray cList=jobj.getJSONArray("details");

					for (int i = 0; i < cList.length(); i++) {
						BusCityListDTO dto=new BusCityListDTO();
						dto.setCityId(cList.getJSONObject(i).getLong("cityId"));
						dto.setCityName(cList.getJSONObject(i).getString("cityName"));
						list.add(dto);
					}
				}
				resp.setCode(code);
				resp.setMessage(jobj.getString("message"));
				resp.setDetails(list);
				resp.setStatus(ResponseStatus.SUCCESS.getKey());
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			resp.setMessage("Service Unavailable");
			resp.setDetails(null);
			resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
		}
		return resp;
	}


	@Override
	public ResponseDTO getAllDestinationCity(GetDestinationCity req) {
		ResponseDTO resp=new ResponseDTO();

		List<BusCityListDTO> list=new ArrayList<>();
		try {
			JSONObject payload = new JSONObject();
			payload.put("clientIp", "49.204.86.246");
			payload.put("cllientKey", UrlMetadatas.MDEX_CLIENTKEY);
			payload.put("clientToken", UrlMetadatas.MDEX_CLIENTTOKEN);
			payload.put("clientApiName", "VPayQwik");
			payload.put("sourceId",req.getSourceId());
			payload.put("destinationKey",req.getDestinationKey());
			Client client = Client.create();
			WebResource webResource = client
					.resource(UrlMetadatas.agentGetBusDestinationCityList(Role.ClIENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);

			String strResponse = response.getEntity(String.class);
			System.out.println(strResponse);
			JSONObject jobj=new JSONObject(strResponse);
			final String code=jobj.getString("code");
			if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
				String strDetails=jobj.getString("details");
				if (strDetails!=null) {
					JSONArray cList=jobj.getJSONArray("details");

					for (int i = 0; i < cList.length(); i++) {
						BusCityListDTO dto=new BusCityListDTO();
						dto.setCityId(cList.getJSONObject(i).getLong("cityId"));
						dto.setCityName(cList.getJSONObject(i).getString("cityName"));
						list.add(dto);
					}
				}
				resp.setCode(code);
				resp.setMessage(jobj.getString("message"));
				resp.setDetails(list);
				resp.setStatus(ResponseStatus.SUCCESS.getKey());
			}
		} catch (Exception e) {
			System.out.println(e);
			resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			resp.setMessage("Service Unavailable");
			resp.setDetails(null);
			resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
		}
		return resp;
	}

	@Override
	public ResponseDTO getAllAvailableTrips(ListOfAvailableTrips req) {
		ResponseDTO resp=new ResponseDTO();

		BDPointResponse result=new BDPointResponse();
		List<BDPointDTO> bdPointsList= new ArrayList<>();
		List<AvailableTripsDTO> availableTripsList=new ArrayList<>();
		List<DPPointsDTO> dpPointsList= new ArrayList<>();
		List<BusOperatorDTO> busOPTList= new ArrayList<>();
		Map<String, List<BDPointDTO>> bdpointMap=new HashMap<>();
		Map<String, List<DPPointsDTO>> dppointMap=new HashMap<>();
		Set<String> boarding=new TreeSet<String>();

		try {

			JSONObject payload = new JSONObject();
			payload.put("clientIp", "49.204.86.246");
			payload.put("cllientKey", UrlMetadatas.MDEX_CLIENTKEY);
			payload.put("clientToken", UrlMetadatas.MDEX_CLIENTTOKEN);
			payload.put("clientApiName", "VPayQwik");
			payload.put("sourceId",req.getSourceId());
			payload.put("destinationId",req.getDestinationId());
			payload.put("date",req.getDate());
			System.err.println("Payload is: "+payload.toString());
			Client client = Client.create();
			WebResource webResource = client
					.resource(UrlMetadatas.agentGetAllAvailableTrip(Role.ClIENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);

			System.err.println(response);

			if (response.getStatus() != 200) {
				System.err.println("RESPONSE CODE :: " + response.getStatus());
				resp.setCode("F00");
				resp.setMessage("Service Unavailable");
				resp.setStatus(ResponseStatus.FAILURE.getKey());

			} else {
				String strResponse = response.getEntity(String.class);
				System.err.println("Payment Response :: " + strResponse);
				if (strResponse != null) {

					JSONObject jsonObject=new JSONObject(strResponse);

					String strBdPoints = jsonObject.getString("bdPoints");
					if(!strBdPoints.equals("null")){
						org.codehaus.jettison.json.JSONArray bdPoints=jsonObject.getJSONArray("bdPoints");
						if (bdPoints.length()>0) {
							for (int i = 0; i < bdPoints.length(); i++) {
								JSONObject jobj=bdPoints.getJSONObject(i);

								bdPointsList.add(boardingPointJson(jobj));
							}
							result.setBdPoints(bdPointsList);
						}
					}

					String strAvailableTrips = jsonObject.getString("availableTripsDTOs");
					if(!strAvailableTrips.equals("null")){
						//bdpointMap=new HashMap<>();
						org.codehaus.jettison.json.JSONArray availableTrips=jsonObject.getJSONArray("availableTripsDTOs");
						System.err.println("Available Trips:: "+availableTrips.length());
						if (availableTrips.length()>0) {
							List<BDPointDTO> bdList=null;
							List<DPPointsDTO> dpList=null;
							List<FareDetailDTO> fareList=null;
							List<String> fare=null;

							for (int i = 0; i < availableTrips.length(); i++) {

								List<CancelPolicyListDTOResp> cpList=new ArrayList<>();
								JSONObject jobj=availableTrips.getJSONObject(i);
								AvailableTripsDTO dto=new AvailableTripsDTO();
								dto.setAc(jobj.getBoolean("ac"));
								dto.setArrivalTime(jobj.getString("arrivalTime"));
								dto.setAvailableSeats(jobj.getString("availableSeats"));
								dto.setBusType(jobj.getString("busType"));
								dto.setBusTypeId("busTypeId");
								dto.setCancellationPolicy(jobj.getString("cancellationPolicy"));
								dto.setDepartureTime(jobj.getString("departureTime"));
								dto.setDoj(jobj.getString("doj").split("T")[0]);
								dto.setDuration(jobj.getString("duration"));

								String strbdPoints = jobj.getString("bdPointDTO");
								if(!strbdPoints.equals("null")){
									org.codehaus.jettison.json.JSONArray jsonArray=jobj.getJSONArray("bdPointDTO");
									bdList= new ArrayList<>();
									for (int j = 0; j < jsonArray.length(); j++) {

										JSONObject bdPointsJobj=jsonArray.getJSONObject(j);
										bdList.add(boardingPointJson(bdPointsJobj));
										boarding.add(boardingPointJson(bdPointsJobj).getBdPoint());
									}
									dto.setBdPointDTO(bdList);
								}

								bdpointMap.put(jobj.getString("id"), bdList);

								String strdpPoints = jobj.getString("dpPointsDTO");
								if(!strdpPoints.equals("null")){
									org.codehaus.jettison.json.JSONArray dpJsonArray=jobj.getJSONArray("dpPointsDTO");
									dpList=new ArrayList<>();

									for (int j = 0; j < dpJsonArray.length(); j++) {

										JSONObject bdPointsJobj=dpJsonArray.getJSONObject(j);
										dpList.add(dropingPointJson(bdPointsJobj));
									}
									dto.setDpPointsDTO(dpList);
								}
								dppointMap.put(jobj.getString("id"), dpList);

								String strfareDetail = jobj.getString("fareDetailDTOs");
								if(!strfareDetail.equals("null")){
									org.codehaus.jettison.json.JSONArray fareJsonArray=jobj.getJSONArray("fareDetailDTOs");
									fareList=new ArrayList<>();
									for (int j = 0; j < fareJsonArray.length(); j++) {
										FareDetailDTO fareDetailDTO=new FareDetailDTO();
										JSONObject fareDetailJobj=fareJsonArray.getJSONObject(j);
										fareDetailDTO.setBaseFare(fareDetailJobj.getString("baseFare"));
										fareDetailDTO.setMarkupFareAbsolute(fareDetailJobj.getString("markupFareAbsolute"));
										fareDetailDTO.setMarkupFarePercentage(fareDetailJobj.getString("markupFarePercentage"));
										fareDetailDTO.setOperatorServiceChargeAbsolute(fareDetailJobj.getString("operatorServiceChargeAbsolute"));
										fareDetailDTO.setOperatorServiceChargePercentage(fareDetailJobj.getString("operatorServiceChargePercentage"));
										fareDetailDTO.setServiceTaxAbsolute(fareDetailJobj.getString("serviceTaxAbsolute"));
										fareDetailDTO.setServiceTaxPercentage(fareDetailJobj.getString("serviceTaxPercentage"));
										fareDetailDTO.setTotalFare(fareDetailJobj.getString("totalFare"));
										fareList.add(fareDetailDTO);
									}
									dto.setFareDetailDTOs(fareList);
								}
								String strfares = jobj.getString("fare");
								if(!strfares.equals("null")){
									org.codehaus.jettison.json.JSONArray fareArray=jobj.getJSONArray("fare");
									fare=new ArrayList<>();
									for (int j = 0; j < fareArray.length(); j++) {

										fare.add(fareArray.getString(j));
									}
									dto.setFare(fare);
								}

								String price=jobj.getString("price");

								String spPrice[]=price.split("\\.");

								dto.setTravels(jobj.getString("travels"));
								dto.setId(jobj.getString("id"));
								dto.setRouteId(jobj.getString("routeId"));
								dto.setPrice(spPrice[0]);
								dto.setSeater(jobj.getBoolean("seater"));
								dto.setSleeper(jobj.getBoolean("sleeper"));
								dto.setIdProofRequired(jobj.getString("idProofRequired"));
								dto.setLiveTrackingAvailable(jobj.getString("liveTrackingAvailable"));
								dto.setNonAC(jobj.getBoolean("nonAC"));
								dto.setOperatorid(jobj.getString("operatorid"));
								dto.setPartialCancellationAllowed(jobj.getString("partialCancellationAllowed"));
								dto.setTatkalTime(jobj.getString("tatkalTime"));
								dto.setVehicleType(jobj.getString("vehicleType"));
								dto.setZeroCancellationTime(jobj.getString("zeroCancellationTime"));
								dto.setmTicketEnabled(jobj.getString("mTicketEnabled"));
								dto.setSortDepTime(jobj.getDouble("sortDepTime"));
								dto.setEngineId(jobj.getLong("engineId"));

								String strcancelPolicyList = jobj.getString("cancelPolicyListDTO");
								if(!strcancelPolicyList.equals("null")){
									org.codehaus.jettison.json.JSONArray cancelPolicyArray=jobj.getJSONArray("cancelPolicyListDTO");

									for (int j = 0; j < cancelPolicyArray.length(); j++) {
										CancelPolicyListDTOResp cpDTO=new CancelPolicyListDTOResp();
										JSONObject fareDetailJobj=cancelPolicyArray.getJSONObject(j);
										cpDTO.setTimeFrom(fareDetailJobj.getDouble("timeFrom"));
										cpDTO.setTimeTo(fareDetailJobj.getDouble("timeTo"));
										cpDTO.setPercentageCharge(fareDetailJobj.getDouble("percentageCharge"));
										cpDTO.setFlatCharge(fareDetailJobj.getDouble("flatCharge"));
										cpDTO.setFlat(fareDetailJobj.getBoolean("flat"));
										cpDTO.setSeatno(fareDetailJobj.getString("seatno"));
										cpList.add(cpDTO);
									}
									dto.setCancelPolicyListDTO(cpList);
								}
								dto.setStatus(jobj.getString("status"));
								dto.setTotalSeat(jobj.getString("totalSeat"));
								dto.setDepartureDate(jobj.getString("departureDate"));
								dto.setArrivalDate(jobj.getString("arrivalDate"));
								dto.setDiscount(jobj.getDouble("discount"));
								dto.setCommission(jobj.getDouble("commission"));
								dto.setMarkup(jobj.getDouble("markup"));
								dto.setTds(jobj.getDouble("tds"));
								dto.setStf(jobj.getDouble("stf"));
								dto.setVolvo(jobj.getBoolean("volvo"));
								dto.setCancellable(jobj.getBoolean("cancellable"));

								//if (jobj.getLong("engineId")!=3) {
								if (!jobj.getString("availableSeats").equalsIgnoreCase("0")) {
									availableTripsList.add(dto);
								}
								//}

							}
						}
					}

					String strdpPoints = jsonObject.getString("droppingPoints");
					if(!strdpPoints.equals("null")){
						org.codehaus.jettison.json.JSONArray dpJsonArray=jsonObject.getJSONArray("droppingPoints");

						for (int j = 0; j < dpJsonArray.length(); j++) {

							JSONObject bdPointsJobj=dpJsonArray.getJSONObject(j);
							dpPointsList.add(dropingPointJson(bdPointsJobj));
						}
						result.setDroppingPoints(dpPointsList);
					}

					result.setTotalSeats(jsonObject.getString("totalSeats"));
					result.setMinDeptTime(jsonObject.getString("minDeptTime"));
					result.setMaxDeptTime(jsonObject.getString("maxDeptTime"));
					result.setMaxPrice(jsonObject.getString("maxPrice"));
					result.setJourneyDate(jsonObject.getString("journeyDate"));
					result.setMinPrice(jsonObject.getString("minPrice"));
					result.setTotalTrips(jsonObject.getInt("totalTrips"));
					result.setSource(jsonObject.getString("source"));
					result.setSourceId(jsonObject.getLong("sourceId"));
					result.setDestination(jsonObject.getString("destination"));
					result.setDestinationId(jsonObject.getLong("destinationId"));
					result.setSessionId(jsonObject.getString("sessionId"));
					result.setCreationDate(jsonObject.getString("creationDate"));
					result.setSortDate(jsonObject.getString("sortDate"));
					result.setSortMaxDepTime(jsonObject.getLong("sortMaxDepTime"));
					result.setSortMinDepTime(jsonObject.getLong("sortMinDepTime"));
					result.setBusAvailable(jsonObject.getBoolean("busAvailable"));


					String strbusOperator = jsonObject.getString("busOperator");
					if(!strbusOperator.equals("null")){
						org.codehaus.jettison.json.JSONArray busOperator=jsonObject.getJSONArray("busOperator");

						for (int j = 0; j < busOperator.length(); j++) {

							JSONObject busOperatorJobj=busOperator.getJSONObject(j);
							busOPTList.add(null);
						}
						result.setBusOperator(busOPTList);
					}

					Collections.sort(availableTripsList);

					result.setAvailableTripsDTOs(availableTripsList);
					result.setBdpointMap(bdpointMap);
					result.setDppointMap(dppointMap);
					result.setBoardingFilter(boarding);
					resp.setCode(jsonObject.getString("code"));
					resp.setStatus(jsonObject.getString("status"));
					resp.setMessage(jsonObject.getString("message"));
					resp.setDetails(result);
				} else {
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus(ResponseStatus.FAILURE.getKey());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus(ResponseStatus.FAILURE.getKey());
		}
		return resp;
	}



	private BDPointDTO boardingPointJson(JSONObject jobj) 
	{
		BDPointDTO dto=new BDPointDTO();
		try {
			dto.setBdid(jobj.getString("bdid"));
			dto.setBdPoint(jobj.getString("bdPoint"));
			dto.setBdLongName(jobj.getString("bdLongName"));
			dto.setContactNumber(jobj.getString("contactNumber"));
			dto.setBdlocation(jobj.getString("bdlocation"));
			dto.setLandmark(jobj.getString("landmark"));
			dto.setPrime(jobj.getString("prime"));
			dto.setTime(jobj.getString("time"));

		} catch (JSONException e) {

			System.err.println("Sround with try catch");
			e.printStackTrace();
		}
		return dto;
	}


	private DPPointsDTO dropingPointJson(JSONObject jobj) 
	{
		DPPointsDTO dto=new DPPointsDTO();
		try {

			dto.setDpId(jobj.getString("dpId"));
			dto.setDpName(jobj.getString("dpName"));
			dto.setLocatoin(jobj.getString("locatoin"));
			dto.setPrime(jobj.getString("prime"));
			dto.setDpTime(jobj.getString("dpTime"));
			dto.setContactNumber(jobj.getString("contactNumber"));

		} catch (JSONException e) {

			System.err.println("Sround with try catch");
			e.printStackTrace();
		}
		return dto;
	}



	@Override
	public ResponseDTO getSeatDetails(GetSeatDetails req) {

		ResponseDTO resp=new ResponseDTO();

		SeatDetailsResponse result=new SeatDetailsResponse();
		LowerDTO lowerDTO=new LowerDTO();
		LowerDTO upperDTO=new LowerDTO();
		List<SeatColumnDTO> columnList= null;
		List<BDPointDTO> listBoardingPoints=new ArrayList<>();
		List<DPPointsDTO> listDropingPoints=new ArrayList<>();

		try {

			JSONObject payload = new JSONObject();
			payload.put("clientIp", "49.204.86.246");
			payload.put("cllientKey", UrlMetadatas.MDEX_CLIENTKEY);
			payload.put("clientToken", UrlMetadatas.MDEX_CLIENTTOKEN);
			payload.put("clientApiName", "VPayQwik");
			payload.put("busId",req.getBusId());
			payload.put("seater",req.isSeater());
			payload.put("sleeper",req.isSleeper());
			payload.put("engineId",req.getEngineId());
			payload.put("journeyDate",req.getJourneyDate());

			Client client = Client.create();
			WebResource webResource = client
					.resource(UrlMetadatas.agentGetseatDetails(Role.ClIENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);

			System.err.println(response);

			if (response.getStatus() != 200) {
				System.err.println("RESPONSE CODE :: " + response.getStatus());
				resp.setCode("F00");
				resp.setMessage("Service Unavailable");
				resp.setStatus(ResponseStatus.FAILURE.getKey());

			} else {
				String strResponse = response.getEntity(String.class);
				System.err.println("Response :: " + strResponse);
				if (strResponse != null) {

					JSONObject jsonObject=new JSONObject(strResponse);

					boolean lowerShow=jsonObject.getBoolean("lowerShow");
					boolean upperShow=jsonObject.getBoolean("upperShow");


					/*Lower Show*/
					if (lowerShow) {

						JSONObject lower=jsonObject.getJSONObject("lower");

						if (lower!=null) {
							//							name(lower);
							String strfirstColumn = lower.getString("firstColumn");
							if(!strfirstColumn.equals("null")){
								org.codehaus.jettison.json.JSONArray firstColumn=lower.getJSONArray("firstColumn");
								if (firstColumn.length()>0) {
									columnList=new ArrayList<>();
									for (int i = 0; i < firstColumn.length(); i++) {
										JSONObject fcJson=firstColumn.getJSONObject(i);
										SeatColumnDTO fColumn=setaDetailsColumnJson(fcJson);
										columnList.add(fColumn);
									}
									lowerDTO.setFirstColumn(columnList);
								}
							}

							String strSecondColumn = lower.getString("secondColumn");
							if(!strSecondColumn.equals("null")){
								org.codehaus.jettison.json.JSONArray secondColumn=lower.getJSONArray("secondColumn");
								if (secondColumn.length()>0) {
									columnList=new ArrayList<>();
									for (int i = 0; i < secondColumn.length(); i++) {
										JSONObject scJson=secondColumn.getJSONObject(i);
										SeatColumnDTO sColumn=setaDetailsColumnJson(scJson);
										columnList.add(sColumn);
									}
									lowerDTO.setSecondColumn(columnList);
								}
							}
							String strThirdColumn = lower.getString("thirdColumn");
							if(!strThirdColumn.equals("null")){
								org.codehaus.jettison.json.JSONArray thirdColumn=lower.getJSONArray("thirdColumn");
								if (thirdColumn.length()>0) {
									columnList=new ArrayList<>();
									for (int i = 0; i < thirdColumn.length(); i++) {
										JSONObject tcJson=thirdColumn.getJSONObject(i);
										SeatColumnDTO tColumn=setaDetailsColumnJson(tcJson);
										columnList.add(tColumn);
									}
									lowerDTO.setThirdColumn(columnList);
								}
							}
							String strFourthColumn = lower.getString("fourthColumn");
							if(!strFourthColumn.equals("null")){
								org.codehaus.jettison.json.JSONArray fourthColumn=lower.getJSONArray("fourthColumn");
								if (fourthColumn.length()>0) {
									columnList=new ArrayList<>();
									for (int i = 0; i < fourthColumn.length(); i++) {
										JSONObject fourthcJson=fourthColumn.getJSONObject(i);
										SeatColumnDTO frthColumn=setaDetailsColumnJson(fourthcJson);
										columnList.add(frthColumn);
									}
									lowerDTO.setFourthColumn(columnList);
								}
							}
							String strFifthColumn = lower.getString("fifthColumn");
							if(!strFifthColumn.equals("null")){
								org.codehaus.jettison.json.JSONArray fifthColumn=lower.getJSONArray("fifthColumn");
								if (fifthColumn.length()>0) {
									columnList=new ArrayList<>();
									for (int i = 0; i < fifthColumn.length(); i++) {
										JSONObject fifthcJson=fifthColumn.getJSONObject(i);
										SeatColumnDTO fthColumn=setaDetailsColumnJson(fifthcJson);
										columnList.add(fthColumn);
									}
									lowerDTO.setFifthColumn(columnList);
								}
							}
							String strSixthColumn = lower.getString("sixthColumn");
							if(!strSixthColumn.equals("null")){
								org.codehaus.jettison.json.JSONArray sixthColumn=lower.getJSONArray("sixthColumn");
								if (sixthColumn.length()>0) {
									columnList=new ArrayList<>();
									for (int i = 0; i < sixthColumn.length(); i++) {
										JSONObject sxthcJson=sixthColumn.getJSONObject(i);
										SeatColumnDTO sthColumn=setaDetailsColumnJson(sxthcJson);
										columnList.add(sthColumn);
									}
									lowerDTO.setSixthColumn(columnList);
								}
							}
							String strseventhColumn = lower.getString("seventhColumn");
							if(!strseventhColumn.equals("null")){
								org.codehaus.jettison.json.JSONArray seventhColumn=lower.getJSONArray("seventhColumn");
								if (seventhColumn.length()>0) {
									columnList=new ArrayList<>();
									for (int i = 0; i < seventhColumn.length(); i++) {
										JSONObject svnthcJson=seventhColumn.getJSONObject(i);
										SeatColumnDTO svnthColumn=setaDetailsColumnJson(svnthcJson);
										columnList.add(svnthColumn);
									}
									lowerDTO.setSeventhColumn(columnList);
								}
							}
							String streightColumn = lower.getString("eightColumn");
							if(!streightColumn.equals("null")){
								org.codehaus.jettison.json.JSONArray eightColumn=lower.getJSONArray("eightColumn");
								if (eightColumn.length()>0) {
									columnList=new ArrayList<>();
									for (int i = 0; i < eightColumn.length(); i++) {
										JSONObject ethcJson=eightColumn.getJSONObject(i);
										SeatColumnDTO ethColumn=setaDetailsColumnJson(ethcJson);
										columnList.add(ethColumn);
									}
									lowerDTO.setEightColumn(columnList);
								}
							}
							String strninethColumn = lower.getString("ninethColumn");
							if(!strninethColumn.equals("null")){
								org.codehaus.jettison.json.JSONArray ninethColumn=lower.getJSONArray("ninethColumn");
								if (ninethColumn.length()>0) {
									columnList=new ArrayList<>();
									for (int i = 0; i < ninethColumn.length(); i++) {
										JSONObject nthcJson=ninethColumn.getJSONObject(i);
										SeatColumnDTO nthColumn=setaDetailsColumnJson(nthcJson);
										columnList.add(nthColumn);
									}
									lowerDTO.setNinethColumn(columnList);
								}
							}
							result.setLower(lowerDTO);
						}else {
							result.setLower(null);
						}
					}

					/*Upper Show*/

					if (upperShow) {

						JSONObject upper=jsonObject.getJSONObject("upper");
						if (upper!=null) {

							String strfirstColumn = upper.getString("firstColumn");
							if(!strfirstColumn.equals("null")){
								org.codehaus.jettison.json.JSONArray firstColumn=upper.getJSONArray("firstColumn");
								if (firstColumn.length()>0) {
									columnList=new ArrayList<>();
									for (int i = 0; i < firstColumn.length(); i++) {
										JSONObject fcJson=firstColumn.getJSONObject(i);
										SeatColumnDTO fColumn=setaDetailsColumnJson(fcJson);
										columnList.add(fColumn);
									}
									upperDTO.setFirstColumn(columnList);
								}
							}
							String strSecondColumn = upper.getString("secondColumn");
							if(!strSecondColumn.equals("null")){
								org.codehaus.jettison.json.JSONArray secondColumn=upper.getJSONArray("secondColumn");
								if (secondColumn.length()>0) {
									columnList=new ArrayList<>();
									for (int i = 0; i < secondColumn.length(); i++) {
										JSONObject scJson=secondColumn.getJSONObject(i);
										SeatColumnDTO sColumn=setaDetailsColumnJson(scJson);
										columnList.add(sColumn);
									}
									upperDTO.setSecondColumn(columnList);
								}
							}
							String strThirdColumn = upper.getString("thirdColumn");
							if(!strThirdColumn.equals("null")){
								org.codehaus.jettison.json.JSONArray thirdColumn=upper.getJSONArray("thirdColumn");
								if (thirdColumn.length()>0) {
									columnList=new ArrayList<>();
									for (int i = 0; i < thirdColumn.length(); i++) {
										JSONObject tcJson=thirdColumn.getJSONObject(i);
										SeatColumnDTO tColumn=setaDetailsColumnJson(tcJson);
										columnList.add(tColumn);
									}
									upperDTO.setThirdColumn(columnList);
								}
							}
							String strFourthColumn = upper.getString("fourthColumn");
							if(!strFourthColumn.equals("null")){
								org.codehaus.jettison.json.JSONArray fourthColumn=upper.getJSONArray("fourthColumn");
								if (fourthColumn.length()>0) {
									columnList=new ArrayList<>();
									for (int i = 0; i < fourthColumn.length(); i++) {
										JSONObject fourthcJson=fourthColumn.getJSONObject(i);
										SeatColumnDTO frthColumn=setaDetailsColumnJson(fourthcJson);
										columnList.add(frthColumn);
									}
									upperDTO.setFourthColumn(columnList);
								}
							}
							String strFifthColumn = upper.getString("fifthColumn");
							if(!strFifthColumn.equals("null")){
								org.codehaus.jettison.json.JSONArray fifthColumn=upper.getJSONArray("fifthColumn");
								if (fifthColumn.length()>0) {
									columnList=new ArrayList<>();
									for (int i = 0; i < fifthColumn.length(); i++) {
										JSONObject fifthcJson=fifthColumn.getJSONObject(i);
										SeatColumnDTO fthColumn=setaDetailsColumnJson(fifthcJson);
										columnList.add(fthColumn);
									}
									upperDTO.setFifthColumn(columnList);
								}
							}
							String strSixthColumn = upper.getString("sixthColumn");
							if(!strSixthColumn.equals("null")){
								org.codehaus.jettison.json.JSONArray sixthColumn=upper.getJSONArray("sixthColumn");
								if (sixthColumn.length()>0) {
									columnList=new ArrayList<>();
									for (int i = 0; i < sixthColumn.length(); i++) {
										JSONObject sxthcJson=sixthColumn.getJSONObject(i);
										SeatColumnDTO sthColumn=setaDetailsColumnJson(sxthcJson);
										columnList.add(sthColumn);
									}
									upperDTO.setSixthColumn(columnList);
								}
							}
							String strseventhColumn = upper.getString("seventhColumn");
							if(!strseventhColumn.equals("null")){
								org.codehaus.jettison.json.JSONArray seventhColumn=upper.getJSONArray("seventhColumn");
								if (seventhColumn.length()>0) {
									columnList=new ArrayList<>();
									for (int i = 0; i < seventhColumn.length(); i++) {
										JSONObject svnthcJson=seventhColumn.getJSONObject(i);
										SeatColumnDTO svnthColumn=setaDetailsColumnJson(svnthcJson);
										columnList.add(svnthColumn);
									}
									upperDTO.setSeventhColumn(columnList);
								}
							}
							String streightColumn = upper.getString("eightColumn");
							if(!streightColumn.equals("null")){
								org.codehaus.jettison.json.JSONArray eightColumn=upper.getJSONArray("eightColumn");
								if (eightColumn.length()>0) {
									columnList=new ArrayList<>();
									for (int i = 0; i < eightColumn.length(); i++) {
										JSONObject ethcJson=eightColumn.getJSONObject(i);
										SeatColumnDTO ethColumn=setaDetailsColumnJson(ethcJson);
										columnList.add(ethColumn);
									}
									upperDTO.setEightColumn(columnList);
								}
							}
							String strninethColumn = upper.getString("ninethColumn");
							if(!strninethColumn.equals("null")){
								org.codehaus.jettison.json.JSONArray ninethColumn=upper.getJSONArray("ninethColumn");
								if (ninethColumn.length()>0) {
									columnList=new ArrayList<>();
									for (int i = 0; i < ninethColumn.length(); i++) {
										JSONObject nthcJson=ninethColumn.getJSONObject(i);
										SeatColumnDTO nthColumn=setaDetailsColumnJson(nthcJson);
										columnList.add(nthColumn);
									}
									upperDTO.setNinethColumn(columnList);
								}
							}
							result.setUpper(upperDTO);
						}else {
							result.setUpper(null);
						}
					}
					String strlistBoardingPoint = jsonObject.getString("listBoardingPoints");
					if(!strlistBoardingPoint.equals("null")){
						org.codehaus.jettison.json.JSONArray listBoardingPoint=jsonObject.getJSONArray("listBoardingPoints");

						if (listBoardingPoint.length()>0) {
							for (int i = 0; i < listBoardingPoint.length(); i++) {
								JSONObject jobj=listBoardingPoint.getJSONObject(i);

								listBoardingPoints.add(boardingPointJson(jobj));
							}
							result.setListBoardingPoints(listBoardingPoints);
						}
						else {
							result.setListBoardingPoints(null);
						}
					}
					String strDrop = jsonObject.getString("listDropingPoints");
					if(!strDrop.equals("null")){

						org.codehaus.jettison.json.JSONArray listDropPoint=jsonObject.getJSONArray("listDropingPoints");

						if (listDropPoint.length()>0) {
							for (int i = 0; i < listDropPoint.length(); i++) {
								JSONObject jobj=listDropPoint.getJSONObject(i);

								listDropingPoints.add(dropingPointJson(jobj));
							}
							result.setListDropingPoints(listDropingPoints);
						}else {
							result.setListDropingPoints(null);
						}
					}

					result.setUpperShow(upperShow);
					result.setLowerShow(lowerShow);
					result.setMaxcolumn(jsonObject.getInt("maxcolumn"));
					result.setMaxrow(jsonObject.getInt("maxrow"));
					result.setSeatAcFare(jsonObject.getDouble("seatAcFare"));
					result.setSeatNacFare(jsonObject.getDouble("seatNacFare"));
					result.setSleepAcFare(jsonObject.getDouble("sleepAcFare"));
					result.setSleepNacFare(jsonObject.getDouble("sleepNacFare"));
					result.setMinFare(jsonObject.getDouble("minFare"));
					result.setMaxFare(jsonObject.getDouble("maxFare"));

					/*result.setCode(ResponseStatus.SUCCESS.getValue());
					result.setStatus(ResponseStatus.SUCCESS.getKey());
					result.setMessage("Get Seat Details");*/

					resp.setCode(ResponseStatus.SUCCESS.getValue());
					resp.setStatus(ResponseStatus.SUCCESS.getKey());
					resp.setMessage(jsonObject.getString("message"));
					resp.setDetails(result);

				} else {
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus(ResponseStatus.FAILURE.getKey());
				}
			}


		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus(ResponseStatus.FAILURE.getKey());
		}

		return resp;

	}



	private SeatColumnDTO setaDetailsColumnJson(JSONObject jsonObject) 
	{
		SeatColumnDTO dto=new SeatColumnDTO();
		try {
			dto.setAvailable(jsonObject.getBoolean("available"));
			dto.setBaseFare(jsonObject.getDouble("baseFare"));
			dto.setColumn(jsonObject.getString("column"));
			dto.setLadiesSeat(jsonObject.getString("ladiesSeat"));
			dto.setLength(jsonObject.getString("length"));
			dto.setSeatAvail(jsonObject.getInt("seatAvail"));
			dto.setName(jsonObject.getString("name"));
			dto.setRow(jsonObject.getString("row"));
			dto.setId(jsonObject.getString("id"));
			dto.setSeatStyle(jsonObject.getString("seatStyle"));
			dto.setWidth(jsonObject.getString("width"));
			dto.setzIndex(jsonObject.getString("zIndex"));
			dto.setFare(jsonObject.getDouble("fare"));
			dto.setSeatType(jsonObject.getString("seatType"));
			dto.setImageUrl(jsonObject.getString("imageUrl"));
			dto.setSleeper(jsonObject.getBoolean("sleeper"));
			dto.setAc(jsonObject.getBoolean("ac"));
			dto.setUpperShow(jsonObject.getBoolean("upperShow"));
			dto.setLowerShow(jsonObject.getBoolean("lowerShow"));
			dto.setGender(jsonObject.getString("gender"));
			dto.setColumnNo(jsonObject.getInt("columnNo"));
			dto.setRowNo(jsonObject.getInt("rowNo"));

		} catch (JSONException e) {

			System.err.println("Sround with try catch");
			e.printStackTrace();
		}
		return dto;
	}





	@Override
	public ResponseDTO getTxnId(String sessionId,GetTransactionId request,UserDetailsResponse uDetailsResponse ) {

		ResponseDTO resp=new ResponseDTO();
		TranasctionIdResponse result=new TranasctionIdResponse();

		try {

			Client client = Client.create();
			WebResource webResource = client
					.resource(UrlMetadatas.agentGetTxnId(Role.ClIENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(request.toString())).post(ClientResponse.class, request.getJson());

			System.err.println(response);

			if (response.getStatus() != 200) {
				System.err.println("RESPONSE CODE :: " + response.getStatus());
				resp.setCode("F00");
				resp.setMessage("Service Unavailable");
				resp.setStatus(ResponseStatus.FAILURE.getKey());

			} else {
				String strResponse = response.getEntity(String.class);
				System.err.println("Payment Response :: " + strResponse);
				if (strResponse != null) {
					JSONObject jsonObject=new JSONObject(strResponse);
					System.err.println("jsonObject:: "+jsonObject);
					if (jsonObject.getBoolean("transactionCreated")==true) {

						JSONObject payload=new JSONObject();
						payload.put("sessionId", sessionId);
						payload.put("userMobile", request.getMobileNo());
						payload.put("emtTxnId", jsonObject.getString("transactionid"));
						payload.put("emtTransactionScreenId", jsonObject.getString("emtTransactionScreenId"));
						payload.put("busId", request.getBusid());
						payload.put("totalFare", request.getTotalFare());
						payload.put("userEmail", request.getEmail());
						payload.put("busType", request.getBusType());
						payload.put("boardId", request.getBoardId());
						payload.put("boardName", request.getBoardName());
						payload.put("arrTime", request.getArrTime());
						payload.put("deptTime", request.getDepTime());
						payload.put("travelName", request.getTravelName());
						payload.put("source", request.getSource());
						payload.put("journeyDate", request.getJourneyDate());
						payload.put("travellers", request.getTravellerDetails().getJSONArray("travellers"));
						payload.put("destination", request.getDestination());
						payload.put("getMdexTxnIdResp", strResponse);
						payload.put("boardTime", request.getBoardTime());

						Client vpqClient = Client.create();
						client.addFilter(new LoggingFilter(System.out));
						WebResource vpqWebResource = vpqClient.resource(
								UrlMetadatas.getBusInitiateURL(Version.VERSION_1,Role.AGENT, Device.WEBSITE, Language.ENGLISH));
						ClientResponse vpqresponse = vpqWebResource.accept("application/json").type("application/json")
								.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);

						String vpqstrResponse = vpqresponse.getEntity(String.class);

						if (vpqstrResponse!=null) {
							JSONObject vJObj=new JSONObject(vpqstrResponse);
							String status=vJObj.getString("status");
							String code=vJObj.getString("code");
							String vMessage=vJObj.getString("message");
							String details=vJObj.getString("details");

							JSONObject cancelPayload = new JSONObject();


							if (code.equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
								resp.setStatus(status);
								resp.setCode(code);
								result.setError(jsonObject.getString("error"));
								result.setTransactionCreated(jsonObject.getBoolean("transactionCreated"));
								result.setTransactionid(jsonObject.getString("transactionid"));
								result.setValidFor(jsonObject.getString("validFor"));
								result.setMpQTxnId(jsonObject.getString("pQTxnId"));
								result.setTransactionid(jsonObject.getString("transactionid"));
								result.setVpQTxnId(vJObj.getJSONObject("details").getString("transactionRefNo"));
								resp.setDetails(result);
								resp.setMessage(vMessage);

								cancelPayload.put("clientIp", "49.204.86.246");
								cancelPayload.put("cllientKey", UrlMetadatas.MDEX_CLIENTKEY);
								cancelPayload.put("clientToken", UrlMetadatas.MDEX_CLIENTTOKEN);
								cancelPayload.put("clientApiName", "VPayQwik");
								cancelPayload.put("pQTxnId",jsonObject.getString("pQTxnId"));
								cancelPayload.put("transactionId", jsonObject.getString("transactionid"));
							}
							else if (code.equalsIgnoreCase("T01")) {

								WebResource cancelwebResource = client
										.resource(UrlMetadatas.agentGetCancelInitMdexPayment(Role.ClIENT, Device.WEBSITE, Language.ENGLISH));
								ClientResponse cancelresponse = cancelwebResource.accept("application/json").type("application/json")
										.header("hash", SecurityUtils.getHash(cancelPayload.toString())).post(ClientResponse.class, cancelPayload);

								String mdexstrResponse = cancelresponse.getEntity(String.class);
								System.err.println(mdexstrResponse);
								resp.setStatus(status);
								resp.setCode("F00");
								resp.setMessage(vMessage);
							}
							else {
								WebResource cancelwebResource = client
										.resource(UrlMetadatas.getCancelInitMdexPayment(Role.ClIENT, Device.WEBSITE, Language.ENGLISH));
								ClientResponse cancelresponse = cancelwebResource.accept("application/json").type("application/json")
										.header("hash", SecurityUtils.getHash(cancelPayload.toString())).post(ClientResponse.class, cancelPayload);

								String mdexstrResponse = cancelresponse.getEntity(String.class);
								System.err.println(mdexstrResponse);

								resp.setStatus(status);
								resp.setCode(code);
								resp.setMessage(vMessage);
							}
						}

						System.err.println("APP Response:: "+vpqstrResponse);


					}else {
						resp.setCode(ResponseStatus.FAILURE.getValue());
						resp.setStatus(ResponseStatus.FAILURE.getKey());
						String error=jsonObject.getString("error");
						if (error!=null) {
							if(!error.equalsIgnoreCase("null")){
								resp.setMessage(error);
							}
							else {
								resp.setMessage("Ticket not booked. Please contact customer care");
							}
						}else {
							resp.setMessage("Ticket not booked. Please contact customer care");
						}

					}				

				} else {
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus(ResponseStatus.FAILURE.getKey());
				}
			}

			if (!resp.getCode().equalsIgnoreCase("T01")) {
				resp.setDetails(result);
			}


		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus(ResponseStatus.FAILURE.getKey());
		}

		return resp;
	}


	@Override
	public ResponseDTO bookTicket(BookTicketReq req) {

		ResponseDTO resp=new ResponseDTO();
		BookTicketResp result=new BookTicketResp();

		try {

			JSONObject payload = new JSONObject();
			payload.put("clientIp", "49.204.86.246");
			payload.put("cllientKey", UrlMetadatas.MDEX_CLIENTKEY);
			payload.put("clientToken", UrlMetadatas.MDEX_CLIENTTOKEN);
			payload.put("clientApiName", "VPayQwik");
			payload.put("pQTxnId",req.getMdexTxnId());
			payload.put("transactionId", req.getTransactionId());

			JSONObject cancelPayload = new JSONObject();
			cancelPayload.put("clientIp", "49.204.86.246");
			cancelPayload.put("cllientKey", UrlMetadatas.MDEX_CLIENTKEY);
			cancelPayload.put("clientToken", UrlMetadatas.MDEX_CLIENTTOKEN);
			cancelPayload.put("clientApiName", "VPayQwik");
			cancelPayload.put("pQTxnId",req.getMdexTxnId());
			cancelPayload.put("transactionId", req.getTransactionId());

			Client client = Client.create();
			WebResource webResource = client
					.resource(UrlMetadatas.getBookTicket(Role.ClIENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);

			System.err.println(response);

			if (response.getStatus() != 200) {

				WebResource cancelwebResource = client.resource(UrlMetadatas.getCancelInitMdexPayment(Role.ClIENT, Device.WEBSITE, Language.ENGLISH));
				ClientResponse cancelresponse = cancelwebResource.accept("application/json").type("application/json")
						.header("hash", SecurityUtils.getHash(cancelPayload.toString())).post(ClientResponse.class, cancelPayload);

				String mdexstrResponse = cancelresponse.getEntity(String.class);
				System.err.println(mdexstrResponse);

				System.err.println("RESPONSE CODE :: " + response.getStatus());
				resp.setCode("F00");
				resp.setMessage("Service Unavailable");
				resp.setStatus(ResponseStatus.FAILURE.getValue());
				resp.setMdexStatus(ResponseStatus.FAILURE.getKey());
			} else {
				String strResponse = response.getEntity(String.class);
				System.err.println("Payment Response :: " + strResponse);
				resp.setMdexBusBookingResp(strResponse);
				if (strResponse != null) {

					JSONObject jObj=new JSONObject(strResponse);

					final boolean isTicket=jObj.getBoolean("ticket");
					final String ticketPnr=jObj.getString("ticketPnr"); 
					final String operatorPnr=jObj.getString("operatorPnr"); 
					final String message=jObj.getString("message"); 

					if (isTicket) {
						resp.setMdexStatus(ResponseStatus.SUCCESS.getKey());
						JSONObject vpayload=new JSONObject();
						vpayload.put("sessionId", req.getSessionId());
						vpayload.put("ticketPnr", ticketPnr);
						vpayload.put("operatorPnr",operatorPnr);
						vpayload.put("pQTxnId", req.getVpqTxnId());
						vpayload.put("success",isTicket);
						vpayload.put("mdexBookingResp", strResponse);
						Client vpqClient = Client.create();
						client.addFilter(new LoggingFilter(System.out));
						WebResource vpqWebResource = vpqClient.resource(
								UrlMetadatas.getBusPaymentURL(Version.VERSION_1,Role.AGENT, Device.WEBSITE, Language.ENGLISH));
						ClientResponse vpqresponse = vpqWebResource.accept("application/json").type("application/json")
								.header("hash", SecurityUtils.getHash(vpayload.toString())).post(ClientResponse.class, vpayload);

						String vpqstrResponse = vpqresponse.getEntity(String.class);

						resp.setVpayQwikBusBookingResp(vpqstrResponse);

						if (vpqstrResponse!=null) {
							JSONObject vJObj=new JSONObject(vpqstrResponse);
							String status=vJObj.getString("status");
							String code=vJObj.getString("code");
							String vMessage=vJObj.getString("message");

							if (code.equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
								resp.setStatus(status);
								resp.setCode(code);
								resp.setMessage(vMessage);
								result.setTicket(isTicket);
								result.setTicketPnr(ticketPnr);
								result.setOperatorPnr(operatorPnr);
								result.setMessage(message);
								resp.setDetails(result);
							}else {
								resp.setStatus(status);
								resp.setCode(code);
								resp.setMessage(vMessage);
							}
						}
						System.err.println("APP Response:: "+vpqstrResponse);

					}else {
						resp.setMdexStatus(ResponseStatus.FAILURE.getKey());
						WebResource cancelwebResource = client
								.resource(UrlMetadatas.getCancelInitMdexPayment(Role.ClIENT, Device.WEBSITE, Language.ENGLISH));
						ClientResponse cancelresponse = cancelwebResource.accept("application/json").type("application/json")
								.header("hash", SecurityUtils.getHash(cancelPayload.toString())).post(ClientResponse.class, cancelPayload);

						String mdexstrResponse = cancelresponse.getEntity(String.class);
						System.err.println(mdexstrResponse);

						resp.setStatus(ResponseStatus.FAILURE.getKey());
						resp.setCode(ResponseStatus.FAILURE.getValue());
						resp.setMessage(message);
					}
				} else {
					resp.setMdexStatus(ResponseStatus.FAILURE.getKey());
					WebResource cancelwebResource = client
							.resource(UrlMetadatas.getCancelInitMdexPayment(Role.ClIENT, Device.WEBSITE, Language.ENGLISH));
					ClientResponse cancelresponse = cancelwebResource.accept("application/json").type("application/json")
							.header("hash", SecurityUtils.getHash(cancelPayload.toString())).post(ClientResponse.class, cancelPayload);

					String mdexstrResponse = cancelresponse.getEntity(String.class);
					System.err.println(mdexstrResponse);

					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus(ResponseStatus.FAILURE.getValue());
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus(ResponseStatus.FAILURE.getValue());
		}
		return resp;
	}



	@Override
	public ResponseDTO isCancellable(IsCancellableReq req) {

		ResponseDTO resp=new ResponseDTO();
		IsCancellableResp result=new IsCancellableResp();
		try {

			JSONObject payload = new JSONObject();
			payload.put("clientIp", "49.204.86.246");
			payload.put("cllientKey", UrlMetadatas.MDEX_CLIENTKEY);
			payload.put("clientToken", UrlMetadatas.MDEX_CLIENTTOKEN);
			payload.put("clientApiName", "VPayQwik");
			payload.put("bookId",req.getBookId());
			payload.put("canceltype",req.getCanceltype());
			payload.put("seatNo",req.getSeatNo());	
			payload.put("ipAddress",req.getIpAddress());


			Client client = Client.create();
			WebResource webResource = client
					.resource(UrlMetadatas.isCancelable(Role.ClIENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);

			System.err.println(response);

			if (response.getStatus() != 200) {
				System.err.println("RESPONSE CODE :: " + response.getStatus());
				resp.setCode("F00");
				resp.setMessage("Service Unavailable");
				resp.setStatus(ResponseStatus.FAILURE.getValue());

			} else {
				String strResponse = response.getEntity(String.class);
				System.err.println("Payment Response :: " + strResponse);
				if (strResponse != null) {

					JSONObject jObj=new JSONObject(strResponse);

			//		final boolean isCancellable=jObj.getBoolean("isCancellable");
					final boolean isCancellable=jObj.getBoolean("cancellable");
					final double refundAmount=jObj.getDouble("refundAmount");
					final double cancellationCharges=jObj.getDouble("cancellationCharges");
					final String seatNo=jObj.getString("seatNo"); 

					if (isCancellable) {
						resp.setStatus(ResponseStatus.SUCCESS.getKey());
						resp.setCode(ResponseStatus.SUCCESS.getValue());
					}else {
						resp.setStatus(ResponseStatus.FAILURE.getKey());
						resp.setCode(ResponseStatus.FAILURE.getValue());
					}
					result.setMessage("Get Cancellation Details");
					result.setCancellable(isCancellable);
					result.setCancellationCharges(cancellationCharges);
					result.setRefundAmount(refundAmount);
					result.setSeatNo(seatNo);
					resp.setDetails(result);
				} else {
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus(ResponseStatus.FAILURE.getValue());
				}
			}


		} catch (Exception e) {
			e.printStackTrace();
		}

		return resp;
	}

	@Override
	public ResponseDTO cancelTicket(IsCancellableReq req) {

		ResponseDTO resp=new ResponseDTO();
		CancelTicketResp result=new CancelTicketResp();
		try {

			JSONObject payload = new JSONObject();
			payload.put("clientIp", "49.204.86.246");
			payload.put("cllientKey", UrlMetadatas.MDEX_CLIENTKEY);
			payload.put("clientToken", UrlMetadatas.MDEX_CLIENTTOKEN);
			payload.put("clientApiName", "VPayQwik");
			payload.put("bookId",req.getBookId());
			payload.put("canceltype",req.getCanceltype());
			payload.put("seatNo",req.getSeatNo());	
			payload.put("ipAddress",req.getIpAddress());

			Client client = Client.create();
			WebResource webResource = client
					.resource(UrlMetadatas.cancelTicket(Role.ClIENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);

			System.err.println(response);

			if (response.getStatus() != 200) {
				System.err.println("RESPONSE CODE :: " + response.getStatus());
				resp.setCode("F00");
				resp.setMessage("Service Unavailable");
				resp.setStatus(ResponseStatus.FAILURE.getValue());

			} else {
				String strResponse = response.getEntity(String.class);
				System.err.println("Payment Response :: " + strResponse);
				if (strResponse != null) {

					JSONObject jObj=new JSONObject(strResponse);

					final boolean isCancelRequested=jObj.getBoolean("isCancelRequested");
					final double refundAmount=jObj.getDouble("refundAmount");
					final double cancellationCharges=jObj.getDouble("cancellationCharges");
					final boolean cancelStatus=jObj.getBoolean("cancelStatus");
					final String pnrNo=jObj.getString("PNRNo");
					final String operatorPnr=jObj.getString("operatorPnr");
					final String remarks=jObj.getString("Remarks");

					if (isCancelRequested) {
						resp.setStatus(ResponseStatus.SUCCESS.getKey());
						resp.setCode(ResponseStatus.SUCCESS.getValue());



					}else {
						resp.setStatus(ResponseStatus.FAILURE.getKey());
						resp.setCode(ResponseStatus.FAILURE.getValue());
					}
					resp.setMessage("Get Cancellation Details");
					result.setCancellationCharges(cancellationCharges);
					result.setRefundAmount(refundAmount);
					result.setCancelRequested(isCancelRequested);
					result.setCancelStatus(cancelStatus);
					result.setPnrNo(pnrNo);

					/*final String cancelSeatNo=jObj.getString("cancelSeatNo");
					if (cancelSeatNo!=null) {
						org.codehaus.jettison.json.JSONArray cancelSeats=jObj.getJSONArray("cancelSeatNo");
						for (int i = 0; i < cancelSeats.length(); i++) {
							cList.add((String)cancelSeats.get(i));
						}
					}*/

					//result.setCancelSeatNo(cancelSeatNo);

					result.setOperatorPnr(operatorPnr);
					result.setRemarks(remarks);
					resp.setDetails(result);

				} else {
					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus(ResponseStatus.FAILURE.getValue());
				}
			}


		} catch (Exception e) {
			e.printStackTrace();
		}

		return resp;
	}


	@Override
	public ResponseDTO cancelInitPayment(BookTicketReq dto) {

		ResponseDTO resp=new ResponseDTO();

		try {

			JSONObject vpayload=new JSONObject();
			vpayload.put("pQTxnId", dto.getVpqTxnId());
			vpayload.put("sessionId", dto.getSessionId());

			Client vpqClient = Client.create();
			vpqClient.addFilter(new LoggingFilter(System.out));
			WebResource vpqWebResource = vpqClient.resource(
					UrlMetadatas.getCancelInitBusPaymentURL(Version.VERSION_1,Role.AGENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse vpqresponse = vpqWebResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(vpayload.toString())).post(ClientResponse.class, vpayload);

			if (vpqresponse!=null) {
				if (vpqresponse.getStatus() != 200) {
					System.err.println("RESPONSE CODE :: " + vpqresponse.getStatus());
					resp.setCode("F00");
					resp.setMessage("Service Unavailable");
					resp.setStatus(ResponseStatus.FAILURE.getValue());

				} else {
					String vpqstrResponse = vpqresponse.getEntity(String.class);
					System.err.println("Payment Response :: " + vpqstrResponse);
					if (vpqstrResponse != null) {

						JSONObject vJObj=new JSONObject(vpqstrResponse);

						JSONObject payload = new JSONObject();
						payload.put("clientIp", "49.204.86.246");
						payload.put("cllientKey", UrlMetadatas.MDEX_CLIENTKEY);
						payload.put("clientToken", UrlMetadatas.MDEX_CLIENTTOKEN);
						payload.put("clientApiName", "VPayQwik");
						payload.put("pQTxnId",dto.getMdexTxnId());
						payload.put("transactionId", dto.getTransactionId());

						Client client = Client.create();
						WebResource webResource = client
								.resource(UrlMetadatas.getCancelInitMdexPayment(Role.ClIENT, Device.WEBSITE, Language.ENGLISH));
						ClientResponse response = webResource.accept("application/json").type("application/json")
								.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);

						resp.setStatus(ResponseStatus.SUCCESS.getKey());
						resp.setCode(ResponseStatus.SUCCESS.getValue());
						resp.setMessage("Transection Cancel Successfully");

					} else {
						resp.setCode("F00");
						resp.setMessage("Service unavailable");
						resp.setStatus(ResponseStatus.FAILURE.getValue());
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return resp;
	}

	@Override
	public ResponseDTO cronCheck(String sessionId) {
		ResponseDTO resp=new ResponseDTO();

		List<BusCityListDTO> list=new ArrayList<>();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", sessionId);

			Client vpqClient = Client.create();
			vpqClient.addFilter(new LoggingFilter(System.out));
			WebResource vpqWebResource = vpqClient.resource(
					UrlMetadatas.cronCheck(Version.VERSION_1,Role.AGENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse vpqresponse = vpqWebResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);


			String strResponse = vpqresponse.getEntity(String.class);
			System.out.println(strResponse);
			JSONObject jobj=new JSONObject(strResponse);
			final String code=jobj.getString("code");
			if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
				String strDetails=jobj.getString("details");
				if (strDetails!=null) {
					JSONArray cList=jobj.getJSONArray("details");

					for (int i = 0; i < cList.length(); i++) {
						BusCityListDTO dto=new BusCityListDTO();
						dto.setCityId(cList.getJSONObject(i).getLong("cityId"));
						dto.setCityName(cList.getJSONObject(i).getString("cityName"));
						list.add(dto);
					}
				}
				resp.setCode(code);
				resp.setMessage("Get All City List");
				resp.setDetails(list);
				resp.setStatus(ResponseStatus.SUCCESS.getKey());
			}
		} catch (Exception e) {
			System.out.println(e);
			resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			resp.setMessage("Service Unavailable");
			resp.setDetails(null);
			resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
		}
		return resp;
	}



	@Override
	public ResponseDTO getTXnBySplitPayment(String sessionId,GetTransactionId request,String emtTxnId) {

		ResponseDTO resp=new ResponseDTO();
		TranasctionIdResponse result=new TranasctionIdResponse();
		try {
			JSONObject payload=new JSONObject();
			payload.put("sessionId", sessionId);
			payload.put("userMobile", request.getMobileNo());
			payload.put("emtTxnId", emtTxnId);
			payload.put("busId", request.getBusid());
			payload.put("totalFare", request.getTotalFare());
			Client vpqClient = Client.create();
			vpqClient.addFilter(new LoggingFilter(System.out));
			WebResource vpqWebResource = vpqClient.resource(
					UrlMetadatas.getBusInitiateURL(Version.VERSION_1,Role.AGENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse vpqresponse = vpqWebResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);

			String vpqstrResponse = vpqresponse.getEntity(String.class);

			if (vpqstrResponse!=null) {
				JSONObject vJObj=new JSONObject(vpqstrResponse);
				String status=vJObj.getString("status");
				String code=vJObj.getString("code");
				String vMessage=vJObj.getString("message");
				String details=vJObj.getString("details");

				if (code.equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
					resp.setStatus(status);
					resp.setCode(code);
					resp.setMessage(vMessage);
					resp.setDetails(vJObj.getJSONObject("details").getString("transactionRefNo"));
				}
				else {
					resp.setStatus(status);
					resp.setCode(code);
					resp.setMessage(vMessage);
				}
			}
		} catch (Exception e) {
			System.out.println(e);
			resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			resp.setMessage("Service Unavailable");
			resp.setDetails(null);
			resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
		}
		return resp;
	}


	@Override
	public ResponseDTO getAllBookTickets(String sessionId) {

		ResponseDTO resp=new ResponseDTO();

		List<Object> list=new ArrayList<>();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", sessionId);

			Client vpqClient = Client.create();
			vpqClient.addFilter(new LoggingFilter(System.out));
			WebResource vpqWebResource = vpqClient.resource(
					UrlMetadatas.getAllTicketByUser(Version.VERSION_1,Role.AGENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse vpqresponse = vpqWebResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);


			String strResponse = vpqresponse.getEntity(String.class);
			System.out.println(strResponse);
			JSONObject jobj=new JSONObject(strResponse);
			final String code=jobj.getString("code");
			if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
				String details=jobj.getString("details");

				resp.setCode(code);
				resp.setMessage("Get All Book Tickets");
				resp.setDetails(details);
				resp.setStatus(ResponseStatus.SUCCESS.getKey());
			}
		} catch (Exception e) {
			System.out.println(e);
			resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			resp.setMessage("Service Unavailable");
			resp.setDetails(null);
			resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
		}
		return resp;
	}



	@Override
	public ResponseDTO getAllMyBookTicketsForWeb(String sessionId) {

		ResponseDTO resp=new ResponseDTO();

		List<BusTicketDTO> list=new ArrayList<>();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", sessionId);

			Client vpqClient = Client.create();
			vpqClient.addFilter(new LoggingFilter(System.out));
			WebResource vpqWebResource = vpqClient.resource(
					UrlMetadatas.agentGetAllTicketByUserForWeb(Version.VERSION_1,Role.AGENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse vpqresponse = vpqWebResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);


			String strResponse = vpqresponse.getEntity(String.class);
			System.out.println(strResponse);
			JSONObject jobj=new JSONObject(strResponse);
			final String code=jobj.getString("code");
			if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
				JSONArray details=jobj.getJSONArray("details");

				for (int i = 0; i < details.length(); i++) {
					BusTicketDTO dto=new BusTicketDTO();
					String source=(String)details.getJSONObject(i).getJSONObject("busTicket").get("source");
					String destination=(String)details.getJSONObject(i).getJSONObject("busTicket").get("destination");
					String journeyDate=(String)details.getJSONObject(i).getJSONObject("busTicket").get("journeyDate");
					String arrTime=(String)details.getJSONObject(i).getJSONObject("busTicket").get("arrTime");
					double totalFare=(double)details.getJSONObject(i).getJSONObject("busTicket").getDouble("totalFare");
					String transactionRefNo=(String)details.getJSONObject(i).getJSONObject("busTicket").getJSONObject("transaction").get("transactionRefNo");
					String status=(String)details.getJSONObject(i).getJSONObject("busTicket").get("status");
					String emtTxnId=(String)details.getJSONObject(i).getJSONObject("busTicket").get("emtTxnId");
					dto.setArrTime(arrTime);
					dto.setDestination(destination);
					dto.setJourneyDate(journeyDate);
					dto.setSource(source);
					dto.setTotalFare(totalFare);
					dto.setTransactionRefNo(transactionRefNo);
					dto.setStatus(status);
					dto.setEmtTxnId(emtTxnId);
					list.add(dto);

				}
				resp.setCode(code);
				resp.setMessage("Get All Book Tickets");
				resp.setDetails(list);
				resp.setStatus(ResponseStatus.SUCCESS.getKey());
			}
		} catch (Exception e) {
			System.out.println(e);
			resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			resp.setMessage("Service Unavailable");
			resp.setDetails(null);
			resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
		}
		return resp;
	}


	@Override
	public ResponseDTO getSingleTicketTravellerDetails(String sessionId, String emtTxnNo) {

		ResponseDTO resp=new ResponseDTO();

		List<TravellerDetailsDTO> list=new ArrayList<>();
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", sessionId);
			payload.put("bookingTxnId", emtTxnNo);
			Client vpqClient = Client.create();
			vpqClient.addFilter(new LoggingFilter(System.out));
			WebResource vpqWebResource = vpqClient.resource(
					UrlMetadatas.agentGetSingleTicketTravellerDetails(Version.VERSION_1,Role.AGENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse vpqresponse = vpqWebResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);


			String strResponse = vpqresponse.getEntity(String.class);
			System.out.println(strResponse);
			JSONObject jobj=new JSONObject(strResponse);
			final String code=jobj.getString("code");
			if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
				JSONArray details=jobj.getJSONArray("details");

				for (int i = 0; i < details.length(); i++) {
					TravellerDetailsDTO dto=new TravellerDetailsDTO();

					dto.setfName((String)details.getJSONObject(i).get("fName"));
					dto.setlName((String)details.getJSONObject(i).get("lName"));
					dto.setGender((String)details.getJSONObject(i).get("gender"));
					dto.setSeatNo((String)details.getJSONObject(i).get("seatNo"));
					dto.setFare((String)details.getJSONObject(i).get("fare"));
					list.add(dto);
				}

				if (details.length()>=0) {
					BusTicketDTO busTicketDTO=new BusTicketDTO();

					String source=(String)details.getJSONObject(0).getJSONObject("busTicketId").get("source");
					String destination=(String)details.getJSONObject(0).getJSONObject("busTicketId").get("destination");
					String journeyDate=(String)details.getJSONObject(0).getJSONObject("busTicketId").get("journeyDate");
					String operatorPnr=(String)details.getJSONObject(0).getJSONObject("busTicketId").get("operatorPnr");
					String busOperator=(String)details.getJSONObject(0).getJSONObject("busTicketId").get("busOperator");
					String ticketPnr=(String)details.getJSONObject(0).getJSONObject("busTicketId").get("ticketPnr");
					double totalFare=(double)details.getJSONObject(0).getJSONObject("busTicketId").getDouble("totalFare");
					String emtTxnId=(String)details.getJSONObject(0).getJSONObject("busTicketId").get("emtTxnId");
					String boardingAddress=(String)details.getJSONObject(0).getJSONObject("busTicketId").get("boardingAddress");
					String arrTime=(String)details.getJSONObject(0).getJSONObject("busTicketId").get("arrTime");
					long transactionRefNo=(long)details.getJSONObject(0).getJSONObject("busTicketId").getJSONObject("transaction").getLong("transactionRefNo");
					busTicketDTO.setSource(source);
					busTicketDTO.setDestination(destination);
					busTicketDTO.setJourneyDate(journeyDate);
					busTicketDTO.setOperatorPnr(operatorPnr);
					busTicketDTO.setBusOperator(busOperator);
					busTicketDTO.setTicketPnr(ticketPnr);
					busTicketDTO.setTotalFare(totalFare);
					busTicketDTO.setEmtTxnId(emtTxnId);
					busTicketDTO.setBoardingAddress(boardingAddress);
					busTicketDTO.setArrTime(arrTime);
					busTicketDTO.setTransactionRefNo(transactionRefNo+"");
					long bookingDate=(long)details.getJSONObject(0).getJSONObject("busTicketId").getJSONObject("transaction").getLong("created");

					Date date=new Date(bookingDate); 
					String txnDate=dateFormat.format(date);

					busTicketDTO.setTxnDate(txnDate);
					resp.setExtraInfo(busTicketDTO);

				}

			}
			resp.setCode(code);
			resp.setMessage("Get All Book Tickets");
			resp.setDetails(list);
			resp.setStatus(ResponseStatus.SUCCESS.getKey());

		} catch (Exception e) {
			System.out.println(e);
			resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			resp.setMessage("Service Unavailable");
			resp.setDetails(null);
			resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
		}
		return resp;

	}


	@Override
	public ResponseDTO getAllCityListByEMT(String sessionId) {

		ResponseDTO resp=new ResponseDTO();

		int size=0;
		List<BusCityListDTO> list=new ArrayList<>();
		try {
			JSONObject payload = new JSONObject();
			payload.put("clientIp", "49.204.86.246");
			payload.put("cllientKey", UrlMetadatas.MDEX_CLIENTKEY);
			payload.put("clientToken", UrlMetadatas.MDEX_CLIENTTOKEN);
			payload.put("clientApiName", "VPayQwik");
			//			payload.put("sessionId", sessionId);

			Client client = Client.create();
			WebResource webResource = client
					.resource(UrlMetadatas.getBusCityList(Role.ClIENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);

			String strResponse = response.getEntity(String.class);
			System.out.println(strResponse);
			JSONObject jobj=new JSONObject(strResponse);
			final String code=jobj.getString("code");
			if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
				String strDetails=jobj.getString("details");
				if (strDetails!=null) {
					JSONArray cList=jobj.getJSONArray("details");

					for (int i = 0; i < cList.length(); i++) {
						BusCityListDTO dto=new BusCityListDTO();
						dto.setCityId(cList.getJSONObject(i).getLong("cityId"));
						dto.setCityName(cList.getJSONObject(i).getString("cityName"));
						list.add(dto);
					}
				}
				resp.setCode(code);
				resp.setMessage("Get All City List");
				resp.setDetails(list);
				resp.setStatus(ResponseStatus.SUCCESS.getKey());
			}
		} catch (Exception e) {
			System.out.println(e);
			resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			resp.setMessage("Service Unavailable");
			resp.setDetails(null);
			resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
		}
		return resp;
	}


	//  Changes for re price

	@Override
	public ResponseDTO getTxnIdUpdated(String session, GetTransactionId request, UserDetailsResponse userDetailsResponse) {

		ResponseDTO resp=new ResponseDTO();
		TranasctionIdResponse result=new TranasctionIdResponse();

		try {

			Client client = Client.create();
			WebResource webResource = client
					.resource(UrlMetadatas.getTxnIdUpdated(Role.ClIENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(request.toString())).post(ClientResponse.class, request.getJson());

			System.err.println(response);

			if (response.getStatus() != 200) {
				System.err.println("RESPONSE CODE :: " + response.getStatus());
				resp.setCode("F00");
				resp.setMessage("Service Unavailable");
				resp.setStatus(ResponseStatus.FAILURE.getKey());

			} else {
				String strResponse = response.getEntity(String.class);
				System.err.println("Payment Response :: " + strResponse);
				if (strResponse != null) {
					JSONObject jsonObject=new JSONObject(strResponse);
					System.err.println("jsonObject:: "+jsonObject);

					JSONObject payload=new JSONObject();
					payload.put("sessionId", session);
					payload.put("userMobile", request.getMobileNo());
					payload.put("emtTxnId", jsonObject.getString("transactionid"));
					payload.put("emtTransactionScreenId", jsonObject.getString("emtTransactionScreenId"));
					payload.put("busId", request.getBusid());
					payload.put("totalFare", request.getTotalFare());
					payload.put("userEmail", request.getEmail());
					payload.put("busType", request.getBusType());
					payload.put("boardId", request.getBoardId());
					payload.put("boardName", request.getBoardName());
					payload.put("arrTime", request.getArrTime());
					payload.put("deptTime", request.getDepTime());
					payload.put("travelName", request.getTravelName());
					payload.put("source", request.getSource());
					payload.put("journeyDate", request.getJourneyDate());
					payload.put("travellers", request.getTravellerDetails().getJSONArray("travellers"));
					payload.put("destination", request.getDestination());
					payload.put("getMdexTxnIdResp", strResponse);
					payload.put("boardTime", request.getBoardTime());
					payload.put("priceRecheckAmt", jsonObject.getString("totalAmount"));
					payload.put("seatHoldId", jsonObject.getString("seatHoldId"));
					payload.put("error", jsonObject.getString("error"));
					payload.put("tripId", request.getTripId());
					
					Client vpqClient = Client.create();
					client.addFilter(new LoggingFilter(System.out));
					WebResource vpqWebResource = vpqClient.resource(
							UrlMetadatas.agentSaveTxnIdURL(Version.VERSION_1,Role.AGENT, Device.WEBSITE, Language.ENGLISH));
					ClientResponse vpqresponse = vpqWebResource.accept("application/json").type("application/json")
							.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);

					String vpqstrResponse = vpqresponse.getEntity(String.class);

					if (vpqstrResponse!=null) {
						JSONObject vJObj=new JSONObject(vpqstrResponse);
						String status=vJObj.getString("status");
						String code=vJObj.getString("code");
						String vMessage=vJObj.getString("message");
						String details=vJObj.getString("details");

						result.setError(jsonObject.getString("error"));
						result.setTransactionCreated(jsonObject.getBoolean("transactionCreated"));
						result.setTransactionid(jsonObject.getString("transactionid"));
						result.setValidFor(jsonObject.getString("validFor"));
						//result.setMpQTxnId(jsonObject.getString("pQTxnId"));
						result.setTransactionid(jsonObject.getString("transactionid"));
						//result.setVpQTxnId(vJObj.getJSONObject("details").getString("transactionRefNo"));
						result.setPriceRecheckAmt(jsonObject.getString("totalAmount"));
						result.setSeatHoldId(jsonObject.getString("seatHoldId"));
						result.setFareBreak(jsonObject.getString("fareBreak"));
						result.setPriceRecheck(jsonObject.getBoolean("fareRecheck"));
						resp.setDetails(result);

						if (code.equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
							resp.setStatus(status);
							resp.setCode(code);
							resp.setMessage(vMessage);
						}
						else {
							resp.setStatus(status);
							resp.setCode(code);
							resp.setMessage(vMessage);
						}
					}

					System.err.println("APP Response:: "+vpqstrResponse);

					if (jsonObject.getBoolean("transactionCreated")==true) {

						resp.setCode(ResponseStatus.SUCCESS.getValue());

					}else {
						resp.setCode(ResponseStatus.FAILURE.getValue());
						resp.setStatus(ResponseStatus.FAILURE.getKey());
						String error=jsonObject.getString("error");
						if (error!=null) {
							if(!error.equalsIgnoreCase("null")){
								resp.setMessage(error);
							}
							else {
								resp.setMessage("Ticket not booked. Please contact customer care");
							}
						}else {
							resp.setMessage("Ticket not booked. Please contact customer care");
						}
					}				

				} else {
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setMessage("Service unavailable");
					resp.setStatus(ResponseStatus.FAILURE.getKey());
				}
			}

			if (!resp.getCode().equalsIgnoreCase("T01")) {
				resp.setDetails(result);
			}


		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus(ResponseStatus.FAILURE.getKey());
		}

		return resp;
	}	





	@Override
	public ResponseDTO bookTicketUpdated(BookTicketReq dto) {

		ResponseDTO resp=new ResponseDTO();
		BookTicketResp result=new BookTicketResp();

		try {

			JSONObject payload = new JSONObject();
			payload.put("clientIp", "49.204.86.246");
			payload.put("cllientKey", UrlMetadatas.MDEX_CLIENTKEY);
			payload.put("clientToken", UrlMetadatas.MDEX_CLIENTTOKEN);
			payload.put("clientApiName", "VPayQwik");
			payload.put("pQTxnId",dto.getMdexTxnId());
			payload.put("transactionId", dto.getTransactionId());
			payload.put("seatHoldId", dto.getSeatHoldId());
			payload.put("amount", dto.getAmount());

			JSONObject cancelVpayload=new JSONObject();
			cancelVpayload.put("pQTxnId", dto.getVpqTxnId());
			cancelVpayload.put("sessionId", dto.getSessionId());

			Client client = Client.create();
			WebResource webResource = client
					.resource(UrlMetadatas.bookTicketUpdated(Role.ClIENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);

			System.err.println(response);

			if (response.getStatus() != 200) {

				Client cancelVpqClient = Client.create();
				cancelVpqClient.addFilter(new LoggingFilter(System.out));
				WebResource vpqWebResource = cancelVpqClient.resource(
						UrlMetadatas.agentGetCancelInitBusPaymentURL(Version.VERSION_1,Role.AGENT, Device.WEBSITE, Language.ENGLISH));
				ClientResponse vpqresponse = vpqWebResource.accept("application/json").type("application/json")
						.header("hash", SecurityUtils.getHash(cancelVpayload.toString())).post(ClientResponse.class, cancelVpayload);

				String CancelVpqresponse = vpqresponse.getEntity(String.class);
				System.err.println(CancelVpqresponse);

				System.err.println("RESPONSE CODE :: " + response.getStatus());
				resp.setCode("F00");
				resp.setMessage("Service Unavailable");
				resp.setStatus(ResponseStatus.FAILURE.getValue());
				resp.setMdexStatus(ResponseStatus.FAILURE.getKey());
			} else {
				String strResponse = response.getEntity(String.class);
				System.err.println("Payment Response :: " + strResponse);
				resp.setMdexBusBookingResp(strResponse);
				if (strResponse != null) {

					JSONObject jObj=new JSONObject(strResponse);

					final boolean isTicket=jObj.getBoolean("ticket");
					final String ticketPnr=jObj.getString("ticketPnr"); 
					final String operatorPnr=jObj.getString("operatorPnr"); 
					final String message=jObj.getString("message"); 

					if (isTicket) {
						resp.setMdexStatus(ResponseStatus.SUCCESS.getKey());
						JSONObject vpayload=new JSONObject();
						vpayload.put("sessionId", dto.getSessionId());
						vpayload.put("ticketPnr", ticketPnr);
						vpayload.put("operatorPnr",operatorPnr);
						vpayload.put("pQTxnId", dto.getVpqTxnId());
						vpayload.put("success",isTicket);
						vpayload.put("mdexBookingResp", strResponse);
						vpayload.put("priceRechecktotalFare", dto.getAmount());
						vpayload.put("seatHoldId", dto.getSeatHoldId());
						Client vpqClient = Client.create();
						client.addFilter(new LoggingFilter(System.out));
						WebResource vpqWebResource = vpqClient.resource(
								UrlMetadatas.agentBusPaymentURL(Version.VERSION_1,Role.AGENT, Device.WEBSITE, Language.ENGLISH));
						ClientResponse vpqresponse = vpqWebResource.accept("application/json").type("application/json")
								.header("hash", SecurityUtils.getHash(vpayload.toString())).post(ClientResponse.class, vpayload);

						String vpqstrResponse = vpqresponse.getEntity(String.class);

						resp.setVpayQwikBusBookingResp(vpqstrResponse);

						if (vpqstrResponse!=null) {
							JSONObject vJObj=new JSONObject(vpqstrResponse);
							String status=vJObj.getString("status");
							String code=vJObj.getString("code");
							String vMessage=vJObj.getString("message");

							if (code.equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
								resp.setStatus(status);
								resp.setCode(code);
								resp.setMessage(vMessage);
								result.setTicket(isTicket);
								result.setTicketPnr(ticketPnr);
								result.setOperatorPnr(operatorPnr);
								result.setMessage(message);
								resp.setDetails(result);
							}else {
								resp.setStatus(status);
								resp.setCode(code);
								resp.setMessage(vMessage);
							}
						}
						System.err.println("APP Response:: "+vpqstrResponse);

					}else {

						Client cancelVpqClient = Client.create();
						cancelVpqClient.addFilter(new LoggingFilter(System.out));
						WebResource vpqWebResource = cancelVpqClient.resource(
								UrlMetadatas.agentGetCancelInitBusPaymentURL(Version.VERSION_1,Role.AGENT, Device.WEBSITE, Language.ENGLISH));
						ClientResponse vpqresponse = vpqWebResource.accept("application/json").type("application/json")
								.header("hash", SecurityUtils.getHash(cancelVpayload.toString())).post(ClientResponse.class, cancelVpayload);

						String CancelVpqresponse = vpqresponse.getEntity(String.class);
						System.err.println(CancelVpqresponse);

						resp.setStatus(ResponseStatus.FAILURE.getKey());
						resp.setCode(ResponseStatus.FAILURE.getValue());
						resp.setMessage(message);
					}
				} else {

					Client cancelVpqClient = Client.create();
					cancelVpqClient.addFilter(new LoggingFilter(System.out));
					WebResource vpqWebResource = cancelVpqClient.resource(
							UrlMetadatas.agentGetCancelInitBusPaymentURL(Version.VERSION_1,Role.AGENT, Device.WEBSITE, Language.ENGLISH));
					ClientResponse vpqresponse = vpqWebResource.accept("application/json").type("application/json")
							.header("hash", SecurityUtils.getHash(cancelVpayload.toString())).post(ClientResponse.class, cancelVpayload);

					String CancelVpqresponse = vpqresponse.getEntity(String.class);
					System.err.println(CancelVpqresponse);

					resp.setCode("F00");
					resp.setMessage("Service unavailable");
					resp.setStatus(ResponseStatus.FAILURE.getValue());
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode("F00");
			resp.setMessage("Service unavailable");
			resp.setStatus(ResponseStatus.FAILURE.getValue());
		}
		return resp;
	}


	@Override
	public ResponseDTO initPayment(BookTicketReq dto) {

		ResponseDTO resp=new ResponseDTO();
		try {
			JSONObject payload = new JSONObject();

			payload.put("sessionId", dto.getSessionId());
			payload.put("emtTxnId", dto.getTransactionId());
			payload.put("seatHoldId", dto.getSeatHoldId());
			payload.put("priceRecheckAmt", dto.getAmount());

			System.out.println(payload.toString());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(
					UrlMetadatas.agentBusPaymentInit(Version.VERSION_1,Role.AGENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);

			String strResponse = response.getEntity(String.class);
			System.err.println(strResponse);

			if (strResponse!=null) {
				JSONObject jObj=new JSONObject(strResponse);
				String code=jObj.getString("code");
				if (code.equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
					JSONObject details=jObj.getJSONObject("details");
					String emtTxnId=details.getString("emtTxnId");
					String seatHoldId=details.getString("seatHoldId");

					resp.setCode(code);
					resp.setStatus(ResponseStatus.SUCCESS.getKey());
					resp.setMessage("Get Txn ref No");
					resp.setTransactionRefNo(details.getString("transactionRefNo"));

				}else {
					resp.setCode(ResponseStatus.FAILURE.getValue());
					resp.setStatus(ResponseStatus.FAILURE.getKey());
					resp.setMessage(jObj.getString("message"));
				}
			}

			return resp;
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus(ResponseStatus.SUCCESS.getValue());
			return resp;
		}
	}


	@Override
	public ResponseDTO getDynamicFareDetails(PriceRecheckDetailsDto dto) {
		ResponseDTO resp=new ResponseDTO();

		List<BusPriceRecheckResp> list=new ArrayList<>();

		try {
			JSONObject payload = new JSONObject();
			payload.put("clientIp", "49.204.86.246");
			payload.put("cllientKey", UrlMetadatas.MDEX_CLIENTKEY);
			payload.put("clientToken", UrlMetadatas.MDEX_CLIENTTOKEN);
			payload.put("clientApiName", "VPayQwik");
			payload.put("seatHoldId",dto.getSeatHoldId());
			Client client = Client.create();
			WebResource webResource = client
					.resource(UrlMetadatas.getDynamicFareDetails(Role.ClIENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);

			String strResponse = response.getEntity(String.class);
			System.out.println(strResponse);
			JSONObject jobj=new JSONObject(strResponse);
			final String code=jobj.getString("code");
			if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(code)) {
				String strDetails=jobj.getString("details");
				if (strDetails!=null) {
					System.err.println("Price recheck: "+strDetails);

					org.codehaus.jettison.json.JSONArray lstfare=new JSONArray(strDetails);


					for (int i = 0; i < lstfare.length(); i++) {

						BusPriceRecheckResp recheckResp=new BusPriceRecheckResp();
						JSONObject lstfareObj= lstfare.getJSONObject(i);

						if (lstfareObj!=null) {

							String seatNo=lstfareObj.getString("seatNo");
							String bookingFee=lstfareObj.getString("bookingFee");
							String basicFare=lstfareObj.getString("basicFare");
							String serviceFee=lstfareObj.getString("serviceFee");
							String serviceCharge=lstfareObj.getString("serviceCharge");
							String serviceTax=lstfareObj.getString("serviceTax");
							String reservationFee=lstfareObj.getString("reservationFee");
							String concessionFee=lstfareObj.getString("concessionFee");
							String discount=lstfareObj.getString("discount");
							String tollFee=lstfareObj.getString("tollFee");
							String tollfeeRajasthan=null;
							if (lstfareObj.has("tollfeeRajasthan")) {
								tollfeeRajasthan=lstfareObj.getString("tollfeeRajasthan");
							}
							String otherCharges=lstfareObj.getString("otherCharges");
							String leviesCharges=lstfareObj.getString("leviesCharges");


							if (basicFare!=null &&  !basicFare.isEmpty() && !basicFare.equalsIgnoreCase("null")) {
								recheckResp.setBasicFare(basicFare);
							}else {
								recheckResp.setBasicFare("NA");
							}
							if (bookingFee!=null &&  !bookingFee.isEmpty() && !bookingFee.equalsIgnoreCase("null")) {
								recheckResp.setBookingFee(bookingFee);
							}else {
								recheckResp.setBookingFee("NA");
							}
							if (concessionFee!=null &&  !concessionFee.isEmpty() && !concessionFee.equalsIgnoreCase("null")) {
								recheckResp.setConcessionFee(concessionFee);
							}else {
								recheckResp.setConcessionFee("NA");
							}
							if (discount!=null &&  !discount.isEmpty() && !discount.equalsIgnoreCase("null")) {
								recheckResp.setDiscount(discount);
							}else {
								recheckResp.setDiscount("NA");
							}
							if (leviesCharges!=null &&  !leviesCharges.isEmpty() && !leviesCharges.equalsIgnoreCase("null")) {
								recheckResp.setLeviesCharges(leviesCharges);
							}else {
								recheckResp.setLeviesCharges("NA");
							}
							if (otherCharges!=null &&  !otherCharges.isEmpty() && !otherCharges.equalsIgnoreCase("null")) {
								recheckResp.setOtherCharges(otherCharges);
							}else {
								recheckResp.setOtherCharges("NA");
							}
							if (reservationFee!=null &&  !reservationFee.isEmpty() && !reservationFee.equalsIgnoreCase("null")) {
								recheckResp.setReservationFee(reservationFee);
							}else {
								recheckResp.setReservationFee("NA");
							}
							if (seatNo!=null &&  !seatNo.isEmpty() && !seatNo.equalsIgnoreCase("null")) {
								recheckResp.setSeatNo(seatNo);
							}else {
								recheckResp.setSeatNo("NA");
							}
							if (serviceCharge!=null &&  !serviceCharge.isEmpty() && !serviceCharge.equalsIgnoreCase("null")) {
								recheckResp.setServiceCharge(serviceCharge);
							}else {
								recheckResp.setServiceCharge("NA");
							}
							if (serviceFee!=null &&  !serviceFee.isEmpty() && !serviceFee.equalsIgnoreCase("null")) {
								recheckResp.setServiceFee(serviceFee);
							}else {
								recheckResp.setServiceFee("NA");
							}
							if (serviceTax!=null &&  !serviceTax.isEmpty() && !serviceTax.equalsIgnoreCase("null")) {
								recheckResp.setServiceTax(serviceTax);
							}else {
								recheckResp.setServiceTax("NA");
							}
							if (tollFee!=null &&  !tollFee.isEmpty() && !tollFee.equalsIgnoreCase("null")) {
								recheckResp.setTollFee(tollFee);
							}else {
								recheckResp.setTollFee("NA");
							}
							if (tollfeeRajasthan!=null &&  !tollfeeRajasthan.isEmpty() && !tollfeeRajasthan.equalsIgnoreCase("null")) {
								recheckResp.setTollfeeRajasthan(tollfeeRajasthan);
							}else {
								recheckResp.setTollfeeRajasthan("NA");
							}

							list.add(recheckResp);

						}
					}

				}
				resp.setCode(code);
				resp.setMessage(jobj.getString("message"));
				resp.setDetails(list);
				resp.setStatus(ResponseStatus.SUCCESS.getKey());
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			resp.setMessage("Service Unavailable");
			resp.setDetails(null);
			resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
		}
		return resp;
	} 


	@Override
	public ResponseDTO getSingleTicketPdf(String sessionId, String emtTxnId) {

		com.payqwikweb.app.model.response.bus.ResponseDTO resp=new com.payqwikweb.app.model.response.bus.ResponseDTO();

		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", sessionId);
			payload.put("bookingTxnId", emtTxnId);
			Client vpqClient = Client.create();
			vpqClient.addFilter(new LoggingFilter(System.out));
			WebResource vpqWebResource = vpqClient.resource(
					UrlMetadatas.agentCreateBusTicketPdfForBus(Version.VERSION_1, Role.ADMIN, Device.WEBSITE, Language.ENGLISH));
			ClientResponse vpqresponse = vpqWebResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String strResponse = vpqresponse.getEntity(String.class);
			org.json.JSONObject jobj=new org.json.JSONObject(strResponse);
			final String code=jobj.getString("code");

			resp.setCode(code);
			resp.setMessage("Create ticket pdf for bus");
			resp.setStatus(ResponseStatus.SUCCESS.getKey());

		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
			resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			resp.setMessage("Service Unavailable");
			resp.setDetails(null);
			resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
		}
		return resp;
	}


	@Override
	public ResponseDTO saveSeatDetails(String session, SaveSeatDetailsDTO request) {

		ResponseDTO resp=new ResponseDTO();

		try {
			
			String tripId=System.currentTimeMillis()+"";
			
			JSONObject payload=new JSONObject();
			payload.put("sessionId", session);
			payload.put("busId", request.getBusid());
			payload.put("totalFare", request.getTotalFare());
			payload.put("busType", request.getBusType());
			payload.put("boardId", request.getBoardId());
			payload.put("boardName", request.getBoardName());
			payload.put("arrTime", request.getArrTime());
			payload.put("deptTime", request.getDepTime());
			payload.put("travelName", request.getTravelName());
			payload.put("source", request.getSource());
			payload.put("journeyDate", request.getJourneyDate());
			payload.put("destination", request.getDestination());
			payload.put("boardTime", request.getBoardTime());
			payload.put("seatDetailsId", tripId);
			
			Client vpqClient = Client.create();
			vpqClient.addFilter(new LoggingFilter(System.out));
			WebResource vpqWebResource = vpqClient.resource(
					UrlMetadatas.saveSeatDetailsURL(Version.VERSION_1,Role.AGENT, Device.WEBSITE, Language.ENGLISH));
			ClientResponse vpqresponse = vpqWebResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);

			String vpqstrResponse = vpqresponse.getEntity(String.class);

			if (vpqstrResponse!=null) {
				JSONObject vJObj=new JSONObject(vpqstrResponse);
				String status=vJObj.getString("status");
				String code=vJObj.getString("code");
				String vMessage=vJObj.getString("message");
				String details=vJObj.getString("details");

				if (code.equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
					resp.setStatus(status);
					resp.setCode(code);
					resp.setMessage(vMessage);
					if (details!=null) {
						
						resp.setDetails(vJObj.getJSONObject("details").getString("tripId"));
					}
				}
				else {
					resp.setStatus(status);
					resp.setCode(code);
					resp.setMessage(vMessage);
				}
			}

			System.err.println("APP Response:: "+vpqstrResponse);

		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode(ResponseStatus.FAILURE.getValue());
			resp.setMessage("Service unavailable");
			resp.setStatus(ResponseStatus.FAILURE.getKey());
		}

		return resp;
	}

}