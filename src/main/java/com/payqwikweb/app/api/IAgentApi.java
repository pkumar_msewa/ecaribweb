package com.payqwikweb.app.api;

import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.request.ChangePasswordRequest;
import com.payqwikweb.app.model.request.ChangePasswordWithOtpRequest;
import com.payqwikweb.app.model.request.EditProfileRequest;
import com.payqwikweb.app.model.request.ForgetPasswordUserRequest;
import com.payqwikweb.app.model.request.LoginRequest;
import com.payqwikweb.app.model.request.MobileOTPRequest;
import com.payqwikweb.app.model.request.ReceiptsRequest;
import com.payqwikweb.app.model.request.RegistrationRequest;
import com.payqwikweb.app.model.request.ResendMobileOTPRequest;
import com.payqwikweb.app.model.request.UploadPictureRequest;
import com.payqwikweb.app.model.request.UserDetailsRequest;
import com.payqwikweb.app.model.response.ChangePasswordResponse;
import com.payqwikweb.app.model.response.ChangePasswordWithOtpResponse;
import com.payqwikweb.app.model.response.EditProfileResponse;
import com.payqwikweb.app.model.response.ForgetPasswordUserResponse;
import com.payqwikweb.app.model.response.LoginResponse;
import com.payqwikweb.app.model.response.MobileOTPResponse;
import com.payqwikweb.app.model.response.ReceiptsResponse;
import com.payqwikweb.app.model.response.RegistrationResponse;
import com.payqwikweb.app.model.response.ResendMobileOTPResponse;
import com.payqwikweb.app.model.response.UploadPictureResponse;
import com.payqwikweb.app.model.response.UserDetailsResponse;
import com.payqwikweb.model.app.request.AgentRegistrationRequest;

public interface IAgentApi {

	ChangePasswordResponse changePassword(ChangePasswordRequest request, Role role);

	EditProfileResponse editProfile(EditProfileRequest request, Role role);

	UploadPictureResponse uploadPicture(UploadPictureRequest request, Role role);

	ReceiptsResponse getReceipts(ReceiptsRequest request, Role role);

	LoginResponse login(LoginRequest request, Role role);

	RegistrationResponse SaveUserToAgent(RegistrationRequest request,Role role);
	
	MobileOTPResponse SaveUsertoAgentMobileOTP(MobileOTPRequest request,Role role);
	
	ResendMobileOTPResponse resendUsertoAgentMobileOTP(ResendMobileOTPRequest request,Role role);
	
	ForgetPasswordUserResponse forgetPasswordUserRequest(ForgetPasswordUserRequest request,Role role);

	ChangePasswordWithOtpResponse changePasswordWithOtpRequest(ChangePasswordWithOtpRequest request,Role role);
	

	UserDetailsResponse getAgentDetails(UserDetailsRequest request,Role role);

	RegistrationResponse getBankList(String sessionId);
	


}