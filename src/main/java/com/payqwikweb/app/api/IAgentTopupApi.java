package com.payqwikweb.app.api;

import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.request.BrowsePlansRequest;
import com.payqwikweb.app.model.request.GetOperatorAndCircleForMobRequest;
import com.payqwikweb.app.model.request.GetOperatorAndCircleRequest;
import com.payqwikweb.app.model.request.RechargeRequestDTO;
import com.payqwikweb.app.model.response.BrowsePlansResponse;
import com.payqwikweb.app.model.response.GetOperatorAndCircleForMobResponse;
import com.payqwikweb.app.model.response.GetOperatorAndCircleResponse;
import com.payqwikweb.app.model.response.RechargeResponseDTO;

public interface IAgentTopupApi 
{
	RechargeResponseDTO prePaid(RechargeRequestDTO request,Role role);

	GetOperatorAndCircleResponse operatorAndcircle(GetOperatorAndCircleRequest request,Role role);

	GetOperatorAndCircleForMobResponse operatorAndcircleForMob(GetOperatorAndCircleForMobRequest request,Role role);
	
	BrowsePlansResponse getPlansForMobile(BrowsePlansRequest request,Role role);


}
