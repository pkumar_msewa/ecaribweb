package com.payqwikweb.app.api;

import com.payqwikweb.app.model.request.BillPaymentCommonDTO;
import com.payqwikweb.app.model.request.DTHBillPaymentRequest;
import com.payqwikweb.app.model.request.ElectricityBillPaymentRequest;
import com.payqwikweb.app.model.request.GasBillPaymentRequest;
import com.payqwikweb.app.model.request.InsuranceBillPaymentRequest;
import com.payqwikweb.app.model.request.LandlineBillPaymentRequest;
import com.payqwikweb.app.model.response.BillPaymentCommonResponseDTO;
import com.payqwikweb.app.model.response.DTHBillPaymentResponse;
import com.payqwikweb.app.model.response.GasBillPaymentResponse;
import com.payqwikweb.app.model.response.InsuranceBillPaymentResponse;
import com.payqwikweb.app.model.response.LandlineBillPaymentResponse;
import com.payqwikweb.app.model.response.ElectricityBillPaymentResponse;

public interface IBillPaymentApi {

	BillPaymentCommonResponseDTO electricBill(BillPaymentCommonDTO request);

	BillPaymentCommonResponseDTO gasBill(BillPaymentCommonDTO request);

	BillPaymentCommonResponseDTO insuranceBill(BillPaymentCommonDTO request);

	BillPaymentCommonResponseDTO landline(BillPaymentCommonDTO request);

	BillPaymentCommonResponseDTO dthBill(BillPaymentCommonDTO request);

}
