package com.payqwikweb.app.api;

import com.payqwikweb.app.model.request.RedeemPointsRequest;
import com.payqwikweb.app.model.response.RedeemPointsResponse;

public interface IRedeemPointsApi {
	public RedeemPointsResponse convertRedeemPointToCash(RedeemPointsRequest request);
}
