package com.payqwikweb.app.api;

import java.util.Date;
import java.util.List;

import com.gcm.model.CronNotificationDTO;
import com.gcm.model.NotificationDTO;
import com.payqwik.visa.util.VisaMerchantRequest;
import com.payqwikweb.app.model.AddServiceTypeDTO;
import com.payqwikweb.app.model.TFilterDTO;
import com.payqwikweb.app.model.flight.dto.FlightCancelableResp;
import com.payqwikweb.app.model.flight.dto.GetFlightDetailsForAdmin;
import com.payqwikweb.app.model.flight.request.FligthcancelableReq;
import com.payqwikweb.app.model.flight.response.FlightResponseDTO;
import com.payqwikweb.app.model.request.AccountTypeRequest;
import com.payqwikweb.app.model.request.AllTransactionRequest;
import com.payqwikweb.app.model.request.AllUserRequest;
import com.payqwikweb.app.model.request.BlockUnBlockUserRequest;
import com.payqwikweb.app.model.request.BlockUserRequest;
import com.payqwikweb.app.model.request.BulkFileUpload;
import com.payqwikweb.app.model.request.BulkMailRequest;
import com.payqwikweb.app.model.request.BulkSMSRequest;
import com.payqwikweb.app.model.request.CPasswordOTPRequest;
import com.payqwikweb.app.model.request.CPasswordRequest;
import com.payqwikweb.app.model.request.EmailLogRequest;
import com.payqwikweb.app.model.request.LoginRequest;
import com.payqwikweb.app.model.request.MRegistrationRequest;
import com.payqwikweb.app.model.request.MailRequest;
import com.payqwikweb.app.model.request.MessageLogRequest;
import com.payqwikweb.app.model.request.MobileSearchRequest;
import com.payqwikweb.app.model.request.PagingDTO;
import com.payqwikweb.app.model.request.PromoCodeRequest;
import com.payqwikweb.app.model.request.ReceiptsRequest;
import com.payqwikweb.app.model.request.RefundDTO;
import com.payqwikweb.app.model.request.SMSRequest;
import com.payqwikweb.app.model.request.ServiceRequest;
import com.payqwikweb.app.model.request.ServicesRequest;
import com.payqwikweb.app.model.request.SessionDTO;
import com.payqwikweb.app.model.request.TreatCardPlanRequest;
import com.payqwikweb.app.model.request.TreatCardRegisterList;
import com.payqwikweb.app.model.request.UserInfoRequest;
import com.payqwikweb.app.model.request.UserTransactionRequest;
import com.payqwikweb.app.model.request.bus.IsCancellableReq;
import com.payqwikweb.app.model.response.AadharServiceResponse;
import com.payqwikweb.app.model.response.AccessListResponse;
import com.payqwikweb.app.model.response.AccountTypeResponse;
import com.payqwikweb.app.model.response.AddMerchantResponse;
import com.payqwikweb.app.model.response.AllTransactionResponse;
import com.payqwikweb.app.model.response.AllUserResponse;
import com.payqwikweb.app.model.response.BlockUnBlockUserResponse;
import com.payqwikweb.app.model.response.BlockUserResponse;
import com.payqwikweb.app.model.response.BulkFileListResponse;
import com.payqwikweb.app.model.response.CPasswordResponse;
import com.payqwikweb.app.model.response.CommissionDTO;
import com.payqwikweb.app.model.response.EmailLogResponse;
import com.payqwikweb.app.model.response.GCMResponse;
import com.payqwikweb.app.model.response.LoginResponse;
import com.payqwikweb.app.model.response.MailLogResponse;
import com.payqwikweb.app.model.response.MailResponse;
import com.payqwikweb.app.model.response.MessageLogResponse;
import com.payqwikweb.app.model.response.MobileSearchResponse;
import com.payqwikweb.app.model.response.NEFTResponse;
import com.payqwikweb.app.model.response.ReceiptsResponse;
import com.payqwikweb.app.model.response.SMSLogResponse;
import com.payqwikweb.app.model.response.SMSResponse;
import com.payqwikweb.app.model.response.ServiceListDTO;
import com.payqwikweb.app.model.response.ServiceListResponse;
import com.payqwikweb.app.model.response.ServiceTypeResponse;
import com.payqwikweb.app.model.response.ServicesDTO;
import com.payqwikweb.app.model.response.ServicesResponse;
import com.payqwikweb.app.model.response.TListResponse;
import com.payqwikweb.app.model.response.TreatCardPlansResponse;
import com.payqwikweb.app.model.response.TreatCardRegisterListResponse;
import com.payqwikweb.app.model.response.UserInfoResponse;
import com.payqwikweb.app.model.response.UserTransactionResponse;
import com.payqwikweb.app.model.response.VersionDTO;
import com.payqwikweb.app.model.response.VersionListResponse;
import com.payqwikweb.app.model.response.VisaSignUpResponse;
import com.payqwikweb.app.model.response.bus.ResponseDTO;
import com.payqwikweb.model.admin.FilterDTO;
import com.payqwikweb.model.app.request.AddServicesDTO;
import com.payqwikweb.model.app.request.UserListRequest;
import com.payqwikweb.model.app.request.VersionRequest;
import com.payqwikweb.model.app.response.TransactionUserResponse;
import com.payqwikweb.model.app.response.UserListResponse;

public interface ISuperAdminApi {

	ServiceListDTO getServiceTypeList(SessionDTO dto);

	TListResponse getFilteredTransactionList(TFilterDTO dto);

	TransactionUserResponse refundTransaction(RefundDTO dto);

	MobileSearchResponse getAccessUserList(SessionDTO dto);

	AccessListResponse getAccessList(SessionDTO dto);

	LoginResponse login(LoginRequest request);

	CPasswordResponse requestChangePassword(CPasswordRequest request);

	CPasswordResponse processChangePassword(CPasswordOTPRequest request);

	UserInfoResponse getUserInfo(UserInfoRequest request);

	ServiceTypeResponse getServiceType();

	ServiceTypeResponse getService();

	GCMResponse getGCMIds(PagingDTO dto);

	SMSResponse sendSingleSMS(SMSRequest dto);

	SMSResponse sendBulkSMS(BulkSMSRequest dto);

	MailResponse sendSingleMail(MailRequest dto);

	MailResponse sendBulkMail(BulkMailRequest dto);

	MobileSearchResponse getPossiblities(MobileSearchRequest request);

	UserListResponse getUsersList(UserListRequest request,String type);

	UserListResponse getUsersListWithFilter(UserListRequest request,String type);

	TListResponse getTransactionList(AllTransactionRequest request);

	SMSLogResponse getSMSLogs(AllTransactionRequest request);

	SMSLogResponse getSMSLogsFilter(AllTransactionRequest request);

	MailLogResponse getMailLogs(AllTransactionRequest request);

	MailLogResponse getMailLogsFilter(AllTransactionRequest request);

	ServiceListResponse getServiceList(SessionDTO request);

	ServicesDTO updateServiceList(ServiceRequest request);

	TListResponse getTransactionListWithFilter(AllTransactionRequest request);

	AllTransactionResponse getAllTransaction(AllTransactionRequest request);

	AllTransactionResponse getSettlementTransactions(AllTransactionRequest request);

	AllTransactionResponse getPromoTransaction(AllTransactionRequest request);

	UserTransactionResponse getUserTransactionValues(SessionDTO dto);

	AllUserResponse getAllUser(AllUserRequest request);

	AllUserResponse getAllMerchants(AllUserRequest request);

	UserTransactionResponse getUserTransaction(UserTransactionRequest request);

	UserTransactionResponse refundLoadMoneyTransactions(RefundDTO dto);

	MessageLogResponse getMessageLog(MessageLogRequest request);

	EmailLogResponse getEmailLog(EmailLogRequest request);

	BlockUnBlockUserResponse blockUser(BlockUnBlockUserRequest request);

	BlockUnBlockUserResponse unblockUser(BlockUnBlockUserRequest request);

	AllTransactionResponse getDaily(AllTransactionRequest request);

	AllUserResponse getAllTransactions(AllTransactionRequest request);

	AllTransactionResponse getSingleUserTransaction(UserTransactionRequest request);
	
	BlockUserResponse userBlock(BlockUserRequest request);


	AddMerchantResponse addMerchant(MRegistrationRequest request);
	
	AllTransactionResponse getSingleUser(UserTransactionRequest request);

	List<NEFTResponse> getNEFTList(SessionDTO dto,boolean flag,Date date1,Date date2);

	List<NEFTResponse> getUserNEFTList(SessionDTO dto);

	List<NEFTResponse> getUserNEFTListFilter(FilterDTO dto);

	List<NEFTResponse> getNEFTList(SessionDTO dto);

	ReceiptsResponse getSingleMerchantTransactionList(ReceiptsRequest request);
	
	AddMerchantResponse addVisaMerchant(VisaMerchantRequest request);
	
	AddMerchantResponse checkVisaMerchant(VisaMerchantRequest request);
	
//	List<VisaMerchantRequest> allVisaMerchant(String sessionId);
	
//	List<VisaMerchantTransaction> getVisaMerchantTransaction(String request);
	
	VisaSignUpResponse merchantSignOffAtM2P(VisaMerchantRequest request);
	
	List<ServicesResponse> getServiceStatus(ServicesRequest request);
	
	ServicesResponse checkServiceStatus(ServicesRequest request);

    AccountTypeResponse updateAccountType(AccountTypeRequest dto);

    AccountTypeResponse getListAccountType(SessionDTO dto);
    
    BlockUnBlockUserResponse kycUpdate(BlockUnBlockUserRequest request);
    
    BlockUnBlockUserResponse NonKycUpdate(BlockUnBlockUserRequest request);

	VersionListResponse getVersionList(SessionDTO request);

	VersionDTO updateVersionList(VersionRequest request);

	TListResponse getFilteredTransactionListOfBillPay(TFilterDTO request);

	TransactionUserResponse refundTransactionBillpay(RefundDTO dto);

	BulkFileListResponse getBulkFile(SessionDTO request);

	BulkFileListResponse uploadBulkFile(BulkFileUpload request);

	TreatCardRegisterListResponse getTreatCardCount(SessionDTO dto);

	TListResponse getTreatCardTransactionListWithFilter(AllTransactionRequest request);

	TListResponse getTreatCardTransactionList(AllTransactionRequest request);

	TreatCardRegisterListResponse getRegisterListFiltered(TreatCardRegisterList dto);

	TreatCardRegisterListResponse getRegisterList(SessionDTO dto);

	TreatCardPlansResponse updatePlans(TreatCardPlanRequest dto);

	TreatCardPlansResponse getListPlans(SessionDTO dto);

	AllTransactionResponse getPromoTransactionFiltered(AllTransactionRequest request);
	
	AllUserResponse getAllFlightsDetail(AllUserRequest request);

	FlightResponseDTO getSingleFlightTicketDetails(String sessionId, long flightId);

	AllUserResponse getBusDetails(AllUserRequest request);

	com.payqwikweb.app.model.response.bus.ResponseDTO getSingleTicketTravellerDetails(String sessionId,
			String emtTxnId);

	com.payqwikweb.app.model.response.bus.ResponseDTO isCancellable(IsCancellableReq dto);

	com.payqwikweb.app.model.response.bus.ResponseDTO cancelBookedTicket(IsCancellableReq dto);

	AllUserResponse getAllFlightsDetailByDate(PagingDTO request);

	FlightCancelableResp isCancellableFlight(FligthcancelableReq dto);

	FlightResponseDTO getFlightDetails(GetFlightDetailsForAdmin getFlightDetails);

	AllUserResponse getBusDetailsByDate(PagingDTO dto);

	ResponseDTO getSingleTicketPdf(String sessionId, String emtTxnNo);

	GCMResponse getGCMIdByUserName(String userName, String sessionId);

	GCMResponse saveGCMIds(CronNotificationDTO dto);

	TListResponse getRefundMerchantTransactionList(RefundDTO request);

	TransactionUserResponse refundMerchantTransaction(RefundDTO dto);

	ServicesDTO updateMdexSwitch(ServicesDTO request);

	ServiceTypeResponse getServicesById(PromoCodeRequest req);

	ServiceTypeResponse getAllCommission(ServiceRequest service);

	ResponseDTO updateCommission(CommissionDTO comDto);

	ResponseDTO AddServices(AddServicesDTO dto);

	ServiceTypeResponse getOperatorList();

	AadharServiceResponse getAadharDetails(SessionDTO request);

	ResponseDTO AddServiceType(AddServiceTypeDTO dto);
}