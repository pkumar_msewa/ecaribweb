package com.payqwikweb.app.api;

import com.payqwikweb.app.model.request.BookBusSeatRequest;
import com.payqwikweb.app.model.request.BusBookingDetailsRequest;
import com.payqwikweb.app.model.request.BusDetailsRequest;
import com.payqwikweb.app.model.request.BusSourcesRequest;
import com.payqwikweb.app.model.response.BookBusSeatResponse;
import com.payqwikweb.app.model.response.BusBookingDetailsResponse;
import com.payqwikweb.app.model.response.BusDetailsResponse;
import com.payqwikweb.app.model.response.BusSourcesResponse;
import com.thirdparty.model.ResponseDTO;

public interface IBusApi {

	BusSourcesResponse getSources (BusSourcesRequest request);
	
	BusDetailsResponse getBusdetails (BusDetailsRequest request);
	
	BusBookingDetailsResponse getTripDetails(BusBookingDetailsRequest request);
	
	BookBusSeatResponse blockBusTicket(BookBusSeatRequest request);
	
	String saveBlockingDetails(BookBusSeatRequest request,String ip, String status);
	
	String bookBusTicketinweb(String request);
	
	String getBusTicketBook(String req);
	
	String busBookingSuccess(String apiRefNo,String blockId,String clientId,String bookingDate,String transactionRefNo,String ds, String currentDate);
	
	
}
