package com.payqwikweb.app.api;

import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.request.LogoutRequest;
import com.payqwikweb.app.model.response.LogoutResponse;

public interface ILogoutApi {

	LogoutResponse logout(LogoutRequest request, Role role);

}
