package com.payqwikweb.app.api;

import javax.servlet.http.HttpSession;

import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.request.ChangePasswordWithOtpRequest;
import com.payqwikweb.app.model.request.ForgetPasswordUserRequest;
import com.payqwikweb.app.model.request.ResendForgotPasswordOtpRequest;
import com.payqwikweb.app.model.response.ChangePasswordWithOtpResponse;
import com.payqwikweb.app.model.response.ForgetPasswordUserResponse;
import com.payqwikweb.app.model.response.LoginResponse;
import com.payqwikweb.app.model.response.ResendForgotPasswordOtpResponse;
import com.payqwikweb.app.model.request.LoginRequest;

public interface ILoginApi {

	LoginResponse login(LoginRequest request, Role role);

	ForgetPasswordUserResponse forgetPasswordUserRequest(ForgetPasswordUserRequest request);

	ChangePasswordWithOtpResponse changePasswordWithOtpRequest(ChangePasswordWithOtpRequest request);

	ResendForgotPasswordOtpResponse resendForgotPasswordOtpRequest(ResendForgotPasswordOtpRequest request);

	ResendForgotPasswordOtpResponse resendDeviceBindingOtpRequest(ResendForgotPasswordOtpRequest request);

	LoginResponse loginNew(LoginRequest request, Role role, HttpSession session);
}
