package com.payqwikweb.app.api;


import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import com.payqwikweb.app.model.FlightPaymentGateWayDTO;
import com.payqwikweb.app.model.flight.request.FligthBookReq;
import com.payqwikweb.app.model.flight.response.FlightClityList;
import com.payqwikweb.app.model.flight.response.FlightResponseDTO;
import com.payqwikweb.app.model.request.AirBookRQ;


import com.payqwikweb.app.model.request.MobileFlightBookRequest;
import com.payqwikweb.app.model.request.TravelFlightRequest;
import com.payqwikweb.app.model.response.FlightPriceCheckRequest;
import com.payqwikweb.app.model.response.bus.ResponseDTO;
import com.payqwikweb.model.app.request.FlightBookRequest;
import com.payqwikweb.model.app.request.MobileRoundwayFlightBookRequest;
import com.payqwikweb.model.app.request.ReturnFlightBookRequest;
import com.payqwikweb.model.app.request.TravellerFlightDetails;

public interface ITravelFlightApi {
	
	String getAirlineNames();

	String getFlightSearchAPI(TravelFlightRequest req);

	String AirRePriceRQ(FlightPriceCheckRequest req);
	
	String AirRePriceRQConnecting(FlightPriceCheckRequest req ,String source, String destination, HttpSession session);

	String mobileAirRePriceRQConnecting(FlightPriceCheckRequest req);
	
	String AirBookRQ(FlightBookRequest req, String source, HttpSession session);

	String AirBookRQForMobile(MobileFlightBookRequest req);

	String AirBookRQreturn(ReturnFlightBookRequest req, String source, String destination, HttpSession session);

	String AirBookreturnRQForMobile(MobileRoundwayFlightBookRequest req);

	String FlightBookingInitiate(String sessionId, String firstName, String paymentAmount, String ticketDetails,
			String paymentmethod);
	String FlightBookingSucess(String sessionId,String ticketNumber,String firstName,String bookingRefId,String transactionRefNomdex,String paymentAmount,String ticketDetails,String statusflight,String paymentstatus,String paymentmethod,
			 String transactionRefno, boolean success,String email,String mobile);
	
	String AirBookRQwithPaymentGatway(FlightBookRequest req, String source, HttpSession session);
	
	String AirBookRQreturnwithPaymentGatway(ReturnFlightBookRequest req, String source, String destination, HttpSession session);
	String FlightBookPaymentGatway(String json);

	String FlightPaymentGatewaySucess(String sessionId,String ticketNumber,String firstName,String bookingRefId,String transactionRefNomdex,String paymentAmount,String ticketDetails,String statusflight,String paymentstatus,String paymentmethod,
			 String transactionRefno, boolean success,String email,String mobile);
	String AirRePriceRQConnectinginternational(org.json.JSONObject payload);

	String AirBookRQForMobileConecting(AirBookRQ req);

	 
	String AirBookRQForPaymentGateWayMobile(FlightPaymentGateWayDTO req);
	String AirBookRQForPaymentGateWayMobileConecting(FlightPaymentGateWayDTO req);
	String AirBookreturnRQForPaymentGateWayMobile(FlightPaymentGateWayDTO req);
	String getTxnRefNo(String session);
	
	
	/* Code Done By Rohit End */


	/*##############################################################################################################*/

	/* Fresh Code Started By Subir */
	
	FlightClityList getAirLineNames(String sessionId);
	FlightResponseDTO searchFlight(TravelFlightRequest req);
	FlightResponseDTO airRePriceRQ(FlightPriceCheckRequest req);
	
	String flightBookingInitiate(FligthBookReq fligthBookReq,List<TravellerFlightDetails> details);
	
	String flightBookingSucess(FligthBookReq fligthBookReq,List<TravellerFlightDetails> details);
	String flightPaymentGatewaySucess(FligthBookReq fligthBookReq,List<TravellerFlightDetails> details);
	
	FlightResponseDTO getAllBookTickets(String sessionId);
	FlightResponseDTO getAllBookTicketsForMobile(String sessionId);
	FlightResponseDTO saveAirLineNamesInDb();
	FlightResponseDTO cronCheck(String session);
    boolean compareCountry(TravelFlightRequest req);
}
