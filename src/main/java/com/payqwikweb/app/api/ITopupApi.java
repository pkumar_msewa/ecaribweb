package com.payqwikweb.app.api;

import com.payqwikweb.app.model.request.BrowsePlansRequest;
import com.payqwikweb.app.model.request.DataCardTopupRequest;
import com.payqwikweb.app.model.request.GetOperatorAndCircleForMobRequest;
import com.payqwikweb.app.model.request.GetOperatorAndCircleRequest;
import com.payqwikweb.app.model.request.PostpaidTopupRequest;
import com.payqwikweb.app.model.request.PrepaidTopupRequest;
import com.payqwikweb.app.model.request.RechargeRequestDTO;
import com.payqwikweb.app.model.response.BrowsePlansResponse;
import com.payqwikweb.app.model.response.DataCardTopupResponse;
import com.payqwikweb.app.model.response.GetOperatorAndCircleForMobResponse;
import com.payqwikweb.app.model.response.GetOperatorAndCircleResponse;
import com.payqwikweb.app.model.response.PostpaidTopupResponse;
import com.payqwikweb.app.model.response.PrepaidTopupResponse;
import com.payqwikweb.app.model.response.RechargeResponseDTO;

public interface ITopupApi {

	RechargeResponseDTO prePaid(RechargeRequestDTO request);

	GetOperatorAndCircleResponse operatorAndcircle(GetOperatorAndCircleRequest request);

	GetOperatorAndCircleForMobResponse operatorAndcircleForMob(GetOperatorAndCircleForMobRequest request);
	
	BrowsePlansResponse getPlansForMobile(BrowsePlansRequest request);

}
