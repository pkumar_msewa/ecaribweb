package com.payqwikweb.app.api;

import com.payqwikweb.app.model.hotel.request.HotelAvalabilityRequest;
import com.payqwikweb.app.model.hotel.request.HotelBookingRequest;
import com.payqwikweb.app.model.hotel.request.HotelInfoRequest;
import com.payqwikweb.app.model.hotel.request.HotellistRequest;
import com.payqwikweb.app.model.hotel.response.HotelResponseDTO;

public interface IHotelApi {

	HotelResponseDTO getAllCityList(String sessionId);
	HotelResponseDTO getavailableHotels(HotellistRequest request);
	HotelResponseDTO gethotelInfo(HotelInfoRequest request);
	HotelResponseDTO gethotelAvailableity(HotelAvalabilityRequest request);
	HotelResponseDTO gethotelBook(HotelBookingRequest request);
	
}
