package com.payqwikweb.app.api;


import com.payqwikweb.app.model.request.OnePayRequest;
import com.payqwikweb.app.model.request.TransactionRequest;
import com.payqwikweb.app.model.response.OnePayResponse;
import com.payqwikweb.app.model.response.TransactionDTO;
import com.thirdparty.model.ResponseDTO;

public interface ITransactionApi {

    ResponseDTO validateTransaction(TransactionRequest dto);

    ResponseDTO validateMTransaction(TransactionRequest dto);

    ResponseDTO getAllServices();

    OnePayResponse getOnePayResponse(OnePayRequest dto);

    int getChoiceByServiceCode(String code);

    ResponseDTO getAllBanks();

    ResponseDTO getIFSCByBank(String bankCode);
    
    ResponseDTO validateAgentTransaction(TransactionRequest dto);
}
