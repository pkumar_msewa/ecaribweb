package com.payqwikweb.app.api;

import com.payqwikweb.model.app.request.QwikrPayRequestDTO;
import com.thirdparty.model.ResponseDTO;

public interface IQwikrPayApi {

	 ResponseDTO initializeQwikrPay(QwikrPayRequestDTO dto);
	 ResponseDTO successQwikrPay(QwikrPayRequestDTO dto);
	
}
