package com.payqwikweb.app.api;

import com.payqwikweb.app.model.request.AllTransactionRequest;
import com.payqwikweb.app.model.request.LoginRequest;
import com.payqwikweb.app.model.request.RefundDTO;
import com.payqwikweb.app.model.request.SessionDTO;
import com.payqwikweb.app.model.request.StatusDTO;
import com.payqwikweb.app.model.response.AllTransactionResponse;
import com.payqwikweb.app.model.response.LoginResponse;
import com.payqwikweb.app.model.response.UserTransactionResponse;
import com.payqwikweb.model.app.response.TransactionUserResponse;
import com.thirdparty.model.ResponseDTO;

public interface IMerchantApi {

    LoginResponse login(LoginRequest request);

    AllTransactionResponse getAllTransaction(AllTransactionRequest request);

	ResponseDTO getStatus(StatusDTO request);

	ResponseDTO requestRefund(String sessionId,String path);

	TransactionUserResponse requestRefundTransaction(RefundDTO dto);

	UserTransactionResponse getMerchatTransactionValues(SessionDTO dto);

}
