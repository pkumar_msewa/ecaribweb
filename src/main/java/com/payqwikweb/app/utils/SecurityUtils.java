package com.payqwikweb.app.utils;

import java.math.BigInteger;
import java.security.MessageDigest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.payqwikweb.util.LogCat;
import com.payqwikweb.util.SecurityUtil;

public class SecurityUtils {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private static final String HASH_KEY = "VPAY_MSS_PAY";

	public static String getHash(String message) {
		try {
			return SecurityUtil.md5(message + HASH_KEY);
		} catch (Exception e) {
			return "";
		}
	}

	public static String generateKey(String str) {
		String hashKey = "";
		try {
			hashKey = md5(str);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return hashKey;

	}

	public static String md5(String str) throws Exception {

		MessageDigest m = MessageDigest.getInstance("MD5");

		byte[] data = str.getBytes();

		m.update(data, 0, data.length);

		BigInteger i = new BigInteger(1, m.digest());

		String hash = String.format("%1$032X", i);

		return hash;
	}

}
