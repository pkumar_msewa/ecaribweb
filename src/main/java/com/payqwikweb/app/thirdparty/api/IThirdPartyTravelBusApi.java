package com.payqwikweb.app.thirdparty.api;

import com.payqwikweb.model.request.thirdpartyService.BlockBusRequest;
import com.payqwikweb.model.request.thirdpartyService.BlockBusTicketRequest;
import com.payqwikweb.model.request.thirdpartyService.BoardingPointDetailsRequest;
import com.payqwikweb.model.request.thirdpartyService.BookBusTicketRequest;
import com.payqwikweb.model.request.thirdpartyService.BusTicketBookingDetailsRequest;
import com.payqwikweb.model.request.thirdpartyService.BusesRequest;
import com.payqwikweb.model.request.thirdpartyService.CancelBusTicketRequest;
import com.payqwikweb.model.request.thirdpartyService.FlightRounwayRqust;
import com.payqwikweb.model.request.thirdpartyService.GetbusbookingetailRequest;
import com.payqwikweb.model.request.thirdpartyService.GetbusdeatailRequest;
import com.payqwikweb.model.request.thirdpartyService.SourcesRequest;
import com.payqwikweb.model.request.thirdpartyService.SourcesResponse;
import com.payqwikweb.model.request.thirdpartyService.TripDetailsRequest;
import com.payqwikweb.model.response.thirdpartyService.BlockBusResponse;
import com.payqwikweb.model.response.thirdpartyService.BlockBusTicketResponse;
import com.payqwikweb.model.response.thirdpartyService.BoardingPointDetailsResponse;
import com.payqwikweb.model.response.thirdpartyService.BookBusTicketResponse;
import com.payqwikweb.model.response.thirdpartyService.BusTicketBookingDetailsResponse;
import com.payqwikweb.model.response.thirdpartyService.BusesResponse;
import com.payqwikweb.model.response.thirdpartyService.CancelBusTicketResponse;
import com.payqwikweb.model.response.thirdpartyService.GetbusbookingetailResponse;
import com.payqwikweb.model.response.thirdpartyService.GetbusdeatailResponse;
import com.payqwikweb.model.response.thirdpartyService.MultipleBlockSeatResponse;
import com.payqwikweb.model.response.thirdpartyService.TripDetailsResponse;

public interface IThirdPartyTravelBusApi {

	SourcesResponse sources(SourcesRequest request);

	BusesResponse getBuses(BusesRequest request);

	BoardingPointDetailsResponse boardingPointDetails(BoardingPointDetailsRequest request);

	CancelBusTicketResponse cancelBusTicket(CancelBusTicketRequest request);

	TripDetailsResponse tripDetails(TripDetailsRequest request);

	BookBusTicketResponse bookBusTicket(BookBusTicketRequest request);

	BlockBusTicketResponse blockBusTicket(BlockBusTicketRequest request);

	BusTicketBookingDetailsResponse busTicketBookingDetails(BusTicketBookingDetailsRequest request);

	SourcesResponse getsources(SourcesRequest request);
	
	MultipleBlockSeatResponse multipleSeat(BlockBusRequest request);
	 
	GetbusdeatailResponse getBusesdeatial(GetbusdeatailRequest request);
	
	GetbusbookingetailResponse getBusesbookingdeatial(GetbusbookingetailRequest request);
	
	BlockBusResponse blockBusTicketuser(BlockBusRequest request);
	
	String bookBusTicketinweb(String request);
	
	String bookbusticketcallapi(BlockBusRequest request,String ref);
	
	String getbusticketbook(String req);
	
	String busbookingsucces(String apiRefNo,String blockId,String clientId,String bookingDate,String transactionRefNo,String ds);

}
