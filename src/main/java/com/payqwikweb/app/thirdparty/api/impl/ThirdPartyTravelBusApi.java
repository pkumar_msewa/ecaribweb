package com.payqwikweb.app.thirdparty.api.impl;

import java.net.URLEncoder;

import org.codehaus.jettison.json.JSONObject;

import com.payqwikweb.app.metadatas.UrlMetadatas;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Language;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.Version;
import com.payqwikweb.app.thirdparty.api.IThirdPartyTravelBusApi;
import com.payqwikweb.app.utils.SecurityUtils;
import com.payqwikweb.model.request.thirdpartyService.BlockBusRequest;
import com.payqwikweb.model.request.thirdpartyService.BlockBusTicketRequest;
import com.payqwikweb.model.request.thirdpartyService.BoardingPointDetailsRequest;
import com.payqwikweb.model.request.thirdpartyService.BookBusTicketRequest;
import com.payqwikweb.model.request.thirdpartyService.BusTicketBookingDetailsRequest;
import com.payqwikweb.model.request.thirdpartyService.BusesRequest;
import com.payqwikweb.model.request.thirdpartyService.CancelBusTicketRequest;
import com.payqwikweb.model.request.thirdpartyService.GetbusbookingetailRequest;
import com.payqwikweb.model.request.thirdpartyService.GetbusdeatailRequest;
import com.payqwikweb.model.request.thirdpartyService.SourcesRequest;
import com.payqwikweb.model.request.thirdpartyService.SourcesResponse;
import com.payqwikweb.model.request.thirdpartyService.TripDetailsRequest;
import com.payqwikweb.model.response.thirdpartyService.BlockBusResponse;
import com.payqwikweb.model.response.thirdpartyService.BlockBusTicketResponse;
import com.payqwikweb.model.response.thirdpartyService.BoardingPointDetailsResponse;
import com.payqwikweb.model.response.thirdpartyService.BookBusTicketResponse;
import com.payqwikweb.model.response.thirdpartyService.BusTicketBookingDetailsResponse;
import com.payqwikweb.model.response.thirdpartyService.BusesResponse;
import com.payqwikweb.model.response.thirdpartyService.CancelBusTicketResponse;
import com.payqwikweb.model.response.thirdpartyService.GetbusbookingetailResponse;
import com.payqwikweb.model.response.thirdpartyService.GetbusdeatailResponse;
import com.payqwikweb.model.response.thirdpartyService.MultipleBlockSeatResponse;
import com.payqwikweb.model.response.thirdpartyService.TripDetailsResponse;
import com.payqwikweb.util.LogCat;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;

public class ThirdPartyTravelBusApi implements IThirdPartyTravelBusApi {

	@Override
	public SourcesResponse sources(SourcesRequest request) {
		SourcesResponse resp = new SourcesResponse();
		try {
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource("http://103.24.202.25/Buses/Sources");
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("ConsumerKey", "59285FC07292811D72785AC287ACE3E8")
					.header("ConsumerSecret", "04DAF1E2C80A758DCC6BCEBE436D25BC").get(ClientResponse.class);
			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed , HTTP error code , " + response.getStatus());
			}
			String output = response.getEntity(String.class);
			resp.setMsg(output);
			resp.setCode(response.getStatus() + "");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}

	@Override
	public BusesResponse getBuses(BusesRequest request) {
		BusesResponse resp = new BusesResponse();
		try {
			Client client = Client.create();
			WebResource webResource = client.resource("http://103.24.202.25/Buses/AvailableBuses?&sourceId="
					+ request.getSourceId() + "&destinationId=" + request.getDestinationId() + "&journeyDate="
					+ request.getJourneyDate() + "&tripType=" + request.getTripType() + "&userType="
					+ request.getUserType() + "&returnDate=" + request.getReturnDate());
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("ConsumerKey", "59285FC07292811D72785AC287ACE3E8")
					.header("ConsumerSecret", "04DAF1E2C80A758DCC6BCEBE436D25BC").get(ClientResponse.class);
			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed , HTTP error code , " + response.getStatus());
			}
			String output = response.getEntity(String.class);
			resp.setMsg(output);
			resp.setCode(response.getStatus() + "");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}

	@Override
	public TripDetailsResponse tripDetails(TripDetailsRequest request) {
		TripDetailsResponse resp = new TripDetailsResponse();
		try {
			Client client = Client.create();
			WebResource webResource = client.resource("http://103.24.202.25/Buses/TripDetails?tripId="
					+ request.getTripId() + "&sourceId=" + request.getSourceId() + "&destinationId="
					+ request.getDestinationId() + "&journeyDate=" + request.getJourneyDate() + "&tripType="
					+ request.getTripType() + "&provider=" + request.getProvider() + "&travelOperator="
					+ URLEncoder.encode(request.getTravelOperator(), "UTF-8") + "&userType=" + request.getUserType());
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("ConsumerKey", "59285FC07292811D72785AC287ACE3E8")
					.header("ConsumerSecret", "04DAF1E2C80A758DCC6BCEBE436D25BC").get(ClientResponse.class);
			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed , HTTP error code , " + response.getStatus());
			}
			String output = response.getEntity(String.class);
			resp.setMsg(output);
			resp.setCode(response.getStatus() + "");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}

	@Override
	public BlockBusTicketResponse blockBusTicket(BlockBusTicketRequest request) {

		BlockBusTicketResponse resp = new BlockBusTicketResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("TripId", request.getTripId());
			payload.put("BoardingId", request.getBoardingId());
			payload.put("NoofSeats", request.getNoofSeats());
			payload.put("Fares", request.getFares());
			payload.put("ConvenienceFee", request.getConvenienceFee());
			payload.put("SeatNos", request.getSeatNos());
			payload.put("Titles", request.getTitles());
			payload.put("Names", request.getNames());
			payload.put("Ages", request.getAges());
			payload.put("Genders", request.getGenders());
			payload.put("Address", request.getAddress());
			payload.put("PostalCode", request.getPostalCode());
			payload.put("IdCardType", request.getIdCardType());
			payload.put("IdCardNo", request.getIdCardNo());
			payload.put("IdCardIssuedBy", request.getIdCardIssuedBy());
			payload.put("MobileNo", request.getMobileNo());
			payload.put("EmergencyMobileNo", request.getEmergencyMobileNo());
			payload.put("EmailId", request.getEmailId());
			payload.put("Provider", request.getProvider());
			payload.put("Operator", request.getOperator());
			payload.put("PartialCancellationAllowed", request.getPartialCancellationAllowed());
			payload.put("BoardingPointDetails", request.getBoardingPointDetails());
			payload.put("BusTypeName", request.getBusTypeName());
			payload.put("DepartureTime", request.getDepartureTime());
			payload.put("CancellationPolicy", request.getCancellationPolicy());
			payload.put("SourceId", request.getSourceId());
			payload.put("SourceName", request.getSourceName());
			payload.put("DestinationId", request.getDestinationId());
			payload.put("DestinationName", request.getDestinationName());
			payload.put("JourneyDate", request.getJourneyDate());
			payload.put("TripType", request.getTripType());
			payload.put("BusType", request.getBusType());
			payload.put("User", request.getUser());
			payload.put("UserType", request.getUserType());
			payload.put("ServiceTax", request.getServiceTax());
			payload.put("ServiceCharge", request.getServiceCharge());
			Client client = Client.create();
			WebResource webResource = client.resource("http://103.24.202.25/Buses/BlockBusTicket");
			ClientResponse response = webResource.header("ConsumerKey", "59285FC07292811D72785AC287ACE3E8")
					.header("ConsumerSecret", "04DAF1E2C80A758DCC6BCEBE436D25BC")
					.header("Content-Type", "application/json").post(ClientResponse.class, payload);
			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed , HTTP error code , " + response.getStatus());
			}
			String output = response.getEntity(String.class);
			resp.setMsg(output);
			resp.setCode(response.getStatus() + "");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}

	@Override
	public BookBusTicketResponse bookBusTicket(BookBusTicketRequest request) {

		BookBusTicketResponse resp = new BookBusTicketResponse();
		try {
			Client client = Client.create();
			WebResource webResource = client
					.resource("http://103.24.202.25/Buses/BookBusTicket?referenceNo=" + request.getReferenceNo());
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("ConsumerKey", "59285FC07292811D72785AC287ACE3E8")
					.header("ConsumerSecret", "04DAF1E2C80A758DCC6BCEBE436D25BC").get(ClientResponse.class);
			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed , HTTP error code , " + response.getStatus());
			}
			String output = response.getEntity(String.class);
			resp.setMsg(output);
			resp.setCode(response.getStatus() + "");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}

	@Override
	public CancelBusTicketResponse cancelBusTicket(CancelBusTicketRequest request) {
		CancelBusTicketResponse resp = new CancelBusTicketResponse();
		try {
			Client client = Client.create();
			WebResource webResource = client
					.resource("http://103.24.202.25/Buses/CancelBusTicket?referenceNo=" + request.getReferenceNo()
							+ "&seatNos=" + request.getSeatNos() + "&emailId=" + request.getEmailId());
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("ConsumerKey", "59285FC07292811D72785AC287ACE3E8")
					.header("ConsumerSecret", "04DAF1E2C80A758DCC6BCEBE436D25BC").get(ClientResponse.class);
			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed , HTTP error code , " + response.getStatus());
			}
			String output = response.getEntity(String.class);
			resp.setMsg(output);
			resp.setCode(response.getStatus() + "");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}

	@Override
	public BusTicketBookingDetailsResponse busTicketBookingDetails(BusTicketBookingDetailsRequest request) {
		BusTicketBookingDetailsResponse resp = new BusTicketBookingDetailsResponse();
		try {
			Client client = Client.create();
			WebResource webResource = client.resource("http://103.24.202.25/Buses/BusTicketBookingDetails?referenceNo="
					+ request.getReferenceNo() + "&type=" + request.getType());
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("ConsumerKey", "59285FC07292811D72785AC287ACE3E8")
					.header("ConsumerSecret", "04DAF1E2C80A758DCC6BCEBE436D25BC").get(ClientResponse.class);
			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed , HTTP error code , " + response.getStatus());
			}
			String output = response.getEntity(String.class);
			resp.setMsg(output);
			resp.setCode(response.getStatus() + "");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}

	@Override
	public BoardingPointDetailsResponse boardingPointDetails(BoardingPointDetailsRequest request) {

		BoardingPointDetailsResponse resp = new BoardingPointDetailsResponse();
		try {
			Client client = Client.create();
			WebResource webResource = client.resource(
					"http://103.24.202.25/Buses/BoardingPointDetails?&boardingPoint=" + request.getBoardingPoint()
							+ "&provider=" + request.getProvider() + "&tripid=" + request.getTripId());
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("ConsumerKey", "59285FC07292811D72785AC287ACE3E8")
					.header("ConsumerSecret", "04DAF1E2C80A758DCC6BCEBE436D25BC").get(ClientResponse.class);
			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed , HTTP error code , " + response.getStatus());
			}
			String output = response.getEntity(String.class);
			resp.setMsg(output);
			resp.setCode(response.getStatus() + "");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}

	@Override
	public GetbusdeatailResponse getBusesdeatial(GetbusdeatailRequest request) {
		GetbusdeatailResponse resp = new GetbusdeatailResponse();
		try {
			Client client = Client.create();
			WebResource webResource = client.resource("http://103.24.202.25/Buses/AvailableBuses?sourceId="
					+ request.getCitysourceoneway() + "&destinationId=" + request.getCitydestoneway() + "&journeyDate="
					+ request.getDatepickeronewaydeparure() + "&tripType=" + request.getTripType() + "&userType="
					+ request.getUserType() + "&returnDate=" + request.getReturnDate());
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("ConsumerKey", "59285FC07292811D72785AC287ACE3E8 ")
					.header("ConsumerSecret", "04DAF1E2C80A758DCC6BCEBE436D25BC").get(ClientResponse.class);
			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed , HTTP error code , " + response.getStatus());
			}
			String output = response.getEntity(String.class);
			resp.setMsg(output);
			resp.setCode(response.getStatus() + "");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}

	@Override //
	public GetbusbookingetailResponse getBusesbookingdeatial(GetbusbookingetailRequest request) {
		// TODO Auto-generated method stub
		GetbusbookingetailResponse resp = new GetbusbookingetailResponse();
		try {
			Client client = Client.create();
			WebResource webResource = client.resource("http://103.24.202.25/Buses/TripDetails?tripId="
					+ request.getTripid() + "&sourceId=" + request.getSourceId() + "&destinationId="
					+ request.getDestinationId() + "&journeyDate=" + request.getJourneyDate() + "&tripType="
					+ request.getTripType() + "&provider=" + request.getPorvidr() + "&travelOperator="
					+ request.getTravels() + "&userType=" + request.getUserType());
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("ConsumerKey", "59285FC07292811D72785AC287ACE3E8")
					.header("ConsumerSecret", "04DAF1E2C80A758DCC6BCEBE436D25BC  ").get(ClientResponse.class);
			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed , HTTP error code , " + response.getStatus());
			}
			String output = response.getEntity(String.class);
			resp.setMsg(output);
			resp.setCode(response.getStatus() + "");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;

	}

	@Override
	public BlockBusResponse blockBusTicketuser(BlockBusRequest request) {
		BlockBusResponse resp = new BlockBusResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("IsOfflineBooking", false);
			payload.put("TripId", request.getTripId());
			payload.put("BoardingId", request.getBoardingId());
			payload.put("NoofSeats", request.getNoofSeats());
			payload.put("Fares", request.getFares());
			payload.put("ConvenienceFee", request.getConvenienceFee());
			payload.put("SeatNos", request.getSeatNos());
			payload.put("Titles", request.getTitles());
			payload.put("Names", request.getNames());
			payload.put("Ages", request.getAges());
			payload.put("Genders", request.getGenders());
			payload.put("Address", request.getAddress());
			payload.put("PostalCode", request.getPostalCode());
			payload.put("IdCardType", request.getIdCardType());
			payload.put("IdCardNo", request.getIdCardNo());
			payload.put("IdCardIssuedBy", request.getIdCardIssuedBy());
			payload.put("MobileNo", request.getMobileNo());
			payload.put("EmergencyMobileNo", request.getMobileNo());
			payload.put("EmailId", request.getEmailId());
			payload.put("Provider", request.getProvider());
			payload.put("Operator", request.getOperator());
			payload.put("PartialCancellationAllowed", request.getPartialCancellationAllowed());
			payload.put("BoardingPointDetails", request.getBoardingPointDetails());
			payload.put("BusTypeName", request.getBusTypeName());
			payload.put("DepartureTime", request.getDepartureTime());
			payload.put("CancellationPolicy", request.getCancellationPolicy());
			payload.put("SourceId", request.getSourceId());
			payload.put("SourceName", request.getSourceName());
			payload.put("DestinationId", request.getDestinationId());
			payload.put("DestinationName", request.getDestinationName());
			payload.put("JourneyDate", request.getJourneyDate());
			payload.put("TripType", request.getTripType());
			payload.put("BusType", request.getBusType());
			payload.put("User", request.getUser());
			payload.put("UserType", request.getUserType());
			payload.put("ServiceTax", request.getServiceTax());
			payload.put("ServiceCharge", request.getServiceCharge());
			// payload.put("transactionRefNo", request.getTransctionrefno());
			Client client = Client.create();
			WebResource webResource = client.resource("http://103.24.202.25/Buses/BlockBusTicket");
			ClientResponse response = webResource.header("ConsumerKey", "59285FC07292811D72785AC287ACE3E8")
					.header("ConsumerSecret", "04DAF1E2C80A758DCC6BCEBE436D25BC")
					.header("Content-Type", "application/json").post(ClientResponse.class, payload);
			String output = response.getEntity(String.class);
			String bookingstatus = ("" + response.getStatus());
			if (bookingstatus.equalsIgnoreCase("1")) {
				throw new RuntimeException("Failed , HTTP error code , " + response.getStatus());
			} else {
				JSONObject obj = new JSONObject(output);
				String val1 = obj.getString("BookingReferenceNo");
				String status = obj.getString("ResponseStatus");
				resp.setMsg(output);
				resp.setCode(val1);
				resp.setCodecondtion(status);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}

	@Override
	public String bookBusTicketinweb(String request) {
		BookBusTicketResponse resp = new BookBusTicketResponse();
		try {
			Client client = Client.create();
			WebResource webResource = client
					.resource("http://103.24.202.25/Buses/BookBusTicket?referenceNo=" + request);
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("ConsumerKey", "59285FC07292811D72785AC287ACE3E8")
					.header("ConsumerSecret", "04DAF1E2C80A758DCC6BCEBE436D25BC").get(ClientResponse.class);
			String output = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed , HTTP error code , " + response.getStatus());
			}
			resp.setMsg(output);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp.getMsg();
	}

	@Override
	public String bookbusticketcallapi(BlockBusRequest request, String ref) {
		// TODO Auto-generated method stub
		System.err.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ RefNo" + ref);
		String data = "";
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", request.getSessionid());
			payload.put("tripId", request.getTripId());
			payload.put("boardingId", request.getBoardingId());// "18901"
			payload.put("boardingAddress", "Ameerpet - 10,45 PM");
			payload.put("blockingRefNo", ref);// "1004600634"
			payload.put("bookingRefNo", ref); // payload.put("bookingRefNo",//
												// ref);
			payload.put("bookingStatus", "1");
			payload.put("noOfSeats", request.getNoofSeats()); // "1");
			payload.put("seatNo", request.getSeatNos());
			payload.put("fare", request.getFares());
			payload.put("sourceId", request.getSourceId()); // "100",
			payload.put("sourceName", request.getSourceName());
			payload.put("destinationId", request.getDestinationId());// "109"
			payload.put("destinationName", request.getDestinationName());
			payload.put("provider", request.getProvider());
			payload.put("bookingDate", "2016-12-25");
			payload.put("journeyDate", "2016-12-25");
			payload.put("returnDate", "2016-12-25");
			payload.put("departureTime", " 10,45 PM");
			payload.put("busTypeName", "Seater AC Volvo B9R Multi Axle Semi Sleeper (2+2)");
			payload.put("travelOperator", "I2space Travels");
			payload.put("salutation", request.getUserType());
			payload.put("name", request.getNames());
			payload.put("age", request.getAges());
			payload.put("gender", request.getGenders());
			payload.put("address", request.getAddress());
			payload.put("postalCode", request.getPostalCode());
			payload.put("mobileNo", request.getMobileNo());
			payload.put("emailId", request.getEmailId());
			payload.put("transactionRefNo", request.getTransctionrefno());
			payload.put("ipAddress", "100.10.10.10");
			Client client = Client.create();
			WebResource webResource = client.resource(
					UrlMetadatas.busBookingUrl(Version.VERSION_1, Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String output = response.getEntity(String.class);
			data = output;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	@Override
	public String getbusticketbook(String req) {
		String data = "";
		try {
			Client client = Client.create();
			WebResource webResource = client
					.resource("http://103.24.202.25/Buses/BusTicketBookingDetails?referenceNo=" + req + "&type=2");
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("ConsumerKey", "59285FC07292811D72785AC287ACE3E8")
					.header("ConsumerSecret", "04DAF1E2C80A758DCC6BCEBE436D25BC  ").get(ClientResponse.class);
			String output = response.getEntity(String.class);
			data = output;
			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed , HTTP error code , " + response.getStatus());
			}

			// resp.setMsg(output);
			// resp.setCode(response.getStatus() + "");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	public String busbookingsucces(String apiRefNo, String blockId, String clientId, String bookingDate,
			String transactionRefNo, String ds) {
		String data = "";
		try {
			JSONObject payload = new JSONObject();
			payload.put("sessionId", ds);
			payload.put("apiRefNo", apiRefNo);
			payload.put("blockId", blockId);
			payload.put("clientId", clientId);
			payload.put("bookingDate", "2016-11-14");
			payload.put("transactionRefNo", transactionRefNo);
			Client client = Client.create();
			WebResource webResource = client.resource(UrlMetadatas.busBookingSuccessResponseUrl(Version.VERSION_1,
					Role.USER, Device.WEBSITE, Language.ENGLISH));
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("hash", SecurityUtils.getHash(payload.toString())).post(ClientResponse.class, payload);
			String output = response.getEntity(String.class);
			data = output;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	@Override
	public SourcesResponse getsources(SourcesRequest request) {
		SourcesResponse resp = new SourcesResponse();

		try {
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource("http://103.24.202.25/Buses/sources");
			ClientResponse response = webResource.accept("application/json").type("application/json")
					.header("ConsumerKey", "59285FC07292811D72785AC287ACE3E8")
					.header("ConsumerSecret", "04DAF1E2C80A758DCC6BCEBE436D25BC ").get(ClientResponse.class);
			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed , HTTP error code , " + response.getStatus());
			}
			String output = response.getEntity(String.class);
			resp.setMsg(output);
			resp.setCode(response.getStatus() + "");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}

	@Override
	public MultipleBlockSeatResponse multipleSeat(BlockBusRequest request) {
		MultipleBlockSeatResponse resp = new MultipleBlockSeatResponse();
		try {
			JSONObject payload = new JSONObject();
			payload.put("IsOfflineBooking", false);
			payload.put("TripId", request.getTripId());
			payload.put("BoardingId", request.getBoardingId());
			payload.put("NoofSeats", request.getNoofSeats());
			payload.put("Fares", request.getFares());
			payload.put("ConvenienceFee", request.getConvenienceFee());
			payload.put("SeatNos", request.getSeatNos());
			payload.put("Titles", request.getTitles());
			payload.put("Names", request.getNames());
			payload.put("Ages", request.getAges());
			payload.put("Genders", request.getGenders());
			payload.put("Address", request.getAddress());
			payload.put("PostalCode", request.getPostalCode());
			payload.put("IdCardType", request.getIdCardType());
			payload.put("IdCardNo", request.getIdCardNo());
			payload.put("IdCardIssuedBy", request.getIdCardIssuedBy());
			payload.put("MobileNo", request.getMobileNo());
			payload.put("EmergencyMobileNo", request.getMobileNo());
			payload.put("EmailId", request.getEmailId());
			payload.put("Provider", request.getProvider());
			payload.put("Operator", request.getOperator());
			payload.put("PartialCancellationAllowed", request.getPartialCancellationAllowed());
			payload.put("BoardingPointDetails", request.getBoardingPointDetails());
			payload.put("BusTypeName", request.getBusTypeName());
			payload.put("DepartureTime", request.getDepartureTime());
			payload.put("CancellationPolicy", request.getCancellationPolicy());
			payload.put("SourceId", request.getSourceId());
			payload.put("SourceName", request.getSourceName());
			payload.put("DestinationId", request.getDestinationId());
			payload.put("DestinationName", request.getDestinationName());
			payload.put("JourneyDate", request.getJourneyDate());
			payload.put("TripType", request.getTripType());
			payload.put("BusType", request.getBusType());
			payload.put("User", request.getUser());
			payload.put("UserType", request.getUserType());
			payload.put("ServiceTax", request.getServiceTax());
			payload.put("ServiceCharge", request.getServiceCharge());
			// payload.put("transactionRefNo", request.getTransctionrefno());
			Client client = Client.create();
			WebResource webResource = client.resource("http://103.24.202.25/Buses/BlockBusTicket");
			ClientResponse response = webResource.header("ConsumerKey", "59285FC07292811D72785AC287ACE3E8")
					.header("ConsumerSecret", "04DAF1E2C80A758DCC6BCEBE436D25BC  ")
					.header("Content-Type", "application/json").post(ClientResponse.class, payload);
			String output = response.getEntity(String.class);
			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed , HTTP error code , " + response.getStatus());
			} else {
				JSONObject obj = new JSONObject(output);
				String val1 = obj.getString("BookingReferenceNo");
				String status = obj.getString("ResponseStatus");
				resp.setMsg(output);
				resp.setCode(val1);
				resp.setCodecondtion(status);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}
}
