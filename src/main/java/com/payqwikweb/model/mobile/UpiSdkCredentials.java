package com.payqwikweb.model.mobile;

/**
 * @author Prashant
 *
 */
public class UpiSdkCredentials {

	private String merchantVPA;
	private String merTxnCurrency;
	private String merTerminalId;
	private String merDeviceInternet;
	private String merTransPassword;
	private String merchantId;
	private String merPayeeCode;
	private String merPaymentType;
	private String merSubMerchantId;
	private String merKek;
	private String merDek;

	public String getMerchantVPA() {
		return merchantVPA;
	}

	public void setMerchantVPA(String merchantVPA) {
		this.merchantVPA = merchantVPA;
	}

	public String getMerTxnCurrency() {
		return merTxnCurrency;
	}

	public void setMerTxnCurrency(String merTxnCurrency) {
		this.merTxnCurrency = merTxnCurrency;
	}

	public String getMerTerminalId() {
		return merTerminalId;
	}

	public void setMerTerminalId(String merTerminalId) {
		this.merTerminalId = merTerminalId;
	}

	public String getMerDeviceInternet() {
		return merDeviceInternet;
	}

	public void setMerDeviceInternet(String merDeviceInternet) {
		this.merDeviceInternet = merDeviceInternet;
	}

	public String getMerTransPassword() {
		return merTransPassword;
	}

	public void setMerTransPassword(String merTransPassword) {
		this.merTransPassword = merTransPassword;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getMerPayeeCode() {
		return merPayeeCode;
	}

	public void setMerPayeeCode(String merPayeeCode) {
		this.merPayeeCode = merPayeeCode;
	}

	public String getMerPaymentType() {
		return merPaymentType;
	}

	public void setMerPaymentType(String merPaymentType) {
		this.merPaymentType = merPaymentType;
	}

	public String getMerSubMerchantId() {
		return merSubMerchantId;
	}

	public void setMerSubMerchantId(String merSubMerchantId) {
		this.merSubMerchantId = merSubMerchantId;
	}

	public String getMerKek() {
		return merKek;
	}

	public void setMerKek(String merKek) {
		this.merKek = merKek;
	}

	public String getMerDek() {
		return merDek;
	}

	public void setMerDek(String merDek) {
		this.merDek = merDek;
	}
}
