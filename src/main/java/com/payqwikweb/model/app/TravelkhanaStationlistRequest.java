package com.payqwikweb.model.app;

public class TravelkhanaStationlistRequest {
      private int trainNum;
      private String sessionId;
      private String session_name;
  	

  	public String getSession_name() {
		return session_name;
	}

	public void setSession_name(String session_name) {
		this.session_name = session_name;
	}

	public String getSessionId() {
  		return sessionId;
  	}

  	public void setSessionId(String sessionId) {
  		this.sessionId = sessionId;
  	}

	public int getTrainNum() {
		return trainNum;
	}

	public void setTrainNum(int trainNum) {
		this.trainNum = trainNum;
	}
  	
}
