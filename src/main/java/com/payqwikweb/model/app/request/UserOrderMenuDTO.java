package com.payqwikweb.model.app.request;

public class UserOrderMenuDTO {
	
	private double itemBasePrice;
	private long quantity;
	private String image;
	private String itemId;
	private String item_name;
	
	public double getItemBasePrice() {
		return itemBasePrice;
	}
	public void setItemBasePrice(double itemBasePrice) {
		this.itemBasePrice = itemBasePrice;
	}
	public long getQuantity() {
		return quantity;
	}
	public void setQuantity(long quantity) {
		this.quantity = quantity;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getItem_name() {
		return item_name;
	}
	public void setItem_name(String item_name) {
		this.item_name = item_name;
	}
	
	
}