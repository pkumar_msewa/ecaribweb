package com.payqwikweb.model.app.request;

import org.springframework.web.multipart.MultipartFile;

public class AgentRegistrationRequest {

	private String sessionId;
	private String firstName;
	private String lastName;
	private String agencyName;
	private String mobileNo;
	private String emailAddress;
	private String address;		//add1
	private String city;
	private String state;
	private String country;
	private String pinCode;

	private String panNo;
	private MultipartFile panCardImg;
	private String password;
	// private String aadharNo;
	private String userType;
	// agentBankDetail:
	private String agentAccountName;
	private String agentBankName;
	private String agentAccountNumber;
	private String agentbranchname;
	private String agentBankLocation;
	private String agentBankIfscCode;
	
	
	public String getAgencyName() {
		return agencyName;
	}

	public void setAgencyName(String agencyName) {
		this.agencyName = agencyName;
	}

	public MultipartFile getPanCardImg() {
		return panCardImg;
	}

	public void setPanCardImg(MultipartFile panCardImg) {
		this.panCardImg = panCardImg;
	}

	public String getAgentBankIfscCode() {
		return agentBankIfscCode;
	}

	public void setAgentBankIfscCode(String agentBankIfscCode) {
		this.agentBankIfscCode = agentBankIfscCode;
	}

	public String getAgentbranchname() {
		return agentbranchname;
	}

	public void setAgentbranchname(String agentbranchname) {
		this.agentbranchname = agentbranchname;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public String getPanNo() {
		return panNo;
	}

	public void setPanNo(String panNo) {
		this.panNo = panNo;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getAgentAccountName() {
		return agentAccountName;
	}

	public void setAgentAccountName(String agentAccountName) {
		this.agentAccountName = agentAccountName;
	}

	public String getAgentBankName() {
		return agentBankName;
	}

	public void setAgentBankName(String agentBankName) {
		this.agentBankName = agentBankName;
	}

	public String getAgentAccountNumber() {
		return agentAccountNumber;
	}

	public void setAgentAccountNumber(String agentAccountNumber) {
		this.agentAccountNumber = agentAccountNumber;
	}

	public String getAgentBankLocation() {
		return agentBankLocation;
	}

	public void setAgentBankLocation(String agentBankLocation) {
		this.agentBankLocation = agentBankLocation;
	}

}
