package com.payqwikweb.model.app.request;

public class GCIOrderProductRequest {
	
private String brandHash;
private String denomination;
private String productType;
private String quantity;
private String productId;


public String getProductId() {
	return productId;
}
public void setProductId(String productId) {
	this.productId = productId;
}
public String getBrandHash() {
	return brandHash;
}
public void setBrandHash(String brandHash) {
	this.brandHash = brandHash;
}
public String getDenomination() {
	return denomination;
}
public void setDenomination(String denomination) {
	this.denomination = denomination;
}
public String getProductType() {
	return productType;
}
public void setProductType(String productType) {
	this.productType = productType;
}
public String getQuantity() {
	return quantity;
}
public void setQuantity(String quantity) {
	this.quantity = quantity;
}


}
