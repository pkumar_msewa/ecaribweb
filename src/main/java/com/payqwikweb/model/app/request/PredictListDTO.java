package com.payqwikweb.model.app.request;

public class PredictListDTO {

	private String matchDate;
	private String firstteamFullName;
	private String secondTeamFullName;
	private String teamCodeOne;
	private String teamcodeSecond;
	
	public String getTeamCodeOne() {
		return teamCodeOne;
	}

	public void setTeamCodeOne(String teamCodeOne) {
		this.teamCodeOne = teamCodeOne;
	}

	public String getTeamcodeSecond() {
		return teamcodeSecond;
	}

	public void setTeamcodeSecond(String teamcodeSecond) {
		this.teamcodeSecond = teamcodeSecond;
	}

	public String getFirstteamFullName() {
		return firstteamFullName;
	}

	public void setFirstteamFullName(String firstteamFullName) {
		this.firstteamFullName = firstteamFullName;
	}

	public String getSecondTeamFullName() {
		return secondTeamFullName;
	}

	public void setSecondTeamFullName(String secondTeamFullName) {
		this.secondTeamFullName = secondTeamFullName;
	}

	public String getMatchDate() {
		return matchDate;
	}

	public void setMatchDate(String matchDate) {
		this.matchDate = matchDate;
	}
	
	
}
