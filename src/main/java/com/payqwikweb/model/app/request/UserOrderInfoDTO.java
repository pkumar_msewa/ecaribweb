package com.payqwikweb.model.app.request;

public class UserOrderInfoDTO {
	
	private double totalCapped;
	private double totalCustomerPayable;
	private double totalCustomerPayableCapped;
	private double totalBasePrice;
	private double discount;
	private double delivery_cost;
	private String station_code;
	private String voucher_code;
	private String order_outlet_id;
	private String train_no;
	private String date;
	private double taxes;
	private String cod;	//Cash On Delivery

	public double getTotalCapped() {
		return totalCapped;
	}
	public void setTotalCapped(double totalCapped) {
		this.totalCapped = totalCapped;
	}
	public double getTotalCustomerPayable() {
		return totalCustomerPayable;
	}
	public void setTotalCustomerPayable(double totalCustomerPayable) {
		this.totalCustomerPayable = totalCustomerPayable;
	}
	public double getTotalCustomerPayableCapped() {
		return totalCustomerPayableCapped;
	}
	public void setTotalCustomerPayableCapped(double totalCustomerPayableCapped) {
		this.totalCustomerPayableCapped = totalCustomerPayableCapped;
	}
	public double getTotalBasePrice() {
		return totalBasePrice;
	}
	public void setTotalBasePrice(double totalBasePrice) {
		this.totalBasePrice = totalBasePrice;
	}
	public double getDiscount() {
		return discount;
	}
	public void setDiscount(double discount) {
		this.discount = discount;
	}
	public double getDelivery_cost() {
		return delivery_cost;
	}
	public void setDelivery_cost(double delivery_cost) {
		this.delivery_cost = delivery_cost;
	}
	public String getStation_code() {
		return station_code;
	}
	public void setStation_code(String station_code) {
		this.station_code = station_code;
	}
	public String getVoucher_code() {
		return voucher_code;
	}
	public void setVoucher_code(String voucher_code) {
		this.voucher_code = voucher_code;
	}
	public String getOrder_outlet_id() {
		return order_outlet_id;
	}
	public void setOrder_outlet_id(String order_outlet_id) {
		this.order_outlet_id = order_outlet_id;
	}
	public String getTrain_no() {
		return train_no;
	}
	public void setTrain_no(String train_no) {
		this.train_no = train_no;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getCod() {
		return cod;
	}
	public void setCod(String cod) {
		this.cod = cod;
	}

}
