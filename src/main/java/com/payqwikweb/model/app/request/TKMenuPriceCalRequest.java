package com.payqwikweb.model.app.request;

import java.io.StringWriter;
import java.util.ArrayList;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonWriter;

import org.codehaus.jettison.json.JSONException;

import com.payqwikweb.app.model.request.TKJsonRequest;

public class TKMenuPriceCalRequest implements TKJsonRequest{
	
    private String sessionId;
	private String station_code;
	private String order_outlet_id;
	private String train_no;
	private String date;
	private String data;
	private String image;
	private double itemBasePrice;
	/*private long quantity;
	private long itemId;*/
	private ArrayList<TKItemsRequest>orderMenu;
	private String item_name;
	
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public ArrayList<TKItemsRequest> getOrderMenu() {
		return orderMenu;
	}
	public void setOrderMenu(ArrayList<TKItemsRequest> orderMenu) {
		this.orderMenu = orderMenu;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public String getStation_code() {
		return station_code;
	}
	public void setStation_code(String station_code) {
		this.station_code = station_code;
	}
	public String getOrder_outlet_id() {
		return order_outlet_id;
	}
	public void setOrder_outlet_id(String order_outlet_id) {
		this.order_outlet_id = order_outlet_id;
	}
	public String getTrain_no() {
		return train_no;
	}
	public void setTrain_no(String train_no) {
		this.train_no = train_no;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public double getItemBasePrice() {
		return itemBasePrice;
	}
	public void setItemBasePrice(double itemBasePrice) {
		this.itemBasePrice = itemBasePrice;
	}
	public String getItem_name() {
		return item_name;
	}
	public void setItem_name(String item_name) {
		this.item_name = item_name;
	}
	@Override
	public String getJsonRequest() throws JSONException, org.json.JSONException {
	 
		    JsonObjectBuilder jsonBuilder = Json.createObjectBuilder();
			JsonArrayBuilder  orderMenu = Json.createArrayBuilder();
			JsonObjectBuilder orderMenuObj = Json.createObjectBuilder();
			jsonBuilder.add("sessionId",getSessionId());
			for(int i=0;i<getOrderMenu().size();i++)
			{
				orderMenuObj.add("itemId",getOrderMenu().get(i).getItemId());
				orderMenuObj.add("quantity",getOrderMenu().get(i).getQuantity());
				orderMenu.add(orderMenuObj.build());
			}
			jsonBuilder.add("orderMenu",orderMenu);
			jsonBuilder.add("order_outlet_id",getOrder_outlet_id());
	
			 JsonObject empObj = jsonBuilder.build();
		     StringWriter jsnReqStr = new StringWriter();
		     JsonWriter jsonWtr = Json.createWriter(jsnReqStr);
		     jsonWtr.writeObject(empObj);
		     jsonWtr.close();
		     System.out.println(jsnReqStr.toString());
				
		   return jsnReqStr.toString();	
		   	
	}
	@Override
	public String getJsonRequest2() {
		JsonObjectBuilder jsonBuilder = Json.createObjectBuilder();
		JsonObjectBuilder orderInfo = Json.createObjectBuilder();
		orderInfo.add("totalCapped","");orderInfo.add("totalCustomerPayable","");
		orderInfo.add("totalCustomerPayableCapped","");orderInfo.add("totalBasePrice","");
		orderInfo.add("discount",0);orderInfo.add("delivery_cost",0);
		orderInfo.add("station_code","");orderInfo.add("voucher_code","");
		orderInfo.add("order_outlet_id",getOrder_outlet_id());
		orderInfo.add("train_no","");orderInfo.add("date","");orderInfo.add("cod","");
		jsonBuilder.add("userOrderInfo",orderInfo);
		
		JsonArrayBuilder  orderMenu = Json.createArrayBuilder();
		JsonObjectBuilder orderMenuObj = Json.createObjectBuilder();
		for(int i=0;i<getOrderMenu().size();i++)
		{
			orderMenuObj.add("itemId",getOrderMenu().get(i).getItemId());
			orderMenuObj.add("quantity",getOrderMenu().get(i).getQuantity());
			orderMenuObj.add("itemBasePrice","");
			orderMenuObj.add("image","");
			orderMenuObj.add("item_name","");
			orderMenu.add(orderMenuObj.build());
		}
		jsonBuilder.add("userOrderMenu",orderMenu);
		jsonBuilder.add("minAmountThreshold","");
		jsonBuilder.add("outletminorderAmount","");
	
		 JsonObject empObj = jsonBuilder.build();
	     StringWriter jsnReqStr = new StringWriter();
	     JsonWriter jsonWtr = Json.createWriter(jsnReqStr);
	     jsonWtr.writeObject(empObj);
	     jsonWtr.close();
	     System.out.println(jsnReqStr.toString());
			
	   return jsnReqStr.toString();	
	}	
}
