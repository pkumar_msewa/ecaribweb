package com.payqwikweb.model.app.request;

import java.io.StringWriter;
import java.util.ArrayList;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonWriter;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.json.JSONArray;

import com.payqwikweb.app.model.request.TKJsonRequest;

public class TKPlaceOrderRequest implements TKJsonRequest {
//public class TKPlaceOrderRequest{
	
	private String sessionId;
	
	//userOrderMenu 
	private ArrayList<TKItemsRequest> items;
	
	//userOrderInfo
	private String cod;
	private String order_outlet_id;
	private String date;
	private String pnr;
	private String station_code;
	private String coach;
	private String seat;
	private String customer_comment;
	private double totalCustomerPayable;
	
	//userBasicInfo
	private String name;
	private String contact_no;
	private String mail_id;
	
	//trainUpdateInfo
	private String train_number;
	private String eta;
	private String data;
	
	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getSessionId() {
		return sessionId;
	}
     
	public ArrayList<TKItemsRequest> getItems() {
		return items;
	}

	public void setItemsDto(ArrayList<TKItemsRequest> items) {
		this.items = items;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getCod() {
		return cod;
	}

	public void setCod(String cod) {
		this.cod = cod;
	}

	public String getOrder_outlet_id() {
		return order_outlet_id;
	}

	public void setOrder_outlet_id(String order_outlet_id) {
		this.order_outlet_id = order_outlet_id;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getStation_code() {
		return station_code;
	}

	public void setStation_code(String station_code) {
		this.station_code = station_code;
	}

	public String getCoach() {
		return coach;
	}

	public void setCoach(String coach) {
		this.coach = coach;
	}

	public String getSeat() {
		return seat;
	}

	public void setSeat(String seat) {
		this.seat = seat;
	}

	public String getCustomer_comment() {
		return customer_comment;
	}

	public void setCustomer_comment(String customer_comment) {
		this.customer_comment = customer_comment;
	}

	public double getTotalCustomerPayable() {
		return totalCustomerPayable;
	}

	public void setTotalCustomerPayable(double totalCustomerPayable) {
		this.totalCustomerPayable = totalCustomerPayable;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContact_no() {
		return contact_no;
	}

	public void setContact_no(String contact_no) {
		this.contact_no = contact_no;
	}

	public String getMail_id() {
		return mail_id;
	}

	public void setMail_id(String mail_id) {
		this.mail_id = mail_id;
	}

	public String getTrain_number() {
		return train_number;
	}

	public void setTrain_number(String train_number) {
		this.train_number = train_number;
	}

	public String getEta() {
		return eta;
	}

	public void setEta(String eta) {
		this.eta = eta;
	}

	 @Override
		public String getJsonRequest() throws JSONException, org.json.JSONException {
		 JsonObjectBuilder jsonBuilder = Json.createObjectBuilder();
			JsonArrayBuilder  orderMenu = Json.createArrayBuilder();
			JsonObjectBuilder orderMenuObj = Json.createObjectBuilder();
			jsonBuilder.add("sessionId",getSessionId());
			for(int i=0;i<getItems().size();i++)
			{
				orderMenuObj.add("itemId",getItems().get(i).getItemId());
				orderMenuObj.add("quantity",getItems().get(i).getQuantity());
				orderMenu.add(orderMenuObj.build());
			}
			jsonBuilder.add("items",orderMenu);
			jsonBuilder.add("station_code",getStation_code());jsonBuilder.add("cod",getCod());
			jsonBuilder.add("order_outlet_id",getOrder_outlet_id());
			jsonBuilder.add("date",getDate());jsonBuilder.add("pnr",getPnr());
			jsonBuilder.add("coach",getCoach());
			jsonBuilder.add("seat",getSeat());jsonBuilder.add("customer_comment",getCustomer_comment());
			jsonBuilder.add("totalCustomerPayable",getTotalCustomerPayable());
			
			jsonBuilder.add("name",getName());
			jsonBuilder.add("contact_no",getContact_no());
			jsonBuilder.add("mail_id",getMail_id());

			jsonBuilder.add("train_number",getTrain_number());
			jsonBuilder.add("station_code",getStation_code());
			jsonBuilder.add("eta",getEta());
			
			 JsonObject empObj = jsonBuilder.build();
		     StringWriter jsnReqStr = new StringWriter();
		     JsonWriter jsonWtr = Json.createWriter(jsnReqStr);
		     jsonWtr.writeObject(empObj);
		     jsonWtr.close();
		     System.out.println(jsnReqStr.toString());
				
		   return jsnReqStr.toString();	
		   	
	   }
	   	@Override
		public String getJsonRequest2(){	
	   	
		JsonObjectBuilder jsonBuilder = Json.createObjectBuilder();
		JsonArrayBuilder  orderMenu = Json.createArrayBuilder();
		JsonObjectBuilder orderMenuObj = Json.createObjectBuilder();
		for(int i=0;i<getItems().size();i++)
		{
			orderMenuObj.add("itemId",getItems().get(i).getItemId());
			orderMenuObj.add("quantity",getItems().get(i).getQuantity());
			orderMenuObj.add("tkServiceTax","");
			orderMenuObj.add("SwachhBharatCess","");
			orderMenuObj.add("KrishiKalyanCess","");
			orderMenu.add(orderMenuObj.build());
		}
		jsonBuilder.add("userOrderMenu",orderMenu);
		
		JsonObjectBuilder orderInfo = Json.createObjectBuilder();
		orderInfo.add("station_code",getStation_code());orderInfo.add("cod",getCod());
		orderInfo.add("order_outlet_id",getOrder_outlet_id());
		orderInfo.add("date",getDate());orderInfo.add("pnr",getPnr());
		orderInfo.add("train_no",getTrain_number());orderInfo.add("coach",getCoach());
		orderInfo.add("seat",getSeat());orderInfo.add("customer_comment",getCustomer_comment());
		orderInfo.add("totalCustomerPayable",getTotalCustomerPayable());orderInfo.add("delivery_cost","0");
		orderInfo.add("online_pay_status","0");orderInfo.add("discount","");
		orderInfo.add("voucher_code","");orderInfo.add("adv_option","");
		orderInfo.add("adv_txn_id","");orderInfo.add("adv_bank","");
		orderInfo.add("adv_pay_date","");orderInfo.add("advance_amount","");
		orderInfo.add("SwachhBharatCess","");orderInfo.add("KrishiKalyanCess","");
		orderInfo.add("tkServiceTax","");
		
		JsonObjectBuilder userInfo = Json.createObjectBuilder();
		userInfo.add("name",getName());
		userInfo.add("contact_no",getContact_no());
		userInfo.add("mail_id",getMail_id());
		userInfo.add("login_id","");
	
		JsonObjectBuilder trainnfo = Json.createObjectBuilder();
		trainnfo.add("train_number",getTrain_number());
		trainnfo.add("station_code",getStation_code());
		trainnfo.add("date",getDate());
		trainnfo.add("sta","");
		trainnfo.add("eta",getEta());
		
		jsonBuilder.add("userOrderInfo", orderInfo);
		jsonBuilder.add("userBasicInfo", userInfo);
		jsonBuilder.add("trainUpdateInfo", trainnfo);
		
		 JsonObject empObj = jsonBuilder.build();
	     StringWriter jsnReqStr = new StringWriter();
	     JsonWriter jsonWtr = Json.createWriter(jsnReqStr);
	     jsonWtr.writeObject(empObj);
	     jsonWtr.close();
	     System.out.println(jsnReqStr.toString());
			
	   return jsnReqStr.toString();	
	}
	
}
	/*javax.json.JsonObject payload1 = Json.createObjectBuilder()
		 	.add("userOrderMenu",Json.createArrayBuilder()
		 			.add(Json.createObjectBuilder()
		 			//	.add("itemId",dto.getItemId()).add("quantity",dto.getQuantity())
		 				.add("tkServiceTax","")
		 				.add("SwachhBharatCess","")
		 				.add("KrishiKalyanCess","")
		 				)
		 		)
		 	.add("userOrderInfo",Json.createObjectBuilder()
		 			.add("delivery_cost","0").add("cod",dto.getCod()).add("online_pay_status","0")
		 			.add("order_outlet_id",dto.getOrder_outlet_id()).add("discount","")
		 			.add("voucher_code","").add("adv_option","").add("adv_txn_id","")
		 			.add("adv_bank","").add("adv_pay_date","").add("date",dto.getDate())
		 			.add("pnr",dto.getPnr()).add("advance_amount","").add("station_code",dto.getStation_code())
		 			.add("train_no", dto.getTrain_number()).add("coach",dto.getCoach())
		 			.add("seat",dto.getSeat()).add("customer_comment",dto.getCustomer_comment())
		 			.add("totalCustomerPayable",dto.getTotalCustomerPayable()).add("SwachhBharatCess","")
		 			.add("KrishiKalyanCess","")
		 			.add("tkServiceTax",""))
		 	
			.add("userBasicInfo", Json.createObjectBuilder()
					.add("login_id","").add("name",dto.getName()).add("contact_no",dto.getContact_no())
					.add("mail_id",dto.getMail_id()))
			
			.add("trainUpdateInfo",Json.createObjectBuilder()
					.add("train_number",dto.getTrain_number()).add("station_code",dto.getStation_code())
					.add("date",dto.getDate()).add("sta","").add("eta",dto.getEta()))
		 			.build();
*/
	
	