package com.payqwikweb.model.app.request;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.thirdparty.model.JSONWrapper;

public class AddServicesDTO implements JSONWrapper {

	private String name;
	private String code;
	private String description;
	private String operator;
	private String serviceType;
	private String sessionId;
	private String minAmount;
	private String maxAmount;
	private boolean fixed;
	private String value;
	private String type;
	private boolean bbpsEnabled;
	

	public boolean isBbpsEnabled() {
		return bbpsEnabled;
	}

	public void setBbpsEnabled(boolean bbpsEnabled) {
		this.bbpsEnabled = bbpsEnabled;
	}

	public boolean isFixed() {
		return fixed;
	}

	public void setFixed(boolean fixed) {
		this.fixed = fixed;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getMinAmount() {
		return minAmount;
	}

	public void setMinAmount(String minAmount) {
		this.minAmount = minAmount;
	}

	public String getMaxAmount() {
		return maxAmount;
	}

	public void setMaxAmount(String maxAmount) {
		this.maxAmount = maxAmount;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}
	
	@Override
	public JSONObject toJSON() {
		JSONObject json = new JSONObject();
		try {
			json.put("sessionId", getSessionId());
			json.put("fixed", isFixed());
			json.put("value", getValue());
			json.put("description", getDescription());
			json.put("minAmount", getMinAmount());
			json.put("maxAmount", getMaxAmount());
			json.put("serviceType",getServiceType());
			if(getServiceType().equalsIgnoreCase("Bill Payment")){
			json.put("code", "V"+getCode());
			}else{
				json.put("code",getCode());	
			}
			json.put("operator", getOperator());
			json.put("name", getName());
			json.put("type", getType());
			json.put("bbpsEnabled", isBbpsEnabled());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return json;
	}

}
