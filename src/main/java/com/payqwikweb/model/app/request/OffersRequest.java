package com.payqwikweb.model.app.request;

import com.payqwikweb.util.Status;

public class OffersRequest {

	private Status activeOffer;
	private String offers;
	private String services;
	private String sessionId;
	

	public String getServices() {
		return services;
	}

	public void setServices(String services) {
		this.services = services;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public Status getActiveOffer() {
		return activeOffer;
	}

	public void setActiveOffer(Status activeOffer) {
		this.activeOffer = activeOffer;
	}

	public String getOffers() {
		return offers;
	}

	public void setOffers(String offers) {
		this.offers = offers;
	}

}
