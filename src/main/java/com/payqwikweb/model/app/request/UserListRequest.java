package com.payqwikweb.model.app.request;

import com.payqwikweb.app.model.request.SessionDTO;

public class UserListRequest extends SessionDTO{
    private String startDate;
    private String endDate;

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}
