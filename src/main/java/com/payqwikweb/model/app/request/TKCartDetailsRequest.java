package com.payqwikweb.model.app.request;

public class TKCartDetailsRequest {
 
	 private String couponCode;
	 private double totalSP;
//	 private String minAmountThreshold;
	 private long outletId;
	 
	public String getCouponCode() {
		return couponCode;
	}
	public void setCouponCode(String couponCode) {
		this.couponCode = couponCode;
	}
	public double getTotalSP() {
		return totalSP;
	}
	public void setTotalSP(double totalSP) {
		this.totalSP = totalSP;
	}
	/*public String getMinAmountThreshold() {
		return minAmountThreshold;
	}
	public void setMinAmountThreshold(String minAmountThreshold) {
		this.minAmountThreshold = minAmountThreshold;
	}*/
	public long getOutletId() {
		return outletId;
	}
	public void setOutletId(long outletId) {
		this.outletId = outletId;
	}
	 
	 
}