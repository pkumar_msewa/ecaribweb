package com.payqwikweb.model.app.request;

import java.util.ArrayList;

public class TKTrainslistRequest {
	//Station List By TrainNumber
	private String sessionId;
	private String trainNumber;
	private String data;
	//OutletList In Time
	private String arrivalTime;
	private String station;              //Station_Code
	private String date;
	
	//MenuList In Time
	private String outletId;
	
	//trainRooutesMenu
	
	//TrackUserOrder
	private long orderId;
	
	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public long getOrderId() {
		return orderId;
	}

	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}



	public String getTrainNumber() {
		return trainNumber;
	}

	public void setTrainNumber(String trainNumber) {
		this.trainNumber = trainNumber;
	}

	public String getSessionId() {
		return sessionId;
	}
	public String getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public String getStation() {
		return station;
	}

	public void setStation(String station) {
		this.station = station;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getOutletId() {
		return outletId;
	}

	public void setOutletId(String outletId) {
		this.outletId = outletId;
	}
	
	
}
