package com.payqwikweb.model.app.request;

import com.payqwikweb.app.model.request.SessionDTO;

public class HouseJoyRequestDTO extends SessionDTO {

	private String sessionId;
	private double amount;
	private String orderId;
	private String mobileNo;
	private String token;
	private String merchantUserName;
	private String transactionRefNo;
	private String code;
	private String UserEmail;
	private String UserName;
	private String serviceName;
	private String jobId;
	private boolean paymentOffline;
	private String hjtxnRefNo;
	private String customerName;
	private String address;
	private String date;
	private String mobile;
	private String time;
	
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getMerchantUserName() {
		return merchantUserName;
	}
	public void setMerchantUserName(String merchantUserName) {
		this.merchantUserName = merchantUserName;
	}
	public String getTransactionRefNo() {
		return transactionRefNo;
	}
	public void setTransactionRefNo(String transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getUserEmail() {
		return UserEmail;
	}
	public void setUserEmail(String UserEmail) {
		this.UserEmail = UserEmail;
	}
	public String getUserName() {
		return UserName;
	}
	public void setUserName(String UserName) {
		this.UserName = UserName;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public String getJobId() {
		return jobId;
	}
	public void setJobId(String jobId) {
		this.jobId = jobId;
	}
	public boolean isPaymentOffline() {
		return paymentOffline;
	}
	public void setPaymentOffline(boolean paymentOffline) {
		this.paymentOffline = paymentOffline;
	}
	public String getHjtxnRefNo() {
		return hjtxnRefNo;
	}
	public void setHjtxnRefNo(String hjtxnRefNo) {
		this.hjtxnRefNo = hjtxnRefNo;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	
}
