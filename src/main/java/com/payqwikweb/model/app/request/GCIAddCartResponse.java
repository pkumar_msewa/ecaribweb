package com.payqwikweb.model.app.request;

public class GCIAddCartResponse {
	
   private String productType;
   private String  quantity;
   private String amount;
   private String brandName;
   private String denomination;
   private String brandHash;
public String getProductType() {
	return productType;
}
public void setProductType(String productType) {
	this.productType = productType;
}
public String getQuantity() {
	return quantity;
}
public void setQuantity(String quantity) {
	this.quantity = quantity;
}
public String getAmount() {
	return amount;
}
public void setAmount(String amount) {
	this.amount = amount;
}
public String getBrandName() {
	return brandName;
}
public void setBrandName(String brandName) {
	this.brandName = brandName;
}
public String getDenomination() {
	return denomination;
}
public void setDenomination(String denomination) {
	this.denomination = denomination;
}
public String getBrandHash() {
	return brandHash;
}
public void setBrandHash(String brandHash) {
	this.brandHash = brandHash;
}

}
