package com.payqwikweb.model.app.request;

import com.payqwikweb.app.model.request.SessionDTO;

public class VersionRequest extends SessionDTO {
	
	private String version;
	private String status;
	private String code;
	

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
