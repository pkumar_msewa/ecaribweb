package com.payqwikweb.model.app.request;

public class GciMobileRequest 
{
	private String sessionId;

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

}
