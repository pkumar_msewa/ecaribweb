package com.payqwikweb.model.app.request;

public class VersionRequestDTO {

	private String versionName;
	private String key;
	private boolean web;
	private boolean splash;
	
	public String getVersionName() {
		return versionName;
	}
	public void setVersionName(String versionName) {
		this.versionName = versionName;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public boolean isWeb() {
		return web;
	}
	public void setWeb(boolean web) {
		this.web = web;
	}
	public boolean isSplash() {
		return splash;
	}
	public void setSplash(boolean splash) {
		this.splash = splash;
	}
}
