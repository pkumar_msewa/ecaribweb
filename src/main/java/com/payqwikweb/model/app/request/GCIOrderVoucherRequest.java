package com.payqwikweb.model.app.request;

public class GCIOrderVoucherRequest {
private String receiptNo ;
private String  accessToken;
public String getReceiptNo() {
	return receiptNo;
}
public void setReceiptNo(String receiptNo) {
	this.receiptNo = receiptNo;
}
public String getAccessToken() {
	return accessToken;
}
public void setAccessToken(String accessToken) {
	this.accessToken = accessToken;
}


}
