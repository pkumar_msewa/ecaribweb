package com.payqwikweb.model.app.request;

import java.io.StringWriter;
import java.util.ArrayList;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonWriter;

import org.codehaus.jettison.json.JSONException;

import com.payqwikweb.model.app.request.TKCartDetailsRequest;
import com.payqwikweb.app.model.request.TKJsonRequest;

public class TKApplyCouponRequest implements TKJsonRequest{
	
    private String sessionId;
	
    private ArrayList<TKCartDetailsRequest>cartDetails;
    private String mobileNumber;
    private String mailId;
    private String data;
    
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public ArrayList<TKCartDetailsRequest> getCartDetails() {
		return cartDetails;
	}
	public void setCartDetails(ArrayList<TKCartDetailsRequest> cartDetails) {
		this.cartDetails = cartDetails;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getMailId() {
		return mailId;
	}
	public void setMailId(String mailId) {
		this.mailId = mailId;
	}/*"cartDetailBean": [
    {
    "couponCode": "CC2000",
    "totalSP": 291.422,
    "totalCustomerPayable":"",
    "totalCustomerPayableCapped":"",
    "minAmountThreshold": "310",
    "outletId": 1146,
    
    "passengerInfoBean": {
"passengerName": "",
"mobileNumber": "1222222222",
"mailId": "info@duronto.in",
    */
	@Override
	public String getJsonRequest() throws JSONException, org.json.JSONException {
	 JsonObjectBuilder jsonBuilder = Json.createObjectBuilder();
		JsonArrayBuilder  orderMenu = Json.createArrayBuilder();
		JsonObjectBuilder orderMenuObj = Json.createObjectBuilder();
		jsonBuilder.add("sessionId",getSessionId());
		for(int i=0;i<getCartDetails().size();i++)
		{
			orderMenuObj.add("couponCode",getCartDetails().get(i).getCouponCode());
			orderMenuObj.add("totalSP",getCartDetails().get(i).getTotalSP());
	//		orderMenuObj.add("minAmountThreshold",getCartDetails().get(i).getMinAmountThreshold());
			orderMenuObj.add("outletId",getCartDetails().get(i).getOutletId());
			orderMenu.add(orderMenuObj.build());
		}
		jsonBuilder.add("cartDetailBean",orderMenu);
		jsonBuilder.add("mobileNumber",getMobileNumber());
		jsonBuilder.add("mailId",getMailId());
		
		 JsonObject empObj = jsonBuilder.build();
	     StringWriter jsnReqStr = new StringWriter();
	     JsonWriter jsonWtr = Json.createWriter(jsnReqStr);
	     jsonWtr.writeObject(empObj);
	     jsonWtr.close();
	     System.out.println(jsnReqStr.toString());
			
	   return jsnReqStr.toString();	
	   	
   }
   	@Override
	public String getJsonRequest2(){	
   		JsonObjectBuilder jsonBuilder = Json.createObjectBuilder();
		JsonArrayBuilder  orderMenu = Json.createArrayBuilder();
		JsonObjectBuilder orderMenuObj = Json.createObjectBuilder();
		for(int i=0;i<getCartDetails().size();i++)
		{
			orderMenuObj.add("couponCode",getCartDetails().get(i).getCouponCode());
			orderMenuObj.add("totalSP",getCartDetails().get(i).getTotalSP());
			orderMenuObj.add("outletId",getCartDetails().get(i).getOutletId());
			orderMenu.add(orderMenuObj.build());
		}
		jsonBuilder.add("cartDetailBean",orderMenu);
		
	    JsonObjectBuilder passengerInfo = Json.createObjectBuilder();
		passengerInfo.add("mobileNumber",getMobileNumber());
		passengerInfo.add("mailId",getMailId());
		passengerInfo.add("channel",12);
		jsonBuilder.add("passengerInfoBean",passengerInfo);
		
		 JsonObject empObj = jsonBuilder.build();
	     StringWriter jsnReqStr = new StringWriter();
	     JsonWriter jsonWtr = Json.createWriter(jsnReqStr);
	     jsonWtr.writeObject(empObj);
	     jsonWtr.close();
	     System.out.println(jsnReqStr.toString());
		
   return jsnReqStr.toString();	
}
    
}

/*"cartDetailBean": [
                   {
                     "couponCode": "CC2000",
                     "totalSP": 291.422,
                     "totalCustomerPayable":"",
                     "totalCustomerPayableCapped":"",
                     "minAmountThreshold": "310",
                     "outletId": 1146,
                     
                     "passengerInfoBean": {
    "passengerName": "",
    "mobileNumber": "1222222222",
    "mailId": "info@duronto.in",
                     */