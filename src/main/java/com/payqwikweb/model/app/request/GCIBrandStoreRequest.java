package com.payqwikweb.model.app.request;

public class GCIBrandStoreRequest {
	
	private String accessToken;
	private String hash;
	public String getAccessToken() {
		return accessToken;
	}
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	public String getHash() {
		return hash;
	}
	public void setHash(String hash) {
		this.hash = hash;
	}
	

}
