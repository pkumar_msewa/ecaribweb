package com.payqwikweb.model.app.request;

public class GCIAmountInitateRequest {
	
	private String sessioniId;
	private String amount;
	private String brandName;
	
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}public String getSessioniId() {
		return sessioniId;
	}
	public void setSessioniId(String sessioniId) {
		this.sessioniId = sessioniId;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	
}
