package com.payqwikweb.model.app.response;

public class AdlabsAddOrder {

	private String success_profile;
	private String success_payment;
	private String error;

	public String getSuccess_profile() {
		return success_profile;
	}

	public void setSuccess_profile(String success_profile) {
		this.success_profile = success_profile;
	}

	public String getSuccess_payment() {
		return success_payment;
	}

	public void setSuccess_payment(String success_payment) {
		this.success_payment = success_payment;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

}
