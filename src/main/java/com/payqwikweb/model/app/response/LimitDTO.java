package com.payqwikweb.model.app.response;

public class LimitDTO {

    private String status;
    private String code;
    private String message;
    private double balanceLimit;
    private double dailyLimit;
    private double monthlyLimit;
    private double creditConsumed;
    private double debitConsumed;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public double getBalanceLimit() {
        return balanceLimit;
    }

    public void setBalanceLimit(double balanceLimit) {
        this.balanceLimit = balanceLimit;
    }

    public double getDailyLimit() {
        return dailyLimit;
    }

    public void setDailyLimit(double dailyLimit) {
        this.dailyLimit = dailyLimit;
    }

    public double getMonthlyLimit() {
        return monthlyLimit;
    }

    public void setMonthlyLimit(double monthlyLimit) {
        this.monthlyLimit = monthlyLimit;
    }

    public double getCreditConsumed() {
        return creditConsumed;
    }

    public void setCreditConsumed(double creditConsumed) {
        this.creditConsumed = creditConsumed;
    }

    public double getDebitConsumed() {
        return debitConsumed;
    }

    public void setDebitConsumed(double debitConsumed) {
        this.debitConsumed = debitConsumed;
    }

}
