package com.payqwikweb.model.app.response;

public class EventOccurence {

	private String occurrence_id;
	private String end_date;
	private String time;
	private String end_time;
	private String date;

	public String getOccurrence_id() {
		return occurrence_id;
	}

	public void setOccurrence_id(String occurrence_id) {
		this.occurrence_id = occurrence_id;
	}

	public String getEnd_date() {
		return end_date;
	}

	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getEnd_time() {
		return end_time;
	}

	public void setEnd_time(String end_time) {
		this.end_time = end_time;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

}
