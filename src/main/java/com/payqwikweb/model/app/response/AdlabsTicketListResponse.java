package com.payqwikweb.model.app.response;

import com.payqwikweb.app.model.ResponseStatus;

public class AdlabsTicketListResponse {

	private boolean success;
	private String code;
	private String message;
	private ResponseStatus status;
	private Object details;
	private String response;
	private Object jsonArray;
	// Api response

	private String prodType;
	private String ticket_des;
	private String title;
	private String image_url;
	private String atype;
	private String aquantity;
	private String acost_per;

	private String atotal_cost;
	private String aproduct_id;
	// for Senior Citizen
	private String sctype;
	private String squantity;
	private String scost_per;
	private String stotal_cost;
	private String sproduct_id;
	// for college
	private String ctype;
	private String cquantity;
	private String ccost_per;
	private String ctotal_cost;
	private String cproduct_id;
	// for child
	private String chtype;
	private String chquantity;
	private String chcost_per;
	private String chtotal_cost;
	private String chproduct_id;

	// for Imagica Express Ticket
	private String iprodType;
	private String iticket_des;
	private String ititle;
	private String iimage_url;
	private String iatype;
	private String iaquantity;
	private String iacost_per;
	private String iatotal_cost;
	private String iaproduct_id;
	// for Senior Citizen
	private String isctype;
	private String isquantity;
	private String iscost_per;
	private String istotal_cost;
	private String isproduct_id;
	// for college
	private String ictype;
	private String icquantity;
	private String iccost_per;
	private String ictotal_cost;
	private String icproduct_id;
	// for child
	private String ichtype;
	private String ichquantity;
	private String ichcost_per;
	private String ichtotal_cost;
	private String ichproduct_id;
	// for Water Park
	// Wat-A-Wednesday (Regular) - Tier 1
	private String t1prodType;

	private String t1ticket_des;
	private String t1title;
	private String t1image_url;
	// for Adult
	private String t1atype;
	private String t1aquantity;
	private String t1acost_per;
	private String t1atotal_cost;
	private String t1aproduct_id;

	// Wat-A-Wednesday (Regular) -Tier 2
	private String t2prodType;
	private String t2ticket_des;
	private String t2title;
	private String t2image_url;
	// for Adult
	private String t2atype;
	private String t2aquantity;
	private String t2acost_per;
	private String t2atotal_cost;
	private String t2aproduct_id;
	// Wat-A-Wednesday (Express)
	private String eprodType;
	private String eticket_des;
	private String etitle;
	private String eimage_url;
	// for Adult
	private String eatype;
	private String eaquantity;
	private String eacost_per;
	private String eatotal_cost;
	private String eaproduct_id;
	// for total cost Theme park

	private float acost_per1;
	private float scost_per2;
	private float ccost_per3;
	private float chcost_per4;
	private float iacost_per1;
	private float iscost_per2;
	private float ichcost_per3;
	// for total cost Water park

	private float acost_per2;
	private float t1acost_per3;
	private float t2acost_per4;
	private float t3acost_per5;

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public ResponseStatus getStatus() {
		return status;
	}

	public void setStatus(ResponseStatus status) {
		this.status = status;
	}

	public Object getDetails() {
		return details;
	}

	public void setDetails(Object details) {
		this.details = details;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public Object getJsonArray() {
		return jsonArray;
	}

	public void setJsonArray(Object jsonArray) {
		this.jsonArray = jsonArray;
	}

	public String getProdType() {
		return prodType;
	}

	public void setProdType(String prodType) {
		this.prodType = prodType;
	}

	public String getTicket_des() {
		return ticket_des;
	}

	public void setTicket_des(String ticket_des) {
		this.ticket_des = ticket_des;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getImage_url() {
		return image_url;
	}

	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}

	public String getAtype() {
		return atype;
	}

	public void setAtype(String atype) {
		this.atype = atype;
	}

	public String getAquantity() {
		return aquantity;
	}

	public void setAquantity(String aquantity) {
		this.aquantity = aquantity;
	}

	public String getAcost_per() {
		return acost_per;
	}

	public void setAcost_per(String acost_per) {
		this.acost_per = acost_per;
	}

	public String getAtotal_cost() {
		return atotal_cost;
	}

	public void setAtotal_cost(String atotal_cost) {
		this.atotal_cost = atotal_cost;
	}

	public String getAproduct_id() {
		return aproduct_id;
	}

	public void setAproduct_id(String aproduct_id) {
		this.aproduct_id = aproduct_id;
	}

	public String getSctype() {
		return sctype;
	}

	public void setSctype(String sctype) {
		this.sctype = sctype;
	}

	public String getSquantity() {
		return squantity;
	}

	public void setSquantity(String squantity) {
		this.squantity = squantity;
	}

	public String getScost_per() {
		return scost_per;
	}

	public void setScost_per(String scost_per) {
		this.scost_per = scost_per;
	}

	public String getStotal_cost() {
		return stotal_cost;
	}

	public void setStotal_cost(String stotal_cost) {
		this.stotal_cost = stotal_cost;
	}

	public String getSproduct_id() {
		return sproduct_id;
	}

	public void setSproduct_id(String sproduct_id) {
		this.sproduct_id = sproduct_id;
	}

	public String getCtype() {
		return ctype;
	}

	public void setCtype(String ctype) {
		this.ctype = ctype;
	}

	public String getCquantity() {
		return cquantity;
	}

	public void setCquantity(String cquantity) {
		this.cquantity = cquantity;
	}

	public String getCcost_per() {
		return ccost_per;
	}

	public void setCcost_per(String ccost_per) {
		this.ccost_per = ccost_per;
	}

	public String getCtotal_cost() {
		return ctotal_cost;
	}

	public void setCtotal_cost(String ctotal_cost) {
		this.ctotal_cost = ctotal_cost;
	}

	public String getCproduct_id() {
		return cproduct_id;
	}

	public void setCproduct_id(String cproduct_id) {
		this.cproduct_id = cproduct_id;
	}

	public String getChtype() {
		return chtype;
	}

	public void setChtype(String chtype) {
		this.chtype = chtype;
	}

	public String getChquantity() {
		return chquantity;
	}

	public void setChquantity(String chquantity) {
		this.chquantity = chquantity;
	}

	public String getChcost_per() {
		return chcost_per;
	}

	public void setChcost_per(String chcost_per) {
		this.chcost_per = chcost_per;
	}

	public String getChtotal_cost() {
		return chtotal_cost;
	}

	public void setChtotal_cost(String chtotal_cost) {
		this.chtotal_cost = chtotal_cost;
	}

	public String getChproduct_id() {
		return chproduct_id;
	}

	public void setChproduct_id(String chproduct_id) {
		this.chproduct_id = chproduct_id;
	}

	public String getIprodType() {
		return iprodType;
	}

	public void setIprodType(String iprodType) {
		this.iprodType = iprodType;
	}

	public String getIticket_des() {
		return iticket_des;
	}

	public void setIticket_des(String iticket_des) {
		this.iticket_des = iticket_des;
	}

	public String getItitle() {
		return ititle;
	}

	public void setItitle(String ititle) {
		this.ititle = ititle;
	}

	public String getIimage_url() {
		return iimage_url;
	}

	public void setIimage_url(String iimage_url) {
		this.iimage_url = iimage_url;
	}

	public String getIatype() {
		return iatype;
	}

	public void setIatype(String iatype) {
		this.iatype = iatype;
	}

	public String getIaquantity() {
		return iaquantity;
	}

	public void setIaquantity(String iaquantity) {
		this.iaquantity = iaquantity;
	}

	public String getIacost_per() {
		return iacost_per;
	}

	public void setIacost_per(String iacost_per) {
		this.iacost_per = iacost_per;
	}

	public String getIatotal_cost() {
		return iatotal_cost;
	}

	public void setIatotal_cost(String iatotal_cost) {
		this.iatotal_cost = iatotal_cost;
	}

	public String getIaproduct_id() {
		return iaproduct_id;
	}

	public void setIaproduct_id(String iaproduct_id) {
		this.iaproduct_id = iaproduct_id;
	}

	public String getIsctype() {
		return isctype;
	}

	public void setIsctype(String isctype) {
		this.isctype = isctype;
	}

	public String getIsquantity() {
		return isquantity;
	}

	public void setIsquantity(String isquantity) {
		this.isquantity = isquantity;
	}

	public String getIscost_per() {
		return iscost_per;
	}

	public void setIscost_per(String iscost_per) {
		this.iscost_per = iscost_per;
	}

	public String getIstotal_cost() {
		return istotal_cost;
	}

	public void setIstotal_cost(String istotal_cost) {
		this.istotal_cost = istotal_cost;
	}

	public String getIsproduct_id() {
		return isproduct_id;
	}

	public void setIsproduct_id(String isproduct_id) {
		this.isproduct_id = isproduct_id;
	}

	public String getIctype() {
		return ictype;
	}

	public void setIctype(String ictype) {
		this.ictype = ictype;
	}

	public String getIcquantity() {
		return icquantity;
	}

	public void setIcquantity(String icquantity) {
		this.icquantity = icquantity;
	}

	public String getIccost_per() {
		return iccost_per;
	}

	public void setIccost_per(String iccost_per) {
		this.iccost_per = iccost_per;
	}

	public String getIctotal_cost() {
		return ictotal_cost;
	}

	public void setIctotal_cost(String ictotal_cost) {
		this.ictotal_cost = ictotal_cost;
	}

	public String getIcproduct_id() {
		return icproduct_id;
	}

	public void setIcproduct_id(String icproduct_id) {
		this.icproduct_id = icproduct_id;
	}

	public String getIchtype() {
		return ichtype;
	}

	public void setIchtype(String ichtype) {
		this.ichtype = ichtype;
	}

	public String getIchquantity() {
		return ichquantity;
	}

	public void setIchquantity(String ichquantity) {
		this.ichquantity = ichquantity;
	}

	public String getIchcost_per() {
		return ichcost_per;
	}

	public void setIchcost_per(String ichcost_per) {
		this.ichcost_per = ichcost_per;
	}

	public String getIchtotal_cost() {
		return ichtotal_cost;
	}

	public void setIchtotal_cost(String ichtotal_cost) {
		this.ichtotal_cost = ichtotal_cost;
	}

	public String getIchproduct_id() {
		return ichproduct_id;
	}

	public void setIchproduct_id(String ichproduct_id) {
		this.ichproduct_id = ichproduct_id;
	}

	public String getT1prodType() {
		return t1prodType;
	}

	public void setT1prodType(String t1prodType) {
		this.t1prodType = t1prodType;
	}

	public String getT1ticket_des() {
		return t1ticket_des;
	}

	public void setT1ticket_des(String t1ticket_des) {
		this.t1ticket_des = t1ticket_des;
	}

	public String getT1title() {
		return t1title;
	}

	public void setT1title(String t1title) {
		this.t1title = t1title;
	}

	public String getT1image_url() {
		return t1image_url;
	}

	public void setT1image_url(String t1image_url) {
		this.t1image_url = t1image_url;
	}

	public String getT1atype() {
		return t1atype;
	}

	public void setT1atype(String t1atype) {
		this.t1atype = t1atype;
	}

	public String getT1aquantity() {
		return t1aquantity;
	}

	public void setT1aquantity(String t1aquantity) {
		this.t1aquantity = t1aquantity;
	}

	public String getT1acost_per() {
		return t1acost_per;
	}

	public void setT1acost_per(String t1acost_per) {
		this.t1acost_per = t1acost_per;
	}

	public String getT1atotal_cost() {
		return t1atotal_cost;
	}

	public void setT1atotal_cost(String t1atotal_cost) {
		this.t1atotal_cost = t1atotal_cost;
	}

	public String getT1aproduct_id() {
		return t1aproduct_id;
	}

	public void setT1aproduct_id(String t1aproduct_id) {
		this.t1aproduct_id = t1aproduct_id;
	}

	public String getT2prodType() {
		return t2prodType;
	}

	public void setT2prodType(String t2prodType) {
		this.t2prodType = t2prodType;
	}

	public String getT2ticket_des() {
		return t2ticket_des;
	}

	public void setT2ticket_des(String t2ticket_des) {
		this.t2ticket_des = t2ticket_des;
	}

	public String getT2title() {
		return t2title;
	}

	public void setT2title(String t2title) {
		this.t2title = t2title;
	}

	public String getT2image_url() {
		return t2image_url;
	}

	public void setT2image_url(String t2image_url) {
		this.t2image_url = t2image_url;
	}

	public String getT2atype() {
		return t2atype;
	}

	public void setT2atype(String t2atype) {
		this.t2atype = t2atype;
	}

	public String getT2aquantity() {
		return t2aquantity;
	}

	public void setT2aquantity(String t2aquantity) {
		this.t2aquantity = t2aquantity;
	}

	public String getT2acost_per() {
		return t2acost_per;
	}

	public void setT2acost_per(String t2acost_per) {
		this.t2acost_per = t2acost_per;
	}

	public String getT2atotal_cost() {
		return t2atotal_cost;
	}

	public void setT2atotal_cost(String t2atotal_cost) {
		this.t2atotal_cost = t2atotal_cost;
	}

	public String getT2aproduct_id() {
		return t2aproduct_id;
	}

	public void setT2aproduct_id(String t2aproduct_id) {
		this.t2aproduct_id = t2aproduct_id;
	}

	public String getEprodType() {
		return eprodType;
	}

	public void setEprodType(String eprodType) {
		this.eprodType = eprodType;
	}

	public String getEticket_des() {
		return eticket_des;
	}

	public void setEticket_des(String eticket_des) {
		this.eticket_des = eticket_des;
	}

	public String getEtitle() {
		return etitle;
	}

	public void setEtitle(String etitle) {
		this.etitle = etitle;
	}

	public String getEimage_url() {
		return eimage_url;
	}

	public void setEimage_url(String eimage_url) {
		this.eimage_url = eimage_url;
	}

	public String getEatype() {
		return eatype;
	}

	public void setEatype(String eatype) {
		this.eatype = eatype;
	}

	public String getEaquantity() {
		return eaquantity;
	}

	public void setEaquantity(String eaquantity) {
		this.eaquantity = eaquantity;
	}

	public String getEacost_per() {
		return eacost_per;
	}

	public void setEacost_per(String eacost_per) {
		this.eacost_per = eacost_per;
	}

	public String getEatotal_cost() {
		return eatotal_cost;
	}

	public void setEatotal_cost(String eatotal_cost) {
		this.eatotal_cost = eatotal_cost;
	}

	public String getEaproduct_id() {
		return eaproduct_id;
	}

	public void setEaproduct_id(String eaproduct_id) {
		this.eaproduct_id = eaproduct_id;
	}

	public float getAcost_per1() {
		return acost_per1;
	}

	public void setAcost_per1(float acost_per1) {
		this.acost_per1 = acost_per1;
	}

	public float getScost_per2() {
		return scost_per2;
	}

	public void setScost_per2(float scost_per2) {
		this.scost_per2 = scost_per2;
	}

	public float getCcost_per3() {
		return ccost_per3;
	}

	public void setCcost_per3(float ccost_per3) {
		this.ccost_per3 = ccost_per3;
	}

	public float getChcost_per4() {
		return chcost_per4;
	}

	public void setChcost_per4(float chcost_per4) {
		this.chcost_per4 = chcost_per4;
	}

	public float getIacost_per1() {
		return iacost_per1;
	}

	public void setIacost_per1(float iacost_per1) {
		this.iacost_per1 = iacost_per1;
	}

	public float getIscost_per2() {
		return iscost_per2;
	}

	public void setIscost_per2(float iscost_per2) {
		this.iscost_per2 = iscost_per2;
	}

	public float getIchcost_per3() {
		return ichcost_per3;
	}

	public void setIchcost_per3(float ichcost_per3) {
		this.ichcost_per3 = ichcost_per3;
	}

	public float getAcost_per2() {
		return acost_per2;
	}

	public void setAcost_per2(float acost_per2) {
		this.acost_per2 = acost_per2;
	}

	public float getT1acost_per3() {
		return t1acost_per3;
	}

	public void setT1acost_per3(float t1acost_per3) {
		this.t1acost_per3 = t1acost_per3;
	}

	public float getT2acost_per4() {
		return t2acost_per4;
	}

	public void setT2acost_per4(float t2acost_per4) {
		this.t2acost_per4 = t2acost_per4;
	}

	public float getT3acost_per5() {
		return t3acost_per5;
	}

	public void setT3acost_per5(float t3acost_per5) {
		this.t3acost_per5 = t3acost_per5;
	}

}
