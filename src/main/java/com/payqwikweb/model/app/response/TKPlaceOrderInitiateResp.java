package com.payqwikweb.model.app.response;

public class TKPlaceOrderInitiateResp {
	private String status;
	private String code;
	private String message;
	private String balance;
	private String details;
	private String txnId;
	private boolean success;
	private String Response;
	
	private long userOrderId;
	
	public long getUserOrderId() {
		return userOrderId;
	}
	public void setUserOrderId(long userOrderId) {
		this.userOrderId = userOrderId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getBalance() {
		return balance;
	}
	public void setBalance(String balance) {
		this.balance = balance;
	}
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}
	public String getTxnId() {
		return txnId;
	}
	public void setTxnId(String txnId) {
		this.txnId = txnId;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getResponse() {
		return Response;
	}
	public void setResponse(String response) {
		Response = response;
	}
	
	
}
