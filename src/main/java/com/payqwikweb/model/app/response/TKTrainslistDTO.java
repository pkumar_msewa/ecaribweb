package com.payqwikweb.model.app.response;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.payqwikweb.app.model.ResponseStatus;
import com.payqwikweb.model.web.Status;

// client response class
public class TKTrainslistDTO {
     
	private boolean success;
	private String code;
	private String message;
	private Status status;
	
	private ArrayList<TrainDTO>listOfTrain=new ArrayList<TrainDTO>();
	private ArrayList<StationDTO>stations;
	
	
	public ArrayList<StationDTO> getStations() {
		return stations;
	}
	public void setStations(ArrayList<StationDTO> stations) {
		this.stations = stations;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	public ArrayList<TrainDTO> getListOfTrain() {
		return listOfTrain;
	}
	public void setListOfTrain(ArrayList<TrainDTO> listOfTrain) {
		this.listOfTrain = listOfTrain;
	}

}
