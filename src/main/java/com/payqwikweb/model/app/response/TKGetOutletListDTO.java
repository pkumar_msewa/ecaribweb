package com.payqwikweb.model.app.response;

import java.util.ArrayList;

public class TKGetOutletListDTO {
	
//	private ArrayList<TravelkhanaVendorDTO>vendorDto = new ArrayList<TravelkhanaVendorDTO>();
	private ArrayList<TKGetMenuListDTO>categoriesMenu  = new ArrayList<TKGetMenuListDTO>();
	private long outletId;
	

	public long getOutletId() {
		return outletId;
	}
	public void setOutletId(long outletId) {
		this.outletId = outletId;
	}
	public ArrayList<TKGetMenuListDTO> getCategoriesMenu() {
		return categoriesMenu;
	}
	public void setCategoriesMenu(ArrayList<TKGetMenuListDTO> categoriesMenu) {
		this.categoriesMenu = categoriesMenu;
	}
	
	
	
}
