package com.payqwikweb.model.app.response;

import com.payqwikweb.model.web.Status;

public class TKMenuPriceCalDTO {
	private boolean success;
	private String code;
	private String message;
	private Status status;
	private MenuPriceCalDTO menuPriceCalDTO = new MenuPriceCalDTO();
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	public MenuPriceCalDTO getMenuPriceCalDTO() {
		return menuPriceCalDTO;
	}
	public void setMenuPriceCalDTO(MenuPriceCalDTO menuPriceCalDTO) {
		this.menuPriceCalDTO = menuPriceCalDTO;
	}
	
	
}
