package com.payqwikweb.model.app.response;

import com.payqwikweb.model.web.Status;

public class BasicDTO {

    private String status;
    private String code;
    private String message;
    private String contactNo;
    private String email;
    private Status emailStatus;
    private Status mobileStatus;
    private String address;
    private String name;
    private String dateOfBirth;
    private String gender;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Status getEmailStatus() {
        return emailStatus;
    }

    public void setEmailStatus(Status emailStatus) {
        this.emailStatus = emailStatus;
    }

    public Status getMobileStatus() {
        return mobileStatus;
    }

    public void setMobileStatus(Status mobileStatus) {
        this.mobileStatus = mobileStatus;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
