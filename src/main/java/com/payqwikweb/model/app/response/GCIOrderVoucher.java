package com.payqwikweb.model.app.response;

public class GCIOrderVoucher {

	private String brandName;
	private String	denomination;
	private String voucherNumber;
	private String voucherPin;
	private String expiryDate;
	private String sessioniId;
	private String amount;
	
	private String receiptno;
	private String transactionRefNo;
	private String code;
	public String getReceiptno() {
		return receiptno;
	}
	public void setReceiptno(String receiptno) {
		this.receiptno = receiptno;
	}
	public String getTransactionRefNo() {
		return transactionRefNo;
	}
	public void setTransactionRefNo(String transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getSessioniId() {
		return sessioniId;
	}
	public void setSessioniId(String sessioniId) {
		this.sessioniId = sessioniId;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	public String getDenomination() {
		return denomination;
	}
	public void setDenomination(String denomination) {
		this.denomination = denomination;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public String getVoucherPin() {
		return voucherPin;
	}
	public void setVoucherPin(String voucherPin) {
		this.voucherPin = voucherPin;
	}
	public String getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	
	

}
