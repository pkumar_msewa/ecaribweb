package com.payqwikweb.model.app.response;

import java.util.ArrayList;

import com.payqwikweb.model.web.Status;

public class TKTrackUserOrderDTO {
	private boolean success;
	private String code;
	private String message;
	private Status status;
	ArrayList<TKUserOrderDTO>tKUserOrderDTO;
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	public ArrayList<TKUserOrderDTO> gettKUserOrderDTO() {
		return tKUserOrderDTO;
	}
	public void settKUserOrderDTO(ArrayList<TKUserOrderDTO> tKUserOrderDTO) {
		this.tKUserOrderDTO = tKUserOrderDTO;
	}
	
	
}
