package com.payqwikweb.model.app.response;

import java.util.ArrayList;

import com.payqwikweb.model.web.Status;

public class TKMenuListDTO {
	private boolean success;
	private String code;
	private String message;
	private Status status;
	private ArrayList<TKGetMenuListDTO>listOfMenuobject=new ArrayList<TKGetMenuListDTO>();

	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	public ArrayList<TKGetMenuListDTO> getListOfMenuobject() {
		return listOfMenuobject;
	}
	public void setListOfMenuobject(ArrayList<TKGetMenuListDTO> listOfMenuobject) {
		this.listOfMenuobject = listOfMenuobject;
	}
	
	
}
