package com.payqwikweb.model.app.response;

public class TKOrderDetailsDTO {
	private String orderDate;
	private String order_outlet_id;
	private String mail_id;
	private String txnDate;
	private String train_number;
	private String orderStatus;
	private String station_code;
	private String transactionRefNo;
	private String userOrderId;
	private String seat;
	private String coach;
	private String contact_no;
	private String eta;
	private String pnr;
	private String name;
	private double totalCustomerPayable;
	private String txnStatus;
	private String creationTime;
//	private String contact_no;
	private double commsissionAmount;
	private UserDetail userDetail;
	
	
	public String getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(String dateOfAccountCreation) {
		this.creationTime = dateOfAccountCreation;
	}

	public String getTxnStatus() {
		return txnStatus;
	}

	public void setTxnStatus(String txnStatus) {
		this.txnStatus = txnStatus;
	}

	public double getTotalCustomerPayable() {
		return totalCustomerPayable;
	}

	public void setTotalCustomerPayable(double totalCustomerPayable) {
		this.totalCustomerPayable = totalCustomerPayable;
	}

	public String getTxnDate() {
		return txnDate;
	}

	public void setTxnDate(String bookingDate) {
		this.txnDate = bookingDate;
	}

	public UserDetail getUserDetail() {
		return userDetail;
	}

	public void setUserDetail(UserDetail userDetail) {
		this.userDetail = userDetail;
	}

	public double getCommsissionAmount() {
		return commsissionAmount;
	}

	public void setCommsissionAmount(double commsissionAmount) {
		this.commsissionAmount = commsissionAmount;
	}
	
	public String getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(String date) {
		this.orderDate = date;
	}
	public String getOrder_outlet_id() {
		return order_outlet_id;
	}
	public void setOrder_outlet_id(String order_outlet_id) {
		this.order_outlet_id = order_outlet_id;
	}
	public String getMail_id() {
		return mail_id;
	}
	public void setMail_id(String mail_id) {
		this.mail_id = mail_id;
	}
	public String getTrain_number() {
		return train_number;
	}
	public void setTrain_number(String train_number) {
		this.train_number = train_number;
	}
	public String getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	public String getStation_code() {
		return station_code;
	}
	public void setStation_code(String station_code) {
		this.station_code = station_code;
	}
	public String getTransactionRefNo() {
		return transactionRefNo;
	}
	public void setTransactionRefNo(String transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}
	public String getUserOrderId() {
		return userOrderId;
	}
	public void setUserOrderId(String userOrderId) {
		this.userOrderId = userOrderId;
	}
	public String getSeat() {
		return seat;
	}
	public void setSeat(String seat) {
		this.seat = seat;
	}
	public String getCoach() {
		return coach;
	}
	public void setCoach(String coach) {
		this.coach = coach;
	}
	public String getContact_no() {
		return contact_no;
	}
	public void setContact_no(String contact_no) {
		this.contact_no = contact_no;
	}
	public String getEta() {
		return eta;
	}
	public void setEta(String eta) {
		this.eta = eta;
	}
	public String getPnr() {
		return pnr;
	}
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
}