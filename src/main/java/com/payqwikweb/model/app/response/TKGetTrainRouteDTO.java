package com.payqwikweb.model.app.response;

import java.util.ArrayList;

public class TKGetTrainRouteDTO {
	
	private ArrayList<StationsDTO> stationsDTO;
	private ArrayList<TKGetMenuListDTO>categoriesMenu;
	private long trainNumber;
	private String trainName;
	private String date;
	private boolean availability;
	private long outletId;
	
	public long getOutletId() {
		return outletId;
	}
	public void setOutletId(long outletId) {
		this.outletId = outletId;
	}
	public ArrayList<StationsDTO> getStationsDTO() {
		return stationsDTO;
	}
	public void setStationsDTO(ArrayList<StationsDTO> stationsDTO) {
		this.stationsDTO = stationsDTO;
	}
	public ArrayList<TKGetMenuListDTO> getCategoriesMenu() {
		return categoriesMenu;
	}
	public void setCategoriesMenu(ArrayList<TKGetMenuListDTO> categoriesMenu) {
		this.categoriesMenu = categoriesMenu;
	}
	public long getTrainNumber() {
		return trainNumber;
	}
	public void setTrainNumber(long trainNumber) {
		this.trainNumber = trainNumber;
	}
	public String getTrainName() {
		return trainName;
	}
	public void setTrainName(String trainName) {
		this.trainName = trainName;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public boolean isAvailability() {
		return availability;
	}
	public void setAvailability(boolean availability) {
		this.availability = availability;
	}
	
	
}
