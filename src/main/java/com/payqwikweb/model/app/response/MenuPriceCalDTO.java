package com.payqwikweb.model.app.response;

import java.util.ArrayList;

import com.payqwikweb.model.app.request.UserOrderInfoDTO;
import com.payqwikweb.model.app.request.UserOrderMenuDTO;

public class MenuPriceCalDTO {
	
	private double minAmountThreshold;
	private double outletminorderAmount;
	public UserOrderInfoDTO  userOrderInfo;
	private ArrayList<UserOrderMenuDTO>userOrderMenu;
	
	public double getMinAmountThreshold() {
		return minAmountThreshold;
	}
	public void setMinAmountThreshold(double minAmountThreshold) {
		this.minAmountThreshold = minAmountThreshold;
	}
	public double getOutletminorderAmount() {
		return outletminorderAmount;
	}
	public void setOutletminorderAmount(double outletminorderAmount) {
		this.outletminorderAmount = outletminorderAmount;
	}

	public UserOrderInfoDTO getUserOrderInfo() {
		return userOrderInfo;
	}
	public void setUserOrderInfo(UserOrderInfoDTO userOrderInfo) {
		this.userOrderInfo = userOrderInfo;
	}
	public ArrayList<UserOrderMenuDTO> getUserOrderMenu() {
		return userOrderMenu;
	}
	public void setUserOrderMenu(ArrayList<UserOrderMenuDTO> userOrderMenu) {
		this.userOrderMenu = userOrderMenu;
	}
}