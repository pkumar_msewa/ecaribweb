package com.payqwikweb.model.app.response;

import java.util.ArrayList;

import com.payqwikweb.model.web.Status;

public class TkApplyCouponDTO {
	private boolean success;
	private String code;
	private String message;
	private Status status;
	private String discountVoucherInfo;
	
	private ArrayList<TKCartDetailsDTO>cartDetails;
	private TKPassengerInfoDTO passengerInfo;
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	public ArrayList<TKCartDetailsDTO> getCartDetails() {
		return cartDetails;
	}
	public void setCartDetails(ArrayList<TKCartDetailsDTO> cartDetails) {
		this.cartDetails = cartDetails;
	}
	public TKPassengerInfoDTO getPassengerInfo() {
		return passengerInfo;
	}
	public void setPassengerInfo(TKPassengerInfoDTO passengerInfo) {
		this.passengerInfo = passengerInfo;
	}	
}