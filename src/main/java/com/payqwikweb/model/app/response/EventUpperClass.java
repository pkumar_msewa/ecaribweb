package com.payqwikweb.model.app.response;

public class EventUpperClass {

	private String occurrence_id;
	private String date;
	private String start_time;
	private String end_time;
	private String end_date;

	public String getEnd_date() {
		return end_date;
	}

	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getStart_time() {
		return start_time;
	}

	public void setStart_time(String start_time) {
		this.start_time = start_time;
	}

	public String getEnd_time() {
		return end_time;
	}

	public void setEnd_time(String end_time) {
		this.end_time = end_time;
	}

	public String getOccurrence_id() {
		return occurrence_id;
	}

	public void setOccurrence_id(String occurrence_id) {
		this.occurrence_id = occurrence_id;
	}

}
