package com.payqwikweb.model.app.response;

public class GCIBrandDenomination {
	
	private String id;
	private String name;
	private String skuId;
	private String type;
	private String valueType;
	private String image;
	private String quantity;
	private String sessioniId;
	private String brandname;
	private String hash;
	
	
	
	
	
	public String getHash() {
		return hash;
	}
	public void setHash(String hash) {
		this.hash = hash;
	}
	public void setBrandname(String brandname) {
		this.brandname = brandname;
	}
	public String getBrandname() {
		return brandname;
	}
	public void setBrandnane(String brandname) {
		this.brandname = brandname;
	}
	public String getSessioniId() {
		return sessioniId;
	}
	public void setSessioniId(String sessioniId) {
		this.sessioniId = sessioniId;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSkuId() {
		return skuId;
	}
	public void setSkuId(String skuId) {
		this.skuId = skuId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getValueType() {
		return valueType;
	}
	public void setValueType(String valueType) {
		this.valueType = valueType;
	}
	

}
