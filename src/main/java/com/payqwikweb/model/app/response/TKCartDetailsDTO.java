package com.payqwikweb.model.app.response;

public class TKCartDetailsDTO {
	
	 private String couponCode;
	 private double totalSP;
	 private double minAmountThreshold;
	 private long outletId;
	 private double discount;
	 private double totalCP;
	 private int sequenceNumber;
	 private String orderDate;
	 private long trainNumber;
	 private String discountMessage;
	 private boolean discountFlag;
	 private double deliveryCost;
	 private double serviceTax;
	 private double totalPayableAmount;
	 private double cappingAmount;
	 private double cashbackAmount;
	 private boolean cashbackFlag;
	 private String menuItems;
	public String getCouponCode() {
		return couponCode;
	}
	public void setCouponCode(String couponCode) {
		this.couponCode = couponCode;
	}
	public double getTotalSP() {
		return totalSP;
	}
	public void setTotalSP(double totalSP) {
		this.totalSP = totalSP;
	}
	public double getMinAmountThreshold() {
		return minAmountThreshold;
	}
	public void setMinAmountThreshold(double minAmountThreshold) {
		this.minAmountThreshold = minAmountThreshold;
	}
	public long getOutletId() {
		return outletId;
	}
	public void setOutletId(long outletId) {
		this.outletId = outletId;
	}
	public double getDiscount() {
		return discount;
	}
	public void setDiscount(double discount) {
		this.discount = discount;
	}
	public double getTotalCP() {
		return totalCP;
	}
	public void setTotalCP(double totalCP) {
		this.totalCP = totalCP;
	}
	public int getSequenceNumber() {
		return sequenceNumber;
	}
	public void setSequenceNumber(int sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	public String getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}
	public long getTrainNumber() {
		return trainNumber;
	}
	public void setTrainNumber(long trainNumber) {
		this.trainNumber = trainNumber;
	}
	public String getDiscountMessage() {
		return discountMessage;
	}
	public void setDiscountMessage(String discountMessage) {
		this.discountMessage = discountMessage;
	}
	public boolean isDiscountFlag() {
		return discountFlag;
	}
	public void setDiscountFlag(boolean discountFlag) {
		this.discountFlag = discountFlag;
	}
	public double getDeliveryCost() {
		return deliveryCost;
	}
	public void setDeliveryCost(double deliveryCost) {
		this.deliveryCost = deliveryCost;
	}
	public double getServiceTax() {
		return serviceTax;
	}
	public void setServiceTax(double serviceTax) {
		this.serviceTax = serviceTax;
	}
	public double getTotalPayableAmount() {
		return totalPayableAmount;
	}
	public void setTotalPayableAmount(double totalPayableAmount) {
		this.totalPayableAmount = totalPayableAmount;
	}
	public double getCappingAmount() {
		return cappingAmount;
	}
	public void setCappingAmount(double cappingAmount) {
		this.cappingAmount = cappingAmount;
	}
	public double getCashbackAmount() {
		return cashbackAmount;
	}
	public void setCashbackAmount(double cashbackAmount) {
		this.cashbackAmount = cashbackAmount;
	}
	public boolean isCashbackFlag() {
		return cashbackFlag;
	}
	public void setCashbackFlag(boolean cashbackFlag) {
		this.cashbackFlag = cashbackFlag;
	}
	public String getMenuItems() {
		return menuItems;
	}
	public void setMenuItems(String string) {
		this.menuItems = string;
	}
	 
	 
}