package com.payqwikweb.model.app.response;

public class OutletTypeDTO {
	
	private String tagType;
	private String tagSubType;
	private String tagName;
	private long tagId;
	
	public String getTagType() {
		return tagType;
	}
	public void setTagType(String tagType) {
		this.tagType = tagType;
	}
	public String getTagSubType() {
		return tagSubType;
	}
	public void setTagSubType(String tagSubType) {
		this.tagSubType = tagSubType;
	}
	public String getTagName() {
		return tagName;
	}
	public void setTagName(String tagName) {
		this.tagName = tagName;
	}
	public long getTagId() {
		return tagId;
	}
	public void setTagId(long tagId) {
		this.tagId = tagId;
	}
	
}