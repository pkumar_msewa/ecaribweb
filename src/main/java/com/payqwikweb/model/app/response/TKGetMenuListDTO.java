package com.payqwikweb.model.app.response;

import java.util.ArrayList;

import com.payqwikweb.app.model.ResponseStatus;

public class TKGetMenuListDTO {
	
    private long categoryId;
    private String categoryName;
 	private ArrayList<MenuDTO>listOfMenuDTOobject=new ArrayList<MenuDTO>();
 	
	public long getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public ArrayList<MenuDTO> getListOfMenuDTOobject() {
		return listOfMenuDTOobject;
	}
	public void setListOfMenuDTOobject(ArrayList<MenuDTO> listOfMenuDTOobject) {
		this.listOfMenuDTOobject = listOfMenuDTOobject;
	}
	
	
	
}
