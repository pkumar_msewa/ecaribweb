package com.payqwikweb.model.app.response;

public class AdlabsOrderVoucher {

	private String success_profile;
	private String success_payment;
	private String sessioniId;
	private String amount;

	private String transactionRefNo;
	private String code;

	public String getSuccess_profile() {
		return success_profile;
	}

	public void setSuccess_profile(String success_profile) {
		this.success_profile = success_profile;
	}

	public String getSuccess_payment() {
		return success_payment;
	}

	public void setSuccess_payment(String success_payment) {
		this.success_payment = success_payment;
	}

	public String getSessioniId() {
		return sessioniId;
	}

	public void setSessioniId(String sessioniId) {
		this.sessioniId = sessioniId;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getTransactionRefNo() {
		return transactionRefNo;
	}

	public void setTransactionRefNo(String transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

}
