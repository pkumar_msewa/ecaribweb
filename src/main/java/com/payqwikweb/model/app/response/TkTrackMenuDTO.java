package com.payqwikweb.model.app.response;

public class TkTrackMenuDTO {
	
   private String itemName;
   private long  quantity;
   private double rate;
   
public String getItemName() {
	return itemName;
}
public void setItemName(String itemName) {
	this.itemName = itemName;
}
public long getQuantity() {
	return quantity;
}
public void setQuantity(long quantity) {
	this.quantity = quantity;
}
public double getRate() {
	return rate;
}
public void setRate(double rate) {
	this.rate = rate;
}
   
   
}
