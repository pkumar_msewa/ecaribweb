package com.payqwikweb.model.app.response;

import java.util.ArrayList;

public class TravelkhanaVendorDTO {
	private int orderTiming;
	private ArrayList<OutletTypeDTO> outletType = new ArrayList<OutletTypeDTO>();
	private String Station;
	private String description;
	private boolean onlineAllow;
	private String closingTime;
	private int outletId;
	private boolean bulkRecommended;
	private int searchCount;
	private boolean showMenu;
	private String outletName;
	private int minOrderAmount;
	private String standardDelRating;
	private boolean foodAvailablity;
	private String openingTime;
	private boolean isIrctc;
	private int deliveryCost;
	
	public int getOrderTiming() {
		return orderTiming;
	}
	public void setOrderTiming(int orderTiming) {
		this.orderTiming = orderTiming;
	}
	public ArrayList<OutletTypeDTO> getOutletType() {
		return outletType;
	}
	public void setOutletType(ArrayList<OutletTypeDTO> outletType) {
		this.outletType = outletType;
	}
	public String getStation() {
		return Station;
	}
	public void setStation(String station) {
		Station = station;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public boolean isOnlineAllow() {
		return onlineAllow;
	}
	public void setOnlineAllow(boolean onlineAllow) {
		this.onlineAllow = onlineAllow;
	}
	public String getClosingTime() {
		return closingTime;
	}
	public void setClosingTime(String closingTime) {
		this.closingTime = closingTime;
	}
	public int getOutletId() {
		return outletId;
	}
	public void setOutletId(int outletId) {
		this.outletId = outletId;
	}
	public boolean isShowMenu() {
		return showMenu;
	}
	public void setShowMenu(boolean showMenu) {
		this.showMenu = showMenu;
	}
	public String getOutletName() {
		return outletName;
	}
	public void setOutletName(String outletName) {
		this.outletName = outletName;
	}
	public int getMinOrderAmount() {
		return minOrderAmount;
	}
	public void setMinOrderAmount(int minOrderAmount) {
		this.minOrderAmount = minOrderAmount;
	}
	public boolean isFoodAvailablity() {
		return foodAvailablity;
	}
	public void setFoodAvailablity(boolean foodAvailablity) {
		this.foodAvailablity = foodAvailablity;
	}
	public String getOpeningTime() {
		return openingTime;
	}
	public void setOpeningTime(String openingTime) {
		this.openingTime = openingTime;
	}
	public boolean isIrctc() {
		return isIrctc;
	}
	public void setIrctc(boolean isIrctc) {
		this.isIrctc = isIrctc;
	}
	public int getDeliveryCost() {
		return deliveryCost;
	}
	public void setDeliveryCost(int deliveryCost) {
		this.deliveryCost = deliveryCost;
	}
	public boolean isBulkRecommended() {
		return bulkRecommended;
	}
	public void setBulkRecommended(boolean bulkRecommended) {
		this.bulkRecommended = bulkRecommended;
	}
	public int getSearchCount() {
		return searchCount;
	}
	public void setSearchCount(int searchCount) {
		this.searchCount = searchCount;
	}
	public String getStandardDelRating() {
		return standardDelRating;
	}
	public void setStandardDelRating(String standardDelRating) {
		this.standardDelRating = standardDelRating;
	}
	
}