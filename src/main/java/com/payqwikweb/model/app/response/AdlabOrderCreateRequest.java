package com.payqwikweb.model.app.response;

public class AdlabOrderCreateRequest {

	private String product_id;
	private String quantity;
	private String total_day;
	private String field_visit_date;
	private String field_departure_date;
	private String field_evt_res_business_unit;
	private String commerce_product;
	private String accessToken;
	private String amount;
	private String sessionId;
	private String token;
	private String sessid;
	
	

	public String getSessid() {
		return sessid;
	}

	public void setSessid(String sessid) {
		this.sessid = sessid;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getSession_name() {
		return session_name;
	}

	public void setSession_name(String session_name) {
		this.session_name = session_name;
	}

	private String session_name;

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getTotal_day() {
		return total_day;
	}

	public void setTotal_day(String total_day) {
		this.total_day = total_day;
	}

	public String getField_visit_date() {
		return field_visit_date;
	}

	public void setField_visit_date(String field_visit_date) {
		this.field_visit_date = field_visit_date;
	}

	public String getField_departure_date() {
		return field_departure_date;
	}

	public void setField_departure_date(String field_departure_date) {
		this.field_departure_date = field_departure_date;
	}

	public String getField_evt_res_business_unit() {
		return field_evt_res_business_unit;
	}

	public void setField_evt_res_business_unit(String field_evt_res_business_unit) {
		this.field_evt_res_business_unit = field_evt_res_business_unit;
	}

	public String getCommerce_product() {
		return commerce_product;
	}

	public void setCommerce_product(String commerce_product) {
		this.commerce_product = commerce_product;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getProduct_id() {
		return product_id;
	}

	public void setProduct_id(String product_id) {
		this.product_id = product_id;
	}

}
