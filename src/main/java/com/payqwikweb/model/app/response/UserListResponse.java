package com.payqwikweb.model.app.response;

import com.payqwikweb.app.model.UserListDTO;

import java.util.List;

public class UserListResponse {

    private boolean success;
    private String message;
    private List<UserListDTO> userList;


    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<UserListDTO> getUserList() {
        return userList;
    }

    public void setUserList(List<UserListDTO> userList) {
        this.userList = userList;
    }
}
