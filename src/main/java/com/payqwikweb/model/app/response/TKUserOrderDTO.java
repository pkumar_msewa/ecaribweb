package com.payqwikweb.model.app.response;

import java.util.ArrayList;

public class TKUserOrderDTO {
	
   private long userOrderId;
   private String name;
   private long contactNo;
   private String coach;
   private String seat;
   private long pnr;
   private String customerComment;
   private double totalAmount;
//   private String bookedDateTime;
   private double advanceAmount;
   private double discount;
   private String delivery_cost;
 //  private String isBulk;
   private long trainNo;
   private String stationName;
   private long orderOutletId;
   private String date;
   private String trainName;
   private String outletName;
   private String stationCode;
   private String sta;
   private String eta;
   private String mailId;
   private String ata;
   private String voucherCode;
   private String status;
   private double payable_amount;
   
   private long orderDateDiff;
   private double walletDiscount;
   private String uId;
 
   private ArrayList<TkTrackMenuDTO> tkTrackMenuDTO;

public long getUserOrderId() {
	return userOrderId;
}

public void setUserOrderId(long userOrderId) {
	this.userOrderId = userOrderId;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public long getContactNo() {
	return contactNo;
}

public void setContactNo(long contactNo) {
	this.contactNo = contactNo;
}

public String getCoach() {
	return coach;
}

public void setCoach(String coach) {
	this.coach = coach;
}

public String getSeat() {
	return seat;
}

public void setSeat(String seat) {
	this.seat = seat;
}

public long getPnr() {
	return pnr;
}

public void setPnr(long pnr) {
	this.pnr = pnr;
}

public String getCustomerComment() {
	return customerComment;
}

public void setCustomerComment(String customerComment) {
	this.customerComment = customerComment;
}

public double getTotalAmount() {
	return totalAmount;
}

public void setTotalAmount(double totalAmount) {
	this.totalAmount = totalAmount;
}

public double getAdvanceAmount() {
	return advanceAmount;
}

public void setAdvanceAmount(double advanceAmount) {
	this.advanceAmount = advanceAmount;
}

public double getDiscount() {
	return discount;
}

public void setDiscount(double discount) {
	this.discount = discount;
}

public String getDelivery_cost() {
	return delivery_cost;
}

public void setDelivery_cost(String delivery_cost) {
	this.delivery_cost = delivery_cost;
}

public long getTrainNo() {
	return trainNo;
}

public void setTrainNo(long trainNo) {
	this.trainNo = trainNo;
}

public String getStationName() {
	return stationName;
}

public void setStationName(String stationName) {
	this.stationName = stationName;
}

public long getOrderOutletId() {
	return orderOutletId;
}

public void setOrderOutletId(long orderOutletId) {
	this.orderOutletId = orderOutletId;
}

public String getDate() {
	return date;
}

public void setDate(String date) {
	this.date = date;
}

public String getTrainName() {
	return trainName;
}

public void setTrainName(String trainName) {
	this.trainName = trainName;
}

public String getOutletName() {
	return outletName;
}

public void setOutletName(String outletName) {
	this.outletName = outletName;
}

public String getStationCode() {
	return stationCode;
}

public void setStationCode(String stationCode) {
	this.stationCode = stationCode;
}

public String getSta() {
	return sta;
}

public void setSta(String sta) {
	this.sta = sta;
}

public String getEta() {
	return eta;
}

public void setEta(String eta) {
	this.eta = eta;
}

public String getMailId() {
	return mailId;
}

public void setMailId(String mailId) {
	this.mailId = mailId;
}

public String getAta() {
	return ata;
}

public void setAta(String ata) {
	this.ata = ata;
}

public String getVoucherCode() {
	return voucherCode;
}

public void setVoucherCode(String voucherCode) {
	this.voucherCode = voucherCode;
}

public String getStatus() {
	return status;
}

public void setStatus(String status) {
	this.status = status;
}

public double getPayable_amount() {
	return payable_amount;
}

public void setPayable_amount(double payable_amount) {
	this.payable_amount = payable_amount;
}


public long getOrderDateDiff() {
	return orderDateDiff;
}

public void setOrderDateDiff(long orderDateDiff) {
	this.orderDateDiff = orderDateDiff;
}

public double getWalletDiscount() {
	return walletDiscount;
}

public void setWalletDiscount(double walletDiscount) {
	this.walletDiscount = walletDiscount;
}

public String getuId() {
	return uId;
}

public void setuId(String uId) {
	this.uId = uId;
}

public ArrayList<TkTrackMenuDTO> getTkTrackMenuDTO() {
	return tkTrackMenuDTO;
}

public void setTkTrackMenuDTO(ArrayList<TkTrackMenuDTO> tkTrackMenuDTO) {
	this.tkTrackMenuDTO = tkTrackMenuDTO;
}
  
   
}