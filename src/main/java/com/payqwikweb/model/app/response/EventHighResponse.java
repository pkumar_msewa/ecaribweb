package com.payqwikweb.model.app.response;

import java.util.ArrayList;

import org.codehaus.jackson.map.annotate.JsonSerialize;

public class EventHighResponse {

	private boolean success;
	private String code;
	private String message;
	private String status;
	private Object details;
	private String response;
	private String id;
	private String city;
	private String url;
	private String title;
	private String description;
	private String img_url;
	private String date;
	private String start_time;
	private String end_time;
	private String price;
	private String name;
	private String eid;
	private ArrayList<EventUpperClass> eventClass;
	private ArrayList<EventPrice> eventPrice;
	private String currency;
	private String value;
	private String validity_start;
	private String validity_end;
	private String time;
	private String booking_url;
	private String booking_enquiry_url;
	private String occurrence_id;
	private String end_date;
	private String address;
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Object getDetails() {
		return details;
	}
	public void setDetails(Object details) {
		this.details = details;
	}
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getImg_url() {
		return img_url;
	}
	public void setImg_url(String img_url) {
		this.img_url = img_url;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getStart_time() {
		return start_time;
	}
	public void setStart_time(String start_time) {
		this.start_time = start_time;
	}
	public String getEnd_time() {
		return end_time;
	}
	public void setEnd_time(String end_time) {
		this.end_time = end_time;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEid() {
		return eid;
	}
	public void setEid(String eid) {
		this.eid = eid;
	}
	public ArrayList<EventUpperClass> getEventClass() {
		return eventClass;
	}
	public void setEventClass(ArrayList<EventUpperClass> eventClass) {
		this.eventClass = eventClass;
	}
	public ArrayList<EventPrice> getEventPrice() {
		return eventPrice;
	}
	public void setEventPrice(ArrayList<EventPrice> eventPrice) {
		this.eventPrice = eventPrice;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getValidity_start() {
		return validity_start;
	}
	public void setValidity_start(String validity_start) {
		this.validity_start = validity_start;
	}
	public String getValidity_end() {
		return validity_end;
	}
	public void setValidity_end(String validity_end) {
		this.validity_end = validity_end;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getBooking_url() {
		return booking_url;
	}
	public void setBooking_url(String booking_url) {
		this.booking_url = booking_url;
	}
	public String getBooking_enquiry_url() {
		return booking_enquiry_url;
	}
	public void setBooking_enquiry_url(String booking_enquiry_url) {
		this.booking_enquiry_url = booking_enquiry_url;
	}
	public String getOccurrence_id() {
		return occurrence_id;
	}
	public void setOccurrence_id(String occurrence_id) {
		this.occurrence_id = occurrence_id;
	}
	public String getEnd_date() {
		return end_date;
	}
	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}

	
}
