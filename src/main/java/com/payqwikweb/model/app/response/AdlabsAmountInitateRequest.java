package com.payqwikweb.model.app.response;

public class AdlabsAmountInitateRequest {

	private String sessioniId;
	private String amount;
	private String brandName;

	public String getSessioniId() {
		return sessioniId;
	}

	public void setSessioniId(String sessioniId) {
		this.sessioniId = sessioniId;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

}
