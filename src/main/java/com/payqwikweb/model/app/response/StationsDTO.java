package com.payqwikweb.model.app.response;

import java.util.ArrayList;

public class StationsDTO {
	private int arrivalDayIncrement;
	private String stationCode;
	private String arrivalTime;
	private int seqNo;
	private String dateAtStation;
	private String departureTime;
	private ArrayList<OutletTypeDTO>outletTypeDTO=new ArrayList<OutletTypeDTO>();
	private String delay;
	private boolean foodAvailablity;
	private String halt;
	private String stationName;
	
	
	public ArrayList<OutletTypeDTO> getOutletTypeDTO() {
		return outletTypeDTO;
	}
	public void setOutletTypeDTO(ArrayList<OutletTypeDTO> outletTypeDTO) {
		this.outletTypeDTO = outletTypeDTO;
	}
	public int getArrivalDayIncrement() {
		return arrivalDayIncrement;
	}
	public void setArrivalDayIncrement(int arrivalDayIncrement) {
		this.arrivalDayIncrement = arrivalDayIncrement;
	}
	public String getStationCode() {
		return stationCode;
	}
	public void setStationCode(String stationCode) {
		this.stationCode = stationCode;
	}
	public String getArrivalTime() {
		return arrivalTime;
	}
	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}
	public int getSeqNo() {
		return seqNo;
	}
	public void setSeqNo(int seqNo) {
		this.seqNo = seqNo;
	}
	public String getDateAtStation() {
		return dateAtStation;
	}
	public void setDateAtStation(String dateAtStation) {
		this.dateAtStation = dateAtStation;
	}
	public String getDepartureTime() {
		return departureTime;
	}
	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}

	public String getDelay() {
		return delay;
	}
	public void setDelay(String delay) {
		this.delay = delay;
	}
	public boolean isFoodAvailablity() {
		return foodAvailablity;
	}
	public void setFoodAvailablity(boolean foodAvailablity) {
		this.foodAvailablity = foodAvailablity;
	}
	public String getHalt() {
		return halt;
	}
	public void setHalt(String halt) {
		this.halt = halt;
	}
	public String getStationName() {
		return stationName;
	}
	public void setStationName(String stationName) {
		this.stationName = stationName;
	}
	
}
