package com.payqwikweb.model.app.response;

import java.util.ArrayList;

public class EventPrice {

	private String name;
	private String eid;
	private String value;
	private ArrayList<EventOccurence> eventoccur;
	
	
	public ArrayList<EventOccurence> getEventoccur() {
		return eventoccur;
	}

	public void setEventoccur(ArrayList<EventOccurence> eventoccur) {
		this.eventoccur = eventoccur;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEid() {
		return eid;
	}

	public void setEid(String eid) {
		this.eid = eid;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
