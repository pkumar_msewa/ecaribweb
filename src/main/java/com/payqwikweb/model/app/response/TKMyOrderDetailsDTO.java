package com.payqwikweb.model.app.response;

import java.util.ArrayList;
import java.util.List;

import com.payqwikweb.model.web.Status;

public class TKMyOrderDetailsDTO {
	private boolean success;
	private String code;
	private String message;
	private Status status;
	
	private ArrayList<TKOrderDetailsDTO> orderDetails;
	
	public ArrayList<TKOrderDetailsDTO> getOrderDetails() {
		return orderDetails;
	}

	public void setOrderDetails(ArrayList<TKOrderDetailsDTO> orderDetails) {
		this.orderDetails = orderDetails;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}
	
}
