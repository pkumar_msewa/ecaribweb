package com.payqwikweb.model.app.response;

public class MenuDTO {
	private double serviceTax;
	private String itemName;
	private int webDisplay;
	private double vat;
	private String image;
	private double customerPayable;
	private String itemId;
	private boolean isActive;
	private String menuTag;
	private double price;
	private String description;
	private String closingTime;
	private String openingTime;
	private double basePrice;
	private String[] additionalTags;
	
	public double getServiceTax() {
		return serviceTax;
	}
	public void setServiceTax(double serviceTax) {
		this.serviceTax = serviceTax;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public int getWebDisplay() {
		return webDisplay;
	}
	public void setWebDisplay(int webDisplay) {
		this.webDisplay = webDisplay;
	}
	public double getVat() {
		return vat;
	}
	public void setVat(double vat) {
		this.vat = vat;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public double getCustomerPayable() {
		return customerPayable;
	}
	public void setCustomerPayable(double customerPayable) {
		this.customerPayable = customerPayable;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public boolean isActive() {
		return isActive;
	}
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	public String getMenuTag() {
		return menuTag;
	}
	public void setMenuTag(String menuTag) {
		this.menuTag = menuTag;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getClosingTime() {
		return closingTime;
	}
	public void setClosingTime(String closingTime) {
		this.closingTime = closingTime;
	}
	public String getOpeningTime() {
		return openingTime;
	}
	public void setOpeningTime(String openingTime) {
		this.openingTime = openingTime;
	}
	public double getBasePrice() {
		return basePrice;
	}
	public void setBasePrice(double basePrice) {
		this.basePrice = basePrice;
	}
	public String[] getAdditionalTags() {
		return additionalTags;
	}
	public void setAdditionalTags(String[] additionalTags) {
		this.additionalTags = additionalTags;
	}
	
	
}
