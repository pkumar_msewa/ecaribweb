package com.payqwikweb.model.response.thirdpartyService;

public class flightdetailgetset {

	double x;

	String AirEquipType;

	String ArrivalAirportCode;

	String ArrivalDateTimeZone;

	String DepartureAirportCode;
	String DepartureDateTimeZone;

	String Duration;

	String FlightNumber;
	String OperatingAirlineCode;

	String OperatingAirlineFlightNumber;
	String RPH;
	String StopQuantity;
	String AirLineName;
	String AirportTax;
	String ImageFileName;
	String ViaFlight;

	String AirportTaxChild;

	String AirportTaxInfant;

	String AdultTaxBreakup;

	String InfantTaxBreakup;

	String OcTax;
	String IntNumStops;
	String IntOperatingAirlineName;
	String IntArrivalAirportName;

	String IntDepartureAirportName;
	String IntMarketingAirlineCode;
	String IntLinkSellAgrmnt;
	String IntConx;
	String IntAirpChg;
	String IntInsideAvailOption;
	String IntGenTrafRestriction;
	String IntDaysOperates;
	String IntJourneyTime;
	String IntEndDate;
	String IntStartTerminal;
	String IntEndTerminal;

	String IntFltTm;

	String IntLSAInd;

	String IntMile;
	String itineraryNumber;
	String arr;
	String dep;
	String arivdate;
	String depadate;

	String Rule;

	String AdultFare;
	String Bookingclass;
	String ClassType;
	String Farebasiscode;
	String AdultCommission;
	String ChildCommission;
	String CommissionOnTCharge;
	String ChildFare;
	String InfantFare;

	String Availability;
	String ResBookDesigCode;
	String IntBIC;
	String actualfare;
	String tax;
	String stax;
	String scharge;

	String tdiscount;
	String tpartneromision;
	String netfare;
	String convensefee;

	String tcharg;

	String tmarkup;

	String tsisount;

	String octax;

	String totalfare;

	String key;

	String Mode;
	String Source;
	String Destination;
	String Adults;
	String Children;
	String Infants;
	String DepartDate;
	String ReturnDate;

	String provider;

	String servi;// allflight.getJSONObject(i).getJSONObject("OriginDestinationoptionId").getString("Key");

	String i;
	String dd;
	String datflight;

	String returnAirEquipType;

	String returnArrivalAirportCode;

	String returnArrivalDateTimeZone;

	String returnDepartureAirportCode;
	String returnDepartureDateTimeZone;

	String returnDuration;

	String returnFlightNumber;
	String returnOperatingAirlineCode;

	String returnOperatingAirlineFlightNumber;
	String returnRPH;
	String returnStopQuantity;
	String returnAirLineName;
	String returnAirportTax;
	String returnImageFileName;
	String returnViaFlight;

	String returnAirportTaxChild;

	String returnAirportTaxInfant;

	String returnAdultTaxBreakup;

	String returnInfantTaxBreakup;

	String returnOcTax;
	String returnIntNumStops;
	String returnIntOperatingAirlineName;
	String returnIntArrivalAirportName;

	String returnIntDepartureAirportName;
	String returnIntMarketingAirlineCode;
	String returnIntLinkSellAgrmnt;
	String returnIntConx;
	String returnIntAirpChg;
	String returnIntInsideAvailOption;
	String returnIntGenTrafRestriction;
	String returnIntDaysOperates;
	String returnIntJourneyTime;
	String returnIntEndDate;
	String returnIntStartTerminal;
	String returnIntEndTerminal;

	String returnIntFltTm;

	String returnIntLSAInd;

	String returnIntMile;
	String returnitineraryNumber;
	String returnarr;
	String returndep;
	String returnarivdate;
	String returndepadate;

	String returnRule;

	String returnAdultFare;
	String returnBookingclass;
	String returnClassType;
	String returnFarebasiscode;
	String returnAdultCommission;
	String returnChildCommission;
	String returnCommissionOnTCharge;
	String returnChildFare;
	String returnInfantFare;

	String returnAvailability;
	String returnResBookDesigCode;
	String returnIntBIC;
	String returnactualfare;
	String returntax;
	String returnstax;
	String returnscharge;

	String returntdiscount;
	String returntpartneromision;
	String returnnetfare;
	String returnconvensefee;

	String returntcharg;

	String returntmarkup;

	String returntsisount;

	String returnoctax;

	String returntotalfare;

	String returnkey;

	String returnMode;
	String returnSource;
	String returnDestination;
	String returnAdults;
	String returnChildren;
	String returnInfants;
	String returnDepartDate;
	String returnReturnDate;

	String returnprovider;

	String returnservi;// allflight.getJSONObject(i).getJSONObject("OriginDestinationoptionId").getString("Key");

	String returni;
	String returndd;
	String returndatflight;
	double returnx;

	public String getReturnAirEquipType() {
		return returnAirEquipType;
	}

	public void setReturnAirEquipType(String returnAirEquipType) {
		this.returnAirEquipType = returnAirEquipType;
	}

	public String getReturnArrivalAirportCode() {
		return returnArrivalAirportCode;
	}

	public void setReturnArrivalAirportCode(String returnArrivalAirportCode) {
		this.returnArrivalAirportCode = returnArrivalAirportCode;
	}

	public String getReturnArrivalDateTimeZone() {
		return returnArrivalDateTimeZone;
	}

	public void setReturnArrivalDateTimeZone(String returnArrivalDateTimeZone) {
		this.returnArrivalDateTimeZone = returnArrivalDateTimeZone;
	}

	public String getReturnDepartureAirportCode() {
		return returnDepartureAirportCode;
	}

	public void setReturnDepartureAirportCode(String returnDepartureAirportCode) {
		this.returnDepartureAirportCode = returnDepartureAirportCode;
	}

	public String getReturnDepartureDateTimeZone() {
		return returnDepartureDateTimeZone;
	}

	public void setReturnDepartureDateTimeZone(String returnDepartureDateTimeZone) {
		this.returnDepartureDateTimeZone = returnDepartureDateTimeZone;
	}

	public String getReturnDuration() {
		return returnDuration;
	}

	public void setReturnDuration(String returnDuration) {
		this.returnDuration = returnDuration;
	}

	public String getReturnFlightNumber() {
		return returnFlightNumber;
	}

	public void setReturnFlightNumber(String returnFlightNumber) {
		this.returnFlightNumber = returnFlightNumber;
	}

	public String getReturnOperatingAirlineCode() {
		return returnOperatingAirlineCode;
	}

	public void setReturnOperatingAirlineCode(String returnOperatingAirlineCode) {
		this.returnOperatingAirlineCode = returnOperatingAirlineCode;
	}

	public String getReturnOperatingAirlineFlightNumber() {
		return returnOperatingAirlineFlightNumber;
	}

	public void setReturnOperatingAirlineFlightNumber(String returnOperatingAirlineFlightNumber) {
		this.returnOperatingAirlineFlightNumber = returnOperatingAirlineFlightNumber;
	}

	public String getReturnRPH() {
		return returnRPH;
	}

	public void setReturnRPH(String returnRPH) {
		this.returnRPH = returnRPH;
	}

	public String getReturnStopQuantity() {
		return returnStopQuantity;
	}

	public void setReturnStopQuantity(String returnStopQuantity) {
		this.returnStopQuantity = returnStopQuantity;
	}

	public String getReturnAirLineName() {
		return returnAirLineName;
	}

	public void setReturnAirLineName(String returnAirLineName) {
		this.returnAirLineName = returnAirLineName;
	}

	public String getReturnAirportTax() {
		return returnAirportTax;
	}

	public void setReturnAirportTax(String returnAirportTax) {
		this.returnAirportTax = returnAirportTax;
	}

	public String getReturnImageFileName() {
		return returnImageFileName;
	}

	public void setReturnImageFileName(String returnImageFileName) {
		this.returnImageFileName = returnImageFileName;
	}

	public String getReturnViaFlight() {
		return returnViaFlight;
	}

	public void setReturnViaFlight(String returnViaFlight) {
		this.returnViaFlight = returnViaFlight;
	}

	public String getReturnAirportTaxChild() {
		return returnAirportTaxChild;
	}

	public void setReturnAirportTaxChild(String returnAirportTaxChild) {
		this.returnAirportTaxChild = returnAirportTaxChild;
	}

	public String getReturnAirportTaxInfant() {
		return returnAirportTaxInfant;
	}

	public void setReturnAirportTaxInfant(String returnAirportTaxInfant) {
		this.returnAirportTaxInfant = returnAirportTaxInfant;
	}

	public String getReturnAdultTaxBreakup() {
		return returnAdultTaxBreakup;
	}

	public void setReturnAdultTaxBreakup(String returnAdultTaxBreakup) {
		this.returnAdultTaxBreakup = returnAdultTaxBreakup;
	}

	public String getReturnInfantTaxBreakup() {
		return returnInfantTaxBreakup;
	}

	public void setReturnInfantTaxBreakup(String returnInfantTaxBreakup) {
		this.returnInfantTaxBreakup = returnInfantTaxBreakup;
	}

	public String getReturnOcTax() {
		return returnOcTax;
	}

	public void setReturnOcTax(String returnOcTax) {
		this.returnOcTax = returnOcTax;
	}

	public String getReturnIntNumStops() {
		return returnIntNumStops;
	}

	public void setReturnIntNumStops(String returnIntNumStops) {
		this.returnIntNumStops = returnIntNumStops;
	}

	public String getReturnIntOperatingAirlineName() {
		return returnIntOperatingAirlineName;
	}

	public void setReturnIntOperatingAirlineName(String returnIntOperatingAirlineName) {
		this.returnIntOperatingAirlineName = returnIntOperatingAirlineName;
	}

	public String getReturnIntArrivalAirportName() {
		return returnIntArrivalAirportName;
	}

	public void setReturnIntArrivalAirportName(String returnIntArrivalAirportName) {
		this.returnIntArrivalAirportName = returnIntArrivalAirportName;
	}

	public String getReturnIntDepartureAirportName() {
		return returnIntDepartureAirportName;
	}

	public void setReturnIntDepartureAirportName(String returnIntDepartureAirportName) {
		this.returnIntDepartureAirportName = returnIntDepartureAirportName;
	}

	public String getReturnIntMarketingAirlineCode() {
		return returnIntMarketingAirlineCode;
	}

	public void setReturnIntMarketingAirlineCode(String returnIntMarketingAirlineCode) {
		this.returnIntMarketingAirlineCode = returnIntMarketingAirlineCode;
	}

	public String getReturnIntLinkSellAgrmnt() {
		return returnIntLinkSellAgrmnt;
	}

	public void setReturnIntLinkSellAgrmnt(String returnIntLinkSellAgrmnt) {
		this.returnIntLinkSellAgrmnt = returnIntLinkSellAgrmnt;
	}

	public String getReturnIntConx() {
		return returnIntConx;
	}

	public void setReturnIntConx(String returnIntConx) {
		this.returnIntConx = returnIntConx;
	}

	public String getReturnIntAirpChg() {
		return returnIntAirpChg;
	}

	public void setReturnIntAirpChg(String returnIntAirpChg) {
		this.returnIntAirpChg = returnIntAirpChg;
	}

	public String getReturnIntInsideAvailOption() {
		return returnIntInsideAvailOption;
	}

	public void setReturnIntInsideAvailOption(String returnIntInsideAvailOption) {
		this.returnIntInsideAvailOption = returnIntInsideAvailOption;
	}

	public String getReturnIntGenTrafRestriction() {
		return returnIntGenTrafRestriction;
	}

	public void setReturnIntGenTrafRestriction(String returnIntGenTrafRestriction) {
		this.returnIntGenTrafRestriction = returnIntGenTrafRestriction;
	}

	public String getReturnIntDaysOperates() {
		return returnIntDaysOperates;
	}

	public void setReturnIntDaysOperates(String returnIntDaysOperates) {
		this.returnIntDaysOperates = returnIntDaysOperates;
	}

	public String getReturnIntJourneyTime() {
		return returnIntJourneyTime;
	}

	public void setReturnIntJourneyTime(String returnIntJourneyTime) {
		this.returnIntJourneyTime = returnIntJourneyTime;
	}

	public String getReturnIntEndDate() {
		return returnIntEndDate;
	}

	public void setReturnIntEndDate(String returnIntEndDate) {
		this.returnIntEndDate = returnIntEndDate;
	}

	public String getReturnIntStartTerminal() {
		return returnIntStartTerminal;
	}

	public void setReturnIntStartTerminal(String returnIntStartTerminal) {
		this.returnIntStartTerminal = returnIntStartTerminal;
	}

	public String getReturnIntEndTerminal() {
		return returnIntEndTerminal;
	}

	public void setReturnIntEndTerminal(String returnIntEndTerminal) {
		this.returnIntEndTerminal = returnIntEndTerminal;
	}

	public String getReturnIntFltTm() {
		return returnIntFltTm;
	}

	public void setReturnIntFltTm(String returnIntFltTm) {
		this.returnIntFltTm = returnIntFltTm;
	}

	public String getReturnIntLSAInd() {
		return returnIntLSAInd;
	}

	public void setReturnIntLSAInd(String returnIntLSAInd) {
		this.returnIntLSAInd = returnIntLSAInd;
	}

	public String getReturnIntMile() {
		return returnIntMile;
	}

	public void setReturnIntMile(String returnIntMile) {
		this.returnIntMile = returnIntMile;
	}

	public String getReturnitineraryNumber() {
		return returnitineraryNumber;
	}

	public void setReturnitineraryNumber(String returnitineraryNumber) {
		this.returnitineraryNumber = returnitineraryNumber;
	}

	public String getReturnarr() {
		return returnarr;
	}

	public void setReturnarr(String returnarr) {
		this.returnarr = returnarr;
	}

	public String getReturndep() {
		return returndep;
	}

	public void setReturndep(String returndep) {
		this.returndep = returndep;
	}

	public String getReturnarivdate() {
		return returnarivdate;
	}

	public void setReturnarivdate(String returnarivdate) {
		this.returnarivdate = returnarivdate;
	}

	public String getReturndepadate() {
		return returndepadate;
	}

	public void setReturndepadate(String returndepadate) {
		this.returndepadate = returndepadate;
	}

	public String getReturnRule() {
		return returnRule;
	}

	public void setReturnRule(String returnRule) {
		this.returnRule = returnRule;
	}

	public String getReturnAdultFare() {
		return returnAdultFare;
	}

	public void setReturnAdultFare(String returnAdultFare) {
		this.returnAdultFare = returnAdultFare;
	}

	public String getReturnBookingclass() {
		return returnBookingclass;
	}

	public void setReturnBookingclass(String returnBookingclass) {
		this.returnBookingclass = returnBookingclass;
	}

	public String getReturnClassType() {
		return returnClassType;
	}

	public void setReturnClassType(String returnClassType) {
		this.returnClassType = returnClassType;
	}

	public String getReturnFarebasiscode() {
		return returnFarebasiscode;
	}

	public void setReturnFarebasiscode(String returnFarebasiscode) {
		this.returnFarebasiscode = returnFarebasiscode;
	}

	public String getReturnAdultCommission() {
		return returnAdultCommission;
	}

	public void setReturnAdultCommission(String returnAdultCommission) {
		this.returnAdultCommission = returnAdultCommission;
	}

	public String getReturnChildCommission() {
		return returnChildCommission;
	}

	public void setReturnChildCommission(String returnChildCommission) {
		this.returnChildCommission = returnChildCommission;
	}

	public String getReturnCommissionOnTCharge() {
		return returnCommissionOnTCharge;
	}

	public void setReturnCommissionOnTCharge(String returnCommissionOnTCharge) {
		this.returnCommissionOnTCharge = returnCommissionOnTCharge;
	}

	public String getReturnChildFare() {
		return returnChildFare;
	}

	public void setReturnChildFare(String returnChildFare) {
		this.returnChildFare = returnChildFare;
	}

	public String getReturnInfantFare() {
		return returnInfantFare;
	}

	public void setReturnInfantFare(String returnInfantFare) {
		this.returnInfantFare = returnInfantFare;
	}

	public String getReturnAvailability() {
		return returnAvailability;
	}

	public void setReturnAvailability(String returnAvailability) {
		this.returnAvailability = returnAvailability;
	}

	public String getReturnResBookDesigCode() {
		return returnResBookDesigCode;
	}

	public void setReturnResBookDesigCode(String returnResBookDesigCode) {
		this.returnResBookDesigCode = returnResBookDesigCode;
	}

	public String getReturnIntBIC() {
		return returnIntBIC;
	}

	public void setReturnIntBIC(String returnIntBIC) {
		this.returnIntBIC = returnIntBIC;
	}

	public String getReturnactualfare() {
		return returnactualfare;
	}

	public void setReturnactualfare(String returnactualfare) {
		this.returnactualfare = returnactualfare;
	}

	public String getReturntax() {
		return returntax;
	}

	public void setReturntax(String returntax) {
		this.returntax = returntax;
	}

	public String getReturnstax() {
		return returnstax;
	}

	public void setReturnstax(String returnstax) {
		this.returnstax = returnstax;
	}

	public String getReturnscharge() {
		return returnscharge;
	}

	public void setReturnscharge(String returnscharge) {
		this.returnscharge = returnscharge;
	}

	public String getReturntdiscount() {
		return returntdiscount;
	}

	public void setReturntdiscount(String returntdiscount) {
		this.returntdiscount = returntdiscount;
	}

	public String getReturntpartneromision() {
		return returntpartneromision;
	}

	public void setReturntpartneromision(String returntpartneromision) {
		this.returntpartneromision = returntpartneromision;
	}

	public String getReturnnetfare() {
		return returnnetfare;
	}

	public void setReturnnetfare(String returnnetfare) {
		this.returnnetfare = returnnetfare;
	}

	public String getReturnconvensefee() {
		return returnconvensefee;
	}

	public void setReturnconvensefee(String returnconvensefee) {
		this.returnconvensefee = returnconvensefee;
	}

	public String getReturntcharg() {
		return returntcharg;
	}

	public void setReturntcharg(String returntcharg) {
		this.returntcharg = returntcharg;
	}

	public String getReturntmarkup() {
		return returntmarkup;
	}

	public void setReturntmarkup(String returntmarkup) {
		this.returntmarkup = returntmarkup;
	}

	public String getReturntsisount() {
		return returntsisount;
	}

	public void setReturntsisount(String returntsisount) {
		this.returntsisount = returntsisount;
	}

	public String getReturnoctax() {
		return returnoctax;
	}

	public void setReturnoctax(String returnoctax) {
		this.returnoctax = returnoctax;
	}

	public String getReturntotalfare() {
		return returntotalfare;
	}

	public void setReturntotalfare(String returntotalfare) {
		this.returntotalfare = returntotalfare;
	}

	public String getReturnkey() {
		return returnkey;
	}

	public void setReturnkey(String returnkey) {
		this.returnkey = returnkey;
	}

	public String getReturnMode() {
		return returnMode;
	}

	public void setReturnMode(String returnMode) {
		this.returnMode = returnMode;
	}

	public String getReturnSource() {
		return returnSource;
	}

	public void setReturnSource(String returnSource) {
		this.returnSource = returnSource;
	}

	public String getReturnDestination() {
		return returnDestination;
	}

	public void setReturnDestination(String returnDestination) {
		this.returnDestination = returnDestination;
	}

	public String getReturnAdults() {
		return returnAdults;
	}

	public void setReturnAdults(String returnAdults) {
		this.returnAdults = returnAdults;
	}

	public String getReturnChildren() {
		return returnChildren;
	}

	public void setReturnChildren(String returnChildren) {
		this.returnChildren = returnChildren;
	}

	public String getReturnInfants() {
		return returnInfants;
	}

	public void setReturnInfants(String returnInfants) {
		this.returnInfants = returnInfants;
	}

	public String getReturnDepartDate() {
		return returnDepartDate;
	}

	public void setReturnDepartDate(String returnDepartDate) {
		this.returnDepartDate = returnDepartDate;
	}

	public String getReturnReturnDate() {
		return returnReturnDate;
	}

	public void setReturnReturnDate(String returnReturnDate) {
		this.returnReturnDate = returnReturnDate;
	}

	public String getReturnprovider() {
		return returnprovider;
	}

	public void setReturnprovider(String returnprovider) {
		this.returnprovider = returnprovider;
	}

	public String getReturnservi() {
		return returnservi;
	}

	public void setReturnservi(String returnservi) {
		this.returnservi = returnservi;
	}

	public String getReturni() {
		return returni;
	}

	public void setReturni(String returni) {
		this.returni = returni;
	}

	public String getReturndd() {
		return returndd;
	}

	public void setReturndd(String returndd) {
		this.returndd = returndd;
	}

	public String getReturndatflight() {
		return returndatflight;
	}

	public void setReturndatflight(String returndatflight) {
		this.returndatflight = returndatflight;
	}

	public double getReturnx() {
		return returnx;
	}

	public void setReturnx(double returnx) {
		this.returnx = returnx;
	}

	public String getAirEquipType() {
		return AirEquipType;
	}

	public void setAirEquipType(String airEquipType) {
		AirEquipType = airEquipType;
	}

	public String getArrivalAirportCode() {
		return ArrivalAirportCode;
	}

	public void setArrivalAirportCode(String arrivalAirportCode) {
		ArrivalAirportCode = arrivalAirportCode;
	}

	public String getArrivalDateTimeZone() {
		return ArrivalDateTimeZone;
	}

	public void setArrivalDateTimeZone(String arrivalDateTimeZone) {
		ArrivalDateTimeZone = arrivalDateTimeZone;
	}

	public String getDepartureAirportCode() {
		return DepartureAirportCode;
	}

	public void setDepartureAirportCode(String departureAirportCode) {
		DepartureAirportCode = departureAirportCode;
	}

	public String getDepartureDateTimeZone() {
		return DepartureDateTimeZone;
	}

	public void setDepartureDateTimeZone(String departureDateTimeZone) {
		DepartureDateTimeZone = departureDateTimeZone;
	}

	public String getDuration() {
		return Duration;
	}

	public void setDuration(String duration) {
		Duration = duration;
	}

	public String getFlightNumber() {
		return FlightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		FlightNumber = flightNumber;
	}

	public String getOperatingAirlineCode() {
		return OperatingAirlineCode;
	}

	public void setOperatingAirlineCode(String operatingAirlineCode) {
		OperatingAirlineCode = operatingAirlineCode;
	}

	public String getOperatingAirlineFlightNumber() {
		return OperatingAirlineFlightNumber;
	}

	public void setOperatingAirlineFlightNumber(String operatingAirlineFlightNumber) {
		OperatingAirlineFlightNumber = operatingAirlineFlightNumber;
	}

	public String getRPH() {
		return RPH;
	}

	public void setRPH(String rPH) {
		RPH = rPH;
	}

	public String getStopQuantity() {
		return StopQuantity;
	}

	public void setStopQuantity(String stopQuantity) {
		StopQuantity = stopQuantity;
	}

	public String getAirLineName() {
		return AirLineName;
	}

	public void setAirLineName(String airLineName) {
		AirLineName = airLineName;
	}

	public String getAirportTax() {
		return AirportTax;
	}

	public void setAirportTax(String airportTax) {
		AirportTax = airportTax;
	}

	public String getImageFileName() {
		return ImageFileName;
	}

	public void setImageFileName(String imageFileName) {
		ImageFileName = imageFileName;
	}

	public String getViaFlight() {
		return ViaFlight;
	}

	public void setViaFlight(String viaFlight) {
		ViaFlight = viaFlight;
	}

	public String getAirportTaxChild() {
		return AirportTaxChild;
	}

	public void setAirportTaxChild(String airportTaxChild) {
		AirportTaxChild = airportTaxChild;
	}

	public String getAirportTaxInfant() {
		return AirportTaxInfant;
	}

	public void setAirportTaxInfant(String airportTaxInfant) {
		AirportTaxInfant = airportTaxInfant;
	}

	public String getAdultTaxBreakup() {
		return AdultTaxBreakup;
	}

	public void setAdultTaxBreakup(String adultTaxBreakup) {
		AdultTaxBreakup = adultTaxBreakup;
	}

	public String getInfantTaxBreakup() {
		return InfantTaxBreakup;
	}

	public void setInfantTaxBreakup(String infantTaxBreakup) {
		InfantTaxBreakup = infantTaxBreakup;
	}

	public String getOcTax() {
		return OcTax;
	}

	public void setOcTax(String ocTax) {
		OcTax = ocTax;
	}

	public String getIntNumStops() {
		return IntNumStops;
	}

	public void setIntNumStops(String intNumStops) {
		IntNumStops = intNumStops;
	}

	public String getIntOperatingAirlineName() {
		return IntOperatingAirlineName;
	}

	public void setIntOperatingAirlineName(String intOperatingAirlineName) {
		IntOperatingAirlineName = intOperatingAirlineName;
	}

	public String getIntArrivalAirportName() {
		return IntArrivalAirportName;
	}

	public void setIntArrivalAirportName(String intArrivalAirportName) {
		IntArrivalAirportName = intArrivalAirportName;
	}

	public String getIntDepartureAirportName() {
		return IntDepartureAirportName;
	}

	public void setIntDepartureAirportName(String intDepartureAirportName) {
		IntDepartureAirportName = intDepartureAirportName;
	}

	public String getIntMarketingAirlineCode() {
		return IntMarketingAirlineCode;
	}

	public void setIntMarketingAirlineCode(String intMarketingAirlineCode) {
		IntMarketingAirlineCode = intMarketingAirlineCode;
	}

	public String getIntLinkSellAgrmnt() {
		return IntLinkSellAgrmnt;
	}

	public void setIntLinkSellAgrmnt(String intLinkSellAgrmnt) {
		IntLinkSellAgrmnt = intLinkSellAgrmnt;
	}

	public String getIntConx() {
		return IntConx;
	}

	public void setIntConx(String intConx) {
		IntConx = intConx;
	}

	public String getIntAirpChg() {
		return IntAirpChg;
	}

	public void setIntAirpChg(String intAirpChg) {
		IntAirpChg = intAirpChg;
	}

	public String getIntInsideAvailOption() {
		return IntInsideAvailOption;
	}

	public void setIntInsideAvailOption(String intInsideAvailOption) {
		IntInsideAvailOption = intInsideAvailOption;
	}

	public String getIntGenTrafRestriction() {
		return IntGenTrafRestriction;
	}

	public void setIntGenTrafRestriction(String intGenTrafRestriction) {
		IntGenTrafRestriction = intGenTrafRestriction;
	}

	public String getIntDaysOperates() {
		return IntDaysOperates;
	}

	public void setIntDaysOperates(String intDaysOperates) {
		IntDaysOperates = intDaysOperates;
	}

	public String getIntJourneyTime() {
		return IntJourneyTime;
	}

	public void setIntJourneyTime(String intJourneyTime) {
		IntJourneyTime = intJourneyTime;
	}

	public String getIntEndDate() {
		return IntEndDate;
	}

	public void setIntEndDate(String intEndDate) {
		IntEndDate = intEndDate;
	}

	public String getIntStartTerminal() {
		return IntStartTerminal;
	}

	public void setIntStartTerminal(String intStartTerminal) {
		IntStartTerminal = intStartTerminal;
	}

	public String getIntEndTerminal() {
		return IntEndTerminal;
	}

	public void setIntEndTerminal(String intEndTerminal) {
		IntEndTerminal = intEndTerminal;
	}

	public String getIntFltTm() {
		return IntFltTm;
	}

	public void setIntFltTm(String intFltTm) {
		IntFltTm = intFltTm;
	}

	public String getIntLSAInd() {
		return IntLSAInd;
	}

	public void setIntLSAInd(String intLSAInd) {
		IntLSAInd = intLSAInd;
	}

	public String getIntMile() {
		return IntMile;
	}

	public void setIntMile(String intMile) {
		IntMile = intMile;
	}

	public String getItineraryNumber() {
		return itineraryNumber;
	}

	public void setItineraryNumber(String itineraryNumber) {
		this.itineraryNumber = itineraryNumber;
	}

	public String getArr() {
		return arr;
	}

	public void setArr(String arr) {
		this.arr = arr;
	}

	public String getDep() {
		return dep;
	}

	public void setDep(String dep) {
		this.dep = dep;
	}

	public String getArivdate() {
		return arivdate;
	}

	public void setArivdate(String arivdate) {
		this.arivdate = arivdate;
	}

	public String getDepadate() {
		return depadate;
	}

	public void setDepadate(String depadate) {
		this.depadate = depadate;
	}

	public String getRule() {
		return Rule;
	}

	public void setRule(String rule) {
		Rule = rule;
	}

	public String getAdultFare() {
		return AdultFare;
	}

	public void setAdultFare(String adultFare) {
		AdultFare = adultFare;
	}

	public String getBookingclass() {
		return Bookingclass;
	}

	public void setBookingclass(String bookingclass) {
		Bookingclass = bookingclass;
	}

	public String getClassType() {
		return ClassType;
	}

	public void setClassType(String classType) {
		ClassType = classType;
	}

	public String getFarebasiscode() {
		return Farebasiscode;
	}

	public void setFarebasiscode(String farebasiscode) {
		Farebasiscode = farebasiscode;
	}

	public String getAdultCommission() {
		return AdultCommission;
	}

	public void setAdultCommission(String adultCommission) {
		AdultCommission = adultCommission;
	}

	public String getChildCommission() {
		return ChildCommission;
	}

	public void setChildCommission(String childCommission) {
		ChildCommission = childCommission;
	}

	public String getCommissionOnTCharge() {
		return CommissionOnTCharge;
	}

	public void setCommissionOnTCharge(String commissionOnTCharge) {
		CommissionOnTCharge = commissionOnTCharge;
	}

	public String getChildFare() {
		return ChildFare;
	}

	public void setChildFare(String childFare) {
		ChildFare = childFare;
	}

	public String getInfantFare() {
		return InfantFare;
	}

	public void setInfantFare(String infantFare) {
		InfantFare = infantFare;
	}

	public String getAvailability() {
		return Availability;
	}

	public void setAvailability(String availability) {
		Availability = availability;
	}

	public String getResBookDesigCode() {
		return ResBookDesigCode;
	}

	public void setResBookDesigCode(String resBookDesigCode) {
		ResBookDesigCode = resBookDesigCode;
	}

	public String getIntBIC() {
		return IntBIC;
	}

	public void setIntBIC(String intBIC) {
		IntBIC = intBIC;
	}

	public String getActualfare() {
		return actualfare;
	}

	public void setActualfare(String actualfare) {
		this.actualfare = actualfare;
	}

	public String getTax() {
		return tax;
	}

	public void setTax(String tax) {
		this.tax = tax;
	}

	public String getStax() {
		return stax;
	}

	public void setStax(String stax) {
		this.stax = stax;
	}

	public String getScharge() {
		return scharge;
	}

	public void setScharge(String scharge) {
		this.scharge = scharge;
	}

	public String getTdiscount() {
		return tdiscount;
	}

	public void setTdiscount(String tdiscount) {
		this.tdiscount = tdiscount;
	}

	public String getTpartneromision() {
		return tpartneromision;
	}

	public void setTpartneromision(String tpartneromision) {
		this.tpartneromision = tpartneromision;
	}

	public String getNetfare() {
		return netfare;
	}

	public void setNetfare(String netfare) {
		this.netfare = netfare;
	}

	public String getConvensefee() {
		return convensefee;
	}

	public void setConvensefee(String convensefee) {
		this.convensefee = convensefee;
	}

	public String getTcharg() {
		return tcharg;
	}

	public void setTcharg(String tcharg) {
		this.tcharg = tcharg;
	}

	public String getTmarkup() {
		return tmarkup;
	}

	public void setTmarkup(String tmarkup) {
		this.tmarkup = tmarkup;
	}

	public String getTsisount() {
		return tsisount;
	}

	public void setTsisount(String tsisount) {
		this.tsisount = tsisount;
	}

	public String getOctax() {
		return octax;
	}

	public void setOctax(String octax) {
		this.octax = octax;
	}

	public String getTotalfare() {
		return totalfare;
	}

	public void setTotalfare(String totalfare) {
		this.totalfare = totalfare;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getMode() {
		return Mode;
	}

	public void setMode(String mode) {
		Mode = mode;
	}

	public String getSource() {
		return Source;
	}

	public void setSource(String source) {
		Source = source;
	}

	public String getDestination() {
		return Destination;
	}

	public void setDestination(String destination) {
		Destination = destination;
	}

	public String getAdults() {
		return Adults;
	}

	public void setAdults(String adults) {
		Adults = adults;
	}

	public String getChildren() {
		return Children;
	}

	public void setChildren(String children) {
		Children = children;
	}

	public String getInfants() {
		return Infants;
	}

	public void setInfants(String infants) {
		Infants = infants;
	}

	public String getDepartDate() {
		return DepartDate;
	}

	public void setDepartDate(String departDate) {
		DepartDate = departDate;
	}

	public String getReturnDate() {
		return ReturnDate;
	}

	public void setReturnDate(String returnDate) {
		ReturnDate = returnDate;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public String getServi() {
		return servi;
	}

	public void setServi(String servi) {
		this.servi = servi;
	}

	public String getI() {
		return i;
	}

	public void setI(String i) {
		this.i = i;
	}

	public String getDd() {
		return dd;
	}

	public void setDd(String dd) {
		this.dd = dd;
	}

	public String getDatflight() {
		return datflight;
	}

	public void setDatflight(String datflight) {
		this.datflight = datflight;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

}
