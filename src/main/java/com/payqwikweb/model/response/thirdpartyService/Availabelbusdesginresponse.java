package com.payqwikweb.model.response.thirdpartyService;

public class Availabelbusdesginresponse
{


	private String citysourceoneway;
	private String citydestoneway;
	private String datepickeronewaydeparure;
	
	private String DisplayName;
	private String ArrivalTime;
	private String Duration;
	private String Fares; 
	private String AvailableSeats;
	private String tripid;
	private String porvidr;
	private String travels; 
	private String i;
	private String CancellationPolicy;
	private String ConvenienceFee;
	private String PartialCancellationAllowed;
	private String BusType;
	private String DepartureTime;
	String landmark;
	String pointId;	
	
	
	public String getLandmark() {
		return landmark;
	}
	public void setLandmark(String landmark) {
		this.landmark = landmark;
	}
	public String getPointId() {
		return pointId;
	}
	public void setPointId(String pointId) {
		this.pointId = pointId;
	}
	public String getCitysourceoneway() {
		return citysourceoneway;
	}
	public void setCitysourceoneway(String citysourceoneway) {
		this.citysourceoneway = citysourceoneway;
	}
	public String getCitydestoneway() {
		return citydestoneway;
	}
	public void setCitydestoneway(String citydestoneway) {
		this.citydestoneway = citydestoneway;
	}
	public String getDatepickeronewaydeparure() {
		return datepickeronewaydeparure;
	}
	public void setDatepickeronewaydeparure(String datepickeronewaydeparure) {
		this.datepickeronewaydeparure = datepickeronewaydeparure;
	}
	public String getDisplayName() {
		return DisplayName;
	}
	public void setDisplayName(String displayName) {
		DisplayName = displayName;
	}
	public String getArrivalTime() {
		return ArrivalTime;
	}
	public void setArrivalTime(String arrivalTime) {
		ArrivalTime = arrivalTime;
	}
	public String getDuration() {
		return Duration;
	}
	public void setDuration(String duration) {
		Duration = duration;
	}
	public String getFares() {
		return Fares;
	}
	public void setFares(String fares) {
		Fares = fares;
	}
	public String getAvailableSeats() {
		return AvailableSeats;
	}
	public void setAvailableSeats(String availableSeats) {
		AvailableSeats = availableSeats;
	}
	public String getTripid() {
		return tripid;
	}
	public void setTripid(String tripid) {
		this.tripid = tripid;
	}
	public String getPorvidr() {
		return porvidr;
	}
	public void setPorvidr(String porvidr) {
		this.porvidr = porvidr;
	}
	public String getTravels() {
		return travels;
	}
	public void setTravels(String travels) {
		this.travels = travels;
	}
	public String getI() {
		return i;
	}
	public void setI(String i) {
		this.i = i;
	}
	public String getCancellationPolicy() {
		return CancellationPolicy;
	}
	public void setCancellationPolicy(String cancellationPolicy) {
		CancellationPolicy = cancellationPolicy;
	}
	public String getConvenienceFee() {
		return ConvenienceFee;
	}
	public void setConvenienceFee(String convenienceFee) {
		ConvenienceFee = convenienceFee;
	}
	public String getPartialCancellationAllowed() {
		return PartialCancellationAllowed;
	}
	public void setPartialCancellationAllowed(String partialCancellationAllowed) {
		PartialCancellationAllowed = partialCancellationAllowed;
	}
	public String getBusType() {
		return BusType;
	}
	public void setBusType(String busType) {
		BusType = busType;
	}
	public String getDepartureTime() {
		return DepartureTime;
	}
	public void setDepartureTime(String departureTime) {
		DepartureTime = departureTime;
	}
		

}
