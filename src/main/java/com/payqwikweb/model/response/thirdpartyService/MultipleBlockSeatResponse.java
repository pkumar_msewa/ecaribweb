package com.payqwikweb.model.response.thirdpartyService;

public class MultipleBlockSeatResponse {

	private String msg;
	private String code;
	private boolean status;
	private String codecondtion;

	public String getCodecondtion() {
		return codecondtion;
	}

	public void setCodecondtion(String codecondtion) {
		this.codecondtion = codecondtion;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

}
