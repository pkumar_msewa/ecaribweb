package com.payqwikweb.model.response.thirdpartyService;

public class Availvelcabdesgin 
{
	String travelType;
	String tripType ;
	String vehicleId ;
	String name ;

	String i;
	String description;
	String image;
	String seatingCapacity;
	String perKm;
	String approxDistance;
	String minimumChargedDistance;
	String driverCharges;
	String nightHalt;
	String days;
	String provider;
	String pmkhg;
	String perKmRateOneWayCharge;

	
	String totalAmount;
	String totalNetAmount;
	String basicRate;
	String extraHourRate;
	String waitingCharges;
	String cancellationPolicy;
	String convenienceFee;
	String affiliateId;
	
	 String sourceId;

	 String destinationId;

	String journeyDate;
	String sourcerName;

	String destinationname;
	
	 String pickUpTime;

	public String getPickUpTime() {
		return pickUpTime;
	}
	public void setPickUpTime(String pickUpTime) {
		this.pickUpTime = pickUpTime;
	}
	public String getSourceId() {
		return sourceId;
	}
	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}
	public String getDestinationId() {
		return destinationId;
	}
	public void setDestinationId(String destinationId) {
		this.destinationId = destinationId;
	}
	public String getJourneyDate() {
		return journeyDate;
	}
	public void setJourneyDate(String journeyDate) {
		this.journeyDate = journeyDate;
	}
	public String getSourcerName() {
		return sourcerName;
	}
	public void setSourcerName(String sourcerName) {
		this.sourcerName = sourcerName;
	}
	
	public String getDestinationname() {
		return destinationname;
	}
	public void setDestinationname(String destinationname) {
		this.destinationname = destinationname;
	}
	public String getTravelType() {
		return travelType;
	}
	public String getT() {
		return travelType;
	}
	public void setTravelType(String travelType) {
		this.travelType = travelType;
	}
	public String getTripType() {
		return tripType;
	}
	public void setTripType(String tripType) {
		this.tripType = tripType;
	}
	public String getVehicleId() {
		return vehicleId;
	}
	public void setVehicleId(String vehicleId) {
		this.vehicleId = vehicleId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getI() {
		return i;
	}
	public void setI(String i) {
		this.i = i;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getSeatingCapacity() {
		return seatingCapacity;
	}
	public void setSeatingCapacity(String seatingCapacity) {
		this.seatingCapacity = seatingCapacity;
	}
	public String getPerKm() {
		return perKm;
	}
	public void setPerKm(String perKm) {
		this.perKm = perKm;
	}
	public String getApproxDistance() {
		return approxDistance;
	}
	public void setApproxDistance(String approxDistance) {
		this.approxDistance = approxDistance;
	}
	public String getMinimumChargedDistance() {
		return minimumChargedDistance;
	}
	public void setMinimumChargedDistance(String minimumChargedDistance) {
		this.minimumChargedDistance = minimumChargedDistance;
	}
	public String getDriverCharges() {
		return driverCharges;
	}
	public void setDriverCharges(String driverCharges) {
		this.driverCharges = driverCharges;
	}
	public String getNightHalt() {
		return nightHalt;
	}
	public void setNightHalt(String nightHalt) {
		this.nightHalt = nightHalt;
	}
	public String getDays() {
		return days;
	}
	public void setDays(String days) {
		this.days = days;
	}
	public String getProvider() {
		return provider;
	}
	public void setProvider(String provider) {
		this.provider = provider;
	}
	public String getPmkhg() {
		return pmkhg;
	}
	public void setPmkhg(String pmkhg) {
		this.pmkhg = pmkhg;
	}
	public String getPerKmRateOneWayCharge() {
		return perKmRateOneWayCharge;
	}
	public void setPerKmRateOneWayCharge(String perKmRateOneWayCharge) {
		this.perKmRateOneWayCharge = perKmRateOneWayCharge;
	}
	public String getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}
	public String getTotalNetAmount() {
		return totalNetAmount;
	}
	public void setTotalNetAmount(String totalNetAmount) {
		this.totalNetAmount = totalNetAmount;
	}
	public String getBasicRate() {
		return basicRate;
	}
	public void setBasicRate(String basicRate) {
		this.basicRate = basicRate;
	}
	public String getExtraHourRate() {
		return extraHourRate;
	}
	public void setExtraHourRate(String extraHourRate) {
		this.extraHourRate = extraHourRate;
	}
	public String getWaitingCharges() {
		return waitingCharges;
	}
	public void setWaitingCharges(String waitingCharges) {
		this.waitingCharges = waitingCharges;
	}
	public String getCancellationPolicy() {
		return cancellationPolicy;
	}
	public void setCancellationPolicy(String cancellationPolicy) {
		this.cancellationPolicy = cancellationPolicy;
	}
	public String getConvenienceFee() {
		return convenienceFee;
	}
	public void setConvenienceFee(String convenienceFee) {
		this.convenienceFee = convenienceFee;
	}
	public String getAffiliateId() {
		return affiliateId;
	}
	public void setAffiliateId(String affiliateId) {
		this.affiliateId = affiliateId;
	}
	
}
