package com.payqwikweb.model.response.thirdpartyService;

public class AvailabelseatsResponse {
	String Number;
	String fare;
	String i;

	public String getNumber() {
		return Number;
	}

	public void setNumber(String number) {
		Number = number;
	}

	public String getFare() {
		return fare;
	}

	public void setFare(String fare) {
		this.fare = fare;
	}

	public String getI() {
		return i;
	}

	public void setI(String i) {
		this.i = i;
	}

}
