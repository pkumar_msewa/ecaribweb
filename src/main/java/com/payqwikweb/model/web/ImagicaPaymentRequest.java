package com.payqwikweb.model.web;

public class ImagicaPaymentRequest {

    private String sessionId;
    private String transctionId;
    private String profileId;
    private boolean success;

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getTransctionId() {
        return transctionId;
    }

    public void setTransctionId(String transctionId) {
        this.transctionId = transctionId;
    }

    public String getProfileId() {
        return profileId;
    }

    public void setProfileId(String profileId) {
        this.profileId = profileId;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
