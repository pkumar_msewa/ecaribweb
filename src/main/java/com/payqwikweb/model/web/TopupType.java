package com.payqwikweb.model.web;

public enum TopupType {
	Prepaid, Postpaid, DataCard,Dth,Landline,Electricity

}
