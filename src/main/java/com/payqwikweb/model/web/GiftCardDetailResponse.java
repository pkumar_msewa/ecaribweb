package com.payqwikweb.model.web;

import java.util.List;

import com.payqwikweb.app.model.WoohooGiftCardDTO;

public class GiftCardDetailResponse {
 
	private String code;
	private String message;
	private Object details;
	private List<WoohooGiftCardDTO> giftCards;
	private boolean success;
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Object getDetails() {
		return details;
	}
	public void setDetails(Object details) {
		this.details = details;
	}
	public List<WoohooGiftCardDTO> getGiftCards() {
		return giftCards;
	}
	public void setGiftCards(List<WoohooGiftCardDTO> giftCards) {
		this.giftCards = giftCards;
	}
	
}
