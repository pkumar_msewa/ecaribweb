package com.payqwikweb.model.web;

public enum Status {

	Inactive("Inactive"), Active("Active"), Deleted("Deleted"), Success(
			"Success"), Failed("Failed"), Blocked("Blocked"), Reversed("Reversed"), Processing("Processing"), Sent("Sent"), Approved("Approved"),Completed("Completed"), Initiated("Initiated"),Refunded("Refunded"),Booked("Booked"),Cancelled("Cancelled");

	private final String value;

	private Status(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return value;
	}

	public String getValue() {
		return value;
	}

	public static Status getEnum(String value) {
		if (value == null)
			throw new IllegalArgumentException();
		for (Status v : values())
			if (value.equalsIgnoreCase(v.getValue()))
				return v;
		throw new IllegalArgumentException();
	}
}
