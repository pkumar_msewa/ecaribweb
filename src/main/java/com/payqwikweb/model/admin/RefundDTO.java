package com.payqwikweb.model.admin;

import com.payqwikweb.app.model.request.SessionDTO;

public class RefundDTO extends SessionDTO{

    private String transactionRefNo;

    public String getTransactionRefNo() {
        return transactionRefNo;
    }

    public void setTransactionRefNo(String transactionRefNo) {
        this.transactionRefNo = transactionRefNo;
    }
}
