package com.payqwikweb.model.admin;


import com.payqwikweb.app.model.request.SessionDTO;

public class FilterDTO extends SessionDTO{

    private String startDate;
    private String endDate;

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}
