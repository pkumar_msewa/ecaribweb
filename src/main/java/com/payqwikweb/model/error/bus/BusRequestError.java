package com.payqwikweb.model.error.bus;

import com.payqwikweb.app.model.ResponseStatus;

public class BusRequestError {

	private ResponseStatus success;
	private String message;
	private String code;
	
	public ResponseStatus getSuccess() {
		return success;
	}
	public void setSuccess(ResponseStatus success) {
		this.success = success;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
}
