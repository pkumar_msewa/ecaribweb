
package com.payqwikweb.model.error;

public class VisaMerchantError {
	
	private boolean valid;
	
	private String mUsername;
	private String firstName;
	private String lastName;
	private String emailAddress;
	private String address1;
	private String address2;
	private String city;
	private String state;
	private String country;
	private String pinCode;
	private String merchantBusinessType;
	private String latitude;
	private String longitude;
	private String isEnabled;
	private String panNo;
	private String mobileNo;
	private String aggregator;

	// merchantBankDetail:
	private String merchantAccountName;
	private String merchantBankName;
	private String merchantAccountNumber;
	private String merhantIfscCode;
	private String merchantBankLoction;

	// settlementBankDetail
	private String settlementAccountName;
	private String settlementBankName;
	private String settlementAccountNumber;
	private String settlementIfscCode;
	private String settlementBankLoction;
	
	public String getmUsername() {
		return mUsername;
	}

	public void setmUsername(String mUsername) {
		this.mUsername = mUsername;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public String getMerchantBusinessType() {
		return merchantBusinessType;
	}

	public void setMerchantBusinessType(String merchantBusinessType) {
		this.merchantBusinessType = merchantBusinessType;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getIsEnabled() {
		return isEnabled;
	}

	public void setIsEnabled(String isEnabled) {
		this.isEnabled = isEnabled;
	}

	public String getPanNo() {
		return panNo;
	}

	public void setPanNo(String panNo) {
		this.panNo = panNo;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getAggregator() {
		return aggregator;
	}

	public void setAggregator(String aggregator) {
		this.aggregator = aggregator;
	}

	public String getMerchantAccountName() {
		return merchantAccountName;
	}

	public void setMerchantAccountName(String merchantAccountName) {
		this.merchantAccountName = merchantAccountName;
	}

	public String getMerchantBankName() {
		return merchantBankName;
	}

	public void setMerchantBankName(String merchantBankName) {
		this.merchantBankName = merchantBankName;
	}

	public String getMerchantAccountNumber() {
		return merchantAccountNumber;
	}

	public void setMerchantAccountNumber(String merchantAccountNumber) {
		this.merchantAccountNumber = merchantAccountNumber;
	}

	public String getMerhantIfscCode() {
		return merhantIfscCode;
	}

	public void setMerhantIfscCode(String merhantIfscCode) {
		this.merhantIfscCode = merhantIfscCode;
	}

	public String getMerchantBankLoction() {
		return merchantBankLoction;
	}

	public void setMerchantBankLoction(String merchantBankLoction) {
		this.merchantBankLoction = merchantBankLoction;
	}

	public String getSettlementAccountName() {
		return settlementAccountName;
	}

	public void setSettlementAccountName(String settlementAccountName) {
		this.settlementAccountName = settlementAccountName;
	}

	public String getSettlementBankName() {
		return settlementBankName;
	}

	public void setSettlementBankName(String settlementBankName) {
		this.settlementBankName = settlementBankName;
	}

	public String getSettlementAccountNumber() {
		return settlementAccountNumber;
	}

	public void setSettlementAccountNumber(String settlementAccountNumber) {
		this.settlementAccountNumber = settlementAccountNumber;
	}

	public String getSettlementIfscCode() {
		return settlementIfscCode;
	}

	public void setSettlementIfscCode(String settlementIfscCode) {
		this.settlementIfscCode = settlementIfscCode;
	}

	public String getSettlementBankLoction() {
		return settlementBankLoction;
	}

	public void setSettlementBankLoction(String settlementBankLoction) {
		this.settlementBankLoction = settlementBankLoction;
	}

	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

}
