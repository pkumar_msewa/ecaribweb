package com.payqwikweb.model.request.thirdpartyService;

public class GetTaxdetailinflight

{

	private String bookingRefNo;
	private String bookingStatus;
	private String provider;

	private String paymentId;
	private String name;
	private String age;
	private String gender;
	private String telePhone;
	private String mobileNo;
	private String emailId;
	private String dob;
	private String psgrtype;
	private String address;
	private String state;
	private String city;
	private String postalCode;
	private String passportDetails;
	private String smsUsageCount;
	private String imagePath;
	private String imagePathRet;
	private String rule;
	private String key;
	private String ruleRet;
	private String keyRet;
	private String flightId;
	private String flightIdRet;
	private String airEquipType;
	private String arrivalAirportCode;
	private String arrivalDateTime;
	private String arrivalDateTimeZone;
	private String departureAirportCode;
	private String departureDateTime;
	private String departureDateTimeZone;
	private String duration;
	private String flightNumber;
	private String operatingAirlineCode;
	private String operatingAirlineFlightNumber;
	private String rPH;
	private String stopQuantity;
	private String airLineName;
	private String airportTax;
	private String imageFileName;
	private String viaFlight;
	private String discount;
	private String airportTaxChild;
	private String airportTaxInfant;
	private String adultTaxBreakup;
	private String childTaxBreakup;
	private String infantTaxBreakup;
	private String ocTaxForFlightSegment;
	private String availability;
	private String resBookDesigCode;
	private String intBIC;
	private String adultFare;
	private String bookingclass;
	private String classType;
	private String fareBasisCode;
	private String ruleForBookingClassFare;
	private String AdultCommission;
	private String childCommission;
	private String commissionOnTCharge;
	private String childFare;
	private String infantFare;
	private String intNumStops;
	private String intOperatingAirlineName;
	private String intArrivalAirportName;
	private String intDepartureAirportName;
	private String intLinkSellAgrmnt;
	private String intConx;
	private String intAirpChg;
	private String intInsideAvailOption;
	private String intGenTrafRestriction;
	private String intDaysOperates;
	private String intJourneyTime;
	private String intEndDate;
	private String intStartTerminal;
	private String intEndTerminal;
	private String intFltTm;
	private String intLSAInd;
	private String intMile;
	private String cabin;
	private String returnFlightSegments;
	private String fareDetails;
	private String bookingDate;
	private String promoCode;
	private String promoCodeAmount;
	private String postMarkup;
	private String ocTax;
	private String actualBaseFare;
	private String tax;
	private String sTax;
	private String sCharge;
	private String tDiscount;
	private String tPartnerCommission;
	private String tCharge;
	private String tMarkup;
	private String tSdiscount;
	private String transactionId;
	private String conveniencefee;
	private String eProductPrice;
	private String actualBaseFareRet;
	private String taxRet;
	private String sTaxRet;
	private String sChargeRet;
	private String tDiscountRet;
	private String tSDiscountRet;
	private String tPartnerCommissionRet;
	private String eProductPriceRet;
	private String tChargeRet;
	private String tMarkupRet;
	private String conveniencefeeRet;
	private String source;
	private String sourceName;
	private String destination;
	private String destinationName;
	private String journeyDate;
	private String returnDate;
	private String tripType;
	private String flightType;
	private String adultPax;
	private String childPax;
	private String infantPax;
	private String travelClass;
	private String isNonStopFlight;
	private String flightTimings;
	private String airlineName;
	private String user;
	private String userType;
	private String isGDS;
	private String intMarketingAirlineCode;

	public String getBookingRefNo() {
		return bookingRefNo;
	}

	public void setBookingRefNo(String bookingRefNo) {
		this.bookingRefNo = bookingRefNo;
	}

	public String getBookingStatus() {
		return bookingStatus;
	}

	public void setBookingStatus(String bookingStatus) {
		this.bookingStatus = bookingStatus;
	}

	public String getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getTelePhone() {
		return telePhone;
	}

	public void setTelePhone(String telePhone) {
		this.telePhone = telePhone;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getPsgrtype() {
		return psgrtype;
	}

	public void setPsgrtype(String psgrtype) {
		this.psgrtype = psgrtype;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getPassportDetails() {
		return passportDetails;
	}

	public void setPassportDetails(String passportDetails) {
		this.passportDetails = passportDetails;
	}

	public String getSmsUsageCount() {
		return smsUsageCount;
	}

	public void setSmsUsageCount(String smsUsageCount) {
		this.smsUsageCount = smsUsageCount;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public String getImagePathRet() {
		return imagePathRet;
	}

	public void setImagePathRet(String imagePathRet) {
		this.imagePathRet = imagePathRet;
	}

	public String getRule() {
		return rule;
	}

	public void setRule(String rule) {
		this.rule = rule;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getRuleRet() {
		return ruleRet;
	}

	public void setRuleRet(String ruleRet) {
		this.ruleRet = ruleRet;
	}

	public String getKeyRet() {
		return keyRet;
	}

	public void setKeyRet(String keyRet) {
		this.keyRet = keyRet;
	}

	public String getFlightId() {
		return flightId;
	}

	public void setFlightId(String flightId) {
		this.flightId = flightId;
	}

	public String getFlightIdRet() {
		return flightIdRet;
	}

	public void setFlightIdRet(String flightIdRet) {
		this.flightIdRet = flightIdRet;
	}

	public String getAirEquipType() {
		return airEquipType;
	}

	public void setAirEquipType(String airEquipType) {
		this.airEquipType = airEquipType;
	}

	public String getArrivalAirportCode() {
		return arrivalAirportCode;
	}

	public void setArrivalAirportCode(String arrivalAirportCode) {
		this.arrivalAirportCode = arrivalAirportCode;
	}

	public String getArrivalDateTime() {
		return arrivalDateTime;
	}

	public void setArrivalDateTime(String arrivalDateTime) {
		this.arrivalDateTime = arrivalDateTime;
	}

	public String getArrivalDateTimeZone() {
		return arrivalDateTimeZone;
	}

	public void setArrivalDateTimeZone(String arrivalDateTimeZone) {
		this.arrivalDateTimeZone = arrivalDateTimeZone;
	}

	public String getDepartureAirportCode() {
		return departureAirportCode;
	}

	public void setDepartureAirportCode(String departureAirportCode) {
		this.departureAirportCode = departureAirportCode;
	}

	public String getDepartureDateTime() {
		return departureDateTime;
	}

	public void setDepartureDateTime(String departureDateTime) {
		this.departureDateTime = departureDateTime;
	}

	public String getDepartureDateTimeZone() {
		return departureDateTimeZone;
	}

	public void setDepartureDateTimeZone(String departureDateTimeZone) {
		this.departureDateTimeZone = departureDateTimeZone;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getOperatingAirlineCode() {
		return operatingAirlineCode;
	}

	public void setOperatingAirlineCode(String operatingAirlineCode) {
		this.operatingAirlineCode = operatingAirlineCode;
	}

	public String getOperatingAirlineFlightNumber() {
		return operatingAirlineFlightNumber;
	}

	public void setOperatingAirlineFlightNumber(String operatingAirlineFlightNumber) {
		this.operatingAirlineFlightNumber = operatingAirlineFlightNumber;
	}

	public String getrPH() {
		return rPH;
	}

	public void setrPH(String rPH) {
		this.rPH = rPH;
	}

	public String getStopQuantity() {
		return stopQuantity;
	}

	public void setStopQuantity(String stopQuantity) {
		this.stopQuantity = stopQuantity;
	}

	public String getAirLineName() {
		return airLineName;
	}

	public void setAirLineName(String airLineName) {
		this.airLineName = airLineName;
	}

	public String getAirportTax() {
		return airportTax;
	}

	public void setAirportTax(String airportTax) {
		this.airportTax = airportTax;
	}

	public String getImageFileName() {
		return imageFileName;
	}

	public void setImageFileName(String imageFileName) {
		this.imageFileName = imageFileName;
	}

	public String getViaFlight() {
		return viaFlight;
	}

	public void setViaFlight(String viaFlight) {
		this.viaFlight = viaFlight;
	}

	public String getDiscount() {
		return discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}

	public String getAirportTaxChild() {
		return airportTaxChild;
	}

	public void setAirportTaxChild(String airportTaxChild) {
		this.airportTaxChild = airportTaxChild;
	}

	public String getAirportTaxInfant() {
		return airportTaxInfant;
	}

	public void setAirportTaxInfant(String airportTaxInfant) {
		this.airportTaxInfant = airportTaxInfant;
	}

	public String getAdultTaxBreakup() {
		return adultTaxBreakup;
	}

	public void setAdultTaxBreakup(String adultTaxBreakup) {
		this.adultTaxBreakup = adultTaxBreakup;
	}

	public String getChildTaxBreakup() {
		return childTaxBreakup;
	}

	public void setChildTaxBreakup(String childTaxBreakup) {
		this.childTaxBreakup = childTaxBreakup;
	}

	public String getInfantTaxBreakup() {
		return infantTaxBreakup;
	}

	public void setInfantTaxBreakup(String infantTaxBreakup) {
		this.infantTaxBreakup = infantTaxBreakup;
	}

	public String getOcTaxForFlightSegment() {
		return ocTaxForFlightSegment;
	}

	public void setOcTaxForFlightSegment(String ocTaxForFlightSegment) {
		this.ocTaxForFlightSegment = ocTaxForFlightSegment;
	}

	public String getAvailability() {
		return availability;
	}

	public void setAvailability(String availability) {
		this.availability = availability;
	}

	public String getResBookDesigCode() {
		return resBookDesigCode;
	}

	public void setResBookDesigCode(String resBookDesigCode) {
		this.resBookDesigCode = resBookDesigCode;
	}

	public String getIntBIC() {
		return intBIC;
	}

	public void setIntBIC(String intBIC) {
		this.intBIC = intBIC;
	}

	public String getAdultFare() {
		return adultFare;
	}

	public void setAdultFare(String adultFare) {
		this.adultFare = adultFare;
	}

	public String getBookingclass() {
		return bookingclass;
	}

	public void setBookingclass(String bookingclass) {
		this.bookingclass = bookingclass;
	}

	public String getClassType() {
		return classType;
	}

	public void setClassType(String classType) {
		this.classType = classType;
	}

	public String getFareBasisCode() {
		return fareBasisCode;
	}

	public void setFareBasisCode(String fareBasisCode) {
		this.fareBasisCode = fareBasisCode;
	}

	public String getRuleForBookingClassFare() {
		return ruleForBookingClassFare;
	}

	public void setRuleForBookingClassFare(String ruleForBookingClassFare) {
		this.ruleForBookingClassFare = ruleForBookingClassFare;
	}

	public String getAdultCommission() {
		return AdultCommission;
	}

	public void setAdultCommission(String adultCommission) {
		AdultCommission = adultCommission;
	}

	public String getChildCommission() {
		return childCommission;
	}

	public void setChildCommission(String childCommission) {
		this.childCommission = childCommission;
	}

	public String getCommissionOnTCharge() {
		return commissionOnTCharge;
	}

	public void setCommissionOnTCharge(String commissionOnTCharge) {
		this.commissionOnTCharge = commissionOnTCharge;
	}

	public String getChildFare() {
		return childFare;
	}

	public void setChildFare(String childFare) {
		this.childFare = childFare;
	}

	public String getInfantFare() {
		return infantFare;
	}

	public void setInfantFare(String infantFare) {
		this.infantFare = infantFare;
	}

	public String getIntNumStops() {
		return intNumStops;
	}

	public void setIntNumStops(String intNumStops) {
		this.intNumStops = intNumStops;
	}

	public String getIntOperatingAirlineName() {
		return intOperatingAirlineName;
	}

	public void setIntOperatingAirlineName(String intOperatingAirlineName) {
		this.intOperatingAirlineName = intOperatingAirlineName;
	}

	public String getIntArrivalAirportName() {
		return intArrivalAirportName;
	}

	public void setIntArrivalAirportName(String intArrivalAirportName) {
		this.intArrivalAirportName = intArrivalAirportName;
	}

	public String getIntDepartureAirportName() {
		return intDepartureAirportName;
	}

	public void setIntDepartureAirportName(String intDepartureAirportName) {
		this.intDepartureAirportName = intDepartureAirportName;
	}

	public String getIntLinkSellAgrmnt() {
		return intLinkSellAgrmnt;
	}

	public void setIntLinkSellAgrmnt(String intLinkSellAgrmnt) {
		this.intLinkSellAgrmnt = intLinkSellAgrmnt;
	}

	public String getIntConx() {
		return intConx;
	}

	public void setIntConx(String intConx) {
		this.intConx = intConx;
	}

	public String getIntAirpChg() {
		return intAirpChg;
	}

	public void setIntAirpChg(String intAirpChg) {
		this.intAirpChg = intAirpChg;
	}

	public String getIntInsideAvailOption() {
		return intInsideAvailOption;
	}

	public void setIntInsideAvailOption(String intInsideAvailOption) {
		this.intInsideAvailOption = intInsideAvailOption;
	}

	public String getIntGenTrafRestriction() {
		return intGenTrafRestriction;
	}

	public void setIntGenTrafRestriction(String intGenTrafRestriction) {
		this.intGenTrafRestriction = intGenTrafRestriction;
	}

	public String getIntDaysOperates() {
		return intDaysOperates;
	}

	public void setIntDaysOperates(String intDaysOperates) {
		this.intDaysOperates = intDaysOperates;
	}

	public String getIntJourneyTime() {
		return intJourneyTime;
	}

	public void setIntJourneyTime(String intJourneyTime) {
		this.intJourneyTime = intJourneyTime;
	}

	public String getIntEndDate() {
		return intEndDate;
	}

	public void setIntEndDate(String intEndDate) {
		this.intEndDate = intEndDate;
	}

	public String getIntStartTerminal() {
		return intStartTerminal;
	}

	public void setIntStartTerminal(String intStartTerminal) {
		this.intStartTerminal = intStartTerminal;
	}

	public String getIntEndTerminal() {
		return intEndTerminal;
	}

	public void setIntEndTerminal(String intEndTerminal) {
		this.intEndTerminal = intEndTerminal;
	}

	public String getIntFltTm() {
		return intFltTm;
	}

	public void setIntFltTm(String intFltTm) {
		this.intFltTm = intFltTm;
	}

	public String getIntLSAInd() {
		return intLSAInd;
	}

	public void setIntLSAInd(String intLSAInd) {
		this.intLSAInd = intLSAInd;
	}

	public String getIntMile() {
		return intMile;
	}

	public void setIntMile(String intMile) {
		this.intMile = intMile;
	}

	public String getCabin() {
		return cabin;
	}

	public void setCabin(String cabin) {
		this.cabin = cabin;
	}

	public String getReturnFlightSegments() {
		return returnFlightSegments;
	}

	public void setReturnFlightSegments(String returnFlightSegments) {
		this.returnFlightSegments = returnFlightSegments;
	}

	public String getFareDetails() {
		return fareDetails;
	}

	public void setFareDetails(String fareDetails) {
		this.fareDetails = fareDetails;
	}

	public String getBookingDate() {
		return bookingDate;
	}

	public void setBookingDate(String bookingDate) {
		this.bookingDate = bookingDate;
	}

	public String getPromoCode() {
		return promoCode;
	}

	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}

	public String getPromoCodeAmount() {
		return promoCodeAmount;
	}

	public void setPromoCodeAmount(String promoCodeAmount) {
		this.promoCodeAmount = promoCodeAmount;
	}

	public String getPostMarkup() {
		return postMarkup;
	}

	public void setPostMarkup(String postMarkup) {
		this.postMarkup = postMarkup;
	}

	public String getOcTax() {
		return ocTax;
	}

	public void setOcTax(String ocTax) {
		this.ocTax = ocTax;
	}

	public String getActualBaseFare() {
		return actualBaseFare;
	}

	public void setActualBaseFare(String actualBaseFare) {
		this.actualBaseFare = actualBaseFare;
	}

	public String getTax() {
		return tax;
	}

	public void setTax(String tax) {
		this.tax = tax;
	}

	public String getsTax() {
		return sTax;
	}

	public void setsTax(String sTax) {
		this.sTax = sTax;
	}

	public String getsCharge() {
		return sCharge;
	}

	public void setsCharge(String sCharge) {
		this.sCharge = sCharge;
	}

	public String gettDiscount() {
		return tDiscount;
	}

	public void settDiscount(String tDiscount) {
		this.tDiscount = tDiscount;
	}

	public String gettPartnerCommission() {
		return tPartnerCommission;
	}

	public void settPartnerCommission(String tPartnerCommission) {
		this.tPartnerCommission = tPartnerCommission;
	}

	public String gettCharge() {
		return tCharge;
	}

	public void settCharge(String tCharge) {
		this.tCharge = tCharge;
	}

	public String gettMarkup() {
		return tMarkup;
	}

	public void settMarkup(String tMarkup) {
		this.tMarkup = tMarkup;
	}

	public String gettSdiscount() {
		return tSdiscount;
	}

	public void settSdiscount(String tSdiscount) {
		this.tSdiscount = tSdiscount;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getConveniencefee() {
		return conveniencefee;
	}

	public void setConveniencefee(String conveniencefee) {
		this.conveniencefee = conveniencefee;
	}

	public String geteProductPrice() {
		return eProductPrice;
	}

	public void seteProductPrice(String eProductPrice) {
		this.eProductPrice = eProductPrice;
	}

	public String getActualBaseFareRet() {
		return actualBaseFareRet;
	}

	public void setActualBaseFareRet(String actualBaseFareRet) {
		this.actualBaseFareRet = actualBaseFareRet;
	}

	public String getTaxRet() {
		return taxRet;
	}

	public void setTaxRet(String taxRet) {
		this.taxRet = taxRet;
	}

	public String getsTaxRet() {
		return sTaxRet;
	}

	public void setsTaxRet(String sTaxRet) {
		this.sTaxRet = sTaxRet;
	}

	public String getsChargeRet() {
		return sChargeRet;
	}

	public void setsChargeRet(String sChargeRet) {
		this.sChargeRet = sChargeRet;
	}

	public String gettDiscountRet() {
		return tDiscountRet;
	}

	public void settDiscountRet(String tDiscountRet) {
		this.tDiscountRet = tDiscountRet;
	}

	public String gettSDiscountRet() {
		return tSDiscountRet;
	}

	public void settSDiscountRet(String tSDiscountRet) {
		this.tSDiscountRet = tSDiscountRet;
	}

	public String gettPartnerCommissionRet() {
		return tPartnerCommissionRet;
	}

	public void settPartnerCommissionRet(String tPartnerCommissionRet) {
		this.tPartnerCommissionRet = tPartnerCommissionRet;
	}

	public String geteProductPriceRet() {
		return eProductPriceRet;
	}

	public void seteProductPriceRet(String eProductPriceRet) {
		this.eProductPriceRet = eProductPriceRet;
	}

	public String gettChargeRet() {
		return tChargeRet;
	}

	public void settChargeRet(String tChargeRet) {
		this.tChargeRet = tChargeRet;
	}

	public String gettMarkupRet() {
		return tMarkupRet;
	}

	public void settMarkupRet(String tMarkupRet) {
		this.tMarkupRet = tMarkupRet;
	}

	public String getConveniencefeeRet() {
		return conveniencefeeRet;
	}

	public void setConveniencefeeRet(String conveniencefeeRet) {
		this.conveniencefeeRet = conveniencefeeRet;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getSourceName() {
		return sourceName;
	}

	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getDestinationName() {
		return destinationName;
	}

	public void setDestinationName(String destinationName) {
		this.destinationName = destinationName;
	}

	public String getJourneyDate() {
		return journeyDate;
	}

	public void setJourneyDate(String journeyDate) {
		this.journeyDate = journeyDate;
	}

	public String getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(String returnDate) {
		this.returnDate = returnDate;
	}

	public String getTripType() {
		return tripType;
	}

	public void setTripType(String tripType) {
		this.tripType = tripType;
	}

	public String getFlightType() {
		return flightType;
	}

	public void setFlightType(String flightType) {
		this.flightType = flightType;
	}

	public String getAdultPax() {
		return adultPax;
	}

	public void setAdultPax(String adultPax) {
		this.adultPax = adultPax;
	}

	public String getChildPax() {
		return childPax;
	}

	public void setChildPax(String childPax) {
		this.childPax = childPax;
	}

	public String getInfantPax() {
		return infantPax;
	}

	public void setInfantPax(String infantPax) {
		this.infantPax = infantPax;
	}

	public String getTravelClass() {
		return travelClass;
	}

	public void setTravelClass(String travelClass) {
		this.travelClass = travelClass;
	}

	public String getIsNonStopFlight() {
		return isNonStopFlight;
	}

	public void setIsNonStopFlight(String isNonStopFlight) {
		this.isNonStopFlight = isNonStopFlight;
	}

	public String getFlightTimings() {
		return flightTimings;
	}

	public void setFlightTimings(String flightTimings) {
		this.flightTimings = flightTimings;
	}

	public String getAirlineName() {
		return airlineName;
	}

	public void setAirlineName(String airlineName) {
		this.airlineName = airlineName;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getIsGDS() {
		return isGDS;
	}

	public void setIsGDS(String isGDS) {
		this.isGDS = isGDS;
	}

	public String getIntMarketingAirlineCode() {
		return intMarketingAirlineCode;
	}

	public void setIntMarketingAirlineCode(String intMarketingAirlineCode) {
		this.intMarketingAirlineCode = intMarketingAirlineCode;
	}

}