package com.payqwikweb.model.request.thirdpartyService;

public class GetonewayflightRequest
{
	private String citysourceonewayflight;

	private String citydestonewayflight;

	private String date;

	private String tripType;

	private String flightType;

	private String numberofadult;
	
	private String numberofchildren;

	private String numberofinfrant;

	private String travelClass;

	private String userType;

	private String returnDate;

	private String user;

	public String getCitysourceonewayflight() {
		return citysourceonewayflight;
	}

	public void setCitysourceonewayflight(String citysourceonewayflight) {
		this.citysourceonewayflight = citysourceonewayflight;
	}

	public String getCitydestonewayflight() {
		return citydestonewayflight;
	}

	public void setCitydestonewayflight(String citydestonewayflight) {
		this.citydestonewayflight = citydestonewayflight;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTripType() {
		return tripType;
	}

	public void setTripType(String tripType) {
		this.tripType = tripType;
	}

	public String getFlightType() {
		return flightType;
	}

	public void setFlightType(String flightType) {
		this.flightType = flightType;
	}

	public String getNumberofadult() {
		return numberofadult;
	}

	public void setNumberofadult(String numberofadult) {
		this.numberofadult = numberofadult;
	}

	public String getNumberofchildren() {
		return numberofchildren;
	}

	public void setNumberofchildren(String numberofchildren) {
		this.numberofchildren = numberofchildren;
	}

	public String getNumberofinfrant() {
		return numberofinfrant;
	}

	public void setNumberofinfrant(String numberofinfrant) {
		this.numberofinfrant = numberofinfrant;
	}

	public String getTravelClass() {
		return travelClass;
	}

	public void setTravelClass(String travelClass) {
		this.travelClass = travelClass;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(String returnDate) {
		this.returnDate = returnDate;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}



	
}
