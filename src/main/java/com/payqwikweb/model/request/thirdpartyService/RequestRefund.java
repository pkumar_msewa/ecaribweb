package com.payqwikweb.model.request.thirdpartyService;

import java.io.StringWriter;
import java.util.List;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonWriter;

import org.springframework.web.multipart.MultipartFile;

import com.payqwikweb.app.model.request.SendMoneyMobileDTO;
import com.thirdparty.model.JSONRequest;

public class RequestRefund implements JSONRequest {

	private MultipartFile file;
	private MultipartFile otherFile;
	private String fileName;
	private List<SendMoneyMobileDTO> mobileDTO;
	private String sessionId;

	public List<SendMoneyMobileDTO> getMobileDTO() {
		return mobileDTO;
	}

	public void setMobileDTO(List<SendMoneyMobileDTO> mobileDTO) {
		this.mobileDTO = mobileDTO;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public MultipartFile getOtherFile() {
		return otherFile;
	}

	public void setOtherFile(MultipartFile otherFile) {
		this.otherFile = otherFile;
	}

	@Override
	public String getJsonRequest() {
		JsonObjectBuilder jsonBuilder = Json.createObjectBuilder();
		JsonArrayBuilder dtoList = Json.createArrayBuilder();
		JsonObjectBuilder mobileDTOs = Json.createObjectBuilder();
		for (int j = 0; j < getMobileDTO().size(); j++) {
			mobileDTOs.add("mobileNumber", getMobileDTO().get(j).getMobileNumber());
			mobileDTOs.add("amount", getMobileDTO().get(j).getAmount());
			mobileDTOs.add("message", getMobileDTO().get(j).getMessage());
			dtoList.add(mobileDTOs.build());
		}
		jsonBuilder.add("sessionId", getSessionId());
		jsonBuilder.add("mobileDTO", dtoList);

		JsonObject empObj = jsonBuilder.build();

		StringWriter jsnReqStr = new StringWriter();
		JsonWriter jsonWtr = Json.createWriter(jsnReqStr);
		jsonWtr.writeObject(empObj);
		jsonWtr.close();
		return jsnReqStr.toString();
	}

}
