package com.payqwikweb.model.request.thirdpartyService;

import org.springframework.web.bind.annotation.RequestParam;

public class GetbusbookingetailRequest
{
	String tripid;
	String porvidr;
	String travels;
	String sourceId;
	String destinationId;
	String journeyDate;
	
	 String tripType;
	
	String userType;

	public String getTripid() {
		return tripid;
	}

	public void setTripid(String tripid) {
		this.tripid = tripid;
	}

	public String getPorvidr() {
		return porvidr;
	}

	public void setPorvidr(String porvidr) {
		this.porvidr = porvidr;
	}

	public String getTravels() {
		return travels;
	}

	public void setTravels(String travels) {
		this.travels = travels;
	}

	public String getSourceId() {
		return sourceId;
	}

	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}

	public String getDestinationId() {
		return destinationId;
	}

	public void setDestinationId(String destinationId) {
		this.destinationId = destinationId;
	}

	public String getJourneyDate() {
		return journeyDate;
	}

	public void setJourneyDate(String journeyDate) {
		this.journeyDate = journeyDate;
	}

	public String getTripType() {
		return tripType;
	}

	public void setTripType(String tripType) {
		this.tripType = tripType;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}
	

}
