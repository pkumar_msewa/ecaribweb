package com.payqwikweb.model.request.thirdpartyService;

public class CitiesRequest {

	
	private String cities;
	
	private String consumerKey;
	private String consumerSecret;
	
	public String getConsumerKey() {
		return consumerKey;
	}

	public void setConsumerKey(String consumerKey) {
		this.consumerKey = consumerKey;
	}

	public String getConsumerSecret() {
		return consumerSecret;
	}
	public void setConsumerSecret(String consumerSecret) {
		this.consumerSecret = consumerSecret;
	}
	public String getCities() {
		return cities;
	}

	public void setCities(String cities) {
		this.cities = cities;
	}
	
	
}
