package com.payqwikweb.model.request.thirdpartyService;

public class BlockBusRequest 
{

	private String transctionrefno;
	

	private String sessionid;
	private String tripId;
	private String boardingId;
	private String noofSeats;
	private String fares;
	private String convenienceFee;
	private String user_boisrindpointsbusdeatil;
	public String getUser_boisrindpointsbusdeatil() {
		return user_boisrindpointsbusdeatil;
	}

	public void setUser_boisrindpointsbusdeatil(String user_boisrindpointsbusdeatil) {
		this.user_boisrindpointsbusdeatil = user_boisrindpointsbusdeatil;
	}

	private String seatNos;
	private String titles;
	private String names;
	private String ages;
	private String genders;
	private String address;
	private String postalCode;
	private String idCardType;
	private String idCardNo;
	private String idCardIssuedBy;
	private String mobileNo;
	private String emergencyMobileNo;
	private String emailId;
	private String provider;
	private String operator;
	private String partialCancellationAllowed;
	private String boardingPointDetails;
	private String busTypeName;
	private String departureTime;
	private String cancellationPolicy;
	private String sourceId;
	private String sourceName;
	private String destinationId;
	private String destinationName;
	private String journeyDate;
	private String tripType;
	private String busType;
	private String user;
	private String userType;
	private String serviceTax;
	private String serviceCharge;
	private String consumerKey;
	private String consumerSecret;
	
	public String getTotalamount() {
		return totalamount;
	}

	public void setTotalamount(String totalamount) {
		this.totalamount = totalamount;
	}

	private String totalamount;
	
	
	public String getConsumerKey() {
		return consumerKey;
	}

	public void setConsumerKey(String consumerKey) {
		this.consumerKey = consumerKey;
	}

	public String getConsumerSecret() {
		return consumerSecret;
	}

	public void setConsumerSecret(String consumerSecret) {
		this.consumerSecret = consumerSecret;
	}


	public String getTripId() {
		return tripId;
	}

	public void setTripId(String tripId) {
		this.tripId = tripId;
	}

	public String getBoardingId() {
		return boardingId;
	}

	public void setBoardingId(String boardingId) {
		this.boardingId = boardingId;
	}

	public String getNoofSeats() {
		return noofSeats;
	}

	public void setNoofSeats(String noofSeats) {
		this.noofSeats = noofSeats;
	}

	public String getFares() {
		return fares;
	}

	public void setFares(String fares) {
		this.fares = fares;
	}

	public String getConvenienceFee() {
		return convenienceFee;
	}

	public void setConvenienceFee(String convenienceFee) {
		this.convenienceFee = convenienceFee;
	}

	public String getSeatNos() {
		return seatNos;
	}

	public void setSeatNos(String seatNos) {
		this.seatNos = seatNos;
	}

	public String getTitles() {
		return titles;
	}

	public void setTitles(String titles) {
		this.titles = titles;
	}

	public String getNames() {
		return names;
	}

	public void setNames(String names) {
		this.names = names;
	}

	public String getAges() {
		return ages;
	}

	public void setAges(String ages) {
		this.ages = ages;
	}

	public String getGenders() {
		return genders;
	}

	public void setGenders(String genders) {
		this.genders = genders;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	
	public String getIdCardType() {
		return idCardType;
	}

	public void setIdCardType(String idCardType) {
		this.idCardType = idCardType;
	}

	public String getIdCardNo() {
		return idCardNo;
	}

	public void setIdCardNo(String idCardNo) {
		this.idCardNo = idCardNo;
	}

	public String getIdCardIssuedBy() {
		return idCardIssuedBy;
	}

	public void setIdCardIssuedBy(String idCardIssuedBy) {
		this.idCardIssuedBy = idCardIssuedBy;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getEmergencyMobileNo() {
		return emergencyMobileNo;
	}

	public void setEmergencyMobileNo(String emergencyMobileNo) {
		this.emergencyMobileNo = emergencyMobileNo;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getSessionid() {
		return sessionid;
	}

	public void setSessionid(String sessionid) {
		this.sessionid = sessionid;
	}

	public String getPartialCancellationAllowed() {
		return partialCancellationAllowed;
	}

	public void setPartialCancellationAllowed(String partialCancellationAllowed) {
		this.partialCancellationAllowed = partialCancellationAllowed;
	}

	public String getBoardingPointDetails() {
		return boardingPointDetails;
	}

	public void setBoardingPointDetails(String boardingPointDetails) {
		this.boardingPointDetails = boardingPointDetails;
	}

	public String getBusTypeName() {
		return busTypeName;
	}

	public void setBusTypeName(String busTypeName) {
		this.busTypeName = busTypeName;
	}

	public String getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}

	public String getCancellationPolicy() {
		return cancellationPolicy;
	}

	public void setCancellationPolicy(String cancellationPolicy) {
		this.cancellationPolicy = cancellationPolicy;
	}

	public String getSourceId() {
		return sourceId;
	}

	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}

	public String getSourceName() {
		return sourceName;
	}

	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	public String getDestinationId() {
		return destinationId;
	}

	public void setDestinationId(String destinationId) {
		this.destinationId = destinationId;
	}

	public String getDestinationName() {
		return destinationName;
	}

	public void setDestinationName(String destinationName) {
		this.destinationName = destinationName;
	}

	public String getJourneyDate() {
		return journeyDate;
	}

	public void setJourneyDate(String journeyDate) {
		this.journeyDate = journeyDate;
	}

	public String getTripType() {
		return tripType;
	}

	public void setTripType(String tripType) {
		this.tripType = tripType;
	}

	public String getBusType() {
		return busType;
	}

	public void setBusType(String busType) {
		this.busType = busType;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getServiceTax() {
		return serviceTax;
	}

	public void setServiceTax(String serviceTax) {
		this.serviceTax = serviceTax;
	}

	public String getServiceCharge() {
		return serviceCharge;
	}

	public void setServiceCharge(String serviceCharge) {
		this.serviceCharge = serviceCharge;
	}
	public String getTransctionrefno() {
		return transctionrefno;
	}

	public void setTransctionrefno(String transctionrefno) {
		this.transctionrefno = transctionrefno;
	}

}
