package com.payqwikweb.model.request.thirdpartyService;

public class CancelBusTicketRequest {

	private String referenceNo;
	private String seatNos;
	private String emailId;
	private String consumerKey;
	private String consumerSecret;
	
	public String getConsumerKey() {
		return consumerKey;
	}

	public void setConsumerKey(String consumerKey) {
		this.consumerKey = consumerKey;
	}

	public String getConsumerSecret() {
		return consumerSecret;
	}

	public void setConsumerSecret(String consumerSecret) {
		this.consumerSecret = consumerSecret;
	}
	
	public String getReferenceNo() {
		return referenceNo;
	}
	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}
	public String getSeatNos() {
		return seatNos;
	}
	public void setSeatNos(String seatNos) {
		this.seatNos = seatNos;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	
	
}
