package com.payqwikweb.model.request.thirdpartyService;

public class Gettaxdetailinternationalroundway {

	private String bookingRefNo;
	private String bookingStatus;
	private String provider;
	private String paymentId;
	private String name;
	private String age;
	private String gender;
	private String telePhone;
	private String mobileNo;
	private String emailId;
	private String dob;
	private String lanme;
	private String psgrtype;
	private String address;
	private String state;
	private String city;
	private String postalCode;
	private String passportDetails;
	private String smsUsageCount;
	private String imagePath;
	private String imagePathRet;
	private String rule;
	private String key;
	private String ruleRet;
	private String keyRet;
	private String flightId;
	private String flightIdRet;
	private String airEquipType;
	private String arrivalAirportCode;
	private String arrivalDateTime;
	private String arrivalDateTimeZone;
	private String departureAirportCode;
	private String departureDateTime;
	private String departureDateTimeZone;
	private String duration;
	private String flightNumber;
	private String operatingAirlineCode;
	private String operatingAirlineFlightNumber;
	private String rPH;
	private String stopQuantity;
	private String airLineName;
	private String airportTax;
	private String imageFileName;
	private String viaFlight;
	private String discount;
	private String airportTaxChild;
	private String airportTaxInfant;
	private String adultTaxBreakup;
	private String childTaxBreakup;
	private String infantTaxBreakup;
	private String ocTaxForFlightSegment;
	private String availability;
	private String resBookDesigCode;
	private String intBIC;
	private String adultFare;
	private String bookingclass;
	private String classType;
	private String fareBasisCode;
	private String ruleForBookingClassFare;
	private String AdultCommission;
	private String childCommission;
	private String commissionOnTCharge;
	private String childFare;
	private String infantFare;
	private String intNumStops;
	private String intOperatingAirlineName;
	private String intArrivalAirportName;
	private String intDepartureAirportName;
	private String intLinkSellAgrmnt;
	private String intConx;
	private String intAirpChg;
	private String intInsideAvailOption;
	private String intGenTrafRestriction;
	private String intDaysOperates;
	private String intJourneyTime;
	private String intEndDate;
	private String intStartTerminal;
	private String intEndTerminal;
	private String intFltTm;
	private String intLSAInd;
	private String intMile;
	private String cabin;
	private String returnFlightSegments;
	private String fareDetails;
	private String bookingDate;
	private String promoCode;
	private String promoCodeAmount;
	private String postMarkup;
	private String ocTax;
	private String actualBaseFare;
	private String tax;
	private String sTax;
	private String sCharge;
	private String tDiscount;
	private String tPartnerCommission;
	private String tCharge;
	private String tMarkup;
	private String tSdiscount;
	private String transactionId;
	private String conveniencefee;
	private String eProductPrice;
	private String actualBaseFareRet;
	private String taxRet;
	private String sTaxRet;
	private String sChargeRet;
	private String tDiscountRet;
	private String tSDiscountRet;
	private String tPartnerCommissionRet;
	private String eProductPriceRet;
	private String tChargeRet;
	private String tMarkupRet;
	private String conveniencefeeRet;
	private String source;
	private String sourceName;
	private String destination;
	private String destinationName;
	private String journeyDate;
	private String returnDate;
	private String tripType;
	private String flightType;
	private String adultPax;
	private String childPax;
	private String infantPax;
	private String travelClass;
	private String isNonStopFlight;
	private String flightTimings;
	private String airlineName;
	private String user;
	private String userType;
	private String isGDS;
	private String intMarketingAirlineCode;

	private String stax;

	private String fareBreakUp;
	private String totalFare;
	
	
	private String sessionid;
	
	
	private String returnbookingRefNo;
	private String returnbookingStatus;
	private String returnprovider;
	private String returnpaymentId;
	
	private String returnsmsUsageCount;
	private String returnimagePath;
	private String returnimagePathRet;
	private String returnrule;
	private String returnkey;
	private String returnruleRet;
	private String returnkeyRet;
	private String returnflightId;
	private String returnflightIdRet;
	private String returnairEquipType;
	private String returnarrivalAirportCode;
	private String returnarrivalDateTime;
	private String returnarrivalDateTimeZone;
	private String returndepartureAirportCode;
	private String returndepartureDateTime;
	private String returndepartureDateTimeZone;
	private String returnduration;
	private String returnflightNumber;
	private String returnoperatingAirlineCode;
	private String returnoperatingAirlineFlightNumber;
	private String returnrPH;
	private String returnstopQuantity;
	private String returnairLineName;
	private String returnairportTax;
	private String returnimageFileName;
	private String returnviaFlight;
	private String returndiscount;
	private String returnairportTaxChild;
	private String returnairportTaxInfant;
	private String returnadultTaxBreakup;
	private String returnchildTaxBreakup;
	private String returninfantTaxBreakup;
	private String returnocTaxForFlightSegment;
	private String returnavailability;
	private String returnresBookDesigCode;
	private String returnintBIC;
	private String returnadultFare;
	private String returnbookingclass;
	private String returnclassType;
	private String returnfareBasisCode;
	private String returnruleForBookingClassFare;
	private String returnadultCommission;
	private String returnchildCommission;
	private String returncommissionOnTCharge;
	private String returnchildFare;
	private String returninfantFare;
	private String returnintNumStops;
	private String returnintOperatingAirlineName;
	private String returnintArrivalAirportName;
	private String returnintDepartureAirportName;
	private String returnintLinkSellAgrmnt;
	private String returnintConx;
	private String returnintAirpChg;
	private String returnintInsideAvailOption;
	private String returnintGenTrafRestriction;
	private String returnintDaysOperates;
	private String returnintJourneyTime;
	private String returnintEndDate;
	private String returnintStartTerminal;
	private String returnintEndTerminal;
	private String returnintFltTm;
	private String returnintLSAInd;
	private String returnintMile;
	private String returncabin;
	private String returnreturnFlightSegments;
	private String returnfareDetails;
	private String returnbookingDate;
	private String returnpromoCode;
	private String returnpromoCodeAmount;
	private String returnpostMarkup;
	private String returnocTax;
	private String returnactualBaseFare;
	private String returntax;
	private String returnsTax;
	private String returnsCharge;
	private String returntDiscount;
	private String returntPartnerCommission;
	private String returntCharge;
	private String returntMarkup;
	private String returntSdiscount;
	private String returntransactionId;
	private String returnconveniencefee;
	private String returneProductPrice;
	private String returnactualBaseFareRet;
	private String returntaxRet;
	private String returnsTaxRet;
	private String returnsChargeRet;
	private String returntDiscountRet;
	private String returntSDiscountRet;
	private String returntPartnerCommissionRet;
	private String returneProductPriceRet;
	private String returntChargeRet;
	private String returntMarkupRet;
	private String returnconveniencefeeRet;
	private String returnsource;
	private String returnsourceName;
	private String returndestination;
	private String returndestinationName;
	private String returnjourneyDate;
	private String returnreturnDate;
	private String returntripType;
	private String returnflightType;
	private String returnadultPax;
	private String returnchildPax;
	private String returninfantPax;
	private String returntravelClass;
	private String returnisNonStopFlight;
	private String returnflightTimings;
	private String returnairlineName;
	private String returnuser;
	private String returnuserType;
	private String returnisGDS;
	private String returnintMarketingAirlineCode;

	private String returnstax;

	private String returnfareBreakUp;
	private String returntotalFare;
	public String getBookingRefNo() {
		return bookingRefNo;
	}
	public void setBookingRefNo(String bookingRefNo) {
		this.bookingRefNo = bookingRefNo;
	}
	public String getBookingStatus() {
		return bookingStatus;
	}
	public void setBookingStatus(String bookingStatus) {
		this.bookingStatus = bookingStatus;
	}
	public String getProvider() {
		return provider;
	}
	public void setProvider(String provider) {
		this.provider = provider;
	}
	public String getPaymentId() {
		return paymentId;
	}
	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getTelePhone() {
		return telePhone;
	}
	public void setTelePhone(String telePhone) {
		this.telePhone = telePhone;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getLanme() {
		return lanme;
	}
	public void setLanme(String lanme) {
		this.lanme = lanme;
	}
	public String getPsgrtype() {
		return psgrtype;
	}
	public void setPsgrtype(String psgrtype) {
		this.psgrtype = psgrtype;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public String getPassportDetails() {
		return passportDetails;
	}
	public void setPassportDetails(String passportDetails) {
		this.passportDetails = passportDetails;
	}
	public String getSmsUsageCount() {
		return smsUsageCount;
	}
	public void setSmsUsageCount(String smsUsageCount) {
		this.smsUsageCount = smsUsageCount;
	}
	public String getImagePath() {
		return imagePath;
	}
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	public String getImagePathRet() {
		return imagePathRet;
	}
	public void setImagePathRet(String imagePathRet) {
		this.imagePathRet = imagePathRet;
	}
	public String getRule() {
		return rule;
	}
	public void setRule(String rule) {
		this.rule = rule;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getRuleRet() {
		return ruleRet;
	}
	public void setRuleRet(String ruleRet) {
		this.ruleRet = ruleRet;
	}
	public String getKeyRet() {
		return keyRet;
	}
	public void setKeyRet(String keyRet) {
		this.keyRet = keyRet;
	}
	public String getFlightId() {
		return flightId;
	}
	public void setFlightId(String flightId) {
		this.flightId = flightId;
	}
	public String getFlightIdRet() {
		return flightIdRet;
	}
	public void setFlightIdRet(String flightIdRet) {
		this.flightIdRet = flightIdRet;
	}
	public String getAirEquipType() {
		return airEquipType;
	}
	public void setAirEquipType(String airEquipType) {
		this.airEquipType = airEquipType;
	}
	public String getArrivalAirportCode() {
		return arrivalAirportCode;
	}
	public void setArrivalAirportCode(String arrivalAirportCode) {
		this.arrivalAirportCode = arrivalAirportCode;
	}
	public String getArrivalDateTime() {
		return arrivalDateTime;
	}
	public void setArrivalDateTime(String arrivalDateTime) {
		this.arrivalDateTime = arrivalDateTime;
	}
	public String getArrivalDateTimeZone() {
		return arrivalDateTimeZone;
	}
	public void setArrivalDateTimeZone(String arrivalDateTimeZone) {
		this.arrivalDateTimeZone = arrivalDateTimeZone;
	}
	public String getDepartureAirportCode() {
		return departureAirportCode;
	}
	public void setDepartureAirportCode(String departureAirportCode) {
		this.departureAirportCode = departureAirportCode;
	}
	public String getDepartureDateTime() {
		return departureDateTime;
	}
	public void setDepartureDateTime(String departureDateTime) {
		this.departureDateTime = departureDateTime;
	}
	public String getDepartureDateTimeZone() {
		return departureDateTimeZone;
	}
	public void setDepartureDateTimeZone(String departureDateTimeZone) {
		this.departureDateTimeZone = departureDateTimeZone;
	}
	public String getDuration() {
		return duration;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}
	public String getFlightNumber() {
		return flightNumber;
	}
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}
	public String getOperatingAirlineCode() {
		return operatingAirlineCode;
	}
	public void setOperatingAirlineCode(String operatingAirlineCode) {
		this.operatingAirlineCode = operatingAirlineCode;
	}
	public String getOperatingAirlineFlightNumber() {
		return operatingAirlineFlightNumber;
	}
	public void setOperatingAirlineFlightNumber(String operatingAirlineFlightNumber) {
		this.operatingAirlineFlightNumber = operatingAirlineFlightNumber;
	}
	public String getrPH() {
		return rPH;
	}
	public void setrPH(String rPH) {
		this.rPH = rPH;
	}
	public String getStopQuantity() {
		return stopQuantity;
	}
	public void setStopQuantity(String stopQuantity) {
		this.stopQuantity = stopQuantity;
	}
	public String getAirLineName() {
		return airLineName;
	}
	public void setAirLineName(String airLineName) {
		this.airLineName = airLineName;
	}
	public String getAirportTax() {
		return airportTax;
	}
	public void setAirportTax(String airportTax) {
		this.airportTax = airportTax;
	}
	public String getImageFileName() {
		return imageFileName;
	}
	public void setImageFileName(String imageFileName) {
		this.imageFileName = imageFileName;
	}
	public String getViaFlight() {
		return viaFlight;
	}
	public void setViaFlight(String viaFlight) {
		this.viaFlight = viaFlight;
	}
	public String getDiscount() {
		return discount;
	}
	public void setDiscount(String discount) {
		this.discount = discount;
	}
	public String getAirportTaxChild() {
		return airportTaxChild;
	}
	public void setAirportTaxChild(String airportTaxChild) {
		this.airportTaxChild = airportTaxChild;
	}
	public String getAirportTaxInfant() {
		return airportTaxInfant;
	}
	public void setAirportTaxInfant(String airportTaxInfant) {
		this.airportTaxInfant = airportTaxInfant;
	}
	public String getAdultTaxBreakup() {
		return adultTaxBreakup;
	}
	public void setAdultTaxBreakup(String adultTaxBreakup) {
		this.adultTaxBreakup = adultTaxBreakup;
	}
	public String getChildTaxBreakup() {
		return childTaxBreakup;
	}
	public void setChildTaxBreakup(String childTaxBreakup) {
		this.childTaxBreakup = childTaxBreakup;
	}
	public String getInfantTaxBreakup() {
		return infantTaxBreakup;
	}
	public void setInfantTaxBreakup(String infantTaxBreakup) {
		this.infantTaxBreakup = infantTaxBreakup;
	}
	public String getOcTaxForFlightSegment() {
		return ocTaxForFlightSegment;
	}
	public void setOcTaxForFlightSegment(String ocTaxForFlightSegment) {
		this.ocTaxForFlightSegment = ocTaxForFlightSegment;
	}
	public String getAvailability() {
		return availability;
	}
	public void setAvailability(String availability) {
		this.availability = availability;
	}
	public String getResBookDesigCode() {
		return resBookDesigCode;
	}
	public void setResBookDesigCode(String resBookDesigCode) {
		this.resBookDesigCode = resBookDesigCode;
	}
	public String getIntBIC() {
		return intBIC;
	}
	public void setIntBIC(String intBIC) {
		this.intBIC = intBIC;
	}
	public String getAdultFare() {
		return adultFare;
	}
	public void setAdultFare(String adultFare) {
		this.adultFare = adultFare;
	}
	public String getBookingclass() {
		return bookingclass;
	}
	public void setBookingclass(String bookingclass) {
		this.bookingclass = bookingclass;
	}
	public String getClassType() {
		return classType;
	}
	public void setClassType(String classType) {
		this.classType = classType;
	}
	public String getFareBasisCode() {
		return fareBasisCode;
	}
	public void setFareBasisCode(String fareBasisCode) {
		this.fareBasisCode = fareBasisCode;
	}
	public String getRuleForBookingClassFare() {
		return ruleForBookingClassFare;
	}
	public void setRuleForBookingClassFare(String ruleForBookingClassFare) {
		this.ruleForBookingClassFare = ruleForBookingClassFare;
	}
	public String getAdultCommission() {
		return AdultCommission;
	}
	public void setAdultCommission(String adultCommission) {
		AdultCommission = adultCommission;
	}
	public String getChildCommission() {
		return childCommission;
	}
	public void setChildCommission(String childCommission) {
		this.childCommission = childCommission;
	}
	public String getCommissionOnTCharge() {
		return commissionOnTCharge;
	}
	public void setCommissionOnTCharge(String commissionOnTCharge) {
		this.commissionOnTCharge = commissionOnTCharge;
	}
	public String getChildFare() {
		return childFare;
	}
	public void setChildFare(String childFare) {
		this.childFare = childFare;
	}
	public String getInfantFare() {
		return infantFare;
	}
	public void setInfantFare(String infantFare) {
		this.infantFare = infantFare;
	}
	public String getIntNumStops() {
		return intNumStops;
	}
	public void setIntNumStops(String intNumStops) {
		this.intNumStops = intNumStops;
	}
	public String getIntOperatingAirlineName() {
		return intOperatingAirlineName;
	}
	public void setIntOperatingAirlineName(String intOperatingAirlineName) {
		this.intOperatingAirlineName = intOperatingAirlineName;
	}
	public String getIntArrivalAirportName() {
		return intArrivalAirportName;
	}
	public void setIntArrivalAirportName(String intArrivalAirportName) {
		this.intArrivalAirportName = intArrivalAirportName;
	}
	public String getIntDepartureAirportName() {
		return intDepartureAirportName;
	}
	public void setIntDepartureAirportName(String intDepartureAirportName) {
		this.intDepartureAirportName = intDepartureAirportName;
	}
	public String getIntLinkSellAgrmnt() {
		return intLinkSellAgrmnt;
	}
	public void setIntLinkSellAgrmnt(String intLinkSellAgrmnt) {
		this.intLinkSellAgrmnt = intLinkSellAgrmnt;
	}
	public String getIntConx() {
		return intConx;
	}
	public void setIntConx(String intConx) {
		this.intConx = intConx;
	}
	public String getIntAirpChg() {
		return intAirpChg;
	}
	public void setIntAirpChg(String intAirpChg) {
		this.intAirpChg = intAirpChg;
	}
	public String getIntInsideAvailOption() {
		return intInsideAvailOption;
	}
	public void setIntInsideAvailOption(String intInsideAvailOption) {
		this.intInsideAvailOption = intInsideAvailOption;
	}
	public String getIntGenTrafRestriction() {
		return intGenTrafRestriction;
	}
	public void setIntGenTrafRestriction(String intGenTrafRestriction) {
		this.intGenTrafRestriction = intGenTrafRestriction;
	}
	public String getIntDaysOperates() {
		return intDaysOperates;
	}
	public void setIntDaysOperates(String intDaysOperates) {
		this.intDaysOperates = intDaysOperates;
	}
	public String getIntJourneyTime() {
		return intJourneyTime;
	}
	public void setIntJourneyTime(String intJourneyTime) {
		this.intJourneyTime = intJourneyTime;
	}
	public String getIntEndDate() {
		return intEndDate;
	}
	public void setIntEndDate(String intEndDate) {
		this.intEndDate = intEndDate;
	}
	public String getIntStartTerminal() {
		return intStartTerminal;
	}
	public void setIntStartTerminal(String intStartTerminal) {
		this.intStartTerminal = intStartTerminal;
	}
	public String getIntEndTerminal() {
		return intEndTerminal;
	}
	public void setIntEndTerminal(String intEndTerminal) {
		this.intEndTerminal = intEndTerminal;
	}
	public String getIntFltTm() {
		return intFltTm;
	}
	public void setIntFltTm(String intFltTm) {
		this.intFltTm = intFltTm;
	}
	public String getIntLSAInd() {
		return intLSAInd;
	}
	public void setIntLSAInd(String intLSAInd) {
		this.intLSAInd = intLSAInd;
	}
	public String getIntMile() {
		return intMile;
	}
	public void setIntMile(String intMile) {
		this.intMile = intMile;
	}
	public String getCabin() {
		return cabin;
	}
	public void setCabin(String cabin) {
		this.cabin = cabin;
	}
	public String getReturnFlightSegments() {
		return returnFlightSegments;
	}
	public void setReturnFlightSegments(String returnFlightSegments) {
		this.returnFlightSegments = returnFlightSegments;
	}
	public String getFareDetails() {
		return fareDetails;
	}
	public void setFareDetails(String fareDetails) {
		this.fareDetails = fareDetails;
	}
	public String getBookingDate() {
		return bookingDate;
	}
	public void setBookingDate(String bookingDate) {
		this.bookingDate = bookingDate;
	}
	public String getPromoCode() {
		return promoCode;
	}
	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}
	public String getPromoCodeAmount() {
		return promoCodeAmount;
	}
	public void setPromoCodeAmount(String promoCodeAmount) {
		this.promoCodeAmount = promoCodeAmount;
	}
	public String getPostMarkup() {
		return postMarkup;
	}
	public void setPostMarkup(String postMarkup) {
		this.postMarkup = postMarkup;
	}
	public String getOcTax() {
		return ocTax;
	}
	public void setOcTax(String ocTax) {
		this.ocTax = ocTax;
	}
	public String getActualBaseFare() {
		return actualBaseFare;
	}
	public void setActualBaseFare(String actualBaseFare) {
		this.actualBaseFare = actualBaseFare;
	}
	public String getTax() {
		return tax;
	}
	public void setTax(String tax) {
		this.tax = tax;
	}
	public String getsTax() {
		return sTax;
	}
	public void setsTax(String sTax) {
		this.sTax = sTax;
	}
	public String getsCharge() {
		return sCharge;
	}
	public void setsCharge(String sCharge) {
		this.sCharge = sCharge;
	}
	public String gettDiscount() {
		return tDiscount;
	}
	public void settDiscount(String tDiscount) {
		this.tDiscount = tDiscount;
	}
	public String gettPartnerCommission() {
		return tPartnerCommission;
	}
	public void settPartnerCommission(String tPartnerCommission) {
		this.tPartnerCommission = tPartnerCommission;
	}
	public String gettCharge() {
		return tCharge;
	}
	public void settCharge(String tCharge) {
		this.tCharge = tCharge;
	}
	public String gettMarkup() {
		return tMarkup;
	}
	public void settMarkup(String tMarkup) {
		this.tMarkup = tMarkup;
	}
	public String gettSdiscount() {
		return tSdiscount;
	}
	public void settSdiscount(String tSdiscount) {
		this.tSdiscount = tSdiscount;
	}
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public String getConveniencefee() {
		return conveniencefee;
	}
	public void setConveniencefee(String conveniencefee) {
		this.conveniencefee = conveniencefee;
	}
	public String geteProductPrice() {
		return eProductPrice;
	}
	public void seteProductPrice(String eProductPrice) {
		this.eProductPrice = eProductPrice;
	}
	public String getActualBaseFareRet() {
		return actualBaseFareRet;
	}
	public void setActualBaseFareRet(String actualBaseFareRet) {
		this.actualBaseFareRet = actualBaseFareRet;
	}
	public String getTaxRet() {
		return taxRet;
	}
	public void setTaxRet(String taxRet) {
		this.taxRet = taxRet;
	}
	public String getsTaxRet() {
		return sTaxRet;
	}
	public void setsTaxRet(String sTaxRet) {
		this.sTaxRet = sTaxRet;
	}
	public String getsChargeRet() {
		return sChargeRet;
	}
	public void setsChargeRet(String sChargeRet) {
		this.sChargeRet = sChargeRet;
	}
	public String gettDiscountRet() {
		return tDiscountRet;
	}
	public void settDiscountRet(String tDiscountRet) {
		this.tDiscountRet = tDiscountRet;
	}
	public String gettSDiscountRet() {
		return tSDiscountRet;
	}
	public void settSDiscountRet(String tSDiscountRet) {
		this.tSDiscountRet = tSDiscountRet;
	}
	public String gettPartnerCommissionRet() {
		return tPartnerCommissionRet;
	}
	public void settPartnerCommissionRet(String tPartnerCommissionRet) {
		this.tPartnerCommissionRet = tPartnerCommissionRet;
	}
	public String geteProductPriceRet() {
		return eProductPriceRet;
	}
	public void seteProductPriceRet(String eProductPriceRet) {
		this.eProductPriceRet = eProductPriceRet;
	}
	public String gettChargeRet() {
		return tChargeRet;
	}
	public void settChargeRet(String tChargeRet) {
		this.tChargeRet = tChargeRet;
	}
	public String gettMarkupRet() {
		return tMarkupRet;
	}
	public void settMarkupRet(String tMarkupRet) {
		this.tMarkupRet = tMarkupRet;
	}
	public String getConveniencefeeRet() {
		return conveniencefeeRet;
	}
	public void setConveniencefeeRet(String conveniencefeeRet) {
		this.conveniencefeeRet = conveniencefeeRet;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getSourceName() {
		return sourceName;
	}
	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getDestinationName() {
		return destinationName;
	}
	public void setDestinationName(String destinationName) {
		this.destinationName = destinationName;
	}
	public String getJourneyDate() {
		return journeyDate;
	}
	public void setJourneyDate(String journeyDate) {
		this.journeyDate = journeyDate;
	}
	public String getReturnDate() {
		return returnDate;
	}
	public void setReturnDate(String returnDate) {
		this.returnDate = returnDate;
	}
	public String getTripType() {
		return tripType;
	}
	public void setTripType(String tripType) {
		this.tripType = tripType;
	}
	public String getFlightType() {
		return flightType;
	}
	public void setFlightType(String flightType) {
		this.flightType = flightType;
	}
	public String getAdultPax() {
		return adultPax;
	}
	public void setAdultPax(String adultPax) {
		this.adultPax = adultPax;
	}
	public String getChildPax() {
		return childPax;
	}
	public void setChildPax(String childPax) {
		this.childPax = childPax;
	}
	public String getInfantPax() {
		return infantPax;
	}
	public void setInfantPax(String infantPax) {
		this.infantPax = infantPax;
	}
	public String getTravelClass() {
		return travelClass;
	}
	public void setTravelClass(String travelClass) {
		this.travelClass = travelClass;
	}
	public String getIsNonStopFlight() {
		return isNonStopFlight;
	}
	public void setIsNonStopFlight(String isNonStopFlight) {
		this.isNonStopFlight = isNonStopFlight;
	}
	public String getFlightTimings() {
		return flightTimings;
	}
	public void setFlightTimings(String flightTimings) {
		this.flightTimings = flightTimings;
	}
	public String getAirlineName() {
		return airlineName;
	}
	public void setAirlineName(String airlineName) {
		this.airlineName = airlineName;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public String getIsGDS() {
		return isGDS;
	}
	public void setIsGDS(String isGDS) {
		this.isGDS = isGDS;
	}
	public String getIntMarketingAirlineCode() {
		return intMarketingAirlineCode;
	}
	public void setIntMarketingAirlineCode(String intMarketingAirlineCode) {
		this.intMarketingAirlineCode = intMarketingAirlineCode;
	}
	public String getStax() {
		return stax;
	}
	public void setStax(String stax) {
		this.stax = stax;
	}
	public String getFareBreakUp() {
		return fareBreakUp;
	}
	public void setFareBreakUp(String fareBreakUp) {
		this.fareBreakUp = fareBreakUp;
	}
	public String getTotalFare() {
		return totalFare;
	}
	public void setTotalFare(String totalFare) {
		this.totalFare = totalFare;
	}
	public String getSessionid() {
		return sessionid;
	}
	public void setSessionid(String sessionid) {
		this.sessionid = sessionid;
	}
	public String getReturnbookingRefNo() {
		return returnbookingRefNo;
	}
	public void setReturnbookingRefNo(String returnbookingRefNo) {
		this.returnbookingRefNo = returnbookingRefNo;
	}
	public String getReturnbookingStatus() {
		return returnbookingStatus;
	}
	public void setReturnbookingStatus(String returnbookingStatus) {
		this.returnbookingStatus = returnbookingStatus;
	}
	public String getReturnprovider() {
		return returnprovider;
	}
	public void setReturnprovider(String returnprovider) {
		this.returnprovider = returnprovider;
	}
	public String getReturnpaymentId() {
		return returnpaymentId;
	}
	public void setReturnpaymentId(String returnpaymentId) {
		this.returnpaymentId = returnpaymentId;
	}
	public String getReturnsmsUsageCount() {
		return returnsmsUsageCount;
	}
	public void setReturnsmsUsageCount(String returnsmsUsageCount) {
		this.returnsmsUsageCount = returnsmsUsageCount;
	}
	public String getReturnimagePath() {
		return returnimagePath;
	}
	public void setReturnimagePath(String returnimagePath) {
		this.returnimagePath = returnimagePath;
	}
	public String getReturnimagePathRet() {
		return returnimagePathRet;
	}
	public void setReturnimagePathRet(String returnimagePathRet) {
		this.returnimagePathRet = returnimagePathRet;
	}
	public String getReturnrule() {
		return returnrule;
	}
	public void setReturnrule(String returnrule) {
		this.returnrule = returnrule;
	}
	public String getReturnkey() {
		return returnkey;
	}
	public void setReturnkey(String returnkey) {
		this.returnkey = returnkey;
	}
	public String getReturnruleRet() {
		return returnruleRet;
	}
	public void setReturnruleRet(String returnruleRet) {
		this.returnruleRet = returnruleRet;
	}
	public String getReturnkeyRet() {
		return returnkeyRet;
	}
	public void setReturnkeyRet(String returnkeyRet) {
		this.returnkeyRet = returnkeyRet;
	}
	public String getReturnflightId() {
		return returnflightId;
	}
	public void setReturnflightId(String returnflightId) {
		this.returnflightId = returnflightId;
	}
	public String getReturnflightIdRet() {
		return returnflightIdRet;
	}
	public void setReturnflightIdRet(String returnflightIdRet) {
		this.returnflightIdRet = returnflightIdRet;
	}
	public String getReturnairEquipType() {
		return returnairEquipType;
	}
	public void setReturnairEquipType(String returnairEquipType) {
		this.returnairEquipType = returnairEquipType;
	}
	public String getReturnarrivalAirportCode() {
		return returnarrivalAirportCode;
	}
	public void setReturnarrivalAirportCode(String returnarrivalAirportCode) {
		this.returnarrivalAirportCode = returnarrivalAirportCode;
	}
	public String getReturnarrivalDateTime() {
		return returnarrivalDateTime;
	}
	public void setReturnarrivalDateTime(String returnarrivalDateTime) {
		this.returnarrivalDateTime = returnarrivalDateTime;
	}
	public String getReturnarrivalDateTimeZone() {
		return returnarrivalDateTimeZone;
	}
	public void setReturnarrivalDateTimeZone(String returnarrivalDateTimeZone) {
		this.returnarrivalDateTimeZone = returnarrivalDateTimeZone;
	}
	public String getReturndepartureAirportCode() {
		return returndepartureAirportCode;
	}
	public void setReturndepartureAirportCode(String returndepartureAirportCode) {
		this.returndepartureAirportCode = returndepartureAirportCode;
	}
	public String getReturndepartureDateTime() {
		return returndepartureDateTime;
	}
	public void setReturndepartureDateTime(String returndepartureDateTime) {
		this.returndepartureDateTime = returndepartureDateTime;
	}
	public String getReturndepartureDateTimeZone() {
		return returndepartureDateTimeZone;
	}
	public void setReturndepartureDateTimeZone(String returndepartureDateTimeZone) {
		this.returndepartureDateTimeZone = returndepartureDateTimeZone;
	}
	public String getReturnduration() {
		return returnduration;
	}
	public void setReturnduration(String returnduration) {
		this.returnduration = returnduration;
	}
	public String getReturnflightNumber() {
		return returnflightNumber;
	}
	public void setReturnflightNumber(String returnflightNumber) {
		this.returnflightNumber = returnflightNumber;
	}
	public String getReturnoperatingAirlineCode() {
		return returnoperatingAirlineCode;
	}
	public void setReturnoperatingAirlineCode(String returnoperatingAirlineCode) {
		this.returnoperatingAirlineCode = returnoperatingAirlineCode;
	}
	public String getReturnoperatingAirlineFlightNumber() {
		return returnoperatingAirlineFlightNumber;
	}
	public void setReturnoperatingAirlineFlightNumber(String returnoperatingAirlineFlightNumber) {
		this.returnoperatingAirlineFlightNumber = returnoperatingAirlineFlightNumber;
	}
	public String getReturnrPH() {
		return returnrPH;
	}
	public void setReturnrPH(String returnrPH) {
		this.returnrPH = returnrPH;
	}
	public String getReturnstopQuantity() {
		return returnstopQuantity;
	}
	public void setReturnstopQuantity(String returnstopQuantity) {
		this.returnstopQuantity = returnstopQuantity;
	}
	public String getReturnairLineName() {
		return returnairLineName;
	}
	public void setReturnairLineName(String returnairLineName) {
		this.returnairLineName = returnairLineName;
	}
	public String getReturnairportTax() {
		return returnairportTax;
	}
	public void setReturnairportTax(String returnairportTax) {
		this.returnairportTax = returnairportTax;
	}
	public String getReturnimageFileName() {
		return returnimageFileName;
	}
	public void setReturnimageFileName(String returnimageFileName) {
		this.returnimageFileName = returnimageFileName;
	}
	public String getReturnviaFlight() {
		return returnviaFlight;
	}
	public void setReturnviaFlight(String returnviaFlight) {
		this.returnviaFlight = returnviaFlight;
	}
	public String getReturndiscount() {
		return returndiscount;
	}
	public void setReturndiscount(String returndiscount) {
		this.returndiscount = returndiscount;
	}
	public String getReturnairportTaxChild() {
		return returnairportTaxChild;
	}
	public void setReturnairportTaxChild(String returnairportTaxChild) {
		this.returnairportTaxChild = returnairportTaxChild;
	}
	public String getReturnairportTaxInfant() {
		return returnairportTaxInfant;
	}
	public void setReturnairportTaxInfant(String returnairportTaxInfant) {
		this.returnairportTaxInfant = returnairportTaxInfant;
	}
	public String getReturnadultTaxBreakup() {
		return returnadultTaxBreakup;
	}
	public void setReturnadultTaxBreakup(String returnadultTaxBreakup) {
		this.returnadultTaxBreakup = returnadultTaxBreakup;
	}
	public String getReturnchildTaxBreakup() {
		return returnchildTaxBreakup;
	}
	public void setReturnchildTaxBreakup(String returnchildTaxBreakup) {
		this.returnchildTaxBreakup = returnchildTaxBreakup;
	}
	public String getReturninfantTaxBreakup() {
		return returninfantTaxBreakup;
	}
	public void setReturninfantTaxBreakup(String returninfantTaxBreakup) {
		this.returninfantTaxBreakup = returninfantTaxBreakup;
	}
	public String getReturnocTaxForFlightSegment() {
		return returnocTaxForFlightSegment;
	}
	public void setReturnocTaxForFlightSegment(String returnocTaxForFlightSegment) {
		this.returnocTaxForFlightSegment = returnocTaxForFlightSegment;
	}
	public String getReturnavailability() {
		return returnavailability;
	}
	public void setReturnavailability(String returnavailability) {
		this.returnavailability = returnavailability;
	}
	public String getReturnresBookDesigCode() {
		return returnresBookDesigCode;
	}
	public void setReturnresBookDesigCode(String returnresBookDesigCode) {
		this.returnresBookDesigCode = returnresBookDesigCode;
	}
	public String getReturnintBIC() {
		return returnintBIC;
	}
	public void setReturnintBIC(String returnintBIC) {
		this.returnintBIC = returnintBIC;
	}
	public String getReturnadultFare() {
		return returnadultFare;
	}
	public void setReturnadultFare(String returnadultFare) {
		this.returnadultFare = returnadultFare;
	}
	public String getReturnbookingclass() {
		return returnbookingclass;
	}
	public void setReturnbookingclass(String returnbookingclass) {
		this.returnbookingclass = returnbookingclass;
	}
	public String getReturnclassType() {
		return returnclassType;
	}
	public void setReturnclassType(String returnclassType) {
		this.returnclassType = returnclassType;
	}
	public String getReturnfareBasisCode() {
		return returnfareBasisCode;
	}
	public void setReturnfareBasisCode(String returnfareBasisCode) {
		this.returnfareBasisCode = returnfareBasisCode;
	}
	public String getReturnruleForBookingClassFare() {
		return returnruleForBookingClassFare;
	}
	public void setReturnruleForBookingClassFare(String returnruleForBookingClassFare) {
		this.returnruleForBookingClassFare = returnruleForBookingClassFare;
	}
	
	public String getReturnadultCommission() {
		return returnadultCommission;
	}
	public void setReturnadultCommission(String returnadultCommission) {
		this.returnadultCommission = returnadultCommission;
	}
	public String getReturnchildCommission() {
		return returnchildCommission;
	}
	public void setReturnchildCommission(String returnchildCommission) {
		this.returnchildCommission = returnchildCommission;
	}
	public String getReturncommissionOnTCharge() {
		return returncommissionOnTCharge;
	}
	public void setReturncommissionOnTCharge(String returncommissionOnTCharge) {
		this.returncommissionOnTCharge = returncommissionOnTCharge;
	}
	public String getReturnchildFare() {
		return returnchildFare;
	}
	public void setReturnchildFare(String returnchildFare) {
		this.returnchildFare = returnchildFare;
	}
	public String getReturninfantFare() {
		return returninfantFare;
	}
	public void setReturninfantFare(String returninfantFare) {
		this.returninfantFare = returninfantFare;
	}
	public String getReturnintNumStops() {
		return returnintNumStops;
	}
	public void setReturnintNumStops(String returnintNumStops) {
		this.returnintNumStops = returnintNumStops;
	}
	public String getReturnintOperatingAirlineName() {
		return returnintOperatingAirlineName;
	}
	public void setReturnintOperatingAirlineName(String returnintOperatingAirlineName) {
		this.returnintOperatingAirlineName = returnintOperatingAirlineName;
	}
	public String getReturnintArrivalAirportName() {
		return returnintArrivalAirportName;
	}
	public void setReturnintArrivalAirportName(String returnintArrivalAirportName) {
		this.returnintArrivalAirportName = returnintArrivalAirportName;
	}
	public String getReturnintDepartureAirportName() {
		return returnintDepartureAirportName;
	}
	public void setReturnintDepartureAirportName(String returnintDepartureAirportName) {
		this.returnintDepartureAirportName = returnintDepartureAirportName;
	}
	public String getReturnintLinkSellAgrmnt() {
		return returnintLinkSellAgrmnt;
	}
	public void setReturnintLinkSellAgrmnt(String returnintLinkSellAgrmnt) {
		this.returnintLinkSellAgrmnt = returnintLinkSellAgrmnt;
	}
	public String getReturnintConx() {
		return returnintConx;
	}
	public void setReturnintConx(String returnintConx) {
		this.returnintConx = returnintConx;
	}
	public String getReturnintAirpChg() {
		return returnintAirpChg;
	}
	public void setReturnintAirpChg(String returnintAirpChg) {
		this.returnintAirpChg = returnintAirpChg;
	}
	public String getReturnintInsideAvailOption() {
		return returnintInsideAvailOption;
	}
	public void setReturnintInsideAvailOption(String returnintInsideAvailOption) {
		this.returnintInsideAvailOption = returnintInsideAvailOption;
	}
	public String getReturnintGenTrafRestriction() {
		return returnintGenTrafRestriction;
	}
	public void setReturnintGenTrafRestriction(String returnintGenTrafRestriction) {
		this.returnintGenTrafRestriction = returnintGenTrafRestriction;
	}
	public String getReturnintDaysOperates() {
		return returnintDaysOperates;
	}
	public void setReturnintDaysOperates(String returnintDaysOperates) {
		this.returnintDaysOperates = returnintDaysOperates;
	}
	public String getReturnintJourneyTime() {
		return returnintJourneyTime;
	}
	public void setReturnintJourneyTime(String returnintJourneyTime) {
		this.returnintJourneyTime = returnintJourneyTime;
	}
	public String getReturnintEndDate() {
		return returnintEndDate;
	}
	public void setReturnintEndDate(String returnintEndDate) {
		this.returnintEndDate = returnintEndDate;
	}
	public String getReturnintStartTerminal() {
		return returnintStartTerminal;
	}
	public void setReturnintStartTerminal(String returnintStartTerminal) {
		this.returnintStartTerminal = returnintStartTerminal;
	}
	public String getReturnintEndTerminal() {
		return returnintEndTerminal;
	}
	public void setReturnintEndTerminal(String returnintEndTerminal) {
		this.returnintEndTerminal = returnintEndTerminal;
	}
	public String getReturnintFltTm() {
		return returnintFltTm;
	}
	public void setReturnintFltTm(String returnintFltTm) {
		this.returnintFltTm = returnintFltTm;
	}
	public String getReturnintLSAInd() {
		return returnintLSAInd;
	}
	public void setReturnintLSAInd(String returnintLSAInd) {
		this.returnintLSAInd = returnintLSAInd;
	}
	public String getReturnintMile() {
		return returnintMile;
	}
	public void setReturnintMile(String returnintMile) {
		this.returnintMile = returnintMile;
	}
	public String getReturncabin() {
		return returncabin;
	}
	public void setReturncabin(String returncabin) {
		this.returncabin = returncabin;
	}
	public String getReturnreturnFlightSegments() {
		return returnreturnFlightSegments;
	}
	public void setReturnreturnFlightSegments(String returnreturnFlightSegments) {
		this.returnreturnFlightSegments = returnreturnFlightSegments;
	}
	public String getReturnfareDetails() {
		return returnfareDetails;
	}
	public void setReturnfareDetails(String returnfareDetails) {
		this.returnfareDetails = returnfareDetails;
	}
	public String getReturnbookingDate() {
		return returnbookingDate;
	}
	public void setReturnbookingDate(String returnbookingDate) {
		this.returnbookingDate = returnbookingDate;
	}
	public String getReturnpromoCode() {
		return returnpromoCode;
	}
	public void setReturnpromoCode(String returnpromoCode) {
		this.returnpromoCode = returnpromoCode;
	}
	public String getReturnpromoCodeAmount() {
		return returnpromoCodeAmount;
	}
	public void setReturnpromoCodeAmount(String returnpromoCodeAmount) {
		this.returnpromoCodeAmount = returnpromoCodeAmount;
	}
	public String getReturnpostMarkup() {
		return returnpostMarkup;
	}
	public void setReturnpostMarkup(String returnpostMarkup) {
		this.returnpostMarkup = returnpostMarkup;
	}
	public String getReturnocTax() {
		return returnocTax;
	}
	public void setReturnocTax(String returnocTax) {
		this.returnocTax = returnocTax;
	}
	public String getReturnactualBaseFare() {
		return returnactualBaseFare;
	}
	public void setReturnactualBaseFare(String returnactualBaseFare) {
		this.returnactualBaseFare = returnactualBaseFare;
	}
	public String getReturntax() {
		return returntax;
	}
	public void setReturntax(String returntax) {
		this.returntax = returntax;
	}
	public String getReturnsTax() {
		return returnsTax;
	}
	public void setReturnsTax(String returnsTax) {
		this.returnsTax = returnsTax;
	}
	public String getReturnsCharge() {
		return returnsCharge;
	}
	public void setReturnsCharge(String returnsCharge) {
		this.returnsCharge = returnsCharge;
	}
	public String getReturntDiscount() {
		return returntDiscount;
	}
	public void setReturntDiscount(String returntDiscount) {
		this.returntDiscount = returntDiscount;
	}
	public String getReturntPartnerCommission() {
		return returntPartnerCommission;
	}
	public void setReturntPartnerCommission(String returntPartnerCommission) {
		this.returntPartnerCommission = returntPartnerCommission;
	}
	public String getReturntCharge() {
		return returntCharge;
	}
	public void setReturntCharge(String returntCharge) {
		this.returntCharge = returntCharge;
	}
	public String getReturntMarkup() {
		return returntMarkup;
	}
	public void setReturntMarkup(String returntMarkup) {
		this.returntMarkup = returntMarkup;
	}
	public String getReturntSdiscount() {
		return returntSdiscount;
	}
	public void setReturntSdiscount(String returntSdiscount) {
		this.returntSdiscount = returntSdiscount;
	}
	public String getReturntransactionId() {
		return returntransactionId;
	}
	public void setReturntransactionId(String returntransactionId) {
		this.returntransactionId = returntransactionId;
	}
	public String getReturnconveniencefee() {
		return returnconveniencefee;
	}
	public void setReturnconveniencefee(String returnconveniencefee) {
		this.returnconveniencefee = returnconveniencefee;
	}
	public String getReturneProductPrice() {
		return returneProductPrice;
	}
	public void setReturneProductPrice(String returneProductPrice) {
		this.returneProductPrice = returneProductPrice;
	}
	public String getReturnactualBaseFareRet() {
		return returnactualBaseFareRet;
	}
	public void setReturnactualBaseFareRet(String returnactualBaseFareRet) {
		this.returnactualBaseFareRet = returnactualBaseFareRet;
	}
	public String getReturntaxRet() {
		return returntaxRet;
	}
	public void setReturntaxRet(String returntaxRet) {
		this.returntaxRet = returntaxRet;
	}
	public String getReturnsTaxRet() {
		return returnsTaxRet;
	}
	public void setReturnsTaxRet(String returnsTaxRet) {
		this.returnsTaxRet = returnsTaxRet;
	}
	public String getReturnsChargeRet() {
		return returnsChargeRet;
	}
	public void setReturnsChargeRet(String returnsChargeRet) {
		this.returnsChargeRet = returnsChargeRet;
	}
	public String getReturntDiscountRet() {
		return returntDiscountRet;
	}
	public void setReturntDiscountRet(String returntDiscountRet) {
		this.returntDiscountRet = returntDiscountRet;
	}
	public String getReturntSDiscountRet() {
		return returntSDiscountRet;
	}
	public void setReturntSDiscountRet(String returntSDiscountRet) {
		this.returntSDiscountRet = returntSDiscountRet;
	}
	public String getReturntPartnerCommissionRet() {
		return returntPartnerCommissionRet;
	}
	public void setReturntPartnerCommissionRet(String returntPartnerCommissionRet) {
		this.returntPartnerCommissionRet = returntPartnerCommissionRet;
	}
	public String getReturneProductPriceRet() {
		return returneProductPriceRet;
	}
	public void setReturneProductPriceRet(String returneProductPriceRet) {
		this.returneProductPriceRet = returneProductPriceRet;
	}
	public String getReturntChargeRet() {
		return returntChargeRet;
	}
	public void setReturntChargeRet(String returntChargeRet) {
		this.returntChargeRet = returntChargeRet;
	}
	public String getReturntMarkupRet() {
		return returntMarkupRet;
	}
	public void setReturntMarkupRet(String returntMarkupRet) {
		this.returntMarkupRet = returntMarkupRet;
	}
	public String getReturnconveniencefeeRet() {
		return returnconveniencefeeRet;
	}
	public void setReturnconveniencefeeRet(String returnconveniencefeeRet) {
		this.returnconveniencefeeRet = returnconveniencefeeRet;
	}
	public String getReturnsource() {
		return returnsource;
	}
	public void setReturnsource(String returnsource) {
		this.returnsource = returnsource;
	}
	public String getReturnsourceName() {
		return returnsourceName;
	}
	public void setReturnsourceName(String returnsourceName) {
		this.returnsourceName = returnsourceName;
	}
	public String getReturndestination() {
		return returndestination;
	}
	public void setReturndestination(String returndestination) {
		this.returndestination = returndestination;
	}
	public String getReturndestinationName() {
		return returndestinationName;
	}
	public void setReturndestinationName(String returndestinationName) {
		this.returndestinationName = returndestinationName;
	}
	public String getReturnjourneyDate() {
		return returnjourneyDate;
	}
	public void setReturnjourneyDate(String returnjourneyDate) {
		this.returnjourneyDate = returnjourneyDate;
	}
	public String getReturnreturnDate() {
		return returnreturnDate;
	}
	public void setReturnreturnDate(String returnreturnDate) {
		this.returnreturnDate = returnreturnDate;
	}
	public String getReturntripType() {
		return returntripType;
	}
	public void setReturntripType(String returntripType) {
		this.returntripType = returntripType;
	}
	public String getReturnflightType() {
		return returnflightType;
	}
	public void setReturnflightType(String returnflightType) {
		this.returnflightType = returnflightType;
	}
	public String getReturnadultPax() {
		return returnadultPax;
	}
	public void setReturnadultPax(String returnadultPax) {
		this.returnadultPax = returnadultPax;
	}
	public String getReturnchildPax() {
		return returnchildPax;
	}
	public void setReturnchildPax(String returnchildPax) {
		this.returnchildPax = returnchildPax;
	}
	public String getReturninfantPax() {
		return returninfantPax;
	}
	public void setReturninfantPax(String returninfantPax) {
		this.returninfantPax = returninfantPax;
	}
	public String getReturntravelClass() {
		return returntravelClass;
	}
	public void setReturntravelClass(String returntravelClass) {
		this.returntravelClass = returntravelClass;
	}
	public String getReturnisNonStopFlight() {
		return returnisNonStopFlight;
	}
	public void setReturnisNonStopFlight(String returnisNonStopFlight) {
		this.returnisNonStopFlight = returnisNonStopFlight;
	}
	public String getReturnflightTimings() {
		return returnflightTimings;
	}
	public void setReturnflightTimings(String returnflightTimings) {
		this.returnflightTimings = returnflightTimings;
	}
	public String getReturnairlineName() {
		return returnairlineName;
	}
	public void setReturnairlineName(String returnairlineName) {
		this.returnairlineName = returnairlineName;
	}
	public String getReturnuser() {
		return returnuser;
	}
	public void setReturnuser(String returnuser) {
		this.returnuser = returnuser;
	}
	public String getReturnuserType() {
		return returnuserType;
	}
	public void setReturnuserType(String returnuserType) {
		this.returnuserType = returnuserType;
	}
	public String getReturnisGDS() {
		return returnisGDS;
	}
	public void setReturnisGDS(String returnisGDS) {
		this.returnisGDS = returnisGDS;
	}
	public String getReturnintMarketingAirlineCode() {
		return returnintMarketingAirlineCode;
	}
	public void setReturnintMarketingAirlineCode(String returnintMarketingAirlineCode) {
		this.returnintMarketingAirlineCode = returnintMarketingAirlineCode;
	}
	public String getReturnstax() {
		return returnstax;
	}
	public void setReturnstax(String returnstax) {
		this.returnstax = returnstax;
	}
	public String getReturnfareBreakUp() {
		return returnfareBreakUp;
	}
	public void setReturnfareBreakUp(String returnfareBreakUp) {
		this.returnfareBreakUp = returnfareBreakUp;
	}
	public String getReturntotalFare() {
		return returntotalFare;
	}
	public void setReturntotalFare(String returntotalFare) {
		this.returntotalFare = returntotalFare;
	}

		
	
	}
