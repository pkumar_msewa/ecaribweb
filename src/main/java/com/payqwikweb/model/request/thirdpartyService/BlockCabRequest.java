package com.payqwikweb.model.request.thirdpartyService;

public class BlockCabRequest {

	private String sessionId;
	private String tripId;
	private String sourceId;
	private String sourceName;
	private String destinationId;
	private String destinationName;
	private String journeyDate;
	private String noOfCars;
	private String tripType;
	private String isAgentPaymentGateway;

	private String travelType;
	private String approxRoundTripDistance;
	private String provider;
	private String operator;
	private String carTypeName;
	private String departureTime;
	private String driverCharges;
	private String operatorName;
	private String perKmRateCharge;
	private String pickUpAddress;
	private String totalFare;
	private String vehicleName;
	private String userType;
	private String title;
	private String name;
	private String age;
	private String gender;
	private String city;
	private String address;
	private String idCardType;
	private String idCardNo;
	private String idCardIssuedBy;
	private String mobileNo;
	private String emailId;
	private String emergencyMobileNo;
	private String transactionRefNo;
	private String promoCodeAmount;
	private String conveniencefee;
	private String noofPassengers;
	private String bookingRefNo;
	private String state;
	private String postalCode;
	private String paymentId;
	private String cancellationPolicy;
	private String promoCode;
	private String minimumChargedDistance;
	private String perKmRateOneWayCharge;
	private String nightHalt;
	private String bookingStatus;
	private String waitingCharges;
	private String extraHourRate;
	private String sMSUsageCount;
	private String basicRate;
	private String postMarkup;
	private String operatorId;
	private String pickUpTime;
	private String days;
	private String user;
	private String pickUpLocation;
	private String dropLocation;
	private String message;
	private String ipAddress;
	private String tokenKey;
	private double amount;
	private String blockRefNo;
	private String consumerKey;
	private String consumerSecret;

	public String getIsAgentPaymentGateway() {
		return isAgentPaymentGateway;
	}

	public void setIsAgentPaymentGateway(String isAgentPaymentGateway) {
		this.isAgentPaymentGateway = isAgentPaymentGateway;
	}

	public String getTokenKey() {
		return tokenKey;
	}

	public void setTokenKey(String tokenKey) {
		this.tokenKey = tokenKey;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getBlockRefNo() {
		return blockRefNo;
	}

	public void setBlockRefNo(String blockRefNo) {
		this.blockRefNo = blockRefNo;
	}

	private String apiReferenceNo;
	String lname;

	public String getEmergencyMobileNo() {
		return emergencyMobileNo;
	}

	public void setEmergencyMobileNo(String emergencyMobileNo) {
		this.emergencyMobileNo = emergencyMobileNo;
	}

	public String getIdCardNo() {
		return idCardNo;
	}

	public void setIdCardNo(String idCardNo) {
		this.idCardNo = idCardNo;
	}

	public String getIdCardIssuedBy() {
		return idCardIssuedBy;
	}

	public void setIdCardIssuedBy(String idCardIssuedBy) {
		this.idCardIssuedBy = idCardIssuedBy;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getIdCardType() {
		return idCardType;
	}

	public void setIdCardType(String idCardType) {
		this.idCardType = idCardType;
	}

	public String getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}

	public String getCarTypeName() {
		return carTypeName;
	}

	public void setCarTypeName(String carTypeName) {
		this.carTypeName = carTypeName;
	}

	public String getTransactionRefNo() {
		return transactionRefNo;
	}

	public void setTransactionRefNo(String transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getConsumerKey() {
		return consumerKey;
	}

	public void setConsumerKey(String consumerKey) {
		this.consumerKey = consumerKey;
	}

	public String getConsumerSecret() {
		return consumerSecret;
	}

	public void setConsumerSecret(String consumerSecret) {
		this.consumerSecret = consumerSecret;
	}

	public String getPromoCodeAmount() {
		return promoCodeAmount;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public void setPromoCodeAmount(String promoCodeAmount) {
		this.promoCodeAmount = promoCodeAmount;
	}

	public String getTripId() {
		return tripId;
	}

	public void setTripId(String tripId) {
		this.tripId = tripId;
	}

	public String getTotalFare() {
		return totalFare;
	}

	public void setTotalFare(String totalFare) {
		this.totalFare = totalFare;
	}

	public String getApiReferenceNo() {
		return apiReferenceNo;
	}

	public void setApiReferenceNo(String apiReferenceNo) {
		this.apiReferenceNo = apiReferenceNo;
	}

	public String getMessage() {
		return message;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getConveniencefee() {
		return conveniencefee;
	}

	public void setConveniencefee(String conveniencefee) {
		this.conveniencefee = conveniencefee;
	}

	public String getNoofPassengers() {
		return noofPassengers;
	}

	public void setNoofPassengers(String noofPassengers) {
		this.noofPassengers = noofPassengers;
	}

	public String getBookingRefNo() {
		return bookingRefNo;
	}

	public void setBookingRefNo(String bookingRefNo) {
		this.bookingRefNo = bookingRefNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getPickUpAddress() {
		return pickUpAddress;
	}

	public void setPickUpAddress(String pickUpAddress) {
		this.pickUpAddress = pickUpAddress;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}

	public String getCancellationPolicy() {
		return cancellationPolicy;
	}

	public void setCancellationPolicy(String cancellationPolicy) {
		this.cancellationPolicy = cancellationPolicy;
	}

	public String getPromoCode() {
		return promoCode;
	}

	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}

	public String getVehicleName() {
		return vehicleName;
	}

	public void setVehicleName(String vehicleName) {
		this.vehicleName = vehicleName;
	}

	public String getApproxRoundTripDistance() {
		return approxRoundTripDistance;
	}

	public void setApproxRoundTripDistance(String approxRoundTripDistance) {
		this.approxRoundTripDistance = approxRoundTripDistance;
	}

	public String getMinimumChargedDistance() {
		return minimumChargedDistance;
	}

	public void setMinimumChargedDistance(String minimumChargedDistance) {
		this.minimumChargedDistance = minimumChargedDistance;
	}

	public String getPerKmRateCharge() {
		return perKmRateCharge;
	}

	public void setPerKmRateCharge(String perKmRateCharge) {
		this.perKmRateCharge = perKmRateCharge;
	}

	public String getPerKmRateOneWayCharge() {
		return perKmRateOneWayCharge;
	}

	public void setPerKmRateOneWayCharge(String perKmRateOneWayCharge) {
		this.perKmRateOneWayCharge = perKmRateOneWayCharge;
	}

	public String getDriverCharges() {
		return driverCharges;
	}

	public void setDriverCharges(String driverCharges) {
		this.driverCharges = driverCharges;
	}

	public String getNightHalt() {
		return nightHalt;
	}

	public void setNightHalt(String nightHalt) {
		this.nightHalt = nightHalt;
	}

	public String getBookingStatus() {
		return bookingStatus;
	}

	public void setBookingStatus(String bookingStatus) {
		this.bookingStatus = bookingStatus;
	}

	public String getNoOfCars() {
		return noOfCars;
	}

	public void setNoOfCars(String noOfCars) {
		this.noOfCars = noOfCars;
	}

	public String getWaitingCharges() {
		return waitingCharges;
	}

	public void setWaitingCharges(String waitingCharges) {
		this.waitingCharges = waitingCharges;
	}

	public String getExtraHourRate() {
		return extraHourRate;
	}

	public void setExtraHourRate(String extraHourRate) {
		this.extraHourRate = extraHourRate;
	}

	public String getsMSUsageCount() {
		return sMSUsageCount;
	}

	public void setsMSUsageCount(String sMSUsageCount) {
		this.sMSUsageCount = sMSUsageCount;
	}

	public String getBasicRate() {
		return basicRate;
	}

	public void setBasicRate(String basicRate) {
		this.basicRate = basicRate;
	}

	public String getPostMarkup() {
		return postMarkup;
	}

	public void setPostMarkup(String postMarkup) {
		this.postMarkup = postMarkup;
	}

	public String getSourceId() {
		return sourceId;
	}

	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}

	public String getSourceName() {
		return sourceName;
	}

	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	public String getDestinationId() {
		return destinationId;
	}

	public void setDestinationId(String destinationId) {
		this.destinationId = destinationId;
	}

	public String getDestinationName() {
		return destinationName;
	}

	public void setDestinationName(String destinationName) {
		this.destinationName = destinationName;
	}

	public String getJourneyDate() {
		return journeyDate;
	}

	public void setJourneyDate(String journeyDate) {
		this.journeyDate = journeyDate;
	}

	public String getTripType() {
		return tripType;
	}

	public void setTripType(String tripType) {
		this.tripType = tripType;
	}

	public String getTravelType() {
		return travelType;
	}

	public void setTravelType(String travelType) {
		this.travelType = travelType;
	}

	public String getOperatorId() {
		return operatorId;
	}

	public void setOperatorId(String operatorId) {
		this.operatorId = operatorId;
	}

	public String getOperatorName() {
		return operatorName;
	}

	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}

	public String getPickUpTime() {
		return pickUpTime;
	}

	public void setPickUpTime(String pickUpTime) {
		this.pickUpTime = pickUpTime;
	}

	public String getDays() {
		return days;
	}

	public void setDays(String days) {
		this.days = days;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getPickUpLocation() {
		return pickUpLocation;
	}

	public void setPickUpLocation(String pickUpLocation) {
		this.pickUpLocation = pickUpLocation;
	}

	public String getDropLocation() {
		return dropLocation;
	}

	public void setDropLocation(String dropLocation) {
		this.dropLocation = dropLocation;
	}

}