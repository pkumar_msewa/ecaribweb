package com.payqwikweb.model.request.thirdpartyService;

public class GetbusdeatailRequest
{

	private String citysourceoneway;
	private String citydestoneway;
	private String datepickeronewaydeparure;
	private String tripType;
	private String userType;
	private String returnDate;
	private String operatorId;
	private String busType;
	private String busTimings;
	private String consumerKey;
	private String consumerSecret;
	public String getCitysourceoneway() {
		return citysourceoneway;
	}
	public void setCitysourceoneway(String citysourceoneway) {
		this.citysourceoneway = citysourceoneway;
	}
	public String getCitydestoneway() {
		return citydestoneway;
	}
	public void setCitydestoneway(String citydestoneway) {
		this.citydestoneway = citydestoneway;
	}
	public String getDatepickeronewaydeparure() {
		return datepickeronewaydeparure;
	}
	public void setDatepickeronewaydeparure(String datepickeronewaydeparure) {
		this.datepickeronewaydeparure = datepickeronewaydeparure;
	}
	public String getTripType() {
		return tripType;
	}
	public void setTripType(String tripType) {
		this.tripType = tripType;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public String getReturnDate() {
		return returnDate;
	}
	public void setReturnDate(String returnDate) {
		this.returnDate = returnDate;
	}
	public String getOperatorId() {
		return operatorId;
	}
	public void setOperatorId(String operatorId) {
		this.operatorId = operatorId;
	}
	public String getBusType() {
		return busType;
	}
	public void setBusType(String busType) {
		this.busType = busType;
	}
	public String getBusTimings() {
		return busTimings;
	}
	public void setBusTimings(String busTimings) {
		this.busTimings = busTimings;
	}
	public String getConsumerKey() {
		return consumerKey;
	}
	public void setConsumerKey(String consumerKey) {
		this.consumerKey = consumerKey;
	}
	public String getConsumerSecret() {
		return consumerSecret;
	}
	public void setConsumerSecret(String consumerSecret) {
		this.consumerSecret = consumerSecret;
	}
	
	
}

