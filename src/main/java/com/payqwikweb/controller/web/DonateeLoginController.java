package com.payqwikweb.controller.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.payqwikweb.api.IAuthenticationApi;
import com.payqwikweb.app.api.IDonateeAPI;
import com.payqwikweb.app.api.ILogoutApi;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.request.LoginRequest;
import com.payqwikweb.app.model.request.LogoutRequest;
import com.payqwikweb.app.model.response.LoginResponse;
import com.payqwikweb.app.model.response.LogoutResponse;
import com.payqwikweb.app.model.response.UserDetailsResponse;
import com.payqwikweb.model.error.LoginError;
import com.payqwikweb.util.Authorities;
import com.payqwikweb.util.ModelMapKey;
import com.payqwikweb.validation.LoginValidation;
@Controller
@RequestMapping("/Donatee")
public class DonateeLoginController {

	  private final IAuthenticationApi authenticationApi;
	  private final LoginValidation loginValidation;
	  private final IDonateeAPI donateeAPI;
	  private final ILogoutApi logoutApi;
	  
	
	 
	

	public DonateeLoginController(IAuthenticationApi authenticationApi, LoginValidation loginValidation,
			IDonateeAPI donateeAPI, ILogoutApi logoutApi) {
		super();
		this.authenticationApi = authenticationApi;
		this.loginValidation = loginValidation;
		this.donateeAPI = donateeAPI;
		this.logoutApi = logoutApi;
	}

	@RequestMapping(value="/Home",method= RequestMethod.GET)
	    public String getMerchantLoginPage(@RequestParam(value="msg",required=false) String message, HttpSession session, Model model){
	        LoginRequest dto  = new LoginRequest();
	        model.addAttribute("login",dto);
	        String sessionId = (String)session.getAttribute("msession");
	        if (message != null) {
	            model.addAttribute(ModelMapKey.MESSAGE, message);
	        }
	        if(sessionId != null) {
	            UserDetailsResponse response = authenticationApi.getUserDetailsFromSession(sessionId);
	            if(response != null) {
	                String authority = response.getAuthority();
	                if(authority != null) {
	                    if (authority.contains(Authorities.DONATEE) && authority.contains(Authorities.AUTHENTICATED)) {
	                        model.addAttribute("user",response);
	                        return "Donatee/Home";
	                    }
	                }
	            }
	        }
	        return "Donatee/Login";
	    }

	    @RequestMapping(value="/Home",method= RequestMethod.POST)
	    public String getMerchantLoginPage(@ModelAttribute("login") LoginRequest dto, HttpServletRequest request, HttpSession session, Model model){
	        dto.setIpAddress(request.getRemoteAddr());
	        LoginError error = loginValidation.checkMerchantLogin(dto);
	        if(error.isValid()){
	            LoginResponse loginResponse = donateeAPI.login(dto);
	            if(loginResponse.isSuccess()){
	                        model.addAttribute("user",loginResponse);
	                        session.setAttribute("msession",loginResponse.getSessionId());
	                        return "Donatee/Home";
	            }
	            model.addAttribute(ModelMapKey.MESSAGE,loginResponse.getMessage());
	        }
	        model.addAttribute("error",error);
	        return "redirect:/Donatee/Login";
	    }

	  //  public ResponseEntity<AllTransactionResponse> getTotalBalance()
	    @RequestMapping(value="/Logout",method= RequestMethod.GET)
	    public String logoutMerchant(HttpSession session,RedirectAttributes model){
	        String sessionId = (String)session.getAttribute("msession");
	        if(sessionId != null){
	            String authority = authenticationApi.getAuthorityFromSession(sessionId,Role.DONATEE);
	            if(authority != null) {
	                if (authority.contains(Authorities.DONATEE) && authority.contains(Authorities.AUTHENTICATED)) {
	                    LogoutRequest logoutRequest = new LogoutRequest();
	                    logoutRequest.setSessionId(sessionId);
	                    LogoutResponse logoutResponse = logoutApi.logout(logoutRequest, Role.DONATEE);
	                    if (logoutResponse.isSuccess()) {
	                        model.addFlashAttribute(ModelMapKey.MESSAGE, "You've been successfully logged out");
	                        session.invalidate();
	                        return "redirect:/Donatee/Home";
	                    }
	                }
	            }
	        }
	        return "Donatee/Login";
	    }

}
