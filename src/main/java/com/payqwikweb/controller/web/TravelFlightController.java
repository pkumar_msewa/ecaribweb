package com.payqwikweb.controller.web;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.payqwikweb.api.IAuthenticationApi;
import com.payqwikweb.api.IFlightApi;
import com.payqwikweb.api.IServiceCheckApi;
import com.payqwikweb.app.api.ILoadMoneyApi;
import com.payqwikweb.app.api.ITravelFlightApi;
import com.payqwikweb.app.metadatas.UrlMetadatas;
import com.payqwikweb.app.model.ResponseStatus;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.busdto.BookTicketReq;
import com.payqwikweb.app.model.flight.dto.FlightLegresArr;
import com.payqwikweb.app.model.flight.dto.PaymentType;
import com.payqwikweb.app.model.flight.request.FligthBookReq;
import com.payqwikweb.app.model.flight.response.FlightClityList;
import com.payqwikweb.app.model.flight.response.FlightResponseDTO;
import com.payqwikweb.app.model.request.CreateJsonRequestFlight;
import com.payqwikweb.app.model.request.FlightVnetRequest;
import com.payqwikweb.app.model.request.TravelFlightRequest;
import com.payqwikweb.app.model.response.EBSRedirectResponse;
import com.payqwikweb.app.model.response.FlightPriceCheckRequest;
import com.payqwikweb.app.model.response.FlightResponse;
import com.payqwikweb.app.model.response.LoadMoneyResponse;
import com.payqwikweb.app.model.response.VNetResponse;
import com.payqwikweb.app.model.response.VRedirectResponse;
import com.payqwikweb.model.app.request.FlightBookRequest;
import com.payqwikweb.model.app.request.LoadMoneyFlightRequest;
import com.payqwikweb.model.app.request.ReturnFlightBookRequest;
import com.payqwikweb.model.app.request.TravellerFlightDetails;
import com.payqwikweb.model.web.Status;
import com.payqwikweb.model.web.WEBSRedirectResponse;
import com.payqwikweb.util.Authorities;
import com.payqwikweb.util.CommonUtil;
import com.payqwikweb.util.ConvertUtil;
import com.payqwikweb.util.ModelMapKey;
import com.thirdparty.model.ResponseDTO;

@Controller
@RequestMapping("/User/Travel")
public class TravelFlightController implements MessageSourceAware {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	private MessageSource messageSource;
	private final ITravelFlightApi travelflightapi;
	private final IAuthenticationApi authenticationApi;
	private final ILoadMoneyApi loadMoneyApi;
	private final IServiceCheckApi serviceCheckApi;
	private final IFlightApi flightApi;

	public TravelFlightController(ITravelFlightApi travelflightapi, IAuthenticationApi authenticationApi,
			ILoadMoneyApi loadMoneyApi, IServiceCheckApi serviceCheckApi, IFlightApi flightApi) {
		this.travelflightapi = travelflightapi;
		this.authenticationApi = authenticationApi;
		this.loadMoneyApi = loadMoneyApi;
		this.serviceCheckApi = serviceCheckApi;
		this.flightApi = flightApi;
	}

	@Override
	public void setMessageSource(MessageSource arg0) {
		this.messageSource = messageSource;

	}

	@RequestMapping(method = RequestMethod.GET, value = "/Flight/Source")
	public String getavailebelbustrevelbus(HttpSession session) {
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				return "User/Travel/Flight/HomeNew";
			} else {
				return "redirect:/Home";
			}
		} else {
			return "redirect:/Home";
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "/Flight/OneWay")
	public String getFlightOneWay(HttpSession session, ModelMap model) {
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				boolean isInternational = (boolean) session.getAttribute("isInternational");
				model.addAttribute("isInternational", isInternational);
				return "User/Travel/Flight/flight-one-way";
			} else {
				return "redirect:/Home";
			}
		} else {
			return "redirect:/Home";
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "/Flight/RoundWay")
	public String getFlightRoundWay(HttpSession session) {
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				return "User/Travel/Flight/flight-roundtrip";
			} else {
				return "redirect:/Home";
			}
		} else {
			return "redirect:/Home";
		}
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(method = RequestMethod.POST, value = "/Flight/OneWay", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<String> getFlightOneWayPost(HttpSession session, @RequestBody TravelFlightRequest req) {
		boolean international = false;
		try {
			String sessionId = (String) session.getAttribute("sessionId");
			if (sessionId != null && sessionId.length() != 0) {
				String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
				if (authority != null) {
					Map<String, Object> flightMapData = null;
					String service = serviceCheckApi.checkServiceActive(sessionId);
					if (service != null && Status.Active.getValue().equalsIgnoreCase(service)) {
						String tripType = "";
						req.setSessionId(sessionId);
						req.setBeginDate(CommonUtil.formateSlashDateToDashDate(req.getBeginDate()));
						req.setEndDate(CommonUtil.formateSlashDateToDashDate(req.getEndDate()));
						String source = travelflightapi.getFlightSearchAPI(req);
						// need to implement checks if source is not available
						if (source != null && isValidJson(source)) {
							JSONObject flightMdexJson = new JSONObject(source);
							JSONObject flightDetailsJson = flightMdexJson.getJSONObject("details");
							ArrayList<FlightResponse> oneWayFlights = null;
							ArrayList<FlightResponse> roundWayFlights = null;
							if (isValidJson(flightDetailsJson.getString("journeys"))) {
								JSONArray totalJourney = flightDetailsJson.getJSONArray("journeys");
								flightMapData = flightApi.getFlightMapData(totalJourney.getJSONObject(0).getJSONArray("segments"));
								oneWayFlights = (ArrayList<FlightResponse>) flightMapData.get("flightList");
								tripType = (String) flightMapData.get("tripType");
								if (totalJourney.length() > 1 && !"internationalRoundWay".equals(tripType)) {
									flightMapData = flightApi.getFlightMapData(totalJourney.getJSONObject(1).getJSONArray("segments"));
									roundWayFlights = (ArrayList<FlightResponse>) flightMapData.get("flightList");
									tripType = "DomesticRoundway";
								}
							} else {
								return new ResponseEntity<String>(ResponseStatus.JSON_EXCEPTION.getValue() + "#"
										+ ResponseStatus.JSON_EXCEPTION.getKey(), HttpStatus.OK);
							}
							// Session values after search operation
							session.setAttribute("flightResponse", source);
							session.setAttribute("isInternational", international);
							session.setAttribute("adultsarr", flightApi.getPassengerList(req.getAdults()));
							session.setAttribute("childsarr", flightApi.getPassengerList(req.getChilds()));
							session.setAttribute("infantsarr", flightApi.getPassengerList(req.getInfants()));
							session.setAttribute("totaltraveller", Integer.parseInt(req.getAdults())
									+ Integer.parseInt(req.getChilds()) + Integer.parseInt(req.getInfants()));
							session.setAttribute("flightNumbersotpage",
									(ArrayList<FlightResponse>) flightMapData.get("noOfStopage"));
							session.setAttribute("flightnumberdata",
									(ArrayList<FlightResponse>) flightMapData.get("flightNumber"));
							session.setAttribute("flightdata", oneWayFlights);
							session.setAttribute("flightdatareturn", roundWayFlights);
							session.setAttribute("journeyTime", flightMapData.get("journeyTime"));
							session.setAttribute("minAmt", flightApi.getMinAmount(oneWayFlights, roundWayFlights));
							session.setAttribute("maxAmt", flightApi.getMaxAmount(oneWayFlights, roundWayFlights));
							international = travelflightapi.compareCountry(req);

							// Request input session values
							session.setAttribute("srcHtml", req.getSrcFullName());
							session.setAttribute("destHtml", req.getDestFullName());
							session.setAttribute("bDate", req.getBeginDate());
							session.setAttribute("eDate", req.getEndDate());
							session.setAttribute("adults", req.getAdults());
							session.setAttribute("childs", req.getChilds());
							session.setAttribute("infants", req.getInfants());
							session.setAttribute("beginDate", req.getBeginDate());
							session.setAttribute("endDate", req.getEndDate());
							session.setAttribute("source", req.getOrigin());
							session.setAttribute("destination", req.getDestination());
							session.setAttribute("triptype", req.getTripType());
							session.setAttribute("origin", req.getOrigin());
							session.setAttribute("destination", req.getDestination());
							session.setAttribute("cabinType", req.getCabin());
							return new ResponseEntity<String>("S00" + "#" + tripType, HttpStatus.OK);
						} else {
							return new ResponseEntity<String>(ResponseStatus.JSON_EXCEPTION.getValue() + "#"
									+ ResponseStatus.JSON_EXCEPTION.getKey(), HttpStatus.OK);
						}
					} else {
						return new ResponseEntity<String>(ResponseStatus.FAILURE.getValue() + "#" + "Service is under maintenance",HttpStatus.OK);
					}
				} else {
					return new ResponseEntity<String>(ResponseStatus.INVALID_SESSION.getValue(), HttpStatus.OK);
				}
			} else {
				return new ResponseEntity<String>(ResponseStatus.INVALID_SESSION.getValue(), HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e);
			return new ResponseEntity<String>("F02", HttpStatus.OK);
		}
	}

	private boolean isValidJson(String flightJson) {
		try {
			new JSONObject(flightJson);
		} catch (JSONException e) {
			try {
				new JSONArray(flightJson);
			} catch (JSONException e2) {
				return false;
			}
		}
		return true;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/Flight/internationalRoundWay")
	public String getFlightinternationalRoundWay(HttpSession session) {
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {

				String source = (String) session.getAttribute("source");
				String dest = (String) session.getAttribute("destination");

				session.setAttribute("source", source);
				session.setAttribute("destination", dest);

				return "User/Travel/Flight/internationalRoundWay";
			} else {
				return "redirect:/Home";
			}
		} else {
			return "redirect:/Home";
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "/Flight/CheckOut")
	public String getFlightCheckOut(HttpSession session, Model model) {
		String sessionId = (String) session.getAttribute("sessionId");
		try {

			if (sessionId != null && sessionId.length() != 0) {

				String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
				if (authority != null) {

					List<FlightLegresArr> flightLegresArrs = new ArrayList<>();

					String airRePriceRQ = "" + session.getAttribute("airRePriceRQ");
					String userflightamount = "";
					JSONObject obj = new JSONObject(airRePriceRQ);

					JSONArray journeys = obj.getJSONObject("details").getJSONArray("journeys");

					JSONArray segments = journeys.getJSONObject(0).getJSONArray("segments");

					JSONArray bonds = segments.getJSONObject(0).getJSONArray("bonds");
					JSONArray fare = segments.getJSONObject(0).getJSONArray("fare");
					String baseFare= fare.getJSONObject(0).getString("basicFare");
					for (int k = 0; k < bonds.length(); k++) {
						JSONArray paxFares = fare.getJSONObject(k).getJSONArray("paxFares");

						JSONArray legs = bonds.getJSONObject(k).getJSONArray("legs");
						String airlineName = "" + legs.getJSONObject(k).getString("airlineName");
						String arrivalTime = "" + legs.getJSONObject(k).getString("arrivalTime");
						String departureDate = "" + legs.getJSONObject(k).getString("departureDate");
						String departureTime = "" + legs.getJSONObject(k).getString("departureTime");
						String destination = "" + legs.getJSONObject(k).getString("destination");
						String duration = "" + legs.getJSONObject(k).getString("duration");
						String flightNumber = "" + legs.getJSONObject(k).getString("flightNumber");
						String origin = "" + legs.getJSONObject(k).getString("origin");
						String arrivalDate = "" + legs.getJSONObject(k).getString("arrivalDate");
						String TotalFare = "" + paxFares.getJSONObject(k).getString("totalFare");
						String TotalTax = "" + paxFares.getJSONObject(k).getString("totalTax");
						String TotalFareWithOutMarkUp = "" + fare.getJSONObject(k).getString("totalFareWithOutMarkUp");

						double amount = Double.parseDouble(TotalFareWithOutMarkUp) + 200;

						userflightamount += "" + origin + "#" + destination + "#" + arrivalDate + "#" + departureDate
								+ "#" + duration + "#" + arrivalTime + "#" + departureTime + "#" + flightNumber + "#"
								+ airlineName + "#" + TotalFare + "#" + TotalTax + "#" + amount+ "#" + baseFare;

						for (int i = 0; i < legs.length(); i++) {

							FlightLegresArr flightLegresArr = new FlightLegresArr();
							flightLegresArr.setArrivalTime("" + legs.getJSONObject(i).getString("arrivalTime"));
							flightLegresArr.setDepartureTime("" + legs.getJSONObject(i).getString("departureTime"));
							flightLegresArr.setOrigin("" + legs.getJSONObject(i).getString("origin"));
							flightLegresArr.setDestination("" + legs.getJSONObject(i).getString("destination"));
							flightLegresArr.setDuration("" + legs.getJSONObject(i).getString("duration"));
							flightLegresArr.setFlightNumber("" + legs.getJSONObject(i).getString("flightNumber"));
							flightLegresArr.setAirlineName("" + legs.getJSONObject(i).getString("airlineName"));

							flightLegresArrs.add(flightLegresArr);
						}

					}
					session.setAttribute("checkoutDeatil", userflightamount);
					model.addAttribute("checkoutDeatilForLGS", flightLegresArrs);

					boolean isInternational = (boolean) session.getAttribute("isInternational");

					if (isInternational) {
						return "User/Travel/Flight/flightIntOneWayCheckout";
					} else {
						return "User/Travel/Flight/flight-checkout";
					}
				} else {
					return "redirect:/Home";
				}
			} else {
				return "redirect:/Home";
			}

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e);
			return "User/Travel/Flight/Home";
		}

	}

	@RequestMapping(method = RequestMethod.GET, value = "/Flight/RoundwayCheckOut")
	public String getFlightRoundwayCheckOut(HttpSession session, Model model) {
		String sessionId = (String) session.getAttribute("sessionId");
		double baseFare=0;
		try {

			if (sessionId != null && sessionId.length() != 0) {
				String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
				if (authority != null) {

					List<FlightLegresArr> flightLegresArrs = new ArrayList<>();

					List<FlightLegresArr> flightLegresArrsReturn = new ArrayList<>();

					String Firstfligt = "" + session.getAttribute("Firstfligt");
					String Secondflight = "" + session.getAttribute("Secondflight");
					if (Firstfligt.equals("Firstfligt")) {
						String airRePriceRQ = "" + session.getAttribute("airRePriceRQoneway");
						String userflightamount = "";
						JSONObject obj = new JSONObject(airRePriceRQ);

						JSONArray journeys = obj.getJSONObject("details").getJSONArray("journeys");

						JSONArray segments = journeys.getJSONObject(0).getJSONArray("segments");

						JSONArray bonds = segments.getJSONObject(0).getJSONArray("bonds");
						JSONArray fare = segments.getJSONObject(0).getJSONArray("fare");
						baseFare= fare.getJSONObject(0).getDouble("basicFare");
						for (int k = 0; k < bonds.length(); k++) {
							JSONArray paxFares = fare.getJSONObject(k).getJSONArray("paxFares");

							JSONArray legs = bonds.getJSONObject(k).getJSONArray("legs");
							String airlineName = "" + legs.getJSONObject(k).getString("airlineName");
							String arrivalTime = "" + legs.getJSONObject(k).getString("arrivalTime");
							String departureDate = "" + legs.getJSONObject(k).getString("departureDate");
							String departureTime = "" + legs.getJSONObject(k).getString("departureTime");
							String destination = "" + legs.getJSONObject(k).getString("destination");
							String duration = "" + legs.getJSONObject(k).getString("duration");
							String flightNumber = "" + legs.getJSONObject(k).getString("flightNumber");
							String origin = "" + legs.getJSONObject(k).getString("origin");
							String arrivalDate = "" + legs.getJSONObject(k).getString("arrivalDate");
							String TotalFare = "" + paxFares.getJSONObject(k).getString("totalFare");
							String TotalTax = "" + paxFares.getJSONObject(k).getString("totalTax");
							String TotalFareWithOutMarkUp = ""
									+ fare.getJSONObject(k).getString("totalFareWithOutMarkUp");
							userflightamount += "" + origin + "#" + destination + "#" + arrivalDate + "#"
									+ departureDate + "#" + duration + "#" + arrivalTime + "#" + departureTime + "#"
									+ flightNumber + "#" + airlineName + "#" + TotalFare + "#" + TotalTax + "#"
									+ TotalFareWithOutMarkUp;

							for (int i = 0; i < legs.length(); i++) {

								FlightLegresArr flightLegresArr = new FlightLegresArr();
								flightLegresArr.setArrivalTime("" + legs.getJSONObject(i).getString("arrivalTime"));
								flightLegresArr.setDepartureTime("" + legs.getJSONObject(i).getString("departureTime"));
								flightLegresArr.setOrigin("" + legs.getJSONObject(i).getString("origin"));
								flightLegresArr.setDestination("" + legs.getJSONObject(i).getString("destination"));
								flightLegresArr.setDuration("" + legs.getJSONObject(i).getString("duration"));
								flightLegresArr.setFlightNumber("" + legs.getJSONObject(i).getString("flightNumber"));
								flightLegresArr.setAirlineName("" + legs.getJSONObject(i).getString("airlineName"));

								flightLegresArrs.add(flightLegresArr);
							}

						}
						session.setAttribute("checkoutDeatil", userflightamount);

						System.err.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%" + userflightamount);
					}
					if (Secondflight.equals("Secondflight")) {
						String airRePriceRQ = "" + session.getAttribute("airRePriceRQroundway");
						String userflightamount = "";
						JSONObject obj = new JSONObject(airRePriceRQ);

						JSONArray journeys = obj.getJSONObject("details").getJSONArray("journeys");

						JSONArray segments = journeys.getJSONObject(0).getJSONArray("segments");

						JSONArray bonds = segments.getJSONObject(0).getJSONArray("bonds");
						JSONArray fare = segments.getJSONObject(0).getJSONArray("fare");
						baseFare =baseFare + fare.getJSONObject(0).getDouble("basicFare");
						for (int k = 0; k < bonds.length(); k++) {
							JSONArray paxFares = fare.getJSONObject(k).getJSONArray("paxFares");

							JSONArray legs = bonds.getJSONObject(k).getJSONArray("legs");
							String airlineName = "" + legs.getJSONObject(k).getString("airlineName");
							String arrivalTime = "" + legs.getJSONObject(k).getString("arrivalTime");
							String departureDate = "" + legs.getJSONObject(k).getString("departureDate");
							String departureTime = "" + legs.getJSONObject(k).getString("departureTime");
							String destination = "" + legs.getJSONObject(k).getString("destination");
							String duration = "" + legs.getJSONObject(k).getString("duration");
							String flightNumber = "" + legs.getJSONObject(k).getString("flightNumber");
							String origin = "" + legs.getJSONObject(k).getString("origin");
							String arrivalDate = "" + legs.getJSONObject(k).getString("arrivalDate");
							String TotalFare = "" + paxFares.getJSONObject(k).getString("totalFare");
							String TotalTax = "" + paxFares.getJSONObject(k).getString("totalTax");
							String TotalFareWithOutMarkUp = ""
									+ fare.getJSONObject(k).getString("totalFareWithOutMarkUp");
							userflightamount += "" + origin + "#" + destination + "#" + arrivalDate + "#"
									+ departureDate + "#" + duration + "#" + arrivalTime + "#" + departureTime + "#"
									+ flightNumber + "#" + airlineName + "#" + TotalFare + "#" + TotalTax + "#"
									+ TotalFareWithOutMarkUp;

							for (int i = 0; i < legs.length(); i++) {

								FlightLegresArr flightLegresArr = new FlightLegresArr();
								flightLegresArr.setArrivalTime("" + legs.getJSONObject(i).getString("arrivalTime"));
								flightLegresArr.setDepartureTime("" + legs.getJSONObject(i).getString("departureTime"));
								flightLegresArr.setOrigin("" + legs.getJSONObject(i).getString("origin"));
								flightLegresArr.setDestination("" + legs.getJSONObject(i).getString("destination"));
								flightLegresArr.setDuration("" + legs.getJSONObject(i).getString("duration"));
								flightLegresArr.setFlightNumber("" + legs.getJSONObject(i).getString("flightNumber"));
								flightLegresArr.setAirlineName("" + legs.getJSONObject(i).getString("airlineName"));

								flightLegresArrsReturn.add(flightLegresArr);
							}

						}
						System.err.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%" + userflightamount);
						session.setAttribute("returncheckoutDeatil", userflightamount);
						model.addAttribute("baseFare", baseFare);
						model.addAttribute("arrivalLGS", flightLegresArrs);
						model.addAttribute("deptLGS", flightLegresArrsReturn);
					}
				} else {
					return "redirect:/Home";
				}
			} else {
				return "redirect:/Home";
			}

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e);
			return "User/Travel/Flight/Home";
		}
		return "User/Travel/Flight/flight-roundway-checkout";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/Flight/InternationalCheckOut")
	public String getFlightCheckOutInternationalCheckOut(HttpSession session, Model model) {
		String sessionId = (String) session.getAttribute("sessionId");
		try {
			String TotalFareWithOutMarkUp = "";
			if (sessionId != null && sessionId.length() != 0) {

				String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
				if (authority != null) {

					List<FlightLegresArr> flightLegresArrs = new ArrayList<>();

					String airRePriceRQ = "" + session.getAttribute("airRePriceRQinternational");
					String userflightamount = "";
					JSONObject obj = new JSONObject(airRePriceRQ);

					JSONArray journeys = obj.getJSONObject("details").getJSONArray("journeys");

					JSONArray segments = journeys.getJSONObject(0).getJSONArray("segments");

					JSONArray bonds = segments.getJSONObject(0).getJSONArray("bonds");
					JSONArray fare = segments.getJSONObject(0).getJSONArray("fare");

					for (int i = 0; i < bonds.length(); i++) {

						JSONArray legs = bonds.getJSONObject(i).getJSONArray("legs");
						for (int k = 0; k < legs.length(); k++) {

							String airlineName = "" + legs.getJSONObject(k).getString("airlineName");
							String arrivalTime = "" + legs.getJSONObject(k).getString("arrivalTime");
							String departureDate = "" + legs.getJSONObject(k).getString("departureDate");
							String departureTime = "" + legs.getJSONObject(k).getString("departureTime");
							String destination = "" + legs.getJSONObject(k).getString("destination");
							String duration = "" + legs.getJSONObject(k).getString("duration");
							String flightNumber = "" + legs.getJSONObject(k).getString("flightNumber");
							String origin = "" + legs.getJSONObject(k).getString("origin");
							String arrivalDate = "" + legs.getJSONObject(k).getString("arrivalDate");

							userflightamount += "" + arrivalDate + "#" + departureDate + "#" + duration + "#"
									+ departureTime + "#" + arrivalTime + "#" + airlineName;
						}

						for (int x = 0; x < legs.length(); x++) {

							FlightLegresArr flightLegresArr = new FlightLegresArr();
							flightLegresArr.setArrivalTime("" + legs.getJSONObject(x).getString("arrivalTime"));
							flightLegresArr.setDepartureTime("" + legs.getJSONObject(x).getString("departureTime"));
							flightLegresArr.setOrigin("" + legs.getJSONObject(x).getString("origin"));
							flightLegresArr.setDestination("" + legs.getJSONObject(x).getString("destination"));
							flightLegresArr.setDuration("" + legs.getJSONObject(x).getString("duration"));
							flightLegresArr.setFlightNumber("" + legs.getJSONObject(x).getString("flightNumber"));
							flightLegresArr.setAirlineName("" + legs.getJSONObject(x).getString("airlineName"));
							flightLegresArr.setFlightName("" + legs.getJSONObject(x).getString("flightName"));

							flightLegresArrs.add(flightLegresArr);
						}

					}
					for (int kk = 0; kk < fare.length(); kk++) {
						TotalFareWithOutMarkUp = "" + fare.getJSONObject(kk).getString("totalFareWithOutMarkUp");

					}

					double amt = Double.parseDouble(TotalFareWithOutMarkUp) + 200;

					session.setAttribute("checkoutDeatilintenational", userflightamount + "@" + amt);
					model.addAttribute("checkoutDeatilForLGS", flightLegresArrs);
				} else {
					return "redirect:/Home";
				}
			} else {
				return "redirect:/Home";
			}

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e);
		}
		return "User/Travel/Flight/flightinternationalcheckout";
	}

	// Get Method finish

	@RequestMapping(method = RequestMethod.POST, value = "/Flight/ShowPriceDetail", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<String> getShowPriceDetail(HttpSession session, @RequestBody FlightPriceCheckRequest req) {
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				try {

					String source = "" + session.getAttribute("flightResponse");
					JSONObject obj = new JSONObject(source);
					JSONArray journeys = obj.getJSONObject("details").getJSONArray("journeys");
					JSONArray segments = journeys.getJSONObject(0).getJSONArray("segments");
					for (int j = 0; j < segments.length(); j++) {

						JSONArray bonds = segments.getJSONObject(j).getJSONArray("bonds");
						for (int k = 0; k < bonds.length(); k++) {

							JSONArray legs = bonds.getJSONObject(k).getJSONArray("legs");
							String flightnumber = legs.getJSONObject(k).getString("flightNumber");
							if (flightnumber.equals(req.getFlightNumber())) {

								req.setEngineID("" + segments.getJSONObject(j).getString("engineID"));
								req.setAirlineName("" + legs.getJSONObject(k).getString("airlineName"));
								req.setArrivalTime("" + legs.getJSONObject(k).getString("arrivalTime"));
								req.setCabin("" + legs.getJSONObject(k).getString("cabin"));
								req.setDepartureTime("" + legs.getJSONObject(k).getString("departureTime"));
								req.setDestination("" + legs.getJSONObject(k).getString("destination"));
								req.setDuration("" + legs.getJSONObject(k).getString("duration"));
								req.setBoundType("" + legs.getJSONObject(k).getString("boundType"));
								req.setFlightNumber("" + legs.getJSONObject(k).getString("flightNumber"));
								req.setOrigin("" + legs.getJSONObject(k).getString("origin"));
								req.setAdults("" + session.getAttribute("adults"));
								req.setChilds("" + session.getAttribute("childs"));
								req.setInfants("" + session.getAttribute("infants"));
								req.setBeginDate("" + session.getAttribute("beginDate"));
								req.setFlightName("" + legs.getJSONObject(k).getString("airlineName"));
								req.setTripType("" + session.getAttribute("triptype"));
								req.setArrivalDate("" + legs.getJSONObject(k).getString("arrivalDate"));
								req.setDepartureDate("" + legs.getJSONObject(k).getString("departureDate"));
								req.setDepartureTerminal("" + legs.getJSONObject(k).getString("departureTerminal"));
								req.setArrivalTerminal("" + legs.getJSONObject(k).getString("arrivalTerminal"));
								req.setCapacity("" + legs.getJSONObject(k).getString("capacity"));
								req.setCarrierCode("" + legs.getJSONObject(k).getString("carrierCode"));
								req.setCurrencyCode("" + legs.getJSONObject(k).getString("currencyCode"));
								req.setBaggageUnit("" + legs.getJSONObject(k).getString("baggageUnit"));
								req.setBaggageWeight("" + legs.getJSONObject(k).getString("baggageWeight"));
								req.setFareClassOfService("" + legs.getJSONObject(k).getString("fareClassOfService"));
								req.setSold("" + legs.getJSONObject(k).getString("sold"));
								req.setStatus("" + legs.getJSONObject(k).getString("status"));
								req.setItineraryKey("" + segments.getJSONObject(j).getString("itineraryKey"));
								req.setBondType("" + segments.getJSONObject(j).getString("bondType"));
								req.setSearchId("" + segments.getJSONObject(j).getString("searchId"));
								req.setEngineID("" + segments.getJSONObject(j).getString("engineID"));
								req.setFareRule("" + segments.getJSONObject(j).getString("fareRule"));
								req.setJourneyTime("" + legs.getJSONObject(k).getString("duration"));
								req.setJourneyIndex("" + segments.getJSONObject(j).getString("journeyIndex"));

								JSONArray fare = segments.getJSONObject(j).getJSONArray("fare");
								for (int l = 0; l < fare.length(); l++) {
									JSONArray paxFares = fare.getJSONObject(l).getJSONArray("paxFares");
									JSONArray fares = paxFares.getJSONObject(l).getJSONArray("fares");

									req.setTotalFare("" + paxFares.getJSONObject(l).getString("totalFare"));
									req.setTotalTax("" + paxFares.getJSONObject(l).getString("totalTax"));
									req.setTotalFareWithOutMarkUp(
											"" + fare.getJSONObject(l).getString("totalFareWithOutMarkUp"));
									req.setTotalTaxWithOutMarkUp(
											"" + fare.getJSONObject(l).getString("totalTaxWithOutMarkUp"));
									req.setAmount("" + fares.getJSONObject(l).getString("amount"));

									req.setChargeCode("" + fares.getJSONObject(l).getString("chargeCode"));
									req.setChargeType("" + fares.getJSONObject(l).getString("chargeType"));

									req.setBasicFare("" + fare.getJSONObject(l).getString("basicFare"));
									req.setCancelPenalty("" + paxFares.getJSONObject(l).getString("cancelPenalty"));
									req.setChangePenalty("" + paxFares.getJSONObject(l).getString("changePenalty"));
									req.setTransactionAmount(
											"" + paxFares.getJSONObject(l).getString("transactionAmount"));
									req.setBaseTransactionAmount(
											"" + paxFares.getJSONObject(l).getString("baseTransactionAmount"));
									req.setTraceId("AYTM00011111111110002");

									req.setExchangeRate("" + fare.getJSONObject(l).getString("exchangeRate"));

									req.setMarkUP("" + paxFares.getJSONObject(l).getString("markUP"));
									req.setPaxType("" + paxFares.getJSONObject(l).getString("paxType"));

								}

							}
						}
					}

					// String airRePriceRQ = travelflightapi.AirRePriceRQ(req);

					String airRePriceRQ = travelflightapi.AirRePriceRQConnecting(req, source,
							"" + session.getAttribute("destination"), session);

					String userflightamount = "";
					JSONObject airRePriceRQobj = new JSONObject(airRePriceRQ);
					String codeairRePriceRQobj = airRePriceRQobj.getString("code");
					if (codeairRePriceRQobj.equalsIgnoreCase("S00")) {
						session.setAttribute("airRePriceRQ", airRePriceRQ);

						JSONArray journeys11 = airRePriceRQobj.getJSONObject("details").getJSONArray("journeys");
						JSONArray segments11 = journeys11.getJSONObject(0).getJSONArray("segments");

						JSONArray bonds = segments11.getJSONObject(0).getJSONArray("bonds");

						JSONObject obj22 = CreateJsonRequestFlight.createjsonForTicket(bonds);
						System.err.println(obj22);
						session.setAttribute("ticketDetails", obj22);

						JSONArray airRePriceRQjourneys = airRePriceRQobj.getJSONObject("details")
								.getJSONArray("journeys");

						JSONArray airRePriceRQsegments = airRePriceRQjourneys.getJSONObject(0).getJSONArray("segments");
						for (int j = 0; j < airRePriceRQsegments.length(); j++) {
							JSONArray fare = airRePriceRQsegments.getJSONObject(j).getJSONArray("fare");
							for (int l = 0; l < fare.length(); l++) {
								JSONArray paxFares = fare.getJSONObject(l).getJSONArray("paxFares");
								JSONArray fares = paxFares.getJSONObject(l).getJSONArray("fares");

								String TotalFare = "" + fare.getJSONObject(l).getString("basicFare");
								String TotalTax = "" + fare.getJSONObject(l).getString("totalTaxWithOutMarkUp");
								String TotalFareWithOutMarkUp = ""
										+ fare.getJSONObject(l).getString("totalFareWithOutMarkUp");

								double amt = Double.parseDouble(TotalFareWithOutMarkUp) + 200;

								String CancelPenalty = "" + paxFares.getJSONObject(l).getString("cancelPenalty");
								String ChangePenalty = "" + paxFares.getJSONObject(l).getString("changePenalty");
								userflightamount += "#" + TotalFare + "#" + TotalTax + "#" + amt + "#" + CancelPenalty
										+ "#" + ChangePenalty;
							}

						}
						return new ResponseEntity<String>("S00" + "@" + userflightamount, HttpStatus.OK);
					} else {
						return new ResponseEntity<String>("F04" + "@" + "Detail", HttpStatus.OK);
					}

				} catch (Exception e) {
					e.printStackTrace();
					System.out.println(e);
					return new ResponseEntity<String>("F04" + "@" + "Detail", HttpStatus.OK);
				}
			} else {
				return new ResponseEntity<String>("F03" + "@" + "Detail", HttpStatus.OK);
			}
		} else {
			return new ResponseEntity<String>("F03" + "@" + "Detail", HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/Flight/ConnectingShowPriceDetail", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<String> getShowPriceDetailConnecting(HttpSession session,
			@RequestBody FlightPriceCheckRequest req) {
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				try {

					String source = "" + session.getAttribute("flightResponse");

					String airRePriceRQ = travelflightapi.AirRePriceRQConnecting(req, source,
							"" + session.getAttribute("destination"), session);

					String userflightamount = "";
					JSONObject airRePriceRQobj = new JSONObject(airRePriceRQ);
					String codeairRePriceRQobj = airRePriceRQobj.getString("code");
					if (codeairRePriceRQobj.equalsIgnoreCase("S00")) {
						session.setAttribute("airRePriceRQ", airRePriceRQ);
						JSONArray journeys11 = airRePriceRQobj.getJSONObject("details").getJSONArray("journeys");
						JSONArray segments11 = journeys11.getJSONObject(0).getJSONArray("segments");
						JSONArray bonds = segments11.getJSONObject(0).getJSONArray("bonds");
						JSONObject obj22 = CreateJsonRequestFlight.createjsonForTicket(bonds);
						System.err.println(obj22);
						session.setAttribute("ticketDetails", obj22);
						JSONArray airRePriceRQjourneys = airRePriceRQobj.getJSONObject("details")
								.getJSONArray("journeys");

						JSONArray airRePriceRQsegments = airRePriceRQjourneys.getJSONObject(0).getJSONArray("segments");
						for (int j = 0; j < airRePriceRQsegments.length(); j++) {
							JSONArray fare = airRePriceRQsegments.getJSONObject(j).getJSONArray("fare");
							for (int l = 0; l < fare.length(); l++) {
								JSONArray paxFares = fare.getJSONObject(l).getJSONArray("paxFares");
								JSONArray fares = paxFares.getJSONObject(l).getJSONArray("fares");

								String TotalFare = "" + fare.getJSONObject(l).getString("basicFare");
								String TotalTax = "" + paxFares.getJSONObject(l).getString("totalTax");
								String TotalFareWithOutMarkUp = ""
										+ fare.getJSONObject(l).getString("totalFareWithOutMarkUp");

								double amt = Double.parseDouble(TotalFareWithOutMarkUp) + 200;

								String CancelPenalty = "" + paxFares.getJSONObject(l).getString("cancelPenalty");
								String ChangePenalty = "" + paxFares.getJSONObject(l).getString("changePenalty");
								userflightamount += "#" + TotalFare + "#" + TotalTax + "#" + amt + "#" + CancelPenalty
										+ "#" + ChangePenalty;
							}

						}
						return new ResponseEntity<String>("S00" + "@" + userflightamount, HttpStatus.OK);
					} else {
						return new ResponseEntity<String>("F04" + "@" + "Detail", HttpStatus.OK);
					}

				} catch (Exception e) {
					e.printStackTrace();
					System.out.println(e);
					return new ResponseEntity<String>("F04" + "@" + "Detail", HttpStatus.OK);
				}
			} else {
				return new ResponseEntity<String>("F03" + "@" + "Detail", HttpStatus.OK);
			}
		} else {
			return new ResponseEntity<String>("F03" + "@" + "Detail", HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/Flight/internationalConnectingShowPriceDetail", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<String> getShowPriceDetailConnectinginternational(HttpSession session,
			@RequestBody FlightPriceCheckRequest req) throws Exception {
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				try {

					String source = "" + session.getAttribute("flightResponse");
					String airRePriceRQ = travelflightapi.AirRePriceRQConnecting(req, source,
							"" + session.getAttribute("destination"), session);

					String userflightamount = "";
					JSONObject airRePriceRQobj = new JSONObject(airRePriceRQ);
					String codeairRePriceRQobj = airRePriceRQobj.getString("code");
					if (codeairRePriceRQobj.equalsIgnoreCase("S00")) {
						session.setAttribute("airRePriceRQinternational", airRePriceRQ);

						JSONArray journeys11 = airRePriceRQobj.getJSONObject("details").getJSONArray("journeys");
						JSONArray segments11 = journeys11.getJSONObject(0).getJSONArray("segments");
						JSONArray bonds = segments11.getJSONObject(0).getJSONArray("bonds");
						JSONObject obj221 = CreateJsonRequestFlight.createjsonForTicket(bonds);
						System.err.println(obj221);
						session.setAttribute("ticketDetails", obj221);
						JSONArray airRePriceRQjourneys = airRePriceRQobj.getJSONObject("details")
								.getJSONArray("journeys");

						JSONArray airRePriceRQsegments = airRePriceRQjourneys.getJSONObject(0).getJSONArray("segments");
						for (int j = 0; j < airRePriceRQsegments.length(); j++) {
							JSONArray fare = airRePriceRQsegments.getJSONObject(j).getJSONArray("fare");
							for (int l = 0; l < fare.length(); l++) {
								JSONArray paxFares = fare.getJSONObject(l).getJSONArray("paxFares");
								JSONArray fares = paxFares.getJSONObject(l).getJSONArray("fares");

								String TotalFare = "" + paxFares.getJSONObject(l).getString("totalFare");
								String TotalTax = "" + paxFares.getJSONObject(l).getString("totalTax");
								String TotalFareWithOutMarkUp = ""
										+ fare.getJSONObject(l).getString("totalFareWithOutMarkUp");

								double amt = Double.parseDouble(TotalFareWithOutMarkUp) + 200;

								String CancelPenalty = "" + paxFares.getJSONObject(l).getString("cancelPenalty");
								String ChangePenalty = "" + paxFares.getJSONObject(l).getString("changePenalty");
								userflightamount += "#" + TotalFare + "#" + TotalTax + "#" + amt + "#" + CancelPenalty
										+ "#" + ChangePenalty;
							}

						}
						return new ResponseEntity<String>("S00" + "@" + userflightamount, HttpStatus.OK);
					} else {
						return new ResponseEntity<String>("F04" + "@" + "Detail", HttpStatus.OK);
					}

				} catch (Exception e) {
					e.printStackTrace();
					System.out.println(e);
					return new ResponseEntity<String>("F04" + "@" + "Detail", HttpStatus.OK);
				}
			} else {
				return new ResponseEntity<String>("F03" + "@" + "Detail", HttpStatus.OK);
			}
		} else {
			return new ResponseEntity<String>("F03" + "@" + "Detail", HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/Flight/ShowPriceDetailRoundTrip", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<String> getShowPriceDetailRoundTrip(HttpSession session,
			@RequestBody FlightPriceCheckRequest req) {
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				try {
					JSONObject firstTicket = null;
					JSONObject secoundTicket = null;

					String sources = null;
					String source = "" + session.getAttribute("flightResponse");

					String airRePriceRQ = travelflightapi.AirRePriceRQConnecting(req, source,
							"" + session.getAttribute("destination"), session);

					String userflightamount = "";
					JSONObject airRePriceRQobj = new JSONObject(airRePriceRQ);
					String codeairRePriceRQobj = airRePriceRQobj.getString("code");
					if (codeairRePriceRQobj.equalsIgnoreCase("S00")) {
						if (req.getOnewayflight().equals("Firstfligt")) {
							session.setAttribute("Firstfligt", req.getOnewayflight());
							session.setAttribute("airRePriceRQoneway", airRePriceRQ);
						}
						if (req.getOnewayflight().equals("Secondflight")) {

							session.setAttribute("Secondflight", req.getOnewayflight());
							session.setAttribute("airRePriceRQroundway", airRePriceRQ);
						}

						session.setAttribute("ticketDetails", firstTicket + "" + secoundTicket);

						JSONArray airRePriceRQjourneys = airRePriceRQobj.getJSONObject("details")
								.getJSONArray("journeys");

						JSONArray airRePriceRQsegments = airRePriceRQjourneys.getJSONObject(0).getJSONArray("segments");
						for (int j = 0; j < airRePriceRQsegments.length(); j++) {
							JSONArray fare = airRePriceRQsegments.getJSONObject(j).getJSONArray("fare");
							for (int l = 0; l < fare.length(); l++) {
								JSONArray paxFares = fare.getJSONObject(l).getJSONArray("paxFares");
								JSONArray fares = paxFares.getJSONObject(l).getJSONArray("fares");

								String TotalFare = "" + paxFares.getJSONObject(l).getString("totalFare");
								String TotalTax = "" + paxFares.getJSONObject(l).getString("totalTax");
								String TotalFareWithOutMarkUp = ""
										+ fare.getJSONObject(l).getString("totalFareWithOutMarkUp");

								String CancelPenalty = "" + paxFares.getJSONObject(l).getString("cancelPenalty");
								String ChangePenalty = "" + paxFares.getJSONObject(l).getString("changePenalty");
								userflightamount += "#" + TotalFare + "#" + TotalTax + "#" + TotalFareWithOutMarkUp
										+ "#" + CancelPenalty + "#" + ChangePenalty;
							}

						}
						return new ResponseEntity<String>("S00" + "@" + userflightamount, HttpStatus.OK);
					} else {
						return new ResponseEntity<String>("F04" + "@" + "Detail", HttpStatus.OK);
					}

				} catch (Exception e) {
					e.printStackTrace();
					System.err.println(e.getMessage());
					return new ResponseEntity<String>("F04" + "@" + "Detail", HttpStatus.OK);
				}
			} else {
				return new ResponseEntity<String>("F03" + "@" + "Detail", HttpStatus.OK);
			}
		} else {
			return new ResponseEntity<String>("F03" + "@" + "Detail", HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/Flight/ShowPriceConnectingDetailRoundTrip", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<String> getShowPriceDetailRoundTripConnecting(HttpSession session,
			@RequestBody FlightPriceCheckRequest req) {
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				try {
					String sources = null;
					if (req.getOnewayflight().equals("Firstfligt")) {
						String source = "" + session.getAttribute("flightResponse");
						sources = "" + session.getAttribute("outSegments");
						JSONObject obj = new JSONObject(source);
						int numberofarray = 0;
						ArrayList<FlightResponse> flightresponsearr = new ArrayList<FlightResponse>();
						JSONArray journeys = obj.getJSONObject("details").getJSONArray("journeys");
						for (int x = 0; x < journeys.length(); x++) {
							JSONArray segments = journeys.getJSONObject(0).getJSONArray("segments");
							for (int j = 0; j < segments.length(); j++) {

								JSONArray bonds = segments.getJSONObject(j).getJSONArray("bonds");
								for (int k = 0; k < bonds.length(); k++) {

									JSONArray legs = bonds.getJSONObject(k).getJSONArray("legs");
									String flightnumber = legs.getJSONObject(k).getString("flightNumber");
									if (flightnumber.equals(req.getFlightNumber())) {

										numberofarray = k;
										req.setEngineID("" + segments.getJSONObject(j).getString("engineID"));
										req.setAirlineName("" + legs.getJSONObject(k).getString("airlineName"));
										req.setArrivalTime("" + legs.getJSONObject(k).getString("arrivalTime"));
										req.setCabin("" + legs.getJSONObject(k).getString("cabin"));
										req.setDepartureTime("" + legs.getJSONObject(k).getString("departureTime"));
										req.setDestination("" + legs.getJSONObject(k).getString("destination"));
										req.setDuration("" + legs.getJSONObject(k).getString("duration"));
										req.setBoundType("" + legs.getJSONObject(k).getString("boundType"));
										req.setFlightNumber("" + legs.getJSONObject(k).getString("flightNumber"));
										req.setOrigin("" + legs.getJSONObject(k).getString("origin"));
										req.setAdults("" + session.getAttribute("adults"));
										req.setChilds("" + session.getAttribute("childs"));
										req.setInfants("" + session.getAttribute("infants"));
										req.setBeginDate("" + session.getAttribute("beginDate"));
										req.setFlightName("" + legs.getJSONObject(k).getString("airlineName"));
										req.setTripType("" + session.getAttribute("triptype"));
										req.setArrivalDate("" + legs.getJSONObject(k).getString("arrivalDate"));
										req.setDepartureDate("" + legs.getJSONObject(k).getString("departureDate"));
										req.setDepartureTerminal(
												"" + legs.getJSONObject(k).getString("departureTerminal"));
										req.setArrivalTerminal("" + legs.getJSONObject(k).getString("arrivalTerminal"));
										req.setCapacity("" + legs.getJSONObject(k).getString("capacity"));
										req.setCarrierCode("" + legs.getJSONObject(k).getString("carrierCode"));
										req.setCurrencyCode("" + legs.getJSONObject(k).getString("currencyCode"));
										req.setBaggageUnit("" + legs.getJSONObject(k).getString("baggageUnit"));
										req.setBaggageWeight("" + legs.getJSONObject(k).getString("baggageWeight"));
										req.setFareClassOfService(
												"" + legs.getJSONObject(k).getString("fareClassOfService"));
										req.setSold("" + legs.getJSONObject(k).getString("sold"));
										req.setStatus("" + legs.getJSONObject(k).getString("status"));
										req.setItineraryKey("" + segments.getJSONObject(j).getString("itineraryKey"));
										req.setBondType("" + segments.getJSONObject(j).getString("bondType"));
										req.setSearchId("" + segments.getJSONObject(j).getString("searchId"));
										req.setEngineID("" + segments.getJSONObject(j).getString("engineID"));
										req.setFareRule("" + segments.getJSONObject(j).getString("fareRule"));
										req.setJourneyTime("" + legs.getJSONObject(k).getString("duration"));
										req.setJourneyIndex("" + segments.getJSONObject(j).getString("journeyIndex"));

										JSONArray fare = segments.getJSONObject(j).getJSONArray("fare");
										for (int l = 0; l < fare.length(); l++) {
											JSONArray paxFares = fare.getJSONObject(l).getJSONArray("paxFares");
											JSONArray fares = paxFares.getJSONObject(l).getJSONArray("fares");

											req.setTotalFare("" + paxFares.getJSONObject(l).getString("totalFare"));
											req.setTotalTax("" + paxFares.getJSONObject(l).getString("totalTax"));
											req.setTotalFareWithOutMarkUp(
													"" + fare.getJSONObject(l).getString("totalFareWithOutMarkUp"));
											req.setTotalTaxWithOutMarkUp(
													"" + fare.getJSONObject(l).getString("totalTaxWithOutMarkUp"));
											req.setAmount("" + fares.getJSONObject(l).getString("amount"));

											req.setChargeCode("" + fares.getJSONObject(l).getString("chargeCode"));
											req.setChargeType("" + fares.getJSONObject(l).getString("chargeType"));

											req.setBasicFare("" + fare.getJSONObject(l).getString("basicFare"));
											req.setCancelPenalty(
													"" + paxFares.getJSONObject(l).getString("cancelPenalty"));
											req.setChangePenalty(
													"" + paxFares.getJSONObject(l).getString("changePenalty"));
											req.setTransactionAmount(
													"" + paxFares.getJSONObject(l).getString("transactionAmount"));
											req.setBaseTransactionAmount(
													"" + paxFares.getJSONObject(l).getString("baseTransactionAmount"));
											req.setTraceId("AYTM00011111111110002");

											req.setExchangeRate("" + fare.getJSONObject(l).getString("exchangeRate"));

											req.setMarkUP("" + paxFares.getJSONObject(l).getString("markUP"));
											req.setPaxType("" + paxFares.getJSONObject(l).getString("paxType"));

										}

									}
								}
							}
						}
					}
					if (req.getOnewayflight().equals("Secondflight")) {
						String source = "" + session.getAttribute("flightResponse");
						sources = "" + session.getAttribute("inSegments");
						JSONObject obj = new JSONObject(source);
						int numberofarray = 0;
						JSONArray journeys = obj.getJSONObject("details").getJSONArray("journeys");
						for (int x = 0; x < journeys.length(); x++) {
							JSONArray segments = journeys.getJSONObject(x).getJSONArray("segments");
							for (int j = 0; j < segments.length(); j++) {

								JSONArray bonds = segments.getJSONObject(j).getJSONArray("bonds");
								for (int k = 0; k < bonds.length(); k++) {

									JSONArray legs = bonds.getJSONObject(k).getJSONArray("legs");
									String flightnumber = legs.getJSONObject(k).getString("flightNumber");
									if (flightnumber.equals(req.getFlightNumber())) {
										req.setEngineID("" + segments.getJSONObject(j).getString("engineID"));
										System.out.println("#######################################");
										req.setAirlineName("" + legs.getJSONObject(k).getString("airlineName"));
										req.setArrivalTime("" + legs.getJSONObject(k).getString("arrivalTime"));
										req.setCabin("" + legs.getJSONObject(k).getString("cabin"));
										req.setDepartureTime("" + legs.getJSONObject(k).getString("departureTime"));
										req.setDestination("" + legs.getJSONObject(k).getString("destination"));
										req.setDuration("" + legs.getJSONObject(k).getString("duration"));
										req.setBoundType("" + legs.getJSONObject(k).getString("boundType"));
										req.setFlightNumber("" + legs.getJSONObject(k).getString("flightNumber"));
										req.setOrigin("" + legs.getJSONObject(k).getString("origin"));
										req.setAdults("" + session.getAttribute("adults"));
										req.setChilds("" + session.getAttribute("childs"));
										req.setInfants("" + session.getAttribute("infants"));
										req.setBeginDate("" + session.getAttribute("endDate"));
										req.setFlightName("" + legs.getJSONObject(k).getString("airlineName"));
										req.setTripType("" + session.getAttribute("triptype"));
										req.setArrivalDate("" + legs.getJSONObject(k).getString("arrivalDate"));
										req.setDepartureDate("" + legs.getJSONObject(k).getString("departureDate"));
										req.setDepartureTerminal(
												"" + legs.getJSONObject(k).getString("departureTerminal"));
										req.setArrivalTerminal("" + legs.getJSONObject(k).getString("arrivalTerminal"));
										req.setCapacity("" + legs.getJSONObject(k).getString("capacity"));
										req.setCarrierCode("" + legs.getJSONObject(k).getString("carrierCode"));
										req.setCurrencyCode("" + legs.getJSONObject(k).getString("currencyCode"));
										req.setBaggageUnit("" + legs.getJSONObject(k).getString("baggageUnit"));
										req.setBaggageWeight("" + legs.getJSONObject(k).getString("baggageWeight"));
										req.setFareClassOfService(
												"" + legs.getJSONObject(k).getString("fareClassOfService"));
										req.setSold("" + legs.getJSONObject(k).getString("sold"));
										req.setStatus("" + legs.getJSONObject(k).getString("status"));
										req.setItineraryKey("" + segments.getJSONObject(j).getString("itineraryKey"));
										req.setBondType("" + segments.getJSONObject(j).getString("bondType"));
										req.setSearchId("" + segments.getJSONObject(j).getString("searchId"));
										req.setEngineID("" + segments.getJSONObject(j).getString("engineID"));
										req.setFareRule("" + segments.getJSONObject(j).getString("fareRule"));
										req.setJourneyTime("" + legs.getJSONObject(k).getString("duration"));
										req.setJourneyIndex("" + segments.getJSONObject(j).getString("journeyIndex"));

										JSONArray fare = segments.getJSONObject(j).getJSONArray("fare");
										for (int l = 0; l < fare.length(); l++) {
											JSONArray paxFares = fare.getJSONObject(l).getJSONArray("paxFares");
											JSONArray fares = paxFares.getJSONObject(l).getJSONArray("fares");

											req.setTotalFare("" + paxFares.getJSONObject(l).getString("totalFare"));
											req.setTotalTax("" + paxFares.getJSONObject(l).getString("totalTax"));
											req.setTotalFareWithOutMarkUp(
													"" + fare.getJSONObject(l).getString("totalFareWithOutMarkUp"));
											req.setTotalTaxWithOutMarkUp(
													"" + fare.getJSONObject(l).getString("totalTaxWithOutMarkUp"));
											req.setAmount("" + fares.getJSONObject(l).getString("amount"));

											req.setChargeCode("" + fares.getJSONObject(l).getString("chargeCode"));
											req.setChargeType("" + fares.getJSONObject(l).getString("chargeType"));

											req.setBasicFare("" + fare.getJSONObject(l).getString("basicFare"));
											req.setCancelPenalty(
													"" + paxFares.getJSONObject(l).getString("cancelPenalty"));
											req.setChangePenalty(
													"" + paxFares.getJSONObject(l).getString("changePenalty"));
											req.setTransactionAmount(
													"" + paxFares.getJSONObject(l).getString("transactionAmount"));
											req.setBaseTransactionAmount(
													"" + paxFares.getJSONObject(l).getString("baseTransactionAmount"));
											req.setTraceId("AYTM00011111111110002");

											req.setExchangeRate("" + fare.getJSONObject(l).getString("exchangeRate"));

											req.setMarkUP("" + paxFares.getJSONObject(l).getString("markUP"));
											req.setPaxType("" + paxFares.getJSONObject(l).getString("paxType"));

										}

									}
								}
							}
						}
					}

					String source = "" + session.getAttribute("flightResponse");
					String airRePriceRQ = travelflightapi.AirRePriceRQConnecting(req, source,
							"" + session.getAttribute("destination"), session);

					String userflightamount = "";
					JSONObject airRePriceRQobj = new JSONObject(airRePriceRQ);
					String codeairRePriceRQobj = airRePriceRQobj.getString("code");
					if (codeairRePriceRQobj.equalsIgnoreCase("S00")) {
						if (req.getOnewayflight().equals("Firstfligt")) {
							session.setAttribute("Firstfligt", req.getOnewayflight());
							session.setAttribute("airRePriceRQoneway", airRePriceRQ);
						}
						if (req.getOnewayflight().equals("Secondflight")) {

							session.setAttribute("Secondflight", req.getOnewayflight());
							session.setAttribute("airRePriceRQroundway", airRePriceRQ);
						}

						JSONArray airRePriceRQjourneys = airRePriceRQobj.getJSONObject("details")
								.getJSONArray("journeys");

						JSONArray airRePriceRQsegments = airRePriceRQjourneys.getJSONObject(0).getJSONArray("segments");
						for (int j = 0; j < airRePriceRQsegments.length(); j++) {
							JSONArray fare = airRePriceRQsegments.getJSONObject(j).getJSONArray("fare");
							for (int l = 0; l < fare.length(); l++) {
								JSONArray paxFares = fare.getJSONObject(l).getJSONArray("paxFares");
								JSONArray fares = paxFares.getJSONObject(l).getJSONArray("fares");

								String TotalFare = "" + paxFares.getJSONObject(l).getString("totalFare");
								String TotalTax = "" + paxFares.getJSONObject(l).getString("totalTax");
								String TotalFareWithOutMarkUp = ""
										+ fare.getJSONObject(l).getString("totalFareWithOutMarkUp");

								String CancelPenalty = "" + paxFares.getJSONObject(l).getString("cancelPenalty");
								String ChangePenalty = "" + paxFares.getJSONObject(l).getString("changePenalty");
								userflightamount += "#" + TotalFare + "#" + TotalTax + "#" + TotalFareWithOutMarkUp
										+ "#" + CancelPenalty + "#" + ChangePenalty;
							}

						}
						return new ResponseEntity<String>("S00" + "@" + userflightamount, HttpStatus.OK);
					} else {
						return new ResponseEntity<String>("F04" + "@" + "Detail", HttpStatus.OK);
					}

				} catch (Exception e) {
					e.printStackTrace();
					System.err.println(e);
					System.err.println(e.getMessage());
					return new ResponseEntity<String>("F04" + "@" + "Detail", HttpStatus.OK);
				}
			} else {
				return new ResponseEntity<String>("F03" + "@" + "Detail", HttpStatus.OK);
			}
		} else {
			return new ResponseEntity<String>("F03" + "@" + "Detail", HttpStatus.OK);
		}
	}

	/* Code Done By Rohit End */

	/*
	 * #########################################################################
	 * ###################
	 */

	@RequestMapping(method = RequestMethod.POST, value = "/Flight/AirLineNames", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<FlightClityList> source(HttpSession session) {

		FlightClityList resp = new FlightClityList();
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
					try {

						resp = travelflightapi.getAirLineNames(sessionId);
						return new ResponseEntity<FlightClityList>(resp, HttpStatus.OK);

					} catch (Exception e) {
						System.out.println(e);
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
						return new ResponseEntity<FlightClityList>(resp, HttpStatus.OK);
					}
				} else {
					resp.setCode("F00");
					resp.setMessage("Unauthorised access");
					resp.setStatus("FAILED");
					return new ResponseEntity<FlightClityList>(resp, HttpStatus.OK);
				}
			} else {
				resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
				resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
				resp.setMessage("Session expired");
				return new ResponseEntity<FlightClityList>(resp, HttpStatus.OK);
			}

		} else {
			resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
			resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
			resp.setMessage("Session expired");
			return new ResponseEntity<FlightClityList>(resp, HttpStatus.OK);
		}
	}

	/*
	 * -----------------------------------Payment gateway
	 * Deduction-----------------------------------------------
	 */

	@RequestMapping(value = "/Flight/PaymentProcess", method = RequestMethod.POST)
	public String processReturnRedirect(@ModelAttribute LoadMoneyFlightRequest dto, HttpServletRequest request,
			ModelMap modelMap, HttpSession session) {
		String sessionId = (String) session.getAttribute("sessionId");
		String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
		if (authority != null) {
			request.setAttribute("sessionId", sessionId);
			session.setAttribute("email", dto.getEmailAddress());
			session.setAttribute("mobile", dto.getMobileNumber());
			FlightBookRequest req = new FlightBookRequest();
			LoadMoneyResponse loadMoneyResponse = new LoadMoneyResponse();
			request.getSession().setAttribute("sessionId", sessionId);
			dto.setSessionId(sessionId);
			String ticketDetails = "" + session.getAttribute("ticketDetails");
			if (dto.getUseVnetWeb().equalsIgnoreCase("vnet")) {
				FlightVnetRequest vNetRequest = new FlightVnetRequest();
				vNetRequest.setSessionId(sessionId);
				vNetRequest.setReturnURL(UrlMetadatas.WEBURL + "/User/Travel/Flight/VRedirect");
				vNetRequest.setAmount(dto.getAmount());
				List<TravellerFlightDetails> travellerDetails = getTravDetails(dto.getFirstName(), dto.getLastName(),
						dto.getUseradultsgender(), dto.getFirstNamechild(), dto.getLastNamechild(),
						dto.getUserchildsgender(), dto.getFirstNameinfant(), dto.getLastNameinfant(),
						dto.getUserinfantsgender());

				vNetRequest.setTicketDetails(ticketDetails);
				vNetRequest.setTravellerDetails(travellerDetails);
				VNetResponse vNetResponse = loadMoneyApi.initiateVnetBankingFlight(vNetRequest);
				if (vNetResponse.isSuccess()) {
	//				req.setTransactionId("" + loadMoneyResponse.getReferenceNo());
					req.setFirstName(dto.getFirstName());
					req.setFirstNamechild(dto.getFirstNamechild());
					req.setFirstNameinfant(dto.getFirstNameinfant());

					req.setLastName(dto.getLastName());
					req.setLastNamechild(dto.getLastNamechild());
					req.setLastNameinfant(dto.getLastNameinfant());

					req.setEmailAddress(dto.getEmail());
					req.setMobileNumber(dto.getPhone());
					req.setGrandtotal(req.getAmount());
					req.setUseradultsgender(dto.getUseradultsgender());
					req.setUserchildsgender(dto.getUserchildsgender());
					req.setUserinfantsgender(dto.getUserinfantsgender());

					req.setInfantDateOfBirth(dto.getInfantDateOfBirth());
					req.setAdultDateOfBirth(dto.getAdultDateOfBirth());
					req.setChildDateOfBirth(dto.getChildDateOfBirth());

					req.setAdultspassNo(dto.getAdultspassNo());
					req.setChildspassNo(dto.getChildspassNo());
					req.setInfantspassNo(dto.getInfantspassNo());

					req.setAdultspassExp(dto.getAdultspassExp());
					req.setChildspassExp(dto.getChildspassExp());
					req.setInfantspassExp(dto.getInfantspassExp());

					req.setAdults("" + session.getAttribute("adults"));
					req.setChilds("" + session.getAttribute("childs"));
					req.setInfants("" + session.getAttribute("infants"));

					String source = "" + session.getAttribute("airRePriceRQ");
					String airRePriceRQ = travelflightapi.AirBookRQwithPaymentGatway(req, source, session);

					session.setAttribute("email", req.getEmailAddress());
					session.setAttribute("mobile", req.getMobileNumber());
					session.setAttribute("FlightBookWithPaymentGateway", airRePriceRQ);
					session.setAttribute("travellerDetails", travellerDetails);

					modelMap.addAttribute("vnet", vNetResponse);
					return "User/LoadMoney/VNetPay";
				}
			} else if (dto.getUseVnetWeb().equals("others")) {
				dto.setReturnUrl(UrlMetadatas.WEBURL + "/User/Travel/Flight/Redirect");
				List<TravellerFlightDetails> travellerDetails = getTravDetails(dto.getFirstName(), dto.getLastName(),
						dto.getUseradultsgender(), dto.getFirstNamechild(), dto.getLastNamechild(),
						dto.getUserchildsgender(), dto.getFirstNameinfant(), dto.getLastNameinfant(),
						dto.getUserinfantsgender());
				dto.setTicketDetails(ticketDetails);
				dto.setTravellerDetails(travellerDetails);
				session.setAttribute("travellerDetails", travellerDetails);
				loadMoneyResponse = loadMoneyApi.SplitpaymentMoneyRequest(dto);
				if (loadMoneyResponse.isSuccess()) {
		//			req.setTransactionId("" + loadMoneyResponse.getReferenceNo());
					req.setFirstName(dto.getFirstName());
					req.setFirstNamechild(dto.getFirstNamechild());
					req.setFirstNameinfant(dto.getFirstNameinfant());
					req.setLastName(dto.getLastName());
					req.setLastNamechild(dto.getLastNamechild());
					req.setLastNameinfant(dto.getLastNameinfant());
					req.setEmailAddress(dto.getEmail());
					req.setMobileNumber(dto.getPhone());
					req.setGrandtotal(req.getAmount());
					req.setUseradultsgender(dto.getUseradultsgender());
					req.setUserchildsgender(dto.getUserchildsgender());
					req.setUserinfantsgender(dto.getUserinfantsgender());
					req.setInfantDateOfBirth(dto.getInfantDateOfBirth());
					req.setInfantDateOfBirth(dto.getInfantDateOfBirth());
					req.setAdultDateOfBirth(dto.getAdultDateOfBirth());
					req.setChildDateOfBirth(dto.getChildDateOfBirth());
					req.setAdultspassNo(dto.getAdultspassNo());
					req.setChildspassNo(dto.getChildspassNo());
					req.setInfantspassNo(dto.getInfantspassNo());
					req.setAdultspassExp(dto.getAdultspassExp());
					req.setChildspassExp(dto.getChildspassExp());
					req.setInfantspassExp(dto.getInfantspassExp());

					req.setAdults("" + session.getAttribute("adults"));
					req.setChilds("" + session.getAttribute("childs"));
					req.setInfants("" + session.getAttribute("infants"));
					String source = "" + session.getAttribute("airRePriceRQ");
					session.setAttribute("email", req.getEmailAddress());
					session.setAttribute("mobile", req.getMobileNumber());
					session.setAttribute("firstName", req.getFirstName());
					String airRePriceRQ = travelflightapi.AirBookRQwithPaymentGatway(req, source, session);
					session.setAttribute("FlightBookWithPaymentGateway", airRePriceRQ);
					modelMap.addAttribute("loadmoney", loadMoneyResponse);
					return "User/Pay";
				} else {
					/*System.err.format("Load money response is %s\n", loadMoneyResponse.getDescription());
					modelMap.addAttribute(ModelMapKey.MESSAGE, loadMoneyResponse.getDescription());*/
					return "User/Home";
				}
			}
		} else {
			return "redirect:/Home";
		}
		return "redirect:/User/Home";

	}

	@RequestMapping(value = "/Flight/ReturnProcess", method = RequestMethod.POST)
	public String processReturnRedirectddd(@ModelAttribute LoadMoneyFlightRequest dto, HttpServletRequest request,
			ModelMap modelMap, HttpSession session) {
		String sessionId = (String) session.getAttribute("sessionId");
		request.setAttribute("sessionId", sessionId);
		String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
		if (authority != null) {
			ReturnFlightBookRequest req = new ReturnFlightBookRequest();
			LoadMoneyResponse loadMoneyResponse = new LoadMoneyResponse();
			request.getSession().setAttribute("sessionId", sessionId);
			dto.setSessionId(sessionId);
			String ticketDetails = "" + session.getAttribute("ticketDetails");

			session.setAttribute("email", dto.getEmail());
			session.setAttribute("mobile", dto.getPhone());

			if (dto.getUseVnetWeb().equalsIgnoreCase("vnet")) {
				System.err.println("inside vnet");
				FlightVnetRequest vNetRequest = new FlightVnetRequest();
				vNetRequest.setSessionId(sessionId);
				vNetRequest.setReturnURL(UrlMetadatas.WEBURL + "/User/Travel/Flight/Return/VRedirect");
				vNetRequest.setAmount(dto.getAmount());

				List<TravellerFlightDetails> travellerDetails = getTravDetails(dto.getFirstName(), dto.getLastName(),
						dto.getUseradultsgender(), dto.getFirstNamechild(), dto.getLastNamechild(),
						dto.getUserchildsgender(), dto.getFirstNameinfant(), dto.getLastNameinfant(),
						dto.getUserinfantsgender());
				List<TravellerFlightDetails> travellerDetailsReturn = travellerDetails;
				travellerDetails.addAll(travellerDetailsReturn);

				vNetRequest.setTicketDetails(ticketDetails);
				vNetRequest.setTravellerDetails(travellerDetails);

				session.setAttribute("travellerDetails", travellerDetails);

				VNetResponse vNetResponse = loadMoneyApi.initiateVnetBankingFlight(vNetRequest);
				if (vNetResponse.isSuccess()) {
			//		req.setTransactionId("" + loadMoneyResponse.getReferenceNo());
					req.setFirstName(dto.getFirstName());
					req.setFirstNamechild(dto.getFirstNamechild());
					req.setFirstNameinfant(dto.getFirstNameinfant());

					req.setLastName(dto.getLastName());
					req.setLastNamechild(dto.getLastNamechild());
					req.setLastNameinfant(dto.getLastNameinfant());
					req.setUseradultsgender(dto.getUseradultsgender());
					req.setUserchildsgender(dto.getUserchildsgender());
					req.setUserinfantsgender(dto.getUserinfantsgender());
					req.setInfantDateOfBirth(dto.getInfantDateOfBirth());

					req.setEmailAddress(dto.getEmail());
					req.setMobileNumber(dto.getPhone());
					req.setGrandtotal(req.getAmount());
					req.setAdults("" + session.getAttribute("adults"));
					req.setChilds("" + session.getAttribute("childs"));
					req.setInfants("" + session.getAttribute("infants"));
					String source = "" + session.getAttribute("airRePriceRQoneway");
					String destination = "" + session.getAttribute("airRePriceRQroundway");
					String airRePriceRQ = travelflightapi.AirBookRQreturnwithPaymentGatway(req, source, destination,
							session);

					session.setAttribute("FlightBookWithPaymentGateway", airRePriceRQ);
					modelMap.addAttribute("vnet", vNetResponse);
					return "User/LoadMoney/VNetPay";
				}
			} else if (dto.getUseVnetWeb().equals("others")) {
				dto.setReturnUrl(UrlMetadatas.WEBURL + "/User/Travel/Flight/ReturnRedirect");

				List<TravellerFlightDetails> travellerDetails = getTravDetails(dto.getFirstName(), dto.getLastName(),
						dto.getUseradultsgender(), dto.getFirstNamechild(), dto.getLastNamechild(),
						dto.getUserchildsgender(), dto.getFirstNameinfant(), dto.getLastNameinfant(),
						dto.getUserinfantsgender());

				List<TravellerFlightDetails> travellerDetailsReturn = travellerDetails;
				travellerDetails.addAll(travellerDetailsReturn);

				dto.setTicketDetails(ticketDetails);
				dto.setTravellerDetails(travellerDetails);

				session.setAttribute("travellerDetails", travellerDetails);

				loadMoneyResponse = loadMoneyApi.SplitpaymentMoneyRequest(dto);
				if (loadMoneyResponse.isSuccess()) {
		//			req.setTransactionId("" + loadMoneyResponse.getReferenceNo());
					req.setFirstName(dto.getFirstName());
					req.setFirstNamechild(dto.getFirstNamechild());
					req.setFirstNameinfant(dto.getFirstNameinfant());

					req.setLastName(dto.getLastName());
					req.setLastNamechild(dto.getLastNamechild());
					req.setLastNameinfant(dto.getLastNameinfant());

					req.setEmailAddress(dto.getEmail());
					req.setMobileNumber(dto.getPhone());
					req.setGrandtotal(req.getAmount());
					req.setUseradultsgender(dto.getUseradultsgender());
					req.setUserchildsgender(dto.getUserchildsgender());
					req.setUserinfantsgender(dto.getUserinfantsgender());
					req.setInfantDateOfBirth(dto.getInfantDateOfBirth());

					req.setEmailAddress(dto.getEmail());
					req.setMobileNumber(dto.getPhone());
					req.setGrandtotal(dto.getAmount());
					req.setAdults("" + session.getAttribute("adults"));
					req.setChilds("" + session.getAttribute("childs"));
					req.setInfants("" + session.getAttribute("infants"));
					String source = "" + session.getAttribute("airRePriceRQoneway");
					String destination = "" + session.getAttribute("airRePriceRQroundway");
					String airRePriceRQ = travelflightapi.AirBookRQreturnwithPaymentGatway(req, source, destination,
							session);

					session.setAttribute("FlightBookWithPaymentGatewayReturn", airRePriceRQ);
					modelMap.addAttribute("loadmoney", loadMoneyResponse);
					return "User/Pay";
				} else {
					/*System.err.format("Load money response is %s\n", loadMoneyResponse.getDescription());
					modelMap.addAttribute(ModelMapKey.MESSAGE, loadMoneyResponse.getDescription());*/
					return "User/Home";
				}
			}
			return "redirect:/User/Home";
		} else {
			return "redirect:/Home";
		}
	}

	// ----------------------------------------International Roundway
	// Start-------------------------------------------------------

	@RequestMapping(value = "/Flight/InternationalReturnProcess", method = RequestMethod.POST)
	public String processReturnRedirectdddInternational(@ModelAttribute LoadMoneyFlightRequest dto,
			HttpServletRequest request, ModelMap modelMap, HttpSession session) throws Exception {
		String sessionId = (String) session.getAttribute("sessionId");
		request.setAttribute("sessionId", sessionId);

		String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
		if (authority != null) {
			ReturnFlightBookRequest req = new ReturnFlightBookRequest();
			LoadMoneyResponse loadMoneyResponse = new LoadMoneyResponse();
			request.getSession().setAttribute("sessionId", sessionId);
			dto.setSessionId(sessionId);

			String ticketDetails = "" + session.getAttribute("ticketDetails");

			session.setAttribute("email", dto.getEmail());
			session.setAttribute("mobile", dto.getPhone());

			if (dto.getUseVnetWeb().equalsIgnoreCase("vnet")) {
				System.err.println("inside vnet");
				FlightVnetRequest vNetRequest = new FlightVnetRequest();
				vNetRequest.setSessionId(sessionId);
				vNetRequest.setReturnURL(UrlMetadatas.WEBURL + "/User/Travel/Flight/Return/VRedirect");
				vNetRequest.setAmount(dto.getAmount());

				List<TravellerFlightDetails> travellerDetails = getTravDetails(dto.getFirstName(), dto.getLastName(),
						dto.getUseradultsgender(), dto.getFirstNamechild(), dto.getLastNamechild(),
						dto.getUserchildsgender(), dto.getFirstNameinfant(), dto.getLastNameinfant(),
						dto.getUserinfantsgender());
				List<TravellerFlightDetails> travellerDetailsReturn = travellerDetails;
				travellerDetails.addAll(travellerDetailsReturn);

				vNetRequest.setTicketDetails(ticketDetails);
				vNetRequest.setTravellerDetails(travellerDetails);

				VNetResponse vNetResponse = loadMoneyApi.initiateVnetBankingFlight(vNetRequest);
				if (vNetResponse.isSuccess()) {
				//	req.setTransactionId("" + loadMoneyResponse.getReferenceNo());
					req.setFirstName(dto.getFirstName());
					req.setFirstNamechild(dto.getFirstNamechild());
					req.setFirstNameinfant(dto.getFirstNameinfant());

					req.setLastName(dto.getLastName());
					req.setLastNamechild(dto.getLastNamechild());
					req.setLastNameinfant(dto.getLastNameinfant());

					req.setEmailAddress(dto.getEmail());
					req.setMobileNumber(dto.getPhone());
					req.setGrandtotal(req.getAmount());
					req.setUseradultsgender(dto.getUseradultsgender());
					req.setUserchildsgender(dto.getUserchildsgender());
					req.setUserinfantsgender(dto.getUserinfantsgender());

					req.setInfantDateOfBirth(dto.getInfantDateOfBirth());
					req.setAdultDateOfBirth(dto.getAdultDateOfBirth());
					req.setChildDateOfBirth(dto.getChildDateOfBirth());

					req.setAdultspassNo(dto.getAdultspassNo());
					req.setChildspassNo(dto.getChildspassNo());
					req.setInfantspassNo(dto.getInfantspassNo());

					req.setAdultspassExp(dto.getAdultspassExp());
					req.setChildspassExp(dto.getChildspassExp());
					req.setInfantspassExp(dto.getInfantspassExp());

					req.setTransactionId("" + System.currentTimeMillis());
					req.setAdults("" + session.getAttribute("adults"));
					req.setChilds("" + session.getAttribute("childs"));
					req.setInfants("" + session.getAttribute("infants"));

					org.json.JSONObject objmain1 = CreateJsonRequestFlight.createjsonBookApiinternationalFlightBook(req,
							session);

					session.setAttribute("FlightBookWithPaymentGateway", objmain1);
					modelMap.addAttribute("vnet", vNetResponse);
					return "User/LoadMoney/VNetPay";
				}
			} else if (dto.getUseVnetWeb().equals("others")) {
				dto.setReturnUrl(UrlMetadatas.WEBURL + "/User/Travel/Flight/ReturnRedirect");
				loadMoneyResponse = loadMoneyApi.SplitpaymentMoneyRequest(dto);
				if (loadMoneyResponse.isSuccess()) {
			//		req.setTransactionId("" + loadMoneyResponse.getReferenceNo());
					req.setFirstName(dto.getFirstName());
					req.setFirstNamechild(dto.getFirstNamechild());
					req.setFirstNameinfant(dto.getFirstNameinfant());

					req.setLastName(dto.getLastName());
					req.setLastNamechild(dto.getLastNamechild());
					req.setLastNameinfant(dto.getLastNameinfant());

					req.setEmailAddress(dto.getEmail());
					req.setMobileNumber(dto.getPhone());
					req.setGrandtotal(req.getAmount());

					req.setEmailAddress(dto.getEmail());
					req.setMobileNumber(dto.getPhone());
					req.setGrandtotal(dto.getAmount());
					req.setUseradultsgender(dto.getUseradultsgender());
					req.setUserchildsgender(dto.getUserchildsgender());
					req.setUserinfantsgender(dto.getUserinfantsgender());

					req.setInfantDateOfBirth(dto.getInfantDateOfBirth());
					req.setAdultDateOfBirth(dto.getAdultDateOfBirth());
					req.setChildDateOfBirth(dto.getChildDateOfBirth());

					req.setAdultspassNo(dto.getAdultspassNo());
					req.setChildspassNo(dto.getChildspassNo());
					req.setInfantspassNo(dto.getInfantspassNo());

					req.setAdultspassExp(dto.getAdultspassExp());
					req.setChildspassExp(dto.getChildspassExp());
					req.setInfantspassExp(dto.getInfantspassExp());

					req.setAdults("" + session.getAttribute("adults"));
					req.setChilds("" + session.getAttribute("childs"));
					req.setInfants("" + session.getAttribute("infants"));
					org.json.JSONObject objmain1 = CreateJsonRequestFlight.createjsonBookApiinternationalFlightBook(req,
							session);

					List<TravellerFlightDetails> travellerDetails = getTravDetails(dto.getFirstName(),
							dto.getLastName(), dto.getUseradultsgender(), dto.getFirstNamechild(),
							dto.getLastNamechild(), dto.getUserchildsgender(), dto.getFirstNameinfant(),
							dto.getLastNameinfant(), dto.getUserinfantsgender());

					session.setAttribute("travellerDetails", travellerDetails);
					session.setAttribute("FlightBookWithPaymentGatewayReturn", objmain1);
					modelMap.addAttribute("loadmoney", loadMoneyResponse);
					return "User/Pay";
				} else {
					/*System.err.format("Load money response is %s\n", loadMoneyResponse.getDescription());
					modelMap.addAttribute(ModelMapKey.MESSAGE, loadMoneyResponse.getDescription());*/
					return "User/Home";
				}
			}
			return "redirect:/User/Home";
		} else {
			return "redirect:/Home";
		}
	}

	@RequestMapping(value = "/Flight/ReturnProcessSplitpaymentInternational", method = RequestMethod.POST)
	public String ReturnProcessSplitpaymentInternational(@ModelAttribute LoadMoneyFlightRequest dto,
			HttpServletRequest request, ModelMap modelMap, HttpSession session) throws Exception {
		String sessionId = (String) session.getAttribute("sessionId");
		request.setAttribute("sessionId", sessionId);
		String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
		if (authority != null) {
			ReturnFlightBookRequest req = new ReturnFlightBookRequest();
			LoadMoneyResponse loadMoneyResponse = new LoadMoneyResponse();

			request.getSession().setAttribute("sessionId", sessionId);
			dto.setSessionId(sessionId);
			dto.setAmount(dto.getReturnamount());
			dto.setName("ddddd");
			dto.setReturnUrl(UrlMetadatas.WEBURL + "/User/Travel/Flight/ReturnRedirectSplitpayment");
			loadMoneyResponse = loadMoneyApi.LoadMoneyFlightRequest(dto);
			if (loadMoneyResponse.isSuccess()) {
		//		req.setTransactionId("" + loadMoneyResponse.getReferenceNo());
				req.setFirstName(dto.getFirstName());
				req.setFirstNamechild(dto.getFirstNamechild());
				req.setFirstNameinfant(dto.getFirstNameinfant());

				req.setLastName(dto.getLastName());
				req.setLastNamechild(dto.getLastNamechild());
				req.setLastNameinfant(dto.getLastNameinfant());

				req.setEmailAddress(dto.getEmail());
				req.setMobileNumber(dto.getPhone());
				req.setGrandtotal(req.getAmount());

				req.setEmailAddress(dto.getEmail());
				req.setMobileNumber(dto.getPhone());
				req.setGrandtotal(dto.getAmount());
				req.setUseradultsgender(dto.getUseradultsgender());
				req.setUserchildsgender(dto.getUserchildsgender());
				req.setUserinfantsgender(dto.getUserinfantsgender());
				req.setAdults("" + session.getAttribute("adults"));
				req.setChilds("" + session.getAttribute("childs"));
				req.setInfants("" + session.getAttribute("infants"));
				org.json.JSONObject objmain1 = CreateJsonRequestFlight.createjsonBookApiinternationalFlightBook(req,
						session);
				System.err.println(objmain1);
				session.setAttribute("FlightBookWithSplitPaymentReturn", objmain1);
				modelMap.addAttribute("loadmoney", loadMoneyResponse);
				return "User/Pay";
			} else {
				/*System.err.format("Load money response is %s\n", loadMoneyResponse.getDescription());
				modelMap.addAttribute(ModelMapKey.MESSAGE, loadMoneyResponse.getDescription());*/
				return "User/Home";
			}
		} else {
			return "redirect:/Home";
		}

	}

	// Split Payment-----------------------------------Payment gateway Deduction
	// Through Split Payment-----------------------------------------------
	@RequestMapping(value = "/Flight/ProcessSplitpayment", method = RequestMethod.POST)
	public String processLoadMoneySplitpayment(@ModelAttribute LoadMoneyFlightRequest dto, HttpServletRequest request,
			ModelMap modelMap, HttpSession session) {
		String sessionId = (String) session.getAttribute("sessionId");
		request.setAttribute("sessionId", sessionId);
		String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
		if (authority != null) {
			FlightBookRequest req = new FlightBookRequest();
			LoadMoneyResponse loadMoneyResponse = new LoadMoneyResponse();

			dto.setName("dssdfsdfsdfds");
			request.getSession().setAttribute("sessionId", sessionId);
			dto.setSessionId(sessionId);
			dto.setAmount(dto.getReturnamount());
			dto.setReturnUrl(UrlMetadatas.WEBURL + "/User/Travel/Flight/RedirectSplitpayment");
			loadMoneyResponse = loadMoneyApi.LoadMoneyFlightRequest(dto);
			if (loadMoneyResponse.isSuccess()) {

				modelMap.addAttribute("loadmoney", loadMoneyResponse);
				return "User/Pay";
			} else {
				/*System.err.format("Load money response is %s\n", loadMoneyResponse.getDescription());
				modelMap.addAttribute(ModelMapKey.MESSAGE, loadMoneyResponse.getDescription());*/
				return "User/Home";
			}
		} else {
			return "redirect:/Home";
		}
	}

	@RequestMapping(value = "/Flight/ReturnProcessSplitpayment", method = RequestMethod.POST)
	public String processReturnRedirectSplitpayment(@ModelAttribute LoadMoneyFlightRequest dto,
			HttpServletRequest request, ModelMap modelMap, HttpSession session) {
		String sessionId = (String) session.getAttribute("sessionId");
		request.setAttribute("sessionId", sessionId);
		String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
		if (authority != null) {
			ReturnFlightBookRequest req = new ReturnFlightBookRequest();
			LoadMoneyResponse loadMoneyResponse = new LoadMoneyResponse();

			request.getSession().setAttribute("sessionId", sessionId);
			dto.setSessionId(sessionId);
			dto.setAmount(dto.getReturnamount());
			dto.setName("ddddd");
			dto.setReturnUrl(UrlMetadatas.WEBURL + "/User/Travel/Flight/ReturnRedirectSplitpayment");
			loadMoneyResponse = loadMoneyApi.LoadMoneyFlightRequest(dto);
			if (loadMoneyResponse.isSuccess()) {

				modelMap.addAttribute("loadmoney", loadMoneyResponse);
				return "User/Pay";
			} else {
			//	System.err.format("Load money response is %s\n", loadMoneyResponse.getDescription());
				modelMap.addAttribute(ModelMapKey.MESSAGE, "Maximum load money amount 10000");
				return "User/Home";
			}
		} else {
			return "redirect:/Home";
		}

	}

	/* Book Ticket methods calling */

	@RequestMapping(method = RequestMethod.POST, value = "/Flight/CheckOut", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<String> getFlightCheckOutPostMethod(HttpSession session, @RequestBody FlightBookRequest req) {
		String sessionId = (String) session.getAttribute("sessionId");
		try {

			if (sessionId != null && sessionId.length() != 0) {
				String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
				if (authority != null) {
					String ticketDetails = "" + session.getAttribute("ticketDetails");
					List<TravellerFlightDetails> travellerDetails = getTravDetails(req.getFirstName(),
							req.getLastName(), req.getUseradultsgender(), req.getFirstNamechild(),
							req.getLastNamechild(), req.getUserchildsgender(), req.getFirstNameinfant(),
							req.getLastNameinfant(), req.getUserinfantsgender());
					FligthBookReq flightBookReq = new FligthBookReq();
					flightBookReq.setSessionId(sessionId);
					flightBookReq.setPaymentAmount(req.getGrandtotal());
					flightBookReq.setTicketDetails(ticketDetails);
					flightBookReq.setPaymentMethod(PaymentType.Wallet.getKey());
					flightBookReq.setSource(req.getOrigin());
					flightBookReq.setDestination(req.getDestination());
					flightBookReq.setBaseFare(req.getBaseFare());
					double bkAmt = Double.parseDouble(req.getGrandtotal()) - 200;
					req.setGrandtotal(bkAmt + "");
					String flightBookingInitiate = travelflightapi.flightBookingInitiate(flightBookReq,
							travellerDetails);
					JSONObject objflightBookingInitiate = new JSONObject(flightBookingInitiate);
					String codeflightBookingInitiate = objflightBookingInitiate.getString("code");
					if (codeflightBookingInitiate.equalsIgnoreCase("S00")) {
						String transactionRefNo = objflightBookingInitiate.getJSONObject("details")
								.getString("transactionRefNo");
						req.setAdults("" + session.getAttribute("adults"));
						req.setChilds("" + session.getAttribute("childs"));
						req.setInfants("" + session.getAttribute("infants"));
						req.setDestination("" + session.getAttribute("destination"));
						req.setTransactionId(transactionRefNo);
						String source = "" + session.getAttribute("airRePriceRQ");

						String airRePriceRQ = travelflightapi.AirBookRQ(req, source, session);
						JSONObject obj = new JSONObject(airRePriceRQ);
						flightBookReq.setTxnRefno(req.getTransactionId());
						String code = obj.getString("code");
						if (code.equalsIgnoreCase("S00")) {

							String bookingRefId = obj.getJSONObject("details").getString("bookingRefId");
							String transactionRefNomdex = obj.getJSONObject("details").getString("transactionRefNo");
							JSONArray tickets = obj.getJSONObject("details").getJSONObject("bookingDetail")
									.getJSONObject("pnrDetail").getJSONArray("tickets");
							String ticketNumber = "";

							for (int i = 0; i < tickets.length(); i++) {
								ticketNumber = tickets.getJSONObject(i).getString("ticketNumber");
								travellerDetails.get(i).setTicketNo(ticketNumber);

							}
							JSONArray objccpnrs = obj.getJSONObject("details").getJSONObject("bookingDetail")
									.getJSONObject("pnrDetail").getJSONArray("pnrs");
							String pnrNo = objccpnrs.getJSONObject(0).getString("pnr");
							flightBookReq.setBookingRefId(bookingRefId);
							flightBookReq.setMdexTxnRefNo(transactionRefNomdex);
							flightBookReq.setStatus(Status.Booked.getValue());
							flightBookReq.setTxnRefno(req.getTransactionId());
							flightBookReq.setSuccess(true);
							flightBookReq.setEmail(req.getEmailAddress());
							flightBookReq.setMobile(req.getMobileNumber());
							flightBookReq.setFlightStatus(Status.Booked.getValue());
							flightBookReq.setPnrNo(pnrNo);
							String flightBookingSucess = travelflightapi.flightBookingSucess(flightBookReq,
									travellerDetails);
							JSONObject flightBookingSucessobj = new JSONObject(flightBookingSucess);
							String flightBookingSucesscode = flightBookingSucessobj.getString("code");
							if (flightBookingSucesscode.equalsIgnoreCase("S00")) {
								return new ResponseEntity<String>("S00" + "@" + "Flight Successfully Booked.",
										HttpStatus.OK);
							} else {
								return new ResponseEntity<String>(
										"F04" + "@"
												+ "Your Flight Ticket is Booked.But Payment not successful Please Contact Customer Care.",
										HttpStatus.OK);
							}
						} else {
							flightBookReq.setSuccess(false);
							travelflightapi.flightBookingSucess(flightBookReq, travellerDetails);
							return new ResponseEntity<String>("F04" + "@" + "" + obj.getString("message"),
									HttpStatus.OK);
						}

					} else if (codeflightBookingInitiate.equalsIgnoreCase("T01")) {
						String source = "" + session.getAttribute("airRePriceRQ");
						req.setTransactionId("" + System.currentTimeMillis());
						req.setAdults("" + session.getAttribute("adults"));
						req.setChilds("" + session.getAttribute("childs"));
						req.setInfants("" + session.getAttribute("infants"));
						req.setDestination("" + session.getAttribute("destination"));
						String airRePriceRQ = travelflightapi.AirBookRQwithPaymentGatway(req, source, session);
						session.setAttribute("FlightBookWithSplitPayment", airRePriceRQ);
						session.setAttribute("splitFlightBookAmt", req.getGrandtotal());
						session.setAttribute("email", req.getEmailAddress());
						session.setAttribute("mobile", req.getMobileNumber());
						session.setAttribute("SPaymentTDetails", travellerDetails);
						return new ResponseEntity<String>(
								"T01" + "@" + "Splitpayment" + "@" + objflightBookingInitiate.getString("balance"),
								HttpStatus.OK);
					} else {

						flightBookReq.setSuccess(false);
						travelflightapi.flightBookingSucess(flightBookReq, travellerDetails);
						return new ResponseEntity<String>(
								"F04" + "@" + "" + objflightBookingInitiate.getString("message"), HttpStatus.OK);
					}

				} else {
					return new ResponseEntity<String>("F03" + "@" + "Please Login and try Again Later", HttpStatus.OK);
				}
			} else {
				return new ResponseEntity<String>("F03" + "@" + "Please Login and try Again Later", HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>("F04" + "@" + "Service Down Please Try Again Later", HttpStatus.OK);
		}

	}

	@RequestMapping(method = RequestMethod.POST, value = "/Flight/ReturnCheckOut", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<String> getFlightReturnCheckOutCheckOutPostMethod(HttpSession session,
			@RequestBody ReturnFlightBookRequest req) {
		String sessionId = (String) session.getAttribute("sessionId");
		try {
			if (sessionId != null && sessionId.length() != 0) {
				String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
				if (authority != null) {
					session.setAttribute("splitRountFlightAmt", req.getGrandtotal());

					// String ticketDetails = "" +
					// session.getAttribute("ticketDetails");

					List<TravellerFlightDetails> travellerDetails = getTravDetails(req.getFirstName(),
							req.getLastName(), req.getUseradultsgender(), req.getFirstNamechild(),
							req.getLastNamechild(), req.getUserchildsgender(), req.getFirstNameinfant(),
							req.getLastNameinfant(), req.getUserinfantsgender());

					List<TravellerFlightDetails> travellerDetailsReturn = getTravDetails(req.getFirstName(),
							req.getLastName(), req.getUseradultsgender(), req.getFirstNamechild(),
							req.getLastNamechild(), req.getUserchildsgender(), req.getFirstNameinfant(),
							req.getLastNameinfant(), req.getUserinfantsgender());

					FligthBookReq flightBookReq = new FligthBookReq();

					flightBookReq.setSessionId(sessionId);
					flightBookReq.setPaymentAmount(req.getGrandtotal());
					flightBookReq.setPaymentMethod(PaymentType.Wallet.getKey());
					flightBookReq.setSource(req.getOrigin());
					flightBookReq.setDestination(req.getDestination());
					flightBookReq.setBaseFare(req.getBaseFare());
					travellerDetails.addAll(travellerDetailsReturn);

					String source = "" + session.getAttribute("airRePriceRQoneway");
					String destination = "" + session.getAttribute("airRePriceRQroundway");

					JSONObject src = new JSONObject(source);
					JSONObject dest = new JSONObject(destination);

					JSONArray journeys11 = src.getJSONObject("details").getJSONArray("journeys");
					JSONArray segments11 = journeys11.getJSONObject(0).getJSONArray("segments");
					JSONArray bonds1 = segments11.getJSONObject(0).getJSONArray("bonds");

					JSONArray journeys12 = dest.getJSONObject("details").getJSONArray("journeys");
					JSONArray segments12 = journeys12.getJSONObject(0).getJSONArray("segments");
					JSONArray bonds2 = segments12.getJSONObject(0).getJSONArray("bonds");
					bonds2.get(0);
					bonds1.put(bonds2.get(0));

					JSONObject ticketDetails = CreateJsonRequestFlight.createjsonForTicket(bonds1);

					flightBookReq.setTicketDetails(ticketDetails + "");

					System.err.println("Ticket Details:: " + ticketDetails + "");

					session.setAttribute("ticketDetails", ticketDetails);
					String flightBookingInitiate = travelflightapi.flightBookingInitiate(flightBookReq,
							travellerDetails);

					double grandtotal = Double.parseDouble(req.getGrandtotal()) - 200;
					req.setGrandtotal(grandtotal + "");

					JSONObject objflightBookingInitiate = new JSONObject(flightBookingInitiate);
					String codeflightBookingInitiate = objflightBookingInitiate.getString("code");
					if (codeflightBookingInitiate.equalsIgnoreCase("S00")) {

						String transactionRefNo = objflightBookingInitiate.getJSONObject("details")
								.getString("transactionRefNo");

						req.setAdults("" + session.getAttribute("adults"));
						req.setChilds("" + session.getAttribute("childs"));
						req.setInfants("" + session.getAttribute("infants"));
						req.setDestination("" + session.getAttribute("destination"));
						req.setTransactionId(transactionRefNo);

						String airRePriceRQ = travelflightapi.AirBookRQreturn(req, source, destination, session);
						JSONObject obj = new JSONObject(airRePriceRQ);

						flightBookReq.setTxnRefno(req.getTransactionId());

						String code = obj.getString("code");

						if (code.equalsIgnoreCase("S00")) {
							String bookingRefId = obj.getJSONObject("details").getString("bookingRefId");
							String transactionRefNomdex = obj.getJSONObject("details").getString("transactionRefNo");
							JSONArray tickets = obj.getJSONObject("details").getJSONObject("bookingDetail")
									.getJSONObject("pnrDetail").getJSONArray("tickets");
							String ticketNumber = "";
							String firstName = "";

							for (int i = 0; i < travellerDetails.size(); i++) {
								ticketNumber = tickets.getJSONObject(i).getString("ticketNumber");
								travellerDetails.get(i).setTicketNo(ticketNumber);
							}

							JSONArray objccpnrs = obj.getJSONObject("details").getJSONObject("bookingDetail")
									.getJSONObject("pnrDetail").getJSONArray("pnrs");
							String pnrNo = objccpnrs.getJSONObject(0).getString("pnr");

							flightBookReq.setBookingRefId(bookingRefId);
							flightBookReq.setMdexTxnRefNo(transactionRefNomdex);
							flightBookReq.setStatus(Status.Booked.getValue());

							flightBookReq.setSuccess(true);
							flightBookReq.setMobile(req.getMobileNumber());
							flightBookReq.setEmail(req.getEmailAddress());
							flightBookReq.setFlightStatus(Status.Booked.getValue());
							flightBookReq.setPnrNo(pnrNo);

							String flightBookingSucess = travelflightapi.flightBookingSucess(flightBookReq,
									travellerDetails);

							JSONObject flightBookingSucessobj = new JSONObject(flightBookingSucess);
							String flightBookingSucesscode = flightBookingSucessobj.getString("code");
							if (flightBookingSucesscode.equalsIgnoreCase("S00")) {
								return new ResponseEntity<String>("S00" + "@" + "Flight Successfully Booked.",
										HttpStatus.OK);
							} else {

								return new ResponseEntity<String>(
										"F04" + "@"
												+ "Ticket is Booked. But payment not successful. Please Try Again Later",
										HttpStatus.OK);
							}
						} else {
							flightBookReq.setSuccess(false);
							travelflightapi.flightBookingSucess(flightBookReq, travellerDetails);
							return new ResponseEntity<String>("F04" + "@" + obj.getString("message"), HttpStatus.OK);
						}
					} else if (codeflightBookingInitiate.equalsIgnoreCase("T01")) {

						req.setTransactionId("" + System.currentTimeMillis());
						req.setAdults("" + session.getAttribute("adults"));
						req.setChilds("" + session.getAttribute("childs"));
						req.setInfants("" + session.getAttribute("infants"));
						req.setDestination("" + session.getAttribute("destination"));

						String airRePriceRQ = travelflightapi.AirBookRQreturnwithPaymentGatway(req, source, destination,
								session);
						session.setAttribute("FlightBookWithSplitPaymentReturn", airRePriceRQ);
						session.setAttribute("SPaymentReturnTDetails", travellerDetails);

						return new ResponseEntity<String>(
								"T01" + "@" + "Splitpayment" + "@" + objflightBookingInitiate.getString("balance"),
								HttpStatus.OK);
					} else {
						flightBookReq.setSuccess(false);
						travelflightapi.flightBookingSucess(flightBookReq, travellerDetails);
						return new ResponseEntity<String>(
								"F04" + "@" + "" + objflightBookingInitiate.getString("message"), HttpStatus.OK);
					}

				} else {
					return new ResponseEntity<String>("F03" + "@" + "Please Login and try Again Later", HttpStatus.OK);
				}
			} else {
				return new ResponseEntity<String>("F03" + "@" + "Please Login and try Again Later", HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>("F04" + "@" + "Service Down Please Try Again Later", HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/Flight/InternationalReturnCheckOut", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<String> getFlightInternationalReturnCheckOut(HttpSession session,
			@RequestBody ReturnFlightBookRequest req) throws Exception {

		String sessionId = (String) session.getAttribute("sessionId");
		try {
			if (sessionId != null && sessionId.length() != 0) {
				String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
				if (authority != null) {
					String ticketDetails = "" + session.getAttribute("ticketDetails");

					List<TravellerFlightDetails> travellerDetails = getTravDetails(req.getFirstName(),
							req.getLastName(), req.getUseradultsgender(), req.getFirstNamechild(),
							req.getLastNamechild(), req.getUserchildsgender(), req.getFirstNameinfant(),
							req.getLastNameinfant(), req.getUserinfantsgender());

					FligthBookReq fligthBookReq = new FligthBookReq();

					fligthBookReq.setSessionId(sessionId);
					fligthBookReq.setPaymentAmount(req.getGrandtotal());
					fligthBookReq.setTicketDetails(ticketDetails);
					fligthBookReq.setPaymentMethod(PaymentType.Wallet.getKey());
					fligthBookReq.setSource(req.getOrigin());
					fligthBookReq.setDestination(req.getDestination());

					double grandtotal = Double.parseDouble(req.getGrandtotal()) - 200;
					req.setGrandtotal(grandtotal + "");

					String flightBookingInitiate = travelflightapi.flightBookingInitiate(fligthBookReq,
							travellerDetails);

					JSONObject objflightBookingInitiate = new JSONObject(flightBookingInitiate);
					String codeflightBookingInitiate = objflightBookingInitiate.getString("code");
					if (codeflightBookingInitiate.equalsIgnoreCase("S00")) {

						String transactionRefNo = objflightBookingInitiate.getJSONObject("details")
								.getString("transactionRefNo");

						req.setTransactionId(transactionRefNo);
						req.setAdults("" + session.getAttribute("adults"));
						req.setChilds("" + session.getAttribute("childs"));
						req.setInfants("" + session.getAttribute("infants"));

						fligthBookReq.setTxnRefno(req.getTransactionId());

						org.json.JSONObject objmain1 = CreateJsonRequestFlight
								.createjsonBookApiinternationalFlightBook(req, session);

						String airRePriceRQ = travelflightapi.FlightBookPaymentGatway(objmain1.toString());

						JSONObject obj = new JSONObject(airRePriceRQ);

						String code = obj.getString("code");
						if (code.equalsIgnoreCase("S00")) {

							String bookingRefId = obj.getJSONObject("details").getString("bookingRefId");
							String transactionRefNomdex = obj.getJSONObject("details").getString("transactionRefNo");
							JSONArray tickets = obj.getJSONObject("details").getJSONObject("bookingDetail")
									.getJSONObject("pnrDetail").getJSONArray("tickets");
							String ticketNumber = "";
							String firstName = "";

							for (int i = 0; i < tickets.length(); i++) {
								ticketNumber = tickets.getJSONObject(i).getString("ticketNumber");
								travellerDetails.get(i).setTicketNo(ticketNumber);

							}

							fligthBookReq.setBookingRefId(bookingRefId);
							fligthBookReq.setMdexTxnRefNo(transactionRefNomdex);
							fligthBookReq.setStatus(Status.Booked.getValue());
							fligthBookReq.setSuccess(true);
							fligthBookReq.setMobile(req.getMobileNumber());
							fligthBookReq.setEmail(req.getEmailAddress());
							fligthBookReq.setFlightStatus(Status.Booked.getValue());

							String flightBookingSucess = travelflightapi.flightBookingSucess(fligthBookReq,
									travellerDetails);

							JSONObject flightBookingSucessobj = new JSONObject(flightBookingSucess);
							String flightBookingSucesscode = flightBookingSucessobj.getString("code");
							if (flightBookingSucesscode.equalsIgnoreCase("S00")) {
								return new ResponseEntity<String>("S00" + "@" + "Flight Successfully Booked.",
										HttpStatus.OK);
							} else {

								return new ResponseEntity<String>(
										"F04" + "@"
												+ "Your Flight Tickeet is Booked.But Payment not successful Please Contact Customer Care.",
										HttpStatus.OK);
							}
						} else {
							fligthBookReq.setSuccess(false);
							travelflightapi.flightBookingSucess(fligthBookReq, travellerDetails);
							return new ResponseEntity<String>("F04" + "@" + "Service Down Please Try Again Later",
									HttpStatus.OK);
						}
					} else if (codeflightBookingInitiate.equalsIgnoreCase("T01")) {

						req.setTransactionId("" + System.currentTimeMillis());
						req.setAdults("" + session.getAttribute("adults"));
						req.setChilds("" + session.getAttribute("childs"));
						req.setInfants("" + session.getAttribute("infants"));

						org.json.JSONObject objmain1 = CreateJsonRequestFlight
								.createjsonBookApiinternationalFlightBook(req, session);

						session.setAttribute("FlightBookWithSplitPaymentReturn", objmain1);
						return new ResponseEntity<String>("T01" + "@" + "Redirect To SplitPayment" + "@"
								+ objflightBookingInitiate.getString("balance"), HttpStatus.OK);
					} else {
						return new ResponseEntity<String>(
								"F04" + "@" + "" + objflightBookingInitiate.getString("message"), HttpStatus.OK);
					}

				} else {
					return new ResponseEntity<String>("F03" + "@" + "Please Login and try Again Later", HttpStatus.OK);
				}
			} else {
				return new ResponseEntity<String>("F03" + "@" + "Please Login and try Again Later", HttpStatus.OK);
			}

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>("F04" + "@" + "Service Down Please Try Again Later", HttpStatus.OK);
		}

	}

	@RequestMapping(value = "/Flight/Redirect", method = RequestMethod.POST)
	public String redirectLoadMoney(WEBSRedirectResponse ebsResponse, Model model, HttpSession session)
			throws Exception {

		String mobile = "" + session.getAttribute("mobile");
		String email = "" + session.getAttribute("email");
		String orig = "" + session.getAttribute("origin");
		String dest = "" + session.getAttribute("destination");

		String ticketDetails = "" + session.getAttribute("ticketDetails");
		session.setAttribute("msgvalue", "1");
		EBSRedirectResponse ebsRedirectResponse = ConvertUtil.convertFromWEBS(ebsResponse);
		String sessionId = (String) session.getAttribute("sessionId");
		String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
		if (authority != null) {
			@SuppressWarnings("unchecked")
			List<TravellerFlightDetails> travellerDetails = (List<TravellerFlightDetails>) session
					.getAttribute("travellerDetails");

			FligthBookReq fligthBookReq = new FligthBookReq();

			fligthBookReq.setSessionId(sessionId);
			fligthBookReq.setPaymentAmount(ebsRedirectResponse.getAmount());
			fligthBookReq.setTicketDetails(ticketDetails);
			fligthBookReq.setPaymentMethod(PaymentType.EBS.getKey());
			fligthBookReq.setSource(orig);
			fligthBookReq.setDestination(dest);

			ResponseDTO result = loadMoneyApi.verifyEBSTransaction(ebsRedirectResponse);
			if (result.isSuccess()) {
				ebsRedirectResponse.setSuccess(true);
				ebsRedirectResponse.setResponseCode("0");
			} else {
				ebsRedirectResponse.setSuccess(false);
				ebsRedirectResponse.setResponseCode("1");
			}
			EBSRedirectResponse redirectResponse = loadMoneyApi
					.processRedirectSDKSplitpaymentMoney(ebsRedirectResponse);
			if (redirectResponse.isSuccess() || redirectResponse.getResponseCode().equals("0")) {

				String origin = "" + session.getAttribute("source");
				String destination = "" + session.getAttribute("destination");

				String jsonflightRequest = "" + session.getAttribute("FlightBookWithPaymentGateway");
				String airRePriceRQ = travelflightapi.FlightBookPaymentGatway(jsonflightRequest);
				JSONObject obj = new JSONObject(airRePriceRQ);

				String code = obj.getString("code");
				if (code.equalsIgnoreCase("S00")) {

					String bookingRefId = obj.getJSONObject("details").getString("bookingRefId");
					String transactionRefNomdex = obj.getJSONObject("details").getString("transactionRefNo");
					JSONArray tickets = obj.getJSONObject("details").getJSONObject("bookingDetail")
							.getJSONObject("pnrDetail").getJSONArray("tickets");
					String ticketNumber = "";
					String firstName = "";
					JSONArray objccpnrs = obj.getJSONObject("details").getJSONObject("bookingDetail")
							.getJSONObject("pnrDetail").getJSONArray("pnrs");
					String pnrNo = objccpnrs.getJSONObject(0).getString("pnr");

					for (int i = 0; i < tickets.length(); i++) {
						ticketNumber = tickets.getJSONObject(i).getString("ticketNumber");
						travellerDetails.get(i).setTicketNo(ticketNumber);

					}

					fligthBookReq.setBookingRefId(bookingRefId);
					fligthBookReq.setMdexTxnRefNo(transactionRefNomdex);
					fligthBookReq.setStatus(Status.Booked.getValue());
					fligthBookReq.setTxnRefno(ebsRedirectResponse.getMerchantRefNo());
					fligthBookReq.setSuccess(true);
					fligthBookReq.setEmail(email);
					fligthBookReq.setMobile(mobile);
					fligthBookReq.setFlightStatus("Booked");
					fligthBookReq.setPaymentStatus("Success");
					fligthBookReq.setPnrNo(pnrNo);

					travelflightapi.flightPaymentGatewaySucess(fligthBookReq,travellerDetails);
					session.setAttribute("flightMsg", "Transasction Successful.Your Flight Is Booked.");

				} else {

					fligthBookReq.setSuccess(false);
					fligthBookReq.setFlightStatus("Processing");
					fligthBookReq.setPaymentStatus("success");
					travelflightapi.flightBookingSucess(fligthBookReq, travellerDetails);
					session.setAttribute("flighterrMsg",
							"Transasction Successful,Your Flight Is Not Booked.Please Contact Your Customer Care");

				}

			} else {
				fligthBookReq.setSuccess(false);
				fligthBookReq.setFlightStatus("Processing");
				fligthBookReq.setPaymentStatus("Failed");
				travelflightapi.flightBookingSucess(fligthBookReq, travellerDetails);

				session.setAttribute("flighterrMsg", "Transaction failed, Please try again later.");
			}
			model.addAttribute("loadmoneyResponse", redirectResponse);
			return "User/Loading";
		} else {
			return "redirect:/Home";
		}

	}

	@RequestMapping(value = "/Flight/VRedirect", method = RequestMethod.POST)
	public String redirectVNetLoadMoney(VRedirectResponse dto, Model model, HttpSession session) throws JSONException {
		String sessionId = (String) session.getAttribute("sessionId");
		String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
		if (authority != null) {
			String mobile = "" + session.getAttribute("mobile");
			String email = "" + session.getAttribute("email");
			String origin = "" + session.getAttribute("origin");
			String dest = "" + session.getAttribute("destination");

			String ticketDetails = "" + session.getAttribute("ticketDetails");

			ResponseDTO responseDTO = loadMoneyApi.handleRedirectRequestFlight(dto);
			if (responseDTO.getCode().equalsIgnoreCase("S00")) {
				String jsonflightRequest = "" + session.getAttribute("FlightBookWithPaymentGateway");
				String airRePriceRQ = travelflightapi.FlightBookPaymentGatway(jsonflightRequest);
				JSONObject obj = new JSONObject(airRePriceRQ);
				String code = obj.getString("code");
				if (code.equalsIgnoreCase("S00")) {
					@SuppressWarnings("unchecked")
					List<TravellerFlightDetails> travellerDetails = (List<TravellerFlightDetails>) session
							.getAttribute("travellerDetails");

					FligthBookReq fligthBookReq = new FligthBookReq();

					fligthBookReq.setSessionId(sessionId);
					fligthBookReq.setPaymentAmount(dto.getAmount());
					fligthBookReq.setTicketDetails(ticketDetails);
					fligthBookReq.setSource(origin);
					fligthBookReq.setDestination(dest);

					fligthBookReq.setPaymentMethod(PaymentType.VNet.getKey());
					String bookingRefId = obj.getJSONObject("details").getString("bookingRefId");
					String transactionRefNomdex = obj.getJSONObject("details").getString("transactionRefNo");
					JSONArray objcc = obj.getJSONObject("details").getJSONObject("bookingDetail")
							.getJSONObject("pnrDetail").getJSONArray("tickets");
					JSONArray tickets = obj.getJSONObject("details").getJSONObject("bookingDetail")
							.getJSONObject("pnrDetail").getJSONArray("tickets");
					String ticketNumber = "";
					String firstName = "";
					JSONArray objccpnrs = obj.getJSONObject("details").getJSONObject("bookingDetail")
							.getJSONObject("pnrDetail").getJSONArray("pnrs");
					String pnrNo = objccpnrs.getJSONObject(0).getString("pnr");

					for (int i = 0; i < tickets.length(); i++) {
						ticketNumber = tickets.getJSONObject(i).getString("ticketNumber");
						travellerDetails.get(i).setTicketNo(ticketNumber);

					}

					fligthBookReq.setBookingRefId(bookingRefId);
					fligthBookReq.setMdexTxnRefNo(transactionRefNomdex);
					fligthBookReq.setStatus(Status.Booked.getValue());
					fligthBookReq.setTxnRefno(dto.getMerchantRefNo());
					fligthBookReq.setSuccess(true);
					fligthBookReq.setEmail(email);
					fligthBookReq.setMobile(mobile);
					fligthBookReq.setPnrNo(pnrNo);

					String flightBookingSucess = travelflightapi.flightPaymentGatewaySucess(fligthBookReq,
							travellerDetails);

					session.setAttribute("flightMsg", "Transasction Successful.Your Flight Is Booked.");
				} else {

					session.setAttribute("flighterrMsg",
							"Transasction Successful,Your Flight Is Not Booked.Please Contact Your Customer Care");

				}
			} else {
				session.setAttribute("flighterrMsg", "Transaction failed, Please try again later.");
			}

			model.addAttribute("response", responseDTO);
			return "User/Loading";
		} else {
			return "redirect:/Home";
		}
	}

	// ----------International Roundway Finish-----------//

	@RequestMapping(value = "/Flight/ReturnRedirect", method = RequestMethod.POST)
	public String redirectReturnFlight(WEBSRedirectResponse ebsResponse, Model model, HttpSession session)
			throws Exception {

		String mobile = "" + session.getAttribute("mobile");
		String email = "" + session.getAttribute("email");
		String orig = "" + session.getAttribute("origin");
		String desti = "" + session.getAttribute("destination");

		String ticketDetails = "" + session.getAttribute("ticketDetails");

		session.setAttribute("msgvalue", "1");
		EBSRedirectResponse ebsRedirectResponse = ConvertUtil.convertFromWEBS(ebsResponse);
		String sessionId = (String) session.getAttribute("sessionId");
		String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
		if (authority != null) {
			@SuppressWarnings("unchecked")
			List<TravellerFlightDetails> travellerDetails = (List<TravellerFlightDetails>) session
					.getAttribute("travellerDetails");

			FligthBookReq fligthBookReq = new FligthBookReq();

			fligthBookReq.setSessionId(sessionId);
			fligthBookReq.setPaymentAmount(ebsRedirectResponse.getAmount());
			fligthBookReq.setSource(orig);
			fligthBookReq.setDestination(desti);

			String source = "" + session.getAttribute("airRePriceRQoneway");
			String destination = "" + session.getAttribute("airRePriceRQroundway");

			if (!source.equalsIgnoreCase("null") && !destination.equalsIgnoreCase("null")) {
				JSONObject src = new JSONObject(source);
				JSONObject dest = new JSONObject(destination);

				JSONArray journeys11 = src.getJSONObject("details").getJSONArray("journeys");
				JSONArray segments11 = journeys11.getJSONObject(0).getJSONArray("segments");
				JSONArray bonds1 = segments11.getJSONObject(0).getJSONArray("bonds");

				JSONArray journeys12 = dest.getJSONObject("details").getJSONArray("journeys");
				JSONArray segments12 = journeys12.getJSONObject(0).getJSONArray("segments");
				JSONArray bonds2 = segments12.getJSONObject(0).getJSONArray("bonds");
				bonds2.get(0);
				bonds1.put(bonds2.get(0));
				JSONObject ticketDetail = CreateJsonRequestFlight.createjsonForTicket(bonds1);
				ticketDetails = ticketDetail + "";
				session.setAttribute("airRePriceRQoneway", null);
				session.setAttribute("airRePriceRQroundway", null);
			}

			fligthBookReq.setTicketDetails(ticketDetails + "");

			fligthBookReq.setPaymentMethod(PaymentType.EBS.getKey());

			ResponseDTO result = loadMoneyApi.verifyEBSTransaction(ebsRedirectResponse);
			if (result.isSuccess()) {
				ebsRedirectResponse.setSuccess(true);
				ebsRedirectResponse.setResponseCode("0");
			} else {
				ebsRedirectResponse.setSuccess(false);
				ebsRedirectResponse.setResponseCode("1");
			}
			EBSRedirectResponse redirectResponse = loadMoneyApi
					.processRedirectSDKSplitpaymentMoney(ebsRedirectResponse);
			if (redirectResponse.isSuccess() || redirectResponse.getResponseCode().equals("0")) {

				String origin = "" + session.getAttribute("source");
				// String destination = "" +
				// session.getAttribute("destination");

				String jsonflightRequest = "" + session.getAttribute("FlightBookWithPaymentGatewayReturn");
				String airRePriceRQ = travelflightapi.FlightBookPaymentGatway(jsonflightRequest);
				JSONObject obj = new JSONObject(airRePriceRQ);

				String code = obj.getString("code");
				if (code.equalsIgnoreCase("S00")) {

					String bookingRefId = obj.getJSONObject("details").getString("bookingRefId");
					String transactionRefNomdex = obj.getJSONObject("details").getString("transactionRefNo");
					JSONArray tickets = obj.getJSONObject("details").getJSONObject("bookingDetail")
							.getJSONObject("pnrDetail").getJSONArray("tickets");
					String ticketNumber = "";
					String firstName = "";
					JSONArray objccpnrs = obj.getJSONObject("details").getJSONObject("bookingDetail")
							.getJSONObject("pnrDetail").getJSONArray("pnrs");
					String pnrNo = objccpnrs.getJSONObject(0).getString("pnr");

					for (int i = 0; i < tickets.length(); i++) {
						ticketNumber = tickets.getJSONObject(i).getString("ticketNumber");
						travellerDetails.get(i).setTicketNo(ticketNumber);

					}

					fligthBookReq.setBookingRefId(bookingRefId);
					fligthBookReq.setMdexTxnRefNo(transactionRefNomdex);
					fligthBookReq.setStatus(Status.Booked.getValue());
					fligthBookReq.setTxnRefno(ebsRedirectResponse.getMerchantRefNo());
					fligthBookReq.setSuccess(true);
					fligthBookReq.setEmail(email);
					fligthBookReq.setMobile(mobile);
					fligthBookReq.setFlightStatus(Status.Booked.getValue());
					fligthBookReq.setPnrNo(pnrNo);

					String flightBookingSucess = travelflightapi.flightPaymentGatewaySucess(fligthBookReq,
							travellerDetails);

					session.setAttribute("msg", "Transasction Successful.Your Flight Is Booked.");

				} else {

					fligthBookReq.setSuccess(false);
					travelflightapi.flightBookingSucess(fligthBookReq, travellerDetails);
					session.setAttribute("msg",
							"Transasction Successful,Your Flight Is Not Booked.Please Contact Your Customer Care");

				}

			} else {
				fligthBookReq.setSuccess(false);
				travelflightapi.flightBookingSucess(fligthBookReq, travellerDetails);

				session.setAttribute("msg", "Transaction failed, Please try again later.");
			}
			model.addAttribute("loadmoneyResponse", redirectResponse);
			return "User/Loading";
		} else {
			return "redirect:/Home";
		}
	}

	@RequestMapping(value = "/Flight/Return/VRedirect", method = RequestMethod.POST)
	public String returnRedirectVNetLoadMoney(VRedirectResponse dto, Model model, HttpSession session)
			throws Exception {

		String sessionId = (String) session.getAttribute("sessionId");
		String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);

		if (authority != null) {
			String mobile = "" + session.getAttribute("mobile");
			String email = "" + session.getAttribute("email");
			String origin = "" + session.getAttribute("origin");
			String desti = "" + session.getAttribute("destination");
			// String ticketDetails = "" +
			// session.getAttribute("ticketDetails");

			ResponseDTO responseDTO = loadMoneyApi.handleRedirectRequestFlight(dto);
			if (responseDTO.getCode().equalsIgnoreCase("S00")) {
				String jsonflightRequest = "" + session.getAttribute("FlightBookWithPaymentGateway");
				String airRePriceRQ = travelflightapi.FlightBookPaymentGatway(jsonflightRequest);
				JSONObject obj = new JSONObject(airRePriceRQ);
				String code = obj.getString("code");
				if (code.equalsIgnoreCase("S00")) {
					@SuppressWarnings("unchecked")
					List<TravellerFlightDetails> travellerDetails = (List<TravellerFlightDetails>) session
							.getAttribute("travellerDetails");

					FligthBookReq fligthBookReq = new FligthBookReq();

					fligthBookReq.setSessionId(sessionId);
					fligthBookReq.setPaymentAmount(dto.getAmount());
					fligthBookReq.setSource(origin);
					fligthBookReq.setDestination(desti);

					String source = "" + session.getAttribute("airRePriceRQoneway");
					String destination = "" + session.getAttribute("airRePriceRQroundway");

					JSONObject src = new JSONObject(source);
					JSONObject dest = new JSONObject(destination);

					JSONArray journeys11 = src.getJSONObject("details").getJSONArray("journeys");
					JSONArray segments11 = journeys11.getJSONObject(0).getJSONArray("segments");
					JSONArray bonds1 = segments11.getJSONObject(0).getJSONArray("bonds");

					JSONArray journeys12 = dest.getJSONObject("details").getJSONArray("journeys");
					JSONArray segments12 = journeys12.getJSONObject(0).getJSONArray("segments");
					JSONArray bonds2 = segments12.getJSONObject(0).getJSONArray("bonds");
					bonds2.get(0);
					bonds1.put(bonds2.get(0));

					JSONObject ticketDetails = CreateJsonRequestFlight.createjsonForTicket(bonds1);

					fligthBookReq.setTicketDetails(ticketDetails + "");

					fligthBookReq.setPaymentMethod(PaymentType.VNet.getKey());

					String bookingRefId = obj.getJSONObject("details").getString("bookingRefId");
					String transactionRefNomdex = obj.getJSONObject("details").getString("transactionRefNo");
					JSONArray objcc = obj.getJSONObject("details").getJSONObject("bookingDetail")
							.getJSONObject("pnrDetail").getJSONArray("tickets");
					JSONArray tickets = obj.getJSONObject("details").getJSONObject("bookingDetail")
							.getJSONObject("pnrDetail").getJSONArray("tickets");
					String ticketNumber = "";
					String firstName = "";
					JSONArray objccpnrs = obj.getJSONObject("details").getJSONObject("bookingDetail")
							.getJSONObject("pnrDetail").getJSONArray("pnrs");
					String pnrNo = objccpnrs.getJSONObject(0).getString("pnr");

					for (int i = 0; i < tickets.length(); i++) {
						ticketNumber = tickets.getJSONObject(i).getString("ticketNumber");
						travellerDetails.get(i).setTicketNo(ticketNumber);

					}

					fligthBookReq.setBookingRefId(bookingRefId);
					fligthBookReq.setMdexTxnRefNo(transactionRefNomdex);
					fligthBookReq.setStatus(Status.Booked.getValue());
					fligthBookReq.setTxnRefno(dto.getMerchantRefNo());
					fligthBookReq.setSuccess(true);
					fligthBookReq.setEmail(email);
					fligthBookReq.setMobile(mobile);
					fligthBookReq.setPnrNo(pnrNo);

					String flightBookingSucess = travelflightapi.flightPaymentGatewaySucess(fligthBookReq,
							travellerDetails);

					session.setAttribute("msg", "Transasction Successful.Your Flight Is Booked.");
				} else {

					session.setAttribute("msg",
							"Transasction Successful,Your Flight Is Not Booked.Please Contact Your Customer Care");

				}
			} else {
				session.setAttribute("msg", "Transaction failed, Please try again later.");
			}

			model.addAttribute("response", responseDTO);
			return "User/Loading";
		} else {
			return "redirect:/Home";
		}
	}

	@RequestMapping(value = "/Flight/RedirectSplitpayment", method = RequestMethod.POST)
	public String redirectLoadMoneySplitpayment(WEBSRedirectResponse ebsResponse, Model model, HttpSession session)
			throws Exception {
		String ticketDetails = "" + session.getAttribute("ticketDetails");

		String mobile = "" + session.getAttribute("mobile");
		String email = "" + session.getAttribute("email");
		String origin = "" + session.getAttribute("origin");
		String dest = "" + session.getAttribute("destination");

		String firstName1 = "~" + session.getAttribute("firstName");
		session.setAttribute("msgvalue", "1");
		EBSRedirectResponse ebsRedirectResponse = ConvertUtil.convertFromWEBS(ebsResponse);
		String sessionId = (String) session.getAttribute("sessionId");
		String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
		if (authority != null) {
			ResponseDTO result = loadMoneyApi.verifyEBSTransaction(ebsRedirectResponse);
			if (result.isSuccess()) {
				ebsRedirectResponse.setSuccess(true);
				ebsRedirectResponse.setResponseCode("0");
			} else {
				ebsRedirectResponse.setSuccess(false);
				ebsRedirectResponse.setResponseCode("1");
			}
			EBSRedirectResponse redirectResponse = loadMoneyApi.processRedirectSDK(ebsRedirectResponse);
			if (redirectResponse.isSuccess() || redirectResponse.getResponseCode().equals("0")) {

				String bookingAmt = (String) session.getAttribute("splitFlightBookAmt");

				FligthBookReq fligthBookReq = new FligthBookReq();

				fligthBookReq.setSessionId(sessionId);
				fligthBookReq.setPaymentAmount(bookingAmt);
				fligthBookReq.setTicketDetails(ticketDetails);
				fligthBookReq.setPaymentMethod(PaymentType.EBS.getKey());
				fligthBookReq.setSource(origin);
				fligthBookReq.setDestination(dest);

				@SuppressWarnings("unchecked")
				List<TravellerFlightDetails> travellerDetails = (List<TravellerFlightDetails>) session
						.getAttribute("SPaymentTDetails");

				String flightBookingInitiate = travelflightapi.flightBookingInitiate(fligthBookReq, travellerDetails);

				JSONObject objflightBookingInitiate = new JSONObject(flightBookingInitiate);
				String codeflightBookingInitiate = objflightBookingInitiate.getString("code");
				if (codeflightBookingInitiate.equalsIgnoreCase("S00")) // (true)//
				{
					String transactionRefNo = objflightBookingInitiate.getJSONObject("details")
							.getString("transactionRefNo");
					String jsonflightRequest = "" + session.getAttribute("FlightBookWithSplitPayment");

					System.err.println("Json is:: " + jsonflightRequest);

					String airRePriceRQ = travelflightapi.FlightBookPaymentGatway(jsonflightRequest);
					JSONObject obj = new JSONObject(airRePriceRQ);
					String code = obj.getString("code");
					if (code.equalsIgnoreCase("S00")) {
						String bookingRefId = obj.getJSONObject("details").getString("bookingRefId");
						String transactionRefNomdex = obj.getJSONObject("details").getString("transactionRefNo");
						JSONArray tickets = obj.getJSONObject("details").getJSONObject("bookingDetail")
								.getJSONObject("pnrDetail").getJSONArray("tickets");
						String ticketNumber = "";
						String firstName = "";

						for (int i = 0; i < tickets.length(); i++) {
							ticketNumber = tickets.getJSONObject(i).getString("ticketNumber");
							travellerDetails.get(i).setTicketNo(ticketNumber);

						}

						fligthBookReq.setBookingRefId(bookingRefId);
						fligthBookReq.setMdexTxnRefNo(transactionRefNomdex);
						fligthBookReq.setStatus(Status.Booked.getValue());
						fligthBookReq.setTxnRefno(transactionRefNo);
						fligthBookReq.setSuccess(true);
						fligthBookReq.setEmail(email);
						fligthBookReq.setMobile(mobile);
						fligthBookReq.setFlightStatus(Status.Booked.getValue());

						String flightBookingSucess = travelflightapi.flightBookingSucess(fligthBookReq,
								travellerDetails);

						JSONObject flightBookingSucessobj = new JSONObject(flightBookingSucess);
						String flightBookingSucesscode = flightBookingSucessobj.getString("code");

						if (flightBookingSucesscode.equalsIgnoreCase("S00")) {
							session.setAttribute("msg", "Transasction Successful.Your Flight Is Booked.");
						} else {

							session.setAttribute("msg",
									"Your Flight Is Booked,But Transasction Not Successful,.Please Contact Your Customer Care");

						}

					} else {
						fligthBookReq.setSuccess(false);
						travelflightapi.flightBookingSucess(fligthBookReq, travellerDetails);
						session.setAttribute("msg", "Transasction Not Successful.Please Contact Your Customer Care");

					}

				} else {

					fligthBookReq.setSuccess(false);
					travelflightapi.flightBookingSucess(fligthBookReq, travellerDetails);

					session.setAttribute("msg",
							"Transasction Successful,Your Flight Is Not Booked.Please Contact Your Customer Care");
					session.setAttribute("msg", "Transaction failed, Please try again later.");
				}

			} else {
				session.setAttribute("msg", "Transaction failed, Please try again later.");
			}
			model.addAttribute("loadmoneyResponse", redirectResponse);
			return "User/Loading";
		} else {
			return "redirect:/Home";
		}
	}

	@RequestMapping(value = "/Flight/ReturnRedirectSplitpayment", method = RequestMethod.POST)
	public String redirectReturnFlightSplitpayment(WEBSRedirectResponse ebsResponse, Model model, HttpSession session)
			throws Exception {
		String ticketDetails = "" + session.getAttribute("ticketDetails");
		String mobile = "" + session.getAttribute("mobile");
		String email = "" + session.getAttribute("email");
		String origin = "" + session.getAttribute("origin");
		String dest = "" + session.getAttribute("destination");

		String firstName1 = "~" + session.getAttribute("firstName");
		session.setAttribute("msgvalue", "1");
		EBSRedirectResponse ebsRedirectResponse = ConvertUtil.convertFromWEBS(ebsResponse);
		String sessionId = (String) session.getAttribute("sessionId");
		String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
		if (authority != null) {
			ResponseDTO result = loadMoneyApi.verifyEBSTransaction(ebsRedirectResponse);
			if (result.isSuccess()) {
				ebsRedirectResponse.setSuccess(true);
				ebsRedirectResponse.setResponseCode("0");
			} else {
				ebsRedirectResponse.setSuccess(false);
				ebsRedirectResponse.setResponseCode("1");
			}
			EBSRedirectResponse redirectResponse = loadMoneyApi.processRedirectSDK(ebsRedirectResponse);
			if (redirectResponse.isSuccess() || redirectResponse.getResponseCode().equals("0")) {
				String bookAmt = (String) session.getAttribute("splitRountFlightAmt");

				List<TravellerFlightDetails> travellerFlightDetailsReturn = new ArrayList<>();

				FligthBookReq fligthBookReq = new FligthBookReq();

				fligthBookReq.setSessionId(sessionId);
				fligthBookReq.setPaymentAmount(bookAmt);
				fligthBookReq.setTicketDetails(ticketDetails);
				fligthBookReq.setPaymentMethod(PaymentType.EBS.getKey());
				fligthBookReq.setSource(origin);
				fligthBookReq.setDestination(dest);

				@SuppressWarnings("unchecked")
				List<TravellerFlightDetails> travellerDetails = (List<TravellerFlightDetails>) session
						.getAttribute("SPaymentReturnTDetails");
				travellerFlightDetailsReturn.addAll(travellerDetails);

				String flightBookingInitiate = travelflightapi.flightBookingInitiate(fligthBookReq,
						travellerFlightDetailsReturn);

				JSONObject objflightBookingInitiate = new JSONObject(flightBookingInitiate);
				String codeflightBookingInitiate = objflightBookingInitiate.getString("code");
				if (codeflightBookingInitiate.equalsIgnoreCase("S00")) // (true)//
				{

					String transactionRefNo = objflightBookingInitiate.getJSONObject("details")
							.getString("transactionRefNo");

					String jsonflightRequest = "" + session.getAttribute("FlightBookWithSplitPaymentReturn");
					String airRePriceRQ = travelflightapi.FlightBookPaymentGatway(jsonflightRequest);
					JSONObject obj = new JSONObject(airRePriceRQ);
					String code = obj.getString("code");

					if (code.equalsIgnoreCase("S00")) {
						String bookingRefId = obj.getJSONObject("details").getString("bookingRefId");
						String transactionRefNomdex = obj.getJSONObject("details").getString("transactionRefNo");
						JSONArray tickets = obj.getJSONObject("details").getJSONObject("bookingDetail")
								.getJSONObject("pnrDetail").getJSONArray("tickets");
						String ticketNumber = "";
						String firstName = "";

						for (int i = 0; i < tickets.length(); i++) {
							ticketNumber = tickets.getJSONObject(i).getString("ticketNumber");
							travellerFlightDetailsReturn.get(i).setTicketNo(ticketNumber);
						}

						fligthBookReq.setBookingRefId(bookingRefId);
						fligthBookReq.setMdexTxnRefNo(transactionRefNomdex);
						fligthBookReq.setStatus(Status.Booked.getValue());
						fligthBookReq.setTxnRefno(transactionRefNo);
						fligthBookReq.setSuccess(true);
						fligthBookReq.setEmail(email);
						fligthBookReq.setMobile(mobile);
						fligthBookReq.setFlightStatus(Status.Booked.getValue());

						String flightBookingSucess = travelflightapi.flightBookingSucess(fligthBookReq,
								travellerFlightDetailsReturn);

						JSONObject flightBookingSucessobj = new JSONObject(flightBookingSucess);
						String flightBookingSucesscode = flightBookingSucessobj.getString("code");

						if (flightBookingSucesscode.equalsIgnoreCase("S00")) {
							session.setAttribute("msg", "Transasction Successful.Your Flight Is Booked.");
						} else {

							session.setAttribute("msg",
									"Your Flight Is Booked,But Transasction Not Successful,.Please Contact Your Customer Care");
						}

						session.setAttribute("msg", "Transasction Successful.Your Flight Is Booked.");
					} else {

						session.setAttribute("error",
								"Your Flight Is Booked,But Transasction Not Successful,.Please Contact Your Customer Care");

					}

				} else {

					fligthBookReq.setSuccess(false);
					travelflightapi.flightBookingSucess(fligthBookReq, travellerDetails);

					session.setAttribute("msg", "Transaction failed, Please try again later.");
				}

			} else {

				session.setAttribute("msg", "Transaction failed, Please try again later.");
			}
			model.addAttribute("loadmoneyResponse", redirectResponse);
			return "User/Loading";
		} else {
			return "redirect:/Home";
		}
	}

	private List<TravellerFlightDetails> getTravDetails(String adultFNames, String adultLNames, String adultGenders,
			String childFNames, String childLNames, String childGenders, String infantFNames, String infantLNames,
			String infantGenders) {
		String adultFName = adultFNames;
		String adultLName = adultLNames;
		String adultGender = adultGenders;
		String childFName = childFNames;
		String childLName = childLNames;
		String childGender = childGenders;
		String infantFName = infantFNames;
		String infantLName = infantLNames;
		String infantGender = infantGenders;
		String gender[] = null;
		String fName[] = null;
		String lName[] = null;

		if (adultFName != null) {
			if (!adultFName.isEmpty()) {

				fName = adultFName.split("~");
				lName = adultLName.split("@");
				gender = adultGender.split("#");
			}
		}

		List<TravellerFlightDetails> list = new ArrayList<>();

		if (fName != null) {

			for (int i = 1; i < fName.length; i++) {
				String pGender = null;
				TravellerFlightDetails travellerDTO = new TravellerFlightDetails();
				travellerDTO.setfName(fName[i]);
				travellerDTO.setlName(lName[i]);
				if (gender[i].equalsIgnoreCase("Mr")) {
					pGender = "Male";
				} else {
					pGender = "Female";
				}
				travellerDTO.setGender(pGender);
				travellerDTO.setType("Adult");
				list.add(travellerDTO);
			}
		}
		fName = null;
		lName = null;
		gender = null;

		if (childFName != null) {
			if (!childFName.isEmpty()) {
				fName = childFName.split("~");
				lName = childLName.split("@");
				gender = childGender.split("#");
			}
		}
		if (fName != null) {
			for (int i = 1; i < fName.length; i++) {
				String pGender = null;
				TravellerFlightDetails travellerDTO = new TravellerFlightDetails();
				travellerDTO.setfName(fName[i]);
				travellerDTO.setlName(lName[i]);
				if (gender[i].equalsIgnoreCase("Mr")) {
					pGender = "Male";
				} else {
					pGender = "Female";
				}
				travellerDTO.setGender(pGender);
				travellerDTO.setType("Child");
				list.add(travellerDTO);
			}
		}
		fName = null;
		lName = null;
		gender = null;

		if (infantFName != null) {
			if (!infantFName.isEmpty()) {
				fName = infantFName.split("~");
				lName = infantLName.split("@");
				gender = infantGender.split("#");
			}
		}
		if (fName != null) {
			for (int i = 1; i < fName.length; i++) {
				String pGender = null;
				TravellerFlightDetails travellerDTO = new TravellerFlightDetails();
				travellerDTO.setfName(fName[i]);
				travellerDTO.setlName(lName[i]);
				if (gender[i].equalsIgnoreCase("Mr")) {
					pGender = "Male";
				} else {
					pGender = "Female";
				}
				travellerDTO.setGender(pGender);
				travellerDTO.setType("Infant");
				list.add(travellerDTO);
			}
		}
		return list;
	}

	@RequestMapping(value = "/Flight/MyTickets", method = RequestMethod.GET)
	public String getAllTicket(@ModelAttribute BookTicketReq dto, ModelMap modelMap, HttpSession session) {
		FlightResponseDTO resp = new FlightResponseDTO();
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
					try {

						dto.setSessionId(sessionId);
						resp = travelflightapi.getAllBookTickets(sessionId);

						modelMap.addAttribute("details", resp.getDetails());

						session.setAttribute("flightMsg", "");
						session.setAttribute("flighterrMsg", "");

						modelMap.addAttribute("val", "flight");
						modelMap.addAttribute("flightMsg", "");
						modelMap.addAttribute("flighterrMsg", "");

						return "User/Travel/MyBookingTickets";

					} catch (Exception e) {
						System.out.println(e);
						e.printStackTrace();
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
					}
				} else {
					resp.setCode("F00");
					resp.setMessage("Unauthorised access");
					resp.setStatus("FAILED");
				}
			} else {
				resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
				resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
				resp.setMessage("Session expired");

			}
		} else {
			resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
			resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
			resp.setMessage("Session expired");

		}
		return "redirect:/Home";
	}

	// Get Method finish
	@RequestMapping(method = RequestMethod.POST, value = "/Flight/OneWays", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<String> getFlightOneWay(HttpSession session, @RequestBody TravelFlightRequest req) {

		try {

			String tripype = "";
			String journeyTime = "";

			String date = req.getBeginDate();
			SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy");
			java.util.Date dateq = sdf1.parse(date);
			java.sql.Date sqlStartDate = new java.sql.Date(dateq.getTime());
			req.setBeginDate("" + sqlStartDate);
			if (req.getTripType().equals("RoundTrip")) {
				String enddate = req.getEndDate();

				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				java.util.Date dateend = sdf.parse(enddate);
				java.sql.Date sqlStartDat = new java.sql.Date(dateend.getTime());
				req.setEndDate("" + sqlStartDat);
			} else {
				req.setEndDate("");
			}
			String sessionId = (String) session.getAttribute("sessionId");
			if (sessionId != null && sessionId.length() != 0) {
				String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
				if (authority != null) {

					session.setAttribute("bDate", req.getBeginDate());
					session.setAttribute("eDate", req.getEndDate());

					String source = travelflightapi.getFlightSearchAPI(req);

					System.err.println("Flight Search:: " + source);
					session.setAttribute("flightResponse", source);

					JSONObject obj = new JSONObject(source);
					String code = obj.getString("code");
					if (code.equalsIgnoreCase("S00")) {

						JSONArray outSegments = null;
						JSONArray inSegments = null;

						session.setAttribute("adults", req.getAdults());
						session.setAttribute("childs", req.getChilds());
						session.setAttribute("infants", req.getInfants());
						session.setAttribute("beginDate", req.getBeginDate());
						session.setAttribute("endDate", req.getEndDate());
						session.setAttribute("source", req.getOrigin());
						session.setAttribute("destination", req.getDestination());
						session.setAttribute("triptype", req.getTripType());
						HashSet<FlightResponse> flightNumber = new HashSet<FlightResponse>();
						HashSet<FlightResponse> flightNumbersotpage = new HashSet<FlightResponse>();

						ArrayList<FlightResponse> flightresponsearr = new ArrayList<FlightResponse>();
						ArrayList<FlightResponse> flightresponsearrreturn = new ArrayList<FlightResponse>();

						JSONObject jobj = obj.getJSONObject("details");

						JSONArray journeys = jobj.getJSONArray("journeys");

						if (journeys.length() == 2) {
							outSegments = journeys.getJSONObject(0).getJSONArray("segments");
							inSegments = journeys.getJSONObject(1).getJSONArray("segments");
							tripype = "DomesticRoundway";
						} else if (journeys.length() == 1) {
							outSegments = journeys.getJSONObject(0).getJSONArray("segments");
							if (outSegments.length() >= 1) {
								JSONArray bonds = outSegments.getJSONObject(0).getJSONArray("bonds");
								if (bonds.length() == 2) {
									tripype = "IntrnationalRoundway";
								} else {
									tripype = "Oneway";
								}
							}
						}

						session.setAttribute("tripype", tripype);
						session.setAttribute("outSegments", outSegments);
						session.setAttribute("inSegments", inSegments);

						session.setAttribute("flightNumbersotpage", flightNumbersotpage);
						session.setAttribute("flightnumberdata", flightNumber);
						session.setAttribute("flightdata", flightresponsearr);
						session.setAttribute("flightdatareturn", flightresponsearrreturn);
						session.setAttribute("origin", req.getOrigin());
						session.setAttribute("destination", req.getDestination());
						session.setAttribute("journeyTime", journeyTime);

						return new ResponseEntity<String>("S00" + "#" + tripype, HttpStatus.OK);

					} else {
						return new ResponseEntity<String>(code + "#" + obj.getString("message"), HttpStatus.OK);
					}
				} else {
					return new ResponseEntity<String>("F03", HttpStatus.OK);
				}
			} else {
				return new ResponseEntity<String>("F03", HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e);
			return new ResponseEntity<String>("F02", HttpStatus.OK);
		}
	}

}
