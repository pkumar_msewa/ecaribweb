package com.payqwikweb.controller.web;

import java.awt.Image;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.recaptcha.model.JCaptchaRequest;
import com.payqwikweb.api.IAuthenticationApi;
import com.payqwikweb.app.api.IAgentApi;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.request.ChangePasswordRequest;
import com.payqwikweb.app.model.request.ChangePasswordWithOtpRequest;
import com.payqwikweb.app.model.request.EditProfileRequest;
import com.payqwikweb.app.model.request.ForgetPasswordUserRequest;
import com.payqwikweb.app.model.request.ReceiptsRequest;
import com.payqwikweb.app.model.request.UploadPictureRequest;
import com.payqwikweb.app.model.response.ChangePasswordResponse;
import com.payqwikweb.app.model.response.ChangePasswordWithOtpResponse;
import com.payqwikweb.app.model.response.EditProfileResponse;
import com.payqwikweb.app.model.response.ForgetPasswordUserResponse;
import com.payqwikweb.app.model.response.ReceiptsResponse;
import com.payqwikweb.app.model.response.TransactionDTO;
import com.payqwikweb.app.model.response.UploadPictureResponse;
import com.payqwikweb.model.error.EditProfileError;
import com.payqwikweb.util.APIUtils;
import com.payqwikweb.util.Authorities;
import com.payqwikweb.util.JSONParserUtil;
import com.payqwikweb.util.ModelMapKey;
import com.payqwikweb.validation.ChangePasswordValidation;
import com.payqwikweb.validation.CommonValidation;
import com.payqwikweb.validation.EditProfileValidation;
import com.payqwikweb.validation.KycValidation;

@Controller
@RequestMapping("/Agent")
public class AgentEditProfilecontroller implements MessageSourceAware {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private MessageSource messageSource;
	private final EditProfileValidation editProfileValidation;
	private final ChangePasswordValidation changePasswordValidation;
	private final IAgentApi agentApi;
	private final IAuthenticationApi authenticationApi;
	// private final IVerificationApi verificationApi;
	private final KycValidation kycValidation;

	private static SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");

	/*
	 * public UserController(EditProfileValidation editProfileValidation,
	 * ChangePasswordValidation changePasswordValidation, IagentApi agentApi,
	 * IAuthenticationApi authenticationApi, IVerificationApi verificationApi,
	 * KycValidation kycValidation) { this.editProfileValidation =
	 * editProfileValidation; this.changePasswordValidation =
	 * changePasswordValidation; this.agentApi = agentApi;
	 * this.authenticationApi = authenticationApi; this.verificationApi =
	 * verificationApi; this.kycValidation = kycValidation; }
	 */

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	public AgentEditProfilecontroller(EditProfileValidation editProfileValidation,
			ChangePasswordValidation changePasswordValidation, IAgentApi agentApi, IAuthenticationApi authenticationApi,
			KycValidation kycValidation) {
		super();
		this.editProfileValidation = editProfileValidation;
		this.changePasswordValidation = changePasswordValidation;
		this.agentApi = agentApi;
		this.authenticationApi = authenticationApi;
		this.kycValidation = kycValidation;
	}

	@RequestMapping(value = "/AgentEditProfile/Process", method = RequestMethod.POST)
	public ResponseEntity<EditProfileResponse> processAgentEditedDetails(
			@ModelAttribute EditProfileRequest dto, HttpServletRequest request,
			HttpServletResponse response, Model modelMap, HttpSession session) {
		EditProfileResponse resp = new EditProfileResponse();
		String sessionId = (dto.getSessionId()==null)?(String)session.getAttribute("sessionId"):dto.getSessionId();
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
			if (authority != null) {
				if (authority.contains(Authorities.AGENT) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionId);
					resp = agentApi.editProfile(dto, Role.AGENT);
					modelMap.addAttribute("message", resp.getMessage());
					return new ResponseEntity<EditProfileResponse>(resp, HttpStatus.OK);
				}
			}
		}
		return new ResponseEntity<EditProfileResponse>(resp, HttpStatus.OK);
	}

	@RequestMapping(value = "/AgentUploadPicture", method = RequestMethod.POST)
	public String processAgentProfilePicture(@ModelAttribute UploadPictureRequest dto, HttpServletRequest request,
			HttpServletResponse response, RedirectAttributes modelMap, HttpSession session) {
		UploadPictureResponse result = new UploadPictureResponse();
		MultipartFile file = dto.getProfilePicture();
		String contentType = file.getContentType();
		boolean validImage = isValidImage(file);
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
			if (authority != null) {
				if (authority.contains(Authorities.AGENT) && authority.contains(Authorities.AUTHENTICATED)) {
					if (validImage) {
						dto.setSessionId(sessionId);
						result = agentApi.uploadPicture(dto, Role.AGENT);
						if (result.isSuccess()) {
							modelMap.addAttribute(ModelMapKey.MESSAGE, result.getMessage());
						} else {
							modelMap.addAttribute(ModelMapKey.ERROR, result.getMessage());
						}
					} else {
						modelMap.addAttribute(ModelMapKey.ERROR, "Please enter valid file format and valid size");
					}
				}
			}
		}
		return "redirect:/";

	}

	@RequestMapping(value = "/AgentUpdatePassword/Process", method = RequestMethod.POST)
	public ResponseEntity<ChangePasswordResponse> processAgentNewPassword(
			@ModelAttribute("updatePassword") ChangePasswordRequest dto, HttpServletRequest request,
			HttpServletResponse response, Model modelMap, HttpSession session) {
		dto.setUsername((String) (session.getAttribute("username")));
		String sessionId = (String) session.getAttribute("sessionId");
		ChangePasswordResponse resp = new ChangePasswordResponse();
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
			if (authority != null) {
				if (authority.contains(Authorities.AGENT) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionId);
					resp = agentApi.changePassword(dto, Role.AGENT);
					modelMap.addAttribute("message", resp.getMessage());
					return new ResponseEntity<ChangePasswordResponse>(resp, HttpStatus.OK);
				}
			}
		}
		return new ResponseEntity<ChangePasswordResponse>(resp, HttpStatus.OK);
	}

	@RequestMapping(value = "/ForgotPassword/AgentRequest", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ForgetPasswordUserResponse> forgotPassword(@RequestBody ForgetPasswordUserRequest dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request) {

		ForgetPasswordUserResponse result = new ForgetPasswordUserResponse();
		JCaptchaRequest cap = new JCaptchaRequest();
		cap.setSessionId(request.getSession().getId());
		cap.setCaptchaResponse(dto.getCaptchaResponse());
		// JCaptchaResponse jCaptchaResponse =
		// verificationApi.isValidJCaptcha(cap);
		if (true) {
			result = agentApi.forgetPasswordUserRequest(dto, Role.AGENT);
			;
		} else {
			result.setCode("F00");
			result.setMessage("Not a valid captcha");
		}
		return new ResponseEntity<ForgetPasswordUserResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/ForgotPassword/AgentChangePassword", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ChangePasswordWithOtpResponse> changePasswordWithOTP(
			@RequestHeader(value = "hash", required = false) String hash,
			@RequestBody ChangePasswordWithOtpRequest dto) {
		ChangePasswordWithOtpResponse result = new ChangePasswordWithOtpResponse();
		result = agentApi.changePasswordWithOtpRequest(dto, Role.AGENT);
		return new ResponseEntity<ChangePasswordWithOtpResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/AgentReceipts", method = RequestMethod.GET)
	public String getAgentReceipts(@ModelAttribute("editUser") ReceiptsRequest dto, ModelMap modelMap,
			HttpSession session) throws JSONException {
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
			if (authority != null) {
				if (authority.contains(Authorities.AGENT) && authority.contains(Authorities.AUTHENTICATED)) {
					ArrayList<TransactionDTO> transactionList = new ArrayList<>();
					dto.setSessionId(sessionId);
					dto.setSize(1000);
					dto.setPage(0);
					ReceiptsResponse resp = agentApi.getReceipts(dto, Role.AGENT);
					JSONArray receiptsArray = (JSONArray) resp.getAdditionalInfo();
					for (int i = 0; i < receiptsArray.length(); i++) {
						JSONObject obj = receiptsArray.getJSONObject(i);
						TransactionDTO transaction = new TransactionDTO();
						transaction.setAmount(JSONParserUtil.getDouble(obj, "amount"));
						long timeinMillis = (long) JSONParserUtil.getLong(obj, "created");
						Date dateTime = new Date(timeinMillis);
						transaction.setDate("" + sdf.format(dateTime));
						transaction.setTransactionRefNo(JSONParserUtil.getString(obj, "transactionRefNo"));
						transaction.setDescription(JSONParserUtil.getString(obj, "description"));
						transaction.setStatus(JSONParserUtil.getString(obj, "status"));
						transaction.setDebit(JSONParserUtil.getBoolean(obj, "debit"));
						transactionList.add(transaction);
					}
					modelMap.put("transactions", transactionList);
					return "Agent/Login/Receipts";
				}
			}
		}
		return "redirect:/";
	}

	private boolean isValidImage(MultipartFile file) {
		long length = 2 * 1024 * 1024;
		File imageFile = null;
		try {
			imageFile = convert(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
		boolean isValid = false;
		if (file.getContentType().contains("image")) {
			isValid = true;
		}
		if (file.getSize() <= length) {
			try {
				Image image = ImageIO.read(imageFile);
				if (image == null) {
				} else {
					isValid = true;
				}
			} catch (IOException ex) {
			}
		}

		return isValid;
	}

	private File convert(MultipartFile file) throws IOException {
		File convFile = new File(file.getOriginalFilename());
		convFile.createNewFile();
		FileOutputStream fos = new FileOutputStream(convFile);
		fos.write(file.getBytes());
		fos.close();
		return convFile;
	}

	private String saveImage(String rootDirectory, MultipartFile image) {
		boolean isSaved = false;
		String filePath = "";
		String seperator = "|";
		String[] format = image.getContentType().split("/");
		String fileFormat = "";
		String[] formats = { "jpeg", "png", "jpg" };
		for (String fmt : formats) {
			if (format[1].equals(fmt)) {
				fileFormat = fmt;
			}
		}
		if (!CommonValidation.isNull(fileFormat)) {
			try {
				String fileName = String.valueOf(System.currentTimeMillis());
				File dirs = new File(rootDirectory + "/resources/profileImages/" + fileName + "." + fileFormat);
				dirs.mkdirs();
				image.transferTo(dirs);
				filePath = "/resources/profileImages/" + fileName + "." + fileFormat;
				isSaved = true;
				return isSaved + seperator + filePath;
			} catch (Exception ex) {
				return "Exception Occurred";
			}
		}
		return "Exception Occurred";
	}

}
