package com.payqwikweb.controller.web;

import java.awt.Image;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.ParserConfigurationException;

import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.xml.sax.SAXException;

import com.payqwikweb.app.model.request.SingleUserAccDetailsReq;
import com.payqwikweb.app.model.response.AddVoucherResponse;
import com.coupon.api.IPromoCodeApi;
import com.gci.api.IGciServiceApi;
import com.gci.model.request.OrderStatusDTO;
import com.gci.model.response.OrderStatusResponse;
import com.gcm.api.INotificationApi;
import com.gcm.model.CronNotificationDTO;
import com.gcm.model.NotificationDTO;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.payqwikweb.api.IAuthenticationApi;
import com.payqwikweb.app.api.IAdminApi;
import com.payqwikweb.app.api.ILoadMoneyApi;
import com.payqwikweb.app.api.ILogoutApi;
import com.payqwikweb.app.api.ISuperAdminApi;
import com.payqwikweb.app.api.ITravelBusApi;
import com.payqwikweb.app.api.IUserApi;
import com.payqwikweb.app.api.IVersionApi;
import com.payqwikweb.app.metadatas.UrlMetadatas;
import com.payqwikweb.app.model.AnalyticsResponse;
import com.payqwikweb.app.model.ResponseStatus;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.UserResponseDTO;
import com.payqwikweb.app.model.UserStatus;
import com.payqwikweb.app.model.VoucherDTO;
import com.payqwikweb.app.model.busdto.BusTicketDTO;
import com.payqwikweb.app.model.flight.dto.FlightCancelableResp;
import com.payqwikweb.app.model.flight.dto.GetFlightDetailsForAdmin;
import com.payqwikweb.app.model.flight.request.FligthcancelableReq;
import com.payqwikweb.app.model.flight.response.FlightResponseDTO;
import com.payqwikweb.app.model.flight.response.FlightTicketAdminDTO;
import com.payqwikweb.app.model.flight.response.FlightTicketResp;
import com.payqwikweb.app.model.request.AccountTypeRequest;
import com.payqwikweb.app.model.request.AddVoucherReq;
import com.payqwikweb.app.model.request.AdminUserDetails;
import com.payqwikweb.app.model.request.AllTransactionRequest;
import com.payqwikweb.app.model.request.AllUserRequest;
import com.payqwikweb.app.model.request.AnalyticsDemo;
import com.payqwikweb.app.model.request.BlockUnBlockUserRequest;
import com.payqwikweb.app.model.request.BlockUserRequest;
import com.payqwikweb.app.model.request.ChangePasswordRequest;
import com.payqwikweb.app.model.request.DRegistrationRequest;
import com.payqwikweb.app.model.request.DateDTO;
import com.payqwikweb.app.model.request.GCMError;
import com.payqwikweb.app.model.request.GCMRequest;
import com.payqwikweb.app.model.request.LoginRequest;
import com.payqwikweb.app.model.request.LogoutRequest;
import com.payqwikweb.app.model.request.MRegistrationRequest;
import com.payqwikweb.app.model.request.MessageLogRequest;
import com.payqwikweb.app.model.request.PagingDTO;
import com.payqwikweb.app.model.request.PartnerDetailsRequest;
import com.payqwikweb.app.model.request.PromoCodeError;
import com.payqwikweb.app.model.request.PromoCodeRequest;
import com.payqwikweb.app.model.request.ReceiptsRequest;
import com.payqwikweb.app.model.request.SendMoneyMobileDTO;
import com.payqwikweb.app.model.request.SessionDTO;
import com.payqwikweb.app.model.request.SingleUserRequest;
import com.payqwikweb.app.model.request.TransactionFilter;
import com.payqwikweb.app.model.request.TransactionRequest;
import com.payqwikweb.app.model.request.TreatCardPlanRequest;
import com.payqwikweb.app.model.request.TreatCardRegisterList;
import com.payqwikweb.app.model.request.UserAnalytics;
import com.payqwikweb.app.model.request.UserByLocationDTO;
import com.payqwikweb.app.model.request.UserResponse;
import com.payqwikweb.app.model.request.UserTransactionRequest;
import com.payqwikweb.app.model.request.VersionDTO;
import com.payqwikweb.app.model.request.bus.IsCancellableReq;
import com.payqwikweb.app.model.request.reports.MISDetailRequest;
import com.payqwikweb.app.model.response.AccountTypeResponse;
import com.payqwikweb.app.model.response.AddDonateeResponse;
import com.payqwikweb.app.model.response.AddMerchantResponse;
import com.payqwikweb.app.model.response.AjaxResponse;
import com.payqwikweb.app.model.response.AllTransactionResponse;
import com.payqwikweb.app.model.response.AllUserResponse;
import com.payqwikweb.app.model.response.BlockUnBlockUserResponse;
import com.payqwikweb.app.model.response.BlockUserResponse;
import com.payqwikweb.app.model.response.CommissionDTO;
import com.payqwikweb.app.model.response.EBSStatusResponseDTO;
import com.payqwikweb.app.model.response.GCMResponse;
import com.payqwikweb.app.model.response.HouseJoyAdminDTO;
import com.payqwikweb.app.model.response.LoginResponse;
import com.payqwikweb.app.model.response.LogoutResponse;
import com.payqwikweb.app.model.response.MTransactionResponseDTO;
import com.payqwikweb.app.model.response.MessageLogResponse;
import com.payqwikweb.app.model.response.NEFTResponse;
import com.payqwikweb.app.model.response.PromoCodeResponse;
import com.payqwikweb.app.model.response.ReceiptsResponse;
import com.payqwikweb.app.model.response.RegistrationResponse;
import com.payqwikweb.app.model.response.ServiceTypeResponse;
import com.payqwikweb.app.model.response.TListResponse;
import com.payqwikweb.app.model.response.TransactionListResponse;
import com.payqwikweb.app.model.response.TransactionReportResponse;
import com.payqwikweb.app.model.response.TreatCardPlansResponse;
import com.payqwikweb.app.model.response.TreatCardRegisterListResponse;
import com.payqwikweb.app.model.response.UserTransactionResponse;
import com.payqwikweb.app.model.response.VersionResponse;
import com.payqwikweb.app.model.response.bus.IsCancellableResp;
import com.payqwikweb.app.model.response.reports.MISDTO;
import com.payqwikweb.app.model.response.reports.MISResponce;
import com.payqwikweb.app.model.response.reports.WalletLoadUserDetails;
import com.payqwikweb.app.model.response.reports.WalletOutsReports;
import com.payqwikweb.app.model.response.reports.WalletResponse;
import com.payqwikweb.model.admin.TListDTO;
import com.payqwikweb.model.app.request.OffersRequest;
import com.payqwikweb.model.app.request.PartnerDetailsDTO;
import com.payqwikweb.model.app.response.AddOffersResponse;
import com.payqwikweb.model.app.response.TKOrderDetailsDTO;
import com.payqwikweb.model.app.response.UserDetail;
import com.payqwikweb.model.error.RegisterError;
import com.payqwikweb.model.request.thirdpartyService.RequestRefund;
import com.payqwikweb.model.web.GciAuthDTO;
import com.payqwikweb.model.web.GiftCardDetailResponse;
import com.payqwikweb.model.web.RefundStatusDTO;
import com.payqwikweb.model.web.Status;
import com.payqwikweb.model.web.TransactionReport;
import com.payqwikweb.model.web.UserListDTO;
import com.payqwikweb.util.APIUtils;
import com.payqwikweb.util.Authorities;
import com.payqwikweb.util.CommonUtil;
import com.payqwikweb.util.ConvertUtil;
import com.payqwikweb.util.JSONParserUtil;
import com.payqwikweb.util.LogCat;
import com.payqwikweb.util.ModelMapKey;
import com.payqwikweb.util.StartupUtil;
import com.payqwikweb.validation.CommonValidation;
import com.payqwikweb.validation.GCMValidation;
import com.payqwikweb.validation.PromoCodeValidation;
import com.payqwikweb.validation.RegisterValidation;
import com.thirdparty.model.ResponseDTO;

@Controller
@RequestMapping("/Admin")
public class AdminViewController implements MessageSourceAware {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private MessageSource messageSource;
	private final IAuthenticationApi authenticationApi;
	private final IAdminApi appAdminApi;
	private final ILogoutApi logoutApi;
	private final IUserApi userApi;
	private final RegisterValidation registerValidation;
	// private final IVerificationApi verificationApi;
	private final IVersionApi versionApi;
	private final PromoCodeValidation promoCodeValidation;
	private final INotificationApi notificationApi;
	private final GCMValidation gcmValidation;
	private final IPromoCodeApi promoCodeApi;
	private final ILoadMoneyApi loadMoneyApi;
	private final IGciServiceApi gciServiceApi;
	private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
	private SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
	private SimpleDateFormat dateFilter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	public static SimpleDateFormat dateFormate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private final ITravelBusApi travelBusApi;
	private final ISuperAdminApi superAdminApi;

	@ModelAttribute("GeneratePromoCode")
	public PromoCodeRequest code() {
		return new PromoCodeRequest();
	}

	public AdminViewController(IAuthenticationApi authenticationApi, IAdminApi appAdminApi, ILogoutApi logoutApi,
			IUserApi userApi, RegisterValidation registerValidation, IVersionApi versionApi,
			PromoCodeValidation promoCodeValidation, INotificationApi notificationApi, GCMValidation gcmValidation,
			IPromoCodeApi promoCodeApi, ILoadMoneyApi loadMoneyApi, IGciServiceApi gciServiceApi,
			ITravelBusApi travelBusApi,ISuperAdminApi superAdminApi) {
		super();
		this.authenticationApi = authenticationApi;
		this.appAdminApi = appAdminApi;
		this.logoutApi = logoutApi;
		this.userApi = userApi;
		this.registerValidation = registerValidation;
		this.versionApi = versionApi;
		this.promoCodeValidation = promoCodeValidation;
		this.notificationApi = notificationApi;
		this.gcmValidation = gcmValidation;
		this.promoCodeApi = promoCodeApi;
		this.loadMoneyApi = loadMoneyApi;
		this.gciServiceApi = gciServiceApi;
		this.travelBusApi = travelBusApi;
		this.superAdminApi=superAdminApi;
	}

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/Home")
	public String getUser(HttpServletRequest request, ModelMap model, HttpServletResponse response,
			HttpSession session) {

		try {
			String adminSessionId = (String) session.getAttribute("adminSessionId");
			if (adminSessionId != null) {
				SessionDTO dto = new SessionDTO();
				dto.setSessionId(adminSessionId);
				String authority = authenticationApi.getAuthorityFromSession(adminSessionId, Role.USER);
				if (authority != null) {
					if (authority.contains(Authorities.ADMINISTRATOR)
							&& authority.contains(Authorities.AUTHENTICATED)) {
						AllTransactionRequest a1 = new AllTransactionRequest();
						a1.setSessionId(adminSessionId);
						ResponseDTO map = new ResponseDTO();
						UserResponseDTO userCount = new UserResponseDTO();
						JsonArray jsonArray = new JsonArray();
						JsonArray userArray = new JsonArray();
						map = appAdminApi.findTotalTransactionByMonth(a1);
						try {
							Iterator it = map.getTransvalue().entrySet().iterator();
							for (int i = 0; i < map.getTransvalue().size(); i++) {
								while (it.hasNext()) {
									Map.Entry pair = (Map.Entry) it.next();
									JsonObject payload = new JsonObject();
									payload.addProperty("date", pair.getKey().toString());
									payload.addProperty("visits", pair.getValue().toString());
									jsonArray.add(payload);
								}
								model.put("key", jsonArray);
							}
							userCount = appAdminApi.findTotalUserByMonth(a1);
							Iterator itUser = userCount.getTransvalue().entrySet().iterator();
							for (int i = 0; i < userCount.getTransvalue().size(); i++) {
								while (itUser.hasNext()) {
									Map.Entry pair = (Map.Entry) itUser.next();
									JsonObject payload1 = new JsonObject();
									payload1.addProperty("date", pair.getKey().toString());
									payload1.addProperty("visits", pair.getValue().toString());
									userArray.add(payload1);
								}
								model.put("user", userArray);
							}
						} catch (NullPointerException e) {
							e.printStackTrace();
						}
						model.put("user", session.getAttribute("user"));
						model.put("key", session.getAttribute("key"));

						return "Admin/Home";
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "Admin/Login";

	}

	@RequestMapping(method = RequestMethod.POST, value = "/Home")
	public String getAdminHomePage(HttpServletRequest request, ModelMap modelMap, HttpServletResponse response,
			@ModelAttribute("login") LoginRequest loginDTO, RedirectAttributes modelmap, HttpSession session) {

		try {
			loginDTO.setUsername(loginDTO.getUsername().trim());
			loginDTO.setPassword(ConvertUtil.decodeBase64String(loginDTO.getPassword()));
			loginDTO.setIpAddress(request.getRemoteAddr());
			LoginResponse loginResponse = appAdminApi.login(loginDTO);
			String sessionId = loginResponse.getSessionId();
			session.setAttribute("adminSessionId", sessionId);
			if (loginResponse.getCode() != null) {
				if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(loginResponse.getCode())) {
					AllTransactionRequest a1 = new AllTransactionRequest();
					a1.setSessionId(sessionId);

					ResponseDTO map = new ResponseDTO();
					UserResponseDTO userCount = new UserResponseDTO();
					JsonArray jsonArray = new JsonArray();
					JsonArray userArray = new JsonArray();
					map = appAdminApi.findTotalTransactionByMonth(a1);
					Iterator it = map.getTransvalue().entrySet().iterator();
					for (int i = 0; i < map.getTransvalue().size(); i++) {
						while (it.hasNext()) {
							Map.Entry pair = (Map.Entry) it.next();
							JsonObject payload = new JsonObject();
							payload.addProperty("date", pair.getKey().toString());
							payload.addProperty("visits", pair.getValue().toString());
							jsonArray.add(payload);
						}
						modelMap.put("key", jsonArray);
						session.setAttribute("key", jsonArray);
					}
					userCount = appAdminApi.findTotalUserByMonth(a1);
					Iterator itUser = userCount.getTransvalue().entrySet().iterator();
					for (int i = 0; i < userCount.getTransvalue().size(); i++) {
						while (itUser.hasNext()) {
							Map.Entry pair = (Map.Entry) itUser.next();
							JsonObject payload1 = new JsonObject();
							payload1.addProperty("date", pair.getKey().toString());
							payload1.addProperty("visits", pair.getValue().toString());
							userArray.add(payload1);
						}
						modelMap.put("user", userArray);
						session.setAttribute("user", userArray);
					}
					return "Admin/Home";
				} else if (loginResponse.getCode().equals("F05")) {
					modelmap.addFlashAttribute(ModelMapKey.ERROR, loginResponse.getMessage());
					return "redirect:/Admin/Home";
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return "Admin/Login";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/HomeInAjax")
	public ResponseEntity<AjaxResponse> getTotalUsers(HttpServletRequest request, HttpServletResponse response,
			HttpSession session, Model model) throws JSONException {

		AjaxResponse entity = new AjaxResponse();
		SessionDTO dto = new SessionDTO();
		dto.setSessionId((String) session.getAttribute("adminSessionId"));
		UserTransactionResponse userTransactionResponse = appAdminApi.getUserTransactionValues(dto);
		if (userTransactionResponse != null) {
			if (userTransactionResponse.isSuccess()) {
				JSONObject json = new JSONObject(userTransactionResponse.getResponse());
				if (json != null) {
					JSONObject details = JSONParserUtil.getObject(json, "details");
					double pool = JSONParserUtil.getDouble(details, "walletBalance");
					double bankTransferCommission = JSONParserUtil.getDouble(details, "bankTransferCommission");
					double merchantPayble = JSONParserUtil.getDouble(details, "merchantPayable");
					double totalCommission = JSONParserUtil.getDouble(details, "totalCommission");
					double travelTransaction = JSONParserUtil.getDouble(details, "travelTransaction");
					double totalLoadMoneyEBS = JSONParserUtil.getDouble(details, "totalLoadMoneyEBS");
					double totalLoadMoneyVNet = JSONParserUtil.getDouble(details, "totalLoadMoneyVNet");
					double totalPayable = JSONParserUtil.getDouble(details, "totalPayable");
					double bankAmount = JSONParserUtil.getDouble(details, "bankAmount");
					double promoBalance = JSONParserUtil.getDouble(details, "promoBalance");
					double iplBalance = JSONParserUtil.getDouble(details, "iplBalance");
					double mvisaTransaction = JSONParserUtil.getDouble(details, "mvisaTransaction");
					double totalLoadMoneyUPI = JSONParserUtil.getDouble(details, "totalLoadMoneyUPI");

					entity.setTotalLoadMoneyEBS(
							BigDecimal.valueOf(totalLoadMoneyEBS).setScale(2, RoundingMode.HALF_UP).doubleValue());
					entity.setTotalLoadMoneyEBS(
							BigDecimal.valueOf(totalLoadMoneyEBS).setScale(2, RoundingMode.HALF_UP).doubleValue());
					entity.setTotalLoadMoneyVNet(
							BigDecimal.valueOf(totalLoadMoneyVNet).setScale(2, RoundingMode.HALF_UP).doubleValue());
					entity.setPool(BigDecimal.valueOf(pool).setScale(2, RoundingMode.HALF_UP).doubleValue());
					entity.setTotalPayable(
							BigDecimal.valueOf(totalPayable).setScale(2, RoundingMode.HALF_UP).doubleValue());
					entity.setMerchantPayable(
							BigDecimal.valueOf(merchantPayble).setScale(2, RoundingMode.HALF_UP).doubleValue());
					entity.setTotalBankTransferCommission(
							BigDecimal.valueOf(bankTransferCommission).setScale(2, RoundingMode.HALF_UP).doubleValue());
					entity.setTotalCommission(
							BigDecimal.valueOf(totalCommission).setScale(2, RoundingMode.HALF_UP).doubleValue());
					entity.setBankAmount(
							BigDecimal.valueOf(bankAmount).setScale(2, RoundingMode.HALF_UP).doubleValue());
					entity.setPromoBalance(
							BigDecimal.valueOf(promoBalance).setScale(2, RoundingMode.HALF_UP).doubleValue());
					entity.setIplBalance(
							BigDecimal.valueOf(iplBalance).setScale(2, RoundingMode.HALF_UP).doubleValue());
					entity.setMvisaTransaction(
							BigDecimal.valueOf(mvisaTransaction).setScale(2, RoundingMode.HALF_UP).doubleValue());
					entity.setTravelTransaction(
							BigDecimal.valueOf(travelTransaction).setScale(2, RoundingMode.HALF_UP).doubleValue());
					entity.setTotalTrans(JSONParserUtil.getLong(details, "totalTransaction"));
					entity.setTotalUser(JSONParserUtil.getLong(details, "totalUsers"));
					entity.setTotalLoadMoneyUPI(
							BigDecimal.valueOf(totalLoadMoneyUPI).setScale(2, RoundingMode.HALF_UP).doubleValue());
				}
			}
		}
		return new ResponseEntity<AjaxResponse>(entity, HttpStatus.OK);

	}

	@RequestMapping(method = RequestMethod.POST, value = "/UserList")
	ResponseEntity<UserListDTO> getUserListInJSON(@ModelAttribute PagingDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) {

		UserListDTO result = new UserListDTO();
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					AllUserRequest userRequest = new AllUserRequest();
					userRequest.setPage(dto.getPage());
					userRequest.setSize(dto.getSize());
					userRequest.setSessionId(sessionCheck);
					userRequest.setStatus(UserStatus.ALL);
					AllUserResponse userResponse = appAdminApi.getAllUser(userRequest);
					JSONArray data = userResponse.getJsonArray();
					List<AdminUserDetails> userList = new ArrayList<>();
					try {
						if (data != null) {
							for (int i = 0; i < data.length(); i++) {
								AdminUserDetails list = new AdminUserDetails();
								JSONObject jsonobject = data.getJSONObject(i);
								String userType = JSONParserUtil.getString(jsonobject, "userType");
								// String
								// pin=jsonobject.getJSONObject("userDetail").getJSONObject("location").getString("pinCode");
								// String
								// circle=jsonobject.getJSONObject("userDetail").getJSONObject("location").getString("circleName");
								JSONObject userDetails = JSONParserUtil.getObject(jsonobject, "userDetail");
								if (userType.equalsIgnoreCase("User")) {
									JSONObject obj = (JSONObject) jsonobject.getJSONObject("userDetail");
									if (obj.isNull("location")) {

										list.setAuthority(jsonobject.getString("authority"));
										list.setFirstName(
												jsonobject.getJSONObject("userDetail").getString("firstName"));
										list.setLastname(jsonobject.getJSONObject("userDetail").getString("lastName"));
										list.setUsername(jsonobject.getJSONObject("userDetail").getString("name"));
										list.setAccountNumber(
												jsonobject.getJSONObject("accountDetail").getString("accountNumber"));
										list.setGender(jsonobject.getJSONObject("userDetail").getString("gender"));
										list.setDob(jsonobject.getJSONObject("userDetail").getString("dateOfBirth"));
										list.setVijayaAccountNumber(jsonobject.getJSONObject("accountDetail")
												.getString("vijayaAccountNumber"));
										list.setKycNonkyc(jsonobject.getJSONObject("accountDetail")
												.getJSONObject("accountType").getString("name"));
										list.setEmail(jsonobject.getJSONObject("userDetail").getString("email"));
										list.setBalance(jsonobject.getJSONObject("accountDetail").getDouble("balance"));
										list.setContactNo(
												jsonobject.getJSONObject("userDetail").getString("contactNo"));
										list.setEmail(jsonobject.getJSONObject("userDetail").getString("email"));
										list.setMobileToken(jsonobject.getString("mobileToken"));
										list.setPoints(jsonobject.getJSONObject("accountDetail").getLong("points"));
										DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");
										long milliSeconds = Long.parseLong(jsonobject.getString("created"));
										Calendar calendar = Calendar.getInstance();
										calendar.setTimeInMillis(milliSeconds);
										list.setDateOfAccountCreation(formatter.format(calendar.getTime()));
										// LogCat.print("User name " +
										// list.getFirstName());
										userList.add(list);
										model.addAttribute("userlist", userList);
									}

									else {
										list.setPinCode(jsonobject.getJSONObject("userDetail").getJSONObject("location")
												.getString("pinCode"));
										list.setCircleName(jsonobject.getJSONObject("userDetail")
												.getJSONObject("location").getString("circleName"));
										list.setAuthority(jsonobject.getString("authority"));
										list.setFirstName(
												jsonobject.getJSONObject("userDetail").getString("firstName"));
										list.setLastname(jsonobject.getJSONObject("userDetail").getString("lastName"));
										list.setUsername(jsonobject.getJSONObject("userDetail").getString("name"));

										list.setAccountNumber(
												jsonobject.getJSONObject("accountDetail").getString("accountNumber"));
										list.setGender(jsonobject.getJSONObject("userDetail").getString("gender"));
										list.setDob(jsonobject.getJSONObject("userDetail").getString("dateOfBirth"));
										list.setVijayaAccountNumber(jsonobject.getJSONObject("accountDetail")
												.getString("vijayaAccountNumber"));
										list.setKycNonkyc(jsonobject.getJSONObject("accountDetail")
												.getJSONObject("accountType").getString("name"));
										list.setEmail(jsonobject.getJSONObject("userDetail").getString("email"));
										list.setBalance(jsonobject.getJSONObject("accountDetail").getDouble("balance"));
										list.setContactNo(
												jsonobject.getJSONObject("userDetail").getString("contactNo"));
										list.setEmail(jsonobject.getJSONObject("userDetail").getString("email"));
										list.setMobileToken(jsonobject.getString("mobileToken"));
										list.setPoints(jsonobject.getJSONObject("accountDetail").getLong("points"));
										DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");
										long milliSeconds = Long.parseLong(jsonobject.getString("created"));
										Calendar calendar = Calendar.getInstance();
										calendar.setTimeInMillis(milliSeconds);
										list.setDateOfAccountCreation(formatter.format(calendar.getTime()));
										userList.add(list);
										model.addAttribute("userlist", userList);
									}
								}
							}
							result.setSuccess(true);
							result.setFirstPage(userResponse.isFirstPage());
							result.setLastPage(userResponse.isLastPage());
							result.setJsonArray(userList);
							result.setNumberOfElements(userResponse.getNumberOfElements());
							result.setSize(userResponse.getSize());
							result.setTotalPages(userResponse.getTotalPages());
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
					return new ResponseEntity<UserListDTO>(result, HttpStatus.OK);
				}
			}
		}
		return new ResponseEntity<UserListDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/VerifiedUsers")
	ResponseEntity<UserListDTO> getVerifiedUserListInJSON(@ModelAttribute PagingDTO dto, HttpServletRequest request,
			HttpSession session) {

		UserListDTO result = new UserListDTO();
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					AllUserRequest userRequest = new AllUserRequest();
					userRequest.setPage(0);
					userRequest.setSize(100000);
					userRequest.setSessionId(sessionCheck);
					userRequest.setStatus(UserStatus.ALL);
					AllUserResponse userResponse = appAdminApi.getAllUser(userRequest);
					JSONArray data = userResponse.getJsonArray();
					List<AdminUserDetails> userList = new ArrayList<>();
					try {
						if (data != null) {
							for (int i = 0; i < data.length(); i++) {
								AdminUserDetails list = new AdminUserDetails();
								JSONObject jsonobject = data.getJSONObject(i);
								String userType = JSONParserUtil.getString(jsonobject, "userType");
								if (userType.equalsIgnoreCase("User")
										&& (jsonobject.getJSONObject("userDetail").getString("contactNo"))
												.equalsIgnoreCase(request.getParameter("search"))) {
									list.setAuthority(jsonobject.getString("authority"));
									list.setFirstName(jsonobject.getJSONObject("userDetail").getString("firstName"));
									list.setLastname(jsonobject.getJSONObject("userDetail").getString("lastName"));
									list.setUsername(jsonobject.getJSONObject("userDetail").getString("name"));
									list.setAccountNumber(
											jsonobject.getJSONObject("accountDetail").getString("accountNumber"));
									list.setGender(jsonobject.getJSONObject("userDetail").getString("gender"));
									list.setDob(jsonobject.getJSONObject("userDetail").getString("dateOfBirth"));
									list.setKycNonkyc(jsonobject.getJSONObject("accountDetail")
											.getJSONObject("accountType").getString("name"));
									list.setEmail(jsonobject.getJSONObject("userDetail").getString("email"));
									list.setBalance(jsonobject.getJSONObject("accountDetail").getDouble("balance"));
									list.setContactNo(jsonobject.getJSONObject("userDetail").getString("contactNo"));
									list.setEmail(jsonobject.getJSONObject("userDetail").getString("email"));
									list.setMobileToken(jsonobject.getString("mobileToken"));
									list.setPoints(jsonobject.getJSONObject("accountDetail").getLong("points"));
									DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");
									long milliSeconds = Long.parseLong(jsonobject.getString("created"));
									Calendar calendar = Calendar.getInstance();
									calendar.setTimeInMillis(milliSeconds);
									list.setDateOfAccountCreation(formatter.format(calendar.getTime()));
									userList.add(list);
								}
							}
							result.setSuccess(true);
							result.setFirstPage(userResponse.isFirstPage());
							result.setLastPage(userResponse.isLastPage());
							result.setJsonArray(userList);
							result.setNumberOfElements(userResponse.getNumberOfElements());
							result.setSize(userResponse.getSize());
							result.setTotalPages(userResponse.getTotalPages());
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
					return new ResponseEntity<UserListDTO>(result, HttpStatus.OK);
				}
			}
		}
		return new ResponseEntity<UserListDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/UserListOnSearch")
	public String searchedUsersListPost(@RequestParam String search, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, SessionDTO dto, ModelMap model) throws JSONException {

		try {
			search = ConvertUtil.decodeBase64String(search);
		} catch (IOException e) {
			e.printStackTrace();
		}

		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String adminAuthority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (adminAuthority != null) {
				if (adminAuthority.contains(Authorities.ADMINISTRATOR)
						&& adminAuthority.contains(Authorities.AUTHENTICATED)) {
					SingleUserRequest userRequest = new SingleUserRequest();
					userRequest.setSessionId(sessionCheck);
					userRequest.setUsername(search);
					AllUserResponse userResponse = appAdminApi.getUserFromMobileNo(userRequest);
					if (userResponse != null) {
						JSONObject data = userResponse.getJsonObject();
						String userType = data.getString("userType");
						if (userType.equalsIgnoreCase("User") && (data.getString("mobile")).equalsIgnoreCase(search)) {
							userRequest.setFirstName(data.getString("username"));
							userRequest.setMobile(data.getString("mobile"));
							userRequest.setEmail(data.getString("email"));
							userRequest.setPoints(Long.parseLong(data.getString("points")));
							userRequest.setBalance(Double.parseDouble(data.getString("balance")));
							userRequest.setCreated(data.getString("created"));
							userRequest.setGender(data.getString("gender"));
							userRequest.setDob(data.getString("dob"));
							userRequest.setAuthority(data.getString("authority"));
							userRequest.setAccountType(data.getString("accountType"));
							userRequest.setPinCode(data.getString("pinCode"));
							userRequest.setCircleName(data.getString("circleName"));
							userRequest.setVijayaBankAccount(data.getString("vijayaBankAccount"));

						} else {
							userRequest.setMsg(data.getString("msg"));
							model.addAttribute(ModelMapKey.MESSAGE, userRequest.getMsg());
						}
					}
					model.addAttribute("userRequest", userRequest);
					return "Admin/SearchUser";
				}
			}
		}

		return "redirect:/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/UserListOnSearch")
	public String searchedUsersList(@RequestParam String search, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, SessionDTO dto, ModelMap model) throws JSONException {
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String adminAuthority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (adminAuthority != null) {
				if (adminAuthority.contains(Authorities.ADMINISTRATOR)
						&& adminAuthority.contains(Authorities.AUTHENTICATED)) {
					SingleUserRequest userRequest = new SingleUserRequest();
					userRequest.setSessionId(sessionCheck);
					userRequest.setUsername(search);
					AllUserResponse userResponse = appAdminApi.getUserFromMobileNo(userRequest);
					if (userResponse != null) {
						JSONObject data = userResponse.getJsonObject();
						String userType = data.getString("userType");
						if (userType.equalsIgnoreCase("User") && (data.getString("mobile")).equalsIgnoreCase(search)) {
							userRequest.setFirstName(data.getString("username"));
							userRequest.setMobile(data.getString("mobile"));
							userRequest.setEmail(data.getString("email"));
							userRequest.setPoints(Long.parseLong(data.getString("points")));
							userRequest.setBalance(Double.parseDouble(data.getString("balance")));
							userRequest.setCreated(data.getString("created"));
							userRequest.setGender(data.getString("gender"));
							userRequest.setDob(data.getString("dob"));
							userRequest.setAuthority(data.getString("authority"));
							userRequest.setAccountType(data.getString("accountType"));
							userRequest.setPinCode(data.getString("pinCode"));
							userRequest.setCircleName(data.getString("circleName"));
							userRequest.setVijayaBankAccount(data.getString("vijayaBankAccount"));

						} else {
							userRequest.setMsg(data.getString("msg"));
							model.addAttribute(ModelMapKey.MESSAGE, userRequest.getMsg());
						}
					}
					model.addAttribute("userRequest", userRequest);
					return "Admin/SearchUser";
				}
			}
		}

		return "redirect:/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/UserList")
	public String getUserList(HttpServletRequest request, HttpSession session) {
		String mobile = request.getParameter("search");

		UserListDTO result = new UserListDTO();
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					AllUserRequest userRequest = new AllUserRequest();
					userRequest.setPage(0);
					userRequest.setSize(10);
					userRequest.setSessionId(sessionCheck);
					userRequest.setStatus(UserStatus.ALL);
					AllUserResponse userResponse = appAdminApi.getAllUser(userRequest);
					JSONArray data = userResponse.getJsonArray();
					List<AdminUserDetails> userList = new ArrayList<>();
					result.setSuccess(true);
					result.setFirstPage(userResponse.isFirstPage());
					result.setLastPage(userResponse.isLastPage());
					result.setJsonArray(userList);
					result.setNumberOfElements(userResponse.getNumberOfElements());
					result.setSize(userResponse.getSize());
					result.setTotalPages(userResponse.getTotalPages());
				}
				return "Admin/UserList";
			}
		}

		return "redirect:/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/BlockedUsers")
	ResponseEntity<UserListDTO> getBlockedUsersInJSON(@ModelAttribute PagingDTO dto, HttpSession session) {
		UserListDTO result = new UserListDTO();
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					AllUserRequest userRequest = new AllUserRequest();
					userRequest.setPage(dto.getPage());
					userRequest.setSize(dto.getSize());
					userRequest.setSessionId((String) session.getAttribute("adminSessionId"));
					userRequest.setStatus(UserStatus.BLOCKED);
					AllUserResponse userResponse = appAdminApi.getAllUser(userRequest);
					JSONArray data = userResponse.getJsonArray();
					List<AdminUserDetails> userList = new ArrayList<>();
					try {
						if (data != null) {
							for (int i = 0; i < data.length(); i++) {
								AdminUserDetails list = new AdminUserDetails();
								JSONObject jsonobject = data.getJSONObject(i);
								String userType = JSONParserUtil.getString(jsonobject, "userType");
								if (userType.equalsIgnoreCase("User")) {
									list.setFirstName(jsonobject.getJSONObject("userDetail").getString("firstName"));
									list.setLastname(jsonobject.getJSONObject("userDetail").getString("lastName"));
									list.setUsername(jsonobject.getJSONObject("userDetail").getString("name"));
									list.setAccountNumber(
											jsonobject.getJSONObject("accountDetail").getString("accountNumber"));
									list.setBalance(jsonobject.getJSONObject("accountDetail").getDouble("balance"));
									list.setPoints(jsonobject.getJSONObject("accountDetail").getLong("points"));
									list.setContactNo(jsonobject.getJSONObject("userDetail").getString("contactNo"));
									list.setEmail(jsonobject.getJSONObject("userDetail").getString("email"));
									list.setMobileToken(jsonobject.getString("mobileToken"));
									userList.add(list);
								}
							}
							result.setSuccess(true);
							result.setFirstPage(userResponse.isFirstPage());
							result.setLastPage(userResponse.isLastPage());
							result.setJsonArray(userList);
							result.setNumberOfElements(userResponse.getNumberOfElements());
							result.setSize(userResponse.getSize());
							result.setTotalPages(userResponse.getTotalPages());
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
					return new ResponseEntity<UserListDTO>(result, HttpStatus.OK);
				}
			}
		}
		return new ResponseEntity<UserListDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/BlockedUsers")
	public String getUserVerified(HttpSession session) {
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					AllUserRequest userRequest = new AllUserRequest();
					userRequest.setPage(0);
					userRequest.setSize(10);
					userRequest.setSessionId((String) session.getAttribute("adminSessionId"));
					userRequest.setStatus(UserStatus.BLOCKED);
					AllUserResponse userResponse = appAdminApi.getAllUser(userRequest);
					JSONArray data = userResponse.getJsonArray();
					List<AdminUserDetails> userList = new ArrayList<>();
					// try {
					// if (data != null) {
					// for (int i = 0; i < data.length(); i++) {
					// AdminUserDetails list = new AdminUserDetails();
					// JSONObject jsonobject = data.getJSONObject(i);
					// String userType = JSONParserUtil.getString(jsonobject,
					// "userType");
					// if (userType.equalsIgnoreCase("User")) {
					// list.setFirstName(jsonobject.getJSONObject("userDetail").getString("firstName"));
					// LogCat.print(list.getFirstName());
					// list.setLastname(jsonobject.getJSONObject("userDetail").getString("lastName"));
					// LogCat.print(list.getLastname());
					// list.setUsername(jsonobject.getJSONObject("userDetail").getString("name"));
					// LogCat.print(list.getUsername());
					// list.setAccountNumber(jsonobject.getJSONObject("accountDetail").getString("accountNumber"));
					// list.setBalance(jsonobject.getJSONObject("accountDetail").getDouble("balance"));
					// list.setPoints(jsonobject.getJSONObject("accountDetail").getLong("points"));
					// list.setContactNo(jsonobject.getJSONObject("userDetail").getString("contactNo"));
					// list.setEmail(jsonobject.getJSONObject("userDetail").getString("email"));
					// list.setMobileToken(jsonobject.getString("mobileToken"));
					// LogCat.print("User name " + list.getFirstName());
					// userList.add(list);
					// }
					// }
					// model.put("userlist", userList);
					// }
					// } catch (JSONException e) {
					// e.printStackTrace();
					// }
					return "Admin/BlockedUsers";
				}
			}
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/LockedUsers")
	ResponseEntity<UserListDTO> getUserLockedInJSON(@ModelAttribute PagingDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) {

		UserListDTO result = new UserListDTO();
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String adminAuthority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (adminAuthority != null) {
				if (adminAuthority.contains(Authorities.ADMINISTRATOR)
						&& adminAuthority.contains(Authorities.AUTHENTICATED)) {
					AllUserRequest userRequest = new AllUserRequest();
					userRequest.setPage(dto.getPage());
					userRequest.setSize(dto.getSize());
					userRequest.setSessionId(sessionCheck);
					userRequest.setStatus(UserStatus.ALL);
					AllUserResponse userResponse = appAdminApi.getAllUser(userRequest);
					JSONArray data = userResponse.getJsonArray();
					List<AdminUserDetails> userList = new ArrayList<>();
					try {
						if (data != null) {
							for (int i = 0; i < data.length(); i++) {
								AdminUserDetails list = new AdminUserDetails();
								JSONObject jsonobject = data.getJSONObject(i);
								String userType = JSONParserUtil.getString(jsonobject, "userType");
								String authority = JSONParserUtil.getString(jsonobject, "authority");
								if (userType.equalsIgnoreCase("User")) {
									if (authority.contains(Authorities.LOCKED)
											&& authority.contains(Authorities.USER)) {
										list.setFirstName(
												jsonobject.getJSONObject("userDetail").getString("firstName"));
										list.setLastname(jsonobject.getJSONObject("userDetail").getString("lastName"));
										list.setUsername(jsonobject.getJSONObject("userDetail").getString("name"));
										list.setAccountNumber(
												jsonobject.getJSONObject("accountDetail").getString("accountNumber"));
										list.setBalance(jsonobject.getJSONObject("accountDetail").getDouble("balance"));
										list.setPoints(jsonobject.getJSONObject("accountDetail").getLong("points"));
										list.setContactNo(
												jsonobject.getJSONObject("userDetail").getString("contactNo"));
										list.setEmail(jsonobject.getJSONObject("userDetail").getString("email"));
										list.setMobileToken(jsonobject.getString("mobileToken"));
										userList.add(list);
									}
								}
							}
							result.setJsonArray(userList);
							result.setTotalPages(userResponse.getTotalPages());
							result.setSize(userResponse.getSize());
							result.setSuccess(true);
							result.setNumberOfElements(userResponse.getNumberOfElements());
							result.setFirstPage(userResponse.isFirstPage());
							result.setLastPage(userResponse.isLastPage());
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
					return new ResponseEntity<UserListDTO>(result, HttpStatus.OK);
				}
			}
		}
		return new ResponseEntity<UserListDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/LockedUsers")
	public String getUserLocked(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) {

		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String adminAuthority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (adminAuthority != null) {
				if (adminAuthority.contains(Authorities.ADMINISTRATOR)
						&& adminAuthority.contains(Authorities.AUTHENTICATED)) {
					AllUserRequest userRequest = new AllUserRequest();
					userRequest.setPage(0);
					userRequest.setSize(10);
					userRequest.setSessionId(sessionCheck);
					userRequest.setStatus(UserStatus.LOCKED);
					AllUserResponse userResponse = appAdminApi.getAllUser(userRequest);
					JSONArray data = userResponse.getJsonArray();
					List<AdminUserDetails> userList = new ArrayList<>();
					try {
						if (data != null) {
							for (int i = 0; i < data.length(); i++) {
								AdminUserDetails list = new AdminUserDetails();
								JSONObject jsonobject = data.getJSONObject(i);
								String userType = JSONParserUtil.getString(jsonobject, "userType");
								String authority = JSONParserUtil.getString(jsonobject, "authority");
								if (userType.equalsIgnoreCase("User")) {
									if (authority.contains(Authorities.LOCKED)
											&& authority.contains(Authorities.USER)) {
										list.setFirstName(
												jsonobject.getJSONObject("userDetail").getString("firstName"));
										list.setLastname(jsonobject.getJSONObject("userDetail").getString("lastName"));
										list.setUsername(jsonobject.getJSONObject("userDetail").getString("name"));
										list.setAccountNumber(
												jsonobject.getJSONObject("accountDetail").getString("accountNumber"));
										list.setBalance(jsonobject.getJSONObject("accountDetail").getDouble("balance"));
										list.setPoints(jsonobject.getJSONObject("accountDetail").getLong("points"));
										list.setContactNo(
												jsonobject.getJSONObject("userDetail").getString("contactNo"));
										list.setEmail(jsonobject.getJSONObject("userDetail").getString("email"));
										list.setMobileToken(jsonobject.getString("mobileToken"));
										userList.add(list);
									}
								}
							}
							model.put("userlist", userList);
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
					return "Admin/LockedUsers";
				}
			}
		}
		return "redirect:/Admin/Home";
	}

	// @RequestMapping(method=RequestMethod.GET,
	// value="/Merchant/{merchantName}")
	// public String getTransactionsOfMerchant(@PathVariable String
	// email,HttpServletRequest request,HttpServletResponse response,HttpSession
	// session)
	// {
	//
	//
	//
	// }

	@RequestMapping(method = RequestMethod.GET, value = "/ActiveUsers")
	public String getActiveUsers(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) {

		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					AllUserRequest userRequest = new AllUserRequest();
					userRequest.setPage(0);
					userRequest.setSize(10);
					userRequest.setSessionId(sessionCheck);
					userRequest.setStatus(UserStatus.ONLINE);
					AllUserResponse userResponse = appAdminApi.getAllUser(userRequest);
					JSONArray data = userResponse.getJsonArray();
					List<AdminUserDetails> userList = new ArrayList<>();
					// try {
					// if(data != null) {
					// for (int i = 0; i < data.length(); i++) {
					// AdminUserDetails list = new AdminUserDetails();
					// JSONObject jsonobject = data.getJSONObject(i);
					// String userType = JSONParserUtil.getString(jsonobject,
					// "userType");
					// if (userType.equalsIgnoreCase("User")) {
					// list.setFirstName(jsonobject.getJSONObject("userDetail").getString("firstName"));
					// LogCat.print(list.getFirstName());
					// list.setLastname(jsonobject.getJSONObject("userDetail").getString("lastName"));
					// LogCat.print(list.getLastname());
					// list.setUsername(jsonobject.getJSONObject("userDetail").getString("name"));
					// LogCat.print(list.getUsername());
					// list.setAccountNumber(jsonobject.getJSONObject("accountDetail").getString("accountNumber"));
					//
					// list.setBalance(jsonobject.getJSONObject("accountDetail").getDouble("balance"));
					// list.setPoints(jsonobject.getJSONObject("accountDetail").getLong("points"));
					// list.setContactNo(jsonobject.getJSONObject("userDetail").getString("contactNo"));
					// list.setEmail(jsonobject.getJSONObject("userDetail").getString("email"));
					// list.setMobileToken(jsonobject.getString("mobileToken"));
					//
					// LogCat.print("User name " + list.getFirstName());
					// userList.add(list);
					// }
					// }
					// model.put("userlist", userList);
					// }
					// } catch (JSONException e) {
					// e.printStackTrace();
					// }
					return "Admin/ActiveUsers";
				}
			}
		}

		return "redirect:/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/ActiveUsers")
	ResponseEntity<UserListDTO> getActiveUserListInJSON(@ModelAttribute PagingDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) {

		UserListDTO result = new UserListDTO();
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					AllUserRequest userRequest = new AllUserRequest();
					userRequest.setPage(dto.getPage());
					userRequest.setSize(dto.getSize());
					userRequest.setSessionId(sessionCheck);
					userRequest.setStatus(UserStatus.ACTIVE);
					AllUserResponse userResponse = appAdminApi.getAllUser(userRequest);
					JSONArray data = userResponse.getJsonArray();
					List<AdminUserDetails> userList = new ArrayList<>();
					try {
						if (data != null) {
							for (int i = 0; i < data.length(); i++) {
								AdminUserDetails list = new AdminUserDetails();
								JSONObject jsonobject = data.getJSONObject(i);
								String userType = JSONParserUtil.getString(jsonobject, "userType");
								if (userType.equalsIgnoreCase("User")) {
									list.setFirstName(jsonobject.getJSONObject("userDetail").getString("firstName"));
									list.setLastname(jsonobject.getJSONObject("userDetail").getString("lastName"));
									list.setUsername(jsonobject.getJSONObject("userDetail").getString("name"));
									list.setAccountNumber(
											jsonobject.getJSONObject("accountDetail").getString("accountNumber"));
									list.setBalance(jsonobject.getJSONObject("accountDetail").getDouble("balance"));
									list.setPoints(jsonobject.getJSONObject("accountDetail").getLong("points"));
									list.setContactNo(jsonobject.getJSONObject("userDetail").getString("contactNo"));
									list.setEmail(jsonobject.getJSONObject("userDetail").getString("email"));
									list.setMobileToken(jsonobject.getString("mobileToken"));
									userList.add(list);
								}
							}
							result.setSuccess(true);
							result.setFirstPage(userResponse.isFirstPage());
							result.setLastPage(userResponse.isLastPage());
							result.setJsonArray(userList);
							result.setNumberOfElements(userResponse.getNumberOfElements());
							result.setSize(userResponse.getSize());
							result.setTotalPages(userResponse.getTotalPages());
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
					return new ResponseEntity<UserListDTO>(result, HttpStatus.OK);
				}
			}
		}
		return new ResponseEntity<UserListDTO>(result, HttpStatus.OK);
	}

	/*
	 * @RequestMapping(method = RequestMethod.GET, value = "/NEFTList") public
	 * String getNEFTList(HttpServletRequest request, HttpServletResponse
	 * response, HttpSession session, ModelMap model) {
	 * 
	 * String sessionCheck = (String) session.getAttribute("adminSessionId");
	 * 
	 * if (sessionCheck != null) { // String authority = //
	 * authenticationApi.getAuthorityFromSession(sessionCheck, // Role.USER); //
	 * if (authority != null) { // if
	 * (authority.contains(Authorities.ADMINISTRATOR) && //
	 * authority.contains(Authorities.AUTHENTICATED)) { // SessionDTO dto = new
	 * SessionDTO(); // dto.setSessionId(sessionCheck); // List<NEFTResponse>
	 * neftList = appAdminApi.getNEFTList(dto, false, // null, null); //
	 * model.addAttribute("neftList", neftList); return "Admin/NEFTList"; }
	 * return "redirect:/Admin/Home"; }
	 */

	@RequestMapping(method = RequestMethod.GET, value = "/MerchantNEFTList")
	public String getMerchantNEFTList(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) {

		String sessionCheck = (String) session.getAttribute("adminSessionId");

		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					SessionDTO dto = new SessionDTO();
					dto.setSessionId(sessionCheck);
					List<NEFTResponse> neftList = appAdminApi.getNEFTList(dto);
					model.addAttribute("neftList", neftList);
					return "Admin/MerchantNeft";
				}
			}
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/MerchantNEFTListInJSON")
	public String getMerchantNEFTRequestList(@ModelAttribute DateDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) {

		try {
			dto.setFromDate(ConvertUtil.decodeBase64String(dto.getFromDate()));
			dto.setToDate(ConvertUtil.decodeBase64String(dto.getToDate()));
		} catch (Exception e) {

		}

		String sessionCheck = (String) session.getAttribute("adminSessionId");

		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionCheck);
					List<NEFTResponse> neftList = appAdminApi.getMerchantNEFTListFiltered(dto, false);
					model.addAttribute("neftList", neftList);
					return "Admin/MerchantNeft";
				}
			}
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/AgentNEFTList")
	public String getAgentNEFTList(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) {

		String sessionCheck = (String) session.getAttribute("adminSessionId");

		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					SessionDTO dto = new SessionDTO();
					dto.setSessionId(sessionCheck);
					List<NEFTResponse> neftList = appAdminApi.getAgentNEFTList(dto);
					model.addAttribute("neftList", neftList);
					return "Admin/AgentNeft";
				}
			}
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/AgentNEFTListInJSON")
	public String getAgentNEFTRequestList(@ModelAttribute DateDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) {

		try {
			dto.setFromDate(ConvertUtil.decodeBase64String(dto.getFromDate()));
			dto.setToDate(ConvertUtil.decodeBase64String(dto.getToDate()));
		} catch (Exception e) {

		}

		String sessionCheck = (String) session.getAttribute("adminSessionId");

		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionCheck);
					List<NEFTResponse> neftList = appAdminApi.getAgentNEFTListFiltered(dto, false);
					model.addAttribute("neftList", neftList);
					return "Admin/AgentNeft";
				}
			}
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/neftListFiltered")
	public String getNeftListFiltered(@ModelAttribute TransactionFilter filter, HttpServletRequest request,
			HttpServletResponse response, Model model, HttpSession session) throws ParseException {

		try {
			filter.setFromDate(ConvertUtil.decodeBase64String(filter.getFromDate()));
			filter.setToDate(ConvertUtil.decodeBase64String(filter.getToDate()));
		} catch (Exception e) {

		}

		String sessionCheck = (String) session.getAttribute("adminSessionId");
		String toDateTime = filter.getEndDate() + " 23:59";
		String fromDateTime = filter.getStartDate() + " 00:00";
		Date to = dateFilter.parse(toDateTime);
		Date from = dateFilter.parse(fromDateTime);
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					SessionDTO dto = new SessionDTO();
					dto.setSessionId(sessionCheck);
					List<NEFTResponse> neftList = appAdminApi.getNEFTList(dto, true, from, to);
					model.addAttribute("neftList", neftList);
					return "Admin/NEFTList";
				}
			}
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/NEFTListInJSON")
	public String getNEFTRequestList(@ModelAttribute DateDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) {

		try {
			dto.setFromDate(ConvertUtil.decodeBase64String(dto.getFromDate()));
			dto.setToDate(ConvertUtil.decodeBase64String(dto.getToDate()));
		} catch (Exception e) {

		}

		String sessionCheck = (String) session.getAttribute("adminSessionId");

		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionCheck);
					List<NEFTResponse> neftList = appAdminApi.getNEFTListFiltered(dto, false);
					model.addAttribute("neftList", neftList);
					return "Admin/NeftListFiltered";
				}
			}
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/GetServiceTypes")
	ResponseEntity<ServiceTypeResponse> getServiceType(HttpServletRequest request, HttpServletResponse response,
			HttpSession session, ModelMap model) {

		String sessionCheck = (String) session.getAttribute("adminSessionId");
		ServiceTypeResponse serviceTypeResponse = null;
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					SessionDTO dto = new SessionDTO();
					dto.setSessionId(sessionCheck);
					serviceTypeResponse = appAdminApi.getServiceType();
					return new ResponseEntity<ServiceTypeResponse>(serviceTypeResponse, HttpStatus.OK);
				}
			}
		}
		return new ResponseEntity<ServiceTypeResponse>(serviceTypeResponse, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/VerifiedUsers")
	public String getVerifiedUsers(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) {
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					AllUserRequest userRequest = new AllUserRequest();
					userRequest.setPage(0);
					userRequest.setSize(10);
					userRequest.setSessionId((String) session.getAttribute("adminSessionId"));
					userRequest.setStatus(UserStatus.ACTIVE);
					AllUserResponse userResponse = appAdminApi.getAllUser(userRequest);
					JSONArray data = userResponse.getJsonArray();
					List<AdminUserDetails> userList = new ArrayList<>();
					// try {
					// if(data != null) {
					// for (int i = 0; i < data.length(); i++) {
					// AdminUserDetails list = new AdminUserDetails();
					// JSONObject jsonobject = data.getJSONObject(i);
					// String userType = JSONParserUtil.getString(jsonobject,
					// "userType");
					// if (userType.equalsIgnoreCase("User")) {
					// list.setFirstName(jsonobject.getJSONObject("userDetail").getString("firstName"));
					// list.setLastname(jsonobject.getJSONObject("userDetail").getString("lastName"));
					// list.setUsername(jsonobject.getJSONObject("userDetail").getString("name"));
					// list.setAccountNumber(jsonobject.getJSONObject("accountDetail").getString("accountNumber"));
					// list.setBalance(jsonobject.getJSONObject("accountDetail").getDouble("balance"));
					// list.setPoints(jsonobject.getJSONObject("accountDetail").getLong("points"));
					// list.setContactNo(jsonobject.getJSONObject("userDetail").getString("contactNo"));
					// list.setEmail(jsonobject.getJSONObject("userDetail").getString("email"));
					// list.setEmailStatus(jsonobject.getString("emailStatus"));
					// list.setMobileStatus(jsonobject.getString("mobileStatus"));
					// list.setMobileToken(jsonobject.getString("mobileToken"));
					// userList.add(list);
					// }
					// }
					//
					// model.put("userlist", userList);
					// }
					// } catch (JSONException e) {
					// e.printStackTrace();
					// }

					return "Admin/VerifiedUsers";
				}
			}
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/Users/VerifiedUsers", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AdminUserDetails> getVerifiedUsersInJSON(HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) {
		JSONArray array = new JSONArray();
		AdminUserDetails result = new AdminUserDetails();
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					AllUserRequest userRequest = new AllUserRequest();
					userRequest.setPage(0);
					userRequest.setSize(100000);
					userRequest.setSessionId(sessionCheck);
					userRequest.setStatus(UserStatus.ACTIVE);
					AllUserResponse userResponse = appAdminApi.getAllUser(userRequest);
					JSONArray data = userResponse.getJsonArray();
					try {
						for (int i = 0; i < data.length(); i++) {
							AdminUserDetails list = new AdminUserDetails();
							JSONObject jsonobject = data.getJSONObject(i);
							String userType = JSONParserUtil.getString(jsonobject, "userType");
							if (userType.equalsIgnoreCase("User")) {
								list.setFirstName(jsonobject.getJSONObject("userDetail").getString("firstName"));
								list.setBalance(jsonobject.getJSONObject("accountDetail").getDouble("balance"));
								list.setPoints(jsonobject.getJSONObject("accountDetail").getLong("points"));
								list.setContactNo(jsonobject.getJSONObject("userDetail").getString("contactNo"));
								list.setEmail(jsonobject.getJSONObject("userDetail").getString("email"));
								array.put(list);
							}
						}
						result.setJsonArray(array);
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
			}
		}
		return new ResponseEntity<AdminUserDetails>(result, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/UnverifiedUsers")
	public String getUnverifiedUsers(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) {

		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					AllUserRequest userRequest = new AllUserRequest();
					userRequest.setPage(0);
					userRequest.setSize(100);
					userRequest.setSessionId(sessionCheck);
					userRequest.setStatus(UserStatus.INACTIVE);
					AllUserResponse userResponse = appAdminApi.getAllUser(userRequest);
					JSONArray data = userResponse.getJsonArray();
					List<AdminUserDetails> userList = new ArrayList<>();
					// try {
					// if(data != null) {
					// for (int i = 0; i < data.length(); i++) {
					// AdminUserDetails list = new AdminUserDetails();
					// JSONObject jsonobject = data.getJSONObject(i);
					// String userType = JSONParserUtil.getString(jsonobject,
					// "userType");
					// if (userType.equalsIgnoreCase("User")) {
					// list.setFirstName(jsonobject.getJSONObject("userDetail").getString("firstName"));
					// LogCat.print(list.getFirstName());
					// list.setLastname(jsonobject.getJSONObject("userDetail").getString("lastName"));
					// LogCat.print(list.getLastname());
					// list.setUsername(jsonobject.getJSONObject("userDetail").getString("name"));
					// LogCat.print(list.getUsername());
					// list.setAccountNumber(jsonobject.getJSONObject("accountDetail").getString("accountNumber"));
					// list.setBalance(jsonobject.getJSONObject("accountDetail").getDouble("balance"));
					// list.setPoints(jsonobject.getJSONObject("accountDetail").getLong("points"));
					// list.setContactNo(jsonobject.getJSONObject("userDetail").getString("contactNo"));
					// list.setEmail(jsonobject.getJSONObject("userDetail").getString("email"));
					// list.setMobileToken(jsonobject.getString("mobileToken"));
					// list.setEmailStatus(jsonobject.getString("emailStatus"));
					// list.setMobileStatus(jsonobject.getString("mobileStatus"));
					// LogCat.print("User name " + list.getFirstName());
					// userList.add(list);
					// }
					// }
					// model.put("userlist", userList);
					// }
					// } catch (JSONException e) {
					// e.printStackTrace();
					// }
					return "Admin/UnverifiedUsers";
				}
			}
		}

		return "redirect:/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/KYCusers")
	public String getKycUsers(HttpServletRequest request, HttpServletResponse response, Model model,
			HttpSession session) {
		UserListDTO result = new UserListDTO();
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					AllUserRequest userRequest = new AllUserRequest();
					userRequest.setPage(0);
					userRequest.setSize(100000);
					userRequest.setSessionId(sessionCheck);
					userRequest.setStatus(UserStatus.ALL);
					AllUserResponse userResponse = appAdminApi.getAllUser(userRequest);
					JSONArray data = userResponse.getJsonArray();
					List<AdminUserDetails> userList = new ArrayList<>();
					try {
						if (data != null) {
							for (int i = 0; i < data.length(); i++) {
								AdminUserDetails list = new AdminUserDetails();
								JSONObject jsonobject = data.getJSONObject(i);
								String userType = JSONParserUtil.getString(jsonobject, "userType");
								String kycnonkyc = jsonobject.getJSONObject("accountDetail")
										.getJSONObject("accountType").getString("name");
								// System.out.println(kycnonkyc +"this is a demo
								// message");

								if (userType.equalsIgnoreCase("User") && kycnonkyc.equalsIgnoreCase("KYC")) {
									list.setAuthority(jsonobject.getString("authority"));
									list.setFirstName(jsonobject.getJSONObject("userDetail").getString("firstName"));
									list.setLastname(jsonobject.getJSONObject("userDetail").getString("lastName"));
									list.setUsername(jsonobject.getJSONObject("userDetail").getString("name"));
									list.setAccountNumber(
											jsonobject.getJSONObject("accountDetail").getString("accountNumber"));
									list.setGender(jsonobject.getJSONObject("userDetail").getString("gender"));
									list.setDob(jsonobject.getJSONObject("userDetail").getString("dateOfBirth"));
									list.setKycNonkyc(jsonobject.getJSONObject("accountDetail")
											.getJSONObject("accountType").getString("name"));
									list.setEmail(jsonobject.getJSONObject("userDetail").getString("email"));
									list.setBalance(jsonobject.getJSONObject("accountDetail").getDouble("balance"));
									list.setContactNo(jsonobject.getJSONObject("userDetail").getString("contactNo"));
									list.setEmail(jsonobject.getJSONObject("userDetail").getString("email"));
									list.setMobileToken(jsonobject.getString("mobileToken"));
									list.setPoints(jsonobject.getJSONObject("accountDetail").getLong("points"));
									DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");
									long milliSeconds = Long.parseLong(jsonobject.getString("created"));
									Calendar calendar = Calendar.getInstance();
									calendar.setTimeInMillis(milliSeconds);
									list.setDateOfAccountCreation(formatter.format(calendar.getTime()));
									// LogCat.print("User name " +
									// list.getFirstName());
									userList.add(list);
								}
							}
							model.addAttribute("KYCuserlist", userList);
							return "Admin/KYCusers";
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}

				}
			}
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/UnverifiedUsers")
	ResponseEntity<UserListDTO> getUnverifiedUserListInJSON(@ModelAttribute PagingDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) {

		UserListDTO result = new UserListDTO();
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					AllUserRequest userRequest = new AllUserRequest();
					userRequest.setPage(dto.getPage());
					userRequest.setSize(dto.getSize());
					userRequest.setSessionId(sessionCheck);
					userRequest.setStatus(UserStatus.INACTIVE);
					AllUserResponse userResponse = appAdminApi.getAllUser(userRequest);
					JSONArray data = userResponse.getJsonArray();
					List<AdminUserDetails> userList = new ArrayList<>();
					try {
						if (data != null) {
							for (int i = 0; i < data.length(); i++) {
								AdminUserDetails list = new AdminUserDetails();
								JSONObject jsonobject = data.getJSONObject(i);
								String userType = JSONParserUtil.getString(jsonobject, "userType");
								if (userType.equalsIgnoreCase("User")) {
									list.setFirstName(jsonobject.getJSONObject("userDetail").getString("firstName"));
									list.setLastname(jsonobject.getJSONObject("userDetail").getString("lastName"));
									list.setUsername(jsonobject.getJSONObject("userDetail").getString("name"));
									list.setAccountNumber(
											jsonobject.getJSONObject("accountDetail").getString("accountNumber"));
									list.setBalance(jsonobject.getJSONObject("accountDetail").getDouble("balance"));
									list.setPoints(jsonobject.getJSONObject("accountDetail").getLong("points"));
									list.setContactNo(jsonobject.getJSONObject("userDetail").getString("contactNo"));
									list.setEmail(jsonobject.getJSONObject("userDetail").getString("email"));
									list.setMobileToken(jsonobject.getString("mobileToken"));
									list.setEmailStatus(jsonobject.getString("emailStatus"));
									list.setMobileStatus(jsonobject.getString("mobileStatus"));
									userList.add(list);
								}
							}
							result.setSuccess(true);
							result.setFirstPage(userResponse.isFirstPage());
							result.setLastPage(userResponse.isLastPage());
							result.setJsonArray(userList);
							result.setNumberOfElements(userResponse.getNumberOfElements());
							result.setSize(userResponse.getSize());
							result.setTotalPages(userResponse.getTotalPages());
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
					return new ResponseEntity<UserListDTO>(result, HttpStatus.OK);
				}
			}
		}
		return new ResponseEntity<UserListDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/TransactionReport")
	public String getTransactionReports(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) throws JSONException {
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			return "Admin/DailyReport";
		}
		return "redirect:/Admin/Home";
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/TransactionReports")
	ResponseEntity<UserListDTO> getTransactionListByPagination(@ModelAttribute PagingDTO dto, HttpSession session) {
		UserListDTO result = new UserListDTO();
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					AllUserRequest userRequest = new AllUserRequest();
					userRequest.setPage(dto.getPage());
					userRequest.setSize(dto.getSize());
					String startDate=dateFormate.format(CommonUtil.getPreviousMonthDate());
					String endDate=	dateFormate.format(CommonUtil.getTodayMonthDate());
					userRequest.setStartDate(startDate);
					userRequest.setEndDate(endDate);
					userRequest.setSessionId(sessionCheck);
					userRequest.setPageable(true);
					TransactionReportResponse listResponse = appAdminApi.getTransactionReportByPage(userRequest);
					result.setSuccess(true);
					result.setJsonArray(listResponse.getList());
					result.setTotalPages(listResponse.getAmount());
					result.setSize(userRequest.getSize());
					return new ResponseEntity<UserListDTO>(result, HttpStatus.OK);
				}
			}
		}
		return new ResponseEntity<UserListDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/TransactionReportInJSON")
	public String getTransactionList(@ModelAttribute DateDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) {

		try {
			dto.setFromDate(ConvertUtil.decodeBase64String(dto.getFromDate()));
			dto.setToDate(ConvertUtil.decodeBase64String(dto.getToDate()));
		} catch (Exception e) {

		}

		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {

			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionCheck);
					TransactionReportResponse listResponse = appAdminApi.getTransactionReport(dto);
					if (listResponse.isSuccess()) {
						model.addAttribute("transactionList", listResponse.getList());
						model.addAttribute(ModelMapKey.MESSAGE,
								"Transaction List b/w " + dto.getFromDate() + " and " + dto.getToDate());
					} else {
						model.addAttribute(ModelMapKey.ERROR, listResponse.getMessage());
					}
					return "Admin/DailyReportFiltered";
				}
			}
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/filteredUserlist")
	public String getFilteredUserList(@ModelAttribute DateDTO dto, HttpServletRequest request,
			HttpServletResponse response, Model model, HttpSession session) throws JSONException {

		try {
			dto.setFromDate(ConvertUtil.decodeBase64String(dto.getFromDate()));
			dto.setToDate(ConvertUtil.decodeBase64String(dto.getToDate()));
		} catch (Exception e) {

		}

		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionCheck);
					AllUserResponse userResponse = appAdminApi.getfilteredUserList(dto);
					List<SingleUserRequest> results = new ArrayList<>();
					JSONArray listArray = userResponse.getJsonArray();
					for (int i = 0; i < userResponse.getJsonArray().length(); i++) {
						JSONObject json = listArray.getJSONObject(i);
						SingleUserRequest userRequest = new SingleUserRequest();
						userRequest.setUsername(JSONParserUtil.getString(json, "username"));
						userRequest.setEmail(JSONParserUtil.getString(json, "email"));
						userRequest.setMobile(JSONParserUtil.getString(json, "mobile"));
						userRequest.setContactNo(JSONParserUtil.getString(json, "mobile"));
						userRequest.setBalance(JSONParserUtil.getDouble(json, "balance"));
						userRequest.setPinCode(JSONParserUtil.getString(json, "pinCode"));
						userRequest.setCircleName(JSONParserUtil.getString(json, "circleName"));
						userRequest.setVijayaBankAccount(JSONParserUtil.getString(json, "vijayaBankAccount"));
						userRequest.setDob(JSONParserUtil.getString(json, "dob"));
						userRequest.setAuthority(JSONParserUtil.getString(json, "authority"));
						userRequest.setPoints(JSONParserUtil.getLong(json, "points"));
						userRequest.setCreated(JSONParserUtil.getString(json, "created"));
						userRequest.setAccountType(JSONParserUtil.getString(json, "accountType"));
						userRequest.setGender(JSONParserUtil.getString(json, "gender"));
						results.add(userRequest);
					}
					model.addAttribute("searchedlist", results);
					model.addAttribute(ModelMapKey.MESSAGE,
							"User List b/w " + dto.getFromDate() + " and " + dto.getToDate());
					return "Admin/filteredUserResults";

				}
			}
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/TransactionReport")
	public String filterTransactionReports(@ModelAttribute TransactionFilter dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) throws JSONException {
		try {
			String toDateTime = dto.getEndDate() + " 23:59";
			String fromDateTime = dto.getStartDate() + " 00:00";
			Date to = dateFilter.parse(toDateTime);
			Date from = dateFilter.parse(fromDateTime);

			String sessionCheck = (String) session.getAttribute("adminSessionId");
			if (sessionCheck != null) {
				String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
				if (authority != null) {
					if (authority.contains(Authorities.ADMINISTRATOR)
							&& authority.contains(Authorities.AUTHENTICATED)) {
						/*
						 * SessionDTO sessionDTO = new SessionDTO();
						 * sessionDTO.setSessionId(sessionCheck);
						 * UserTransactionResponse userTransactionResponse =
						 * appAdminApi .getUserTransactionValues(sessionDTO); if
						 * (userTransactionResponse != null) { if
						 * (userTransactionResponse.isSuccess()) { JSONObject
						 * json = null; try { json = new
						 * JSONObject(userTransactionResponse.getResponse()); }
						 * catch (JSONException e) { } if (json != null) {
						 * JSONObject details = JSONParserUtil.getObject(json,
						 * "details"); model.addAttribute("nlme",
						 * JSONParserUtil.getDouble(details,
						 * "totalLoadMoneyEBSNow")); model.addAttribute("nlmv",
						 * JSONParserUtil.getDouble(details,
						 * "totalLoadMoneyVNETNow"));
						 * model.addAttribute("ntopup",
						 * JSONParserUtil.getDouble(details,
						 * "totalPayableNow")); } } }
						 */ // model.addAttribute(ModelMapKey.MESSAGE,
						// "Transactions From " + dto.getStartDate() + " to
						// " + dto.getEndDate());
						AllTransactionRequest transRequest = new AllTransactionRequest();
						transRequest.setPage(0);
						transRequest.setSize(10000000);
						transRequest.setSessionId((String) session.getAttribute("adminSessionId"));
						AllUserResponse allTransaction = appAdminApi.getAllTransactions(transRequest);
						List<TransactionListResponse> results = new ArrayList<>();
						JSONArray listArray = allTransaction.getJsonArray();
						for (int i = 0; i < allTransaction.getJsonArray().length(); i++) {

							JSONObject json = listArray.getJSONObject(i);
							// long milliSecond =
							// Long.parseLong(json.getString("created"));
							// Calendar calendarr = Calendar.getInstance();
							// calendarr.setTimeInMillis(milliSecond);
							long milliSeconds = JSONParserUtil.getLong(json, "dateOfTransaction");
							Calendar calendar = Calendar.getInstance();
							calendar.setTimeInMillis(milliSeconds);
							Date txdate = calendar.getTime();
							// Date transactionDate = calendarr.getTime();
							// System.out.println("the date
							// is:"+transactionDate);
							if (txdate.after(from) && txdate.before(to)) {
								TransactionListResponse userListDTO = new TransactionListResponse();
								userListDTO.setId(JSONParserUtil.getLong(json, "id"));
								userListDTO.setUsername(JSONParserUtil.getString(json, "username"));
								// userListDTO.setAmount(JSONParserUtil.getDouble(json,"amount"));
								userListDTO.setAuthority(JSONParserUtil.getString(json, "authority"));
								userListDTO.setCommission(JSONParserUtil.getDouble(json, "commission"));
								userListDTO.setContactNo(JSONParserUtil.getString(json, "contactNo"));
								userListDTO.setStatus(Status.valueOf(JSONParserUtil.getString(json, "status")));
								userListDTO.setCurrentBalance(JSONParserUtil.getDouble(json, "currentBalance"));
								userListDTO.setDateOfTransaction(dateFilter.format(txdate));
								if (JSONParserUtil.getBoolean(json, "debit")) {
									userListDTO.setAmountPayable((Double.parseDouble(json.getString("amount"))));
								} else {
									userListDTO.setAmountReceivable((Double.parseDouble(json.getString("amount"))));
								}
								userListDTO.setDebit(JSONParserUtil.getBoolean(json, "debit"));
								userListDTO.setTransactionRefNo(JSONParserUtil.getString(json, "transactionRefNo"));
								userListDTO.setDescription(JSONParserUtil.getString(json, "description"));
								userListDTO.setEmail(JSONParserUtil.getString(json, "email"));
								userListDTO.setServiceType(JSONParserUtil.getString(json, "serviceType"));
								results.add(userListDTO);
								// System.out.println(results);

							}

						}
						model.addAttribute("userList", results);
					}

					return "Admin/DailyReportFiltered";
				}
			}
		} catch (Exception ex) {
			model.addAttribute(ModelMapKey.MESSAGE, "Please Enter Date in valid format(YYYY-MM-DD)");
		}
		return "redirect:/Admin/Home";
	}

	// @RequestMapping(method = RequestMethod.POST, value =
	// "/filteredUserKYCResults")
	// public String getFilteredKYCUserList(@ModelAttribute TransactionFilter
	// dto, HttpServletRequest request,
	// HttpServletResponse response, Model model, HttpSession session) throws
	// ParseException {
	//
	// UserListDTO result = new UserListDTO();
	// String toDateTime = dto.getEndDate() + " 23:59";
	// String fromDateTime = dto.getStartDate() + " 00:00";
	// Date to = dateFilter.parse(toDateTime);
	// Date from = dateFilter.parse(fromDateTime);
	//
	// String sessionCheck = (String) session.getAttribute("adminSessionId");
	// if (sessionCheck != null) {
	// String authority =
	// authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
	// if (authority != null) {
	// if (authority.contains(Authorities.ADMINISTRATOR) &&
	// authority.contains(Authorities.AUTHENTICATED)) {
	// AllUserRequest userRequest = new AllUserRequest();
	// userRequest.setPage(0);
	// userRequest.setSize(100000);
	// userRequest.setSessionId(sessionCheck);
	// userRequest.setStatus(UserStatus.ALL);
	// AllUserResponse userResponse = appAdminApi.getAllUser(userRequest);
	// JSONArray data = userResponse.getJsonArray();
	// List<AdminUserDetails> userList = new ArrayList<>();
	// try {
	// if (data != null) {
	// for (int i = 0; i < data.length(); i++) {
	// AdminUserDetails list = new AdminUserDetails();
	// JSONObject jsonobject = data.getJSONObject(i);
	// String userType = JSONParserUtil.getString(jsonobject, "userType");
	// long milliSeconds = Long.parseLong(jsonobject.getString("created"));
	// String kycnonkyc = jsonobject.getJSONObject("accountDetail")
	// .getJSONObject("accountType").getString("name");
	// Calendar calendar = Calendar.getInstance();
	// calendar.setTimeInMillis(milliSeconds);
	// Date txdate = calendar.getTime();
	// if (txdate.after(from) && txdate.before(to)) {
	// if (userType.equalsIgnoreCase("User") &&
	// kycnonkyc.equalsIgnoreCase("KYC")) {
	// list.setAuthority(jsonobject.getString("authority"));
	// list.setFirstName(
	// jsonobject.getJSONObject("userDetail").getString("firstName"));
	// list.setLastname(jsonobject.getJSONObject("userDetail").getString("lastName"));
	// list.setUsername(jsonobject.getJSONObject("userDetail").getString("name"));
	// list.setAccountNumber(
	// jsonobject.getJSONObject("accountDetail").getString("accountNumber"));
	// list.setGender(jsonobject.getJSONObject("userDetail").getString("gender"));
	// list.setDob(jsonobject.getJSONObject("userDetail").getString("dateOfBirth"));
	// list.setKycNonkyc(jsonobject.getJSONObject("accountDetail")
	// .getJSONObject("accountType").getString("name"));
	// list.setEmail(jsonobject.getJSONObject("userDetail").getString("email"));
	// list.setBalance(jsonobject.getJSONObject("accountDetail").getDouble("balance"));
	// list.setContactNo(
	// jsonobject.getJSONObject("userDetail").getString("contactNo"));
	// list.setEmail(jsonobject.getJSONObject("userDetail").getString("email"));
	// list.setMobileToken(jsonobject.getString("mobileToken"));
	// list.setPoints(jsonobject.getJSONObject("accountDetail").getLong("points"));
	// DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");
	// long milliSecond = Long.parseLong(jsonobject.getString("created"));
	// Calendar calendarr = Calendar.getInstance();
	// calendarr.setTimeInMillis(milliSecond);
	// list.setDateOfAccountCreation(formatter.format(calendarr.getTime()));
	// userList.add(list);
	//
	// }
	// }
	// }
	// model.addAttribute("searchedlist", userList);
	// return "Admin/filteredUserResults";
	// }
	//
	// } catch (JSONException e) {
	// e.printStackTrace();
	// }
	//
	// }
	// }
	// }
	// return "Admin/Home";
	//
	// }

	@RequestMapping(method = RequestMethod.GET, value = "/filteredUserResults")
	public String getFilteredUserList(@ModelAttribute TransactionFilter dto, HttpServletRequest request,
			HttpServletResponse response, Model model, HttpSession session) throws ParseException {

		UserListDTO result = new UserListDTO();
		String toDateTime = dto.getEndDate() + " 23:59";
		String fromDateTime = dto.getStartDate() + " 00:00";
		Date to = dateFilter.parse(toDateTime);
		Date from = dateFilter.parse(fromDateTime);

		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					AllUserRequest userRequest = new AllUserRequest();
					userRequest.setPage(0);
					userRequest.setSize(100000);
					userRequest.setSessionId(sessionCheck);
					userRequest.setStatus(UserStatus.ALL);
					AllUserResponse userResponse = appAdminApi.getAllUser(userRequest);
					JSONArray data = userResponse.getJsonArray();
					List<AdminUserDetails> userList = new ArrayList<>();
					try {
						if (data != null) {
							for (int i = 0; i < data.length(); i++) {
								AdminUserDetails list = new AdminUserDetails();
								JSONObject jsonobject = data.getJSONObject(i);
								JSONObject obj = jsonobject.getJSONObject("userDetail");
								String userType = JSONParserUtil.getString(jsonobject, "userType");
								long milliSeconds = Long.parseLong(jsonobject.getString("created"));
								Calendar calendar = Calendar.getInstance();
								calendar.setTimeInMillis(milliSeconds);
								Date txdate = calendar.getTime();
								if (userType.equalsIgnoreCase("User")) {
									if (txdate.after(from) && txdate.before(to)) {
										if (obj.isNull("location") == true) {
											list.setAuthority(jsonobject.getString("authority"));
											list.setFirstName(
													jsonobject.getJSONObject("userDetail").getString("firstName"));
											list.setLastname(
													jsonobject.getJSONObject("userDetail").getString("lastName"));
											list.setUsername(jsonobject.getJSONObject("userDetail").getString("name"));
											list.setVijayaAccountNumber(jsonobject.getJSONObject("accountDetail")
													.getString("vijayaAccountNumber"));
											list.setAccountNumber(jsonobject.getJSONObject("accountDetail")
													.getString("accountNumber"));
											list.setGender(jsonobject.getJSONObject("userDetail").getString("gender"));
											list.setDob(
													jsonobject.getJSONObject("userDetail").getString("dateOfBirth"));
											list.setKycNonkyc(jsonobject.getJSONObject("accountDetail")
													.getJSONObject("accountType").getString("name"));
											list.setEmail(jsonobject.getJSONObject("userDetail").getString("email"));
											list.setBalance(
													jsonobject.getJSONObject("accountDetail").getDouble("balance"));
											list.setContactNo(
													jsonobject.getJSONObject("userDetail").getString("contactNo"));
											list.setEmail(jsonobject.getJSONObject("userDetail").getString("email"));
											list.setMobileToken(jsonobject.getString("mobileToken"));
											list.setPoints(jsonobject.getJSONObject("accountDetail").getLong("points"));
											DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");
											long milliSecond = Long.parseLong(jsonobject.getString("created"));
											Calendar calendarr = Calendar.getInstance();
											calendarr.setTimeInMillis(milliSecond);
											list.setDateOfAccountCreation(formatter.format(calendarr.getTime()));
											userList.add(list);
										} else {
											list.setPinCode(jsonobject.getJSONObject("userDetail")
													.getJSONObject("location").getString("pinCode"));
											list.setCircleName(jsonobject.getJSONObject("userDetail")
													.getJSONObject("location").getString("circleName"));
											list.setAuthority(jsonobject.getString("authority"));
											list.setFirstName(
													jsonobject.getJSONObject("userDetail").getString("firstName"));
											list.setLastname(
													jsonobject.getJSONObject("userDetail").getString("lastName"));
											list.setUsername(jsonobject.getJSONObject("userDetail").getString("name"));

											list.setAccountNumber(jsonobject.getJSONObject("accountDetail")
													.getString("accountNumber"));
											list.setVijayaAccountNumber(jsonobject.getJSONObject("accountDetail")
													.getString("vijayaAccountNumber"));
											list.setGender(jsonobject.getJSONObject("userDetail").getString("gender"));
											list.setDob(
													jsonobject.getJSONObject("userDetail").getString("dateOfBirth"));
											list.setKycNonkyc(jsonobject.getJSONObject("accountDetail")
													.getJSONObject("accountType").getString("name"));
											list.setEmail(jsonobject.getJSONObject("userDetail").getString("email"));
											list.setBalance(
													jsonobject.getJSONObject("accountDetail").getDouble("balance"));
											list.setContactNo(
													jsonobject.getJSONObject("userDetail").getString("contactNo"));
											list.setEmail(jsonobject.getJSONObject("userDetail").getString("email"));
											list.setMobileToken(jsonobject.getString("mobileToken"));
											list.setPoints(jsonobject.getJSONObject("accountDetail").getLong("points"));
											DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");
											long milliSecond = Long.parseLong(jsonobject.getString("created"));
											Calendar calendarr = Calendar.getInstance();
											calendarr.setTimeInMillis(milliSecond);
											list.setDateOfAccountCreation(formatter.format(calendarr.getTime()));
											userList.add(list);
										}
									}
								}
							}
							model.addAttribute("searchedlist", userList);
							return "Admin/filteredUserResults";
						}

					} catch (JSONException e) {
						e.printStackTrace();
					}

				}
			}
		}
		return "Admin/Home";

	}

	@RequestMapping(method = RequestMethod.POST, value = "/filteredUserKYCResults")
	public String getFilteredKYCUserList(@ModelAttribute TransactionFilter dto, HttpServletRequest request,
			HttpServletResponse response, Model model, HttpSession session) throws ParseException {

		try {
			dto.setStartDate(ConvertUtil.decodeBase64String(dto.getStartDate()));
			dto.setEndDate(ConvertUtil.decodeBase64String(dto.getEndDate()));
		} catch (Exception e) {

		}

		UserListDTO result = new UserListDTO();
		String toDateTime = dto.getEndDate() + " 23:59";
		String fromDateTime = dto.getStartDate() + " 00:00";
		Date to = dateFilter.parse(toDateTime);
		Date from = dateFilter.parse(fromDateTime);

		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					AllUserRequest userRequest = new AllUserRequest();
					userRequest.setPage(0);
					userRequest.setSize(100000);
					userRequest.setSessionId(sessionCheck);
					userRequest.setStatus(UserStatus.ALL);
					AllUserResponse userResponse = appAdminApi.getAllUser(userRequest);
					JSONArray data = userResponse.getJsonArray();
					List<AdminUserDetails> userList = new ArrayList<>();
					try {
						if (data != null) {
							for (int i = 0; i < data.length(); i++) {
								AdminUserDetails list = new AdminUserDetails();
								JSONObject jsonobject = data.getJSONObject(i);
								String userType = JSONParserUtil.getString(jsonobject, "userType");
								long milliSeconds = Long.parseLong(jsonobject.getString("created"));
								String kycnonkyc = jsonobject.getJSONObject("accountDetail")
										.getJSONObject("accountType").getString("name");
								Calendar calendar = Calendar.getInstance();
								calendar.setTimeInMillis(milliSeconds);
								Date txdate = calendar.getTime();
								if (txdate.after(from) && txdate.before(to)) {
									if (userType.equalsIgnoreCase("User") && kycnonkyc.equalsIgnoreCase("KYC")) {
										list.setAuthority(jsonobject.getString("authority"));
										list.setFirstName(
												jsonobject.getJSONObject("userDetail").getString("firstName"));
										list.setLastname(jsonobject.getJSONObject("userDetail").getString("lastName"));
										list.setUsername(jsonobject.getJSONObject("userDetail").getString("name"));
										list.setAccountNumber(
												jsonobject.getJSONObject("accountDetail").getString("accountNumber"));
										list.setGender(jsonobject.getJSONObject("userDetail").getString("gender"));
										list.setDob(jsonobject.getJSONObject("userDetail").getString("dateOfBirth"));
										list.setKycNonkyc(jsonobject.getJSONObject("accountDetail")
												.getJSONObject("accountType").getString("name"));
										list.setEmail(jsonobject.getJSONObject("userDetail").getString("email"));
										list.setBalance(jsonobject.getJSONObject("accountDetail").getDouble("balance"));
										list.setContactNo(
												jsonobject.getJSONObject("userDetail").getString("contactNo"));
										list.setEmail(jsonobject.getJSONObject("userDetail").getString("email"));
										list.setMobileToken(jsonobject.getString("mobileToken"));
										list.setPoints(jsonobject.getJSONObject("accountDetail").getLong("points"));
										DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");
										long milliSecond = Long.parseLong(jsonobject.getString("created"));
										Calendar calendarr = Calendar.getInstance();
										calendarr.setTimeInMillis(milliSecond);
										list.setDateOfAccountCreation(formatter.format(calendarr.getTime()));
										list.setCreated(formatter.format(calendarr.getTime()));
										list.setUserType(kycnonkyc);
										userList.add(list);

										// System.out.println("boooom--"+userList);

									}
								}
							}
							model.addAttribute("searchedlist", userList);
							return "Admin/filteredKycUserResults";
						}

					} catch (JSONException e) {
						e.printStackTrace();
					}

				}
			}
		}
		return "Admin/Home";

	}

	// @RequestMapping(method=RequestMethod.GET,value="/filteredUserResults")
	// public String getFilteredUserList(@ModelAttribute TransactionFilter
	// dto,HttpServletRequest request,HttpServletResponse response,Model
	// model,HttpSession session) throws ParseException {
	//
	// UserListDTO result=new UserListDTO();
	// String toDateTime = dto.getEndDate() + " 23:59";
	// String fromDateTime = dto.getStartDate() + " 00:00";
	// Date to = dateFilter.parse(toDateTime);
	// Date from = dateFilter.parse(fromDateTime);
	//
	// String sessionCheck = (String) session.getAttribute("adminSessionId");
	// if (sessionCheck != null) {
	// String authority =
	// authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
	// if(authority != null) {
	// if (authority.contains(Authorities.ADMINISTRATOR) &&
	// authority.contains(Authorities.AUTHENTICATED)) {
	// AllUserRequest userRequest = new AllUserRequest();
	// userRequest.setPage(0);
	// userRequest.setSize(100000);
	// userRequest.setSessionId(sessionCheck);
	// userRequest.setStatus(UserStatus.ALL);
	// AllUserResponse userResponse = appAdminApi.getAllUser(userRequest);
	// JSONArray data = userResponse.getJsonArray();
	// List<AdminUserDetails> userList = new ArrayList<>();
	// try {
	// if (data != null ) {
	//
	// for (int i = 0; i < data.length(); i++) {
	// AdminUserDetails list = new AdminUserDetails();
	// JSONObject jsonobject = data.getJSONObject(i);
	// JSONObject obj=jsonobject.getJSONObject("userDetail");
	// String userType = JSONParserUtil.getString(jsonobject, "userType");
	// long milliSeconds = Long.parseLong(jsonobject.getString("created"));
	// Calendar calendar = Calendar.getInstance();
	// calendar.setTimeInMillis(milliSeconds);
	// Date txdate=calendar.getTime();
	// if(userType.equalsIgnoreCase("User")){
	// if (txdate.after(from) && txdate.before(to)){
	// if (obj.isNull("location")==true) {
	// list.setAuthority(jsonobject.getString("authority"));
	// list.setFirstName(jsonobject.getJSONObject("userDetail").getString("firstName"));
	// list.setLastname(jsonobject.getJSONObject("userDetail").getString("lastName"));
	// list.setUsername(jsonobject.getJSONObject("userDetail").getString("name"));
	// list.setVijayaAccountNumber(jsonobject.getJSONObject("accountDetail").getString("vijayaAccountNumber"));
	// list.setAccountNumber(jsonobject.getJSONObject("accountDetail").getString("accountNumber"));
	// list.setGender(jsonobject.getJSONObject("userDetail").getString("gender"));
	// list.setDob(jsonobject.getJSONObject("userDetail").getString("dateOfBirth"));
	// list.setKycNonkyc(jsonobject.getJSONObject("accountDetail").getJSONObject("accountType").getString("name"));
	// list.setEmail(jsonobject.getJSONObject("userDetail").getString("email"));
	// list.setBalance(jsonobject.getJSONObject("accountDetail").getDouble("balance"));
	// list.setContactNo(jsonobject.getJSONObject("userDetail").getString("contactNo"));
	// list.setEmail(jsonobject.getJSONObject("userDetail").getString("email"));
	// list.setMobileToken(jsonobject.getString("mobileToken"));
	// list.setPoints(jsonobject.getJSONObject("accountDetail").getLong("points"));
	// DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");
	// long milliSecond = Long.parseLong(jsonobject.getString("created"));
	// Calendar calendarr = Calendar.getInstance();
	// calendarr.setTimeInMillis(milliSecond);
	// list.setDateOfAccountCreation(formatter.format(calendarr.getTime()));
	// userList.add(list);
	// }
	// else{
	// list.setPinCode(jsonobject.getJSONObject("userDetail").getJSONObject("location").getString("pinCode"));
	// list.setCircleName(jsonobject.getJSONObject("userDetail").getJSONObject("location").getString("circleName"));
	// list.setAuthority(jsonobject.getString("authority"));
	// list.setFirstName(jsonobject.getJSONObject("userDetail").getString("firstName"));
	// list.setLastname(jsonobject.getJSONObject("userDetail").getString("lastName"));
	// list.setUsername(jsonobject.getJSONObject("userDetail").getString("name"));
	//
	// list.setAccountNumber(jsonobject.getJSONObject("accountDetail").getString("accountNumber"));
	// list.setVijayaAccountNumber(jsonobject.getJSONObject("accountDetail").getString("vijayaAccountNumber"));
	// list.setGender(jsonobject.getJSONObject("userDetail").getString("gender"));
	// list.setDob(jsonobject.getJSONObject("userDetail").getString("dateOfBirth"));
	// list.setKycNonkyc(jsonobject.getJSONObject("accountDetail").getJSONObject("accountType").getString("name"));
	// list.setEmail(jsonobject.getJSONObject("userDetail").getString("email"));
	// list.setBalance(jsonobject.getJSONObject("accountDetail").getDouble("balance"));
	// list.setContactNo(jsonobject.getJSONObject("userDetail").getString("contactNo"));
	// list.setEmail(jsonobject.getJSONObject("userDetail").getString("email"));
	// list.setMobileToken(jsonobject.getString("mobileToken"));
	// list.setPoints(jsonobject.getJSONObject("accountDetail").getLong("points"));
	// DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");
	// long milliSecond = Long.parseLong(jsonobject.getString("created"));
	// Calendar calendarr = Calendar.getInstance();
	// calendarr.setTimeInMillis(milliSecond);
	// list.setDateOfAccountCreation(formatter.format(calendarr.getTime()));
	// userList.add(list);
	// }
	// }
	// }
	// }
	// model.addAttribute("searchedlist",userList);
	// return "Admin/filteredUserResults";
	// }
	// } catch (JSONException e) {
	// e.printStackTrace();
	// }
	//
	// }
	// }
	// }
	// return "Admin/Home";
	// }

	@RequestMapping(method = RequestMethod.GET, value = "/PromoTransactions")
	public String promoTransactionReports(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) throws JSONException {
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					AllTransactionRequest transRequest = new AllTransactionRequest();
					transRequest.setSessionId(sessionCheck);
					AllTransactionResponse allTransaction = appAdminApi.getPromoTransaction(transRequest);
					if (allTransaction.isSuccess()) {
						model.addAttribute("userList", allTransaction.getList());
					} else {
						model.addAttribute(ModelMapKey.ERROR, allTransaction.getMessage());
					}
					return "Admin/PromoTransactions";
				}
			}
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/PromoTransactionsFiltered")
	public String filterPromoTransactionReports(@ModelAttribute AllTransactionRequest dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) throws JSONException {
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionCheck);
					AllTransactionResponse allTransaction = appAdminApi.getPromoTransactionFiltered(dto);
					if (allTransaction.isSuccess()) {
						model.addAttribute("userList", allTransaction.getList());
					} else {
						model.addAttribute(ModelMapKey.ERROR, allTransaction.getMessage());
					}
					return "Admin/PromoTransactions";
				}
			}
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/EmailLog")
	public String getEmail(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) {
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null)
			return "/Admin/EmailLog";
		return "/Admin/Login";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/MaleUsers")
	public String getMaleUser(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) {
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					AllUserRequest onlineRequest = new AllUserRequest();
					onlineRequest.setPage(0);
					onlineRequest.setSize(10000);
					onlineRequest.setSessionId((String) session.getAttribute("adminSessionId"));
					onlineRequest.setStatus(UserStatus.ALL);
					AllUserResponse onlineResponse = appAdminApi.getAllUser(onlineRequest);
					int m = 0;
					int f = 0;
					List<AdminUserDetails> maleUserList = new ArrayList<>();
					try {
						for (int i = 0; i < onlineResponse.getJsonArray().length(); i++) {
							AdminUserDetails single = new AdminUserDetails();
							String gender;
							gender = onlineResponse.getJsonArray().getJSONObject(i).getJSONObject("userDetail")
									.getString("gender");

							single.setUsername(onlineResponse.getJsonArray().getJSONObject(i)
									.getJSONObject("userDetail").getString("firstName"));
							single.setContactNo(onlineResponse.getJsonArray().getJSONObject(i)
									.getJSONObject("userDetail").getString("contactNo"));

							single.setEmail(onlineResponse.getJsonArray().getJSONObject(i).getJSONObject("userDetail")
									.getString("email"));

							single.setAmount(onlineResponse.getJsonArray().getJSONObject(i)
									.getJSONObject("accountDetail").getString("balance"));

							if (gender.equals("M")) {
								maleUserList.add(single);
							}
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
					model.addAttribute("userList", maleUserList);
					return "Admin/MaleUsers";
				}
			}
		}

		return "redirect:/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/FemaleUsers")
	public String getFemaleUser(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) {

		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					AllUserRequest onlineRequest = new AllUserRequest();
					onlineRequest.setPage(0);
					onlineRequest.setSize(10000);
					onlineRequest.setSessionId((String) session.getAttribute("adminSessionId"));
					onlineRequest.setStatus(UserStatus.ALL);
					AllUserResponse onlineResponse = appAdminApi.getAllUser(onlineRequest);
					int m = 0;
					int f = 0;
					List<AdminUserDetails> maleUserList = new ArrayList<>();
					try {
						if (onlineResponse != null) {
							for (int i = 0; i < onlineResponse.getJsonArray().length(); i++) {
								AdminUserDetails single = new AdminUserDetails();
								String gender;
								gender = onlineResponse.getJsonArray().getJSONObject(i).getJSONObject("userDetail")
										.getString("gender");
								single.setUsername(onlineResponse.getJsonArray().getJSONObject(i)
										.getJSONObject("userDetail").getString("firstName"));
								single.setContactNo(onlineResponse.getJsonArray().getJSONObject(i)
										.getJSONObject("userDetail").getString("contactNo"));
								single.setEmail(onlineResponse.getJsonArray().getJSONObject(i)
										.getJSONObject("userDetail").getString("email"));
								single.setAmount(onlineResponse.getJsonArray().getJSONObject(i)
										.getJSONObject("accountDetail").getString("balance"));
								if (gender.equals("F")) {
									maleUserList.add(single);
								}
							}
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
					model.addAttribute("userList", maleUserList);
					return "Admin/FemaleUsers";
				}
			}
		}

		return "redirect:/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/SMSLog")
	public String getSMSLogGet(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) {

		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null)
			return "/Admin/SMSLog";
		return "/Admin/Login";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/SMSLog")
	public String getSMSLog(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model, @RequestParam("startDate") String start, @RequestParam("endDate") String end) {

		try {
			start = ConvertUtil.decodeBase64String(start);
			end = ConvertUtil.decodeBase64String(end);
		} catch (IOException e1) {

		}

		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					MessageLogRequest transRequest = new MessageLogRequest();
					transRequest.setStartDate(start);
					transRequest.setEndDate(end);
					transRequest.setSessionId(sessionCheck);
					MessageLogResponse userResponse = appAdminApi.getMessageLog(transRequest);
					JSONArray data = userResponse.getJsonArray();
					List<AdminUserDetails> userList = new ArrayList<>();
					try {
						if (data != null) {
							for (int i = 0; i < data.length(); i++) {
								AdminUserDetails list = new AdminUserDetails();
								JSONObject jsonobject = data.getJSONObject(i);
								list.setContactNo(jsonobject.getString("destination"));
								list.setEmailTemplate(jsonobject.getString("template"));
								list.setCommision(jsonobject.getString("message"));
								DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");
								long milliSeconds = Long.parseLong(jsonobject.getString("created"));
								Calendar calendar = Calendar.getInstance();
								calendar.setTimeInMillis(milliSeconds);
								list.setDateOfTransaction(formatter.format(calendar.getTime()));
								userList.add(list);
							}
						}
						model.put("userList", userList);
					} catch (JSONException e) {
						e.printStackTrace();
					}

					return "Admin/SMSLog";
				}
			}
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(value = "/{username}/Merchant", method = RequestMethod.GET)
	public String merchantTransaction(@PathVariable String username, Model model, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws JSONException {
		double totaltx = 0;
		double totalcommission = 0;
		long count = 0;
		ReceiptsResponse result = new ReceiptsResponse();
		String sessionId = (String) session.getAttribute("adminSessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					ReceiptsRequest receipts = new ReceiptsRequest();
					receipts.setSessionId(sessionId);
					receipts.setPage(0);
					receipts.setSize(10000);
					receipts.setUsername(username);
					result = appAdminApi.getSingleMerchantTransactionList(receipts);
					List<MTransactionResponseDTO> transactionList = new ArrayList<>();
					try {
						JSONObject json = new JSONObject(result.getResponse());
						JSONObject details = JSONParserUtil.getObject(json, "details");
						JSONObject userList = JSONParserUtil.getObject(details, "userList");
						JSONArray userContent = JSONParserUtil.getArray(userList, "content");
						JSONObject contentList = JSONParserUtil.getObject(details, "commissionList");
						JSONArray commissionContent = JSONParserUtil.getArray(contentList, "content");
						if (userContent.length() != 0 && commissionContent.length() != 0) {
							for (int i = 0; i < userContent.length(); i++) {
								MTransactionResponseDTO transaction = new MTransactionResponseDTO();
								JSONObject debitTransaction = userContent.getJSONObject(i);
								if (commissionContent.length() > i) {
									JSONObject commissionTransaction = commissionContent.getJSONObject(i);
									if (JSONParserUtil.getString(debitTransaction, "status")
											.equalsIgnoreCase("Success"))
										totalcommission += JSONParserUtil.getDouble(commissionTransaction,
												"commission");
									transaction.setCommission(
											JSONParserUtil.getDouble(commissionTransaction, "commission"));
								} else {
									transaction.setCommission(0.0);
								}
								if (!JSONParserUtil.getString(debitTransaction, "serviceType")
										.equals("NEFT Merchant")) {
									if (JSONParserUtil.getString(debitTransaction, "status")
											.equalsIgnoreCase("Success")) {
										count++;
										totaltx += JSONParserUtil.getDouble(debitTransaction, "amount");
									}
									long milliseconds = JSONParserUtil.getLong(debitTransaction, "dateOfTransaction");
									Calendar calendar = Calendar.getInstance();
									calendar.setTimeInMillis(milliseconds);
									transaction.setDate(dateFilter.format(calendar.getTime()));
									transaction.setTransactionRefNo(
											JSONParserUtil.getString(debitTransaction, "transactionRefNo"));
									transaction.setCurrentBalance(
											JSONParserUtil.getDouble(debitTransaction, "currentBalance"));
									transaction.setUsername(debitTransaction.getString("username"));
									transaction.setServiceType(debitTransaction.getString("serviceType"));
									transaction.setAmount(JSONParserUtil.getDouble(debitTransaction, "amount"));
									transaction.setContactNo(JSONParserUtil.getString(debitTransaction, "contactNo"));
									transaction.setEmail(JSONParserUtil.getString(debitTransaction, "email"));
									// transaction.setTotalPages(totalPages);
									transaction.setStatus(
											Status.valueOf(JSONParserUtil.getString(debitTransaction, "status")));
									transaction
											.setDescription(JSONParserUtil.getString(debitTransaction, "description"));
									transaction.setMobileNo(JSONParserUtil.getString(debitTransaction, "userMobileNo"));
									transaction
											.setFirstName(JSONParserUtil.getString(debitTransaction, "userFirstName"));
									transactionList.add(transaction);
								}
							}
							request.setAttribute("totaltx",
									BigDecimal.valueOf(totaltx).setScale(2, RoundingMode.HALF_UP).doubleValue());
							request.setAttribute("totalcommission", BigDecimal.valueOf(totalcommission)
									.setScale(2, RoundingMode.HALF_UP).doubleValue());
							request.setAttribute("totalnotxs", count);

						}
						model.addAttribute("userList", transactionList);
						return "Admin/merchant";
					} catch (JSONException e) {
						e.printStackTrace();
						// return "Admin/Home";
					}
					return "Admin/merchant";
				}

			}
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(value = "/{username}/Merchants", method = RequestMethod.POST)
	public String merchantTransactionFilter(@PathVariable String username, Model model, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws JSONException {
		UserListDTO resultSet = new UserListDTO();
		double totaltx = 0;
		long count = 0;
		ReceiptsResponse result = new ReceiptsResponse();
		String sessionId = (String) session.getAttribute("adminSessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					ReceiptsRequest receipts = new ReceiptsRequest();
					receipts.setSessionId(sessionId);
					receipts.setPage(0);
					receipts.setSize(1000000);
					receipts.setUsername(username);
					result = appAdminApi.getSingleMerchantTransactionList(receipts);
					List<MTransactionResponseDTO> transactionList = new ArrayList<>();
					if (result.isSuccess()) {
						try {
							JSONObject json = new JSONObject(result.getResponse());
							JSONObject details = JSONParserUtil.getObject(json, "details");
							JSONObject userList = JSONParserUtil.getObject(details, "userList");
							JSONArray content = JSONParserUtil.getArray(userList, "content");
							if (content != null) {
								for (int i = 0; i < content.length(); i++) {
									JSONObject temp = content.getJSONObject(i);
									if (!JSONParserUtil.getString(temp, "serviceType").equals("NEFT ")) {
										count++;
										boolean debit = JSONParserUtil.getBoolean(temp, "debit");
										totaltx += JSONParserUtil.getDouble(temp, "amount");
										MTransactionResponseDTO transaction = new MTransactionResponseDTO();
										long milliseconds = JSONParserUtil.getLong(temp, "dateOfTransaction");
										Calendar calendar = Calendar.getInstance();
										calendar.setTimeInMillis(milliseconds);
										transaction.setDate(dateFilter.format(calendar.getTime()));
										transaction.setTransactionRefNo(
												JSONParserUtil.getString(temp, "transactionRefNo"));
										transaction.setCurrentBalance(JSONParserUtil.getDouble(temp, "currentBalance"));
										transaction.setUsername(temp.getString("username"));
										transaction.setServiceType(temp.getString("serviceType"));
										transaction.setAmount(JSONParserUtil.getDouble(temp, "amount"));
										transaction.setContactNo(JSONParserUtil.getString(temp, "contactNo"));
										transaction.setEmail(JSONParserUtil.getString(temp, "email"));
										// transaction.setTotalPages(totalPages);
										transaction.setStatus(Status.valueOf(JSONParserUtil.getString(temp, "status")));
										transaction.setDescription(JSONParserUtil.getString(temp, "description"));
										transaction.setMobileNo(JSONParserUtil.getString(temp, "userMobileNo"));
										transaction.setFirstName(JSONParserUtil.getString(temp, "userFirstName"));
										transactionList.add(transaction);
									}
								}
								request.setAttribute("totaltx",
										BigDecimal.valueOf(totaltx).setScale(2, RoundingMode.HALF_UP).doubleValue());
								request.setAttribute("totalnotxs", count);

							}
							JSONObject commissionList = JSONParserUtil.getObject(details, "commissionList");
							JSONArray contentList = JSONParserUtil.getArray(commissionList, "content");
							if (contentList != null) {
								for (int i = 0; i < contentList.length(); i++) {
									JSONObject temp = contentList.getJSONObject(i);
									if (!JSONParserUtil.getString(temp, "serviceType").equals("NEFT Merchant")) {
										MTransactionResponseDTO transaction = new MTransactionResponseDTO();
										transaction.setCommission(JSONParserUtil.getDouble(temp, "commission"));
										transactionList.add(transaction);
									}
								}
							}
							model.addAttribute("userList", transactionList);
							return "Admin/merchant";
						} catch (JSONException e) {
							e.printStackTrace();
							// return "Admin/Home";
						}
						return "Admin/merchant";
					}

				}
			}
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(value = "/Merchant/{transactionRefNo}", method = RequestMethod.GET)
	public String transactionIdClickable(ModelMap modelMap, @PathVariable String transactionRefNo, HttpSession session,
			Model model) throws JSONException {

		String sessionCheck = (String) session.getAttribute("adminSessionId");
		TransactionRequest req = new TransactionRequest();
		if (transactionRefNo.length() > 14) {
			req.setSessionId(sessionCheck);
			req.setTransactionRefNo(transactionRefNo);
		} else {
			String temp = transactionRefNo.substring(0, 13);
			req.setSessionId(sessionCheck);
			req.setTransactionRefNo(temp + "D");
		}
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {

					UserResponse response = appAdminApi.transactionIdClickable(req);
					model.addAttribute("created", response.getCreated());
					model.addAttribute("firstname", response.getFirstName());
					model.addAttribute("email", response.getEmail());
					model.addAttribute("contactNo", response.getContactNo());
					model.addAttribute("balance", response.getBalance());
					model.addAttribute("accountType", response.getAccountType());
					model.addAttribute("dob", response.getDateOfBirth());
					model.addAttribute("authority", response.getAuthority());

				}

			}
		}
		return "Admin/UserTransactions";
	}

	@RequestMapping(value = "/UserDetail/{transactionRefNo}", method = RequestMethod.GET)
	public String transactionId(ModelMap modelMap, @PathVariable String transactionRefNo, HttpSession session,
			Model model) throws JSONException {

		String sessionCheck = (String) session.getAttribute("adminSessionId");
		TransactionRequest req = new TransactionRequest();
		if (transactionRefNo.length() > 14) {
			req.setSessionId(sessionCheck);
			req.setTransactionRefNo(transactionRefNo);
		} else {
			String temp = transactionRefNo.substring(0, 13);
			req.setSessionId(sessionCheck);
			req.setTransactionRefNo(temp + "D");
		}
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {

					UserResponse response = appAdminApi.transactionRefNo(req);
					model.addAttribute("created", response.getCreated());
					model.addAttribute("firstname", response.getFirstName());
					model.addAttribute("email", response.getEmail());
					model.addAttribute("contactNo", response.getContactNo());
					model.addAttribute("balance", response.getBalance());
					model.addAttribute("accountType", response.getAccountType());
					model.addAttribute("dob", response.getDateOfBirth());
					model.addAttribute("authority", response.getAuthority());

				}

			}
		}
		return "Admin/UserTransactions";
	}

	/*
	 * @RequestMapping(value = "/User/{userName}", method = RequestMethod.GET)
	 * public String userDetails(ModelMap modelMap, @PathVariable String
	 * userName, HttpSession session, Model model) throws JSONException {
	 * 
	 * String sessionCheck = (String) session.getAttribute("adminSessionId");
	 * String userAccountNumber = null; List<AdminUserDetails> userList = new
	 * ArrayList<AdminUserDetails>(); if (sessionCheck != null) { String
	 * authority = authenticationApi.getAuthorityFromSession(sessionCheck,
	 * Role.USER); if (authority != null) { if
	 * (authority.contains(Authorities.ADMINISTRATOR) &&
	 * authority.contains(Authorities.AUTHENTICATED)) { UserTransactionRequest
	 * req = new UserTransactionRequest(); req.setUsername(userName);
	 * req.setPage(0); req.setSize(1000000); req.setSessionId((String)
	 * session.getAttribute("adminSessionId")); AllTransactionResponse
	 * allTransaction = appAdminApi.getSingleUser(req);
	 * allTransaction.getResponse(); JSONArray data =
	 * allTransaction.getJsonArray(); for (int i = 0; i < data.length(); i++) {
	 * AdminUserDetails list = new AdminUserDetails(); JSONObject jsonobject =
	 * data.getJSONObject(i); list.setAmount(jsonobject.getString("amount"));
	 * list.setCurrentBalance(jsonobject.getString("currentBalance"));
	 * list.setStatus(jsonobject.getString("status"));
	 * list.setContactNo(userName);
	 * list.setTransactionRefNo(removeLastChar(jsonobject.getString(
	 * "transactionRefNo")));
	 * list.setServiceType(jsonobject.getString("description")); if
	 * ((jsonobject.getString("transactionRefNo").contains("D"))) {
	 * list.setDebit(jsonobject.getString("amount")); } else {
	 * list.setCredit(jsonobject.getString("amount")); } if (i == 0) {
	 * userAccountNumber =
	 * jsonobject.getJSONObject("account").getLong("accountNumber") + ""; }
	 * DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm"); long
	 * milliSeconds = Long.parseLong(jsonobject.getString("created")); Calendar
	 * calendar = Calendar.getInstance();
	 * calendar.setTimeInMillis(milliSeconds);
	 * list.setDateOfTransaction(formatter.format(calendar.getTime()));
	 * userList.add(list); } AllUserRequest userRequest = new AllUserRequest();
	 * userRequest.setPage(0); userRequest.setSize(1000000);
	 * userRequest.setSessionId((String)
	 * session.getAttribute("adminSessionId"));
	 * userRequest.setStatus(UserStatus.ALL); AllUserResponse userResponse =
	 * appAdminApi.getAllUser(userRequest); JSONArray userData =
	 * userResponse.getJsonArray(); AdminUserDetails l = new AdminUserDetails();
	 * if (userData != null) { for (int j = 0; j < userData.length(); j++) {
	 * JSONObject obj = new JSONObject(); obj = userData.getJSONObject(j);
	 * String oneAccontNumber =
	 * obj.getJSONObject("accountDetail").getString("accountNumber"); if
	 * (userAccountNumber != null && oneAccontNumber != null) { if
	 * (userAccountNumber.equals(oneAccontNumber)) {
	 * l.setAuthority(obj.getString("authority"));
	 * l.setMobileStatus(obj.getString("mobileStatus"));
	 * l.setEmailStatus(obj.getString("emailStatus"));
	 * l.setEmail(obj.getJSONObject("userDetail").getString("email"));
	 * l.setMobileToken(obj.getString("mobileToken"));
	 * l.setFirstName(obj.getJSONObject("userDetail").getString("name"));
	 * l.setContactNo(obj.getJSONObject("userDetail").getString("contactNo"));
	 * if (obj.getJSONObject("userDetail").getString("image") == null) {
	 * l.setImage("/resources/images/sample.jpg"); } else {
	 * l.setImage(obj.getJSONObject("userDetail").getString("image")); } }
	 * 
	 * } } }
	 * 
	 * Calendar cal = Calendar.getInstance(); model.addAttribute("year",
	 * cal.get(Calendar.YEAR)); model.addAttribute("month", (new
	 * Date().getMonth()) + 1); model.addAttribute("date",
	 * cal.get(Calendar.DATE)); model.addAttribute("userList", userList);
	 * model.addAttribute("user", l); return "Admin/User"; } } } return
	 * "redirect:/Admin/Home"; }
	 */

	@RequestMapping(value = "/User/{userName}", method = RequestMethod.GET)
	public String userDetails(ModelMap modelMap, @PathVariable String userName, HttpSession session, Model model)
			throws JSONException {
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		String userAccountNumber = null;
		List<AdminUserDetails> userList = new ArrayList<AdminUserDetails>();
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					UserTransactionRequest req = new UserTransactionRequest();
					req.setUsername(userName);
					req.setPage(0);
					req.setSize(10000000);
					req.setSessionId((String) session.getAttribute("adminSessionId"));
					AllTransactionResponse allTransaction = appAdminApi.getSingleUser(req);
					allTransaction.getResponse();
					JSONArray data = allTransaction.getJsonArray();
					if (data != null) {
						for (int i = 0; i < data.length(); i++) {
							AdminUserDetails list = new AdminUserDetails();
							JSONObject jsonobject = data.getJSONObject(i);
							list.setAmount(jsonobject.getString("amount"));
							list.setCurrentBalance(jsonobject.getString("currentBalance"));
							list.setStatus(jsonobject.getString("status"));
							list.setContactNo(userName);
							list.setTransactionRefNo(removeLastChar(jsonobject.getString("transactionRefNo")));
							list.setServiceType(jsonobject.getString("description"));
							if ((jsonobject.getString("transactionRefNo").contains("D"))) {
								list.setDebit(jsonobject.getString("amount"));
							} else {
								list.setCredit(jsonobject.getString("amount"));
							}
							if (i == 0) {
								userAccountNumber = jsonobject.getJSONObject("account").getLong("accountNumber") + "";
							}
							DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");
							long milliSeconds = Long.parseLong(jsonobject.getString("created"));
							Calendar calendar = Calendar.getInstance();
							calendar.setTimeInMillis(milliSeconds);
							list.setDateOfTransaction(formatter.format(calendar.getTime()));
							userList.add(list);
						}
					}
					SingleUserRequest userRequest = new SingleUserRequest();
					userRequest.setSessionId((String) session.getAttribute("adminSessionId"));
					userRequest.setUsername(userName);
					AllUserResponse userResponse = appAdminApi.getUserFromMobileNo(userRequest);
					JSONObject userData = userResponse.getJsonObject();
					String userType = userData.getString("userType");
					if (userType.equalsIgnoreCase("User") || userType.equalsIgnoreCase("Agent")
							&& (userData.getString("mobile")).equalsIgnoreCase(userName)) {
						userRequest.setAuthority(userData.getString("authority"));
						userRequest.setMobileStatus(userData.getString("mobileStatus"));
						userRequest.setEmailStatus(userData.getString("emailStatus"));
						userRequest.setMobileToken(userData.getString("mobileToken"));
						userRequest.setFirstName(userData.getString("username"));
						userRequest.setMobile(userData.getString("mobile"));
						userRequest.setEmail(userData.getString("email"));
						userRequest.setCreated(userData.getString("created"));
						userRequest.setGcmId(userData.getString("gcmId"));
						userRequest.setOsName(userData.getString("osName"));
						userRequest.setAccountTypeStatus(userData.getString("accountTypeStatus"));
						userRequest.setAccountBalance(userData.getDouble("accountBalance"));
						if (userData.getString("image") == null) {
							userRequest.setImage("/resources/images/sample.jpg");
						} else {
							userRequest.setImage(userData.getString("image"));
						}
					}
					ResponseDTO map = new ResponseDTO();
					JsonArray jsonArray = new JsonArray();
					try {
						map = appAdminApi.findSingleUserTransaction(sessionCheck, userName);
						Iterator it = map.getTransvalue().entrySet().iterator();
						for (int i = 0; i < map.getTransvalue().size(); i++) {
							while (it.hasNext()) {
								Map.Entry pair = (Map.Entry) it.next();
								JsonObject payload = new JsonObject();
								payload.addProperty("date", pair.getKey().toString());
								payload.addProperty("visits", pair.getValue().toString());
								jsonArray.add(payload);
							}
						}
					} catch (Exception e) {
						logger.error(e.getMessage());
					}
					modelMap.put("key", jsonArray);
					Calendar cal = Calendar.getInstance();
					model.addAttribute("year", cal.get(Calendar.YEAR));
					model.addAttribute("month", (new Date().getMonth()) + 1);
					model.addAttribute("date", cal.get(Calendar.DATE));
					model.addAttribute("userList", userList);
					model.addAttribute("user", userRequest);
					return "Admin/User";
				}
			}
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(value = "/User/Block/{userName}", method = RequestMethod.GET)
	public String userBlock(ModelMap modelMap, @PathVariable String userName, HttpSession session, Model model) {
		LogCat.print("Block Users :: " + userName);
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					BlockUserRequest req = new BlockUserRequest();
					req.setAdminSessionId(sessionCheck);
					req.setUsername(userName);
					BlockUserResponse resp = appAdminApi.userBlock(req);
					model.addAttribute("message", resp.getMessage());
					return "redirect:/Admin/User/" + userName;
				}
			}
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(value = "/User/Unblock/{userName}", method = RequestMethod.GET)
	public String userUnBlock(ModelMap modelMap, @PathVariable String userName, HttpSession session, Model model) {
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					BlockUnBlockUserRequest req = new BlockUnBlockUserRequest();
					req.setSessionId((String) session.getAttribute("adminSessionId"));
					req.setUsername(userName);
					BlockUnBlockUserResponse resp = appAdminApi.unblockUser(req);
					model.addAttribute("message", resp.getMessage());
					return "redirect:/Admin/User/" + userName;
				}
			}
		}
		return "/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/PoolAccount")
	public String getPoolAccount(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) throws JSONException {

		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					AllTransactionRequest transRequest = new AllTransactionRequest();
					transRequest.setPage(0);
					transRequest.setSize(100);
					transRequest.setSessionId(sessionCheck);
					AllTransactionResponse allTransaction = appAdminApi.getAllTransaction(transRequest);
					AllUserRequest user = new AllUserRequest();
					user.setPage(0);
					user.setSize(10000);
					user.setSessionId((String) session.getAttribute("adminSessionId"));
					user.setStatus(UserStatus.ALL);
					AllUserResponse allUsers = appAdminApi.getAllUser(user);
					List<AdminUserDetails> userList = new ArrayList<AdminUserDetails>();
					JSONArray u = allUsers.getJsonArray();
					JSONArray t = allTransaction.getJsonArray();
					if (u != null && t != null) {
						for (int s = 0; s < t.length(); s++) {
							for (int i = 0; i < u.length(); i++) {
								JSONObject singleU = u.getJSONObject(i);
								String di = singleU.getJSONObject("accountDetail").getString("id");
								AdminUserDetails list = new AdminUserDetails();
								JSONObject singleT = t.getJSONObject(s);
								String id = singleT.getJSONObject("account").getString("id");
								if (id.equals(di) && singleT.get("status").equals("Success")
										&& !singleU.get("username").equals("instantpay@payqwik.in")
										&& !singleT.get("service").equals(null)
										&& !singleT.getJSONObject("service").get("code").equals("SMR")
										&& !singleT.getJSONObject("service").get("code").equals("SMU")) {
									if (!singleT.get("transactionType").equals("COMMISSION")
											&& !singleT.get("transactionType").equals("SETTLEMENT")) {
										list.setAmount(singleT.getString("amount"));
										list.setCurrentBalance(singleT.getString("currentBalance"));
										list.setStatus(singleT.getString("status"));
										list.setTransactionRefNo(removeLastChar(singleT.getString("transactionRefNo")));
										list.setServiceType(singleT.getString("description"));
										long milliSeconds = Long.parseLong(singleT.getString("created"));
										Calendar calendar = Calendar.getInstance();
										calendar.setTimeInMillis(milliSeconds);
										list.setUsername(singleU.getJSONObject("userDetail").getString("firstName")
												+ " " + singleU.getJSONObject("userDetail").getString("lastName"));
										list.setContactNo(singleU.getJSONObject("userDetail").getString("contactNo"));
										list.setBalance(singleU.getJSONObject("accountDetail").getDouble("balance"));
										list.setDateOfTransaction(dateFilter.format(calendar.getTime()));
										if ((singleT.getBoolean("debit"))) {
											list.setDebit(singleT.getString("amount"));
										} else {
											list.setCredit(singleT.getString("amount"));
										}
										userList.add(list);
									}
								}
							}
						}
					}
					try {
						model.put("userList", userList);
					} catch (Exception e) {
						e.printStackTrace();
					}
					return "Admin/PoolAccount";
				}
			}
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/PoolAccount")
	public String filterPoolAccountTransactions(@ModelAttribute TransactionFilter dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) throws JSONException {
		try {

			dto.setStartDate(ConvertUtil.decodeBase64String(dto.getStartDate()));
			dto.setEndDate(ConvertUtil.decodeBase64String(dto.getEndDate()));

			String fromDateTime = dto.getStartDate() + " 00:00";
			String toDateTime = dto.getEndDate() + " 23:59";
			Date to = dateFilter.parse(toDateTime);
			Date from = dateFilter.parse(fromDateTime);

			String sessionCheck = (String) session.getAttribute("adminSessionId");
			if (sessionCheck != null) {

				String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
				if (authority != null) {
					if (authority.contains(Authorities.ADMINISTRATOR)
							&& authority.contains(Authorities.AUTHENTICATED)) {
						model.addAttribute(ModelMapKey.MESSAGE,
								"Pool Transactions From " + dto.getStartDate() + " to " + dto.getEndDate());
						AllTransactionRequest transRequest = new AllTransactionRequest();
						transRequest.setPage(0);
						transRequest.setSize(10000);
						transRequest.setSessionId((String) session.getAttribute("adminSessionId"));
						AllTransactionResponse allTransaction = appAdminApi.getAllTransaction(transRequest);
						AllUserRequest user = new AllUserRequest();
						user.setPage(0);
						user.setSize(100);
						user.setSessionId((String) session.getAttribute("adminSessionId"));
						user.setStatus(UserStatus.ALL);
						AllUserResponse allUsers = appAdminApi.getAllUser(user);
						List<AdminUserDetails> userList = new ArrayList<AdminUserDetails>();
						JSONArray u = allUsers.getJsonArray();
						JSONArray t = allTransaction.getJsonArray();
						if (t != null && u != null) {
							for (int s = 0; s < t.length(); s++) {
								for (int i = 0; i < u.length(); i++) {
									JSONObject singleU = u.getJSONObject(i);
									String di = singleU.getJSONObject("accountDetail").getString("id");
									AdminUserDetails list = new AdminUserDetails();
									JSONObject singleT = t.getJSONObject(s);
									String id = singleT.getJSONObject("account").getString("id");
									if (id.equals(di) && singleT.get("status").equals("Success")
											&& !singleU.get("username").equals("instantpay@payqwik.in")
											&& !singleT.get("service").equals(null)
											&& !singleT.getJSONObject("service").get("code").equals("SMR")
											&& !singleT.getJSONObject("service").get("code").equals("SMU")) {
										if (!singleT.get("transactionType").equals("COMMISSION")
												&& !singleT.get("transactionType").equals("SETTLEMENT")) {
											// list.setCommision(z.getString("commission"));
											long milliSeconds = Long.parseLong(singleT.getString("created"));
											Calendar calendar = Calendar.getInstance();
											calendar.setTimeInMillis(milliSeconds);
											Date transactionDate = calendar.getTime();
											if (transactionDate.after(from) && transactionDate.before(to)) {
												list.setAmount(singleT.getString("amount"));
												list.setCurrentBalance(singleT.getString("currentBalance"));
												list.setStatus(singleT.getString("status"));
												list.setTransactionRefNo(
														removeLastChar(singleT.getString("transactionRefNo")));
												list.setServiceType(singleT.getString("description"));
												list.setUsername(singleU.getJSONObject("userDetail")
														.getString("firstName") + " "
														+ singleU.getJSONObject("userDetail").getString("lastName"));
												list.setContactNo(
														singleU.getJSONObject("userDetail").getString("contactNo"));
												list.setBalance(
														singleU.getJSONObject("accountDetail").getDouble("balance"));
												list.setDateOfTransaction(dateFilter.format(calendar.getTime()));
												if ((singleT.getBoolean("debit"))) {
													list.setDebit(singleT.getString("amount"));
												} else {
													list.setCredit(singleT.getString("amount"));
												}

												userList.add(list);
											}
										}
									}
								}
							}
						}
						try {

							model.put("userList", userList);
						} catch (Exception e) {
							e.printStackTrace();
						}
						return "Admin/PoolAccount";
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/CommissionReportInJSON")
	public ResponseEntity<UserListDTO> getCommissionReportInJson(@ModelAttribute PagingDTO dto,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model)
			throws JSONException {
		UserListDTO resultSet = new UserListDTO();
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		// String sessionCheck = dto.getSessionId();
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					AllTransactionRequest transRequest = new AllTransactionRequest();
					transRequest.setPage(dto.getPage());
					transRequest.setSize(dto.getSize());
					transRequest.setSessionId(sessionCheck);
					AllUserResponse allTransaction = appAdminApi.getAllTransactions(transRequest);
					List<TransactionListResponse> results = new ArrayList<>();
					JSONArray listArray = allTransaction.getJsonArray();
					for (int i = 0; i < allTransaction.getJsonArray().length(); i++) {
						JSONObject json = listArray.getJSONObject(i);
						TransactionListResponse userListDTO = new TransactionListResponse();
						userListDTO.setId(JSONParserUtil.getLong(json, "id"));
						userListDTO.setUsername(JSONParserUtil.getString(json, "username"));
						userListDTO.setAmount(JSONParserUtil.getDouble(json, "amount"));
						userListDTO.setAuthority(JSONParserUtil.getString(json, "authority"));
						userListDTO.setCommission(JSONParserUtil.getDouble(json, "commission"));
						userListDTO.setContactNo(JSONParserUtil.getString(json, "contactNo"));
						userListDTO.setCurrentBalance(JSONParserUtil.getDouble(json, "currentBalance"));
						long milliSeconds = JSONParserUtil.getLong(json, "dateOfTransaction");
						Calendar calendar = Calendar.getInstance();
						calendar.setTimeInMillis(milliSeconds);
						userListDTO.setDateOfTransaction(dateFilter.format(calendar.getTime()));
						userListDTO.setDebit(JSONParserUtil.getBoolean(json, "debit"));
						userListDTO.setTransactionRefNo(JSONParserUtil.getString(json, "transactionRefNo"));
						userListDTO.setDescription(JSONParserUtil.getString(json, "description"));
						userListDTO.setEmail(JSONParserUtil.getString(json, "email"));
						userListDTO.setServiceType(JSONParserUtil.getString(json, "serviceType"));
						userListDTO.setStatus(Status.valueOf(JSONParserUtil.getString(json, "status")));
						results.add(userListDTO);
					}
					resultSet.setSuccess(allTransaction.isSuccess());
					resultSet.setJsonArray(results);
					resultSet.setFirstPage(allTransaction.isFirstPage());
					resultSet.setLastPage(allTransaction.isLastPage());
					resultSet.setNumberOfElements(allTransaction.getNumberOfElements());
					resultSet.setSize(allTransaction.getSize());
					resultSet.setTotalPages(allTransaction.getTotalPages());
				}
			}
		}

		return new ResponseEntity<UserListDTO>(resultSet, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/CommissionAccount")
	public String getCommissionAccount(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) throws JSONException {
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					// AllTransactionRequest transRequest = new
					// AllTransactionRequest();
					// transRequest.setPage(0);
					// transRequest.setSize(100000);
					// transRequest.setSessionId((String)
					// session.getAttribute("adminSessionId"));
					// AllTransactionResponse allTransaction =
					// appAdminApi.getAllTransaction(transRequest);
					// AllUserRequest user = new AllUserRequest();
					// user.setPage(0);
					// user.setSize(100000);
					// user.setSessionId((String)
					// session.getAttribute("adminSessionId"));
					// user.setStatus(UserStatus.ALL);
					// AllUserResponse allUsers = appAdminApi.getAllUser(user);
					// List<AdminUserDetails> userList = new
					// ArrayList<AdminUserDetails>();
					// JSONArray u = allUsers.getJsonArray();
					// JSONArray t = allTransaction.getJsonArray();
					// if (u != null && t != null) {
					// for (int i = 0; i < u.length(); i++) {
					// JSONObject k = u.getJSONObject(i);
					// String di =
					// k.getJSONObject("accountDetail").getString("id");
					// for (int s = 0; s < t.length(); s++) {
					// AdminUserDetails list = new AdminUserDetails();
					// JSONObject z = t.getJSONObject(s);
					// String id = z.getJSONObject("account").getString("id");
					// if (id.equals(di)) {
					// if (!z.get("service").equals(null)
					// && !z.getJSONObject("service").get("code").equals("LMC")
					// && z.get("status").equals("Success")
					// && z.get("transactionType").equals("COMMISSION")) {
					// list.setAmount(z.getString("amount"));
					// list.setCurrentBalance(z.getString("currentBalance"));
					// list.setStatus(z.getString("status"));
					// list.setTransactionRefNo(z.getString("transactionRefNo"));
					// list.setServiceType(z.getString("description"));
					// // list.setCommision(z.getString("commission"));
					// long milliSeconds =
					// Long.parseLong(z.getString("created"));
					// Calendar calendar = Calendar.getInstance();
					// calendar.setTimeInMillis(milliSeconds);
					// list.setUsername(k.getJSONObject("userDetail").getString("firstName")
					// + " "
					// + k.getJSONObject("userDetail").getString("lastName"));
					// list.setContactNo(k.getJSONObject("userDetail").getString("contactNo"));
					// list.setBalance(k.getJSONObject("accountDetail").getDouble("balance"));
					// list.setDateOfTransaction(dateFilter.format(calendar.getTime()));
					// // list.setCommision(z.getString("commission"));
					// list.setCommision(z.getString("amount"));
					// userList.add(list);
					// }
					// }
					// }
					// }
					// }
					// try {
					// model.put("userList", userList);
					// } catch (Exception e) {
					// e.printStackTrace();
					// }
					return "Admin/CommissionAccount";
				}
			}
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/CommissionAccounts")
	public String getCommissionAccount(@ModelAttribute TransactionFilter dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) throws JSONException {

		try {

			dto.setFromDate(ConvertUtil.decodeBase64String(dto.getFromDate()));
			dto.setToDate(ConvertUtil.decodeBase64String(dto.getToDate()));

			String sessionCheck = (String) session.getAttribute("adminSessionId");
			if (sessionCheck != null) {
				String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
				if (authority != null) {
					if (authority.contains(Authorities.ADMINISTRATOR)
							&& authority.contains(Authorities.AUTHENTICATED)) {
						model.addAttribute(ModelMapKey.MESSAGE,
								"Commission Transactions From " + dto.getFromDate() + " to " + dto.getToDate());
						AllTransactionRequest transRequest = new AllTransactionRequest();
						transRequest.setPage(0);
						transRequest.setSize(100000);
						transRequest.setStartDate(dto.getFromDate());
						transRequest.setEndDate(dto.getToDate());
						transRequest.setReportType(dto.getServiceType());
						transRequest.setSessionId((String) session.getAttribute("adminSessionId"));
						AllTransactionResponse allTransaction = appAdminApi.getAllTransaction(transRequest);
						if (allTransaction.isSuccess()) {
							List<TransactionReport> reports = new ArrayList<>();
							JSONArray reportArray = allTransaction.getJsonArray();
							if (reportArray != null) {
								for (int i = 0; i < reportArray.length(); i++) {
									JSONObject json = reportArray.getJSONObject(i);
									TransactionReport report = new TransactionReport();
									report.setAmount(JSONParserUtil.getDouble(json, "amount"));
									report.setDate(JSONParserUtil.getString(json, "date"));
									report.setService(JSONParserUtil.getString(json, "service"));
									/*
									 * report.setDebit(JSONParserUtil.getBoolean
									 * (json, "debit"));
									 */
									report.setTransactionRefNo(JSONParserUtil.getString(json, "transactionRefNo"));
									report.setStatus(com.payqwikweb.model.web.Status
											.valueOf(JSONParserUtil.getString(json, "status")));
									report.setDescription(JSONParserUtil.getString(json, "description"));
									reports.add(report);
								}
								model.addAttribute("transactions", reports);
							}
							return "Admin/commissionReportFiltered";
						}
						return "Admin/CommissionAccount";
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return "redirect:/Admin/Home";
	}

	// For nikki Transaction

	@RequestMapping(method = RequestMethod.GET, value = "/NikkiTransaction")
	public String getNikkiTransaction(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) throws JSONException {
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {

					return "Admin/NikkiChatTransactions";
				}
			}
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/NikkiTransactions")
	public String getNikkiTransaction(@ModelAttribute TransactionFilter dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) throws JSONException {

		try {

			dto.setFromDate(ConvertUtil.decodeBase64String(dto.getFromDate()));
			dto.setToDate(ConvertUtil.decodeBase64String(dto.getToDate()));

			String sessionCheck = (String) session.getAttribute("adminSessionId");
			if (sessionCheck != null) {
				String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
				if (authority != null) {
					if (authority.contains(Authorities.ADMINISTRATOR)
							&& authority.contains(Authorities.AUTHENTICATED)) {
						model.addAttribute(ModelMapKey.MESSAGE,
								"Nikki Chat Transactions From " + dto.getFromDate() + " to " + dto.getToDate());
						AllTransactionRequest transRequest = new AllTransactionRequest();
						transRequest.setPage(0);
						transRequest.setSize(100000);
						transRequest.setStartDate(dto.getFromDate());
						transRequest.setEndDate(dto.getToDate());
						// transRequest.setReportType(dto.getServiceType());
						transRequest.setSessionId((String) session.getAttribute("adminSessionId"));
						AllTransactionResponse allTransaction = appAdminApi.getNikkiTransaction(transRequest);
						if (allTransaction.isSuccess()) {
							List<TransactionReport> reports = new ArrayList<>();
							JSONArray reportArray = allTransaction.getJsonArray();
							if (reportArray != null) {
								for (int i = 0; i < reportArray.length(); i++) {
									JSONObject json = reportArray.getJSONObject(i);
									TransactionReport report = new TransactionReport();
									report.setContactNo(JSONParserUtil.getString(json, "contactNo"));

									report.setUsername(JSONParserUtil.getString(json, "username"));
									report.setAmount(JSONParserUtil.getDouble(json, "amount"));
									report.setDate(JSONParserUtil.getString(json, "date"));
									report.setService(JSONParserUtil.getString(json, "service"));
									/*
									 * report.setDebit(JSONParserUtil.getBoolean
									 * (json, "debit"));
									 */
									report.setTransactionRefNo(JSONParserUtil.getString(json, "transactionRefNo"));
									report.setStatus(com.payqwikweb.model.web.Status
											.valueOf(JSONParserUtil.getString(json, "status")));
									report.setDescription(JSONParserUtil.getString(json, "description"));
									reports.add(report);
								}
								model.addAttribute("transactions", reports);
							}
							return "Admin/NikkiChatFilter";
						}
						return "Admin/NikkiChatTransactions";
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/SettlementAccount")
	public String getSettlementAccount(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) throws JSONException {
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					return "Admin/SettlementAccount";
				}
			}
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/SettlementAccount")
	public String getSettlementAccount(@ModelAttribute TransactionFilter dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) throws JSONException {
		try {
			dto.setStartDate(ConvertUtil.decodeBase64String(dto.getStartDate()));
			dto.setEndDate(ConvertUtil.decodeBase64String(dto.getEndDate()));

			String sessionCheck = (String) session.getAttribute("adminSessionId");
			if (sessionCheck != null) {
				String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
				if (authority != null) {
					if (authority.contains(Authorities.ADMINISTRATOR)
							&& authority.contains(Authorities.AUTHENTICATED)) {
						model.addAttribute(ModelMapKey.MESSAGE,
								"Settlement Transactions From " + dto.getStartDate() + " to " + dto.getEndDate());
						AllTransactionRequest transRequest = new AllTransactionRequest();
						transRequest.setStartDate(dto.getStartDate());
						transRequest.setEndDate(dto.getEndDate());
						transRequest.setReportType(dto.getServiceType());
						transRequest.setSessionId((String) session.getAttribute("adminSessionId"));
						AllTransactionResponse allTransaction = appAdminApi.getSettlementTransactions(transRequest);
						if (allTransaction.isSuccess()) {
							List<TransactionReport> reports = new ArrayList<>();
							JSONArray reportArray = allTransaction.getJsonArray();
							if (reportArray != null) {
								for (int i = 0; i < reportArray.length(); i++) {
									JSONObject json = reportArray.getJSONObject(i);
									TransactionReport report = new TransactionReport();
									report.setAmount(JSONParserUtil.getDouble(json, "amount"));
									report.setDate(JSONParserUtil.getString(json, "date"));
									report.setService(JSONParserUtil.getString(json, "service"));
									report.setDebit(JSONParserUtil.getBoolean(json, "debit"));
									report.setTransactionRefNo(JSONParserUtil.getString(json, "transactionRefNo"));
									report.setStatus(com.payqwikweb.model.web.Status
											.valueOf(JSONParserUtil.getString(json, "status")));
									report.setDescription(JSONParserUtil.getString(json, "description"));
									String receiptNo = JSONParserUtil.getString(json, "receiptNo");
									if (receiptNo != null) {
										report.setReceiptNo(receiptNo);
									} else {
										report.setReceiptNo("NA");
									}
									reports.add(report);
								}
								model.addAttribute("transactions", reports);
								model.addAttribute(ModelMapKey.MESSAGE, dto.getServiceType() + " reports from "
										+ dto.getStartDate() + "to " + dto.getEndDate());
							}
							return "Admin/SettlementTransactions";
						}
						return "Admin/SettlementAccount";
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			return "redirect:/Admin/Home";
		}

		return "redirect:/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/ListMerchant")
	public String getListMerchant(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) {

		String sessionCheck = (String) session.getAttribute("adminSessionId");

		if (sessionCheck != null) {
			String adminAuthority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (adminAuthority != null) {
				if (adminAuthority.contains(Authorities.ADMINISTRATOR)
						&& adminAuthority.contains(Authorities.AUTHENTICATED)) {
					AllUserRequest userRequest = new AllUserRequest();

					userRequest.setPage(0);
					userRequest.setSize(100000);
					userRequest.setSessionId((String) session.getAttribute("adminSessionId"));
					userRequest.setStatus(UserStatus.ALL);
					AllUserResponse userResponse = appAdminApi.getAllMerchants(userRequest);
					JSONArray data = userResponse.getJsonArray();
					List<AdminUserDetails> userList = new ArrayList<AdminUserDetails>();
					try {
						for (int i = 0; i < data.length(); i++) {
							AdminUserDetails list = new AdminUserDetails();
							JSONObject json = data.getJSONObject(i);
							list.setContactNo(JSONParserUtil.getString(json, "contactNo"));
							list.setDateOfAccountCreation(JSONParserUtil.getString(json, "dateOfRegistration"));
							list.setEmail(JSONParserUtil.getString(json, "email"));
							list.setFirstName(JSONParserUtil.getString(json, "name"));
							list.setImage(UrlMetadatas.WEBURL + JSONParserUtil.getString(json, "image"));
							userList.add(list);
						}

						model.put("userlist", userList);
					} catch (JSONException e) {
						e.printStackTrace();
					}
					return "Admin/ListMerchant";
				}
			}
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/AddMerchant")
	public String getAddMerchant(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) {

		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null)
			return "/Admin/AddMerchant";
		return "/Admin/Login";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/SendNotification")
	public String sendNotification(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model,@ModelAttribute("msg")String msg) {
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if(msg!=null)
		model.addAttribute("gcmMsg", msg);
		System.out.println();
		if (sessionCheck != null)
			return "/Admin/SendNotification";
		return "/Admin/Login";
	}

	// new method for gcm

	@RequestMapping(method = RequestMethod.POST, value = "/SendNotification")
	public String processNotification(@ModelAttribute GCMRequest dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) {
		int pageNo = 0;
		int size = 1000;
		String status = "";
		String[] parts = new String[5];
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (!CommonValidation.isNull(sessionCheck)) {
			GCMError error = gcmValidation.checkError(dto);
			if (error.isValid()) {
				if (dto.isImageGCM()) {
					String rootDirectory = request.getSession().getServletContext().getRealPath("/");
					status = saveLogo(rootDirectory, dto.getGcmImage());
					parts = status.split("\\|");
				}
				if (dto.getNotificationType().equalsIgnoreCase("SingleUser")) {
					GCMResponse gcmResponse = appAdminApi.getGCMIdByUserName(dto.getUserName(), sessionCheck);
					if (gcmResponse.isSuccess() && gcmResponse.getGcmList().size() > 0) {
						NotificationDTO notificationDTO = new NotificationDTO();
						notificationDTO.setImageGCM(dto.isImageGCM());
						notificationDTO.setTitle(dto.getTitle());
						notificationDTO.setMessage(dto.getMessage());
						if (dto.isImageGCM()) {
							notificationDTO.setImage(UrlMetadatas.WEBURL + parts[1]);
						}
						notificationDTO.setRegsitrationIds(gcmResponse.getGcmList());
						notificationApi.sendNotification(notificationDTO);
					}
				} else {
					CronNotificationDTO notificationDTO = new CronNotificationDTO();
					notificationDTO.setSessionId(sessionCheck);
					notificationDTO.setImageGCM(dto.isImageGCM());
					notificationDTO.setTitle(dto.getTitle());
					notificationDTO.setMessage(dto.getMessage());
					if (dto.isImageGCM()) {
						notificationDTO.setImage(UrlMetadatas.WEBURL + parts[1]);
					}
					superAdminApi.saveGCMIds(notificationDTO);
				}
				model.addAttribute(ModelMapKey.MESSAGE, "Notification Sent Successfully.");
				return "redirect:/Admin/SendNotification";
			} else {
				model.addAttribute(ModelMapKey.ERROR, error.getMessage());
				return "redirect:/Admin/SendNotification";
			}
		} else {
			return "/Admin/Login";
		}
	}

	private String saveLogo(String rootDirectory, MultipartFile image) {
		boolean saved = false;
		String contentType = image.getContentType();
		String[] fileExtension = contentType.split("/");
		String filePath = null;
		if (fileExtension[1].equals("png") || fileExtension[1].equals("jpg") || fileExtension[1].equals("jpeg")) {
			String fileName = String.valueOf(System.currentTimeMillis());
			File dirs = new File(rootDirectory + "/resources/gcm/" + fileName + "." + fileExtension[1]);
			dirs.mkdirs();
			try {
				image.transferTo(dirs);
				filePath = "/resources/gcm/" + fileName + "." + fileExtension[1];
				saved = true;
				return saved + "|" + filePath;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return saved + "|" + filePath;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/GeneratePromoCode")
	public String getServiceTypes(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model,@ModelAttribute("pId") String pId) {
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String adminAuthority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (adminAuthority != null) {
				if (adminAuthority.contains(Authorities.ADMINISTRATOR)
						&& adminAuthority.contains(Authorities.AUTHENTICATED)) {
					PromoCodeRequest promoReq = new PromoCodeRequest();
					if (pId != null && !pId.isEmpty()) {
						 promoReq=appAdminApi.getPromoCode(pId,sessionCheck);
						 JsonElement element= getJsonElement(promoReq.getServiceList());
						 model.addAttribute("selectedService", element);
					}
					model.addAttribute("promoCodeId", pId);
					model.addAttribute("promoCodeRequest", promoReq);
					return "Admin/GeneratePromoCode2";
				}
			}
		}
		return "redirect:/Admin/Home";
	}

	private JsonElement getJsonElement(List<CommissionDTO> list) {
		try {
			Gson gson = new Gson();
			JsonElement element = gson.toJsonTree(list, new TypeToken<List<CommissionDTO>>() {}.getType());
			return element;
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return null;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/GetServiceType")
	public ResponseEntity<ServiceTypeResponse> generatePromoCode(HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) {
		ServiceTypeResponse servicesList = appAdminApi.getServiceType();
		model.addAttribute("promoCodeRequest", new PromoCodeRequest());
		return new ResponseEntity<ServiceTypeResponse>(servicesList, HttpStatus.OK);
	}

	/*
	 * @RequestMapping(method = RequestMethod.GET, value = "/GeneratePromoCode")
	 * public String generatePromoCode(HttpServletRequest request,
	 * HttpServletResponse response, HttpSession session, ModelMap model) {
	 * String sessionCheck = (String) session.getAttribute("adminSessionId"); if
	 * (sessionCheck != null) { ServiceTypeResponse servicesList =
	 * appAdminApi.getServiceType(); if (servicesList != null) { if
	 * (servicesList.isSuccess()) { model.addAttribute("services",
	 * servicesList.getServiceDTOs()); } }
	 * model.addAttribute("promoCodeRequest", new PromoCodeRequest()); return
	 * "Admin/GeneratePromoCode2"; } return "redirect:/Admin/Home"; }
	 */

	@RequestMapping(method = RequestMethod.POST, value = "/GetServices")
	public ResponseEntity<ServiceTypeResponse> getservices(HttpServletRequest request,
			@RequestBody PromoCodeRequest promoCode, HttpServletResponse response, HttpSession session,
			ModelMap model) {
		ServiceTypeResponse servicesList = appAdminApi.getServicesById(promoCode);
		if (servicesList != null) {
			if (servicesList.isSuccess()) {
				servicesList.setServiceList(servicesList.getServicesDTOs());
				model.addAttribute("services", servicesList.getServiceDTOs());
			}
		}
		// model.addAttribute("promoCodeRequest", new PromoCodeRequest());
		return new ResponseEntity<ServiceTypeResponse>(servicesList, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/GeneratePromoCode")
	public String generatePromoCodeFrom(@ModelAttribute("promoCodeRequest") PromoCodeRequest promoCode,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model) {
		String sessionId = (String) session.getAttribute("adminSessionId");
		String startDateTime = promoCode.getStartDate();
		String endDateTime = promoCode.getEndDate();
		promoCode.setStartDate(startDateTime);
		promoCode.setEndDate(endDateTime);
		if (sessionId != null) {
			PromoCodeError error = promoCodeValidation.checkRequest(promoCode);
			if (error.isValid()) {
				promoCode.setSessionId(sessionId);
				PromoCodeResponse resp = promoCodeApi.generateRequest(promoCode);
				if (resp.getCode().equalsIgnoreCase("F03") || resp.getCode().equalsIgnoreCase("F05")) {
					return "/Admin/Login";
				}
				if(resp.isSuccess()){
					return "redirect:/Admin/PromoCodeList";
				}
				model.addAttribute("resp", resp);
				model.addAttribute("respCode", resp.getCode());
			}
			model.addAttribute("error", error);
			return "Admin/GeneratePromoCode2";
		}
		return "/Admin/Login";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/PromoCodeList")
	public String promoCodeList(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model, SessionDTO dto) throws JSONException {
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String adminAuthority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (adminAuthority != null) {
				if (adminAuthority.contains(Authorities.ADMINISTRATOR)
						&& adminAuthority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionCheck);
					PromoCodeResponse resp = promoCodeApi.listPromo(dto);
					JSONArray arr = (JSONArray) resp.getDetails();
					List<PromoCodeRequest> list = new ArrayList<PromoCodeRequest>();
					if (arr != null && arr.length() > 0) {
						for (int i = 0; i < arr.length(); i++) {
							PromoCodeRequest re = new PromoCodeRequest();
							JSONObject jsonobject = arr.getJSONObject(i);
							re.setPromoCodeId(jsonobject.getString("promoCodeId"));
							re.setPromoCode(jsonobject.getString("promoCode"));
							re.setEndDate(jsonobject.getString("endDate"));
							re.setStartDate(jsonobject.getString("startDate"));
							re.setTerms(jsonobject.getString("terms"));
							re.setDescription(jsonobject.getString("description"));
							re.setFixed(jsonobject.getBoolean("fixed"));
							re.setValue(jsonobject.getDouble("value"));
							re.setCashBackValue(Double.toString((jsonobject.getDouble("cashBackValue"))));
							list.add(re);
						}
					}
					model.addAttribute("respMsg", session.getAttribute("respMsg"));
					model.addAttribute("respCode", session.getAttribute("respCode"));
					model.addAttribute("errorMsg", session.getAttribute("errorMsg"));
					session.setAttribute("respMsg", "");
					session.setAttribute("respCode", "");
					session.setAttribute("errorMsg", "");
					model.addAttribute("list", list);
					return "/Admin/PromoCodeList";
				}
			}
		}
		return "/Admin/Login";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/UpdateLimit")
	public String getListAccountType(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model, SessionDTO dto) throws JSONException {

		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String adminAuthority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (adminAuthority != null) {
				if (adminAuthority.contains(Authorities.ADMINISTRATOR)
						&& adminAuthority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionCheck);
					AccountTypeResponse resp = appAdminApi.getListAccountType(dto);
					JSONArray arr = (JSONArray) resp.getJsonArray();
					List<AccountTypeResponse> list = new ArrayList<AccountTypeResponse>();
					for (int i = 0; i < arr.length(); i++) {
						AccountTypeResponse re = new AccountTypeResponse();
						JSONObject jsonobject = arr.getJSONObject(i);
						re.setName(jsonobject.getString("name"));
						re.setCode(jsonobject.getString("code"));
						re.setDescription(jsonobject.getString("description"));
						re.setTransactionLimit(jsonobject.getInt("transactionLimit"));
						re.setMonthlyLimit(jsonobject.getDouble("monthlyLimit"));
						re.setDailyLimit(jsonobject.getDouble("dailyLimit"));
						re.setBalanceLimit(jsonobject.getDouble("balanceLimit"));
						list.add(re);
					}
					model.addAttribute("list", list);
					return "/Admin/UpdateLimit";
				}
			}
		}
		return "/Admin/Login";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/EditLimit")
	public String editLimit(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			SessionDTO dto, ModelMap model) throws JSONException {
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String adminAuthority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (adminAuthority != null) {
				if (adminAuthority.contains(Authorities.ADMINISTRATOR)
						&& adminAuthority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionCheck);
					AccountTypeRequest merchantRequest = new AccountTypeRequest();
					model.addAttribute("accounts", merchantRequest);
					return "Admin/EditLimit";
				}
			}
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(value = "/EditLimit", method = RequestMethod.POST)
	public String updateMovie(@ModelAttribute("accounts") AccountTypeRequest dto, HttpServletRequest request,
			HttpSession session, ModelMap model, HttpServletResponse response) throws JSONException {
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String adminAuthority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (adminAuthority != null) {
				if (adminAuthority.contains(Authorities.ADMINISTRATOR)
						&& adminAuthority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionCheck);
					AccountTypeResponse resp = appAdminApi.updateAccountType(dto);
					JSONArray arr = (JSONArray) resp.getJsonArray();
					List<AccountTypeResponse> list = new ArrayList<AccountTypeResponse>();
					for (int i = 0; i < arr.length(); i++) {
						AccountTypeResponse re = new AccountTypeResponse();
						JSONObject jsonobject = arr.getJSONObject(i);
						re.setName(jsonobject.getString("name"));
						re.setCode(jsonobject.getString("code"));
						re.setDescription(jsonobject.getString("description"));
						re.setTransactionLimit(jsonobject.getInt("transactionLimit"));
						re.setMonthlyLimit(jsonobject.getDouble("monthlyLimit"));
						re.setDailyLimit(jsonobject.getDouble("dailyLimit"));
						re.setBalanceLimit(jsonobject.getDouble("balanceLimit"));
						list.add(re);
					}
					model.addAttribute("list", list);
					return "redirect:/Admin/UpdateLimit";
				}
			}
		}
		return "redirect:/";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/SendPromotionalSMS")
	public String SendPromotionalSMS(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) {

		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null)
			return "/Admin/SendPromotionalSMS";
		return "/Admin/Login";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/Logout")
	public String userLogout(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap modelMap) {
		String adminSessionId = (String) session.getAttribute("adminSessionId");
		LogoutRequest logout = new LogoutRequest();
		logout.setSessionId(adminSessionId);
		if (adminSessionId != null && adminSessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(adminSessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					LogoutResponse resp = logoutApi.logout(logout, Role.ADMIN);
					session.invalidate();
					return "redirect:/Admin/Home";
				}
			}
		}
		return "Admin/Login";

	}

	@RequestMapping(value = "/Version")
	public String versiion(HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model)
			throws JSONException {
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			SessionDTO dto = new SessionDTO();
			dto.setSessionId(sessionCheck);
			VersionResponse resp = versionApi.listVersion(dto);
			JSONArray arr = resp.getArrayDetails();
			ArrayList<VersionDTO> list = new ArrayList<>();
			for (int i = 0; i < arr.length(); i++) {
				VersionDTO obj = new VersionDTO();
				JSONObject singleV = arr.getJSONObject(i);
				DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");
				long milliSeconds = Long.parseLong(singleV.getString("created"));
				Calendar calendar = Calendar.getInstance();
				calendar.setTimeInMillis(milliSeconds);
				obj.setCreated(formatter.format(calendar.getTime()));
				obj.setVersion("V." + singleV.get("versionCode") + "." + singleV.getString("subversionCode"));
				obj.setStatus(singleV.getString("status"));
				list.add(obj);
			}
			model.addAttribute("userList", list);
			return "Admin/Version";
		}
		return "Admin/Login";
	}

	@RequestMapping(value = { "/Merchant" }, method = RequestMethod.GET)
	public String getMerchantFormPage(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			Model model) {
		String sessionId = (String) session.getAttribute("adminSessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					MRegistrationRequest merchantRequest = new MRegistrationRequest();
					model.addAttribute("addMerchant", merchantRequest);
					return "Admin/AddMerchant";
				}
			}

		}
		return "Admin/Login";
	}

	@RequestMapping(value = { "/Merchant" }, method = RequestMethod.POST)
	public String processMerchantRegistration(@ModelAttribute("addMerchant") MRegistrationRequest merchant,
			@RequestParam(value = "mlogo", required = false) MultipartFile image, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) {
		String sessionId = (String) session.getAttribute("adminSessionId");

		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					merchant.setSessionId(sessionId);
					RegisterError error = registerValidation.checkMerchantRegistrationError(merchant);
					if (error.isValid()) {
						if (image.getSize() > 0) {
							boolean validImage = isValidImage(image);
							if (validImage) {
								String rootDirectory = request.getSession().getServletContext().getRealPath("/");
								String status = saveLogo(rootDirectory, image);
								if (status.contains("|")) {
									String saved[] = status.split("\\|");
									if (saved[0].contains("true")) {
										merchant.setImage(saved[1]);
									}
								}
							}
						} else {
							error.setMiddleName("Choose Image");
						}
						AddMerchantResponse resp = appAdminApi.addMerchant(merchant);
						if (resp.isSuccess()) {
							model.addAttribute(ModelMapKey.MESSAGE, resp.getDetails());
							return "Admin/AddMerchant";
						} else {
							model.addAttribute(ModelMapKey.MESSAGE, resp.getMessage());
							return "Admin/AddMerchant";
						}
					} else {
						model.addAttribute(ModelMapKey.MESSAGE, error);
					}
					return "Admin/AddMerchant";
				}
			}
		}
		return "Admin/Login";
	}

	@RequestMapping(value = { "/Donatee" }, method = RequestMethod.GET)
	public String addDonateeFromAdmin(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			Model model) {
		String sessionId = (String) session.getAttribute("adminSessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					DRegistrationRequest donateeRequest = new DRegistrationRequest();
					model.addAttribute("addDonatee", donateeRequest);
					return "Admin/Adddonatee";
				}
			}

		}
		return "Admin/Login";
	}

	//
	// @RequestMapping(value = { "/AgentCreditlist" }, method =
	// RequestMethod.GET)
	// public String processAgentCreditlistGet(@ModelAttribute PagingDTO dto,
	// HttpServletRequest request,
	// HttpServletResponse response, HttpSession session, ModelMap model) {
	// String sessionId = (String) session.getAttribute("adminSessionId");
	// if (sessionId != null && sessionId.length() != 0) {
	// String authority = authenticationApi.getAuthorityFromSession(sessionId,
	// Role.USER);
	// if (authority != null) {
	// if (authority.contains(Authorities.ADMINISTRATOR) &&
	// authority.contains(Authorities.AUTHENTICATED)) {
	//
	// return "Admin/ListAgent";
	// }
	// }
	//
	// }
	// return "Admin/Login";
	// }

	@RequestMapping(value = { "/AgentCreditlist" }, method = RequestMethod.GET)
	public String processAgentCreditlist(@ModelAttribute AllUserRequest dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) {
		UserListDTO result = new UserListDTO();
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {

					dto.setPage(100);
					dto.setSize(1);
					dto.setSessionId(sessionCheck);
					AllUserResponse userResponse = appAdminApi.getAllCreditAgent(dto);
					JSONArray data = userResponse.getJsonArray();
					List<AdminUserDetails> userList = new ArrayList<>();
					try {
						if (data != null) {
							for (int i = 0; i < data.length(); i++) {
								AdminUserDetails list = new AdminUserDetails();
								JSONObject jsonobject = data.getJSONObject(i);
								String userType = JSONParserUtil.getString(jsonobject, "userType");
								// String
								// pin=jsonobject.getJSONObject("userDetail").getJSONObject("location").getString("pinCode");
								// String
								// circle=jsonobject.getJSONObject("userDetail").getJSONObject("location").getString("circleName");
								JSONObject userDetails = JSONParserUtil.getObject(jsonobject, "userDetail");
								if (userType.equalsIgnoreCase("User")) {
									JSONObject obj = (JSONObject) jsonobject.getJSONObject("userDetail");
									// System.out.println(obj);
									if (obj.isNull("location")) {

										list.setAuthority(jsonobject.getString("authority"));
										list.setMobileStatus(jsonobject.getString("mobileStatus"));
										list.setFirstName(
												jsonobject.getJSONObject("userDetail").getString("firstName"));
										list.setLastname(jsonobject.getJSONObject("userDetail").getString("lastName"));
										list.setUsername(jsonobject.getJSONObject("userDetail").getString("name"));
										list.setAccountNumber(
												jsonobject.getJSONObject("accountDetail").getString("accountNumber"));
										list.setGender(jsonobject.getJSONObject("userDetail").getString("gender"));
										list.setDob(jsonobject.getJSONObject("userDetail").getString("dateOfBirth"));
										list.setVijayaAccountNumber(jsonobject.getJSONObject("accountDetail")
												.getString("vijayaAccountNumber"));
										list.setKycNonkyc(jsonobject.getJSONObject("accountDetail")
												.getJSONObject("accountType").getString("name"));
										list.setEmail(jsonobject.getJSONObject("userDetail").getString("email"));
										list.setBalance(jsonobject.getJSONObject("accountDetail").getDouble("balance"));
										list.setContactNo(
												jsonobject.getJSONObject("userDetail").getString("contactNo"));
										list.setEmail(jsonobject.getJSONObject("userDetail").getString("email"));
										list.setMobileToken(jsonobject.getString("mobileToken"));
										list.setPoints(jsonobject.getJSONObject("accountDetail").getLong("points"));
										DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");
										long milliSeconds = Long.parseLong(jsonobject.getString("created"));
										Calendar calendar = Calendar.getInstance();
										calendar.setTimeInMillis(milliSeconds);
										list.setDateOfAccountCreation(formatter.format(calendar.getTime()));
										// LogCat.print("User name " +
										// list.getFirstName());
										userList.add(list);
										model.addAttribute("userlist", userList);
									}

								}
							}
							result.setSuccess(true);
							result.setFirstPage(userResponse.isFirstPage());
							result.setLastPage(userResponse.isLastPage());
							result.setJsonArray(userList);
							result.setNumberOfElements(userResponse.getNumberOfElements());
							result.setSize(userResponse.getSize());
							result.setTotalPages(userResponse.getTotalPages());
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
					return "Admin/ListAgent";
				}
			}

		}
		return "Admin/Login";
	}

	@RequestMapping(value = "/Agent/ChangePasswordWithMobile", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<RegistrationResponse> changePasswordWithOTP(@RequestBody ChangePasswordRequest dto,
			HttpServletRequest request, HttpSession session, HttpServletResponse response) {
		RegistrationResponse result = new RegistrationResponse();
		try {
			String sessionId = (String) session.getAttribute("adminSessionId");
			if (sessionId != null) {
				System.out.print("username" + dto.getUsername());
				System.out.print("password" + dto.getNewPassword());
				System.out.print("confirm password ::" + dto.getConfirmPassword());
				result = appAdminApi.changePassWithMobNo(dto);
			} else {
				result.setCode(ResponseStatus.INVALID_SESSION.getValue());
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<RegistrationResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = { "/Donatee" }, method = RequestMethod.POST)
	public String processDonateeRegistration(@ModelAttribute("addDonatee") DRegistrationRequest donatee,
			@RequestParam(value = "dlogo", required = false) MultipartFile image, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) {

		String sessionId = (String) session.getAttribute("adminSessionId");

		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					donatee.setSessionId(sessionId);
					RegisterError error = registerValidation.checkDonateeRegistrationError(donatee);
					if (error.isValid()) {
						if (image.getSize() > 0) {
							boolean validImage = isValidImage(image);
							if (validImage) {
								String rootDirectory = request.getSession().getServletContext().getRealPath("/");
								String status = saveLogo(rootDirectory, image);
								if (status.contains("|")) {
									String saved[] = status.split("\\|");
									if (saved[0].contains("true")) {
										donatee.setImage(saved[1]);
									}
								}
							}
						} else {
							error.setMiddleName("Choose Image");
						}
						AddDonateeResponse resp = appAdminApi.addDonatee(donatee);
						if (resp.isSuccess()) {
							model.addAttribute(ModelMapKey.MESSAGE, resp.getMessage());
							return "Admin/Adddonatee";
						} else {
							model.addAttribute(ModelMapKey.MESSAGE, resp.getMessage());
							return "Admin/Adddonatee";
						}
					} else {
						model.addAttribute(ModelMapKey.MESSAGE, error);
					}
					return "Admin/Adddonatee";
				}
			}
		}
		return "Admin/Login";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/ListAgent")
	public String getListAgent(@ModelAttribute PagingDTO dto, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, ModelMap model) {
		UserListDTO result = new UserListDTO();
		String sessionCheck = (String) session.getAttribute("adminSessionId");

		if (sessionCheck != null) {
			String adminAuthority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.AGENT);
			if (adminAuthority != null) {
				if (adminAuthority.contains(Authorities.ADMINISTRATOR)
						&& adminAuthority.contains(Authorities.AUTHENTICATED)) {
					AllUserRequest userRequest = new AllUserRequest();
					userRequest.setPage(0);
					userRequest.setSize(1000);
					userRequest.setSessionId((String) session.getAttribute("adminSessionId"));
					userRequest.setStatus(UserStatus.ALL);
					AllUserResponse userResponse = appAdminApi.getAllAgents(userRequest);
					JSONArray data = userResponse.getJsonArray();
					List<AdminUserDetails> userList = new ArrayList<>();
					try {
						if (data != null) {
							for (int i = 0; i < data.length(); i++) {
								AdminUserDetails list = new AdminUserDetails();
								JSONObject jsonobject = data.getJSONObject(i);
								String userType = JSONParserUtil.getString(jsonobject, "userType");
								// circle=jsonobject.getJSONObject("userDetail").getJSONObject("location").getString("circleName");
								JSONObject userDetails = JSONParserUtil.getObject(jsonobject, "userDetail");
								if (userType.equalsIgnoreCase("Agent")) {
									JSONObject obj = (JSONObject) jsonobject.getJSONObject("userDetail");
									if (obj.isNull("location")) {
										list.setAuthority(jsonobject.getString("authority"));
										list.setMobileStatus(jsonobject.getString("mobileStatus"));
										list.setUserType(jsonobject.getString("userType"));
										list.setFirstName(
												jsonobject.getJSONObject("userDetail").getString("firstName"));
										list.setLastname(jsonobject.getJSONObject("userDetail").getString("lastName"));
										list.setUsername(jsonobject.getJSONObject("userDetail").getString("name"));
										list.setAccountNumber(
												jsonobject.getJSONObject("accountDetail").getString("accountNumber"));
										list.setVijayaAccountNumber(jsonobject.getJSONObject("accountDetail")
												.getString("vijayaAccountNumber"));
										list.setKycNonkyc(jsonobject.getJSONObject("accountDetail")
												.getJSONObject("accountType").getString("name"));
										list.setEmail(jsonobject.getJSONObject("userDetail").getString("email"));
										list.setBalance(jsonobject.getJSONObject("accountDetail").getDouble("balance"));
										list.setContactNo(jsonobject.getString("username"));
										list.setEmail(jsonobject.getJSONObject("userDetail").getString("email"));
										list.setMobileToken(jsonobject.getString("mobileToken"));
										list.setPoints(jsonobject.getJSONObject("accountDetail").getLong("points"));
										list.setAgentBankName(
												jsonobject.getJSONObject("agentDetails").getString("bankName"));
										list.setAgentBranchName(
												jsonobject.getJSONObject("agentDetails").getString("branchName"));
										list.setAgentBankAccountName(
												jsonobject.getJSONObject("agentDetails").getString("bankAccountName"));
										list.setAgentBankAccountNo(
												jsonobject.getJSONObject("agentDetails").getString("bankAccountNo"));
										list.setAgentIfscCode(
												jsonobject.getJSONObject("agentDetails").getString("ifscCode"));
										list.setAgentName(
												jsonobject.getJSONObject("agentDetails").getString("agentName"));
										list.setAgentPanCardNo(
												jsonobject.getJSONObject("agentDetails").getString("panCardNo"));
										list.setImage(jsonobject.getJSONObject("agentDetails").getString("image"));
										list.setImageContent(
												jsonobject.getJSONObject("agentDetails").getString("imageContent"));
                                        list.setAgentId(String.valueOf(jsonobject.getJSONObject("agentDetails").getLong("agentId")));
										DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");
										long milliSeconds = Long.parseLong(jsonobject.getString("created"));
										Calendar calendar = Calendar.getInstance();
										calendar.setTimeInMillis(milliSeconds);
										list.setDateOfAccountCreation(formatter.format(calendar.getTime()));
										userList.add(list);
										model.addAttribute("agentList", userList);
									} else {
										list.setPinCode(jsonobject.getJSONObject("userDetail").getJSONObject("location")
												.getString("pinCode"));
										list.setCircleName(jsonobject.getJSONObject("userDetail")
												.getJSONObject("location").getString("circleName"));
										list.setAuthority(jsonobject.getString("authority"));
										list.setMobileStatus(jsonobject.getString("mobileStatus"));
										list.setUserType(jsonobject.getString("userType"));
										list.setFirstName(
												jsonobject.getJSONObject("userDetail").getString("firstName"));
										list.setLastname(jsonobject.getJSONObject("userDetail").getString("lastName"));
										list.setUsername(jsonobject.getJSONObject("userDetail").getString("name"));
										list.setAccountNumber(
												jsonobject.getJSONObject("accountDetail").getString("accountNumber"));
										list.setVijayaAccountNumber(jsonobject.getJSONObject("accountDetail")
												.getString("vijayaAccountNumber"));
										list.setKycNonkyc(jsonobject.getJSONObject("accountDetail")
												.getJSONObject("accountType").getString("name"));
										list.setEmail(jsonobject.getJSONObject("userDetail").getString("email"));
										list.setBalance(jsonobject.getJSONObject("accountDetail").getDouble("balance"));
										list.setContactNo(jsonobject.getString("username"));
										list.setEmail(jsonobject.getJSONObject("userDetail").getString("email"));
										list.setMobileToken(jsonobject.getString("mobileToken"));
										list.setPoints(jsonobject.getJSONObject("accountDetail").getLong("points"));
										list.setAgentBankName(
												jsonobject.getJSONObject("agentDetails").getString("bankName"));
										list.setAgentBranchName(
												jsonobject.getJSONObject("agentDetails").getString("branchName"));
										list.setAgentBankAccountName(
												jsonobject.getJSONObject("agentDetails").getString("bankAccountName"));
										list.setAgentBankAccountNo(
												jsonobject.getJSONObject("agentDetails").getString("bankAccountNo"));
										list.setAgentIfscCode(
												jsonobject.getJSONObject("agentDetails").getString("ifscCode"));
										list.setAgentName(
												jsonobject.getJSONObject("agentDetails").getString("agentName"));
										list.setAgentPanCardNo(
												jsonobject.getJSONObject("agentDetails").getString("panCardNo"));
										list.setImage(jsonobject.getJSONObject("agentDetails").getString("image"));
										list.setImageContent(
												jsonobject.getJSONObject("agentDetails").getString("imageContent"));

										DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");
										long milliSeconds = Long.parseLong(jsonobject.getString("created"));
										Calendar calendar = Calendar.getInstance();
										calendar.setTimeInMillis(milliSeconds);
										list.setDateOfAccountCreation(formatter.format(calendar.getTime()));
										userList.add(list);
										model.addAttribute("agentList", userList);
									}

								}
							}
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
					return "Admin/ListAgent";
				}
			}

		}
		return "Admin/Login";
	}

	private boolean isValidImage(MultipartFile file) {
		long length = 2 * 1024 * 1024;
		File imageFile = null;
		try {
			imageFile = convert(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
		boolean isValid = false;
		if (file.getContentType().contains("image")) {
			isValid = true;
		}
		if (file.getSize() <= length) {
			try {
				Image image = ImageIO.read(imageFile);

				if (image == null) {
				} else {
					isValid = true;
				}
			} catch (IOException ex) {
			}
		}
		return isValid;
	}

	private File convert(MultipartFile file) throws IOException {
		File convFile = new File(file.getOriginalFilename());
		convFile.createNewFile();
		FileOutputStream fos = new FileOutputStream(convFile);
		fos.write(file.getBytes());
		fos.close();
		return convFile;
	}

	private String saveImage(String rootDirectory, MultipartFile image) {
		boolean isSaved = false;
		String filePath = "";
		String seperator = "|";
		String[] format = image.getContentType().split("/");
		String fileFormat = "";
		String[] formats = { "jpeg", "png", "jpg" };
		for (String fmt : formats) {
			if (format[1].equals(fmt)) {
				fileFormat = fmt;
			}
		}
		if (!CommonValidation.isNull(fileFormat)) {
			try {
				String fileName = String.valueOf(System.currentTimeMillis());
				File dirs = new File(rootDirectory + "/resources/profileImages/" + fileName + "." + fileFormat);
				dirs.mkdirs();
				image.transferTo(dirs);
				filePath = "/resources/profileImages/" + fileName + "." + fileFormat;
				isSaved = true;
				return isSaved + seperator + filePath;
			} catch (Exception ex) {
				return "Exception Occurred";
			}
		}
		return "Exception Occurred";
	}

	String debitTransactions(String transactionId) {
		char lastChar = transactionId.charAt(transactionId.length() - 1);
		return lastChar + "";
	}

	String debitLastTwoChar(String transactionId) {
		char lastChar = transactionId.charAt(transactionId.length() - 2);
		return lastChar + "";
	}

	String removeLastChar(String transactionId) {
		String lastChar = transactionId.substring(0, transactionId.length() - 1);
		return lastChar;
	}

	String removeDSCSCC(String transactionId) {
		char lastChar = transactionId.charAt(transactionId.length() - 1);
		char secLastChar = transactionId.charAt(transactionId.length() - 2);
		return secLastChar + lastChar + "";
	}

	String findTransactionId(String transaction) {
		char last = transaction.charAt(transaction.length() - 1);
		char secLast;
		if (last == 68) {
			String s = transaction.substring(0, transaction.length() - 1);
			return s;
		}
		return null;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/ShowAnalyticss")
	public String getAnanlytics(@ModelAttribute DateDTO dto, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, Model model) {

		try {
			dto.setFromDate(ConvertUtil.decodeBase64String(dto.getFromDate()));
			dto.setToDate(ConvertUtil.decodeBase64String(dto.getToDate()));
		} catch (Exception e) {

		}

		List<AnalyticsDemo> list1 = new ArrayList<>();
		String adminSessionId = (String) session.getAttribute("adminSessionId");
		dto.setSessionId(adminSessionId);
		session.setAttribute("toDate", dto.getToDate());
		session.setAttribute("fromDate", dto.getFromDate());
		if (adminSessionId != null && adminSessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(adminSessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					List<AnalyticsResponse> list = appAdminApi.getAnalytics(dto);
					List<AnalyticsResponse> listCredit = appAdminApi.getAnalyticsCredit(dto);
					long countLoadMoney = 0;

					model.addAttribute("Analytics", list);
					model.addAttribute("LoadMoney", listCredit);
					return "Admin/Analytics";
				}
			}
		}
		return "Admin/Login";

	}

	@RequestMapping(method = RequestMethod.GET, value = "/ShowAnalytics")
	public String getAnanlyticss(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			Model model) {
		String adminSessionId = (String) session.getAttribute("adminSessionId");
		if (adminSessionId != null && adminSessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(adminSessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					DateDTO dto = new DateDTO();
					dto.setFromDate("2016-12-02");
					dto.setToDate("2016-12-02");
					dto.setSessionId(adminSessionId);

					List<AnalyticsResponse> list = appAdminApi.getAnalytics(dto);
					List<AnalyticsResponse> listCredit = appAdminApi.getAnalyticsCredit(dto);
					model.addAttribute("Analytics", list);
					model.addAttribute("LoadMoney", listCredit);

					return "Admin/Analytics";
				}
			}
		}
		return "Admin/filter";

	}

	@RequestMapping(method = RequestMethod.POST, value = "/ShowUserListAnalytics", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> getAnanlyticss(@ModelAttribute DateDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) {
		List<UserAnalytics> list = null;
		ResponseDTO resp = new ResponseDTO();
		String adminSessionId = (String) session.getAttribute("adminSessionId");
		String toDate = (String) session.getAttribute("toDate");
		String fromDate = (String) session.getAttribute("fromDate");
		dto.setToDate(toDate);
		dto.setFromDate(fromDate);
		dto.setSessionId(adminSessionId);
		if (adminSessionId != null && adminSessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(adminSessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					list = appAdminApi.getAllUsersAnalytics(dto);
					resp.setInfo(list);
					return new ResponseEntity<>(resp, HttpStatus.OK);
				}
			}
		}
		return new ResponseEntity<>(resp, HttpStatus.OK);

	}

	@RequestMapping(method = RequestMethod.GET, value = "/ReconcileEBS")
	public String getReconcileReports(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
		String adminSessionId = (String) session.getAttribute("adminSessionId");
		if (adminSessionId != null && adminSessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(adminSessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {

					String url = request.getScheme() + "://49.204.86.246:8080/EBS/indexmain.php";
					return "redirect:" + url;
				}
			}
		}
		return "redirect:/Admin/Home";

	}

	@RequestMapping(method = RequestMethod.GET, value = "/ReconcileInstantPay")
	public String getReconcileReportsInstantPay(HttpServletRequest request, HttpServletResponse response,
			HttpSession session) {
		String adminSessionId = (String) session.getAttribute("adminSessionId");
		if (adminSessionId != null && adminSessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(adminSessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {

					String url = request.getScheme() + "http://49.204.86.246:8080/Ipay/indexmain.php";
					return "redirect:" + url;
				}
			}
		}
		return "redirect:/Admin/Home";

	}

	@RequestMapping(method = RequestMethod.GET, value = "/PartnerDetails")
	public String getPartnerInstalls(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) throws JSONException {
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {

					return "Admin/PartnerDetails";
				}
			}
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/PartnerDetails")
	public String getPartnerInstalls(@ModelAttribute PartnerDetailsDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) throws JSONException {
		try {

			String sessionCheck = (String) session.getAttribute("adminSessionId");
			if (sessionCheck != null) {
				String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
				if (authority != null) {
					if (authority.contains(Authorities.ADMINISTRATOR)
							&& authority.contains(Authorities.AUTHENTICATED)) {
						AllTransactionRequest transRequest = new AllTransactionRequest();
						// transRequest.setStartDate(dto.getStartDate());
						// transRequest.setEndDate(dto.getEndDate());
						transRequest.setPartnerName(dto.getPartnerName());
						transRequest.setSessionId((String) session.getAttribute("adminSessionId"));
						AllTransactionResponse allTransaction = appAdminApi.getPartnerDetails(transRequest);
						if (allTransaction.isSuccess()) {
							List<PartnerDetailsDTO> reports = new ArrayList<>();
							JSONArray reportArray = allTransaction.getJsonArray();
							if (reportArray != null) {
								for (int i = 0; i < reportArray.length(); i++) {
									JSONObject json = reportArray.getJSONObject(i);
									PartnerDetailsDTO report = new PartnerDetailsDTO();
									report.setImeiNo(JSONParserUtil.getString(json, "imei"));
									DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");
									long milliSeconds = Long.parseLong(JSONParserUtil.getString(json, "date"));
									Calendar calendar = Calendar.getInstance();
									calendar.setTimeInMillis(milliSeconds);
									report.setDate(formatter.format(calendar.getTime()));

									report.setModel(JSONParserUtil.getString(json, "model"));
									report.setDevice(JSONParserUtil.getString(json, "device"));
									reports.add(report);
								}
								model.addAttribute("installations", reports);
								model.addAttribute(ModelMapKey.MESSAGE,
										" Installation Report of " + dto.getPartnerName());
							}
							return "Admin/PartnerInstall";
						}
						return "Admin/PartnerDetails";
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			return "redirect:/Admin/Home";
		}

		return "redirect:/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/PartnerInstallFilter")
	public String getPartnerInstallFilter(@ModelAttribute PartnerDetailsDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) throws JSONException {
		try {

			String sessionCheck = (String) session.getAttribute("adminSessionId");
			if (sessionCheck != null) {
				String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
				if (authority != null) {
					if (authority.contains(Authorities.ADMINISTRATOR)
							&& authority.contains(Authorities.AUTHENTICATED)) {
						model.addAttribute(ModelMapKey.MESSAGE,
								"Partner Installation From " + dto.getFromDate() + " to " + dto.getEndDate());
						PartnerDetailsRequest transRequest = new PartnerDetailsRequest();
						transRequest.setFromDate(dto.getFromDate());
						transRequest.setEndDate(dto.getEndDate());
						transRequest.setPartnerName(dto.getPartnerName());
						transRequest.setSessionId((String) session.getAttribute("adminSessionId"));
						AllTransactionResponse allTransaction = appAdminApi.getPartnerDetailsFilter(transRequest);
						if (allTransaction.isSuccess()) {
							List<PartnerDetailsDTO> reports = new ArrayList<>();
							JSONArray reportArray = allTransaction.getJsonArray();
							if (reportArray != null) {
								for (int i = 0; i < reportArray.length(); i++) {
									JSONObject json = reportArray.getJSONObject(i);
									PartnerDetailsDTO report = new PartnerDetailsDTO();
									report.setImeiNo(JSONParserUtil.getString(json, "imei"));
									DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");
									long milliSeconds = Long.parseLong(JSONParserUtil.getString(json, "date"));
									Calendar calendar = Calendar.getInstance();
									calendar.setTimeInMillis(milliSeconds);
									report.setDate(formatter.format(calendar.getTime()));

									report.setModel(JSONParserUtil.getString(json, "model"));
									report.setDevice(JSONParserUtil.getString(json, "device"));
									reports.add(report);
								}
								model.addAttribute("installations", reports);
								// model.addAttribute(ModelMapKey.MESSAGE, "
								// Installation Report of
								// "+dto.getPartnerName());
							}
							return "Admin/PartnerInstall";
						}
						return "Admin/PartnerDetails";
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			return "redirect:/Admin/Home";
		}

		return "redirect:/Admin/Home";
	}

	@RequestMapping(value = "/refundEBS", method = RequestMethod.POST)
	public String refundStatusEBS(@ModelAttribute RefundStatusDTO dto, Model model, HttpServletRequest request,
			HttpServletResponse response, HttpSession session)
			throws JSONException, ParserConfigurationException, SAXException {
		System.err.println("tofrom " + dto.getFrom());
		System.err.println(" " + dto.getTo());
		try {
			dto.setFrom(ConvertUtil.decodeBase64String(dto.getFrom()));
			dto.setTo(ConvertUtil.decodeBase64String(dto.getTo()));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		List<RefundStatusDTO> list = new ArrayList<RefundStatusDTO>();

		TransactionReportResponse resp = loadMoneyApi.getRefundStatus(dto);
		JSONArray jarr = resp.getJsonArray();
		for (int i = 0; i < jarr.length(); i++) {
			JSONObject jobject = jarr.getJSONObject(i);
			String transactionRefNo = JSONParserUtil.getString(jobject, "transactionRefNo");
			String ActualTx = transactionRefNo.substring(0, transactionRefNo.length() - 1);
			String status = JSONParserUtil.getString(jobject, "status");
			EBSStatusResponseDTO ebsResp = loadMoneyApi.crossCheckEBSStatus(ActualTx);
			RefundStatusDTO responseEBS = new RefundStatusDTO();
			if (ebsResp != null) {
				if (ebsResp.getErrorMessage() == null) {
					responseEBS.setCode(ebsResp.getCode());
					responseEBS.setAmount(ebsResp.getAmount());
					responseEBS.setTransactionRefNo(ebsResp.getTransactionRefNo());
					responseEBS.setStatus(ebsResp.getStatus());
					responseEBS.setDescription(ebsResp.getDescription());
					responseEBS.setCreated(ebsResp.getCreated());
					list.add(responseEBS);
				} else {
				}
			}
		}
		model.addAttribute("EBSReport", list);
		return "Admin/ShowReconReport";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/refundEBS")
	public String getEBSRecon(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) throws JSONException {
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {

					return "Admin/EBSRecon";
				}
			}
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(value = "/Gci/{receiptNo}", method = RequestMethod.GET)
	public String getReceiptValue(ModelMap modelMap, @PathVariable String receiptNo, HttpSession session, Model model)
			throws JSONException {

		String sessionCheck = (String) session.getAttribute("adminSessionId");
		OrderStatusDTO req = new OrderStatusDTO();
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					SessionDTO sessionDTO = new SessionDTO();
					sessionDTO.setSessionId(sessionCheck);
					GciAuthDTO gciAuthDTO = userApi.getGciAuth(sessionDTO);
					if (gciAuthDTO.getSuccess()) {
						req.setToken(gciAuthDTO.getToken());
						req.setReceiptNo(receiptNo);
						List<OrderStatusResponse> response = gciServiceApi.getOrderStatus(req);
						model.addAttribute("statusList", response);

					}

				}
			}
			return "Admin/GciStatus";

		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/GetUserByLocation")
	public String getUserByLocation(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) throws JSONException {
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {

					return "Admin/FindUserByPincode";
				}
			}
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/GetUserListLocation")
	public String getListOfUser(@RequestParam String search, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, @ModelAttribute UserByLocationDTO dto, ModelMap model) throws JSONException {

		try {
			dto.setFromDate(ConvertUtil.decodeBase64String(dto.getFromDate()));
			dto.setEndDate(ConvertUtil.decodeBase64String(dto.getEndDate()));
			search = ConvertUtil.decodeBase64String(search);
		} catch (Exception e) {

		}

		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String adminAuthority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (adminAuthority != null) {
				if (adminAuthority.contains(Authorities.ADMINISTRATOR)
						&& adminAuthority.contains(Authorities.AUTHENTICATED)) {
					model.addAttribute(ModelMapKey.MESSAGE, "User For Location Code " + search + "from"
							+ dto.getFromDate() + " to " + dto.getEndDate());
					SingleUserRequest userRequest = new SingleUserRequest();
					userRequest.setSessionId(sessionCheck);
					userRequest.setLocationCode(search);
					userRequest.setFromDate(dto.getFromDate());
					userRequest.setToDate(dto.getEndDate());
					AllUserResponse userResponse = appAdminApi.getUserFromLocationCode(userRequest);
					if (userResponse != null) {
						List<UserByLocationDTO> reports = new ArrayList<>();
						JSONArray array = userResponse.getJsonArray();
						if (array.length() != 0) {
							for (int i = 0; i < array.length(); i++) {
								UserByLocationDTO listRequest = new UserByLocationDTO();
								JSONObject data = array.getJSONObject(i);
								String userType = data.getString("userType");
								if (userType.equalsIgnoreCase("User")
										&& (data.getString("pinCode")).equalsIgnoreCase(search)) {
									listRequest.setFirstName(data.getString("username"));
									listRequest.setMobile(data.getString("mobile"));
									listRequest.setEmail(data.getString("email"));
									listRequest.setPoints(Long.parseLong(data.getString("points")));
									listRequest.setBalance(Double.parseDouble(data.getString("balance")));
									listRequest.setCreated(data.getString("created"));
									listRequest.setGender(data.getString("gender"));
									listRequest.setDob(data.getString("dob"));
									listRequest.setAuthority(data.getString("authority"));
									listRequest.setAccountType(data.getString("accountType"));
									listRequest.setPinCode(data.getString("pinCode"));
									listRequest.setCircleName(data.getString("circleName"));
									listRequest.setVijayaBankAccount(data.getString("vijayaBankAccount"));
									reports.add(listRequest);

								} else {
									listRequest.setMsg(data.getString("msg"));
									model.addAttribute(ModelMapKey.MESSAGE, userRequest.getMsg());
								}
							}
						} else {
							userResponse.setMessage("No Users Found between date from " + dto.getFromDate() + "to"
									+ dto.getEndDate() + " for the location code " + search);
							model.addAttribute(ModelMapKey.MESSAGE, userResponse.getMessage());
							return "Admin/FindUserByPincode";
						}
						model.addAttribute("searchedlist", reports);
						return "Admin/ListOfUserByPincode";
					}
				}
			}
		}
		return "redirect:/Admin/Home";
	}

	// Flight Details Show in Backend

	@RequestMapping(method = RequestMethod.GET, value = "/Agent/FlightdeatilList")
	String getAgentFlightdeatilListInJSON(@ModelAttribute PagingDTO dto, HttpServletRequest request, ModelMap model,
			HttpSession session) {

		UserListDTO result = new UserListDTO();
		String sessionCheck = (String) session.getAttribute("adminSessionId");

		if (sessionCheck != null) {
			String adminAuthority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.AGENT);
			if (adminAuthority != null) {
				if (adminAuthority.contains(Authorities.ADMINISTRATOR)
						&& adminAuthority.contains(Authorities.AUTHENTICATED)) {
					AllUserRequest userRequest = new AllUserRequest();
					userRequest.setPage(0);
					userRequest.setSize(1000);
					userRequest.setSessionId((String) session.getAttribute("adminSessionId"));
					userRequest.setStatus(UserStatus.ALL);
					AllUserResponse userResponse = appAdminApi.getAllAgentFlightsDetail(userRequest);
					try {
						List<FlightTicketAdminDTO> userList = new ArrayList<>();
						if (userResponse.getCode().equals("S00")) {
							JSONObject obj = new JSONObject(userResponse.getResponse());
							JSONArray data = obj.getJSONArray("details");

							if (data != null) {

								for (int i = 0; i < data.length(); i++) {

									FlightTicketAdminDTO list = new FlightTicketAdminDTO();
									JSONObject userDetail = null;
									JSONObject account = null;
									JSONObject transaction = data.getJSONObject(i).getJSONObject("transaction");
									JSONObject user = data.getJSONObject(i).getJSONObject("user");
									if (user != null) {
										userDetail = user.getJSONObject("userDetail");
										list.setUserDetails(userDetail.getString("firstName") + " "
												+ userDetail.getString("lastName"));
										list.setUserName(user.getString("username"));
									}

									list.setBookingStatus(data.getJSONObject(i).getString("flightStatus"));
									double commissionAmount = (data.getJSONObject(i).getDouble("commissionAmt"));
									double totalCommissionAmount = commissionAmount - 200;
									list.setCommsissionAmount(BigDecimal.valueOf(totalCommissionAmount)
											.setScale(3, RoundingMode.HALF_UP).doubleValue());
									if (transaction != null) {
										list.setTransactionRefNo(transaction.getString("transactionRefNo"));
										list.setTransactionStatus(transaction.getString("status"));
										long dt = transaction.getLong("created");
										Date date = new Date(dt);
										String txnDate = dateFormat.format(date);
										list.setTxnDate(txnDate);
										account = transaction.getJSONObject("account");
									} else {
										list.setTransactionStatus(data.getJSONObject(i).getString("paymentStatus"));
									}

									if (account != null) {
										list.setAccountNumber(account.getLong("accountNumber"));
									}

									String bookingRefId = data.getJSONObject(i).getString("bookingRefId");

									try {
										String a[] = data.getJSONObject(i).getString("bookingRefId").split("#");
										if (a.length > 1) {
											bookingRefId = a[1];
										}
									} catch (Exception e) {
										e.printStackTrace();
									}

									list.setBookingRefId(bookingRefId);

									list.setPaymentAmount(data.getJSONObject(i).getDouble("paymentAmount"));
									list.setFlightTicketId(data.getJSONObject(i).getLong("id"));
									list.setTicketDetails(data.getJSONObject(i).getString("ticketDetails"));

									list.setFlightNameOnward(data.getJSONObject(i).getString("flightNumberOnward"));
									list.setFlightNameReturn(data.getJSONObject(i).getString("flightNumberReturn"));
									list.setTripType(data.getJSONObject(i).getString("tripType"));
									list.setBaseFare(data.getJSONObject(i).getDouble("baseFare"));
									list.setAgentAmount(data.getJSONObject(i).getDouble("agentAmount"));

									userList.add(list);

								}
							}
							model.addAttribute("FlightDeatils", userList);
						} else {
							model.addAttribute("FlightDeatils", userList);
						}

					} catch (Exception e) {
						e.printStackTrace();
						return "Admin/Login";
					}
					return "Admin/AgentFlightDetail";
				}
			}

		}
		return "Admin/Login";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/Agent/FlightdeatilList")
	String getAgentFlightdeatilListInJSONPost(@ModelAttribute PagingDTO dto, HttpServletRequest request, ModelMap model,
			HttpSession session) {
		try {
			dto.setStartDate(ConvertUtil.decodeBase64String(dto.getStartDate()));
			dto.setEndDate(ConvertUtil.decodeBase64String(dto.getEndDate()));
		} catch (Exception e) {
             e.getMessage();
		}
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String adminAuthority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.AGENT);
			if (adminAuthority != null) {
				if (adminAuthority.contains(Authorities.ADMINISTRATOR)
						&& adminAuthority.contains(Authorities.AUTHENTICATED)) {
					AllUserRequest userRequest = new AllUserRequest();
					userRequest.setPage(0);
					userRequest.setSize(1000);
					userRequest.setSessionId((String) session.getAttribute("adminSessionId"));
					userRequest.setStatus(UserStatus.ALL);
					AllUserResponse userResponse = appAdminApi.getAllAgentFlightsDetail(userRequest);
					try {
						List<FlightTicketAdminDTO> userList = new ArrayList<>();
						if (userResponse.getCode().equals("S00")) {
							JSONObject obj = new JSONObject(userResponse.getResponse());
							JSONArray data = obj.getJSONArray("details");
							if (data != null) {
								for (int i = 0; i < data.length(); i++) {
									FlightTicketAdminDTO list = new FlightTicketAdminDTO();
									JSONObject userDetail = null;
									JSONObject account = null;
									JSONObject transaction = data.getJSONObject(i).getJSONObject("transaction");
									JSONObject user = data.getJSONObject(i).getJSONObject("user");
									if (user != null) {
										userDetail = user.getJSONObject("userDetail");
										list.setUserDetails(userDetail.getString("firstName") + " " + userDetail.getString("lastName"));
										list.setUserName(user.getString("username"));
									}
									list.setBookingStatus(data.getJSONObject(i).getString("flightStatus"));
									double commissionAmount = (data.getJSONObject(i).getDouble("commissionAmt"));
									double totalCommissionAmount = commissionAmount - 200;
									list.setCommsissionAmount(BigDecimal.valueOf(totalCommissionAmount)
											.setScale(3, RoundingMode.HALF_UP).doubleValue());
									if (transaction != null) {
										list.setTransactionRefNo(transaction.getString("transactionRefNo"));
										list.setTransactionStatus(transaction.getString("status"));
										long dt = transaction.getLong("created");
										Date date = new Date(dt);
										String txnDate = dateFormat.format(date);
										list.setTxnDate(txnDate);
										account = transaction.getJSONObject("account");
									} else {
										list.setTransactionStatus(data.getJSONObject(i).getString("paymentStatus"));
									}
									if (account != null) {
										list.setAccountNumber(account.getLong("accountNumber"));
									}
									String bookingRefId = data.getJSONObject(i).getString("bookingRefId");
									try {
										String a[] = data.getJSONObject(i).getString("bookingRefId").split("#");
										if (a.length > 1) {
											bookingRefId = a[1];
										}
									} catch (Exception e) {
										e.printStackTrace();
									}
									list.setBookingRefId(bookingRefId);
									list.setPaymentAmount(data.getJSONObject(i).getDouble("paymentAmount"));
									list.setFlightTicketId(data.getJSONObject(i).getLong("id"));
									list.setTicketDetails(data.getJSONObject(i).getString("ticketDetails"));
									list.setFlightNameOnward(data.getJSONObject(i).getString("flightNumberOnward"));
									list.setFlightNameReturn(data.getJSONObject(i).getString("flightNumberReturn"));
									list.setTripType(data.getJSONObject(i).getString("tripType"));
									userList.add(list);
								}
							}
							model.addAttribute("FlightDeatils", userList);
						} else {
							model.addAttribute("FlightDeatils", userList);
						}

					} catch (Exception e) {
						e.printStackTrace();
						return "Admin/Login";
					}
					return "Admin/AgentFlightDetail";
				}
			}

		}
		return "Admin/Login";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/FlightdeatilList")
	String getFlightdeatilListInJSON(@ModelAttribute PagingDTO dto, HttpServletRequest request, ModelMap model,
			HttpSession session) {
		UserListDTO result = new UserListDTO();
		String sessionCheck = (String) session.getAttribute("adminSessionId");

		if (sessionCheck != null) {
			String adminAuthority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (adminAuthority != null) {
				if (adminAuthority.contains(Authorities.ADMINISTRATOR)
						&& adminAuthority.contains(Authorities.AUTHENTICATED)) {
					AllUserRequest userRequest = new AllUserRequest();
					userRequest.setPage(0);
					userRequest.setSize(1000);
					userRequest.setSessionId((String) session.getAttribute("adminSessionId"));
					userRequest.setStatus(UserStatus.ALL);
					AllUserResponse userResponse = appAdminApi.getAllFlightsDetail(userRequest);

					try {
						List<FlightTicketAdminDTO> userList = new ArrayList<>();
						if (userResponse.getCode().equals("S00")) {
							JSONObject obj = new JSONObject(userResponse.getResponse());
							JSONArray data = obj.getJSONArray("details");

							if (data != null) {

								for (int i = 0; i < data.length(); i++) {

									FlightTicketAdminDTO list = new FlightTicketAdminDTO();
									JSONObject userDetail = null;
									JSONObject account = null;
									JSONObject transaction = data.getJSONObject(i).getJSONObject("transaction");
									JSONObject user = data.getJSONObject(i).getJSONObject("user");
									if (user != null) {
										userDetail = user.getJSONObject("userDetail");
										list.setUserDetails(userDetail.getString("firstName") + " "
												+ userDetail.getString("lastName"));
										list.setUserName(user.getString("username"));
									}
									list.setBookingStatus(data.getJSONObject(i).getString("flightStatus"));
									if (transaction != null) {
										if (transaction.getString("status").equalsIgnoreCase("Success")) {
											double commissionAmount = (data.getJSONObject(i)
													.getDouble("commissionAmt"));
											double totalCommissionAmount = commissionAmount - 200;
											list.setCommsissionAmount(BigDecimal.valueOf(totalCommissionAmount)
													.setScale(3, RoundingMode.HALF_UP).doubleValue());
										} else {
											list.setCommsissionAmount(0.0);
										}
									}
									if (transaction != null) {
										list.setTransactionRefNo(transaction.getString("transactionRefNo"));
										list.setTransactionStatus(transaction.getString("status"));
										long dt = transaction.getLong("created");
										Date date = new Date(dt);
										String txnDate = dateFormat.format(date);
										list.setTxnDate(txnDate);
										account = transaction.getJSONObject("account");
									} else {
										list.setTransactionStatus(data.getJSONObject(i).getString("paymentStatus"));
									}
									if (account != null) {
										list.setAccountNumber(account.getLong("accountNumber"));
									}
									String bookingRefId = data.getJSONObject(i).getString("bookingRefId");
									try {
										String a[] = data.getJSONObject(i).getString("bookingRefId").split("#");
										if (a.length > 1) {
											bookingRefId = a[1];
										}
									} catch (Exception e) {
										e.printStackTrace();
									}
									list.setBookingRefId(bookingRefId);
									list.setPaymentAmount(data.getJSONObject(i).getDouble("paymentAmount"));
									list.setFlightTicketId(data.getJSONObject(i).getLong("id"));
									list.setTicketDetails(data.getJSONObject(i).getString("ticketDetails"));
									list.setFlightNameOnward(data.getJSONObject(i).getString("flightNumberOnward"));
									list.setFlightNameReturn(data.getJSONObject(i).getString("flightNumberReturn"));
									list.setTripType(data.getJSONObject(i).getString("tripType"));
									list.setBaseFare(data.getJSONObject(i).getDouble("baseFare"));
									userList.add(list);
								}
							}
							model.addAttribute("FlightDeatils", userList);
						} else {
							model.addAttribute("FlightDeatils", userList);
						}

					} catch (Exception e) {
						e.printStackTrace();
						return "Admin/Login";
					}
					return "Admin/FlightDetail";
				}
			}

		}
		return "Admin/Login";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/FlightdeatilList")
	String getFlightdeatilListInJSONPost(@ModelAttribute PagingDTO dto, HttpServletRequest request, ModelMap model,
			HttpSession session) {
		UserListDTO result = new UserListDTO();
		String sessionCheck = (String) session.getAttribute("adminSessionId");

		try {
			dto.setStartDate(ConvertUtil.decodeBase64String(dto.getEndDate()));
			dto.setEndDate(ConvertUtil.decodeBase64String(dto.getEndDate()));
		} catch (Exception e) {

		}

		if (sessionCheck != null) {
			String adminAuthority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (adminAuthority != null) {
				if (adminAuthority.contains(Authorities.ADMINISTRATOR)
						&& adminAuthority.contains(Authorities.AUTHENTICATED)) {
					AllUserRequest userRequest = new AllUserRequest();
					userRequest.setPage(0);
					userRequest.setSize(1000);
					userRequest.setSessionId((String) session.getAttribute("adminSessionId"));
					userRequest.setStatus(UserStatus.ALL);
					AllUserResponse userResponse = appAdminApi.getAllFlightsDetail(userRequest);

					try {
						List<FlightTicketAdminDTO> userList = new ArrayList<>();
						if (userResponse.getCode().equals("S00")) {
							JSONObject obj = new JSONObject(userResponse.getResponse());
							JSONArray data = obj.getJSONArray("details");

							if (data != null) {

								for (int i = 0; i < data.length(); i++) {

									FlightTicketAdminDTO list = new FlightTicketAdminDTO();
									JSONObject userDetail = null;
									JSONObject account = null;
									JSONObject transaction = data.getJSONObject(i).getJSONObject("transaction");
									JSONObject user = data.getJSONObject(i).getJSONObject("user");
									if (user != null) {
										userDetail = user.getJSONObject("userDetail");
										list.setUserDetails(userDetail.getString("firstName") + " "
												+ userDetail.getString("lastName"));
										list.setUserName(user.getString("username"));
									}

									list.setBookingStatus(data.getJSONObject(i).getString("flightStatus"));
									if (transaction != null) {
										if (transaction.getString("status").equalsIgnoreCase("Success")) {
											double commissionAmount = (data.getJSONObject(i)
													.getDouble("commissionAmt"));
											double totalCommissionAmount = commissionAmount - 200;
											list.setCommsissionAmount(BigDecimal.valueOf(totalCommissionAmount)
													.setScale(3, RoundingMode.HALF_UP).doubleValue());
										} else {
											list.setCommsissionAmount(0.0);
										}
									}
									if (transaction != null) {
										list.setTransactionRefNo(transaction.getString("transactionRefNo"));
										list.setTransactionStatus(transaction.getString("status"));
										long dt = transaction.getLong("created");
										Date date = new Date(dt);
										String txnDate = dateFormat.format(date);
										list.setTxnDate(txnDate);
										account = transaction.getJSONObject("account");
									} else {
										list.setTransactionStatus(data.getJSONObject(i).getString("paymentStatus"));
									}

									if (account != null) {
										list.setAccountNumber(account.getLong("accountNumber"));
									}

									String bookingRefId = data.getJSONObject(i).getString("bookingRefId");

									try {
										String a[] = data.getJSONObject(i).getString("bookingRefId").split("#");
										if (a.length > 1) {
											bookingRefId = a[1];
										}
									} catch (Exception e) {
										e.printStackTrace();
									}

									list.setBookingRefId(bookingRefId);

									list.setPaymentAmount(data.getJSONObject(i).getDouble("paymentAmount"));
									list.setFlightTicketId(data.getJSONObject(i).getLong("id"));
									list.setTicketDetails(data.getJSONObject(i).getString("ticketDetails"));

									list.setFlightNameOnward(data.getJSONObject(i).getString("flightNumberOnward"));
									list.setFlightNameReturn(data.getJSONObject(i).getString("flightNumberReturn"));
									list.setTripType(data.getJSONObject(i).getString("tripType"));

									userList.add(list);

								}
							}
							model.addAttribute("FlightDeatils", userList);
						} else {
							model.addAttribute("FlightDeatils", userList);
						}

					} catch (Exception e) {
						e.printStackTrace();
						return "Admin/Login";
					}
					return "Admin/FlightDetail";
				}
			}

		}
		return "Admin/Login";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/getSingleFlightTicketDetails", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<FlightResponseDTO> getSingleFlightTicketDetails(HttpSession session,
			@RequestBody FlightTicketAdminDTO dto) {

		FlightResponseDTO resp = new FlightResponseDTO();
		FlightCancelableResp resp1 = new FlightCancelableResp();

		String sessionId = (String) session.getAttribute("adminSessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					try {

						if (dto.getFlightTicketId() != 0) {

							resp = appAdminApi.getSingleFlightTicketDetails(sessionId, dto.getFlightTicketId());

							FlightTicketResp flightTicketResp = (FlightTicketResp) resp.getDetails();
							int tickets = flightTicketResp.getTicketsResp().getTickets().getOneway().size();

							FligthcancelableReq req = new FligthcancelableReq();

							req.setTransactionScreenId(flightTicketResp.getBookingRefNo());
							req.setSubUserId(flightTicketResp.getEmail());

							resp1 = appAdminApi.isCancellableFlight(req);

						} else {
							resp.setStatus(ResponseStatus.BAD_REQUEST.getKey());
							resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
							resp.setMessage("Please Enter Emt Txn Id");
						}

					} catch (Exception e) {
						e.printStackTrace();
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
					}
				} else {
					resp.setCode("F00");
					resp.setMessage("Unauthorised access");
					resp.setStatus("FAILED");
				}
			} else {
				resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
				resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
				resp.setMessage("Session expired");

			}
		} else {
			resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
			resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
			resp.setMessage("Session expired");

		}
		return new ResponseEntity<FlightResponseDTO>(resp, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/getSingleAgentFlightTicketDetails", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<FlightResponseDTO> getSingleAgentFlightTicketDetails(HttpSession session,
			@RequestBody FlightTicketAdminDTO dto) {

		FlightResponseDTO resp = new FlightResponseDTO();
		FlightCancelableResp resp1 = new FlightCancelableResp();

		String sessionId = (String) session.getAttribute("adminSessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					try {

						if (dto.getFlightTicketId() != 0) {

							resp = appAdminApi.getSingleAgentFlightTicketDetails(sessionId, dto.getFlightTicketId());

							FlightTicketResp flightTicketResp = (FlightTicketResp) resp.getDetails();
							int tickets = flightTicketResp.getTicketsResp().getTickets().getOneway().size();

							FligthcancelableReq req = new FligthcancelableReq();

							req.setTransactionScreenId(flightTicketResp.getBookingRefNo());
							req.setSubUserId(flightTicketResp.getEmail());

							resp1 = appAdminApi.isCancellableFlight(req);

						} else {
							resp.setStatus(ResponseStatus.BAD_REQUEST.getKey());
							resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
							resp.setMessage("Please Enter Emt Txn Id");
						}

					} catch (Exception e) {
						e.printStackTrace();
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
					}
				} else {
					resp.setCode("F00");
					resp.setMessage("Unauthorised access");
					resp.setStatus("FAILED");
				}
			} else {
				resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
				resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
				resp.setMessage("Session expired");

			}
		} else {
			resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
			resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
			resp.setMessage("Session expired");

		}
		return new ResponseEntity<FlightResponseDTO>(resp, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/getFlightDetails", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<FlightResponseDTO> getFlightDetails(HttpSession session,
			@RequestBody GetFlightDetailsForAdmin dto) {

		FlightResponseDTO resp = new FlightResponseDTO();
		String sessionId = (String) session.getAttribute("adminSessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					try {

						String bookingRefId = dto.getBookingRefId();
						/* String email=dto.getEmail(); */

						if (bookingRefId != null) {

							resp = appAdminApi.getFlightDetails(dto);
							resp.setStatus(ResponseStatus.SUCCESS.getKey());
							resp.setCode(ResponseStatus.SUCCESS.getValue());
						} else {
							resp.setStatus(ResponseStatus.BAD_REQUEST.getKey());
							resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
							resp.setMessage("Please Enter Emt Txn Id");
						}

					} catch (Exception e) {
						e.printStackTrace();
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
					}
				} else {
					resp.setCode("F00");
					resp.setMessage("Unauthorised access");
					resp.setStatus("FAILED");
				}
			} else {
				resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
				resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
				resp.setMessage("Session expired");

			}
		} else {
			resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
			resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
			resp.setMessage("Session expired");

		}
		return new ResponseEntity<FlightResponseDTO>(resp, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/getAgentFlightDetails", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<FlightResponseDTO> getAgentFlightDetails(HttpSession session,
			@RequestBody GetFlightDetailsForAdmin dto) {

		FlightResponseDTO resp = new FlightResponseDTO();
		String sessionId = (String) session.getAttribute("adminSessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					try {

						String bookingRefId = dto.getBookingRefId();
						/* String email=dto.getEmail(); */

						if (bookingRefId != null) {

							resp = appAdminApi.getFlightDetails(dto);
							resp.setStatus(ResponseStatus.SUCCESS.getKey());
							resp.setCode(ResponseStatus.SUCCESS.getValue());
						} else {
							resp.setStatus(ResponseStatus.BAD_REQUEST.getKey());
							resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
							resp.setMessage("Please Enter Emt Txn Id");
						}

					} catch (Exception e) {
						e.printStackTrace();
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
					}
				} else {
					resp.setCode("F00");
					resp.setMessage("Unauthorised access");
					resp.setStatus("FAILED");
				}
			} else {
				resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
				resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
				resp.setMessage("Session expired");

			}
		} else {
			resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
			resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
			resp.setMessage("Session expired");

		}
		return new ResponseEntity<FlightResponseDTO>(resp, HttpStatus.OK);
	}

	// Bus Details Show in Back end

	@RequestMapping(method = RequestMethod.GET, value = "/BusDeatilsList")
	String getBusdeatilListInJSON(@ModelAttribute PagingDTO dto, HttpServletRequest request, ModelMap model,
			HttpSession session) {
		double totaltx = 0;
		double totalcommission = 0;
		long count = 0;
		double test = 0;
		UserListDTO result = new UserListDTO();
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		List<BusTicketDTO> list = new ArrayList<>();
		if (sessionCheck != null) {
			String adminAuthority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (adminAuthority != null) {
				if (adminAuthority.contains(Authorities.ADMINISTRATOR)
						&& adminAuthority.contains(Authorities.AUTHENTICATED)) {
					AllUserRequest userRequest = new AllUserRequest();
					userRequest.setPage(0);
					userRequest.setSize(1000);
					userRequest.setSessionId((String) session.getAttribute("adminSessionId"));
					userRequest.setStatus(UserStatus.ALL);

					System.err.println("Inside Bus Details");

					AllUserResponse userResponse = appAdminApi.getBusDetails(userRequest);
					try {
						if (userResponse.getCode().equals("S00")) {
							JSONObject obj = new JSONObject(userResponse.getResponse());
							JSONObject obj1 = obj.getJSONObject("details");
							JSONArray details = obj1.getJSONArray("userList");
							System.err.println("details: " + details);
							JSONArray commissionList = obj1.getJSONArray("commissionList");
							System.err.println("commissionList: " + commissionList);
							for (int i = 0; i < details.length(); i++) {
								BusTicketDTO busTicketDTO = new BusTicketDTO();

								/*
								 * if(commissionList.length() >i){ JSONObject
								 * commissionTransaction =
								 * commissionList.getJSONObject(i);
								 * busTicketDTO.setCommsissionAmount(
								 * JSONParserUtil.getDouble(
								 * commissionTransaction, "commission"));
								 * 
								 * }else {
								 * busTicketDTO.setCommsissionAmount(0.0); }
								 */

								JSONObject val = details.getJSONObject(i);
								JSONObject txn = null;
								if (val != null) {
									if (val.getString("transaction") != null && !val.getString("transaction").isEmpty()
											&& !val.getString("transaction").equalsIgnoreCase("null")) {
										txn = val.getJSONObject("transaction");
									}
								}

								String ticketPnr = val.getString("ticketPnr");
								String operatorPnr = val.getString("operatorPnr");
								String source = val.getString("source");
								String destination = val.getString("destination");
								String journeyDate = val.getString("journeyDate");
								String arrTime = val.getString("arrTime");
								double totalFare = (double) details.getJSONObject(i).getDouble("totalFare");
								String priceRecheckTotalFare = details.getJSONObject(i)
										.getString("priceRecheckTotalFare");

								if (priceRecheckTotalFare != null && !priceRecheckTotalFare.isEmpty()
										&& !priceRecheckTotalFare.equalsIgnoreCase("null")) {

									totalFare = Double.valueOf(priceRecheckTotalFare);

								}

								double a = (totalFare * 4) / 100;
								String transactionRefNo = "NA";
								if (txn != null) {
									transactionRefNo = txn.getString("transactionRefNo");
									if (txn.getString("status").equalsIgnoreCase("Success")) {
										count++;
										totaltx += totalFare;
										/*
										 * if(commissionList.length() >i){
										 * JSONObject commissionTransaction =
										 * commissionList.getJSONObject(i); test
										 * += JSONParserUtil.getDouble(
										 * commissionTransaction, "commission");
										 * 
										 * }
										 */
										totalcommission += a;
										busTicketDTO.setCommsissionAmount(a);
									} else {
										busTicketDTO.setCommsissionAmount(0.0);
									}

									busTicketDTO.setTxnStatus(txn.getString("status"));
								} else {
									busTicketDTO.setTxnStatus("NA");
								}
								String status = val.getString("status");
								String emtTxnId = val.getString("emtTxnId");

								busTicketDTO.setArrTime(arrTime);
								busTicketDTO.setDestination(destination);
								busTicketDTO.setJourneyDate(journeyDate);
								busTicketDTO.setSource(source);
								busTicketDTO.setTotalFare(totalFare);
								busTicketDTO.setTransactionRefNo(transactionRefNo);
								busTicketDTO.setStatus(status);
								busTicketDTO.setEmtTxnId(emtTxnId);
								busTicketDTO.setTicketPnr(ticketPnr);
								busTicketDTO.setOperatorPnr(operatorPnr);

								String strUsr = val.getString("user");
								JSONObject usr = null;
								if (!strUsr.equalsIgnoreCase("null")) {
									usr = val.getJSONObject("user");
								}
								JSONObject usrDtl = null;
								if (usr != null) {
									usrDtl = usr.getJSONObject("userDetail");
								}

								UserDetail userDetail = new UserDetail();
								if (usrDtl != null) {
									String userFname = (String) details.getJSONObject(i).getJSONObject("user")
											.getJSONObject("userDetail").get("firstName");
									String userLname = (String) details.getJSONObject(i).getJSONObject("user")
											.getJSONObject("userDetail").get("lastName");
									String username = (String) details.getJSONObject(i).getJSONObject("user")
											.get("username");
									long accountNo = (long) details.getJSONObject(i).getJSONObject("user")
											.getJSONObject("accountDetail").getLong("accountNumber");

									userDetail.setFirstName(userFname);
									userDetail.setLastName(userLname);
									userDetail.setUsername(username);
									userDetail.setAccountNumber(accountNo);
								}

								busTicketDTO.setUserDetail(userDetail);

								long bookingDate = (long) details.getJSONObject(i).getLong("created");

								Date date = new Date(bookingDate);
								String txnDate = dateFormat.format(date);
								busTicketDTO.setTxnDate(txnDate);
								list.add(busTicketDTO);
							}

							System.err.println("test: " + test);

							request.setAttribute("totaltx",
									BigDecimal.valueOf(totaltx).setScale(2, RoundingMode.HALF_UP).doubleValue());
							request.setAttribute("totalcommission", BigDecimal.valueOf(totalcommission)
									.setScale(2, RoundingMode.HALF_UP).doubleValue());
							request.setAttribute("totalnotxs", count);
							model.addAttribute("BusDetails", list);
						} else {
							model.addAttribute("BusDetails", list);
						}
					} catch (Exception e) {
						e.printStackTrace();
						return "redirect:/Admin/Home";
					}
					return "Admin/BusDetails";
				}
			}

		}
		return "Admin/Login";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/BusDeatilsList")
	String getBusdeatilListInJSONPost(@ModelAttribute PagingDTO dto, HttpServletRequest request, ModelMap model,
			HttpSession session) {
		double totaltx = 0;
		double totalcommission = 0;
		long count = 0;
		double test = 0;

		try {
			dto.setStartDate(ConvertUtil.decodeBase64String(dto.getStartDate()));
			dto.setEndDate(ConvertUtil.decodeBase64String(dto.getEndDate()));
		} catch (Exception e) {

		}

		UserListDTO result = new UserListDTO();
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		List<BusTicketDTO> list = new ArrayList<>();
		if (sessionCheck != null) {
			String adminAuthority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (adminAuthority != null) {
				if (adminAuthority.contains(Authorities.ADMINISTRATOR)
						&& adminAuthority.contains(Authorities.AUTHENTICATED)) {
					AllUserRequest userRequest = new AllUserRequest();
					userRequest.setPage(0);
					userRequest.setSize(1000);
					userRequest.setSessionId((String) session.getAttribute("adminSessionId"));
					userRequest.setStatus(UserStatus.ALL);

					System.err.println("Inside Bus Details");

					AllUserResponse userResponse = appAdminApi.getBusDetails(userRequest);
					try {
						if (userResponse.getCode().equals("S00")) {
							JSONObject obj = new JSONObject(userResponse.getResponse());
							JSONObject obj1 = obj.getJSONObject("details");
							JSONArray details = obj1.getJSONArray("userList");
							System.err.println("details: " + details);
							JSONArray commissionList = obj1.getJSONArray("commissionList");
							System.err.println("commissionList: " + commissionList);
							for (int i = 0; i < details.length(); i++) {
								BusTicketDTO busTicketDTO = new BusTicketDTO();

								/*
								 * if(commissionList.length() >i){ JSONObject
								 * commissionTransaction =
								 * commissionList.getJSONObject(i);
								 * busTicketDTO.setCommsissionAmount(
								 * JSONParserUtil.getDouble(
								 * commissionTransaction, "commission"));
								 * 
								 * }else {
								 * busTicketDTO.setCommsissionAmount(0.0); }
								 */

								JSONObject val = details.getJSONObject(i);
								JSONObject txn = null;
								if (val != null) {
									if (val.getString("transaction") != null && !val.getString("transaction").isEmpty()
											&& !val.getString("transaction").equalsIgnoreCase("null")) {
										txn = val.getJSONObject("transaction");
									}
								}

								String ticketPnr = val.getString("ticketPnr");
								String operatorPnr = val.getString("operatorPnr");
								String source = val.getString("source");
								String destination = val.getString("destination");
								String journeyDate = val.getString("journeyDate");
								String arrTime = val.getString("arrTime");
								double totalFare = (double) details.getJSONObject(i).getDouble("totalFare");
								String priceRecheckTotalFare = details.getJSONObject(i)
										.getString("priceRecheckTotalFare");

								if (priceRecheckTotalFare != null && !priceRecheckTotalFare.isEmpty()
										&& !priceRecheckTotalFare.equalsIgnoreCase("null")) {

									totalFare = Double.valueOf(priceRecheckTotalFare);

								}

								double a = (totalFare * 4) / 100;
								String transactionRefNo = "NA";
								if (txn != null) {
									transactionRefNo = txn.getString("transactionRefNo");
									if (txn.getString("status").equalsIgnoreCase("Success")) {
										count++;
										totaltx += totalFare;
										/*
										 * if(commissionList.length() >i){
										 * JSONObject commissionTransaction =
										 * commissionList.getJSONObject(i); test
										 * += JSONParserUtil.getDouble(
										 * commissionTransaction, "commission");
										 * 
										 * }
										 */
										totalcommission += a;
										busTicketDTO.setCommsissionAmount(a);
									} else {
										busTicketDTO.setCommsissionAmount(0.0);
									}

									busTicketDTO.setTxnStatus(txn.getString("status"));
								} else {
									busTicketDTO.setTxnStatus("NA");
								}
								String status = val.getString("status");
								String emtTxnId = val.getString("emtTxnId");

								busTicketDTO.setArrTime(arrTime);
								busTicketDTO.setDestination(destination);
								busTicketDTO.setJourneyDate(journeyDate);
								busTicketDTO.setSource(source);
								busTicketDTO.setTotalFare(totalFare);
								busTicketDTO.setTransactionRefNo(transactionRefNo);
								busTicketDTO.setStatus(status);
								busTicketDTO.setEmtTxnId(emtTxnId);
								busTicketDTO.setTicketPnr(ticketPnr);
								busTicketDTO.setOperatorPnr(operatorPnr);

								String strUsr = val.getString("user");
								JSONObject usr = null;
								if (!strUsr.equalsIgnoreCase("null")) {
									usr = val.getJSONObject("user");
								}
								JSONObject usrDtl = null;
								if (usr != null) {
									usrDtl = usr.getJSONObject("userDetail");
								}

								UserDetail userDetail = new UserDetail();
								if (usrDtl != null) {
									String userFname = (String) details.getJSONObject(i).getJSONObject("user")
											.getJSONObject("userDetail").get("firstName");
									String userLname = (String) details.getJSONObject(i).getJSONObject("user")
											.getJSONObject("userDetail").get("lastName");
									String username = (String) details.getJSONObject(i).getJSONObject("user")
											.get("username");
									long accountNo = (long) details.getJSONObject(i).getJSONObject("user")
											.getJSONObject("accountDetail").getLong("accountNumber");

									userDetail.setFirstName(userFname);
									userDetail.setLastName(userLname);
									userDetail.setUsername(username);
									userDetail.setAccountNumber(accountNo);
								}

								busTicketDTO.setUserDetail(userDetail);

								long bookingDate = (long) details.getJSONObject(i).getLong("created");

								Date date = new Date(bookingDate);
								String txnDate = dateFormat.format(date);
								busTicketDTO.setTxnDate(txnDate);
								list.add(busTicketDTO);
							}

							System.err.println("test: " + test);

							request.setAttribute("totaltx",
									BigDecimal.valueOf(totaltx).setScale(2, RoundingMode.HALF_UP).doubleValue());
							request.setAttribute("totalcommission", BigDecimal.valueOf(totalcommission)
									.setScale(2, RoundingMode.HALF_UP).doubleValue());
							request.setAttribute("totalnotxs", count);
							model.addAttribute("BusDetails", list);
						} else {
							model.addAttribute("BusDetails", list);
						}
					} catch (Exception e) {
						e.printStackTrace();
						return "redirect:/Admin/Home";
					}
					return "Admin/BusDetails";
				}
			}

		}
		return "Admin/Login";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/Agent/BusDeatilsList")
	String getAgentBusdeatilListInJSONPost(@ModelAttribute PagingDTO dto, HttpServletRequest request, ModelMap model,
			HttpSession session) {

		try {
			dto.setStartDate(ConvertUtil.decodeBase64String(dto.getStartDate()));
			dto.setEndDate(ConvertUtil.decodeBase64String(dto.getEndDate()));
		} catch (Exception e) {

		}

		double totaltx = 0;
		double totalcommission = 0;
		long count = 0;
		double test = 0;
		UserListDTO result = new UserListDTO();
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		List<BusTicketDTO> list = new ArrayList<>();
		if (sessionCheck != null) {
			String adminAuthority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.AGENT);
			if (adminAuthority != null) {
				if (adminAuthority.contains(Authorities.ADMINISTRATOR)
						&& adminAuthority.contains(Authorities.AUTHENTICATED)) {
					AllUserRequest userRequest = new AllUserRequest();
					userRequest.setPage(0);
					userRequest.setSize(1000);
					userRequest.setSessionId((String) session.getAttribute("adminSessionId"));
					userRequest.setStatus(UserStatus.ALL);

					System.err.println("Inside Bus Details");

					AllUserResponse userResponse = appAdminApi.getAgentBusDetails(userRequest);
					try {
						if (userResponse.getCode().equals("S00")) {
							JSONObject obj = new JSONObject(userResponse.getResponse());
							JSONObject obj1 = obj.getJSONObject("details");
							JSONArray details = obj1.getJSONArray("userList");
							System.err.println("details: " + details);
							JSONArray commissionList = obj1.getJSONArray("commissionList");
							System.err.println("commissionList: " + commissionList);
							for (int i = 0; i < details.length(); i++) {
								BusTicketDTO busTicketDTO = new BusTicketDTO();

								/*
								 * if(commissionList.length() >i){ JSONObject
								 * commissionTransaction =
								 * commissionList.getJSONObject(i);
								 * busTicketDTO.setCommsissionAmount(
								 * JSONParserUtil.getDouble(
								 * commissionTransaction, "commission"));
								 * 
								 * }else {
								 * busTicketDTO.setCommsissionAmount(0.0); }
								 */

								JSONObject val = details.getJSONObject(i);
								JSONObject txn = null;
								if (val != null) {
									if (val.getString("transaction") != null && !val.getString("transaction").isEmpty()
											&& !val.getString("transaction").equalsIgnoreCase("null")) {
										txn = val.getJSONObject("transaction");
									}
								}

								String ticketPnr = val.getString("ticketPnr");
								String operatorPnr = val.getString("operatorPnr");
								String source = val.getString("source");
								String destination = val.getString("destination");
								String journeyDate = val.getString("journeyDate");
								String arrTime = val.getString("arrTime");
								double totalFare = (double) details.getJSONObject(i).getDouble("totalFare");
								String priceRecheckTotalFare = details.getJSONObject(i)
										.getString("priceRecheckTotalFare");

								if (priceRecheckTotalFare != null && !priceRecheckTotalFare.isEmpty()
										&& !priceRecheckTotalFare.equalsIgnoreCase("null")) {

									totalFare = Double.valueOf(priceRecheckTotalFare);

								}

								double a = (totalFare * 4) / 100;
								System.err.println("commisssion: " + a);

								String transactionRefNo = "NA";
								if (txn != null) {
									transactionRefNo = txn.getString("transactionRefNo");
									if (txn.getString("status").equalsIgnoreCase("Success")) {
										count++;
										totaltx += totalFare;
										/*
										 * if(commissionList.length() >i){
										 * JSONObject commissionTransaction =
										 * commissionList.getJSONObject(i); test
										 * += JSONParserUtil.getDouble(
										 * commissionTransaction, "commission");
										 * 
										 * }
										 */
										totalcommission += a;
										busTicketDTO.setCommsissionAmount(a);
									} else {
										busTicketDTO.setCommsissionAmount(0.0);
									}

									busTicketDTO.setTxnStatus(txn.getString("status"));
								} else {
									busTicketDTO.setTxnStatus("NA");
								}
								String status = val.getString("status");
								String emtTxnId = val.getString("emtTxnId");

								busTicketDTO.setArrTime(arrTime);
								busTicketDTO.setDestination(destination);
								busTicketDTO.setJourneyDate(journeyDate);
								busTicketDTO.setSource(source);
								busTicketDTO.setTotalFare(totalFare);
								busTicketDTO.setTransactionRefNo(transactionRefNo);
								busTicketDTO.setStatus(status);
								busTicketDTO.setEmtTxnId(emtTxnId);
								busTicketDTO.setTicketPnr(ticketPnr);
								busTicketDTO.setOperatorPnr(operatorPnr);

								String strUsr = val.getString("user");
								JSONObject usr = null;
								if (!strUsr.equalsIgnoreCase("null")) {
									usr = val.getJSONObject("user");
								}
								JSONObject usrDtl = null;
								if (usr != null) {
									usrDtl = usr.getJSONObject("userDetail");
								}

								UserDetail userDetail = new UserDetail();
								if (usrDtl != null) {
									String userFname = (String) details.getJSONObject(i).getJSONObject("user")
											.getJSONObject("userDetail").get("firstName");
									String userLname = (String) details.getJSONObject(i).getJSONObject("user")
											.getJSONObject("userDetail").get("lastName");
									String username = (String) details.getJSONObject(i).getJSONObject("user")
											.get("username");
									long accountNo = (long) details.getJSONObject(i).getJSONObject("user")
											.getJSONObject("accountDetail").getLong("accountNumber");

									userDetail.setFirstName(userFname);
									userDetail.setLastName(userLname);
									userDetail.setUsername(username);
									userDetail.setAccountNumber(accountNo);
								}

								busTicketDTO.setUserDetail(userDetail);

								long bookingDate = (long) details.getJSONObject(i).getLong("created");

								Date date = new Date(bookingDate);
								String txnDate = dateFormat.format(date);
								busTicketDTO.setTxnDate(txnDate);
								list.add(busTicketDTO);
							}

							System.err.println("test: " + test);

							request.setAttribute("totaltx",
									BigDecimal.valueOf(totaltx).setScale(2, RoundingMode.HALF_UP).doubleValue());
							request.setAttribute("totalcommission", BigDecimal.valueOf(totalcommission)
									.setScale(2, RoundingMode.HALF_UP).doubleValue());
							request.setAttribute("totalnotxs", count);
							model.addAttribute("BusDetails", list);
						} else {
							model.addAttribute("BusDetails", list);
						}
					} catch (Exception e) {
						e.printStackTrace();
						return "redirect:/Admin/Home";
					}
					return "Admin/AgentBusDetails";
				}
			}

		}
		return "Admin/Login";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/Agent/BusDeatilsList")
	String getAgentBusdeatilListInJSON(@ModelAttribute PagingDTO dto, HttpServletRequest request, ModelMap model,
			HttpSession session) {
		double totaltx = 0;
		double totalcommission = 0;
		long count = 0;
		double test = 0;
		UserListDTO result = new UserListDTO();
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		List<BusTicketDTO> list = new ArrayList<>();
		if (sessionCheck != null) {
			String adminAuthority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.AGENT);
			if (adminAuthority != null) {
				if (adminAuthority.contains(Authorities.ADMINISTRATOR)
						&& adminAuthority.contains(Authorities.AUTHENTICATED)) {
					AllUserRequest userRequest = new AllUserRequest();
					userRequest.setPage(0);
					userRequest.setSize(1000);
					userRequest.setSessionId((String) session.getAttribute("adminSessionId"));
					userRequest.setStatus(UserStatus.ALL);

					System.err.println("Inside Bus Details");

					AllUserResponse userResponse = appAdminApi.getAgentBusDetails(userRequest);
					try {
						if (userResponse.getCode().equals("S00")) {
							JSONObject obj = new JSONObject(userResponse.getResponse());
							JSONObject obj1 = obj.getJSONObject("details");
							JSONArray details = obj1.getJSONArray("userList");
							System.err.println("details: " + details);
							JSONArray commissionList = obj1.getJSONArray("commissionList");
							System.err.println("commissionList: " + commissionList);
							for (int i = 0; i < details.length(); i++) {
								BusTicketDTO busTicketDTO = new BusTicketDTO();

								/*
								 * if(commissionList.length() >i){ JSONObject
								 * commissionTransaction =
								 * commissionList.getJSONObject(i);
								 * busTicketDTO.setCommsissionAmount(
								 * JSONParserUtil.getDouble(
								 * commissionTransaction, "commission"));
								 * 
								 * }else {
								 * busTicketDTO.setCommsissionAmount(0.0); }
								 */

								JSONObject val = details.getJSONObject(i);
								JSONObject txn = null;
								if (val != null) {
									if (val.getString("transaction") != null && !val.getString("transaction").isEmpty()
											&& !val.getString("transaction").equalsIgnoreCase("null")) {
										txn = val.getJSONObject("transaction");
									}
								}

								String ticketPnr = val.getString("ticketPnr");
								String operatorPnr = val.getString("operatorPnr");
								String source = val.getString("source");
								String destination = val.getString("destination");
								String journeyDate = val.getString("journeyDate");
								String arrTime = val.getString("arrTime");
								double totalFare = (double) details.getJSONObject(i).getDouble("totalFare");
								String priceRecheckTotalFare = details.getJSONObject(i)
										.getString("priceRecheckTotalFare");

								if (priceRecheckTotalFare != null && !priceRecheckTotalFare.isEmpty()
										&& !priceRecheckTotalFare.equalsIgnoreCase("null")) {

									totalFare = Double.valueOf(priceRecheckTotalFare);

								}

								double a = (totalFare * 4) / 100;
								System.err.println("commisssion: " + a);

								String transactionRefNo = "NA";
								if (txn != null) {
									transactionRefNo = txn.getString("transactionRefNo");
									if (txn.getString("status").equalsIgnoreCase("Success")) {
										count++;
										totaltx += totalFare;
										/*
										 * if(commissionList.length() >i){
										 * JSONObject commissionTransaction =
										 * commissionList.getJSONObject(i); test
										 * += JSONParserUtil.getDouble(
										 * commissionTransaction, "commission");
										 * 
										 * }
										 */
										totalcommission += a;
										busTicketDTO.setCommsissionAmount(a);
									} else {
										busTicketDTO.setCommsissionAmount(0.0);
									}

									busTicketDTO.setTxnStatus(txn.getString("status"));
								} else {
									busTicketDTO.setTxnStatus("NA");
								}
								String status = val.getString("status");
								String emtTxnId = val.getString("emtTxnId");

								busTicketDTO.setArrTime(arrTime);
								busTicketDTO.setDestination(destination);
								busTicketDTO.setJourneyDate(journeyDate);
								busTicketDTO.setSource(source);
								busTicketDTO.setTotalFare(totalFare);
								busTicketDTO.setTransactionRefNo(transactionRefNo);
								busTicketDTO.setStatus(status);
								busTicketDTO.setEmtTxnId(emtTxnId);
								busTicketDTO.setTicketPnr(ticketPnr);
								busTicketDTO.setOperatorPnr(operatorPnr);

								String strUsr = val.getString("user");
								JSONObject usr = null;
								if (!strUsr.equalsIgnoreCase("null")) {
									usr = val.getJSONObject("user");
								}
								JSONObject usrDtl = null;
								if (usr != null) {
									usrDtl = usr.getJSONObject("userDetail");
								}

								UserDetail userDetail = new UserDetail();
								if (usrDtl != null) {
									String userFname = (String) details.getJSONObject(i).getJSONObject("user")
											.getJSONObject("userDetail").get("firstName");
									String userLname = (String) details.getJSONObject(i).getJSONObject("user")
											.getJSONObject("userDetail").get("lastName");
									String username = (String) details.getJSONObject(i).getJSONObject("user")
											.get("username");
									long accountNo = (long) details.getJSONObject(i).getJSONObject("user")
											.getJSONObject("accountDetail").getLong("accountNumber");

									userDetail.setFirstName(userFname);
									userDetail.setLastName(userLname);
									userDetail.setUsername(username);
									userDetail.setAccountNumber(accountNo);
								}

								busTicketDTO.setUserDetail(userDetail);

								long bookingDate = (long) details.getJSONObject(i).getLong("created");

								Date date = new Date(bookingDate);
								String txnDate = dateFormat.format(date);
								busTicketDTO.setTxnDate(txnDate);
								list.add(busTicketDTO);
							}

							System.err.println("test: " + test);

							request.setAttribute("totaltx",
									BigDecimal.valueOf(totaltx).setScale(2, RoundingMode.HALF_UP).doubleValue());
							request.setAttribute("totalcommission", BigDecimal.valueOf(totalcommission)
									.setScale(2, RoundingMode.HALF_UP).doubleValue());
							request.setAttribute("totalnotxs", count);
							model.addAttribute("BusDetails", list);
						} else {
							model.addAttribute("BusDetails", list);
						}
					} catch (Exception e) {
						e.printStackTrace();
						return "redirect:/Admin/Home";
					}
					return "Admin/AgentBusDetails";
				}
			}

		}
		return "Admin/Login";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/BusDeatilsListByDate")
	String getBusdeatilByDate(@ModelAttribute PagingDTO dto, HttpServletRequest request, ModelMap model,
			HttpSession session) {

		double totaltx = 0;
		double totalcommission = 0;
		long count = 0;

		UserListDTO result = new UserListDTO();
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		List<BusTicketDTO> list = new ArrayList<>();
		if (sessionCheck != null) {
			String adminAuthority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (adminAuthority != null) {
				if (adminAuthority.contains(Authorities.ADMINISTRATOR)
						&& adminAuthority.contains(Authorities.AUTHENTICATED)) {
					AllUserRequest userRequest = new AllUserRequest();
					userRequest.setPage(0);
					userRequest.setSize(1000);
					dto.setSessionId((String) session.getAttribute("adminSessionId"));
					userRequest.setStatus(UserStatus.ALL);
					AllUserResponse userResponse = appAdminApi.getBusDetailsByDate(dto);
					try {
						if (userResponse.getCode().equals("S00")) {
							JSONObject obj = new JSONObject(userResponse.getResponse());
							JSONObject obj1 = obj.getJSONObject("details");
							JSONArray details = obj1.getJSONArray("userList");
							JSONArray commissionList = obj1.getJSONArray("commissionList");
							for (int i = 0; i < details.length(); i++) {
								BusTicketDTO busTicketDTO = new BusTicketDTO();

								/*
								 * if(commissionList.length() >i){ JSONObject
								 * commissionTransaction =
								 * commissionList.getJSONObject(i);
								 * busTicketDTO.setCommsissionAmount(
								 * JSONParserUtil.getDouble(
								 * commissionTransaction, "commission"));
								 * 
								 * }else {
								 * busTicketDTO.setCommsissionAmount(0.0); }
								 */

								JSONObject val = details.getJSONObject(i);
								JSONObject txn = null;
								if (val != null) {
									if (val.getString("transaction") != null && !val.getString("transaction").isEmpty()
											&& !val.getString("transaction").equalsIgnoreCase("null")) {
										txn = val.getJSONObject("transaction");
									}
								}

								String ticketPnr = val.getString("ticketPnr");
								String operatorPnr = val.getString("operatorPnr");
								String source = val.getString("source");
								String destination = val.getString("destination");
								String journeyDate = val.getString("journeyDate");
								String arrTime = val.getString("arrTime");
								double totalFare = (double) details.getJSONObject(i).getDouble("totalFare");
								String priceRecheckTotalFare = details.getJSONObject(i)
										.getString("priceRecheckTotalFare");

								if (priceRecheckTotalFare != null && !priceRecheckTotalFare.isEmpty()
										&& !priceRecheckTotalFare.equalsIgnoreCase("null")) {

									totalFare = Double.valueOf(priceRecheckTotalFare);

								}

								double a = (totalFare * 4) / 100;
								System.err.println("commisssion: " + a);

								String transactionRefNo = "NA";
								if (txn != null) {
									transactionRefNo = txn.getString("transactionRefNo");
									if (txn.getString("status").equalsIgnoreCase("Success")) {
										count++;
										totaltx += totalFare;
										/*
										 * if(commissionList.length() >i){
										 * JSONObject commissionTransaction =
										 * commissionList.getJSONObject(i);
										 * totalcommission +=
										 * JSONParserUtil.getDouble(
										 * commissionTransaction, "commission");
										 * }
										 */
										totalcommission += a;
										busTicketDTO.setCommsissionAmount(a);
									} else {
										busTicketDTO.setCommsissionAmount(0.0);
									}
									busTicketDTO.setTxnStatus(txn.getString("status"));
								} else {
									busTicketDTO.setTxnStatus("NA");
								}
								String status = val.getString("status");
								String emtTxnId = val.getString("emtTxnId");

								busTicketDTO.setArrTime(arrTime);
								busTicketDTO.setDestination(destination);
								busTicketDTO.setJourneyDate(journeyDate);
								busTicketDTO.setSource(source);
								busTicketDTO.setTotalFare(totalFare);
								busTicketDTO.setTransactionRefNo(transactionRefNo);
								busTicketDTO.setStatus(status);
								busTicketDTO.setEmtTxnId(emtTxnId);
								busTicketDTO.setTicketPnr(ticketPnr);
								busTicketDTO.setOperatorPnr(operatorPnr);

								String strUsr = val.getString("user");
								JSONObject usr = null;
								if (!strUsr.equalsIgnoreCase("null")) {
									usr = val.getJSONObject("user");
								}
								JSONObject usrDtl = null;
								if (usr != null) {
									usrDtl = usr.getJSONObject("userDetail");
								}

								UserDetail userDetail = new UserDetail();
								if (usrDtl != null) {
									String userFname = (String) details.getJSONObject(i).getJSONObject("user")
											.getJSONObject("userDetail").get("firstName");
									String userLname = (String) details.getJSONObject(i).getJSONObject("user")
											.getJSONObject("userDetail").get("lastName");
									String username = (String) details.getJSONObject(i).getJSONObject("user")
											.get("username");
									long accountNo = (long) details.getJSONObject(i).getJSONObject("user")
											.getJSONObject("accountDetail").getLong("accountNumber");

									userDetail.setFirstName(userFname);
									userDetail.setLastName(userLname);
									userDetail.setUsername(username);
									userDetail.setAccountNumber(accountNo);
								}

								busTicketDTO.setUserDetail(userDetail);

								long bookingDate = (long) details.getJSONObject(i).getLong("created");

								Date date = new Date(bookingDate);
								String txnDate = dateFormat.format(date);
								busTicketDTO.setTxnDate(txnDate);
								list.add(busTicketDTO);
							}
							request.setAttribute("totaltx",
									BigDecimal.valueOf(totaltx).setScale(2, RoundingMode.HALF_UP).doubleValue());
							request.setAttribute("totalcommission", BigDecimal.valueOf(totalcommission)
									.setScale(2, RoundingMode.HALF_UP).doubleValue());
							request.setAttribute("totalnotxs", count);
							model.addAttribute("BusDetails", list);
						} else {
							model.addAttribute("BusDetails", list);
						}

					} catch (Exception e) {
						e.printStackTrace();
						return "redirect:/Admin/Home";
					}
					return "Admin/BusDetails";
				}
			}

		}
		return "Admin/Login";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/BusDeatilsListByDate")
	String getBusdeatilByDatePost(@ModelAttribute PagingDTO dto, HttpServletRequest request, ModelMap model,
			HttpSession session) {

		try {
			dto.setStartDate(ConvertUtil.decodeBase64String(dto.getStartDate()));
			dto.setEndDate(ConvertUtil.decodeBase64String(dto.getEndDate()));
		} catch (Exception e) {

		}

		double totaltx = 0;
		double totalcommission = 0;
		long count = 0;

		UserListDTO result = new UserListDTO();
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		List<BusTicketDTO> list = new ArrayList<>();
		if (sessionCheck != null) {
			String adminAuthority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (adminAuthority != null) {
				if (adminAuthority.contains(Authorities.ADMINISTRATOR)
						&& adminAuthority.contains(Authorities.AUTHENTICATED)) {
					AllUserRequest userRequest = new AllUserRequest();
					userRequest.setPage(0);
					userRequest.setSize(1000);
					dto.setSessionId((String) session.getAttribute("adminSessionId"));
					userRequest.setStatus(UserStatus.ALL);
					AllUserResponse userResponse = appAdminApi.getBusDetailsByDate(dto);
					try {
						if (userResponse.getCode().equals("S00")) {
							JSONObject obj = new JSONObject(userResponse.getResponse());
							JSONObject obj1 = obj.getJSONObject("details");
							JSONArray details = obj1.getJSONArray("userList");
							JSONArray commissionList = obj1.getJSONArray("commissionList");
							for (int i = 0; i < details.length(); i++) {
								BusTicketDTO busTicketDTO = new BusTicketDTO();

								/*
								 * if(commissionList.length() >i){ JSONObject
								 * commissionTransaction =
								 * commissionList.getJSONObject(i);
								 * busTicketDTO.setCommsissionAmount(
								 * JSONParserUtil.getDouble(
								 * commissionTransaction, "commission"));
								 * 
								 * }else {
								 * busTicketDTO.setCommsissionAmount(0.0); }
								 */

								JSONObject val = details.getJSONObject(i);
								JSONObject txn = null;
								if (val != null) {
									if (val.getString("transaction") != null && !val.getString("transaction").isEmpty()
											&& !val.getString("transaction").equalsIgnoreCase("null")) {
										txn = val.getJSONObject("transaction");
									}
								}

								String ticketPnr = val.getString("ticketPnr");
								String operatorPnr = val.getString("operatorPnr");
								String source = val.getString("source");
								String destination = val.getString("destination");
								String journeyDate = val.getString("journeyDate");
								String arrTime = val.getString("arrTime");
								double totalFare = (double) details.getJSONObject(i).getDouble("totalFare");
								String priceRecheckTotalFare = details.getJSONObject(i)
										.getString("priceRecheckTotalFare");

								if (priceRecheckTotalFare != null && !priceRecheckTotalFare.isEmpty()
										&& !priceRecheckTotalFare.equalsIgnoreCase("null")) {

									totalFare = Double.valueOf(priceRecheckTotalFare);

								}

								double a = (totalFare * 4) / 100;
								System.err.println("commisssion: " + a);

								String transactionRefNo = "NA";
								if (txn != null) {
									transactionRefNo = txn.getString("transactionRefNo");
									if (txn.getString("status").equalsIgnoreCase("Success")) {
										count++;
										totaltx += totalFare;
										/*
										 * if(commissionList.length() >i){
										 * JSONObject commissionTransaction =
										 * commissionList.getJSONObject(i);
										 * totalcommission +=
										 * JSONParserUtil.getDouble(
										 * commissionTransaction, "commission");
										 * }
										 */
										totalcommission += a;
										busTicketDTO.setCommsissionAmount(a);
									} else {
										busTicketDTO.setCommsissionAmount(0.0);
									}
									busTicketDTO.setTxnStatus(txn.getString("status"));
								} else {
									busTicketDTO.setTxnStatus("NA");
								}
								String status = val.getString("status");
								String emtTxnId = val.getString("emtTxnId");

								busTicketDTO.setArrTime(arrTime);
								busTicketDTO.setDestination(destination);
								busTicketDTO.setJourneyDate(journeyDate);
								busTicketDTO.setSource(source);
								busTicketDTO.setTotalFare(totalFare);
								busTicketDTO.setTransactionRefNo(transactionRefNo);
								busTicketDTO.setStatus(status);
								busTicketDTO.setEmtTxnId(emtTxnId);
								busTicketDTO.setTicketPnr(ticketPnr);
								busTicketDTO.setOperatorPnr(operatorPnr);

								String strUsr = val.getString("user");
								JSONObject usr = null;
								if (!strUsr.equalsIgnoreCase("null")) {
									usr = val.getJSONObject("user");
								}
								JSONObject usrDtl = null;
								if (usr != null) {
									usrDtl = usr.getJSONObject("userDetail");
								}

								UserDetail userDetail = new UserDetail();
								if (usrDtl != null) {
									String userFname = (String) details.getJSONObject(i).getJSONObject("user")
											.getJSONObject("userDetail").get("firstName");
									String userLname = (String) details.getJSONObject(i).getJSONObject("user")
											.getJSONObject("userDetail").get("lastName");
									String username = (String) details.getJSONObject(i).getJSONObject("user")
											.get("username");
									long accountNo = (long) details.getJSONObject(i).getJSONObject("user")
											.getJSONObject("accountDetail").getLong("accountNumber");

									userDetail.setFirstName(userFname);
									userDetail.setLastName(userLname);
									userDetail.setUsername(username);
									userDetail.setAccountNumber(accountNo);
								}

								busTicketDTO.setUserDetail(userDetail);

								long bookingDate = (long) details.getJSONObject(i).getLong("created");

								Date date = new Date(bookingDate);
								String txnDate = dateFormat.format(date);
								busTicketDTO.setTxnDate(txnDate);
								list.add(busTicketDTO);
							}
							request.setAttribute("totaltx",
									BigDecimal.valueOf(totaltx).setScale(2, RoundingMode.HALF_UP).doubleValue());
							request.setAttribute("totalcommission", BigDecimal.valueOf(totalcommission)
									.setScale(2, RoundingMode.HALF_UP).doubleValue());
							request.setAttribute("totalnotxs", count);
							model.addAttribute("BusDetails", list);
						} else {
							model.addAttribute("BusDetails", list);
						}

					} catch (Exception e) {
						e.printStackTrace();
						return "redirect:/Admin/Home";
					}
					return "Admin/BusDetails";
				}
			}

		}
		return "Admin/Login";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/Agent/BusDeatilsListByDate")
	String getAgentBusdeatilByDate(@ModelAttribute PagingDTO dto, HttpServletRequest request, ModelMap model,
			HttpSession session) {

		double totaltx = 0;
		double totalcommission = 0;
		long count = 0;

		UserListDTO result = new UserListDTO();
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		List<BusTicketDTO> list = new ArrayList<>();
		if (sessionCheck != null) {
			String adminAuthority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.AGENT);
			if (adminAuthority != null) {
				if (adminAuthority.contains(Authorities.ADMINISTRATOR)
						&& adminAuthority.contains(Authorities.AUTHENTICATED)) {
					AllUserRequest userRequest = new AllUserRequest();
					userRequest.setPage(0);
					userRequest.setSize(1000);
					dto.setSessionId((String) session.getAttribute("adminSessionId"));
					userRequest.setStatus(UserStatus.ALL);
					AllUserResponse userResponse = appAdminApi.getAgentBusDetailsByDate(dto);
					try {
						if (userResponse.getCode().equals("S00")) {
							JSONObject obj = new JSONObject(userResponse.getResponse());
							JSONObject obj1 = obj.getJSONObject("details");
							JSONArray details = obj1.getJSONArray("userList");
							JSONArray commissionList = obj1.getJSONArray("commissionList");
							for (int i = 0; i < details.length(); i++) {
								BusTicketDTO busTicketDTO = new BusTicketDTO();

								/*
								 * if(commissionList.length() >i){ JSONObject
								 * commissionTransaction =
								 * commissionList.getJSONObject(i);
								 * busTicketDTO.setCommsissionAmount(
								 * JSONParserUtil.getDouble(
								 * commissionTransaction, "commission"));
								 * 
								 * }else {
								 * busTicketDTO.setCommsissionAmount(0.0); }
								 */

								JSONObject val = details.getJSONObject(i);
								JSONObject txn = null;
								if (val != null) {
									if (val.getString("transaction") != null && !val.getString("transaction").isEmpty()
											&& !val.getString("transaction").equalsIgnoreCase("null")) {
										txn = val.getJSONObject("transaction");
									}
								}

								String ticketPnr = val.getString("ticketPnr");
								String operatorPnr = val.getString("operatorPnr");
								String source = val.getString("source");
								String destination = val.getString("destination");
								String journeyDate = val.getString("journeyDate");
								String arrTime = val.getString("arrTime");
								double totalFare = (double) details.getJSONObject(i).getDouble("totalFare");
								String priceRecheckTotalFare = details.getJSONObject(i)
										.getString("priceRecheckTotalFare");

								if (priceRecheckTotalFare != null && !priceRecheckTotalFare.isEmpty()
										&& !priceRecheckTotalFare.equalsIgnoreCase("null")) {

									totalFare = Double.valueOf(priceRecheckTotalFare);

								}

								double a = (totalFare * 4) / 100;
								System.err.println("commisssion: " + a);

								String transactionRefNo = "NA";
								if (txn != null) {
									transactionRefNo = txn.getString("transactionRefNo");
									if (txn.getString("status").equalsIgnoreCase("Success")) {
										count++;
										totaltx += totalFare;
										/*
										 * if(commissionList.length() >i){
										 * JSONObject commissionTransaction =
										 * commissionList.getJSONObject(i);
										 * totalcommission +=
										 * JSONParserUtil.getDouble(
										 * commissionTransaction, "commission");
										 * }
										 */
										totalcommission += a;
										busTicketDTO.setCommsissionAmount(a);
									} else {
										busTicketDTO.setCommsissionAmount(0.0);
									}
									busTicketDTO.setTxnStatus(txn.getString("status"));
								} else {
									busTicketDTO.setTxnStatus("NA");
								}
								String status = val.getString("status");
								String emtTxnId = val.getString("emtTxnId");

								busTicketDTO.setArrTime(arrTime);
								busTicketDTO.setDestination(destination);
								busTicketDTO.setJourneyDate(journeyDate);
								busTicketDTO.setSource(source);
								busTicketDTO.setTotalFare(totalFare);
								busTicketDTO.setTransactionRefNo(transactionRefNo);
								busTicketDTO.setStatus(status);
								busTicketDTO.setEmtTxnId(emtTxnId);
								busTicketDTO.setTicketPnr(ticketPnr);
								busTicketDTO.setOperatorPnr(operatorPnr);

								String strUsr = val.getString("user");
								JSONObject usr = null;
								if (!strUsr.equalsIgnoreCase("null")) {
									usr = val.getJSONObject("user");
								}
								JSONObject usrDtl = null;
								if (usr != null) {
									usrDtl = usr.getJSONObject("userDetail");
								}

								UserDetail userDetail = new UserDetail();
								if (usrDtl != null) {
									String userFname = (String) details.getJSONObject(i).getJSONObject("user")
											.getJSONObject("userDetail").get("firstName");
									String userLname = (String) details.getJSONObject(i).getJSONObject("user")
											.getJSONObject("userDetail").get("lastName");
									String username = (String) details.getJSONObject(i).getJSONObject("user")
											.get("username");
									long accountNo = (long) details.getJSONObject(i).getJSONObject("user")
											.getJSONObject("accountDetail").getLong("accountNumber");

									userDetail.setFirstName(userFname);
									userDetail.setLastName(userLname);
									userDetail.setUsername(username);
									userDetail.setAccountNumber(accountNo);
								}

								busTicketDTO.setUserDetail(userDetail);

								long bookingDate = (long) details.getJSONObject(i).getLong("created");

								Date date = new Date(bookingDate);
								String txnDate = dateFormat.format(date);
								busTicketDTO.setTxnDate(txnDate);
								list.add(busTicketDTO);
							}
							request.setAttribute("totaltx",
									BigDecimal.valueOf(totaltx).setScale(2, RoundingMode.HALF_UP).doubleValue());
							request.setAttribute("totalcommission", BigDecimal.valueOf(totalcommission)
									.setScale(2, RoundingMode.HALF_UP).doubleValue());
							request.setAttribute("totalnotxs", count);
							model.addAttribute("BusDetails", list);
						} else {
							model.addAttribute("BusDetails", list);
						}

					} catch (Exception e) {
						e.printStackTrace();
						return "redirect:/Admin/Home";
					}
					return "Admin/AgentBusDetails";
				}
			}

		}
		return "Admin/Login";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/Agent/BusDeatilsListByDate")
	String getAgentBusdeatilByDatePost(@ModelAttribute PagingDTO dto, HttpServletRequest request, ModelMap model,
			HttpSession session) {

		try {
			dto.setStartDate(ConvertUtil.decodeBase64String(dto.getStartDate()));
			dto.setEndDate(ConvertUtil.decodeBase64String(dto.getEndDate()));
		} catch (Exception e) {

		}

		double totaltx = 0;
		double totalcommission = 0;
		long count = 0;

		UserListDTO result = new UserListDTO();
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		List<BusTicketDTO> list = new ArrayList<>();
		if (sessionCheck != null) {
			String adminAuthority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.AGENT);
			if (adminAuthority != null) {
				if (adminAuthority.contains(Authorities.ADMINISTRATOR)
						&& adminAuthority.contains(Authorities.AUTHENTICATED)) {
					AllUserRequest userRequest = new AllUserRequest();
					userRequest.setPage(0);
					userRequest.setSize(1000);
					dto.setSessionId((String) session.getAttribute("adminSessionId"));
					userRequest.setStatus(UserStatus.ALL);
					AllUserResponse userResponse = appAdminApi.getAgentBusDetailsByDate(dto);
					try {
						if (userResponse.getCode().equals("S00")) {
							JSONObject obj = new JSONObject(userResponse.getResponse());
							JSONObject obj1 = obj.getJSONObject("details");
							JSONArray details = obj1.getJSONArray("userList");
							JSONArray commissionList = obj1.getJSONArray("commissionList");
							for (int i = 0; i < details.length(); i++) {
								BusTicketDTO busTicketDTO = new BusTicketDTO();

								/*
								 * if(commissionList.length() >i){ JSONObject
								 * commissionTransaction =
								 * commissionList.getJSONObject(i);
								 * busTicketDTO.setCommsissionAmount(
								 * JSONParserUtil.getDouble(
								 * commissionTransaction, "commission"));
								 * 
								 * }else {
								 * busTicketDTO.setCommsissionAmount(0.0); }
								 */

								JSONObject val = details.getJSONObject(i);
								JSONObject txn = null;
								if (val != null) {
									if (val.getString("transaction") != null && !val.getString("transaction").isEmpty()
											&& !val.getString("transaction").equalsIgnoreCase("null")) {
										txn = val.getJSONObject("transaction");
									}
								}

								String ticketPnr = val.getString("ticketPnr");
								String operatorPnr = val.getString("operatorPnr");
								String source = val.getString("source");
								String destination = val.getString("destination");
								String journeyDate = val.getString("journeyDate");
								String arrTime = val.getString("arrTime");
								double totalFare = (double) details.getJSONObject(i).getDouble("totalFare");
								String priceRecheckTotalFare = details.getJSONObject(i)
										.getString("priceRecheckTotalFare");

								if (priceRecheckTotalFare != null && !priceRecheckTotalFare.isEmpty()
										&& !priceRecheckTotalFare.equalsIgnoreCase("null")) {

									totalFare = Double.valueOf(priceRecheckTotalFare);

								}

								double a = (totalFare * 4) / 100;
								System.err.println("commisssion: " + a);

								String transactionRefNo = "NA";
								if (txn != null) {
									transactionRefNo = txn.getString("transactionRefNo");
									if (txn.getString("status").equalsIgnoreCase("Success")) {
										count++;
										totaltx += totalFare;
										/*
										 * if(commissionList.length() >i){
										 * JSONObject commissionTransaction =
										 * commissionList.getJSONObject(i);
										 * totalcommission +=
										 * JSONParserUtil.getDouble(
										 * commissionTransaction, "commission");
										 * }
										 */
										totalcommission += a;
										busTicketDTO.setCommsissionAmount(a);
									} else {
										busTicketDTO.setCommsissionAmount(0.0);
									}
									busTicketDTO.setTxnStatus(txn.getString("status"));
								} else {
									busTicketDTO.setTxnStatus("NA");
								}
								String status = val.getString("status");
								String emtTxnId = val.getString("emtTxnId");

								busTicketDTO.setArrTime(arrTime);
								busTicketDTO.setDestination(destination);
								busTicketDTO.setJourneyDate(journeyDate);
								busTicketDTO.setSource(source);
								busTicketDTO.setTotalFare(totalFare);
								busTicketDTO.setTransactionRefNo(transactionRefNo);
								busTicketDTO.setStatus(status);
								busTicketDTO.setEmtTxnId(emtTxnId);
								busTicketDTO.setTicketPnr(ticketPnr);
								busTicketDTO.setOperatorPnr(operatorPnr);

								String strUsr = val.getString("user");
								JSONObject usr = null;
								if (!strUsr.equalsIgnoreCase("null")) {
									usr = val.getJSONObject("user");
								}
								JSONObject usrDtl = null;
								if (usr != null) {
									usrDtl = usr.getJSONObject("userDetail");
								}

								UserDetail userDetail = new UserDetail();
								if (usrDtl != null) {
									String userFname = (String) details.getJSONObject(i).getJSONObject("user")
											.getJSONObject("userDetail").get("firstName");
									String userLname = (String) details.getJSONObject(i).getJSONObject("user")
											.getJSONObject("userDetail").get("lastName");
									String username = (String) details.getJSONObject(i).getJSONObject("user")
											.get("username");
									long accountNo = (long) details.getJSONObject(i).getJSONObject("user")
											.getJSONObject("accountDetail").getLong("accountNumber");

									userDetail.setFirstName(userFname);
									userDetail.setLastName(userLname);
									userDetail.setUsername(username);
									userDetail.setAccountNumber(accountNo);
								}

								busTicketDTO.setUserDetail(userDetail);

								long bookingDate = (long) details.getJSONObject(i).getLong("created");

								Date date = new Date(bookingDate);
								String txnDate = dateFormat.format(date);
								busTicketDTO.setTxnDate(txnDate);
								list.add(busTicketDTO);
							}
							request.setAttribute("totaltx",
									BigDecimal.valueOf(totaltx).setScale(2, RoundingMode.HALF_UP).doubleValue());
							request.setAttribute("totalcommission", BigDecimal.valueOf(totalcommission)
									.setScale(2, RoundingMode.HALF_UP).doubleValue());
							request.setAttribute("totalnotxs", count);
							model.addAttribute("BusDetails", list);
						} else {
							model.addAttribute("BusDetails", list);
						}

					} catch (Exception e) {
						e.printStackTrace();
						return "redirect:/Admin/Home";
					}
					return "Admin/AgentBusDetails";
				}
			}

		}
		return "Admin/Login";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/getSingleTicketDetails", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<com.payqwikweb.app.model.response.bus.ResponseDTO> getSingleTicketDetails(HttpSession session,
			@RequestBody BusTicketDTO dto) {

		com.payqwikweb.app.model.response.bus.ResponseDTO resp = new com.payqwikweb.app.model.response.bus.ResponseDTO();

		com.payqwikweb.app.model.response.bus.ResponseDTO resp1 = new com.payqwikweb.app.model.response.bus.ResponseDTO();

		String sessionId = (String) session.getAttribute("adminSessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					try {

						IsCancellableReq cancellableReq = new IsCancellableReq();
						if (dto.getEmtTxnId() != null) {
							resp = appAdminApi.getSingleTicketTravellerDetails(sessionId, dto.getEmtTxnId());

							BusTicketDTO busTicketDTO = (BusTicketDTO) resp.getExtraInfo();

							cancellableReq.setSeatNo(resp.getSeatNo());
							cancellableReq.setBookId(busTicketDTO.getEmtTransactionScreenId());

							session.setAttribute("emtTxnScreenId", busTicketDTO.getEmtTransactionScreenId());
							session.setAttribute("seatForCancel", resp.getSeatNo());

							resp1 = appAdminApi.isCancellable(cancellableReq);
							if (resp1.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
								IsCancellableResp result = (IsCancellableResp) resp1.getDetails();
								resp.setRefAmt(result.getRefundAmount());
								resp.setCancellationCharges(result.getCancellationCharges());
								resp.setTransactionRefNo(busTicketDTO.getTransactionRefNo());
								resp.setCancelable(result.isCancellable());
							}
							// resp.setDetails(busTicketDTO);
						} else {
							resp.setStatus(ResponseStatus.BAD_REQUEST.getKey());
							resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
							resp.setMessage("Please Enter Emt Txn Id");
						}

					} catch (Exception e) {
						e.printStackTrace();
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
					}
				} else {
					resp.setCode("F00");
					resp.setMessage("Unauthorised access");
					resp.setStatus("FAILED");
				}
			} else {
				resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
				resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
				resp.setMessage("Session expired");

			}
		} else {
			resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
			resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
			resp.setMessage("Session expired");

		}
		return new ResponseEntity<com.payqwikweb.app.model.response.bus.ResponseDTO>(resp, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/getSingleAgentTicketDetails", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<com.payqwikweb.app.model.response.bus.ResponseDTO> getSingleAgentTicketDetails(
			HttpSession session, @RequestBody BusTicketDTO dto) {

		com.payqwikweb.app.model.response.bus.ResponseDTO resp = new com.payqwikweb.app.model.response.bus.ResponseDTO();

		com.payqwikweb.app.model.response.bus.ResponseDTO resp1 = new com.payqwikweb.app.model.response.bus.ResponseDTO();

		String sessionId = (String) session.getAttribute("adminSessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					try {

						IsCancellableReq cancellableReq = new IsCancellableReq();
						if (dto.getEmtTxnId() != null) {
							resp = appAdminApi.getSingleAgentTicketTravellerDetails(sessionId, dto.getEmtTxnId());

							BusTicketDTO busTicketDTO = (BusTicketDTO) resp.getExtraInfo();

							cancellableReq.setSeatNo(resp.getSeatNo());
							cancellableReq.setBookId(busTicketDTO.getEmtTransactionScreenId());

							session.setAttribute("emtTxnScreenId", busTicketDTO.getEmtTransactionScreenId());
							session.setAttribute("seatForCancel", resp.getSeatNo());

							resp1 = appAdminApi.isCancellable(cancellableReq);
							if (resp1.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
								IsCancellableResp result = (IsCancellableResp) resp1.getDetails();
								resp.setRefAmt(result.getRefundAmount());
								resp.setCancellationCharges(result.getCancellationCharges());
								resp.setTransactionRefNo(busTicketDTO.getTransactionRefNo());
								resp.setCancelable(result.isCancellable());
							}
							// resp.setDetails(busTicketDTO);
						} else {
							resp.setStatus(ResponseStatus.BAD_REQUEST.getKey());
							resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
							resp.setMessage("Please Enter Emt Txn Id");
						}

					} catch (Exception e) {
						e.printStackTrace();
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
					}
				} else {
					resp.setCode("F00");
					resp.setMessage("Unauthorised access");
					resp.setStatus("FAILED");
				}
			} else {
				resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
				resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
				resp.setMessage("Session expired");

			}
		} else {
			resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
			resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
			resp.setMessage("Session expired");

		}
		return new ResponseEntity<com.payqwikweb.app.model.response.bus.ResponseDTO>(resp, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/cancelBookedTicket", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<com.payqwikweb.app.model.response.bus.ResponseDTO> cancelBookedTicket(HttpSession session,
			@RequestBody IsCancellableReq dto) {

		com.payqwikweb.app.model.response.bus.ResponseDTO resp = new com.payqwikweb.app.model.response.bus.ResponseDTO();
		String sessionId = (String) session.getAttribute("adminSessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					try {

						String emtTxnScreenId = (String) session.getAttribute("emtTxnScreenId");
						@SuppressWarnings("unchecked")
						List<String> seatNo = (List<String>) session.getAttribute("seatForCancel");

						dto.setSessionId(sessionId);
						dto.setSeatNo(seatNo);
						dto.setBookId(emtTxnScreenId);

						if (dto.getBookId() != null) {

							resp = appAdminApi.cancelBookedTicket(dto);

						} else {
							resp.setStatus(ResponseStatus.BAD_REQUEST.getKey());
							resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
							resp.setMessage("Please Enter Emt Txn Id");
						}

					} catch (Exception e) {
						e.printStackTrace();
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
					}
				} else {
					resp.setCode("F00");
					resp.setMessage("Unauthorised access");
					resp.setStatus("FAILED");
				}
			} else {
				resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
				resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
				resp.setMessage("Session expired");

			}
		} else {
			resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
			resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
			resp.setMessage("Session expired");

		}
		return new ResponseEntity<com.payqwikweb.app.model.response.bus.ResponseDTO>(resp, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "Agent/cancelBookedTicket", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<com.payqwikweb.app.model.response.bus.ResponseDTO> cancelAgentBookedTicket(
			HttpSession session, @RequestBody IsCancellableReq dto) {

		com.payqwikweb.app.model.response.bus.ResponseDTO resp = new com.payqwikweb.app.model.response.bus.ResponseDTO();
		String sessionId = (String) session.getAttribute("adminSessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					try {

						String emtTxnScreenId = (String) session.getAttribute("emtTxnScreenId");
						@SuppressWarnings("unchecked")
						List<String> seatNo = (List<String>) session.getAttribute("seatForCancel");

						dto.setSessionId(sessionId);
						dto.setSeatNo(seatNo);
						dto.setBookId(emtTxnScreenId);

						if (dto.getBookId() != null) {

							resp = appAdminApi.cancelAgentBookedTicket(dto);

						} else {
							resp.setStatus(ResponseStatus.BAD_REQUEST.getKey());
							resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
							resp.setMessage("Please Enter Emt Txn Id");
						}

					} catch (Exception e) {
						e.printStackTrace();
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
					}
				} else {
					resp.setCode("F00");
					resp.setMessage("Unauthorised access");
					resp.setStatus("FAILED");
				}
			} else {
				resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
				resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
				resp.setMessage("Session expired");

			}
		} else {
			resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
			resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
			resp.setMessage("Session expired");

		}
		return new ResponseEntity<com.payqwikweb.app.model.response.bus.ResponseDTO>(resp, HttpStatus.OK);
	}

	// ForPayStstore users

	@RequestMapping(method = RequestMethod.POST, value = "/PayAtStoreList")
	ResponseEntity<UserListDTO> getPayAtSoreListInJSON(@ModelAttribute PagingDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) {
		UserListDTO result = new UserListDTO();
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					AllUserRequest userRequest = new AllUserRequest();
					userRequest.setPage(dto.getPage());
					userRequest.setSize(dto.getSize());
					userRequest.setSessionId(sessionCheck);
					userRequest.setStatus(UserStatus.ALL);
					AllUserResponse userResponse = appAdminApi.getAllUser(userRequest);
					JSONArray data = userResponse.getJsonArray();
					List<AdminUserDetails> userList = new ArrayList<>();
					try {
						if (data != null) {
							for (int i = 0; i < data.length(); i++) {
								AdminUserDetails list = new AdminUserDetails();
								JSONObject jsonobject = data.getJSONObject(i);
								String userType = JSONParserUtil.getString(jsonobject, "userType");
								// String
								// pin=jsonobject.getJSONObject("userDetail").getJSONObject("location").getString("pinCode");
								// String
								// circle=jsonobject.getJSONObject("userDetail").getJSONObject("location").getString("circleName");
								JSONObject userDetails = JSONParserUtil.getObject(jsonobject, "userDetail");
								if (userType.equalsIgnoreCase("User")) {
									JSONObject obj = (JSONObject) jsonobject.getJSONObject("userDetail");
									if (obj.isNull("location")) {

										list.setAuthority(jsonobject.getString("authority"));
										list.setFirstName(
												jsonobject.getJSONObject("userDetail").getString("firstName"));
										list.setLastname(jsonobject.getJSONObject("userDetail").getString("lastName"));
										list.setUsername(jsonobject.getJSONObject("userDetail").getString("name"));
										list.setAccountNumber(
												jsonobject.getJSONObject("accountDetail").getString("accountNumber"));
										list.setGender(jsonobject.getJSONObject("userDetail").getString("gender"));
										list.setDob(jsonobject.getJSONObject("userDetail").getString("dateOfBirth"));
										list.setVijayaAccountNumber(jsonobject.getJSONObject("accountDetail")
												.getString("vijayaAccountNumber"));
										list.setKycNonkyc(jsonobject.getJSONObject("accountDetail")
												.getJSONObject("accountType").getString("name"));
										list.setEmail(jsonobject.getJSONObject("userDetail").getString("email"));
										list.setBalance(jsonobject.getJSONObject("accountDetail").getDouble("balance"));
										list.setContactNo(
												jsonobject.getJSONObject("userDetail").getString("contactNo"));
										list.setEmail(jsonobject.getJSONObject("userDetail").getString("email"));
										list.setMobileToken(jsonobject.getString("mobileToken"));
										list.setPoints(jsonobject.getJSONObject("accountDetail").getLong("points"));
										DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");
										long milliSeconds = Long.parseLong(jsonobject.getString("created"));
										Calendar calendar = Calendar.getInstance();
										calendar.setTimeInMillis(milliSeconds);
										list.setDateOfAccountCreation(formatter.format(calendar.getTime()));
										// LogCat.print("User name " +
										// list.getFirstName());
										userList.add(list);
										model.addAttribute("userlist", userList);
									}

									else {
										list.setPinCode(jsonobject.getJSONObject("userDetail").getJSONObject("location")
												.getString("pinCode"));
										list.setCircleName(jsonobject.getJSONObject("userDetail")
												.getJSONObject("location").getString("circleName"));
										list.setAuthority(jsonobject.getString("authority"));
										list.setFirstName(
												jsonobject.getJSONObject("userDetail").getString("firstName"));
										list.setLastname(jsonobject.getJSONObject("userDetail").getString("lastName"));
										list.setUsername(jsonobject.getJSONObject("userDetail").getString("name"));

										list.setAccountNumber(
												jsonobject.getJSONObject("accountDetail").getString("accountNumber"));
										list.setGender(jsonobject.getJSONObject("userDetail").getString("gender"));
										list.setDob(jsonobject.getJSONObject("userDetail").getString("dateOfBirth"));
										list.setVijayaAccountNumber(jsonobject.getJSONObject("accountDetail")
												.getString("vijayaAccountNumber"));
										list.setKycNonkyc(jsonobject.getJSONObject("accountDetail")
												.getJSONObject("accountType").getString("name"));
										list.setEmail(jsonobject.getJSONObject("userDetail").getString("email"));
										list.setBalance(jsonobject.getJSONObject("accountDetail").getDouble("balance"));
										list.setContactNo(
												jsonobject.getJSONObject("userDetail").getString("contactNo"));
										list.setEmail(jsonobject.getJSONObject("userDetail").getString("email"));
										list.setMobileToken(jsonobject.getString("mobileToken"));
										list.setPoints(jsonobject.getJSONObject("accountDetail").getLong("points"));
										DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");
										long milliSeconds = Long.parseLong(jsonobject.getString("created"));
										Calendar calendar = Calendar.getInstance();
										calendar.setTimeInMillis(milliSeconds);
										list.setDateOfAccountCreation(formatter.format(calendar.getTime()));
										userList.add(list);
										model.addAttribute("userlist", userList);
									}
								}
							}
							result.setSuccess(true);
							result.setFirstPage(userResponse.isFirstPage());
							result.setLastPage(userResponse.isLastPage());
							result.setJsonArray(userList);
							result.setNumberOfElements(userResponse.getNumberOfElements());
							result.setSize(userResponse.getSize());
							result.setTotalPages(userResponse.getTotalPages());
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
					return new ResponseEntity<UserListDTO>(result, HttpStatus.OK);
				}
			}
		}
		return new ResponseEntity<UserListDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/PayAtStoreList")
	public String getPayAtStoreUserList(HttpServletRequest request, HttpSession session) {
		String mobile = request.getParameter("search");

		UserListDTO result = new UserListDTO();
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					AllUserRequest userRequest = new AllUserRequest();
					userRequest.setPage(0);
					userRequest.setSize(10);
					userRequest.setSessionId(sessionCheck);
					userRequest.setStatus(UserStatus.ALL);
					AllUserResponse userResponse = appAdminApi.getAllUser(userRequest);
					JSONArray data = userResponse.getJsonArray();
					List<AdminUserDetails> userList = new ArrayList<>();

					result.setSuccess(true);
					result.setFirstPage(userResponse.isFirstPage());
					result.setLastPage(userResponse.isLastPage());
					result.setJsonArray(userList);
					result.setNumberOfElements(userResponse.getNumberOfElements());
					result.setSize(userResponse.getSize());
					result.setTotalPages(userResponse.getTotalPages());
				}
				//
				// } catch (JSONException e) {
				// e.printStackTrace();
				// }
				return "Admin/PayAtStoreList";
			}
		}

		return "redirect:/Admin/Home";
	}

	// for imagica

	@RequestMapping(method = RequestMethod.GET, value = "/ImagicaTransaction")
	public String getImagicaTransaction(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) throws JSONException {
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {

					return "Admin/ImagicaTransactions";
				}
			}
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/ImagicaTransactions")
	public String getImagicaTransaction(@ModelAttribute TransactionFilter dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) throws JSONException {

		try {

			dto.setFromDate(ConvertUtil.decodeBase64String(dto.getFromDate()));
			dto.setToDate(ConvertUtil.decodeBase64String(dto.getToDate()));

			String sessionCheck = (String) session.getAttribute("adminSessionId");
			if (sessionCheck != null) {
				String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
				if (authority != null) {
					if (authority.contains(Authorities.ADMINISTRATOR)
							&& authority.contains(Authorities.AUTHENTICATED)) {
						model.addAttribute(ModelMapKey.MESSAGE,
								" Imagica Transactions From " + dto.getFromDate() + " to " + dto.getToDate());
						AllTransactionRequest transRequest = new AllTransactionRequest();
						transRequest.setPage(0);
						transRequest.setSize(100000);
						transRequest.setStartDate(dto.getFromDate());
						transRequest.setEndDate(dto.getToDate());
						// transRequest.setReportType(dto.getServiceType());
						transRequest.setSessionId((String) session.getAttribute("adminSessionId"));
						AllTransactionResponse allTransaction = appAdminApi.getImagicaTransaction(transRequest);
						if (allTransaction.isSuccess()) {
							List<TransactionReport> reports = new ArrayList<>();
							JSONArray reportArray = allTransaction.getJsonArray();
							if (reportArray != null) {
								for (int i = 0; i < reportArray.length(); i++) {
									JSONObject json = reportArray.getJSONObject(i);
									TransactionReport report = new TransactionReport();
									report.setContactNo(JSONParserUtil.getString(json, "contactNo"));

									report.setUsername(JSONParserUtil.getString(json, "username"));
									report.setAmount(JSONParserUtil.getDouble(json, "amount"));
									report.setDate(JSONParserUtil.getString(json, "date"));
									report.setService(JSONParserUtil.getString(json, "service"));
									/*
									 * report.setDebit(JSONParserUtil.getBoolean
									 * (json, "debit"));
									 */
									report.setTransactionRefNo(JSONParserUtil.getString(json, "transactionRefNo"));
									report.setStatus(com.payqwikweb.model.web.Status
											.valueOf(JSONParserUtil.getString(json, "status")));
									report.setDescription(JSONParserUtil.getString(json, "description"));
									reports.add(report);
								}
								model.addAttribute("transactions", reports);
							}
						}
						return "Admin/ImagicaTransactions";
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return "redirect:/Admin/Home";
	}

	// for Debit transaction

	@RequestMapping(method = RequestMethod.GET, value = "/DebitTransaction")
	public String getDebitTransaction(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) throws JSONException {
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {

					return "Admin/DebitTransaction";
				}
			}
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/DebitTransactions")
	public String getDebitTransaction(@ModelAttribute TransactionFilter dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) throws JSONException {
		try {

			dto.setFromDate(ConvertUtil.decodeBase64String(dto.getFromDate()));
			dto.setToDate(ConvertUtil.decodeBase64String(dto.getToDate()));

			String sessionCheck = (String) session.getAttribute("adminSessionId");
			if (sessionCheck != null) {
				String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
				if (authority != null) {
					if (authority.contains(Authorities.ADMINISTRATOR)
							&& authority.contains(Authorities.AUTHENTICATED)) {
						model.addAttribute(ModelMapKey.MESSAGE,
								"Debit Transactions From " + dto.getFromDate() + " to " + dto.getToDate());
						AllTransactionRequest transRequest = new AllTransactionRequest();
						transRequest.setPage(0);
						transRequest.setSize(100000);
						transRequest.setStartDate(dto.getFromDate());
						transRequest.setEndDate(dto.getToDate());
						// transRequest.setReportType(dto.getServiceType());
						transRequest.setSessionId((String) session.getAttribute("adminSessionId"));
						AllTransactionResponse allTransaction = appAdminApi.getDebitTransaction(transRequest);
						if (allTransaction.isSuccess()) {
							List<TransactionReport> reports = new ArrayList<>();
							JSONArray reportArray = allTransaction.getJsonArray();
							if (reportArray != null) {
								for (int i = 0; i < reportArray.length(); i++) {
									JSONObject json = reportArray.getJSONObject(i);
									TransactionReport report = new TransactionReport();
									report.setContactNo(JSONParserUtil.getString(json, "contactNo"));

									report.setUsername(JSONParserUtil.getString(json, "username"));
									report.setAmount(JSONParserUtil.getDouble(json, "amount"));
									report.setDate(JSONParserUtil.getString(json, "date"));
									report.setService(JSONParserUtil.getString(json, "service"));
									/*
									 * report.setDebit(JSONParserUtil.getBoolean
									 * (json, "debit"));
									 */
									report.setTransactionRefNo(JSONParserUtil.getString(json, "transactionRefNo"));
									report.setStatus(com.payqwikweb.model.web.Status
											.valueOf(JSONParserUtil.getString(json, "status")));
									report.setDescription(JSONParserUtil.getString(json, "description"));
									reports.add(report);
								}
								model.addAttribute("transactions", reports);
							}
							return "Admin/DebitTransactions";
						}
						return "Admin/DebitTransaction";
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return "redirect:/Admin/Home";
	}

	// for Credit transaction

	@RequestMapping(method = RequestMethod.GET, value = "/CreditTransaction")
	public String getCreditTransaction(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) throws JSONException {
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {

					return "Admin/CreditTransaction";
				}
			}
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/CreditTransactions")
	public String getCreditTransaction(@ModelAttribute TransactionFilter dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) throws JSONException {
		try {

			dto.setFromDate(ConvertUtil.decodeBase64String(dto.getFromDate()));
			dto.setToDate(ConvertUtil.decodeBase64String(dto.getToDate()));

			String sessionCheck = (String) session.getAttribute("adminSessionId");
			if (sessionCheck != null) {
				String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
				if (authority != null) {
					if (authority.contains(Authorities.ADMINISTRATOR)
							&& authority.contains(Authorities.AUTHENTICATED)) {
						model.addAttribute(ModelMapKey.MESSAGE,
								"Credit Transactions From " + dto.getFromDate() + " to " + dto.getToDate());
						AllTransactionRequest transRequest = new AllTransactionRequest();
						transRequest.setPage(0);
						transRequest.setSize(100000);
						transRequest.setStartDate(dto.getFromDate());
						transRequest.setEndDate(dto.getToDate());
						// transRequest.setReportType(dto.getServiceType());
						transRequest.setSessionId((String) session.getAttribute("adminSessionId"));
						AllTransactionResponse allTransaction = appAdminApi.getCreditTransaction(transRequest);
						if (allTransaction.isSuccess()) {
							List<TransactionReport> reports = new ArrayList<>();
							JSONArray reportArray = allTransaction.getJsonArray();
							if (reportArray != null) {
								for (int i = 0; i < reportArray.length(); i++) {
									JSONObject json = reportArray.getJSONObject(i);
									TransactionReport report = new TransactionReport();
									report.setContactNo(JSONParserUtil.getString(json, "contactNo"));

									report.setUsername(JSONParserUtil.getString(json, "username"));
									report.setAmount(JSONParserUtil.getDouble(json, "amount"));
									report.setDate(JSONParserUtil.getString(json, "date"));
									report.setService(JSONParserUtil.getString(json, "service"));
									/*
									 * report.setDebit(JSONParserUtil.getBoolean
									 * (json, "debit"));
									 */
									report.setTransactionRefNo(JSONParserUtil.getString(json, "transactionRefNo"));
									report.setStatus(com.payqwikweb.model.web.Status
											.valueOf(JSONParserUtil.getString(json, "status")));
									report.setDescription(JSONParserUtil.getString(json, "description"));
									reports.add(report);
								}
								model.addAttribute("transactions", reports);
							}
							return "Admin/CreditTransactions";
						}
						return "Admin/CreditTransaction";
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/TreatCardPlansList")
	public String getTreatCardPlans(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model, SessionDTO dto) throws JSONException {

		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String adminAuthority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (adminAuthority != null) {
				if (adminAuthority.contains(Authorities.ADMINISTRATOR)
						&& adminAuthority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionCheck);
					TreatCardPlansResponse resp = appAdminApi.getListPlans(dto);
					JSONArray arr = (JSONArray) resp.getJsonArray();
					List<TreatCardPlansResponse> list = new ArrayList<TreatCardPlansResponse>();
					for (int i = 0; i < arr.length(); i++) {
						TreatCardPlansResponse re = new TreatCardPlansResponse();
						JSONObject json = arr.getJSONObject(i);
						re.setDays(JSONParserUtil.getString(json, "days"));
						re.setAmount(JSONParserUtil.getDouble(json, "amount"));
						re.setPlanStatus(JSONParserUtil.getString(json, "status"));
						list.add(re);
					}
					model.addAttribute("list", list);
					return "/Admin/TreatCardPlansList";
				}
			}
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(value = "/EditPlans", method = RequestMethod.POST)
	ResponseEntity<TreatCardPlansResponse> updatePlans(@ModelAttribute TreatCardPlanRequest dto,
			HttpServletRequest request, HttpSession session, ModelMap model, HttpServletResponse response)
			throws JSONException {
		TreatCardPlansResponse resp = new TreatCardPlansResponse();
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String adminAuthority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (adminAuthority != null) {
				if (adminAuthority.contains(Authorities.ADMINISTRATOR)
						&& adminAuthority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionCheck);
					resp = appAdminApi.updatePlans(dto);
					JSONArray arr = (JSONArray) resp.getJsonArray();
					for (int i = 0; i < arr.length(); i++) {
						JSONObject jsonobject = arr.getJSONObject(i);
						resp.setDays(jsonobject.getString("days"));
						resp.setAmount(jsonobject.getDouble("amount"));
						resp.setPlanStatus(jsonobject.getString("status"));

					}
					resp.setJsonArray(null);
				}
			}
		}
		return new ResponseEntity<TreatCardPlansResponse>(resp, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/FlightdeatilListByDate")
	String getFlightdeatilByDate(@ModelAttribute PagingDTO dto, HttpServletRequest request, ModelMap model,
			HttpSession session) {
		UserListDTO result = new UserListDTO();
		String sessionCheck = (String) session.getAttribute("adminSessionId");

		if (sessionCheck != null) {
			String adminAuthority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (adminAuthority != null) {
				if (adminAuthority.contains(Authorities.ADMINISTRATOR)
						&& adminAuthority.contains(Authorities.AUTHENTICATED)) {
					AllUserRequest userRequest = new AllUserRequest();
					userRequest.setPage(0);
					userRequest.setSize(1000);
					userRequest.setSessionId((String) session.getAttribute("adminSessionId"));
					userRequest.setStatus(UserStatus.ALL);
					dto.setSessionId((String) session.getAttribute("adminSessionId"));
					AllUserResponse userResponse = appAdminApi.getAllFlightsDetailByDate(dto);

					try {
						List<FlightTicketAdminDTO> userList = new ArrayList<>();
						if (userResponse.getCode().equals("S00")) {
							JSONObject obj = new JSONObject(userResponse.getResponse());
							JSONArray data = obj.getJSONArray("details");

							if (data != null) {

								for (int i = 0; i < data.length(); i++) {

									FlightTicketAdminDTO list = new FlightTicketAdminDTO();
									JSONObject userDetail = null;
									JSONObject account = null;
									JSONObject transaction = data.getJSONObject(i).getJSONObject("transaction");
									JSONObject user = data.getJSONObject(i).getJSONObject("user");
									if (user != null) {
										userDetail = user.getJSONObject("userDetail");
										list.setUserDetails(userDetail.getString("firstName") + " "
												+ userDetail.getString("lastName"));
										list.setUserName(user.getString("username"));
									}

									list.setBookingStatus(data.getJSONObject(i).getString("flightStatus"));
									double commissionAmount = data.getJSONObject(i).getDouble("commissionAmt");
									double totalCommissionAmount = commissionAmount - 200;
									list.setCommsissionAmount(BigDecimal.valueOf(totalCommissionAmount)
											.setScale(3, RoundingMode.HALF_UP).doubleValue());

									if (transaction != null) {
										list.setTransactionRefNo(transaction.getString("transactionRefNo"));
										list.setTransactionStatus(transaction.getString("status"));
										long dt = transaction.getLong("created");
										Date date = new Date(dt);
										String txnDate = dateFormat.format(date);
										list.setTxnDate(txnDate);
										account = transaction.getJSONObject("account");
									} else {
										list.setTransactionStatus(data.getJSONObject(i).getString("paymentStatus"));
									}

									if (account != null) {
										list.setAccountNumber(account.getLong("accountNumber"));
									}

									String bookingRefId = data.getJSONObject(i).getString("bookingRefId");

									try {
										String a[] = data.getJSONObject(i).getString("bookingRefId").split("#");

										if (a.length > 1) {
											bookingRefId = a[1];
										}
									} catch (Exception e) {
										e.printStackTrace();
									}

									list.setBookingRefId(bookingRefId);

									list.setPaymentAmount(data.getJSONObject(i).getDouble("paymentAmount"));
									list.setFlightTicketId(data.getJSONObject(i).getLong("id"));
									list.setTicketDetails(data.getJSONObject(i).getString("ticketDetails"));

									list.setFlightNameOnward(data.getJSONObject(i).getString("flightNumberOnward"));
									list.setFlightNameReturn(data.getJSONObject(i).getString("flightNumberReturn"));
									list.setTripType(data.getJSONObject(i).getString("tripType"));

									userList.add(list);

								}
							}
							model.addAttribute("FlightDeatils", userList);
						} else {
							model.addAttribute("FlightDeatils", userList);
						}

					} catch (Exception e) {
						e.printStackTrace();
						return "Admin/Login";
					}
					return "Admin/FlightDetail";
				}
			}

		}
		return "Admin/Login";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/FlightdeatilListByDate")
	String getFlightdeatilByDatePost(@ModelAttribute PagingDTO dto, HttpServletRequest request, ModelMap model,
			HttpSession session) {
		UserListDTO result = new UserListDTO();
		String sessionCheck = (String) session.getAttribute("adminSessionId");

		try {
			dto.setStartDate(ConvertUtil.decodeBase64String(dto.getStartDate()));
			dto.setEndDate(ConvertUtil.decodeBase64String(dto.getEndDate()));
		} catch (Exception e) {

		}

		if (sessionCheck != null) {
			String adminAuthority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (adminAuthority != null) {
				if (adminAuthority.contains(Authorities.ADMINISTRATOR)
						&& adminAuthority.contains(Authorities.AUTHENTICATED)) {
					AllUserRequest userRequest = new AllUserRequest();
					userRequest.setPage(0);
					userRequest.setSize(1000);
					userRequest.setSessionId((String) session.getAttribute("adminSessionId"));
					userRequest.setStatus(UserStatus.ALL);
					dto.setSessionId((String) session.getAttribute("adminSessionId"));
					AllUserResponse userResponse = appAdminApi.getAllFlightsDetailByDate(dto);

					try {
						List<FlightTicketAdminDTO> userList = new ArrayList<>();
						if (userResponse.getCode().equals("S00")) {
							JSONObject obj = new JSONObject(userResponse.getResponse());
							JSONArray data = obj.getJSONArray("details");

							if (data != null) {

								for (int i = 0; i < data.length(); i++) {

									FlightTicketAdminDTO list = new FlightTicketAdminDTO();
									JSONObject userDetail = null;
									JSONObject account = null;
									JSONObject transaction = data.getJSONObject(i).getJSONObject("transaction");
									JSONObject user = data.getJSONObject(i).getJSONObject("user");
									if (user != null) {
										userDetail = user.getJSONObject("userDetail");
										list.setUserDetails(userDetail.getString("firstName") + " "
												+ userDetail.getString("lastName"));
										list.setUserName(user.getString("username"));
									}

									list.setBookingStatus(data.getJSONObject(i).getString("flightStatus"));
									double commissionAmount = data.getJSONObject(i).getDouble("commissionAmt");
									double totalCommissionAmount = commissionAmount - 200;
									list.setCommsissionAmount(BigDecimal.valueOf(totalCommissionAmount)
											.setScale(3, RoundingMode.HALF_UP).doubleValue());

									if (transaction != null) {
										list.setTransactionRefNo(transaction.getString("transactionRefNo"));
										list.setTransactionStatus(transaction.getString("status"));
										long dt = transaction.getLong("created");
										Date date = new Date(dt);
										String txnDate = dateFormat.format(date);
										list.setTxnDate(txnDate);
										account = transaction.getJSONObject("account");
									} else {
										list.setTransactionStatus(data.getJSONObject(i).getString("paymentStatus"));
									}

									if (account != null) {
										list.setAccountNumber(account.getLong("accountNumber"));
									}

									String bookingRefId = data.getJSONObject(i).getString("bookingRefId");

									try {
										String a[] = data.getJSONObject(i).getString("bookingRefId").split("#");

										if (a.length > 1) {
											bookingRefId = a[1];
										}
									} catch (Exception e) {
										e.printStackTrace();
									}

									list.setBookingRefId(bookingRefId);

									list.setPaymentAmount(data.getJSONObject(i).getDouble("paymentAmount"));
									list.setFlightTicketId(data.getJSONObject(i).getLong("id"));
									list.setTicketDetails(data.getJSONObject(i).getString("ticketDetails"));

									list.setFlightNameOnward(data.getJSONObject(i).getString("flightNumberOnward"));
									list.setFlightNameReturn(data.getJSONObject(i).getString("flightNumberReturn"));
									list.setTripType(data.getJSONObject(i).getString("tripType"));

									userList.add(list);

								}
							}
							model.addAttribute("FlightDeatils", userList);
						} else {
							model.addAttribute("FlightDeatils", userList);
						}

					} catch (Exception e) {
						e.printStackTrace();
						return "Admin/Login";
					}
					return "Admin/FlightDetail";
				}
			}

		}
		return "Admin/Login";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/Agent/FlightdeatilListByDate")
	String getAgentFlightdeatilByDate(@ModelAttribute PagingDTO dto, HttpServletRequest request, ModelMap model,
			HttpSession session) {
		UserListDTO result = new UserListDTO();
		String sessionCheck = (String) session.getAttribute("adminSessionId");

		if (sessionCheck != null) {
			String adminAuthority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.AGENT);
			if (adminAuthority != null) {
				if (adminAuthority.contains(Authorities.ADMINISTRATOR)
						&& adminAuthority.contains(Authorities.AUTHENTICATED)) {
					AllUserRequest userRequest = new AllUserRequest();
					userRequest.setPage(0);
					userRequest.setSize(1000);
					userRequest.setSessionId((String) session.getAttribute("adminSessionId"));
					userRequest.setStatus(UserStatus.ALL);
					dto.setSessionId((String) session.getAttribute("adminSessionId"));
					AllUserResponse userResponse = appAdminApi.getAllAgentFlightsDetailByDate(dto);

					try {
						List<FlightTicketAdminDTO> userList = new ArrayList<>();
						if (userResponse.getCode().equals("S00")) {
							JSONObject obj = new JSONObject(userResponse.getResponse());
							JSONArray data = obj.getJSONArray("details");

							if (data != null) {

								for (int i = 0; i < data.length(); i++) {

									FlightTicketAdminDTO list = new FlightTicketAdminDTO();
									JSONObject userDetail = null;
									JSONObject account = null;
									JSONObject transaction = data.getJSONObject(i).getJSONObject("transaction");
									JSONObject user = data.getJSONObject(i).getJSONObject("user");
									if (user != null) {
										userDetail = user.getJSONObject("userDetail");
										list.setUserDetails(userDetail.getString("firstName") + " "
												+ userDetail.getString("lastName"));
										list.setUserName(user.getString("username"));
									}

									list.setBookingStatus(data.getJSONObject(i).getString("flightStatus"));
									double commissionAmount = data.getJSONObject(i).getDouble("commissionAmt");
									double totalCommissionAmount = commissionAmount - 200;
									list.setCommsissionAmount(BigDecimal.valueOf(totalCommissionAmount)
											.setScale(3, RoundingMode.HALF_UP).doubleValue());

									if (transaction != null) {
										list.setTransactionRefNo(transaction.getString("transactionRefNo"));
										list.setTransactionStatus(transaction.getString("status"));
										long dt = transaction.getLong("created");
										Date date = new Date(dt);
										String txnDate = dateFormat.format(date);
										list.setTxnDate(txnDate);
										account = transaction.getJSONObject("account");
									} else {
										list.setTransactionStatus(data.getJSONObject(i).getString("paymentStatus"));
									}

									if (account != null) {
										list.setAccountNumber(account.getLong("accountNumber"));
									}

									String bookingRefId = data.getJSONObject(i).getString("bookingRefId");

									try {
										String a[] = data.getJSONObject(i).getString("bookingRefId").split("#");

										if (a.length > 1) {
											bookingRefId = a[1];
										}
									} catch (Exception e) {
										e.printStackTrace();
									}

									list.setBookingRefId(bookingRefId);

									list.setPaymentAmount(data.getJSONObject(i).getDouble("paymentAmount"));
									list.setFlightTicketId(data.getJSONObject(i).getLong("id"));
									list.setTicketDetails(data.getJSONObject(i).getString("ticketDetails"));

									list.setFlightNameOnward(data.getJSONObject(i).getString("flightNumberOnward"));
									list.setFlightNameReturn(data.getJSONObject(i).getString("flightNumberReturn"));
									list.setTripType(data.getJSONObject(i).getString("tripType"));

									userList.add(list);

								}
							}
							model.addAttribute("FlightDeatils", userList);
						} else {
							model.addAttribute("FlightDeatils", userList);
						}

					} catch (Exception e) {
						e.printStackTrace();
						return "Admin/Login";
					}
					return "Admin/AgentFlightDetail";
				}
			}
		}
		return "Admin/Login";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/Agent/FlightdeatilListByDate")
	String getAgentFlightdeatilByDatePost(@ModelAttribute PagingDTO dto, HttpServletRequest request, ModelMap model,
			HttpSession session) {

		try {
			dto.setStartDate(ConvertUtil.decodeBase64String(dto.getStartDate()));
			dto.setEndDate(ConvertUtil.decodeBase64String(dto.getEndDate()));
		} catch (Exception e) {
			e.printStackTrace();
		}

		UserListDTO result = new UserListDTO();
		String sessionCheck = (String) session.getAttribute("adminSessionId");

		if (sessionCheck != null) {
			String adminAuthority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.AGENT);
			if (adminAuthority != null) {
				if (adminAuthority.contains(Authorities.ADMINISTRATOR)
						&& adminAuthority.contains(Authorities.AUTHENTICATED)) {
					AllUserRequest userRequest = new AllUserRequest();
					userRequest.setPage(0);
					userRequest.setSize(1000);
					userRequest.setSessionId((String) session.getAttribute("adminSessionId"));
					userRequest.setStatus(UserStatus.ALL);
					dto.setSessionId((String) session.getAttribute("adminSessionId"));
					AllUserResponse userResponse = appAdminApi.getAllAgentFlightsDetailByDate(dto);

					try {
						List<FlightTicketAdminDTO> userList = new ArrayList<>();
						if (userResponse.getCode().equals("S00")) {
							JSONObject obj = new JSONObject(userResponse.getResponse());
							JSONArray data = obj.getJSONArray("details");

							if (data != null) {

								for (int i = 0; i < data.length(); i++) {

									FlightTicketAdminDTO list = new FlightTicketAdminDTO();
									JSONObject userDetail = null;
									JSONObject account = null;
									JSONObject transaction = data.getJSONObject(i).getJSONObject("transaction");
									JSONObject user = data.getJSONObject(i).getJSONObject("user");
									if (user != null) {
										userDetail = user.getJSONObject("userDetail");
										list.setUserDetails(userDetail.getString("firstName") + " "
												+ userDetail.getString("lastName"));
										list.setUserName(user.getString("username"));
									}

									list.setBookingStatus(data.getJSONObject(i).getString("flightStatus"));
									double commissionAmount = data.getJSONObject(i).getDouble("commissionAmt");
									double totalCommissionAmount = commissionAmount - 200;
									list.setCommsissionAmount(BigDecimal.valueOf(totalCommissionAmount)
											.setScale(3, RoundingMode.HALF_UP).doubleValue());

									if (transaction != null) {
										list.setTransactionRefNo(transaction.getString("transactionRefNo"));
										list.setTransactionStatus(transaction.getString("status"));
										long dt = transaction.getLong("created");
										Date date = new Date(dt);
										String txnDate = dateFormat.format(date);
										list.setTxnDate(txnDate);
										account = transaction.getJSONObject("account");
									} else {
										list.setTransactionStatus(data.getJSONObject(i).getString("paymentStatus"));
									}

									if (account != null) {
										list.setAccountNumber(account.getLong("accountNumber"));
									}

									String bookingRefId = data.getJSONObject(i).getString("bookingRefId");

									try {
										String a[] = data.getJSONObject(i).getString("bookingRefId").split("#");

										if (a.length > 1) {
											bookingRefId = a[1];
										}
									} catch (Exception e) {
										e.printStackTrace();
									}

									list.setBookingRefId(bookingRefId);

									list.setPaymentAmount(data.getJSONObject(i).getDouble("paymentAmount"));
									list.setFlightTicketId(data.getJSONObject(i).getLong("id"));
									list.setTicketDetails(data.getJSONObject(i).getString("ticketDetails"));

									list.setFlightNameOnward(data.getJSONObject(i).getString("flightNumberOnward"));
									list.setFlightNameReturn(data.getJSONObject(i).getString("flightNumberReturn"));
									list.setTripType(data.getJSONObject(i).getString("tripType"));

									userList.add(list);

								}
							}
							model.addAttribute("FlightDeatils", userList);
						} else {
							model.addAttribute("FlightDeatils", userList);
						}

					} catch (Exception e) {
						e.printStackTrace();
						return "Admin/Login";
					}
					return "Admin/AgentFlightDetail";
				}
			}
		}
		return "Admin/Login";
	}

	/*
	 * @RequestMapping(method = RequestMethod.GET, value =
	 * "/TKOrderDeatilsList") String getTKOrderDeatils(@ModelAttribute PagingDTO
	 * dto, HttpServletRequest request, ModelMap model, HttpSession session) {
	 * UserListDTO result = new UserListDTO(); String sessionCheck = (String)
	 * session.getAttribute("adminSessionId"); List<TKOrderDetailsDTO> list=new
	 * ArrayList<>(); if (sessionCheck != null) { String adminAuthority =
	 * authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER); if
	 * (adminAuthority != null) { if
	 * (adminAuthority.contains(Authorities.ADMINISTRATOR) &&
	 * adminAuthority.contains(Authorities.AUTHENTICATED)) {
	 * 
	 * return "Admin/TKOrderDetails2"; } } } return "redirect:/Admin/Home"; }
	 */

	@RequestMapping(method = RequestMethod.GET, value = "/TKOrderDeatilsList")
	String getTKOrderDeatil(@ModelAttribute PagingDTO dto, HttpServletRequest request, ModelMap model,
			HttpSession session) {
		UserListDTO result = new UserListDTO();
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		List<TKOrderDetailsDTO> list = new ArrayList<>();
		if (sessionCheck != null) {
			String adminAuthority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (adminAuthority != null) {
				if (adminAuthority.contains(Authorities.ADMINISTRATOR)
						&& adminAuthority.contains(Authorities.AUTHENTICATED)) {
					AllUserRequest userRequest = new AllUserRequest();
					userRequest.setPage(dto.getPage());
					userRequest.setSize(dto.getSize());
					userRequest.setSessionId((String) session.getAttribute("adminSessionId"));
					userRequest.setStatus(UserStatus.ALL);
					userRequest.setPageable(true);
					AllUserResponse userResponse = appAdminApi.getTKOrderDetails(userRequest);
					try {
						if (userResponse.getCode().equals("S00")) {
							JSONObject obj = new JSONObject(userResponse.getResponse());
							JSONObject obj1 = obj.getJSONObject("details");
							JSONArray details = obj1.getJSONArray("userList");
							JSONArray commissionList = obj1.getJSONArray("commissionList");

							for (int i = 0; i < details.length(); i++) {
								TKOrderDetailsDTO orderDetail = new TKOrderDetailsDTO();
								if (commissionList.length() > i) {
									JSONObject commissionTransaction = commissionList.getJSONObject(i);
									orderDetail.setCommsissionAmount(
											JSONParserUtil.getDouble(commissionTransaction, "commission"));
								} else {
									orderDetail.setCommsissionAmount(0.0);
								}

								JSONObject val = details.getJSONObject(i);
								// JSONObject txn=null;
								// if (val!=null) {
								// txn=val.getJSONObject("transaction");
								// }

								long id = details.getJSONObject(i).getLong("id");
								// System.out.println("ID--"+id);
								String date = details.getJSONObject(i).getString("date");
								String order_outlet_id = details.getJSONObject(i).getString("order_outlet_id");
								String mail_id = details.getJSONObject(i).getString("mail_id");
								// System.out.println(date+"---"+order_outlet_id+"--"+mail_id+"-");
								// long created =
								// details.getJSONObject(i).getLong("created");
								String train_number = details.getJSONObject(i).getString("train_number");
								String orderStatus = details.getJSONObject(i).getString("orderStatus");
								String station_code = details.getJSONObject(i).getString("station_code");
								String transactionRefNo = details.getJSONObject(i).getString("transactionRefNo");
								String userOrderId = details.getJSONObject(i).getString("userOrderId");
								String seat = details.getJSONObject(i).getString("seat");
								String eta = details.getJSONObject(i).getString("eta");
								String pnr = details.getJSONObject(i).getString("pnr");
								String coach = details.getJSONObject(i).getString("coach");
								String contact_no = details.getJSONObject(i).getString("contact_no");
								String name = details.getJSONObject(i).getString("name");
								double totalCustomerPayable = details.getJSONObject(i)
										.getDouble("totalCustomerPayable");
								String txnStatus = details.getJSONObject(i).getString("txnStatus");
								orderDetail.setCoach(coach);
								orderDetail.setContact_no(contact_no);
								orderDetail.setTxnStatus(txnStatus);
								orderDetail.setOrderDate(date);
								orderDetail.setEta(eta);
								orderDetail.setMail_id(mail_id);
								orderDetail.setName(name);
								orderDetail.setOrder_outlet_id(order_outlet_id);
								orderDetail.setOrderStatus(orderStatus);
								orderDetail.setPnr(pnr);
								orderDetail.setSeat(seat);
								orderDetail.setStation_code(station_code);
								orderDetail.setTrain_number(train_number);
								orderDetail.setTransactionRefNo(transactionRefNo);
								orderDetail.setUserOrderId(userOrderId);
								orderDetail.setTotalCustomerPayable(totalCustomerPayable);

								// if (txn!=null) {
								// transactionRefNo=txn.getString("transactionRefNo");
								// }
								String strUsr = val.getString("user");
								JSONObject usr = null;
								if (!strUsr.equalsIgnoreCase("null")) {
									usr = val.getJSONObject("user");
								}
								JSONObject usrDtl = null;
								if (usr != null) {
									usrDtl = usr.getJSONObject("userDetail");
								}

								UserDetail userDetail = new UserDetail();
								if (usrDtl != null) {
									String userFname = (String) details.getJSONObject(i).getJSONObject("user")
											.getJSONObject("userDetail").get("firstName");
									String userLname = (String) details.getJSONObject(i).getJSONObject("user")
											.getJSONObject("userDetail").get("lastName");
									String username = (String) details.getJSONObject(i).getJSONObject("user")
											.get("username");
									long accountNo = (long) details.getJSONObject(i).getJSONObject("user")
											.getJSONObject("accountDetail").getLong("accountNumber");

									userDetail.setFirstName(userFname);
									userDetail.setLastName(userLname);
									userDetail.setUsername(username);
									userDetail.setAccountNumber(accountNo);
								}
								orderDetail.setUserDetail(userDetail);

								long txnDate = details.getJSONObject(i).getLong("created");
								Date date2 = new Date(txnDate);
								String txnDate2 = dateFormat.format(date2);
								orderDetail.setTxnDate(txnDate2);

								DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");
								// long milliSeconds =
								// Long.parseLong(jsonobject.getString("created"));
								Calendar calendar = Calendar.getInstance();
								calendar.setTimeInMillis(txnDate);
								orderDetail.setCreationTime(formatter.format(calendar.getTime()));

								list.add(orderDetail);
							}
							result.setJsonArray(list);
							result.setTotalPages(obj.getLong("count"));
							result.setSize(userRequest.getSize());

							model.addAttribute("OrderDetails", list);
						} else {
							model.addAttribute("OrderDetails", list);
						}

					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
		return "Admin/TKOrderDetails";
		// return new ResponseEntity<UserListDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/TKOrderDeatilsList")
	String getTKOrderDeatilPost(@ModelAttribute PagingDTO dto, HttpServletRequest request, ModelMap model,
			HttpSession session) {

		try {
			dto.setStartDate(ConvertUtil.decodeBase64String(dto.getStartDate()));
			dto.setEndDate(ConvertUtil.decodeBase64String(dto.getEndDate()));
		} catch (Exception e) {

		}

		UserListDTO result = new UserListDTO();
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		List<TKOrderDetailsDTO> list = new ArrayList<>();
		if (sessionCheck != null) {
			String adminAuthority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (adminAuthority != null) {
				if (adminAuthority.contains(Authorities.ADMINISTRATOR)
						&& adminAuthority.contains(Authorities.AUTHENTICATED)) {
					AllUserRequest userRequest = new AllUserRequest();
					userRequest.setPage(dto.getPage());
					userRequest.setSize(dto.getSize());
					userRequest.setSessionId((String) session.getAttribute("adminSessionId"));
					userRequest.setStatus(UserStatus.ALL);
					userRequest.setPageable(true);
					AllUserResponse userResponse = appAdminApi.getTKOrderDetails(userRequest);
					try {
						if (userResponse.getCode().equals("S00")) {
							JSONObject obj = new JSONObject(userResponse.getResponse());
							JSONObject obj1 = obj.getJSONObject("details");
							JSONArray details = obj1.getJSONArray("userList");
							JSONArray commissionList = obj1.getJSONArray("commissionList");

							for (int i = 0; i < details.length(); i++) {
								TKOrderDetailsDTO orderDetail = new TKOrderDetailsDTO();
								if (commissionList.length() > i) {
									JSONObject commissionTransaction = commissionList.getJSONObject(i);
									orderDetail.setCommsissionAmount(
											JSONParserUtil.getDouble(commissionTransaction, "commission"));
								} else {
									orderDetail.setCommsissionAmount(0.0);
								}

								JSONObject val = details.getJSONObject(i);
								// JSONObject txn=null;
								// if (val!=null) {
								// txn=val.getJSONObject("transaction");
								// }

								long id = details.getJSONObject(i).getLong("id");
								// System.out.println("ID--"+id);
								String date = details.getJSONObject(i).getString("date");
								String order_outlet_id = details.getJSONObject(i).getString("order_outlet_id");
								String mail_id = details.getJSONObject(i).getString("mail_id");
								// System.out.println(date+"---"+order_outlet_id+"--"+mail_id+"-");
								// long created =
								// details.getJSONObject(i).getLong("created");
								String train_number = details.getJSONObject(i).getString("train_number");
								String orderStatus = details.getJSONObject(i).getString("orderStatus");
								String station_code = details.getJSONObject(i).getString("station_code");
								String transactionRefNo = details.getJSONObject(i).getString("transactionRefNo");
								String userOrderId = details.getJSONObject(i).getString("userOrderId");
								String seat = details.getJSONObject(i).getString("seat");
								String eta = details.getJSONObject(i).getString("eta");
								String pnr = details.getJSONObject(i).getString("pnr");
								String coach = details.getJSONObject(i).getString("coach");
								String contact_no = details.getJSONObject(i).getString("contact_no");
								String name = details.getJSONObject(i).getString("name");
								double totalCustomerPayable = details.getJSONObject(i)
										.getDouble("totalCustomerPayable");
								String txnStatus = details.getJSONObject(i).getString("txnStatus");
								orderDetail.setCoach(coach);
								orderDetail.setContact_no(contact_no);
								orderDetail.setTxnStatus(txnStatus);
								orderDetail.setOrderDate(date);
								orderDetail.setEta(eta);
								orderDetail.setMail_id(mail_id);
								orderDetail.setName(name);
								orderDetail.setOrder_outlet_id(order_outlet_id);
								orderDetail.setOrderStatus(orderStatus);
								orderDetail.setPnr(pnr);
								orderDetail.setSeat(seat);
								orderDetail.setStation_code(station_code);
								orderDetail.setTrain_number(train_number);
								orderDetail.setTransactionRefNo(transactionRefNo);
								orderDetail.setUserOrderId(userOrderId);
								orderDetail.setTotalCustomerPayable(totalCustomerPayable);

								// if (txn!=null) {
								// transactionRefNo=txn.getString("transactionRefNo");
								// }
								String strUsr = val.getString("user");
								JSONObject usr = null;
								if (!strUsr.equalsIgnoreCase("null")) {
									usr = val.getJSONObject("user");
								}
								JSONObject usrDtl = null;
								if (usr != null) {
									usrDtl = usr.getJSONObject("userDetail");
								}

								UserDetail userDetail = new UserDetail();
								if (usrDtl != null) {
									String userFname = (String) details.getJSONObject(i).getJSONObject("user")
											.getJSONObject("userDetail").get("firstName");
									String userLname = (String) details.getJSONObject(i).getJSONObject("user")
											.getJSONObject("userDetail").get("lastName");
									String username = (String) details.getJSONObject(i).getJSONObject("user")
											.get("username");
									long accountNo = (long) details.getJSONObject(i).getJSONObject("user")
											.getJSONObject("accountDetail").getLong("accountNumber");

									userDetail.setFirstName(userFname);
									userDetail.setLastName(userLname);
									userDetail.setUsername(username);
									userDetail.setAccountNumber(accountNo);
								}
								orderDetail.setUserDetail(userDetail);

								long txnDate = details.getJSONObject(i).getLong("created");
								Date date2 = new Date(txnDate);
								String txnDate2 = dateFormat.format(date2);
								orderDetail.setTxnDate(txnDate2);

								DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");
								// long milliSeconds =
								// Long.parseLong(jsonobject.getString("created"));
								Calendar calendar = Calendar.getInstance();
								calendar.setTimeInMillis(txnDate);
								orderDetail.setCreationTime(formatter.format(calendar.getTime()));

								list.add(orderDetail);
							}
							result.setJsonArray(list);
							result.setTotalPages(obj.getLong("count"));
							result.setSize(userRequest.getSize());

							model.addAttribute("OrderDetails", list);
						} else {
							model.addAttribute("OrderDetails", list);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
		return "Admin/TKOrderDetails";
		// return new ResponseEntity<UserListDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/TKOrderDeatilsListByDate")
	String getTKOrderDeatilByDateFilter(@ModelAttribute PagingDTO dto, HttpServletRequest request, ModelMap model,
			HttpSession session) {
		UserListDTO result = new UserListDTO();
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		List<TKOrderDetailsDTO> list = new ArrayList<>();
		if (sessionCheck != null) {
			String adminAuthority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (adminAuthority != null) {
				if (adminAuthority.contains(Authorities.ADMINISTRATOR)
						&& adminAuthority.contains(Authorities.AUTHENTICATED)) {
					AllUserRequest userRequest = new AllUserRequest();
					userRequest.setPage(0);
					userRequest.setSize(1000);
					userRequest.setSessionId((String) session.getAttribute("adminSessionId"));
					userRequest.setStatus(UserStatus.ALL);
					userRequest.setStartDate(dto.getStartDate());
					userRequest.setEndDate(dto.getEndDate());
					AllUserResponse userResponse = appAdminApi.getTKOrderDetailsByDate(userRequest);
					try {
						if (userResponse.getCode().equals("S00")) {
							JSONObject obj = new JSONObject(userResponse.getResponse());
							JSONObject obj1 = obj.getJSONObject("details");
							JSONArray details = obj1.getJSONArray("userList");
							JSONArray commissionList = obj1.getJSONArray("commissionList");
							for (int i = 0; i < details.length(); i++) {
								TKOrderDetailsDTO orderDetail = new TKOrderDetailsDTO();
								if (commissionList.length() > i) {
									JSONObject commissionTransaction = commissionList.getJSONObject(i);
									orderDetail.setCommsissionAmount(
											JSONParserUtil.getDouble(commissionTransaction, "commission"));
								} else {
									orderDetail.setCommsissionAmount(0.0);
								}

								JSONObject val = details.getJSONObject(i);
								// JSONObject txn=null;
								// if (val!=null) {
								// txn=val.getJSONObject("transaction");
								// }

								long id = details.getJSONObject(i).getLong("id");
								// System.out.println("ID--"+id);
								String date = details.getJSONObject(i).getString("date");
								String order_outlet_id = details.getJSONObject(i).getString("order_outlet_id");
								String mail_id = details.getJSONObject(i).getString("mail_id");
								// System.out.println(date+"---"+order_outlet_id+"--"+mail_id+"-");
								// long created =
								// details.getJSONObject(i).getLong("created");
								String train_number = details.getJSONObject(i).getString("train_number");
								String orderStatus = details.getJSONObject(i).getString("orderStatus");
								String station_code = details.getJSONObject(i).getString("station_code");
								String transactionRefNo = details.getJSONObject(i).getString("transactionRefNo");
								String userOrderId = details.getJSONObject(i).getString("userOrderId");
								String seat = details.getJSONObject(i).getString("seat");
								String eta = details.getJSONObject(i).getString("eta");
								String pnr = details.getJSONObject(i).getString("pnr");
								String coach = details.getJSONObject(i).getString("coach");
								String contact_no = details.getJSONObject(i).getString("contact_no");
								String name = details.getJSONObject(i).getString("name");
								double totalCustomerPayable = details.getJSONObject(i)
										.getDouble("totalCustomerPayable");
								String txnStatus = details.getJSONObject(i).getString("txnStatus");
								orderDetail.setCoach(coach);
								orderDetail.setContact_no(contact_no);
								orderDetail.setTxnStatus(txnStatus);
								orderDetail.setOrderDate(date);
								orderDetail.setEta(eta);
								orderDetail.setMail_id(mail_id);
								orderDetail.setName(name);
								orderDetail.setOrder_outlet_id(order_outlet_id);
								orderDetail.setOrderStatus(orderStatus);
								orderDetail.setPnr(pnr);
								orderDetail.setSeat(seat);
								orderDetail.setStation_code(station_code);
								orderDetail.setTrain_number(train_number);
								orderDetail.setTransactionRefNo(transactionRefNo);
								orderDetail.setUserOrderId(userOrderId);
								orderDetail.setTotalCustomerPayable(totalCustomerPayable);

								// if (txn!=null) {
								// transactionRefNo=txn.getString("transactionRefNo");
								// }
								String strUsr = val.getString("user");
								JSONObject usr = null;
								if (!strUsr.equalsIgnoreCase("null")) {
									usr = val.getJSONObject("user");
								}
								JSONObject usrDtl = null;
								if (usr != null) {
									usrDtl = usr.getJSONObject("userDetail");
								}

								UserDetail userDetail = new UserDetail();
								if (usrDtl != null) {
									String userFname = (String) details.getJSONObject(i).getJSONObject("user")
											.getJSONObject("userDetail").get("firstName");
									String userLname = (String) details.getJSONObject(i).getJSONObject("user")
											.getJSONObject("userDetail").get("lastName");
									String username = (String) details.getJSONObject(i).getJSONObject("user")
											.get("username");
									long accountNo = (long) details.getJSONObject(i).getJSONObject("user")
											.getJSONObject("accountDetail").getLong("accountNumber");

									userDetail.setFirstName(userFname);
									userDetail.setLastName(userLname);
									userDetail.setUsername(username);
									userDetail.setAccountNumber(accountNo);
								}

								orderDetail.setUserDetail(userDetail);

								long txnDate = details.getJSONObject(i).getLong("created");
								Date date2 = new Date(txnDate);
								String txnDate2 = dateFormat.format(date2);
								orderDetail.setTxnDate(txnDate2);

								DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");
								// long milliSeconds =
								// Long.parseLong(jsonobject.getString("created"));
								Calendar calendar = Calendar.getInstance();
								calendar.setTimeInMillis(txnDate);
								orderDetail.setCreationTime(formatter.format(calendar.getTime()));

								list.add(orderDetail);
							}

							model.addAttribute("OrderDetails", list);
						} else {
							model.addAttribute("OrderDetails", list);
						}

					} catch (Exception e) {
						e.printStackTrace();
						return "redirect:/Admin/Home";
					}
					return "Admin/TKOrderDetails";
				}
			}

		}
		return "Admin/Login";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/TKOrderDeatilsListByDate")
	String getTKOrderDeatilByDateFilterPost(@ModelAttribute PagingDTO dto, HttpServletRequest request, ModelMap model,
			HttpSession session) {

		try {
			dto.setStartDate(ConvertUtil.decodeBase64String(dto.getStartDate()));
			dto.setEndDate(ConvertUtil.decodeBase64String(dto.getEndDate()));
		} catch (Exception e) {

		}

		UserListDTO result = new UserListDTO();
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		List<TKOrderDetailsDTO> list = new ArrayList<>();
		if (sessionCheck != null) {
			String adminAuthority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (adminAuthority != null) {
				if (adminAuthority.contains(Authorities.ADMINISTRATOR)
						&& adminAuthority.contains(Authorities.AUTHENTICATED)) {
					AllUserRequest userRequest = new AllUserRequest();
					userRequest.setPage(0);
					userRequest.setSize(1000);
					userRequest.setSessionId((String) session.getAttribute("adminSessionId"));
					userRequest.setStatus(UserStatus.ALL);
					userRequest.setStartDate(dto.getStartDate());
					userRequest.setEndDate(dto.getEndDate());
					AllUserResponse userResponse = appAdminApi.getTKOrderDetailsByDate(userRequest);
					try {
						if (userResponse.getCode().equals("S00")) {
							JSONObject obj = new JSONObject(userResponse.getResponse());
							JSONObject obj1 = obj.getJSONObject("details");
							JSONArray details = obj1.getJSONArray("userList");
							JSONArray commissionList = obj1.getJSONArray("commissionList");
							for (int i = 0; i < details.length(); i++) {
								TKOrderDetailsDTO orderDetail = new TKOrderDetailsDTO();
								if (commissionList.length() > i) {
									JSONObject commissionTransaction = commissionList.getJSONObject(i);
									orderDetail.setCommsissionAmount(
											JSONParserUtil.getDouble(commissionTransaction, "commission"));
								} else {
									orderDetail.setCommsissionAmount(0.0);
								}

								JSONObject val = details.getJSONObject(i);
								// JSONObject txn=null;
								// if (val!=null) {
								// txn=val.getJSONObject("transaction");
								// }

								long id = details.getJSONObject(i).getLong("id");
								// System.out.println("ID--"+id);
								String date = details.getJSONObject(i).getString("date");
								String order_outlet_id = details.getJSONObject(i).getString("order_outlet_id");
								String mail_id = details.getJSONObject(i).getString("mail_id");
								// System.out.println(date+"---"+order_outlet_id+"--"+mail_id+"-");
								// long created =
								// details.getJSONObject(i).getLong("created");
								String train_number = details.getJSONObject(i).getString("train_number");
								String orderStatus = details.getJSONObject(i).getString("orderStatus");
								String station_code = details.getJSONObject(i).getString("station_code");
								String transactionRefNo = details.getJSONObject(i).getString("transactionRefNo");
								String userOrderId = details.getJSONObject(i).getString("userOrderId");
								String seat = details.getJSONObject(i).getString("seat");
								String eta = details.getJSONObject(i).getString("eta");
								String pnr = details.getJSONObject(i).getString("pnr");
								String coach = details.getJSONObject(i).getString("coach");
								String contact_no = details.getJSONObject(i).getString("contact_no");
								String name = details.getJSONObject(i).getString("name");
								double totalCustomerPayable = details.getJSONObject(i)
										.getDouble("totalCustomerPayable");
								String txnStatus = details.getJSONObject(i).getString("txnStatus");
								orderDetail.setCoach(coach);
								orderDetail.setContact_no(contact_no);
								orderDetail.setTxnStatus(txnStatus);
								orderDetail.setOrderDate(date);
								orderDetail.setEta(eta);
								orderDetail.setMail_id(mail_id);
								orderDetail.setName(name);
								orderDetail.setOrder_outlet_id(order_outlet_id);
								orderDetail.setOrderStatus(orderStatus);
								orderDetail.setPnr(pnr);
								orderDetail.setSeat(seat);
								orderDetail.setStation_code(station_code);
								orderDetail.setTrain_number(train_number);
								orderDetail.setTransactionRefNo(transactionRefNo);
								orderDetail.setUserOrderId(userOrderId);
								orderDetail.setTotalCustomerPayable(totalCustomerPayable);

								// if (txn!=null) {
								// transactionRefNo=txn.getString("transactionRefNo");
								// }
								String strUsr = val.getString("user");
								JSONObject usr = null;
								if (!strUsr.equalsIgnoreCase("null")) {
									usr = val.getJSONObject("user");
								}
								JSONObject usrDtl = null;
								if (usr != null) {
									usrDtl = usr.getJSONObject("userDetail");
								}

								UserDetail userDetail = new UserDetail();
								if (usrDtl != null) {
									String userFname = (String) details.getJSONObject(i).getJSONObject("user")
											.getJSONObject("userDetail").get("firstName");
									String userLname = (String) details.getJSONObject(i).getJSONObject("user")
											.getJSONObject("userDetail").get("lastName");
									String username = (String) details.getJSONObject(i).getJSONObject("user")
											.get("username");
									long accountNo = (long) details.getJSONObject(i).getJSONObject("user")
											.getJSONObject("accountDetail").getLong("accountNumber");

									userDetail.setFirstName(userFname);
									userDetail.setLastName(userLname);
									userDetail.setUsername(username);
									userDetail.setAccountNumber(accountNo);
								}

								orderDetail.setUserDetail(userDetail);

								long txnDate = details.getJSONObject(i).getLong("created");
								Date date2 = new Date(txnDate);
								String txnDate2 = dateFormat.format(date2);
								orderDetail.setTxnDate(txnDate2);

								DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");
								// long milliSeconds =
								// Long.parseLong(jsonobject.getString("created"));
								Calendar calendar = Calendar.getInstance();
								calendar.setTimeInMillis(txnDate);
								orderDetail.setCreationTime(formatter.format(calendar.getTime()));

								list.add(orderDetail);
							}

							model.addAttribute("OrderDetails", list);
						} else {
							model.addAttribute("OrderDetails", list);
						}

					} catch (Exception e) {
						e.printStackTrace();
						return "redirect:/Admin/Home";
					}
					return "Admin/TKOrderDetails";
				}
			}

		}
		return "Admin/Login";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/ReconciliationReport")
	public String getReconciliationReports(HttpServletRequest request, HttpServletResponse response,
			HttpSession session, ModelMap model) throws JSONException {
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			return "Admin/ReconciliationReport";
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/ReconciliationReportInJSON")
	public String getReconciliationList(@ModelAttribute DateDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) {

		try {
			dto.setFromDate(ConvertUtil.decodeBase64String(dto.getFromDate()));
			dto.setToDate(ConvertUtil.decodeBase64String(dto.getToDate()));
		} catch (Exception e) {

		}

		UserListDTO result = new UserListDTO();
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionCheck);
					TransactionReportResponse listResponse = appAdminApi.getReconReport(dto);
					if (listResponse.isSuccess()) {
						model.addAttribute("transactionList", listResponse.getList());
						model.addAttribute("serviceName", listResponse.getServiceName());
						model.addAttribute(ModelMapKey.MESSAGE,
								"Transaction List b/w " + dto.getFromDate() + " and " + dto.getToDate()
										+ " | Service - " + listResponse.getServiceName() + " | Status - "
										+ dto.getServiceStatus());
					} else {
						model.addAttribute(ModelMapKey.ERROR, listResponse.getMessage());
					}
					return "Admin/ReconFilterReport";
				}
			}
		}
		return "redirect:/Admin/Home";
	}

	// HouseJoyApi Details

	@RequestMapping(method = RequestMethod.GET, value = "/HouseJoyDetailsByDate")
	String getHouseJoyByDate(@ModelAttribute PagingDTO dto, HttpServletRequest request, ModelMap model,
			HttpSession session) {

		UserListDTO result = new UserListDTO();
		String sessionCheck = (String) session.getAttribute("adminSessionId");

		if (sessionCheck != null) {
			String adminAuthority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (adminAuthority != null) {
				if (adminAuthority.contains(Authorities.ADMINISTRATOR)
						&& adminAuthority.contains(Authorities.AUTHENTICATED)) {
					AllUserRequest userRequest = new AllUserRequest();
					userRequest.setPage(0);
					userRequest.setSize(1000);
					userRequest.setSessionId((String) session.getAttribute("adminSessionId"));
					userRequest.setStatus(UserStatus.ALL);
					dto.setSessionId((String) session.getAttribute("adminSessionId"));
					AllUserResponse userResponse = appAdminApi.getHouseJoy(dto);

					try {
						List<HouseJoyAdminDTO> userList = new ArrayList<>();
						if (userResponse.getCode().equals("S00")) {
							JSONObject obj = new JSONObject(userResponse.getResponse());
							JSONArray data = obj.getJSONArray("details");

							if (data != null) {

								for (int i = 0; i < data.length(); i++) {

									HouseJoyAdminDTO list = new HouseJoyAdminDTO();
									JSONObject userDetail = null;
									JSONObject account = null;
									String txnStr = data.getJSONObject(i).getString("transaction");
									JSONObject transaction = null;
									if (!txnStr.equalsIgnoreCase("null")) {
										transaction = data.getJSONObject(i).getJSONObject("transaction");
									}

									JSONObject user = data.getJSONObject(i).getJSONObject("user");
									if (user != null) {
										userDetail = user.getJSONObject("userDetail");
										list.setUserDetails(userDetail.getString("firstName") + " "
												+ userDetail.getString("lastName"));
										list.setUserName(user.getString("username"));
									}

									/*
									 * list.setBookingStatus(data.getJSONObject(
									 * i).getString("flightStatus"));
									 */
									/*
									 * list.setCommsissionAmount(data.
									 * getJSONObject(i).getDouble(
									 * "commissionAmt"));
									 */

									if (transaction != null) {
										list.setTransactionRefNo(transaction.getString("transactionRefNo"));
										list.setTransactionStatus(transaction.getString("status"));
										long dt = transaction.getLong("created");
										Date date = new Date(dt);
										String txnDate = dateFormat.format(date);
										list.setTxnDate(txnDate);
										account = transaction.getJSONObject("account");
									} else {
										list.setTransactionRefNo("NA");
										list.setTransactionStatus("NA");
										list.setTxnDate("NA");
									}

									if (account != null) {
										list.setAccountNumber(account.getLong("accountNumber"));
									}
									/*
									 * list.setBookingRefId(data.getJSONObject(i
									 * ).getString("bookingRefId"));
									 */
									list.setPaymentAmount(data.getJSONObject(i).getDouble("amount"));
									list.setJobId(data.getJSONObject(i).getString("jobId"));
									list.setServiceName(data.getJSONObject(i).getString("serviceName"));
									list.setCustomerName(data.getJSONObject(i).getString("customerName"));
									list.setAddress(data.getJSONObject(i).getString("address"));
									list.setDate(data.getJSONObject(i).getString("date"));
									list.setMobile(data.getJSONObject(i).getString("mobile"));
									list.setTime(data.getJSONObject(i).getString("time"));
									list.setPaymentType(data.getJSONObject(i).getString("paymentType"));
									list.setHjtxnRefNo(data.getJSONObject(i).getString("hjtxnRefNo"));

									// list.setFlightTicketId(data.getJSONObject(i).getLong("id"));
									/*
									 * list.setTicketDetails(data.getJSONObject(
									 * i).getString("ticketDetails"));
									 */
									userList.add(list);

								}
							}
							model.addAttribute("HouseJoyDeatils", userList);
						} else {
							model.addAttribute("HouseJoyDeatils", userList);
						}

					} catch (Exception e) {
						e.printStackTrace();
						return "Admin/Login";
					}
					return "Admin/HouseJoyDetails";
				}
			}

		}
		return "Admin/Login";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/HouseJoyDetailsByDate")
	String getHouseJoyByDatepost(@ModelAttribute PagingDTO dto, HttpServletRequest request, ModelMap model,
			HttpSession session) {

		try {
			dto.setStartDate(ConvertUtil.decodeBase64String(dto.getStartDate()));
			dto.setEndDate(ConvertUtil.decodeBase64String(dto.getEndDate()));
		} catch (Exception e) {

		}

		UserListDTO result = new UserListDTO();
		String sessionCheck = (String) session.getAttribute("adminSessionId");

		if (sessionCheck != null) {
			String adminAuthority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (adminAuthority != null) {
				if (adminAuthority.contains(Authorities.ADMINISTRATOR)
						&& adminAuthority.contains(Authorities.AUTHENTICATED)) {
					AllUserRequest userRequest = new AllUserRequest();
					userRequest.setPage(0);
					userRequest.setSize(1000);
					userRequest.setSessionId((String) session.getAttribute("adminSessionId"));
					userRequest.setStatus(UserStatus.ALL);
					dto.setSessionId((String) session.getAttribute("adminSessionId"));
					AllUserResponse userResponse = appAdminApi.getHouseJoy(dto);

					try {
						List<HouseJoyAdminDTO> userList = new ArrayList<>();
						if (userResponse.getCode().equals("S00")) {
							JSONObject obj = new JSONObject(userResponse.getResponse());
							JSONArray data = obj.getJSONArray("details");

							if (data != null) {

								for (int i = 0; i < data.length(); i++) {

									HouseJoyAdminDTO list = new HouseJoyAdminDTO();
									JSONObject userDetail = null;
									JSONObject account = null;
									String txnStr = data.getJSONObject(i).getString("transaction");
									JSONObject transaction = null;
									if (!txnStr.equalsIgnoreCase("null")) {
										transaction = data.getJSONObject(i).getJSONObject("transaction");
									}

									JSONObject user = data.getJSONObject(i).getJSONObject("user");
									if (user != null) {
										userDetail = user.getJSONObject("userDetail");
										list.setUserDetails(userDetail.getString("firstName") + " "
												+ userDetail.getString("lastName"));
										list.setUserName(user.getString("username"));
									}

									/*
									 * list.setBookingStatus(data.getJSONObject(
									 * i).getString("flightStatus"));
									 */
									/*
									 * list.setCommsissionAmount(data.
									 * getJSONObject(i).getDouble(
									 * "commissionAmt"));
									 */

									if (transaction != null) {
										list.setTransactionRefNo(transaction.getString("transactionRefNo"));
										list.setTransactionStatus(transaction.getString("status"));
										long dt = transaction.getLong("created");
										Date date = new Date(dt);
										String txnDate = dateFormat.format(date);
										list.setTxnDate(txnDate);
										account = transaction.getJSONObject("account");
									} else {
										list.setTransactionRefNo("NA");
										list.setTransactionStatus("NA");
										list.setTxnDate("NA");
									}

									if (account != null) {
										list.setAccountNumber(account.getLong("accountNumber"));
									}
									/*
									 * list.setBookingRefId(data.getJSONObject(i
									 * ).getString("bookingRefId"));
									 */
									list.setPaymentAmount(data.getJSONObject(i).getDouble("amount"));
									list.setJobId(data.getJSONObject(i).getString("jobId"));
									list.setServiceName(data.getJSONObject(i).getString("serviceName"));
									list.setCustomerName(data.getJSONObject(i).getString("customerName"));
									list.setAddress(data.getJSONObject(i).getString("address"));
									list.setDate(data.getJSONObject(i).getString("date"));
									list.setMobile(data.getJSONObject(i).getString("mobile"));
									list.setTime(data.getJSONObject(i).getString("time"));
									list.setPaymentType(data.getJSONObject(i).getString("paymentType"));
									list.setHjtxnRefNo(data.getJSONObject(i).getString("hjtxnRefNo"));

									// list.setFlightTicketId(data.getJSONObject(i).getLong("id"));
									/*
									 * list.setTicketDetails(data.getJSONObject(
									 * i).getString("ticketDetails"));
									 */
									userList.add(list);

								}
							}
							model.addAttribute("HouseJoyDeatils", userList);
						} else {
							model.addAttribute("HouseJoyDeatils", userList);
						}

					} catch (Exception e) {
						e.printStackTrace();
						return "Admin/Login";
					}
					return "Admin/HouseJoyDetails";
				}
			}

		}
		return "Admin/Login";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/HouseJoyDetails")
	String getHouseJoyDetails(@ModelAttribute PagingDTO dto, HttpServletRequest request, ModelMap model,
			HttpSession session) {
		String sessionCheck = (String) session.getAttribute("adminSessionId");

		if (sessionCheck != null) {
			String adminAuthority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (adminAuthority != null) {
				if (adminAuthority.contains(Authorities.ADMINISTRATOR)
						&& adminAuthority.contains(Authorities.AUTHENTICATED)) {
					AllUserRequest userRequest = new AllUserRequest();
					userRequest.setPage(0);
					userRequest.setSize(1000);
					userRequest.setSessionId((String) session.getAttribute("adminSessionId"));
					userRequest.setStatus(UserStatus.ALL);
					dto.setSessionId((String) session.getAttribute("adminSessionId"));
					AllUserResponse userResponse = appAdminApi.getHouseJoy(dto);

					try {
						List<HouseJoyAdminDTO> userList = new ArrayList<>();
						if (userResponse.getCode().equals("S00")) {
							JSONObject obj = new JSONObject(userResponse.getResponse());
							JSONArray data = obj.getJSONArray("details");

							if (data != null) {

								for (int i = 0; i < data.length(); i++) {

									HouseJoyAdminDTO list = new HouseJoyAdminDTO();
									JSONObject userDetail = null;
									JSONObject account = null;
									String txnStr = data.getJSONObject(i).getString("transaction");
									JSONObject transaction = null;
									if (!txnStr.equalsIgnoreCase("null")) {
										transaction = data.getJSONObject(i).getJSONObject("transaction");
									}

									JSONObject user = data.getJSONObject(i).getJSONObject("user");
									if (user != null) {
										userDetail = user.getJSONObject("userDetail");
										list.setUserDetails(userDetail.getString("firstName") + " "
												+ userDetail.getString("lastName"));
										list.setUserName(user.getString("username"));
									}

									/*
									 * list.setBookingStatus(data.getJSONObject(
									 * i).getString("flightStatus"));
									 */
									/*
									 * list.setCommsissionAmount(data.
									 * getJSONObject(i).getDouble(
									 * "commissionAmt"));
									 */

									if (transaction != null) {
										list.setTransactionRefNo(transaction.getString("transactionRefNo"));
										list.setTransactionStatus(transaction.getString("status"));
										long dt = transaction.getLong("created");
										Date date = new Date(dt);
										String txnDate = dateFormat.format(date);
										list.setTxnDate(txnDate);
										account = transaction.getJSONObject("account");
									} else {
										list.setTransactionRefNo("NA");
										list.setTransactionStatus("NA");
										list.setTxnDate("NA");
									}

									if (account != null) {
										list.setAccountNumber(account.getLong("accountNumber"));
									}
									/*
									 * list.setBookingRefId(data.getJSONObject(i
									 * ).getString("bookingRefId"));
									 */
									list.setPaymentAmount(data.getJSONObject(i).getDouble("amount"));
									list.setJobId(data.getJSONObject(i).getString("jobId"));
									list.setServiceName(data.getJSONObject(i).getString("serviceName"));
									list.setCustomerName(data.getJSONObject(i).getString("customerName"));
									list.setAddress(data.getJSONObject(i).getString("address"));
									list.setDate(data.getJSONObject(i).getString("date"));
									list.setMobile(data.getJSONObject(i).getString("mobile"));
									list.setTime(data.getJSONObject(i).getString("time"));
									list.setPaymentType(data.getJSONObject(i).getString("paymentType"));
									list.setHjtxnRefNo(data.getJSONObject(i).getString("hjtxnRefNo"));

									// list.setFlightTicketId(data.getJSONObject(i).getLong("id"));
									/*
									 * list.setTicketDetails(data.getJSONObject(
									 * i).getString("ticketDetails"));
									 */
									userList.add(list);

								}
							}
							model.addAttribute("HouseJoyDeatils", userList);
						} else {
							model.addAttribute("HouseJoyDeatils", userList);
						}

					} catch (Exception e) {
						e.printStackTrace();
						return "Admin/Login";
					}
					return "Admin/HouseJoyDetails";
				}
			}

		}
		return "Admin/Login";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/HouseJoyDetails")
	String getHouseJoyDetailsPost(@ModelAttribute PagingDTO dto, HttpServletRequest request, ModelMap model,
			HttpSession session) {

		try {
			dto.setStartDate(ConvertUtil.decodeBase64String(dto.getStartDate()));
			dto.setEndDate(ConvertUtil.decodeBase64String(dto.getEndDate()));
		} catch (Exception e) {

		}

		String sessionCheck = (String) session.getAttribute("adminSessionId");

		if (sessionCheck != null) {
			String adminAuthority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (adminAuthority != null) {
				if (adminAuthority.contains(Authorities.ADMINISTRATOR)
						&& adminAuthority.contains(Authorities.AUTHENTICATED)) {
					AllUserRequest userRequest = new AllUserRequest();
					userRequest.setPage(0);
					userRequest.setSize(1000);
					userRequest.setSessionId((String) session.getAttribute("adminSessionId"));
					userRequest.setStatus(UserStatus.ALL);
					dto.setSessionId((String) session.getAttribute("adminSessionId"));
					AllUserResponse userResponse = appAdminApi.getHouseJoy(dto);

					try {
						List<HouseJoyAdminDTO> userList = new ArrayList<>();
						if (userResponse.getCode().equals("S00")) {
							JSONObject obj = new JSONObject(userResponse.getResponse());
							JSONArray data = obj.getJSONArray("details");

							if (data != null) {

								for (int i = 0; i < data.length(); i++) {

									HouseJoyAdminDTO list = new HouseJoyAdminDTO();
									JSONObject userDetail = null;
									JSONObject account = null;
									String txnStr = data.getJSONObject(i).getString("transaction");
									JSONObject transaction = null;
									if (!txnStr.equalsIgnoreCase("null")) {
										transaction = data.getJSONObject(i).getJSONObject("transaction");
									}

									JSONObject user = data.getJSONObject(i).getJSONObject("user");
									if (user != null) {
										userDetail = user.getJSONObject("userDetail");
										list.setUserDetails(userDetail.getString("firstName") + " "
												+ userDetail.getString("lastName"));
										list.setUserName(user.getString("username"));
									}

									/*
									 * list.setBookingStatus(data.getJSONObject(
									 * i).getString("flightStatus"));
									 */
									/*
									 * list.setCommsissionAmount(data.
									 * getJSONObject(i).getDouble(
									 * "commissionAmt"));
									 */

									if (transaction != null) {
										list.setTransactionRefNo(transaction.getString("transactionRefNo"));
										list.setTransactionStatus(transaction.getString("status"));
										long dt = transaction.getLong("created");
										Date date = new Date(dt);
										String txnDate = dateFormat.format(date);
										list.setTxnDate(txnDate);
										account = transaction.getJSONObject("account");
									} else {
										list.setTransactionRefNo("NA");
										list.setTransactionStatus("NA");
										list.setTxnDate("NA");
									}

									if (account != null) {
										list.setAccountNumber(account.getLong("accountNumber"));
									}
									/*
									 * list.setBookingRefId(data.getJSONObject(i
									 * ).getString("bookingRefId"));
									 */
									list.setPaymentAmount(data.getJSONObject(i).getDouble("amount"));
									list.setJobId(data.getJSONObject(i).getString("jobId"));
									list.setServiceName(data.getJSONObject(i).getString("serviceName"));
									list.setCustomerName(data.getJSONObject(i).getString("customerName"));
									list.setAddress(data.getJSONObject(i).getString("address"));
									list.setDate(data.getJSONObject(i).getString("date"));
									list.setMobile(data.getJSONObject(i).getString("mobile"));
									list.setTime(data.getJSONObject(i).getString("time"));
									list.setPaymentType(data.getJSONObject(i).getString("paymentType"));
									list.setHjtxnRefNo(data.getJSONObject(i).getString("hjtxnRefNo"));

									// list.setFlightTicketId(data.getJSONObject(i).getLong("id"));
									/*
									 * list.setTicketDetails(data.getJSONObject(
									 * i).getString("ticketDetails"));
									 */
									userList.add(list);

								}
							}
							model.addAttribute("HouseJoyDeatils", userList);
						} else {
							model.addAttribute("HouseJoyDeatils", userList);
						}

					} catch (Exception e) {
						e.printStackTrace();
						return "Admin/Login";
					}
					return "Admin/HouseJoyDetails";
				}
			}

		}
		return "Admin/Login";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/BulkUpload")
	public String getRequestRefund(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) {
		String sessionId = (String) session.getAttribute("adminSessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					return "/Admin/BulkUpload";
				}

			}
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/BulkUpload")
	public String submitRequestRefund(@ModelAttribute RequestRefund filter, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model)  {

		String sessionId = (String) session.getAttribute("adminSessionId");
		try {
			if (sessionId != null && sessionId.length() != 0) {
				String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
				if (authority != null) {
					if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
						filter.setSessionId(sessionId);
						String rootDirectory = request.getSession().getServletContext().getRealPath("/");
						if(!CommonUtil.isValidCSVFormate(filter.getFile())){
							model.addAttribute("message", "Bad format, please upload csv format only");
							return "Admin/BulkUpload";
						}
						String path = saveRefundReport(rootDirectory, filter.getFile(), filter.getFileName());
						String file = path.substring(18);
						String fileName = StartupUtil.CSV_FILE + file;
						if (fileName != null) {
							Set<SendMoneyMobileDTO> sendMoneyList = readFromFile(fileName);
							Iterator<SendMoneyMobileDTO> iterator = sendMoneyList.iterator();
							List<SendMoneyMobileDTO> tokens = new ArrayList<SendMoneyMobileDTO>();
							if (sendMoneyList != null && !sendMoneyList.isEmpty()) {
								while (iterator.hasNext()) {
									SendMoneyMobileDTO dto = iterator.next();
									tokens.add(dto);
								}
								filter.setMobileDTO(tokens);
								ResponseDTO responseDTO = appAdminApi.requestRefund(filter);
								model.addAttribute("message", responseDTO.getMessage());
								return "Admin/BulkUpload";
							}else{
								model.addAttribute("message", "Data incorrect or found empty data");
								return "Admin/BulkUpload";
							}
						}
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			model.addAttribute("message", "Internal Server Error");
			return "Admin/BulkUpload";
		}
		
		return "redirect:/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/TreatCardReport")
	public String getTreatCardReport(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model, SessionDTO dto) throws JSONException {

		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String adminAuthority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (adminAuthority != null) {
				if (adminAuthority.contains(Authorities.ADMINISTRATOR)
						&& adminAuthority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionCheck);
					TreatCardRegisterListResponse resp = appAdminApi.getRegisterList(dto);
					JSONArray arr = (JSONArray) resp.getJsonArray();
					List<TreatCardRegisterListResponse> list = new ArrayList<TreatCardRegisterListResponse>();
					for (int i = 0; i < arr.length(); i++) {
						TreatCardRegisterListResponse re = new TreatCardRegisterListResponse();
						JSONObject json = arr.getJSONObject(i);
						re.setMembershipCode(JSONParserUtil.getLong(json, "membershipCode"));
						re.setMemberShipCodeExpire(
								JSONParserUtil.getString(json, "memberShipCodeExpire").substring(0, 16));
						re.setUsername(JSONParserUtil.getString(json, "username"));
						re.setEmailId(JSONParserUtil.getString(json, "emailId"));
						re.setFirstName(JSONParserUtil.getString(json, "firstName"));
						re.setCreated(JSONParserUtil.getString(json, "created"));
						list.add(re);
					}
					model.addAttribute("list", list);
					return "/Admin/TreatCardRegisterList";
				}
			}
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/TreatCardReports")
	public String getTreatCardReportFilter(HttpServletRequest request, HttpServletResponse response,
			HttpSession session, ModelMap model, @RequestParam("fromDate") String start,
			@RequestParam("toDate") String end) throws JSONException {

		try {
			start = ConvertUtil.decodeBase64String(start);
			end = ConvertUtil.decodeBase64String(end);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String sessionCheck = (String) session.getAttribute("adminSessionId");
		TreatCardRegisterList dto = new TreatCardRegisterList();
		if (sessionCheck != null) {
			String adminAuthority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (adminAuthority != null) {
				if (adminAuthority.contains(Authorities.ADMINISTRATOR)
						&& adminAuthority.contains(Authorities.AUTHENTICATED)) {
					model.addAttribute(ModelMapKey.MESSAGE,
							" TreatCard Registeration List From " + start + " to " + end);
					dto.setSessionId(sessionCheck);
					dto.setFromDate(start);
					dto.setToDate(end);
					TreatCardRegisterListResponse resp = appAdminApi.getRegisterListFiltered(dto);
					JSONArray arr = (JSONArray) resp.getJsonArray();
					List<TreatCardRegisterListResponse> list = new ArrayList<TreatCardRegisterListResponse>();
					for (int i = 0; i < arr.length(); i++) {
						TreatCardRegisterListResponse re = new TreatCardRegisterListResponse();
						JSONObject json = arr.getJSONObject(i);
						re.setMembershipCode(JSONParserUtil.getLong(json, "membershipCode"));
						re.setMemberShipCodeExpire(
								JSONParserUtil.getString(json, "memberShipCodeExpire").substring(0, 16));
						re.setUsername(JSONParserUtil.getString(json, "username"));
						re.setEmailId(JSONParserUtil.getString(json, "emailId"));
						re.setFirstName(JSONParserUtil.getString(json, "firstName"));
						re.setCreated(JSONParserUtil.getString(json, "created"));
						list.add(re);
					}
					model.addAttribute("list", list);
					return "/Admin/TreatCardFilteredList";
				}
			}
		}
		return "redirect:/Admin/Home";
	}

	private String saveRefundReport(String rootDirectory, MultipartFile file, String code) {
		String contentType = file.getContentType();
		String[] fileExtension = contentType.split("/");
		String filePath = null;
		String fileName = String.valueOf(System.currentTimeMillis());
		File dirs = new File(rootDirectory + "/resources/refund/" + file.getOriginalFilename());
		dirs.mkdirs();
		try {
			file.transferTo(dirs);
			filePath = "/resources/refund/" + file.getOriginalFilename();
			return filePath;
		} catch (IOException e) {
			e.printStackTrace();

		}
		return filePath;
	}
	
	public Set<SendMoneyMobileDTO> readFromFile(String fileName) {
		Set<SendMoneyMobileDTO> sendMoneyList = new HashSet<>();
		BufferedReader br = null;
		String line = "";
		try {
			br = new BufferedReader(new FileReader(fileName));
			SendMoneyMobileDTO dto = null;
			while ((line = br.readLine()) != null) {
				Pattern p = Pattern.compile("(([^\"][^,]*)|\"([^\"]*)\"),?");
				Matcher m = p.matcher(line);
				dto = new SendMoneyMobileDTO();
				String value = null;
				int index = 1;
				while (m.find()) {
					if (m.group(2) != null) {
						value = m.group(2);
					}
					if (m.group(3) != null) {
						value = m.group(3);
					}
					if (value != null) {
						if (dto != null) {
							switch (index) {
							case 1:
								dto.setMobileNumber(value);
								break;
							case 2:
								dto.setMessage(value);
								break;
							case 3:
								dto.setAmount(value);
								break;
							default:
								break;
							}
							index = index + 1;
						}
					}
				}
				if(CommonValidation.checkLength10(dto.getMobileNumber()) && CommonValidation.isNumeric(dto.getMobileNumber()) && CommonValidation.isValidMobileNumber(dto.getMobileNumber())){
					sendMoneyList.add(dto);	
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return sendMoneyList;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/TreatCardTransactionReport")
	public String getTransactionList(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) {
		AllTransactionRequest dto = new AllTransactionRequest();
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionCheck);
					TListResponse listResponse = appAdminApi.getTreatCardTransactionList(dto);
					if (listResponse.isSuccess()) {
						model.addAttribute("transactionList", listResponse.getList());
					} else {
						model.addAttribute(ModelMapKey.ERROR, listResponse.getMessage());
					}
					return "Admin/TreatCardReport";
				}
			}
		}
		return "redirect:/Admin/Home";
	}

	// Get Transaction List using filter

	@RequestMapping(method = RequestMethod.POST, value = "/TreatCardTransactionReport")
	public String getTransactionList(@ModelAttribute AllTransactionRequest dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) {
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionCheck);
					TListResponse listResponse = appAdminApi.getTreatCardTransactionListWithFilter(dto);
					if (listResponse.isSuccess()) {
						model.addAttribute("transactionList", listResponse.getList());
						model.addAttribute(ModelMapKey.MESSAGE,
								"Transaction List b/w " + dto.getStartDate() + " and " + dto.getEndDate());
					} else {
						model.addAttribute(ModelMapKey.ERROR, listResponse.getMessage());
					}
					return "Admin/TreatCardReport";
				}
			}
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/TreatCardCount")
	public String getTreatCardCount(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model, SessionDTO dto) throws JSONException {

		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String adminAuthority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (adminAuthority != null) {
				if (adminAuthority.contains(Authorities.ADMINISTRATOR)
						&& adminAuthority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionCheck);
					TreatCardRegisterListResponse resp = appAdminApi.getTreatCardCount(dto);
					JSONArray arr = (JSONArray) resp.getJsonArray();
					List<TreatCardRegisterListResponse> list = new ArrayList<TreatCardRegisterListResponse>();
					for (int i = 0; i < arr.length(); i++) {
						TreatCardRegisterListResponse re = new TreatCardRegisterListResponse();
						JSONObject json = arr.getJSONObject(i);
						re.setMembershipCode(JSONParserUtil.getLong(json, "membershipCode"));
						re.setUsername(JSONParserUtil.getString(json, "username"));
						re.setEmailId(JSONParserUtil.getString(json, "emailId"));
						re.setCount(JSONParserUtil.getLong(json, "count"));
						re.setCreated(JSONParserUtil.getString(json, "created"));
						list.add(re);
					}
					model.addAttribute("list", list);
					return "/Admin/TreatCardCount";
				}
			}
		}
		return "redirect:/Admin/Home";
	}

	/* Ticket pdf for Bus */

	@RequestMapping(value = "/singleTicketPdfForBus", method = RequestMethod.POST)
	ModelAndView generateUserPdf(@ModelAttribute BusTicketDTO dto, HttpServletRequest request, HttpSession session,
			HttpServletResponse response) throws Exception {

		com.payqwikweb.app.model.response.bus.ResponseDTO resp = new com.payqwikweb.app.model.response.bus.ResponseDTO();

		Map<String, Object> userDataTicket = new HashMap<String, Object>();
		String sessionId = (String) session.getAttribute("adminSessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					try {

						resp = appAdminApi.getSingleTicketPdf(sessionId, dto.getEmtTxnId());
						BusTicketDTO busTicketDTO = (BusTicketDTO) resp.getExtraInfo();

						ObjectMapper mapperObj = new ObjectMapper();
						String jsonStr = mapperObj.writeValueAsString(busTicketDTO);

						userDataTicket.put("busTicket", jsonStr.toString());

						return new ModelAndView("userSummaryTicket", "userDataTicket", userDataTicket);

					} catch (Exception e) {
						e.printStackTrace();
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
					}
				} else {
					resp.setCode("F00");
					resp.setMessage("Unauthorised access");
					resp.setStatus("FAILED");
				}
			} else {
				resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
				resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
				resp.setMessage("Session expired");

			}
		} else {
			resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
			resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
			resp.setMessage("Session expired");

		}
		return new ModelAndView("userSummaryTicket", userDataTicket);
	}

	@RequestMapping(value = "/singleAgentTicketPdfForBus", method = RequestMethod.POST)
	ModelAndView generateAgentUserPdf(@ModelAttribute BusTicketDTO dto, HttpServletRequest request, HttpSession session,
			HttpServletResponse response) throws Exception {

		com.payqwikweb.app.model.response.bus.ResponseDTO resp = new com.payqwikweb.app.model.response.bus.ResponseDTO();

		Map<String, Object> userDataTicket = new HashMap<String, Object>();
		String sessionId = (String) session.getAttribute("adminSessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					try {

						resp = appAdminApi.getSingleAgentTicketPdf(sessionId, dto.getEmtTxnId());
						BusTicketDTO busTicketDTO = (BusTicketDTO) resp.getExtraInfo();

						ObjectMapper mapperObj = new ObjectMapper();
						String jsonStr = mapperObj.writeValueAsString(busTicketDTO);

						userDataTicket.put("busTicket", jsonStr.toString());

						return new ModelAndView("userSummaryTicket", "userDataTicket", userDataTicket);

					} catch (Exception e) {
						e.printStackTrace();
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
					}
				} else {
					resp.setCode("F00");
					resp.setMessage("Unauthorised access");
					resp.setStatus("FAILED");
				}
			} else {
				resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
				resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
				resp.setMessage("Session expired");

			}
		} else {
			resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
			resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
			resp.setMessage("Session expired");

		}
		return new ModelAndView("userSummaryTicket", userDataTicket);
	}
	/* Ticket pdf for Flight */

	@RequestMapping(value = "/VpayQwikFlightTicket", method = RequestMethod.POST)
	ModelAndView generateflightPdf(@ModelAttribute FlightTicketAdminDTO dto, HttpServletRequest request,
			HttpSession session, HttpServletResponse response) throws Exception {

		FlightResponseDTO resp = new FlightResponseDTO();

		Map<String, Object> userDataTicket = new HashMap<String, Object>();
		String sessionId = (String) session.getAttribute("adminSessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					try {

						resp = appAdminApi.getSingleFlightTicketDetails(sessionId, dto.getFlightTicketId());

						FlightTicketResp flightTicketResp = (FlightTicketResp) resp.getDetails();
						ObjectMapper mapperObj = new ObjectMapper();
						String jsonStr = mapperObj.writeValueAsString(flightTicketResp);
						userDataTicket.put("busTicket", jsonStr.toString());
						return new ModelAndView("FlightTicket", "userDataTicket", userDataTicket);
					} catch (Exception e) {
						e.printStackTrace();
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
					}
				} else {
					resp.setCode("F00");
					resp.setMessage("Unauthorised access");
					resp.setStatus("FAILED");
				}
			} else {
				resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
				resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
				resp.setMessage("Session expired");

			}
		} else {
			resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
			resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
			resp.setMessage("Session expired");

		}
		return new ModelAndView("userSummaryTicket", "userDataTicket", userDataTicket);
	}

	@RequestMapping(value = "/VpayQwikAgentFlightTicketPdf", method = RequestMethod.POST)
	ModelAndView generateAgentflightPdfForAgent(@ModelAttribute FlightTicketAdminDTO dto, HttpServletRequest request,
			HttpSession session, HttpServletResponse response) throws Exception {

		FlightResponseDTO resp = new FlightResponseDTO();

		Map<String, Object> userDataTicket = new HashMap<String, Object>();
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
			if (authority != null) {
				if (authority.contains(Authorities.AGENT) && authority.contains(Authorities.AUTHENTICATED)) {
					try {
						resp = appAdminApi.getSingleAgentFlightTicketDetailsPdf(sessionId, dto.getFlightTicketId());
						FlightTicketResp flightTicketResp = (FlightTicketResp) resp.getDetails();
						ObjectMapper mapperObj = new ObjectMapper();
						String jsonStr = mapperObj.writeValueAsString(flightTicketResp);
						userDataTicket.put("busTicket", jsonStr.toString());
						return new ModelAndView("FlightTicket", "userDataTicket", userDataTicket);
					} catch (Exception e) {
						e.printStackTrace();
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
					}
				} else {
					resp.setCode("F00");
					resp.setMessage("Unauthorised access");
					resp.setStatus("FAILED");
				}
			} else {
				resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
				resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
				resp.setMessage("Session expired");

			}
		} else {
			resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
			resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
			resp.setMessage("Session expired");

		}
		return new ModelAndView("userSummaryTicket", "userDataTicket", userDataTicket);
	}
	
	
	@RequestMapping(value = "/VpayQwikAgentFlightTicket", method = RequestMethod.POST)
	ModelAndView generateAgentflightPdf(@ModelAttribute FlightTicketAdminDTO dto, HttpServletRequest request,
			HttpSession session, HttpServletResponse response) throws Exception {

		FlightResponseDTO resp = new FlightResponseDTO();

		Map<String, Object> userDataTicket = new HashMap<String, Object>();
		String sessionId = (String) session.getAttribute("adminSessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					try {
						resp = appAdminApi.getSingleAgentFlightTicketDetails(sessionId, dto.getFlightTicketId());
						FlightTicketResp flightTicketResp = (FlightTicketResp) resp.getDetails();
						ObjectMapper mapperObj = new ObjectMapper();
						String jsonStr = mapperObj.writeValueAsString(flightTicketResp);
						userDataTicket.put("busTicket", jsonStr.toString());
						return new ModelAndView("FlightTicket", "userDataTicket", userDataTicket);
					} catch (Exception e) {
						e.printStackTrace();
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
					}
				} else {
					resp.setCode("F00");
					resp.setMessage("Unauthorised access");
					resp.setStatus("FAILED");
				}
			} else {
				resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
				resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
				resp.setMessage("Session expired");

			}
		} else {
			resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
			resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
			resp.setMessage("Session expired");

		}
		return new ModelAndView("userSummaryTicket", "userDataTicket", userDataTicket);
	}

	/* Admin (Backend) Repots New */

	@RequestMapping(method = RequestMethod.GET, value = "/WalletBalOustanding")
	public String getWalletBalOustanding(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) {

		UserListDTO result = new UserListDTO();
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					AllUserRequest userRequest = new AllUserRequest();
					userRequest.setPage(0);
					userRequest.setSize(10);
					userRequest.setSessionId(sessionCheck);
					userRequest.setStatus(UserStatus.ALL);
					AllUserResponse userResponse = appAdminApi.getAllUser(userRequest);
					List<AdminUserDetails> userList = new ArrayList<>();
					result.setSuccess(true);
					result.setFirstPage(userResponse.isFirstPage());
					result.setLastPage(userResponse.isLastPage());
					result.setJsonArray(userList);
					result.setNumberOfElements(userResponse.getNumberOfElements());
					result.setSize(userResponse.getSize());
					result.setTotalPages(userResponse.getTotalPages());
				}
				return "Admin/WalletBalace";
			}
		}
		return "redirect:/Admin/Home";
	}

	// @RequestMapping(method = RequestMethod.POST, value =
	// "/WalletBalOustanding")
	// public String getWalletBalOustandingPost(@RequestBody WalletBalanceDTO
	// search, HttpServletRequest request, HttpServletResponse response,
	// HttpSession session,
	// ModelMap model) {
	//
	// try {
	// search.setMobile(ConvertUtil.decodeBase64String(search.getMobile()));
	//
	// } catch (IOException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	//
	// UserListDTO result = new UserListDTO();
	// String sessionCheck = (String) session.getAttribute("adminSessionId");
	// if (sessionCheck != null) {
	// String authority =
	// authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
	// if (authority != null) {
	// if (authority.contains(Authorities.ADMINISTRATOR) &&
	// authority.contains(Authorities.AUTHENTICATED)) {
	// AllUserRequest userRequest = new AllUserRequest();
	// userRequest.setPage(0);
	// userRequest.setSize(10);
	// userRequest.setSessionId(sessionCheck);
	// userRequest.setStatus(UserStatus.ALL);
	// AllUserResponse userResponse = appAdminApi.getAllUser(userRequest);
	// List<AdminUserDetails> userList = new ArrayList<>();
	// result.setSuccess(true);
	// result.setFirstPage(userResponse.isFirstPage());
	// result.setLastPage(userResponse.isLastPage());
	// result.setJsonArray(userList);
	// result.setNumberOfElements(userResponse.getNumberOfElements());
	// result.setSize(userResponse.getSize());
	// result.setTotalPages(userResponse.getTotalPages());
	// }
	// return "Admin/WalletBalace";
	// }
	// }
	// return "redirect:/Admin/Home";
	// }

	@RequestMapping(method = RequestMethod.POST, value = "/WalletBalOustanding")
	ResponseEntity<UserListDTO> getUserWalletBalOustanding(@ModelAttribute PagingDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) {

		try {
			dto.setStartDate(ConvertUtil.decodeBase64String(dto.getStartDate()));
			dto.setEndDate(ConvertUtil.decodeBase64String(dto.getEndDate()));
		} catch (Exception e) {

		}

		UserListDTO result = new UserListDTO();
		String pincode = null;
		String circleName = null;
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					AllUserRequest userRequest = new AllUserRequest();
					userRequest.setPage(dto.getPage());
					userRequest.setSize(dto.getSize());
					userRequest.setSessionId(sessionCheck);
					userRequest.setStatus(UserStatus.ALL);
					AllUserResponse userResponse = appAdminApi.getWalletBalanceReport(userRequest);
					JSONArray data = userResponse.getJsonArray();
					List<WalletOutsReports> userList = new ArrayList<>();
					try {
						if (data != null) {
							for (int i = 0; i < data.length(); i++) {
								AdminUserDetails list = new AdminUserDetails();
								WalletOutsReports wBal = new WalletOutsReports();
								JSONObject jsonobject = data.getJSONObject(i);

								wBal.setBalance(JSONParserUtil.getDouble(jsonobject, "balance"));
								wBal.setEmail(JSONParserUtil.getString(jsonobject, "email"));
								wBal.setMobile(JSONParserUtil.getString(jsonobject, "mobile"));
								wBal.setName(JSONParserUtil.getString(jsonobject, "name"));
								wBal.setStatus(JSONParserUtil.getString(jsonobject, "status"));

								DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");
								long milliSeconds = Long.parseLong(jsonobject.getString("date"));
								Calendar calendar = Calendar.getInstance();
								calendar.setTimeInMillis(milliSeconds);
								wBal.setDate(formatter.format(calendar.getTime()));

								userList.add(wBal);
								model.addAttribute("userlist", wBal);
							}
							result.setSuccess(true);
							result.setFirstPage(userResponse.isFirstPage());
							result.setLastPage(userResponse.isLastPage());
							result.setJsonArray(userList);
							result.setNumberOfElements(userResponse.getNumberOfElements());
							result.setSize(userResponse.getSize());
							result.setTotalPages(userResponse.getTotalPages());
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
					return new ResponseEntity<UserListDTO>(result, HttpStatus.OK);
				}
			}
		}
		return new ResponseEntity<UserListDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/CustomerTransactionReport")
	public String getCustomerTransaction(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) throws JSONException {
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {

			return "Admin/CustomerDailyReport";
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/CustomerTransactionReportInJSON")
	ResponseEntity<UserListDTO> getCustomerTransactionReportsInJSON(@ModelAttribute PagingDTO dto,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model)
			throws JSONException {
		UserListDTO resultSet = new UserListDTO();
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					AllTransactionRequest transRequest = new AllTransactionRequest();
					transRequest.setPage(dto.getPage());
					transRequest.setSize(dto.getSize());
					transRequest.setSessionId(sessionCheck);
					AllUserResponse allTransaction = appAdminApi.getAllTransactions(transRequest);
					List<TransactionListResponse> results = new ArrayList<>();
					JSONArray listArray = allTransaction.getJsonArray();
					for (int i = 0; i < allTransaction.getJsonArray().length(); i++) {
						JSONObject json = listArray.getJSONObject(i);
						TransactionListResponse userListDTO = new TransactionListResponse();
						userListDTO.setId(JSONParserUtil.getLong(json, "id"));
						userListDTO.setUsername(JSONParserUtil.getString(json, "username"));
						userListDTO.setEmail(JSONParserUtil.getString(json, "email"));
						userListDTO.setAmount(JSONParserUtil.getDouble(json, "amount"));
						userListDTO.setAuthority(JSONParserUtil.getString(json, "authority"));
						userListDTO.setCommission(JSONParserUtil.getDouble(json, "commission"));
						userListDTO.setContactNo(JSONParserUtil.getString(json, "contactNo"));
						userListDTO.setCurrentBalance(JSONParserUtil.getDouble(json, "currentBalance"));
						long milliSeconds = JSONParserUtil.getLong(json, "dateOfTransaction");
						Calendar calendar = Calendar.getInstance();
						calendar.setTimeInMillis(milliSeconds);
						userListDTO.setDateOfTransaction(dateFilter.format(calendar.getTime()));

						userListDTO.setDebit(JSONParserUtil.getBoolean(json, "debit"));
						userListDTO.setTransactionRefNo(JSONParserUtil.getString(json, "transactionRefNo"));
						userListDTO.setDescription(JSONParserUtil.getString(json, "description"));
						userListDTO.setEmail(JSONParserUtil.getString(json, "email"));
						userListDTO.setServiceType(JSONParserUtil.getString(json, "serviceType"));
						userListDTO.setStatus(Status.valueOf(JSONParserUtil.getString(json, "status")));
						results.add(userListDTO);
					}

					resultSet.setSuccess(allTransaction.isSuccess());
					resultSet.setJsonArray(results);
					resultSet.setFirstPage(allTransaction.isFirstPage());
					resultSet.setLastPage(allTransaction.isLastPage());
					resultSet.setNumberOfElements(allTransaction.getNumberOfElements());
					resultSet.setSize(allTransaction.getSize());
					resultSet.setTotalPages(allTransaction.getTotalPages());

				}
			}
		}

		return new ResponseEntity<UserListDTO>(resultSet, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/CustomerTransactionReport")
	public String filterCustomerTransactionReports(@ModelAttribute TransactionFilter dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) throws JSONException {

		try {

			dto.setStartDate(ConvertUtil.decodeBase64String(dto.getStartDate()));
			dto.setEndDate(ConvertUtil.decodeBase64String(dto.getEndDate()));

			String toDateTime = dto.getEndDate() + " 23:59";
			String fromDateTime = dto.getStartDate() + " 00:00";
			Date to = dateFilter.parse(toDateTime);
			Date from = dateFilter.parse(fromDateTime);

			String sessionCheck = (String) session.getAttribute("adminSessionId");
			if (sessionCheck != null) {
				String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
				if (authority != null) {
					if (authority.contains(Authorities.ADMINISTRATOR)
							&& authority.contains(Authorities.AUTHENTICATED)) {
						SessionDTO sessionDTO = new SessionDTO();
						sessionDTO.setSessionId(sessionCheck);
						UserTransactionResponse userTransactionResponse = appAdminApi
								.getUserTransactionValues(sessionDTO);
						if (userTransactionResponse != null) {
							if (userTransactionResponse.isSuccess()) {
								JSONObject json = null;
								try {
									json = new JSONObject(userTransactionResponse.getResponse());
								} catch (JSONException e) {
								}
								if (json != null) {
									JSONObject details = JSONParserUtil.getObject(json, "details");
									model.addAttribute("nlmca",
											JSONParserUtil.getDouble(details, "totalLoadMoneyCCAvenueNow"));
									model.addAttribute("nlmbd",
											JSONParserUtil.getDouble(details, "totalLoadMoneyBillDeskNow"));
									model.addAttribute("ntopup", JSONParserUtil.getDouble(details, "totalPayableNow"));
								}
							}
						}
						model.addAttribute(ModelMapKey.MESSAGE,
								"Transactions From " + dto.getStartDate() + " to " + dto.getEndDate());
						AllTransactionRequest transRequest = new AllTransactionRequest();
						transRequest.setPage(0);
						transRequest.setSize(100000);
						transRequest.setSessionId((String) session.getAttribute("adminSessionId"));
						AllUserResponse allTransaction = appAdminApi.getAllTransactions(transRequest);
						List<TransactionListResponse> results = new ArrayList<>();
						JSONArray listArray = allTransaction.getJsonArray();
						for (int i = 0; i < allTransaction.getJsonArray().length(); i++) {

							JSONObject json = listArray.getJSONObject(i);

							long milliSeconds = JSONParserUtil.getLong(json, "dateOfTransaction");
							Calendar calendar = Calendar.getInstance();
							calendar.setTimeInMillis(milliSeconds);
							Date txdate = calendar.getTime();

							if (txdate.after(from) && txdate.before(to)) {
								TransactionListResponse userListDTO = new TransactionListResponse();
								userListDTO.setId(JSONParserUtil.getLong(json, "id"));
								userListDTO.setUsername(JSONParserUtil.getString(json, "username"));
								userListDTO.setEmail(JSONParserUtil.getString(json, "email"));
								userListDTO.setAmount(JSONParserUtil.getDouble(json, "amount"));
								userListDTO.setAuthority(JSONParserUtil.getString(json, "authority"));
								userListDTO.setCommission(JSONParserUtil.getDouble(json, "commission"));
								userListDTO.setContactNo(JSONParserUtil.getString(json, "contactNo"));
								userListDTO.setStatus(Status.valueOf(JSONParserUtil.getString(json, "status")));
								userListDTO.setCurrentBalance(JSONParserUtil.getDouble(json, "currentBalance"));
								userListDTO.setDateOfTransaction(dateFilter.format(txdate));
								userListDTO.setDebit(JSONParserUtil.getBoolean(json, "debit"));
								userListDTO.setTransactionRefNo(JSONParserUtil.getString(json, "transactionRefNo"));
								userListDTO.setDescription(JSONParserUtil.getString(json, "description"));
								userListDTO.setEmail(JSONParserUtil.getString(json, "email"));
								userListDTO.setServiceType(JSONParserUtil.getString(json, "serviceType"));
								results.add(userListDTO);
							}
						}
						model.addAttribute("userList", results);
					}
					return "Admin/CustomerDailyReportFiltered";
				}
			}
		} catch (Exception ex) {
			model.addAttribute(ModelMapKey.MESSAGE, "Please Enter Date in valid format(YYYY-MM-DD)");
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/BankTransferList")
	public String getNEFTFormatList(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) {

		String sessionCheck = (String) session.getAttribute("adminSessionId");

		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					SessionDTO dto = new SessionDTO();
					/*
					 * dto.setSessionId(sessionCheck); List<NEFTResponse>
					 * neftList = appAdminApi.getNEFTList(dto, false, null,
					 * null); for (NEFTResponse neftResponse : neftList) { }
					 * model.addAttribute("neftList", neftList);
					 */
					return "Admin/NEFTList";
				}
			}
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/BankTransferListFiltered")
	public String getNeftFormatListFiltered(@ModelAttribute DateDTO filter, HttpServletRequest request,
			HttpServletResponse response, Model model, HttpSession session) throws ParseException {

		try {
			filter.setFromDate(ConvertUtil.decodeBase64String(filter.getFromDate()));
			filter.setToDate(ConvertUtil.decodeBase64String(filter.getToDate()));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String sessionCheck = (String) session.getAttribute("adminSessionId");
		/*
		 * String toDateTime = filter.getEndDate() + " 23:59"; String
		 * fromDateTime = filter.getStartDate() + " 00:00"; Date to =
		 * dateFilter.parse(toDateTime); Date from =
		 * dateFilter.parse(fromDateTime);
		 */
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					SessionDTO dto = new SessionDTO();
					dto.setSessionId(sessionCheck);
					List<NEFTResponse> neftList = appAdminApi.getNEFTListFiltered(filter, true);
					model.addAttribute("neftList", neftList);
					return "Admin/NeftListFiltered";
				}
			}
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/SummaryReport")
	public String getDailyUserTransaction(@ModelAttribute PagingDTO dto, HttpServletRequest request, ModelMap model,
			HttpServletResponse response, HttpSession session) {
		SimpleDateFormat dateFilter2 = new SimpleDateFormat("yyyy-MM-dd");
		String curDate = dateFilter2.format(new Date());
		if (dto.getStartDate() == null) {
			dto.setStartDate(curDate);
		} else {
			curDate = dto.getStartDate();
		}
		String adminSessionId = (String) session.getAttribute("adminSessionId");
		if (adminSessionId != null) {
			dto.setSessionId(adminSessionId);
			String authority = authenticationApi.getAuthorityFromSession(adminSessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					UserTransactionResponse userTransactionResponse = appAdminApi.getDailyTransactionReportValues(dto);
					if (userTransactionResponse != null) {
						if (userTransactionResponse.isSuccess()) {
							JSONObject json = null;
							try {
								json = new JSONObject(userTransactionResponse.getResponse());
							} catch (JSONException e) {
							}
							if (json != null) {
								JSONObject details = JSONParserUtil.getObject(json, "details");
								double walletOpBalance = JSONParserUtil.getDouble(details, "walletBalance");
								double poolBalance = JSONParserUtil.getDouble(details, "poolBalance");
								// double poolBalance = 0;

								double totalDailyLoadMoneyAmount = JSONParserUtil.getDouble(details,
										"totalDailyLoadMoneyAmount");
								int totalDailyLoadMoneyCount = (int) JSONParserUtil.getDouble(details,
										"totalDailyLoadMoneyCount");

								double topupTotalNow = JSONParserUtil.getDouble(details, "totalDailyPayoutAmount");
								int topupTotalCountNow = (int) JSONParserUtil.getDouble(details,
										"totalDailyPayoutCount");
								double totalBankTranferNow = JSONParserUtil.getDouble(details, "totalBankTranferNow");
								double totalPromoCode = JSONParserUtil.getDouble(details, "totalPromoCode");
								double totalMerchantRefund = JSONParserUtil.getDouble(details, "totalMerchantRefund");
								int totalCountBankTranferNow = (int) JSONParserUtil.getDouble(details,
										"totalCountBankTranferNow");
								int totalCountPromoCode = (int) JSONParserUtil.getDouble(details,
										"totalCountPromoCode");
								int totalCountMerchantRefund = (int) JSONParserUtil.getDouble(details,
										"totalCountMerchantRefund");

								double totalSendMoneyCredit = JSONParserUtil.getDouble(details, "totalSendMoneyCredit");
								double totalSendMoneyDebit = JSONParserUtil.getDouble(details, "totalSendMoneyDebit");
								int countSendMoneyCredit = (int) JSONParserUtil.getDouble(details,
										"countSendMoneyCredit");
								int countSendMoneyDebit = (int) JSONParserUtil.getDouble(details,
										"countSendMoneyDebit");

								double walletCloseExcepBalNow = 0;
								int walletCloseExcepCount = 0;

								double totalCredit = walletOpBalance + totalDailyLoadMoneyAmount + totalPromoCode
										+ totalMerchantRefund + totalSendMoneyCredit;
								double totalDebit = topupTotalNow + totalBankTranferNow + walletCloseExcepBalNow
										+ totalSendMoneyDebit;

								double walletCloseBal = totalCredit - totalDebit;
								double differece = walletCloseBal - poolBalance;

								String todaysDate = "";
								try {
									SimpleDateFormat dateFilter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
									Date ss = dateFilter.parse(dto.getStartDate() + " 00:00");
									String date = dateFilter.format(ss);
									Date newDate = dateFilter.parse(date);
									String s1 = newDate.toString();
									String[] words = s1.split(" ");
									todaysDate = words[2] + "-" + words[1] + "-" + words[5];
								} catch (ParseException e) {
									e.printStackTrace();
								}

								model.addAttribute("curDate", curDate);
								model.addAttribute("todaysDate", todaysDate);
								model.addAttribute("walletBalance", walletOpBalance);
								model.addAttribute("poolBalance", poolBalance);

								// POINT 1
								model.addAttribute("totalDailyLoadMoneyCount", totalDailyLoadMoneyCount);
								model.addAttribute("totalDailyLoadMoneyAmount", totalDailyLoadMoneyAmount);
								model.addAttribute("totalCountPromoCode", totalCountPromoCode);
								model.addAttribute("totalPromoCode", totalPromoCode);
								model.addAttribute("totalCountMerchantRefund", totalCountMerchantRefund);
								model.addAttribute("totalMerchantRefund", totalMerchantRefund);
								model.addAttribute("totalSendMoneyCredit", totalSendMoneyCredit);
								model.addAttribute("countSendMoneyCredit", countSendMoneyCredit);
								model.addAttribute("totalCountPromoCode", totalCountPromoCode);

								String val = String.format("%.2f", totalCredit);
								totalCredit = Double.parseDouble(val);

								model.addAttribute("totalCredit", totalCredit);

								// POINT 2
								model.addAttribute("topupTotalCountNow", topupTotalCountNow);
								model.addAttribute("topupTotalNow", topupTotalNow);
								model.addAttribute("totalCountBankTranferNow", totalCountBankTranferNow);
								model.addAttribute("totalBankTranferNow", totalBankTranferNow);
								model.addAttribute("countSendMoneyDebit", countSendMoneyDebit);
								model.addAttribute("totalSendMoneyDebit", totalSendMoneyDebit);
								model.addAttribute("walletCloseExcepCount", walletCloseExcepCount);
								model.addAttribute("walletCloseExcepBalNow", walletCloseExcepBalNow);
								model.addAttribute("totalDebit", totalDebit);

								// POINT 3
								model.addAttribute("walletClose", walletCloseBal);

								// POINT 4
								String v = String.format("%.2f", differece);
								differece = Double.parseDouble(v);
								model.addAttribute("differece", differece);

							}
						}
					}
					return "Admin/SummaryReport";
				}
			}
		}
		return "Admin/Login";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/SummaryReport")
	public String getDailyUserTransactionPOst(@ModelAttribute PagingDTO dto, HttpServletRequest request, ModelMap model,
			HttpServletResponse response, HttpSession session) {
		SimpleDateFormat dateFilter2 = new SimpleDateFormat("yyyy-MM-dd");
		String curDate = dateFilter2.format(new Date());
		if (dto.getStartDate() == null) {
			dto.setStartDate(curDate);
		} else {
			curDate = dto.getStartDate();
		}
		String adminSessionId = (String) session.getAttribute("adminSessionId");
		if (adminSessionId != null) {
			dto.setSessionId(adminSessionId);
			String authority = authenticationApi.getAuthorityFromSession(adminSessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					UserTransactionResponse userTransactionResponse = appAdminApi.getDailyTransactionReportValues(dto);
					if (userTransactionResponse != null) {
						if (userTransactionResponse.isSuccess()) {
							JSONObject json = null;
							try {
								json = new JSONObject(userTransactionResponse.getResponse());
							} catch (JSONException e) {
							}
							if (json != null) {
								JSONObject details = JSONParserUtil.getObject(json, "details");
								double walletOpBalance = JSONParserUtil.getDouble(details, "walletBalance");
								double poolBalance = JSONParserUtil.getDouble(details, "poolBalance");
								// double poolBalance = 0;

								double totalDailyLoadMoneyAmount = JSONParserUtil.getDouble(details,
										"totalDailyLoadMoneyAmount");
								int totalDailyLoadMoneyCount = (int) JSONParserUtil.getDouble(details,
										"totalDailyLoadMoneyCount");

								double topupTotalNow = JSONParserUtil.getDouble(details, "totalDailyPayoutAmount");
								int topupTotalCountNow = (int) JSONParserUtil.getDouble(details,
										"totalDailyPayoutCount");
								double totalBankTranferNow = JSONParserUtil.getDouble(details, "totalBankTranferNow");
								double totalPromoCode = JSONParserUtil.getDouble(details, "totalPromoCode");
								double totalMerchantRefund = JSONParserUtil.getDouble(details, "totalMerchantRefund");
								int totalCountBankTranferNow = (int) JSONParserUtil.getDouble(details,
										"totalCountBankTranferNow");
								int totalCountPromoCode = (int) JSONParserUtil.getDouble(details,
										"totalCountPromoCode");
								int totalCountMerchantRefund = (int) JSONParserUtil.getDouble(details,
										"totalCountMerchantRefund");

								double totalSendMoneyCredit = JSONParserUtil.getDouble(details, "totalSendMoneyCredit");
								double totalSendMoneyDebit = JSONParserUtil.getDouble(details, "totalSendMoneyDebit");
								int countSendMoneyCredit = (int) JSONParserUtil.getDouble(details,
										"countSendMoneyCredit");
								int countSendMoneyDebit = (int) JSONParserUtil.getDouble(details,
										"countSendMoneyDebit");

								double walletCloseExcepBalNow = 0;
								int walletCloseExcepCount = 0;

								double totalCredit = walletOpBalance + totalDailyLoadMoneyAmount + totalPromoCode
										+ totalMerchantRefund + totalSendMoneyCredit;
								double totalDebit = topupTotalNow + totalBankTranferNow + walletCloseExcepBalNow
										+ totalSendMoneyDebit;

								double walletCloseBal = totalCredit - totalDebit;
								double differece = walletCloseBal - poolBalance;

								String todaysDate = "";
								try {
									SimpleDateFormat dateFilter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
									Date ss = dateFilter.parse(dto.getStartDate() + " 00:00");
									String date = dateFilter.format(ss);
									Date newDate = dateFilter.parse(date);
									String s1 = newDate.toString();
									String[] words = s1.split(" ");
									todaysDate = words[2] + "-" + words[1] + "-" + words[5];
								} catch (ParseException e) {
									e.printStackTrace();
								}

								model.addAttribute("curDate", curDate);
								model.addAttribute("todaysDate", todaysDate);
								model.addAttribute("walletBalance", walletOpBalance);
								model.addAttribute("poolBalance", poolBalance);

								// POINT 1
								model.addAttribute("totalDailyLoadMoneyCount", totalDailyLoadMoneyCount);
								model.addAttribute("totalDailyLoadMoneyAmount", totalDailyLoadMoneyAmount);
								model.addAttribute("totalCountPromoCode", totalCountPromoCode);
								model.addAttribute("totalPromoCode", totalPromoCode);
								model.addAttribute("totalCountMerchantRefund", totalCountMerchantRefund);
								model.addAttribute("totalMerchantRefund", totalMerchantRefund);
								model.addAttribute("totalSendMoneyCredit", totalSendMoneyCredit);
								model.addAttribute("countSendMoneyCredit", countSendMoneyCredit);
								model.addAttribute("totalCountPromoCode", totalCountPromoCode);

								String val = String.format("%.2f", totalCredit);
								totalCredit = Double.parseDouble(val);

								model.addAttribute("totalCredit", totalCredit);

								// POINT 2
								model.addAttribute("topupTotalCountNow", topupTotalCountNow);
								model.addAttribute("topupTotalNow", topupTotalNow);
								model.addAttribute("totalCountBankTranferNow", totalCountBankTranferNow);
								model.addAttribute("totalBankTranferNow", totalBankTranferNow);
								model.addAttribute("countSendMoneyDebit", countSendMoneyDebit);
								model.addAttribute("totalSendMoneyDebit", totalSendMoneyDebit);
								model.addAttribute("walletCloseExcepCount", walletCloseExcepCount);
								model.addAttribute("walletCloseExcepBalNow", walletCloseExcepBalNow);
								model.addAttribute("totalDebit", totalDebit);

								// POINT 3
								model.addAttribute("walletClose", walletCloseBal);

								// POINT 4
								String v = String.format("%.2f", differece);
								differece = Double.parseDouble(v);
								model.addAttribute("differece", differece);

							}
						}
					}
					return "Admin/SummaryReport";
				}
			}
		}
		return "Admin/Login";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/MISReport")
	public String getMisPost(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) {
		String mobile = request.getParameter("search");

		MISDTO result = new MISDTO();
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		try {

			if (sessionCheck != null) {
				String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
				if (authority != null) {
					if (authority.contains(Authorities.ADMINISTRATOR)
							&& authority.contains(Authorities.AUTHENTICATED)) {
						MISDetailRequest userRequest = new MISDetailRequest();

						userRequest.setSessionId(sessionCheck);

						MISResponce userResponse = appAdminApi.getMISreport(userRequest);
						// JSONArray data = userResponse.getJsonArray();
						// List<AdminUserDetails> userList = new ArrayList<>();
						List<MISDTO> list = new ArrayList<>();

						for (int i = 0; i < 7; i++) {
							MISDTO dto = new MISDTO();
							dto.setClosingBal((Double) userResponse.getClosing().get(i));
							dto.setLoadAmount((Double) userResponse.getLoadMoney().get(i));
							dto.setNoOfWallet((Integer) userResponse.getCreated().get(i));
							dto.setWalletSpentAmt((Double) userResponse.getSpent().get(i));
							dto.setTotalWallet((Integer) userResponse.getWallet().get(i));
							list.add(dto);
						}

						model.addAttribute("list", list);

					}
					return "Admin/MISReport";
				}
			}
		} catch (Exception e) {
			return "redirect:/Admin/Home";
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/MISReport")
	public String getMis(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) {
		String mobile = request.getParameter("search");

		MISDTO result = new MISDTO();
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		try {

			if (sessionCheck != null) {
				String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
				if (authority != null) {
					if (authority.contains(Authorities.ADMINISTRATOR)
							&& authority.contains(Authorities.AUTHENTICATED)) {
						MISDetailRequest userRequest = new MISDetailRequest();

						userRequest.setSessionId(sessionCheck);

						MISResponce userResponse = appAdminApi.getMISreport(userRequest);
						// JSONArray data = userResponse.getJsonArray();
						// List<AdminUserDetails> userList = new ArrayList<>();
						List<MISDTO> list = new ArrayList<>();

						for (int i = 0; i < 7; i++) {
							MISDTO dto = new MISDTO();
							dto.setClosingBal((Double) userResponse.getClosing().get(i));
							dto.setLoadAmount((Double) userResponse.getLoadMoney().get(i));
							dto.setNoOfWallet((Integer) userResponse.getCreated().get(i));
							dto.setWalletSpentAmt((Double) userResponse.getSpent().get(i));
							dto.setTotalWallet((Integer) userResponse.getWallet().get(i));
							list.add(dto);
						}

						model.addAttribute("list", list);

					}
					return "Admin/MISReport";
				}
			}
		} catch (Exception e) {
			return "redirect:/Admin/Home";
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/WalletLoadReport")
	public String getWalletLoadReportPost(@ModelAttribute TransactionFilter dto1, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) throws JSONException {

		String mobile = request.getParameter("search");
		String toDateTime = dto1.getStartDate() + " 00:00";
		String endDate = dto1.getEndDate() + " 00:00";

		if (dto1.getStartDate() == null && dto1.getEndDate() == null) {

			Calendar cal = Calendar.getInstance();
			Date date = cal.getTime();
			dateFilter.format(date);
			toDateTime = dateFilter.format(date);
			String a[] = toDateTime.split(" ");
			a[1] = " 00:00";
			toDateTime = a[0] + a[1];
			endDate = toDateTime;
			model.addAttribute("msg", "Today's Wallet Load Report");
		} else {
			model.addAttribute("msg", "Wallet Load Report");
		}

		MISDTO result = new MISDTO();
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		try {
			if (sessionCheck != null) {
				String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
				if (authority != null) {
					if (authority.contains(Authorities.ADMINISTRATOR)
							&& authority.contains(Authorities.AUTHENTICATED)) {
						MISDetailRequest userRequest = new MISDetailRequest();

						userRequest.setSessionId(sessionCheck);
						userRequest.setDate(toDateTime);
						userRequest.setEndDate(endDate);
						WalletResponse userResponse = appAdminApi.getWalletReport(userRequest);

						List<WalletLoadUserDetails> list = new ArrayList<>();

						if (userResponse.getLoadMoneyDetails().length() != 0) {
							for (int i = 0; i < userResponse.getUserListDetails().length(); i++) {

								for (int j = 0; j < userResponse.getLoadMoneyDetails().length(); j++) {
									WalletLoadUserDetails dto = new WalletLoadUserDetails();
									JSONObject lJob = (JSONObject) userResponse.getLoadMoneyDetails().getJSONObject(j);
									JSONObject uJobj = userResponse.getUserListDetails().getJSONObject(i);

									long milliSeconds = Long.parseLong(lJob.getString("created"));
									long milliSeconds2 = Long.parseLong(uJobj.getString("created"));
									Calendar calendar = Calendar.getInstance();
									calendar.setTimeInMillis(milliSeconds);
									Date txdate = calendar.getTime();
									calendar.setTimeInMillis(milliSeconds2);
									Date crdate = calendar.getTime();

									if (uJobj.getJSONObject("accountDetail").getLong("id") == lJob
											.getJSONObject("account").getLong("id")) {
										dto.setDate(sdf.format(txdate));
										dto.setcEmailId(uJobj.getJSONObject("userDetail").getString("email"));
										dto.setcName(uJobj.getJSONObject("userDetail").getString("firstName"));
										dto.setMobile(uJobj.getJSONObject("userDetail").getString("contactNo"));
										dto.setAmt(lJob.getDouble("amount"));
										dto.setWalletCreated(sdf.format(crdate));
										list.add(dto);

									}

								}
							}
						}

						model.addAttribute("list", list);

					}
					return "Admin/WalletLoadReport";
				}
			}
		} catch (Exception e) {
			return "redirect:/Admin/Home";
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/WalletLoadReport")
	public String getWalletLoadReport(@ModelAttribute TransactionFilter dto1, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) throws JSONException {

		String mobile = request.getParameter("search");
		String toDateTime = dto1.getStartDate() + " 00:00";
		String endDate = dto1.getEndDate() + " 00:00";

		if (dto1.getStartDate() == null && dto1.getEndDate() == null) {

			Calendar cal = Calendar.getInstance();
			Date date = cal.getTime();
			dateFilter.format(date);
			toDateTime = dateFilter.format(date);
			String a[] = toDateTime.split(" ");
			a[1] = " 00:00";
			toDateTime = a[0] + a[1];
			endDate = toDateTime;
			model.addAttribute("msg", "Today's Wallet Load Report");
		} else {
			model.addAttribute("msg", "Wallet Load Report");
		}

		MISDTO result = new MISDTO();
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		try {
			if (sessionCheck != null) {
				String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
				if (authority != null) {
					if (authority.contains(Authorities.ADMINISTRATOR)
							&& authority.contains(Authorities.AUTHENTICATED)) {
						MISDetailRequest userRequest = new MISDetailRequest();

						userRequest.setSessionId(sessionCheck);
						userRequest.setDate(toDateTime);
						userRequest.setEndDate(endDate);
						WalletResponse userResponse = appAdminApi.getWalletReport(userRequest);

						List<WalletLoadUserDetails> list = new ArrayList<>();

						if (userResponse.getLoadMoneyDetails().length() != 0) {
							for (int i = 0; i < userResponse.getUserListDetails().length(); i++) {

								for (int j = 0; j < userResponse.getLoadMoneyDetails().length(); j++) {
									WalletLoadUserDetails dto = new WalletLoadUserDetails();
									JSONObject lJob = (JSONObject) userResponse.getLoadMoneyDetails().getJSONObject(j);
									JSONObject uJobj = userResponse.getUserListDetails().getJSONObject(i);

									long milliSeconds = Long.parseLong(lJob.getString("created"));
									long milliSeconds2 = Long.parseLong(uJobj.getString("created"));
									Calendar calendar = Calendar.getInstance();
									calendar.setTimeInMillis(milliSeconds);
									Date txdate = calendar.getTime();
									calendar.setTimeInMillis(milliSeconds2);
									Date crdate = calendar.getTime();

									if (uJobj.getJSONObject("accountDetail").getLong("id") == lJob
											.getJSONObject("account").getLong("id")) {
										dto.setDate(sdf.format(txdate));
										dto.setcEmailId(uJobj.getJSONObject("userDetail").getString("email"));
										dto.setcName(uJobj.getJSONObject("userDetail").getString("firstName"));
										dto.setMobile(uJobj.getJSONObject("userDetail").getString("contactNo"));
										dto.setAmt(lJob.getDouble("amount"));
										dto.setWalletCreated(sdf.format(crdate));
										list.add(dto);

									}

								}
							}
						}

						model.addAttribute("list", list);

					}
					return "Admin/WalletLoadReport";
				}
			}
		} catch (Exception e) {
			return "redirect:/Admin/Home";
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/filteredUserWalletResults")
	public String getFilteredUserWalletResultsPost(@ModelAttribute TransactionFilter dto, HttpServletRequest request,
			HttpServletResponse response, Model model, HttpSession session) throws ParseException {

		try {
			dto.setStartDate(ConvertUtil.decodeBase64String(dto.getStartDate()));
			dto.setEndDate(ConvertUtil.decodeBase64String(dto.getEndDate()));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		UserListDTO result = new UserListDTO();
		// String toDateTime = dto.getEndDate() + " 23:59";
		// String fromDateTime = dto.getStartDate() + " 00:00";
		// Date to = dateFilter.parse(toDateTime);
		// Date from = dateFilter.parse(fromDateTime);

		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					PagingDTO userRequest = new PagingDTO();
					userRequest.setPage(0);
					userRequest.setSize(100000);
					userRequest.setSessionId(sessionCheck);
					userRequest.setStartDate(dto.getStartDate());
					userRequest.setEndDate(dto.getEndDate());

					/*
					 * AllUserRequest userRequest = new AllUserRequest();
					 * userRequest.setPage(0); userRequest.setSize(100000);
					 * userRequest.setSessionId(sessionCheck);
					 * userRequest.setStatus(UserStatus.ALL);
					 */
					AllUserResponse userResponse = appAdminApi.getWalletBalanceReportByDate(userRequest);
					JSONArray data = userResponse.getJsonArray();
					List<WalletOutsReports> userList = new ArrayList<>();
					// List<AdminUserDetails> userList = new ArrayList<>();
					try {
						if (data != null) {
							for (int i = 0; i < data.length(); i++) {
								AdminUserDetails list = new AdminUserDetails();
								WalletOutsReports wBal = new WalletOutsReports();
								JSONObject jsonobject = data.getJSONObject(i);

								wBal.setBalance(JSONParserUtil.getDouble(jsonobject, "balance"));
								wBal.setEmail(JSONParserUtil.getString(jsonobject, "email"));
								wBal.setMobile(JSONParserUtil.getString(jsonobject, "mobile"));
								wBal.setName(JSONParserUtil.getString(jsonobject, "name"));
								wBal.setStatus(JSONParserUtil.getString(jsonobject, "status"));

								DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");
								long milliSeconds = Long.parseLong(jsonobject.getString("date"));
								Calendar calendar = Calendar.getInstance();
								calendar.setTimeInMillis(milliSeconds);
								wBal.setDate(formatter.format(calendar.getTime()));

								userList.add(wBal);
							}
							model.addAttribute("searchedlist", userList);
							return "Admin/filteredUserWalletResults";
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}

				}
			}
		}
		return "Admin/Home";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/filteredUserWalletResults")
	public String getFilteredUserWalletResults(@ModelAttribute TransactionFilter dto, HttpServletRequest request,
			HttpServletResponse response, Model model, HttpSession session) throws ParseException {

		UserListDTO result = new UserListDTO();
		// String toDateTime = dto.getEndDate() + " 23:59";
		// String fromDateTime = dto.getStartDate() + " 00:00";
		// Date to = dateFilter.parse(toDateTime);
		// Date from = dateFilter.parse(fromDateTime);

		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					PagingDTO userRequest = new PagingDTO();
					userRequest.setPage(0);
					userRequest.setSize(100000);
					userRequest.setSessionId(sessionCheck);
					userRequest.setStartDate(dto.getStartDate());
					userRequest.setEndDate(dto.getEndDate());

					/*
					 * AllUserRequest userRequest = new AllUserRequest();
					 * userRequest.setPage(0); userRequest.setSize(100000);
					 * userRequest.setSessionId(sessionCheck);
					 * userRequest.setStatus(UserStatus.ALL);
					 */
					AllUserResponse userResponse = appAdminApi.getWalletBalanceReportByDate(userRequest);
					JSONArray data = userResponse.getJsonArray();
					List<WalletOutsReports> userList = new ArrayList<>();
					// List<AdminUserDetails> userList = new ArrayList<>();
					try {
						if (data != null) {
							for (int i = 0; i < data.length(); i++) {
								AdminUserDetails list = new AdminUserDetails();
								WalletOutsReports wBal = new WalletOutsReports();
								JSONObject jsonobject = data.getJSONObject(i);

								wBal.setBalance(JSONParserUtil.getDouble(jsonobject, "balance"));
								wBal.setEmail(JSONParserUtil.getString(jsonobject, "email"));
								wBal.setMobile(JSONParserUtil.getString(jsonobject, "mobile"));
								wBal.setName(JSONParserUtil.getString(jsonobject, "name"));
								wBal.setStatus(JSONParserUtil.getString(jsonobject, "status"));

								DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");
								long milliSeconds = Long.parseLong(jsonobject.getString("date"));
								Calendar calendar = Calendar.getInstance();
								calendar.setTimeInMillis(milliSeconds);
								wBal.setDate(formatter.format(calendar.getTime()));

								userList.add(wBal);
							}
							model.addAttribute("searchedlist", userList);
							return "Admin/filteredUserWalletResults";
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}

				}
			}
		}
		return "Admin/Home";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/ListMerchantUpdated")
	public String getListMerchantUpdated(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model, PagingDTO dto) {

		String sessionCheck = (String) session.getAttribute("adminSessionId");

		if (sessionCheck != null) {
			String adminAuthority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (adminAuthority != null) {
				if (adminAuthority.contains(Authorities.ADMINISTRATOR)
						&& adminAuthority.contains(Authorities.AUTHENTICATED)) {
					AllUserRequest userRequest = new AllUserRequest();

					userRequest.setPage(0);
					userRequest.setSize(100000);
					userRequest.setSessionId((String) session.getAttribute("adminSessionId"));
					userRequest.setStatus(UserStatus.ALL);
					userRequest.setStartDate(dto.getStartDate());
					userRequest.setEndDate(dto.getEndDate());

					AllUserResponse userResponse = appAdminApi.getAllMerchants(userRequest);
					JSONArray data = userResponse.getJsonArray();
					List<AdminUserDetails> userList = new ArrayList<AdminUserDetails>();
					try {
						for (int i = 0; i < data.length(); i++) {
							AdminUserDetails list = new AdminUserDetails();
							JSONObject json = data.getJSONObject(i);
							list.setContactNo(JSONParserUtil.getString(json, "contactNo"));
							list.setDateOfAccountCreation(JSONParserUtil.getString(json, "dateOfRegistration"));
							list.setEmail(JSONParserUtil.getString(json, "email"));
							list.setFirstName(JSONParserUtil.getString(json, "name"));
							list.setImage(UrlMetadatas.WEBURL + JSONParserUtil.getString(json, "image"));
							userList.add(list);
						}

						model.put("userlist", userList);
					} catch (JSONException e) {
						e.printStackTrace();
					}
					return "Admin/ListMerchantUpdated";
				}
			}
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/ListMerchantUpdated")
	public String getListMerchantUpdatedPost(HttpServletRequest request, HttpServletResponse response,
			HttpSession session, ModelMap model, PagingDTO dto) {

		String sessionCheck = (String) session.getAttribute("adminSessionId");

		if (sessionCheck != null) {
			String adminAuthority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (adminAuthority != null) {
				if (adminAuthority.contains(Authorities.ADMINISTRATOR)
						&& adminAuthority.contains(Authorities.AUTHENTICATED)) {
					AllUserRequest userRequest = new AllUserRequest();

					userRequest.setPage(0);
					userRequest.setSize(100000);
					userRequest.setSessionId((String) session.getAttribute("adminSessionId"));
					userRequest.setStatus(UserStatus.ALL);
					userRequest.setStartDate(dto.getStartDate());
					userRequest.setEndDate(dto.getEndDate());

					AllUserResponse userResponse = appAdminApi.getAllMerchants(userRequest);
					JSONArray data = userResponse.getJsonArray();
					List<AdminUserDetails> userList = new ArrayList<AdminUserDetails>();
					try {
						for (int i = 0; i < data.length(); i++) {
							AdminUserDetails list = new AdminUserDetails();
							JSONObject json = data.getJSONObject(i);
							list.setContactNo(JSONParserUtil.getString(json, "contactNo"));
							list.setDateOfAccountCreation(JSONParserUtil.getString(json, "dateOfRegistration"));
							list.setEmail(JSONParserUtil.getString(json, "email"));
							list.setFirstName(JSONParserUtil.getString(json, "name"));
							list.setImage(UrlMetadatas.WEBURL + JSONParserUtil.getString(json, "image"));
							userList.add(list);
						}

						model.put("userlist", userList);
					} catch (JSONException e) {
						e.printStackTrace();
					}
					return "Admin/ListMerchantUpdated";
				}
			}
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/InstantPayRefundReport")
	public String InstantPayTransactionRefundReports(@ModelAttribute TransactionFilter dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) throws JSONException {
		try {

			String sessionCheck = (String) session.getAttribute("adminSessionId");
			if (sessionCheck != null) {
				String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
				if (authority != null) {
					if (authority.contains(Authorities.ADMINISTRATOR)
							&& authority.contains(Authorities.AUTHENTICATED)) {
						SessionDTO sessionDTO = new SessionDTO();
						sessionDTO.setSessionId(sessionCheck);
						UserTransactionResponse userTransactionResponse = appAdminApi
								.getUserTransactionValues(sessionDTO);
						if (userTransactionResponse != null) {
							if (userTransactionResponse.isSuccess()) {
								JSONObject json = null;
								try {
									json = new JSONObject(userTransactionResponse.getResponse());
								} catch (JSONException e) {
								}
								if (json != null) {
									JSONObject details = JSONParserUtil.getObject(json, "details");
									model.addAttribute("nlmca",
											JSONParserUtil.getDouble(details, "totalLoadMoneyCCAvenueNow"));
									model.addAttribute("nlmbd",
											JSONParserUtil.getDouble(details, "totalLoadMoneyBillDeskNow"));
									model.addAttribute("ntopup", JSONParserUtil.getDouble(details, "totalPayableNow"));
								}
							}
						}
						model.addAttribute(ModelMapKey.MESSAGE,
								"Transactions From " + dto.getStartDate() + " to " + dto.getEndDate());
						AllTransactionRequest transRequest = new AllTransactionRequest();
						transRequest.setPage(0);
						transRequest.setSize(100000);
						transRequest.setSessionId((String) session.getAttribute("adminSessionId"));
						AllUserResponse allTransaction = appAdminApi.getAllIPayRefundTxn(transRequest);
						List<TransactionListResponse> results = new ArrayList<>();
						JSONArray listArray = allTransaction.getJsonArray();
						for (int i = 0; i < allTransaction.getJsonArray().length(); i++) {

							JSONObject json = listArray.getJSONObject(i);

							long milliSeconds = JSONParserUtil.getLong(json, "dateOfTransaction");
							Calendar calendar = Calendar.getInstance();
							calendar.setTimeInMillis(milliSeconds);
							Date txdate = calendar.getTime();
							TransactionListResponse userListDTO = new TransactionListResponse();
							userListDTO.setId(JSONParserUtil.getLong(json, "id"));
							userListDTO.setUsername(JSONParserUtil.getString(json, "username"));
							userListDTO.setAuthority(JSONParserUtil.getString(json, "authority"));
							userListDTO.setCommission(JSONParserUtil.getDouble(json, "commission"));
							userListDTO.setContactNo(JSONParserUtil.getString(json, "contactNo"));
							userListDTO.setStatus(Status.valueOf(JSONParserUtil.getString(json, "status")));
							userListDTO.setCurrentBalance(JSONParserUtil.getDouble(json, "currentBalance"));
							userListDTO.setDateOfTransaction(dateFilter.format(txdate));
							if (JSONParserUtil.getBoolean(json, "debit")) {
								userListDTO.setAmountPayable((Double.parseDouble(json.getString("amount"))));
							} else {
								userListDTO.setAmountReceivable((Double.parseDouble(json.getString("amount"))));
							}
							userListDTO.setDebit(JSONParserUtil.getBoolean(json, "debit"));
							userListDTO.setTransactionRefNo(JSONParserUtil.getString(json, "transactionRefNo"));
							// userListDTO.setRetrievalRefNo(JSONParserUtil.getString(json,
							// "retrievalRefNo"));
							userListDTO.setDescription(JSONParserUtil.getString(json, "description"));
							userListDTO.setEmail(JSONParserUtil.getString(json, "email"));
							userListDTO.setServiceType(JSONParserUtil.getString(json, "serviceType"));
							// if
							// (userListDTO.getStatus().getValue().equalsIgnoreCase("Refunded"))
							// {
							// results.add(userListDTO);
							// }
							results.add(userListDTO);

						}
						model.addAttribute("userList", results);
					}
					return "Admin/InstantPayRefunded";
				}
			}
		} catch (Exception ex) {
			model.addAttribute(ModelMapKey.MESSAGE, "Please Enter Date in valid format(YYYY-MM-DD)");
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/InstantPayRefundFilturedReport")
	public String InstantPayRefundFilturedReports(@ModelAttribute TransactionFilter dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) throws JSONException {
		try {

			String toDateTime = dto.getEndDate() + " 23:59";
			String fromDateTime = dto.getStartDate() + " 00:00";
			Date to = dateFilter.parse(toDateTime);
			Date from = dateFilter.parse(fromDateTime);

			String sessionCheck = (String) session.getAttribute("adminSessionId");
			if (sessionCheck != null) {
				String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
				if (authority != null) {
					if (authority.contains(Authorities.ADMINISTRATOR)
							&& authority.contains(Authorities.AUTHENTICATED)) {
						SessionDTO sessionDTO = new SessionDTO();
						sessionDTO.setSessionId(sessionCheck);
						UserTransactionResponse userTransactionResponse = appAdminApi
								.getUserTransactionValues(sessionDTO);
						if (userTransactionResponse != null) {
							if (userTransactionResponse.isSuccess()) {
								JSONObject json = null;
								try {
									json = new JSONObject(userTransactionResponse.getResponse());
								} catch (JSONException e) {
								}
								if (json != null) {
									JSONObject details = JSONParserUtil.getObject(json, "details");
									model.addAttribute("nlmca",
											JSONParserUtil.getDouble(details, "totalLoadMoneyCCAvenueNow"));
									model.addAttribute("nlmbd",
											JSONParserUtil.getDouble(details, "totalLoadMoneyBillDeskNow"));
									model.addAttribute("ntopup", JSONParserUtil.getDouble(details, "totalPayableNow"));
								}
							}
						}
						model.addAttribute(ModelMapKey.MESSAGE,
								"Transactions From " + dto.getStartDate() + " to " + dto.getEndDate());
						AllTransactionRequest transRequest = new AllTransactionRequest();
						transRequest.setPage(0);
						transRequest.setSize(100000);
						transRequest.setSessionId((String) session.getAttribute("adminSessionId"));
						AllUserResponse allTransaction = appAdminApi.getAllIPayRefundByDate(transRequest);
						List<TransactionListResponse> results = new ArrayList<>();
						JSONArray listArray = allTransaction.getJsonArray();
						for (int i = 0; i < allTransaction.getJsonArray().length(); i++) {

							JSONObject json = listArray.getJSONObject(i);
							long milliSeconds = JSONParserUtil.getLong(json, "dateOfTransaction");
							Calendar calendar = Calendar.getInstance();
							calendar.setTimeInMillis(milliSeconds);
							Date txdate = calendar.getTime();
							if (txdate.after(from) && txdate.before(to)) {
								TransactionListResponse userListDTO = new TransactionListResponse();
								userListDTO.setId(JSONParserUtil.getLong(json, "id"));
								userListDTO.setUsername(JSONParserUtil.getString(json, "username"));
								userListDTO.setAuthority(JSONParserUtil.getString(json, "authority"));
								userListDTO.setCommission(JSONParserUtil.getDouble(json, "commission"));
								userListDTO.setContactNo(JSONParserUtil.getString(json, "contactNo"));
								userListDTO.setStatus(Status.valueOf(JSONParserUtil.getString(json, "status")));
								userListDTO.setCurrentBalance(JSONParserUtil.getDouble(json, "currentBalance"));
								userListDTO.setDateOfTransaction(dateFilter.format(txdate));

								if (JSONParserUtil.getBoolean(json, "debit")) {
									userListDTO.setAmountPayable((Double.parseDouble(json.getString("amount"))));
								} else {
									userListDTO.setAmountReceivable((Double.parseDouble(json.getString("amount"))));
								}
								userListDTO.setDebit(JSONParserUtil.getBoolean(json, "debit"));
								userListDTO.setTransactionRefNo(JSONParserUtil.getString(json, "transactionRefNo"));
								// userListDTO.setRetrievalRefNo(JSONParserUtil.getString(json,
								// "retrievalRefNo"));
								userListDTO.setDescription(JSONParserUtil.getString(json, "description"));
								userListDTO.setEmail(JSONParserUtil.getString(json, "email"));
								userListDTO.setServiceType(JSONParserUtil.getString(json, "serviceType"));

								results.add(userListDTO);

							}
						}
						model.addAttribute("userList", results);
					}
					return "Admin/InstantPayRefunded";
				}
			}
		} catch (Exception ex) {
			model.addAttribute(ModelMapKey.MESSAGE, "Please Enter Date in valid format(YYYY-MM-DD)");
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/WooHooCardList")
	public String getWooHooCardList(@ModelAttribute DateDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) {
		UserListDTO result = new UserListDTO();
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionCheck);
					GiftCardDetailResponse cards = appAdminApi.getAllGiftCards(dto);
					if (cards.isSuccess()) {
						model.addAttribute("transactionList", cards.getGiftCards());
						model.addAttribute(ModelMapKey.MESSAGE, cards.getMessage());
					} else {
						model.addAttribute(ModelMapKey.ERROR, cards.getMessage());
					}
					return "Admin/WooHooCardList";
				}
			}
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/WooHooTransactionReport")
	public String getWooHooTransactionList(@ModelAttribute DateDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) {
		try {
			String sessionCheck = (String) session.getAttribute("adminSessionId");
			if (sessionCheck != null) {
				String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
				if (authority != null) {
					if (authority.contains(Authorities.ADMINISTRATOR)
							&& authority.contains(Authorities.AUTHENTICATED)) {
						if (dto.getFromDate() == null || dto.getToDate() == null
								|| dto.getFromDate().equalsIgnoreCase("") || dto.getToDate().equalsIgnoreCase("")) {
							Date date = new Date();
							SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
							String dateText = df2.format(date);
							dto.setToDate(dateText);
							Calendar cal = Calendar.getInstance();
							cal.add(Calendar.MONTH, -1);
							dto.setFromDate(df2.format(cal.getTime()));
						}
						model.addAttribute(ModelMapKey.MESSAGE,
								" WooHoo Transactions From " + dto.getFromDate() + " to " + dto.getToDate());
						AllTransactionRequest transRequest = new AllTransactionRequest();
						transRequest.setPage(0);
						transRequest.setSize(100000);
						transRequest.setStartDate(dto.getFromDate());
						transRequest.setEndDate(dto.getToDate());
						transRequest.setSessionId((String) session.getAttribute("adminSessionId"));
						AllTransactionResponse allTransaction = appAdminApi.getWoohooTransactionReport(transRequest);
						if (allTransaction.isSuccess()) {
							List<TransactionReport> reports = new ArrayList<>();
							JSONArray reportArray = allTransaction.getJsonArray();
							if (reportArray != null) {
								for (int i = 0; i < reportArray.length(); i++) {
									JSONObject json = reportArray.getJSONObject(i);
									TransactionReport report = new TransactionReport();
									report.setContactNo(JSONParserUtil.getString(json, "contactNo"));
									report.setUsername(JSONParserUtil.getString(json, "username"));
									report.setAmount(JSONParserUtil.getDouble(json, "amount"));
									report.setDate(JSONParserUtil.getString(json, "date"));
									report.setTransactionRefNo(JSONParserUtil.getString(json, "transactionRefNo"));
									report.setStatus(Status.valueOf(JSONParserUtil.getString(json, "status")));
									report.setDescription(JSONParserUtil.getString(json, "description"));
									reports.add(report);
								}
								model.addAttribute("transactionList", reports);
							}
						}
						return "Admin/WooHooTransactions";
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/WooHooTransactionReport")
	public String getWooHooTransactionPostList(@ModelAttribute DateDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) {
		try {
			String sessionCheck = (String) session.getAttribute("adminSessionId");
			if (sessionCheck != null) {
				String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
				if (authority != null) {
					if (authority.contains(Authorities.ADMINISTRATOR)
							&& authority.contains(Authorities.AUTHENTICATED)) {
						if (dto.getFromDate() == null || dto.getToDate() == null
								|| dto.getFromDate().equalsIgnoreCase("") || dto.getToDate().equalsIgnoreCase("")) {
							Date date = new Date();
							SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
							String dateText = df2.format(date);
							dto.setToDate(dateText);
							Calendar cal = Calendar.getInstance();
							cal.add(Calendar.MONTH, -1);
							dto.setFromDate(df2.format(cal.getTime()));
						}
						model.addAttribute(ModelMapKey.MESSAGE,
								" WooHoo Transactions From " + dto.getFromDate() + " to " + dto.getToDate());
						AllTransactionRequest transRequest = new AllTransactionRequest();
						transRequest.setPage(0);
						transRequest.setSize(100000);
						transRequest.setStartDate(dto.getFromDate());
						transRequest.setEndDate(dto.getToDate());
						transRequest.setSessionId((String) session.getAttribute("adminSessionId"));
						AllTransactionResponse allTransaction = appAdminApi.getWoohooTransactionReport(transRequest);
						if (allTransaction.isSuccess()) {
							List<TransactionReport> reports = new ArrayList<>();
							JSONArray reportArray = allTransaction.getJsonArray();
							if (reportArray != null) {
								for (int i = 0; i < reportArray.length(); i++) {
									JSONObject json = reportArray.getJSONObject(i);
									TransactionReport report = new TransactionReport();
									report.setContactNo(JSONParserUtil.getString(json, "contactNo"));

									report.setUsername(JSONParserUtil.getString(json, "username"));
									report.setAmount(JSONParserUtil.getDouble(json, "amount"));
									report.setDate(JSONParserUtil.getString(json, "date"));
									// report.setService(JSONParserUtil.getString(json,
									// "service"));
									/*
									 * report.setDebit(JSONParserUtil.getBoolean
									 * (json, "debit"));
									 */
									report.setTransactionRefNo(JSONParserUtil.getString(json, "transactionRefNo"));
									report.setStatus(com.payqwikweb.model.web.Status
											.valueOf(JSONParserUtil.getString(json, "status")));
									report.setDescription(JSONParserUtil.getString(json, "description"));
									reports.add(report);
								}
								model.addAttribute("transactionList", reports);
							}
						}
						return "Admin/WooHooTransactions";
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/upiTxn")
	public String getUPITransaction(HttpSession session) {
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					return "Admin/UPITransaction";
				}
			}
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/upiTxn")
	ResponseEntity<UserListDTO> getFilteredTransactions(@ModelAttribute PagingDTO dto, HttpSession session) {
		UserListDTO result = new UserListDTO();
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					AllUserRequest userRequest = new AllUserRequest();
					userRequest.setPage(dto.getPage());
					userRequest.setSize(dto.getSize());
					userRequest.setSessionId((String) session.getAttribute("adminSessionId"));
					userRequest.setPageable(true);
					TransactionReportResponse listResponse = appAdminApi.getUpiTransaction(userRequest);
					List<TListDTO> txnList = listResponse.getList();
					result.setSuccess(true);
					// result.setFirstPage(true);
					// result.setLastPage(true);
					result.setJsonArray(txnList);
					result.setTotalPages(listResponse.getAmount());
					result.setSize(userRequest.getSize());
					return new ResponseEntity<UserListDTO>(result, HttpStatus.OK);
				}
			}
		}
		return new ResponseEntity<UserListDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/filteredUpiTxn")
	public String getFilteredUpiTxn(@ModelAttribute DateDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) {

		try {
			dto.setFromDate(ConvertUtil.decodeBase64String(dto.getFromDate()));
			dto.setToDate(ConvertUtil.decodeBase64String(dto.getToDate()));
		} catch (Exception e) {

		}

		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionCheck);
					AllUserRequest userRequest = new AllUserRequest();
					userRequest.setSessionId((String) session.getAttribute("adminSessionId"));
					userRequest.setPageable(false);
					if (CommonValidation.isNull(dto.getFromDate()) || CommonValidation.isNull(dto.getToDate())) {
						String[] date = CommonUtil.getDefaultDateRange();
						userRequest.setStartDate(date[0]);
						userRequest.setEndDate(date[1]);
						// dto.setFromDate(date[0]);
						// dto.setToDate(date[1]);
					} else {
						userRequest.setStartDate(dto.getFromDate());
						userRequest.setEndDate(dto.getToDate());
					}
					TransactionReportResponse listResponse = appAdminApi.getUpiTransaction(userRequest);
					if (listResponse.isSuccess()) {
						model.addAttribute("transactionList", listResponse.getList());
						model.addAttribute(ModelMapKey.MESSAGE,
								"Transaction List b/w " + dto.getFromDate() + " and " + dto.getToDate());
					} else {
						model.addAttribute(ModelMapKey.ERROR, listResponse.getMessage());
					}
					return "Admin/filteredUpiTxn";
				}
			}
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(value = { "/Offers" }, method = RequestMethod.GET)
	public String getAddOffers(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			Model model) {
		String sessionId = (String) session.getAttribute("adminSessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					OffersRequest offerRequest = new OffersRequest();
					model.addAttribute("addOffers", offerRequest);
					return "Admin/AddOffers";
				}
			}

		}
		return "Admin/Login";
	}

	@RequestMapping(value = { "/Offers" }, method = RequestMethod.POST)
	public String processOffers(@ModelAttribute("addOffers") OffersRequest offer, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) {
		String sessionId = (String) session.getAttribute("adminSessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					offer.setSessionId(sessionId);
					AddOffersResponse resp = appAdminApi.addOffers(offer);
					if (resp.isSuccess()) {
						model.addAttribute(ModelMapKey.MESSAGE, resp.getDetails());
						return "Admin/AddOffers";
					} else {
						model.addAttribute(ModelMapKey.MESSAGE, resp.getMessage());
						return "Admin/AddOffers";
					}
				}
			}
		}
		return "Admin/Login";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/EditPromoCode")
	public String editPromoCode(PromoCodeRequest promoCode, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, ModelMap model, RedirectAttributes redirect) {
		String sessionId = (String) session.getAttribute("adminSessionId");
		String startDateTime = promoCode.getStartDate();
		String endDateTime = promoCode.getEndDate();
		promoCode.setStartDate(startDateTime);
		promoCode.setEndDate(endDateTime);
		if (sessionId != null) {
			PromoCodeError error = promoCodeValidation.checkEditRequest(promoCode);
			if (error.isValid()) {
				promoCode.setSessionId(sessionId);
				PromoCodeResponse resp = promoCodeApi.editRequest(promoCode);
				if (resp.getCode().equalsIgnoreCase("F03") || resp.getCode().equalsIgnoreCase("F05")) {
					return "/Admin/Login";
				}

				session.setAttribute("respMsg", resp.getMessage());
				session.setAttribute("respCode", resp.getCode());

				return "redirect:/Admin/PromoCodeList";
			}
			session.setAttribute("errorMsg", error.getMessage());
		}
		return "redirect:/Admin/PromoCodeList";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/NEFTList")
	public String getUserNeftList(HttpSession session) {
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					return "Admin/NEFTList";
				}
			}
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/FilteredNeftList")
	ResponseEntity<UserListDTO> getFilteredNeftTransactions(@ModelAttribute PagingDTO dto, HttpSession session) {
		UserListDTO result = new UserListDTO();
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					AllUserRequest userRequest = new AllUserRequest();
					userRequest.setPage(dto.getPage());
					userRequest.setSize(dto.getSize());
					userRequest.setSessionId((String) session.getAttribute("adminSessionId"));
					userRequest.setPageable(true);
					TransactionReportResponse listResponse = appAdminApi.getUpiTransaction(userRequest);
					List<TListDTO> txnList = listResponse.getList();
					result.setSuccess(true);
					result.setJsonArray(txnList);
					result.setTotalPages(listResponse.getAmount());
					result.setSize(userRequest.getSize());
					return new ResponseEntity<UserListDTO>(result, HttpStatus.OK);
				}
			}
		}
		return new ResponseEntity<UserListDTO>(result, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/gcmList")
	ResponseEntity<UserListDTO> getGcmList(@ModelAttribute PagingDTO dto, HttpSession session) {
		UserListDTO result = new UserListDTO();
		boolean isValid = validateAdminAndSuperAdmin((String) session.getAttribute("adminSessionId"),
				(String) session.getAttribute("superAdminSession"));
		if (isValid) {
			AllUserRequest userRequest = new AllUserRequest();
			userRequest.setPage(dto.getPage());
			userRequest.setSize(dto.getSize());
			userRequest.setPageable(true);
			TransactionReportResponse listResponse = appAdminApi.getGcmList(userRequest);
			result.setSuccess(true);
			result.setJsonArray(listResponse.getGcmNotification());
			result.setTotalPages(listResponse.getAmount());
			result.setSize(userRequest.getSize());
		}
		return new ResponseEntity<UserListDTO>(result, HttpStatus.OK);
	}

	private boolean validateAdminAndSuperAdmin(String adminSess, String superAdminSess) {
		if (adminSess != null) {
			String authority = authenticationApi.getAuthorityFromSession(adminSess, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					return true;
				}
			}
		} else if (superAdminSess != null) {
			String authority = authenticationApi.getAuthorityFromSession(superAdminSess, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					return true;
				}
			}
		}
		return false;
	}
	@RequestMapping(method = RequestMethod.GET, value = "/PredictAndWinUpload")
	public String getPredictFile(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) {
		String sessionId = (String) session.getAttribute("adminSessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					return "/Admin/PredictAndWinUpload";
				}

			}
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/PredictAndWinUploaded")
	public String submitPredictFile(@ModelAttribute RequestRefund filter, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model)  {

		String sessionId = (String) session.getAttribute("adminSessionId");
		try {
			if (sessionId != null && sessionId.length() != 0) {
				String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
				if (authority != null) {
					if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
						filter.setSessionId(sessionId);
						String rootDirectory = request.getSession().getServletContext().getRealPath("/");
						if(!CommonUtil.isValidCSVFormate(filter.getFile())){
							model.addAttribute("message", "Bad format, please upload csv format only");
							return "Admin/PredictAndWinUpload";
						}
						String path = saveRefundReport(rootDirectory, filter.getFile(), filter.getFileName());
						String file = path.substring(18);
						String fileName = StartupUtil.CSV_FILE + file;
						if (fileName != null) {
							Set<SendMoneyMobileDTO> sendMoneyList = readFromFile(fileName);
							Iterator<SendMoneyMobileDTO> iterator = sendMoneyList.iterator();
							List<SendMoneyMobileDTO> tokens = new ArrayList<SendMoneyMobileDTO>();
							if (sendMoneyList != null && !sendMoneyList.isEmpty()) {
								while (iterator.hasNext()) {
									SendMoneyMobileDTO dto = iterator.next();
									tokens.add(dto);
								}
								filter.setMobileDTO(tokens);
								ResponseDTO responseDTO = appAdminApi.predictAndWinPayment(filter);
								model.addAttribute("message", responseDTO.getMessage());
								return "Admin/PredictAndWinUpload";
							}else{
								model.addAttribute("message", "Data incorrect or found empty data");
								return "Admin/PredictAndWinUpload";
							}
						}
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			model.addAttribute("message", "Internal Server Error");
			return "Admin/BulkUpload";
		}
		
		return "redirect:/Admin/Home";
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/AddVoucher")
	public String addVouchers(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			Model model) {
		SessionDTO dto=new SessionDTO();
		String adminSessionId = (String) session.getAttribute("adminSessionId");
		dto.setSessionId(adminSessionId);
		if (adminSessionId != null && adminSessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(adminSessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					return "Admin/AddVoucher";
				}
			}
		}
		return "Admin/Login";
	}
	
	
	@RequestMapping(method = RequestMethod.POST, value = "/AddVoucher")
	ResponseEntity<AddVoucherResponse> addVoucher(@RequestBody AddVoucherReq dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) {
		AddVoucherResponse result = new AddVoucherResponse();
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					SingleUserAccDetailsReq userRequest = new SingleUserAccDetailsReq();
					userRequest.setSessionId(sessionCheck);
					dto.setSessionId(sessionCheck);
					result=appAdminApi.addVouchers(dto);
					return new ResponseEntity<AddVoucherResponse>(result, HttpStatus.OK);
				}
			}
		}
		result.setMessage("session Expired. Please Login");
		return new ResponseEntity<AddVoucherResponse>(result, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/VoucherList")
	ResponseEntity<UserListDTO> getVoucherListInJSON(@ModelAttribute PagingDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) {
		UserListDTO result = new UserListDTO();
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					AllUserRequest userRequest = new AllUserRequest();
					userRequest.setPage(dto.getPage());
					userRequest.setSize(dto.getSize());
					userRequest.setSessionId(sessionCheck);
					userRequest.setStatus(UserStatus.ALL);
					AllUserResponse userResponse = appAdminApi.getAllVoucher(userRequest);
					JSONArray data = userResponse.getJsonArray();
					List<VoucherDTO> voucherList = new ArrayList<>();
					try {
						if (data != null) {
							for (int i = 0; i < data.length(); i++) {
								String expired="";
								String redeemed="";
								VoucherDTO voucherDTO = new VoucherDTO();
								JSONObject jsonobject = data.getJSONObject(i);
								voucherDTO.setVoucherNo(jsonobject.getString("voucherNo"));
								voucherDTO.setAmount(jsonobject.getDouble("amount"));
								if (jsonobject.getBoolean("expired")) {
									expired="Yes";
								}else {
									expired="No";
								}
								if (jsonobject.getBoolean("redeemed")) {
									redeemed="Yes";
								} else {
									redeemed="No";
								}
								voucherDTO.setExpired(expired);
								voucherDTO.setRedeemed(redeemed);
								String expiryDate=convertLongToDate(jsonobject.getLong("expiryDate"));
								String createdDate=convertLongToDate(jsonobject.getLong("created"));
								voucherDTO.setCreatedDate(createdDate);
								voucherDTO.setExpiryDate(expiryDate);
								voucherList.add(voucherDTO);
							}
							result.setSuccess(true);
							result.setFirstPage(userResponse.isFirstPage());
							result.setLastPage(userResponse.isLastPage());
							result.setJsonArray(voucherList);
							result.setNumberOfElements(userResponse.getNumberOfElements());
							result.setSize(userResponse.getSize());
							result.setTotalPages(userResponse.getTotalPages());
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
					return new ResponseEntity<UserListDTO>(result, HttpStatus.OK);
				}
			}
		}
		return new ResponseEntity<UserListDTO>(result, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/VoucherList")
	public String getVoucherList(HttpServletRequest request, HttpSession session) {
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					return "Admin/VoucherList";
				}
			}
		}
		return "redirect:/Admin/Home";
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/filteredVoucherlist")
	public String getFilteredVoucherList(@ModelAttribute DateDTO dto, HttpServletRequest request,
			HttpServletResponse response, Model model, HttpSession session) throws JSONException {

		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionCheck);
					AllUserResponse userResponse = appAdminApi.getfilteredVoucherList(dto);
					List<VoucherDTO> results = new ArrayList<>();
					JSONArray listArray = userResponse.getJsonArray();
					for (int i = 0; i < listArray.length(); i++) {
						JSONObject json = listArray.getJSONObject(i);
						String expired="";
						String redeemed="";
						VoucherDTO voucherDTO = new VoucherDTO();
						voucherDTO.setVoucherNo(json.getString("voucherNo"));
						voucherDTO.setAmount(json.getDouble("amount"));
						if (json.getBoolean("expired")) {
							expired="Yes";
						}else {
							expired="No";
						}
						if (json.getBoolean("redeemed")) {
							redeemed="Yes";
						} else {
							redeemed="No";
						}
						voucherDTO.setExpired(expired);
						voucherDTO.setRedeemed(redeemed);
						String expiryDate=convertLongToDate(json.getLong("expiryDate"));
						String createdDate=convertLongToDate(json.getLong("created"));
						voucherDTO.setCreatedDate(createdDate);
						voucherDTO.setExpiryDate(expiryDate);
						results.add(voucherDTO);
					}
					model.addAttribute("searchedlist", results);
					return "Admin/filteredVoucherList";

				}
			}
		}
		return "redirect:/Admin/Home";
	}
	
	private String convertLongToDate(long param)
	{
		Date date=new Date(param);
	    SimpleDateFormat df2 = new SimpleDateFormat("dd/MM/yy");
	    String dateText = df2.format(date);
		return dateText;
	}
}
