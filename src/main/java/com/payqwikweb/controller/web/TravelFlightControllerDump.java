package com.payqwikweb.controller.web;

import java.net.Inet4Address;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.instantpay.api.IValidationApi;
import com.payqwikweb.api.IAuthenticationApi;
import com.payqwikweb.app.api.ILoadMoneyApi;
import com.payqwikweb.app.api.ITravelFlightApi;
import com.payqwikweb.app.metadatas.UrlMetadatas;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.ResponseStatus;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.flight.response.FlightResponseDTO;
import com.payqwikweb.app.model.request.CreateJsonRequestFlight;
import com.payqwikweb.app.model.request.FlightVnetRequest;
import com.payqwikweb.app.model.request.TravelFlightRequest;
import com.payqwikweb.app.model.request.VNetRequest;
import com.payqwikweb.app.model.response.EBSRedirectResponse;
import com.payqwikweb.app.model.response.FlightPriceCheckRequest;
import com.payqwikweb.app.model.response.FlightResponse;
import com.payqwikweb.app.model.response.LoadMoneyResponse;
import com.payqwikweb.app.model.response.VNetResponse;
import com.payqwikweb.app.model.response.VRedirectResponse;
import com.payqwikweb.model.app.request.FlightBookRequest;
import com.payqwikweb.model.app.request.LoadMoneyFlightRequest;
import com.payqwikweb.model.app.request.ReturnFlightBookRequest;
import com.payqwikweb.model.web.WEBSRedirectResponse;
import com.payqwikweb.util.APIUtils;
import com.payqwikweb.util.Authorities;
import com.payqwikweb.util.ConvertUtil;
import com.payqwikweb.util.ModelMapKey;
import com.payqwikweb.validation.LoadMoneyValidation;
import com.sun.org.apache.bcel.internal.classfile.Code;
import com.thirdparty.model.ResponseDTO;

@Controller
@RequestMapping("/User/Travel")
public class TravelFlightControllerDump implements MessageSourceAware {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private MessageSource messageSource;
	private final ITravelFlightApi travelflightapi;
	private final IAuthenticationApi authenticationApi;
	private final IValidationApi validationApi;
	private final ILoadMoneyApi loadMoneyApi;
	private final LoadMoneyValidation loadMoneyValidation;

	public TravelFlightControllerDump(ITravelFlightApi travelflightapi, IAuthenticationApi authenticationApi,
			IValidationApi validationApi, ILoadMoneyApi loadMoneyApi, LoadMoneyValidation loadMoneyValidation) {
		// TODO Auto-generated constructor stub
		this.travelflightapi = travelflightapi;
		this.authenticationApi = authenticationApi;
		this.validationApi = validationApi;
		this.loadMoneyApi = loadMoneyApi;
		this.loadMoneyValidation = loadMoneyValidation;
	}

	@Override
	public void setMessageSource(MessageSource arg0) {
		// TODO Auto-generated method stub
		this.messageSource = messageSource;

	}

	@RequestMapping(method = RequestMethod.GET, value = "/Flight/OneWay")
	public String getFlightOneWay(HttpSession session) {
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null && sessionId.length() != 0) {
			return "User/Travel/Flight/flight-one-way";
		} else {
			return "redirect:/Home";
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "/Flight/RoundWay")
	public String getFlightRoundWay(HttpSession session) {
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null && sessionId.length() != 0) {
			return "User/Travel/Flight/flight-roundtrip";
		} else {
			return "redirect:/Home";
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "/Flight/internationalRoundWay")
	public String getFlightinternationalRoundWay(HttpSession session) {
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null && sessionId.length() != 0) {
			return "User/Travel/Flight/internationalRoundWay";
		} else {
			return "redirect:/Home";
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "/Flight/CheckOut")
	public String getFlightCheckOut(HttpSession session) {
		String sessionId = (String) session.getAttribute("sessionId");
		try {

			if (sessionId != null && sessionId.length() != 0) {

				String airRePriceRQ = "" + session.getAttribute("airRePriceRQ");
				String userflightamount = "";
				JSONObject obj = new JSONObject(airRePriceRQ);

				JSONArray journeys = obj.getJSONObject("details").getJSONArray("journeys");

				JSONArray segments = journeys.getJSONObject(0).getJSONArray("segments");

				JSONArray bonds = segments.getJSONObject(0).getJSONArray("bonds");
				JSONArray fare = segments.getJSONObject(0).getJSONArray("fare");

				for (int k = 0; k < bonds.length(); k++) {
					JSONArray paxFares = fare.getJSONObject(k).getJSONArray("paxFares");

					JSONArray legs = bonds.getJSONObject(k).getJSONArray("legs");
					String airlineName = "" + legs.getJSONObject(k).getString("airlineName");
					String arrivalTime = "" + legs.getJSONObject(k).getString("arrivalTime");
					String departureDate = "" + legs.getJSONObject(k).getString("departureDate");
					String departureTime = "" + legs.getJSONObject(k).getString("departureTime");
					String destination = "" + legs.getJSONObject(k).getString("destination");
					String duration = "" + legs.getJSONObject(k).getString("duration");
					String flightNumber = "" + legs.getJSONObject(k).getString("flightNumber");
					String origin = "" + legs.getJSONObject(k).getString("origin");
					String arrivalDate = "" + legs.getJSONObject(k).getString("arrivalDate");
					String TotalFare = "" + paxFares.getJSONObject(k).getString("totalFare");
					String TotalTax = "" + paxFares.getJSONObject(k).getString("totalTax");
					String TotalFareWithOutMarkUp = "" + fare.getJSONObject(k).getString("totalFareWithOutMarkUp");
					userflightamount += "" + origin + "#" + destination + "#" + arrivalDate + "#" + departureDate + "#"
							+ duration + "#" + arrivalTime + "#" + departureTime + "#" + flightNumber + "#"
							+ airlineName + "#" + TotalFare + "#" + TotalTax + "#" + TotalFareWithOutMarkUp;
				}
				session.setAttribute("checkoutDeatil", userflightamount);
			} else {

			}

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e);
		}
		return "User/Travel/Flight/flight-checkout";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/Flight/RoundwayCheckOut")
	public String getFlightRoundwayCheckOut(HttpSession session) {
		String sessionId = (String) session.getAttribute("sessionId");
		try {

			if (sessionId != null && sessionId.length() != 0) {
				String Firstfligt = "" + session.getAttribute("Firstfligt");
				String Secondflight = "" + session.getAttribute("Secondflight");
				if (Firstfligt.equals("Firstfligt")) {
					String airRePriceRQ = "" + session.getAttribute("airRePriceRQoneway");
					String userflightamount = "";
					JSONObject obj = new JSONObject(airRePriceRQ);

					JSONArray journeys = obj.getJSONObject("details").getJSONArray("journeys");

					JSONArray segments = journeys.getJSONObject(0).getJSONArray("segments");

					JSONArray bonds = segments.getJSONObject(0).getJSONArray("bonds");
					JSONArray fare = segments.getJSONObject(0).getJSONArray("fare");

					for (int k = 0; k < bonds.length(); k++) {
						JSONArray paxFares = fare.getJSONObject(k).getJSONArray("paxFares");

						JSONArray legs = bonds.getJSONObject(k).getJSONArray("legs");
						String airlineName = "" + legs.getJSONObject(k).getString("airlineName");
						String arrivalTime = "" + legs.getJSONObject(k).getString("arrivalTime");
						String departureDate = "" + legs.getJSONObject(k).getString("departureDate");
						String departureTime = "" + legs.getJSONObject(k).getString("departureTime");
						String destination = "" + legs.getJSONObject(k).getString("destination");
						String duration = "" + legs.getJSONObject(k).getString("duration");
						String flightNumber = "" + legs.getJSONObject(k).getString("flightNumber");
						String origin = "" + legs.getJSONObject(k).getString("origin");
						String arrivalDate = "" + legs.getJSONObject(k).getString("arrivalDate");
						String TotalFare = "" + paxFares.getJSONObject(k).getString("totalFare");
						String TotalTax = "" + paxFares.getJSONObject(k).getString("totalTax");
						String TotalFareWithOutMarkUp = "" + fare.getJSONObject(k).getString("totalFareWithOutMarkUp");
						userflightamount += "" + origin + "#" + destination + "#" + arrivalDate + "#" + departureDate
								+ "#" + duration + "#" + arrivalTime + "#" + departureTime + "#" + flightNumber + "#"
								+ airlineName + "#" + TotalFare + "#" + TotalTax + "#" + TotalFareWithOutMarkUp;
					}
					session.setAttribute("checkoutDeatil", userflightamount);

					System.err.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%" + userflightamount);
				}
				if (Secondflight.equals("Secondflight")) {

					String airRePriceRQ = "" + session.getAttribute("airRePriceRQroundway");
					String userflightamount = "";
					JSONObject obj = new JSONObject(airRePriceRQ);

					JSONArray journeys = obj.getJSONObject("details").getJSONArray("journeys");

					JSONArray segments = journeys.getJSONObject(0).getJSONArray("segments");

					JSONArray bonds = segments.getJSONObject(0).getJSONArray("bonds");
					JSONArray fare = segments.getJSONObject(0).getJSONArray("fare");

					for (int k = 0; k < bonds.length(); k++) {
						JSONArray paxFares = fare.getJSONObject(k).getJSONArray("paxFares");

						JSONArray legs = bonds.getJSONObject(k).getJSONArray("legs");
						String airlineName = "" + legs.getJSONObject(k).getString("airlineName");
						String arrivalTime = "" + legs.getJSONObject(k).getString("arrivalTime");
						String departureDate = "" + legs.getJSONObject(k).getString("departureDate");
						String departureTime = "" + legs.getJSONObject(k).getString("departureTime");
						String destination = "" + legs.getJSONObject(k).getString("destination");
						String duration = "" + legs.getJSONObject(k).getString("duration");
						String flightNumber = "" + legs.getJSONObject(k).getString("flightNumber");
						String origin = "" + legs.getJSONObject(k).getString("origin");
						String arrivalDate = "" + legs.getJSONObject(k).getString("arrivalDate");
						String TotalFare = "" + paxFares.getJSONObject(k).getString("totalFare");
						String TotalTax = "" + paxFares.getJSONObject(k).getString("totalTax");
						String TotalFareWithOutMarkUp = "" + fare.getJSONObject(k).getString("totalFareWithOutMarkUp");
						userflightamount += "" + origin + "#" + destination + "#" + arrivalDate + "#" + departureDate
								+ "#" + duration + "#" + arrivalTime + "#" + departureTime + "#" + flightNumber + "#"
								+ airlineName + "#" + TotalFare + "#" + TotalTax + "#" + TotalFareWithOutMarkUp;
					}
					System.err.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%" + userflightamount);
					session.setAttribute("returncheckoutDeatil", userflightamount);
				}

			} else {

			}

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e);
		}
		return "User/Travel/Flight/flight-roundway-checkout";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/Flight/InternationalCheckOut")
	public String getFlightCheckOutInternationalCheckOut(HttpSession session) {
		String sessionId = (String) session.getAttribute("sessionId");
		try {
			String TotalFareWithOutMarkUp = "";
			if (sessionId != null && sessionId.length() != 0) {

				String airRePriceRQ = "" + session.getAttribute("airRePriceRQinternational");
				String userflightamount = "";
				JSONObject obj = new JSONObject(airRePriceRQ);

				JSONArray journeys = obj.getJSONObject("details").getJSONArray("journeys");

				JSONArray segments = journeys.getJSONObject(0).getJSONArray("segments");

				JSONArray bonds = segments.getJSONObject(0).getJSONArray("bonds");
				JSONArray fare = segments.getJSONObject(0).getJSONArray("fare");

				for (int i = 0; i < bonds.length(); i++) {

					JSONArray legs = bonds.getJSONObject(i).getJSONArray("legs");
					for (int k = 0; k < legs.length(); k++) {

						String airlineName = "" + legs.getJSONObject(k).getString("airlineName");
						String arrivalTime = "" + legs.getJSONObject(k).getString("arrivalTime");
						String departureDate = "" + legs.getJSONObject(k).getString("departureDate");
						String departureTime = "" + legs.getJSONObject(k).getString("departureTime");
						String destination = "" + legs.getJSONObject(k).getString("destination");
						String duration = "" + legs.getJSONObject(k).getString("duration");
						String flightNumber = "" + legs.getJSONObject(k).getString("flightNumber");
						String origin = "" + legs.getJSONObject(k).getString("origin");
						String arrivalDate = "" + legs.getJSONObject(k).getString("arrivalDate");

						userflightamount += "" + arrivalDate + "#" + departureDate + "#" + duration + "#"
								+ departureTime + "#" + arrivalTime + "#" + airlineName;
					}

				}
				for (int kk = 0; kk < fare.length(); kk++) {
					TotalFareWithOutMarkUp = "" + fare.getJSONObject(kk).getString("totalFareWithOutMarkUp");

				}

				session.setAttribute("checkoutDeatilintenational", userflightamount + "@" + TotalFareWithOutMarkUp);
			} else {

			}

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e);
		}
		return "User/Travel/Flight/flightinternationalcheckout";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/Flight/Source")
	public String getavailebelbustrevelbus(HttpSession session) {

		String sourcename = "";
		String sourcecode = "";
		String airportName="";
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				try {
					String source = travelflightapi.getAirlineNames();
					// String source = Test.readfile("flightsr.txt");
					JSONObject obj = new JSONObject(source);
					String code = obj.getString("code");
					if (code.equalsIgnoreCase("S00")) {

						JSONArray objsourecArray = obj.getJSONArray("details");
						for (int i = 0; i < objsourecArray.length(); i++) {
							String sourcename1 = objsourecArray.getJSONObject(i).getString("cityName");
							String sourcecode1 = objsourecArray.getJSONObject(i).getString("cityCode");
							String airportName1 = objsourecArray.getJSONObject(i).getString("airportName");
							
							sourcecode += "#" + sourcecode1;
							sourcename += "@" + sourcename1;
							airportName="&" + airportName1;
						}
						
						session.setAttribute("sourcecode", sourcecode + "!" + sourcename+"!"+airportName);
						session.setAttribute("sessionId", sessionId);
						return "User/Travel/Flight/HomeNew";
					} else {
						return "User/Home";
					}
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println(e);
					return "User/Home";
				}
			} else {
				return "redirect:/Home";
			}

		} else {
			return "redirect:/Home";
		}
	}

	// Get Method finish
	@RequestMapping(method = RequestMethod.POST, value = "/Flight/OneWay", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<String> getFlightOneWayPost(HttpSession session, @RequestBody TravelFlightRequest req) {

		String engineID = "";
		String airlineName = "";
		try {

			String tripype = "";
			String date = req.getBeginDate();
			SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy");
			java.util.Date dateq = sdf1.parse(date);
			java.sql.Date sqlStartDate = new java.sql.Date(dateq.getTime());
			req.setBeginDate("" + sqlStartDate);
			if (req.getTripType().equals("RoundTrip")) {
				String enddate = req.getEndDate();

				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				java.util.Date dateend = sdf.parse(enddate);
				java.sql.Date sqlStartDat = new java.sql.Date(dateend.getTime());
				req.setEndDate("" + sqlStartDat);
			}
			String sessionId = (String) session.getAttribute("sessionId");
			if (sessionId != null && sessionId.length() != 0) {
				String source = travelflightapi.getFlightSearchAPI(req);
				session.setAttribute("flightResponse", source);
				JSONObject obj = new JSONObject(source);
				String code = obj.getString("code");
				if (code.equalsIgnoreCase("S00")) {
					session.setAttribute("adults", req.getAdults());
					session.setAttribute("childs", req.getChilds());
					session.setAttribute("infants", req.getInfants());
					session.setAttribute("beginDate", req.getBeginDate());
					session.setAttribute("endDate", req.getEndDate());
					session.setAttribute("source", req.getOrigin());
					session.setAttribute("destination", req.getDestination());
					session.setAttribute("triptype", req.getTripType());
					HashSet<FlightResponse> flightNumber = new HashSet<FlightResponse>();
					HashSet<FlightResponse> flightNumbersotpage = new HashSet<FlightResponse>();
					HashSet<String> flightNumberarr = new HashSet<String>();

					ArrayList<FlightResponse> flightresponsearr = new ArrayList<FlightResponse>();

					ArrayList<FlightResponse> flightresponsearrreturn = new ArrayList<FlightResponse>();
					JSONArray journeys = obj.getJSONObject("details").getJSONArray("journeys");
					for (int x = 0; x < journeys.length(); x++) {
						if (x == 0) {
							JSONArray segments = journeys.getJSONObject(x).getJSONArray("segments");

							for (int j = 0; j < segments.length(); j++) {
								JSONArray bonds = segments.getJSONObject(j).getJSONArray("bonds");
								if (bonds.length() == 1) {
									FlightResponse flightresponseobj = new FlightResponse();

									tripype = "Oneway";
									flightresponseobj
											.setItineraryKey("" + segments.getJSONObject(j).getString("itineraryKey"));
									flightresponseobj.setSearchId("" + segments.getJSONObject(j).getString("searchId"));
									flightresponseobj.setEngineID("" + segments.getJSONObject(j).getString("engineID"));

									JSONArray fare = segments.getJSONObject(j).getJSONArray("fare");
									for (int l = 0; l < fare.length(); l++) {
										flightresponseobj.setAmount(
												"" + fare.getJSONObject(l).getString("totalFareWithOutMarkUp"));
									}
									// System.out.println("Bond Domestic");
									for (int k = 0; k < bonds.length(); k++) {

										JSONArray legs = bonds.getJSONObject(k).getJSONArray("legs");
										if (legs.length() == 1) {
											flightresponseobj.setStopage("Non-Stoppage");

										}
										if (legs.length() == 2) {
											flightresponseobj.setStopage("1-Stoppage");
										}

										if (legs.length() == 3) {
											flightresponseobj.setStopage("2-Stoppage");
										}
										boolean isConnecting = legs.getJSONObject(k).getBoolean("isConnecting");

										if (isConnecting) {
											System.err.println(isConnecting);
										} else {
											flightresponseobj.setAircraftType(
													"" + legs.getJSONObject(k).getString("aircraftType"));
											flightresponseobj.setAirlineName(
													"" + legs.getJSONObject(k).getString("airlineName"));
											flightresponseobj.setArrivalTime(
													"" + legs.getJSONObject(k).getString("arrivalTime"));
											flightresponseobj.setCabin("" + legs.getJSONObject(k).getString("cabin"));
											flightresponseobj.setDepartureTime(
													"" + legs.getJSONObject(k).getString("departureTime"));
											flightresponseobj.setDestination(
													"" + legs.getJSONObject(k).getString("destination"));
											flightresponseobj
													.setDuration("" + legs.getJSONObject(k).getString("duration"));
											flightresponseobj.setFlightNumber(
													"" + legs.getJSONObject(k).getString("flightNumber"));

											flightresponseobj.setOrigin("" + legs.getJSONObject(k).getString("origin"));
											flightresponseobj.setBaggageUnit(
													"" + legs.getJSONObject(k).getString("baggageUnit"));
											flightresponseobj.setBaggageWeight(
													"" + legs.getJSONObject(k).getString("baggageWeight"));
											flightresponseobj.setNumber("" + j);
											flightresponsearr.add(flightresponseobj);

											if (flightNumberarr
													.contains("" + segments.getJSONObject(j).getString("engineID"))
													|| flightNumberarr.contains(
															"" + legs.getJSONObject(k).getString("airlineName"))) {

											} else {
												flightNumberarr
														.add("" + segments.getJSONObject(j).getString("engineID"));
												flightNumberarr
														.add("" + legs.getJSONObject(k).getString("airlineName"));

												flightNumber.add(flightresponseobj);
											}
											if (flightNumberarr.contains("" + flightresponseobj.getStopage())) {

											} else {
												flightNumberarr.add("" + flightresponseobj.getStopage());

												flightNumbersotpage.add(flightresponseobj);
											}

										}

									}
								} else {

									JSONArray bondsroundwayinternational = segments.getJSONObject(j)
											.getJSONArray("bonds");
									for (int l = 0; l < bondsroundwayinternational.length(); l++) {

										JSONArray legsroundwayinternational = bondsroundwayinternational
												.getJSONObject(l).getJSONArray("legs");
										for (int m = 0; m < legsroundwayinternational.length(); m++) {

											if (l == 0) {
												tripype = "internationalOneway";
												if (m == 0) {
													FlightResponse flightresponseobjinternational = new FlightResponse();
													flightresponseobjinternational
															.setJson("" + bondsroundwayinternational);
													flightresponseobjinternational.setEngineID(
															"" + segments.getJSONObject(j).getString("engineID"));
													flightresponseobjinternational
															.setAircraftType("" + legsroundwayinternational
																	.getJSONObject(m).getString("aircraftType"));
													flightresponseobjinternational
															.setAirlineName("" + legsroundwayinternational
																	.getJSONObject(m).getString("airlineName"));
													flightresponseobjinternational
															.setArrivalTime("" + legsroundwayinternational
																	.getJSONObject(m).getString("arrivalTime"));
													flightresponseobjinternational
															.setCabin("" + legsroundwayinternational.getJSONObject(m)
																	.getString("cabin"));
													flightresponseobjinternational
															.setDepartureTime("" + legsroundwayinternational
																	.getJSONObject(m).getString("departureTime"));
													flightresponseobjinternational
															.setDestination("" + legsroundwayinternational
																	.getJSONObject(m).getString("destination"));
													flightresponseobjinternational
															.setDuration("" + legsroundwayinternational.getJSONObject(1)
																	.getString("duration"));
													flightresponseobjinternational.setFlightNumber(""
															+ legsroundwayinternational.getJSONObject(m)
																	.getString("flightNumber")
															+ "#" + legsroundwayinternational.getJSONObject(m + 1)
																	.getString("flightNumber"));

													flightresponseobjinternational
															.setOrigin("" + legsroundwayinternational.getJSONObject(m)
																	.getString("origin"));
													flightresponseobjinternational
															.setBaggageUnit("" + legsroundwayinternational
																	.getJSONObject(m).getString("baggageUnit"));
													flightresponseobjinternational
															.setBaggageWeight("" + legsroundwayinternational
																	.getJSONObject(m).getString("baggageWeight"));
													flightresponseobjinternational.setNumber("" + j);
													if (legsroundwayinternational.length() == 1) {
														flightresponseobjinternational.setStopage("Non-Stoppage");
													}
													if (legsroundwayinternational.length() == 2) {
														flightresponseobjinternational.setStopage("1-Stoppage");
													}

													if (legsroundwayinternational.length() == 3) {
														flightresponseobjinternational.setStopage("2-Stoppage");
													}

													JSONArray fare = segments.getJSONObject(j).getJSONArray("fare");
													for (int kk = 0; kk < fare.length(); kk++) {
														flightresponseobjinternational.setAmount("" + fare
																.getJSONObject(kk).getString("totalFareWithOutMarkUp"));
													}
													flightresponseobjinternational.setJsonpaxfare("" + fare);
													flightresponsearr.add(flightresponseobjinternational);
												}

											} else {
												if (m == 0) {
													FlightResponse flightresponseobjinternational = new FlightResponse();
													flightresponseobjinternational.setEngineIDreturn(
															"" + segments.getJSONObject(j).getString("engineID"));
													flightresponseobjinternational
															.setAircraftTypereturn("" + legsroundwayinternational
																	.getJSONObject(m).getString("aircraftType"));
													flightresponseobjinternational
															.setAirlineNamereturn("" + legsroundwayinternational
																	.getJSONObject(m).getString("airlineName"));
													flightresponseobjinternational
															.setArrivalTimereturn("" + legsroundwayinternational
																	.getJSONObject(m).getString("arrivalTime"));
													flightresponseobjinternational
															.setCabinreturn("" + legsroundwayinternational
																	.getJSONObject(m).getString("cabin"));
													flightresponseobjinternational
															.setDepartureTimereturn("" + legsroundwayinternational
																	.getJSONObject(m).getString("departureTime"));
													flightresponseobjinternational
															.setDestinationreturn("" + legsroundwayinternational
																	.getJSONObject(m).getString("destination"));
													flightresponseobjinternational
															.setDurationreturn("" + legsroundwayinternational
																	.getJSONObject(m).getString("duration"));
													flightresponseobjinternational.setFlightNumberreturn(""
															+ legsroundwayinternational.getJSONObject(m)
																	.getString("flightNumber")
															+ "#" + legsroundwayinternational.getJSONObject(m + 1)
																	.getString("flightNumber"));

													flightresponseobjinternational
															.setOriginreturn("" + legsroundwayinternational
																	.getJSONObject(m).getString("origin"));
													flightresponseobjinternational
															.setBaggageUnitreturn("" + legsroundwayinternational
																	.getJSONObject(m).getString("baggageUnit"));
													flightresponseobjinternational
															.setBaggageWeightreturn("" + legsroundwayinternational
																	.getJSONObject(m).getString("baggageWeight"));
													flightresponseobjinternational.setNumberreturn("" + j);

													if (legsroundwayinternational.length() == 1) {
														flightresponseobjinternational.setStopagereturn("Non-Stoppage");
													}
													if (legsroundwayinternational.length() == 2) {
														flightresponseobjinternational.setStopagereturn("1-Stoppage");
													}

													if (legsroundwayinternational.length() == 3) {
														flightresponseobjinternational.setStopagereturn("2-Stoppage");
													}
													JSONArray fare = segments.getJSONObject(j).getJSONArray("fare");
													for (int kk = 0; kk < fare.length(); kk++) {
														flightresponseobjinternational.setAmount("" + fare
																.getJSONObject(kk).getString("totalFareWithOutMarkUp"));
													}
													flightresponsearrreturn.add(flightresponseobjinternational);
												}
											}

										}
									}

								}

							}

						} else {
							JSONArray segments = journeys.getJSONObject(x).getJSONArray("segments");

							for (int j = 0; j < segments.length(); j++) {
								tripype = "DomesticRoundway";
								FlightResponse flightresponseobj = new FlightResponse();
								flightresponseobj.setItineraryKeyreturn(
										"" + segments.getJSONObject(j).getString("itineraryKey"));
								flightresponseobj
										.setSearchIdreturn("" + segments.getJSONObject(j).getString("searchId"));
								flightresponseobj
										.setEngineIDreturn("" + segments.getJSONObject(j).getString("engineID"));
								JSONArray bonds = segments.getJSONObject(j).getJSONArray("bonds");
								JSONArray fare = segments.getJSONObject(j).getJSONArray("fare");
								for (int l = 0; l < fare.length(); l++) {
									flightresponseobj.setAmountreturn(
											"" + fare.getJSONObject(l).getString("totalFareWithOutMarkUp"));
								}
								for (int k = 0; k < bonds.length(); k++) {
									JSONArray legs = bonds.getJSONObject(k).getJSONArray("legs");
									if (legs.length() == 1) {
										flightresponseobj.setStopagereturn("Non-Stoppage");
									}
									if (legs.length() == 2) {
										flightresponseobj.setStopagereturn("1-Stoppage");
									}

									if (legs.length() == 3) {
										flightresponseobj.setStopagereturn("2-Stoppage");
									}
									boolean isConnecting = legs.getJSONObject(k).getBoolean("isConnecting");
									if (isConnecting) {
										System.err.println(isConnecting);
									} else {
										flightresponseobj.setAircraftTypereturn(
												"" + legs.getJSONObject(k).getString("aircraftType"));
										flightresponseobj.setAirlineNamereturn(
												"" + legs.getJSONObject(k).getString("airlineName"));
										flightresponseobj.setArrivalTimereturn(
												"" + legs.getJSONObject(k).getString("arrivalTime"));
										flightresponseobj.setCabinreturn("" + legs.getJSONObject(k).getString("cabin"));
										flightresponseobj.setDepartureTimereturn(
												"" + legs.getJSONObject(k).getString("departureTime"));
										flightresponseobj.setDestinationreturn(
												"" + legs.getJSONObject(k).getString("destination"));
										flightresponseobj
												.setDurationreturn("" + legs.getJSONObject(k).getString("duration"));
										flightresponseobj.setFlightNumberreturn(
												"" + legs.getJSONObject(k).getString("flightNumber"));
										flightresponseobj
												.setOriginreturn("" + legs.getJSONObject(k).getString("origin"));
										flightresponseobj.setBaggageUnitreturn(
												"" + legs.getJSONObject(k).getString("baggageUnit"));
										flightresponseobj.setBaggageWeightreturn(
												"" + legs.getJSONObject(k).getString("baggageWeight"));
										flightresponseobj.setNumberreturn("" + j);
										flightresponsearrreturn.add(flightresponseobj);
									}
								}

							}
						}

					}
					session.setAttribute("flightNumbersotpage", flightNumbersotpage);
					session.setAttribute("flightnumberdata", flightNumber);
					session.setAttribute("flightdata", flightresponsearr);
					session.setAttribute("flightdatareturn", flightresponsearrreturn);
					session.setAttribute("origin", req.getOrigin());
					session.setAttribute("destination", req.getDestination());
					//

					return new ResponseEntity<String>("S00" + "#" + tripype, HttpStatus.OK);

				} else {
					return new ResponseEntity<String>("F04" + "#" + obj.getString("message"), HttpStatus.OK);
				}

			} else {
				return new ResponseEntity<String>("F04", HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e);
			return new ResponseEntity<String>("F04", HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/Flight/ShowPriceDetail", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<String> getShowPriceDetail(HttpSession session, @RequestBody FlightPriceCheckRequest req) {
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null && sessionId.length() != 0) {
			try {

				String source = "" + session.getAttribute("flightResponse");
				JSONObject obj = new JSONObject(source);
				int numberofarray = 0;
				JSONArray journeys = obj.getJSONObject("details").getJSONArray("journeys");

				JSONArray segments = journeys.getJSONObject(0).getJSONArray("segments");
				for (int j = 0; j < segments.length(); j++) {

					JSONArray bonds = segments.getJSONObject(j).getJSONArray("bonds");
					for (int k = 0; k < bonds.length(); k++) {

						JSONArray legs = bonds.getJSONObject(k).getJSONArray("legs");
						String flightnumber = legs.getJSONObject(k).getString("flightNumber");
						if (flightnumber.equals(req.getFlightNumber())) {

							numberofarray = k;
							req.setEngineID("" + segments.getJSONObject(j).getString("engineID"));
							req.setAirlineName("" + legs.getJSONObject(k).getString("airlineName"));
							req.setArrivalTime("" + legs.getJSONObject(k).getString("arrivalTime"));
							req.setCabin("" + legs.getJSONObject(k).getString("cabin"));
							req.setDepartureTime("" + legs.getJSONObject(k).getString("departureTime"));
							req.setDestination("" + legs.getJSONObject(k).getString("destination"));
							req.setDuration("" + legs.getJSONObject(k).getString("duration"));
							req.setBoundType("" + legs.getJSONObject(k).getString("boundType"));
							req.setFlightNumber("" + legs.getJSONObject(k).getString("flightNumber"));
							req.setOrigin("" + legs.getJSONObject(k).getString("origin"));
							req.setAdults("" + session.getAttribute("adults"));
							req.setChilds("" + session.getAttribute("childs"));
							req.setInfants("" + session.getAttribute("infants"));
							req.setBeginDate("" + session.getAttribute("beginDate"));
							req.setFlightName("" + legs.getJSONObject(k).getString("airlineName"));
							req.setTripType("" + session.getAttribute("triptype"));
							req.setArrivalDate("" + legs.getJSONObject(k).getString("arrivalDate"));
							req.setDepartureDate("" + legs.getJSONObject(k).getString("departureDate"));
							req.setDepartureTerminal("" + legs.getJSONObject(k).getString("departureTerminal"));
							req.setArrivalTerminal("" + legs.getJSONObject(k).getString("arrivalTerminal"));
							req.setCapacity("" + legs.getJSONObject(k).getString("capacity"));
							req.setCarrierCode("" + legs.getJSONObject(k).getString("carrierCode"));
							req.setCurrencyCode("" + legs.getJSONObject(k).getString("currencyCode"));
							req.setBaggageUnit("" + legs.getJSONObject(k).getString("baggageUnit"));
							req.setBaggageWeight("" + legs.getJSONObject(k).getString("baggageWeight"));
							req.setFareClassOfService("" + legs.getJSONObject(k).getString("fareClassOfService"));
							req.setSold("" + legs.getJSONObject(k).getString("sold"));
							req.setStatus("" + legs.getJSONObject(k).getString("status"));
							req.setItineraryKey("" + segments.getJSONObject(j).getString("itineraryKey"));
							req.setBondType("" + segments.getJSONObject(j).getString("bondType"));
							req.setSearchId("" + segments.getJSONObject(j).getString("searchId"));
							req.setEngineID("" + segments.getJSONObject(j).getString("engineID"));
							req.setFareRule("" + segments.getJSONObject(j).getString("fareRule"));
							req.setJourneyTime("" + legs.getJSONObject(k).getString("duration"));
							req.setJourneyIndex("" + segments.getJSONObject(j).getString("journeyIndex"));

							JSONArray fare = segments.getJSONObject(j).getJSONArray("fare");
							for (int l = 0; l < fare.length(); l++) {
								JSONArray paxFares = fare.getJSONObject(l).getJSONArray("paxFares");
								JSONArray fares = paxFares.getJSONObject(l).getJSONArray("fares");

								req.setTotalFare("" + paxFares.getJSONObject(l).getString("totalFare"));
								req.setTotalTax("" + paxFares.getJSONObject(l).getString("totalTax"));
								req.setTotalFareWithOutMarkUp(
										"" + fare.getJSONObject(l).getString("totalFareWithOutMarkUp"));
								req.setTotalTaxWithOutMarkUp(
										"" + fare.getJSONObject(l).getString("totalTaxWithOutMarkUp"));
								req.setAmount("" + fares.getJSONObject(l).getString("amount"));

								req.setChargeCode("" + fares.getJSONObject(l).getString("chargeCode"));
								req.setChargeType("" + fares.getJSONObject(l).getString("chargeType"));

								req.setBasicFare("" + fare.getJSONObject(l).getString("basicFare"));
								req.setCancelPenalty("" + paxFares.getJSONObject(l).getString("cancelPenalty"));
								req.setChangePenalty("" + paxFares.getJSONObject(l).getString("changePenalty"));
								req.setTransactionAmount("" + paxFares.getJSONObject(l).getString("transactionAmount"));
								req.setBaseTransactionAmount(
										"" + paxFares.getJSONObject(l).getString("baseTransactionAmount"));
								req.setTraceId("AYTM00011111111110002");

								req.setExchangeRate("" + fare.getJSONObject(l).getString("exchangeRate"));

								req.setMarkUP("" + paxFares.getJSONObject(l).getString("markUP"));
								req.setPaxType("" + paxFares.getJSONObject(l).getString("paxType"));

							}

						}
					}
				}
				String airRePriceRQ = travelflightapi.AirRePriceRQ(req);

				String userflightamount = "";
				JSONObject airRePriceRQobj = new JSONObject(airRePriceRQ);
				String codeairRePriceRQobj = airRePriceRQobj.getString("code");
				if (codeairRePriceRQobj.equalsIgnoreCase("S00")) {
					session.setAttribute("airRePriceRQ", airRePriceRQ);

					JSONArray journeys11 = airRePriceRQobj.getJSONObject("details").getJSONArray("journeys");
					JSONArray segments11 = journeys11.getJSONObject(0).getJSONArray("segments");

					JSONArray bonds = segments11.getJSONObject(0).getJSONArray("bonds");

					JSONObject obj22 = CreateJsonRequestFlight.createjsonForTicket(bonds);
					System.err.println(obj22);
					session.setAttribute("ticketDetails", obj22);

					JSONArray airRePriceRQjourneys = airRePriceRQobj.getJSONObject("details").getJSONArray("journeys");

					JSONArray airRePriceRQsegments = airRePriceRQjourneys.getJSONObject(0).getJSONArray("segments");
					for (int j = 0; j < airRePriceRQsegments.length(); j++) {
						JSONArray fare = airRePriceRQsegments.getJSONObject(j).getJSONArray("fare");
						for (int l = 0; l < fare.length(); l++) {
							JSONArray paxFares = fare.getJSONObject(l).getJSONArray("paxFares");
							JSONArray fares = paxFares.getJSONObject(l).getJSONArray("fares");

							String TotalFare = "" + paxFares.getJSONObject(l).getString("totalFare");
							String TotalTax = "" + paxFares.getJSONObject(l).getString("totalTax");
							String TotalFareWithOutMarkUp = ""
									+ fare.getJSONObject(l).getString("totalFareWithOutMarkUp");

							String CancelPenalty = "" + paxFares.getJSONObject(l).getString("cancelPenalty");
							String ChangePenalty = "" + paxFares.getJSONObject(l).getString("changePenalty");
							userflightamount += "#" + TotalFare + "#" + TotalTax + "#" + TotalFareWithOutMarkUp + "#"
									+ CancelPenalty + "#" + ChangePenalty;
						}

					}
					return new ResponseEntity<String>("S00" + "@" + userflightamount, HttpStatus.OK);
				} else {
					return new ResponseEntity<String>("F04" + "@" + "Detail", HttpStatus.OK);
				}

			} catch (Exception e) {
				e.printStackTrace();
				System.out.println(e);
				return new ResponseEntity<String>("F04" + "@" + "Detail", HttpStatus.OK);
			}
		} else {
			return new ResponseEntity<String>("F04" + "@" + "Detail", HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/Flight/ConnectingShowPriceDetail", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<String> getShowPriceDetailConnecting(HttpSession session,
			@RequestBody FlightPriceCheckRequest req) {
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null && sessionId.length() != 0) {
			try {

				String source = "" + session.getAttribute("flightResponse");

				String airRePriceRQ = travelflightapi.AirRePriceRQConnecting(req, source,
						"" + session.getAttribute("destination"), session);

				String userflightamount = "";
				JSONObject airRePriceRQobj = new JSONObject(airRePriceRQ);
				String codeairRePriceRQobj = airRePriceRQobj.getString("code");
				if (codeairRePriceRQobj.equalsIgnoreCase("S00")) {
					session.setAttribute("airRePriceRQ", airRePriceRQ);
					JSONArray journeys11 = airRePriceRQobj.getJSONObject("details").getJSONArray("journeys");
					JSONArray segments11 = journeys11.getJSONObject(0).getJSONArray("segments");
					JSONArray bonds = segments11.getJSONObject(0).getJSONArray("bonds");
					JSONObject obj22 = CreateJsonRequestFlight.createjsonForTicket(bonds);
					System.err.println(obj22);
					session.setAttribute("ticketDetails", obj22);
					JSONArray airRePriceRQjourneys = airRePriceRQobj.getJSONObject("details").getJSONArray("journeys");

					JSONArray airRePriceRQsegments = airRePriceRQjourneys.getJSONObject(0).getJSONArray("segments");
					for (int j = 0; j < airRePriceRQsegments.length(); j++) {
						JSONArray fare = airRePriceRQsegments.getJSONObject(j).getJSONArray("fare");
						for (int l = 0; l < fare.length(); l++) {
							JSONArray paxFares = fare.getJSONObject(l).getJSONArray("paxFares");
							JSONArray fares = paxFares.getJSONObject(l).getJSONArray("fares");

							String TotalFare = "" + paxFares.getJSONObject(l).getString("totalFare");
							String TotalTax = "" + paxFares.getJSONObject(l).getString("totalTax");
							String TotalFareWithOutMarkUp = ""
									+ fare.getJSONObject(l).getString("totalFareWithOutMarkUp");

							String CancelPenalty = "" + paxFares.getJSONObject(l).getString("cancelPenalty");
							String ChangePenalty = "" + paxFares.getJSONObject(l).getString("changePenalty");
							userflightamount += "#" + TotalFare + "#" + TotalTax + "#" + TotalFareWithOutMarkUp + "#"
									+ CancelPenalty + "#" + ChangePenalty;
						}

					}
					return new ResponseEntity<String>("S00" + "@" + userflightamount, HttpStatus.OK);
				} else {
					return new ResponseEntity<String>("F04" + "@" + "Detail", HttpStatus.OK);
				}

			} catch (Exception e) {
				e.printStackTrace();
				System.out.println(e);
				return new ResponseEntity<String>("F04" + "@" + "Detail", HttpStatus.OK);
			}
		} else {
			return new ResponseEntity<String>("F04" + "@" + "Detail", HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/Flight/internationalConnectingShowPriceDetail", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<String> getShowPriceDetailConnectinginternational(HttpSession session,
			@RequestBody FlightPriceCheckRequest req) throws Exception {
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null && sessionId.length() != 0) {
			try {

				org.json.JSONArray obj = new org.json.JSONArray(req.getJsonbonds());

				String df = "" + req.getJsonpaxfare();

				String val = df.replaceAll("fares", "bookFares");
				JSONArray obj1 = new JSONArray(val);
				JSONObject obj22 = CreateJsonRequestFlight.createbondslegsarray(obj, obj1);
				JSONObject objmain = new JSONObject();

				org.json.JSONObject objmain1 = new org.json.JSONObject();

				org.json.JSONObject objmain2 = new org.json.JSONObject();
				org.json.simple.JSONArray objkbond = new org.json.simple.JSONArray();
				objmain.put("fares", obj1);

				ArrayList<String> objhasmap = new ArrayList<>();

				org.json.simple.JSONArray objk = new org.json.simple.JSONArray();

				obj22.put("engineID", req.getEngineID());

				objk.add(obj22);
				objhasmap.add("" + req.getEngineID());
				objmain1.put("clientIp", Inet4Address.getLocalHost().getHostAddress());
				objmain1.put("cllientKey", UrlMetadatas.MDEX_CLIENTKEY);
				objmain1.put("clientToken", UrlMetadatas.MDEX_CLIENTTOKEN);
				objmain1.put("clientApiName", "Flight Search");

				JSONObject flightSearchDetailsjsonobject = new JSONObject();
				JSONObject flightSearchDetailsjsonobject1 = new JSONObject();
				JSONObject payload1 = new JSONObject();
				JSONArray flightSearchDetailsaray = new JSONArray();
				flightSearchDetailsjsonobject.put("beginDate", "" + session.getAttribute("beginDate"));
				flightSearchDetailsjsonobject.put("destination", "" + session.getAttribute("destination"));
				flightSearchDetailsjsonobject.put("origin", "" + session.getAttribute("source"));
				flightSearchDetailsaray.put(flightSearchDetailsjsonobject);
				flightSearchDetailsjsonobject1.put("beginDate", "" + session.getAttribute("endDate"));
				flightSearchDetailsjsonobject1.put("destination", "" + session.getAttribute("source"));
				flightSearchDetailsjsonobject1.put("origin", "" + session.getAttribute("destination"));

				flightSearchDetailsaray.put(flightSearchDetailsjsonobject1);
				payload1.put("engineIDs", objhasmap);
				payload1.put("flightSearchDetails", flightSearchDetailsaray);

				payload1.put("traceId", "AYTM00011111111110001");
				payload1.put("adults", "" + session.getAttribute("adults"));
				payload1.put("cabin", "Economy");
				payload1.put("childs", "" + session.getAttribute("childs"));
				payload1.put("infants", "" + session.getAttribute("infants"));
				payload1.put("tripType", "" + session.getAttribute("triptype"));

				objmain1.put("flightAvailability", payload1);
				objmain1.put("segments", objk);

				String airRePriceRQ = travelflightapi.AirRePriceRQConnectinginternational(objmain1);

				String userflightamount = "";
				JSONObject airRePriceRQobj = new JSONObject(airRePriceRQ);
				String codeairRePriceRQobj = airRePriceRQobj.getString("code");
				if (codeairRePriceRQobj.equalsIgnoreCase("S00")) {
					session.setAttribute("airRePriceRQinternational", airRePriceRQ);

					JSONArray journeys11 = airRePriceRQobj.getJSONObject("details").getJSONArray("journeys");
					JSONArray segments11 = journeys11.getJSONObject(0).getJSONArray("segments");
					JSONArray bonds = segments11.getJSONObject(0).getJSONArray("bonds");
					JSONObject obj221 = CreateJsonRequestFlight.createjsonForTicket(bonds);
					System.err.println(obj221);
					session.setAttribute("ticketDetails", obj221);
					JSONArray airRePriceRQjourneys = airRePriceRQobj.getJSONObject("details").getJSONArray("journeys");

					JSONArray airRePriceRQsegments = airRePriceRQjourneys.getJSONObject(0).getJSONArray("segments");
					for (int j = 0; j < airRePriceRQsegments.length(); j++) {
						JSONArray fare = airRePriceRQsegments.getJSONObject(j).getJSONArray("fare");
						for (int l = 0; l < fare.length(); l++) {
							JSONArray paxFares = fare.getJSONObject(l).getJSONArray("paxFares");
							JSONArray fares = paxFares.getJSONObject(l).getJSONArray("fares");

							String TotalFare = "" + paxFares.getJSONObject(l).getString("totalFare");
							String TotalTax = "" + paxFares.getJSONObject(l).getString("totalTax");
							String TotalFareWithOutMarkUp = ""
									+ fare.getJSONObject(l).getString("totalFareWithOutMarkUp");

							String CancelPenalty = "" + paxFares.getJSONObject(l).getString("cancelPenalty");
							String ChangePenalty = "" + paxFares.getJSONObject(l).getString("changePenalty");
							userflightamount += "#" + TotalFare + "#" + TotalTax + "#" + TotalFareWithOutMarkUp + "#"
									+ CancelPenalty + "#" + ChangePenalty;
						}

					}
					return new ResponseEntity<String>("S00" + "@" + userflightamount, HttpStatus.OK);
				} else {
					return new ResponseEntity<String>("F04" + "@" + "Detail", HttpStatus.OK);
				}

			} catch (Exception e) {
				e.printStackTrace();
				System.out.println(e);
				return new ResponseEntity<String>("F04" + "@" + "Detail", HttpStatus.OK);
			}
		} else {
			return new ResponseEntity<String>("F04" + "@" + "Detail", HttpStatus.OK);
		}

	}

	@RequestMapping(method = RequestMethod.POST, value = "/Flight/ShowPriceDetailRoundTrip", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<String> getShowPriceDetailRoundTrip(HttpSession session,
			@RequestBody FlightPriceCheckRequest req) {
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null && sessionId.length() != 0) {
			try {

				if (req.getOnewayflight().equals("Firstfligt")) {
					String source = "" + session.getAttribute("flightResponse");
					JSONObject obj = new JSONObject(source);
					int numberofarray = 0;
					ArrayList<FlightResponse> flightresponsearr = new ArrayList<FlightResponse>();
					JSONArray journeys = obj.getJSONObject("details").getJSONArray("journeys");
					for (int x = 0; x < journeys.length(); x++) {
						JSONArray segments = journeys.getJSONObject(0).getJSONArray("segments");
						for (int j = 0; j < segments.length(); j++) {

							JSONArray bonds = segments.getJSONObject(j).getJSONArray("bonds");
							for (int k = 0; k < bonds.length(); k++) {

								JSONArray legs = bonds.getJSONObject(k).getJSONArray("legs");
								String flightnumber = legs.getJSONObject(k).getString("flightNumber");
								if (flightnumber.equals(req.getFlightNumber())) {

									numberofarray = k;
									req.setEngineID("" + segments.getJSONObject(j).getString("engineID"));
									req.setAirlineName("" + legs.getJSONObject(k).getString("airlineName"));
									req.setArrivalTime("" + legs.getJSONObject(k).getString("arrivalTime"));
									req.setCabin("" + legs.getJSONObject(k).getString("cabin"));
									req.setDepartureTime("" + legs.getJSONObject(k).getString("departureTime"));
									req.setDestination("" + legs.getJSONObject(k).getString("destination"));
									req.setDuration("" + legs.getJSONObject(k).getString("duration"));
									req.setBoundType("" + legs.getJSONObject(k).getString("boundType"));
									req.setFlightNumber("" + legs.getJSONObject(k).getString("flightNumber"));
									req.setOrigin("" + legs.getJSONObject(k).getString("origin"));
									req.setAdults("" + session.getAttribute("adults"));
									req.setChilds("" + session.getAttribute("childs"));
									req.setInfants("" + session.getAttribute("infants"));
									req.setBeginDate("" + session.getAttribute("beginDate"));
									req.setFlightName("" + legs.getJSONObject(k).getString("airlineName"));
									req.setTripType("" + session.getAttribute("triptype"));
									req.setArrivalDate("" + legs.getJSONObject(k).getString("arrivalDate"));
									req.setDepartureDate("" + legs.getJSONObject(k).getString("departureDate"));
									req.setDepartureTerminal("" + legs.getJSONObject(k).getString("departureTerminal"));
									req.setArrivalTerminal("" + legs.getJSONObject(k).getString("arrivalTerminal"));
									req.setCapacity("" + legs.getJSONObject(k).getString("capacity"));
									req.setCarrierCode("" + legs.getJSONObject(k).getString("carrierCode"));
									req.setCurrencyCode("" + legs.getJSONObject(k).getString("currencyCode"));
									req.setBaggageUnit("" + legs.getJSONObject(k).getString("baggageUnit"));
									req.setBaggageWeight("" + legs.getJSONObject(k).getString("baggageWeight"));
									req.setFareClassOfService(
											"" + legs.getJSONObject(k).getString("fareClassOfService"));
									req.setSold("" + legs.getJSONObject(k).getString("sold"));
									req.setStatus("" + legs.getJSONObject(k).getString("status"));
									req.setItineraryKey("" + segments.getJSONObject(j).getString("itineraryKey"));
									req.setBondType("" + segments.getJSONObject(j).getString("bondType"));
									req.setSearchId("" + segments.getJSONObject(j).getString("searchId"));
									req.setEngineID("" + segments.getJSONObject(j).getString("engineID"));
									req.setFareRule("" + segments.getJSONObject(j).getString("fareRule"));
									req.setJourneyTime("" + legs.getJSONObject(k).getString("duration"));
									req.setJourneyIndex("" + segments.getJSONObject(j).getString("journeyIndex"));

									JSONArray fare = segments.getJSONObject(j).getJSONArray("fare");
									for (int l = 0; l < fare.length(); l++) {
										JSONArray paxFares = fare.getJSONObject(l).getJSONArray("paxFares");
										JSONArray fares = paxFares.getJSONObject(l).getJSONArray("fares");

										req.setTotalFare("" + paxFares.getJSONObject(l).getString("totalFare"));
										req.setTotalTax("" + paxFares.getJSONObject(l).getString("totalTax"));
										req.setTotalFareWithOutMarkUp(
												"" + fare.getJSONObject(l).getString("totalFareWithOutMarkUp"));
										req.setTotalTaxWithOutMarkUp(
												"" + fare.getJSONObject(l).getString("totalTaxWithOutMarkUp"));
										req.setAmount("" + fares.getJSONObject(l).getString("amount"));

										req.setChargeCode("" + fares.getJSONObject(l).getString("chargeCode"));
										req.setChargeType("" + fares.getJSONObject(l).getString("chargeType"));

										req.setBasicFare("" + fare.getJSONObject(l).getString("basicFare"));
										req.setCancelPenalty("" + paxFares.getJSONObject(l).getString("cancelPenalty"));
										req.setChangePenalty("" + paxFares.getJSONObject(l).getString("changePenalty"));
										req.setTransactionAmount(
												"" + paxFares.getJSONObject(l).getString("transactionAmount"));
										req.setBaseTransactionAmount(
												"" + paxFares.getJSONObject(l).getString("baseTransactionAmount"));
										req.setTraceId("AYTM00011111111110002");

										req.setExchangeRate("" + fare.getJSONObject(l).getString("exchangeRate"));

										req.setMarkUP("" + paxFares.getJSONObject(l).getString("markUP"));
										req.setPaxType("" + paxFares.getJSONObject(l).getString("paxType"));

									}

								}
							}
						}
					}
				}
				if (req.getOnewayflight().equals("Secondflight")) {
					String source = "" + session.getAttribute("flightResponse");
					JSONObject obj = new JSONObject(source);
					int numberofarray = 0;
					JSONArray journeys = obj.getJSONObject("details").getJSONArray("journeys");
					for (int x = 0; x < journeys.length(); x++) {
						JSONArray segments = journeys.getJSONObject(x).getJSONArray("segments");
						for (int j = 0; j < segments.length(); j++) {

							JSONArray bonds = segments.getJSONObject(j).getJSONArray("bonds");
							for (int k = 0; k < bonds.length(); k++) {

								JSONArray legs = bonds.getJSONObject(k).getJSONArray("legs");
								String flightnumber = legs.getJSONObject(k).getString("flightNumber");
								if (flightnumber.equals(req.getFlightNumber())) {
									req.setEngineID("" + segments.getJSONObject(j).getString("engineID"));
									System.out.println("#######################################");
									req.setAirlineName("" + legs.getJSONObject(k).getString("airlineName"));
									req.setArrivalTime("" + legs.getJSONObject(k).getString("arrivalTime"));
									req.setCabin("" + legs.getJSONObject(k).getString("cabin"));
									req.setDepartureTime("" + legs.getJSONObject(k).getString("departureTime"));
									req.setDestination("" + legs.getJSONObject(k).getString("destination"));
									req.setDuration("" + legs.getJSONObject(k).getString("duration"));
									req.setBoundType("" + legs.getJSONObject(k).getString("boundType"));
									req.setFlightNumber("" + legs.getJSONObject(k).getString("flightNumber"));
									req.setOrigin("" + legs.getJSONObject(k).getString("origin"));
									req.setAdults("" + session.getAttribute("adults"));
									req.setChilds("" + session.getAttribute("childs"));
									req.setInfants("" + session.getAttribute("infants"));
									req.setBeginDate("" + session.getAttribute("endDate"));
									req.setFlightName("" + legs.getJSONObject(k).getString("airlineName"));
									req.setTripType("" + session.getAttribute("triptype"));
									req.setArrivalDate("" + legs.getJSONObject(k).getString("arrivalDate"));
									req.setDepartureDate("" + legs.getJSONObject(k).getString("departureDate"));
									req.setDepartureTerminal("" + legs.getJSONObject(k).getString("departureTerminal"));
									req.setArrivalTerminal("" + legs.getJSONObject(k).getString("arrivalTerminal"));
									req.setCapacity("" + legs.getJSONObject(k).getString("capacity"));
									req.setCarrierCode("" + legs.getJSONObject(k).getString("carrierCode"));
									req.setCurrencyCode("" + legs.getJSONObject(k).getString("currencyCode"));
									req.setBaggageUnit("" + legs.getJSONObject(k).getString("baggageUnit"));
									req.setBaggageWeight("" + legs.getJSONObject(k).getString("baggageWeight"));
									req.setFareClassOfService(
											"" + legs.getJSONObject(k).getString("fareClassOfService"));
									req.setSold("" + legs.getJSONObject(k).getString("sold"));
									req.setStatus("" + legs.getJSONObject(k).getString("status"));
									req.setItineraryKey("" + segments.getJSONObject(j).getString("itineraryKey"));
									req.setBondType("" + segments.getJSONObject(j).getString("bondType"));
									req.setSearchId("" + segments.getJSONObject(j).getString("searchId"));
									req.setEngineID("" + segments.getJSONObject(j).getString("engineID"));
									req.setFareRule("" + segments.getJSONObject(j).getString("fareRule"));
									req.setJourneyTime("" + legs.getJSONObject(k).getString("duration"));
									req.setJourneyIndex("" + segments.getJSONObject(j).getString("journeyIndex"));

									JSONArray fare = segments.getJSONObject(j).getJSONArray("fare");
									for (int l = 0; l < fare.length(); l++) {
										JSONArray paxFares = fare.getJSONObject(l).getJSONArray("paxFares");
										JSONArray fares = paxFares.getJSONObject(l).getJSONArray("fares");

										req.setTotalFare("" + paxFares.getJSONObject(l).getString("totalFare"));
										req.setTotalTax("" + paxFares.getJSONObject(l).getString("totalTax"));
										req.setTotalFareWithOutMarkUp(
												"" + fare.getJSONObject(l).getString("totalFareWithOutMarkUp"));
										req.setTotalTaxWithOutMarkUp(
												"" + fare.getJSONObject(l).getString("totalTaxWithOutMarkUp"));
										req.setAmount("" + fares.getJSONObject(l).getString("amount"));

										req.setChargeCode("" + fares.getJSONObject(l).getString("chargeCode"));
										req.setChargeType("" + fares.getJSONObject(l).getString("chargeType"));

										req.setBasicFare("" + fare.getJSONObject(l).getString("basicFare"));
										req.setCancelPenalty("" + paxFares.getJSONObject(l).getString("cancelPenalty"));
										req.setChangePenalty("" + paxFares.getJSONObject(l).getString("changePenalty"));
										req.setTransactionAmount(
												"" + paxFares.getJSONObject(l).getString("transactionAmount"));
										req.setBaseTransactionAmount(
												"" + paxFares.getJSONObject(l).getString("baseTransactionAmount"));
										req.setTraceId("AYTM00011111111110002");

										req.setExchangeRate("" + fare.getJSONObject(l).getString("exchangeRate"));

										req.setMarkUP("" + paxFares.getJSONObject(l).getString("markUP"));
										req.setPaxType("" + paxFares.getJSONObject(l).getString("paxType"));

									}

								}
							}
						}
					}
				}

				String airRePriceRQ = travelflightapi.AirRePriceRQ(req);

				String userflightamount = "";
				JSONObject airRePriceRQobj = new JSONObject(airRePriceRQ);
				String codeairRePriceRQobj = airRePriceRQobj.getString("code");
				if (codeairRePriceRQobj.equalsIgnoreCase("S00")) {
					if (req.getOnewayflight().equals("Firstfligt")) {
						session.setAttribute("Firstfligt", req.getOnewayflight());
						session.setAttribute("airRePriceRQoneway", airRePriceRQ);
					}
					if (req.getOnewayflight().equals("Secondflight")) {

						session.setAttribute("Secondflight", req.getOnewayflight());
						session.setAttribute("airRePriceRQroundway", airRePriceRQ);
					}

					JSONArray airRePriceRQjourneys = airRePriceRQobj.getJSONObject("details").getJSONArray("journeys");

					JSONArray airRePriceRQsegments = airRePriceRQjourneys.getJSONObject(0).getJSONArray("segments");
					for (int j = 0; j < airRePriceRQsegments.length(); j++) {
						JSONArray fare = airRePriceRQsegments.getJSONObject(j).getJSONArray("fare");
						for (int l = 0; l < fare.length(); l++) {
							JSONArray paxFares = fare.getJSONObject(l).getJSONArray("paxFares");
							JSONArray fares = paxFares.getJSONObject(l).getJSONArray("fares");

							String TotalFare = "" + paxFares.getJSONObject(l).getString("totalFare");
							String TotalTax = "" + paxFares.getJSONObject(l).getString("totalTax");
							String TotalFareWithOutMarkUp = ""
									+ fare.getJSONObject(l).getString("totalFareWithOutMarkUp");

							String CancelPenalty = "" + paxFares.getJSONObject(l).getString("cancelPenalty");
							String ChangePenalty = "" + paxFares.getJSONObject(l).getString("changePenalty");
							userflightamount += "#" + TotalFare + "#" + TotalTax + "#" + TotalFareWithOutMarkUp + "#"
									+ CancelPenalty + "#" + ChangePenalty;
						}

					}
					return new ResponseEntity<String>("S00" + "@" + userflightamount, HttpStatus.OK);
				} else {
					return new ResponseEntity<String>("F04" + "@" + "Detail", HttpStatus.OK);
				}

			} catch (Exception e) {
				e.printStackTrace();
				System.err.println(e.getMessage());
				return new ResponseEntity<String>("F04" + "@" + "Detail", HttpStatus.OK);
			}
		} else {
			return new ResponseEntity<String>("F04" + "@" + "Detail", HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/Flight/ShowPriceConnectingDetailRoundTrip", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<String> getShowPriceDetailRoundTripConnecting(HttpSession session,
			@RequestBody FlightPriceCheckRequest req) {
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null && sessionId.length() != 0) {
			try {

				if (req.getOnewayflight().equals("Firstfligt")) {
					String source = "" + session.getAttribute("flightResponse");
					JSONObject obj = new JSONObject(source);
					int numberofarray = 0;
					ArrayList<FlightResponse> flightresponsearr = new ArrayList<FlightResponse>();
					JSONArray journeys = obj.getJSONObject("details").getJSONArray("journeys");
					for (int x = 0; x < journeys.length(); x++) {
						JSONArray segments = journeys.getJSONObject(0).getJSONArray("segments");
						for (int j = 0; j < segments.length(); j++) {

							JSONArray bonds = segments.getJSONObject(j).getJSONArray("bonds");
							for (int k = 0; k < bonds.length(); k++) {

								JSONArray legs = bonds.getJSONObject(k).getJSONArray("legs");
								String flightnumber = legs.getJSONObject(k).getString("flightNumber");
								if (flightnumber.equals(req.getFlightNumber())) {

									numberofarray = k;
									req.setEngineID("" + segments.getJSONObject(j).getString("engineID"));
									req.setAirlineName("" + legs.getJSONObject(k).getString("airlineName"));
									req.setArrivalTime("" + legs.getJSONObject(k).getString("arrivalTime"));
									req.setCabin("" + legs.getJSONObject(k).getString("cabin"));
									req.setDepartureTime("" + legs.getJSONObject(k).getString("departureTime"));
									req.setDestination("" + legs.getJSONObject(k).getString("destination"));
									req.setDuration("" + legs.getJSONObject(k).getString("duration"));
									req.setBoundType("" + legs.getJSONObject(k).getString("boundType"));
									req.setFlightNumber("" + legs.getJSONObject(k).getString("flightNumber"));
									req.setOrigin("" + legs.getJSONObject(k).getString("origin"));
									req.setAdults("" + session.getAttribute("adults"));
									req.setChilds("" + session.getAttribute("childs"));
									req.setInfants("" + session.getAttribute("infants"));
									req.setBeginDate("" + session.getAttribute("beginDate"));
									req.setFlightName("" + legs.getJSONObject(k).getString("airlineName"));
									req.setTripType("" + session.getAttribute("triptype"));
									req.setArrivalDate("" + legs.getJSONObject(k).getString("arrivalDate"));
									req.setDepartureDate("" + legs.getJSONObject(k).getString("departureDate"));
									req.setDepartureTerminal("" + legs.getJSONObject(k).getString("departureTerminal"));
									req.setArrivalTerminal("" + legs.getJSONObject(k).getString("arrivalTerminal"));
									req.setCapacity("" + legs.getJSONObject(k).getString("capacity"));
									req.setCarrierCode("" + legs.getJSONObject(k).getString("carrierCode"));
									req.setCurrencyCode("" + legs.getJSONObject(k).getString("currencyCode"));
									req.setBaggageUnit("" + legs.getJSONObject(k).getString("baggageUnit"));
									req.setBaggageWeight("" + legs.getJSONObject(k).getString("baggageWeight"));
									req.setFareClassOfService(
											"" + legs.getJSONObject(k).getString("fareClassOfService"));
									req.setSold("" + legs.getJSONObject(k).getString("sold"));
									req.setStatus("" + legs.getJSONObject(k).getString("status"));
									req.setItineraryKey("" + segments.getJSONObject(j).getString("itineraryKey"));
									req.setBondType("" + segments.getJSONObject(j).getString("bondType"));
									req.setSearchId("" + segments.getJSONObject(j).getString("searchId"));
									req.setEngineID("" + segments.getJSONObject(j).getString("engineID"));
									req.setFareRule("" + segments.getJSONObject(j).getString("fareRule"));
									req.setJourneyTime("" + legs.getJSONObject(k).getString("duration"));
									req.setJourneyIndex("" + segments.getJSONObject(j).getString("journeyIndex"));

									JSONArray fare = segments.getJSONObject(j).getJSONArray("fare");
									for (int l = 0; l < fare.length(); l++) {
										JSONArray paxFares = fare.getJSONObject(l).getJSONArray("paxFares");
										JSONArray fares = paxFares.getJSONObject(l).getJSONArray("fares");

										req.setTotalFare("" + paxFares.getJSONObject(l).getString("totalFare"));
										req.setTotalTax("" + paxFares.getJSONObject(l).getString("totalTax"));
										req.setTotalFareWithOutMarkUp(
												"" + fare.getJSONObject(l).getString("totalFareWithOutMarkUp"));
										req.setTotalTaxWithOutMarkUp(
												"" + fare.getJSONObject(l).getString("totalTaxWithOutMarkUp"));
										req.setAmount("" + fares.getJSONObject(l).getString("amount"));

										req.setChargeCode("" + fares.getJSONObject(l).getString("chargeCode"));
										req.setChargeType("" + fares.getJSONObject(l).getString("chargeType"));

										req.setBasicFare("" + fare.getJSONObject(l).getString("basicFare"));
										req.setCancelPenalty("" + paxFares.getJSONObject(l).getString("cancelPenalty"));
										req.setChangePenalty("" + paxFares.getJSONObject(l).getString("changePenalty"));
										req.setTransactionAmount(
												"" + paxFares.getJSONObject(l).getString("transactionAmount"));
										req.setBaseTransactionAmount(
												"" + paxFares.getJSONObject(l).getString("baseTransactionAmount"));
										req.setTraceId("AYTM00011111111110002");

										req.setExchangeRate("" + fare.getJSONObject(l).getString("exchangeRate"));

										req.setMarkUP("" + paxFares.getJSONObject(l).getString("markUP"));
										req.setPaxType("" + paxFares.getJSONObject(l).getString("paxType"));

									}

								}
							}
						}
					}
				}
				if (req.getOnewayflight().equals("Secondflight")) {
					String source = "" + session.getAttribute("flightResponse");
					JSONObject obj = new JSONObject(source);
					int numberofarray = 0;
					JSONArray journeys = obj.getJSONObject("details").getJSONArray("journeys");
					for (int x = 0; x < journeys.length(); x++) {
						JSONArray segments = journeys.getJSONObject(x).getJSONArray("segments");
						for (int j = 0; j < segments.length(); j++) {

							JSONArray bonds = segments.getJSONObject(j).getJSONArray("bonds");
							for (int k = 0; k < bonds.length(); k++) {

								JSONArray legs = bonds.getJSONObject(k).getJSONArray("legs");
								String flightnumber = legs.getJSONObject(k).getString("flightNumber");
								if (flightnumber.equals(req.getFlightNumber())) {
									req.setEngineID("" + segments.getJSONObject(j).getString("engineID"));
									System.out.println("#######################################");
									req.setAirlineName("" + legs.getJSONObject(k).getString("airlineName"));
									req.setArrivalTime("" + legs.getJSONObject(k).getString("arrivalTime"));
									req.setCabin("" + legs.getJSONObject(k).getString("cabin"));
									req.setDepartureTime("" + legs.getJSONObject(k).getString("departureTime"));
									req.setDestination("" + legs.getJSONObject(k).getString("destination"));
									req.setDuration("" + legs.getJSONObject(k).getString("duration"));
									req.setBoundType("" + legs.getJSONObject(k).getString("boundType"));
									req.setFlightNumber("" + legs.getJSONObject(k).getString("flightNumber"));
									req.setOrigin("" + legs.getJSONObject(k).getString("origin"));
									req.setAdults("" + session.getAttribute("adults"));
									req.setChilds("" + session.getAttribute("childs"));
									req.setInfants("" + session.getAttribute("infants"));
									req.setBeginDate("" + session.getAttribute("endDate"));
									req.setFlightName("" + legs.getJSONObject(k).getString("airlineName"));
									req.setTripType("" + session.getAttribute("triptype"));
									req.setArrivalDate("" + legs.getJSONObject(k).getString("arrivalDate"));
									req.setDepartureDate("" + legs.getJSONObject(k).getString("departureDate"));
									req.setDepartureTerminal("" + legs.getJSONObject(k).getString("departureTerminal"));
									req.setArrivalTerminal("" + legs.getJSONObject(k).getString("arrivalTerminal"));
									req.setCapacity("" + legs.getJSONObject(k).getString("capacity"));
									req.setCarrierCode("" + legs.getJSONObject(k).getString("carrierCode"));
									req.setCurrencyCode("" + legs.getJSONObject(k).getString("currencyCode"));
									req.setBaggageUnit("" + legs.getJSONObject(k).getString("baggageUnit"));
									req.setBaggageWeight("" + legs.getJSONObject(k).getString("baggageWeight"));
									req.setFareClassOfService(
											"" + legs.getJSONObject(k).getString("fareClassOfService"));
									req.setSold("" + legs.getJSONObject(k).getString("sold"));
									req.setStatus("" + legs.getJSONObject(k).getString("status"));
									req.setItineraryKey("" + segments.getJSONObject(j).getString("itineraryKey"));
									req.setBondType("" + segments.getJSONObject(j).getString("bondType"));
									req.setSearchId("" + segments.getJSONObject(j).getString("searchId"));
									req.setEngineID("" + segments.getJSONObject(j).getString("engineID"));
									req.setFareRule("" + segments.getJSONObject(j).getString("fareRule"));
									req.setJourneyTime("" + legs.getJSONObject(k).getString("duration"));
									req.setJourneyIndex("" + segments.getJSONObject(j).getString("journeyIndex"));

									JSONArray fare = segments.getJSONObject(j).getJSONArray("fare");
									for (int l = 0; l < fare.length(); l++) {
										JSONArray paxFares = fare.getJSONObject(l).getJSONArray("paxFares");
										JSONArray fares = paxFares.getJSONObject(l).getJSONArray("fares");

										req.setTotalFare("" + paxFares.getJSONObject(l).getString("totalFare"));
										req.setTotalTax("" + paxFares.getJSONObject(l).getString("totalTax"));
										req.setTotalFareWithOutMarkUp(
												"" + fare.getJSONObject(l).getString("totalFareWithOutMarkUp"));
										req.setTotalTaxWithOutMarkUp(
												"" + fare.getJSONObject(l).getString("totalTaxWithOutMarkUp"));
										req.setAmount("" + fares.getJSONObject(l).getString("amount"));

										req.setChargeCode("" + fares.getJSONObject(l).getString("chargeCode"));
										req.setChargeType("" + fares.getJSONObject(l).getString("chargeType"));

										req.setBasicFare("" + fare.getJSONObject(l).getString("basicFare"));
										req.setCancelPenalty("" + paxFares.getJSONObject(l).getString("cancelPenalty"));
										req.setChangePenalty("" + paxFares.getJSONObject(l).getString("changePenalty"));
										req.setTransactionAmount(
												"" + paxFares.getJSONObject(l).getString("transactionAmount"));
										req.setBaseTransactionAmount(
												"" + paxFares.getJSONObject(l).getString("baseTransactionAmount"));
										req.setTraceId("AYTM00011111111110002");

										req.setExchangeRate("" + fare.getJSONObject(l).getString("exchangeRate"));

										req.setMarkUP("" + paxFares.getJSONObject(l).getString("markUP"));
										req.setPaxType("" + paxFares.getJSONObject(l).getString("paxType"));

									}

								}
							}
						}
					}
				}

				String source = "" + session.getAttribute("flightResponse");
				String airRePriceRQ = travelflightapi.AirRePriceRQConnecting(req, source,
						"" + session.getAttribute("destination"), session);

				String userflightamount = "";
				JSONObject airRePriceRQobj = new JSONObject(airRePriceRQ);
				String codeairRePriceRQobj = airRePriceRQobj.getString("code");
				if (codeairRePriceRQobj.equalsIgnoreCase("S00")) {
					if (req.getOnewayflight().equals("Firstfligt")) {
						session.setAttribute("Firstfligt", req.getOnewayflight());
						session.setAttribute("airRePriceRQoneway", airRePriceRQ);
					}
					if (req.getOnewayflight().equals("Secondflight")) {

						session.setAttribute("Secondflight", req.getOnewayflight());
						session.setAttribute("airRePriceRQroundway", airRePriceRQ);
					}

					JSONArray airRePriceRQjourneys = airRePriceRQobj.getJSONObject("details").getJSONArray("journeys");

					JSONArray airRePriceRQsegments = airRePriceRQjourneys.getJSONObject(0).getJSONArray("segments");
					for (int j = 0; j < airRePriceRQsegments.length(); j++) {
						JSONArray fare = airRePriceRQsegments.getJSONObject(j).getJSONArray("fare");
						for (int l = 0; l < fare.length(); l++) {
							JSONArray paxFares = fare.getJSONObject(l).getJSONArray("paxFares");
							JSONArray fares = paxFares.getJSONObject(l).getJSONArray("fares");

							String TotalFare = "" + paxFares.getJSONObject(l).getString("totalFare");
							String TotalTax = "" + paxFares.getJSONObject(l).getString("totalTax");
							String TotalFareWithOutMarkUp = ""
									+ fare.getJSONObject(l).getString("totalFareWithOutMarkUp");

							String CancelPenalty = "" + paxFares.getJSONObject(l).getString("cancelPenalty");
							String ChangePenalty = "" + paxFares.getJSONObject(l).getString("changePenalty");
							userflightamount += "#" + TotalFare + "#" + TotalTax + "#" + TotalFareWithOutMarkUp + "#"
									+ CancelPenalty + "#" + ChangePenalty;
						}

					}
					return new ResponseEntity<String>("S00" + "@" + userflightamount, HttpStatus.OK);
				} else {
					return new ResponseEntity<String>("F04" + "@" + "Detail", HttpStatus.OK);
				}

			} catch (Exception e) {
				e.printStackTrace();
				System.err.println(e);
				System.err.println(e.getMessage());
				return new ResponseEntity<String>("F04" + "@" + "Detail", HttpStatus.OK);
			}
		} else {
			return new ResponseEntity<String>("F04" + "@" + "Detail", HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/Flight/CheckOut", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<String> getFlightCheckOutPostMethod(HttpSession session, @RequestBody FlightBookRequest req) {
		String sessionId = (String) session.getAttribute("sessionId");
		try {

			if (sessionId != null && sessionId.length() != 0) {

				System.err.println("Payment Amount:: " + req.getGrandtotal());

				String ticketDetails = "" + session.getAttribute("ticketDetails");
				String flightBookingInitiate = travelflightapi.FlightBookingInitiate(sessionId, req.getFirstName(),
						req.getGrandtotal(), ticketDetails, "Wallet");

				JSONObject objflightBookingInitiate = new JSONObject(flightBookingInitiate);
				String codeflightBookingInitiate = objflightBookingInitiate.getString("code");
				if (codeflightBookingInitiate.equalsIgnoreCase("S00")) // (true)//
				{
					String transactionRefNo = objflightBookingInitiate.getJSONObject("details")
							.getString("transactionRefNo");// ""+System.currentTimeMillis();//
					req.setAdults("" + session.getAttribute("adults"));
					req.setChilds("" + session.getAttribute("childs"));
					req.setInfants("" + session.getAttribute("infants"));
					req.setDestination("" + session.getAttribute("destination"));
					req.setTransactionId(transactionRefNo);
					String source = "" + session.getAttribute("airRePriceRQ");
					String airRePriceRQ = travelflightapi.AirBookRQ(req, source, session);
					JSONObject obj = new JSONObject(airRePriceRQ);
					String code = obj.getString("code");
					if (code.equalsIgnoreCase("S00")) {
						String bookingRefId = obj.getJSONObject("details").getString("bookingRefId");
						String transactionRefNomdex = obj.getJSONObject("details").getString("transactionRefNo");
						JSONArray objcc = obj.getJSONObject("details").getJSONObject("bookingDetail")
								.getJSONObject("pnrDetail").getJSONArray("tickets");
						String ticketNumber = "";
						String firstName = "";

						for (int i = 0; i < objcc.length(); i++) {
							ticketNumber += "~" + objcc.getJSONObject(i).getString("ticketNumber");
							firstName += "~" + objcc.getJSONObject(i).getString("firstName");
						}
						JSONArray objccpnrs = obj.getJSONObject("details").getJSONObject("bookingDetail")
								.getJSONObject("pnrDetail").getJSONArray("pnrs");
						String pnrNo = objccpnrs.getJSONObject(0).getString("pnr");

						String flightBookingSucess = travelflightapi.FlightBookingSucess(sessionId, ticketNumber,
								firstName, bookingRefId, transactionRefNomdex, req.getGrandtotal(), ticketDetails,
								"Booked", "Success", "Wallet", transactionRefNo, true, req.getEmailAddress(),
								req.getMobileNumber());

						JSONObject flightBookingSucessobj = new JSONObject(flightBookingSucess);
						String flightBookingSucesscode = flightBookingSucessobj.getString("code");
						if (flightBookingSucesscode.equalsIgnoreCase("S00")) {
							return new ResponseEntity<String>("S00" + "@" + "Flight Successfully Booked.",
									HttpStatus.OK);
						} else {
							travelflightapi.FlightBookingSucess(sessionId, ticketNumber, firstName, bookingRefId,
									transactionRefNomdex, req.getGrandtotal(), ticketDetails, "Booked", "Success",
									"Wallet", transactionRefNo, false, req.getEmailAddress(), req.getMobileNumber());
							return new ResponseEntity<String>("F04" + "@" + "Service Down Please Try Again Later",
									HttpStatus.OK);
						}
					} else {
						travelflightapi.FlightBookingSucess(sessionId, "Flight Not booked", req.getFirstName(),
								"Flight Not booked", "Flight Not booked", req.getGrandtotal(), ticketDetails,
								"Processing", "Initiated", "Wallet", transactionRefNo, false, req.getEmailAddress(),
								req.getMobileNumber());
						return new ResponseEntity<String>(
								"F04" + "@" + "" + objflightBookingInitiate.getString("message"), HttpStatus.OK);
					}

				} else if (codeflightBookingInitiate.equalsIgnoreCase("T01")) {
					String source = "" + session.getAttribute("airRePriceRQ");
					req.setTransactionId("" + System.currentTimeMillis());
					req.setAdults("" + session.getAttribute("adults"));
					req.setChilds("" + session.getAttribute("childs"));
					req.setInfants("" + session.getAttribute("infants"));
					req.setDestination("" + session.getAttribute("destination"));
					System.err.println("First Name:: " + req.getFirstName());
					String airRePriceRQ = travelflightapi.AirBookRQwithPaymentGatway(req, source, session);
					session.setAttribute("FlightBookWithSplitPayment", airRePriceRQ);
					session.setAttribute("splitFlightBookAmt", req.getGrandtotal());
					session.setAttribute("email", req.getEmailAddress());
					session.setAttribute("mobile", req.getMobileNumber());

					return new ResponseEntity<String>(
							"T01" + "@" + "Splitpayment" + "@" + objflightBookingInitiate.getString("balance"),
							HttpStatus.OK);
				} else {

					travelflightapi.FlightBookingSucess(sessionId, "Flight Not booked", req.getFirstName(),
							"Flight Not booked", "Flight Not booked", req.getGrandtotal(), ticketDetails, "Processing",
							objflightBookingInitiate.getString("message"), "Wallet", "Flightnot booked", false,
							req.getEmailAddress(), req.getMobileNumber());
					return new ResponseEntity<String>("F04" + "@" + "" + objflightBookingInitiate.getString("message"),
							HttpStatus.OK);
				}

			} else {
				return new ResponseEntity<String>("F04" + "@" + "Please Login and try Again Later", HttpStatus.OK);
			}

		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
			return new ResponseEntity<String>("F04" + "@" + "Service Down Please Try Again Later", HttpStatus.OK);
		}

	}

	@RequestMapping(method = RequestMethod.POST, value = "/Flight/ReturnCheckOut", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<String> getFlightReturnCheckOutCheckOutPostMethod(HttpSession session,
			@RequestBody ReturnFlightBookRequest req) {
		String sessionId = (String) session.getAttribute("sessionId");
		try {
			if (sessionId != null && sessionId.length() != 0) {

				session.setAttribute("splitRountFlightAmt", req.getGrandtotal());

				String ticketDetails = "" + session.getAttribute("ticketDetails");
				String flightBookingInitiate = travelflightapi.FlightBookingInitiate(sessionId, req.getFirstName(),
						req.getGrandtotal(), ticketDetails, "Wallet");

				JSONObject objflightBookingInitiate = new JSONObject(flightBookingInitiate);
				String codeflightBookingInitiate = objflightBookingInitiate.getString("code");
				if (codeflightBookingInitiate.equalsIgnoreCase("S00")) // (true)//
				{

					String transactionRefNo = objflightBookingInitiate.getJSONObject("details")
							.getString("transactionRefNo"); // "" +
															// System.currentTimeMillis();//
					req.setAdults("" + session.getAttribute("adults"));
					req.setChilds("" + session.getAttribute("childs"));
					req.setInfants("" + session.getAttribute("infants"));
					req.setDestination("" + session.getAttribute("destination"));
					req.setTransactionId(transactionRefNo);
					String source = "" + session.getAttribute("airRePriceRQoneway");
					String destination = "" + session.getAttribute("airRePriceRQroundway");
					String airRePriceRQ = travelflightapi.AirBookRQreturn(req, source, destination, session);
					JSONObject obj = new JSONObject(airRePriceRQ);
					String code = obj.getString("code");
					if (code.equalsIgnoreCase("S00")) {
						String bookingRefId = obj.getJSONObject("details").getString("bookingRefId");
						String transactionRefNomdex = obj.getJSONObject("details").getString("transactionRefNo");
						JSONArray objcc = obj.getJSONObject("details").getJSONObject("bookingDetail")
								.getJSONObject("pnrDetail").getJSONArray("tickets");
						String ticketNumber = "";
						String firstName = "";

						for (int i = 0; i < objcc.length(); i++) {
							ticketNumber += "~" + objcc.getJSONObject(i).getString("ticketNumber");
							firstName += "~" + objcc.getJSONObject(i).getString("firstName");
						}
						JSONArray objccpnrs = obj.getJSONObject("details").getJSONObject("bookingDetail")
								.getJSONObject("pnrDetail").getJSONArray("pnrs");
						String flightBookingSucess = travelflightapi.FlightBookingSucess(sessionId, ticketNumber,
								firstName, bookingRefId, transactionRefNomdex, req.getGrandtotal(), ticketDetails,
								"Booked", "Success", "Wallet", transactionRefNo, true, req.getEmailAddress(),
								req.getMobileNumber());

						JSONObject flightBookingSucessobj = new JSONObject(flightBookingSucess);
						String flightBookingSucesscode = flightBookingSucessobj.getString("code");
						if (flightBookingSucesscode.equalsIgnoreCase("S00")) {
							return new ResponseEntity<String>("S00" + "@" + "Flight Successfully Booked.",
									HttpStatus.OK);
						} else {
							travelflightapi.FlightBookingSucess(sessionId, ticketNumber, firstName, bookingRefId,
									transactionRefNomdex, req.getGrandtotal(), ticketDetails, "Booked", "Initiated",
									"Wallet", transactionRefNo, false, req.getEmailAddress(), req.getMobileNumber());

							return new ResponseEntity<String>("F04" + "@" + "Service Down Please Try Again Later",
									HttpStatus.OK);
						}
					} else {
						travelflightapi.FlightBookingSucess(sessionId, "Flight Not booked", req.getFirstName(),
								"Flight Not booked", "Flight Not booked", req.getGrandtotal(), ticketDetails,
								"Processing", "Initiated", "Wallet", transactionRefNo, false, req.getEmailAddress(),
								req.getMobileNumber());
						return new ResponseEntity<String>("F04" + "@" + "Service Down Please Try Again Later",
								HttpStatus.OK);
					}
				} else if (codeflightBookingInitiate.equalsIgnoreCase("T01")) {

					req.setTransactionId("" + System.currentTimeMillis());
					req.setAdults("" + session.getAttribute("adults"));
					req.setChilds("" + session.getAttribute("childs"));
					req.setInfants("" + session.getAttribute("infants"));
					req.setDestination("" + session.getAttribute("destination"));
					String source = "" + session.getAttribute("airRePriceRQoneway");
					String destination = "" + session.getAttribute("airRePriceRQroundway");
					String airRePriceRQ = travelflightapi.AirBookRQreturnwithPaymentGatway(req, source, destination,
							session);
					session.setAttribute("FlightBookWithSplitPaymentReturn", airRePriceRQ);
					return new ResponseEntity<String>(
							"T01" + "@" + "Splitpayment" + "@" + objflightBookingInitiate.getString("balance"),
							HttpStatus.OK);
				} else {
					travelflightapi.FlightBookingSucess(sessionId, "Flight Not booked", req.getFirstName(),
							"Flight Not booked", "Flight Not booked", req.getGrandtotal(), ticketDetails, "Processing",
							"Initiated", "Wallet", "Flight Not booked", false, req.getEmailAddress(),
							req.getMobileNumber());
					return new ResponseEntity<String>("F04" + "@" + "" + objflightBookingInitiate.getString("message"),
							HttpStatus.OK);
				}

			} else {
				return new ResponseEntity<String>("F04" + "@" + "Please Login and try Again Later", HttpStatus.OK);
			}

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>("F04" + "@" + "Service Down Please Try Again Later", HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/Flight/InternationalReturnCheckOut", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<String> getFlightInternationalReturnCheckOut(HttpSession session,
			@RequestBody ReturnFlightBookRequest req) throws Exception {

		String sessionId = (String) session.getAttribute("sessionId");
		try {
			if (sessionId != null && sessionId.length() != 0) {

				String ticketDetails = "" + session.getAttribute("ticketDetails");
				String flightBookingInitiate = travelflightapi.FlightBookingInitiate(sessionId, req.getFirstName(),
						req.getGrandtotal(), ticketDetails, "Wallet");

				JSONObject objflightBookingInitiate = new JSONObject(flightBookingInitiate);
				String codeflightBookingInitiate = objflightBookingInitiate.getString("code");
				if (codeflightBookingInitiate.equalsIgnoreCase("S00")) // (true)//
				{

					String transactionRefNo = objflightBookingInitiate.getJSONObject("details")
							.getString("transactionRefNo"); // ""
															// +System.currentTimeMillis();//
					req.setTransactionId(transactionRefNo);
					req.setAdults("" + session.getAttribute("adults"));
					req.setChilds("" + session.getAttribute("childs"));
					req.setInfants("" + session.getAttribute("infants"));
					org.json.JSONObject objmain1 = CreateJsonRequestFlight.createjsonBookApiinternationalFlightBook(req,
							session);

					String airRePriceRQ = travelflightapi.FlightBookPaymentGatway(objmain1.toString());
					JSONObject obj = new JSONObject(airRePriceRQ);
					String code = obj.getString("code");
					if (code.equalsIgnoreCase("S00")) {

						String bookingRefId = obj.getJSONObject("details").getString("bookingRefId");
						String transactionRefNomdex = obj.getJSONObject("details").getString("transactionRefNo");
						JSONArray objcc = obj.getJSONObject("details").getJSONObject("bookingDetail")
								.getJSONObject("pnrDetail").getJSONArray("tickets");
						String ticketNumber = "";
						String firstName = "";

						for (int i = 0; i < objcc.length(); i++) {
							ticketNumber += "~" + objcc.getJSONObject(i).getString("ticketNumber");
							firstName += "~" + objcc.getJSONObject(i).getString("firstName");
						}
						String flightBookingSucess = travelflightapi.FlightBookingSucess(sessionId, ticketNumber,
								firstName, bookingRefId, transactionRefNomdex, req.getGrandtotal(), ticketDetails,
								"Booked", "Success", "Wallet", transactionRefNo, true, req.getEmailAddress(),
								req.getMobileNumber());

						JSONObject flightBookingSucessobj = new JSONObject(flightBookingSucess);
						String flightBookingSucesscode = flightBookingSucessobj.getString("code");
						if (flightBookingSucesscode.equalsIgnoreCase("S00")) {
							return new ResponseEntity<String>("S00" + "@" + "Flight Successfully Booked.",
									HttpStatus.OK);
						} else {
							travelflightapi.FlightBookingSucess(sessionId, ticketNumber, firstName, bookingRefId,
									transactionRefNomdex, req.getGrandtotal(), ticketDetails, "Booked", "Initiated",
									"Wallet", transactionRefNo, false, req.getEmailAddress(), req.getMobileNumber());

							return new ResponseEntity<String>("F04" + "@" + "Service Down Please Try Again Later",
									HttpStatus.OK);
						}
					} else {
						travelflightapi.FlightBookingSucess(sessionId, "Flight Not booked", req.getFirstName(),
								"Flight Not booked", "Flight Not booked", req.getGrandtotal(), ticketDetails,
								"Processing", "Initiated", "Wallet", transactionRefNo, false, req.getEmailAddress(),
								req.getMobileNumber());
						return new ResponseEntity<String>("F04" + "@" + "Service Down Please Try Again Later",
								HttpStatus.OK);
					}
				} else if (codeflightBookingInitiate.equalsIgnoreCase("T01")) {

					req.setTransactionId("" + System.currentTimeMillis());
					req.setAdults("" + session.getAttribute("adults"));
					req.setChilds("" + session.getAttribute("childs"));
					req.setInfants("" + session.getAttribute("infants"));

					org.json.JSONObject objmain1 = CreateJsonRequestFlight.createjsonBookApiinternationalFlightBook(req,
							session);

					session.setAttribute("FlightBookWithSplitPaymentReturn", objmain1);
					return new ResponseEntity<String>("T01" + "@" + "Redirect To SplitPayment" + "@"
							+ objflightBookingInitiate.getString("balance"), HttpStatus.OK);
				} else {
					return new ResponseEntity<String>("F04" + "@" + "" + objflightBookingInitiate.getString("message"),
							HttpStatus.OK);
				}

			} else {
				return new ResponseEntity<String>("F04" + "@" + "Please Login and try Again Later", HttpStatus.OK);
			}

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>("F04" + "@" + "Service Down Please Try Again Later", HttpStatus.OK);
		}

	}

	// -----------------------------------Payment gateway
	// Deduction-----------------------------------------------
	@RequestMapping(value = "/Flight/PaymentProcess", method = RequestMethod.POST)
	public String processReturnRedirect(@ModelAttribute LoadMoneyFlightRequest dto, HttpServletRequest request,
			ModelMap modelMap, HttpSession session) {
		String sessionId = (String) session.getAttribute("sessionId");
		request.setAttribute("sessionId", sessionId);
		session.setAttribute("email", dto.getEmailAddress());
		session.setAttribute("mobile", dto.getMobileNumber());
		FlightBookRequest req = new FlightBookRequest();
		LoadMoneyResponse loadMoneyResponse = new LoadMoneyResponse();
		request.getSession().setAttribute("sessionId", sessionId);
		dto.setSessionId(sessionId);
		if (dto.getUseVnetWeb().equalsIgnoreCase("vnet")) {
			System.err.println("inside vnet");
			FlightVnetRequest vNetRequest = new FlightVnetRequest();
			vNetRequest.setSessionId(sessionId);
			vNetRequest.setReturnURL(UrlMetadatas.WEBURL + "/User/Travel/Flight/VRedirect");
			vNetRequest.setAmount(dto.getAmount());
			VNetResponse vNetResponse = loadMoneyApi.initiateVnetBankingFlight(vNetRequest);
			if (vNetResponse.isSuccess()) {
		//		req.setTransactionId("" + loadMoneyResponse.getReferenceNo());
				req.setFirstName(dto.getFirstName());
				req.setFirstNamechild(dto.getFirstNamechild());
				req.setFirstNameinfant(dto.getFirstNameinfant());

				req.setLastName(dto.getLastName());
				req.setLastNamechild(dto.getLastNamechild());
				req.setLastNameinfant(dto.getLastNameinfant());

				req.setEmailAddress(dto.getEmail());
				req.setMobileNumber(dto.getPhone());
				req.setGrandtotal(req.getAmount());
				req.setUseradultsgender(dto.getUseradultsgender());
				req.setUserchildsgender(dto.getUserchildsgender());
				req.setUserinfantsgender(dto.getUserinfantsgender());

				req.setAdults("" + session.getAttribute("adults"));
				req.setChilds("" + session.getAttribute("childs"));
				req.setInfants("" + session.getAttribute("infants"));

				String source = "" + session.getAttribute("airRePriceRQ");
				String airRePriceRQ = travelflightapi.AirBookRQwithPaymentGatway(req, source, session);

				session.setAttribute("FlightBookWithPaymentGateway", airRePriceRQ);
				modelMap.addAttribute("vnet", vNetResponse);
				return "User/LoadMoney/vnetpay";
			}
		} else if (dto.getUseVnetWeb().equals("others")) {
			dto.setReturnUrl(UrlMetadatas.WEBURL + "/User/Travel/Flight/Redirect");
			loadMoneyResponse = loadMoneyApi.SplitpaymentMoneyRequest(dto);
			if (loadMoneyResponse.isSuccess()) {
			//	req.setTransactionId("" + loadMoneyResponse.getReferenceNo());
				req.setFirstName(dto.getFirstName());
				req.setFirstNamechild(dto.getFirstNamechild());
				req.setFirstNameinfant(dto.getFirstNameinfant());

				req.setLastName(dto.getLastName());
				req.setLastNamechild(dto.getLastNamechild());
				req.setLastNameinfant(dto.getLastNameinfant());
				req.setEmailAddress(dto.getEmail());
				req.setMobileNumber(dto.getPhone());
				req.setGrandtotal(req.getAmount());
				req.setUseradultsgender(dto.getUseradultsgender());
				req.setUserchildsgender(dto.getUserchildsgender());
				req.setUserinfantsgender(dto.getUserinfantsgender());

				req.setAdults("" + session.getAttribute("adults"));
				req.setChilds("" + session.getAttribute("childs"));
				req.setInfants("" + session.getAttribute("infants"));

				String source = "" + session.getAttribute("airRePriceRQ");
				session.setAttribute("firstName", req.getFirstName());
				String airRePriceRQ = travelflightapi.AirBookRQwithPaymentGatway(req, source, session);
				System.out.println(airRePriceRQ);
				session.setAttribute("FlightBookWithPaymentGateway", airRePriceRQ);
				modelMap.addAttribute("loadmoney", loadMoneyResponse);
				return "User/Pay";
			} else {
				/*System.err.format("Load money response is %s\n", loadMoneyResponse.getDescription());
				modelMap.addAttribute(ModelMapKey.MESSAGE, loadMoneyResponse.getDescription());*/
				return "User/Home";
			}
		}
		return "redirect:/User/Home";

	}

	@RequestMapping(value = "/Flight/Redirect", method = RequestMethod.POST)
	public String redirectLoadMoney(WEBSRedirectResponse ebsResponse, Model model, HttpSession session)
			throws Exception {
		String mobile = "" + session.getAttribute("mobile");
		String email = "" + session.getAttribute("email");
		
		String ticketDetails = "" + session.getAttribute("ticketDetails");
		session.setAttribute("msgvalue", "1");
		EBSRedirectResponse ebsRedirectResponse = ConvertUtil.convertFromWEBS(ebsResponse);
		String sessionId = (String) session.getAttribute("sessionId");
		ResponseDTO result = loadMoneyApi.verifyEBSTransaction(ebsRedirectResponse);
		if (result.isSuccess()) {
			ebsRedirectResponse.setSuccess(true);
			ebsRedirectResponse.setResponseCode("0");
		} else {
			ebsRedirectResponse.setSuccess(false);
			ebsRedirectResponse.setResponseCode("1");
		}
		EBSRedirectResponse redirectResponse = loadMoneyApi.processRedirectSDKSplitpaymentMoney(ebsRedirectResponse);
		if (redirectResponse.isSuccess() || redirectResponse.getResponseCode().equals("0")) {

			String origin = "" + session.getAttribute("source");
			String destination = "" + session.getAttribute("destination");

			String jsonflightRequest = "" + session.getAttribute("FlightBookWithPaymentGateway");
			String airRePriceRQ = travelflightapi.FlightBookPaymentGatway(jsonflightRequest);
			JSONObject obj = new JSONObject(airRePriceRQ);
			String code = obj.getString("code");
			if (code.equalsIgnoreCase("S00")) {

				String bookingRefId = obj.getJSONObject("details").getString("bookingRefId");
				String transactionRefNomdex = obj.getJSONObject("details").getString("transactionRefNo");
				JSONArray objcc = obj.getJSONObject("details").getJSONObject("bookingDetail").getJSONObject("pnrDetail")
						.getJSONArray("tickets");
				String ticketNumber = "";
				String firstName = "";
				JSONArray objccpnrs = obj.getJSONObject("details").getJSONObject("bookingDetail")
						.getJSONObject("pnrDetail").getJSONArray("pnrs");
				String pnrNo = objccpnrs.getJSONObject(0).getString("pnr");

				for (int i = 0; i < objcc.length(); i++) {
					ticketNumber += "~" + objcc.getJSONObject(i).getString("ticketNumber");

					firstName += "~" + objcc.getJSONObject(i).getString("firstName");
				}

				String flightBookingSucess = travelflightapi.FlightPaymentGatewaySucess(sessionId, ticketNumber,
						firstName, bookingRefId, transactionRefNomdex, ebsRedirectResponse.getAmount(), ticketDetails,
						"Booked", "Success", "Pay with ebs payment gateway", ebsRedirectResponse.getMerchantRefNo(),
						true,email,mobile);

				session.setAttribute("msg", "Transasction Successful.Your Flight Is Booked.");

			} else {
				String bookingRefId = obj.getString("bookingRefId");
				String firstName = "" + session.getAttribute("firstName");
				String flightBookingSucess = travelflightapi.FlightPaymentGatewaySucess(sessionId, "Flight is Not Book",
						firstName, bookingRefId, "Flight is Not Book", ebsRedirectResponse.getAmount(), ticketDetails,
						"Processing", "Success", "Pay with ebs payment gateway", ebsRedirectResponse.getMerchantRefNo(),
						false,email,mobile);
				session.setAttribute("msg",
						"Transasction Successful,Your Flight Is Not Booked.Please Contact Your Customer Care");

			}

		} else {

			travelflightapi.FlightPaymentGatewaySucess(sessionId, "Flight is Not Book",
					"" + session.getAttribute("firstName"), "Flight is Not Book", "Flight is Not Book",
					ebsRedirectResponse.getAmount(), ticketDetails, "Processing", "Initiated",
					"Pay with ebs payment gateway", ebsRedirectResponse.getMerchantRefNo(), false,email,mobile);
			session.setAttribute("msg", "Transaction failed, Please try again later.");
		}
		model.addAttribute("loadmoneyResponse", redirectResponse);
		return "User/Loading";

	}

	@RequestMapping(value = "/Flight/VRedirect", method = RequestMethod.POST)
	public String redirectVNetLoadMoney(VRedirectResponse dto, Model model, HttpSession session) throws JSONException {
		String sessionId = (String) session.getAttribute("sessionId");
		ResponseDTO responseDTO = loadMoneyApi.handleRedirectRequestFlight(dto);
		if (responseDTO.getCode().equalsIgnoreCase("S00")) {
			String jsonflightRequest = "" + session.getAttribute("FlightBookWithPaymentGateway");
			String airRePriceRQ = travelflightapi.FlightBookPaymentGatway(jsonflightRequest);
			JSONObject obj = new JSONObject(airRePriceRQ);
			String code = obj.getString("code");
			if (code.equalsIgnoreCase("S00")) {
				String bookingRefId = obj.getJSONObject("details").getString("bookingRefId");
				JSONArray objcc = obj.getJSONObject("details").getJSONObject("bookingDetail").getJSONObject("pnrDetail")
						.getJSONArray("tickets");
				String ticketNumber = objcc.getJSONObject(0).getString("ticketNumber");
				JSONArray objccpnrs = obj.getJSONObject("details").getJSONObject("bookingDetail")
						.getJSONObject("pnrDetail").getJSONArray("pnrs");
				String pnrNo = objccpnrs.getJSONObject(0).getString("pnr");

				// String flightBookingSucess =
				// travelflightapi.FlightBookingSucess(sessionId, bookingRefId,
				// String.valueOf(responseDTO.getAmount()), ticketNumber,
				// responseDTO.getTransactionId(), pnrNo,
				// true);
				session.setAttribute("msg", "Transasction Successful.Your Flight Is Booked.");
			} else {
				// String flightBookingSucess =
				// travelflightapi.FlightBookingSucess(sessionId, "FlightNot
				// Book",
				// String.valueOf(responseDTO.getAmount()), "Flight Not Book",
				// responseDTO.getTransactionId(),
				// "ddd", false);
				session.setAttribute("msg",
						"Transasction Successful,Your Flight Is Not Booked.Please Contact Your Customer Care");

			}
		} else {
			session.setAttribute("msg", "Transaction failed, Please try again later.");
		}
		model.addAttribute("response", responseDTO);
		return "User/Loading";

	}

	@RequestMapping(value = "/Flight/ReturnProcess", method = RequestMethod.POST)
	public String processReturnRedirectddd(@ModelAttribute LoadMoneyFlightRequest dto, HttpServletRequest request,
			ModelMap modelMap, HttpSession session) {
		String sessionId = (String) session.getAttribute("sessionId");
		request.setAttribute("sessionId", sessionId);

		ReturnFlightBookRequest req = new ReturnFlightBookRequest();
		LoadMoneyResponse loadMoneyResponse = new LoadMoneyResponse();
		request.getSession().setAttribute("sessionId", sessionId);
		dto.setSessionId(sessionId);
		if (dto.getUseVnetWeb().equalsIgnoreCase("vnet")) {
			System.err.println("inside vnet");
			VNetRequest vNetRequest = new VNetRequest();
			vNetRequest.setSessionId(sessionId);
			vNetRequest.setReturnURL(UrlMetadatas.WEBURL + "/User/Travel/Flight/Return/VRedirect");
			vNetRequest.setAmount(dto.getAmount());
			VNetResponse vNetResponse = loadMoneyApi.initiateVnetBanking(vNetRequest);
			if (vNetResponse.isSuccess()) {
		//		req.setTransactionId("" + loadMoneyResponse.getReferenceNo());
				req.setFirstName(dto.getFirstName());
				req.setFirstNamechild(dto.getFirstNamechild());
				req.setFirstNameinfant(dto.getFirstNameinfant());

				req.setLastName(dto.getLastName());
				req.setLastNamechild(dto.getLastNamechild());
				req.setLastNameinfant(dto.getLastNameinfant());
				req.setUseradultsgender(dto.getUseradultsgender());
				req.setUserchildsgender(dto.getUserchildsgender());
				req.setUserinfantsgender(dto.getUserinfantsgender());
				req.setEmailAddress(dto.getEmail());
				req.setMobileNumber(dto.getPhone());
				req.setGrandtotal(req.getAmount());
				req.setAdults("" + session.getAttribute("adults"));
				req.setChilds("" + session.getAttribute("childs"));
				req.setInfants("" + session.getAttribute("infants"));
				String source = "" + session.getAttribute("airRePriceRQoneway");
				String destination = "" + session.getAttribute("airRePriceRQroundway");
				String airRePriceRQ = travelflightapi.AirBookRQreturnwithPaymentGatway(req, source, destination,
						session);

				session.setAttribute("FlightBookWithPaymentGateway", airRePriceRQ);
				modelMap.addAttribute("vnet", vNetResponse);
				return "User/LoadMoney/VNetPay";
			}
		} else if (dto.getUseVnetWeb().equals("others")) {
			dto.setReturnUrl(UrlMetadatas.WEBURL + "/User/Travel/Flight/ReturnRedirect");
			loadMoneyResponse = loadMoneyApi.SplitpaymentMoneyRequest(dto);
			if (loadMoneyResponse.isSuccess()) {
			//	req.setTransactionId("" + loadMoneyResponse.getReferenceNo());
				req.setFirstName(dto.getFirstName());
				req.setFirstNamechild(dto.getFirstNamechild());
				req.setFirstNameinfant(dto.getFirstNameinfant());

				req.setLastName(dto.getLastName());
				req.setLastNamechild(dto.getLastNamechild());
				req.setLastNameinfant(dto.getLastNameinfant());

				req.setEmailAddress(dto.getEmail());
				req.setMobileNumber(dto.getPhone());
				req.setGrandtotal(req.getAmount());
				req.setUseradultsgender(dto.getUseradultsgender());
				req.setUserchildsgender(dto.getUserchildsgender());
				req.setUserinfantsgender(dto.getUserinfantsgender());
				req.setEmailAddress(dto.getEmail());
				req.setMobileNumber(dto.getPhone());
				req.setGrandtotal(dto.getAmount());
				req.setAdults("" + session.getAttribute("adults"));
				req.setChilds("" + session.getAttribute("childs"));
				req.setInfants("" + session.getAttribute("infants"));
				String source = "" + session.getAttribute("airRePriceRQoneway");
				String destination = "" + session.getAttribute("airRePriceRQroundway");
				String airRePriceRQ = travelflightapi.AirBookRQreturnwithPaymentGatway(req, source, destination,
						session);

				session.setAttribute("FlightBookWithPaymentGatewayReturn", airRePriceRQ);
				modelMap.addAttribute("loadmoney", loadMoneyResponse);
				return "User/Pay";
			} else {
				/*System.err.format("Load money response is %s\n", loadMoneyResponse.getDescription());
				modelMap.addAttribute(ModelMapKey.MESSAGE, loadMoneyResponse.getDescription());*/
				return "User/Home";
			}
		}
		return "redirect:/User/Home";
	}

	// ----------------------------------------International Roundway
	// Start-------------------------------------------------------

	@RequestMapping(value = "/Flight/InternationalReturnProcess", method = RequestMethod.POST)
	public String processReturnRedirectdddInternational(@ModelAttribute LoadMoneyFlightRequest dto,
			HttpServletRequest request, ModelMap modelMap, HttpSession session) throws Exception {
		String sessionId = (String) session.getAttribute("sessionId");
		request.setAttribute("sessionId", sessionId);

		ReturnFlightBookRequest req = new ReturnFlightBookRequest();
		LoadMoneyResponse loadMoneyResponse = new LoadMoneyResponse();
		request.getSession().setAttribute("sessionId", sessionId);
		dto.setSessionId(sessionId);
		if (dto.getUseVnetWeb().equalsIgnoreCase("vnet")) {
			System.err.println("inside vnet");
			VNetRequest vNetRequest = new VNetRequest();
			vNetRequest.setSessionId(sessionId);
			vNetRequest.setReturnURL(UrlMetadatas.WEBURL + "/User/Travel/Flight/Return/VRedirect");
			vNetRequest.setAmount(dto.getAmount());
			VNetResponse vNetResponse = loadMoneyApi.initiateVnetBanking(vNetRequest);
			if (vNetResponse.isSuccess()) {
		//		req.setTransactionId("" + loadMoneyResponse.getReferenceNo());
				req.setFirstName(dto.getFirstName());
				req.setFirstNamechild(dto.getFirstNamechild());
				req.setFirstNameinfant(dto.getFirstNameinfant());

				req.setLastName(dto.getLastName());
				req.setLastNamechild(dto.getLastNamechild());
				req.setLastNameinfant(dto.getLastNameinfant());

				req.setEmailAddress(dto.getEmail());
				req.setMobileNumber(dto.getPhone());
				req.setGrandtotal(req.getAmount());
				req.setUseradultsgender(dto.getUseradultsgender());
				req.setUserchildsgender(dto.getUserchildsgender());
				req.setUserinfantsgender(dto.getUserinfantsgender());
				req.setTransactionId("" + System.currentTimeMillis());
				req.setAdults("" + session.getAttribute("adults"));
				req.setChilds("" + session.getAttribute("childs"));
				req.setInfants("" + session.getAttribute("infants"));

				org.json.JSONObject objmain1 = CreateJsonRequestFlight.createjsonBookApiinternationalFlightBook(req,
						session);

				session.setAttribute("FlightBookWithPaymentGateway", objmain1);
				modelMap.addAttribute("vnet", vNetResponse);
				return "User/LoadMoney/VNetPay";
			}
		} else if (dto.getUseVnetWeb().equals("others")) {
			dto.setReturnUrl(UrlMetadatas.WEBURL + "/User/Travel/Flight/ReturnRedirect");
			loadMoneyResponse = loadMoneyApi.SplitpaymentMoneyRequest(dto);
			if (loadMoneyResponse.isSuccess()) {
		//		req.setTransactionId("" + loadMoneyResponse.getReferenceNo());
				req.setFirstName(dto.getFirstName());
				req.setFirstNamechild(dto.getFirstNamechild());
				req.setFirstNameinfant(dto.getFirstNameinfant());

				req.setLastName(dto.getLastName());
				req.setLastNamechild(dto.getLastNamechild());
				req.setLastNameinfant(dto.getLastNameinfant());

				req.setEmailAddress(dto.getEmail());
				req.setMobileNumber(dto.getPhone());
				req.setGrandtotal(req.getAmount());

				req.setEmailAddress(dto.getEmail());
				req.setMobileNumber(dto.getPhone());
				req.setGrandtotal(dto.getAmount());
				req.setUseradultsgender(dto.getUseradultsgender());
				req.setUserchildsgender(dto.getUserchildsgender());
				req.setUserinfantsgender(dto.getUserinfantsgender());
				req.setAdults("" + session.getAttribute("adults"));
				req.setChilds("" + session.getAttribute("childs"));
				req.setInfants("" + session.getAttribute("infants"));
				org.json.JSONObject objmain1 = CreateJsonRequestFlight.createjsonBookApiinternationalFlightBook(req,
						session);

				session.setAttribute("FlightBookWithPaymentGatewayReturn", objmain1);
				modelMap.addAttribute("loadmoney", loadMoneyResponse);
				return "User/Pay";
			} else {
				/*System.err.format("Load money response is %s\n", loadMoneyResponse.getDescription());
				modelMap.addAttribute(ModelMapKey.MESSAGE, loadMoneyResponse.getDescription());*/
				return "User/Home";
			}
		}
		return "redirect:/User/Home";
	}

	@RequestMapping(value = "/Flight/ReturnProcessSplitpaymentInternational", method = RequestMethod.POST)
	public String ReturnProcessSplitpaymentInternational(@ModelAttribute LoadMoneyFlightRequest dto,
			HttpServletRequest request, ModelMap modelMap, HttpSession session) throws Exception {
		String sessionId = (String) session.getAttribute("sessionId");
		request.setAttribute("sessionId", sessionId);

		ReturnFlightBookRequest req = new ReturnFlightBookRequest();
		LoadMoneyResponse loadMoneyResponse = new LoadMoneyResponse();
		// LoadMoneyError error = loadMoneyValidation.checkError(dto);
		// if (error.isValid()) {
		// if (sessionId != null && sessionId.length() != 0) {
		// String authority =
		// authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
		// if (authority != null) {
		// if (authority.contains(Authorities.USER) &&
		// authority.contains(Authorities.AUTHENTICATED)) {
		request.getSession().setAttribute("sessionId", sessionId);
		dto.setSessionId(sessionId);
		dto.setAmount(dto.getReturnamount());
		dto.setName("ddddd");
		dto.setReturnUrl(UrlMetadatas.WEBURL + "/User/Travel/Flight/ReturnRedirectSplitpayment");
		loadMoneyResponse = loadMoneyApi.LoadMoneyFlightRequest(dto);
		if (loadMoneyResponse.isSuccess()) {
		//	req.setTransactionId("" + loadMoneyResponse.getReferenceNo());
			req.setFirstName(dto.getFirstName());
			req.setFirstNamechild(dto.getFirstNamechild());
			req.setFirstNameinfant(dto.getFirstNameinfant());

			req.setLastName(dto.getLastName());
			req.setLastNamechild(dto.getLastNamechild());
			req.setLastNameinfant(dto.getLastNameinfant());

			req.setEmailAddress(dto.getEmail());
			req.setMobileNumber(dto.getPhone());
			req.setGrandtotal(req.getAmount());

			req.setEmailAddress(dto.getEmail());
			req.setMobileNumber(dto.getPhone());
			req.setGrandtotal(dto.getAmount());
			req.setUseradultsgender(dto.getUseradultsgender());
			req.setUserchildsgender(dto.getUserchildsgender());
			req.setUserinfantsgender(dto.getUserinfantsgender());
			req.setAdults("" + session.getAttribute("adults"));
			req.setChilds("" + session.getAttribute("childs"));
			req.setInfants("" + session.getAttribute("infants"));
			org.json.JSONObject objmain1 = CreateJsonRequestFlight.createjsonBookApiinternationalFlightBook(req,
					session);
			System.err.println(objmain1);
			session.setAttribute("FlightBookWithSplitPaymentReturn", objmain1);
			modelMap.addAttribute("loadmoney", loadMoneyResponse);
			return "User/Pay";
		} else {
			/*System.err.format("Load money response is %s\n", loadMoneyResponse.getDescription());
			modelMap.addAttribute(ModelMapKey.MESSAGE, loadMoneyResponse.getDescription());*/
			return "User/Home";
		}

		// }
		// }
		// }
		// } else {
		// modelMap.put(ModelMapKey.ERROR, error);
		// return "User/Home";
		// }
		// return "redirect:/User/Home";
	}

	// ----------------------------------------International Roundway
	// Finish-------------------------------------------------------

	@RequestMapping(value = "/Flight/ReturnRedirect", method = RequestMethod.POST)
	public String redirectReturnFlight(WEBSRedirectResponse ebsResponse, Model model, HttpSession session)
			throws Exception {
		String mobile = "" + session.getAttribute("mobile");
		String email = "" + session.getAttribute("email");
		
		String ticketDetails = "" + session.getAttribute("ticketDetails");
		session.setAttribute("msgvalue", "1");
		EBSRedirectResponse ebsRedirectResponse = ConvertUtil.convertFromWEBS(ebsResponse);
		String sessionId = (String) session.getAttribute("sessionId");
		ResponseDTO result = loadMoneyApi.verifyEBSTransaction(ebsRedirectResponse);
		if (result.isSuccess()) {
			ebsRedirectResponse.setSuccess(true);
			ebsRedirectResponse.setResponseCode("0");
		} else {
			ebsRedirectResponse.setSuccess(false);
			ebsRedirectResponse.setResponseCode("1");
		}
		EBSRedirectResponse redirectResponse = loadMoneyApi.processRedirectSDKSplitpaymentMoney(ebsRedirectResponse);
		if (redirectResponse.isSuccess() || redirectResponse.getResponseCode().equals("0")) {

			String jsonflightRequest = "" + session.getAttribute("FlightBookWithPaymentGatewayReturn");
			String airRePriceRQ = travelflightapi.FlightBookPaymentGatway(jsonflightRequest);
			JSONObject obj = new JSONObject(airRePriceRQ);
			String code = obj.getString("code");
			if (code.equalsIgnoreCase("S00")) {
				String bookingRefId = obj.getJSONObject("details").getString("bookingRefId");
				String transactionRefNomdex = obj.getJSONObject("details").getString("transactionRefNo");
				JSONArray objcc = obj.getJSONObject("details").getJSONObject("bookingDetail").getJSONObject("pnrDetail")
						.getJSONArray("tickets");
				String ticketNumber = "";
				String firstName = "";
				JSONArray objccpnrs = obj.getJSONObject("details").getJSONObject("bookingDetail")
						.getJSONObject("pnrDetail").getJSONArray("pnrs");
				String pnrNo = objccpnrs.getJSONObject(0).getString("pnr");

				for (int i = 0; i < objcc.length(); i++) {
					ticketNumber += "~" + objcc.getJSONObject(i).getString("ticketNumber");

					firstName += "~" + objcc.getJSONObject(i).getString("firstName");
				}

				String flightBookingSucess = travelflightapi.FlightPaymentGatewaySucess(sessionId, ticketNumber,
						firstName, bookingRefId, transactionRefNomdex, ebsRedirectResponse.getAmount(), ticketDetails,
						"Booked", "Success", "Pay with ebs payemnt gateway", ebsRedirectResponse.getMerchantRefNo(),
						true,email,mobile);

				session.setAttribute("msg", "Transasction Successful.Your Flight Is Booked.");

			} else {
				String bookingRefId = "sss";
				String firstName = "" + session.getAttribute("firstName");
				travelflightapi.FlightPaymentGatewaySucess(sessionId, "Flight is Not Book",
						"" + session.getAttribute("firstName"), "Flight is Not Book", "Flight is Not Book",
						ebsRedirectResponse.getAmount(), ticketDetails, "Processing", "Success",
						"Pay with ebs payment gateway", ebsRedirectResponse.getMerchantRefNo(), false,email,mobile);
				session.setAttribute("msg",
						"Transasction Successful,Your Flight Is Not Booked.Please Contact Your Customer Care");

			}

		} else {
			session.setAttribute("msg", "Transaction failed, Please try again later.");
			travelflightapi.FlightPaymentGatewaySucess(sessionId, "Flight is Not Book",
					"" + session.getAttribute("firstName"), "Flight is Not Book", "Flight is Not Book",
					ebsRedirectResponse.getAmount(), ticketDetails, "Processing", "Initiated",
					"Pay with ebs payment gateway", ebsRedirectResponse.getMerchantRefNo(), false,email,mobile);
		}
		model.addAttribute("loadmoneyResponse", redirectResponse);
		return "User/Loading";

	}

	@RequestMapping(value = "/Flight/Return/VRedirect", method = RequestMethod.POST)
	public String returnRedirectVNetLoadMoney(VRedirectResponse dto, Model model, HttpSession session)
			throws JSONException {
		String sessionId = (String) session.getAttribute("sessionId");
		ResponseDTO responseDTO = loadMoneyApi.handleRedirectRequestFlight(dto);
		if (responseDTO.getCode().equalsIgnoreCase("S00")) {
			String jsonflightRequest = "" + session.getAttribute("FlightBookWithPaymentGateway");
			String origin = "" + session.getAttribute("source");
			String destination = "" + session.getAttribute("destination");
			String airRePriceRQ = travelflightapi.FlightBookPaymentGatway(jsonflightRequest);
			JSONObject obj = new JSONObject(airRePriceRQ);
			String code = obj.getString("code");
			if (code.equalsIgnoreCase("S00")) {
				String bookingRefId = obj.getJSONObject("details").getString("bookingRefId");
				JSONArray objcc = obj.getJSONObject("details").getJSONObject("bookingDetail").getJSONObject("pnrDetail")
						.getJSONArray("tickets");
				String ticketNumber = "";
				for (int i = 0; i < objcc.length(); i++) {
					ticketNumber += "|" + objcc.getJSONObject(i).getString("ticketNumber");
				}
				String firstName = "";
				for (int i = 0; i < objcc.length(); i++) {
					firstName += "|" + objcc.getJSONObject(i).getString("firstName");
				}
				JSONArray objccpnrs = obj.getJSONObject("details").getJSONObject("bookingDetail")
						.getJSONObject("pnrDetail").getJSONArray("pnrs");
				String pnrNo = objccpnrs.getJSONObject(0).getString("pnr");
				// String flightBookingSucess =
				// travelflightapi.FlightPaymentGatewaySucess(sessionId,
				// bookingRefId,
				// String.valueOf(responseDTO.getAmount()), ticketNumber,
				// responseDTO.getTransactionId(), pnrNo,
				// origin, destination, firstName, true);
				session.setAttribute("msg", "Transasction Successful.Your Flight Is Booked.");
			} else {
				String bookingRefId = obj.getString("bookingRefId");
				String firstName = "" + session.getAttribute("firstName");
				// String flightBookingSucess =
				// travelflightapi.FlightPaymentGatewaySucess(sessionId,
				// bookingRefId,
				// String.valueOf(responseDTO.getAmount()), "Flight Not Book",
				// responseDTO.getTransactionId(),
				// "ddd", origin, destination, firstName, false);
				session.setAttribute("msg",
						"Transasction Successful,Your Flight Is Not Booked.Please Contact Your Customer Care");

			}
		} else {
			session.setAttribute("msg", "Transaction failed, Please try again later.");
		}
		model.addAttribute("response", responseDTO);
		return "User/Loading";

	}

	// Splitpayment-----------------------------------Payment gateway Deduction
	// Through Splitpayment-----------------------------------------------
	@RequestMapping(value = "/Flight/ProcessSplitpayment", method = RequestMethod.POST)
	public String processLoadMoneySplitpayment(@ModelAttribute LoadMoneyFlightRequest dto, HttpServletRequest request,
			ModelMap modelMap, HttpSession session) {
		String sessionId = (String) session.getAttribute("sessionId");
		request.setAttribute("sessionId", sessionId);

		FlightBookRequest req = new FlightBookRequest();
		LoadMoneyResponse loadMoneyResponse = new LoadMoneyResponse();
		// LoadMoneyError error = loadMoneyValidation.checkError(dto);
		// if (error.isValid()) {
		// if (sessionId != null && sessionId.length() != 0) {
		// String authority =
		// authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
		// if (authority != null) {
		// if (authority.contains(Authorities.USER) &&
		// authority.contains(Authorities.AUTHENTICATED)) {
		dto.setName("dssdfsdfsdfds");
		request.getSession().setAttribute("sessionId", sessionId);
		dto.setSessionId(sessionId);
		dto.setAmount(dto.getReturnamount());
		dto.setReturnUrl(UrlMetadatas.WEBURL + "/User/Travel/Flight/RedirectSplitpayment");
		loadMoneyResponse = loadMoneyApi.LoadMoneyFlightRequest(dto);
		if (loadMoneyResponse.isSuccess()) {
		//	req.setTransactionId("" + loadMoneyResponse.getReferenceNo());

			req.setEmailAddress(dto.getEmail());
			req.setMobileNumber(dto.getPhone());
			req.setGrandtotal(req.getAmount());
			req.setLastName("ddd");
			req.setUseradultsgender(dto.getUseradultsgender());
			req.setUserchildsgender(dto.getUserchildsgender());
			req.setUserinfantsgender(dto.getUserinfantsgender());
			req.setAdults("" + session.getAttribute("adults"));
			req.setChilds("" + session.getAttribute("childs"));
			req.setInfants("" + session.getAttribute("infants"));
			String source = "" + session.getAttribute("airRePriceRQ");
			String airRePriceRQ = travelflightapi.AirBookRQwithPaymentGatway(req, source, session);

			session.setAttribute("FlightBookWithPaymentGateway", airRePriceRQ);
			modelMap.addAttribute("loadmoney", loadMoneyResponse);
			return "User/Pay";
		} else {
			/*System.err.format("Load money response is %s\n", loadMoneyResponse.getDescription());
			modelMap.addAttribute(ModelMapKey.MESSAGE, loadMoneyResponse.getDescription());*/
			return "User/Home";
		}
	}

	@RequestMapping(value = "/Flight/ReturnProcessSplitpayment", method = RequestMethod.POST)
	public String processReturnRedirectSplitpayment(@ModelAttribute LoadMoneyFlightRequest dto,
			HttpServletRequest request, ModelMap modelMap, HttpSession session) {
		String sessionId = (String) session.getAttribute("sessionId");
		request.setAttribute("sessionId", sessionId);

		ReturnFlightBookRequest req = new ReturnFlightBookRequest();
		LoadMoneyResponse loadMoneyResponse = new LoadMoneyResponse();
		// LoadMoneyError error = loadMoneyValidation.checkError(dto);
		// if (error.isValid()) {
		// if (sessionId != null && sessionId.length() != 0) {
		// String authority =
		// authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
		// if (authority != null) {
		// if (authority.contains(Authorities.USER) &&
		// authority.contains(Authorities.AUTHENTICATED)) {
		request.getSession().setAttribute("sessionId", sessionId);
		dto.setSessionId(sessionId);
		dto.setAmount(dto.getReturnamount());
		dto.setName("ddddd");
		dto.setReturnUrl(UrlMetadatas.WEBURL + "/User/Travel/Flight/ReturnRedirectSplitpayment");
		loadMoneyResponse = loadMoneyApi.LoadMoneyFlightRequest(dto);
		if (loadMoneyResponse.isSuccess()) {
		//	req.setTransactionId("" + loadMoneyResponse.getReferenceNo());
			req.setFirstName(dto.getName());
			req.setEmailAddress(dto.getEmail());
			req.setMobileNumber(dto.getPhone());
			req.setGrandtotal(dto.getAmount());
			req.setUseradultsgender(dto.getUseradultsgender());
			req.setUserchildsgender(dto.getUserchildsgender());
			req.setUserinfantsgender(dto.getUserinfantsgender());
			req.setAdults("" + session.getAttribute("adults"));
			req.setChilds("" + session.getAttribute("childs"));
			req.setInfants("" + session.getAttribute("infants"));
			String source = "" + session.getAttribute("airRePriceRQoneway");
			String destination = "" + session.getAttribute("airRePriceRQroundway");
			String airRePriceRQ = travelflightapi.AirBookRQreturnwithPaymentGatway(req, source, destination, session);

			session.setAttribute("FlightBookWithPaymentGatewayReturn", airRePriceRQ);
			modelMap.addAttribute("loadmoney", loadMoneyResponse);
			return "User/Pay";
		} else {
			/*System.err.format("Load money response is %s\n", loadMoneyResponse.getDescription());
			modelMap.addAttribute(ModelMapKey.MESSAGE, loadMoneyResponse.getDescription());*/
			return "User/Home";
		}

		// }
		// }
		// }
		// } else {
		// modelMap.put(ModelMapKey.ERROR, error);
		// return "User/Home";
		// }
		// return "redirect:/User/Home";
	}

	@RequestMapping(value = "/Flight/RedirectSplitpayment", method = RequestMethod.POST)
	public String redirectLoadMoneySplitpayment(WEBSRedirectResponse ebsResponse, Model model, HttpSession session)
			throws Exception {
		String ticketDetails = "" + session.getAttribute("ticketDetails");

		String mobile = "" + session.getAttribute("mobile");
		String email = "" + session.getAttribute("email");

		String firstName1 = "~" + session.getAttribute("firstName");
		session.setAttribute("msgvalue", "1");
		EBSRedirectResponse ebsRedirectResponse = ConvertUtil.convertFromWEBS(ebsResponse);
		String sessionId = (String) session.getAttribute("sessionId");
		ResponseDTO result = loadMoneyApi.verifyEBSTransaction(ebsRedirectResponse);
		if (result.isSuccess()) {
			ebsRedirectResponse.setSuccess(true);
			ebsRedirectResponse.setResponseCode("0");
		} else {
			ebsRedirectResponse.setSuccess(false);
			ebsRedirectResponse.setResponseCode("1");
		}
		EBSRedirectResponse redirectResponse = loadMoneyApi.processRedirectSDK(ebsRedirectResponse);
		if (redirectResponse.isSuccess() || redirectResponse.getResponseCode().equals("0")) {

			String bookingAmt = (String) session.getAttribute("splitFlightBookAmt");

			String flightBookingInitiate = travelflightapi.FlightBookingInitiate(sessionId, firstName1, bookingAmt,
					ticketDetails, "Wallet");

			JSONObject objflightBookingInitiate = new JSONObject(flightBookingInitiate);
			String codeflightBookingInitiate = objflightBookingInitiate.getString("code");
			if (codeflightBookingInitiate.equalsIgnoreCase("S00")) // (true)//
			{
				String transactionRefNo = objflightBookingInitiate.getJSONObject("details")
						.getString("transactionRefNo");
				String jsonflightRequest = "" + session.getAttribute("FlightBookWithSplitPayment");

				System.err.println("Json is:: " + jsonflightRequest);

				String airRePriceRQ = travelflightapi.FlightBookPaymentGatway(jsonflightRequest);
				JSONObject obj = new JSONObject(airRePriceRQ);
				String code = obj.getString("code");
				if (code.equalsIgnoreCase("S00")) {
					String bookingRefId = obj.getJSONObject("details").getString("bookingRefId");
					String transactionRefNomdex = obj.getJSONObject("details").getString("transactionRefNo");
					JSONArray objcc = obj.getJSONObject("details").getJSONObject("bookingDetail")
							.getJSONObject("pnrDetail").getJSONArray("tickets");
					String ticketNumber = "";
					String firstName = "";

					for (int i = 0; i < objcc.length(); i++) {
						ticketNumber += "~" + objcc.getJSONObject(i).getString("ticketNumber");
						firstName += "~" + objcc.getJSONObject(i).getString("firstName");
					}

					String flightBookingSucess = travelflightapi.FlightBookingSucess(sessionId, ticketNumber, firstName,
							bookingRefId, transactionRefNomdex, ebsRedirectResponse.getAmount(), ticketDetails,
							"Booked", "Success", "Wallet", transactionRefNo, true, email, mobile);
					JSONObject flightBookingSucessobj = new JSONObject(flightBookingSucess);
					String flightBookingSucesscode = flightBookingSucessobj.getString("code");
					if (flightBookingSucesscode.equalsIgnoreCase("S00")) {
						session.setAttribute("msg", "Transasction Successful.Your Flight Is Booked.");
					} else {
						travelflightapi.FlightBookingSucess(sessionId, ticketNumber, firstName, bookingRefId,
								transactionRefNomdex, ebsRedirectResponse.getAmount(), ticketDetails, "Booked",
								"Initiated", "Wallet", transactionRefNo, false, email, mobile);
						session.setAttribute("msg",
								"Transasction Not Successful,Your Flight Is Not Booked.Please Contact Your Customer Care");

					}

				} else {
					travelflightapi.FlightBookingSucess(sessionId, "Flight Not booked", firstName1, "Flight Not booked",
							"Flight Not booked", ebsRedirectResponse.getAmount(), ticketDetails, "Processing",
							"Initiated", "Wallet", transactionRefNo, false, email, mobile);
					session.setAttribute("msg", "Transasction Not Successful.Please Contact Your Customer Care");

				}

			} else {
				travelflightapi.FlightBookingSucess(sessionId, "Flight Not booked", firstName1, "Flight Not booked",
						"Flight Not booked", ebsRedirectResponse.getAmount(), ticketDetails, "Processing", "Initiated",
						"Wallet", "Flight Not booked", false, email, mobile);
				session.setAttribute("msg",
						"Transasction Successful,Your Flight Is Not Booked.Please Contact Your Customer Care");
				session.setAttribute("msg", "Transaction failed, Please try again later.");
			}

		} else {
			session.setAttribute("msg", "Transaction failed, Please try again later.");
		}
		model.addAttribute("loadmoneyResponse", redirectResponse);
		return "User/Loading";

	}

	@RequestMapping(value = "/Flight/ReturnRedirectSplitpayment", method = RequestMethod.POST)
	public String redirectReturnFlightSplitpayment(WEBSRedirectResponse ebsResponse, Model model, HttpSession session)
			throws Exception {
		String ticketDetails = "" + session.getAttribute("ticketDetails");
		String mobile = "" + session.getAttribute("mobile");
		String email = "" + session.getAttribute("email");

		String firstName1 = "~" + session.getAttribute("firstName");
		session.setAttribute("msgvalue", "1");
		EBSRedirectResponse ebsRedirectResponse = ConvertUtil.convertFromWEBS(ebsResponse);
		String sessionId = (String) session.getAttribute("sessionId");
		ResponseDTO result = loadMoneyApi.verifyEBSTransaction(ebsRedirectResponse);
		if (result.isSuccess()) {
			ebsRedirectResponse.setSuccess(true);
			ebsRedirectResponse.setResponseCode("0");
		} else {
			ebsRedirectResponse.setSuccess(false);
			ebsRedirectResponse.setResponseCode("1");
		}
		EBSRedirectResponse redirectResponse = loadMoneyApi.processRedirectSDK(ebsRedirectResponse);
		if (redirectResponse.isSuccess() || redirectResponse.getResponseCode().equals("0")) {
			String bookAmt = (String) session.getAttribute("splitRountFlightAmt");

			String flightBookingInitiate = travelflightapi.FlightBookingInitiate(sessionId, "dddd", bookAmt,
					ticketDetails, "Wallet");

			JSONObject objflightBookingInitiate = new JSONObject(flightBookingInitiate);
			String codeflightBookingInitiate = objflightBookingInitiate.getString("code");
			if (codeflightBookingInitiate.equalsIgnoreCase("S00")) // (true)//
			{

				String transactionRefNo = objflightBookingInitiate.getJSONObject("details")
						.getString("transactionRefNo");

				String jsonflightRequest = "" + session.getAttribute("FlightBookWithSplitPaymentReturn");
				String airRePriceRQ = travelflightapi.FlightBookPaymentGatway(jsonflightRequest);
				JSONObject obj = new JSONObject(airRePriceRQ);
				String code = obj.getString("code");
				if (code.equalsIgnoreCase("S00")) {
					String bookingRefId = obj.getJSONObject("details").getString("bookingRefId");
					String transactionRefNomdex = obj.getJSONObject("details").getString("transactionRefNo");
					JSONArray objcc = obj.getJSONObject("details").getJSONObject("bookingDetail")
							.getJSONObject("pnrDetail").getJSONArray("tickets");
					String ticketNumber = "";
					String firstName = "";

					for (int i = 0; i < objcc.length(); i++) {
						ticketNumber += "~" + objcc.getJSONObject(i).getString("ticketNumber");
						firstName += "~" + objcc.getJSONObject(i).getString("firstName");
					}
					travelflightapi.FlightBookingSucess(sessionId, ticketNumber, firstName, bookingRefId,
							transactionRefNomdex, ebsRedirectResponse.getAmount(), ticketDetails, "Booked", "Success",
							"Wallet", transactionRefNo, true, email, mobile);

					session.setAttribute("msg", "Transasction Successful.Your Flight Is Booked.");
				} else {
					travelflightapi.FlightBookingSucess(sessionId, "Flight Not booked", firstName1, "Flight Not booked",
							"Flight Not booked", ebsRedirectResponse.getAmount(), ticketDetails, "Processing",
							"Initiated", "Wallet", transactionRefNo, false, email, mobile);

					session.setAttribute("error",
							"Transasction Successful,Your Flight Is Not Booked.Please Contact Your Customer Care");

				}

			} else {
				travelflightapi.FlightBookingSucess(sessionId, "Flight Not booked", firstName1, "Flight Not booked",
						"Flight Not booked", ebsRedirectResponse.getAmount(), ticketDetails, "Processing", "Initiated",
						"Wallet", "Flight Not booked", false, email, mobile);

				session.setAttribute("msg", "Transaction failed, Please try 111again later.");
			}

		} else {
			travelflightapi.FlightBookingSucess(sessionId, "Flight Not booked", firstName1, "Flight Not booked",
					"Flight Not booked", ebsRedirectResponse.getAmount(), ticketDetails, "Processing", "Initiated",
					"Wallet", "Flight Not booked", false, email, mobile);

			session.setAttribute("msg", "Transaction failed, Please try againfhgf later.");
		}
		model.addAttribute("loadmoneyResponse", redirectResponse);
		return "User/Loading";

	}

	
	
	
	
	
		/* Code Done By Rohit End */
	
	/*##############################################################################################################*/
	
	
	/*@RequestMapping(method = RequestMethod.POST, value = "/Flight/AirLineNames", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<FlightResponseDTO> source(HttpSession session) {
		
		FlightResponseDTO resp=new FlightResponseDTO();
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
					try {
						
						resp= travelflightapi.getAirLineNames();
						return new ResponseEntity<FlightResponseDTO>(resp, HttpStatus.OK);
						
					} catch (Exception e) {
						System.out.println(e);
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
						return new ResponseEntity<FlightResponseDTO>(resp, HttpStatus.OK);
					}
				}
				else {
					resp.setCode("F00");
					resp.setMessage("Unauthorised access");
					resp.setStatus("FAILED");
					return new ResponseEntity<FlightResponseDTO>(resp, HttpStatus.OK);
				}
			}else {
				resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
				resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
				resp.setMessage("Session expired");
				return new ResponseEntity<FlightResponseDTO>(resp, HttpStatus.OK);
			}

		}else {
			resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
			resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
			resp.setMessage("Session expired");
			return new ResponseEntity<FlightResponseDTO>(resp, HttpStatus.OK);
		}
	}*/
	
	
}
