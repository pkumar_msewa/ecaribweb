package com.payqwikweb.controller.web;

import org.json.JSONException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.payqwikweb.app.api.IVersionApi;
import com.payqwikweb.app.model.request.Utility;
import com.payqwikweb.app.model.request.VersionRequest;
import com.payqwikweb.app.model.response.VersionResponse;

@Controller
@RequestMapping("/Version")
public class VersionController {

	private final IVersionApi versionApi;

	public VersionController(IVersionApi versionApi){
			this.versionApi = versionApi;
	}

    @RequestMapping(value = {"/Check","/Validate"}, method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<VersionResponse> checkAppVersion(@RequestBody Utility dto) throws JSONException {
		VersionResponse result = versionApi.validateVersion(dto);
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@RequestMapping(value = {"/Update","/Increment"}, method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<VersionResponse> checkAppVersion(@RequestBody VersionRequest dto) throws JSONException {
		VersionResponse result = versionApi.updateVersion(dto);
		return new ResponseEntity<>(result, HttpStatus.OK);
	}


}
