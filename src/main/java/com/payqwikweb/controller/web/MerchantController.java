package com.payqwikweb.controller.web;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.payqwik.visa.util.VisaMerchantRequest;
import com.payqwik.visa.util.VisaMerchantTransaction;
import com.payqwik.visa.util.VisaModel;
import com.payqwikweb.api.IAuthenticationApi;
import com.payqwikweb.app.api.IAdminApi;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.request.MerchantTransactionRequest;
import com.payqwikweb.app.model.request.VisaMerchantBalanceRequest;
import com.payqwikweb.app.model.response.TransactionReportResponse;
import com.payqwikweb.util.Authorities;
import com.payqwikweb.util.ModelMapKey;
import com.payqwikweb.validation.RegisterValidation;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

@RequestMapping(value="/Merchant")
public class MerchantController {

	private final IAdminApi appAdminApi;
	private final IAuthenticationApi authenticationApi;
	private final RegisterValidation registerValidation;
	
	
	public MerchantController(IAdminApi appAdminApi, IAuthenticationApi authenticationApi,
			RegisterValidation registerValidation) {
		super();
		this.appAdminApi = appAdminApi;
		this.authenticationApi = authenticationApi;
		this.registerValidation = registerValidation;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/VisaMerchantBalance")
	public String visaMerchantBalance(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) throws JSONException {
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			Client cl = Client.create();
			WebResource webResource = cl
					.resource("http://54.254.241.95:8080/Yappay/business-entity-manager/fetchbalance/60392");
			ClientResponse rsp = webResource.header("Authorization", "Basic YWRtaW46YWRtaW4=")
					.header("Content-Type", "application/json").header("TENANT", "M2P").get(ClientResponse.class);
			if (rsp.getStatus() != 200) {
				model.addAttribute("data", null);
			} else {
				JSONArray data = null;
				String strResponse = rsp.getEntity(String.class);
				ArrayList<VisaMerchantBalanceRequest> req = new ArrayList<VisaMerchantBalanceRequest>();
				if (strResponse != null) {
					org.json.JSONObject jobj = new org.json.JSONObject(strResponse);
					if (jobj != null) {
						data = jobj.getJSONArray("result");
						for (int i = 0; i < data.length(); i++) {
							VisaMerchantBalanceRequest re = new VisaMerchantBalanceRequest();
							JSONObject ob = new JSONObject();
							ob = (JSONObject) data.get(i);
							re.setBalance(ob.getString("balance"));
							re.setEntityId(ob.getString("entityId"));
							re.setProductId(ob.getString("productId"));
							re.setYseId(ob.getString("yseId"));
							req.add(re);
						}
					}
				}
				model.addAttribute("data", req);
			}
			return "/Admin/VisaMerchantBalance";
		}
		return "/Admin/Login";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/AddVisaMerchant")
	public String addVisaMerchant(@ModelAttribute("addVisaMerchant") VisaMerchantRequest merchant,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model) {
		String sessionId = (String) session.getAttribute("adminSessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					//VisaMerchantRequest merchantRequest = new VisaMerchantRequest();
					//model.addAttribute("addMerchant", merchantRequest);
					return "Admin/AddVisaMerchant";
				}
			}
		}
		return "Admin/Login";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/VisaMerchantTransaction")
	public String VisaMerchantTransactions(HttpServletRequest request, HttpServletResponse response,
			HttpSession session, Model model) {
		String sessionId = (String) session.getAttribute("adminSessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					List<TransactionReportResponse> allVisaMerchant = appAdminApi.allVisaMerchant(sessionId);
					//session.setAttribute("details", allVisaMerchant);
					model.addAttribute("addMerchant", allVisaMerchant);
					return "Admin/VisaMerchantTransaction";
				}
			}
		}
		return "Admin/Login";
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/VisaMerchantTransactions")
	public String VisaMerchantTransactionsFilter(HttpServletRequest request, HttpServletResponse response,
			HttpSession session, Model model, @ModelAttribute MerchantTransactionRequest req) {
		String sessionId = (String) session.getAttribute("adminSessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					model.addAttribute(ModelMapKey.MESSAGE,
							"Visa Merchant Transactions from " + req.getFromDate() + " to " + req.getToDate());
					req.setSessionId(sessionId);
					List<TransactionReportResponse> allVisaMerchant = appAdminApi.allVisaMerchantFilter(req);
					//session.setAttribute("details", allVisaMerchant);
					model.addAttribute("addMerchant", allVisaMerchant);
					return "Admin/VisaMerchantTransaction";
				}
			}
		}
		return "Admin/Login";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/VisaMerchantTransaction")
	public String VisaMerchantTransactionByMvisa(HttpServletRequest request, HttpServletResponse response,
			HttpSession session, Model model, @ModelAttribute VisaModel req) {
		System.err.println("SYS ::" + req.getRequest());
		String sessionId = (String) session.getAttribute("adminSessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					List<VisaMerchantTransaction> allVisaMerchantTransaction = appAdminApi
							.getVisaMerchantTransaction(req.getRequest());
					model.addAttribute("transactions", allVisaMerchantTransaction);
					System.err.println("Transctions :: " + allVisaMerchantTransaction.size());
					List<VisaMerchantRequest> allVisaMerchant = (List<VisaMerchantRequest>) session
							.getAttribute("details");
					model.addAttribute("addMerchant", allVisaMerchant);
					return "Admin/VisaMerchantTransaction";
				}
			}
		}
		return "Admin/Login";
	}

}
