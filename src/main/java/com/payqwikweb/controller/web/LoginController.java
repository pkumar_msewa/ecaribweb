package com.payqwikweb.controller.web;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.recaptcha.api.IVerificationApi;
import com.payqwikweb.app.api.ILoginApi;
import com.payqwikweb.app.api.IUserApi;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.request.LoginRequest;
import com.payqwikweb.app.model.response.LoginResponse;
import com.payqwikweb.model.app.request.UpdateReceiptDTO;
import com.payqwikweb.model.app.response.UpdateReceiptResponse;
import com.payqwikweb.model.web.UserDTO;
import com.payqwikweb.util.LogCat;
import com.payqwikweb.util.ModelMapKey;
import com.payqwikweb.validation.CommonValidation;

@Controller
@RequestMapping("/User")
public class LoginController implements MessageSourceAware {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private MessageSource messageSource;

	private final ILoginApi appLoginApi;
	private final CommonValidation commonValidation;
	private final IUserApi userApilog;
//	private final IVerificationApi verificationApi;

	/*public LoginController(ILoginApi loginApi, CommonValidation commonValidation, IVerificationApi verificationApi) {
		this.appLoginApi = loginApi;
		this.commonValidation = commonValidation;
		this.verificationApi = verificationApi;
	}*/
	
	

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	

	public LoginController(ILoginApi appLoginApi, CommonValidation commonValidation, IUserApi userApilog) {
	super();
	this.appLoginApi = appLoginApi;
	this.commonValidation = commonValidation;
	this.userApilog = userApilog;
}



	@RequestMapping(method = RequestMethod.GET, value = "/Travel/BusTravel")
	public String gettrevelbus(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			ModelMap model, HttpSession session) {
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null && sessionId.length() != 0)
		{
			String id = "";
			String city = "";
			try {
				String val = LoginController.reafile();
				ArrayList<String> arrayidval = new ArrayList<>();
				ArrayList<String> arraynameval = new ArrayList<>();
				JSONArray jsonarray = new JSONArray(val);
				for (int i = 0; i < jsonarray.length(); i++) {
					JSONObject jsonobject = jsonarray.getJSONObject(i);
					String idname = jsonobject.getString("Id");
					String cityname = jsonobject.getString("Name");
					id += "#" + idname;
					city += "@" + cityname;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			model.addAttribute("cityandid", id + "$" + city);
			return "User/Travel/Bus/BusTravel";

		} else {
			return "redirect:/Home";
		}

	}

	@RequestMapping(method = RequestMethod.GET, value = "/Travel/Bus/Allbusshowdetail")
	public String getavailebelbustrevelbus(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			ModelMap model, HttpSession session) {
		String sessionId = (String) session.getAttribute("sessionId");
		if(sessionId != null && sessionId.length() != 0) {
			return "User/Travel/Bus/Allbusshowdetail";
		} else {
			return "redirect:/Home";
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/Login")
	public String processLoginRequest(RedirectAttributes model, HttpServletRequest request,
			@ModelAttribute("login") LoginRequest loginDTO, HttpSession session,ModelMap map) {
		try{
		if (true) {
			UpdateReceiptDTO dto=new UpdateReceiptDTO();
			String property = "X-Forwarded-For";
			String ipAddress = request.getHeader(property);
			if(ipAddress == null) {
				System.err.println("Original IP is NULL");
				ipAddress = "N/A";
			}
			loginDTO.setIpAddress(ipAddress);
			LoginResponse resp = appLoginApi.login(loginDTO, Role.USER);
			if (resp.getCode() != null) {
				
				if (resp.getCode().equals("S00")) {
					dto.setSessionId(resp.getSessionId());
				  UpdateReceiptResponse res=userApilog.userReceipts(dto);
					session.setAttribute("sessionId", resp.getSessionId());
					session.setAttribute("balance", resp.getBalance());
					session.setAttribute("totalExpense", res.getCredit()+","+res.getDebit());
					session.setAttribute("firstName", resp.getFirstName());
					session.setAttribute("userDetails", resp);
					session.setAttribute("username", resp.getUsername());
					session.setAttribute("emailId", resp.getEmail());
					session.setAttribute("isKycType", resp.getUserType());
					return "redirect:/User/Home";
				} else if (resp.getCode().equals("F05")) {
					model.addFlashAttribute(ModelMapKey.ERROR, resp.getMessage());
					return "redirect:/Home";
				} else if(resp.getCode().equals("L01")) {
					System.err.println(resp.getMessage());
					map.addAttribute(ModelMapKey.MESSAGE,resp.getMessage());
					map.addAttribute("loginDTO",loginDTO);
					return "VerifyMobile";
				}
			}
		} else {
			model.addFlashAttribute(ModelMapKey.ERROR, "Invalid Captcha");
			return "redirect:/Home";
		  }
		 }catch(Exception e){ 
			  e.printStackTrace();
		}
		return "redirect:/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/Login/VerifyMobile")
	public String getHome(RedirectAttributes model, HttpServletRequest request,
						  @ModelAttribute("loginDTO") LoginRequest loginDTO, HttpSession session,ModelMap map) {
		try{
		if (true) {
			loginDTO.setValidate(true);
            String property = "X-Forwarded-For";
            String ipAddress = request.getHeader(property);
            if(ipAddress == null) {
                System.err.println("Original IP is NULL");
                ipAddress = "N/A";
            }
            loginDTO.setIpAddress(ipAddress);
			LoginResponse resp = appLoginApi.login(loginDTO, Role.USER);
			if (resp.getCode() != null) {
				if (resp.getCode().equals("S00")) {
					session.setAttribute("sessionId", resp.getSessionId());
					session.setAttribute("balance", resp.getBalance());
					session.setAttribute("firstName", resp.getFirstName());
					session.setAttribute("userDetails", resp);
					session.setAttribute("username", resp.getUsername());
					session.setAttribute("emailId", resp.getEmail());
					return "redirect:/User/Home";
				} else if (resp.getCode().equals("F05")) {
					model.addFlashAttribute(ModelMapKey.ERROR, resp.getMessage());
					return "redirect:/Home";
				} else  {
					System.err.println(resp.getMessage());
					map.addAttribute(ModelMapKey.MESSAGE,resp.getMessage());
					map.addAttribute("loginDTO",loginDTO);
					return "VerifyMobile";
				}
			}
		} else {
			model.addFlashAttribute(ModelMapKey.ERROR, "Invalid Captcha");
			return "redirect:/Home";
		}
	  }catch(Exception e){ 
		  e.printStackTrace();
		  
	  }
		return "redirect:/Home";
	}


	public static String reafile() {
		String data = "";
		try {
			String val = System.getProperty("user.dir");
			// File f = new
			// File("/usr/local/apps/tomcat/webapps/ROOT/resources/BusSource.txt");
			File f = new File("/home/vibhanshu/Documents/PayQwikCentralSystem/PayQwikWeb/WebContent/resources/BusSource.txt");
			// if(f.exists())
			// {
			// System.out.println(f.getAbsolutePath());
			FileInputStream fin = new FileInputStream(f);
			byte[] buffer = new byte[(int) f.length()];
			new DataInputStream(fin).readFully(buffer);
			fin.close();
			data = new String(buffer, "UTF-8");
			//
			// }
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
		}

		return data;
	}

}
