package com.payqwikweb.controller.web;

import java.io.File;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.coupon.api.IPromoCodeApi;
import com.gcm.api.INotificationApi;
import com.gcm.model.CronNotificationDTO;
import com.gcm.model.NotificationDTO;
import com.google.recaptcha.api.IVerificationApi;
import com.instantpay.api.IBalanceApi;
import com.instantpay.api.IStatusCheckApi;
import com.instantpay.model.Balance;
import com.instantpay.model.StatusCheck;
import com.instantpay.model.request.StatusCheckRequest;
import com.instantpay.model.response.BalanceResponse;
import com.instantpay.model.response.StatusCheckResponse;
import com.payqwikweb.api.IAuthenticationApi;
import com.payqwikweb.api.IDataConfigApi;
import com.payqwikweb.app.api.ILogoutApi;
import com.payqwikweb.app.api.ISuperAdminApi;
import com.payqwikweb.app.api.IUserApi;
import com.payqwikweb.app.api.IVersionApi;
import com.payqwikweb.app.metadatas.UrlMetadatas;
import com.payqwikweb.app.model.AccessDTO;
import com.payqwikweb.app.model.AddServiceTypeDTO;
import com.payqwikweb.app.model.NotificationsDTO;
import com.payqwikweb.app.model.ResponseStatus;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.TFilterDTO;
import com.payqwikweb.app.model.UserStatus;
import com.payqwikweb.app.model.busdto.BusTicketDTO;
import com.payqwikweb.app.model.flight.dto.FlightCancelableResp;
import com.payqwikweb.app.model.flight.dto.GetFlightDetailsForAdmin;
import com.payqwikweb.app.model.flight.request.FligthcancelableReq;
import com.payqwikweb.app.model.flight.response.FlightResponseDTO;
import com.payqwikweb.app.model.flight.response.FlightTicketAdminDTO;
import com.payqwikweb.app.model.flight.response.FlightTicketResp;
import com.payqwikweb.app.model.request.AccountTypeRequest;
import com.payqwikweb.app.model.request.AdminUserDetails;
import com.payqwikweb.app.model.request.AllTransactionRequest;
import com.payqwikweb.app.model.request.AllUserRequest;
import com.payqwikweb.app.model.request.BlockUnBlockUserRequest;
import com.payqwikweb.app.model.request.BulkFileUpload;
import com.payqwikweb.app.model.request.BulkMailRequest;
import com.payqwikweb.app.model.request.BulkSMSRequest;
import com.payqwikweb.app.model.request.CPasswordOTPRequest;
import com.payqwikweb.app.model.request.CPasswordRequest;
import com.payqwikweb.app.model.request.GCMError;
import com.payqwikweb.app.model.request.GCMRequest;
import com.payqwikweb.app.model.request.LoginRequest;
import com.payqwikweb.app.model.request.LogoutRequest;
import com.payqwikweb.app.model.request.MailRequest;
import com.payqwikweb.app.model.request.MobileSearchRequest;
import com.payqwikweb.app.model.request.PagingDTO;
import com.payqwikweb.app.model.request.PromoCodeError;
import com.payqwikweb.app.model.request.PromoCodeRequest;
import com.payqwikweb.app.model.request.ReceiptsRequest;
import com.payqwikweb.app.model.request.RefundDTO;
import com.payqwikweb.app.model.request.SMSRequest;
import com.payqwikweb.app.model.request.ServiceRequest;
import com.payqwikweb.app.model.request.SessionDTO;
import com.payqwikweb.app.model.request.TransactionFilter;
import com.payqwikweb.app.model.request.TreatCardPlanRequest;
import com.payqwikweb.app.model.request.TreatCardRegisterList;
import com.payqwikweb.app.model.request.UserInfoRequest;
import com.payqwikweb.app.model.request.bus.IsCancellableReq;
import com.payqwikweb.app.model.response.AadharServiceResponse;
import com.payqwikweb.app.model.response.AccessListResponse;
import com.payqwikweb.app.model.response.AccountTypeResponse;
import com.payqwikweb.app.model.response.AjaxResponse;
import com.payqwikweb.app.model.response.AllTransactionResponse;
import com.payqwikweb.app.model.response.AllUserResponse;
import com.payqwikweb.app.model.response.BlockUnBlockUserResponse;
import com.payqwikweb.app.model.response.BulkFileListResponse;
import com.payqwikweb.app.model.response.CPasswordResponse;
import com.payqwikweb.app.model.response.CommissionDTO;
import com.payqwikweb.app.model.response.GCMResponse;
import com.payqwikweb.app.model.response.LoginResponse;
import com.payqwikweb.app.model.response.LogoutResponse;
import com.payqwikweb.app.model.response.MTransactionResponseDTO;
import com.payqwikweb.app.model.response.MailLogResponse;
import com.payqwikweb.app.model.response.MailResponse;
import com.payqwikweb.app.model.response.MobileSearchResponse;
import com.payqwikweb.app.model.response.NEFTResponse;
import com.payqwikweb.app.model.response.PromoCodeResponse;
import com.payqwikweb.app.model.response.ReceiptsResponse;
import com.payqwikweb.app.model.response.SMSLogResponse;
import com.payqwikweb.app.model.response.SMSResponse;
import com.payqwikweb.app.model.response.ServiceListDTO;
import com.payqwikweb.app.model.response.ServiceListResponse;
import com.payqwikweb.app.model.response.ServiceTypeResponse;
import com.payqwikweb.app.model.response.ServicesDTO;
import com.payqwikweb.app.model.response.TListResponse;
import com.payqwikweb.app.model.response.TreatCardPlansResponse;
import com.payqwikweb.app.model.response.TreatCardRegisterListResponse;
import com.payqwikweb.app.model.response.UpdateAccessResponse;
import com.payqwikweb.app.model.response.UserInfoResponse;
import com.payqwikweb.app.model.response.UserTransactionResponse;
import com.payqwikweb.app.model.response.VersionDTO;
import com.payqwikweb.app.model.response.VersionListResponse;
import com.payqwikweb.app.model.response.bus.IsCancellableResp;
import com.payqwikweb.app.model.response.bus.ResponseDTO;
import com.payqwikweb.model.admin.FilterDTO;
import com.payqwikweb.model.admin.SearchDTO;
import com.payqwikweb.model.admin.TListDTO;
import com.payqwikweb.model.app.request.AddServicesDTO;
import com.payqwikweb.model.app.request.UserListRequest;
import com.payqwikweb.model.app.request.VersionRequest;
import com.payqwikweb.model.app.response.TransactionUserResponse;
import com.payqwikweb.model.app.response.UserDetail;
import com.payqwikweb.model.app.response.UserListResponse;
import com.payqwikweb.model.web.Status;
import com.payqwikweb.model.web.SystemInfoDTO;
import com.payqwikweb.model.web.TransactionReport;
import com.payqwikweb.model.web.UserListDTO;
import com.payqwikweb.util.Authorities;
import com.payqwikweb.util.JSONParserUtil;
import com.payqwikweb.util.LogCat;
import com.payqwikweb.util.ModelMapKey;
import com.payqwikweb.validation.CommonValidation;
import com.payqwikweb.validation.GCMValidation;
import com.payqwikweb.validation.PromoCodeValidation;
import com.payqwikweb.validation.RegisterValidation;
import com.sun.management.OperatingSystemMXBean;

@Controller
@RequestMapping("/SuperAdmin")
public class SuperAdminController implements MessageSourceAware {

	private MessageSource messageSource;

	private final ISuperAdminApi superAdminApi;
	private final IAuthenticationApi authenticationApi;
	private final ILogoutApi logoutApi;
	private final IUserApi userApi;
	private final RegisterValidation registerValidation;
//	private final IVerificationApi verificationApi;
	private final IVersionApi versionApi;
	private final PromoCodeValidation promoCodeValidation;
	private final IPromoCodeApi promoCodeApi;
	private final INotificationApi notificationApi;
	private final GCMValidation gcmValidation;
	private final IBalanceApi balanceApi;
	private final IStatusCheckApi statusCheckApi;
	private final IDataConfigApi dataConfigApi;

	private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
	private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	private SimpleDateFormat dateFilter = new SimpleDateFormat("yyyy-MM-dd HH:mm");

	public SuperAdminController(ISuperAdminApi superAdminApi, IAuthenticationApi authenticationApi,
			ILogoutApi logoutApi, IUserApi userApi, RegisterValidation registerValidation,
			IVersionApi versionApi, PromoCodeValidation promoCodeValidation,
			IPromoCodeApi promoCodeApi, INotificationApi notificationApi, GCMValidation gcmValidation,
			IBalanceApi balanceApi, IStatusCheckApi statusCheckApi,IDataConfigApi dataConfigApi) {
		this.superAdminApi = superAdminApi;
		this.authenticationApi = authenticationApi;
		this.logoutApi = logoutApi;
		this.userApi = userApi;
		this.registerValidation = registerValidation;
//		this.verificationApi = verificationApi;
		this.versionApi = versionApi;
		this.promoCodeValidation = promoCodeValidation;
		this.promoCodeApi = promoCodeApi;
		this.notificationApi = notificationApi;
		this.gcmValidation = gcmValidation;
		this.balanceApi = balanceApi;
		this.statusCheckApi = statusCheckApi;
		this.dataConfigApi = dataConfigApi;
	}

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/Home")
	public String getUser(@RequestParam(value = ModelMapKey.MESSAGE, required = false) String msg,
			@RequestParam(value = ModelMapKey.ERROR, required = false) String error, HttpServletRequest request,
			ModelMap model, HttpServletResponse response, HttpSession session) {
		if (msg != null) {
			model.addAttribute(ModelMapKey.MESSAGE, msg);
		}
		if (error != null) {
			model.addAttribute(ModelMapKey.ERROR, error);
		}
		String adminSessionId = (String) session.getAttribute("superAdminSession");
		if (adminSessionId != null) {
			SessionDTO dto = new SessionDTO();
			dto.setSessionId(adminSessionId);
			String authority = authenticationApi.getAuthorityFromSession(adminSessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					return "SuperAdmin/Home";
				}
			}

		}
		return "SuperAdmin/Login";

	}

	@RequestMapping(method = RequestMethod.POST, value = "/Home")
	public String getAdminHomePage(HttpServletRequest request, ModelMap modelMap, HttpServletResponse response,
			@ModelAttribute("login") LoginRequest loginDTO, ModelMap model, HttpSession session,
			RedirectAttributes modelmap) {

		try {
			loginDTO.setIpAddress(request.getRemoteAddr());
			LoginResponse loginResponse = superAdminApi.login(loginDTO);
			String sessionId = loginResponse.getSessionId();
			session.setAttribute("superAdminSession", sessionId);
			if (loginResponse.getCode() != null) {
				if (loginResponse.getCode().toString().equals("S00")) {
					return "SuperAdmin/Home";
				} else if (loginResponse.getCode().equals("F05")) {
					modelmap.addFlashAttribute(ModelMapKey.ERROR, loginResponse.getMessage());
					return "redirect:/SuperAdmin/Home?error=" + loginResponse.getMessage();
				} else if (loginResponse.getCode().equals("F04")) {
					modelmap.addFlashAttribute(ModelMapKey.ERROR, loginResponse.getMessage());
					return "redirect:/SuperAdmin/Home?error=" + loginResponse.getMessage();
				} else if (loginResponse.getCode().equals("L01")) {
					System.err.println(loginResponse.getMessage());
					model.addAttribute(ModelMapKey.MESSAGE, loginResponse.getMessage());
					model.addAttribute("loginDTO", loginDTO);
					return "SuperAdmin/VerifyMobile";
				} else {
					return "redirect:/SuperAdmin/Home?error=" + loginResponse.getMessage();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "SuperAdmin/Login";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/Login/VerifyMobile")
	public String getVerifyMobile(RedirectAttributes model, HttpServletRequest request,
			@ModelAttribute("loginDTO") LoginRequest loginDTO, HttpSession session, ModelMap map) {
		loginDTO.setValidate(true);
		loginDTO.setIpAddress(request.getRemoteAddr());
		LoginResponse resp = superAdminApi.login(loginDTO);
		String sessionId = resp.getSessionId();
		session.setAttribute("superAdminSession", sessionId);
		if (resp.getCode() != null) {
			if (resp.getCode().equals("S00")) {
				System.err.println("inside verify mobile controller");
				return "redirect:/SuperAdmin/Home";
			} else if (resp.getCode().equals("F05")) {
				model.addFlashAttribute(ModelMapKey.ERROR, resp.getMessage());
				return "redirect:/SuperAdmin/Home?error=" + resp.getMessage();
			} else {
				System.err.println(resp.getMessage());
				map.addAttribute(ModelMapKey.MESSAGE, resp.getMessage());
				map.addAttribute("loginDTO", loginDTO);
				return "SuperAdmin/VerifyMobile";
			}
		}
		return "redirect:/SuperAdmin/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/HomeInAjax")
	public ResponseEntity<AjaxResponse> getTotalUsers(HttpServletRequest request, HttpServletResponse response,
			HttpSession session, Model model) throws JSONException {
		AjaxResponse entity = new AjaxResponse();
		SessionDTO dto = new SessionDTO();
		dto.setSessionId((String) session.getAttribute("superAdminSession"));
		UserTransactionResponse userTransactionResponse = superAdminApi.getUserTransactionValues(dto);
		if (userTransactionResponse != null) {
			if (userTransactionResponse.isSuccess()) {
				JSONObject json = new JSONObject(userTransactionResponse.getResponse());
				if (json != null) {
					JSONObject details = JSONParserUtil.getObject(json, "details");
					double d = JSONParserUtil.getDouble(details, "walletBalance");
					entity.setTotalLoadMoneyEBS(JSONParserUtil.getDouble(details, "totalLoadMoneyEBS"));
					entity.setTotalLoadMoneyVNet(JSONParserUtil.getDouble(details, "totalLoadMoneyVNet"));
					entity.setPool(BigDecimal.valueOf(d).setScale(2, RoundingMode.HALF_UP).doubleValue());
					entity.setTotalPayable(JSONParserUtil.getDouble(details, "totalPayable"));
					double merchantPayble = JSONParserUtil.getDouble(details, "merchantPayable");
					entity.setMerchantPayable(
							BigDecimal.valueOf(merchantPayble).setScale(2, RoundingMode.HALF_UP).doubleValue());
					double totalCommission = JSONParserUtil.getDouble(details, "totalCommission");
					entity.setTotalCommission(BigDecimal.valueOf(totalCommission).setScale(2, RoundingMode.HALF_UP).doubleValue());
					entity.setBankAmount(JSONParserUtil.getDouble(details, "bankAmount"));
					entity.setTotalTrans(JSONParserUtil.getLong(details, "totalTransaction"));
					entity.setTotalUser(JSONParserUtil.getLong(details, "totalUsers"));
				}
			}
		}
		return new ResponseEntity<AjaxResponse>(entity, HttpStatus.OK);

	}

	@RequestMapping(method = RequestMethod.GET, value = "/SystemInfo")
	public ResponseEntity<SystemInfoDTO> getSystemInfo(HttpServletRequest request, HttpServletResponse response,
			HttpSession session, Model model) throws JSONException {

		SystemInfoDTO info = new SystemInfoDTO();
		SessionDTO dto = new SessionDTO();
		dto.setSessionId((String) session.getAttribute("superAdminSession"));
		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					info.setValid(true);
					info.setOsName(System.getProperty("os.name"));
					info.setOsType(System.getProperty("os.arch"));
					info.setOsVersion(System.getProperty("os.version"));
					OperatingSystemMXBean operatingSystemMXBean = (com.sun.management.OperatingSystemMXBean) ManagementFactory
							.getOperatingSystemMXBean();
					double cpuLoad = operatingSystemMXBean.getSystemCpuLoad();
					info.setCpuUsage((long) Math.ceil(cpuLoad * 100));
					long freeMemoryBytes = operatingSystemMXBean.getFreePhysicalMemorySize();
					long totalMemoryBytes = operatingSystemMXBean.getTotalPhysicalMemorySize();
					info.setFreeSpace((long) Math.ceil(freeMemoryBytes / (1024 * 1024)));
					info.setTotalSpace((long) Math.ceil(totalMemoryBytes / (1024 * 1024)));
				}
			}
		}
		return new ResponseEntity<SystemInfoDTO>(info, HttpStatus.OK);

	}

	// Display last 1000 users
	@RequestMapping(method = RequestMethod.GET, value = "/UserList")
	public String getUserList(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) {
		UserListDTO result = new UserListDTO();
		UserListRequest listRequest = new UserListRequest();
		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					listRequest.setSessionId(sessionCheck);
					UserListResponse listResponse = superAdminApi.getUsersList(listRequest, "ALL");
					if (listResponse.isSuccess()) {
						model.addAttribute("userList", listResponse.getUserList());
					} else {
						model.addAttribute(ModelMapKey.ERROR, listResponse.getMessage());
					}
					return "SuperAdmin/UserList";
				}
			}
		}
		return "redirect:/SuperAdmin/Home";
	}

	// Display Filtered Users
	@RequestMapping(method = RequestMethod.POST, value = "/UserList")
	public String getUserListFilter(@ModelAttribute UserListRequest dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) {
		UserListDTO result = new UserListDTO();
		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionCheck);
					UserListResponse listResponse = superAdminApi.getUsersListWithFilter(dto, "ALL");
					if (listResponse.isSuccess()) {
						model.addAttribute("userList", listResponse.getUserList());
						model.addAttribute(ModelMapKey.MESSAGE,
								"All Users b/w " + dto.getStartDate() + " and " + dto.getEndDate());
					} else {
						model.addAttribute(ModelMapKey.ERROR, listResponse.getMessage());
					}
					return "SuperAdmin/UserList";
				}
			}
		}
		return "redirect:/SuperAdmin/Home";
	}

	// Display all verified users
	@RequestMapping(method = RequestMethod.GET, value = "/VerifiedUsers")
	public String getVerifiedUserList(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) {
		UserListDTO result = new UserListDTO();
		UserListRequest listRequest = new UserListRequest();
		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					listRequest.setSessionId(sessionCheck);
					UserListResponse listResponse = superAdminApi.getUsersList(listRequest, "VERIFIED");
					if (listResponse.isSuccess()) {
						model.addAttribute("userList", listResponse.getUserList());
					} else {
						model.addAttribute(ModelMapKey.ERROR, listResponse.getMessage());
					}
					return "SuperAdmin/VerifiedUsers";
				}
			}
		}
		return "redirect:/SuperAdmin/Home";
	}

	// display filtered verified users

	@RequestMapping(method = RequestMethod.POST, value = "/VerifiedUsers")
	public String getVerifiedUserListFilter(@ModelAttribute UserListRequest dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) {
		UserListDTO result = new UserListDTO();
		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionCheck);
					UserListResponse listResponse = superAdminApi.getUsersListWithFilter(dto, "VERIFIED");
					if (listResponse.isSuccess()) {
						model.addAttribute("userList", listResponse.getUserList());
						model.addAttribute(ModelMapKey.MESSAGE,
								"Verified Users b/w " + dto.getStartDate() + " and " + dto.getEndDate());
					} else {
						model.addAttribute(ModelMapKey.ERROR, listResponse.getMessage());
					}
					return "SuperAdmin/VerifiedUsers";
				}
			}
		}
		return "redirect:/SuperAdmin/Home";
	}

	// display last 1000 locked users
	@RequestMapping(method = RequestMethod.GET, value = "/LockedUsers")
	public String getLockedUserList(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) {
		UserListDTO result = new UserListDTO();
		UserListRequest listRequest = new UserListRequest();
		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					listRequest.setSessionId(sessionCheck);
					UserListResponse listResponse = superAdminApi.getUsersList(listRequest, "LOCKED");
					if (listResponse.isSuccess()) {
						model.addAttribute("userList", listResponse.getUserList());
					} else {
						model.addAttribute(ModelMapKey.ERROR, listResponse.getMessage());
					}
					return "SuperAdmin/LockedUsers";
				}
			}
		}
		return "redirect:/SuperAdmin/Home";
	}

	// display filtered locked users

	@RequestMapping(method = RequestMethod.POST, value = "/LockedUsers")
	public String getLockedUserListFilter(@ModelAttribute UserListRequest dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) {
		UserListDTO result = new UserListDTO();
		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionCheck);
					UserListResponse listResponse = superAdminApi.getUsersListWithFilter(dto, "LOCKED");
					if (listResponse.isSuccess()) {
						model.addAttribute("userList", listResponse.getUserList());
						model.addAttribute(ModelMapKey.MESSAGE,
								"Locked Users b/w " + dto.getStartDate() + " and " + dto.getEndDate());
					} else {
						model.addAttribute(ModelMapKey.ERROR, listResponse.getMessage());
					}
					return "SuperAdmin/LockedUsers";
				}
			}
		}
		return "redirect:/SuperAdmin/Home";
	}

	// display last 1000 blocked users
	@RequestMapping(method = RequestMethod.GET, value = "/BlockedUsers")
	public String getBlockedUserList(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) {
		UserListDTO result = new UserListDTO();
		UserListRequest listRequest = new UserListRequest();
		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					listRequest.setSessionId(sessionCheck);
					UserListResponse listResponse = superAdminApi.getUsersList(listRequest, "BLOCKED");
					if (listResponse.isSuccess()) {
						model.addAttribute("userList", listResponse.getUserList());
					} else {
						model.addAttribute(ModelMapKey.ERROR, listResponse.getMessage());
					}
					return "SuperAdmin/BlockedUsers";
				}
			}
		}
		return "redirect:/SuperAdmin/Home";
	}

	// display filtered blocked users

	@RequestMapping(method = RequestMethod.POST, value = "/BlockedUsers")
	public String getBlockedUserListFilter(@ModelAttribute UserListRequest dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) {
		UserListDTO result = new UserListDTO();
		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionCheck);
					UserListResponse listResponse = superAdminApi.getUsersListWithFilter(dto, "BLOCKED");
					if (listResponse.isSuccess()) {
						model.addAttribute("userList", listResponse.getUserList());
						model.addAttribute(ModelMapKey.MESSAGE,
								"Blocked Users b/w " + dto.getStartDate() + " and " + dto.getEndDate());
					} else {
						model.addAttribute(ModelMapKey.ERROR, listResponse.getMessage());
					}
					return "SuperAdmin/BlockedUsers";
				}
			}
		}
		return "redirect:/SuperAdmin/Home";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/OnlineUsers")
	public String getOnlineUserList(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) {
		UserListDTO result = new UserListDTO();
		UserListRequest listRequest = new UserListRequest();
		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					listRequest.setSessionId(sessionCheck);
					UserListResponse listResponse = superAdminApi.getUsersList(listRequest, "ONLINE");
					if (listResponse.isSuccess()) {
						model.addAttribute("userList", listResponse.getUserList());
						model.addAttribute(ModelMapKey.MESSAGE, listResponse.getMessage());
					} else {
						model.addAttribute(ModelMapKey.ERROR, listResponse.getMessage());
					}
					return "SuperAdmin/ActiveUsers";
				}
			}
		}
		return "redirect:/SuperAdmin/Home";
	}

	// display last 1000 male users
	@RequestMapping(method = RequestMethod.GET, value = "/MaleUsers")
	public String getMaleUserList(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) {
		UserListDTO result = new UserListDTO();
		UserListRequest listRequest = new UserListRequest();
		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					listRequest.setSessionId(sessionCheck);
					UserListResponse listResponse = superAdminApi.getUsersList(listRequest, "MALE");
					if (listResponse.isSuccess()) {
						model.addAttribute("userList", listResponse.getUserList());
					} else {
						model.addAttribute(ModelMapKey.ERROR, listResponse.getMessage());
					}
					return "SuperAdmin/MaleUsers";
				}
			}
		}
		return "redirect:/SuperAdmin/Home";
	}

	// display filtered male users

	@RequestMapping(method = RequestMethod.POST, value = "/MaleUsers")
	public String getMaleUserListFilter(@ModelAttribute UserListRequest dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) {
		UserListDTO result = new UserListDTO();
		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionCheck);
					UserListResponse listResponse = superAdminApi.getUsersListWithFilter(dto, "MALE");
					if (listResponse.isSuccess()) {
						model.addAttribute("userList", listResponse.getUserList());
						model.addAttribute(ModelMapKey.MESSAGE,
								"Male Users b/w " + dto.getStartDate() + " and " + dto.getEndDate());
					} else {
						model.addAttribute(ModelMapKey.ERROR, listResponse.getMessage());
					}
					return "SuperAdmin/MaleUsers";
				}
			}
		}
		return "redirect:/SuperAdmin/Home";
	}

	// display 1000 female users
	@RequestMapping(method = RequestMethod.GET, value = "/FemaleUsers")
	public String getFemaleUserList(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) {
		UserListDTO result = new UserListDTO();
		UserListRequest listRequest = new UserListRequest();
		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					listRequest.setSessionId(sessionCheck);
					UserListResponse listResponse = superAdminApi.getUsersList(listRequest, "FEMALE");
					if (listResponse.isSuccess()) {
						model.addAttribute("userList", listResponse.getUserList());
					} else {
						model.addAttribute(ModelMapKey.ERROR, listResponse.getMessage());
					}
					return "SuperAdmin/FemaleUsers";
				}
			}
		}
		return "redirect:/SuperAdmin/Home";
	}

	// display filtered female users

	@RequestMapping(method = RequestMethod.POST, value = "/FemaleUsers")
	public String getFemaleUserListFilter(@ModelAttribute UserListRequest dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) {
		UserListDTO result = new UserListDTO();
		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionCheck);
					UserListResponse listResponse = superAdminApi.getUsersListWithFilter(dto, "FEMALE");
					if (listResponse.isSuccess()) {
						model.addAttribute("userList", listResponse.getUserList());
						model.addAttribute(ModelMapKey.MESSAGE,
								"Female Users b/w " + dto.getStartDate() + " and " + dto.getEndDate());
					} else {
						model.addAttribute(ModelMapKey.ERROR, listResponse.getMessage());
					}
					return "SuperAdmin/FemaleUsers";
				}
			}
		}
		return "redirect:/SuperAdmin/Home";
	}

	// display 1000 kyc users
	@RequestMapping(method = RequestMethod.GET, value = "/KYCUsers")
	public String getKYCUserList(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) {
		UserListDTO result = new UserListDTO();
		UserListRequest listRequest = new UserListRequest();
		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					listRequest.setSessionId(sessionCheck);
					UserListResponse listResponse = superAdminApi.getUsersList(listRequest, "KYC");
					if (listResponse.isSuccess()) {
						model.addAttribute("userList", listResponse.getUserList());
					} else {
						model.addAttribute(ModelMapKey.ERROR, listResponse.getMessage());
					}
					return "SuperAdmin/KYCusers";
				}
			}
		}
		return "redirect:/SuperAdmin/Home";
	}

	// display filtered kyc users list

	@RequestMapping(method = RequestMethod.POST, value = "/KYCUsers")
	public String getKYCUserListFilter(@ModelAttribute UserListRequest dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) {
		UserListDTO result = new UserListDTO();
		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionCheck);
					UserListResponse listResponse = superAdminApi.getUsersListWithFilter(dto, "KYC");
					if (listResponse.isSuccess()) {
						model.addAttribute("userList", listResponse.getUserList());
						model.addAttribute(ModelMapKey.MESSAGE,
								"KYC Users b/w " + dto.getStartDate() + " and " + dto.getEndDate());
					} else {
						model.addAttribute(ModelMapKey.ERROR, listResponse.getMessage());
					}
					return "SuperAdmin/KYCusers";
				}
			}
		}
		return "redirect:/SuperAdmin/Home";
	}

	// display last 1000 unverified users
	@RequestMapping(method = RequestMethod.GET, value = "/UnverifiedUsers")
	public String getUnverifiedUserList(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) {
		UserListDTO result = new UserListDTO();
		UserListRequest listRequest = new UserListRequest();
		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					listRequest.setSessionId(sessionCheck);
					UserListResponse listResponse = superAdminApi.getUsersList(listRequest, "UNVERIFIED");
					if (listResponse.isSuccess()) {
						model.addAttribute("userList", listResponse.getUserList());
					} else {
						model.addAttribute(ModelMapKey.ERROR, listResponse.getMessage());
					}
					return "SuperAdmin/UnverifiedUsers";
				}
			}
		}
		return "redirect:/SuperAdmin/Home";
	}

	// display filtered unverified users
	@RequestMapping(method = RequestMethod.POST, value = "/UnverifiedUsers")
	public String getUnverifiedUserListFilter(@ModelAttribute UserListRequest dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) {
		UserListDTO result = new UserListDTO();
		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionCheck);
					UserListResponse listResponse = superAdminApi.getUsersList(dto, "UNVERIFIED");
					if (listResponse.isSuccess()) {
						model.addAttribute("userList", listResponse.getUserList());
						model.addAttribute(ModelMapKey.MESSAGE,
								"Unverified Users b/w " + dto.getStartDate() + " and " + dto.getEndDate());
					} else {
						model.addAttribute(ModelMapKey.ERROR, listResponse.getMessage());
					}
					return "SuperAdmin/UnverifiedUsers";
				}
			}
		}
		return "redirect:/SuperAdmin/Home";
	}

	// Get Transaction List Now

	@RequestMapping(method = RequestMethod.GET, value = "/TransactionReport")
	public String getTransactionList(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) {
		UserListDTO result = new UserListDTO();
		AllTransactionRequest dto = new AllTransactionRequest();
		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionCheck);
					TListResponse listResponse = superAdminApi.getTransactionList(dto);
					if (listResponse.isSuccess()) {
						model.addAttribute("transactionList", listResponse.getList());
					} else {
						model.addAttribute(ModelMapKey.ERROR, listResponse.getMessage());
					}
					return "SuperAdmin/DailyReport";
				}
			}
		}
		return "redirect:/SuperAdmin/Home";
	}

	// Get Transaction List using filter

	@RequestMapping(method = RequestMethod.POST, value = "/TransactionReport")
	public String getTransactionList(@ModelAttribute AllTransactionRequest dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) {
		UserListDTO result = new UserListDTO();
		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionCheck);
					TListResponse listResponse = superAdminApi.getTransactionListWithFilter(dto);
					if (listResponse.isSuccess()) {
						model.addAttribute("transactionList", listResponse.getList());
						model.addAttribute(ModelMapKey.MESSAGE,
								"Transaction List b/w " + dto.getStartDate() + " and " + dto.getEndDate());
					} else {
						model.addAttribute(ModelMapKey.ERROR, listResponse.getMessage());
					}
					return "SuperAdmin/DailyReport";
				}
			}
		}
		return "redirect:/SuperAdmin/Home";
	}

	// settlement account get

	@RequestMapping(method = RequestMethod.GET, value = "/SettlementAccount")
	public String getSettlementAccount(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) throws JSONException {
		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					return "SuperAdmin/SettlementAccount";
				}
			}
		}
		return "redirect:/SuperAdmin/Home";
	}

	// settlement account post

	@RequestMapping(method = RequestMethod.POST, value = "/SettlementAccount")
	public String getSettlementAccount(@ModelAttribute TransactionFilter dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) throws JSONException {
		try {

			String sessionCheck = (String) session.getAttribute("superAdminSession");
			if (sessionCheck != null) {
				String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
				if (authority != null) {
					if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
						model.addAttribute(ModelMapKey.MESSAGE,
								"Settlement Transactions From " + dto.getStartDate() + " to " + dto.getEndDate());
						AllTransactionRequest transRequest = new AllTransactionRequest();
						transRequest.setStartDate(dto.getStartDate());
						transRequest.setEndDate(dto.getEndDate());
						transRequest.setReportType(dto.getServiceType());
						transRequest.setSessionId((String) session.getAttribute("superAdminSession"));
						AllTransactionResponse allTransaction = superAdminApi.getSettlementTransactions(transRequest);
						if (allTransaction.isSuccess()) {
							List<TransactionReport> reports = new ArrayList<>();
							JSONArray reportArray = allTransaction.getJsonArray();
							if (reportArray != null) {
								for (int i = 0; i < reportArray.length(); i++) {
									JSONObject json = reportArray.getJSONObject(i);
									TransactionReport report = new TransactionReport();
									report.setAmount(JSONParserUtil.getDouble(json, "amount"));
									report.setDate(JSONParserUtil.getString(json, "date"));
									report.setService(JSONParserUtil.getString(json, "service"));
									report.setDebit(JSONParserUtil.getBoolean(json, "debit"));
									report.setTransactionRefNo(JSONParserUtil.getString(json, "transactionRefNo"));
									report.setStatus(com.payqwikweb.model.web.Status
											.valueOf(JSONParserUtil.getString(json, "status")));
									report.setDescription(JSONParserUtil.getString(json, "description"));
									reports.add(report);
								}
								model.addAttribute("transactions", reports);
								model.addAttribute(ModelMapKey.MESSAGE, dto.getServiceType() + " reports from "
										+ dto.getStartDate() + "to " + dto.getEndDate());
							}
							return "SuperAdmin/SettlementTransactions";
						}
						return "SuperAdmin/SettlementAccount";
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			return "redirect:/SuperAdmin/Home";
		}

		return "redirect:/SuperAdmin/Home";
	}

	// SMSLogs ALL
	@RequestMapping(method = RequestMethod.GET, value = "/SMSLogs")
	public String getSMSLogsList(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) {
		AllTransactionRequest dto = new AllTransactionRequest();
		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionCheck);
					SMSLogResponse listResponse = superAdminApi.getSMSLogs(dto);
					if (listResponse.isSuccess()) {
						model.addAttribute("logs", listResponse.getLogList());
					} else {
						model.addAttribute(ModelMapKey.ERROR, listResponse.getMessage());
					}
					return "SuperAdmin/SMSLogs";
				}
			}
		}
		return "redirect:/SuperAdmin/Home";
	}

	// SMSLogs ALL
	@RequestMapping(method = RequestMethod.POST, value = "/SMSLogs")
	public String getSMSLogsListBetween(@ModelAttribute AllTransactionRequest dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) {
		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionCheck);
					SMSLogResponse listResponse = superAdminApi.getSMSLogsFilter(dto);
					if (listResponse.isSuccess()) {
						model.addAttribute("logs", listResponse.getLogList());
						model.addAttribute(ModelMapKey.MESSAGE,
								"SMS Logs b/w " + dto.getStartDate() + " and " + dto.getEndDate());
					} else {
						model.addAttribute(ModelMapKey.ERROR, listResponse.getMessage());
					}
					return "SuperAdmin/SMSLogs";
				}
			}
		}
		return "redirect:/SuperAdmin/Home";
	}

	// MailLogs ALL
	@RequestMapping(method = RequestMethod.GET, value = "/MailLogs")
	public String getMailLogsList(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) {
		AllTransactionRequest dto = new AllTransactionRequest();
		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionCheck);
					MailLogResponse listResponse = superAdminApi.getMailLogs(dto);
					if (listResponse.isSuccess()) {
						model.addAttribute("logs", listResponse.getLogList());
					} else {
						model.addAttribute(ModelMapKey.ERROR, listResponse.getMessage());
					}
					return "SuperAdmin/MailLogs";
				}
			}
		}
		return "redirect:/SuperAdmin/Home";
	}

	// MailLogs Filter
	@RequestMapping(method = RequestMethod.POST, value = "/MailLogs")
	public String getMailLogsListBetween(@ModelAttribute AllTransactionRequest dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) {
		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionCheck);
					MailLogResponse listResponse = superAdminApi.getMailLogsFilter(dto);
					if (listResponse.isSuccess()) {
						model.addAttribute("logs", listResponse.getLogList());
						model.addAttribute(ModelMapKey.MESSAGE,
								"Mail Logs b/w " + dto.getStartDate() + " and " + dto.getEndDate());
					} else {
						model.addAttribute(ModelMapKey.ERROR, listResponse.getMessage());
					}
					return "SuperAdmin/MailLogs";
				}
			}
		}
		return "redirect:/SuperAdmin/Home";
	}

	// ALL Services List
	@RequestMapping(method = RequestMethod.GET, value = "/Services")
	public String getAllServicesList(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) {
		SessionDTO dto = new SessionDTO();
		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionCheck);
					ServiceListResponse listResponse = superAdminApi.getServiceList(dto);
					if (listResponse.isSuccess()) {
						model.addAttribute("services", listResponse.getServiceList());
					} else {
						model.addAttribute(ModelMapKey.ERROR, listResponse.getMessage());
					}
					return "SuperAdmin/ServiceList";
				}
			}
		}
		return "redirect:/SuperAdmin/Home";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/Version")
	public String getAllVersionList(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) {
		SessionDTO dto = new SessionDTO();
		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionCheck);
					VersionListResponse listResponse = superAdminApi.getVersionList(dto);
					if (listResponse.isSuccess()) {
						model.addAttribute("versions", listResponse.getVersionList());
					} else {
						model.addAttribute(ModelMapKey.ERROR, listResponse.getMessage());
					}
					return "SuperAdmin/VersionList";
				}
			}
		}
		return "redirect:/SuperAdmin/Home";
	}

	

	// GET send AccessUsers
	@RequestMapping(method = RequestMethod.GET, value = "/AccessList")
	public String getAccessUsers(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) {
		SessionDTO dto = new SessionDTO();
		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionCheck);
					AccessListResponse listResponse = superAdminApi.getAccessList(dto);
					if (listResponse.isSuccess()) {
						model.addAttribute("access", listResponse.getList());
					} else {
						model.addAttribute(ModelMapKey.ERROR, listResponse.getMessage());
					}
					return "SuperAdmin/AccessList";
				}
			}
		}
		return "redirect:/SuperAdmin/Home";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{username}/GetAccessListByUser/")
	ResponseEntity<AccessDTO> getAccessListByUser(@PathVariable(value = "username") String username,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model) {
		SessionDTO dto = new SessionDTO();
		AccessDTO access = new AccessDTO();
		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionCheck);
					AccessListResponse listResponse = superAdminApi.getAccessList(dto);
					for (AccessDTO a : listResponse.getList()) {
						if (a != null) {
							if (a.getUsername().equals(username)) {
								access = a;
								break;
							}
						}
					}
				}
			}
		}
		return new ResponseEntity<AccessDTO>(access, HttpStatus.OK);
	}
	
	// GET send notifications
		@RequestMapping(method = RequestMethod.GET, value = "/SendNotification")
		public String sendNotification(HttpServletRequest request, HttpServletResponse response, HttpSession session,
				ModelMap model) {
			String sessionCheck = (String) session.getAttribute("superAdminSession");
			if (sessionCheck != null)
				return "/SuperAdmin/SendNotification";
			return "redirect:/SuperAdmin/Home";
		}

	// POST GCM IDs
/*	@RequestMapping(method = RequestMethod.POST, value = "/SendNotification")
	public String getGCMIDs(@ModelAttribute GCMRequest dto, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, ModelMap model) {
		int pageNo = 0;
		int size = 1000;
		String status = "";
		String[] parts = new String[5];
		String sessionCheck = (String) session.getAttribute("superAdminSession");
		System.err.println(dto.toString());
		GCMError error = gcmValidation.checkError(dto);
		if (error.isValid()) {
			if (dto.isImageGCM()) {
				String rootDirectory = request.getSession().getServletContext().getRealPath("/");
				status = saveLogo(rootDirectory, dto.getGcmImage());
				parts = status.split("\\|");
			}
			PagingDTO page = new PagingDTO();
			page.setSessionId(sessionCheck);
			page.setPage(pageNo);
			page.setSize(size);
			GCMResponse gcmResponse = superAdminApi.getGCMIds(page);
			long totalPages = gcmResponse.getTotalPages();
			while (pageNo <= totalPages) {
				PagingDTO pagingDTO = new PagingDTO();
				pagingDTO.setSessionId(sessionCheck);
				page.setPage(pageNo);
				page.setSize(1000);
				GCMResponse gcmResp = superAdminApi.getGCMIds(page);
				if (gcmResp.isSuccess() && gcmResp.getGcmList().size() > 0) {
					NotificationDTO notificationDTO = new NotificationDTO();
					notificationDTO.setImageGCM(dto.isImageGCM());
					notificationDTO.setTitle(dto.getTitle());
					notificationDTO.setMessage(dto.getMessage());
					if (dto.isImageGCM()) {
						notificationDTO.setImage(com.gcm.utils.APIConstants.HOST_NAME + parts[1]);
					}
					System.err.println("size::" + gcmResp.getGcmList().size());
					notificationDTO.setRegsitrationIds(gcmResp.getGcmList());
					notificationApi.sendNotification(notificationDTO);
					pageNo += 1;
				} else {
					break;
				}
			}
			return "/SuperAdmin/SendNotification";
		} else {
			model.addAttribute("error", error);
			return "/SuperAdmin/SendNotification";
		}
	}*/
	
	
	@RequestMapping(method = RequestMethod.POST, value = "/SendNotification")
	public String processNotification(@ModelAttribute GCMRequest dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) {
		int pageNo = 0;
		int size = 1000;
		String status = "";
		String[] parts = new String[5];
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if(!CommonValidation.isNull(sessionCheck)){
			System.err.println(dto.toString());
			GCMError error = gcmValidation.checkError(dto);
			if (error.isValid()) {
				if (dto.isImageGCM()) {
					String rootDirectory = request.getSession().getServletContext().getRealPath("/");
					status = saveLogo(rootDirectory, dto.getGcmImage());
					parts = status.split("\\|");
				}
				if(dto.getNotificationType().equalsIgnoreCase("SingleUser")){
					GCMResponse gcmResponse = superAdminApi.getGCMIdByUserName(dto.getUserName(), sessionCheck);
					if (gcmResponse.isSuccess() && gcmResponse.getGcmList().size() > 0) {
						NotificationDTO notificationDTO = new NotificationDTO();
						notificationDTO.setImageGCM(dto.isImageGCM());
						notificationDTO.setTitle(dto.getTitle());
						notificationDTO.setMessage(dto.getMessage());
						if (dto.isImageGCM()) {
							notificationDTO.setImage(UrlMetadatas.WEBURL + parts[1]);
						}
						notificationDTO.setRegsitrationIds(gcmResponse.getGcmList());
						notificationApi.sendNotification(notificationDTO);
					}
				}else{
					dto.getNotificationType();
					PagingDTO page = new PagingDTO();
					page.setSessionId(sessionCheck);
					page.setPage(pageNo);
					page.setSize(size);
					GCMResponse gcmResponse = superAdminApi.getGCMIds(page);
					long totalPages = gcmResponse.getTotalPages();
					while (pageNo <= totalPages) {
						PagingDTO pagingDTO = new PagingDTO();
						pagingDTO.setSessionId(sessionCheck);
						page.setPage(pageNo);
						page.setSize(1000);
						GCMResponse gcmResp = superAdminApi.getGCMIds(page);
						if (gcmResp.isSuccess() && gcmResp.getGcmList().size() > 0) {
							NotificationDTO notificationDTO = new NotificationDTO();
							notificationDTO.setImageGCM(dto.isImageGCM());
							notificationDTO.setTitle(dto.getTitle());
							notificationDTO.setMessage(dto.getMessage());
							if (dto.isImageGCM()) {
								notificationDTO.setImage(UrlMetadatas.WEBURL + parts[1]);
							}
							notificationDTO.setRegsitrationIds(gcmResp.getGcmList());
							notificationApi.sendNotification(notificationDTO);
							pageNo += 1;
						} else {
							break;
						}
					}
				}
				model.addAttribute(ModelMapKey.MESSAGE, "Notification Sent Successfully.");
				return "/SuperAdmin/SendNotification";
			} else {
				model.addAttribute(ModelMapKey.ERROR, error.getMessage());
				return "/SuperAdmin/SendNotification";
			}
		}else{
			return "/SuperAdmin/Login";
		}
	}
	
	/*new method for send notifcation*/
	
	@RequestMapping(method = RequestMethod.POST, value = "/SendNotifications")
	public String saveGCMNotification(@ModelAttribute GCMRequest dto, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, ModelMap model) {
		System.err.println("inside notification to save in db");
		String status = "";
		String[] parts = new String[5];
		String sessionCheck = (String) session.getAttribute("superAdminSession");
		System.err.println(dto.toString());
		GCMError error = gcmValidation.checkError(dto);
		if (error.isValid()) {
			if (dto.isImageGCM()) {
				String rootDirectory = request.getSession().getServletContext().getRealPath("/");
				System.err.println("rootDirectory ::" + rootDirectory);
				status = saveLogo(rootDirectory, dto.getGcmImage());
				parts = status.split("\\|");
			       }
					CronNotificationDTO notificationDTO = new CronNotificationDTO();
					notificationDTO.setSessionId(sessionCheck);
					notificationDTO.setImageGCM(dto.isImageGCM());
					notificationDTO.setTitle(dto.getTitle());
					notificationDTO.setMessage(dto.getMessage());
					if (dto.isImageGCM()) {
						notificationDTO.setImage(UrlMetadatas.WEBURL + parts[1]);
					}
					GCMResponse gcmResp = superAdminApi.saveGCMIds(notificationDTO);
					model.addAttribute(ModelMapKey.MESSAGE, "Notification Sent Successfully.");
					return "/SuperAdmin/SendNotification";
			}
		return "redirect:/SuperAdmin/Home";
	}

	private String saveLogo(String rootDirectory, MultipartFile image) {
		boolean saved = false;
		String contentType = image.getContentType();
		String[] fileExtension = contentType.split("/");
		String filePath = null;
		if (fileExtension[1].equals("png") || fileExtension[1].equals("jpg") || fileExtension[1].equals("jpeg")) {
			String fileName = String.valueOf(System.currentTimeMillis());
			File dirs = new File(rootDirectory + "/resources/gcm/" + fileName + "." + fileExtension[1]);
			dirs.mkdirs();
			try {
				image.transferTo(dirs);
				filePath = "/resources/gcm/" + fileName + "." + fileExtension[1];
				saved = true;
				return saved + "|" + filePath;
			} catch (IOException e) {
				e.printStackTrace();

			}
		}
		return saved + "|" + filePath;
	}

	// GET Generate Promo Code
/*	@RequestMapping(method = RequestMethod.GET, value = "/GeneratePromoCode")
	public String generatePromoCode(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) {
		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			ServiceTypeResponse servicesList = superAdminApi.getService();
			if (servicesList != null) {
				if (servicesList.isSuccess()) {
					model.addAttribute("services", servicesList.getServicesDTOs());
				}
			}
			model.addAttribute("promoCodeRequest", new PromoCodeRequest());
			return "SuperAdmin/GeneratePromoCode";
		}
		return "redirect:/SuperAdmin/Home";
	}*/
	
	@RequestMapping(method = RequestMethod.GET, value = "/GeneratePromoCode")
	public String getServiceTypes(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) {
		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			model.addAttribute("promoCodeRequest", new PromoCodeRequest());
			return "SuperAdmin/GeneratePromoCode2";
		}
		return "redirect:/SUperAdmin/Home";
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/GetServiceType")
	public ResponseEntity<ServiceTypeResponse> generatePromoCode(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) {
			ServiceTypeResponse servicesList = superAdminApi.getServiceType();
			model.addAttribute("promoCodeRequest", new PromoCodeRequest());
		return new ResponseEntity<ServiceTypeResponse>(servicesList,HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/GetServices")
	public ResponseEntity<ServiceTypeResponse> getservices(HttpServletRequest request,@RequestBody PromoCodeRequest promoCode, HttpServletResponse response, HttpSession session,
			ModelMap model) {
			ServiceTypeResponse servicesList = superAdminApi.getServicesById(promoCode);
			if (servicesList != null) {
				if (servicesList.isSuccess()) {
					servicesList.setServiceList(servicesList.getServicesDTOs());
					model.addAttribute("services", servicesList.getServiceDTOs());
				}
			}
			return new ResponseEntity<ServiceTypeResponse>(servicesList,HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/GeneratePromoCode")
	public String generatePromoCodeFrom(@ModelAttribute("promoCodeRequest") PromoCodeRequest promoCode,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model) {
		String sessionId = (String) session.getAttribute("superAdminSession");
		String startDateTime = promoCode.getStartDate();
		String endDateTime = promoCode.getEndDate();
		promoCode.setStartDate(startDateTime);
		promoCode.setEndDate(endDateTime);
		if (sessionId != null) {
			PromoCodeError error = promoCodeValidation.checkRequest(promoCode);
			if (error.isValid()) {
				System.err.println("error is valid");
				promoCode.setSessionId(sessionId);
				PromoCodeResponse resp = promoCodeApi.generateRequestOfSuperAdmin(promoCode);
				model.addAttribute("resp", resp);
				model.addAttribute("respCode",resp.getCode());
				return "SuperAdmin/GeneratePromoCode2";
			}
			model.addAttribute("error", error);
			return "SuperAdmin/GeneratePromoCode2";
		}
		return "redirect:/SuperAdmin/Home";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/PromoCodeList")
	public String promoCodeList(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model, SessionDTO dto) throws JSONException {

		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			dto.setSessionId(sessionCheck);
			PromoCodeResponse resp = promoCodeApi.listPromoOfSuperAdmin(dto);
			JSONArray arr = (JSONArray) resp.getDetails();
			List<PromoCodeRequest> list = new ArrayList<PromoCodeRequest>();
			for (int i = 0; i < arr.length(); i++) {
				PromoCodeRequest re = new PromoCodeRequest();
				JSONObject jsonobject = arr.getJSONObject(i);
				re.setPromoCode(jsonobject.getString("promoCode"));
				re.setEndDate(jsonobject.getString("endDate"));
				re.setStartDate(jsonobject.getString("startDate"));
				re.setTerms(jsonobject.getString("terms"));
				re.setDescription(jsonobject.getString("description"));
				re.setFixed(jsonobject.getBoolean("fixed"));
				re.setValue(jsonobject.getDouble("value"));
				list.add(re);
			}
			model.addAttribute("list", list);
			return "/SuperAdmin/PromoCodeList";
		}
		return "/SuperAdmin/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/UpdateServices")
	ResponseEntity<ServicesDTO> updateSerivce(@ModelAttribute ServiceRequest dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) throws JSONException {

		ServicesDTO result = new ServicesDTO();
		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionCheck);
					result = superAdminApi.updateServiceList(dto);
				}
			}
		}
		return new ResponseEntity<ServicesDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/UpdateVersions")
	ResponseEntity<VersionDTO> updateVersions(@ModelAttribute VersionRequest dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) throws JSONException {

		VersionDTO result = new VersionDTO();
		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionCheck);
					result = superAdminApi.updateVersionList(dto);
				}
			}
		}
		return new ResponseEntity<VersionDTO>(result, HttpStatus.OK);
	}

	// Search User no
	@RequestMapping(method = RequestMethod.POST, value = "/Search")
	ResponseEntity<MobileSearchResponse> searchUser(@ModelAttribute MobileSearchRequest dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) throws JSONException {

		MobileSearchResponse result = new MobileSearchResponse();
		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionCheck);
					result = superAdminApi.getPossiblities(dto);
				}
			}
		}
		return new ResponseEntity<MobileSearchResponse>(result, HttpStatus.OK);
	}

	// GET Send SMS Page
	@RequestMapping(method = RequestMethod.GET, value = "/SendSMS")
	public String sendSMS(@RequestParam(value = ModelMapKey.MESSAGE, required = false) String msg,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model)
					throws JSONException {

		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					SMSRequest smsRequest = new SMSRequest();
					model.addAttribute("smsRequest", smsRequest);
					if (msg != null) {
						model.addAttribute(ModelMapKey.MESSAGE, msg);
					}
					return "SuperAdmin/SendSMS";
				}
			}
		}
		return "redirect:/SuperAdmin/Home";
	}

	// GET Transaction Filter Page
	@RequestMapping(method = RequestMethod.GET, value = "/FilterTransactions")
	public String transactionFilter(@RequestParam(value = ModelMapKey.MESSAGE, required = false) String msg,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model)
					throws JSONException {

		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					SessionDTO dto = new SessionDTO();
					dto.setSessionId(sessionCheck);
					ServiceListDTO resp = superAdminApi.getServiceTypeList(dto);
					TFilterDTO tFilterDTO = new TFilterDTO();
					model.addAttribute("tFilterDTO", tFilterDTO);
					model.addAttribute("serviceMap", resp.getServiceMap());
					if (msg != null) {
						model.addAttribute(ModelMapKey.MESSAGE, msg);
					}
					return "SuperAdmin/FilterTransactions";
				}
			}
		}
		return "redirect:/SuperAdmin/Home";
	}

	// GET Transaction Refund Filter Page
	@RequestMapping(method = RequestMethod.GET, value = "/FilterRefundTransactions")
	public String transactionFilterRefund(@RequestParam(value = ModelMapKey.MESSAGE, required = false) String msg,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model)
					throws JSONException {

		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					TFilterDTO tFilterDTO = new TFilterDTO();
					model.addAttribute("tFilterDTO", tFilterDTO);
					if (msg != null) {
						model.addAttribute(ModelMapKey.MESSAGE, msg);
					}
					return "SuperAdmin/FilterRefundTransactions";
				}
			}
		}
		return "redirect:/SuperAdmin/Home";
	}

	// POST Transaction Refund Filter Page
	@RequestMapping(method = RequestMethod.POST, value = "/FilterRefundTransactions")
	public String transactionFilterRefundProcess(@ModelAttribute("tFilterDTO") TFilterDTO tFilterDTO,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model)
					throws JSONException {

		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					tFilterDTO.setSessionId(sessionCheck);
					switch (tFilterDTO.getServiceType()) {
					case "Load Money":
						tFilterDTO.setStatus(Status.Initiated);
						break;
					case "UPI Load Money":
						tFilterDTO.setServiceType("Load Money");
						tFilterDTO.setStatus(Status.Failed);
						break;
					/*case "Bill Payment":
						tFilterDTO.setStatus(Status.Success);
						break;*/
					case "Fund Transfer":
						tFilterDTO.setStatus(Status.Success);
						break;
					case "Fund Transfer Bank":
						tFilterDTO.setStatus(Status.Success);
						break;
					case "Travel Wallet":
						tFilterDTO.setStatus(Status.Success);
						break;
					default:
						break;
					}
					TListResponse resp = superAdminApi.getFilteredTransactionList(tFilterDTO);
					if (resp.isSuccess()) {
						model.addAttribute(ModelMapKey.MESSAGE, tFilterDTO.getServiceType() + " Transactions  from "
								+ tFilterDTO.getStartDate() + " to " + tFilterDTO.getEndDate());
						model.addAttribute("transactionLists", resp.getList());
					} else {
						model.addAttribute(ModelMapKey.ERROR, resp.getMessage());
					}
					return "SuperAdmin/RefundReport";
				}
			}
		}
		return "redirect:/SuperAdmin/Home";
	}

	// GET Transaction Refund Filter Page
	@RequestMapping(method = RequestMethod.GET, value = "/FilterRefundTransactionsOfBillPayment")
	public String transactionFilterRefundBillPay(
			@RequestParam(value = ModelMapKey.MESSAGE, required = false) String msg, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) throws JSONException {

		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					TFilterDTO tFilterDTO = new TFilterDTO();
					model.addAttribute("tFilterDTO", tFilterDTO);
					if (msg != null) {
						model.addAttribute(ModelMapKey.MESSAGE, msg);
					}
					return "SuperAdmin/FilterBillpayRefundTransaction";
				}
			}
		}
		return "redirect:/SuperAdmin/Home";
	}

	// POST Transaction Refund Filter Page
	@RequestMapping(method = RequestMethod.POST, value = "/FilterRefundTransactionsOfBillPayment")
	public String transactionFilterRefundProcessOfBillPay(@ModelAttribute("tFilterDTO") TFilterDTO tFilterDTO,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model)
					throws JSONException {

		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					tFilterDTO.setSessionId(sessionCheck);
					switch (tFilterDTO.getServiceType()) {
					case "Bill Payment":
						tFilterDTO.setStatus(Status.Success);
						break;
					default:
						break;
					}
					TListResponse resp = superAdminApi.getFilteredTransactionListOfBillPay(tFilterDTO);
					if (resp.isSuccess()) {
						model.addAttribute(ModelMapKey.MESSAGE, tFilterDTO.getServiceType() + " Transactions  from "
								+ tFilterDTO.getStartDate() + " to " + tFilterDTO.getEndDate());
						model.addAttribute("transactionList", resp.getList());
					} else {
						model.addAttribute(ModelMapKey.ERROR, resp.getMessage());
					}
					return "SuperAdmin/RefundBillPayReport";
				}
			}
		}
		return "redirect:/SuperAdmin/Home";
	}

	// POST Transaction Filter Page
	@RequestMapping(method = RequestMethod.POST, value = "/FilterTransactions")
	public String transactionFilterProcess(@ModelAttribute("tFilterDTO") TFilterDTO tFilterDTO,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model)
					throws JSONException {

		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					tFilterDTO.setSessionId(sessionCheck);
					TListResponse resp = superAdminApi.getFilteredTransactionList(tFilterDTO);
					if (resp.isSuccess()) {
						model.addAttribute(ModelMapKey.MESSAGE, tFilterDTO.getServiceType() + " Transactions  from "
								+ tFilterDTO.getStartDate() + " to " + tFilterDTO.getEndDate());
						model.addAttribute("transactionList", resp.getList());
					} else {
						model.addAttribute(ModelMapKey.ERROR, resp.getMessage());
					}
					return "SuperAdmin/DailyReport";
				}
			}
		}
		return "redirect:/SuperAdmin/Home";
	}

	// Refund Transaction Process
	@RequestMapping(method = RequestMethod.GET, value = "/{transactionRefNo}/Refund")
	ResponseEntity<TransactionUserResponse> refundTransaction(
			@PathVariable(value = "transactionRefNo") String transactionRefNo, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) {
		RefundDTO dto = new RefundDTO();
		TransactionUserResponse result = new TransactionUserResponse();
		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionCheck);
					dto.setTransactionRefNo(transactionRefNo);
					result = superAdminApi.refundTransaction(dto);
				}
			}
		}
		return new ResponseEntity<TransactionUserResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{transactionRefNo}/RefundBillPayment")
	ResponseEntity<TransactionUserResponse> refundTransactionBillPay(
			@PathVariable(value = "transactionRefNo") String transactionRefNo, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) {
		RefundDTO dto = new RefundDTO();
		TransactionUserResponse result = new TransactionUserResponse();
		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionCheck);
					dto.setTransactionRefNo(transactionRefNo);
					result = superAdminApi.refundTransactionBillpay(dto);
				}
			}
		}
		return new ResponseEntity<TransactionUserResponse>(result, HttpStatus.OK);
	}

	// POST Send SMS Page
	@RequestMapping(method = RequestMethod.POST, value = "/SendSMS")
	public String sendSMSProcess(@ModelAttribute("smsRequest") @Valid SMSRequest smsRequest, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) throws JSONException {

		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					if (result.hasErrors()) {
						return "SuperAdmin/SendSMS";
					} else {
						smsRequest.setSessionId(sessionCheck);
						SMSResponse resp = superAdminApi.sendSingleSMS(smsRequest);
						return "redirect:/SuperAdmin/SendSMS?msg=" + resp.getMessage();
					}
				}
			}
		}
		return "redirect:/SuperAdmin/Home";
	}

	// GET Send Bulk SMS Page
	@RequestMapping(method = RequestMethod.GET, value = "/SendBulkSMS")
	public String sendBulkSMS(@RequestParam(value = ModelMapKey.MESSAGE, required = false) String msg,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model)
					throws JSONException {
		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					BulkSMSRequest bulkSMSRequest = new BulkSMSRequest();
					model.addAttribute("bulkSMSRequest", bulkSMSRequest);
					if (msg != null) {
						model.addAttribute(ModelMapKey.MESSAGE, msg);
					}
					return "SuperAdmin/SendBulkSMS";
				}
			}
		}
		return "redirect:/SuperAdmin/Home";
	}

	// POST Send Bulk SMS Page
	@RequestMapping(method = RequestMethod.POST, value = "/SendBulkSMS")
	public String sendBulkSMS(@ModelAttribute("bulkSMSRequest") @Valid BulkSMSRequest bulkSMSRequest,
			BindingResult result, HttpServletRequest request, HttpServletResponse response, HttpSession session,
			Model model) throws JSONException {
		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					if (result.hasErrors()) {
						return "SuperAdmin/SendBulkSMS";
					} else {
						bulkSMSRequest.setSessionId(sessionCheck);
						SMSResponse resp = superAdminApi.sendBulkSMS(bulkSMSRequest);
						return "redirect:/SuperAdmin/SendBulkSMS?msg=" + resp.getMessage();
					}
				}
			}
		}
		return "redirect:/SuperAdmin/Home";
	}

	// GET Transaction List AJAX

	@RequestMapping(method = RequestMethod.GET, value = "/TransactionReportJSON")
	ResponseEntity<TListResponse> getTransactionListAjax(HttpServletRequest request, HttpServletResponse response,
			HttpSession session, ModelMap model) {
		TListResponse result = new TListResponse();
		AllTransactionRequest dto = new AllTransactionRequest();
		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionCheck);
					result = superAdminApi.getTransactionList(dto);
					List<TListDTO> tlist = result.getList();
					result.setList(tlist.subList(0, Math.min(tlist.size(), 5)));
				}
			}
		}
		return new ResponseEntity<TListResponse>(result, HttpStatus.OK);
	}

	// LOGOUT

	@RequestMapping(method = RequestMethod.GET, value = "/Logout")
	public String userLogout(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			RedirectAttributes modelMap) {
		String adminSessionId = (String) session.getAttribute("superAdminSession");
		LogoutRequest logout = new LogoutRequest();
		logout.setSessionId(adminSessionId);
		if (adminSessionId != null && adminSessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(adminSessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					LogoutResponse resp = logoutApi.logout(logout, Role.SUPERADMIN);
					modelMap.addFlashAttribute(ModelMapKey.MESSAGE, "You've successfully logged out");
					session.invalidate();
					return "redirect:/SuperAdmin/Home";
				}
			}
		}
		return "redirect:/SuperAdmin/Home";
	}

	// GET Send MAIL Page
	@RequestMapping(method = RequestMethod.GET, value = "/SendMail")
	public String sendMail(@RequestParam(value = ModelMapKey.MESSAGE, required = false) String msg,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model)
					throws JSONException {

		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					MailRequest mailRequest = new MailRequest();
					model.addAttribute("mailRequest", mailRequest);
					if (msg != null) {
						model.addAttribute(ModelMapKey.MESSAGE, msg);
					}
					return "SuperAdmin/SendMail";
				}
			}
		}
		return "redirect:/SuperAdmin/Home";
	}

	// GET Update Access Page
	@RequestMapping(method = RequestMethod.GET, value = "/UpdateAccess")
	public String getUpdateAccess(@RequestParam(value = ModelMapKey.MESSAGE, required = false) String msg,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model)
					throws JSONException {

		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					AccessDTO accessDTO = new AccessDTO();
					model.addAttribute("accessDTO", accessDTO);
					if (msg != null) {
						model.addAttribute(ModelMapKey.MESSAGE, msg);
					}
					return "SuperAdmin/UpdateAccess";
				}
			}
		}
		return "redirect:/SuperAdmin/Home";
	}

	// POST Update Access Page
	@RequestMapping(method = RequestMethod.POST, value = "/UpdateAccess")
	public String updateAccessProcess(@ModelAttribute("accessDTO") AccessDTO accessDTO, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws JSONException {

		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					accessDTO.setSessionId(sessionCheck);
					UpdateAccessResponse resp = userApi.updateAccessFromSuperAdmin(accessDTO);
					if (resp.isSuccess()) {
						return "redirect:/SuperAdmin/AccessList?msg=" + resp.getMessage();
					} else {
						return "redirect:/SuperAdmin/AccessList?error=" + resp.getMessage();
					}
				}
			}
		}
		return "redirect:/SuperAdmin/Home";
	}

	// POST Send MAIL Page
	@RequestMapping(method = RequestMethod.POST, value = "/SendMail")
	public String sendSMSProcess(@ModelAttribute("mailRequest") @Valid MailRequest mailRequest, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) throws JSONException {

		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					if (result.hasErrors()) {
						return "SuperAdmin/SendMail";
					} else {
						mailRequest.setSessionId(sessionCheck);
						MailResponse resp = superAdminApi.sendSingleMail(mailRequest);
						return "redirect:/SuperAdmin/SendMail?msg=" + resp.getMessage();
					}
				}
			}
		}
		return "redirect:/SuperAdmin/Home";
	}

	// GET Send Bulk MAIL Page
	@RequestMapping(method = RequestMethod.GET, value = "/SendBulkMail")
	public String sendBulkMail(@RequestParam(value = ModelMapKey.MESSAGE, required = false) String msg,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model)
					throws JSONException {

		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					BulkMailRequest bulkMailRequest = new BulkMailRequest();
					model.addAttribute("bulkMailRequest", bulkMailRequest);
					if (msg != null) {
						model.addAttribute(ModelMapKey.MESSAGE, msg);
					}
					return "SuperAdmin/SendBulkMail";
				}
			}
		}
		return "redirect:/SuperAdmin/Home";
	}

	// POST Send Bulk MAIL Page
	@RequestMapping(method = RequestMethod.POST, value = "/SendBulkMail")
	public String sendBulkMailProcess(@ModelAttribute("mailRequest") @Valid BulkMailRequest bulkMailRequest,
			BindingResult result, HttpServletRequest request, HttpServletResponse response, HttpSession session)
					throws JSONException {

		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					if (result.hasErrors()) {
						return "SuperAdmin/SendBulkMail";
					} else {
						bulkMailRequest.setSessionId(sessionCheck);
						MailResponse resp = superAdminApi.sendBulkMail(bulkMailRequest);
						return "redirect:/SuperAdmin/SendBulkMail?msg=" + resp.getMessage();
					}
				}
			}
		}
		return "redirect:/SuperAdmin/Login";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/ListMerchant")
	public String getListMerchant(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) {

		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String adminAuthority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (adminAuthority != null) {
				if (adminAuthority.contains(Authorities.SUPER_ADMIN)
						&& adminAuthority.contains(Authorities.AUTHENTICATED)) {
					AllUserRequest userRequest = new AllUserRequest();
					userRequest.setPage(0);
					userRequest.setSize(100000);
					userRequest.setSessionId(sessionCheck);
					userRequest.setStatus(UserStatus.ALL);
					AllUserResponse userResponse = superAdminApi.getAllMerchants(userRequest);
					JSONArray data = userResponse.getJsonArray();
					List<AdminUserDetails> userList = new ArrayList<AdminUserDetails>();
					try {
						for (int i = 0; i < data.length(); i++) {
							AdminUserDetails list = new AdminUserDetails();
							JSONObject json = data.getJSONObject(i);
							list.setContactNo(JSONParserUtil.getString(json, "contactNo"));
							list.setDateOfAccountCreation(JSONParserUtil.getString(json, "dateOfRegistration"));
							list.setEmail(JSONParserUtil.getString(json, "email"));
							list.setFirstName(JSONParserUtil.getString(json, "name"));
							list.setImage(UrlMetadatas.WEBURL + JSONParserUtil.getString(json, "image"));
							userList.add(list);
						}
						model.put("userlist", userList);
					} catch (JSONException e) {
						e.printStackTrace();
					}
					return "SuperAdmin/ListMerchant";
				}
			}
		}
		return "redirect:/SuperAdmin/Home";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{username}/Info")
	public String getByUsername(@PathVariable("username") String username, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model, Throwable userInfoResp) {
		System.err.println("mobile ::" + username);
		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String adminAuthority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (adminAuthority != null) {
				if (adminAuthority.contains(Authorities.SUPER_ADMIN)
						&& adminAuthority.contains(Authorities.AUTHENTICATED)) {
					if (username != null || username != "") {
						UserInfoRequest info = new UserInfoRequest();
						info.setSessionId(sessionCheck);
						info.setUsername(username);
						UserInfoResponse userInfoResponse = superAdminApi.getUserInfo(info);
						if (userInfoResponse.isSuccess()) {
							model.addAttribute("transactions", userInfoResponse.getTransactions());
							model.addAttribute("loginLogs", userInfoResponse.getLoginLogs());
							model.addAttribute("basic", userInfoResponse.getBasic());
							model.addAttribute("account", userInfoResponse.getAccount());
							model.addAttribute("online", userInfoResponse.isOnline());
							model.addAttribute("totalCredit", userInfoResponse.getTotalCredit());
							model.addAttribute("totalDebit", userInfoResponse.getTotalDebit());
							return "SuperAdmin/User";
						} else if (userInfoResponse.getCode().equalsIgnoreCase("F05")) {
							model.addAttribute("msg", userInfoResponse.getMessage());
							return "SuperAdmin/User";
						} else {
							return "redirect:/SuperAdmin/Home?msg=" + userInfoResp.getMessage();
						}
					} 
				}
			}
		}
		return "redirect:/SuperAdmin/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/SearchUser")
	public String getByUsername(@ModelAttribute SearchDTO dto, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, ModelMap model) {
		System.err.println("mobile ::" + dto.getMobile());
		return "redirect:/SuperAdmin/" + dto.getMobile() + "/Info";

	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/User/{userName}")
	public String getByUsername(@PathVariable String userName, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, ModelMap model) {
		return "redirect:/SuperAdmin/" + userName + "/Info";

	}

	@RequestMapping(method = RequestMethod.GET, value = "/AccountsList")
	public String getListAccountType(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model, SessionDTO dto) throws JSONException {

		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String adminAuthority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (adminAuthority != null) {
				if (adminAuthority.contains(Authorities.SUPER_ADMIN)
						&& adminAuthority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionCheck);
					AccountTypeResponse resp = superAdminApi.getListAccountType(dto);
					JSONArray arr = (JSONArray) resp.getJsonArray();
					List<AccountTypeResponse> list = new ArrayList<AccountTypeResponse>();
					for (int i = 0; i < arr.length(); i++) {
						AccountTypeResponse re = new AccountTypeResponse();
						JSONObject jsonobject = arr.getJSONObject(i);
						re.setName(jsonobject.getString("name"));
						re.setCode(jsonobject.getString("code"));
						re.setDescription(jsonobject.getString("description"));
						re.setTransactionLimit(jsonobject.getInt("transactionLimit"));
						re.setMonthlyLimit(jsonobject.getDouble("monthlyLimit"));
						re.setDailyLimit(jsonobject.getDouble("dailyLimit"));
						re.setBalanceLimit(jsonobject.getDouble("balanceLimit"));
						list.add(re);
					}
					model.addAttribute("list", list);
					return "SuperAdmin/AccountsList";
				}
			}
		}
		return "redirect:/SuperAdmin/Home";
	}

	@RequestMapping(value = "/EditLimit", method = RequestMethod.POST)
	ResponseEntity<AccountTypeResponse> updateLimit(@ModelAttribute AccountTypeRequest dto, HttpServletRequest request,
			HttpSession session, ModelMap model, HttpServletResponse response) throws JSONException {
		AccountTypeResponse resp = new AccountTypeResponse();
		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String adminAuthority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (adminAuthority != null) {
				if (adminAuthority.contains(Authorities.SUPER_ADMIN)
						&& adminAuthority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionCheck);
					resp = superAdminApi.updateAccountType(dto);
					JSONArray arr = (JSONArray) resp.getJsonArray();
					for (int i = 0; i < arr.length(); i++) {
						JSONObject jsonobject = arr.getJSONObject(i);
						if (dto.getCode().equalsIgnoreCase(jsonobject.getString("code"))) {
							resp.setCode(jsonobject.getString("code"));
							resp.setName(jsonobject.getString("name"));
							resp.setDescription(jsonobject.getString("description"));
							resp.setTransactionLimit(jsonobject.getInt("transactionLimit"));
							resp.setMonthlyLimit(jsonobject.getDouble("monthlyLimit"));
							resp.setDailyLimit(jsonobject.getDouble("dailyLimit"));
							resp.setBalanceLimit(jsonobject.getDouble("balanceLimit"));

						}
					}
					resp.setJsonObject(null);
					resp.setJsonArray(null);
				}
			}
		}
		return new ResponseEntity<AccountTypeResponse>(resp, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/BankTransfer")
	public String getUserBankTransferList(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) throws JSONException {

		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String adminAuthority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (adminAuthority != null) {
				if (adminAuthority.contains(Authorities.SUPER_ADMIN)
						&& adminAuthority.contains(Authorities.AUTHENTICATED)) {
					SessionDTO dto = new SessionDTO();
					dto.setSessionId(sessionCheck);
					List<NEFTResponse> list = superAdminApi.getUserNEFTList(dto);
					model.addAttribute("neftList", list);
					return "SuperAdmin/BankTransfer";
				}
			}
		}
		return "redirect:/SuperAdmin/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/BankTransfer")
	public String getUserBankTransferListFilter(@ModelAttribute FilterDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) throws JSONException {

		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String adminAuthority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (adminAuthority != null) {
				if (adminAuthority.contains(Authorities.SUPER_ADMIN)
						&& adminAuthority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionCheck);
					List<NEFTResponse> list = superAdminApi.getUserNEFTListFilter(dto);
					model.addAttribute("neftList", list);
					return "SuperAdmin/BankTransfer";
				}
			}
		}
		return "redirect:/SuperAdmin/Home";
	}

	@RequestMapping(value = "/User/Block/{userName}", method = RequestMethod.GET)
	public String userBlock(ModelMap modelMap, @PathVariable String userName, HttpSession session, Model model) {
		LogCat.print("Block Users :: " + userName);
		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					BlockUnBlockUserRequest req = new BlockUnBlockUserRequest();
					req.setSessionId(sessionCheck);
					req.setUsername(userName);
					BlockUnBlockUserResponse resp = superAdminApi.blockUser(req);
					model.addAttribute("message", resp.getMessage());
					return "redirect:/SuperAdmin/" + userName + "/Info";
				}
			}
		}
		return "redirect:/SuperAdmin/Home";
	}

	@RequestMapping(value = "/User/Unblock/{userName}", method = RequestMethod.GET)
	public String userUnBlock(ModelMap modelMap, @PathVariable String userName, HttpSession session, Model model) {
		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					BlockUnBlockUserRequest req = new BlockUnBlockUserRequest();
					req.setSessionId(sessionCheck);
					req.setUsername(userName);
					BlockUnBlockUserResponse resp = superAdminApi.unblockUser(req);
					model.addAttribute("message", resp.getMessage());
					return "redirect:/SuperAdmin/" + userName + "/Info";
				}
			}
		}
		return "/SuperAdmin/Home";
	}

	@RequestMapping(value = "/User/KycUpdate/{userName}", method = RequestMethod.GET)
	public String userKycUpdate(ModelMap modelMap, @PathVariable String userName, HttpSession session, Model model) {
		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					BlockUnBlockUserRequest req = new BlockUnBlockUserRequest();
					System.err.println("Mobile No ::" + userName);
					req.setSessionId(sessionCheck);
					req.setUsername(userName);
					BlockUnBlockUserResponse resp = superAdminApi.kycUpdate(req);
					model.addAttribute("message", resp.getMessage());
					return "redirect:/SuperAdmin/" + userName + "/Info";
				}
			}
		}
		return "/SuperAdmin/Home";
	}

	@RequestMapping(value = "/User/Non-KycUpdate/{userName}", method = RequestMethod.GET)
	public String userNonKycUpdate(ModelMap modelMap, @PathVariable String userName, HttpSession session, Model model) {
		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					BlockUnBlockUserRequest req = new BlockUnBlockUserRequest();
					System.err.println("Mobile No ::" + userName);
					req.setSessionId(sessionCheck);
					req.setUsername(userName);
					BlockUnBlockUserResponse resp = superAdminApi.NonKycUpdate(req);
					model.addAttribute("message", resp.getMessage());
					return "redirect:/SuperAdmin/" + userName + "/Info";
				}
			}
		}
		return "/SuperAdmin/Home";
	}

	// GET Change Password Page
	@RequestMapping(method = RequestMethod.GET, value = "/ChangePassword")
	public String getChangePassword(@RequestParam(value = ModelMapKey.MESSAGE, required = false) String msg,
			@RequestParam(value = ModelMapKey.ERROR, required = false) String error, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) throws JSONException {

		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					CPasswordRequest cPasswordRequest = new CPasswordRequest();
					model.addAttribute("cPasswordRequest", cPasswordRequest);
					if (msg != null) {
						model.addAttribute(ModelMapKey.MESSAGE, msg);
					}
					if (error != null) {
						model.addAttribute(ModelMapKey.ERROR, error);
					}
					return "SuperAdmin/ChangePassword";
				}
			}
		}
		return "redirect:/SuperAdmin/Login";
	}

	// GET NOTIFICATIONS API
	@RequestMapping(method = RequestMethod.GET, value = "/Notifications")
	ResponseEntity<NotificationsDTO> getNotifications(
			@RequestParam(value = ModelMapKey.MESSAGE, required = false) String msg,
			@RequestParam(value = ModelMapKey.ERROR, required = false) String error, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) throws JSONException {

		NotificationsDTO dto = new NotificationsDTO();
		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					BalanceResponse balanceResponse = balanceApi.request();
					if (balanceResponse.isSuccess()) {
						Balance balance = balanceResponse.getBalance();
						dto.setIpayBalance(balance.getWallet());
					} else {
						dto.setIpayBalance("");
					}
				}
			}
		}
		return new ResponseEntity<NotificationsDTO>(dto, HttpStatus.OK);
	}

	// GET IPAY TRANSACTION DETAILS API
	@RequestMapping(method = RequestMethod.GET, value = "/TransactionCheck/{refNo}")
	ResponseEntity<StatusCheck> getIpayTransactionStatus(@PathVariable("refNo") String refNo,
			@RequestParam(value = ModelMapKey.MESSAGE, required = false) String msg,
			@RequestParam(value = ModelMapKey.ERROR, required = false) String error, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model) throws JSONException {

		StatusCheck dto = new StatusCheck();
		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					String transactionRefNo = refNo.substring(0, refNo.length() - 1);
					StatusCheckRequest statusCheckRequest = new StatusCheckRequest();
					statusCheckRequest.setAgentId(transactionRefNo);
					StatusCheckResponse resp = statusCheckApi.request(statusCheckRequest);
					if (resp.isSuccess()) {
						dto = resp.getStatusCheck();
					}
				}
			}
		}
		return new ResponseEntity<StatusCheck>(dto, HttpStatus.OK);
	}

	// POST ChangePassword Page
	@RequestMapping(method = RequestMethod.POST, value = "/ChangePassword")
	public String requestChangePassword(@ModelAttribute("cPasswordRequest") @Valid CPasswordRequest cPasswordRequest,
			BindingResult result, HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap modelMap) throws JSONException {

		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					if (result.hasErrors()) {
						return "SuperAdmin/ChangePassword";
					} else {
						cPasswordRequest.setSessionId(sessionCheck);
						CPasswordResponse resp = superAdminApi.requestChangePassword(cPasswordRequest);
						if (resp.isSuccess()) {
							CPasswordOTPRequest otpRequest = new CPasswordOTPRequest();
							otpRequest.setCurrentPassword(cPasswordRequest.getCurrentPassword());
							otpRequest.setNewPassword(cPasswordRequest.getNewPassword());
							modelMap.addAttribute(ModelMapKey.MESSAGE, resp.getMessage());
							modelMap.addAttribute("otpRequest", otpRequest);
							return "SuperAdmin/ChangePasswordOTP";
						} else {
							if (resp.getCode().equalsIgnoreCase("F04")) {
								return "redirect:/SuperAdmin/ChangePassword?error=" + resp.getPassword();
							} else {
								return "redirect:/SuperAdmin/ChangePassword?error=" + resp.getMessage();
							}
						}
					}
				}
			}
		}
		return "redirect:/SuperAdmin/Login";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/ChangePasswordOTP")
	public String processChangePasswordOTP(@ModelAttribute("otpRequest") @Valid CPasswordOTPRequest otpRequest,
			BindingResult result, HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap map) throws JSONException {
		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					if (result.hasErrors()) {
						return "SuperAdmin/ChangePasswordOTP";
					} else {
						otpRequest.setSessionId(sessionCheck);
						CPasswordResponse resp = superAdminApi.processChangePassword(otpRequest);
						if (resp.isSuccess()) {
							return "redirect:/SuperAdmin/ChangePassword?msg=" + resp.getMessage();
						} else {
							map.addAttribute(ModelMapKey.ERROR, resp.getMessage());
							return "SuperAdmin/ChangePasswordOTP";
						}
					}
				}
			}
		}
		return "redirect:/SuperAdmin/Login";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/BulkFileList")
	public String getBulkFile(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) {
		SessionDTO dto = new SessionDTO();
		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionCheck);
					BulkFileListResponse listResponse = superAdminApi.getBulkFile(dto);
					if (listResponse.isSuccess()) {
						model.addAttribute("bulkFile", listResponse.getList());
					} else {
						model.addAttribute(ModelMapKey.ERROR, listResponse.getMessage());
					}
					model.addAttribute(ModelMapKey.ERROR, "Their is no file to approve .");
					return "SuperAdmin/BulkFileList";
				}
			}
		}
		return "redirect:/SuperAdmin/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/BulkFileRecord")
	ResponseEntity<BulkFileListResponse> saveBulkFile(HttpServletRequest request, HttpServletResponse response,
			HttpSession session, ModelMap model, @RequestBody BulkFileUpload dto) {
		String sessionCheck = (String) session.getAttribute("superAdminSession");
		BulkFileListResponse listResponse = new BulkFileListResponse();
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionCheck);
					listResponse = superAdminApi.uploadBulkFile(dto);
				}
			}
		}
		return new ResponseEntity<BulkFileListResponse>(listResponse, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/{username}/Merchant", method = RequestMethod.GET)
	public String merchantTransaction(@PathVariable String username, Model model, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws JSONException {
		double totaltx = 0;
		double totalcommission = 0;
		long count = 0;
		ReceiptsResponse result = new ReceiptsResponse();
		String sessionId = (String) session.getAttribute("superAdminSession");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					ReceiptsRequest receipts = new ReceiptsRequest();
					receipts.setSessionId(sessionId);
					receipts.setPage(0);
					receipts.setSize(10000);
					receipts.setUsername(username);
					result = superAdminApi.getSingleMerchantTransactionList(receipts);
					List<MTransactionResponseDTO> transactionList = new ArrayList<>();
					try {
						JSONObject json = new JSONObject(result.getResponse());
						JSONObject details = JSONParserUtil.getObject(json, "details");
						JSONObject userList = JSONParserUtil.getObject(details, "userList");
						JSONArray userContent = JSONParserUtil.getArray(userList, "content");
						JSONObject contentList = JSONParserUtil.getObject(details, "commissionList");
						JSONArray commissionContent = JSONParserUtil.getArray(contentList, "content");
						if (userContent.length() != 0 && commissionContent.length() != 0) {
							for (int i = 0; i < userContent.length(); i++) {
								MTransactionResponseDTO transaction = new MTransactionResponseDTO();
								JSONObject debitTransaction = userContent.getJSONObject(i);
								if(commissionContent.length() >i){
									JSONObject commissionTransaction = commissionContent.getJSONObject(i);
									totalcommission += JSONParserUtil.getDouble(commissionTransaction, "commission");
									transaction.setCommission(JSONParserUtil.getDouble(commissionTransaction, "commission"));
								}else {
									transaction.setCommission(0.0);
								}
								if (!JSONParserUtil.getString(debitTransaction, "serviceType").equals("NEFT Merchant")) {
									count++;
									totaltx += JSONParserUtil.getDouble(debitTransaction, "amount");
									long milliseconds = JSONParserUtil.getLong(debitTransaction, "dateOfTransaction");
									Calendar calendar = Calendar.getInstance();
									calendar.setTimeInMillis(milliseconds);
									transaction.setDate(dateFilter.format(calendar.getTime()));
									transaction.setTransactionRefNo(JSONParserUtil.getString(debitTransaction, "transactionRefNo"));
									transaction.setCurrentBalance(JSONParserUtil.getDouble(debitTransaction, "currentBalance"));
									transaction.setUsername(debitTransaction.getString("username"));
									transaction.setServiceType(debitTransaction.getString("serviceType"));
									transaction.setAmount(JSONParserUtil.getDouble(debitTransaction, "amount"));
									transaction.setContactNo(JSONParserUtil.getString(debitTransaction, "contactNo"));
									transaction.setEmail(JSONParserUtil.getString(debitTransaction, "email"));
									// transaction.setTotalPages(totalPages);
									transaction.setStatus(Status.valueOf(JSONParserUtil.getString(debitTransaction, "status")));
									transaction.setDescription(JSONParserUtil.getString(debitTransaction, "description"));
									transaction.setMobileNo(JSONParserUtil.getString(debitTransaction, "userMobileNo"));
									transaction.setFirstName(JSONParserUtil.getString(debitTransaction, "userFirstName"));
									transactionList.add(transaction);
								}

							}
							request.setAttribute("totaltx",BigDecimal.valueOf(totaltx).setScale(2, RoundingMode.HALF_UP).doubleValue());
							request.setAttribute("totalcommission",BigDecimal.valueOf(totalcommission).setScale(2, RoundingMode.HALF_UP).doubleValue());
							request.setAttribute("totalnotxs", count);

						}
						model.addAttribute("userList", transactionList);
						return "SuperAdmin/Merchant";
					} catch (JSONException e) {
						e.printStackTrace();
						// return "Admin/Home";
					}
					return "SuperAdmin/Merchant";
				}

			}
		}
		return "redirect:/SuperAdmin/Home";
	}

	@RequestMapping(value = "/{username}/Merchants", method = RequestMethod.POST)
	public String merchantTransactionFilter(@PathVariable String username, Model model, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws JSONException {
		UserListDTO resultSet = new UserListDTO();
		double totaltx = 0;
		long count = 0;
		ReceiptsResponse result = new ReceiptsResponse();
		String sessionId = (String) session.getAttribute("superAdminSession");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					ReceiptsRequest receipts = new ReceiptsRequest();
					receipts.setSessionId(sessionId);
					receipts.setPage(0);
					receipts.setSize(1000000);
					receipts.setUsername(username);
					result = superAdminApi.getSingleMerchantTransactionList(receipts);
					List<MTransactionResponseDTO> transactionList = new ArrayList<>();
					if (result.isSuccess()) {
						try {
							JSONObject json = new JSONObject(result.getResponse());
							JSONObject details = JSONParserUtil.getObject(json, "details");
							JSONObject userList = JSONParserUtil.getObject(details, "userList");
							JSONArray content = JSONParserUtil.getArray(userList, "content");
							if (content != null) {
								for (int i = 0; i < content.length(); i++) {
									JSONObject temp = content.getJSONObject(i);
									if (!JSONParserUtil.getString(temp, "serviceType").equals("NEFT Merchant")) {
										count++;
										boolean debit = JSONParserUtil.getBoolean(temp, "debit");
										totaltx += JSONParserUtil.getDouble(temp, "amount");
										MTransactionResponseDTO transaction = new MTransactionResponseDTO();
										long milliseconds = JSONParserUtil.getLong(temp, "dateOfTransaction");
										Calendar calendar = Calendar.getInstance();
										calendar.setTimeInMillis(milliseconds);
										transaction.setDate(dateFilter.format(calendar.getTime()));
										transaction.setTransactionRefNo(
												JSONParserUtil.getString(temp, "transactionRefNo"));
										transaction.setCurrentBalance(JSONParserUtil.getDouble(temp, "currentBalance"));
										transaction.setUsername(temp.getString("username"));
										transaction.setServiceType(temp.getString("serviceType"));
										transaction.setAmount(JSONParserUtil.getDouble(temp, "amount"));
										transaction.setContactNo(JSONParserUtil.getString(temp, "contactNo"));
										transaction.setEmail(JSONParserUtil.getString(temp, "email"));
										// transaction.setTotalPages(totalPages);
										transaction.setStatus(Status.valueOf(JSONParserUtil.getString(temp, "status")));
										transaction.setDescription(JSONParserUtil.getString(temp, "description"));
										transaction.setMobileNo(JSONParserUtil.getString(temp, "userMobileNo"));
										transaction.setFirstName(JSONParserUtil.getString(temp, "userFirstName"));
										transactionList.add(transaction);
									}
								}
								request.setAttribute("totaltx",
										BigDecimal.valueOf(totaltx).setScale(2, RoundingMode.HALF_UP).doubleValue());
								request.setAttribute("totalnotxs", count);

							}
							JSONObject commissionList = JSONParserUtil.getObject(details, "commissionList");
							JSONArray contentList = JSONParserUtil.getArray(commissionList, "content");
							if (contentList != null) {
								for (int i = 0; i < contentList.length(); i++) {
									JSONObject temp = contentList.getJSONObject(i);
									if (!JSONParserUtil.getString(temp, "serviceType").equals("NEFT Merchant")) {
										MTransactionResponseDTO transaction = new MTransactionResponseDTO();
										transaction.setCommission(JSONParserUtil.getDouble(temp, "commission"));
										transactionList.add(transaction);
									}
								}
							}
							model.addAttribute("userList", transactionList);
							return "SuperAdmin/Merchant";
						} catch (JSONException e) {
							e.printStackTrace();
							// return "Admin/Home";
						}
						return "SuperAdmin/Merchant";
					}

				}
			}
		}
		return "redirect:/SuperAdmin/Home";
	}
	
	
	 @RequestMapping(method = RequestMethod.GET, value = "/TreatCardTransactionReport")
		public String getTreatCardTransactionList(HttpServletRequest request, HttpServletResponse response, HttpSession session,
				ModelMap model) {
			AllTransactionRequest dto = new AllTransactionRequest();
			String sessionCheck = (String) session.getAttribute("superAdminSession");
			if (sessionCheck != null) {
				String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
				if (authority != null) {
					if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
						dto.setSessionId(sessionCheck);
						TListResponse listResponse = superAdminApi.getTreatCardTransactionList(dto);
						if (listResponse.isSuccess()) {
							model.addAttribute("transactionList", listResponse.getList());
						} else {
							model.addAttribute(ModelMapKey.ERROR, listResponse.getMessage());
						}
						return "SuperAdmin/TreatCardReport";
					}
				}
			}
			return "redirect:/SuperAdmin/Home";
		}

		// Get Transaction List using filter

		@RequestMapping(method = RequestMethod.POST, value = "/TreatCardTransactionReport")
		public String getTreatCardTransactionList(@ModelAttribute AllTransactionRequest dto, HttpServletRequest request,
				HttpServletResponse response, HttpSession session, ModelMap model) {
			UserListDTO result = new UserListDTO();
			String sessionCheck = (String) session.getAttribute("superAdminSession");
			if (sessionCheck != null) {
				String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
				if (authority != null) {
					if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
						dto.setSessionId(sessionCheck);
						TListResponse listResponse = superAdminApi.getTreatCardTransactionListWithFilter(dto);
						if (listResponse.isSuccess()) {
							model.addAttribute("transactionList", listResponse.getList());
							model.addAttribute(ModelMapKey.MESSAGE,
									"Transaction List b/w " + dto.getStartDate() + " and " + dto.getEndDate());
						} else {
							model.addAttribute(ModelMapKey.ERROR, listResponse.getMessage());
						}
						return "SuperAdmin/TreatCardReport";
					}
				}
			}
			return "redirect:/SuperAdmin/Home";
		}
		
		@RequestMapping(method = RequestMethod.GET, value = "/TreatCardCount")
		public String getTreatCardCount(HttpServletRequest request, HttpServletResponse response, HttpSession session,
				ModelMap model,SessionDTO dto) throws JSONException {

			String sessionCheck = (String) session.getAttribute("superAdminSession");
			if (sessionCheck != null) {
				String adminAuthority = authenticationApi.getAuthorityFromSession(sessionCheck,Role.USER);
				if(adminAuthority != null) {
					if(adminAuthority.contains(Authorities.SUPER_ADMIN) && adminAuthority.contains(Authorities.AUTHENTICATED)) {
						dto.setSessionId(sessionCheck);
						TreatCardRegisterListResponse resp = superAdminApi.getTreatCardCount(dto);
						JSONArray arr = (JSONArray) resp.getJsonArray();
						List<TreatCardRegisterListResponse> list = new ArrayList<TreatCardRegisterListResponse>();
						for (int i = 0; i < arr.length(); i++) {
							TreatCardRegisterListResponse re = new TreatCardRegisterListResponse();
							JSONObject json = arr.getJSONObject(i);
							re.setMembershipCode(JSONParserUtil.getLong(json,"membershipCode"));
							re.setUsername(JSONParserUtil.getString(json,"username"));
							re.setEmailId(JSONParserUtil.getString(json,"emailId"));
							re.setCount(JSONParserUtil.getLong(json,"count"));
							re.setCreated(JSONParserUtil.getString(json,"created"));
							list.add(re);
						}
						model.addAttribute("list", list);
						return "SuperAdmin/TreatCardCount";
					}
				}
			}
			return "redirect:/SuperAdmin/Home";
		}
		
		
		@RequestMapping(method = RequestMethod.GET, value = "/TreatCardReport")
		public String getTreatCardReport(HttpServletRequest request, HttpServletResponse response, HttpSession session,
				ModelMap model,SessionDTO dto) throws JSONException {

			String sessionCheck = (String) session.getAttribute("superAdminSession");
			if (sessionCheck != null) {
				String adminAuthority = authenticationApi.getAuthorityFromSession(sessionCheck,Role.USER);
				if(adminAuthority != null) {
					if(adminAuthority.contains(Authorities.SUPER_ADMIN) && adminAuthority.contains(Authorities.AUTHENTICATED)) {
						dto.setSessionId(sessionCheck);
						TreatCardRegisterListResponse resp = superAdminApi.getRegisterList(dto);
						JSONArray arr = (JSONArray) resp.getJsonArray();
						List<TreatCardRegisterListResponse> list = new ArrayList<TreatCardRegisterListResponse>();
						for (int i = 0; i < arr.length(); i++) {
							TreatCardRegisterListResponse re = new TreatCardRegisterListResponse();
							JSONObject json = arr.getJSONObject(i);
							re.setMembershipCode(JSONParserUtil.getLong(json,"membershipCode"));
							re.setMemberShipCodeExpire(JSONParserUtil.getString(json,"memberShipCodeExpire").substring(0, 16));
							re.setUsername(JSONParserUtil.getString(json,"username"));
							re.setEmailId(JSONParserUtil.getString(json,"emailId"));
							re.setFirstName(JSONParserUtil.getString(json,"firstName"));
							re.setCreated(JSONParserUtil.getString(json,"created"));
							list.add(re);
						}
						model.addAttribute("list", list);
						return "SuperAdmin/TreatCardRegisterList";
					}
				}
			}
			return "redirect:/SuperAdmin/Home";
		}
		
		
		@RequestMapping(method = RequestMethod.POST, value = "/TreatCardReports")
		public String getTreatCardReportFilter(HttpServletRequest request, HttpServletResponse response, HttpSession session,
				ModelMap model,  @RequestParam("fromDate") String start, @RequestParam("toDate") String end) throws JSONException {

			String sessionCheck = (String) session.getAttribute("superAdminSession");
			TreatCardRegisterList dto=new TreatCardRegisterList();
			if (sessionCheck != null) {
				String adminAuthority = authenticationApi.getAuthorityFromSession(sessionCheck,Role.USER);
				if(adminAuthority != null) {
					if(adminAuthority.contains(Authorities.SUPER_ADMIN) && adminAuthority.contains(Authorities.AUTHENTICATED)) {
						model.addAttribute(ModelMapKey.MESSAGE,
								" TreatCard Registeration List From " + start + " to " + end);
						dto.setSessionId(sessionCheck);
						dto.setFromDate(start);
						dto.setToDate(end);
						TreatCardRegisterListResponse resp = superAdminApi.getRegisterListFiltered(dto);
						JSONArray arr = (JSONArray) resp.getJsonArray();
						List<TreatCardRegisterListResponse> list = new ArrayList<TreatCardRegisterListResponse>();
						for (int i = 0; i < arr.length(); i++) {
							TreatCardRegisterListResponse re = new TreatCardRegisterListResponse();
							JSONObject json = arr.getJSONObject(i);
							re.setMembershipCode(JSONParserUtil.getLong(json,"membershipCode"));
							re.setMemberShipCodeExpire(JSONParserUtil.getString(json,"memberShipCodeExpire").substring(0, 16));
							re.setUsername(JSONParserUtil.getString(json,"username"));
							re.setEmailId(JSONParserUtil.getString(json,"emailId"));
							re.setFirstName(JSONParserUtil.getString(json,"firstName"));
							re.setCreated(JSONParserUtil.getString(json,"created"));
							list.add(re);
						}
						model.addAttribute("list", list);
						return "SuperAdmin/TreatCardFilteredList";
					}
				}
			}
			return "redirect:/SuperAdmin/Home";
		}
		
		@RequestMapping(method = RequestMethod.GET, value = "/TreatCardPlansList")
		public String getTreatCardPlans(HttpServletRequest request, HttpServletResponse response, HttpSession session,
				ModelMap model, SessionDTO dto) throws JSONException {

			String sessionCheck = (String) session.getAttribute("superAdminSession");
			if (sessionCheck != null) {
				String adminAuthority = authenticationApi.getAuthorityFromSession(sessionCheck,Role.USER);
				if(adminAuthority != null) {
					if(adminAuthority.contains(Authorities.SUPER_ADMIN) && adminAuthority.contains(Authorities.AUTHENTICATED)) {
						dto.setSessionId(sessionCheck);
						TreatCardPlansResponse resp = superAdminApi.getListPlans(dto);
						JSONArray arr = (JSONArray) resp.getJsonArray();
						List<TreatCardPlansResponse> list = new ArrayList<TreatCardPlansResponse>();
						for (int i = 0; i < arr.length(); i++) {
							TreatCardPlansResponse re = new TreatCardPlansResponse();
							JSONObject json = arr.getJSONObject(i);
							re.setDays(JSONParserUtil.getString(json,"days"));
							re.setAmount(JSONParserUtil.getDouble(json,"amount"));
							re.setPlanStatus(JSONParserUtil.getString(json,"status"));
							list.add(re);
						}
						model.addAttribute("list", list);
						return "SuperAdmin/TreatCardPlansList";
					}
				}
			}
			return "redirect:/SuperAdmin/Home";
		}

		@RequestMapping(value = "/EditPlans", method = RequestMethod.POST)
		ResponseEntity<TreatCardPlansResponse> updatePlans(@ModelAttribute TreatCardPlanRequest dto, HttpServletRequest request,HttpSession session,
				ModelMap model, HttpServletResponse response) throws JSONException {
			TreatCardPlansResponse resp = new TreatCardPlansResponse();
			String sessionCheck = (String) session.getAttribute("superAdminSession");
			if (sessionCheck != null) {
				String adminAuthority = authenticationApi.getAuthorityFromSession(sessionCheck,Role.USER);
				if(adminAuthority != null) {
					if(adminAuthority.contains(Authorities.SUPER_ADMIN) && adminAuthority.contains(Authorities.AUTHENTICATED)) {
						dto.setSessionId(sessionCheck);
						resp = superAdminApi.updatePlans(dto);
						JSONArray arr = (JSONArray) resp.getJsonArray();
						for (int i = 0; i < arr.length(); i++) {
							JSONObject jsonobject = arr.getJSONObject(i);
							resp.setDays(jsonobject.getString("days"));
							resp.setAmount(jsonobject.getDouble("amount"));
							resp.setPlanStatus(jsonobject.getString("status"));

						}
						resp.setJsonArray(null);
					}
				}
			}
			return new ResponseEntity<TreatCardPlansResponse>(resp,HttpStatus.OK);
		}
		
		
		@RequestMapping(method = RequestMethod.GET, value = "/PromoTransactions")
		public String promoTransactionReports(HttpServletRequest request, HttpServletResponse response,
				HttpSession session, ModelMap model) throws JSONException {
			String sessionCheck = (String) session.getAttribute("superAdminSession");
			if (sessionCheck != null) {
				String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
				if (authority != null) {
					if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
						AllTransactionRequest transRequest = new AllTransactionRequest();
						transRequest.setSessionId(sessionCheck);
						AllTransactionResponse allTransaction = superAdminApi.getPromoTransaction(transRequest);
						if (allTransaction.isSuccess()) {
							model.addAttribute("userList", allTransaction.getList());
						} else {
							model.addAttribute(ModelMapKey.ERROR, allTransaction.getMessage());
						}
						return "SuperAdmin/PromoTransactions";
					}
				}
			}
			return "redirect:/SuperAdmin/Home";
		}


		@RequestMapping(method = RequestMethod.POST, value = "/PromoTransactionsFiltered")
		public String filterPromoTransactionReports(@ModelAttribute AllTransactionRequest dto,HttpServletRequest request, HttpServletResponse response,
				HttpSession session, ModelMap model) throws JSONException {
			String sessionCheck = (String) session.getAttribute("superAdminSession");
			if (sessionCheck != null) {
				String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
				if (authority != null) {
					if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
						dto.setSessionId(sessionCheck);
						AllTransactionResponse allTransaction = superAdminApi.getPromoTransactionFiltered(dto);
						if (allTransaction.isSuccess()) {
							model.addAttribute("userList", allTransaction.getList());
						} else {
							model.addAttribute(ModelMapKey.ERROR, allTransaction.getMessage());
						}
						return "SuperAdmin/PromoTransactions";
					}
				}
			}
			return "redirect:/SuperAdmin/Home";
		}
		
		
		@RequestMapping(method = RequestMethod.GET, value = "/FlightdeatilList")
		String getFlightdeatilListInJSON(@ModelAttribute PagingDTO dto, HttpServletRequest request, ModelMap model,
				HttpSession session) {
			UserListDTO result = new UserListDTO();
			String sessionCheck = (String) session.getAttribute("superAdminSession");

			if (sessionCheck != null) {
				String adminAuthority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
				if (adminAuthority != null) {
					if (adminAuthority.contains(Authorities.SUPER_ADMIN)
							&& adminAuthority.contains(Authorities.AUTHENTICATED)) {
						AllUserRequest userRequest = new AllUserRequest();
						userRequest.setPage(0);
						userRequest.setSize(1000);
						userRequest.setSessionId(sessionCheck);
						userRequest.setStatus(UserStatus.ALL);
						AllUserResponse userResponse = superAdminApi.getAllFlightsDetail(userRequest);

						try {
							List<FlightTicketAdminDTO> userList = new ArrayList<>();
							if(userResponse.getCode().equals("S00"))
							{
								System.err.println(userResponse.getResponse());
								JSONObject obj = new JSONObject(userResponse.getResponse());
								JSONArray data = obj.getJSONArray("details");

								if (data!=null) {

									for (int i = 0; i < data.length(); i++) {

										FlightTicketAdminDTO list = new FlightTicketAdminDTO();
										JSONObject userDetail=null;
										JSONObject account=null;
										JSONObject transaction=data.getJSONObject(i).getJSONObject("transaction");
										JSONObject user=data.getJSONObject(i).getJSONObject("user");
										if (user!=null) {
											userDetail=user.getJSONObject("userDetail");
											list.setUserDetails(userDetail.getString("firstName")+" "+userDetail.getString("lastName"));
											list.setUserName(user.getString("username"));
										}

										list.setBookingStatus(data.getJSONObject(i).getString("flightStatus"));
										double commissionAmount=(data.getJSONObject(i).getDouble("commissionAmt"));
										double totalCommissionAmount=commissionAmount-200;
										list.setCommsissionAmount(totalCommissionAmount);
										if (transaction!=null) {
											list.setTransactionRefNo(transaction.getString("transactionRefNo"));
											list.setTransactionStatus(transaction.getString("status"));	
											long dt=transaction.getLong("created");
											Date date=new Date(dt); 
											String txnDate=dateFormat.format(date);
											list.setTxnDate(txnDate);
											account=transaction.getJSONObject("account");
										}else{
											list.setTransactionStatus(data.getJSONObject(i).getString("paymentStatus"));					
										}

										if (account!=null) {
											list.setAccountNumber(account.getLong("accountNumber"));
										}
										list.setBookingRefId(data.getJSONObject(i).getString("bookingRefId"));
										list.setPaymentAmount(data.getJSONObject(i).getDouble("paymentAmount"));
										list.setFlightTicketId(data.getJSONObject(i).getLong("id"));
										list.setTicketDetails(data.getJSONObject(i).getString("ticketDetails"));
										
										list.setFlightNameOnward(data.getJSONObject(i).getString("flightNumberOnward"));
										list.setFlightNameReturn(data.getJSONObject(i).getString("flightNumberReturn"));
										list.setTripType(data.getJSONObject(i).getString("tripType"));
										
										userList.add(list);

									}
								}
								model.addAttribute("FlightDeatils", userList);
							}
							else
							{
								model.addAttribute("FlightDeatils", userList);
							}

						} catch (Exception e) {
							e.printStackTrace();
							return "SuperAdmin/Login";
						}
						return "SuperAdmin/FlightDetail";
					}
				}

			}
			return "SuperAdmin/Login";
		}
		
		@RequestMapping(method = RequestMethod.POST, value = "/getSingleFlightTicketDetails", consumes = {
				MediaType.APPLICATION_JSON_VALUE })
		public ResponseEntity<FlightResponseDTO> getSingleFlightTicketDetails
		(HttpSession session, @RequestBody FlightTicketAdminDTO dto) {

			FlightResponseDTO resp=new FlightResponseDTO();
			FlightCancelableResp resp1=new FlightCancelableResp();
			
			String sessionId = (String) session.getAttribute("superAdminSession");
			if (sessionId != null && sessionId.length() != 0) {
				String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
				if (authority != null) {
					if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
						try {

							if (dto.getFlightTicketId()!=0) {

								resp=superAdminApi.getSingleFlightTicketDetails(sessionId,dto.getFlightTicketId());

								FlightTicketResp  flightTicketResp=(FlightTicketResp)resp.getDetails();
								int tickets=flightTicketResp.getTicketsResp().getTickets().getOneway().size();

								
								FligthcancelableReq req=new FligthcancelableReq();
								
								req.setTransactionScreenId(flightTicketResp.getBookingRefNo());
								req.setSubUserId(flightTicketResp.getEmail());
								
								resp1=superAdminApi.isCancellableFlight(req);
								
							}else{
								resp.setStatus(ResponseStatus.BAD_REQUEST.getKey());
								resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
								resp.setMessage("Please Enter Emt Txn Id");
							}

						} catch (Exception e) {
							System.out.println(e);
							e.printStackTrace();
							resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
							resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
							resp.setMessage("Service Unavailable");
						}
					}
					else {
						resp.setCode("F00");
						resp.setMessage("Unauthorised access");
						resp.setStatus("FAILED");
					}
				}
				else {
					resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
					resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
					resp.setMessage("Session expired");

				}
			}else {
				resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
				resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
				resp.setMessage("Session expired");

			}
			return new ResponseEntity<FlightResponseDTO>(resp, HttpStatus.OK);
		}



		@RequestMapping(method = RequestMethod.POST, value = "/getFlightDetails", consumes = {
				MediaType.APPLICATION_JSON_VALUE })
		public ResponseEntity<FlightResponseDTO> getFlightDetails (HttpSession session, @RequestBody GetFlightDetailsForAdmin dto) {

			FlightResponseDTO resp=new FlightResponseDTO();
			String sessionId = (String) session.getAttribute("superAdminSession");
			if (sessionId != null && sessionId.length() != 0) {
				String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
				if (authority != null) {
					if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
						try {

							String bookingRefId=dto.getBookingRefId();
							/*String email=dto.getEmail();*/

							if (bookingRefId!=null) {

								resp=superAdminApi.getFlightDetails(dto);
								resp.setStatus(ResponseStatus.SUCCESS.getKey());
								resp.setCode(ResponseStatus.SUCCESS.getValue());
							}else{
								resp.setStatus(ResponseStatus.BAD_REQUEST.getKey());
								resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
								resp.setMessage("Please Enter Emt Txn Id");
							}

						} catch (Exception e) {
							System.out.println(e);
							e.printStackTrace();
							resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
							resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
							resp.setMessage("Service Unavailable");
						}
					}
					else {
						resp.setCode("F00");
						resp.setMessage("Unauthorised access");
						resp.setStatus("FAILED");
					}
				}
				else {
					resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
					resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
					resp.setMessage("Session expired");

				}
			}else {
				resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
				resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
				resp.setMessage("Session expired");

			}
			return new ResponseEntity<FlightResponseDTO>(resp, HttpStatus.OK);
		}


		// Bus Details Show in Back end

		@RequestMapping(method = RequestMethod.GET, value = "/BusDeatilsList")
		String getBusdeatilListInJSON(@ModelAttribute PagingDTO dto, HttpServletRequest request, ModelMap model,
				HttpSession session) {
			double totaltx = 0;
			double totalcommission = 0;
			long count = 0;
			UserListDTO result = new UserListDTO();
			String sessionCheck = (String) session.getAttribute("superAdminSession");
			List<BusTicketDTO> list=new ArrayList<>();
			if (sessionCheck != null) {
				String adminAuthority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
				if (adminAuthority != null) {
					if (adminAuthority.contains(Authorities.SUPER_ADMIN)
							&& adminAuthority.contains(Authorities.AUTHENTICATED)) {
						AllUserRequest userRequest = new AllUserRequest();
						userRequest.setPage(0);
						userRequest.setSize(1000);
						userRequest.setSessionId(sessionCheck);
						userRequest.setStatus(UserStatus.ALL);

						System.err.println("Inside Bus Details");

						AllUserResponse userResponse = superAdminApi.getBusDetails(userRequest);
						try {
							if(userResponse.getCode().equals("S00")){
								JSONObject obj = new JSONObject(userResponse.getResponse());
								JSONObject obj1 = obj.getJSONObject("details");
								JSONArray details = obj1.getJSONArray("userList");
								JSONArray commissionList = obj1.getJSONArray("commissionList");
								for (int i = 0; i < details.length(); i++) {
									BusTicketDTO busTicketDTO=new BusTicketDTO();
									if(commissionList.length() >i){
										JSONObject commissionTransaction = commissionList.getJSONObject(i);
										busTicketDTO.setCommsissionAmount(JSONParserUtil.getDouble(commissionTransaction, "commission"));
										
									}else {
										busTicketDTO.setCommsissionAmount(0.0);
									}

									JSONObject val=details.getJSONObject(i);
									JSONObject txn=null;
									if (val!=null) {
										if(val.getString("transaction")!=null && !val.getString("transaction").isEmpty()&& !val.getString("transaction").equalsIgnoreCase("null"))
										{
											txn=val.getJSONObject("transaction");
										}
									}

									String ticketPnr=val.getString("ticketPnr");
									String operatorPnr=val.getString("operatorPnr");
									String source=val.getString("source");
									String destination=val.getString("destination");
									String journeyDate=val.getString("journeyDate");
									String arrTime=val.getString("arrTime");
									double totalFare=(double)details.getJSONObject(i).getDouble("totalFare");
									String priceRecheckTotalFare=details.getJSONObject(i).getString("priceRecheckTotalFare");

									if (priceRecheckTotalFare!=null && !priceRecheckTotalFare.isEmpty() && !priceRecheckTotalFare.equalsIgnoreCase("null")) {
										
										totalFare=Double.valueOf(priceRecheckTotalFare);
										
									}
									
									
									String transactionRefNo="NA";
									if (txn!=null) {
										transactionRefNo=txn.getString("transactionRefNo");
									if(txn.getString("status").equalsIgnoreCase("Success")){
										count++;
										totaltx += totalFare;
										if(commissionList.length() >i){
											JSONObject commissionTransaction = commissionList.getJSONObject(i);
										totalcommission += JSONParserUtil.getDouble(commissionTransaction, "commission");
										}
									}
									busTicketDTO.setTxnStatus(txn.getString("status"));
									}
									else {
										busTicketDTO.setTxnStatus("NA");
									}
									String status=val.getString("status");
									String emtTxnId=val.getString("emtTxnId");

									busTicketDTO.setArrTime(arrTime);
									busTicketDTO.setDestination(destination);
									busTicketDTO.setJourneyDate(journeyDate);
									busTicketDTO.setSource(source);
									busTicketDTO.setTotalFare(totalFare);
									busTicketDTO.setTransactionRefNo(transactionRefNo);
									busTicketDTO.setStatus(status);
									busTicketDTO.setEmtTxnId(emtTxnId);
									busTicketDTO.setTicketPnr(ticketPnr);
									busTicketDTO.setOperatorPnr(operatorPnr);
								
									String strUsr=val.getString("user");
									JSONObject usr=null;
									if (!strUsr.equalsIgnoreCase("null")) {
										usr=val.getJSONObject("user");
									}
									JSONObject usrDtl=null;
									if (usr!=null) {
										usrDtl=usr.getJSONObject("userDetail");
									}

									UserDetail userDetail=new UserDetail();
									if (usrDtl!=null) {
										String userFname=(String)details.getJSONObject(i).getJSONObject("user").getJSONObject("userDetail").get("firstName");
										String userLname=(String)details.getJSONObject(i).getJSONObject("user").getJSONObject("userDetail").get("lastName");
										String username=(String)details.getJSONObject(i).getJSONObject("user").get("username");
										long accountNo=(long)details.getJSONObject(i).getJSONObject("user").getJSONObject("accountDetail").getLong("accountNumber");

										userDetail.setFirstName(userFname);
										userDetail.setLastName(userLname);
										userDetail.setUsername(username);
										userDetail.setAccountNumber(accountNo);
									}

									busTicketDTO.setUserDetail(userDetail);

									long bookingDate=(long)details.getJSONObject(i).getLong("created");

									Date date=new Date(bookingDate); 
									String txnDate=dateFormat.format(date);
									busTicketDTO.setTxnDate(txnDate);
									list.add(busTicketDTO);
								}
								request.setAttribute("totaltx",BigDecimal.valueOf(totaltx).setScale(2, RoundingMode.HALF_UP).doubleValue());
								request.setAttribute("totalcommission",BigDecimal.valueOf(totalcommission).setScale(2, RoundingMode.HALF_UP).doubleValue());
								request.setAttribute("totalnotxs", count);
								model.addAttribute("BusDetails", list);
							}else{
								model.addAttribute("BusDetails", list);
							}
						} catch (Exception e) {
							e.printStackTrace();
							return "redirect:/SuperAdmin/Home";
						}
						return "SuperAdmin/BusDetails";
					}
				}

			}
			return "SuperAdmin/Login";
		}


		@RequestMapping(method = RequestMethod.GET, value = "/BusDeatilsListByDate")
		String getBusdeatilByDate(@ModelAttribute PagingDTO dto, HttpServletRequest request, ModelMap model,
				HttpSession session) {
			
			double totaltx = 0;
			double totalcommission = 0;
			long count = 0;
			
			UserListDTO result = new UserListDTO();
			String sessionCheck = (String) session.getAttribute("superAdminSession");
			List<BusTicketDTO> list=new ArrayList<>();
			if (sessionCheck != null) {
				String adminAuthority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
				if (adminAuthority != null) {
					if (adminAuthority.contains(Authorities.SUPER_ADMIN)
							&& adminAuthority.contains(Authorities.AUTHENTICATED)) {
						AllUserRequest userRequest = new AllUserRequest();
						userRequest.setPage(0);
						userRequest.setSize(1000);
						dto.setSessionId((String) session.getAttribute("superAdminSession"));
						userRequest.setStatus(UserStatus.ALL);
						AllUserResponse userResponse = superAdminApi.getBusDetailsByDate(dto);
						try {
							if(userResponse.getCode().equals("S00")){
								JSONObject obj = new JSONObject(userResponse.getResponse());
								JSONObject obj1 = obj.getJSONObject("details");
								JSONArray details = obj1.getJSONArray("userList");
								JSONArray commissionList = obj1.getJSONArray("commissionList");
								for (int i = 0; i < details.length(); i++) {
									BusTicketDTO busTicketDTO=new BusTicketDTO();
									if(commissionList.length() >i){
										JSONObject commissionTransaction = commissionList.getJSONObject(i);
										busTicketDTO.setCommsissionAmount(JSONParserUtil.getDouble(commissionTransaction, "commission"));
										
									}else {
										busTicketDTO.setCommsissionAmount(0.0);
									}

									JSONObject val=details.getJSONObject(i);
									JSONObject txn=null;
									if (val!=null) {
										if(val.getString("transaction")!=null && !val.getString("transaction").isEmpty()&& !val.getString("transaction").equalsIgnoreCase("null"))
										{
											txn=val.getJSONObject("transaction");
										}
									}

									String ticketPnr=val.getString("ticketPnr");
									String operatorPnr=val.getString("operatorPnr");
									String source=val.getString("source");
									String destination=val.getString("destination");
									String journeyDate=val.getString("journeyDate");
									String arrTime=val.getString("arrTime");
									double totalFare=(double)details.getJSONObject(i).getDouble("totalFare");
									String priceRecheckTotalFare=details.getJSONObject(i).getString("priceRecheckTotalFare");

									if (priceRecheckTotalFare!=null && !priceRecheckTotalFare.isEmpty() && !priceRecheckTotalFare.equalsIgnoreCase("null")) {
										
										totalFare=Double.valueOf(priceRecheckTotalFare);
										
									}
									
									
									String transactionRefNo="NA";
									if (txn!=null) {
										transactionRefNo=txn.getString("transactionRefNo");
									if(txn.getString("status").equalsIgnoreCase("Success")){
										count++;
										totaltx += totalFare;
										if(commissionList.length() >i){
											JSONObject commissionTransaction = commissionList.getJSONObject(i);
										totalcommission += JSONParserUtil.getDouble(commissionTransaction, "commission");
										}
									}
									busTicketDTO.setTxnStatus(txn.getString("status"));
									}
									else {
										busTicketDTO.setTxnStatus("NA");
									}
									String status=val.getString("status");
									String emtTxnId=val.getString("emtTxnId");

									busTicketDTO.setArrTime(arrTime);
									busTicketDTO.setDestination(destination);
									busTicketDTO.setJourneyDate(journeyDate);
									busTicketDTO.setSource(source);
									busTicketDTO.setTotalFare(totalFare);
									busTicketDTO.setTransactionRefNo(transactionRefNo);
									busTicketDTO.setStatus(status);
									busTicketDTO.setEmtTxnId(emtTxnId);
									busTicketDTO.setTicketPnr(ticketPnr);
									busTicketDTO.setOperatorPnr(operatorPnr);
								
									String strUsr=val.getString("user");
									JSONObject usr=null;
									if (!strUsr.equalsIgnoreCase("null")) {
										usr=val.getJSONObject("user");
									}
									JSONObject usrDtl=null;
									if (usr!=null) {
										usrDtl=usr.getJSONObject("userDetail");
									}

									UserDetail userDetail=new UserDetail();
									if (usrDtl!=null) {
										String userFname=(String)details.getJSONObject(i).getJSONObject("user").getJSONObject("userDetail").get("firstName");
										String userLname=(String)details.getJSONObject(i).getJSONObject("user").getJSONObject("userDetail").get("lastName");
										String username=(String)details.getJSONObject(i).getJSONObject("user").get("username");
										long accountNo=(long)details.getJSONObject(i).getJSONObject("user").getJSONObject("accountDetail").getLong("accountNumber");

										userDetail.setFirstName(userFname);
										userDetail.setLastName(userLname);
										userDetail.setUsername(username);
										userDetail.setAccountNumber(accountNo);
									}

									busTicketDTO.setUserDetail(userDetail);

									long bookingDate=(long)details.getJSONObject(i).getLong("created");

									Date date=new Date(bookingDate); 
									String txnDate=dateFormat.format(date);
									busTicketDTO.setTxnDate(txnDate);
									list.add(busTicketDTO);
								}
								request.setAttribute("totaltx",BigDecimal.valueOf(totaltx).setScale(2, RoundingMode.HALF_UP).doubleValue());
								request.setAttribute("totalcommission",BigDecimal.valueOf(totalcommission).setScale(2, RoundingMode.HALF_UP).doubleValue());
								request.setAttribute("totalnotxs", count);
								model.addAttribute("BusDetails", list);
							}else{
								model.addAttribute("BusDetails", list);
							}

						} catch (Exception e) {
							e.printStackTrace();
							return "SuperAdmin:/Admin/Home";
						}
						return "SuperAdmin/BusDetails";
					}
				}

			}
			return "SuperAdmin/Home";
		}

		
		@RequestMapping(method = RequestMethod.POST, value = "/getSingleTicketDetails", consumes = {
				MediaType.APPLICATION_JSON_VALUE })
		public ResponseEntity<com.payqwikweb.app.model.response.bus.ResponseDTO> getSingleTicketDetails
		(HttpSession session, @RequestBody BusTicketDTO dto) {

			com.payqwikweb.app.model.response.bus.ResponseDTO resp=new com.payqwikweb.app.model.response.bus.ResponseDTO();

			com.payqwikweb.app.model.response.bus.ResponseDTO resp1=new com.payqwikweb.app.model.response.bus.ResponseDTO();

			String sessionId = (String) session.getAttribute("superAdminSession");
			if (sessionId != null && sessionId.length() != 0) {
				String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
				if (authority != null) {
					if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
						try {

							IsCancellableReq cancellableReq=new IsCancellableReq();
							if (dto.getEmtTxnId()!=null) {
								resp=superAdminApi.getSingleTicketTravellerDetails(sessionId,dto.getEmtTxnId());

								BusTicketDTO busTicketDTO=(BusTicketDTO)resp.getExtraInfo();

								cancellableReq.setSeatNo(resp.getSeatNo());
								cancellableReq.setBookId(busTicketDTO.getEmtTransactionScreenId());

								session.setAttribute("emtTxnScreenId", busTicketDTO.getEmtTransactionScreenId());
								session.setAttribute("seatForCancel", resp.getSeatNo());
								
								
									resp1=superAdminApi.isCancellable(cancellableReq);
									if (resp1.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
									IsCancellableResp result=(IsCancellableResp)resp1.getDetails();
									resp.setRefAmt(result.getRefundAmount());
									resp.setCancellationCharges(result.getCancellationCharges());
									resp.setTransactionRefNo(busTicketDTO.getTransactionRefNo());
									resp.setCancelable(result.isCancellable());
									}
								//resp.setDetails(busTicketDTO);
							}else{
								resp.setStatus(ResponseStatus.BAD_REQUEST.getKey());
								resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
								resp.setMessage("Please Enter Emt Txn Id");
							}

						} catch (Exception e) {
							System.out.println(e);
							e.printStackTrace();
							resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
							resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
							resp.setMessage("Service Unavailable");
						}
					}
					else {
						resp.setCode("F00");
						resp.setMessage("Unauthorised access");
						resp.setStatus("FAILED");
					}
				}
				else {
					resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
					resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
					resp.setMessage("Session expired");

				}
			}else {
				resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
				resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
				resp.setMessage("Session expired");

			}
			return new ResponseEntity<com.payqwikweb.app.model.response.bus.ResponseDTO>(resp, HttpStatus.OK);
		}


		@RequestMapping(method = RequestMethod.POST, value = "/cancelBookedTicket", consumes = {
				MediaType.APPLICATION_JSON_VALUE })
		public ResponseEntity<com.payqwikweb.app.model.response.bus.ResponseDTO> cancelBookedTicket
		(HttpSession session, @RequestBody IsCancellableReq dto) {

			com.payqwikweb.app.model.response.bus.ResponseDTO resp=new com.payqwikweb.app.model.response.bus.ResponseDTO();
			String sessionId = (String) session.getAttribute("superAdminSession");
			if (sessionId != null && sessionId.length() != 0) {
				String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
				if (authority != null) {
					if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
						try {

							String emtTxnScreenId=(String)session.getAttribute("emtTxnScreenId");
							@SuppressWarnings("unchecked")
							List<String> seatNo=(List<String>)session.getAttribute("seatForCancel");

							dto.setSessionId(sessionId);
							dto.setSeatNo(seatNo);
							dto.setBookId(emtTxnScreenId);

							if (dto.getBookId()!=null) {

								resp=superAdminApi.cancelBookedTicket(dto);
								
							}else{
								resp.setStatus(ResponseStatus.BAD_REQUEST.getKey());
								resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
								resp.setMessage("Please Enter Emt Txn Id");
							}

						} catch (Exception e) {
							System.out.println(e);
							e.printStackTrace();
							resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
							resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
							resp.setMessage("Service Unavailable");
						}
					}
					else {
						resp.setCode("F00");
						resp.setMessage("Unauthorised access");
						resp.setStatus("FAILED");
					}
				}
				else {
					resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
					resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
					resp.setMessage("Session expired");

				}
			}else {
				resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
				resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
				resp.setMessage("Session expired");

			}
			return new ResponseEntity<com.payqwikweb.app.model.response.bus.ResponseDTO>(resp, HttpStatus.OK);
		}
		
		 @RequestMapping(value = "/singleTicketPdfForBus", method = RequestMethod.POST)
		  ModelAndView generateUserPdf(@ModelAttribute  BusTicketDTO dto, HttpServletRequest request,
				    HttpSession session,HttpServletResponse response) throws Exception {
			  
			com.payqwikweb.app.model.response.bus.ResponseDTO resp=new com.payqwikweb.app.model.response.bus.ResponseDTO();

			Map<String,Object> userDataTicket =new HashMap<String,Object>();
			String sessionId = (String) session.getAttribute("superAdminSession");
			if (sessionId != null && sessionId.length() != 0) {
				String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
				if (authority != null) {
					if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
						try {

							resp=superAdminApi.getSingleTicketPdf(sessionId,dto.getEmtTxnId());
							BusTicketDTO busTicketDTO=(BusTicketDTO)resp.getExtraInfo();
							System.err.println("BusTicketDTO: "+busTicketDTO.getSource());
							
							   ObjectMapper mapperObj = new ObjectMapper();
							   String jsonStr = mapperObj.writeValueAsString(busTicketDTO);
							   userDataTicket.put("busTicket",jsonStr.toString());
							   return new ModelAndView("userSummaryTicket","userDataTicket",userDataTicket);
							

						} catch (Exception e) {
							System.out.println(e);
							e.printStackTrace();
							resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
							resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
							resp.setMessage("Service Unavailable");
						}
					}
					else {
						resp.setCode("F00");
						resp.setMessage("Unauthorised access");
						resp.setStatus("FAILED");
					}
				}
				else {
					resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
					resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
					resp.setMessage("Session expired");

				}
			}else {
				resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
				resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
				resp.setMessage("Session expired");

			}
			  return new ModelAndView("userSummaryTicket",userDataTicket);
		}
		 
		 
		 /*Ticket pdf for Flight*/
		 
		 @RequestMapping(value = "/VpayQwikFlightTicket", method = RequestMethod.POST)
		  ModelAndView generateflightPdf(@ModelAttribute  FlightTicketAdminDTO dto, HttpServletRequest request,
				    HttpSession session,HttpServletResponse response) throws Exception {
			  
			FlightResponseDTO resp=new FlightResponseDTO();

			Map<String,Object> userDataTicket =new HashMap<String,Object>();
			String sessionId = (String) session.getAttribute("superAdminSession");
			if (sessionId != null && sessionId.length() != 0) {
				String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
				if (authority != null) {
					if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
						try {
							resp=superAdminApi.getSingleFlightTicketDetails(sessionId,dto.getFlightTicketId());
							FlightTicketResp  flightTicketResp=(FlightTicketResp)resp.getDetails();
							
							   ObjectMapper mapperObj = new ObjectMapper();
							   String jsonStr = mapperObj.writeValueAsString(flightTicketResp);
							   userDataTicket.put("busTicket",jsonStr.toString());
							   return new ModelAndView("FlightTicket","userDataTicket",userDataTicket);
						} catch (Exception e) {
							System.out.println(e);
							e.printStackTrace();
							resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
							resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
							resp.setMessage("Service Unavailable");
						}
					}
					else {
						resp.setCode("F00");
						resp.setMessage("Unauthorised access");
						resp.setStatus("FAILED");
					}
				}
				else {
					resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
					resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
					resp.setMessage("Session expired");

				}
			}else {
				resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
				resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
				resp.setMessage("Session expired");

			}
			 return new ModelAndView("userSummaryTicket","userDataTicket",userDataTicket);
		}
		 
			@RequestMapping(method = RequestMethod.GET, value = "/getMerchantRefundRequest")
			public String getMerchantRefundRequest(@ModelAttribute RefundDTO dto,
					HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model)
							throws JSONException {
				String sessionCheck = (String) session.getAttribute("superAdminSession");
				if (sessionCheck != null) {
					String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
					if (authority != null) {
						if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
							dto.setSessionId(sessionCheck);
							TListResponse resp = superAdminApi.getRefundMerchantTransactionList(dto);
							if (resp.isSuccess()) {
								model.addAttribute("refundTransactionLists", resp.getRefundList());
							} else {
								model.addAttribute(ModelMapKey.ERROR, resp.getMessage());
							}
							return "SuperAdmin/RefundRequest";
						}
					}
				}
				return "redirect:/SuperAdmin/Home";
			}
			
			@RequestMapping(method = RequestMethod.GET, value = "/{transactionRefNo}/RefundMerchant")
			ResponseEntity<TransactionUserResponse> refundTransactionMerchant(
					@PathVariable(value = "transactionRefNo") String transactionRefNo, HttpServletRequest request,
					HttpServletResponse response, HttpSession session, ModelMap model) {
				RefundDTO dto = new RefundDTO();
				TransactionUserResponse result = new TransactionUserResponse();
				String sessionCheck = (String) session.getAttribute("superAdminSession");
				if (sessionCheck != null) {
					String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
					if (authority != null) {
						if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
							dto.setSessionId(sessionCheck);
							dto.setTransactionRefNo(transactionRefNo);
							result = superAdminApi.refundMerchantTransaction(dto);
						}
					}
				}
				return new ResponseEntity<TransactionUserResponse>(result, HttpStatus.OK);
			}
		 
			// ALL Services List
			@RequestMapping(method = RequestMethod.GET, value = "/MdexSwitch")
			public String mdexSwitch(HttpServletRequest request, HttpServletResponse response, HttpSession session,
					ModelMap model) {
				SessionDTO dto = new SessionDTO();
				String sessionCheck = (String) session.getAttribute("superAdminSession");
				if (sessionCheck != null) {
					String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
					if (authority != null) {
						if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
							dto.setSessionId(sessionCheck);
							ServiceListResponse listResponse = dataConfigApi.getServiceList(dto);
							if (listResponse.isSuccess()) {
								model.addAttribute("mdexSwitch", listResponse.getServiceList());
							} else {
								model.addAttribute(ModelMapKey.ERROR, listResponse.getMessage());
							}
							return "SuperAdmin/MdexSwitch";
						}
					}
				}
				return "redirect:/SuperAdmin/Home";
			}
			
			@RequestMapping(method = RequestMethod.POST, value = "/UpdateMdexSwitch")
			ResponseEntity<ServicesDTO> updateSerivce(@ModelAttribute ServicesDTO dto, HttpServletRequest request,
					HttpServletResponse response, HttpSession session, ModelMap model) throws JSONException {

				ServicesDTO result = new ServicesDTO();
				String sessionCheck = (String) session.getAttribute("superAdminSession");
				if (sessionCheck != null) {
					String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
					if (authority != null) {
						if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
							dto.setSessionId(sessionCheck);
							result = superAdminApi.updateMdexSwitch(dto);
						}
					}
				}
				return new ResponseEntity<ServicesDTO>(result, HttpStatus.OK);
			}

	@RequestMapping(method = RequestMethod.GET, value = "/commissionUpdate")
	String getServiceType(HttpSession session, ModelMap model, @ModelAttribute("updated") String updated) {
		RefundDTO dto = new RefundDTO();
		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionCheck);
					ServiceTypeResponse servicesList = superAdminApi.getServiceType();
					model.addAttribute("allService", servicesList.getServiceDTOs());
					model.addAttribute("updateMsg", updated);
					return "SuperAdmin/UpdateCommission";
				}
			}
		}
		return "redirect:/SuperAdmin/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/CommissionList")
	ResponseEntity<ServiceTypeResponse> getCommissionList(HttpSession session, ModelMap model,
			@RequestBody ServiceRequest service) {
		ServiceTypeResponse comResponse = null;
		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					comResponse = superAdminApi.getAllCommission(service);
					model.addAttribute("allCommission", comResponse);
					return new ResponseEntity<ServiceTypeResponse>(comResponse, HttpStatus.OK);
				}
			}
		}
		return new ResponseEntity<ServiceTypeResponse>(comResponse, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/commissionUpdate")
	String updateCommissionType(HttpSession session, ModelMap model, @ModelAttribute CommissionDTO comDto) {
		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					comDto.setSessionId(sessionCheck);
					ResponseDTO updateResp = superAdminApi.updateCommission(comDto);
					model.addAttribute("updated", updateResp.getMessage());
					return "redirect:/SuperAdmin/commissionUpdate";
				}
			}
		}
		return "redirect:/SuperAdmin/Home";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/AddServices")
	public String getAddServicesGet(@RequestParam(value = ModelMapKey.MESSAGE, required = false) String msg,
			@RequestParam(value = ModelMapKey.ERROR, required = false) String error, HttpServletRequest request,
			ModelMap model, HttpServletResponse response, HttpSession session) {
		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					if (msg != null) {
						model.addAttribute(ModelMapKey.MESSAGE, msg);
					}
					if (error != null) {
						model.addAttribute(ModelMapKey.ERROR, error);
					}
					return "SuperAdmin/AddServices";
				}
			}
		}
		return "redirect:/SuperAdmin/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/AddServices")
	String updateCommissionType(HttpSession session, ModelMap model, @ModelAttribute AddServicesDTO dto) {
		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionCheck);
					ResponseDTO updateResp = superAdminApi.AddServices(dto);
					if (updateResp.isSuccess()) {
						model.addAttribute(ModelMapKey.MESSAGE, updateResp.getMessage());
						return "redirect:/SuperAdmin/AddServices";
					} else {
						model.addAttribute(ModelMapKey.ERROR, updateResp.getMessage());
						return "redirect:/SuperAdmin/AddServices";
					}
				}
			}
		}
		return "redirect:/SuperAdmin/Home";
	}
				
	
	@RequestMapping(method = RequestMethod.GET, value = "/GetServiceTypeList")
	public ResponseEntity<ServiceTypeResponse> getServiceType(HttpServletRequest request, HttpServletResponse response,
			HttpSession session, ModelMap model) {
		ServiceTypeResponse servicesList = superAdminApi.getServiceType();
		return new ResponseEntity<ServiceTypeResponse>(servicesList, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/GetOperatorList")
	public ResponseEntity<ServiceTypeResponse> getOperator(HttpServletRequest request, HttpServletResponse response,
			HttpSession session, ModelMap model) {
		ServiceTypeResponse servicesList = superAdminApi.getOperatorList();
		return new ResponseEntity<ServiceTypeResponse>(servicesList, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/AadharDetails")
	public String getAllAadharList(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) {
		SessionDTO dto = new SessionDTO();
		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionCheck);
					AadharServiceResponse listResponse = superAdminApi.getAadharDetails(dto);
					if (listResponse.isSuccess()) {
						model.addAttribute("aadharServices", listResponse.getAadharList());
					} else {
						model.addAttribute(ModelMapKey.ERROR, listResponse.getMessage());
					}
					return "SuperAdmin/AadharServiceList";
				}
			}
		}
		return "redirect:/SuperAdmin/Home";
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/AddServiceType")
	public String getServiceTypes(@RequestParam(value = ModelMapKey.MESSAGE, required = false) String msg,
			@RequestParam(value = ModelMapKey.ERROR, required = false) String error, HttpServletRequest request,
			ModelMap model, HttpServletResponse response, HttpSession session) {
		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					if (msg != null) {
						model.addAttribute(ModelMapKey.MESSAGE, msg);
					}
					if (error != null) {
						model.addAttribute(ModelMapKey.ERROR, error);
					}
					return "SuperAdmin/AddServiceType";
				}
			}
		}
		return "redirect:/SuperAdmin/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/AddServiceType")
	String addServiceTypes(HttpSession session, ModelMap model, @ModelAttribute AddServiceTypeDTO dto) {
		String sessionCheck = (String) session.getAttribute("superAdminSession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.SUPER_ADMIN) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionCheck);
					ResponseDTO updateResp = superAdminApi.AddServiceType(dto);
					if (updateResp.isSuccess()) {
						model.addAttribute(ModelMapKey.MESSAGE, updateResp.getMessage());
						return "redirect:/SuperAdmin/AddServiceType";
					} else {
						model.addAttribute(ModelMapKey.ERROR, updateResp.getMessage());
						return "redirect:/SuperAdmin/AddServiceType";
					}
				}
			}
		}
		return "redirect:/SuperAdmin/Home";
	}

}


