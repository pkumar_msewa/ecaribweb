package com.payqwikweb.controller.web.user;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.recaptcha.api.IVerificationApi;
import com.google.recaptcha.model.JCaptchaRequest;
import com.google.recaptcha.model.JCaptchaResponse;
import com.payqwikweb.app.api.IRegistrationApi;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.request.MobileOTPRequest;
import com.payqwikweb.app.model.request.RegistrationRequest;
import com.payqwikweb.app.model.request.ResendMobileOTPRequest;
import com.payqwikweb.app.model.request.VerifyEmailRequest;
import com.payqwikweb.app.model.response.MobileOTPResponse;
import com.payqwikweb.app.model.response.QuestionResponse;
import com.payqwikweb.app.model.response.RegistrationResponse;
import com.payqwikweb.app.model.response.ResendMobileOTPResponse;
import com.payqwikweb.app.model.response.VerifyEmailResponse;
import com.payqwikweb.model.error.MobileOTPError;
import com.payqwikweb.model.error.RegisterError;
import com.payqwikweb.util.APIUtils;
import com.payqwikweb.validation.MobileOTPValidation;
import com.payqwikweb.validation.RegisterValidation;

@Controller
@RequestMapping(value = "/Api/{version}/{role}/{device}/{language}/WebRegistration")
public class RegistrationController implements MessageSourceAware {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private MessageSource messageSource;
	private SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
	private final IRegistrationApi registrationApi;
//	private final IVerificationApi verificationApi;
	private final RegisterValidation registerValidation;
	private final MobileOTPValidation mobileOTPValidation;

	public RegistrationController(IRegistrationApi registrationApi,
			RegisterValidation registerValidation, MobileOTPValidation mobileOTPValidation) {
		this.registrationApi = registrationApi;
	//	this.verificationApi = verificationApi;
		this.registerValidation = registerValidation;
		this.mobileOTPValidation = mobileOTPValidation;
	}

	@Override
	public void setMessageSource(MessageSource arg0) {
		this.messageSource = messageSource;
	}

	@RequestMapping(value = "/Process", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<RegistrationResponse> processRegistration(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody RegistrationRequest dto,
			HttpServletRequest request) {

		RegistrationResponse result = new RegistrationResponse();
		/*JCaptchaRequest cap = new JCaptchaRequest();
		cap.setSessionId(request.getSession().getId());
		cap.setCaptchaResponse(dto.getCaptchaResponse());
		JCaptchaResponse jCaptchaResponse = verificationApi.isValidJCaptcha(cap);*/
		RegisterError error = registerValidation.checkError(dto);
		if (error.isValid()) {
	//		if (jCaptchaResponse.isValid()) {
				if (role.equalsIgnoreCase(Role.USER.getValue())) {
					if (device.equalsIgnoreCase(Device.ANDROID.getValue())
							|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
							|| device.equalsIgnoreCase(Device.IOS.getValue())) {
						result = registrationApi.register(dto);
					} else {
						result.setSuccess(false);
						result.setCode("F00");
						result.setMessage("Unknown device");
						result.setStatus("FAILED");
						result.setResponse(APIUtils.getFailedJSON().toString());
					}
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Unauthorised access");
					result.setStatus("FAILED");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			/*} else {
				result.setCode("F00");
				result.setMessage("Invalid Captcha");
			}*/
		} else {
			result.setCode("F00");
			result.setMessage("Input is not valid");
		}
		return new ResponseEntity<RegistrationResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/MobileOTP", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<MobileOTPResponse> mobileOTP(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody MobileOTPRequest dto) {
		MobileOTPResponse result = new MobileOTPResponse();
		MobileOTPError error = mobileOTPValidation.checkError(dto);
		if (error.isValid()) {
			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue())
						|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					result = registrationApi.mobileOTP(dto);
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Unknown device");
					result.setStatus("FAILED");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unauthorised access");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			result.setCode("F00");
			result.setMessage("Invalid Input");
		}
		return new ResponseEntity<MobileOTPResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/ResendMobileOTP", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResendMobileOTPResponse> resendMobileOTP(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody ResendMobileOTPRequest dto,
			HttpServletRequest request) {
		ResendMobileOTPResponse result = new ResendMobileOTPResponse();
		JCaptchaRequest cap = new JCaptchaRequest();
		cap.setSessionId(request.getSession().getId());
		cap.setCaptchaResponse(dto.getCaptchaResponse());
		// JCaptchaResponse jCaptchaResponse =
		// verificationApi.isValidJCaptcha(cap);
		MobileOTPError error = mobileOTPValidation.checkError(dto);
		if (true) {
			if (error.isValid()) {
				if (role.equalsIgnoreCase(Role.USER.getValue())) {
					if (device.equalsIgnoreCase(Device.ANDROID.getValue())
							|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
							|| device.equalsIgnoreCase(Device.IOS.getValue())) {
						result = registrationApi.resendMobileOTP(dto);
					} else {
						result.setSuccess(false);
						result.setCode("F00");
						result.setMessage("Unknown device");
						result.setStatus("FAILED");
						result.setResponse(APIUtils.getFailedJSON().toString());
					}
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Unauthorised access");
					result.setStatus("FAILED");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				result.setCode("F00");
				result.setMessage("Invalid Input");
			}
		} else {
			result.setCode("F00");
			result.setMessage("Invalid Captcha");
		}
		return new ResponseEntity<ResendMobileOTPResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/Activate/Email/{key}")
	public String verifyEmail(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @PathVariable("key") String key) {
		VerifyEmailRequest emailRequest = new VerifyEmailRequest();
		if (key != null) {
			emailRequest.setKey(key);
			VerifyEmailResponse emailResponse = registrationApi.verifyEmail(emailRequest);
		}
		return "VerifyEmail";
	}

	@RequestMapping(value = "/Questions", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<Object> getBusSources(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, HttpServletRequest request, HttpServletResponse response,
			ModelMap model, HttpSession session) {
		//System.err.println("inside security question controller");
		String id = "";
		String name = "";
		QuestionResponse resp = new QuestionResponse();
		ArrayList<String> list = new ArrayList<String>();
		try {
			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue())
						|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())
						|| device.equalsIgnoreCase(Device.WEBSITE.getValue())) {
					resp = registrationApi.getQuestions();
					String responseFromApi = resp.getResponse();
					org.json.JSONObject jobj = new org.json.JSONObject(responseFromApi);
					if (jobj != null) {
						org.json.JSONArray arr = jobj.getJSONArray("questions");
						if (arr != null) {
							for (int i = 0; i < arr.length(); i++) {
								JSONObject j = (JSONObject) arr.get(i);
								String question = j.getString("question");
								String code = j.getString("code");
								id = code;
								name = question;
								list.add(id + "#" + name);
							}

						}
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();

		}
		return new ResponseEntity<Object>(list, HttpStatus.OK);

	}
	
	
	@RequestMapping(value = {"/ICallApi","/URegisterUser"} , method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<RegistrationResponse> processRegister(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody RegistrationRequest dto,
			HttpServletRequest request,@RequestHeader(value = "API_KEY", required = true) String apiKey) {

		RegistrationResponse result = new RegistrationResponse();
		RegisterError error = registerValidation.checkError(dto);
		if (error.isValid()) {
				if (role.equalsIgnoreCase(Role.USER.getValue())) {
					if (device.equalsIgnoreCase(Device.ANDROID.getValue())
							|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
							|| device.equalsIgnoreCase(Device.IOS.getValue())) {
						result = registrationApi.newRegister(dto);
					} else {
						result.setSuccess(false);
						result.setCode("F00");
						result.setMessage("Unknown device");
						result.setStatus("FAILED");
						result.setResponse(APIUtils.getFailedJSON().toString());
					}
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Unauthorised access");
					result.setStatus("FAILED");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
		} else {
			result.setCode("F00");
			result.setMessage("Input is not valid");
		}
		return new ResponseEntity<RegistrationResponse>(result, HttpStatus.OK);
	}

}
