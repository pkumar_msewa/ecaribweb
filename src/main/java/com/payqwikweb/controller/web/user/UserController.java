package com.payqwikweb.controller.web.user;

import java.awt.Image;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.aadhar.api.IAadharServiceApi;
import com.aadhar.model.AadharOTPDTO;
import com.aadhar.model.AadharOTPResponse;
import com.aadhar.model.AadharOTPValidateDTO;
import com.aadhar.model.AadharOTPValidateResponse;
import com.google.recaptcha.api.IVerificationApi;
import com.google.recaptcha.model.JCaptchaRequest;
import com.google.recaptcha.model.JCaptchaResponse;
import com.payqwikweb.api.IAuthenticationApi;
import com.payqwikweb.app.api.IUserApi;
import com.payqwikweb.app.model.ResponseStatus;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.request.ChangeMPINRequest;
import com.payqwikweb.app.model.request.ChangePasswordRequest;
import com.payqwikweb.app.model.request.DeleteMPINRequest;
import com.payqwikweb.app.model.request.EditProfileRequest;
import com.payqwikweb.app.model.request.InviteFriendEmailRequest;
import com.payqwikweb.app.model.request.InviteFriendMobileRequest;
import com.payqwikweb.app.model.request.KycVerificationDTO;
import com.payqwikweb.app.model.request.ReceiptsRequest;
import com.payqwikweb.app.model.request.RedeemCodeRequest;
import com.payqwikweb.app.model.request.SetMPINRequest;
import com.payqwikweb.app.model.request.UploadPictureRequest;
import com.payqwikweb.app.model.request.UserDetailsRequest;
import com.payqwikweb.app.model.response.ChangePasswordResponse;
import com.payqwikweb.app.model.response.EditProfileResponse;
import com.payqwikweb.app.model.response.InviteFriendEmailResponse;
import com.payqwikweb.app.model.response.InviteFriendMobileResponse;
import com.payqwikweb.app.model.response.KycResponse;
import com.payqwikweb.app.model.response.MPINResponse;
import com.payqwikweb.app.model.response.ReceiptsResponse;
import com.payqwikweb.app.model.response.TransactionDTO;
import com.payqwikweb.app.model.response.UploadPictureResponse;
import com.payqwikweb.app.model.response.UserDetailsResponse;
import com.payqwikweb.model.app.response.RedeemCodeResponse;
import com.payqwikweb.model.error.EditProfileError;
import com.payqwikweb.model.error.KycError;
import com.payqwikweb.util.APIUtils;
import com.payqwikweb.util.Authorities;
import com.payqwikweb.util.JSONParserUtil;
import com.payqwikweb.util.ModelMapKey;
import com.payqwikweb.validation.ChangePasswordValidation;
import com.payqwikweb.validation.CommonValidation;
import com.payqwikweb.validation.EditProfileValidation;
import com.payqwikweb.validation.KycValidation;
import com.thirdparty.model.ResponseDTO;

@Controller
@RequestMapping("/User")
public class UserController implements MessageSourceAware {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private MessageSource messageSource;
	private final EditProfileValidation editProfileValidation;
	private final ChangePasswordValidation changePasswordValidation;
	private final IUserApi userApi;
	private final IAuthenticationApi authenticationApi;
//	private final IVerificationApi verificationApi;
	private final KycValidation kycValidation;
	private final IAadharServiceApi aadharServiceApi;
	
	private static SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	public UserController(EditProfileValidation editProfileValidation,
			ChangePasswordValidation changePasswordValidation, IUserApi userApi, IAuthenticationApi authenticationApi,
			KycValidation kycValidation,IAadharServiceApi aadharServiceApi) {
		super();
		this.editProfileValidation = editProfileValidation;
		this.changePasswordValidation = changePasswordValidation;
		this.userApi = userApi;
		this.authenticationApi = authenticationApi;
//		this.verificationApi = verificationApi;
		this.kycValidation = kycValidation;
		this.aadharServiceApi=aadharServiceApi;
	}

	@RequestMapping(value = "/EditProfile/Process", method = RequestMethod.POST)
	public String processEditedDetails(@ModelAttribute("editUser") EditProfileRequest dto, ModelMap modelMap,
			HttpSession session) {
		String sessionId = (String) session.getAttribute("sessionId");
		EditProfileError error = editProfileValidation.checkError(dto);
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
					if (error.isValid()) {
						dto.setSessionId(sessionId);
						EditProfileResponse resp = userApi.editProfile(dto);
						modelMap.addAttribute("message", resp.getMessage());
						return "User/Settings";
					} else {
						modelMap.addAttribute(ModelMapKey.ERROR, error);
						return "User/Settings";
					}
				}
			}
		}
		return "redirect:/";
	}

	@RequestMapping(value = "/UploadPicture", method = RequestMethod.POST)
	public String processProfilePicture(@ModelAttribute UploadPictureRequest dto, HttpServletRequest request,
			HttpServletResponse response, RedirectAttributes modelMap, HttpSession session) {
		UploadPictureResponse result = new UploadPictureResponse();
		try {
		MultipartFile file = dto.getProfilePicture();
		// String contentType = file.getContentType();
		boolean validImage = isValidImage(file);
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
					if (validImage) {
						dto.setSessionId(sessionId);
						result = userApi.uploadPicture(dto);
						if (result.isSuccess()) {
							modelMap.addAttribute(ModelMapKey.MESSAGE, result.getMessage());
						} else {
							modelMap.addAttribute(ModelMapKey.ERROR, result.getMessage());
						}
					} else {
						modelMap.addAttribute(ModelMapKey.ERROR, "Please enter valid file format and valid size");
					}
				}
			}
		 }
		} catch (Exception e) {
			e.printStackTrace();
			modelMap.addFlashAttribute(ModelMapKey.ERROR, "Please enter valid file format and valid size");
			return "redirect:/";
		}
		return "redirect:/";

	}

	@RequestMapping(value = "/UpdatePassword/Process", method = RequestMethod.POST)
	public ResponseEntity<ChangePasswordResponse> processNewPassword(
			@ModelAttribute("updatePassword") ChangePasswordRequest dto, HttpServletRequest request,
			HttpServletResponse response, Model modelMap, HttpSession session) {
		dto.setUsername((String) (session.getAttribute("username")));
		String sessionId = (String) session.getAttribute("sessionId");
		ChangePasswordResponse resp = new ChangePasswordResponse();
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionId);
					resp = userApi.changePassword(dto);
					modelMap.addAttribute("message", resp.getMessage());
					return new ResponseEntity<ChangePasswordResponse>(resp, HttpStatus.OK);
				}
			}
		}
		return new ResponseEntity<ChangePasswordResponse>(resp, HttpStatus.OK);
	}

	@RequestMapping(value = "/MPIN/Set", method = RequestMethod.POST)
	public String setMPIN(@ModelAttribute("setMPIN") SetMPINRequest dto, ModelMap modelMap, HttpSession session) {
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionId);
					MPINResponse resp = userApi.setMPIN(dto);
					modelMap.addAttribute("message", resp.getMessage());
					return "User/MPIN";
				}
			}
		}
		return "redirect:/";
	}

	@RequestMapping(value = "/MPIN/Change", method = RequestMethod.POST)
	public String setMPIN(@ModelAttribute("changeMPIN") ChangeMPINRequest dto, ModelMap modelMap, HttpSession session) {
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionId);
					MPINResponse resp = userApi.changeMPIN(dto);
					modelMap.addAttribute("message", resp.getMessage());
					return "User/MPIN";
				}
			}
		}
		return "redirect:/";
	}

	@RequestMapping(value = "/MPIN/Delete", method = RequestMethod.POST)
	public String setMPIN(@ModelAttribute("deleteMPIN") DeleteMPINRequest dto, ModelMap modelMap, HttpSession session) {
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionId);
					MPINResponse resp = userApi.deleteMPIN(dto);
					modelMap.addAttribute("message", resp.getMessage());
					return "User/MPIN";
				}
			}
		}
		return "redirect:/";
	}

	@RequestMapping(value = "/Receipts", method = RequestMethod.GET)
	public String getReceipts(@ModelAttribute("editUser") ReceiptsRequest dto, ModelMap modelMap, HttpSession session)
			throws JSONException {
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
					ArrayList<TransactionDTO> transactionList = new ArrayList<>();
					dto.setSessionId(sessionId);
					dto.setSize(1000);
					dto.setPage(0);
					ReceiptsResponse resp = userApi.getReceipts(dto);
					JSONArray receiptsArray = (JSONArray) resp.getAdditionalInfo();
					for (int i = 0; i < receiptsArray.length(); i++) {
						JSONObject obj = receiptsArray.getJSONObject(i);
						TransactionDTO transaction = new TransactionDTO();
						transaction.setAmount(JSONParserUtil.getDouble(obj, "amount"));
						long timeinMillis = (long) JSONParserUtil.getLong(obj, "created");
						Date dateTime = new Date(timeinMillis);
						transaction.setDate("" + sdf.format(dateTime));
						transaction.setTransactionRefNo(JSONParserUtil.getString(obj, "transactionRefNo"));
						transaction.setDescription(JSONParserUtil.getString(obj, "description"));
						transaction.setStatus(JSONParserUtil.getString(obj, "status"));
						transaction.setDebit(JSONParserUtil.getBoolean(obj, "debit"));
						transactionList.add(transaction);
					}
					modelMap.put("transactions", transactionList);
					return "User/Receipts";
				}
			}
		}
		return "redirect:/";
	}

	@RequestMapping(value = "Receipts/ReceiptAjax", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> getReceiptsInAjax(@ModelAttribute ReceiptsRequest dto, ModelMap modelMap,
			HttpSession session, HttpServletRequest request, HttpServletResponse response) throws JSONException {
		String sessionId = (String) session.getAttribute("sessionId");
		ArrayList<TransactionDTO> transactionList = new ArrayList<>();
		ResponseDTO resps = new ResponseDTO();
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionId);
					// dto.setSize(dto.getSize());
					// dto.setPage(dto.getPage());
					ReceiptsResponse resp = userApi.getReceipts(dto);
					System.err.println("the page is ::" + dto.getPage() + dto.getSize());
					String responseObj = resp.getResponse();
					JSONObject jobj = new JSONObject(responseObj);
					if (jobj != null) {
						JSONObject receiptsObject = jobj.getJSONObject("details");
						if (receiptsObject != null) {
							JSONArray receiptsArray = receiptsObject.getJSONArray("content");
							for (int i = 0; i < receiptsArray.length(); i++) {
								JSONObject obj = receiptsArray.getJSONObject(i);
								TransactionDTO transaction = new TransactionDTO();
								transaction.setAmount(JSONParserUtil.getDouble(obj, "amount"));
								long timeinMillis = (long) JSONParserUtil.getLong(obj, "created");
								Date dateTime = new Date(timeinMillis);
								transaction.setDate("" + sdf.format(dateTime));
								transaction.setTransactionRefNo(JSONParserUtil.getString(obj, "transactionRefNo"));
								transaction.setDescription(JSONParserUtil.getString(obj, "description"));
								transaction.setStatus(JSONParserUtil.getString(obj, "status"));
								transaction.setDebit(JSONParserUtil.getBoolean(obj, "debit"));

								transactionList.add(transaction);
							}
							
							resps.setTotalPages(JSONParserUtil.getLong(receiptsObject, "totalPages"));
						}
					}
					resps.setCode(resp.getCode());
					resps.setInfo(transactionList);

					return new ResponseEntity<>(resps, HttpStatus.OK);
				}
			}
		}
		return new ResponseEntity<>(resps, HttpStatus.OK);
	}

	@RequestMapping(value = "Receipts/ReceiptAjaxFilter", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> getReceiptsInAjaxDateFilter(@RequestBody ReceiptsRequest dto, ModelMap modelMap,
			HttpSession session, HttpServletRequest request, HttpServletResponse response) throws JSONException {
		String sessionId = (String) session.getAttribute("sessionId");
		ArrayList<TransactionDTO> transactionList = new ArrayList<>();
		ResponseDTO resps = new ResponseDTO();
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionId);
					ReceiptsResponse resp = userApi.getReceiptsFilter(dto);
					System.err.println("the page is ::" + dto.getPage()+"" + dto.getSize());
					String responseObj = resp.getResponse();
					JSONObject jobj = new JSONObject(responseObj);
					if (jobj != null) {
						JSONObject receiptsObject = jobj.getJSONObject("details");
						if (receiptsObject != null) {
							JSONArray receiptsArray = receiptsObject.getJSONArray("content");
							for (int i = 0; i < receiptsArray.length(); i++) {
								JSONObject obj = receiptsArray.getJSONObject(i);
								TransactionDTO transaction = new TransactionDTO();
								transaction.setAmount(JSONParserUtil.getDouble(obj, "amount"));
								long timeinMillis = (long) JSONParserUtil.getLong(obj, "created");
								Date dateTime = new Date(timeinMillis);
								transaction.setDate("" + sdf.format(dateTime));
								transaction.setTransactionRefNo(JSONParserUtil.getString(obj, "transactionRefNo"));
								transaction.setDescription(JSONParserUtil.getString(obj, "description"));
								transaction.setStatus(JSONParserUtil.getString(obj, "status"));
								transaction.setDebit(JSONParserUtil.getBoolean(obj, "debit"));

								transactionList.add(transaction);
							}
							resps.setTotalPages(JSONParserUtil.getLong(receiptsObject, "totalPages"));
						}
					}
					resps.setCode(resp.getCode());
					resps.setInfo(transactionList);

					return new ResponseEntity<>(resps, HttpStatus.OK);
				}
			}
		}
		return new ResponseEntity<>(resps, HttpStatus.OK);
	}

	@RequestMapping(value = "/GetMReceiptsInJSON", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ReceiptsResponse> getReceiptsInJSON(@RequestBody ReceiptsRequest dto, HttpSession session) {
		ReceiptsResponse result = new ReceiptsResponse();
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.MERCHANT) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionId);
					result = userApi.getMerchantReceipts(dto);
				}
			}
		}
		return new ResponseEntity<ReceiptsResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/Invite/Email", method = RequestMethod.POST)
	public ResponseEntity<InviteFriendEmailResponse> inviteEmailFriend(
			@ModelAttribute("invitewebfriend") InviteFriendEmailRequest ifw, HttpServletRequest request, ModelMap mp,
			HttpSession session) {
		InviteFriendEmailResponse result = new InviteFriendEmailResponse();
		String sessionId = (String) session.getAttribute("sessionId");
		/*JCaptchaRequest cap = new JCaptchaRequest();
		cap.setSessionId(request.getSession().getId());
		cap.setCaptchaResponse(ifw.getCaptchaResponse());
		JCaptchaResponse jCaptchaResponse = verificationApi.isValidJCaptcha(cap);
		if (jCaptchaResponse != null) {*/
			if (sessionId != null && sessionId.length() != 0) {
				String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
				if (authority != null) {
					if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
						ifw.setSessionId((String) session.getAttribute("sessionId"));
						InviteFriendEmailResponse resp = userApi.inviteEmailFriend(ifw);
						mp.addAttribute("message", resp.getMessage());
						return new ResponseEntity<InviteFriendEmailResponse>(resp, HttpStatus.OK);
					}
				}
			}
		/*} else {
			result.setCode("F00");
			result.setMessage("Invalid Captcha");
		}*/
		return new ResponseEntity<InviteFriendEmailResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/Invite/Mobile", method = RequestMethod.POST)
	public ResponseEntity<InviteFriendMobileResponse> inviteMobileFriend(
			@ModelAttribute("invitewebfriend") InviteFriendMobileRequest ifw, HttpServletRequest request, ModelMap mp,
			HttpSession session) {
		InviteFriendMobileResponse result = new InviteFriendMobileResponse();
		String sessionId = (String) session.getAttribute("sessionId");
//		JCaptchaRequest cap = new JCaptchaRequest();
//		cap.setSessionId(request.getSession().getId());
//		cap.setCaptchaResponse(ifw.getCaptchaResponse());
//		JCaptchaResponse jCaptchaResponse = verificationApi.isValidJCaptcha(cap);
//		if (jCaptchaResponse != null) {
			if (sessionId != null && sessionId.length() != 0) {
				String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
				if (authority != null) {
					if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
						ifw.setSessionId((String) session.getAttribute("sessionId"));
						InviteFriendMobileResponse resp = userApi.inviteMobileFriend(ifw);
						mp.addAttribute("message", resp.getMessage());
						return new ResponseEntity<InviteFriendMobileResponse>(resp, HttpStatus.OK);
					}
				}
			}
//		} else {
//			result.setCode("F00");
//			result.setMessage("Invalid Captcha");
//		}
		return new ResponseEntity<InviteFriendMobileResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/GetUserDetails", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<UserDetailsResponse> getUserDetails(@ModelAttribute("userDetails") UserDetailsRequest dto,
			HttpSession session) {
		UserDetailsResponse result = new UserDetailsResponse();
		List<UserDetailsResponse> list = new ArrayList<UserDetailsResponse>();
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionId);
					result = userApi.getUserDetails(dto, Role.USER);
					JSONObject obj = new JSONObject(result);
					try {
						// boolean flag;
						String usertype = obj.getString("userType");
						// System.out.println(usertype);
						if (usertype.equalsIgnoreCase("KYC")) {
							result.setFlag(true);
						} else {
							result.setFlag(false);
						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					// System.out.println("this is
					// response:"+result.toString());
					dto.setSessionId(sessionId);
					return new ResponseEntity<UserDetailsResponse>(result, HttpStatus.OK);
				}
			}
		}
		return new ResponseEntity<UserDetailsResponse>(result, HttpStatus.OK);
	}

	private boolean isValidImage(MultipartFile file) {
		long length = 2 * 1024 * 1024;
		File imageFile = null;
		String[] format = file.getContentType().split("/");
		String fileFormat = "";
		String[] formats = { "jpeg", "png", "jpg" };
		for (String fmt : formats) {
			System.out.println("Fmt :: " + fmt);
			System.err.println("format[1]:: " + format[1]);
			if (format[1].equals(fmt)) {
				fileFormat = fmt;
				// isValid = true;
				System.out.println("FileFormat " + fileFormat);
			}
		}
		try {
			imageFile = convert(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
		boolean isValid = false;
		if (file.getContentType().contains("image")) {
			isValid = true;
		}
		if (file.getSize() <= length) {
			try {
				Image image = ImageIO.read(imageFile);
				if (image == null) {
					isValid = false;
					System.out.println("The file " + file.getName() + " could not be opened , it is not an image");
				} else {
					isValid = true;
				}
			} catch (IOException ex) {
				System.out
						.println("The file" + file.getOriginalFilename() + "could not be opened , an error occurred.");
				isValid = false;
			}
		}

		return isValid;
	}

	private File convert(MultipartFile file) throws IOException {
		File convFile = new File(file.getOriginalFilename());
		convFile.createNewFile();
		FileOutputStream fos = new FileOutputStream(convFile);
		fos.write(file.getBytes());
		fos.close();
		return convFile;
	}

	private String saveImage(String rootDirectory, MultipartFile image) {
		boolean isSaved = false;
		String filePath = "";
		String seperator = "|";
		String[] format = image.getContentType().split("/");
		String fileFormat = "";
		String[] formats = { "jpeg", "png", "jpg" };
		for (String fmt : formats) {
			if (format[1].equals(fmt)) {
				fileFormat = fmt;
			}
		}
		if (!CommonValidation.isNull(fileFormat)) {
			try {
				String fileName = String.valueOf(System.currentTimeMillis());
				File dirs = new File(rootDirectory + "/resources/profileImages/" + fileName + "." + fileFormat);
				dirs.mkdirs();
				image.transferTo(dirs);
				filePath = "/resources/profileImages/" + fileName + "." + fileFormat;
				isSaved = true;
				return isSaved + seperator + filePath;
			} catch (Exception ex) {
				return "Exception Occurred";
			}
		}
		return "Exception Occurred";
	}

	@RequestMapping(value = "/ReSendEmailOTP", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<UserDetailsResponse> getReSendEmailOTP(@ModelAttribute("userDetails") UserDetailsRequest dto,
			HttpSession session) {
		UserDetailsResponse result = new UserDetailsResponse();
		String sessionId = (session.getAttribute("sessionId") == null ? dto.getSessionId()
				: (String) session.getAttribute("sessionId"));
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionId);
					result = userApi.reSendEmailTop(dto);
					result.setCode("S00");
					result.setMessage("Email Sent Successfully");
					result.setSuccess(true);
					dto.setSessionId(sessionId);
					return new ResponseEntity<UserDetailsResponse>(result, HttpStatus.OK);
				}
			}
		} else {
			result.setCode("F03");
			result.setSuccess(false);
			result.setMessage("Invalid Session ID");
		}
		return new ResponseEntity<UserDetailsResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/RedeemCoupon", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<RedeemCodeResponse> getRedeemCode(@ModelAttribute("userDetails") RedeemCodeRequest dto,
			HttpSession session) {
		String sessionId = (String) session.getAttribute("sessionId");
		RedeemCodeResponse resp = new RedeemCodeResponse();
		try {
			if (!CommonValidation.isNull(dto.getPromoCode())) {
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionId);
					resp = userApi.redeemPromoCode(dto);
					dto.setSessionId(sessionId);
					return new ResponseEntity<RedeemCodeResponse>(resp, HttpStatus.OK);
				   } else {
					resp.setCode("F00");
					resp.setMessage("Un-Authorized, User");
					resp.setDetails("Un-Authorized, User");
				 }
			   } else {
				resp.setCode("F00");
				resp.setMessage("Un-Authorized, Role");
				resp.setDetails("Un-Authorized, Role");
			     }
		       }
	          } else {
				resp.setCode("F00");
				resp.setMessage("Please enter PromoCode");
				resp.setDetails("Please enter PromoCode");
			}

		} catch (Exception e) {
			resp.setCode("F00");
			resp.setMessage("Internal Server Error");
			resp.setDetails("Internal Server Error");
		}
		return new ResponseEntity<RedeemCodeResponse>(resp, HttpStatus.OK);
	}

	@RequestMapping(value = "/KycRequest/process", method = RequestMethod.POST)
	public String processUpgradeWallet(@ModelAttribute("upgradeWallet") KycVerificationDTO dto, ModelMap modelMap,
			HttpSession session) {

		KycVerificationDTO verificationDTO = new KycVerificationDTO();
		KycResponse result = new KycResponse();
		KycError error = new KycError();
		String sessionId = (String) session.getAttribute("sessionId");
		verificationDTO.setSessionId(sessionId);
		verificationDTO.setAccountNumber(dto.getAccountNumber());
		verificationDTO.setMobileNumber(dto.getMobileNumber());
		if (sessionId != null && sessionId.length() != 0) {
			error = kycValidation.checkKycError(verificationDTO);
			if (error.isValid()) {
				result = userApi.kycVerification(verificationDTO);
			} else {
				result.setSuccess(false);
				result.setCode("F04");
				result.setDetails(error);
			}
		} else {
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("Unknown device");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		modelMap.addAttribute("resp", result.getCode());
		return "redirect:/Home";
	}

	@RequestMapping(value = "/OTP/kyc/Verify", method = RequestMethod.GET)
	public @ResponseBody String otpVerify(@RequestParam("key") String otp, HttpSession session,
			KycVerificationDTO dto) {
		KycResponse result = new KycResponse();
		String sessionId = (String) session.getAttribute("sessionId");
		String mobileNumber = (String) session.getAttribute("mobileNumber");
		KycError error = new KycError();
		if (sessionId != null && sessionId.length() != 0) {
			error = kycValidation.checkKycOTPError(dto);
			if (error.isValid()) {
				result = userApi.kycOTPVerification(dto);
			} else {
				result.setSuccess(false);
				result.setCode("F04");
				result.setDetails(error);
			}
		} else {
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("Unknown device");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}

		return "/User/Home";
	}

	@RequestMapping(value = "/Kyc/resendOtp", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<KycResponse> resendOTPOffLineMerchant(@RequestBody KycVerificationDTO dto, HttpSession session) {
		String sessionId = (String) session.getAttribute("sessionId");
		String mobileNumber = (String) session.getAttribute("mobileNumber");
		KycVerificationDTO req = new KycVerificationDTO();
		KycResponse result = new KycResponse();
		req.setSessionId(sessionId);
		req.setMobileNumber(mobileNumber);
		result = userApi.resendKycOTP(req);
		return new ResponseEntity<KycResponse>(result, HttpStatus.OK);
	}
	
	
	@RequestMapping(method = RequestMethod.GET, value = "/Travel")
	public String getUserByLocation(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model) throws JSONException {
		
	 return "/User/Travel";
	}

	@RequestMapping(value = "/Kyc/webUpdate", method = RequestMethod.POST)
	public ResponseEntity<AadharOTPResponse> updateKycFromWeb(@ModelAttribute("updateKycByAadhar") 
	AadharOTPDTO aadharRequest, HttpServletRequest request,HttpSession session) {
		AadharOTPResponse result = new AadharOTPResponse();
		String sessionId = (String) session.getAttribute("sessionId");
			if (sessionId != null && sessionId.length() != 0) {
				String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
				AadharOTPResponse	resp= new AadharOTPResponse();
				if (authority != null) {
					if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
						aadharRequest.setSessionId(sessionId);
						resp = aadharServiceApi.getAadharOtp(aadharRequest);
						return new ResponseEntity<AadharOTPResponse>(resp, HttpStatus.OK);
					}
				}result.setCode(ResponseStatus.INVALID_SESSION.getValue());
			}
		return new ResponseEntity<AadharOTPResponse>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/Kyc/webUpdateOtp", method = RequestMethod.POST)
	public ResponseEntity<AadharOTPValidateResponse> kycOtpVerification(@ModelAttribute("updateKycByAadhar") AadharOTPValidateDTO aadharRequest, 
			HttpServletRequest request,HttpSession session) {
		String sessionId = (String) session.getAttribute("sessionId");
		AadharOTPValidateResponse resp = new AadharOTPValidateResponse();
			if (sessionId != null && sessionId.length() != 0) {
				String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
				if (authority != null) {
					if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
						aadharRequest.setSessionId(sessionId);
						resp = aadharServiceApi.validateAadharOtp(aadharRequest);
						if (resp.isSuccess()) {
							aadharRequest.setCustomerName(resp.getCustomerName());
							aadharRequest.setCustomerAddress(resp.getCustomerAddress());
							resp = aadharServiceApi.saveAadhar(aadharRequest);
						}
						return new ResponseEntity<AadharOTPValidateResponse>(resp, HttpStatus.OK);
					}
				}resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
			}
		return new ResponseEntity<AadharOTPValidateResponse>(resp, HttpStatus.OK);
	}
	
}