package com.payqwikweb.controller.web.user;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.instantpay.api.IValidationApi;
import com.instantpay.model.request.ValidationRequest;
import com.instantpay.util.IPayConvertUtil;
import com.payqwikweb.app.api.ITransactionApi;
import com.payqwikweb.app.model.request.TransactionRequest;
import com.payqwikweb.model.web.CarrotFryDTO;
import com.payqwikweb.util.APIUtils;
import com.payqwikweb.util.ConvertUtil;
import com.thirdparty.model.ResponseDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.payqwikweb.api.IAuthenticationApi;
import com.payqwikweb.app.api.IAgentTopupApi;
import com.payqwikweb.app.api.ITopupApi;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.request.DataCardTopupRequest;
import com.payqwikweb.app.model.request.PostpaidTopupRequest;
import com.payqwikweb.app.model.request.PrepaidTopupRequest;
import com.payqwikweb.app.model.request.RechargeRequestDTO;
import com.payqwikweb.app.model.response.RechargeResponseDTO;
import com.payqwikweb.app.model.response.RechargeResponseDTO;
import com.payqwikweb.app.model.response.RechargeResponseDTO;
import com.payqwikweb.app.model.response.RechargeResponseDTO;
import com.payqwikweb.model.error.TopupError;
import com.payqwikweb.util.Authorities;
import com.payqwikweb.validation.TopupValidation;

@Controller
@RequestMapping("/Agent/MobileTopup")
public class AgentTopupController implements MessageSourceAware {


	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private MessageSource messageSource;

	private final IAgentTopupApi appAgentTopupApi;
	private final IAuthenticationApi authenticationApi;
	private final TopupValidation topupValidation;
	private final ITransactionApi transactionApi;
	private final IValidationApi validationApi;

	public AgentTopupController(IAgentTopupApi appAgentTopupApi, IAuthenticationApi authenticationApi, TopupValidation topupValidation,
			ITransactionApi transactionApi, IValidationApi validationApi) {
		this.appAgentTopupApi = appAgentTopupApi;
		this.authenticationApi = authenticationApi;
		this.topupValidation = topupValidation;
		this.transactionApi = transactionApi;
		this.validationApi = validationApi;
	}

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	@RequestMapping(value = "/AgentProcessPrepaid", method = RequestMethod.POST)
	ResponseEntity<RechargeResponseDTO> prePaidAgent(@ModelAttribute("topup") RechargeRequestDTO dto,
			HttpSession session) {

		RechargeResponseDTO result = new RechargeResponseDTO();
		TopupError error = topupValidation.checkError(dto);
		if (error.isValid()) {
			String sessionId = (String) session.getAttribute("sessionId");
			if (sessionId != null && sessionId.length() != 0) {
				String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
				if (authority != null) {
					if (authority.contains(Authorities.AGENT) && authority.contains(Authorities.AUTHENTICATED)) {
						TransactionRequest transactionRequest = new TransactionRequest();
						transactionRequest.setSessionId(sessionId);
						transactionRequest.setAmount(Double.parseDouble(dto.getAmount()));
							TransactionRequest newRequest = new TransactionRequest();
							newRequest.setSessionId(sessionId);
							newRequest.setAmount(Double.parseDouble(dto.getAmount()));
							newRequest.setTransactionRefNo("D");
							ResponseDTO responseDTO1 = transactionApi.validateAgentTransaction(newRequest);
							if (responseDTO1.isValid()) {
								dto.setSessionId(sessionId);
								result = appAgentTopupApi.prePaid(dto,Role.AGENT);
								// CarrotFryDTO carrotFryDTO = new
								// CarrotFryDTO();
								// carrotFryDTO.setAmount(dto.getAmount());
								// carrotFryDTO.setCity(" ");
								// carrotFryDTO.setEmail((String)
								// session.getAttribute("emailId"));
								// carrotFryDTO.setMobileNo((String)
								// session.getAttribute("username"));
								// couponApi.sendCarrotFryCoupons(carrotFryDTO);
							} else {
								result.setCode("F00");
								result.setMessage(responseDTO1.getMessage());
								result.setResponse(APIUtils.getCustomJSON("F00", responseDTO1.getMessage()).toString());
							}
						return new ResponseEntity<RechargeResponseDTO>(result, HttpStatus.OK);
					}
				}
			}
		}

		return new ResponseEntity<RechargeResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/ProcessAgentPostpaid", method = RequestMethod.POST)
	ResponseEntity<RechargeResponseDTO> postPaidAgent(@ModelAttribute("topup") RechargeRequestDTO dto,
			HttpSession session) {
		String sessionId = (String) session.getAttribute("sessionId");
		RechargeResponseDTO result = new RechargeResponseDTO();
		TopupError error = topupValidation.checkError(dto);
		if (error.isValid()) {
			if (sessionId != null && sessionId.length() != 0) {
				String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
				if (authority != null) {
					if (authority.contains(Authorities.AGENT) && authority.contains(Authorities.AUTHENTICATED)) {
						TransactionRequest transactionRequest = new TransactionRequest();
						transactionRequest.setSessionId(sessionId);
						transactionRequest.setAmount(Double.parseDouble(dto.getAmount()));
							TransactionRequest newRequest = new TransactionRequest();
							newRequest.setSessionId(sessionId);
							newRequest.setAmount(Double.parseDouble(dto.getAmount()));
							newRequest.setTransactionRefNo("D");
							ResponseDTO responseDTO1 = transactionApi.validateAgentTransaction(newRequest);
							if (responseDTO1.isValid()) {
								dto.setSessionId(sessionId);
								result = appAgentTopupApi.prePaid(dto,Role.AGENT);
								// CarrotFryDTO carrotFryDTO = new
								// CarrotFryDTO();
								// carrotFryDTO.setAmount(dto.getAmount());
								// carrotFryDTO.setCity(" ");
								// carrotFryDTO.setEmail((String)
								// session.getAttribute("emailId"));
								// carrotFryDTO.setMobileNo((String)
								// session.getAttribute("username"));
								// couponApi.sendCarrotFryCoupons(carrotFryDTO);
							} else {
								result.setMessage(responseDTO1.getMessage());
								result.setCode("F00");
								result.setResponse(APIUtils.getCustomJSON("F00", responseDTO1.getMessage()).toString());
							}
						
						return new ResponseEntity<RechargeResponseDTO>(result, HttpStatus.OK);
					}
				}
			}
		} else {
			result.setCode("F00");
			result.setMessage("Invalid Input");
		}
		return new ResponseEntity<RechargeResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/AgentProcessDataCard", method = RequestMethod.POST)
	ResponseEntity<RechargeResponseDTO> dataCardAgent(@ModelAttribute("topup") RechargeRequestDTO dto,
			HttpSession session) {
		String sessionId = (String) session.getAttribute("sessionId");
		RechargeResponseDTO result = new RechargeResponseDTO();
		TopupError error = topupValidation.checkError(dto);
		if (error.isValid()) {
			if (sessionId != null && sessionId.length() != 0) {
				String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
				if (authority != null) {
					if (authority.contains(Authorities.AGENT) && authority.contains(Authorities.AUTHENTICATED)) {
						TransactionRequest transactionRequest = new TransactionRequest();
						transactionRequest.setSessionId(sessionId);
						transactionRequest.setAmount(Double.parseDouble(dto.getAmount()));
							TransactionRequest newRequest = new TransactionRequest();
							newRequest.setSessionId(sessionId);
							newRequest.setAmount(Double.parseDouble(dto.getAmount()));
							newRequest.setTransactionRefNo("D");
							ResponseDTO responseDTO1 = transactionApi.validateAgentTransaction(newRequest);
							if (responseDTO1.isValid()) {
								dto.setSessionId(sessionId);
								result = appAgentTopupApi.prePaid(dto,Role.AGENT);
							} else {
								result.setCode("F00");
								result.setMessage(responseDTO1.getMessage());
								result.setResponse(APIUtils.getCustomJSON("F00", responseDTO1.getMessage()).toString());
							}

						return new ResponseEntity<RechargeResponseDTO>(result, HttpStatus.OK);
					}
				}
			}
		} else {
			result.setCode("F00");
			result.setMessage("Invalid Input");
		}
		return new ResponseEntity<RechargeResponseDTO>(result, HttpStatus.OK);
	}

}
