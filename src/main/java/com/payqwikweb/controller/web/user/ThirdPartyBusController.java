package com.payqwikweb.controller.web.user;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.payqwikweb.app.thirdparty.api.IThirdPartyTravelBusApi;
import com.payqwikweb.model.request.thirdpartyService.BlockBusRequest;
import com.payqwikweb.model.request.thirdpartyService.GetbusbookingetailRequest;
import com.payqwikweb.model.request.thirdpartyService.GetbusdeatailRequest;
import com.payqwikweb.model.request.thirdpartyService.SourcesRequest;
import com.payqwikweb.model.request.thirdpartyService.SourcesResponse;
import com.payqwikweb.model.response.thirdpartyService.Availabelbusdesginresponse;
import com.payqwikweb.model.response.thirdpartyService.AvailabelseatsResponse;
import com.payqwikweb.model.response.thirdpartyService.BlockBusResponse;
import com.payqwikweb.model.response.thirdpartyService.GetbusbookingetailResponse;
import com.payqwikweb.model.response.thirdpartyService.GetbusdeatailResponse;
import com.payqwikweb.model.response.thirdpartyService.MultipleBlockSeatResponse;

@Controller
@RequestMapping("/Api/{version}/{role}/{device}/{language}/ThirdParty/Travel/Buses")
public class ThirdPartyBusController {

	protected Logger logger = LoggerFactory.getLogger(this.getClass());

	private MessageSource messageSource;

	private IThirdPartyTravelBusApi thirdPartyTravelBusApi;

	public ThirdPartyBusController(IThirdPartyTravelBusApi thirdPartyTravelBusApi) {
		this.thirdPartyTravelBusApi = thirdPartyTravelBusApi;
	}

	////////////////////////////////////////// SONU /////////////////////////

	@RequestMapping(method = RequestMethod.POST, value = "/Travel/BusTravel")
	public ResponseEntity<String> gettrevelbus(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device) {

		SourcesRequest request1 = new SourcesRequest();
		// request1.setConsumerSecret("04DAF1E2C80A758DCC6BCEBE436D25BC");
		// request1.setConsumerKey("59285FC07292811D72785AC287ACE3E8");
		if (device.equalsIgnoreCase("Mobile")) {
			SourcesResponse sourcesResponse = thirdPartyTravelBusApi.getsources(request1);
			return new ResponseEntity<String>(sourcesResponse.getMsg(), HttpStatus.OK);

		} else {

			return new ResponseEntity<String>("No data", HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/Getbusdeatail", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<String> getbusonway(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device,
			HttpSession session, @RequestBody GetbusdeatailRequest request)

	{
		if (device.equalsIgnoreCase("Website")) {
			request.setUserType("5");
			if (request.getReturnDate().equals("")) {
				request.setTripType("1");
				request.setReturnDate(request.getDatepickeronewaydeparure());
			} else {
				request.setTripType("2");
			}
			String data = "";
			String data1 = "";
			String data2 = "";
			String id = request.getCitydestoneway();
			String name = request.getCitysourceoneway();
			Availabelbusdesginresponse c = new Availabelbusdesginresponse();
			ArrayList<Availabelbusdesginresponse> desginbus = new ArrayList<Availabelbusdesginresponse>();
			ArrayList<Availabelbusdesginresponse> bordingpoints = new ArrayList<Availabelbusdesginresponse>();
			String ids[] = id.split("@@");
			String namde[] = name.split("@@");
			session.setAttribute("destinationname", "" + ids[1]);
			session.setAttribute("sourcename", "" + namde[0]);
			request.setCitysourceoneway(namde[1]);
			request.setCitydestoneway(ids[0]);
			try {
				GetbusdeatailResponse resp = thirdPartyTravelBusApi.getBusesdeatial(request);
				if (resp.getCode().equals("200")) {
					JSONObject jsonarray = new JSONObject(resp.getMsg());
					JSONArray jsonarray1 = jsonarray.getJSONArray("AvailableTrips");
					for (int i = 0; i < jsonarray1.length(); i++) {
						JSONArray jsonarray2 = jsonarray1.getJSONObject(i).getJSONArray("BoardingTimes");
						Availabelbusdesginresponse objavailabelsbusdesgin = new Availabelbusdesginresponse();
						for (int j = 0; j < jsonarray2.length(); j++) {
							Availabelbusdesginresponse objavailabelsbus = new Availabelbusdesginresponse();
							String Landmark = jsonarray2.getJSONObject(j).getString("Location");
							String PointId = jsonarray2.getJSONObject(j).getString("PointId");
							objavailabelsbusdesgin.setLandmark(Landmark);
							objavailabelsbusdesgin.setPointId(PointId);
							bordingpoints.add(objavailabelsbus);
						}
						String DisplayName = jsonarray1.getJSONObject(i).getString("DisplayName");
						String BusType = jsonarray1.getJSONObject(i).getString("BusType");
						String DepartureTime = jsonarray1.getJSONObject(i).getString("DepartureTime");
						String Duration = jsonarray1.getJSONObject(i).getString("Duration");
						String Fares = jsonarray1.getJSONObject(i).getString("Fares");
						String Journeydate = jsonarray1.getJSONObject(i).getString("Journeydate");
						String AvailableSeats = jsonarray1.getJSONObject(i).getString("AvailableSeats");
						String ArrivalTime = jsonarray1.getJSONObject(i).getString("ArrivalTime");
						String tripid = jsonarray1.getJSONObject(i).getString("Id");
						String porvidr = jsonarray1.getJSONObject(i).getString("Provider");
						String travels = jsonarray1.getJSONObject(i).getString("Travels");
						String CancellationPolicy = jsonarray1.getJSONObject(i).getString("CancellationPolicy");
						String ConvenienceFee = jsonarray1.getJSONObject(i).getString("ConvenienceFee");
						String PartialCancellationAllowed = jsonarray1.getJSONObject(i)
								.getString("PartialCancellationAllowed");
						objavailabelsbusdesgin.setDisplayName(DisplayName);
						objavailabelsbusdesgin.setDuration(Duration);
						objavailabelsbusdesgin.setAvailableSeats(AvailableSeats);
						objavailabelsbusdesgin.setTripid(tripid);
						objavailabelsbusdesgin.setPorvidr(porvidr);
						objavailabelsbusdesgin.setTravels(travels);
						objavailabelsbusdesgin.setCitydestoneway(request.getCitydestoneway());
						objavailabelsbusdesgin.setCitysourceoneway(request.getCitysourceoneway());
						objavailabelsbusdesgin.setDatepickeronewaydeparure(request.getDatepickeronewaydeparure());
						objavailabelsbusdesgin.setI("" + i);
						objavailabelsbusdesgin.setCancellationPolicy(CancellationPolicy);
						objavailabelsbusdesgin.setConvenienceFee(ConvenienceFee);
						objavailabelsbusdesgin.setPartialCancellationAllowed(PartialCancellationAllowed);
						objavailabelsbusdesgin.setBusType(BusType);
						objavailabelsbusdesgin.setDepartureTime(DepartureTime);
						objavailabelsbusdesgin.setFares(Fares);
						desginbus.add(objavailabelsbusdesgin);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			session.setAttribute("Triptype", request.getTripType());
			session.setAttribute("busalltail", desginbus);
			session.setAttribute("boisrindpoints", bordingpoints);
			return new ResponseEntity<String>("" + desginbus.size(), HttpStatus.OK);
		} else if (device.equalsIgnoreCase("Mobile")) {
			request.setUserType("5");
			if (request.getReturnDate().equals("")) {
				request.setTripType("1");
				request.setReturnDate(request.getDatepickeronewaydeparure());
			} else {
				request.setTripType("2");
			}
			String data = "";
			String data1 = "";
			String data2 = "";
			GetbusdeatailResponse resp = thirdPartyTravelBusApi.getBusesdeatial(request);
			if (resp.getCode().equals("200")) {
				data = resp.getMsg();
				return new ResponseEntity<String>(data, HttpStatus.OK);

			} else {
				data = resp.getMsg();
				return new ResponseEntity<String>(data, HttpStatus.OK);
			}
		} else {
			return new ResponseEntity<String>("ddd", HttpStatus.OK);

		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/getcheckbusbookdetial", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody ResponseEntity<String> checkbusbook(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, HttpServletRequest request, HttpServletResponse response,
			HttpSession session, ModelMap modelMap, @RequestBody GetbusbookingetailRequest requestGetbusbookinget)
			throws Exception {
		if (device.equalsIgnoreCase("Website")) {
			String d = requestGetbusbookinget.getPorvidr().replace(" ", "+");
			String d1 = requestGetbusbookinget.getTravels().replace(" ", "");
			String d2 = requestGetbusbookinget.getJourneyDate().replace(" ", "");
			requestGetbusbookinget.setJourneyDate(d2);
			requestGetbusbookinget.setPorvidr(d);
			requestGetbusbookinget.setTravels(d1);
			requestGetbusbookinget.setTripType("1");
			requestGetbusbookinget.setUserType("5");
			session.setAttribute("destinationId", requestGetbusbookinget.getDestinationId());
			session.setAttribute("sourceId", requestGetbusbookinget.getSourceId());
			session.setAttribute("journeyDate", requestGetbusbookinget.getJourneyDate());
			session.setAttribute("provider", requestGetbusbookinget.getPorvidr());
			session.setAttribute("travelOperator", requestGetbusbookinget.getTravels());
			session.setAttribute("tripId", requestGetbusbookinget.getTripid());
			String data1 = "";
			String data2 = "";
			String data3 = "";
			String data4 = "";
			String data5 = "";
			String data6 = "";
			String data7 = "";
			String data8 = "";
			GetbusbookingetailResponse resp = thirdPartyTravelBusApi.getBusesbookingdeatial(requestGetbusbookinget);
			JSONObject jsonarray = new JSONObject(resp.getMsg());
			JSONArray jsonarray1 = jsonarray.getJSONArray("Seats");
			ArrayList<AvailabelseatsResponse> seats = new ArrayList<>();
			for (int j = 0; j < jsonarray1.length(); j++) {
				AvailabelseatsResponse seatsvalstore = new AvailabelseatsResponse();
				String a = jsonarray1.getJSONObject(j).getString("Fare");
				String a1 = jsonarray1.getJSONObject(j).getString("NetFare");
				String a2 = jsonarray1.getJSONObject(j).getString("IsAvailableSeat");
				String a3 = jsonarray1.getJSONObject(j).getString("IsLadiesSeat");
				String a4 = jsonarray1.getJSONObject(j).getString("Length");
				String a5 = jsonarray1.getJSONObject(j).getString("Number");
				String a6 = jsonarray1.getJSONObject(j).getString("Zindex");
				String a7 = jsonarray1.getJSONObject(j).getString("Servicetax");
				String a8 = jsonarray1.getJSONObject(j).getString("OperatorServiceCharge");
				String row = jsonarray1.getJSONObject(j).getString("Row");
				String col = jsonarray1.getJSONObject(j).getString("Column");
				String width = jsonarray1.getJSONObject(j).getString("Width");
				if (a2.equalsIgnoreCase("true")) {
					// System.err.println("Available Seat ::::::"+a2);
					if (a6.equalsIgnoreCase("0")) {
						if (a4.equalsIgnoreCase("1") && width.equalsIgnoreCase("1")) {
							data1 += a5 + "##";
							data2 += a + "@@";
							data4 += a8 + "$$";
							data5 += a7 + "%%";
							data8 += "book" + "^^";
						} else if (a4.equalsIgnoreCase("2") && width.equalsIgnoreCase("1")) {
							data1 += a5 + "##";
							data2 += a + "@@";
							data4 += a8 + "$$";
							data5 += a7 + "%%";
							data8 += "book" + "^^";
						} else {
							data1 += a5 + "##";
							data2 += a + "@@";
							data4 += a8 + "$$";
							data5 += a7 + "%%";
							data8 += "book" + "^^";
						}

					} else {
						if (a4.equalsIgnoreCase("1") && width.equalsIgnoreCase("1")) {
							data1 += a5 + "##";
							data2 += a + "@@";
							data4 += a8 + "$$";
							data5 += a7 + "%%";
							data8 += "book" + "^^";
						} else if (a4.equalsIgnoreCase("2") && width.equalsIgnoreCase("1")) {
							data1 += a5 + "##";
							data2 += a + "@@";
							data4 += a8 + "$$";
							data5 += a7 + "%%";
							data8 += "book" + "^^";
						} else {
							data1 += a5 + "##";
							data2 += a + "@@";
							data4 += a8 + "$$";
							data5 += a7 + "%%";
							data8 += "book" + "^^";
						}

					}

					seatsvalstore.setNumber(a5);
					seatsvalstore.setFare(a);
					seatsvalstore.setI("" + j);
				} else {
					if (a6.equalsIgnoreCase("0")) {
						if (a4.equalsIgnoreCase("1") && width.equalsIgnoreCase("1")) {
							data1 += a5 + "##";
							data2 += a + "@@";
							data4 += a8 + "$$";
							data5 += a7 + "%%";
							data8 += "unbook" + "^^";
						} else if (a4.equalsIgnoreCase("2") && width.equalsIgnoreCase("1")) {
							data1 += a5 + "##";
							data2 += a + "@@";
							data4 += a8 + "$$";
							data5 += a7 + "%%";
							data8 += "unbook" + "^^";
						} else {
							data1 += a5 + "##";
							data2 += a + "@@";
							data4 += a8 + "$$";
							data5 += a7 + "%%";
							data8 += "unbook" + "^^";
						}

					} else {
						if (a4.equalsIgnoreCase("1") && width.equalsIgnoreCase("1")) {
							data1 += a5 + "##";
							data2 += a + "@@";
							data4 += a8 + "$$";
							data5 += a7 + "%%";
							data8 += "unbook" + "^^";
						} else if (a4.equalsIgnoreCase("2") && width.equalsIgnoreCase("1")) {
							data1 += a5 + "##";
							data2 += a + "@@";
							data4 += a8 + "$$";
							data5 += a7 + "%%";
							data8 += "unbook" + "^^";
						} else {
							data1 += a5 + "##";
							data2 += a + "@@";
							data4 += a8 + "$$";
							data5 += a7 + "%%";
							data8 += "unbook" + "^^";
						}
					}
				}
			}
			data3 += data1 + "**" + data2;
			data6 += data4 + "~~" + data5;
			session.setAttribute("seatsdestail", seats);
			return new ResponseEntity<String>(data3 + "&&" + data6 + "!!" + data8, HttpStatus.OK);
		} else if (device.equalsIgnoreCase("Mobile")) {
			String d = requestGetbusbookinget.getTravels().replaceAll(" ", "");
			requestGetbusbookinget.setTravels(d);
			requestGetbusbookinget.setTripType("1");
			requestGetbusbookinget.setUserType("5");
			GetbusbookingetailResponse resp = thirdPartyTravelBusApi.getBusesbookingdeatial(requestGetbusbookinget);
			return new ResponseEntity<String>(resp.getMsg(), HttpStatus.OK);
		} else {
			return new ResponseEntity<String>("null", HttpStatus.OK);
		}

	}

	@RequestMapping(method = RequestMethod.POST, value = "/Busbookingandgetticket", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody ResponseEntity<String> busblock(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device,
			HttpSession session, @RequestBody BlockBusRequest requestBlock) throws Exception {
		String data = "";
		if (device.equalsIgnoreCase("Website")) {
			String bordingpts[] = requestBlock.getUser_boisrindpointsbusdeatil().split("##");
			requestBlock.setTripId("" + session.getAttribute("tripId"));
			requestBlock.setBoardingId("" + bordingpts[1]);
			requestBlock.setAddress("hyderbad");
			requestBlock.setPostalCode("502278");
			requestBlock.setProvider("" + session.getAttribute("provider"));
			requestBlock.setOperator("" + session.getAttribute("travelOperator"));
			requestBlock.setBoardingPointDetails("" + bordingpts[0]);
			requestBlock.setSourceId("" + session.getAttribute("sourceId"));
			requestBlock.setSourceName("" + session.getAttribute("sourcename"));
			requestBlock.setDestinationId("" + session.getAttribute("destinationId"));
			requestBlock.setDestinationName("" + session.getAttribute("destinationname"));
			requestBlock.setJourneyDate("" + session.getAttribute("journeyDate"));
			requestBlock.setTripType("1");
			requestBlock.setBusType("4");
			requestBlock.setUser("null");
			requestBlock.setUserType("5");
			requestBlock.setSessionid("" + session.getAttribute("sessionId"));
			/*
			 * String seat=requestBlock.getSeatNos(); String
			 * er=seat.replaceAll(",", "~"); requestBlock.setSeatNos( er);
			 */
			long transctionrefno = System.currentTimeMillis();
			requestBlock.setTransctionrefno("" + transctionrefno);
			BlockBusResponse resp = thirdPartyTravelBusApi.blockBusTicketuser(requestBlock);
			{
				requestBlock.setTotalamount("" + requestBlock.getFares());
				// String ip = (String) session.getAttribute("ipaddress");
				String appapical = thirdPartyTravelBusApi.bookbusticketcallapi(requestBlock,
						"" + requestBlock.getTransctionrefno());
				JSONObject obj = new JSONObject(appapical);
				//
				// String status=obj.getString("code");
				String msg = obj.getString("code");
				if (msg.equalsIgnoreCase("S00")) {
					String resp1 = thirdPartyTravelBusApi.bookBusTicketinweb(resp.getCode());
					String c = thirdPartyTravelBusApi.getbusticketbook(resp.getCode());
					JSONObject ticket = new JSONObject(c);
					String a = ticket.getString("BookingDate");
					String a1 = ticket.getString("MobileNo");
					String a2 = ticket.getString("EmailId");
					String a3 = ticket.getString("BookingRefNo");
					String a4 = ticket.getString("JourneyDate");
					String a5 = ticket.getString("Names");
					String a6 = ticket.getString("SeatNos");
					String a7 = ticket.getString("DepartureTime");
					String a8 = ticket.getString("SourceName");
					String desti = ticket.getString("DestinationName");
					String bookid = ticket.getString("BlockId");
					String a9 = ticket.getString("BusTypeName");
					String a10 = ticket.getString("DisplayName");
					String a11 = ticket.getString("DepartureTime");
					String a12 = ticket.getString("SeatNos");
					String a13 = ticket.getJSONObject("BoardingDroppingDetails").getString("Location");
					String a14 = ticket.getString("Names");
					String a15 = ticket.getString("ActualFare");
					String a16 = ticket.getString("Servicetax");
					String a17 = ticket.getString("ServiceCharge");
					String a18 = ticket.getString("ClientID");
					double d1 = Double.parseDouble(a15);
					double d2 = Double.parseDouble(a16);
					double d3 = Double.parseDouble(a17);
					double total = d1 + d2 + d3;
					//
					String databussuucess = thirdPartyTravelBusApi.busbookingsucces(a3, bookid, a18, a,
							requestBlock.getTransctionrefno(), requestBlock.getSessionid());

					data += a1 + "@@" + a2 + "@@" + a3 + "@@" + bookid + "@@" + a13 + "@@" + a4 + "@@" + a8 + "@@"
							+ desti + "@@" + a10 + "@@" + a9 + "@@" + a4 + "@@" + a11 + "@@" + a14 + "@@" + a12 + "@@"
							+ a15 + "@@" + a16 + "@@" + a17 + "@@" + total + "@@" + a + "@@" + a7;
					return new ResponseEntity<String>(data, HttpStatus.OK);
				} else {
					return new ResponseEntity<String>(appapical, HttpStatus.OK);
				}

			}
		} else if (device.equalsIgnoreCase("Mobile")) {
			requestBlock.setTripType("1");
			requestBlock.setBusType("4");
			requestBlock.setUser("null");
			requestBlock.setUserType("5");
			requestBlock.setAddress("hgjghj");
			long transctionrefno = System.currentTimeMillis();
			System.out.println(transctionrefno);
			requestBlock.setTransctionrefno("" + transctionrefno);

			// requestBlock.setSourceName("Hyderabad");

			BlockBusResponse resp = thirdPartyTravelBusApi.blockBusTicketuser(requestBlock);
			requestBlock.setTotalamount("" + requestBlock.getFares());
			if (resp.getCodecondtion().equals("200")) {
				// System.out.println(requestBlock.getTransctionrefno());
				String appapical = thirdPartyTravelBusApi.bookbusticketcallapi(requestBlock,
						"" + requestBlock.getTransctionrefno());
				JSONObject obj = new JSONObject(appapical);
				String msg = obj.getString("code");
				if (msg.equalsIgnoreCase("S00")) {
					// System.out.println(resp.getCode());
					String resp1 = thirdPartyTravelBusApi.bookBusTicketinweb(resp.getCode());
					String c = thirdPartyTravelBusApi.getbusticketbook(resp.getCode());
					JSONObject ticket = new JSONObject(c);
					String a = ticket.getString("BookingDate");
					String a3 = ticket.getString("BookingRefNo");
					String bookid = ticket.getString("BlockId");
					String a18 = ticket.getString("ClientID");
					String databussuucess = thirdPartyTravelBusApi.busbookingsucces("" + transctionrefno,
							"" + transctionrefno, "" + transctionrefno, "" + transctionrefno, "" + transctionrefno,
							requestBlock.getSessionid());
					return new ResponseEntity<String>(resp1, HttpStatus.OK);
				} else {
					return new ResponseEntity<String>(appapical, HttpStatus.OK);
				}
			} else {
				return new ResponseEntity<String>(resp.getMsg(), HttpStatus.OK);
			}
		} else {
			return new ResponseEntity<String>(data, HttpStatus.OK);
		}

	}

	public static String reafile() {
		String data = "";
		try {
			String val = System.getProperty("user.dir");
			File f = new File("/usr/local/apps/tomcat/webapps/ROOT/WebContent/resources/hkjkj.txt");
			// File f = new File(val + "/WebContent/resources/hkjkj.txt");
			// if(f.exists())
			// {
			// System.out.println(f.getAbsolutePath());
			FileInputStream fin = new FileInputStream(f);
			byte[] buffer = new byte[(int) f.length()];
			new DataInputStream(fin).readFully(buffer);
			fin.close();
			data = new String(buffer, "UTF-8");
			//
			// }

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return data;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/MultipleBusBook", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody ResponseEntity<String> busblock1(@PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device,
			HttpSession session,  @RequestBody BlockBusRequest requestBlock) throws Exception {
		String data = "";
		if (device.equalsIgnoreCase("Website")) {
			String bordingpts[] = requestBlock.getUser_boisrindpointsbusdeatil().split("##");
			requestBlock.setTripId("" + session.getAttribute("tripId"));
			requestBlock.setBoardingId("" + bordingpts[1]);
			requestBlock.setAddress("hyderbad");
			requestBlock.setPostalCode("502278");
			requestBlock.setProvider("" + session.getAttribute("provider"));
			requestBlock.setOperator("" + session.getAttribute("travelOperator"));
			requestBlock.setBoardingPointDetails("" + bordingpts[0]);
			requestBlock.setSourceId("" + session.getAttribute("sourceId"));
			requestBlock.setSourceName("" + session.getAttribute("sourcename"));
			requestBlock.setDestinationId("" + session.getAttribute("destinationId"));
			requestBlock.setDestinationName("" + session.getAttribute("destinationname"));
			requestBlock.setJourneyDate("" + session.getAttribute("journeyDate"));
			requestBlock.setTripType("1");
			requestBlock.setBusType("4");
			requestBlock.setUser("null");
			requestBlock.setUserType("5");
			requestBlock.setSessionid("" + session.getAttribute("sessionId"));
			String seat = requestBlock.getSeatNos();
			String seatno = seat.replaceAll(",", "~");
			requestBlock.setSeatNos(seatno);
			String fare = requestBlock.getFares();
			String fares = fare.replaceAll(",", "~");
			requestBlock.setFares(fares);
			String age = requestBlock.getAges();
			String age1 = age.replaceAll(",", "~");
			requestBlock.setAges(age1);
			String nam = requestBlock.getNames();
			String nam1 = nam.replaceAll(",", "~");
			requestBlock.setNames(nam1);
			if (requestBlock.getNoofSeats().equals("2")) {
				String servicetax = requestBlock.getServiceTax();
				String serviceCharge = requestBlock.getServiceCharge();
				requestBlock.setServiceTax("" + servicetax + "~" + servicetax);
				requestBlock.setServiceCharge(serviceCharge + "~" + serviceCharge);
				String title = requestBlock.getTitles();
				requestBlock.setTitles(title + "~" + title);
				String gen = requestBlock.getGenders();
				requestBlock.setGenders(gen + "~" + gen);
			} else if (requestBlock.getNoofSeats().equals("3")) {
				String servicetax = requestBlock.getServiceTax();
				String serviceCharge = requestBlock.getServiceCharge();
				requestBlock.setServiceTax("" + servicetax + "~" + servicetax + "~" + servicetax);
				requestBlock.setServiceCharge(serviceCharge + "~" + serviceCharge + "~" + serviceCharge);
				String title = requestBlock.getTitles();
				requestBlock.setTitles(title + "~" + title + "~" + title);
				String gen = requestBlock.getGenders();
				requestBlock.setGenders(gen + "~" + gen + "~" + gen);
			} else if (requestBlock.getNoofSeats().equals("4")) {
				String servicetax = requestBlock.getServiceTax();
				String serviceCharge = requestBlock.getServiceCharge();
				requestBlock.setServiceTax("" + servicetax + "~" + servicetax + "~" + servicetax + "~" + servicetax);
				requestBlock.setServiceCharge(
						serviceCharge + "~" + serviceCharge + "~" + serviceCharge + "~" + serviceCharge);
				String title = requestBlock.getTitles();
				requestBlock.setTitles(title + "~" + title + "~" + title + "~" + title);
				String gen = requestBlock.getGenders();
				requestBlock.setGenders(gen + "~" + gen + "~" + gen + "~" + gen);
			}
			long transctionrefno = System.currentTimeMillis();
			requestBlock.setTransctionrefno("" + transctionrefno);
			MultipleBlockSeatResponse resp = thirdPartyTravelBusApi.multipleSeat(requestBlock);
			String appapical = thirdPartyTravelBusApi.bookbusticketcallapi(requestBlock, resp.getCode());
			{
				if (true) {
					String resp1 = thirdPartyTravelBusApi.bookBusTicketinweb(resp.getCode());
					String c = thirdPartyTravelBusApi.getbusticketbook(resp.getCode());
					JSONObject ticket = new JSONObject(c);
					String a = ticket.getString("BookingDate");
					String a1 = ticket.getString("MobileNo");
					String a2 = ticket.getString("EmailId");
					String a3 = ticket.getString("BookingRefNo");
					String a4 = ticket.getString("JourneyDate");
					String a5 = ticket.getString("Names");
					String a6 = ticket.getString("SeatNos");
					String a7 = ticket.getString("DepartureTime");
					String a8 = ticket.getString("SourceName");
					String desti = ticket.getString("DestinationName");
					String bookid = ticket.getString("BlockId");
					String a9 = ticket.getString("BusTypeName");
					String a10 = ticket.getString("DisplayName");
					String a11 = ticket.getString("DepartureTime");
					String a12 = ticket.getString("SeatNos");
					String a13 = ticket.getJSONObject("BoardingDroppingDetails").getString("Location");
					String a14 = ticket.getString("Names");
					String a15 = ticket.getString("ActualFare");
					String a16 = ticket.getString("Servicetax");
					String a17 = ticket.getString("ServiceCharge");
					String a18 = ticket.getString("ClientID");
					// double d1 = Double.parseDouble(a15);
					// double d2 = Double.parseDouble(a16);
					// double d3 = Double.parseDouble(a17);
					//
					// double total = 67;
					// d1 + d2 + d3;
					String databussuucess = thirdPartyTravelBusApi.busbookingsucces(a3, bookid, a18, a,
							requestBlock.getTransctionrefno(), requestBlock.getSessionid());
					/*
					 * data += a1 + "@@" + a2 + "@@" + a3 + "@@" + bookid + "@@"
					 * + a13 + "@@" + a4 + "@@" + a8 + "@@" + desti + "@@" + a10
					 * + "@@" + a9 + "@@" + a4 + "@@" + a11 + "@@" + a14 + "@@"
					 * + a12 + "@@" + a15 + "@@" + a16 + "@@" + a17 + "@@" +
					 * total + "@@" + a + "@@" + a7;
					 */
					data += a1 + "@@" + a2 + "@@" + a3 + "@@" + bookid + "@@" + a13 + "@@" + a4 + "@@" + a8 + "@@"
							+ desti + "@@" + a10 + "@@" + a9 + "@@" + a4 + "@@" + a11 + "@@" + a14 + "@@" + a12 + "@@"
							+ a15 + "@@" + a16 + "@@" + a17 + "@@" + fare + "@@" + a + "@@" + a7;
					return new ResponseEntity<String>(data, HttpStatus.OK);
				} else {
					return new ResponseEntity<String>("", HttpStatus.OK);
				}

			}
		} else if (device.equalsIgnoreCase("Mobile")) {
			requestBlock.setTripType("1");
			requestBlock.setBusType("4");
			requestBlock.setUser("null");
			requestBlock.setUserType("5");
			if (requestBlock.getNoofSeats().equals("2")) {
				String servicetax = requestBlock.getServiceTax();
				String serviceCharge = requestBlock.getServiceCharge();
				String fare = requestBlock.getFares();
				String servicetax1[] = servicetax.split("~");
				String serviceCharge1[] = serviceCharge.split("~");
				String fare1[] = fare.split("~");
				double f1 = Double.parseDouble(fare1[0]);
				double f2 = Double.parseDouble(fare1[1]);
				double st1 = Double.parseDouble(servicetax1[0]);
				double st2 = Double.parseDouble(servicetax1[1]);
				double sc1 = Double.parseDouble(serviceCharge1[0]);
				double sc2 = Double.parseDouble(serviceCharge1[1]);
				double total = f1 + f2 + st1 + st2 + sc1 + sc2;
				requestBlock.setTotalamount("" + total);
			} else if (requestBlock.getNoofSeats().equals("3")) {
				String servicetax = requestBlock.getServiceTax();
				String serviceCharge = requestBlock.getServiceCharge();
				String fare = requestBlock.getFares();
				String servicetax1[] = servicetax.split("~");
				String serviceCharge1[] = serviceCharge.split("~");
				String fare1[] = fare.split("~");
				double f1 = Double.parseDouble(fare1[0]);
				double f2 = Double.parseDouble(fare1[1]);
				double f3 = Double.parseDouble(fare1[2]);
				double st1 = Double.parseDouble(servicetax1[0]);
				double st2 = Double.parseDouble(servicetax1[1]);
				double st3 = Double.parseDouble(servicetax1[2]);
				double sc1 = Double.parseDouble(serviceCharge1[0]);
				double sc2 = Double.parseDouble(serviceCharge1[1]);
				double sc3 = Double.parseDouble(serviceCharge1[2]);
				double total = f1 + f2 + f3 + st1 + st2 + st3 + sc1 + sc2 + sc3;
				requestBlock.setTotalamount("" + total);
			} else if (requestBlock.getNoofSeats().equals("4")) {
				String servicetax = requestBlock.getServiceTax();
				String serviceCharge = requestBlock.getServiceCharge();
				String fare = requestBlock.getFares();
				String servicetax1[] = servicetax.split("~");
				String serviceCharge1[] = serviceCharge.split("~");
				String fare1[] = fare.split("~");
				double f1 = Double.parseDouble(fare1[0]);
				double f2 = Double.parseDouble(fare1[1]);
				double f3 = Double.parseDouble(fare1[2]);
				double f4 = Double.parseDouble(fare1[3]);
				double st1 = Double.parseDouble(servicetax1[0]);
				double st2 = Double.parseDouble(servicetax1[1]);
				double st3 = Double.parseDouble(servicetax1[2]);
				double st4 = Double.parseDouble(servicetax1[3]);
				double sc1 = Double.parseDouble(serviceCharge1[0]);
				double sc2 = Double.parseDouble(serviceCharge1[1]);
				double sc3 = Double.parseDouble(serviceCharge1[2]);
				double sc4 = Double.parseDouble(serviceCharge1[3]);
				double total = f1 + f2 + f3 + f4 + st1 + st2 + st3 + st3 + sc1 + sc2 + sc3 + sc4;
				requestBlock.setTotalamount("" + total);
			} else if (requestBlock.getNoofSeats().equals("5")) {
				String servicetax = requestBlock.getServiceTax();
				String serviceCharge = requestBlock.getServiceCharge();
				String fare = requestBlock.getFares();
				String servicetax1[] = servicetax.split("~");
				String serviceCharge1[] = serviceCharge.split("~");
				String fare1[] = fare.split("~");
				double f1 = Double.parseDouble(fare1[0]);
				double f2 = Double.parseDouble(fare1[1]);
				double f3 = Double.parseDouble(fare1[2]);
				double f4 = Double.parseDouble(fare1[3]);
				double f5 = Double.parseDouble(fare1[4]);
				double st1 = Double.parseDouble(servicetax1[0]);
				double st2 = Double.parseDouble(servicetax1[1]);
				double st3 = Double.parseDouble(servicetax1[2]);
				double st4 = Double.parseDouble(servicetax1[3]);
				double st5 = Double.parseDouble(servicetax1[4]);
				double sc1 = Double.parseDouble(serviceCharge1[0]);
				double sc2 = Double.parseDouble(serviceCharge1[1]);
				double sc3 = Double.parseDouble(serviceCharge1[2]);
				double sc4 = Double.parseDouble(serviceCharge1[3]);
				double sc5 = Double.parseDouble(serviceCharge1[4]);
				double total = f1 + f2 + f3 + f4 + f5 + st1 + st2 + st3 + st5 + st4 + sc1 + sc2 + sc3 + sc4 + sc5;
				System.out.println(total);
				requestBlock.setTotalamount("" + total);
			} else if (requestBlock.getNoofSeats().equals("6")) {
				String servicetax = requestBlock.getServiceTax();
				String serviceCharge = requestBlock.getServiceCharge();
				String fare = requestBlock.getFares();
				String servicetax1[] = servicetax.split("~");
				String serviceCharge1[] = serviceCharge.split("~");
				String fare1[] = fare.split("~");
				double f1 = Double.parseDouble(fare1[0]);
				double f2 = Double.parseDouble(fare1[1]);
				double f3 = Double.parseDouble(fare1[2]);
				double f4 = Double.parseDouble(fare1[3]);
				double f5 = Double.parseDouble(fare1[4]);
				double f6 = Double.parseDouble(fare1[5]);
				double st1 = Double.parseDouble(servicetax1[0]);
				double st2 = Double.parseDouble(servicetax1[1]);
				double st3 = Double.parseDouble(servicetax1[2]);
				double st4 = Double.parseDouble(servicetax1[3]);
				double st5 = Double.parseDouble(servicetax1[4]);
				double st6 = Double.parseDouble(servicetax1[5]);
				double sc1 = Double.parseDouble(serviceCharge1[0]);
				double sc2 = Double.parseDouble(serviceCharge1[1]);
				double sc3 = Double.parseDouble(serviceCharge1[2]);
				double sc4 = Double.parseDouble(serviceCharge1[3]);
				double sc5 = Double.parseDouble(serviceCharge1[4]);
				double sc6 = Double.parseDouble(serviceCharge1[5]);
				double total = f1 + f2 + f3 + f4 + f5 + f6 + st1 + st2 + st3 + st4 + st5 + st6 + sc1 + sc2 + sc3 + sc4
						+ sc5 + sc6;
				requestBlock.setTotalamount("" + total);
			}
			long transctionrefno = System.currentTimeMillis();
			requestBlock.setTransctionrefno("" + transctionrefno);
			MultipleBlockSeatResponse resp = thirdPartyTravelBusApi.multipleSeat(requestBlock);
			{
				String appapical = thirdPartyTravelBusApi.bookbusticketcallapi(requestBlock, "" + resp.getCode());
				JSONObject obj = new JSONObject(appapical);
				String msg = obj.getString("code");
				if (msg.equalsIgnoreCase("S00")) {
					String resp1 = thirdPartyTravelBusApi.bookBusTicketinweb(resp.getCode());
					String c = thirdPartyTravelBusApi.getbusticketbook(resp.getCode());
					JSONObject ticket = new JSONObject(c);
					String a = ticket.getString("BookingDate");
					String a3 = ticket.getString("BookingRefNo");
					String bookid = ticket.getString("BlockId");
					String a18 = ticket.getString("ClientID");
					String databussuucess = thirdPartyTravelBusApi.busbookingsucces(a3, bookid, a18, a,
							requestBlock.getTransctionrefno(), requestBlock.getSessionid());
					return new ResponseEntity<String>(resp1, HttpStatus.OK);
				} else {
					return new ResponseEntity<String>(appapical, HttpStatus.OK);
				}
			}
		} else {
			return new ResponseEntity<String>(data, HttpStatus.OK);
		}

	}

}

/*
 * @RequestMapping(method = RequestMethod.GET, value = "/Sources", produces = {
 * MediaType.APPLICATION_JSON_VALUE }) ResponseEntity<String>
 * sources(@PathVariable(value = "role") String role,
 * 
 * @PathVariable(value = "device") String device, ModelMap modelMap,
 * HttpServletRequest request, HttpServletResponse
 * response, @RequestHeader(value = "ConsumerKey", required = true) String
 * consumerKey,
 * 
 * @RequestHeader(value = "ConsumerSecret", required = true) String
 * consumerSecret) { SourcesRequest sourcesRequest = new SourcesRequest();
 * sourcesRequest.setConsumerKey(consumerKey);
 * sourcesRequest.setConsumerSecret(consumerSecret); SourcesResponse
 * sourcesResponse = thirdPartyTravelBusApi.sources(sourcesRequest); return new
 * ResponseEntity<String>(sourcesResponse.getMsg(), HttpStatus.OK); }
 * 
 * @RequestMapping(method = RequestMethod.GET, value = "/AvailableBuses",
 * produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {
 * MediaType.APPLICATION_JSON_VALUE }) ResponseEntity<String>
 * availableBuses(@PathVariable(value = "role") String role,
 * 
 * @PathVariable(value = "device") String device, ModelMap modelMap,
 * HttpServletRequest request, HttpServletResponse
 * response, @RequestHeader(value = "ConsumerKey", required = true) String
 * consumerKey,
 * 
 * @RequestHeader(value = "ConsumerSecret", required = true) String
 * consumerSecret, ModelMap model,
 * 
 * @RequestBody BusesRequest req) { // BusesRequest req = new BusesRequest();
 * req.setConsumerKey(consumerKey); req.setConsumerSecret(consumerSecret);
 * 
 * BusesResponse resp = thirdPartyTravelBusApi.getBuses(req); return new
 * ResponseEntity<String>(resp.getMsg(), HttpStatus.OK); }
 * 
 * @RequestMapping(method = RequestMethod.GET, value = "/TripDetails", produces
 * = { MediaType.APPLICATION_JSON_VALUE }, consumes = {
 * MediaType.APPLICATION_JSON_VALUE }) ResponseEntity<String>
 * tripDetails(@PathVariable(value = "role") String role,
 * 
 * @PathVariable(value = "device") String device, ModelMap modelMap,
 * HttpServletRequest request, HttpServletResponse
 * response, @RequestHeader(value = "ConsumerKey", required = true) String
 * consumerKey,
 * 
 * @RequestHeader(value = "ConsumerSecret", required = true) String
 * consumerSecret, ModelMap model, TripDetailsRequest req) { //
 * TripDetailsRequest req = new TripDetailsRequest();
 * req.setConsumerKey(consumerKey); req.setConsumerSecret(consumerSecret);
 * TripDetailsResponse resp = thirdPartyTravelBusApi.tripDetails(req); return
 * new ResponseEntity<String>(resp.getMsg(), HttpStatus.OK); }
 * 
 * @RequestMapping(method = RequestMethod.POST, value = "/BlockBusTicket",
 * produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {
 * MediaType.APPLICATION_JSON_VALUE }) ResponseEntity<String>
 * blockBusTicket(@PathVariable(value = "role") String role,
 * 
 * @PathVariable(value = "device") String device, ModelMap modelMap,
 * HttpServletRequest request, HttpServletResponse
 * response, @RequestHeader(value = "ConsumerKey", required = true) String
 * consumerKey,
 * 
 * @RequestHeader(value = "ConsumerSecret", required = true) String
 * consumerSecret, ModelMap model, BlockBusTicketRequest req) { //
 * BlockBusTicketRequest req = new BlockBusTicketRequest();
 * req.setConsumerKey(consumerKey); req.setConsumerSecret(consumerSecret);
 * BlockBusTicketResponse resp = thirdPartyTravelBusApi.blockBusTicket(req);
 * return new ResponseEntity<String>(resp.getMsg(), HttpStatus.OK); }
 * 
 * @RequestMapping(method = RequestMethod.GET, value = "/BookBusTicket",
 * produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {
 * MediaType.APPLICATION_JSON_VALUE }) ResponseEntity<String>
 * bookBusTicket(@PathVariable(value = "role") String role,
 * 
 * @PathVariable(value = "device") String device, ModelMap modelMap,
 * HttpServletRequest request, HttpServletResponse
 * response, @RequestHeader(value = "ConsumerKey", required = true) String
 * consumerKey,
 * 
 * @RequestHeader(value = "ConsumerSecret", required = true) String
 * consumerSecret, ModelMap model, BookBusTicketRequest req) { //
 * BookBusTicketRequest req = new BookBusTicketRequest();
 * req.setConsumerKey(consumerKey); req.setConsumerSecret(consumerSecret);
 * BookBusTicketResponse resp = thirdPartyTravelBusApi.bookBusTicket(req);
 * return new ResponseEntity<String>(resp.getMsg(), HttpStatus.OK); }
 * 
 * @RequestMapping(method = RequestMethod.GET, value = "/CancelBusTicket",
 * produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {
 * MediaType.APPLICATION_JSON_VALUE }) ResponseEntity<String>
 * cancelBusTicket(ModelMap modelMap, HttpServletRequest request,
 * HttpServletResponse response,
 * 
 * @RequestHeader(value = "ConsumerKey", required = true) String consumerKey,
 * 
 * @RequestHeader(value = "ConsumerSecret", required = true) String
 * consumerSecret, ModelMap model, CancelBusTicketRequest req) { //
 * CancelBusTicketRequest req = new CancelBusTicketRequest(); //
 * ICancelBusTicketRequest req = new CancelBusTicketRequest();
 * req.setConsumerKey(consumerKey); req.setConsumerSecret(consumerSecret);
 * CancelBusTicketResponse resp = thirdPartyTravelBusApi.cancelBusTicket(req);
 * return new ResponseEntity<String>(resp.getMsg(), HttpStatus.OK); }
 * 
 * @RequestMapping(method = RequestMethod.GET, value =
 * "/BusTicketBookingDetails", produces = { MediaType.APPLICATION_JSON_VALUE },
 * consumes = { MediaType.APPLICATION_JSON_VALUE }) ResponseEntity<String>
 * ticketBokingDetails(@PathVariable(value = "role") String role,
 * 
 * @PathVariable(value = "device") String device, ModelMap modelMap,
 * HttpServletRequest request, HttpServletResponse
 * response, @RequestHeader(value = "ConsumerKey", required = true) String
 * consumerKey,
 * 
 * @RequestHeader(value = "ConsumerSecret", required = true) String
 * consumerSecret, ModelMap model, BusTicketBookingDetailsRequest req) { //
 * BusTicketBookingDetailsRequest req = new // BusTicketBookingDetailsRequest();
 * // IBusTicketBookingDetailsRequest req = new //
 * BusTicketBookingDetailsRequest(); req.setConsumerKey(consumerKey);
 * req.setConsumerSecret(consumerSecret); BusTicketBookingDetailsResponse resp =
 * thirdPartyTravelBusApi.busTicketBookingDetails(req); return new
 * ResponseEntity<String>(resp.getMsg(), HttpStatus.OK); }
 */ ////////////////////////////////////////// SONU /////////////////////////
