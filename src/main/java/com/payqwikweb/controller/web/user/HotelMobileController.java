package com.payqwikweb.controller.web.user;

import java.text.SimpleDateFormat;

import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.payqwikweb.api.IAuthenticationApi;
import com.payqwikweb.app.api.IHotelApi;
import com.payqwikweb.app.api.ILoadMoneyApi;
import com.payqwikweb.app.api.IUserApi;
import com.payqwikweb.app.model.ResponseStatus;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.hotel.error.HotelRequestError;
import com.payqwikweb.app.model.hotel.request.HotelAvalabilityRequest;
import com.payqwikweb.app.model.hotel.request.HotelBookingRequest;
import com.payqwikweb.app.model.hotel.request.HotelInfoRequest;
import com.payqwikweb.app.model.hotel.request.HotellistRequest;
import com.payqwikweb.app.model.hotel.response.HotelResponseDTO;
import com.payqwikweb.app.model.hotel.validation.HotelRequestValidation;
import com.payqwikweb.util.Authorities;

@Controller
@RequestMapping("/User/Travel/Hotel")
public class HotelMobileController implements MessageSourceAware {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private MessageSource messageSource;
	private final IHotelApi hotelApi;
	private final IAuthenticationApi authenticationApi;
	private final HotelRequestValidation validation;
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	private final IUserApi userApi;
	private final ILoadMoneyApi loadMoneyApi;

	public HotelMobileController(IHotelApi hotelApi, IAuthenticationApi authenticationApi,
			HotelRequestValidation validation,IUserApi userApi,ILoadMoneyApi loadMoneyApi) {

		this.hotelApi=hotelApi;	
		this.authenticationApi = authenticationApi;
		this.validation = validation;
		this.userApi=userApi;
		this.loadMoneyApi=loadMoneyApi;
	}

	@Override
	public void setMessageSource(MessageSource arg0) {
		this.messageSource = messageSource;

	}

/*	@RequestMapping(method = RequestMethod.POST, value = "/GetAllCityList", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> getAllCityList(HttpSession session) {

		ResponseDTO resp=new ResponseDTO();
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
					try {
						resp=travelBusApi.getAllCityList(sessionId);
						//System.err.println("Working");
						return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
					} catch (Exception e) {
						System.out.println(e);
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
						return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
					}
				}
				else {
					resp.setCode("F00");
					resp.setMessage("Unauthorised access");
					resp.setStatus("FAILED");
					return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
				}
			}else {
				resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
				resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
				resp.setMessage("Session expired");
				return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
			}

		}else {
			resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
			resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
			resp.setMessage("Session expired");
			return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
		}
	}*/

	
	@RequestMapping(method = RequestMethod.POST, value = "/HotelSearchList", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<HotelResponseDTO> getHotelList(HttpSession session, @RequestBody HotellistRequest dto) {

		HotelResponseDTO resp=new HotelResponseDTO();
		String sessionId = dto.getSessionId();
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
					try {
						
						HotelRequestError error=validation.hotelSearchValidation(dto);
						
						if (error.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
							resp=hotelApi.getavailableHotels(dto);
						}
						else {
							resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
							resp.setStatus(ResponseStatus.BAD_REQUEST.getKey());
							resp.setMessage(error.getMessage());
						}
						
						return new ResponseEntity<HotelResponseDTO>(resp, HttpStatus.OK);

					} catch (Exception e) {
						System.out.println(e);
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
						return new ResponseEntity<HotelResponseDTO>(resp, HttpStatus.OK);
					}
				}
				else {
					resp.setCode("F00");
					resp.setMessage("Unauthorised access");
					resp.setStatus("FAILED");
					return new ResponseEntity<HotelResponseDTO>(resp, HttpStatus.OK);
				}
			}else {
				resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
				resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
				resp.setMessage("Session expired");
				return new ResponseEntity<HotelResponseDTO>(resp, HttpStatus.OK);
			}
		}else {
			resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
			resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
			resp.setMessage("Session expired");
			return new ResponseEntity<HotelResponseDTO>(resp, HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/HotelInfo", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<HotelResponseDTO> getHotelInfo(HttpSession session, @RequestBody HotelInfoRequest dto) {

		HotelResponseDTO resp=new HotelResponseDTO();
		String sessionId = dto.getSessionId();
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
					try {
						
						HotelRequestError error=validation.hotelInfoVal(dto);
						
						if (error.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
							resp=hotelApi.gethotelInfo(dto);
						}
						else {
							resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
							resp.setStatus(ResponseStatus.BAD_REQUEST.getKey());
							resp.setMessage(error.getMessage());
						}
						
						return new ResponseEntity<HotelResponseDTO>(resp, HttpStatus.OK);

					} catch (Exception e) {
						System.out.println(e);
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
						return new ResponseEntity<HotelResponseDTO>(resp, HttpStatus.OK);
					}
				}
				else {
					resp.setCode("F00");
					resp.setMessage("Unauthorised access");
					resp.setStatus("FAILED");
					return new ResponseEntity<HotelResponseDTO>(resp, HttpStatus.OK);
				}
			}else {
				resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
				resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
				resp.setMessage("Session expired");
				return new ResponseEntity<HotelResponseDTO>(resp, HttpStatus.OK);
			}
		}else {
			resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
			resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
			resp.setMessage("Session expired");
			return new ResponseEntity<HotelResponseDTO>(resp, HttpStatus.OK);
		}
	}
	
	

	@RequestMapping(method = RequestMethod.POST, value = "/HotelAvailability", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<HotelResponseDTO> getHotelAvailablity(HttpSession session, @RequestBody HotelAvalabilityRequest dto) {

		HotelResponseDTO resp=new HotelResponseDTO();
		String sessionId = dto.getSessionId();
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
					try {
						
						HotelRequestError error=validation.hotelAVailableVal(dto);
						
						if (error.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
							resp=hotelApi.gethotelAvailableity(dto);
						}
						else {
							resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
							resp.setStatus(ResponseStatus.BAD_REQUEST.getKey());
							resp.setMessage(error.getMessage());
						}
						System.out.println("valusss"+resp.getDetails());
						return new ResponseEntity<HotelResponseDTO>(resp, HttpStatus.OK);

					} catch (Exception e) {
						System.out.println(e);
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
						return new ResponseEntity<HotelResponseDTO>(resp, HttpStatus.OK);
					}
				}
				else {
					resp.setCode("F00");
					resp.setMessage("Unauthorised access");
					resp.setStatus("FAILED");
					return new ResponseEntity<HotelResponseDTO>(resp, HttpStatus.OK);
				}
			}else {
				resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
				resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
				resp.setMessage("Session expired");
				return new ResponseEntity<HotelResponseDTO>(resp, HttpStatus.OK);
			}
		}else {
			resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
			resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
			resp.setMessage("Session expired");
			return new ResponseEntity<HotelResponseDTO>(resp, HttpStatus.OK);
		}
	}
	
	
	@RequestMapping(method = RequestMethod.POST, value = "/HotelBook", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<HotelResponseDTO> getHotelBook(HttpSession session, @RequestBody HotelBookingRequest dto) {

		HotelResponseDTO resp=new HotelResponseDTO();
		String sessionId = dto.getSessionId();
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
					try {
						
						HotelRequestError error=validation.hotelBookVal(dto);
						
						if (error.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
							resp=hotelApi.gethotelBook(dto);
						}
						else {
							resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
							resp.setStatus(ResponseStatus.BAD_REQUEST.getKey());
							resp.setMessage(error.getMessage());
						}
						System.out.println("valusss"+resp.getDetails());
						return new ResponseEntity<HotelResponseDTO>(resp, HttpStatus.OK);

					} catch (Exception e) {
						System.out.println(e);
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
						return new ResponseEntity<HotelResponseDTO>(resp, HttpStatus.OK);
					}
				}
				else {
					resp.setCode("F00");
					resp.setMessage("Unauthorised access");
					resp.setStatus("FAILED");
					return new ResponseEntity<HotelResponseDTO>(resp, HttpStatus.OK);
				}
			}else {
				resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
				resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
				resp.setMessage("Session expired");
				return new ResponseEntity<HotelResponseDTO>(resp, HttpStatus.OK);
			}
		}else {
			resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
			resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
			resp.setMessage("Session expired");
			return new ResponseEntity<HotelResponseDTO>(resp, HttpStatus.OK);
		}
	}
}
