package com.payqwikweb.controller.web.user;

import javax.servlet.http.HttpSession;

import com.instantpay.api.IValidationApi;
import com.instantpay.model.request.ValidationRequest;
import com.instantpay.util.IPayConvertUtil;
import com.payqwikweb.app.api.ITransactionApi;
import com.payqwikweb.app.model.request.*;
import com.payqwikweb.app.model.response.*;
import com.payqwikweb.model.web.CarrotFryDTO;
import com.payqwikweb.util.APIUtils;
import com.payqwikweb.util.ConvertUtil;
import com.thirdparty.model.ResponseDTO;
import com.thirdparty.model.ValidationResponseDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.payqwikweb.api.IAuthenticationApi;
import com.payqwikweb.app.api.IAgentBillPaymentApi;
import com.payqwikweb.app.api.IBillPaymentApi;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.model.error.DTHBillPaymentError;
import com.payqwikweb.model.error.ElectricityBillPaymentError;
import com.payqwikweb.model.error.GasBillPaymentError;
import com.payqwikweb.model.error.InsuranceBillPaymentError;
import com.payqwikweb.model.error.LandlineBillPaymentError;
import com.payqwikweb.util.Authorities;
import com.payqwikweb.validation.DTHBillPaymentValidation;
import com.payqwikweb.validation.ElectricityBillPaymentValidation;
import com.payqwikweb.validation.GasBillPaymentValidation;
import com.payqwikweb.validation.InsuranceBillPaymentValidation;
import com.payqwikweb.validation.LandlineBillPaymentValidation;


@Controller
@RequestMapping("/Agent/BillPayment")
public class AgentBillPaymentController implements MessageSourceAware {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private MessageSource messageSource;

	private  IAgentBillPaymentApi billAgentPaymentApi;
	private  IAuthenticationApi authenticationApi;
	private DTHBillPaymentValidation dTHBillPaymentValidation;
	private LandlineBillPaymentValidation landlineBillPaymentValidation;
	private ElectricityBillPaymentValidation electricityBillPaymentValidation;
	private GasBillPaymentValidation gasBillPaymentValidation;
	private InsuranceBillPaymentValidation insuranceBillPaymentValidation;
	private  IValidationApi validationApi;
	private  ITransactionApi transactionApi;
	public AgentBillPaymentController(IAgentBillPaymentApi billAgentPaymentApi, IAuthenticationApi authenticationApi,
			DTHBillPaymentValidation dTHBillPaymentValidation,
			LandlineBillPaymentValidation landlineBillPaymentValidation,
			ElectricityBillPaymentValidation electricityBillPaymentValidation,
			GasBillPaymentValidation gasBillPaymentValidation,
			InsuranceBillPaymentValidation insuranceBillPaymentValidation,IValidationApi validationApi,ITransactionApi transactionApi) {
		this.billAgentPaymentApi = billAgentPaymentApi;
		this.authenticationApi = authenticationApi;
		this.dTHBillPaymentValidation = dTHBillPaymentValidation;
		this.landlineBillPaymentValidation = landlineBillPaymentValidation;
		this.electricityBillPaymentValidation = electricityBillPaymentValidation;
		this.gasBillPaymentValidation = gasBillPaymentValidation;
		this.insuranceBillPaymentValidation = insuranceBillPaymentValidation;
		this.validationApi = validationApi;
		this.transactionApi = transactionApi;
	}

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	@RequestMapping(value = "/AgentProcessDTH", method = RequestMethod.POST)
	ResponseEntity<BillPaymentCommonResponseDTO> dthBill(@ModelAttribute("paydth") BillPaymentCommonDTO dto, HttpSession session) {
		BillPaymentCommonResponseDTO result = new BillPaymentCommonResponseDTO();
		String sessionId = (String) session.getAttribute("sessionId");
		DTHBillPaymentError error = dTHBillPaymentValidation.checkError(dto);
		if (error.isValid()) {
			if (sessionId != null && sessionId.length() != 0) {
				String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
				if (authority != null) {
					if (authority.contains(Authorities.AGENT) && authority.contains(Authorities.AUTHENTICATED)) {
						dto.setSessionId((String) session.getAttribute("sessionId"));
						ValidationRequest validationRequest = IPayConvertUtil.convertDTHBillPaymentRequest(dto);
						ValidationResponseDTO responseDTO = validationApi.validateTransaction(validationRequest);
						if(responseDTO.isSuccess()) {
							TransactionRequest transactionRequest = new TransactionRequest();
							transactionRequest.setSessionId(dto.getSessionId());
							transactionRequest.setAmount(Double.parseDouble(dto.getAmount()));
							transactionRequest.setTransactionRefNo("D");
							ResponseDTO responseDTO1 = transactionApi.validateAgentTransaction(transactionRequest);
							if(responseDTO1.isValid()) {
								result = billAgentPaymentApi.dthBill(dto,Role.AGENT);
								CarrotFryDTO carrotFryDTO = new CarrotFryDTO();
								carrotFryDTO.setAmount(dto.getAmount());
								carrotFryDTO.setCity(" ");
								carrotFryDTO.setEmail((String) session.getAttribute("emailId"));
								carrotFryDTO.setMobileNo((String) session.getAttribute("username"));
//								couponApi.sendCarrotFryCoupons(carrotFryDTO);
							}else {
								result.setCode("F00");
								result.setMessage(responseDTO1.getMessage());
								result.setResponse(APIUtils.getCustomJSON("F00",responseDTO1.getMessage()).toString());
							}
						}else {
							result.setCode("F00");
							result.setMessage(responseDTO.getMessage());
							result.setResponse(APIUtils.getCustomJSON("F00",responseDTO.getMessage()).toString());
						}
						return new ResponseEntity<BillPaymentCommonResponseDTO>(result, HttpStatus.OK);
					}
				}
			}
		} else {
			result.setCode("F00");
			result.setMessage("Bad Request");
		}
		return new ResponseEntity<BillPaymentCommonResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/AgentProcessLandline", method = RequestMethod.POST)
	ResponseEntity<BillPaymentCommonResponseDTO> landline(@ModelAttribute("paylandline") BillPaymentCommonDTO dto, HttpSession session) {
		BillPaymentCommonResponseDTO result = new BillPaymentCommonResponseDTO();
		String sessionId = (String) session.getAttribute("sessionId");
		LandlineBillPaymentError error = landlineBillPaymentValidation.checkError(dto);
		if (error.isValid()) {
			if (sessionId != null && sessionId.length() != 0) {
				String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
				if (authority != null) {
					if (authority.contains(Authorities.AGENT) && authority.contains(Authorities.AUTHENTICATED)) {
						dto.setSessionId(sessionId);
						ValidationRequest validationRequest = IPayConvertUtil.convertLandlineBillPaymentRequest(dto);
						ValidationResponseDTO responseDTO = validationApi.validateTransaction(validationRequest);
						if(responseDTO.isSuccess()) {
							TransactionRequest transactionRequest = new TransactionRequest();
							transactionRequest.setSessionId(dto.getSessionId());
							transactionRequest.setAmount(Double.parseDouble(dto.getAmount()));
							transactionRequest.setTransactionRefNo("D");
							ResponseDTO responseDTO1 = transactionApi.validateAgentTransaction(transactionRequest);
							if(responseDTO1.isValid()) {
								result = billAgentPaymentApi.landline(dto,Role.AGENT);
//								CarrotFryDTO carrotFryDTO = new CarrotFryDTO();
//								carrotFryDTO.setAmount(dto.getAmount());
//								carrotFryDTO.setCity(" ");
//								carrotFryDTO.setEmail((String) session.getAttribute("emailId"));
//								carrotFryDTO.setMobileNo((String) session.getAttribute("username"));
//								couponApi.sendCarrotFryCoupons(carrotFryDTO);
							}else {
								result.setCode("F00");
								result.setMessage(responseDTO1.getMessage());
								result.setResponse(APIUtils.getCustomJSON("F00",responseDTO1.getMessage()).toString());
							}
						}else {
							result.setCode("F00");
							result.setMessage(responseDTO.getMessage());
							result.setResponse(APIUtils.getCustomJSON("F00",responseDTO.getMessage()).toString());
						}
						return new ResponseEntity<BillPaymentCommonResponseDTO>(result, HttpStatus.OK);
					}
				}
			}
		} else {
			result.setCode("F00");
			result.setMessage("Bad Request");
		}
		return new ResponseEntity<BillPaymentCommonResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/AgentProcessElectricity", method = RequestMethod.POST)
	ResponseEntity<BillPaymentCommonResponseDTO> electricBill(
			@ModelAttribute("payelectricity") BillPaymentCommonDTO electricity, HttpSession session) {
		BillPaymentCommonResponseDTO result = new BillPaymentCommonResponseDTO();
		String sessionId = (String) session.getAttribute("sessionId");
		ElectricityBillPaymentError error = electricityBillPaymentValidation.checkError(electricity);
		if (true) {
			if (sessionId != null && sessionId.length() != 0) {
				String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
				if (authority != null) {
					if (authority.contains(Authorities.AGENT) && authority.contains(Authorities.AUTHENTICATED)) {
						electricity.setSessionId(sessionId);
						ValidationRequest validationRequest = IPayConvertUtil.convertElectricityBillPaymentRequest(electricity);
						ValidationResponseDTO responseDTO = validationApi.validateTransaction(validationRequest);
						if(responseDTO.isSuccess()) {
							TransactionRequest transactionRequest = new TransactionRequest();
							transactionRequest.setSessionId(electricity.getSessionId());
							transactionRequest.setAmount(Double.parseDouble(electricity.getAmount()));
							transactionRequest.setTransactionRefNo("D");
							ResponseDTO responseDTO1 = transactionApi.validateAgentTransaction(transactionRequest);
							if(responseDTO1.isValid()) {
								result = billAgentPaymentApi.electricBill(electricity,Role.AGENT);
//								CarrotFryDTO carrotFryDTO = new CarrotFryDTO();
//								carrotFryDTO.setAmount(electricity.getAmount());
//								carrotFryDTO.setCity(" ");
//								carrotFryDTO.setEmail((String) session.getAttribute("emailId"));
//								carrotFryDTO.setMobileNo((String) session.getAttribute("username"));
//								couponApi.sendCarrotFryCoupons(carrotFryDTO);
							}else {
								result.setCode("F00");
								result.setMessage(responseDTO1.getMessage());
								result.setResponse(APIUtils.getCustomJSON("F00",responseDTO1.getMessage()).toString());
							}
						}else {
							result.setCode("F00");
							result.setMessage(responseDTO.getMessage());
							result.setResponse(APIUtils.getCustomJSON("F00",responseDTO.getMessage()).toString());
						}
						return new ResponseEntity<BillPaymentCommonResponseDTO>(result, HttpStatus.OK);
					}
				}
			}
		} else {
			result.setCode("F00");
			result.setMessage("Bad Request");
			
		}
		return new ResponseEntity<BillPaymentCommonResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/AgentProcessGas", method = RequestMethod.POST)
	ResponseEntity<BillPaymentCommonResponseDTO> gasBill(@ModelAttribute("paygas") BillPaymentCommonDTO gas,HttpSession session) {

		BillPaymentCommonResponseDTO result = new BillPaymentCommonResponseDTO();
		String sessionId = (String) session.getAttribute("sessionId");
		GasBillPaymentError error = gasBillPaymentValidation.checkError(gas);
		if (error.isValid()) {
			if (sessionId != null && sessionId.length() != 0) {
				String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
				if (authority != null) {
					if (authority.contains(Authorities.AGENT) && authority.contains(Authorities.AUTHENTICATED)) {
						gas.setSessionId(sessionId);
						ValidationRequest validationRequest = IPayConvertUtil.convertGasBillPaymentRequest(gas);
						ValidationResponseDTO responseDTO = validationApi.validateTransaction(validationRequest);
						if(responseDTO.isSuccess()) {
							TransactionRequest transactionRequest = new TransactionRequest();
							transactionRequest.setSessionId(gas.getSessionId());
							transactionRequest.setAmount(Double.parseDouble(gas.getAmount()));
							transactionRequest.setTransactionRefNo("D");
							ResponseDTO responseDTO1 = transactionApi.validateAgentTransaction(transactionRequest);
							if(responseDTO1.isValid()) {
								result = billAgentPaymentApi.gasBill(gas,Role.AGENT);
//								CarrotFryDTO carrotFryDTO = new CarrotFryDTO();
//								carrotFryDTO.setAmount(gas.getAmount());
//								carrotFryDTO.setCity(" ");
//								carrotFryDTO.setEmail((String) session.getAttribute("emailId"));
//								carrotFryDTO.setMobileNo((String) session.getAttribute("username"));
//								couponApi.sendCarrotFryCoupons(carrotFryDTO);
							}else {
								result.setCode("F00");
								result.setMessage(responseDTO1.getMessage());
								result.setResponse(APIUtils.getCustomJSON("F00",responseDTO1.getMessage()).toString());
							}
						}else {
							result.setCode("F00");
							result.setMessage(responseDTO.getMessage());
							result.setResponse(APIUtils.getCustomJSON("F00",responseDTO.getMessage()).toString());
						}
						return new ResponseEntity<BillPaymentCommonResponseDTO>(result, HttpStatus.OK);
					}
				}
			}
		} else {
			result.setCode("F00");
			result.setMessage("Bad Request");
		}
		return new ResponseEntity<BillPaymentCommonResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/AgentProcessInsurance", method = RequestMethod.POST)
	ResponseEntity<BillPaymentCommonResponseDTO> insuranceBill(
			@ModelAttribute("payinsurance") BillPaymentCommonDTO insurance, HttpSession session) {
		String sessionId = (String) session.getAttribute("sessionId");
		BillPaymentCommonResponseDTO result = new BillPaymentCommonResponseDTO();
		InsuranceBillPaymentError error = insuranceBillPaymentValidation.checkError(insurance);
		if (error.isValid()) {
			if (sessionId != null && sessionId.length() != 0) {
				String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
				if (authority != null) {
					if (authority.contains(Authorities.AGENT) && authority.contains(Authorities.AUTHENTICATED)) {
						insurance.setSessionId(sessionId);
						ValidationRequest validationRequest = IPayConvertUtil.convertInsuranceBillPaymentRequest(insurance);
						ValidationResponseDTO responseDTO = validationApi.validateTransaction(validationRequest);
						if(responseDTO.isSuccess()){
							TransactionRequest transactionRequest = new TransactionRequest();
							transactionRequest.setSessionId(insurance.getSessionId());
							transactionRequest.setAmount(Double.parseDouble(insurance.getAmount()));
							transactionRequest.setTransactionRefNo("D");
							ResponseDTO responseDTO1 = transactionApi.validateAgentTransaction(transactionRequest);
							if(responseDTO1.isValid()) {
								result = billAgentPaymentApi.insuranceBill(insurance,Role.AGENT);
//								CarrotFryDTO carrotFryDTO = new CarrotFryDTO();
//								carrotFryDTO.setAmount(insurance.getAmount());
//								carrotFryDTO.setCity(" ");
//								carrotFryDTO.setEmail((String) session.getAttribute("emailId"));
//								carrotFryDTO.setMobileNo((String) session.getAttribute("username"));
//								couponApi.sendCarrotFryCoupons(carrotFryDTO);
							}else {
								result.setCode("F00");
								result.setMessage(responseDTO1.getMessage());
								result.setResponse(APIUtils.getCustomJSON("F00",responseDTO1.getMessage()).toString());
							}
					}else {
						result.setCode("F00");
						result.setMessage(responseDTO.getMessage());
							result.setResponse(APIUtils.getCustomJSON("F00",responseDTO.getMessage()).toString());
						}
						return new ResponseEntity<BillPaymentCommonResponseDTO>(result, HttpStatus.OK);
					}
				}
			}
		} else {
			result.setCode("F00");
			result.setMessage("Bad Request");
		}
		return new ResponseEntity<BillPaymentCommonResponseDTO>(result, HttpStatus.OK);
	}
	
}

