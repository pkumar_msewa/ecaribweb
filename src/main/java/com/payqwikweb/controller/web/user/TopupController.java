package com.payqwikweb.controller.web.user;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.instantpay.api.IValidationApi;
import com.instantpay.model.request.ValidationRequest;
import com.instantpay.util.IPayConvertUtil;
import com.payqwikweb.api.IAuthenticationApi;
import com.payqwikweb.app.api.ITopupApi;
import com.payqwikweb.app.api.ITransactionApi;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.request.RechargeRequestDTO;
import com.payqwikweb.app.model.request.TransactionRequest;
import com.payqwikweb.app.model.response.RechargeResponseDTO;
import com.payqwikweb.model.error.TopupError;
import com.payqwikweb.util.APIUtils;
import com.payqwikweb.util.Authorities;
import com.payqwikweb.util.ConvertUtil;
import com.payqwikweb.validation.TopupValidation;
import com.thirdparty.model.ResponseDTO;

@Controller
@RequestMapping("/User/MobileTopup")
public class TopupController implements MessageSourceAware {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private MessageSource messageSource;

	private final ITopupApi appTopupApi;
	private final IAuthenticationApi authenticationApi;
	private final TopupValidation topupValidation;
	private final ITransactionApi transactionApi;
	private final IValidationApi validationApi;
	public TopupController(ITopupApi appTopupApi, IAuthenticationApi authenticationApi, TopupValidation topupValidation,
			ITransactionApi transactionApi,IValidationApi validationApi) {
		this.appTopupApi = appTopupApi;
		this.authenticationApi = authenticationApi;
		this.topupValidation = topupValidation;
		this.transactionApi = transactionApi;
		this.validationApi = validationApi;
	}

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	@RequestMapping(value = "/ProcessPrepaid", method = RequestMethod.POST)
	ResponseEntity<RechargeResponseDTO> prePaid(@ModelAttribute("topup") RechargeRequestDTO dto, HttpSession session) {
		System.err.println("inside top up controller");
		RechargeResponseDTO result = new RechargeResponseDTO();
		TopupError error = topupValidation.checkError(dto);
		if (error.isValid()) {
			String sessionId = (String) session.getAttribute("sessionId");
			if (sessionId != null && sessionId.length() != 0) {
				String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
				if (authority != null) {
					if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
								dto.setSessionId(sessionId);
								result = appTopupApi.prePaid(dto);
						return new ResponseEntity<RechargeResponseDTO>(result, HttpStatus.OK);
					}
					else{
						result.setCode("F00");
						result.setMessage("Unauthorized User.");
						result.setResponse(APIUtils.getCustomJSON("F00",result.getMessage()).toString());
					}
				}else{
					result.setCode("F03");
					result.setMessage("Session Invalid. Please, login and try again");
					result.setResponse(APIUtils.getCustomJSON("F00",result.getMessage()).toString());
				}
			}
		} 

		return new ResponseEntity<RechargeResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/ProcessPostpaid", method = RequestMethod.POST)
	ResponseEntity<RechargeResponseDTO> postPaid(@ModelAttribute("topup") RechargeRequestDTO dto, HttpSession session) {
		String sessionId = (String) session.getAttribute("sessionId");
		RechargeResponseDTO result = new RechargeResponseDTO();
		TopupError error = topupValidation.checkError(dto);
		if (error.isValid()) {
			if (sessionId != null && sessionId.length() != 0) {
				String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
				if (authority != null) {
					if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
						dto.setSessionId(sessionId);
						result = appTopupApi.prePaid(dto);
				return new ResponseEntity<RechargeResponseDTO>(result, HttpStatus.OK);
			}
			else{
				result.setCode("F00");
				result.setMessage("Unauthorized User.");
				result.setResponse(APIUtils.getCustomJSON("F00",result.getMessage()).toString());
			}				
				}
			}
		} else {
			result.setCode("F00");
			result.setMessage("Invalid Input");
		}
		return new ResponseEntity<RechargeResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/ProcessDataCard", method = RequestMethod.POST)
	ResponseEntity<RechargeResponseDTO> dataCard(@ModelAttribute("topup") RechargeRequestDTO dto, HttpSession session) {
		String sessionId = (String) session.getAttribute("sessionId");
		RechargeResponseDTO result = new RechargeResponseDTO();
		TopupError error = topupValidation.checkError(dto);
		if (error.isValid()) {
			if (sessionId != null && sessionId.length() != 0) {
				String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
				if (authority != null) {
					if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
						dto.setSessionId(sessionId);
						result = appTopupApi.prePaid(dto);
				return new ResponseEntity<RechargeResponseDTO>(result, HttpStatus.OK);
			}
			else{
				result.setCode("F00");
				result.setMessage("Unauthorized User.");
				result.setResponse(APIUtils.getCustomJSON("F00",result.getMessage()).toString());
			}				
				}
			}
		} else {
			result.setCode("F00");
			result.setMessage("Invalid Input");
		}
		return new ResponseEntity<RechargeResponseDTO>(result, HttpStatus.OK);
	} 
	
	
	@RequestMapping(value = "/checkForSplitPay", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<RechargeResponseDTO> transactionCheckForSplitPayment(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody RechargeRequestDTO dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response) {
		RechargeResponseDTO result = new RechargeResponseDTO();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.WEBSITE.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				
					TransactionRequest newRequest = new TransactionRequest();
					newRequest.setSessionId(dto.getSessionId());
					newRequest.setAmount(Double.parseDouble(dto.getAmount()));
					newRequest.setTransactionRefNo("D");
					ResponseDTO responseDTO1 = transactionApi.validateTransaction(newRequest);
					if(responseDTO1.getCode().equalsIgnoreCase("T01")) {
						result.setCode(responseDTO1.getCode());
						result.setMessage(responseDTO1.getMessage());
						dto.setAmount(Double.toString(responseDTO1.getAmount()));
						result.setDto(dto);
					}else {
						result.setCode(responseDTO1.getCode());
						result.setMessage(responseDTO1.getMessage());
						result.setDto(dto);
					}
				}else{
					result.setCode("F00");
					result.setMessage("not a valid device");
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unknown device");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		
		return new ResponseEntity<RechargeResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/ProcessPrepaidRecharge", method = RequestMethod.POST)
	ResponseEntity<RechargeResponseDTO> prePaidRecharge(@ModelAttribute("topup") RechargeRequestDTO dto, HttpSession session) {
		String sessionId = (String) session.getAttribute("sessionId");
		RechargeResponseDTO result = new RechargeResponseDTO();
		TopupError error = topupValidation.checkError(dto);
		if (error.isValid()) {
			if (sessionId != null && sessionId.length() != 0) {
				String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
				if (authority != null) {
					if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
						dto.setSessionId(sessionId);
						result = appTopupApi.prePaid(dto);
				return new ResponseEntity<RechargeResponseDTO>(result, HttpStatus.OK);
			}
			else{
				result.setCode("F00");
				result.setMessage("Unauthorized User.");
				result.setResponse(APIUtils.getCustomJSON("F00",result.getMessage()).toString());
			}				
				}
			}
		} else {
			result.setCode("F00");
			result.setMessage("Invalid Input");
		}
		return new ResponseEntity<RechargeResponseDTO>(result, HttpStatus.OK);
	}

}
