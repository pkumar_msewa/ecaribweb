package com.payqwikweb.controller.web.user;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.ParserConfigurationException;

import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.xml.sax.SAXException;

import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.ResponseStatus;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.response.EBSRedirectResponse;
import com.payqwikweb.model.web.Status;
import com.payqwikweb.util.ConvertUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import com.thirdparty.model.BescomS2SRequest;
import com.thirdparty.model.CheckUpiStatus;
import com.thirdparty.model.StatusResponse;
import com.upi.api.IUPIApi;
import com.upi.model.requet.MerchantCheckTxnStatus;
import com.upi.model.requet.MerchantCollectMoneyRequest;
import com.upi.model.requet.MerchantTokenValidRequest;
import com.upi.model.requet.VPARequest;
import com.upi.model.response.MerchantMetaVPAResponse;
import com.upi.model.response.MerchantTokenGenResponse;
import com.upi.model.response.MerchantTokenValidResponse;
import com.upi.model.response.MerchantTxnStatusResponse;
import com.upi.util.DesEncryption;

import javassist.runtime.DotClass;

@Controller
@RequestMapping("/User/UPI")
public class UPIController {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	private MessageSource messageSource;

	private final IUPIApi upiApi;

	public UPIController(IUPIApi upiApi) {
		this.upiApi = upiApi;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	@RequestMapping(value = "/ValidateToken", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<MerchantTokenValidResponse> validateToken(MerchantCollectMoneyRequest dto,
			HttpServletRequest request, HttpServletResponse response, HttpSession session,
			@RequestHeader(value = "sdk", required = false) boolean sdk,
			@RequestHeader(value = "deviceId", required = false) String deviceId) {
		MerchantTokenValidResponse validResponse = new MerchantTokenValidResponse();
		try {
			dto.setSdk(sdk);
			dto.setDeviceId(deviceId);
			MerchantTokenGenResponse result = upiApi.genMerchantToken(ConvertUtil.generateMerchantTokenRequest(dto));
			if (result.getCode().equals(ResponseStatus.SUCCESS.getValue())) {
				MerchantTokenValidRequest tokenValidRequest = ConvertUtil.tokenValidRequest(result.getToken(),
						dto.isSdk(), dto.getDeviceId());
				validResponse = upiApi
						.validateMerchantMetaDataInfo(ConvertUtil.merchantMetaInfoRequest(tokenValidRequest.getMsgId(),
								DesEncryption.desEncrypt(tokenValidRequest.getRequest(), tokenValidRequest.getMsgId()),
								dto.isSdk()));
				logger.info("Generated Token Was +++++++++ ++++++++++++ ++++++++++  " + result.getToken());
				validResponse.setToken(result.getToken());
			} else {
				validResponse.setStatus(ResponseStatus.FAILURE);
				validResponse.setMessage(result.getMessage());
			}
		} catch (Exception e) {
			e.printStackTrace();
			validResponse.setStatus(ResponseStatus.FAILURE);
			validResponse.setMessage("Internal Server Error");
		}
		return new ResponseEntity<MerchantTokenValidResponse>(validResponse, HttpStatus.OK);
	}

	@RequestMapping(value = "/CollectMoney", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<MerchantMetaVPAResponse> sendMoneyMobileRequest(MerchantCollectMoneyRequest dto,
			HttpServletRequest request, HttpServletResponse response, HttpSession session,
			@RequestHeader(value = "expTime", required = false) String expTime) {
		MerchantMetaVPAResponse metaVPAResponse = new MerchantMetaVPAResponse();
		logger.info("Generated Token Was +++++++++ ++++++++++++ ++++++++++  " + expTime);
		logger.info("trnasid  +++++++++ ++++++++++++ ++++++++++  " + dto.getRefId());

		try {
			dto.setExpTime(expTime);
			VPARequest vpaRequest = ConvertUtil.vpaRequest(dto);
			metaVPAResponse = upiApi.validateMerchantMetaVPA(ConvertUtil.validateMerchantVPARequest(
					vpaRequest.getMsgId(), DesEncryption.desEncrypt(vpaRequest.getRequest(), vpaRequest.getMsgId())));
			if (metaVPAResponse.getCode().equals(ResponseStatus.SUCCESS.getValue())) {
				MerchantCollectMoneyRequest cmRequest = ConvertUtil.collectMoneyRequest(dto);
				metaVPAResponse = upiApi.validateCollectMoney(ConvertUtil.validateMerchantCollectMoneyRequest(
						cmRequest.getMsgId(), DesEncryption.desEncrypt(cmRequest.getRequest(), cmRequest.getMsgId())));
			}
		} catch (Exception e) {
			e.printStackTrace();
			metaVPAResponse.setStatus(ResponseStatus.FAILURE);
			metaVPAResponse.setMessage("Internal Server Error");
		}
		return new ResponseEntity<MerchantMetaVPAResponse>(metaVPAResponse, HttpStatus.OK);
	}

	@RequestMapping(value = "/UpiLoadStatus", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<MerchantTxnStatusResponse> upiLoadStatus(MerchantCollectMoneyRequest dto) {
		MerchantTxnStatusResponse resp = new MerchantTxnStatusResponse();
		try {
			MerchantCheckTxnStatus checkTxnStatus = ConvertUtil.merchantStatusRequest(dto.getMsgId());
			resp = upiApi.maerchantTxnStatus(ConvertUtil.merchantTxnStatusRequest(checkTxnStatus.getMsgId(),
					DesEncryption.desEncrypt(checkTxnStatus.getRequest(), checkTxnStatus.getMsgId())));
			return new ResponseEntity<MerchantTxnStatusResponse>(resp, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode("F00");
			resp.setMessage("We are sorry for inconvenience, Please try again later .");
		}
		return new ResponseEntity<MerchantTxnStatusResponse>(resp, HttpStatus.OK);
	}

	// TODO BESCom UPI Controllers
	@RequestMapping(value = "/ValidateBesomToken", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<MerchantTokenValidResponse> validateBesomToken(MerchantCollectMoneyRequest dto,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) {
		MerchantTokenValidResponse validResponse = new MerchantTokenValidResponse();
		try {
			MerchantTokenGenResponse result = upiApi.genMerchantTokenBescom(ConvertUtil.generateBescomMerchantTokenRequest(dto));
			if (result.getCode().equals(ResponseStatus.SUCCESS.getValue())) {
				MerchantTokenValidRequest tokenValidRequest = ConvertUtil.tokenValidBescomRequest(result.getToken(),dto);
				validResponse = upiApi.validateMerchantMetaDataInfoBescom(ConvertUtil.merchantMetaInfoBescomRequest(
						tokenValidRequest.getMsgId(),
						DesEncryption.desEncrypt(tokenValidRequest.getBescomUpiRequest(), tokenValidRequest.getMsgId()),
						dto.getPublicKey()));
				validResponse.setToken(result.getToken());
			} else {
				validResponse.setStatus(ResponseStatus.FAILURE);
				validResponse.setMessage(result.getMessage());
			}
		} catch (Exception e) {
			e.printStackTrace();
			validResponse.setStatus(ResponseStatus.FAILURE);
			validResponse.setMessage("Internal Server Error");
		}
		return new ResponseEntity<MerchantTokenValidResponse>(validResponse, HttpStatus.OK);
	}

	@RequestMapping(value = "/BesomCollectMoney", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<MerchantMetaVPAResponse> collectMoneyBesom(MerchantCollectMoneyRequest dto,
			HttpServletRequest request, HttpServletResponse response, HttpSession session,
			@RequestHeader(value = "expTime", required = false) String expTime) {
		MerchantMetaVPAResponse metaVPAResponse = new MerchantMetaVPAResponse();
		try {
			dto.setExpTime(expTime);
			VPARequest vpaRequest = ConvertUtil.vpaBescomRequest(dto);
			metaVPAResponse = upiApi
					.validateMerchantMetaVPABescom(ConvertUtil.validateMerchantVPABescomRequest(vpaRequest.getMsgId(),
							DesEncryption.desEncrypt(vpaRequest.getBescomUpiRequest(), vpaRequest.getMsgId()), dto.getPublicKey()));
			if (metaVPAResponse.getCode().equals(ResponseStatus.SUCCESS.getValue())) {
				MerchantCollectMoneyRequest cmRequest = ConvertUtil.collectMoneyBescomRequest(dto);
				metaVPAResponse = upiApi.validateCollectMoneyBescom(
						ConvertUtil.validateMerchantCollectMoneyBescomRequest(cmRequest.getMsgId(),
								DesEncryption.desEncrypt(cmRequest.getBescomUpiRequest(), cmRequest.getMsgId()), dto.getPublicKey()));
			}
		} catch (Exception e) {
			e.printStackTrace();
			metaVPAResponse.setStatus(ResponseStatus.FAILURE);
			metaVPAResponse.setMessage("Internal Server Error");
		}
		return new ResponseEntity<MerchantMetaVPAResponse>(metaVPAResponse, HttpStatus.OK);
	}

	@RequestMapping(value = "/UpiStatus", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<MerchantTxnStatusResponse> upiBescomStatus(MerchantCollectMoneyRequest dto) {
		MerchantTxnStatusResponse resp = new MerchantTxnStatusResponse();
		try {
			logger.info("Merchant : Msg ID : : " + dto.getMsgId());
			MerchantCheckTxnStatus checkTxnStatus = ConvertUtil.merchantStatusBescomRequest(dto.getMsgId(), dto);
			resp = upiApi.maerchantTxnStatusBescom(ConvertUtil.merchantTxnStatusBescomRequest(checkTxnStatus.getMsgId(),
					DesEncryption.desEncrypt(checkTxnStatus.getBescomUpiRequest(), checkTxnStatus.getMsgId()), dto.getPublicKey()));
			return new ResponseEntity<MerchantTxnStatusResponse>(resp, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode("F00");
			resp.setMessage("We are sorry for inconvenience, Please try again later .");
		}
		return new ResponseEntity<MerchantTxnStatusResponse>(resp, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/UpiTestDemo", method = RequestMethod.GET)
	String testUPIdd(MerchantCollectMoneyRequest dto) {
		return "Redirect:/Vijaya/Login";
	}
}
