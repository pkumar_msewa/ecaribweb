package com.payqwikweb.controller.web.user;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.recaptcha.api.IVerificationApi;
import com.google.recaptcha.model.JCaptchaRequest;
import com.google.recaptcha.model.JCaptchaResponse;
import com.payqwikweb.app.api.ILoginApi;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.request.ChangePasswordWithOtpRequest;
import com.payqwikweb.app.model.request.ForgetPasswordUserRequest;
import com.payqwikweb.app.model.request.LoginRequest;
import com.payqwikweb.app.model.request.ResendForgotPasswordOtpRequest;
import com.payqwikweb.app.model.response.ChangePasswordWithOtpResponse;
import com.payqwikweb.app.model.response.ForgetPasswordUserResponse;
import com.payqwikweb.app.model.response.LoginResponse;
import com.payqwikweb.app.model.response.ResendForgotPasswordOtpResponse;
import com.payqwikweb.model.error.LoginError;
import com.payqwikweb.util.ModelMapKey;
import com.payqwikweb.validation.LoginValidation;
import com.payqwikweb.util.APIUtils;

@Controller
@RequestMapping("/")
public class LoginController implements MessageSourceAware {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private MessageSource messageSource;

	private final ILoginApi loginApi;
	private final LoginValidation loginValidation;
//	private final IVerificationApi verificationApi;

	public LoginController(ILoginApi loginApi, LoginValidation loginValidation) {
		this.loginApi = loginApi;
		this.loginValidation = loginValidation;
	//	this.verificationApi = verificationApi;
	}
	
	

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	
	/*public LoginController(ILoginApi loginApi, LoginValidation loginValidation) {
	this.loginApi = loginApi;
	this.loginValidation = loginValidation;
}*/


	@RequestMapping(method = RequestMethod.GET, value = "/Travel/BusTravel")
	public String gettrevelbus(ModelMap model, HttpSession session) {

		String sessionId = (String) session.getAttribute("sessionId");
		if(sessionId != null && sessionId.length() != 0)
		{
			String id = "";
			String city = "";
			try {
				String val = LoginController.reafile();
				// API CALL FROM DATABASE

				// jsonString is a string variable that holds the JSON
				ArrayList<String> arrayidval = new ArrayList<>();
				ArrayList<String> arraynameval = new ArrayList<>();

				JSONArray jsonarray = new JSONArray(val);
				for (int i = 0; i < jsonarray.length(); i++) {
					JSONObject jsonobject = jsonarray.getJSONObject(i);
					String idname = jsonobject.getString("Id");
					String cityname = jsonobject.getString("Name");
					id += "#" + idname;
					city += "@" + cityname;
				}

				System.out.println(id + "$" + city);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			model.addAttribute("cityandid", id + "$" + city);
			return "User/Travel/Bus/BusTravel";

		} else {
			return "redirect:/Home";
		}

	}

	@RequestMapping(method = RequestMethod.GET, value = "/Travel/Bus/Allbusshowdetail")
	public String getavailebelbustrevelbus(HttpSession session) {
		String sessionId = (String) session.getAttribute("sessionId");
		if(sessionId != null && sessionId.length() != 0)
		{
			return "User/Travel/Bus/Allbusshowdetail";
		} else {
			return "redirect:/Home";
		}
	}


	@RequestMapping(method = RequestMethod.POST, value = "/User/Login")
	public String processUserLogin(@ModelAttribute("login") LoginRequest loginDTO, HttpServletRequest request ,RedirectAttributes modelMap) {
		LoginError error = loginValidation.checkError(loginDTO);
		if (error.isValid()) {
			System.err.println("USER LOGIN ");
			loginDTO.setIpAddress(request.getRemoteAddr());
			LoginResponse resp = loginApi.login(loginDTO, Role.USER);
			if (resp.getCode().equals("F00")) {
				modelMap.addFlashAttribute(ModelMapKey.ERROR, "Invalid Username/Password");
			}
		}
		return "forward:/";

	}

	@RequestMapping(method = RequestMethod.POST, value = "/Admin/Login")
	public String processAdminLogin(@ModelAttribute("login") LoginRequest loginDTO, ModelMap modelMap) {
		LoginResponse resp = loginApi.login(loginDTO, Role.ADMIN);
		modelMap.addAttribute("message", resp.getMessage());
		return "redirect:/";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/ForgotPassword/Request")
	public String forgetPasswordUserRequest(@ModelAttribute("forgotPassword") ForgetPasswordUserRequest psw, ModelMap modelMap) {
		ForgetPasswordUserResponse resp = loginApi.forgetPasswordUserRequest(psw);
		modelMap.addAttribute("message", resp.getMessage());
		return "User/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/ForgotPassword/ChangePassword")
	public String changePasswordWithOtpRequest(
			@ModelAttribute("forgotPassword") ChangePasswordWithOtpRequest changePassword, ModelMap modelMap) {
		ChangePasswordWithOtpResponse resp = loginApi.changePasswordWithOtpRequest(changePassword);
		modelMap.addAttribute("message", resp.getMessage());
		return "User/Home";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/ForgotPassword/Resend")
	public String resendForgotPasswordOtpRequest(
			@ModelAttribute("forgotPassword") ResendForgotPasswordOtpRequest resendPassword,  ModelMap modelMap) {
		ResendForgotPasswordOtpResponse resp = loginApi.resendForgotPasswordOtpRequest(resendPassword);
		modelMap.addAttribute("message", resp.getMessage());
		return "User/Home";
	}

	@RequestMapping(value = "ForgotPassword", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ForgetPasswordUserResponse> forgotPassword(@RequestBody ForgetPasswordUserRequest dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request) {

		ForgetPasswordUserResponse result = new ForgetPasswordUserResponse();
		try{
		/*JCaptchaRequest cap = new JCaptchaRequest();
		cap.setSessionId(request.getSession().getId());
		cap.setCaptchaResponse(dto.getCaptchaResponse());
		JCaptchaResponse jCaptchaResponse = verificationApi.isValidJCaptcha(cap);*/
	//	if (jCaptchaResponse.isValid()) {
			result = loginApi.forgetPasswordUserRequest(dto);
		/*} else {
			result.setCode("F00");
			result.setMessage("Not a valid captcha");
		}*/
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("Service unavailable");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<ForgetPasswordUserResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "ChangePasswordWithOTP", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ChangePasswordWithOtpResponse> changePasswordWithOTP(
			@RequestHeader(value = "hash", required = false) String hash, @RequestBody ChangePasswordWithOtpRequest dto) {
		ChangePasswordWithOtpResponse result = new ChangePasswordWithOtpResponse();
		try {
		result = loginApi.changePasswordWithOtpRequest(dto);
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("Service unavailable");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}

		return new ResponseEntity<ChangePasswordWithOtpResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "ResendForgotPasswordOTP", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResendForgotPasswordOtpResponse> resendForgotPasswordOTP(
			@RequestBody ResendForgotPasswordOtpRequest dto, HttpServletRequest request) {
		ResendForgotPasswordOtpResponse result = new ResendForgotPasswordOtpResponse();
		JCaptchaRequest cap = new JCaptchaRequest();
		try {
		cap.setSessionId(request.getSession().getId());
		cap.setCaptchaResponse(dto.getCaptchaResponse());
//		JCaptchaResponse jCaptchaResponse = verificationApi.isValidJCaptcha(cap);
		if (true) {
			result = loginApi.resendForgotPasswordOtpRequest(dto);
		} else {
			result.setCode("F00");
			result.setMessage("Not a valid captcha");
		}
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("Service unavailable");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<ResendForgotPasswordOtpResponse>(result, HttpStatus.OK);
	}
	
	public static String reafile() {
		String data = "";
		try {
			// File f = new
			// File("/usr/local/apps/tomcat/webapps/ROOT/resources/BusSource.txt");
			File f = new File("/home/vibhanshu/Documents/PayQwikCentralSystem/PayQwikWeb/WebContent/resources/BusSource.txt");
			// if(f.exists())
			// {
			// System.out.println(f.getAbsolutePath());
			FileInputStream fin = new FileInputStream(f);
			byte[] buffer = new byte[(int) f.length()];

			new DataInputStream(fin).readFully(buffer);
			fin.close();
			data = new String(buffer, "UTF-8");
			//
			// }
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
		}

		return data;
	}


}
