package com.payqwikweb.controller.web.user;

import java.io.BufferedReader;
import java.text.SimpleDateFormat;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.payqwikweb.api.IAuthenticationApi;
import com.payqwikweb.app.api.ILoadMoneyApi;
import com.payqwikweb.app.metadatas.UrlMetadatas;
import com.payqwikweb.app.model.ResponseStatus;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.request.LoadMoneyRequest;
import com.payqwikweb.app.model.request.VNetRequest;
import com.payqwikweb.app.model.response.EBSRedirectResponse;
import com.payqwikweb.app.model.response.LoadMoneyResponse;
import com.payqwikweb.app.model.response.VNetResponse;
import com.payqwikweb.app.model.response.VRedirectResponse;
import com.payqwikweb.model.app.response.VNetStatusResponse;
import com.payqwikweb.model.error.LoadMoneyError;
import com.payqwikweb.model.web.Status;
import com.payqwikweb.model.web.WEBSRedirectResponse;
import com.payqwikweb.util.Authorities;
import com.payqwikweb.util.ConvertUtil;
import com.payqwikweb.util.ModelMapKey;
import com.payqwikweb.validation.LoadMoneyValidation;
import com.thirdparty.model.ResponseDTO;
import com.thirdparty.model.StatusResponse;
import com.thirdparty.model.UpiSdkResponseDTO;
import com.thirdparty.model.error.CommonError;
import com.upi.api.IUPIApi;
import com.upi.model.requet.MerchantApprovedResponse;
import com.upi.model.requet.UPIRedirect;
import com.upi.model.response.MerchantMetaVPAResponse;

@Controller
@RequestMapping("/User/LoadMoney")
public class LoadMoneyController implements MessageSourceAware {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

	private MessageSource messageSource;

	private final ILoadMoneyApi loadMoneyApi;
	private final IAuthenticationApi authenticationApi;
	private final LoadMoneyValidation loadMoneyValidation;
	private final IUPIApi upiApi;

	public LoadMoneyController(ILoadMoneyApi loadMoneyApi, IAuthenticationApi authenticationApi,
			LoadMoneyValidation loadMoneyValidation, IUPIApi upiApi) {
		this.loadMoneyApi = loadMoneyApi;
		this.authenticationApi = authenticationApi;
		this.loadMoneyValidation = loadMoneyValidation;
		this.upiApi = upiApi;
	}

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	@RequestMapping(value = "/Process", method = RequestMethod.POST)
	public String processLoadMoney(@ModelAttribute LoadMoneyRequest dto, HttpServletRequest request, ModelMap modelMap,
			HttpSession session) {
		String sessionId = (String) session.getAttribute("sessionId");
		request.setAttribute("sessionId", sessionId);
		LoadMoneyResponse loadMoneyResponse = new LoadMoneyResponse();
		LoadMoneyError error = loadMoneyValidation.checkError(dto);
		if (error.isValid()) {
			if (sessionId != null && sessionId.length() != 0) {
				String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
				if (authority != null) {
					if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
						request.getSession().setAttribute("sessionId", sessionId);
						dto.setSessionId(sessionId);
						if (dto.getUseVnetWeb().equals("vnet")) {
							VNetRequest vNetRequest = new VNetRequest();
							vNetRequest.setSessionId(sessionId);
							vNetRequest.setReturnURL(UrlMetadatas.WEBURL + "/User/LoadMoney/VRedirect");
							vNetRequest.setAmount(dto.getAmount());
							VNetResponse vNetResponse = loadMoneyApi.initiateVnetBanking(vNetRequest);
							if (vNetResponse.isSuccess()) {
								modelMap.addAttribute("vnet", vNetResponse);
								return "User/LoadMoney/VNetPay";
						 } else {
							modelMap.addAttribute(ModelMapKey.MESSAGE, vNetResponse.getMessage());
							return "User/Home";
						 }
						} else if (dto.getUseVnetWeb().equals("others")) {
							dto.setReturnUrl(UrlMetadatas.WEBURL + "/User/LoadMoney/Redirect");
							loadMoneyResponse = loadMoneyApi.loadMoneyRequest(dto);
							if (loadMoneyResponse.isSuccess()) {
								modelMap.addAttribute("loadmoney", loadMoneyResponse);
								return "User/Pay";
							} else {
							//	modelMap.addAttribute(ModelMapKey.MESSAGE, loadMoneyResponse.getDescription());
								return "User/Home";
							}
						} else if (dto.getUseVnetWeb().equals("Upi")) {
							UpiSdkResponseDTO result = loadMoneyApi.loadMoneyUIPRequest(dto);
							if (result.getCode().equals(ResponseStatus.SUCCESS.getValue())) {
								modelMap.addAttribute(ModelMapKey.MESSAGE, result.getMessage());
								request.getSession().setAttribute("tRefId", result.getTransactionId());
								modelMap.addAttribute("netAmount", dto.getAmount());
								return "User/UpiSuccess";
								// return "User/Home";
							} else {
//								modelMap.addAttribute(ModelMapKey.ERROR, "Invalid VPA Request");
								modelMap.addAttribute(ModelMapKey.MESSAGE, result.getMessage());
								return "User/Home";
							}

						}
					}
				}
			}
		} else {
			modelMap.put(ModelMapKey.ERROR, error);
			return "User/Home";
		}
		return "redirect:/User/Home";
	}

	@RequestMapping(value = "/Redirect", method = RequestMethod.POST)
	public String redirectLoadMoney(WEBSRedirectResponse ebsResponse, Model model, HttpServletRequest request,
			HttpSession session) {
						EBSRedirectResponse ebsRedirectResponse = ConvertUtil.convertFromWEBS(ebsResponse);
						ResponseDTO result = loadMoneyApi.verifyEBSTransaction(ebsRedirectResponse);
						if (result.isSuccess()) {
							ebsRedirectResponse.setSuccess(true);
							ebsRedirectResponse.setResponseCode("0");
						} else {
							ebsRedirectResponse.setSuccess(false);
							ebsRedirectResponse.setResponseCode("1");
						}
						EBSRedirectResponse redirectResponse = loadMoneyApi.processRedirectSDK(ebsRedirectResponse);
						session.setAttribute("sessionId", redirectResponse.getSessionId());
						if (redirectResponse.isSuccess() || redirectResponse.getResponseCode().equals("0")) {
							model.addAttribute("msg", "Transasction Successful,You've successfully loaded amount "
									+ redirectResponse.getAmount() + " in your wallet");
						} else {
							model.addAttribute("msg", "Transaction failed, Please try again later.");
						}
						model.addAttribute("loadmoneyResponse", redirectResponse);
						return "User/Loading";
					}
				
	@RequestMapping(value = "/EbsHome", method = RequestMethod.GET)
	public String processLoadMoney(HttpServletRequest request, HttpSession session) {

		String sessionId = (String) session.getAttribute("sessionId");
		request.setAttribute("sessionId", sessionId);
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
					return "User/Home";
				}
			}
		}
		return "redirect:/";
	}

	@RequestMapping(value = "/VRedirect")
	public String redirectVNetLoadMoney(VRedirectResponse dto, Model model) {
		VNetStatusResponse result = loadMoneyApi.verifyVNetTransaction(dto);
		if (result.isSuccess()) {
			ResponseDTO responseDTO = loadMoneyApi.handleRedirectRequest(dto);
			model.addAttribute("response", responseDTO);
		} else {
			dto.setPAID("N");
			ResponseDTO responseDTO = loadMoneyApi.handleRedirectRequest(dto);
			model.addAttribute("response", responseDTO);
		}
		return "User/Loading";

	}

	@RequestMapping(value = "/UPIRedirect", method = RequestMethod.POST)
	public ResponseEntity<MerchantApprovedResponse> redirectUPILoadMoney(UPIRedirect dto, Model model,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) {
		System.err.println("INside UPI Redirect");
		MerchantApprovedResponse apprResp = new MerchantApprovedResponse();
		StringBuffer jb = new StringBuffer();
		String line = null;
		try {
			BufferedReader reader = request.getReader();
			while ((line = reader.readLine()) != null) {
				jb.append(line);
				System.err.println(line);
			}
			String output = jb.toString();
			MerchantMetaVPAResponse resp = ConvertUtil.getUPIresponse(output);
			System.out.println("Txnid : : " + resp.getTxnId());
			if (resp.getCode().equals(ResponseStatus.SUCCESS.getValue())) {
				dto.setReferenceId(resp.getTxnId());
				dto.setRespCode("0");
				apprResp.setOrgTxnId(resp.getOrgTxnId());
				apprResp.setOrgTxnRefId(resp.getReferenceNo());
				apprResp.setTxnId(resp.getTxnId());
				apprResp.setTimeStamp(System.currentTimeMillis() + "");
				System.err.println("UPI Redirect ************* ************** ******** *** *****  Success");
			} else {
				dto.setReferenceId(resp.getTxnId());
				dto.setRespCode("1");
				System.err.println("UPI Redirect ************* ************** ******** *** *****  Failed");
			}
		} catch (Exception e) {
			dto.setRespCode("1");
			System.out.println("UPI Redirect ************* ************** ******** *** *****  Exception");
		}
		ResponseDTO responseDTO = loadMoneyApi.handleUPIRedirectRequest(dto);
		if (responseDTO.isSuccess()) {
			apprResp.setRespCode("000");
			apprResp.setRespDesc("APPROVED");
		} else {
			apprResp.setRespCode("111");
			apprResp.setRespDesc("DECLINED");
		}
		System.out.println("Approved Response : : : ----------  " + apprResp.getRequest());
		System.out.println("Approved Response Code : : : ----------  " + apprResp.getRespCode());
		System.out.println("Approved Response DESC : : : ----------  " + apprResp.getRespDesc());
		return new ResponseEntity<>(apprResp, HttpStatus.OK);
	}

	@RequestMapping(value = "/UpiRedirectSuccess", method = RequestMethod.GET)
	public String successUpiPayment(HttpServletRequest request, HttpSession session, Model model) {
		try {
			String sessionId = (String) session.getAttribute("sessionId");
			request.setAttribute("sessionId", sessionId);
			if (sessionId != null && sessionId.length() != 0) {
				String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
				if (authority != null) {
					if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
						String tRefId = (String) request.getSession().getAttribute("tRefId");
						LoadMoneyRequest dto = new LoadMoneyRequest();
						dto.setSessionId(sessionId);
						dto.setReferenceNo(tRefId);
						boolean condition = true;
						System.err.println("Condition time : :  " + condition);
						while (condition) {
							StatusResponse statusResponse = loadMoneyApi.checkUpiStausAfterRedirectRequest(dto);
							System.err.println(
									"Code ----------------------------------------------- " + statusResponse.getCode());
							System.err.println("Message ----------------------------------------------- "
									+ statusResponse.getMessage());
							if (!statusResponse.getCode().equals("F11")) {
								System.err.println("Response after status check :   :  " + statusResponse.getStatus());
								System.err.println("Status Comparission : :  "
										+ statusResponse.getStatus().equals(Status.Success));
								if (statusResponse.getStatus().equals(Status.Success)) {
									System.err.println("After Redirection Txn ID : : " + tRefId);
									return "User/UpiRedirectSuccess";
								} else if (statusResponse.getStatus().equals(Status.Failed)) {
									System.err.println("After Redirection Txn ID : : " + tRefId);
									// TODO Call failed transactions
									return "User/UpiRedirecFailure";
								} else {
									condition = true;
								}
							} else {
								System.err.println(
										"Redirecting the transaction and the merchant transaction ID is  : : :  : : :   "
												+ statusResponse.getMessage());
								model.addAttribute("msg", statusResponse.getMessage());
								// TODO Call failed transactions
								return "User/UPIRedirectDateExpired";
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "User/UpiRedirecFailure";
		}
		return "redirect:/";
	}

	@RequestMapping(value = "/UpiHome", method = RequestMethod.GET)
	public String successHome(HttpServletRequest request, HttpSession session) {
		String sessionId = (String) session.getAttribute("sessionId");
		request.setAttribute("sessionId", sessionId);
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
					return "redirect:/User/Home";
				}
			}
		}
		return "redirect:/";
	}
	
	@RequestMapping(value = "/UpiRSuccess", method = RequestMethod.GET)
	public String succesUpiPaymet(Model model) {
//		model.addAttribute("returnURL", "https://www.bescom.co.in/SCP/Myhome.aspx");
//		return "ThirdParty/UpiRedirectFailureAcknow";
		return "User/UpiCountDown";
	}
	
	@RequestMapping(value = "/Voucher/Process", method = RequestMethod.GET)
	public String loadMoneyWithVoucher(HttpServletRequest request, HttpSession session) {
		String sessionId = (String) session.getAttribute("sessionId");
		request.setAttribute("sessionId", sessionId);
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
					return "redirect:/User/Home";
				}
			}
		}
		return "redirect:/";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/Voucher/Process")
	String loadMoneyWithVoucher(@ModelAttribute LoadMoneyRequest dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) {
		String sessionId = (String) session.getAttribute("sessionId");
		LoadMoneyResponse resp = new LoadMoneyResponse();
		CommonError error = loadMoneyValidation.checkLoadMoneyError(dto);
		try {
			if (error.isValid()) {
				if (sessionId != null && sessionId.length() != 0) {
					String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
					if (authority != null) {
						if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
							dto.setSessionId(sessionId);
							resp = loadMoneyApi.loadMoneyUsingVoucher(dto,Role.USER.getValue());
							if(resp.isSuccess()){
							   model.put("msg", resp.getMessage());
							}else{
							   model.put("error",resp.getMessage());
							}
							return "User/Home";
						}
					}
				}
			} else {
				model.put("error", error.getMessage());
				return "User/Home";
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return "redirect:/User/Home";
	}
}
