package com.payqwikweb.controller.web.user;

import java.awt.Image;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.payqwikweb.app.api.ITransactionApi;
import com.payqwikweb.app.model.request.ASendMoneyBankRequest;
import com.payqwikweb.app.model.request.SendMoneyBankRequest;
import com.payqwikweb.app.model.request.TransactionRequest;
import com.payqwikweb.app.model.response.SendMoneyBankResponse;
import com.payqwikweb.model.error.SendMoneyBankError;
import com.payqwikweb.util.APIUtils;
import com.thirdparty.model.ResponseDTO;

import org.apache.commons.fileupload.disk.DiskFileItem;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.itextpdf.text.log.SysoLogger;
import com.letsManage.model.response.ResponseStatus;
import com.payqwikweb.api.IAuthenticationApi;
import com.payqwikweb.app.api.IAgentSendMoneyApi;
import com.payqwikweb.app.api.ISendMoneyApi;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.request.SendMoneyMobileRequest;
import com.payqwikweb.app.model.response.SendMoneyMobileResponse;
import com.payqwikweb.model.error.SendMoneyMobileError;
import com.payqwikweb.util.Authorities;
import com.payqwikweb.validation.PayAtStoreValidation;
import com.payqwikweb.validation.SendMoneyValidation;

@Controller
@RequestMapping("/Agent/SendMoney")
public class AgentSendMoneyController implements MessageSourceAware {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private MessageSource messageSource;

	private final IAgentSendMoneyApi sendAgentMoneyApi;
	private final IAuthenticationApi authenticationApi;
	private final SendMoneyValidation sendMoneyValidation;
	private final PayAtStoreValidation payAtStoreValidation;
	private final ITransactionApi transactionApi;

	public AgentSendMoneyController(IAgentSendMoneyApi sendAgentMoneyApi, IAuthenticationApi authenticationApi,
			SendMoneyValidation sendMoneyValidation, PayAtStoreValidation payAtStoreValidation,
			ITransactionApi transactionApi) {
		this.sendAgentMoneyApi = sendAgentMoneyApi;
		this.authenticationApi = authenticationApi;
		this.sendMoneyValidation = sendMoneyValidation;
		this.payAtStoreValidation = payAtStoreValidation;
		this.transactionApi = transactionApi;
	}

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	
	@RequestMapping(value = "/AgentMobile", method = RequestMethod.POST)
	ResponseEntity<SendMoneyMobileResponse> sendAgentMoneyMobileRequest(@ModelAttribute SendMoneyMobileRequest dto,
			HttpServletRequest request, HttpServletResponse response, ModelMap modelMap, HttpSession session) {
		String sessionId = (String) session.getAttribute("sessionId");
		SendMoneyMobileResponse result = new SendMoneyMobileResponse();
		SendMoneyMobileError error = sendMoneyValidation.checkMobileError(dto);
		if (error.isValid()) {
			if (sessionId != null && sessionId.length() != 0) {
				String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
				if (authority != null) {
					if (authority.contains(Authorities.AGENT) && authority.contains(Authorities.AUTHENTICATED)) {
						TransactionRequest transactionRequest = new TransactionRequest();
						transactionRequest.setSessionId(sessionId);
						transactionRequest.setAmount(Double.parseDouble(dto.getAmount()));
						transactionRequest.setTransactionRefNo("D");
						ResponseDTO responseDTO1 = transactionApi.validateAgentTransaction(transactionRequest);
						
						if (responseDTO1.isValid()) {
							dto.setSessionId(sessionId);
							result = sendAgentMoneyApi.sendMoneyMobileRequest(dto,Role.AGENT);
						} else {
							result.setCode("F00");
							result.setMessage(responseDTO1.getMessage());
							result.setResponse(APIUtils.getCustomJSON("F00", responseDTO1.getMessage()).toString());
						}
						return new ResponseEntity<SendMoneyMobileResponse>(result, HttpStatus.OK);
					}
				}
			}
		} else {
			result.setCode("F00");
			result.setMessage("Invalid Input");
		}
		return new ResponseEntity<SendMoneyMobileResponse>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value="/BankTransfer", method=RequestMethod.POST)
	String BankTransfer(@ModelAttribute ASendMoneyBankRequest req,HttpServletRequest request,
			HttpServletResponse response,HttpSession session,ModelMap map){
		String sessionId = (String)session.getAttribute("sessionId");
		SendMoneyBankResponse result = new SendMoneyBankResponse();
		try{
				if (sessionId != null && sessionId.length() != 0) {
					String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
					if (authority != null) {
						if (authority.contains(Authorities.AGENT) && authority.contains(Authorities.AUTHENTICATED)) {
						   SendMoneyBankError error = sendMoneyValidation.agentSendBankError(req);
						 if(error.isValid()){
							MultipartFile file = req.getIdProofImage();
							boolean validImage = isValidImage(file);
							
							MultipartFile file2 = req.getIdProofBImage();
							boolean validImage2 = isValidImage(file2);
							
                           if(validImage && validImage2){
							TransactionRequest transactionRequest = new TransactionRequest();
							transactionRequest.setSessionId(sessionId);
							transactionRequest.setAmount(Double.parseDouble(req.getAmount()));
							transactionRequest.setTransactionRefNo("D");
							ResponseDTO responseDTO1 = transactionApi.validateAgentTransaction(transactionRequest);
							if(responseDTO1.isValid()) {
								req.setSessionId(sessionId);
								result = sendAgentMoneyApi.sendMoneyBankRequest(req);
								
								if(result.isSuccess()){
									map.addAttribute("bankRespCode",ResponseStatus.SUCCESS.getValue());
									map.addAttribute("bankResp",result.getMessage());
								}
								else{
									map.addAttribute("bankRespCode",ResponseStatus.FAILURE.getValue());
									map.addAttribute("bankResp",result.getMessage());
								}
							}else {
								map.addAttribute("bankRespCode",ResponseStatus.FAILURE.getValue());
								map.addAttribute("bankResp",responseDTO1.getMessage());
							}
                           }else{
                        	    map.addAttribute("bankRespCode",ResponseStatus.FAILURE.getValue());
                        	    map.addAttribute("bankResp","Enter valid Image");
                            }
						 }else{
								map.addAttribute("bankRespCode",ResponseStatus.FAILURE.getValue());
								map.addAttribute("bankResp",error.getMessage());
						  }
					}else{
					    map.addAttribute("bankRespCode",ResponseStatus.INVALID_SESSION.getValue());
						map.addAttribute("bankResp","Unauthorized Access");
					}
				 }else{
					map.addAttribute("bankRespCode",ResponseStatus.INVALID_SESSION.getValue());
					map.addAttribute("bankResp","Session Expired");
				 }
			  }else{
					map.addAttribute("bankRespCode",ResponseStatus.FAILURE.getValue());
					map.addAttribute("bankResp","Unauthorized Access");
				}
		}catch(Exception e){
		  System.out.println(e);
		}
	//	return new ResponseEntity<SendMoneyBankResponse>(result, HttpStatus.OK);
		return "Agent/Login/AgentBankTransfer";
	}
	private boolean isValidImage(MultipartFile file) {
		long length = 2 * 1024 * 1024;
		File imageFile = null;
		try {
			imageFile = convert(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
		boolean isValid = false;
		if (file.getContentType().contains("image")) {
			isValid = true;
		}
		if (file.getSize() <= length) {
			try {
				Image image = ImageIO.read(imageFile);

				if (image == null) {
				} else {
					isValid = true;
				}
			} catch (IOException ex) {
			}
		}
		return isValid;
	}
	private File convert(MultipartFile file) throws IOException {
		File convFile = new File(file.getOriginalFilename());
		convFile.createNewFile();
		FileOutputStream fos = new FileOutputStream(convFile);
		fos.write(file.getBytes());
		fos.close();
		return convFile;
	}
	
	@RequestMapping(value = "/sendMoneyRequestSuperAgent", method = RequestMethod.POST)
	ResponseEntity<SendMoneyMobileResponse> sendMoneyRequestSuperAgent(@ModelAttribute SendMoneyMobileRequest dto,
			HttpSession session) {
		String sessionId = (String) session.getAttribute("sessionId");
		SendMoneyMobileResponse result = new SendMoneyMobileResponse();
		SendMoneyMobileError error = sendMoneyValidation.checkMobileError(dto);
		if (error.isValid())
			{
			if (sessionId != null && sessionId.length() != 0) {
				String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
				if (authority != null) {
					if (authority.contains(Authorities.AGENT) && authority.contains(Authorities.AUTHENTICATED)) {
						TransactionRequest transactionRequest = new TransactionRequest();
						transactionRequest.setSessionId(sessionId);
						transactionRequest.setAmount(Double.parseDouble(dto.getAmount()));
						transactionRequest.setTransactionRefNo("D");
//						ResponseDTO responseDTO1 = transactionApi.validateAgentTransaction(transactionRequest);
//						if (responseDTO1.isValid()) 
//						{
							dto.setSessionId(sessionId);
							result = sendAgentMoneyApi.sendMoneyRequestSuperAgent(dto,Role.AGENT);
//						} else {
//							result.setMessage(responseDTO1.getMessage());
//							result.setCode("F00");
//						}
						return new ResponseEntity<SendMoneyMobileResponse>(result, HttpStatus.OK);
					}
				}
			}
		} else {
			result.setCode("F00");
			result.setMessage("Invalid Input");
		}
		return new ResponseEntity<SendMoneyMobileResponse>(result, HttpStatus.OK);
	}
}
