package com.payqwikweb.controller.web.user;

import java.awt.Desktop;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.payqwikweb.api.IAuthenticationApi;
import com.payqwikweb.api.IServiceCheckApi;
import com.payqwikweb.app.api.IATravelBusApi;
import com.payqwikweb.app.api.ILoadMoneyApi;
import com.payqwikweb.app.api.IUserApi;
import com.payqwikweb.app.model.ResponseStatus;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.busdto.AvailableTripsDTO;
import com.payqwikweb.app.model.busdto.BDPointDTO;
import com.payqwikweb.app.model.busdto.BDPointResponse;
import com.payqwikweb.app.model.busdto.BookTicketReq;
import com.payqwikweb.app.model.busdto.CancelPolicyListDTO;
import com.payqwikweb.app.model.busdto.DPPointsDTO;
import com.payqwikweb.app.model.busdto.GSTDetailDTO;
import com.payqwikweb.app.model.busdto.GetTransactionId;
import com.payqwikweb.app.model.busdto.PriceRecheckDetailsDto;
import com.payqwikweb.app.model.busdto.SaveSeatDetailsDTO;
import com.payqwikweb.app.model.busdto.TravellerDetailsDTO;
import com.payqwikweb.app.model.request.LoadMoneyRequest;
import com.payqwikweb.app.model.request.UserDetailsRequest;
import com.payqwikweb.app.model.request.bus.GetDestinationCity;
import com.payqwikweb.app.model.request.bus.GetSeatDetails;
import com.payqwikweb.app.model.request.bus.GetSourceCity;
import com.payqwikweb.app.model.request.bus.IsCancellableReq;
import com.payqwikweb.app.model.request.bus.ListOfAvailableTrips;
import com.payqwikweb.app.model.request.bus.TakeSeatDetails;
import com.payqwikweb.app.model.request.bus.TravellerAndGSTDetails;
import com.payqwikweb.app.model.response.LoadMoneyResponse;
import com.payqwikweb.app.model.response.UserDetailsResponse;
import com.payqwikweb.app.model.response.bus.CancelPolicyListDTOResp;
import com.payqwikweb.app.model.response.bus.ResponseDTO;
import com.payqwikweb.app.model.response.bus.SeatDetailsResponse;
import com.payqwikweb.app.model.response.bus.TranasctionIdResponse;
import com.payqwikweb.model.error.bus.BusRequestError;
import com.payqwikweb.model.web.Status;
import com.payqwikweb.util.Authorities;
import com.payqwikweb.validation.bus.BusRequestValidation;

@Controller
@RequestMapping("/Agent/Travel/Bus")
public class AgentBusController implements MessageSourceAware {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private MessageSource messageSource;
	private final IATravelBusApi aTravelBusApi;
	private final IAuthenticationApi authenticationApi;
	private final BusRequestValidation validation;
	private final IUserApi userApi;
	private final ILoadMoneyApi loadMoneyApi;
	private final IServiceCheckApi serviceCheckApi;

	//private static final String pdfUrlLive="D:/All App/payqwikapp/src/main/resources/HelloWorld.pdf";
	private static final String pdfUrlLive="/usr/local/tomcat7/webapps/PayQwikApp/WEB-INF/classes/HelloWorld.pdf";
//	private static final String pdfUrlLive = "D:/Piyush/Software/VPayQwik-app/payqwikapp/src/main/resources/HelloWorld.pdf";
	
	public AgentBusController(IATravelBusApi aTravelBusApi, IAuthenticationApi authenticationApi,
			BusRequestValidation validation,IUserApi userApi,ILoadMoneyApi loadMoneyApi,IServiceCheckApi serviceCheckApi) {

		this.aTravelBusApi=aTravelBusApi;	
		this.authenticationApi = authenticationApi;
		this.validation = validation;
		this.userApi=userApi;
		this.loadMoneyApi=loadMoneyApi;
		this.serviceCheckApi=serviceCheckApi;
	}

	@Override
	public void setMessageSource(MessageSource arg0) {
		this.messageSource = messageSource;

	}

	@RequestMapping(method = RequestMethod.POST, value = "/GetAllCityList", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> getAllCityList(HttpSession session) {

		ResponseDTO resp=new ResponseDTO();
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
			if (authority != null) {
				if (authority.contains(Authorities.AGENT) && authority.contains(Authorities.AUTHENTICATED)) {
					try {

						resp=aTravelBusApi.getAllCityList(sessionId);

						session.setAttribute("isTxnIdCreated", false);
						//System.err.println("Working");

						return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
					} catch (Exception e) {
						System.out.println(e);
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
						return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
					}
				}
				else {
					resp.setCode("F00");
					resp.setMessage("Unauthorised access");
					resp.setStatus("FAILED");
					return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
				}
			}else {
				resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
				resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
				resp.setMessage("Session expired");
				return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
			}

		}else {
			resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
			resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
			resp.setMessage("Session expired");
			return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/GetSourceCityBySourceKey", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> getSourceCity(HttpSession session, @RequestBody GetSourceCity dto) {

		ResponseDTO resp=new ResponseDTO();
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
			if (authority != null) {
				if (authority.contains(Authorities.AGENT) && authority.contains(Authorities.AUTHENTICATED)) {
					try {
						BusRequestError busError=validation.getAllSourceCityVal(dto);
						if (busError.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
							resp=aTravelBusApi.getAllSourceCity(dto);
							session.setAttribute("isTxnIdCreated", false);
						}
						else {
							resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
							resp.setStatus(ResponseStatus.BAD_REQUEST.getKey());
							resp.setMessage(busError.getMessage());
						}
						return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);

					} catch (Exception e) {
						System.out.println(e);
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
						return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
					}
				}
				else {
					resp.setCode("F00");
					resp.setMessage("Unauthorised access");
					resp.setStatus("FAILED");
					return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
				}
			}else {
				resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
				resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
				resp.setMessage("Session expired");
				return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
			}
		}else {
			resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
			resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
			resp.setMessage("Session expired");
			return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/GetDestinationCity", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> getDestinationCity(HttpSession session,@RequestBody GetDestinationCity dto) {

		ResponseDTO resp=new ResponseDTO();
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
			if (authority != null) {
				if (authority.contains(Authorities.AGENT) && authority.contains(Authorities.AUTHENTICATED)) {
					try {
						BusRequestError busError=validation.getAlldestinationCityVal(dto);
						if (busError.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
							resp=aTravelBusApi.getAllDestinationCity(dto);
							session.setAttribute("isTxnIdCreated", false);
						}
						else {
							resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
							resp.setStatus(ResponseStatus.BAD_REQUEST.getKey());
							resp.setMessage(busError.getMessage());
						}
						return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);

					} catch (Exception e) {
						System.out.println(e);
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
						return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
					}
				}else {
					resp.setCode("F00");
					resp.setMessage("Unauthorised access");
					resp.setStatus("FAILED");
					return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
				}
			}else {
				resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
				resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
				resp.setMessage("Session expired");
				return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
			}
		}else {
			resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
			resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
			resp.setMessage("Session expired");
			return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
		}
	}


	@RequestMapping(value = "/GetAllAvailableTrips", method = RequestMethod.GET)
	public String processEditedDetails(@ModelAttribute ListOfAvailableTrips dto, ModelMap modelMap,
			HttpSession session) {
		ResponseDTO resp=new ResponseDTO();
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
			if (authority != null) {
				if (authority.contains(Authorities.AGENT) && authority.contains(Authorities.AUTHENTICATED)) {
					try {
						BusRequestError busError=validation.getAllAvailableTripsVal(dto);
						if (busError.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
							modelMap.addAttribute("depDate", dto.getDate());
							String date=chaneDateFormat(dto.getDate());
							dto.setDate(date);
							String service=serviceCheckApi.agentCheckServiceActive(sessionId);
							if (Status.Active.getValue().equalsIgnoreCase(service)) {
								resp=aTravelBusApi.getAllAvailableTrips(dto);
								session.setAttribute("isTxnIdCreated", false);
								BDPointResponse response=(BDPointResponse)resp.getDetails();
								@SuppressWarnings("unchecked")
								Map<String, List<BDPointDTO>> bdpointMap =(Map<String, List<BDPointDTO>>)response.getBdpointMap();

								@SuppressWarnings("unchecked")
								Map<String, List<DPPointsDTO>> dppointMap=(Map<String, List<DPPointsDTO>>)response.getDppointMap();

								session.setAttribute("bdpointMap", bdpointMap);
								session.setAttribute("dppointMap", dppointMap);


								modelMap.addAttribute("message", resp.getMessage());
								modelMap.addAttribute("details", resp.getDetails());


								BDPointResponse bdPointResponse=(BDPointResponse)resp.getDetails();
								if (bdPointResponse.getAvailableTripsDTOs().size()==0) {
									modelMap.addAttribute("nobusmsg","No Bus Available");
									return "Agent/Travel/Flight/Home";
								}

								
								modelMap.addAttribute("minAmt", bdPointResponse.getAvailableTripsDTOs().get(0).getPrice());
								modelMap.addAttribute("maxAmt",  bdPointResponse.getAvailableTripsDTOs().get(bdPointResponse.getAvailableTripsDTOs().size()-1).getPrice());
								
								session.setAttribute("minAmt", bdPointResponse.getAvailableTripsDTOs().get(0).getPrice());
								session.setAttribute("maxAmt", bdPointResponse.getAvailableTripsDTOs().get(bdPointResponse.getAvailableTripsDTOs().size()-1).getPrice());
								session.setAttribute("tripsDetailsMsg", resp.getMessage());
								session.setAttribute("tripsDetails", resp.getDetails());

								return "Agent/Travel/Bus/ListOfAvailableTrips";
							}
							else {
								modelMap.addAttribute("bserrMsg","Service is under maintenance");
								return "Agent/Travel/Flight/Home";
							}
						}
						else {
							resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
							resp.setStatus(ResponseStatus.BAD_REQUEST.getKey());
							resp.setMessage(busError.getMessage());
						}

					} catch (Exception e) {
						System.out.println(e);
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
					}
				}
				else {
					resp.setCode("F00");
					resp.setMessage("Unauthorised access");
					resp.setStatus("FAILED");
				}
			}
			else {
				resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
				resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
				resp.setMessage("Session expired");

			}
		}else {
			resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
			resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
			resp.setMessage("Session expired");

		}
		return "redirect:/Home";
	}


	@RequestMapping(method = RequestMethod.POST, value = "/GetSeatDetails", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> getSeatDetails(HttpSession session, @RequestBody GetSeatDetails dto) {

		ResponseDTO resp=new ResponseDTO();
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
			if (authority != null) {
				if (authority.contains(Authorities.AGENT) && authority.contains(Authorities.AUTHENTICATED)) {
					try {
						BusRequestError busError=validation.getSeatDetailsVal(dto);
						if (busError.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {

							dto.setJourneyDate(changeDate(dto.getJourneyDate()));
							resp=aTravelBusApi.getSeatDetails(dto);

							session.setAttribute("isTxnIdCreated", false);
							session.setAttribute("seatDetails", resp.getDetails());

							@SuppressWarnings("unchecked")
							Map<String, List<BDPointDTO>> bdpointMap =(Map<String, List<BDPointDTO>>)session.getAttribute("bdpointMap");
							List<BDPointDTO> bdPointDTO=bdpointMap.get(dto.getBusId());

							@SuppressWarnings("unchecked")
							Map<String, List<DPPointsDTO>> dpPointMap=(Map<String, List<DPPointsDTO>>)session.getAttribute("dppointMap");
							List<DPPointsDTO> dpPointsDTO=dpPointMap.get(dto.getBusId());

							for (int i = 0; i < dpPointsDTO.size(); i++) {
								System.err.println("dpPointDTO: "+dpPointsDTO.get(i).getDpName());
							}

							SeatDetailsResponse seatDResp=(SeatDetailsResponse)resp.getDetails();
							seatDResp.setListBoardingPoints(bdPointDTO);
							seatDResp.setListDropingPoints(dpPointsDTO);
						}
						else {
							resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
							resp.setStatus(ResponseStatus.BAD_REQUEST.getKey());
							resp.setMessage(busError.getMessage());
						}
						return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);

					} catch (Exception e) {
						System.out.println(e);
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
						return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
					}
				}
				else {
					resp.setCode("F00");
					resp.setMessage("Unauthorised access");
					resp.setStatus("FAILED");
					return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
				}
			}
			else {
				resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
				resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
				resp.setMessage("Session expired");
				return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
			}
		}else {
			resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
			resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
			resp.setMessage("Session expired");
			return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/takeSeatDetails", method = RequestMethod.GET)
	public String takeSeatDetails(@ModelAttribute TakeSeatDetails dto, ModelMap modelMap,
			HttpSession session) {
		ResponseDTO resp=new ResponseDTO();
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
			if (authority != null) {
				if (authority.contains(Authorities.AGENT) && authority.contains(Authorities.AUTHENTICATED)) {
					try {
						//BusRequestError busError=validation.getAllAvailableTripsVal(dto);
						if (dto.getBoardingInfo()!=null) {

							//resp=aTravelBusApi.getAllAvailableTrips(dto);

							/*System.err.println(dto.getBoardingInfo());
							System.err.println(dto.getDroppingInfo());*/

							session.setAttribute("isTxnIdCreated", false);
							session.setAttribute("takeSeatDetails", dto);

							BDPointDTO bdPointDTO=getBdInfo(dto.getBoardingInfo());
							
							String tseats[]=splitSeats(dto.getSeats());

							String a[]=dto.getBoardingInfo().split(",");

							SaveSeatDetailsDTO seatDetailsDTO=new SaveSeatDetailsDTO();
							
							seatDetailsDTO.setBusType(dto.getBusType());
							seatDetailsDTO.setTravelName(dto.getTravelName());
							seatDetailsDTO.setJourneyDate(dto.getDoj());
							seatDetailsDTO.setSeatDetail(tseats.length+"");
							if (dto.getTotalFare()!=null && !dto.getTotalFare().isEmpty()) {
							   seatDetailsDTO.setTotalFare(Double.parseDouble(dto.getTotalFare()));
							}
							
							seatDetailsDTO.setDepTime(dto.getDepartureTime());
							seatDetailsDTO.setArrTime(dto.getArrivalTime());
							seatDetailsDTO.setSource(dto.getSource());
							seatDetailsDTO.setDestination(dto.getDestination());
							seatDetailsDTO.setBoardTime(bdPointDTO.getTime());
							seatDetailsDTO.setBusid(dto.getBusId());
							seatDetailsDTO.setBoardId(bdPointDTO.getBdid());
							seatDetailsDTO.setBoardLocation(bdPointDTO.getBdlocation());
							seatDetailsDTO.setBoardContactNo(bdPointDTO.getContactNumber());
						
							resp=aTravelBusApi.saveSeatDetails(sessionId, seatDetailsDTO);
							
							modelMap.addAttribute("busType", dto.getBusType());
							modelMap.addAttribute("travelName", dto.getTravelName());
							modelMap.addAttribute("date", dto.getDoj());
							modelMap.addAttribute("seats", dto.getSeats());
							modelMap.addAttribute("totalSeats",tseats.length);
							modelMap.addAttribute("totalpas",tseats);
							modelMap.addAttribute("totalpasSize",tseats.length);
							modelMap.addAttribute("totalAmt",dto.getTotalFare());
							modelMap.addAttribute("dTime", dto.getDepartureTime());
							modelMap.addAttribute("aTime", dto.getArrivalTime());
							modelMap.addAttribute("source", dto.getSource());
							modelMap.addAttribute("destination", dto.getDestination());
							modelMap.addAttribute("duration", dto.getDuration());
							modelMap.addAttribute("boarding", a[1]);
							
							session.setAttribute("tripId", resp.getDetails());
							
							
							return "Agent/Travel/Bus/TravellerDetails";
						}
						else {
							resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
							resp.setStatus(ResponseStatus.BAD_REQUEST.getKey());
						}
						return "Agent/Travel/Flight/Home";
					} catch (Exception e) {
						e.printStackTrace();
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
						return "Agent/Travel/Flight/Home";
					}
				}
				else {
					resp.setCode("F00");
					resp.setMessage("Unauthorised access");
					resp.setStatus("FAILED");
					return "Agent/Travel/Flight/Home";
				}
			}
			else {
				resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
				resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
				resp.setMessage("Session expired");
				return "Agent/Travel/Flight/Home";
			}
		}else {
			resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
			resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
			resp.setMessage("Session expired");
			return "Agent/Travel/Flight/Home";
		}
	}




	@RequestMapping(value = "/getTxnId", method = RequestMethod.POST)
	public String getTxnId(@ModelAttribute TravellerAndGSTDetails dto, ModelMap modelMap,
			HttpSession session) {
		ResponseDTO resp=new ResponseDTO();
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
			if (authority != null) {
				if (authority.contains(Authorities.AGENT) && authority.contains(Authorities.AUTHENTICATED)) {
					try {

						if (dto.getEmail()!=null) {
							UserDetailsResponse uDetailsResponse = new UserDetailsResponse();
							UserDetailsRequest userDetails=new UserDetailsRequest();
							userDetails.setSessionId(sessionId);
							uDetailsResponse=userApi.getUserDetails(userDetails, Role.AGENT);

							SeatDetailsResponse seatDetails=(SeatDetailsResponse)session.getAttribute("seatDetails");

							BDPointResponse sessionResp=(BDPointResponse)session.getAttribute("tripsDetails");

							List<AvailableTripsDTO> availableTrips=sessionResp.getAvailableTripsDTOs();

							TakeSeatDetails takeSeatDetails=(TakeSeatDetails)session.getAttribute("takeSeatDetails"); 

							List<CancelPolicyListDTO> cancelPolicyList=getCanInfo(availableTrips, takeSeatDetails.getBusId());

							String tseats[]=splitSeats(takeSeatDetails.getSeats());
							String fares[]=	getfares(takeSeatDetails.getFares());

							System.err.println("Fares:: "+fares[0]);

							double totalfare=0;

							sessionResp.getSource();
							sessionResp.getDestination();
							sessionResp.getSourceId();
							sessionResp.getDestinationId();

							takeSeatDetails.getBusType();
							takeSeatDetails.getDepartureTime();
							takeSeatDetails.getArrivalTime();
							takeSeatDetails.getTravelName();
							takeSeatDetails.getBusId();
							BDPointDTO bdPointDTO=getBdInfo(takeSeatDetails.getBoardingInfo());
							DPPointsDTO dpPointsDTO=getDpInfo(takeSeatDetails.getDroppingInfo());

							List<TravellerDetailsDTO> travellerList=getTravDetails(dto,tseats,fares);

							GetTransactionId req=new GetTransactionId();
							String seatDetail="";

							for (int i = 0; i < tseats.length; i++) {
								if (i>0) {
									seatDetail+="#";
								}
								seatDetail+=tseats[i]+"@"+fares[i];
								totalfare+=Double.parseDouble(fares[i]);
							}
							req.setSeatDetail(seatDetail);

							req.setMobileNo(dto.getMobile());
							req.setEmail(dto.getEmail());
							req.setEngineId(takeSeatDetails.getEngineId());
							req.setBusid(takeSeatDetails.getBusId());
							req.setBusType(takeSeatDetails.getBusType());
							req.setBoardId(bdPointDTO.getBdid());
							req.setBoardLocation("");
							req.setBoardName(bdPointDTO.getBdPoint());
							req.setArrTime(takeSeatDetails.getArrivalTime());
							req.setDepTime(takeSeatDetails.getDepartureTime());
							req.setTravelName(takeSeatDetails.getTravelName());
							req.setSource(sessionResp.getSource());
							req.setDestination(sessionResp.getDestination());
							req.setSourceid(sessionResp.getSourceId()+"");
							req.setDestinationId(sessionResp.getDestinationId()+"");
							req.setJourneyDate(changeDate(takeSeatDetails.getDoj()));
							req.setBoardpoint(bdPointDTO.getBdPoint());
							req.setBoardprime(bdPointDTO.getPrime());
							req.setBoardTime(bdPointDTO.getTime());
							req.setBoardlandmark(bdPointDTO.getLandmark());
							req.setBoardContactNo(bdPointDTO.getContactNumber());
							req.setDropId(dpPointsDTO.getDpId());
							req.setDropName(dpPointsDTO.getDpName());
							req.setDroplocatoin(dpPointsDTO.getLocatoin());
							req.setDropprime(dpPointsDTO.getPrime());
							req.setDropTime(dpPointsDTO.getDpTime());
							req.setRouteId(takeSeatDetails.getRoutId());

							req.setCouponCode("");
							req.setDiscount(0);
							req.setTravellers(travellerList);
							req.setGstDetails(getGstInfo(dto));
							req.setCancelPolicyList(cancelPolicyList);
							req.setwLCode("Bus");
							req.setIpAddress("123");
							req.setVersion("1.0");
							req.setCommission(0);
							req.setMarkup(0);
							req.setAgentCode("");
							req.setAddress("Bangalore");
							req.setTotalFare(totalfare);

							session.setAttribute("travellerAndGstDetails", dto);

							resp=aTravelBusApi.getTxnId(sessionId,req,uDetailsResponse);

							if (resp.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())){
								modelMap.addAttribute("noOfPas",tseats.length);
								modelMap.addAttribute("travellerList",travellerList);
								modelMap.addAttribute("source",sessionResp.getSource());
								modelMap.addAttribute("destination",sessionResp.getDestination());
								modelMap.addAttribute("fare",totalfare);
								String a[]=takeSeatDetails.getDoj().split("-");
								modelMap.addAttribute("yy",a[0]);
								modelMap.addAttribute("mm",a[1]);
								modelMap.addAttribute("dd",a[2]);

								TranasctionIdResponse txnIdResponse=(TranasctionIdResponse)resp.getDetails();
								session.setAttribute("vpTxnNo", txnIdResponse.getVpQTxnId());
								session.setAttribute("mpQTxnId", txnIdResponse.getMpQTxnId());
								session.setAttribute("txnId", txnIdResponse.getTransactionid());

								session.setAttribute("isTxnIdCreated", true);
								session.setAttribute("noOfPas",tseats.length);
								session.setAttribute("travellerList",travellerList);
								session.setAttribute("source",sessionResp.getSource());
								session.setAttribute("destination",sessionResp.getDestination());
								session.setAttribute("fare",totalfare);
								session.setAttribute("yy",a[0]);
								session.setAttribute("mm",a[1]);
								session.setAttribute("dd",a[2]);

								return "Agent/Travel/Bus/Checkout";
							}
							else if (resp.getCode().equalsIgnoreCase("H01")) {
								return "Agent/Home";
							}
							else if ((resp.getCode().equalsIgnoreCase("T01"))) {

								LoadMoneyRequest loadMoneyRequest=new LoadMoneyRequest(); 
								loadMoneyRequest.setAmount(resp.getSplitAmount());
								loadMoneyRequest.setSessionId(sessionId);
								loadMoneyRequest.setName(uDetailsResponse.getFirstName());
								loadMoneyRequest.setEmail(uDetailsResponse.getEmail());
								loadMoneyRequest.setPhone(uDetailsResponse.getContactNo());
								loadMoneyRequest.setReturnUrl("http://localhost:8090/Agent/Travel/Bus/getTxnIdBySplitPayment");
								LoadMoneyResponse loadMoneyResponse=loadMoneyApi.loadMoneyRequest(loadMoneyRequest);
								if (loadMoneyResponse.isSuccess()) {

									session.setAttribute("getTxnRespforSplit", resp);
									modelMap.addAttribute("loadmoney", loadMoneyResponse);
									return "User/Pay";

								}else {
									resp.setStatus("Failure");
									resp.setCode("H01");
								//	resp.setMessage(loadMoneyResponse.getDescription());
								}

							}

							modelMap.addAttribute("txnErrMsg",resp.getMessage());
							BDPointResponse bdPointResponse=(BDPointResponse)session.getAttribute("tripsDetails");
							modelMap.addAttribute("message", session.getAttribute("tripsDetailsMsg"));
							modelMap.addAttribute("details", bdPointResponse);
							return "Agent/Travel/Bus/ListOfAvailableTrips";
						}
						else {
							resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
							resp.setStatus(ResponseStatus.BAD_REQUEST.getKey());
						}
						return "Agent/Travel/Flight/Home";
					} catch (Exception e) {
						System.err.println(e);
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
						return "Agent/Travel/Flight/Home";
					}
				}
				else {
					resp.setCode("F00");
					resp.setMessage("Unauthorised access");
					resp.setStatus("FAILED");
					return "Agent/Travel/Flight/Home";
				}
			}
			else {
				resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
				resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
				resp.setMessage("Session expired");
				return "Agent/Travel/Flight/Home";
			}
		}else {
			resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
			resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
			resp.setMessage("Session expired");
			return "Agent/Travel/Flight/Home";
		}
	}

	@RequestMapping(value = "/cancelInitPayment", method = RequestMethod.GET)
	public String cancelInitPayment(@ModelAttribute BookTicketReq dto, ModelMap modelMap,
			HttpSession session) {
		ResponseDTO resp=new ResponseDTO();
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
			if (authority != null) {
				if (authority.contains(Authorities.AGENT) && authority.contains(Authorities.AUTHENTICATED)) {
					try {
						dto.setVpqTxnId((String)session.getAttribute("vpTxnNo"));

						session.setAttribute("isTxnIdCreated", false);
						if (dto.getVpqTxnId()!=null || !dto.getVpqTxnId().isEmpty()) {

							String mpQTxnId=(String)session.getAttribute("mpQTxnId");
							String txnId=(String)session.getAttribute("txnId");
							dto.setSessionId(sessionId);
							dto.setMdexTxnId(mpQTxnId);
							dto.setTransactionId(txnId);
							resp=aTravelBusApi.cancelInitPayment(dto);

							BDPointResponse bdPointResponse=(BDPointResponse)session.getAttribute("tripsDetails");
							modelMap.addAttribute("message", session.getAttribute("tripsDetailsMsg"));
							modelMap.addAttribute("details", bdPointResponse);
							modelMap.addAttribute("minAmt", session.getAttribute("minAmt"));
							modelMap.addAttribute("maxAmt",  session.getAttribute("maxAmt"));
							
							return "Agent/Travel/Bus/ListOfAvailableTrips";
						}
						else {
							resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
							resp.setStatus(ResponseStatus.BAD_REQUEST.getKey());
							resp.setMessage("Please Enter Valid Transection No");
							return "Agent/Travel/Bus/ListOfAvailableTrips";
						}

					} catch (Exception e) {
						System.out.println(e);
						e.printStackTrace();
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
					}
				}
				else {
					resp.setCode("F00");
					resp.setMessage("Unauthorised access");
					resp.setStatus("FAILED");
				}
			}
			else {
				resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
				resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
				resp.setMessage("Session expired");

			}
		}else {
			resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
			resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
			resp.setMessage("Session expired");
		}
		return "redirect:/Home";
	}



	@RequestMapping(method = RequestMethod.POST, value = "/paymentForBookTicket", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> bookTicketPayment(HttpSession session) {
		ResponseDTO resp=new ResponseDTO();
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
			if (authority != null) {
				if (authority.contains(Authorities.AGENT) && authority.contains(Authorities.AUTHENTICATED)) {
					try {
						BookTicketReq dto=new BookTicketReq();

						session.setAttribute("isTxnIdCreated", false);
						dto.setMdexTxnId((String)session.getAttribute("mpQTxnId"));
						dto.setSessionId(sessionId);
						dto.setTransactionId((String)session.getAttribute("txnId"));
						dto.setVpqTxnId((String)session.getAttribute("vpTxnNo"));

						BusRequestError busError=validation.bookTicketVal(dto);
						if (busError.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {

							resp=aTravelBusApi.bookTicket(dto);

							if (!(ResponseStatus.SUCCESS.getKey().equalsIgnoreCase(resp.getMdexStatus()))) {
								aTravelBusApi.cancelInitPayment(dto);
								resp.setMessage("Something went wrong. Please try later");
							}
							else if (ResponseStatus.SUCCESS.getKey().equalsIgnoreCase(resp.getMdexStatus())) {
								if (!(ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(resp.getStatus()))) {
									resp.setMessage("Your Ticket Is Booked.But Payment Not Successful.Please Contact Customer Care");
								}
							}
						}
						else {
							resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
							resp.setStatus(ResponseStatus.BAD_REQUEST.getKey());
							resp.setMessage(busError.getMessage());
						}

					} catch (Exception e) {
						System.out.println(e);
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
					}
				}
				else {
					resp.setCode("F00");
					resp.setMessage("Unauthorised access");
					resp.setStatus("FAILED");
				}
			}
			else {
				resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
				resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
				resp.setMessage("Session expired");

			}
		}else {
			resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
			resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
			resp.setMessage("Session expired");

		}
		return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
	}



	private String chaneDateFormat(String date)
	{
		String a[]=date.split("/");
		String fDate="";
		for (int i = 0; i < a.length-1; i++) {
			fDate+=a[i]+"-";
		}
		fDate+=a[a.length-1];

		return fDate;
	}

	private String changeDate(String date)
	{
		String a[]=date.split("-");
		String fDate="";
		for (int i = a.length-1; i >0 ; i--) {
			fDate+=a[i]+"-";
		}
		fDate+=a[0];

		return fDate;
	}

	private String[] splitSeats(String seats)
	{
		seats=seats.trim();
		String a[]=seats.split(" ");

		return a;
	}


	private List<TravellerDetailsDTO> getTravDetails(TravellerAndGSTDetails details,String[] tseats,String[] fares)
	{
		String title[]=details.getTitle().split(",");
		String fName[]=details.getfName().split(",");
		String lName[]=details.getlName().split(",");
		String age[]=details.getAge().split(",");

		List<TravellerDetailsDTO> list=new ArrayList<>();

		for (int i = 0; i < age.length; i++) {
			String gender="";
			if (title[i].equalsIgnoreCase("Mr.")) {
				gender="male"; 
			}else{
				gender="female";
			}
			TravellerDetailsDTO  travellerDTO=new TravellerDetailsDTO();
			travellerDTO.setTitle(title[i]);
			travellerDTO.setfName(fName[i]);
			travellerDTO.setlName(lName[i]);
			travellerDTO.setAge(age[i]);
			travellerDTO.setSeatNo(tseats[i]);
			travellerDTO.setFare(fares[i]);
			travellerDTO.setGender(gender);
			travellerDTO.setSeatType("normal");
			list.add(travellerDTO);
		}
		return list;
	}

	private BDPointDTO getBdInfo(String details)
	{
		String info[]=details.split(",");

		BDPointDTO  bdPointDTO=new BDPointDTO();

		bdPointDTO.setBdid(info[0]);
		bdPointDTO.setBdPoint(info[1]);
		bdPointDTO.setPrime(info[2]);
		bdPointDTO.setTime(info[3]);

		return bdPointDTO;
	}

	private DPPointsDTO getDpInfo(String details)
	{
		String info[]=details.split(",");

		DPPointsDTO  bdPointDTO=new DPPointsDTO();

		bdPointDTO.setDpId(info[0]);
		bdPointDTO.setDpName(info[1]);
		bdPointDTO.setPrime(info[2]);
		bdPointDTO.setDpTime(info[3]);

		return bdPointDTO;
	}

	private GSTDetailDTO getGstInfo(TravellerAndGSTDetails details)
	{
		GSTDetailDTO gstDetailDTO=new GSTDetailDTO();

		gstDetailDTO.setAddress(details.getgAddress());
		gstDetailDTO.setCompanyName(details.getGcName());
		gstDetailDTO.setGstNumber(details.getGstNo());
		gstDetailDTO.setAddress(details.getgAddress());
		gstDetailDTO.setPhone(details.getGmobile());

		return gstDetailDTO;
	}

	private List<CancelPolicyListDTO> getCanInfo(List<AvailableTripsDTO> details,String busId)
	{
		List<CancelPolicyListDTOResp> list=null;

		List<CancelPolicyListDTO> cList=new ArrayList<>();

		for (AvailableTripsDTO availableTripsDTO : details) {

			if(availableTripsDTO.getId().equalsIgnoreCase(busId))
			{
				list=availableTripsDTO.getCancelPolicyListDTO();
			}
		}

		for (int i = 0; i < list.size(); i++) {
			CancelPolicyListDTO cancelPolicyListDTO=new CancelPolicyListDTO();

			cancelPolicyListDTO.setTimeFrom(list.get(i).getTimeFrom());
			cancelPolicyListDTO.setTimeTo(list.get(i).getTimeTo());
			cancelPolicyListDTO.setPercentageCharge(list.get(i).getPercentageCharge());
			cancelPolicyListDTO.setFlatCharge(list.get(i).getFlatCharge());
			cancelPolicyListDTO.setFlat(list.get(i).isFlat());
			cancelPolicyListDTO.setSeatno(list.get(i).getSeatno());
			cList.add(cancelPolicyListDTO);
		}

		return cList;
	}



	private String[] getfares(String details)
	{
		String[] fares=details.split(",");

		return fares;
	}




	@RequestMapping(value = "/getTxnIdBySplitPayment", method = RequestMethod.POST)
	public String getTxnIdBySplitPayment(@ModelAttribute TravellerAndGSTDetails dto1, ModelMap modelMap,
			HttpSession session) {
		ResponseDTO resp=new ResponseDTO();
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
			if (authority != null) {
				if (authority.contains(Authorities.AGENT) && authority.contains(Authorities.AUTHENTICATED)) {
					try {

						TravellerAndGSTDetails dto=(TravellerAndGSTDetails)(session.getAttribute("travellerAndGstDetails"));

						if (dto.getEmail()!=null) {
							UserDetailsResponse uDetailsResponse = new UserDetailsResponse();
							UserDetailsRequest userDetails=new UserDetailsRequest();
							userDetails.setSessionId(sessionId);
							uDetailsResponse=userApi.getUserDetails(userDetails, Role.AGENT);

							SeatDetailsResponse seatDetails=(SeatDetailsResponse)session.getAttribute("seatDetails");

							BDPointResponse sessionResp=(BDPointResponse)session.getAttribute("tripsDetails");

							List<AvailableTripsDTO> availableTrips=sessionResp.getAvailableTripsDTOs();

							TakeSeatDetails takeSeatDetails=(TakeSeatDetails)session.getAttribute("takeSeatDetails"); 

							List<CancelPolicyListDTO> cancelPolicyList=getCanInfo(availableTrips, takeSeatDetails.getBusId());

							String tseats[]=splitSeats(takeSeatDetails.getSeats());
							String fares[]=	getfares(takeSeatDetails.getFares());

							System.err.println("Fares:: "+fares[0]);

							double totalfare=0;


							sessionResp.getSource();
							sessionResp.getDestination();
							sessionResp.getSourceId();
							sessionResp.getDestinationId();

							takeSeatDetails.getBusType();
							takeSeatDetails.getDepartureTime();
							takeSeatDetails.getArrivalTime();
							takeSeatDetails.getTravelName();
							takeSeatDetails.getBusId();
							BDPointDTO bdPointDTO=getBdInfo(takeSeatDetails.getBoardingInfo());
							DPPointsDTO dpPointsDTO=getDpInfo(takeSeatDetails.getDroppingInfo());


							List<TravellerDetailsDTO> travellerList=getTravDetails(dto,tseats,fares);


							GetTransactionId req=new GetTransactionId();
							String seatDetail="";

							for (int i = 0; i < tseats.length; i++) {
								if (i>0) {
									seatDetail+="#";
								}
								seatDetail+=tseats[i]+"@"+fares[i];
								totalfare+=Double.parseDouble(fares[i]);
							}
							req.setSeatDetail(seatDetail);

							req.setMobileNo(dto.getMobile());
							req.setEmail(dto.getEmail());
							req.setEngineId(takeSeatDetails.getEngineId());
							req.setBusid(takeSeatDetails.getBusId());
							req.setBusType(takeSeatDetails.getBusType());
							req.setBoardId(bdPointDTO.getBdid());
							req.setBoardLocation("");
							req.setBoardName(bdPointDTO.getBdPoint());
							req.setArrTime(takeSeatDetails.getArrivalTime());
							req.setDepTime(takeSeatDetails.getDepartureTime());
							req.setTravelName(takeSeatDetails.getTravelName());
							req.setSource(sessionResp.getSource());
							req.setDestination(sessionResp.getDestination());
							req.setSourceid(sessionResp.getSourceId()+"");
							req.setDestinationId(sessionResp.getDestinationId()+"");
							req.setJourneyDate(changeDate(takeSeatDetails.getDoj()));
							req.setBoardpoint(bdPointDTO.getBdPoint());
							req.setBoardprime(bdPointDTO.getPrime());
							req.setBoardTime(bdPointDTO.getTime());
							req.setBoardlandmark(bdPointDTO.getLandmark());
							req.setBoardContactNo(bdPointDTO.getContactNumber());
							req.setDropId(dpPointsDTO.getDpId());
							req.setDropName(dpPointsDTO.getDpName());
							req.setDroplocatoin(dpPointsDTO.getLocatoin());
							req.setDropprime(dpPointsDTO.getPrime());
							req.setDropTime(dpPointsDTO.getDpTime());
							req.setRouteId(takeSeatDetails.getRoutId());

							req.setCouponCode("");
							req.setDiscount(0);
							req.setTravellers(travellerList);
							req.setGstDetails(getGstInfo(dto));
							req.setCancelPolicyList(cancelPolicyList);
							req.setwLCode("Bus");
							req.setIpAddress("123");
							req.setVersion("1.0");
							req.setCommission(0);
							req.setMarkup(0);
							req.setAgentCode("");
							req.setAddress("Bangalore");

							ResponseDTO responsefromSession=(ResponseDTO)session.getAttribute("getTxnRespforSplit");

							req.setTotalFare(totalfare);
							resp=aTravelBusApi.getTXnBySplitPayment(sessionId, req, responsefromSession.getSplitAmount());

							if (resp.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())){
								modelMap.addAttribute("noOfPas",tseats.length);
								modelMap.addAttribute("travellerList",travellerList);
								modelMap.addAttribute("source",sessionResp.getSource());
								modelMap.addAttribute("destination",sessionResp.getDestination());
								modelMap.addAttribute("fare",totalfare);
								String a[]=takeSeatDetails.getDoj().split("-");
								modelMap.addAttribute("yy",a[0]);
								modelMap.addAttribute("mm",a[1]);
								modelMap.addAttribute("dd",a[2]);

								TranasctionIdResponse txnIdResponse=(TranasctionIdResponse)responsefromSession.getDetails();
								session.setAttribute("vpTxnNo", resp.getDetails());
								session.setAttribute("mpQTxnId", txnIdResponse.getMpQTxnId());
								session.setAttribute("txnId", txnIdResponse.getTransactionid());

								return "Agent/Travel/Bus/Checkout";
							}
							else if (resp.getCode().equalsIgnoreCase("H01")) {
								return "Agent/Home";
							}
							else if ((resp.getCode().equalsIgnoreCase("T01"))) {


								LoadMoneyRequest loadMoneyRequest=new LoadMoneyRequest(); 
								/*loadMoneyRequest.setAmount((String)resp.getDetails());*/
								loadMoneyRequest.setAmount((String)resp.getDetails());
								loadMoneyRequest.setSessionId(sessionId);
								loadMoneyRequest.setName(uDetailsResponse.getFirstName());
								loadMoneyRequest.setEmail(uDetailsResponse.getEmail());
								loadMoneyRequest.setPhone(uDetailsResponse.getContactNo());
								loadMoneyRequest.setReturnUrl("http://localhost:8090/Agent/Travel/Bus");
								LoadMoneyResponse loadMoneyResponse=loadMoneyApi.loadMoneyRequest(loadMoneyRequest);
								if (loadMoneyResponse.isSuccess()) {

									resp.setStatus("Success");
									resp.setCode("T01");
									resp.setDetails(loadMoneyResponse);

								}else {
									resp.setStatus("Failure");
									resp.setCode("H01");
									//resp.setMessage(loadMoneyResponse.getDescription());
								}

								modelMap.addAttribute("loadmoney", resp.getDetails());
								return "Agent/Pay";
							}

							modelMap.addAttribute("txnErrMsg",resp.getMessage());
							BDPointResponse bdPointResponse=(BDPointResponse)session.getAttribute("tripsDetails");
							modelMap.addAttribute("message", session.getAttribute("tripsDetailsMsg"));
							modelMap.addAttribute("details", bdPointResponse);
							return "Agent/Travel/Bus/ListOfAvailableTrips";
						}
						else {
							resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
							resp.setStatus(ResponseStatus.BAD_REQUEST.getKey());
						}
						return "Agent/Travel/Flight/Home";
					} catch (Exception e) {
						System.err.println(e);
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
						return "Agent/Travel/Flight/Home";
					}
				}
				else {
					resp.setCode("F00");
					resp.setMessage("Unauthorised access");
					resp.setStatus("FAILED");
					return "Agent/Travel/Flight/Home";
				}
			}
			else {
				resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
				resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
				resp.setMessage("Session expired");
				return "Agent/Travel/Flight/Home";
			}
		}else {
			resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
			resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
			resp.setMessage("Session expired");
			return "Agent/Travel/Flight/Home";
		}
	}


	@RequestMapping(value = "/MyTickets", method = RequestMethod.GET)
	public String getAllTicket(@ModelAttribute BookTicketReq dto, ModelMap modelMap,
			HttpSession session) {
		ResponseDTO resp=new ResponseDTO();
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
			if (authority != null) {
				if (authority.contains(Authorities.AGENT) && authority.contains(Authorities.AUTHENTICATED)) {
					try {

						dto.setSessionId(sessionId);
						resp=aTravelBusApi.getAllMyBookTicketsForWeb(sessionId);

						session.setAttribute("isTxnIdCreated", false);
						modelMap.addAttribute("details", resp.getDetails());

						modelMap.addAttribute("val", "bus");
						//modelMap.addAttribute("ticketCreated",);
						return "Agent/Travel/MyBookingTickets";


					} catch (Exception e) {
						System.out.println(e);
						e.printStackTrace();
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
					}
				}
				else {
					resp.setCode("F00");
					resp.setMessage("Unauthorised access");
					resp.setStatus("FAILED");
				}
			}
			else {
				resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
				resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
				resp.setMessage("Session expired");

			}
		}else {
			resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
			resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
			resp.setMessage("Session expired");

		}
		return "redirect:/Home";
	}


	@RequestMapping(value = "/getSingleTicketTravellerDetails", method = RequestMethod.GET)
	public String getSingleTicketTravellerDetails(@RequestParam String emtTxnId, ModelMap modelMap,
			HttpSession session) {
		ResponseDTO resp=new ResponseDTO();
		ResponseDTO resp1=new ResponseDTO();
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
			if (authority != null) {
				if (authority.contains(Authorities.AGENT) && authority.contains(Authorities.AUTHENTICATED)) {
					try {

						session.setAttribute("isTxnIdCreated", false);
						if (emtTxnId!=null) {

							resp=aTravelBusApi.getSingleTicketTravellerDetails(sessionId,emtTxnId);
							IsCancellableReq dto=new IsCancellableReq();
							resp1=aTravelBusApi.isCancellable(dto);

							modelMap.addAttribute("details", resp.getDetails());

							modelMap.addAttribute("extraInfo", resp.getExtraInfo());

							return "Agent/Travel/Bus/TicketDetais";
						}
						else {
							return "Agent/Travel/Bus/MyTickets";
						}
					} catch (Exception e) {
						System.out.println(e);
						e.printStackTrace();
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
					}
				}
				else {
					resp.setCode("F00");
					resp.setMessage("Unauthorised access");
					resp.setStatus("FAILED");
				}
			}
			else {
				resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
				resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
				resp.setMessage("Session expired");

			}
		}else {
			resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
			resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
			resp.setMessage("Session expired");

		}
		return "redirect:/Home";
	}

	@RequestMapping(value = "/getTxnId", method = RequestMethod.GET)
	public String getTxnId(ModelMap modelMap,
			HttpSession session) {
		ResponseDTO resp=new ResponseDTO();
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
			if (authority != null) {
				if (authority.contains(Authorities.AGENT) && authority.contains(Authorities.AUTHENTICATED)) {
					try {

						boolean isTxnCreated=(boolean)session.getAttribute("isTxnIdCreated");

						if (isTxnCreated) {

							modelMap.addAttribute("noOfPas",session.getAttribute("noOfPas"));
							modelMap.addAttribute("travellerList",session.getAttribute("travellerList"));
							modelMap.addAttribute("source",session.getAttribute("source"));
							modelMap.addAttribute("destination",session.getAttribute("destination"));
							modelMap.addAttribute("fare",session.getAttribute("fare"));
							modelMap.addAttribute("yy",session.getAttribute("yy"));
							modelMap.addAttribute("mm",session.getAttribute("mm"));
							modelMap.addAttribute("dd",session.getAttribute("dd"));
							return "Agent/Travel/Bus/Checkout";
						}else {
							BDPointResponse bdPointResponse=(BDPointResponse)session.getAttribute("tripsDetails");
							modelMap.addAttribute("message", session.getAttribute("tripsDetailsMsg"));
							modelMap.addAttribute("details", bdPointResponse);
							return "Agent/Travel/Bus/ListOfAvailableTrips";
						}

					} catch (Exception e) {
						System.out.println(e);
						e.printStackTrace();
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
					}
				}
				else {
					resp.setCode("F00");
					resp.setMessage("Unauthorised access");
					resp.setStatus("FAILED");
				}
			}
			else {
				resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
				resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
				resp.setMessage("Session expired");

			}
		}else {
			resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
			resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
			resp.setMessage("Session expired");

		}
		return "redirect:/Home";
	}
	
	
	//    Changes for re price

	@RequestMapping(value = "/getTxnIdUpdate", method = RequestMethod.POST)
	public String getTxnIdgetTxnIdUpdate(@ModelAttribute TravellerAndGSTDetails dto, ModelMap modelMap,
			HttpSession session) {
		ResponseDTO resp=new ResponseDTO();
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
			if (authority != null) {
				if (authority.contains(Authorities.AGENT) && authority.contains(Authorities.AUTHENTICATED)) {
					try {

						if (dto.getEmail()!=null) {
							UserDetailsResponse uDetailsResponse = new UserDetailsResponse();
							UserDetailsRequest userDetails=new UserDetailsRequest();
							userDetails.setSessionId(sessionId);
							uDetailsResponse=userApi.getUserDetails(userDetails, Role.AGENT);

							SeatDetailsResponse seatDetails=(SeatDetailsResponse)session.getAttribute("seatDetails");

							BDPointResponse sessionResp=(BDPointResponse)session.getAttribute("tripsDetails");

							List<AvailableTripsDTO> availableTrips=sessionResp.getAvailableTripsDTOs();

							TakeSeatDetails takeSeatDetails=(TakeSeatDetails)session.getAttribute("takeSeatDetails"); 

							List<CancelPolicyListDTO> cancelPolicyList=getCanInfo(availableTrips, takeSeatDetails.getBusId());

							String tseats[]=splitSeats(takeSeatDetails.getSeats());
							String fares[]=	getfares(takeSeatDetails.getFares());

							System.err.println("Fares:: "+fares[0]);

							String tripId=(String)session.getAttribute("tripId");
							
							
							double totalfare=0;

							sessionResp.getSource();
							sessionResp.getDestination();
							sessionResp.getSourceId();
							sessionResp.getDestinationId();

							takeSeatDetails.getBusType();
							takeSeatDetails.getDepartureTime();
							takeSeatDetails.getArrivalTime();
							takeSeatDetails.getTravelName();
							takeSeatDetails.getBusId();
							BDPointDTO bdPointDTO=getBdInfo(takeSeatDetails.getBoardingInfo());
							DPPointsDTO dpPointsDTO=getDpInfo(takeSeatDetails.getDroppingInfo());

							List<TravellerDetailsDTO> travellerList=getTravDetails(dto,tseats,fares);

							GetTransactionId req=new GetTransactionId();
							String seatDetail="";

							for (int i = 0; i < tseats.length; i++) {
								if (i>0) {
									seatDetail+="#";
								}
								seatDetail+=tseats[i]+"@"+fares[i];
								totalfare+=Double.parseDouble(fares[i]);
							}
							req.setSeatDetail(seatDetail);

							req.setMobileNo(dto.getMobile());
							req.setEmail(dto.getEmail());
							req.setEngineId(takeSeatDetails.getEngineId());
							req.setBusid(takeSeatDetails.getBusId());
							req.setBusType(takeSeatDetails.getBusType());
							req.setBoardId(bdPointDTO.getBdid());
							req.setBoardLocation("");
							req.setBoardName(bdPointDTO.getBdPoint());
							req.setArrTime(takeSeatDetails.getArrivalTime());
							req.setDepTime(takeSeatDetails.getDepartureTime());
							req.setTravelName(takeSeatDetails.getTravelName());
							req.setSource(sessionResp.getSource());
							req.setDestination(sessionResp.getDestination());
							req.setSourceid(sessionResp.getSourceId()+"");
							req.setDestinationId(sessionResp.getDestinationId()+"");
							req.setJourneyDate(changeDate(takeSeatDetails.getDoj()));
							req.setBoardpoint(bdPointDTO.getBdPoint());
							req.setBoardprime(bdPointDTO.getPrime());
							req.setBoardTime(bdPointDTO.getTime());
							req.setBoardlandmark(bdPointDTO.getLandmark());
							req.setBoardContactNo(bdPointDTO.getContactNumber());
							req.setDropId(dpPointsDTO.getDpId());
							req.setDropName(dpPointsDTO.getDpName());
							req.setDroplocatoin(dpPointsDTO.getLocatoin());
							req.setDropprime(dpPointsDTO.getPrime());
							req.setDropTime(dpPointsDTO.getDpTime());
							req.setRouteId(takeSeatDetails.getRoutId());

							req.setCouponCode("");
							req.setDiscount(0);
							req.setTravellers(travellerList);
							req.setGstDetails(getGstInfo(dto));
							req.setCancelPolicyList(cancelPolicyList);
							req.setwLCode("Bus");
							req.setIpAddress("123");
							req.setVersion("1.0");
							req.setCommission(0);
							req.setMarkup(0);
							req.setAgentCode("");
							req.setAddress("Bangalore");
							req.setTotalFare(totalfare);

							req.setTripId(tripId);
							
							session.setAttribute("travellerAndGstDetails", dto);

							resp=aTravelBusApi.getTxnIdUpdated(sessionId,req,uDetailsResponse);

							if (resp.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())){
								modelMap.addAttribute("noOfPas",tseats.length);
								modelMap.addAttribute("travellerList",travellerList);
								modelMap.addAttribute("source",sessionResp.getSource());
								modelMap.addAttribute("destination",sessionResp.getDestination());
								modelMap.addAttribute("fare",totalfare);
								String a[]=takeSeatDetails.getDoj().split("-");
								modelMap.addAttribute("yy",a[0]);
								modelMap.addAttribute("mm",a[1]);
								modelMap.addAttribute("dd",a[2]);

								TranasctionIdResponse txnIdResponse=(TranasctionIdResponse)resp.getDetails();
								session.setAttribute("vpTxnNo", txnIdResponse.getVpQTxnId());
								session.setAttribute("mpQTxnId", txnIdResponse.getMpQTxnId());
								session.setAttribute("txnId", txnIdResponse.getTransactionid());
								session.setAttribute("seatHoldId", txnIdResponse.getSeatHoldId());
								session.setAttribute("priceeRecheckAmt", txnIdResponse.getPriceRecheckAmt());
								modelMap.addAttribute("priceeRecheckAmt", txnIdResponse.getPriceRecheckAmt());
								modelMap.addAttribute("seatHoldId", txnIdResponse.getSeatHoldId());
								
								modelMap.addAttribute("priceRecheck", txnIdResponse.isPriceRecheck());
								modelMap.addAttribute("fareBreak", txnIdResponse.getFareBreak());
								
								return "Agent/Travel/Bus/CheckoutUpdated";
							}
							else if (resp.getCode().equalsIgnoreCase("H01")) {
								return "Agent/Home";
							}
							else if ((resp.getCode().equalsIgnoreCase("T01"))) {

								LoadMoneyRequest loadMoneyRequest=new LoadMoneyRequest(); 
								loadMoneyRequest.setAmount(resp.getSplitAmount());
								loadMoneyRequest.setSessionId(sessionId);
								loadMoneyRequest.setName(uDetailsResponse.getFirstName());
								loadMoneyRequest.setEmail(uDetailsResponse.getEmail());
								loadMoneyRequest.setPhone(uDetailsResponse.getContactNo());
								loadMoneyRequest.setReturnUrl("http://localhost:8090/Agent/Travel/Bus/getTxnIdBySplitPayment");
								LoadMoneyResponse loadMoneyResponse=loadMoneyApi.loadMoneyRequest(loadMoneyRequest);
								if (loadMoneyResponse.isSuccess()) {

									session.setAttribute("getTxnRespforSplit", resp);
									modelMap.addAttribute("loadmoney", loadMoneyResponse);
									return "User/Pay";

								}else {
									resp.setStatus("Failure");
									resp.setCode("H01");
								//	resp.setMessage(loadMoneyResponse.getDescription());
								}

							}

							modelMap.addAttribute("txnErrMsg",resp.getMessage());
							BDPointResponse bdPointResponse=(BDPointResponse)session.getAttribute("tripsDetails");
							modelMap.addAttribute("message", session.getAttribute("tripsDetailsMsg"));
							modelMap.addAttribute("details", bdPointResponse);
							return "Agent/Travel/Bus/ListOfAvailableTrips";
						}
						else {
							resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
							resp.setStatus(ResponseStatus.BAD_REQUEST.getKey());
						}
						return "Agent/Travel/Flight/HomeNew";
					} catch (Exception e) {
						System.err.println(e);
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
						return "Agent/Travel/Flight/HomeNew";
					}
				}
				else {
					resp.setCode("F00");
					resp.setMessage("Unauthorised access");
					resp.setStatus("FAILED");
					return "Agent/Travel/Flight/HomeNew";
				}
			}
			else {
				resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
				resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
				resp.setMessage("Session expired");
				return "Agent/Travel/Flight/HomeNew";
			}
		}else {
			resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
			resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
			resp.setMessage("Session expired");
			return "Agent/Travel/Flight/HomeNew";
		}
	}


	@RequestMapping(method = RequestMethod.POST, value = "/BookTicketUpdate", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> bookTicket(HttpSession session) {
		ResponseDTO resp=new ResponseDTO();
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
			if (authority != null) {
				if (authority.contains(Authorities.AGENT) && authority.contains(Authorities.AUTHENTICATED)) {
					try {
						BookTicketReq dto=new BookTicketReq();

						session.setAttribute("isTxnIdCreated", false);
						dto.setMdexTxnId((String)session.getAttribute("mpQTxnId"));
						dto.setSessionId(sessionId);
						dto.setTransactionId((String)session.getAttribute("txnId"));
						dto.setVpqTxnId((String)session.getAttribute("vpTxnNo"));
						dto.setSeatHoldId((String)session.getAttribute("seatHoldId"));
						
						String amt=(String)session.getAttribute("priceeRecheckAmt");

						if (amt!=null && !amt.equalsIgnoreCase("null") && !amt.isEmpty()) {
							dto.setAmount(Double.parseDouble(amt));
						}

						BusRequestError busError=validation.bookTicketValUpdated(dto);
						if (busError.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {

							resp=aTravelBusApi.initPayment(dto);

							if (resp.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {

								dto.setVpqTxnId(resp.getTransactionRefNo());
								resp=aTravelBusApi.bookTicketUpdated(dto);

								if (!(ResponseStatus.SUCCESS.getKey().equalsIgnoreCase(resp.getMdexStatus()))) {
//									aTravelBusApi.cancelInitPayment(dto);
									resp.setMessage("Something went wrong. Please try later");
								}
								else if (ResponseStatus.SUCCESS.getKey().equalsIgnoreCase(resp.getMdexStatus())) {
									if (!(ResponseStatus.SUCCESS.getKey().equalsIgnoreCase(resp.getStatus()))) {
										resp.setMessage("Your Ticket Is Booked.But Payment Not Successful.Please Contact Customer Care");
									}
								}
							}else {
								resp.setCode(ResponseStatus.FAILURE.getValue());
								resp.setStatus(ResponseStatus.FAILURE.getKey());
								//resp.setMessage("");
							}

						}
						else {
							resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
							resp.setStatus(ResponseStatus.BAD_REQUEST.getKey());
							resp.setMessage(busError.getMessage());
						}

					} catch (Exception e) {
						System.out.println(e);
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
					}
				}
				else {
					resp.setCode("F00");
					resp.setMessage("Unauthorised access");
					resp.setStatus("FAILED");
				}
			}
			else {
				resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
				resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
				resp.setMessage("Session expired");

			}
		}else {
			resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
			resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
			resp.setMessage("Session expired");

		}
		return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
	}

	

	@RequestMapping(value = "/cancelCheckout", method = RequestMethod.GET)
	public String cancelCheckout(@ModelAttribute BookTicketReq dto, ModelMap modelMap,
			HttpSession session) {
		ResponseDTO resp=new ResponseDTO();
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
			if (authority != null) {
				if (authority.contains(Authorities.AGENT) && authority.contains(Authorities.AUTHENTICATED)) {
					try {

						session.setAttribute("isTxnIdCreated", false);

						BDPointResponse bdPointResponse=(BDPointResponse)session.getAttribute("tripsDetails");
						modelMap.addAttribute("message", session.getAttribute("tripsDetailsMsg"));
						modelMap.addAttribute("details", bdPointResponse);
						
						modelMap.addAttribute("minAmt", session.getAttribute("minAmt"));
						modelMap.addAttribute("maxAmt",  session.getAttribute("maxAmt"));
						
						return "Agent/Travel/Bus/ListOfAvailableTrips";


					} catch (Exception e) {
						System.out.println(e);
						e.printStackTrace();
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
					}
				}
				else {
					resp.setCode("F00");
					resp.setMessage("Unauthorised access");
					resp.setStatus("FAILED");
				}
			}
			else {
				resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
				resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
				resp.setMessage("Session expired");

			}
		}else {
			resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
			resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
			resp.setMessage("Session expired");
		}
		return "redirect:/Home";
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/getDynamicFareDetails", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> getDynamicFareDetails(HttpSession session, @RequestBody PriceRecheckDetailsDto dto) {

		ResponseDTO resp=new ResponseDTO();
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
			if (authority != null) {
				if (authority.contains(Authorities.AGENT) && authority.contains(Authorities.AUTHENTICATED)) {
					try {
						BusRequestError busError=validation.getDynamicFareDetails(dto.getSeatHoldId());
						if (busError.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
							
							resp=aTravelBusApi.getDynamicFareDetails(dto);
						}
						else {
							resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
							resp.setStatus(ResponseStatus.BAD_REQUEST.getKey());
							resp.setMessage(busError.getMessage());
						}
						return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);

					} catch (Exception e) {
						System.out.println(e);
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
						return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
					}
				}
				else {
					resp.setCode("F00");
					resp.setMessage("Unauthorised access");
					resp.setStatus("FAILED");
					return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
				}
			}else {
				resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
				resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
				resp.setMessage("Session expired");
				return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
			}
		}else {
			resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
			resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
			resp.setMessage("Session expired");
			return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
		}
	}
	
	/*Ticket pdf for Bus*/


	@RequestMapping(method = RequestMethod.POST, value = "/singleTicketPdfForBus", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> singleTicketPdfForBus(HttpSession session, @RequestBody BookTicketReq dto) {

		ResponseDTO resp=new ResponseDTO();
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
			if (authority != null) {
				if (authority.contains(Authorities.AGENT) && authority.contains(Authorities.AUTHENTICATED)) {
					try {
							
							resp=aTravelBusApi.getSingleTicketPdf(sessionId,dto.getEmtTxnId());
							
							if (resp.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
							
							Desktop desktop = Desktop.getDesktop();
							File file = new File(pdfUrlLive);
							desktop.open(file);
							
							}
						
						return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);

					} catch (Exception e) {
						System.out.println(e);
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
						return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
					}
				}
				else {
					resp.setCode("F00");
					resp.setMessage("Unauthorised access");
					resp.setStatus("FAILED");
					return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
				}
			}else {
				resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
				resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
				resp.setMessage("Session expired");
				return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
			}
		}else {
			resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
			resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
			resp.setMessage("Session expired");
			return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
		}
	}
	
}
