package com.payqwikweb.controller.web.user;

import java.text.SimpleDateFormat;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.payqwikweb.api.IAuthenticationApi;
import com.payqwikweb.app.api.IAgentLoadMoneyApi;
import com.payqwikweb.app.metadatas.UrlMetadatas;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.request.LoadMoneyRequest;
import com.payqwikweb.app.model.request.VNetRequest;
import com.payqwikweb.app.model.response.EBSRedirectResponse;
import com.payqwikweb.app.model.response.LoadMoneyResponse;
import com.payqwikweb.app.model.response.VNetResponse;
import com.payqwikweb.app.model.response.VRedirectResponse;
import com.payqwikweb.model.error.LoadMoneyError;
import com.payqwikweb.model.web.WEBSRedirectResponse;
import com.payqwikweb.util.Authorities;
import com.payqwikweb.util.ConvertUtil;
import com.payqwikweb.util.ModelMapKey;
import com.payqwikweb.validation.LoadMoneyValidation;
import com.thirdparty.model.ResponseDTO;

@Controller
@RequestMapping("/Agent/LoadMoney")
public class AgentLoadMoneyController implements MessageSourceAware {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

	private MessageSource messageSource;

	private final IAgentLoadMoneyApi loadAgentMoneyApi;
	private final IAuthenticationApi authenticationApi;
	private final LoadMoneyValidation loadMoneyValidation;

	public AgentLoadMoneyController(IAgentLoadMoneyApi loadAgentMoneyApi, IAuthenticationApi authenticationApi,
			LoadMoneyValidation loadMoneyValidation) {
		this.loadAgentMoneyApi = loadAgentMoneyApi;
		this.authenticationApi = authenticationApi;
		this.loadMoneyValidation = loadMoneyValidation;
	}

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	@RequestMapping(value = "/AgentProcess", method = RequestMethod.POST)
	public String processAgentLoadMoney(@ModelAttribute LoadMoneyRequest dto, HttpServletRequest request,
			ModelMap modelMap, HttpSession session) {
		String sessionId = (String) session.getAttribute("sessionId");
		request.setAttribute("sessionId", sessionId);
		LoadMoneyResponse loadMoneyResponse = new LoadMoneyResponse();
		LoadMoneyError error = loadMoneyValidation.agentLoadMoney(dto);
		if (error.isValid()) {
			if (sessionId != null && sessionId.length() != 0) {
				String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
				if (authority != null) {
					if (authority.contains(Authorities.AGENT) && authority.contains(Authorities.AUTHENTICATED)) {
						request.getSession().setAttribute("sessionId", sessionId);
						dto.setSessionId(sessionId);
						if (dto.isUseVnet()) {
							VNetRequest vNetRequest = new VNetRequest();
							vNetRequest.setSessionId(sessionId);
							vNetRequest.setReturnURL(UrlMetadatas.WEBURL + "/Agent/LoadMoney/VRedirect");
							vNetRequest.setAmount(dto.getAmount());
							VNetResponse vNetResponse = loadAgentMoneyApi.initiateVnetBanking(vNetRequest, Role.AGENT);
							if (vNetResponse.isSuccess()) {
								modelMap.addAttribute("vnet", vNetResponse);
								return "Agent/LoadMoney/VNetPay";
							}
						} else {
							dto.setReturnUrl(UrlMetadatas.WEBURL + "/Agent/LoadMoney/Redirect");
							loadMoneyResponse = loadAgentMoneyApi.loadMoneyRequest(dto, Role.AGENT);
							if (loadMoneyResponse.isSuccess()) {
								modelMap.addAttribute("loadmoney", loadMoneyResponse);
								return "Agent/LoadMoney/Pay";
							} else {
								/*System.err.format("Load money response is %s\n", loadMoneyResponse.getDescription());
								modelMap.addAttribute(ModelMapKey.MESSAGE, loadMoneyResponse.getDescription());*/
								return "Agent/Login/LoadMoney";
							}
						}
					}
				}
			}
		} else {
			modelMap.put(ModelMapKey.ERROR, error);
			return "Agent/Login/LoadMoney";
		}
		return "redirect:/Agent/Login/LoadMoney";
	}

	@RequestMapping(value = "/AgentProcess", method = RequestMethod.GET)
	public String processAgentLoadMoney(HttpServletRequest request, HttpSession session) {

		String sessionId = (String) session.getAttribute("sessionId");
		request.setAttribute("sessionId", sessionId);
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
			if (authority != null) {
				if (authority.contains(Authorities.AGENT) && authority.contains(Authorities.AUTHENTICATED)) {
					return "Agent/Login/LoadMoney";
				}
			}
		}
		return "redirect:/";
	}
	@RequestMapping(value = "/Redirect", method = RequestMethod.POST)
	public String redirectLoadMoney(WEBSRedirectResponse ebsResponse, Model model, HttpServletRequest request,
			HttpSession session) {
		EBSRedirectResponse ebsRedirectResponse = ConvertUtil.convertFromWEBS(ebsResponse);
		ResponseDTO result = loadAgentMoneyApi.verifyEBSTransaction(ebsRedirectResponse, Role.AGENT);
		if (result.isSuccess()) {
			ebsRedirectResponse.setSuccess(true);
			ebsRedirectResponse.setResponseCode("0");
		} else {
			ebsRedirectResponse.setSuccess(false);
			ebsRedirectResponse.setResponseCode("1");
		}
		EBSRedirectResponse redirectResponse = loadAgentMoneyApi.processRedirectSDK(ebsRedirectResponse, Role.AGENT);
		System.err.println("sessionId ::" + redirectResponse.getSessionId());
		session.setAttribute("sessionId", redirectResponse.getSessionId());
		if (redirectResponse.isSuccess() || redirectResponse.getResponseCode().equals("0")) {
			model.addAttribute("msg", "Transasction Successful,You've successfully loaded amount "
					+ redirectResponse.getAmount() + " in your wallet");
		} else {
			model.addAttribute("msg", "Transaction failed, Please try again later.");
		}
		model.addAttribute("loadmoneyResponse", redirectResponse);
		return "/Agent/Login/Loading";
	}

	@RequestMapping(value = "/AgentVRedirect")
	public String redirectAgentVNetLoadMoney(VRedirectResponse dto, Model model) {

		ResponseDTO responseDTO = loadAgentMoneyApi.handleRedirectRequest(dto, Role.AGENT);
		model.addAttribute("response", responseDTO);
		return "/Agent/Login/Loading";

	}

}
