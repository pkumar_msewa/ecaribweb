package com.payqwikweb.controller.web.user;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.payqwikweb.app.api.IRegistrationApi;
import com.payqwikweb.app.api.IRedeemPointsApi;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.request.MobileOTPRequest;
import com.payqwikweb.app.model.request.RedeemPointsRequest;
import com.payqwikweb.app.model.request.RegistrationRequest;
import com.payqwikweb.app.model.request.ResendMobileOTPRequest;
import com.payqwikweb.app.model.request.VerifyEmailRequest;
import com.payqwikweb.app.model.response.MobileOTPResponse;
import com.payqwikweb.app.model.response.QuestionResponse;
import com.payqwikweb.app.model.response.RedeemPointsResponse;
import com.payqwikweb.app.model.response.RegistrationResponse;
import com.payqwikweb.app.model.response.ResendMobileOTPResponse;
import com.payqwikweb.app.model.response.VerifyEmailResponse;
import com.payqwikweb.model.error.MobileOTPError;
import com.payqwikweb.model.error.RedeemPointsError;
import com.payqwikweb.util.APIUtils;
import com.payqwikweb.validation.MobileOTPValidation;
import com.payqwikweb.validation.RedeemPointsValidation;

@Controller
@RequestMapping(value = "/Api/{version}/{role}/{device}/{language}/RedeemPoints")
public class RedeemPointsController implements MessageSourceAware {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private MessageSource messageSource;
	private SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
    private final IRedeemPointsApi redeemPointApi;
    private final RedeemPointsValidation redeemPointsValidation;
	public RedeemPointsController(IRedeemPointsApi redeemPointApi,RedeemPointsValidation redeemPointsValidation) {
		this.redeemPointApi=redeemPointApi;
		this.redeemPointsValidation=redeemPointsValidation;
	}

	@Override
	public void setMessageSource(MessageSource arg0) {
		this.messageSource = messageSource;
	}

	@RequestMapping(value = "/Process", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<RedeemPointsResponse> processRegistration(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody RedeemPointsRequest dto,
			HttpServletRequest request) {

		RedeemPointsResponse result = new RedeemPointsResponse();
	//	RegisterError error = registerValidation.checkError(dto);
		RedeemPointsError error = redeemPointsValidation.checkPointsError(dto);
		if (error.isValid()) {
				if (role.equalsIgnoreCase(Role.USER.getValue())) {
					if (device.equalsIgnoreCase(Device.ANDROID.getValue())
							|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
							|| device.equalsIgnoreCase(Device.IOS.getValue())) {
						result = redeemPointApi.convertRedeemPointToCash(dto);
					} else {
						result.setSuccess(false);
						result.setCode("F00");
						result.setMessage("Unknown device");
						result.setStatus("FAILED");
						result.setResponse(APIUtils.getFailedJSON().toString());
					}
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Unauthorised access");
					result.setStatus("FAILED");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			} 
		 else {
			result.setCode("F00");
			result.setMessage(error.getMessage());
			result.setDetails(error);
	   }
		return new ResponseEntity<RedeemPointsResponse>(result, HttpStatus.OK);
	}
}