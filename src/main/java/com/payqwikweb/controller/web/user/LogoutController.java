package com.payqwikweb.controller.web.user;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.payqwikweb.api.IAuthenticationApi;
import com.payqwikweb.app.api.ILogoutApi;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.request.LogoutRequest;
import com.payqwikweb.app.model.response.LogoutResponse;
import com.payqwikweb.util.Authorities;
import com.payqwikweb.util.ModelMapKey;

@Controller
@RequestMapping("/")
public class LogoutController implements MessageSourceAware {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private MessageSource messageSource;

	private final ILogoutApi logoutApi;
	private final IAuthenticationApi authenticationApi;

	public LogoutController(ILogoutApi logoutApi, IAuthenticationApi authenticationApi) {
		this.logoutApi = logoutApi;
		this.authenticationApi = authenticationApi;
	}

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	@RequestMapping(method = RequestMethod.GET, value = "User/Logout")
	public String userLogout(HttpSession session, RedirectAttributes modelMap) {
		String sessionId = (String) session.getAttribute("sessionId");
		LogoutRequest logout = new LogoutRequest();
		logout.setSessionId(sessionId);
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
					LogoutResponse resp = logoutApi.logout(logout, Role.USER);
					modelMap.addFlashAttribute(ModelMapKey.MESSAGE, "You've successfully logged out");
					session.invalidate();
					return "redirect:/Home";
				}
			}
			session.setAttribute("Alogout","log");
		}
		return "redirect:/";

	}

	@RequestMapping(method = RequestMethod.POST, value = "Admin/Logout")
	public String adminLogout(@ModelAttribute("login") LogoutRequest dto, HttpSession session, ModelMap modelMap) {
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
					LogoutResponse resp = logoutApi.logout(dto, Role.ADMIN);
					modelMap.addAttribute("message", resp.getMessage());
					session.invalidate();
					return "redirect:/Admin/Home";
				}
			}
		}
		return "redirect:/Admin/Home";

	}

}
