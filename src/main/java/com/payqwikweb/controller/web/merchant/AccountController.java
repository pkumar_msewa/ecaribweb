package com.payqwikweb.controller.web.merchant;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.payqwikweb.api.IAuthenticationApi;
import com.payqwikweb.app.api.ISendMoneyApi;
import com.payqwikweb.app.api.ITransactionApi;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.request.SendMoneyBankRequest;
import com.payqwikweb.app.model.response.SendMoneyBankResponse;
import com.payqwikweb.model.error.SendMoneyBankError;
import com.payqwikweb.validation.SendMoneyValidation;

@Controller
@RequestMapping("/Api/{version}/{role}/{device}/{language}/SendMoney")
public class AccountController implements MessageSourceAware {
	
	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private MessageSource messageSource;

	private final ISendMoneyApi sendMoneyApi;
	private final IAuthenticationApi authenticationApi;
	private final SendMoneyValidation sendMoneyValidation;
	private final ITransactionApi transactionApi;

	public AccountController(ISendMoneyApi sendMoneyApi,IAuthenticationApi authenticationApi,
						SendMoneyValidation sendMoneyValidation,ITransactionApi transactionApi) {
		this.sendMoneyApi = sendMoneyApi;
		this.authenticationApi = authenticationApi;
		this.sendMoneyValidation = sendMoneyValidation;
		this.transactionApi = transactionApi;
	}

	@Override
	public void setMessageSource(MessageSource arg0) {
		this.messageSource = messageSource;
	}
	
	@RequestMapping(value = "/BankAccount", method = RequestMethod.POST)
	ResponseEntity<SendMoneyBankResponse> sendMoneyBankRequest(@PathVariable(value = "version") String version, @PathVariable(value = "role") String role,
            												   @PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
            												   @RequestBody SendMoneyBankRequest dto,@RequestHeader(value = "hash", required = false) String hash) {
		
		SendMoneyBankResponse result = new SendMoneyBankResponse();
		if(role.equalsIgnoreCase(Role.MERCHANT.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				
				SendMoneyBankError error = sendMoneyValidation.checkBankError(dto);
				if (error.isValid()) {
					result = sendMoneyApi.sendMoneyToMBankRequest(dto);
					return new ResponseEntity<SendMoneyBankResponse>(result, HttpStatus.OK);
				} else {
					result.setCode("F04");
					result.setMessage("Invalid Input");
					result.setDetails(error.toJSON().toString());
				} 
			}else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unknown device");
				result.setStatus("FAILED");
			}
		}else {
			result.setCode("F06");
			result.setMessage("Unauthorized role");
			result.setStatus("FAILED");
		}
		return new ResponseEntity<SendMoneyBankResponse>(result, HttpStatus.OK);
	}
	
}
