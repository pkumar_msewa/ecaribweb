package com.payqwikweb.controller.web.merchant;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import com.payqwikweb.api.IAuthenticationApi;
import com.payqwikweb.app.api.IMerchantApi;
import com.payqwikweb.app.api.IUserApi;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.merchant.MReportDTO;
import com.payqwikweb.app.model.request.AllTransactionRequest;
import com.payqwikweb.app.model.request.PagingDTO;
import com.payqwikweb.app.model.request.ReceiptsRequest;
import com.payqwikweb.app.model.request.RefundDTO;
import com.payqwikweb.app.model.request.SessionDTO;
import com.payqwikweb.app.model.request.TransactionFilter;
import com.payqwikweb.app.model.response.AllTransactionResponse;
import com.payqwikweb.app.model.response.MTransactionResponseDTO;
import com.payqwikweb.app.model.response.ReceiptsResponse;
import com.payqwikweb.app.model.response.UserDetailsResponse;
import com.payqwikweb.model.app.response.MerchantTransactionResponse;
import com.payqwikweb.model.app.response.TransactionUserResponse;
import com.payqwikweb.model.request.thirdpartyService.RequestRefund;
import com.payqwikweb.model.web.Status;
import com.payqwikweb.model.web.UserListDTO;
import com.payqwikweb.util.Authorities;
import com.payqwikweb.util.JSONParserUtil;
import com.thirdparty.model.ResponseDTO;

@Controller
@RequestMapping("/Merchant")
public class ReportController {

    private final IMerchantApi merchantApi;
    private final IAuthenticationApi authenticationApi;
    private final IUserApi userApi;

    private DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
    public ReportController(IMerchantApi merchantApi, IAuthenticationApi authenticationApi,IUserApi userApi) {
        this.merchantApi = merchantApi;
        this.authenticationApi = authenticationApi;
        this.userApi = userApi;
    }

//    @RequestMapping(value="/Transactions",method= RequestMethod.GET)
//    public String getAllTransactions(HttpServletRequest request, HttpServletResponse response, HttpSession session,Model model){
//        AllTransactionRequest dto = new AllTransactionRequest();
//        try {
//            String sessionId = (String) session.getAttribute("msession");
//            if (sessionId != null) {
//                UserDetailsResponse userDetailsResponse = authenticationApi.getUserDetailsFromSession(sessionId);
//                if (userDetailsResponse != null) {
//                    String authority = userDetailsResponse.getAuthority();
//                    if (authority.contains(Authorities.MERCHANT) && authority.contains(Authorities.AUTHENTICATED)) {
//                        List<MerchantTransactionResponse> transactionsList = new ArrayList<>();
//                        dto.setSessionId(sessionId);
//                        model.addAttribute("user", userDetailsResponse);
//                        AllTransactionResponse allTransactionResponse = merchantApi.getAllTransaction(dto);
//                        if (allTransactionResponse != null) {
//                            JSONArray jsonArray = allTransactionResponse.getJsonArray();
//                            if (jsonArray != null) {
//                                System.err.println("jsonArray is not null");
//                                for (int i = 0; i < jsonArray.length(); i++) {
//                                    JSONObject json = jsonArray.getJSONObject(i);
//                                    System.err.println("inside for loop of json object " + i);
//                                    MerchantTransactionResponse transactionResponse = new MerchantTransactionResponse();
//                                    System.err.println(JSONParserUtil.getDouble(json, "amount"));
//                                    transactionResponse.setAmount("" + JSONParserUtil.getDouble(json, "amount"));
//                                    transactionResponse.setTransactionRefNo(JSONParserUtil.getString(json, "orderId"));
//                                    System.err.println(JSONParserUtil.getDouble(json, "orderId"));
//                                    transactionResponse.setVpayqwikRefNo(JSONParserUtil.getString(json, "transactionRefNo"));
//                                    transactionResponse.setStatus(JSONParserUtil.getString(json, "status"));
//                                    long milliseconds = JSONParserUtil.getLong(json, "created");
//                                    Calendar calendar = Calendar.getInstance();
//                                    calendar.setTimeInMillis(milliseconds);
//                                    transactionResponse.setDateTime(dateFormat.format(calendar.getTime()));
//
//                                    JSONObject user = JSONParserUtil.getObject(json, "user");
//
//                                    if (user != null) {
//                                        JSONObject userDetail = JSONParserUtil.getObject(user, "userDetail");
//                                        if (userDetail != null) {
//                                            transactionResponse.setUsername(JSONParserUtil.getString(userDetail, "firstName") + " " + JSONParserUtil.getString(userDetail, "lastName"));
//                                            System.err.println(JSONParserUtil.getString(userDetail, "email"));
//                                            transactionResponse.setEmail(JSONParserUtil.getString(userDetail, "email"));
//                                            transactionResponse.setMobileNumber(JSONParserUtil.getString(userDetail, "contactNo"));
//                                        }
//                                    }
//                                    transactionsList.add(transactionResponse);
//                                }
//                            }
//                        }
//                        model.addAttribute("transactionList", transactionsList);
//                        return "Merchant/Transactions";
//                    }
//                }
//            }
//        }catch(Exception e){
//            e.printStackTrace();
//        }
//        return "redirect:/Merchant/Home";
//    }



/*    @RequestMapping(value = "/Transactions", method = RequestMethod.GET)
    public String getReceiptsOfMerchant(HttpSession session) {
//    	System.out.println("inside transactions get....");
//    	        ReceiptsResponse result = new ReceiptsResponse();
        String sessionId = (String) session.getAttribute("msession");
       if (sessionId != null && sessionId.length() != 0) {
           String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
          if (authority != null) {
              if (authority.contains(Authorities.MERCHANT) && authority.contains(Authorities.AUTHENTICATED)) {
//                   ReceiptsRequest dto = new ReceiptsRequest();
//
//                    dto.setSessionId(sessionId);
//                   dto.setPage(0);
//                    dto.setSize(100000);
//                    result = userApi.getMerchantReceipts(dto);
//                    List<MTransactionResponseDTO> transactionList = new ArrayList<>();
//                   if(result.isSuccess()) {
//                        try {
//                            JSONObject json = new JSONObject(result.getResponse());
//                            JSONObject details = JSONParserUtil.getObject(json,"details");
//                            JSONArray content = JSONParserUtil.getArray(details,"content");
//                            if(content != null) {
//                                for (int i=0 ; i < content.length(); i++) {
//                                	JSONObject temp = content.getJSONObject(i);
//                                
//                                    boolean debit = JSONParserUtil.getBoolean(temp,"debit");
//                                   if(debit) {
//                                        MTransactionResponseDTO transaction = new MTransactionResponseDTO();
//                                        long milliseconds = JSONParserUtil.getLong(temp, "date");
//                                        Calendar calendar = Calendar.getInstance();
//                                        calendar.setTimeInMillis(milliseconds);
//                                        transaction.setDate(dateFormat.format(calendar.getTime()));
//                                        transaction.setTransactionRefNo(JSONParserUtil.getString(temp, "transactionRefNo"));
//                                        transaction.setAmount(JSONParserUtil.getDouble(temp, "amount"));
//                                        transaction.setContactNo(JSONParserUtil.getString(temp, "contactNo"));
//                                        transaction.setEmail(JSONParserUtil.getString(temp, "email"));
//                                       transaction.setStatus(Status.valueOf(JSONParserUtil.getString(temp, "status")));
//                                        transactionList.add(transaction);
//                                    }
//                                }
//                                model.addAttribute("transactionList",transactionList);
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                       }
//                  }
//                    return "Merchant/Transactions";
            	  return "Merchant/Transactions";
            }
            }
    	 
    	   }
       return "redirect:/Merchant/Home";   
       }*/
@RequestMapping(method=RequestMethod.GET,value="/Transactions")
public String getTransactionListInJSON(@ModelAttribute PagingDTO dto,HttpSession session,ModelMap model){
	UserListDTO resultSet = new UserListDTO();
	ReceiptsResponse result = new ReceiptsResponse();
     String sessionId = (String) session.getAttribute("msession");
     if (sessionId != null && sessionId.length() != 0) {
         String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
         if (authority != null) {
             if (authority.contains(Authorities.MERCHANT) && authority.contains(Authorities.AUTHENTICATED)) {
                 ReceiptsRequest receipts = new ReceiptsRequest();
                 receipts.setSessionId(sessionId);
                 receipts.setPage(0);
                 receipts.setSize(100000);
                 result = userApi.getMerchantTransactions(receipts);
                 List<MReportDTO> transactionList = new ArrayList<>();
                 if(result.isSuccess()) {
                     try {
                         JSONObject json = new JSONObject(result.getResponse());
                         JSONObject details = JSONParserUtil.getObject(json,"details");
                         JSONArray content = JSONParserUtil.getArray(details,"content");
                         System.out.println("length=="+content.length());
                         if(content != null && content.length()>0) {
                             for (int i=0 ; i < content.length(); i++) {
                                 JSONObject temp = content.getJSONObject(i);
                                 boolean debit = JSONParserUtil.getBoolean(temp,"debit");
                                 if(true) {
                                	 MReportDTO transaction = new MReportDTO();
                                     long milliseconds = JSONParserUtil.getLong(temp, "date");
                                     Calendar calendar = Calendar.getInstance();
                                     calendar.setTimeInMillis(milliseconds);
                                     transaction.setDate(dateFormat.format(calendar.getTime()));
                                     transaction.setTransactionRefNo(JSONParserUtil.getString(temp, "transactionRefNo"));
                                     transaction.setAmount(JSONParserUtil.getDouble(temp, "amount"));
                                     transaction.setContactNo(JSONParserUtil.getString(temp, "contactNo"));
                                     transaction.setEmail(JSONParserUtil.getString(temp, "email"));
                                    //transaction.setTotalPages(totalPages);
                                     transaction.setStatus(Status.valueOf(JSONParserUtil.getString(temp, "status")));
                                     transaction.setOrderId(JSONParserUtil.getString(temp, "orderId"));
                                     transaction.setFirstName(JSONParserUtil.getString(temp, "firstName"));
                                     transactionList.add(transaction);
                                     model.addAttribute("transactionList",transactionList);
                                 }
                            }
//                             resultSet.setTotalPages(details.getLong("totalPages"));
//                             resultSet.setFirstPage(details.getBoolean("firstPage"));
//                             resultSet.setLastPage(details.getBoolean("lastPage"));
//                             resultSet.setNumberOfElements(details.getLong("numberOfElements"));
//                             resultSet.setJsonArray(transactionList);
                             //return new ResponseEntity<UserListDTO>(resultSet,HttpStatus.OK) ;
                         }
                         
                     } catch (JSONException e) {
                         e.printStackTrace();
                     }
                 }
                 return "Merchant/Transactions";
                
             }
         }
     }
     return "redirect:/Merchant/Home"; 
 }
    @RequestMapping(value="/MTransactions",method= RequestMethod.POST)
    ResponseEntity<AllTransactionResponse> getAllTransactions(SessionDTO sessionDTO){
        AllTransactionRequest dto = new AllTransactionRequest();
        AllTransactionResponse allTransactionResponse = new AllTransactionResponse();
        try {
            String sessionId = sessionDTO.getSessionId();
            if (sessionId != null) {
                UserDetailsResponse userDetailsResponse = authenticationApi.getUserDetailsFromSession(sessionId);
                if (userDetailsResponse != null) {
                    String authority = userDetailsResponse.getAuthority();
                    if (authority.contains(Authorities.MERCHANT) && authority.contains(Authorities.AUTHENTICATED)) {
                        List<MerchantTransactionResponse> transactionsList = new ArrayList<>();
                        dto.setSessionId(sessionId);
                        allTransactionResponse = merchantApi.getAllTransaction(dto);
                        return new ResponseEntity<AllTransactionResponse>(allTransactionResponse , HttpStatus.OK);
                    }
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return new ResponseEntity<AllTransactionResponse>(allTransactionResponse,HttpStatus.OK);
    }
    
    @RequestMapping(method=RequestMethod.POST,value="/TransactionFiltered")
    public String getTransactionList(@ModelAttribute TransactionFilter filter ,HttpServletRequest request,HttpServletResponse response,HttpSession session,Model model) throws ParseException  {
    	 List<MTransactionResponseDTO> transactionList = new ArrayList<>();
    	ReceiptsResponse result = new ReceiptsResponse();
         String sessionId = (String) session.getAttribute("msession");
         if (sessionId != null && sessionId.length() != 0) {
             String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
             if (authority != null) {
                 if (authority.contains(Authorities.MERCHANT) && authority.contains(Authorities.AUTHENTICATED)) {
                     ReceiptsRequest receipts = new ReceiptsRequest();
                     receipts.setSessionId(sessionId);
                     receipts.setPage(0);
                     receipts.setSize(100000);
                     receipts.setFromDate(filter.getFromDate());
                     receipts.setToDate(filter.getToDate());
                     result = userApi.getMerchantReceiptsFiltered(receipts);
                     if(result.isSuccess()) {
                         try {
                             JSONObject json = new JSONObject(result.getResponse());
                             JSONObject details = JSONParserUtil.getObject(json,"details");
                             JSONArray content = JSONParserUtil.getArray(details,"content");
                             if(content != null) {
                                 for (int i=0 ; i < content.length(); i++) {
                                     JSONObject temp = content.getJSONObject(i);
                                     boolean debit = JSONParserUtil.getBoolean(temp,"debit");
                                     long milliseconds = JSONParserUtil.getLong(temp,"date");
                                    	 Calendar calender=Calendar.getInstance();
                                    	 calender.setTimeInMillis(milliseconds);
                                    	 Date txdate=calender.getTime();
                                         MTransactionResponseDTO transaction = new MTransactionResponseDTO();
//                                           Calendar calendar = Calendar.getInstance();
//                                         calendar.setTimeInMillis(milliseconds);
                                         transaction.setDate(dateFormat.format(calender.getTime()));
                                         transaction.setTransactionRefNo(JSONParserUtil.getString(temp, "transactionRefNo"));
                                         transaction.setAmount(JSONParserUtil.getDouble(temp, "amount"));
                                         transaction.setContactNo(JSONParserUtil.getString(temp, "contactNo"));
                                         transaction.setEmail(JSONParserUtil.getString(temp, "email"));
                                         transaction.setOrderId(JSONParserUtil.getString(temp, "orderId"));
                                        //transaction.setTotalPages(totalPages);
                                         transaction.setStatus(Status.valueOf(JSONParserUtil.getString(temp, "status")));
                                         transactionList.add(transaction);
                                 }
                                 model.addAttribute("txList", transactionList);
                             }
                             return "Merchant/transactionsFiltered";
                         } catch (JSONException e) {
                             e.printStackTrace();
                         }
                     }
                 }
             }
         }
         return "redirect:/Merchant/Home" ;
     }
    
    @RequestMapping(method=RequestMethod.GET,value="/RequestRefund")
    public String getRequestRefund(HttpServletRequest request,HttpServletResponse response,HttpSession session,ModelMap model){
         String sessionId = (String) session.getAttribute("msession");
         if (sessionId != null && sessionId.length() != 0) {
             String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
             if (authority != null) {
                 if (authority.contains(Authorities.MERCHANT) && authority.contains(Authorities.AUTHENTICATED)) {
                         return "/Merchant/RequestRefund";
                     }
                    
             }
         }
         return "redirect:/Merchant/Home" ;
    }
    
    
    @RequestMapping(method=RequestMethod.POST,value="/RequestRefund")
    public String submitRequestRefund(@ModelAttribute RequestRefund filter ,HttpServletRequest request,HttpServletResponse response,HttpSession session,Model model) throws ParseException  {

    	String sessionId = (String) session.getAttribute("msession");
         if (sessionId != null && sessionId.length() != 0) {
             String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
             if (authority != null) {
                 if (authority.contains(Authorities.MERCHANT) && authority.contains(Authorities.AUTHENTICATED)) {
						String rootDirectory = request.getSession().getServletContext().getRealPath("/");
						String path=saveRefundReport(rootDirectory, filter.getFile());
						System.err.println(path);
						ResponseDTO responseDTO=merchantApi.requestRefund(sessionId,rootDirectory+path);
						
						model.addAttribute("message", responseDTO.getMessage());
						return "Merchant/RequestRefund";
                 }
             }
         }
                     return "redirect:/Merchant/Home" ;
     }
    
   
    private String saveRefundReport(String rootDirectory, MultipartFile image,String code) {
		String contentType = image.getContentType();
		String[] fileExtension = contentType.split("/");
		System.err.println("original filename::"+image.getOriginalFilename());
		String filePath = null;
			String fileName =String.valueOf(System.currentTimeMillis());
			File dirs = new File(rootDirectory + "/resources/refund/" + image.getOriginalFilename());
			dirs.mkdirs();
			try {
				image.transferTo(dirs);
				filePath = "/resources/refund/" + image.getOriginalFilename();
				return filePath;
			} catch (IOException e) {
				e.printStackTrace();

		}
		return filePath;
	}
	@RequestMapping(method = RequestMethod.POST, value = "/RefundMTransaction")
	ResponseEntity<TransactionUserResponse> refundTransactionBillPay(@RequestBody RefundDTO dto,HttpServletRequest request,
									HttpServletResponse response, HttpSession session, ModelMap model) {
		TransactionUserResponse result = new TransactionUserResponse();
		String sessionCheck = (String) session.getAttribute("msession");
		if (sessionCheck != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.MERCHANT) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionCheck);
					result = merchantApi.requestRefundTransaction(dto);
					}
				}
			}
		return new ResponseEntity<TransactionUserResponse>(result,HttpStatus.OK);
		}
	
    private String saveRefundReport(String rootDirectory, MultipartFile file) {
		String contentType = file.getContentType();
		String[] fileExtension = contentType.split("/");
		System.err.println("original filename::"+file.getOriginalFilename());
		String filePath = null;
			String fileName =String.valueOf(System.currentTimeMillis());

			File dirs = new File(rootDirectory + "/resources/refund/" + file.getOriginalFilename());
			dirs.mkdirs();
			try {
				file.transferTo(dirs);
				filePath = "/resources/refund/" + file.getOriginalFilename();
				return filePath;
			} catch (IOException e) {
				e.printStackTrace();

		}
		return filePath;
	}
    
}
