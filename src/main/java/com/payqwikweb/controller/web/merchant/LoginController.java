package com.payqwikweb.controller.web.merchant;

import com.payqwikweb.api.IAuthenticationApi;
import com.payqwikweb.app.api.ILogoutApi;
import com.payqwikweb.app.api.IMerchantApi;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.request.AllTransactionRequest;
import com.payqwikweb.app.model.request.LoginRequest;
import com.payqwikweb.app.model.request.LogoutRequest;
import com.payqwikweb.app.model.response.AllTransactionResponse;
import com.payqwikweb.app.model.response.LoginResponse;
import com.payqwikweb.app.model.response.LogoutResponse;
import com.payqwikweb.app.model.response.UserDetailsResponse;
import com.payqwikweb.model.app.request.UpdateReceiptDTO;
import com.payqwikweb.model.app.response.UpdateReceiptResponse;
import com.payqwikweb.model.error.LoginError;
import com.payqwikweb.util.Authorities;
import com.payqwikweb.util.ModelMapKey;
import com.payqwikweb.validation.LoginValidation;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/Merchant")
public class LoginController {
    private final IMerchantApi merchantApi;
    private final ILogoutApi logoutApi;
    private final LoginValidation loginValidation;
    private final IAuthenticationApi authenticationApi;
    public LoginController(IMerchantApi merchantApi, ILogoutApi logoutApi,LoginValidation loginValidation,IAuthenticationApi authenticationApi) {
        this.merchantApi = merchantApi;
        this.logoutApi = logoutApi;
        this.loginValidation = loginValidation;
        this.authenticationApi = authenticationApi;
    }
    
	
	@RequestMapping(value = "/WebProcess", method = RequestMethod.POST)
	ResponseEntity<LoginResponse> processLogin1(RedirectAttributes model,@ModelAttribute("login") LoginRequest dto, HttpServletRequest request, HttpSession session,
			HttpServletResponse response) {
		System.err.println("inside the login controller");
		LoginResponse result = new LoginResponse();
	    dto.setIpAddress(request.getRemoteAddr());
		result = merchantApi.login(dto);
		if (result.getCode().equals("S00")) {
		session.setAttribute("msession",result.getSessionId());
		session.setAttribute("balance", result.getBalance());
		session.setAttribute("firstName", result.getFirstName());
		session.setAttribute("userDetails", result);
		session.setAttribute("username", result.getUsername());
		session.setAttribute("emailId", result.getEmail());
		}
		return new ResponseEntity<LoginResponse>(result, HttpStatus.OK);
	}

    @RequestMapping(value="/Home",method= RequestMethod.GET)
    public String getMerchantLoginPage(@RequestParam(value="msg",required=false) String message, HttpSession session, Model model){
        LoginRequest dto  = new LoginRequest();
        model.addAttribute("login",dto);
        String sessionId = (String)session.getAttribute("msession");
        if (message != null) {
            model.addAttribute(ModelMapKey.MESSAGE, message);
        }
        if(sessionId != null) {
            UserDetailsResponse response = authenticationApi.getUserDetailsFromSession(sessionId);
            if(response != null) {
                String authority = response.getAuthority();
                if(authority != null) {
                    if (authority.contains(Authorities.MERCHANT) && authority.contains(Authorities.AUTHENTICATED)) {
                        model.addAttribute("user",response);
                        return "Merchant/Home";
                    }
                }
            }
        }
        return "Merchant/Login";
    }

    @RequestMapping(value="/Home",method= RequestMethod.POST)
    public String getMerchantLoginPage(@ModelAttribute("login") LoginRequest dto, HttpServletRequest request, HttpSession session, Model model){
        dto.setIpAddress(request.getRemoteAddr());
        LoginError error = loginValidation.checkMerchantLogin(dto);
        if(error.isValid()){
            LoginResponse loginResponse = merchantApi.login(dto);
            if(loginResponse.isSuccess()){
                        model.addAttribute("user",loginResponse);
                        session.setAttribute("msession",loginResponse.getSessionId());
                        return "Merchant/Home";
            }
            model.addAttribute(ModelMapKey.MESSAGE,loginResponse.getMessage());
        }
        model.addAttribute("error",error);
        return "Merchant/Login";
    }

    @RequestMapping(value="/Logout",method= RequestMethod.GET)
    public String logoutMerchant(HttpSession session,RedirectAttributes model){
        String sessionId = (String)session.getAttribute("msession");
        if(sessionId != null){
            String authority = authenticationApi.getAuthorityFromSession(sessionId,Role.USER);
            if(authority != null) {
                if (authority.contains(Authorities.MERCHANT) && authority.contains(Authorities.AUTHENTICATED)) {
                    LogoutRequest logoutRequest = new LogoutRequest();
                    logoutRequest.setSessionId(sessionId);
                    LogoutResponse logoutResponse = logoutApi.logout(logoutRequest, Role.MERCHANT);
                    if (logoutResponse.isSuccess()) {
                        model.addFlashAttribute(ModelMapKey.MESSAGE, "You've been successfully logged out");
                        session.invalidate();
                        return "redirect:/Home";
                    }
                }
            }
        }
        return "Merchant/Login";
    }

}
