package com.payqwikweb.controller.web;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.payqwikweb.api.IAuthenticationApi;
import com.payqwikweb.app.api.ICallBackApi;
import com.payqwikweb.app.metadatas.Test;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.response.FlightResponse;
import com.payqwikweb.app.model.response.IPayCallBack;
import com.payqwikweb.model.app.response.FlightResponsetr;
import com.payqwikweb.util.Authorities;
import com.payqwikweb.util.ModelMapKey;
import com.payqwikweb.util.SecurityUtil;

@Controller
public class MainController implements MessageSourceAware {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private MessageSource messageSource;

	private final IAuthenticationApi authenticationApi;
	private final ICallBackApi callBackApi;

	public MainController(IAuthenticationApi authenticationApi, ICallBackApi callBackApi) {
		this.authenticationApi = authenticationApi;
		this.callBackApi = callBackApi;
	}

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/")
	public String getHome(@RequestParam(value = ModelMapKey.MESSAGE, required = false) String msg,
			@RequestParam(value = ModelMapKey.ERROR, required = false) String error, RedirectAttributes modelMap,
			HttpSession session) {
		String sessionId = (String) session.getAttribute("sessionId");
		if (msg != null) {
			modelMap.addAttribute(ModelMapKey.MESSAGE, msg);
		}
		if (error != null) {
			modelMap.addAttribute(ModelMapKey.ERROR, error);
		}
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					return "redirect:/Admin/Home";
				} else if (authority.contains(Authorities.MERCHANT) && authority.contains(Authorities.AUTHENTICATED)) {
					return "redirect:/Merchant/Home";
				} else if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
					return "redirect:/User/Home";
				} else if (authority.contains(Authorities.AGENT) && authority.contains(Authorities.AUTHENTICATED)) {
					return "redirect:/Agent/Home";
				} else {
					return "Home";
				}
			}
		}
		return "Home";
	}

	@RequestMapping(value = "/IPayCall", method = RequestMethod.GET)
	ResponseEntity<String> processCallBack(IPayCallBack dto, HttpServletRequest request) {
		return new ResponseEntity<String>(HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/Flight")
	public String getFlightOneWay(HttpSession session) throws Exception {
		session.setAttribute("dataflight", "");
		session.setAttribute("dataflightreturn", "");
		String source = Test.readfile("b.txt");
          
		JSONObject obj = new JSONObject(source);

		JSONArray legs = obj.getJSONObject("Tickets").getJSONArray("Oneway");
		JSONArray legs1 = obj.getJSONObject("Tickets").getJSONArray("Roundway");
		
		ArrayList<FlightResponsetr> flightresponse = new ArrayList<FlightResponsetr>();
		ArrayList<FlightResponsetr> flightresponsearrreturn = new ArrayList<FlightResponsetr>();
		
		flightresponsearrreturn.clear();
		flightresponse.clear();
		for (int k = 0; k < legs.length(); k++) {
           System.out.println(k);
			FlightResponsetr flightresponseobj = new FlightResponsetr();
			flightresponseobj.setJourneyTime("" + legs.getJSONObject(k).getString("journeyTime"));
			flightresponseobj.setDepartureDate("" + legs.getJSONObject(k).getString("departureDate"));
			flightresponseobj.setFlightNumber("" + legs.getJSONObject(k).getString("flightNumber"));
			flightresponseobj.setArrivalDate("" + legs.getJSONObject(k).getString("arrivalDate"));
			flightresponseobj.setAirlineName("" + legs.getJSONObject(k).getString("airlineName"));
			flightresponseobj.setArrivalTime("" + legs.getJSONObject(k).getString("arrivalTime"));
			flightresponseobj.setCabin("" + legs.getJSONObject(k).getString("cabin"));
			flightresponseobj.setDepartureTime("" + legs.getJSONObject(k).getString("departureTime"));
			flightresponseobj.setDestination("" + legs.getJSONObject(k).getString("destination"));
			flightresponseobj.setDuration("" + legs.getJSONObject(k).getString("duration"));
			flightresponseobj.setFlightNumber("" + legs.getJSONObject(k).getString("flightNumber"));
			flightresponseobj.setOrigin("" + legs.getJSONObject(k).getString("origin"));
			flightresponseobj.setBaggageUnit("" + legs.getJSONObject(k).getString("baggageUnit"));
			flightresponseobj.setBaggageWeight("" + legs.getJSONObject(k).getString("baggageWeight"));
			flightresponse.add(flightresponseobj);
		}
		for (int k = 0; k < legs1.length(); k++) {
			System.err.println(k);
			FlightResponsetr flightresponseobj = new FlightResponsetr();
			flightresponseobj.setJourneyTimereturn("" + legs1.getJSONObject(k).getString("journeyTime"));
			flightresponseobj.setDepartureDatereturn("" + legs1.getJSONObject(k).getString("departureDate"));
			flightresponseobj.setFlightNumberreturn("" + legs1.getJSONObject(k).getString("flightNumber"));
			flightresponseobj.setArrivalDatereturn("" + legs1.getJSONObject(k).getString("arrivalDate"));
			flightresponseobj.setAirlineNamereturn("" + legs1.getJSONObject(k).getString("airlineName"));
			flightresponseobj.setArrivalTimereturn("" + legs1.getJSONObject(k).getString("arrivalTime"));
			flightresponseobj.setCabinreturn("" + legs1.getJSONObject(k).getString("cabin"));
			flightresponseobj.setDepartureTimereturn("" + legs1.getJSONObject(k).getString("departureTime"));
			flightresponseobj.setDestinationreturn("" + legs1.getJSONObject(k).getString("destination"));
			flightresponseobj.setDurationreturn("" + legs1.getJSONObject(k).getString("duration"));
			flightresponseobj.setFlightNumberreturn("" + legs1.getJSONObject(k).getString("flightNumber"));
			flightresponseobj.setOriginreturn("" + legs1.getJSONObject(k).getString("origin"));
			flightresponseobj.setBaggageUnitreturn("" + legs1.getJSONObject(k).getString("baggageUnit"));
			flightresponseobj.setBaggageWeightreturn("" + legs1.getJSONObject(k).getString("baggageWeight"));
			flightresponsearrreturn.add(flightresponseobj);
		}
		
		session.setAttribute("dataflight", flightresponse);
		session.setAttribute("dataflightreturn", flightresponsearrreturn);
		return "Flight";

	}

	@RequestMapping(method = RequestMethod.GET, value = "/Home")
	public String getHome(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = ModelMapKey.MESSAGE, required = false) String msg,
			@RequestParam(value = ModelMapKey.ERROR, required = false) String error, HttpSession session) {
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				return "redirect:/";
			}
		}
		if (msg != null) {
			modelMap.put(ModelMapKey.MESSAGE, msg);
		}
		if (error != null) {
			modelMap.put(ModelMapKey.ERROR, error);
		}
		session.setAttribute("isTxnIdCreated", false);
		
		return "Home";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/VerifyMobile")
	public String getVerifyMobile(ModelMap modelMap,
			@RequestParam(value = ModelMapKey.MESSAGE, required = false) String msg,
			@RequestParam(value = "mobileNumber", required = false) String mobileNumber) {
		if (msg != null) {
			modelMap.put(ModelMapKey.MESSAGE, msg);
		}
		if (mobileNumber != null) {
			modelMap.put("mobileNumber", mobileNumber);
		}
		return "VerifyMobile";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/Static/{staticPage}")
	public String getStatic(ModelMap map, @PathVariable("staticPage") String staticPage) {
		logger.debug("GET /Static/{}", staticPage);
		return "Static/" + staticPage;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/Static/{staticPage}")
	public String getStaticPost(@PathVariable("staticPage") String staticPage) {
		return "Static/" + staticPage;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{page}")
	public String getPage(@PathVariable("page") String page) {
		return page.toLowerCase();
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/Blogs/{page}")
	public String getBlogPages(@PathVariable("page") String page) {
		return "Blogs/"+page;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/{page}")
	public String getPagePost(@PathVariable("page") String page) {
		return page;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/Admin/{adminPage}")
	public String getAdmin(@PathVariable("adminPage") String adminPage, HttpSession session) {
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					return "Admin/" + adminPage;
				}
			}
		}
		return "redirect:/";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/Admin/{adminPage}")
	public String getAdminPost(ModelMap map, @PathVariable("adminPage") String adminPage, HttpSession session) {
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
					return "Admin/" + adminPage;
				}
			}
		}
		return "redirect:/";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/User/{mainPage}")
	public String getUser(ModelMap map, @PathVariable("mainPage") String mainPage,
			@RequestParam(value = "msg", required = false) String msg,
			@RequestParam(value = "menu", required = false) String menu, HttpSession session) {
		String msgval = "" + session.getAttribute("msgvalue");
		session.setAttribute("isTxnIdCreated", false);
		if (msgval.equals("1")) {

			session.setAttribute("msgvalue", "2");
		} else {
			session.setAttribute("msg", "");
		}
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
					map.put("name", (String) session.getAttribute("firstName"));
					map.put("balance", (double) session.getAttribute("balance"));
					return "User/" + mainPage;
				}
			}

		}
		
		return "redirect:/";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/User/{mainPage}")
	public String postUser(ModelMap map, @PathVariable("mainPage") String mainPage,
			@RequestParam(value = "msg", required = false) String msg,
			@RequestParam(value = "menu", required = false) String menu, HttpSession session) {

		String msgval = "" + session.getAttribute("msgvalue");
		if (msgval.equals("1")) {

			session.setAttribute("msgvalue", "2");
		} else {
			session.setAttribute("msg", "");
		}
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
					map.put("msg", msg);
					map.put("menu", menu);
					return "User/" + mainPage;
				}
			}
		}
		return "redirect:/";
	}
}
