package com.payqwikweb.controller.web;

import java.awt.Image;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import com.coupon.api.IPromoCodeApi;
import com.gcm.api.INotificationApi;
import com.letsManage.model.response.ResponseStatus;
import com.payqwikweb.api.IAuthenticationApi;
import com.payqwikweb.app.api.IAdminApi;
import com.payqwikweb.app.api.ILogoutApi;
import com.payqwikweb.app.api.IUserApi;
import com.payqwikweb.app.api.IVersionApi;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.request.MobileOTPRequest;
import com.payqwikweb.app.model.response.MobileOTPResponse;
import com.payqwikweb.app.model.response.RegistrationResponse;
import com.payqwikweb.model.app.request.AgentRegistrationRequest;
import com.payqwikweb.model.error.AgentError;
import com.payqwikweb.util.Authorities;
import com.payqwikweb.validation.AgentValidation;
import com.payqwikweb.validation.GCMValidation;
import com.payqwikweb.validation.PromoCodeValidation;
import com.payqwikweb.validation.RegisterValidation;

@Controller
@RequestMapping("/Admin")
public class AgentController {
	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private MessageSource messageSource;
	private final IAuthenticationApi authenticationApi;
	private final IAdminApi appAdminApi;
	private final ILogoutApi logoutApi;
	private final IUserApi userApi;
	private final RegisterValidation registerValidation;
	// private final IVerificationApi verificationApi;
	private final IVersionApi versionApi;
	private final PromoCodeValidation promoCodeValidation;
	private final INotificationApi notificationApi;
	private final GCMValidation gcmValidation;
	private final IPromoCodeApi promoCodeApi;
	private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
	private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	private SimpleDateFormat dateFilter = new SimpleDateFormat("yyyy-MM-dd HH:mm");

	public AgentController(IAuthenticationApi authenticationApi, IAdminApi appAdminApi, ILogoutApi logoutApi,
			IUserApi userApi, RegisterValidation registerValidation, IVersionApi versionApi,
			PromoCodeValidation promoCodeValidation, INotificationApi notificationApi, GCMValidation gcmValidation,
			IPromoCodeApi promoCodeApi) {

		this.authenticationApi = authenticationApi;
		this.appAdminApi = appAdminApi;
		this.logoutApi = logoutApi;
		this.userApi = userApi;
		this.registerValidation = registerValidation;
		this.versionApi = versionApi;
		this.promoCodeValidation = promoCodeValidation;
		this.notificationApi = notificationApi;
		this.gcmValidation = gcmValidation;
		this.promoCodeApi = promoCodeApi;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/AddAgent")
	public String getAddAgent(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			ModelMap model,@ModelAttribute("aId") String aId) {
		String sessionCheck = (String) session.getAttribute("adminSessionId");
		if (sessionCheck != null) {
			String adminAuthority = authenticationApi.getAuthorityFromSession(sessionCheck, Role.USER);
			if (adminAuthority != null) {
				if (adminAuthority.contains(Authorities.ADMINISTRATOR)
						&& adminAuthority.contains(Authorities.AUTHENTICATED)) {
					if (aId != null && !aId.isEmpty()) {
					appAdminApi.getAgentDetails(aId,sessionCheck);
					}
					model.addAttribute("agentId", aId);
					return "Admin/AddAgent";
				}
			}else{
				return "redirect:/Admin/Home";
			}
		}
		return "redirect:/Admin/Home";
	}

	@RequestMapping(value = "/SaveAgentDetail", method = RequestMethod.POST)
	String processRegistration(@ModelAttribute AgentRegistrationRequest dto, HttpServletRequest request,
			HttpSession session, ModelMap map) {
		RegistrationResponse result = new RegistrationResponse();
		String sessionId = (String) session.getAttribute("adminSessionId");
		if (sessionId != null) {
			dto.setSessionId(sessionId);
			AgentValidation valid = new AgentValidation();
			AgentError error = valid.checkError(dto);
			if (error.isValid()) {
				MultipartFile file = dto.getPanCardImg();
				boolean validImage = isValidImage(file);
				if (validImage) {
					result = appAdminApi.Agnetregister(dto);
					if (result.getCode().equalsIgnoreCase("F03")) {
						return "Admin/Login";
					} else {
						if (result.isSuccess()) {
							map.addAttribute("agentRespCode", ResponseStatus.SUCCESS.getValue());
							map.addAttribute("agentResp", result.getMessage());
						} else {
							map.addAttribute("agentRespCode", ResponseStatus.FAILURE.getValue());
							map.addAttribute("agentResp", result.getMessage());
						}
					}
				} else {
					map.addAttribute("agentRespCode", ResponseStatus.FAILURE.getValue());
					map.addAttribute("agentResp", "Enter valid Image");
				}
			} else {
				map.addAttribute("agentRespCode", ResponseStatus.BAD_REQUEST.getValue());
				map.addAttribute("agentResp", error.getMessage());
			}
		} else {
			map.addAttribute("bankRespCode", ResponseStatus.INVALID_SESSION.getValue());
			map.addAttribute("bankResp", "Session Expired");
			return "Admin/Login";
		}
		return "Admin/AddAgent";
	}
	private boolean isValidImage(MultipartFile file) {
		long length = 2 * 1024 * 1024;
		File imageFile = null;
		try {
			imageFile = convert(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
		boolean isValid = false;
		if (file.getContentType().contains("image")) {
			isValid = true;
		}
		if (file.getSize() <= length) {
			try {
				Image image = ImageIO.read(imageFile);

				if (image == null) {
				} else {
					isValid = true;
				}
			} catch (IOException ex) {
			}
		}
		return isValid;
	}
	private File convert(MultipartFile file) throws IOException {
		File convFile = new File(file.getOriginalFilename());
		convFile.createNewFile();
		FileOutputStream fos = new FileOutputStream(convFile);
		fos.write(file.getBytes());
		fos.close();
		return convFile;
	}
	
	
	@RequestMapping(value = "/GetCityAndState", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<RegistrationResponse> getCityAndState(@RequestBody AgentRegistrationRequest dto,
			HttpServletRequest request,HttpSession session) {

		RegistrationResponse result = new RegistrationResponse();
			
		String sessionId = (String)session.getAttribute("adminSessionId");
	 if(sessionId!=null){
		dto.setSessionId(sessionId);
		AgentValidation valid = new AgentValidation();
		AgentError error = valid.checkPincode(dto);
		 if(error.isValid()){
			result = appAdminApi.getCityAndState(dto);
		 }
	   	else{
			result.setCode(ResponseStatus.BAD_REQUEST.getValue());
			result.setMessage(error.getMessage());
		  }
	   }else{
		   result.setCode(ResponseStatus.INVALID_SESSION.getValue());
	   }
		return new ResponseEntity<RegistrationResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/Agent/BankList", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<RegistrationResponse> getBankList(@RequestBody AgentRegistrationRequest dto,
			HttpServletRequest request,HttpSession session) {

		RegistrationResponse result = new RegistrationResponse();
			
		String sessionId = (String)session.getAttribute("adminSessionId");
	 if(sessionId!=null){
		dto.setSessionId(sessionId);
			result = appAdminApi.getBankList(sessionId);
	   }
	   else{
		   result.setCode(ResponseStatus.INVALID_SESSION.getValue());
	   }
		return new ResponseEntity<RegistrationResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/Agent/GetBankIfscCode", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<RegistrationResponse> getBankIfscCode(@RequestBody AgentRegistrationRequest dto,
			HttpServletRequest request,HttpSession session) {

		RegistrationResponse result = new RegistrationResponse();
			
		String sessionId = (String)session.getAttribute("adminSessionId");
		dto.setSessionId(sessionId);
	 if(sessionId!=null){
		dto.setSessionId(sessionId);
			result = appAdminApi.getBankIfscCode(dto);
	   }
	   else{
		   result.setCode(ResponseStatus.INVALID_SESSION.getValue());
	   }
		return new ResponseEntity<RegistrationResponse>(result, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/AgentMobileOTP", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<MobileOTPResponse> mobileOTP(@RequestBody MobileOTPRequest dto,HttpSession session) {
		MobileOTPResponse result = new MobileOTPResponse();

		String sessionId = (String)session.getAttribute("adminSessionId");
		dto.setSessionId(sessionId);
		if(sessionId!=null){
			result = appAdminApi.agnetMobileOTP(dto);
			System.out.println("result=="+result);
		}
		else{
		  result.setCode(ResponseStatus.INVALID_SESSION.getValue());
		   }
		return new ResponseEntity<MobileOTPResponse>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value="/AgentResendOTP", method=RequestMethod.POST,produces={
		MediaType.APPLICATION_JSON_VALUE }, consumes={MediaType.APPLICATION_JSON_VALUE})
	 ResponseEntity<MobileOTPResponse>ResendMobileOTP(@RequestBody MobileOTPRequest dto,HttpSession session){
		MobileOTPResponse result = new MobileOTPResponse();
		String sessionId = (String)session.getAttribute("adminSessionId");
		if(sessionId!=null){
			dto.setSessionId(sessionId);
			result = appAdminApi.agentResendOTP(dto);
		}
		else{
			 result.setCode(ResponseStatus.INVALID_SESSION.getValue());
		}
		return new ResponseEntity<MobileOTPResponse>(result,HttpStatus.OK);
		
	}
}
