package com.payqwikweb.controller.web;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.recaptcha.api.IVerificationApi;
import com.google.recaptcha.model.JCaptchaRequest;
import com.google.recaptcha.model.JCaptchaResponse;
import com.letsManage.model.response.ResponseStatus;
import com.payqwikweb.api.IAuthenticationApi;
import com.payqwikweb.app.api.IAgentApi;
import com.payqwikweb.app.api.ILoginApi;
import com.payqwikweb.app.api.ILogoutApi;
import com.payqwikweb.app.api.IRegistrationApi;
import com.payqwikweb.app.api.IUserApi;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.request.LoginRequest;
import com.payqwikweb.app.model.request.LogoutRequest;
import com.payqwikweb.app.model.request.MobileOTPRequest;
import com.payqwikweb.app.model.request.RegistrationRequest;
import com.payqwikweb.app.model.request.ResendMobileOTPRequest;
import com.payqwikweb.app.model.request.UserDetailsRequest;
import com.payqwikweb.app.model.response.LoginResponse;
import com.payqwikweb.app.model.response.LogoutResponse;
import com.payqwikweb.app.model.response.MobileOTPResponse;
import com.payqwikweb.app.model.response.RegistrationResponse;
import com.payqwikweb.app.model.response.ResendMobileOTPResponse;
import com.payqwikweb.app.model.response.UserDetailsResponse;
import com.payqwikweb.model.app.request.AgentRegistrationRequest;
import com.payqwikweb.model.error.MobileOTPError;
import com.payqwikweb.model.error.RegisterError;
import com.payqwikweb.util.APIUtils;
import com.payqwikweb.util.Authorities;
import com.payqwikweb.util.ModelMapKey;
import com.payqwikweb.validation.CommonValidation;
import com.payqwikweb.validation.MobileOTPValidation;
import com.payqwikweb.validation.RegisterValidation;

@Controller
@RequestMapping("/Agent")
public class AgentInterfaceGui {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private MessageSource messageSource;

	private final ILoginApi appLoginApi;
	private final IUserApi userApi;
	private final IAgentApi agentApi;
	private final IAuthenticationApi authenticationApi;
//	private final IVerificationApi verificationApi;
	private final RegisterValidation registerValidation;
	private final MobileOTPValidation mobileOTPValidation;
	private final ILogoutApi logoutApi;

	public AgentInterfaceGui(ILoginApi appLoginApi, IUserApi userApi, IAgentApi agentApi,
			IAuthenticationApi authenticationApi,RegisterValidation registerValidation,
			 MobileOTPValidation mobileOTPValidation,ILogoutApi logoutApi) {
		this.appLoginApi = appLoginApi;
		this.userApi = userApi;
		this.agentApi = agentApi;
		this.authenticationApi = authenticationApi;
//		this.verificationApi = verificationApi;
		this.registerValidation = registerValidation;
		this.mobileOTPValidation = mobileOTPValidation;
		this.logoutApi=logoutApi;

	}

	@RequestMapping(method = RequestMethod.POST, value = "/Home/Login")
	public String processAgentLoginRequest(RedirectAttributes model, HttpServletRequest request,
			@ModelAttribute("login") LoginRequest loginDTO, HttpSession session, ModelMap map) {
		if (true) {
			String property = "X-Forwarded-For";
			String ipAddress = request.getHeader(property);
			if(ipAddress == null) {
				System.err.println("Original IP is NULL");
				ipAddress = "N/A";
			}
			loginDTO.setIpAddress(ipAddress);
			LoginResponse resp = agentApi.login(loginDTO, Role.AGENT);
			if (resp.getCode() != null) {
				if (resp.getCode().equals("S00")) {
					session.setAttribute("sessionId", resp.getSessionId());
					session.setAttribute("balance", resp.getBalance());
					session.setAttribute("firstName", resp.getFirstName());
					session.setAttribute("userDetails", resp);
					session.setAttribute("username", resp.getUsername());
					session.setAttribute("emailId", resp.getEmail());
					session.setAttribute("userRegister", resp.getTotalRegisterUser());
					session.setAttribute("debitAmount", resp.getDebitAmount());
					return "/Agent/Login/Home";
				} else if (resp.getCode().equals("F05")) {
					System.err.println(resp.getMessage());
					map.addAttribute(ModelMapKey.ERROR, resp.getMessage());
					return "/Home";
				} else if(resp.getCode().equals("L01")) {
					System.err.println(resp.getMessage());
					map.addAttribute(ModelMapKey.MESSAGE,resp.getMessage());
					map.addAttribute("loginDTO",loginDTO);
					return "Agent/VerifyMobile";
				}
			}
		} else {
			model.addFlashAttribute(ModelMapKey.ERROR, "Invalid Captcha");
			return "/Agent/Home";
		}
		return "/Home";
	}
			
	@RequestMapping(method = RequestMethod.POST, value = "/Login")
	public String processLoginRequest(RedirectAttributes model, HttpServletRequest request,
			@ModelAttribute("login") LoginRequest loginDTO, HttpSession session, ModelMap map) {
		if (true) {
			String property = "X-Forwarded-For";
			String ipAddress = request.getHeader(property);
			if(ipAddress == null) {
				System.err.println("Original IP is NULL");
				ipAddress = "N/A";
			}
			loginDTO.setIpAddress(ipAddress);
			LoginResponse resp = agentApi.login(loginDTO, Role.AGENT);
			if (resp.getCode() != null) {
				if (resp.getCode().equals("S00")) {
					session.setAttribute("sessionId", resp.getSessionId());
					session.setAttribute("balance", resp.getBalance());
					session.setAttribute("firstName", resp.getFirstName());
					session.setAttribute("userDetails", resp);
					session.setAttribute("username", resp.getUsername());
					session.setAttribute("emailId", resp.getEmail());
					session.setAttribute("userRegister", resp.getTotalRegisterUser());
					session.setAttribute("debitAmount", resp.getDebitAmount());
					return "/Agent/Login/Home";
				} else if (resp.getCode().equals("F05")) {
					System.err.println(resp.getMessage());
					map.addAttribute(ModelMapKey.ERROR, resp.getMessage());
					return "/Agent/Home";
				} else if(resp.getCode().equals("L01")) {
					System.err.println(resp.getMessage());
					map.addAttribute(ModelMapKey.MESSAGE,resp.getMessage());
					map.addAttribute("loginDTO",loginDTO);
					return "Agent/VerifyMobile";
				}
			}
		} else {
			model.addFlashAttribute(ModelMapKey.ERROR, "Invalid Captcha");
			return "/Agent/Home";
		}
		return "/Agent/Home";
	}
	
	
	@RequestMapping(method = RequestMethod.POST, value = "/Login/VerifyMobile")
	public String getHome(RedirectAttributes model, HttpServletRequest request,
						  @ModelAttribute("loginDTO") LoginRequest loginDTO, HttpSession session,ModelMap map) {
		try{
		if (true) {
			loginDTO.setValidate(true);
            String property = "X-Forwarded-For";
            String ipAddress = request.getHeader(property);
            if(ipAddress == null) {
                System.err.println("Original IP is NULL");
                ipAddress = "N/A";
            }
            loginDTO.setIpAddress(ipAddress);
			LoginResponse resp = agentApi.login(loginDTO, Role.AGENT);
			System.err.println("response ::" + resp.getCode());
			if (resp.getCode() != null) {
				if (resp.getCode().equals("S00")) {
					session.setAttribute("sessionId", resp.getSessionId());
					session.setAttribute("balance", resp.getBalance());
					session.setAttribute("firstName", resp.getFirstName());
					session.setAttribute("userDetails", resp);
					session.setAttribute("username", resp.getUsername());
					session.setAttribute("emailId", resp.getEmail());
					session.setAttribute("userRegister", resp.getTotalRegisterUser());
					session.setAttribute("debitAmount", resp.getDebitAmount());
					return "/Agent/Login/Home";
				} else if (resp.getCode().equals("F05")) {
					model.addFlashAttribute(ModelMapKey.ERROR, resp.getMessage());
					return "redirect:/Home";
				} else  {
					System.err.println(resp.getMessage());
					map.addAttribute(ModelMapKey.MESSAGE,resp.getMessage());
					map.addAttribute("loginDTO",loginDTO);
					return "Agent/VerifyMobile";
				}
			}
		} else {
			model.addFlashAttribute(ModelMapKey.ERROR, "Invalid Captcha");
			return "redirect:/Home";
		}
	  }catch(Exception e){ 
		  e.printStackTrace();
		  
	  }
		return "redirect:/Home";
	}

	@RequestMapping(value = "/GetAgentDetails", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<UserDetailsResponse> getUserDetails(@ModelAttribute("userDetails") UserDetailsRequest dto,
			HttpSession session,ModelMap model) {
		UserDetailsResponse result = new UserDetailsResponse();
		List<UserDetailsResponse> list = new ArrayList<UserDetailsResponse>();
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
			if (authority != null) {
				if (authority.contains(Authorities.AGENT) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionId);
					result = agentApi.getAgentDetails(dto, Role.AGENT);
					JSONObject obj = new JSONObject(result);
					try {
						session.setAttribute("userregister", result.getTotalRegisterUser());
						session.setAttribute("debitAmount", result.getDebitAmount());
						String usertype = obj.getString("userType");
						// System.out.println(usertype);
						if (usertype.equalsIgnoreCase("KYC")) {
							result.setFlag(true);
						} else {
							result.setFlag(false);
						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					dto.setSessionId(sessionId);
					model.addAttribute("debitAmount", result.getDebitAmount());
					return new ResponseEntity<UserDetailsResponse>(result, HttpStatus.OK);
				}
			}
		}
		return new ResponseEntity<UserDetailsResponse>(result, HttpStatus.OK);
	}

	/* Get method Page in jsp */

	@RequestMapping(method = RequestMethod.GET, value = "/Login")
	public String processLoginGet(RedirectAttributes model, HttpServletRequest request,
			@ModelAttribute("login") LoginRequest loginDTO, HttpSession session, ModelMap map) {
		String sessionId = (String) session.getAttribute("sessionId");
		String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
		if (authority != null) {

			return "/Agent/Login/Home";
		} else {

			return "/Agent/Home";
		}

	}

	@RequestMapping(method = RequestMethod.GET, value = "/Home")
	public String AgnetLogin(RedirectAttributes model, HttpServletRequest request, LoginRequest loginDTO,
			HttpSession session, ModelMap map) {

		String sessionId = (String) session.getAttribute("sessionId");
		String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
		if (authority != null) {
			
			return "/Agent/Login/Home";
		} else {

			return "/Agent/Home";
		}

	}

	@RequestMapping(method = RequestMethod.GET, value = "/Settings")
	public String AgnetSetting(RedirectAttributes model, HttpServletRequest request, LoginRequest loginDTO,
			HttpSession session, ModelMap map) {

		String sessionId = (String) session.getAttribute("sessionId");
		String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
		if (authority != null) {

			return "/Agent/Login/Settings";
		} else {

			return "/Agent/Home";
		}

	}

	@RequestMapping(method = RequestMethod.GET, value = "/RequestMoneyToAdmin")
	public String AgnetrequestLoadmoney(RedirectAttributes model, HttpServletRequest request, LoginRequest loginDTO,
			HttpSession session, ModelMap map) {

		String sessionId = (String) session.getAttribute("sessionId");
		String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
		if (authority != null) {

			return "/Agent/Login/RequestMoneyToAdmin";
		} else {

			return "/Agent/Home";
		}

	}

	/*
	 * @RequestMapping(method = RequestMethod.GET, value = "/BillPayment")
	 * public String AgnetBillpayment(RedirectAttributes model,
	 * HttpServletRequest request, LoginRequest loginDTO, HttpSession session,
	 * ModelMap map) {
	 * 
	 * String sessionId = (String) session.getAttribute("sessionId"); String
	 * authority = authenticationApi.getAuthorityFromSession(sessionId,
	 * Role.AGENT); if (authority != null) {
	 * 
	 * return "/Agent/Login/BillPayment"; } else {
	 * 
	 * return "/Agent/Home"; }
	 * 
	 * }
	 */

	@RequestMapping(method = RequestMethod.GET, value = "/DTH")
	public String AgnetDTH(RedirectAttributes model, HttpServletRequest request, LoginRequest loginDTO,
			HttpSession session, ModelMap map) {

		String sessionId = (String) session.getAttribute("sessionId");
		String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
		if (authority != null) {

			return "/Agent/Login/DTH";
		} else {

			return "/Agent/Home";
		}

	}

	@RequestMapping(method = RequestMethod.GET, value = "/landline")
	public String Agnetlandline(RedirectAttributes model, HttpServletRequest request, LoginRequest loginDTO,
			HttpSession session, ModelMap map) {

		String sessionId = (String) session.getAttribute("sessionId");
		String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
		if (authority != null) {

			return "/Agent/Login/landline";
		} else {

			return "/Agent/Home";
		}

	}

	@RequestMapping(method = RequestMethod.GET, value = "/Electricity")
	public String AgnetElectricity(RedirectAttributes model, HttpServletRequest request, LoginRequest loginDTO,
			HttpSession session, ModelMap map) {

		String sessionId = (String) session.getAttribute("sessionId");
		String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
		if (authority != null) {

			return "/Agent/Login/Electricity";
		} else {

			return "/Agent/Home";
		}

	}

	@RequestMapping(method = RequestMethod.GET, value = "/Gas")
	public String AgnetGas(RedirectAttributes model, HttpServletRequest request, LoginRequest loginDTO,
			HttpSession session, ModelMap map) {

		String sessionId = (String) session.getAttribute("sessionId");
		String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
		if (authority != null) {

			return "/Agent/Login/Gas";
		} else {

			return "/Agent/Home";
		}

	}

	@RequestMapping(method = RequestMethod.GET, value = "/Insurance")
	public String AgnetInsuranc(RedirectAttributes model, HttpServletRequest request, LoginRequest loginDTO,
			HttpSession session, ModelMap map) {

		String sessionId = (String) session.getAttribute("sessionId");
		String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
		if (authority != null) {

			return "/Agent/Login/Insuranc";
		} else {

			return "/Agent/Home";
		}

	}

	@RequestMapping(method = RequestMethod.GET, value = "/RegisterUser")
	public String RegisterUser(RedirectAttributes model, HttpServletRequest request, LoginRequest loginDTO,
			HttpSession session, ModelMap map) {
		String sessionId = (String) session.getAttribute("sessionId");
		String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
		if (authority != null) {

			return "/Agent/Login/registernewuser";
		} else {

			return "/Agent/Home";
		}

	}

	/*
	 * @RequestMapping(method = RequestMethod.GET, value = "/BillPayment")
	 * public String AgnetBillpayment(RedirectAttributes model,
	 * HttpServletRequest request, LoginRequest loginDTO, HttpSession session,
	 * ModelMap map) {
	 * 
	 * String sessionId = (String) session.getAttribute("sessionId"); if
	 * (sessionId != null && sessionId.length() != 0) { return
	 * "/Agent/Login/BillPayment"; } else {
	 * 
	 * return "/Agent/Home"; }
	 * 
	 * }
	 */

	@RequestMapping(method = RequestMethod.GET, value = "/Prepaid")
	public String AgnetTopup(RedirectAttributes model, HttpServletRequest request, LoginRequest loginDTO,
			HttpSession session, ModelMap map) {

		String sessionId = (String) session.getAttribute("sessionId");
		String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
		if (authority != null) {

			return "/Agent/Login/Prepaid";
		} else {

			return "/Agent/Home";
		}

	}

	@RequestMapping(method = RequestMethod.GET, value = "/Postpaid")
	public String AgnetPostpaid(RedirectAttributes model, HttpServletRequest request, LoginRequest loginDTO,
			HttpSession session, ModelMap map) {

		String sessionId = (String) session.getAttribute("sessionId");
		String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
		if (authority != null) {

			return "/Agent/Login/Postpaid";
		} else {

			return "/Agent/Home";
		}

	}

	@RequestMapping(method = RequestMethod.GET, value = "/DataCard")
	public String AgnetDataCard(RedirectAttributes model, HttpServletRequest request, LoginRequest loginDTO,
			HttpSession session, ModelMap map) {

		String sessionId = (String) session.getAttribute("sessionId");
		String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
		if (authority != null) {

			return "/Agent/Login/DataCard";
		} else {

			return "/Agent/Home";
		}

	}

	@RequestMapping(method = RequestMethod.GET, value = "/SendMoney")
	public String AgnetSendMoney(RedirectAttributes model, HttpServletRequest request, LoginRequest loginDTO,
			HttpSession session, ModelMap map) {

		String sessionId = (String) session.getAttribute("sessionId");
		String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
		if (authority != null) {

			return "/Agent/Login/SendMoney";
		} else {

			return "/Agent/Home";
		}

	}

	@RequestMapping(method = RequestMethod.GET,value="/SendMoney/Bank")
	public String AgentSendMoneyToBank(HttpServletRequest request,HttpSession session){
		String sessionId = (String)session.getAttribute("sessionId");
		String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
		if(authority!=null){
			return "/Agent/Login/AgentBankTransfer";
		}
		return "/Agent/Home";
	}
	@RequestMapping(value = "/BankList", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<RegistrationResponse> getBankList(@RequestBody AgentRegistrationRequest dto,
			HttpServletRequest request,HttpSession session) {

		RegistrationResponse result = new RegistrationResponse();
			
		String sessionId = (String)session.getAttribute("sessionId");
	 if(sessionId!=null){
		dto.setSessionId(sessionId);
			result = agentApi.getBankList(sessionId);
	   }
	   else{
		   result.setCode(ResponseStatus.INVALID_SESSION.getValue());
	   }
		return new ResponseEntity<RegistrationResponse>(result, HttpStatus.OK);
	}
	@RequestMapping(method = RequestMethod.GET, value = "/LoadMoney/Process")
	public String AgnetLoadmoney(RedirectAttributes model, HttpServletRequest request, LoginRequest loginDTO,
			HttpSession session, ModelMap map) {

		String sessionId = (String) session.getAttribute("sessionId");
		String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
		if (authority != null) {
			return "/Agent/Login/LoadMoney";
		} else {
			return "/Agent/Home";
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "/Receipts")
	public String AgnetReceipts(RedirectAttributes model, HttpServletRequest request, LoginRequest loginDTO,
			HttpSession session, ModelMap map) {

		String sessionId = (String) session.getAttribute("sessionId");
		String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
		if (authority != null) {

			return "Agent/Login/Receipts";
		} else {

			return "/Agent/Home";
		}

	}

	@RequestMapping(value = "/SaveUserToAgent", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<RegistrationResponse> SaveUserToAgent(@RequestBody RegistrationRequest dto,
			HttpServletRequest request, HttpSession session) {
		RegistrationResponse result = new RegistrationResponse();
		String sessionId = (String) session.getAttribute("sessionId");
		String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
		if (authority != null) {

			dto.setSessionId(sessionId);

			/*JCaptchaRequest cap = new JCaptchaRequest();
			cap.setSessionId(request.getSession().getId());
			cap.setCaptchaResponse(dto.getCaptchaResponse());
			JCaptchaResponse jCaptchaResponse = verificationApi.isValidJCaptcha(cap);
			if((jCaptchaResponse.isValid()))
			{*/
				RegisterError error = registerValidation.checkErrorAgenttoUser(dto);
				if(error.isValid())
				{
					result = agentApi.SaveUserToAgent(dto, Role.AGENT);
				} else {
					result.setCode("F09");
					result.setMessage(error.getMessage());
				}

			/*} else {
				result.setCode("F02");
				result.setMessage("Invalid Captcha");
			}*/
		} else {
			result.setCode("F00");
			result.setMessage("Session Invalid");
		}

		return new ResponseEntity<RegistrationResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/SaveUsertoAgentMobileOTP", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<MobileOTPResponse> SaveUsertoAgentMobileOTP(@RequestBody MobileOTPRequest dto, HttpSession session) {
		MobileOTPResponse result = new MobileOTPResponse();

		String sessionId = (String) session.getAttribute("sessionId");

		String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
		if (authority != null) {
			dto.setSessionId(sessionId);
			result = agentApi.SaveUsertoAgentMobileOTP(dto, Role.AGENT);
		} else {
			result.setCode("F00");
			result.setMessage("Session Invalid");
		}

		return new ResponseEntity<MobileOTPResponse>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/AgentResendMobileOTP", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResendMobileOTPResponse> resendMobileOTP(String language, @RequestBody ResendMobileOTPRequest dto,
			HttpServletRequest request,HttpSession session) {
		ResendMobileOTPResponse result = new ResendMobileOTPResponse();
		JCaptchaRequest cap = new JCaptchaRequest();
		String sessionId = (String) session.getAttribute("sessionId");
		dto.setSessionId(sessionId);
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
			if (authority != null) {
				if (authority.contains(Authorities.AGENT) && authority.contains(Authorities.AUTHENTICATED)){
						/*cap.setSessionId(request.getSession().getId());
						cap.setCaptchaResponse(dto.getCaptchaResponse());
						JCaptchaResponse jCaptchaResponse = verificationApi.isValidJCaptcha(cap);
					if(jCaptchaResponse.isValid()){*/
						MobileOTPError error = mobileOTPValidation.checkError(dto);
						if (error.isValid()) 
						{
							result = agentApi.resendUsertoAgentMobileOTP(dto, Role.AGENT);
						} else {
							result.setCode(ResponseStatus.BAD_REQUEST.getKey());
							result.setMessage("Invalid Input");
						}
					/*}else{
						result.setCode(ResponseStatus.FAILURE.getKey());
						result.setMessage("Invalid captcha");
					}*/
				}
				else{
					result.setCode(ResponseStatus.UNAUTHORIZED_ROLE.getKey());
					result.setMessage("Unauthorized User");
				}	
			 }else{
					result.setCode(ResponseStatus.UNAUTHORIZED_USER.getKey());
					result.setMessage("Unauthorized User");
				}
		  }
		else{
			result.setCode(ResponseStatus.INVALID_SESSION.getKey());
			result.setMessage("Session Invalid");
		}
		return new ResponseEntity<ResendMobileOTPResponse>(result, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/Logout")
	public String agentLogout(HttpSession session,RedirectAttributes model) {
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null && sessionId.length() != 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
			if (authority != null) {
				if (authority.contains(Authorities.AGENT) && authority.contains(Authorities.AUTHENTICATED)) {
					LogoutRequest dto = new LogoutRequest();
					dto.setSessionId(sessionId);
					LogoutResponse resp = logoutApi.logout(dto, Role.AGENT);
					if (resp.isSuccess()) {
                        model.addFlashAttribute(ModelMapKey.MESSAGE, "You've been successfully logged out");
                        session.invalidate();
                        return "redirect:/Home";
					}
				}
			}
		}
		return "/Agent/Home";

	}

}
