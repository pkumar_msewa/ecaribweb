package com.payqwikweb.controller.web.thirdparty;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.payqwikweb.api.IAuthenticationApi;
import com.payqwikweb.api.IEventHighApi;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.model.app.request.EventRequest;
import com.payqwikweb.model.app.response.EventHighResponse;
import com.payqwikweb.model.app.response.EventOccurence;
import com.payqwikweb.model.app.response.EventPrice;
import com.payqwikweb.model.app.response.EventUpperClass;
import com.payqwikweb.model.app.response.GCIResponse;
import com.payqwikweb.util.APIUtils;
import com.payqwikweb.util.Authorities;

@Controller
@RequestMapping("/User/Eventhigh")
public class EventHighController implements MessageSourceAware {

	private final IAuthenticationApi authenticationApi;
	private IEventHighApi eventhighApi;

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	private MessageSource messageSource;

	public EventHighController(IAuthenticationApi authenticationApi, IEventHighApi eventhighApi,
			MessageSource messageSource) {
		this.authenticationApi = authenticationApi;
		this.eventhighApi = eventhighApi;
		this.messageSource = messageSource;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	@RequestMapping(value = "/EventCity", method = RequestMethod.GET)
	String denominationyuyu(Model model, HttpServletRequest request, HttpServletResponse response, HttpSession session)
			throws Exception {
		GCIResponse resp = new GCIResponse();
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {

				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("User authentication failed, please become a user");
					model.addAttribute("msg", resp.getMessage());
					resp.setStatus("Failure");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("User authority null,please become a user");
				model.addAttribute("msg", resp.getMessage());
				resp.setStatus("Failure");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			return "redirect:/Home";
		}

		return "User/EventHigh/EventCityList";
	}
	
	
	@RequestMapping(value = "/Events", method = RequestMethod.GET)
	String remove(Model model, EventRequest request, HttpServletResponse response, HttpSession session)
			throws Exception {

		EventHighResponse resp = new EventHighResponse();
		System.err.println("CITY USER ENTER::::::::::::::::::=============+================"+request.getCity());
		request.setCity(request.getCity());
		session.setAttribute("city", "" +request.getCity());

		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED))

				{
					String data = "";
					// request.setSessionId(sessionId);
					EventHighResponse eventlist = eventhighApi.eventlist(request);

					// if (proceed.getCode().equalsIgnoreCase("S00")) {

					ArrayList<EventHighResponse> list = new ArrayList<EventHighResponse>();
					JSONObject jsonObject = new JSONObject(eventlist.getMessage());
					JSONArray jsonArray = jsonObject.getJSONArray("events");
					// EventHighResponse el = new EventHighResponse();
					for (int i = 0; i < jsonArray.length(); i++) {
						EventHighResponse el = new EventHighResponse();
						try {

							String img_url = jsonArray.getJSONObject(i).getString("img_url");
							String id = jsonArray.getJSONObject(i).getString("id");
							String city = jsonArray.getJSONObject(i).getString("city");
							String title = jsonArray.getJSONObject(i).getString("title");
							String description = jsonArray.getJSONObject(i).getString("description");
							String booking_url;
							if (jsonArray.getJSONObject(i).has("booking_url")) {
								booking_url = jsonArray.getJSONObject(i).getString("booking_url");
							} else {
								booking_url = "";
							}
							String booking_enquiry_url = jsonArray.getJSONObject(i).getString("booking_enquiry_url");
							el.setId(id);
							el.setCity(city);
							el.setTitle(title);
							el.setDescription(description);
							el.setImg_url(img_url);
							el.setBooking_url(booking_url);
							el.setBooking_enquiry_url(booking_enquiry_url);
							ArrayList<EventUpperClass> arrayList = new ArrayList<EventUpperClass>();
							JSONArray jsonArray1 = jsonArray.getJSONObject(i).getJSONArray("upcoming_occurrences");

							for (int j = 0; j < jsonArray1.length(); j++) {
								EventUpperClass el1 = new EventUpperClass();
								String occurrence_id = jsonArray1.getJSONObject(j).getString("occurrence_id");
								String date = jsonArray1.getJSONObject(j).getString("date");
								String start_time = jsonArray1.getJSONObject(j).getString("start_time");
								String end_time = jsonArray1.getJSONObject(j).getString("end_time");
								String end_date = jsonArray1.getJSONObject(j).getString("end_date");
								el1.setOccurrence_id(occurrence_id);
								el1.setDate(date);
								el1.setStart_time(start_time);
								el1.setEnd_time(end_time);
								el1.setEnd_date(end_date);
								arrayList.add(el1);

								el.setEventClass(arrayList);
							}

							JSONObject jsonObject2 = jsonArray.getJSONObject(i).getJSONObject("venue");
							String name = jsonObject2.getString("name");
							String address = jsonObject2.getString("address");
							el.setName(name);
							el.setAddress(address);
							// list.add(el);
							if (jsonArray.getJSONObject(i).has("price")) {

								{
									ArrayList<EventPrice> priceList = new ArrayList<EventPrice>();
									JSONArray jsonArray4 = jsonArray.getJSONObject(i).getJSONArray("price");
									for (int l = 0; l < jsonArray4.length(); l++) {
										EventPrice el3 = new EventPrice();
										String eid = jsonArray4.getJSONObject(l).getString("eid");
										String value = jsonArray4.getJSONObject(l).getString("value");
										el3.setValue(value);
										el3.setEid(eid);
										priceList.add(el3);
										el.setEventPrice(priceList);
										ArrayList<EventOccurence> occurList = new ArrayList<EventOccurence>();
										JSONArray jsonArray5 = jsonArray4.getJSONObject(l).getJSONArray("occurrences");
										for (int m = 0; m < jsonArray5.length(); m++) {
											EventOccurence el4 = new EventOccurence();
											String occurrence_id = jsonArray5.getJSONObject(m)
													.getString("occurrence_id");
											String date = jsonArray5.getJSONObject(m).getString("date");
											String end_date = jsonArray5.getJSONObject(m).getString("end_date");
											String time = jsonArray5.getJSONObject(m).getString("time");
											String end_time = jsonArray5.getJSONObject(m).getString("end_time");
											el4.setOccurrence_id(occurrence_id);
											el4.setDate(date);
											el4.setEnd_date(end_date);
											el4.setTime(time);
											el4.setEnd_time(end_time);
											occurList.add(el4);
										}
										el3.setEventoccur(occurList);
									}

								}

							} else {

							}

						} catch (Exception e) {
							e.printStackTrace();
						}
						list.add(el);
					}

					model.addAttribute("ShowEvent", list);

					/*
					 * } else { resp.setSuccess(false); resp.setCode("F00");
					 * resp.setMessage(
					 * "Please login again client authentication failed");
					 * model.addAttribute("msg", resp.getMessage());
					 * resp.setStatus("Failure");
					 * resp.setResponse(APIUtils.getFailedJSON().toString()); }
					 */

				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("User authentication failed,login again");
					model.addAttribute("msg", resp.getMessage());
					resp.setStatus("Failure");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("User authority null,login again");
				model.addAttribute("msg", resp.getMessage());
				resp.setStatus("Failure");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			return "redirect:/Home";
		}

		return "User/EventHigh/ShowEvent";
	}

	///////////////////////////////////////////////// for invidual EVENT

	///////////////////////////////// post method

	@RequestMapping(value = "/EventDetail", method = RequestMethod.POST)
	String event(Model model, @RequestBody EventRequest request, HttpServletResponse response, HttpSession session)
			throws Exception {

		System.err.println("CITY USER ENTER: IN Cotroller EventDetail:::::::::::::::::=============+================"+request.getCity());
		request.setCity(request.getCity());

		session.setAttribute("id", "" + request.getId());

		System.err.println("EVENT ID ::::::::========+++++++ IN   POsT METHOD ::::::::::::::::::::" + request.getId());

		EventHighResponse resp = new EventHighResponse();
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {

				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("User authentication failed  please become a user");
					model.addAttribute("msg", resp.getMessage());
					resp.setStatus("Failure");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("User authority null,please become a User");
				model.addAttribute("msg", resp.getMessage());
				resp.setStatus("Failure");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			return "redirect:/Home";
		}

		return "User/EventHigh/EventDetail";
	}

	@RequestMapping(value = "/EventDetail", method = RequestMethod.GET)
	String eventDetail(Model model, EventRequest request, HttpServletResponse response, HttpSession session)
			throws Exception {
		
		String data = "";
		request.setId((String) session.getAttribute("id"));
		request.setCity((String) session.getAttribute("city"));
		request.setCity(request.getCity());
		System.err.println("CITY USER ENTER: IN Cotroller EventDetail: IN GET Method::::::::::::::::=============+================"+request.getCity());
		
		String eventid = "" + request.getId();
		EventHighResponse resp = new EventHighResponse();
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED))

				{

					EventHighResponse eventlist = eventhighApi.eventlist(request);

					// if (proceed.getCode().equalsIgnoreCase("S00")) {

					ArrayList<EventHighResponse> list = new ArrayList<EventHighResponse>();
					JSONObject jsonObject = new JSONObject(eventlist.getMessage());
					JSONArray jsonArray = jsonObject.getJSONArray("events");
					for (int i = 0; i < jsonArray.length(); i++) {
						EventHighResponse el = new EventHighResponse();
						try {
							String id = jsonArray.getJSONObject(i).getString("id");

							if (eventid.trim().equals(id.trim())) {
								String img_url = jsonArray.getJSONObject(i).getString("img_url");
								String city = jsonArray.getJSONObject(i).getString("city");
								String title = jsonArray.getJSONObject(i).getString("title");
								String description = jsonArray.getJSONObject(i).getString("description");
								String booking_url;
								if (jsonArray.getJSONObject(i).has("booking_url")) {
									booking_url = jsonArray.getJSONObject(i).getString("booking_url");
								} else {
									booking_url = "";
								}
								String booking_enquiry_url = jsonArray.getJSONObject(i)
										.getString("booking_enquiry_url");
								el.setId(id);
								el.setCity(city);
								el.setTitle(title);
								el.setDescription(description);
								el.setImg_url(img_url);
								el.setBooking_url(booking_url);
								el.setBooking_enquiry_url(booking_enquiry_url);
								ArrayList<EventUpperClass> arrayList = new ArrayList<EventUpperClass>();
								JSONArray jsonArray1 = jsonArray.getJSONObject(i).getJSONArray("upcoming_occurrences");

								for (int j = 0; j < jsonArray1.length(); j++) {
									EventUpperClass el1 = new EventUpperClass();
									String occurrence_id = jsonArray1.getJSONObject(j).getString("occurrence_id");
									String date = jsonArray1.getJSONObject(j).getString("date");
									String start_time = jsonArray1.getJSONObject(j).getString("start_time");
									String end_time = jsonArray1.getJSONObject(j).getString("end_time");
									String end_date = jsonArray1.getJSONObject(j).getString("end_date");
									el1.setOccurrence_id(occurrence_id);
									el1.setDate(date);
									el1.setStart_time(start_time);
									el1.setEnd_time(end_time);
									el1.setEnd_date(end_date);
									arrayList.add(el1);

									el.setEventClass(arrayList);
								}

								JSONObject jsonObject2 = jsonArray.getJSONObject(i).getJSONObject("venue");
								String name = jsonObject2.getString("name");
								String address = jsonObject2.getString("address");
								el.setName(name);
								el.setAddress(address);
								// list.add(el);
								if (jsonArray.getJSONObject(i).has("price")) {

									{
										ArrayList<EventPrice> priceList = new ArrayList<EventPrice>();
										JSONArray jsonArray4 = jsonArray.getJSONObject(i).getJSONArray("price");
										for (int l = 0; l < jsonArray4.length(); l++) {
											EventPrice el3 = new EventPrice();
											String eid = jsonArray4.getJSONObject(l).getString("eid");
											String value = jsonArray4.getJSONObject(l).getString("value");
											el3.setValue(value);
											el3.setEid(eid);
											priceList.add(el3);
											el.setEventPrice(priceList);
											ArrayList<EventOccurence> occurList = new ArrayList<EventOccurence>();
											JSONArray jsonArray5 = jsonArray4.getJSONObject(l)
													.getJSONArray("occurrences");
											for (int m = 0; m < jsonArray5.length(); m++) {
												EventOccurence el4 = new EventOccurence();
												String occurrence_id = jsonArray5.getJSONObject(m)
														.getString("occurrence_id");
												String date = jsonArray5.getJSONObject(m).getString("date");
												String end_date = jsonArray5.getJSONObject(m).getString("end_date");
												String time = jsonArray5.getJSONObject(m).getString("time");
												String end_time = jsonArray5.getJSONObject(m).getString("end_time");
												el4.setOccurrence_id(occurrence_id);
												el4.setDate(date);
												el4.setEnd_date(end_date);
												el4.setTime(time);
												el4.setEnd_time(end_time);
												occurList.add(el4);
											}
											el3.setEventoccur(occurList);
										}

									}

								} else {

								}

								list.add(el);
							}

							else {

							}

						} catch (Exception e) {
							e.printStackTrace();
						}
					}

					model.addAttribute("EventList", list);

					/*
					 * } else { resp.setSuccess(false); resp.setCode("F00");
					 * resp.setMessage(
					 * "Please login again client authentication failed");
					 * model.addAttribute("msg", resp.getMessage());
					 * resp.setStatus("Failure");
					 * resp.setResponse(APIUtils.getFailedJSON().toString()); }
					 */

				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("User authentication failed,login again");
					model.addAttribute("msg", resp.getMessage());
					resp.setStatus("Failure");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("User authority null,login again");
				model.addAttribute("msg", resp.getMessage());
				resp.setStatus("Failure");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			return "redirect:/Home";
		}

		return "User/EventHigh/EventDetail";
	}

}
