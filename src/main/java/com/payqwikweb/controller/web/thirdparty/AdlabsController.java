package com.payqwikweb.controller.web.thirdparty;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.payqwikweb.api.IAdlabsAPI;
import com.payqwikweb.api.IAuthenticationApi;
import com.payqwikweb.app.model.ResponseStatus;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.model.app.request.AdlabsAccessTokenRequest;
import com.payqwikweb.model.app.request.AdlabsTicketRequest;
import com.payqwikweb.model.app.response.AdlabOrderCreateRequest;
import com.payqwikweb.model.app.response.AdlabsAccessTokenResponse;
import com.payqwikweb.model.app.response.AdlabsAddOrder;
import com.payqwikweb.model.app.response.AdlabsAmountInitateRequest;
import com.payqwikweb.model.app.response.AdlabsAmountInitateResponse;
import com.payqwikweb.model.app.response.AdlabsAmountsuccessRequest;
import com.payqwikweb.model.app.response.AdlabsOrderCreateResponse;
import com.payqwikweb.model.app.response.AdlabsOrderVoucher;
import com.payqwikweb.model.app.response.AdlabsPaymentRequest;
import com.payqwikweb.model.app.response.AdlabsProceedResponse;
import com.payqwikweb.model.app.response.AdlabsTicketListResponse;
import com.payqwikweb.util.APIUtils;
import com.payqwikweb.util.Authorities;

@RequestMapping("/User/Adlabs")
public class AdlabsController implements MessageSourceAware {
	private IAdlabsAPI adLabsApi;
	private final IAuthenticationApi authenticationApi;

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	private MessageSource messageSource;

	public AdlabsController(IAdlabsAPI adLabsApi, IAuthenticationApi authenticationApi) {
		this.authenticationApi = authenticationApi;
		this.adLabsApi = adLabsApi;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	@RequestMapping(value = "/SelectEntertainment", method = RequestMethod.GET)
	public String getAccessToken(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
		AdlabsTicketListResponse resp = new AdlabsTicketListResponse();
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
					return "User/Adlabs/Entertainment";
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("User Authentication Failed");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("User Authority null");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Session null");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return "redirect:/Home";
	}

	@RequestMapping(value = "/SelectTicket", method = RequestMethod.POST)
	public String selectTicket(@RequestHeader(value = "hash", required = false) String hash, Model model,
							   HttpServletRequest request, HttpServletResponse response, HttpSession session, AdlabsTicketRequest dto)
			throws Exception {
		AdlabsAccessTokenRequest tokenreq = new AdlabsAccessTokenRequest();
		AdlabsTicketListResponse resp = new AdlabsTicketListResponse();
		dto.setDate_departure(dto.getDate_departure());
		dto.setDate_visit(dto.getDate_departure());
		dto.setDestination(dto.getDestination());
		System.err.println("DESTINATION :::::::" + dto.getDestination());
		System.err.println("DATE:::::::::::" + dto.getDate_departure());
		System.err.println("DESTINATION::::::::::::::::::::::::" + dto.getDestination());

		session.setAttribute("date", "" + dto.getDate_departure());
		session.setAttribute("destination", "" + dto.getDestination());

		String data = "";
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED))

				{
					AdlabsAccessTokenResponse access = adLabsApi.auth(tokenreq);
					JSONObject jsonObject1 = new JSONObject(access.getMessage());
					String acessToken = jsonObject1.getString("token");
					session.setAttribute("acessToken", "" + acessToken);
					String sessid = jsonObject1.getString("sessid");
					String session_name = jsonObject1.getString("session_name");
					String cookie = (session_name + "=" + sessid);
					dto.setAcessToken(acessToken);
					dto.setSession_name(cookie);
					session.setAttribute("session_name", "" + dto.getSession_name());
					AdlabsTicketListResponse wtc = new AdlabsTicketListResponse();
					AdlabsTicketListResponse result = adLabsApi.ticketList(dto);
					List<AdlabsTicketListResponse> ticketList = new ArrayList<>();
					if (result.getCode().equalsIgnoreCase("S00")) {
						try {
							if (dto.getDestination().equalsIgnoreCase("Theme Park")) {

								JSONObject jsonObject = new JSONObject(result.getMessage());
								AdlabsTicketListResponse tc = new AdlabsTicketListResponse();
								// for Regular Ticket
								Object prodType = jsonObject.getJSONObject("ticket").getJSONObject("Regular Ticket")
										.get("prodType");
								String ticket_des = (String) jsonObject.getJSONObject("ticket")
										.getJSONObject("Regular Ticket").get("ticket_des");
								String title = (String) jsonObject.getJSONObject("ticket")
										.getJSONObject("Regular Ticket").get("title");
								String image_url = (String) jsonObject.getJSONObject("ticket")
										.getJSONObject("Regular Ticket").get("image_url");
								// for Adult

								if (jsonObject.getJSONObject("ticket").has("Adult")) {
									String atype = (String) jsonObject.getJSONObject("ticket")
											.getJSONObject("Regular Ticket").getJSONObject("Adult").get("type");
									String aquantity = (String) jsonObject.getJSONObject("ticket")
											.getJSONObject("Regular Ticket").getJSONObject("Adult").get("quantity");
									String acost_per = (String) jsonObject.getJSONObject("ticket")
											.getJSONObject("Regular Ticket").getJSONObject("Adult").get("cost_per");
									String atotal_cost = (String) jsonObject.getJSONObject("ticket")
											.getJSONObject("Regular Ticket").getJSONObject("Adult").get("total_cost");
									String aproduct_id = (String) jsonObject.getJSONObject("ticket")
											.getJSONObject("Regular Ticket").getJSONObject("Adult").get("product_id");

									// for total cost calculatation

									double price_per_quantity1 = Double.parseDouble(
											(String) jsonObject.getJSONObject("ticket").getJSONObject("Regular Ticket")
													.getJSONObject("Adult").getJSONObject("tax_details")
													.getJSONObject("service_tax").get("price_per_quantity"));

									double price_per_quantity2 = Double.parseDouble(
											(String) jsonObject.getJSONObject("ticket").getJSONObject("Regular Ticket")
													.getJSONObject("Adult").getJSONObject("tax_details")
													.getJSONObject("swachh_tax").get("price_per_quantity"));

									double price_per_quantity3 = Double.parseDouble(
											(String) jsonObject.getJSONObject("ticket").getJSONObject("Regular Ticket")
													.getJSONObject("Adult").getJSONObject("tax_details")
													.getJSONObject("kkc_tax").get("price_per_quantity"));

									double total = Double.parseDouble(acost_per);
									float acost_per1 = (float) (total + price_per_quantity1 + price_per_quantity2
											+ price_per_quantity3);

								}

								// float acost_per2=(float) (total1+a+b+c);
								// for Senior Citizen
								String sctype = (String) jsonObject.getJSONObject("ticket")
										.getJSONObject("Regular Ticket").getJSONObject("Senior Citizen").get("type");
								String squantity = (String) jsonObject.getJSONObject("ticket")
										.getJSONObject("Regular Ticket").getJSONObject("Senior Citizen")
										.get("quantity");
								String scost_per = (String) jsonObject.getJSONObject("ticket")
										.getJSONObject("Regular Ticket").getJSONObject("Senior Citizen")
										.get("cost_per");
								String stotal_cost = (String) jsonObject.getJSONObject("ticket")
										.getJSONObject("Regular Ticket").getJSONObject("Senior Citizen")
										.get("total_cost");
								String sproduct_id = (String) jsonObject.getJSONObject("ticket")
										.getJSONObject("Regular Ticket").getJSONObject("Senior Citizen")
										.get("product_id");

								// for total cost calculatation

								double a = Double.parseDouble(
										(String) jsonObject.getJSONObject("ticket").getJSONObject("Regular Ticket")
												.getJSONObject("Senior Citizen").getJSONObject("tax_details")
												.getJSONObject("service_tax").get("price_per_quantity"));

								double b = Double.parseDouble(
										(String) jsonObject.getJSONObject("ticket").getJSONObject("Regular Ticket")
												.getJSONObject("Senior Citizen").getJSONObject("tax_details")
												.getJSONObject("swachh_tax").get("price_per_quantity"));

								double c = Double.parseDouble(
										(String) jsonObject.getJSONObject("ticket").getJSONObject("Regular Ticket")
												.getJSONObject("Senior Citizen").getJSONObject("tax_details")
												.getJSONObject("kkc_tax").get("price_per_quantity"));

								double total1 = Double.parseDouble(scost_per);
								float scost_per2 = (float) (total1 + a + b + c);
								// float acost_per2=(float) (total1+a+b+c);

								// for college
								String ctype = (String) jsonObject.getJSONObject("ticket")
										.getJSONObject("Regular Ticket").getJSONObject("College").get("type");
								String cquantity = (String) jsonObject.getJSONObject("ticket")
										.getJSONObject("Regular Ticket").getJSONObject("College").get("quantity");
								String ccost_per = (String) jsonObject.getJSONObject("ticket")
										.getJSONObject("Regular Ticket").getJSONObject("College").get("cost_per");
								String ctotal_cost = (String) jsonObject.getJSONObject("ticket")
										.getJSONObject("Regular Ticket").getJSONObject("College").get("total_cost");
								String cproduct_id = (String) jsonObject.getJSONObject("ticket")
										.getJSONObject("Regular Ticket").getJSONObject("College").get("product_id");

								// for total cost calculatation

								double d = Double.parseDouble(
										(String) jsonObject.getJSONObject("ticket").getJSONObject("Regular Ticket")
												.getJSONObject("College").getJSONObject("tax_details")
												.getJSONObject("service_tax").get("price_per_quantity"));

								double e = Double.parseDouble(
										(String) jsonObject.getJSONObject("ticket").getJSONObject("Regular Ticket")
												.getJSONObject("College").getJSONObject("tax_details")
												.getJSONObject("swachh_tax").get("price_per_quantity"));

								double f = Double.parseDouble(
										(String) jsonObject.getJSONObject("ticket").getJSONObject("Regular Ticket")
												.getJSONObject("College").getJSONObject("tax_details")
												.getJSONObject("kkc_tax").get("price_per_quantity"));

								double total2 = Double.parseDouble(ccost_per);
								float ccost_per3 = (float) (total2 + d + e + f);

								// float acost_per2=(float) (total1+a+b+c);
								// for child
								if (jsonObject.getJSONObject("ticket").has("Child")) {
									String chtype = (String) jsonObject.getJSONObject("ticket")
											.getJSONObject("Regular Ticket").getJSONObject("Child").get("type");
									String chquantity = (String) jsonObject.getJSONObject("ticket")
											.getJSONObject("Regular Ticket").getJSONObject("Child").get("quantity");
									String chcost_per = (String) jsonObject.getJSONObject("ticket")
											.getJSONObject("Regular Ticket").getJSONObject("Child").get("cost_per");
									String chtotal_cost = (String) jsonObject.getJSONObject("ticket")
											.getJSONObject("Regular Ticket").getJSONObject("Child").get("total_cost");
									String chproduct_id = (String) jsonObject.getJSONObject("ticket")
											.getJSONObject("Regular Ticket").getJSONObject("Child").get("product_id");

									// for total cost calculatation

									double g = Double.parseDouble(
											(String) jsonObject.getJSONObject("ticket").getJSONObject("Regular Ticket")
													.getJSONObject("Child").getJSONObject("tax_details")
													.getJSONObject("service_tax").get("price_per_quantity"));

									double h = Double.parseDouble(
											(String) jsonObject.getJSONObject("ticket").getJSONObject("Regular Ticket")
													.getJSONObject("Child").getJSONObject("tax_details")
													.getJSONObject("swachh_tax").get("price_per_quantity"));

									double i = Double.parseDouble(
											(String) jsonObject.getJSONObject("ticket").getJSONObject("Regular Ticket")
													.getJSONObject("Child").getJSONObject("tax_details")
													.getJSONObject("kkc_tax").get("price_per_quantity"));

									double total3 = Double.parseDouble(ccost_per);
									float chcost_per4 = (float) (total3 + g + h + i);
								}
								// float acost_per2=(float) (total1+a+b+c);

								// for Imagica Express Ticket
								String iprodType = (String) jsonObject.getJSONObject("ticket")
										.getJSONObject("Imagica Express").get("prodType");
								String iticket_des = (String) jsonObject.getJSONObject("ticket")
										.getJSONObject("Imagica Express").get("ticket_des");
								String ititle = (String) jsonObject.getJSONObject("ticket")
										.getJSONObject("Imagica Express").get("title");
								String iimage_url = (String) jsonObject.getJSONObject("ticket")
										.getJSONObject("Imagica Express").get("image_url");
								String iatype = (String) jsonObject.getJSONObject("ticket")
										.getJSONObject("Imagica Express").getJSONObject("Adult").get("type");
								String iaquantity = (String) jsonObject.getJSONObject("ticket")
										.getJSONObject("Imagica Express").getJSONObject("Adult").get("quantity");
								String iacost_per = (String) jsonObject.getJSONObject("ticket")
										.getJSONObject("Imagica Express").getJSONObject("Adult").get("cost_per");
								String iatotal_cost = (String) jsonObject.getJSONObject("ticket")
										.getJSONObject("Imagica Express").getJSONObject("Adult").get("total_cost");
								String iaproduct_id = (String) jsonObject.getJSONObject("ticket")
										.getJSONObject("Imagica Express").getJSONObject("Adult").get("product_id");
								// for Senior Citizen
								// String isctype ;

								// for total cost calculatation

								double j = Double.parseDouble(
										(String) jsonObject.getJSONObject("ticket").getJSONObject("Imagica Express")
												.getJSONObject("Adult").getJSONObject("tax_details")
												.getJSONObject("service_tax").get("price_per_quantity"));

								double k = Double.parseDouble(
										(String) jsonObject.getJSONObject("ticket").getJSONObject("Imagica Express")
												.getJSONObject("Adult").getJSONObject("tax_details")
												.getJSONObject("swachh_tax").get("price_per_quantity"));

								double l = Double.parseDouble(
										(String) jsonObject.getJSONObject("ticket").getJSONObject("Imagica Express")
												.getJSONObject("Adult").getJSONObject("tax_details")
												.getJSONObject("kkc_tax").get("price_per_quantity"));

								double total4 = Double.parseDouble(iacost_per);
								float iacost_per1 = (float) (total4 + j + k + l);
								// float acost_per2=(float) (total1+a+b+c);

								if (jsonObject.getJSONObject("ticket").has("Senior Citizen")) {

									String isctype = (String) jsonObject.getJSONObject("ticket")
											.getJSONObject("Imagica Express").getJSONObject("Senior Citizen")
											.get("type");
									String isquantity = (String) jsonObject.getJSONObject("ticket")
											.getJSONObject("Imagica Express").getJSONObject("Senior Citizen")
											.get("quantity");
									String iscost_per = (String) jsonObject.getJSONObject("ticket")
											.getJSONObject("Imagica Express").getJSONObject("Senior Citizen")
											.get("cost_per");
									String istotal_cost = (String) jsonObject.getJSONObject("ticket")
											.getJSONObject("Imagica Express").getJSONObject("Senior Citizen")
											.get("total_cost");
									String isproduct_id = (String) jsonObject.getJSONObject("ticket")
											.getJSONObject("Imagica Express").getJSONObject("Senior Citizen")
											.get("product_id");
								} // else {

								// for college
								String ictype = (String) jsonObject.getJSONObject("ticket")
										.getJSONObject("Imagica Express").getJSONObject("College").get("type");
								String icquantity = (String) jsonObject.getJSONObject("ticket")
										.getJSONObject("Imagica Express").getJSONObject("College").get("quantity");
								String iccost_per = (String) jsonObject.getJSONObject("ticket")
										.getJSONObject("Imagica Express").getJSONObject("College").get("cost_per");
								String ictotal_cost = (String) jsonObject.getJSONObject("ticket")
										.getJSONObject("Imagica Express").getJSONObject("College").get("total_cost");
								String icproduct_id = (String) jsonObject.getJSONObject("ticket")
										.getJSONObject("Imagica Express").getJSONObject("College").get("product_id");

								// for total cost calculatation

								double m = Double.parseDouble(
										(String) jsonObject.getJSONObject("ticket").getJSONObject("Imagica Express")
												.getJSONObject("College").getJSONObject("tax_details")
												.getJSONObject("service_tax").get("price_per_quantity"));

								double n = Double.parseDouble(
										(String) jsonObject.getJSONObject("ticket").getJSONObject("Imagica Express")
												.getJSONObject("College").getJSONObject("tax_details")
												.getJSONObject("swachh_tax").get("price_per_quantity"));

								double o = Double.parseDouble(
										(String) jsonObject.getJSONObject("ticket").getJSONObject("Imagica Express")
												.getJSONObject("College").getJSONObject("tax_details")
												.getJSONObject("kkc_tax").get("price_per_quantity"));

								double total5 = Double.parseDouble(iccost_per);
								float iscost_per2 = (float) (total5 + m + n + o);

								// float acost_per2=(float) (total1+a+b+c);

								// for child
								String ichtype = (String) jsonObject.getJSONObject("ticket")
										.getJSONObject("Imagica Express").getJSONObject("Child").get("type");
								String ichquantity = (String) jsonObject.getJSONObject("ticket")
										.getJSONObject("Imagica Express").getJSONObject("Child").get("quantity");
								String ichcost_per = (String) jsonObject.getJSONObject("ticket")
										.getJSONObject("Imagica Express").getJSONObject("Child").get("cost_per");
								String ichtotal_cost = (String) jsonObject.getJSONObject("ticket")
										.getJSONObject("Imagica Express").getJSONObject("Child").get("total_cost");
								String ichproduct_id = (String) jsonObject.getJSONObject("ticket")
										.getJSONObject("Imagica Express").getJSONObject("Child").get("product_id");

								// for total cost calculatation

								double p = Double.parseDouble(
										(String) jsonObject.getJSONObject("ticket").getJSONObject("Imagica Express")
												.getJSONObject("College").getJSONObject("tax_details")
												.getJSONObject("service_tax").get("price_per_quantity"));

								double q = Double.parseDouble(
										(String) jsonObject.getJSONObject("ticket").getJSONObject("Imagica Express")
												.getJSONObject("College").getJSONObject("tax_details")
												.getJSONObject("swachh_tax").get("price_per_quantity"));

								double r = Double.parseDouble(
										(String) jsonObject.getJSONObject("ticket").getJSONObject("Imagica Express")
												.getJSONObject("College").getJSONObject("tax_details")
												.getJSONObject("kkc_tax").get("price_per_quantity"));

								double total6 = Double.parseDouble(iccost_per);
								float ichcost_per3 = (float) (total6 + p + q + r);

								// float acost_per2=(float) (total1+a+b+c);

								// tc.setProdType(prodType);
								tc.setTicket_des(ticket_des);
								tc.setTitle(title);
								tc.setImage_url(image_url);
								//tc.setAtype(atype);
								//tc.setAquantity(aquantity);
								// tc.setAcost_per1(acost_per1);
								//tc.setAcost_per1(acost_per1);
								//tc.setAtotal_cost(atotal_cost);
								//tc.setAproduct_id(aproduct_id);
								tc.setSctype(sctype);
								tc.setSquantity(squantity);
								tc.setScost_per2(scost_per2);
								tc.setStotal_cost(stotal_cost);
								tc.setSproduct_id(sproduct_id);

								tc.setCtype(ctype);
								tc.setCquantity(cquantity);
								tc.setCcost_per3(ccost_per3);
								tc.setCtotal_cost(ctotal_cost);
								tc.setCproduct_id(cproduct_id);
								//tc.setChtype(chtype);
								//tc.setChquantity(chquantity);
								//tc.setChcost_per4(chcost_per4);
								//tc.setChtotal_cost(chtotal_cost);
								//tc.setChproduct_id(chproduct_id);

								tc.setIprodType(iprodType);
								tc.setIticket_des(iticket_des);
								tc.setItitle(ititle);
								tc.setImage_url(iimage_url);
								tc.setIatype(iatype);
								tc.setIaquantity(iaquantity);
								tc.setIacost_per1(iacost_per1);
								tc.setIatotal_cost(iatotal_cost);
								tc.setIaproduct_id(iaproduct_id);

								// tc.setIsctype(isctype);
								// tc.setIsquantity(isquantity);
								// tc.setIscost_per(iscost_per);
								// tc.setIstotal_cost(istotal_cost);
								// tc.setIsproduct_id(isproduct_id);
								tc.setIctype(ictype);
								tc.setIcquantity(icquantity);
								tc.setIscost_per2(iscost_per2);
								tc.setIctotal_cost(ictotal_cost);
								tc.setIcproduct_id(icproduct_id);
								tc.setIchtype(ichtype);
								tc.setIchquantity(ichquantity);
								tc.setIchcost_per3(ichcost_per3);
								tc.setIchtotal_cost(ichtotal_cost);
								tc.setIchproduct_id(ichproduct_id);
								//tc.setChtype(chtype);
								//tc.setChquantity(chquantity);
								//tc.setChproduct_id(chproduct_id);
								//tc.setChcost_per4(chcost_per4);
								ticketList.add(tc);
								session.setAttribute("ticketlist", ticketList);

								// }
							}
							// for water park

							else {

								JSONObject jsonObject = new JSONObject(result.getMessage());
								// AdlabsTicketListResponse wtc = new
								// AdlabsTicketListResponse();
								// for Regular Ticket
								Object prodType = jsonObject.getJSONObject("ticket").getJSONObject("Regular Ticket")
										.get("prodType");
								String ticket_des = (String) jsonObject.getJSONObject("ticket")
										.getJSONObject("Regular Ticket").get("ticket_des");
								String title = (String) jsonObject.getJSONObject("ticket")
										.getJSONObject("Regular Ticket").get("title");
								String image_url = (String) jsonObject.getJSONObject("ticket")
										.getJSONObject("Regular Ticket").get("image_url");

								// for Adult
								String atype = (String) jsonObject.getJSONObject("ticket")
										.getJSONObject("Regular Ticket").getJSONObject("Adult").get("type");
								String aquantity = (String) jsonObject.getJSONObject("ticket")
										.getJSONObject("Regular Ticket").getJSONObject("Adult").get("quantity");
								String acost_per = (String) jsonObject.getJSONObject("ticket")
										.getJSONObject("Regular Ticket").getJSONObject("Adult").get("cost_per");
								String atotal_cost = (String) jsonObject.getJSONObject("ticket")
										.getJSONObject("Regular Ticket").getJSONObject("Adult").get("total_cost");
								String aproduct_id = (String) jsonObject.getJSONObject("ticket")
										.getJSONObject("Regular Ticket").getJSONObject("Adult").get("product_id");

								// for total cost calculatation

								double a = Double.parseDouble(
										(String) jsonObject.getJSONObject("ticket").getJSONObject("Regular Ticket")
												.getJSONObject("Adult").getJSONObject("tax_details")
												.getJSONObject("service_tax").get("price_per_quantity"));

								double b = Double.parseDouble(
										(String) jsonObject.getJSONObject("ticket").getJSONObject("Regular Ticket")
												.getJSONObject("Adult").getJSONObject("tax_details")
												.getJSONObject("swachh_tax").get("price_per_quantity"));

								double c = Double.parseDouble(
										(String) jsonObject.getJSONObject("ticket").getJSONObject("Regular Ticket")
												.getJSONObject("Adult").getJSONObject("tax_details")
												.getJSONObject("kkc_tax").get("price_per_quantity"));

								double total1 = Double.parseDouble(acost_per);
								float acost_per2 = (float) (total1 + a + b + c);
								// float t3acost_per5=(float) (total4+j+k+l);

								// Wat-A-Wednesday (Regular) - Tier 1
								if (jsonObject.getJSONObject("ticket").has("Wat-A-Wednesday (Regular) - Tier 1")) {

									// AdlabsTicketListResponse wtc1 = new
									// AdlabsTicketListResponse();
									Object t1prodType = jsonObject.getJSONObject("ticket")
											.getJSONObject("Wat-A-Wednesday (Regular) - Tier 1").get("prodType");
									String t1ticket_des = (String) jsonObject.getJSONObject("ticket")
											.getJSONObject("Wat-A-Wednesday (Regular) - Tier 1").get("ticket_des");
									String t1title = (String) jsonObject.getJSONObject("ticket")
											.getJSONObject("Wat-A-Wednesday (Regular) - Tier 1").get("title");
									String t1image_url = (String) jsonObject.getJSONObject("ticket")
											.getJSONObject("Wat-A-Wednesday (Regular) - Tier 1").get("image_url");

									// for Adult
									String t1atype = (String) jsonObject.getJSONObject("ticket")
											.getJSONObject("Wat-A-Wednesday (Regular) - Tier 1").getJSONObject("Adult")
											.get("type");
									String t1aquantity = (String) jsonObject.getJSONObject("ticket")
											.getJSONObject("Wat-A-Wednesday (Regular) - Tier 1").getJSONObject("Adult")
											.get("quantity");
									String t1acost_per = (String) jsonObject.getJSONObject("ticket")
											.getJSONObject("Wat-A-Wednesday (Regular) - Tier 1").getJSONObject("Adult")
											.get("cost_per");
									String t1atotal_cost = (String) jsonObject.getJSONObject("ticket")
											.getJSONObject("Wat-A-Wednesday (Regular) - Tier 1").getJSONObject("Adult")
											.get("total_cost");
									String t1aproduct_id = (String) jsonObject.getJSONObject("ticket")
											.getJSONObject("Wat-A-Wednesday (Regular) - Tier 1").getJSONObject("Adult")
											.get("product_id");

									// for total cost calculatation

									double d = Double.parseDouble((String) jsonObject.getJSONObject("ticket")
											.getJSONObject("Wat-A-Wednesday (Regular) - Tier 1").getJSONObject("Adult")
											.getJSONObject("tax_details").getJSONObject("service_tax")
											.get("price_per_quantity"));

									double e = Double.parseDouble((String) jsonObject.getJSONObject("ticket")
											.getJSONObject("Wat-A-Wednesday (Regular) - Tier 1").getJSONObject("Adult")
											.getJSONObject("tax_details").getJSONObject("swachh_tax")
											.get("price_per_quantity"));

									double f = Double.parseDouble((String) jsonObject.getJSONObject("ticket")
											.getJSONObject("Wat-A-Wednesday (Regular) - Tier 1").getJSONObject("Adult")
											.getJSONObject("tax_details").getJSONObject("kkc_tax")
											.get("price_per_quantity"));

									double total2 = Double.parseDouble(t1acost_per);
									float t1acost_per3 = (float) (total2 + d + e + f);
									// float t3acost_per5=(float)
									// (total4+j+k+l);

									wtc.setT1ticket_des(t1ticket_des);
									wtc.setT1title(t1title);
									wtc.setT1image_url(t1image_url);
									wtc.setT1atype(t1atype);
									wtc.setT1aquantity(t1aquantity);
									wtc.setT1acost_per3(t1acost_per3);
									wtc.setT1atotal_cost(t1atotal_cost);
									wtc.setT1aproduct_id(t1aproduct_id);

								}

								// Wat-A-Wednesday (Regular) -Tier 2
								if (jsonObject.getJSONObject("ticket").has("Wat-A-Wednesday (Regular) - Tier 2")) {
									Object t2prodType = jsonObject.getJSONObject("ticket")
											.getJSONObject("Wat-A-Wednesday (Regular) - Tier 2").get("prodType");
									String t2ticket_des = (String) jsonObject.getJSONObject("ticket")
											.getJSONObject("Wat-A-Wednesday (Regular) - Tier 2").get("ticket_des");
									String t2title = (String) jsonObject.getJSONObject("ticket")
											.getJSONObject("Wat-A-Wednesday (Regular) - Tier 2").get("title");
									String t2image_url = (String) jsonObject.getJSONObject("ticket")
											.getJSONObject("Wat-A-Wednesday (Regular) - Tier 2").get("image_url");

									// for Adult
									String t2atype = (String) jsonObject.getJSONObject("ticket")
											.getJSONObject("Wat-A-Wednesday (Regular) - Tier 2").getJSONObject("Adult")
											.get("type");
									String t2aquantity = (String) jsonObject.getJSONObject("ticket")
											.getJSONObject("Wat-A-Wednesday (Regular) - Tier 2").getJSONObject("Adult")
											.get("quantity");
									String t2acost_per = (String) jsonObject.getJSONObject("ticket")
											.getJSONObject("Wat-A-Wednesday (Regular) - Tier 2").getJSONObject("Adult")
											.get("cost_per");
									String t2atotal_cost = (String) jsonObject.getJSONObject("ticket")
											.getJSONObject("Wat-A-Wednesday (Regular) - Tier 2").getJSONObject("Adult")
											.get("total_cost");
									String t2aproduct_id = (String) jsonObject.getJSONObject("ticket")
											.getJSONObject("Wat-A-Wednesday (Regular) - Tier 2").getJSONObject("Adult")
											.get("product_id");

									// for total cost calculatation

									double g = Double.parseDouble((String) jsonObject.getJSONObject("ticket")
											.getJSONObject("Wat-A-Wednesday (Regular) - Tier 2").getJSONObject("Adult")
											.getJSONObject("tax_details").getJSONObject("service_tax")
											.get("price_per_quantity"));

									double h = Double.parseDouble((String) jsonObject.getJSONObject("ticket")
											.getJSONObject("Wat-A-Wednesday (Regular) - Tier 2").getJSONObject("Adult")
											.getJSONObject("tax_details").getJSONObject("swachh_tax")
											.get("price_per_quantity"));

									double i = Double.parseDouble((String) jsonObject.getJSONObject("ticket")
											.getJSONObject("Wat-A-Wednesday (Regular) - Tier 2").getJSONObject("Adult")
											.getJSONObject("tax_details").getJSONObject("kkc_tax")
											.get("price_per_quantity"));

									double total3 = Double.parseDouble(t2acost_per);
									float t2acost_per4 = (float) (total3 + g + h + i);

									// float t3acost_per5=(float)
									// (total4+j+k+l);

									wtc.setT2ticket_des(t2ticket_des);
									wtc.setT2title(t2title);
									wtc.setT2image_url(t2image_url);
									wtc.setT2atype(t2atype);
									wtc.setT2aquantity(t2aquantity);
									wtc.setT2acost_per4(t2acost_per4);
									wtc.setT2atotal_cost(t2atotal_cost);
									wtc.setT2aproduct_id(t2aproduct_id);
								}
								// Wat-A-Wednesday (Express)
								if (jsonObject.getJSONObject("ticket").has("Wat-A-Wednesday (Express)")) {
									System.err.println("eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee8888888888888888");
									Object eprodType = jsonObject.getJSONObject("ticket")
											.getJSONObject("Wat-A-Wednesday (Express)").get("prodType");
									String eticket_des = (String) jsonObject.getJSONObject("ticket")
											.getJSONObject("Wat-A-Wednesday (Express)").get("ticket_des");
									String etitle = (String) jsonObject.getJSONObject("ticket")
											.getJSONObject("Wat-A-Wednesday (Express)").get("title");
									String eimage_url = (String) jsonObject.getJSONObject("ticket")
											.getJSONObject("Wat-A-Wednesday (Express)").get("image_url");

									// for Adult
									String eatype = (String) jsonObject.getJSONObject("ticket")
											.getJSONObject("Wat-A-Wednesday (Express)").getJSONObject("Adult")
											.get("type");
									String eaquantity = (String) jsonObject.getJSONObject("ticket")
											.getJSONObject("Wat-A-Wednesday (Express)").getJSONObject("Adult")
											.get("quantity");
									String eacost_per = (String) jsonObject.getJSONObject("ticket")
											.getJSONObject("Wat-A-Wednesday (Express)").getJSONObject("Adult")
											.get("cost_per");
									String eatotal_cost = (String) jsonObject.getJSONObject("ticket")
											.getJSONObject("Wat-A-Wednesday (Express)").getJSONObject("Adult")
											.get("total_cost");
									String eaproduct_id = (String) jsonObject.getJSONObject("ticket")
											.getJSONObject("Wat-A-Wednesday (Express)").getJSONObject("Adult")
											.get("product_id");

									// for total cost calculatation

									double j = Double.parseDouble((String) jsonObject.getJSONObject("ticket")
											.getJSONObject("Wat-A-Wednesday (Express)").getJSONObject("Adult")
											.getJSONObject("tax_details").getJSONObject("service_tax")
											.get("price_per_quantity"));

									double k = Double.parseDouble((String) jsonObject.getJSONObject("ticket")
											.getJSONObject("Wat-A-Wednesday (Express)").getJSONObject("Adult")
											.getJSONObject("tax_details").getJSONObject("swachh_tax")
											.get("price_per_quantity"));

									double l = Double.parseDouble((String) jsonObject.getJSONObject("ticket")
											.getJSONObject("Wat-A-Wednesday (Express)").getJSONObject("Adult")
											.getJSONObject("tax_details").getJSONObject("kkc_tax")
											.get("price_per_quantity"));

									double total4 = Double.parseDouble(eacost_per);
									float t3acost_per5 = (float) (total4 + j + k + l);

									System.err.println(j + "bbbbbbb" + k + "vvvvvvvvvv" + l + "" + total4);
									System.err.println(
											"cost for wat a wednesday:::::::::++++++++++++::::::::::" + t3acost_per5);

									wtc.setEticket_des(eticket_des);
									wtc.setEtitle(etitle);
									wtc.setEimage_url(eimage_url);
									wtc.setEatype(eatype);
									wtc.setEaquantity(eaquantity);
									wtc.setEacost_per(eacost_per);
									wtc.setT3acost_per5(t3acost_per5);
									wtc.setEatotal_cost(eatotal_cost);
									wtc.setEaproduct_id(eaproduct_id);

								}
								wtc.setTicket_des(ticket_des);
								wtc.setTitle(title);
								wtc.setImage_url(image_url);
								wtc.setAtype(atype);
								wtc.setAquantity(aquantity);
								wtc.setAcost_per2(acost_per2);
								wtc.setAtotal_cost(atotal_cost);
								wtc.setAproduct_id(aproduct_id);
								ticketList.add(wtc);
								session.setAttribute("ticketlist", ticketList);

								return "User/Adlabs/WEntertainmentDetail";

							}

						} catch (Exception e) {
							e.printStackTrace();
						}
					} else {
						resp.setSuccess(false);
						resp.setCode("F00");
						resp.setMessage(result.getMessage());
						resp.setStatus(ResponseStatus.FAILURE);
						resp.setResponse(APIUtils.getFailedJSON().toString());
						model.addAttribute("msg", result.getMessage());
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("This Ticket not for you Please become a user");
					model.addAttribute("msg", resp.getMessage());

					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("User Authority null,Please become a user");
				model.addAttribute("msg", resp.getMessage());

				resp.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			return "redirect:/Home";
		}

		return "User/Adlabs/EntertainmentDetail";
	}

	@RequestMapping(value = "/AdlabsUserDetail", method = RequestMethod.POST)
	public String postUserDetail(HttpServletRequest request, HttpServletResponse response, HttpSession session,
								 Model model, @RequestBody AdlabOrderCreateRequest dto) {

		System.err.println("::::: INside POST METHOD::::::id" + dto.getProduct_id());
		System.err.println("::::: INside POST METHOD::::::Quantity" + dto.getQuantity());
		System.err.println("::::: INside POST METHOD::::::Amount" + dto.getAmount());
		double a = Double.parseDouble(dto.getQuantity());
		double b = Double.parseDouble(dto.getAmount());

		float totalamount = (float) (a*b);
		dto.setAmount(""+totalamount);
		System.err.println("TOTAL AMOUNT ::::::"+totalamount +""+dto.getAmount());
		session.setAttribute("amount", "" + dto.getAmount());
		dto.setField_visit_date((String) session.getAttribute("date"));
		dto.setField_departure_date((String) session.getAttribute("date"));
		dto.setAccessToken((String) session.getAttribute("acessToken"));
		dto.setField_evt_res_business_unit((String) session.getAttribute("destination"));

		dto.setSession_name((String) session.getAttribute("session_name"));

		AdlabsTicketListResponse resp = new AdlabsTicketListResponse();
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED))

				{
					String data = "";
					ArrayList<AdlabsOrderCreateResponse> list = new ArrayList<AdlabsOrderCreateResponse>();
					AdlabsOrderCreateResponse order = adLabsApi.ordercreate(dto);
					if (order.getCode().equalsIgnoreCase("S00")) {
						try {
							JSONObject jsonObject = new JSONObject(order.getMessage());
							System.err.println("order create Api Response++++++++++++++" + order.getMessage());

							if (jsonObject.has("error")) {
								resp.setMessage(jsonObject.getString("error"));
								model.addAttribute("msg", resp.getMessage());
							} else {
								AdlabsOrderCreateResponse ac = new AdlabsOrderCreateResponse();
								String uid = jsonObject.getString("uid");
								String order_id = jsonObject.getString("order_id");
								// String amount = (String)
								// jsonObject.getJSONObject("commerce_order_total").get("amount");

								ac.setUid(uid);
								ac.setOrder_id(order_id);

								System.err.println("UID:::::::" + uid + " order id :::::::::" + order_id);
								session.setAttribute("uid", "" + uid);
								session.setAttribute("order_id", "" + order_id);
								// session.setAttribute("amount1","" +amount);
								model.addAttribute("uid", "" + ac.getUid());
								model.addAttribute("order_id", "" + ac.getOrder_id());
								list.add(ac);
							}
							session.setAttribute("ticketlist", list);
						} catch (Exception e) {
							e.printStackTrace();
						}
					} else {
						resp.setSuccess(false);
						resp.setCode("F00");
						resp.setMessage("Please try after some time,we will display ");
						resp.setResponse(APIUtils.getFailedJSON().toString());
						model.addAttribute("msg", resp.getMessage());

					}

				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("This card not for you,please become a user");
					resp.setResponse(APIUtils.getFailedJSON().toString());
					model.addAttribute("msg", resp.getMessage());
				}
			} else {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("User authority null,It's not for you");
				resp.setResponse(APIUtils.getFailedJSON().toString());
				model.addAttribute("msg", resp.getMessage());
				resp.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			return "redirect:/Home";
		}

		return "User/Adlabs/AdlabsUserDetail";
	}

	@RequestMapping(value = "/AdlabsUserDetail", method = RequestMethod.GET)
	public String getUserDetail(HttpServletRequest request, HttpServletResponse response, HttpSession session,
								AdlabOrderCreateRequest dto) {
		AdlabsTicketListResponse resp = new AdlabsTicketListResponse();
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {

				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("User Authentication Failed");

					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("User Authority null");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Session null");
			resp.setResponse(APIUtils.getFailedJSON().toString());
			return "redirect:/Home";
		}
		return "User/Adlabs/AdlabsUserDetail";
	}

	@RequestMapping(value = "/Payment", method = RequestMethod.POST)
	public String postpayment(HttpServletRequest request, HttpServletResponse response, HttpSession session,
							  Model model, @ModelAttribute AdlabsPaymentRequest dto) throws JSONException {

		AdlabsTicketListResponse resp = new AdlabsTicketListResponse();

		dto.setAccessToken((String) session.getAttribute("acessToken"));

		dto.setSession_name((String) session.getAttribute("session_name"));
		dto.setAmount((String) session.getAttribute("amount"));
		dto.setUid((String) session.getAttribute("uid"));
		dto.setOrder_id((String) session.getAttribute("order_id"));
		System.err.println("AMOUNT INSIDE  inititate Controller " + dto.getAmount());
		dto.setUid(dto.getUid());
		dto.setOrder_id(dto.getOrder_id());

		System.err.println("UID:::::::::::::::::::" + dto.getUid() + "orderid++++++++++++++++++++" + dto.getOrder_id());
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED))

				{
					AdlabsOrderVoucher ov = new AdlabsOrderVoucher();
					ov.setSessioniId((String) session.getAttribute("sessionId"));
					AdlabsAmountInitateRequest objinit = new AdlabsAmountInitateRequest();
					AdlabsAmountsuccessRequest objsuucess = new AdlabsAmountsuccessRequest();
					objinit.setAmount(dto.getAmount());
					System.err.println("AMOUNT INSIDE  inititate Controller " + objinit.getAmount());
					objinit.setSessioniId(sessionId);
					// ov.setAmount(request.getDenomination());
					// ov.setSessioniId(sessionId);
					// String accesstoken = giftCartApi.GetaccessToken();
					// request.setAccessToken(accesstoken);
					String data = "";

					// amount initate api calling
					AdlabsAmountInitateResponse respinitate = adLabsApi.initate(objinit);
					System.err.println("INITATE AMOUNT RESPONSE::::::::::::" + respinitate.getDetails());
					System.err.println("INITATE AMOUNT RESPONSE transaction id::::::::::::" + respinitate.getTxnId());

					dto.setTransaction_id(respinitate.getTxnId());

					if (respinitate.getCode().equalsIgnoreCase("S00")) {
						// AdlabsOrderCreateResponse add = new
						// AdlabsOrderCreateResponse();
						
						AdlabsOrderCreateResponse addOrder = adLabsApi.payment(dto);
						System.err.println("Payment  API RESPONSE" + addOrder.getMessage());

						JSONObject jsonObject = new JSONObject(addOrder.getMessage());
						if (jsonObject.has("error")) {
							ov.setTransactionRefNo(respinitate.getTxnId());
							ov.setCode("F04");
							AdlabsProceedResponse success = adLabsApi.sucees(ov);
							resp.setSuccess(false);
							resp.setCode("F00");
							resp.setMessage("Your order  not placed, please try after some time");
							model.addAttribute("msg", resp.getMessage());
							// resp.setStatus("Failure");
							resp.setResponse(APIUtils.getFailedJSON().toString());

						}

						else {
							try {

								AdlabsAddOrder ao = new AdlabsAddOrder();
								String success_profile = jsonObject.getString("success_profile");
								String success_payment = jsonObject.getString("success_payment");

								ov.setSuccess_payment(success_payment);
								ov.setSuccess_profile(success_profile);
								ov.setTransactionRefNo(respinitate.getTxnId());
								ov.setAmount(dto.getAmount());

								System.err.println("AMOUNT :::::::::::::::::"+dto.getAmount());
								ov.setCode("S00");
								objsuucess.setSessioniId(sessionId);
								// objsuucess.setReceiptno(receiptNo);

								try {

									// Amount Success Api Calling
									AdlabsProceedResponse success = adLabsApi.sucees(ov);
									success.setSessioniId((String) session.getAttribute("sessionId"));

									System.err.println("Amount Success API  RESPONSE" + success.getDetails());
									if (success.getCode().equalsIgnoreCase("S00")) {
										
										resp.setMessage(
												" Order placed successfully, Detail have been sent to your Enter mobile number and email id");
										model.addAttribute("msg", resp.getMessage());

									} else {
										ov.setTransactionRefNo(respinitate.getTxnId());
										ov.setCode("F04");
										AdlabsProceedResponse success1 = adLabsApi.sucees(ov);
										resp.setSuccess(false);
										resp.setCode("F00");
										resp.setMessage("" + success.getDetails());
										model.addAttribute("msg", "" + success.getDetails());
										// resp.setStatus("Failure");
										resp.setResponse(APIUtils.getFailedJSON().toString());

									}
								} catch (Exception e) {
									e.printStackTrace();
								}

							} catch (Exception e) {
								e.printStackTrace();
							}

						}

					} else {

						ov.setCode("F04");
						ov.setTransactionRefNo(respinitate.getTxnId());

						AdlabsProceedResponse success = adLabsApi.sucees(ov);
						// ov.setCode("F04");
						resp.setSuccess(false);
						resp.setCode("F00");
						resp.setDetails(resp.getDetails());
						resp.setMessage("" + respinitate.getDetails());
						model.addAttribute("msg", "" + respinitate.getDetails());
						// resp.setStatus("Failure");
						resp.setResponse(APIUtils.getFailedJSON().toString());
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("User authentication failed,please login again");
					model.addAttribute("msg", resp.getMessage());
					// resp.setStatus("Failure");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("User authority null,please login again");
				model.addAttribute("msg", resp.getMessage());
				// resp.setStatus("Failure");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			System.err.println(" HOME  REDIRECT");
			return "redirect:/Home";
		}

		return "User/Adlabs/AdlabsUserDetail";

	}
	///////////////////////////////////////////////////////// All Get Method
	///////////////////////////////////////////////////////// ////////////////////////

	@RequestMapping(value = "/SelectTicket", method = RequestMethod.GET)
	public String getSelectTicket(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
		AdlabsTicketListResponse resp = new AdlabsTicketListResponse();
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {

				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("User Authentication Failed");

					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("User Authority null");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Session null");
			resp.setResponse(APIUtils.getFailedJSON().toString());
			return "redirect:/Home";
		}
		return "User/Adlabs/EntertainmentDetail";
	}


	@RequestMapping(value = "/Payment", method = RequestMethod.GET)
	public String getpayment(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
		AdlabsTicketListResponse resp = new AdlabsTicketListResponse();
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {

				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("User Authentication Failed");

					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("User Authority null");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Session null");
			resp.setResponse(APIUtils.getFailedJSON().toString());
			return "redirect:/Home";
		}
		return "User/Adlabs/AdlabsUserDetail";

	}













}