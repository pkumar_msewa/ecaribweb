package com.payqwikweb.controller.web.thirdparty;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jettison.json.JSONObject;
import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.payqwikweb.api.IAuthenticationApi;
import com.payqwikweb.api.IGifCartAPI;
import com.payqwikweb.app.api.IGiftCartAPI;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.model.app.request.GCIAddCartRequest;
import com.payqwikweb.model.app.request.GCIAddOrderRequest;
import com.payqwikweb.model.app.request.GCIAmountInitateRequest;
import com.payqwikweb.model.app.request.GCIAmountsuccessRequest;
import com.payqwikweb.model.app.request.GCIOrderProductRequest;
import com.payqwikweb.model.app.request.GCIOrderVoucherRequest;
import com.payqwikweb.model.app.request.GCIProceedRequest;
import com.payqwikweb.model.app.request.GetBrandDenominationRequest;
import com.payqwikweb.model.app.response.GCIAddCartResponse;
import com.payqwikweb.model.app.response.GCIAddOrder;
import com.payqwikweb.model.app.response.GCIAddOrderResponse;
import com.payqwikweb.model.app.response.GCIAmountInitateResponse;
import com.payqwikweb.model.app.response.GCIBrandDenomination;
import com.payqwikweb.model.app.response.GCIBrandDenominationResponse;
import com.payqwikweb.model.app.response.GCIOrderVoucher;
import com.payqwikweb.model.app.response.GCIOrderVoucherResponse;
import com.payqwikweb.model.app.response.GCIProceedResponse;
import com.payqwikweb.model.app.response.GCIResponse;
import com.payqwikweb.model.app.response.GcoiBrandsResponse;
import com.payqwikweb.util.APIUtils;
import com.payqwikweb.util.Authorities;

@Controller
@RequestMapping("/User/GciProducts")
public class Gcicontroller implements MessageSourceAware {
	private IGifCartAPI giftCartApi;
	private final IAuthenticationApi authenticationApi;

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	private MessageSource messageSource;

	public Gcicontroller(IGifCartAPI giftCartApi, IAuthenticationApi authenticationApi) {
		this.authenticationApi = authenticationApi;
		this.giftCartApi = giftCartApi;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	@RequestMapping(value = "/GetBrands", method = RequestMethod.GET)
	public String GetBrands(@RequestHeader(value = "hash", required = false) String hash, Model model,
							HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {

		String data = "";
		GCIResponse resp = new GCIResponse();

		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED))

				{
					String accesstoken = giftCartApi.GetaccessToken();
					String getbrands = giftCartApi.GetBrand(accesstoken);
					String denominationlength = getbrands.toString();
					if (denominationlength.length() > 50) {
						JSONArray brands = new JSONArray(getbrands);
						ArrayList<GcoiBrandsResponse> setthegci = new ArrayList<>();
						for (int i = 0; i < brands.length(); i++) {
							String getbrandsimage = brands.getJSONObject(i).getString("image");
							{
								GcoiBrandsResponse respbrandsgci = new GcoiBrandsResponse();
								String getbrandshash = brands.getJSONObject(i).getString("hash");
								String getbrandsname = brands.getJSONObject(i).getString("name");
								String getbrandsdescription = brands.getJSONObject(i).getString("description");
								String getbrandsterms = brands.getJSONObject(i).getString("terms");
								String getbrandswebsite = brands.getJSONObject(i).getString("website");
								respbrandsgci.setHash(getbrandshash);
								respbrandsgci.setName(getbrandsname);
								respbrandsgci.setTerms(getbrandsterms);
								respbrandsgci.setImage(getbrandsimage);
								respbrandsgci.setWebsite(getbrandswebsite);
								respbrandsgci.setI("" + i);
								setthegci.add(respbrandsgci);
							}

						}

						session.setAttribute("Gcibrands", setthegci);

					} else {
						resp.setSuccess(false);
						resp.setCode("F00");
						resp.setMessage("Please try after some time");
						resp.setStatus("Failure");
						resp.setResponse(APIUtils.getFailedJSON().toString());
						model.addAttribute("msg", resp.getMessage());
					}

				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("This card not for you Please become a user");
					model.addAttribute("msg", resp.getMessage());
					resp.setStatus("Failure");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("User Authority null,Please become a user");
				model.addAttribute("msg", resp.getMessage());
				resp.setStatus("Failure");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {

			return "redirect:/Home";

		}

		return "User/GCI/Giftcart";
	}

	@RequestMapping(value = "/BrandDenominations", method = RequestMethod.POST)
	String denomination(Model model, HttpServletRequest request, HttpServletResponse response,
						GetBrandDenominationRequest request1, HttpSession session) throws Exception {
		String data = "";
		GCIResponse resp = new GCIResponse();
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED))

				{
					String accesstoken = giftCartApi.GetaccessToken();
					request1.setAccessToken(accesstoken);
					GCIBrandDenominationResponse denomation = giftCartApi.denomation(request1);
					String denominationlength = denomation.toString();
					System.err.println(""+denomation.getMessage());
					System.err.println("gegewwgftwgtfrewgtregt"+((denomation.getError())));
					if (denominationlength.length()>65) {
						ArrayList<GCIBrandDenomination> list = new ArrayList<GCIBrandDenomination>();
						try {
							JSONObject jsonObject = new JSONObject(denomation.getMessage());
							org.codehaus.jettison.json.JSONArray jsonArray = jsonObject.getJSONArray("denominations");
							for (int i = 0; i < jsonArray.length(); i++) {
								GCIBrandDenomination gbd = new GCIBrandDenomination();
								String id = jsonArray.getJSONObject(i).getString("id");
								String name, skuId;
								if (jsonArray.getJSONObject(i).has("name")) {
									name = jsonArray.getJSONObject(i).getString("name");
								} else {
									name = "";
								}
								if (jsonArray.getJSONObject(i).has("skuId")) {
									skuId = jsonArray.getJSONObject(i).getString("skuId");
								} else {
									skuId = "";
								}
								String type = jsonArray.getJSONObject(i).getString("type");
								String valueType = jsonArray.getJSONObject(i).getString("valueType");
								gbd.setImage(request1.getImage());
								gbd.setId(id);
								gbd.setBrandnane(request1.getName());
								gbd.setName(name);
								gbd.setSkuId(skuId);
								gbd.setType(type);
								gbd.setValueType(valueType);
								gbd.setHash(request1.getHash());
								list.add(gbd);
								session.setAttribute("PriceList", list);
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					} else {
						resp.setSuccess(false);
						resp.setCode("F00");
						resp.setMessage("Please try after some time");
						resp.setStatus("Failure");
						resp.setResponse(APIUtils.getFailedJSON().toString());
						model.addAttribute("msg", resp.getMessage());
					}

				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("This card not for you, please become a user");
					model.addAttribute("msg", resp.getMessage());
					resp.setStatus("Failure");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("User authority null,Please become a user");
				model.addAttribute("msg", resp.getMessage());
				resp.setStatus("Failure");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			return "redirect:/Home";
		}

		return "User/GCI/BrandDenomination";
	}

	@RequestMapping(value = "/BrandDenominations", method = RequestMethod.GET)
	String denominationyuyu(Model model, HttpServletRequest request, HttpServletResponse response, HttpSession session)
			throws Exception {
		GCIResponse resp = new GCIResponse();
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {

				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("User authentication failed, please become a user");
					model.addAttribute("msg", resp.getMessage());
					resp.setStatus("Failure");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("User authority null,please become a user");
				model.addAttribute("msg", resp.getMessage());
				resp.setStatus("Failure");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			return "redirect:/Home";
		}

		return "User/GCI/BrandDenomination";
	}

	@RequestMapping(value = "/Order", method = RequestMethod.POST)
	String order(Model model, HttpServletResponse response, HttpSession session, GCIOrderProductRequest request)
			throws Exception {
		GCIResponse resp = new GCIResponse();
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
					model.addAttribute("quantity", request.getQuantity());
					model.addAttribute("brandHash", request.getBrandHash());
					model.addAttribute("amount", request.getDenomination());
					model.addAttribute("productType", request.getProductType());
					model.addAttribute("productId", request.getProductId());
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("User authentication failed,please login again..");
					model.addAttribute("msg", resp.getMessage());
					resp.setStatus("Failure");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("User authority null,please become a user");
				model.addAttribute("msg", resp.getMessage());
				resp.setStatus("Failure");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			return "redirect:/Home";
		}

		return "User/GCI/Order";
	}

	@RequestMapping(value = "/AddOrder", method = RequestMethod.POST)
	String addorder(Model model, HttpServletResponse response, HttpServletRequest request1, GCIAddOrderRequest request,
					HttpSession session) throws Exception {

		GCIResponse resp = new GCIResponse();

		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
					GCIOrderVoucher ov = new GCIOrderVoucher();
					GCIAmountInitateRequest objinit = new GCIAmountInitateRequest();
					GCIAmountsuccessRequest objsuucess = new GCIAmountsuccessRequest();
					objinit.setAmount(request.getDenomination());
					objinit.setSessioniId(sessionId);
					ov.setAmount(request.getDenomination());
					ov.setSessioniId(sessionId);
					String accesstoken = giftCartApi.GetaccessToken();
					request.setAccessToken(accesstoken);
					String data = "";
					String denominationlength = accesstoken.toString();
					if (denominationlength.length() > 20) {
						// amount initate api calling
						GCIAmountInitateResponse respinitate = giftCartApi.initate(objinit);
						System.err.println("INITATE AMOUNT RESPONSE::::::::::::"+respinitate.getDetails());
						
						request.setClientOrderId(respinitate.getTxnId());
						
						if (respinitate.getCode().equalsIgnoreCase("S00")) {
							GCIAddOrderResponse add = new GCIAddOrderResponse();
							GCIAddOrderResponse addOrder = giftCartApi.addOrder(request);
							System.err.println("ADD ORDER API RESPONSE"+addOrder.getMessage());
							//String denominationlength2 = addOrder.toString();
							//ov.setTransactionRefNo(respinitate.getTxnId());
							JSONObject jsonObject = new JSONObject(addOrder.getMessage());
							if (jsonObject.has("error")) {
								ov.setTransactionRefNo(respinitate.getTxnId());
								ov.setCode("F04");
								GCIProceedResponse success = giftCartApi.sucees(ov);
								resp.setSuccess(false);
								resp.setCode("F00");
								resp.setMessage("Your order  not placed, please try after some time");
								model.addAttribute("msg", resp.getMessage());
								resp.setStatus("Failure");
								resp.setResponse(APIUtils.getFailedJSON().toString());
								//	if(denominationlength2.length()>60)
								//{
							}

							else{
								try {
									/*JSONObject jsonObject = new JSONObject(addOrder.getMessage());*/
									GCIAddOrder ao = new GCIAddOrder();
									String receiptNo = jsonObject.getString("receiptNo");
									ao.setReceiptNo(receiptNo);
									ov.setReceiptno(receiptNo);
									ov.setTransactionRefNo(respinitate.getTxnId());
									ov.setCode("S00");
									objsuucess.setSessioniId(sessionId);
									objsuucess.setReceiptno(receiptNo);
									// voucher Api Calling
									GCIOrderVoucherRequest voucherobj = new GCIOrderVoucherRequest();
									String accesstoken1 = giftCartApi.GetaccessToken();
									voucherobj.setAccessToken(accesstoken);
									voucherobj.setReceiptNo(receiptNo);
									GCIOrderVoucherResponse voucher = giftCartApi.voucher(voucherobj);
									System.err.println("VOUCHER API  RESPONSE"+voucher.getMessage());
									String denominationlength3 = voucher.toString();
									System.err.println("VOUCHER API  RESPONSE length"+denominationlength3.length());
									if (jsonObject.has("error"))
									{
										ov.setCode("F04");
										ov.setTransactionRefNo(respinitate.getTxnId());

										GCIProceedResponse success = giftCartApi.sucees(ov);

										resp.setSuccess(false);
										resp.setCode("F00");
										resp.setMessage("Your order  not placed, please try after some time voucher");
										model.addAttribute("msg", resp.getMessage());
										resp.setStatus("Failure");
										resp.setResponse(APIUtils.getFailedJSON().toString());
									}
									else{

										try {
											JSONArray ja = new JSONArray(voucher.getMessage());
											for (int i = 0; i < ja.length(); i++) {
												String brandName = ja.getJSONObject(i).getString("brandName");
												String denomination = ja.getJSONObject(i).getString("cardPrice");
												String voucherNumber = ja.getJSONObject(i).getString("voucherNumber");
												String voucherPin = ja.getJSONObject(i).getString("voucherPin");
												String expiryDate = ja.getJSONObject(i).getString("expiryDate");
												ov.setBrandName(brandName);
												ov.setDenomination(denomination);
												ov.setVoucherNumber(voucherNumber);
												ov.setVoucherPin(voucherPin);
												ov.setExpiryDate(expiryDate);
												model.addAttribute("brandName", ov.getBrandName());
												model.addAttribute("denomination", ov.getDenomination());
												model.addAttribute("voucherNumber", ov.getVoucherNumber());
												model.addAttribute("voucherPin", ov.getVoucherPin());
												model.addAttribute("expiryDate", ov.getExpiryDate());
											}
											// Amount Success Api Calling
											GCIProceedResponse success = giftCartApi.sucees(ov);
											System.err.println("VOUCHER API  RESPONSE"+success.getDetails());
											if (success.getCode().equalsIgnoreCase("S00")) {

												resp.setMessage(
														" Order placed successfully,Card detail have been sent to your registerd mobile number and email id");
												model.addAttribute("msg", resp.getMessage());

											} else {


												resp.setSuccess(false);
												resp.setCode("F00");
												resp.setMessage(""+success.getDetails());
												model.addAttribute("msg", ""+success.getDetails());
												resp.setStatus("Failure");
												resp.setResponse(APIUtils.getFailedJSON().toString());

											}
										} catch (Exception e) {
											e.printStackTrace();
										}
										model.addAttribute("receiptNo", receiptNo);
										//} else {
										/*ov.setCode("F04");
										GCIProceedResponse success = giftCartApi.sucees(ov);
										resp.setSuccess(false);
										resp.setCode("F00");
										resp.setMessage("There is no voucher pin,please load money..or login Again");
										model.addAttribute("msg", resp.getMessage());
										resp.setStatus("Failure");
										resp.setResponse(APIUtils.getFailedJSON().toString());
*/									//}
									}} catch (Exception e) {
									e.printStackTrace();
								}

								//} else {

								/*resp.setSuccess(false);
								//resp.setCode("F00");
								//resp.setMessage("Your order  not placed, please try after some time");
								//model.addAttribute("msg",resp.getMessage());
								//resp.setStatus("Failure");
								//resp.setResponse(APIUtils.getFailedJSON().toString());

							//}
*/
							}

						} else {

							ov.setCode("F04");
							ov.setTransactionRefNo(respinitate.getTxnId());

							GCIProceedResponse success = giftCartApi.sucees(ov);
							//ov.setCode("F04");
							resp.setSuccess(false);
							resp.setCode("F00");
							resp.setDetails(resp.getDetails());
							resp.setMessage(""+respinitate.getDetails());
							model.addAttribute("msg", ""+respinitate.getDetails());
							resp.setStatus("Failure");
							resp.setResponse(APIUtils.getFailedJSON().toString());
						}
					} else {
						resp.setSuccess(false);
						resp.setCode("F00");
						resp.setMessage("</p>Invalid access token please login again");
						model.addAttribute("msg", resp.getMessage());
						resp.setStatus("Failure");
						resp.setResponse(APIUtils.getFailedJSON().toString());
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("User authentication failed,please login again");
					model.addAttribute("msg", resp.getMessage());
					resp.setStatus("Failure");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("User authority null,please login again");
				model.addAttribute("msg", resp.getMessage());
				resp.setStatus("Failure");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			System.err.println(" HOME  REDIRECT");
			return "redirect:/Home";
		}

		return "User/GCI/Order";

	}

	@RequestMapping(value = "/AddCart", method = RequestMethod.POST)
	String addCart(@RequestBody GCIAddCartRequest request, Model model, HttpSession session) {

		GCIResponse resp = new GCIResponse();

		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED))

				{
					request.setSessioniId(sessionId);
					GCIAddCartResponse result = new GCIAddCartResponse();
					result = giftCartApi.addcart(request);
					String denominationlength = result.toString();
					if (result.getCode().equalsIgnoreCase("S00")) {
						try {

							JSONObject jsonObject = new JSONObject(result.getMessage());
							String quantity = jsonObject.getString("quantity");
							result.setQuantity(quantity);
							model.addAttribute("quantity", result.getQuantity());

						} catch (Exception e) {
						}
					}

					else {
						resp.setSuccess(false);
						resp.setCode("F00");
						resp.setMessage("Please try after some time,login again");
						model.addAttribute("msg", resp.getMessage());
						resp.setStatus("Failure");
						resp.setResponse(APIUtils.getFailedJSON().toString());
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("User authentication failed,login again");
					model.addAttribute("msg", resp.getMessage());
					resp.setStatus("Failure");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("User authority null,login again");
				model.addAttribute("msg", resp.getMessage());
				resp.setStatus("Failure");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {

			return "redirect:/Home";
		}

		return "User/GCI/BrandDenomination";
	}

	@RequestMapping(value = "/ShowCart", method = RequestMethod.GET)
	String showcart(Model model, HttpServletResponse response, HttpSession session) throws Exception {
		GCIResponse resp = new GCIResponse();
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED))

				{
					GCIAddCartRequest request = new GCIAddCartRequest();
					String data = "";
					ArrayList<GCIAddCartResponse> list = new ArrayList<GCIAddCartResponse>();
					request.setSessioniId(sessionId);
					GCIAddCartResponse showcart = giftCartApi.showcart(request);
					String denominationlength = showcart.toString();
					if (showcart.getCode().equalsIgnoreCase("S00"))  {
						try {
							JSONObject jsonObject = new JSONObject(showcart.getResponse());
							org.codehaus.jettison.json.JSONArray jsonArray = jsonObject.getJSONArray("details");
							for (int i = 0; i < jsonArray.length(); i++) {
								GCIAddCartResponse ac = new GCIAddCartResponse();
								String productType = jsonArray.getJSONObject(i).getString("productType");
								String quantity = jsonArray.getJSONObject(i).getString("quantity");
								String brandName = jsonArray.getJSONObject(i).getString("brandName");
								String totalamount = jsonArray.getJSONObject(i).getString("totalamount");
								String brandHash = jsonArray.getJSONObject(i).getString("brandHash");
								String amount = jsonArray.getJSONObject(i).getString("amount");
								String imagepath = jsonArray.getJSONObject(i).getString("imagepath");
								String productid = jsonArray.getJSONObject(i).getString("productid");
								String skuId = jsonArray.getJSONObject(i).getString("skuId");
								ac.setImagepath(imagepath);
								ac.setProductId(productid);
								ac.setSkuId(skuId);
								ac.setProductType(productType);
								ac.setQuantity(quantity);
								ac.setBrandName(brandName);
								ac.setTotalamount(totalamount);
								ac.setBrandHash(brandHash);
								ac.setAmount(amount);
								list.add(ac);
							}
							model.addAttribute("ShowCart", list);
						} catch (Exception e) {
							e.printStackTrace();
						}
					} else {
						resp.setSuccess(false);
						resp.setCode("F00");
						resp.setMessage("Please try after some time,we will display ");
						resp.setStatus("Failure");
						resp.setResponse(APIUtils.getFailedJSON().toString());
						model.addAttribute("msg", resp.getMessage());

					}

				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("This card not for you,please become a user");
					resp.setStatus("Failure");
					resp.setResponse(APIUtils.getFailedJSON().toString());
					model.addAttribute("msg", resp.getMessage());
				}
			} else {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("User authority null,It's not for you");
				resp.setStatus("Failure");
				resp.setResponse(APIUtils.getFailedJSON().toString());
				model.addAttribute("msg", resp.getMessage());
				resp.setStatus("Failure");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			return "redirect:/Home";
		}

		return "User/GCI/ShowCart";
	}

	@RequestMapping(value = "/Remove", method = RequestMethod.POST)
	String remove(Model model, GCIProceedRequest request, HttpServletResponse response, HttpSession session)
			throws Exception {

		GCIResponse resp = new GCIResponse();

		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED))

				{
					String data = "";
					request.setSessionId(sessionId);
					GCIProceedResponse proceed = giftCartApi.proceed(request);
					String denominationlength = proceed.toString();
					if (proceed.getCode().equalsIgnoreCase("S00")) {
						try {
							ArrayList<GCIProceedResponse> list = new ArrayList<GCIProceedResponse>();
							JSONObject jsonObject = new JSONObject(proceed.getResponse());
							org.codehaus.jettison.json.JSONArray jsonArray = jsonObject.getJSONArray("details");
							for (int i = 0; i < jsonArray.length(); i++) {
								GCIProceedResponse ac = new GCIProceedResponse();
								String productType = jsonArray.getJSONObject(i).getString("productType");
								String quantity = jsonArray.getJSONObject(i).getString("quantity");
								String brandName = jsonArray.getJSONObject(i).getString("brandName");
								String totalamount = jsonArray.getJSONObject(i).getString("totalamount");
								String brandHash = jsonArray.getJSONObject(i).getString("brandHash");
								String amount = jsonArray.getJSONObject(i).getString("amount");
								String imagepath = jsonArray.getJSONObject(i).getString("imagepath");
								String productid = jsonArray.getJSONObject(i).getString("productid");
								String skuId = jsonArray.getJSONObject(i).getString("skuId");
								ac.setImagepath(imagepath);
								ac.setProductId(productid);
								ac.setSkuId(skuId);
								ac.setProductType(productType);
								ac.setQuantity(quantity);
								ac.setBrandName(brandName);
								ac.setTotalamount(totalamount);
								ac.setBrandHash(brandHash);
								ac.setAmount(amount);
								list.add(ac);
							}
							model.addAttribute("ShowCart", list);
						} catch (Exception e) {
							e.printStackTrace();
						}
					} else {
						resp.setSuccess(false);
						resp.setCode("F00");
						resp.setMessage("Please login again client authentication failed");
						model.addAttribute("msg", resp.getMessage());
						resp.setStatus("Failure");
						resp.setResponse(APIUtils.getFailedJSON().toString());
					}

				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("User authentication failed,login again");
					model.addAttribute("msg", resp.getMessage());
					resp.setStatus("Failure");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("User authority null,login again");
				model.addAttribute("msg", resp.getMessage());
				resp.setStatus("Failure");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			return "redirect:/Home";
		}

		return "User/GCI/ShowCart";
	}

	///////////////////////////////////////////// /////////////////////all get
	///////////////////////////////////////////// method
	@RequestMapping(value = "/AddOrder", method = RequestMethod.GET)
	String addorder(Model model, HttpServletRequest request, HttpServletResponse response, HttpSession session)
			throws Exception {
		GCIResponse resp = new GCIResponse();
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED))

				{

				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("User authentication failed,please become a user");
					model.addAttribute("msg", resp.getMessage());
					resp.setStatus("Failure");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("User authority null,please become a user");
				model.addAttribute("msg", resp.getMessage());
				resp.setStatus("Failure");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			return "redirect:/Home";
		}

		return "User/GCI/Order";
	}

	@RequestMapping(value = "/Order", method = RequestMethod.GET)
	String order(Model model, HttpServletRequest request, HttpServletResponse response, HttpSession session)
			throws Exception {
		GCIResponse resp = new GCIResponse();
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {

				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("User authentication failed  please become a user");
					model.addAttribute("msg", resp.getMessage());
					resp.setStatus("Failure");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("User authority null,please become a User");
				model.addAttribute("msg", resp.getMessage());
				resp.setStatus("Failure");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			return "redirect:/Home";
		}

		return "User/GCI/Order";
	}

	@RequestMapping(value = "/Remove", method = RequestMethod.GET)
	String removeget(Model model, GCIProceedRequest request, HttpServletResponse response, HttpSession session)
			throws Exception {
		GCIResponse resp = new GCIResponse();
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
					String data = "";
					request.setSessionId(sessionId);
					GCIProceedResponse proceed = giftCartApi.proceed(request);
					String denominationlength = proceed.toString();
					if (denominationlength.length() > 50) {
						try {
							ArrayList<GCIProceedResponse> list = new ArrayList<GCIProceedResponse>();
							JSONObject jsonObject = new JSONObject(proceed.getResponse());
							org.codehaus.jettison.json.JSONArray jsonArray = jsonObject.getJSONArray("details");
							for (int i = 0; i < jsonArray.length(); i++) {
								GCIProceedResponse ac = new GCIProceedResponse();
								String productType = jsonArray.getJSONObject(i).getString("productType");
								String quantity = jsonArray.getJSONObject(i).getString("quantity");
								String brandName = jsonArray.getJSONObject(i).getString("brandName");
								String totalamount = jsonArray.getJSONObject(i).getString("totalamount");
								String brandHash = jsonArray.getJSONObject(i).getString("brandHash");
								String amount = jsonArray.getJSONObject(i).getString("amount");
								String imagepath = jsonArray.getJSONObject(i).getString("imagepath");
								String productid = jsonArray.getJSONObject(i).getString("productid");
								String skuId = jsonArray.getJSONObject(i).getString("skuId");
								ac.setImagepath(imagepath);
								ac.setProductId(productid);
								ac.setSkuId(skuId);
								ac.setProductType(productType);
								ac.setQuantity(quantity);
								ac.setBrandName(brandName);
								ac.setTotalamount(totalamount);
								ac.setBrandHash(brandHash);
								ac.setAmount(amount);
								list.add(ac);
							}
							model.addAttribute("ShowCart", list);
						} catch (Exception e) {
							e.printStackTrace();
						}
					} else {
						resp.setSuccess(false);
						resp.setCode("F00");
						resp.setMessage("Please login again client authentication failed,pogin again");
						model.addAttribute("msg", resp.getMessage());
						resp.setStatus("Failure");
						resp.setResponse(APIUtils.getFailedJSON().toString());
					}

				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("User authentication failed,login again");
					model.addAttribute("msg", resp.getMessage());
					resp.setStatus("Failure");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("User authority null,login again");
				model.addAttribute("msg", resp.getMessage());
				resp.setStatus("Failure");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			return "redirect:/Home";
		}

		return "User/GCI/ShowCart";
	}


	///////////////////////    get brand post method
	@RequestMapping(value = "/GetBrandsInPost", method = RequestMethod.POST)
	public ResponseEntity<String> GetBrand(@RequestHeader(value = "hash", required = false) String hash, Model model,
										   HttpServletRequest request,@RequestBody GcoiBrandsResponse term,  HttpServletResponse response, HttpSession session) throws Exception {

		String data = "";
		String t = "";


		GCIResponse resp = new GCIResponse();

		System.err.println("terms:++++++++==========::::::::"+term.getHash());
		t= ""+term.getHash();
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED))

				{
					String accesstoken = giftCartApi.GetaccessToken();
					String getbrands = giftCartApi.GetBrand(accesstoken);
					String denominationlength = getbrands.toString();
					if (denominationlength.length() > 50) {
						JSONArray brands = new JSONArray(getbrands);
						ArrayList<GcoiBrandsResponse> setthegci = new ArrayList<>();
						for (int i = 0; i < brands.length(); i++) {
							String getbrandsimage = brands.getJSONObject(i).getString("image");
							{
								GcoiBrandsResponse respbrandsgci = new GcoiBrandsResponse();


								String getbrandshash = brands.getJSONObject(i).getString("hash");

								if(t.equals(getbrandshash))
								{
									String getbrandsterms = brands.getJSONObject(i).getString("terms");
									respbrandsgci.setTerms(getbrandsterms);
									//respbrandsgci.setI("" + i);
									setthegci.add(respbrandsgci);
									data+=getbrandsterms;
									//System.err.println("TERMS::::::::++++++========"+getbrandsterms);
								}

							}

						}

						session.setAttribute("Gciterms", setthegci);

					} else {
						resp.setSuccess(false);
						resp.setCode("F00");
						resp.setMessage("Please try after some time");
						resp.setStatus("Failure");
						resp.setResponse(APIUtils.getFailedJSON().toString());
						model.addAttribute("msg", resp.getMessage());
					}

				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("This card not for you Please become a user");
					model.addAttribute("msg", resp.getMessage());
					resp.setStatus("Failure");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("User Authority null,Please become a user");
				model.addAttribute("msg", resp.getMessage());
				resp.setStatus("Failure");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {

			return new ResponseEntity<String>(data , HttpStatus.OK);

		}

		return new ResponseEntity<String>(""+data , HttpStatus.OK);

	}




}