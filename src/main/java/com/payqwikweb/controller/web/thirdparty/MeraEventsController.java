package com.payqwikweb.controller.web.thirdparty;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.payqwikweb.api.IAuthenticationApi;
import com.payqwikweb.app.api.IMeraEventsApi;
import com.payqwikweb.app.api.ITransactionApi;
import com.payqwikweb.app.model.ResponseStatus;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.request.MeraEventDetailsRequest;
import com.payqwikweb.app.model.request.MeraEventsBookingRequest;
import com.payqwikweb.app.model.request.MeraEventsCommonRequest;
import com.payqwikweb.app.model.request.MeraEventsListRequest;
import com.payqwikweb.app.model.request.TransactionRequest;
import com.payqwikweb.app.model.response.MeraEventCategoryListResponse;
import com.payqwikweb.app.model.response.MeraEventCityListResponse;
import com.payqwikweb.app.model.response.MeraEventTicketCalculationResponse;
import com.payqwikweb.app.model.response.MeraEventsAttendeeFormRequest;
import com.payqwikweb.app.model.response.MeraEventsListResponse;
import com.payqwikweb.app.model.response.MeraEventsResponse;
import com.payqwikweb.app.model.response.MeraEventsTicketDetailsResponse;
import com.payqwikweb.controller.mobile.api.thirdparty.MeraEventAmountInitateRequest;
import com.payqwikweb.util.APIUtils;
import com.payqwikweb.util.Authorities;
import com.payqwikweb.util.LogCat;
import com.thirdparty.model.ResponseDTO;
///////////////////////////// SONU GUPTA ////////////////////////////////////
@Controller
@RequestMapping("/MeraEvents")
public class MeraEventsController implements MessageSourceAware {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private MessageSource messageSource;
	String orderId = "";
	String eventSignUpID = "";
	double netAmount = 0.0;
	String quantity = "";
	
	private final IAuthenticationApi authenticationApi;
	private final IMeraEventsApi meraEventsApi;
	private final ITransactionApi transactionApi;

	public MeraEventsController(IAuthenticationApi authenticationApi, IMeraEventsApi meraEventsApi,
			ITransactionApi transactionApi) {
		this.authenticationApi = authenticationApi;
		this.meraEventsApi = meraEventsApi;
		this.transactionApi = transactionApi;
	}

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	@RequestMapping(value = "/AuthCode", method = RequestMethod.GET)
	public String getAccessToken(@ModelAttribute MeraEventsCommonRequest dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		MeraEventsResponse resp = new MeraEventsResponse();
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionId);
					//dto.setClientId(APIUtils.ClientID);
					//resp = meraEventsApi.getAuthorizationCode(dto);
					//session.setAttribute("authenticationCode", resp.getResponse());
	/*				if (resp.getCode().equalsIgnoreCase("S00")) {
						dto.setAuthorizationCode((String) session.getAttribute("authenticationCode"));
						dto.setClientSecret(APIUtils.ClientSecret);
						resp = meraEventsApi.getAccessToken(dto);
						if (resp.getCode().equalsIgnoreCase("S00")) {
							session.setAttribute("accessToken", resp.getResponse());
							dto.setAccess_token(resp.getResponse());
							resp = meraEventsApi.saveAccessToken(dto);
							if (resp.getCode().equalsIgnoreCase("S00")) {
								resp.setCode("S00");
								resp.setSuccess(true);
								resp.setStatus("Success");
								resp.setMessage("Access token saved");
								resp.setResponse(resp.getResponse());
							} else {
								resp.setCode("F00");
								resp.setSuccess(false);
								resp.setStatus("Failure");
								resp.setMessage("Unable to save access Token");
								resp.setResponse(resp.getResponse());
							}
						} else {
							resp.setCode("F00");
							resp.setSuccess(false);
							resp.setStatus("Failure");
							resp.setMessage("Unable to generate access Token");
							resp.setResponse(resp.getResponse());
						}
						if (resp.getCode().equalsIgnoreCase("S00")) {
							session.setAttribute("accessToken", resp.getResponse());
							resp.setCode("S00");
							resp.setSuccess(true);
							resp.setStatus("Success");
							resp.setMessage("Access token generated");
							resp.setResponse(resp.getResponse());
						} else {
							resp.setSuccess(false);
							resp.setCode("F00");
							resp.setMessage("Access Denied");
							resp.setStatus("Failure");
							resp.setResponse(APIUtils.getFailedJSON().toString());
						}
					} else {
						resp.setSuccess(false);
						resp.setCode("F00");
						resp.setMessage("Client Authentication Failed");
						resp.setStatus("Failure");
						resp.setResponse(APIUtils.getFailedJSON().toString());
					}*/
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("User Authentication Failed");
					resp.setStatus("Failure");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("User Authority null");
				resp.setStatus("Failure");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Session null");
			resp.setStatus("Failure");
			resp.setResponse(APIUtils.getFailedJSON().toString());
			return "redirect:/Home";
		}
		return "User/MeraEvent/events";
	}

	@RequestMapping(value = "/CategoryList", method = RequestMethod.GET)
	public @ResponseBody String categoryList(@ModelAttribute MeraEventsCommonRequest dto,
			HttpServletRequest request,	HttpServletResponse response, HttpSession session) throws JSONException {
		MeraEventsResponse result = new MeraEventsResponse();

		System.err.println("INSIDE CategoryList controller  ");
		String id="";
		String name = "";
		String categories = "";
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionId);
					MeraEventCategoryListResponse resp = meraEventsApi.getEventCategory(dto);
					System.err.println("INSIDE CategoryList controller API RESPONSE  "+resp.getResponse());
					JSONArray array = new JSONArray(resp.getResponse());
					//JSONArray array = (JSONArray) resp.getJsonArray();
					List<MeraEventCategoryListResponse> categoryList = new ArrayList<>();
					if (resp.getCode().equalsIgnoreCase("S00")) {
						try {
							int count = 0;
							for (int i = 0; i < array.length(); i++) {
								JSONObject jsonObject = array.getJSONObject(i);
								count++;
								LogCat.print("" + count);
								id+="#"+(int)jsonObject.getInt("id");
								name+="$"+jsonObject.getString("name");
							}
							result.setCode("S00");
							result.setMessage("Category List : ");
							result.setSuccess(true);
							result.setStatus("Success");
							result.setDetails(array);
						} catch (Exception e) {
							e.printStackTrace();
						}
					} else {
						result.setSuccess(false);
						result.setCode("F00");
						result.setMessage("Client Authentication Failed");
						result.setStatus("Failure");
						result.setResponse(APIUtils.getFailedJSON().toString());
					}
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("User Authentication Failed");
					result.setStatus("Failure");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("User Authority null");
				result.setStatus("Failure");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("Session null");
			result.setStatus("Failure");
			result.setResponse(APIUtils.getFailedJSON().toString());
			return "redirect:/Home";
		}
		String id1="#"+0+id;
		String name1="$"+"All Categories"+name;
		categories = id1+"@@"+name1;
		return categories;
	}

	@RequestMapping(value = "/CityList", method = RequestMethod.POST)
	public @ResponseBody String getCities( MeraEventsCommonRequest dto,
			HttpServletRequest request1,HttpServletResponse response, HttpSession session) throws JSONException {
		
		MeraEventsResponse result = new MeraEventsResponse();
		String sessionId = (String) session.getAttribute("sessionId");
		String id="";
		String name="";
		String cities="";
		if (sessionId != null ) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionId);
					MeraEventCityListResponse resp = meraEventsApi.getCities(dto);
					JSONArray array = new JSONArray(resp.getResponse());
					List<MeraEventCityListResponse> cityList = new ArrayList<>();
					if (resp.getCode().equalsIgnoreCase("S00")) {
						try {
							int count = 0;
							for (int i = 0; i < array.length(); i++) {
								MeraEventCityListResponse list = new MeraEventCityListResponse();
								JSONObject jsonObject = array.getJSONObject(i);
								list.setCityID(jsonObject.getInt("id"));
								list.setCityName(jsonObject.getString("name"));
								list.setOrder(jsonObject.getInt("order"));
								list.setCountryId(jsonObject.getInt("countryId"));
								count++;
								LogCat.print("" + count);
								cityList.add(list);
								id += "#" + (int) jsonObject.getInt("id");
								name += "$" + jsonObject.getString("name");
							}
							result.setCode("S00");
							result.setMessage("City List : ");
							result.setSuccess(true);
							result.setStatus("Success");
							result.setDetails(cityList);
						} catch (Exception e) {
							e.printStackTrace();
						}
					} else {
						result.setSuccess(false);
						result.setCode("F00");
						result.setMessage("Client Authentication Failed");
						result.setStatus("Failure");
						result.setResponse(APIUtils.getFailedJSON().toString());
					}
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("User Authentication Failed");
					result.setStatus("Failure");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("User Authority null");
				result.setStatus("Failure");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("Session null");
			result.setStatus("Failure");
			result.setResponse(APIUtils.getFailedJSON().toString());
			return "redirect:/Home";
		}
		String id1="#"+0+id;
		System.err.println(id1);
		String name1="$"+"All Cities"+name;
		System.err.println(name1);
		cities=id1+"@@"+name1;
		return cities;
	}

	@RequestMapping(value = "/EventList", method = RequestMethod.POST)
	public String getEventList(@ModelAttribute("events") MeraEventsListRequest dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) {
		MeraEventsListResponse result = new MeraEventsListResponse();
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
					
					System.err.println("Category ID:::::::"+dto.getCategoryId());
					System.err.println("City ID:::::::"+dto.getCityId());
					dto.setSessionId(sessionId);
					//dto.setCountryId(14);
					//dto.setAccess_token((String)session.getAttribute("accessToken"));
					if(dto.getCategoryId() == 0 && dto.getCityId() == 0) {
					result = meraEventsApi.getEventList(dto);
					} else if(dto.getCategoryId() >0 && dto.getCityId() > 0) {
						result = meraEventsApi.getEventList(dto);
					} else if(dto.getCategoryId()>0 && dto.getCityId() == 0) {
						result = meraEventsApi.getEventList(dto);
					} else if(dto.getCategoryId() == 0 && dto.getCityId() > 0) {
						result = meraEventsApi.getEventList(dto);
					} 
					List<MeraEventsListResponse> eventList = new ArrayList<>();
					JSONArray array = (JSONArray) result.getJsonArray();
					if (result.getCode().equalsIgnoreCase("S00")) {
						try {
							if (array != null) {
								int count = 0;
								for (int i = 0; i < array.length(); i++) {
									MeraEventsListResponse list = new MeraEventsListResponse();
									JSONObject jsonObject = array.getJSONObject(i);
									list.setEventId(jsonObject.getString("id"));
									list.setTitle(jsonObject.getString("title"));
									list.setBannerImage(jsonObject.getString("bannerImage"));
								
									list.setStartDate(jsonObject.getString("startDate"));
									list.setEndDate(jsonObject.getString("endDate"));
									list.setCategoryName(jsonObject.getString("categoryName"));
									list.setRegistrationType(jsonObject.getString("registrationType"));
									list.setCityName(jsonObject.getString("cityName"));
									count++;
									LogCat.print("" + count);
									eventList.add(list);
									model.addAttribute("eventList", eventList);
								}
							}
							result.setJsonArray(eventList);
						} catch (Exception e) {
							e.printStackTrace();
						}
					} else {
						result.setSuccess(false);
						result.setCode("F00");
						result.setMessage(result.getMessage());
						result.setStatus(ResponseStatus.FAILURE);
						result.setResponse(APIUtils.getFailedJSON().toString());
						model.addAttribute("msg", result.getMessage());
					}
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("User Authentication Failed");
					result.setStatus(ResponseStatus.FAILURE);
					result.setResponse(APIUtils.getFailedJSON().toString());
					model.addAttribute("msg", result.getMessage());
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("User Authority null");
				result.setStatus(ResponseStatus.FAILURE);
				result.setResponse(APIUtils.getFailedJSON().toString());
				model.addAttribute("msg", result.getMessage());
			}
		} else {
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("Session null");
			result.setStatus(ResponseStatus.FAILURE);
			result.setResponse(APIUtils.getFailedJSON().toString());
			model.addAttribute("msg", result.getMessage());
			return "redirect:/Home";
		}
		return "User/MeraEvent/EventList";
	}

	@RequestMapping(value = "/EventList", method = RequestMethod.GET)
	String addorder(Model model, HttpServletRequest request, HttpServletResponse response, HttpSession session)
			throws Exception {
		MeraEventsListResponse resp = new MeraEventsListResponse();
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED))

				{

				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("User authentication failed,please become a user");
					model.addAttribute("msg", resp.getMessage());
					//resp.setStatus("Failure");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("User authority null,please become a user");
				model.addAttribute("msg", resp.getMessage());
				//resp.setStatus("Failure");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			return "redirect:/Home";
		}

		return "User/MeraEvent/EventList";
	}
	MeraEventsListResponse eventDetails = new MeraEventsListResponse();
	MeraEventsListResponse result1 = new MeraEventsListResponse();
	MeraEventsTicketDetailsResponse ticketresp = new MeraEventsTicketDetailsResponse();
	@RequestMapping(value = "/{eventId}/EventDetails", method = RequestMethod.GET)
	public String getEventDetails(@PathVariable int eventId,@ModelAttribute MeraEventsCommonRequest dto,
			HttpServletRequest request,HttpServletResponse response, HttpSession session, ModelMap model) throws JSONException {
		
		System.err.println("EVENT ID INSIDE EVENT DETAIL API "+dto.getEventId());
		String sessionId = (String) session.getAttribute("sessionId");
		MeraEventsListResponse result = new MeraEventsListResponse();
		if (sessionId != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionId);
					eventDetails = meraEventsApi.getEventDetails(dto);
					if (eventDetails.getCode().equalsIgnoreCase("S00")) {
						try {
							org.codehaus.jettison.json.JSONObject jsonObject = new org.codehaus.jettison.json.JSONObject(eventDetails.getResponse());
							  System.err.println(jsonObject);
							  result1.setEventId(jsonObject.getString("id"));
							  result1.setOwnerId(jsonObject.getInt("ownerId"));
							  result1.setStartDate(jsonObject.getString("startDate"));
							  result1.setEndDate(jsonObject.getString("endDate"));
							  result1.setTitle(jsonObject.getString("title"));
							  result1.setDetails(jsonObject.getString("description"));
							  result1.setEventStatus(jsonObject.getInt("status"));
							  result1.setCategoryName(jsonObject.getString("categoryName"));
							  result1.setSubCategoryName(jsonObject.getString("subCategoryName"));
							  result1.setEventUrl(jsonObject.getString("eventUrl"));
							  result1.setUrl(jsonObject.getString("url"));
							  result1.setBannerImage(jsonObject.getString("bannerPath"));
							  result1.setThumbImage(jsonObject.getString("thumbnailPath"));
							  result1.setBooknowButtonValue(jsonObject.getJSONObject("eventDetails").getString("bookButtonValue"));
							  result1.setCountryName(jsonObject.getJSONObject("location").getString("countryName"));
							  result1.setStateName(jsonObject.getJSONObject("location").getString("stateName"));
							  result1.setCityName(jsonObject.getJSONObject("location").getString("cityName")); 
							  result1.setVenueName(jsonObject.getJSONObject("location").getString("venueName"));
							  result1.setAddress1(jsonObject.getJSONObject("location").getString("address1"));
							  result1.setAddress2(jsonObject.getJSONObject("location").getString("address2")); 
							  result1.setTimeZone(jsonObject.getJSONObject("location").getString("timeZone")); 
							  result1.setTimeZoneName(jsonObject.getJSONObject("location").getString("timeZoneName"));
							  model.addAttribute("ed", result1);
						} catch (Exception e) {
							e.printStackTrace();
						}
					} else {
						result.setSuccess(false);
						result.setCode("F00");
						result.setMessage("Please try again later..");
						result.setStatus(ResponseStatus.FAILURE);
						result.setResponse(APIUtils.getFailedJSON().toString());
					}
			
					ticketresp = meraEventsApi.getEventTicketDetails(dto);
					JSONArray array2 = new JSONArray(ticketresp.getResponse());

					List<MeraEventsTicketDetailsResponse> ticketList = new ArrayList<>();
					if (ticketresp.getCode().equalsIgnoreCase("S00")) {
						try {
							int count = 0;
							for (int i = 0; i < array2.length(); i++) {
								MeraEventsTicketDetailsResponse list = new MeraEventsTicketDetailsResponse();
								JSONObject jsonObject = array2.getJSONObject(i);
								list.setTicketId(jsonObject.getInt("id"));
								list.setTicketName(jsonObject.getString("name"));
								list.setDescription(jsonObject.getString("description"));
								list.setEventId(jsonObject.getInt("eventId"));
								list.setTicketPrice(jsonObject.getDouble("price"));
								list.setCurrencyCode(jsonObject.getString("currencyCode"));
								list.setQuantity(jsonObject.getInt("quantity"));
								list.setMinOrderQuantity(jsonObject.getInt("minOrderQuantity"));
								list.setMaxOrderQuantity(jsonObject.getInt("maxOrderQuantity"));
								list.setStartDate(jsonObject.getString("startDate"));
								list.setEndDate(jsonObject.getString("endDate"));
								list.setTicketStatus(jsonObject.getInt("status"));
								list.setTotalSoldTickets(jsonObject.getInt("totalSoldTickets"));
								list.setTicketType(jsonObject.getString("type"));
								list.setDisplayStatus(jsonObject.getInt("displayStatus"));
								list.setSoldout(jsonObject.getInt("soldout"));
								list.setOrder(jsonObject.getInt("order"));
								list.setI(""+i);
								
								count++;
								ticketList.add(list);
							}
							model.addAttribute("ticket", ticketList);
							result.setCode("S00");
							result.setMessage("Ticket Details : ");
							result.setSuccess(true);
							result.setStatus(ResponseStatus.SUCCESS);
							result.setDetails(ticketList);
						} catch (Exception e) {
							e.printStackTrace();
						}
					} else {
						result.setSuccess(false);
						result.setCode("F00");
						result.setMessage("Please try again later..");
						result.setStatus(ResponseStatus.FAILURE);
						result.setResponse(APIUtils.getFailedJSON().toString());
					}
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("User Authentication Failed");
					result.setStatus(ResponseStatus.FAILURE);
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("User Authentication Failed");
				result.setStatus(ResponseStatus.FAILURE);
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("User Authority null");
			result.setStatus(ResponseStatus.FAILURE);
			result.setResponse(APIUtils.getFailedJSON().toString());
			return "redirect:/Home";
		}
		return "User/MeraEvent/EventDetails";
	}

	
	MeraEventTicketCalculationResponse totalAmountResp = new MeraEventTicketCalculationResponse();
	List<MeraEventTicketCalculationResponse> ticketList = new ArrayList<>();
	
	
@RequestMapping(value = "/CalculateTotalAmount", method = RequestMethod.GET)
	public ResponseEntity<String> calculateAmount(@RequestParam(value = "eventId") String  eventId,
			@RequestParam(value = "ticketId") String ticketId,@RequestParam(value = "quantity") String  noOfTickets,@RequestParam(value = "ticketPrice") String  ticketPrice,MeraEventsBookingRequest dto,
			HttpServletRequest request,HttpServletResponse response, HttpSession session, ModelMap model) {
		MeraEventsResponse result = new MeraEventsResponse();
		
		session.setAttribute("noOfTickets",""+noOfTickets);
		session.setAttribute("ticketPrice",""+ticketPrice);
		
		System.err.println("++++++++++++++INSIDE CALCULATE CONTROLLER++++++++++++++");
		double totalAmount = 0.0;
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {

					double a = Double.parseDouble(noOfTickets);
					double b = Double.parseDouble(ticketPrice);
					totalAmount=(a*b);
					
					
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("User Authentication Failed");
					result.setStatus("Failure");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("User Authority null");
				result.setStatus("Failure");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("Session null");
			result.setStatus("Failure");
			result.setResponse(APIUtils.getFailedJSON().toString());
			
		}
	return new ResponseEntity<String>(""+totalAmount,HttpStatus.OK);
	}
	@RequestMapping(value = "/{eventId}/{ticketId}/InitiateBooking", method = RequestMethod.GET)
	public String initiateBookingProcess(@PathVariable String eventId,@PathVariable String ticketId,
			@ModelAttribute MeraEventsBookingRequest dto,HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) {

		System.err.println("INSIDE Initiate booking Controller");
		dto.setNoOfTickets((String) session.getAttribute("noOfTickets"));
		model.addAttribute("quantity", dto.getNoOfTickets());
     System.err.println("No of tickets++++++++++++"+dto.getNoOfTickets());
		MeraEventsResponse result = new MeraEventsResponse();
		List<MeraEventsAttendeeFormRequest> fieldList = new ArrayList<>();
		List<String> fieldList1 = new ArrayList<>();
		session.setAttribute("eventId", eventId);
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionId);
					dto.setAccess_token((String) session.getAttribute("accessToken"));
					dto.setEventId(eventId);
					dto.setTicketId(ticketId);
					//dto.setNoOfTickets(quantity);
					dto.setNoOfTickets((String) session.getAttribute("noOfTickets"));


					System.err.println("Quantity:::::++++++++++++++++:::::::::::" + dto.getNoOfTickets());
					System.err.println("EventID:::::++++++++++++++++:::::::::::" + eventId);
					System.err.println("Ticket ID:::::++++++++++++++++:::::::::::" + ticketId);

					session.setAttribute("orderId", "" + ticketId);
					model.addAttribute("event", eventId);

					try {
						result = meraEventsApi.initiateBooking(dto);
						//orderId = result.getMessage();
						//dto.setOrderId(orderId);
						if (result.getCode().equalsIgnoreCase("S00")) {
							//result = meraEventsApi.attendeeForm(dto);
							JSONArray customFields = (JSONArray) result.getJsonArray();
							System.err.println("CUSTOM FIELD :::::::::::::::::::::" + customFields);
							if (result.getCode().equalsIgnoreCase("S00")) {
								if (customFields != null) {
								/*JSONObject obj=new JSONObject(result.getAllresponse());
								JSONArray customFieldsjson=obj.getJSONObject("response").getJSONArray("customFields");
								System.err.println(customFieldsjson.length());*/
								/*for (int i = 0; i < customFieldsjson.length(); i++) 
								{
									JSONArray formFieldsjson=customFieldsjson.getJSONObject(i).getJSONArray("formFields");
									System.err.println(customFieldsjson.length());
									for (int j = 0; j < formFieldsjson.length(); j++) 
									{
										MeraEventsAttendeeFormRequest list = new MeraEventsAttendeeFormRequest();
										String fields = formFieldsjson.getJSONObject(j).getString("fieldname");
//										String fieldId = formFieldsjson.getJSONObject(j).getString("fieldnameid");
										System.err.println("rrrr"+j+fields);
										list.setFields(fields);
										list.setJ(""+j);
										
											fieldList.add(list);	
//										fieldList1.add(fields);
									}
								}*/
//								for (int i = 0; i < customFields.length(); i++) {
//									MeraEventsAttendeeFormRequest list = new MeraEventsAttendeeFormRequest();
//									JSONObject jsonObject = customFields.getJSONObject(i);
//									list.setTicketId(jsonObject.getInt("id"));
//									list.setTicketName(jsonObject.getString("ticketName"));
//									JSONArray fields = jsonObject.getJSONArray("formFields");
//									System.err.println("########"+fields);
//									if(fields != null) {
//										for(int j=0; j<fields.length(); j++) {
//									JSONObject js = fields.getJSONObject(j);
//									String fullName = js.getString("fieldname");
////									String 
//							//		String emailId = js.getString("")
//										}
//									}
//									fieldList.add(list);
//								}
									System.err.println(fieldList1);
									System.err.println(fieldList);
								}
							} else {
								result.setSuccess(false);
								result.setCode("F00");
								result.setMessage("Please try again later..");
								result.setStatus("Failure");
								result.setResponse(APIUtils.getFailedJSON().toString());
							}
						}
						} catch(Exception e){
							e.printStackTrace();
						}
					} else{
						result.setSuccess(false);
						result.setCode("F00");
						result.setMessage("User Authentication Failed");
						result.setStatus("Failure");
						result.setResponse(APIUtils.getFailedJSON().toString());
					}
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("User Authority null");
					result.setStatus("Failure");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Session null");
				result.setStatus("Failure");
				result.setResponse(APIUtils.getFailedJSON().toString());
				return "redirect:/Home";
			}
			System.err.println(fieldList);
//		session.setAttribute("savevalform", "");
//			model.addAttribute("savevalform", fieldid + "&" + fieldname);
			model.addAttribute("attendeeForm", fieldList);
			model.addAttribute("attendeeForm1", fieldList1);
			return "User/MeraEvent/AttendeeForm";
		}



	@RequestMapping(value = "/AttendeeForm", method = RequestMethod.POST)
	public ResponseEntity<MeraEventsResponse> attendeeForm(@ModelAttribute MeraEventsCommonRequest dto,
			HttpServletRequest request,HttpServletResponse response, HttpSession session) {
		MeraEventsResponse result = new MeraEventsResponse();
		String sessionId = dto.getSessionId();
		if (dto.getSessionId() != null || dto.getSessionId().length() > 0) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionId);
					// dto.setAccess_token((String)session.getAttribute("accessToken"));
					// dto.setOrderId((String) session.getAttribute("orderId"));
					result = meraEventsApi.attendeeForm(dto);
					if (result.getCode().equalsIgnoreCase("S00")) {
						result.setCode("S00");
						result.setMessage("Attendee Form For Registration ");
						result.setSuccess(true);
						result.setStatus("Success");
						result.setResponse(result.getResponse());
					} else {
						result.setSuccess(false);
						result.setCode("F00");
						result.setMessage("Please try again later..");
						result.setStatus("Failure");
						result.setResponse(APIUtils.getFailedJSON().toString());
					}
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("User Authentication Failed");
					result.setStatus("Failure");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("User Authority null");
				result.setStatus("Failure");
				result.setResponse(APIUtils.getFailedJSON().toString());
				
			}
		} else {
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("Session null");
			result.setStatus("Failure");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<MeraEventsResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/SaveAttendee", method = RequestMethod.POST)
	public String saveAttendee(@RequestBody MeraEventsAttendeeFormRequest dto,
			HttpServletRequest request,HttpServletResponse response, HttpSession session, ModelMap model) throws JSONException {
		
		
		
		MeraEventsResponse result = new MeraEventsResponse();
		MeraEventDetailsRequest event = new MeraEventDetailsRequest();
		MeraEventsCommonRequest req = new MeraEventsCommonRequest();
		dto.setOrderId((String) session.getAttribute("orderId"));

		dto.setQuantity((String) session.getAttribute("noOfTickets"));
		dto.setEventId((String) session.getAttribute("eventId"));
		System.err.println("QUANTITY:::::::::++++++++++==========="+dto.getQuantity());
		System.err.println("QUANTITY:::::::::++++++++++==========="+dto.getEventId());
		model.addAttribute("quantity",dto.getQuantity());
		String sessionId = (String) session.getAttribute("sessionId");
		System.err.println("----------- INSIDE SAVE ATTENDEE CONTROLLER--------------");
		if (sessionId != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionId);
					dto.setAccess_token((String)session.getAttribute("accessToken"));
					dto.setOrderId(orderId);
					dto.setEmailEnable(true);
					dto.setSmsEnable(true);
					
				//	dto.setEventId(dto.getEventId());
					System.err.println("emailid"+dto.getEmailId());
					System.err.println("mobileno"+dto.getMobileNo());
					System.err.println("fullname"+dto.getFullName());
					
					if (dto.getQuantity().equals("1")) {
						

						System.err.println("email id"+dto.getEmailId());
						System.err.println("mobile no"+dto.getMobileNo());
						System.err.println("full name"+dto.getFullName());
						String name =dto.getFullName();
						String[] nparts = name.split("#");
						String name1 = nparts[1];
						
						System.err.println("NAME 1:::::::"+name1);
						String mlobile = dto.getMobileNo();
						String[] mnparts = mlobile.split("@");
						String mn1 = mnparts[1];
						System.err.println("MOBILE  1:::::::"+mn1);
						String email = dto.getEmailId();
						String[] eparts = email.split(",");
						String email1 = eparts[1];
						System.err.println("email  1:::::::"+email1);
						dto.setFullName1(name1);
						dto.setMobileNo1(mn1);
						dto.setEmailId1(email1);
					}
					if (dto.getQuantity().equals("2")) {
						
						

						System.err.println("email id"+dto.getEmailId());
						System.err.println("mobile no"+dto.getMobileNo());
						System.err.println("full name"+dto.getFullName());
						String name =dto.getFullName();
						String[] nparts = name.split("#");
						String name1 = nparts[1];
						String name2 = nparts[2];
						String mlobile = dto.getMobileNo();
						String[] mnparts = mlobile.split("@");
						String mn1 = mnparts[1];
						String mn2 = mnparts[2];
						String email = dto.getEmailId();
						String[] eparts = email.split(",");
						String email1 = eparts[1];
						String email2 = eparts[2];
						dto.setFullName1(name1);
						dto.setFullName2(name2);
						dto.setMobileNo1(mn1);
						dto.setMobileNo2(mn2);
						dto.setEmailId1(email1);
						dto.setEmailId2(email2);
					}
					if (dto.getQuantity().equals("3")) {
						

						System.err.println("email id"+dto.getEmailId());
						System.err.println("mobile no"+dto.getMobileNo());
						System.err.println("full name"+dto.getFullName());
						String name =dto.getFullName();
						String[] nparts = name.split("#");
						String name1 = nparts[1];
						String name2 = nparts[2];
						String name3 = nparts[3];
						String mlobile = dto.getMobileNo();
						String[] mnparts = mlobile.split("@");
						String mn1 = mnparts[1];
						String mn2 = mnparts[2];
						String mn3 = mnparts[3];
						String email = dto.getEmailId();
						String[] eparts = email.split(",");
						String email1 = eparts[1];
						String email2 = eparts[2];
						String email3 = eparts[3];
						dto.setFullName1(name1);
						dto.setFullName2(name2);
						dto.setFullName3(name3);
						dto.setMobileNo1(mn1);
						dto.setMobileNo2(mn2);
						dto.setMobileNo3(mn3);
						dto.setEmailId1(email1);
						dto.setEmailId2(email2);
						dto.setEmailId3(email3);
					}
					if (dto.getQuantity().equals("4")) {
						

						System.err.println("email id"+dto.getEmailId());
						System.err.println("mobile no"+dto.getMobileNo());
						System.err.println("full name"+dto.getFullName());
						String name =dto.getFullName();
						String[] nparts = name.split("#");
						String name1 = nparts[1];
						String name2 = nparts[2];
						String name3 = nparts[3];
						String name4 = nparts[4];
						String mlobile = dto.getMobileNo();
						String[] mnparts = mlobile.split("@");
						String mn1 = mnparts[1];
						String mn2 = mnparts[2];
						String mn3 = mnparts[3];
						String mn4 = mnparts[4];
						String email = dto.getEmailId();
						String[] eparts = email.split(",");
						String email1 = eparts[1];
						String email2 = eparts[2];
						String email3 = eparts[3];
						String email4 = eparts[4];
						dto.setFullName1(name1);
						dto.setFullName2(name2);
						dto.setFullName3(name3);
						dto.setFullName4(name4);
						dto.setMobileNo1(mn1);
						dto.setMobileNo2(mn2);
						dto.setMobileNo3(mn3);
						dto.setMobileNo4(mn4);
						dto.setEmailId1(email1);
						dto.setEmailId2(email2);
						dto.setEmailId3(email3);
						dto.setEmailId4(email4);
					}
					result = meraEventsApi.saveAttendeeDetails(dto);
					JSONObject jsonObject = new JSONObject(result.getResponse());
					String statusCode = jsonObject.getString("statusCode");
					System.err.println(" STATUS CODE for::::::::::: "+statusCode);
					if (statusCode.equalsIgnoreCase("401")) 
					{
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("User Data entered missing or incorrect!");
					result.setStatus("Failure");
					result.setResponse(APIUtils.getFailedJSON().toString());
					}
					 else {
					if (result.getCode().equalsIgnoreCase("S00")) {
						eventSignUpID = result.getEventSignUpId();
						session.setAttribute("eventSignUpID", eventSignUpID);
					//eventSignUpID = 1234567;
					//netAmount = 10.0;
						netAmount = calculateNetAmount(result.getTotalAmount());
						System.err.println("NET AMMOUNT::::::::"+netAmount);
						session.setAttribute("netAmount",""+netAmount);
						String  eventId = (String) session.getAttribute("eventId");
						event.setSessionId(sessionId);
						//event.setOrderId(orderId);
						
						event.setOrderId((String) session.getAttribute("orderId"));
						event.setTicketId((String) session.getAttribute("orderId"));
						event.setQuantity((String) session.getAttribute("noOfTickets"));
						event.setNoOfAttendees((String) session.getAttribute("noOfTickets"));
						event.setPrice((String)session.getAttribute("ticketPrice"));
						/////////////////// for review jsp page
						/*model.addAttribute("eventId",event.getEventId());
						model.addAttribute("eventName",result1.getTitle());
						model.addAttribute("ticketId",event.getTicketId());
						model.addAttribute("noOfTicket",event.getQuantity());
						model.addAttribute("address",result1.getVenueName());
						model.addAttribute("totalamount",netAmount);*/
						  
						
						
						event.setEventSignUpId(eventSignUpID);
						event.setCity(result1.getCityName());
						if ((result1.getEventId().equals(eventId))) {
							event.setEventId(eventId);
							event.setCity(result1.getCityName());
							event.setState(result1.getStateName());
							event.setCountry(result1.getCountryName());
							event.setEventCategory(result1.getCategoryName());
							event.setEventName(result1.getTitle());
							event.setEventStartDate(result1.getStartDate());
							event.setEventEndDate(result1.getEndDate());
							event.setEventVanue(result1.getVenueName());
							event.setQuantity(event.getQuantity());
							event.setEventType(ticketresp.getTicketType());
							event.setTotalAmount(netAmount);
							//event.setPrice(ticketresp.getTicketPrice());
							//event.setAccess_token((String)session.getAttribute("accessToken"));
							//event.setNoOfAttendees(totalAmountResp.getTotalTicketQuantity());
						} else {
							req.setEventId(eventId);
							MeraEventsListResponse res = meraEventsApi.getEventDetails(req);
							event.setCity(res.getCityName());
							event.setState(res.getStateName());
							event.setCountry(res.getCountryName());
							event.setEventCategory(res.getCategoryName());
							event.setEventName(res.getTitle());
							event.setEventStartDate(res.getStartDate());
							event.setEventEndDate(res.getEndDate());
							event.setEventVanue(res.getVenueName());
							event.setTotalAmount(netAmount);
							//event.setPrice(ticketresp.getTicketPrice());
							//event.setAccess_token((String)session.getAttribute("accessToken"));
							//event.setNoOfAttendees(totalAmountResp.getTotalTicketQuantity());
						}
						System.err.println("-----------CALLING SAVE EVENT DETAILS API-----------");
						///// save event data in app part 
					MeraEventsResponse res = meraEventsApi.saveEventDetails(event);
						
						if (res.getCode().equalsIgnoreCase("S00")) {
							System.err.println("-------EVENT DETAILS SAVED--------");
						} else {
							res.setCode("F00");
							res.setMessage("Unable to save event details");
							res.setSuccess(false);
							res.setStatus("Failure");
							res.setResponse(res.getResponse());
						}
					} else {
						result.setSuccess(false);
						result.setCode("F00");
						result.setMessage("Please try again later..");
						result.setStatus("Failure");
						result.setResponse(APIUtils.getFailedJSON().toString());
					}
					 }
					
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("User Authentication Failed");
					result.setStatus("Failure");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("User Authority null");
				result.setStatus("Failure");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("Session null");
			result.setStatus("Failure");
			result.setResponse(APIUtils.getFailedJSON().toString());
			return "redirect:/Home";
		}
		return "User/MeraEvent/ReviewBooking";
	}

	
	
	@RequestMapping(value = "/eventPayment", method = RequestMethod.GET)
	public String eventPayment(@ModelAttribute MeraEventsCommonRequest dto,Model model,
			HttpServletRequest request,HttpServletResponse response, HttpSession session) {
		
		dto.setNetAmount((String) session.getAttribute("netAmount"));
		MeraEventsResponse result = new MeraEventsResponse();
		String sessionId = (String) session.getAttribute("sessionId");
		System.err.println("-------------INSIDE EVENT PAYMENT CONTROLLER-------------");
		if (sessionId != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionId);
					dto.setOrderId(orderId);
					dto.setEventSignUpId(eventSignUpID);
					//dto.setNetAmount(netAmount);
				
					// Uncomment this line
					// dto.setNetAmount((double)session.getAttribute("netAmount"));
					TransactionRequest newRequest = new TransactionRequest();
					newRequest.setSessionId(dto.getSessionId());
					// // Uncomment this line
					
					newRequest.setAmount(netAmount);
					newRequest.setTransactionRefNo("D");
					MeraEventAmountInitateRequest objinitate= new MeraEventAmountInitateRequest();
					objinitate.setNetAmount((String)session.getAttribute("netAmount"));
					objinitate.setSessioniId ((String)session.getAttribute("sessionId"));
					// transaction Validation API  api of app part
					ResponseDTO responseDTO = transactionApi.validateTransaction(newRequest);
					
				
					if (responseDTO.isValid()) {
						
					// amount initate api calling
					MeraEventsResponse respinitate = meraEventsApi.initate(objinitate);
					if (respinitate.getCode().equalsIgnoreCase("S00")) {
						
						// third party book api
						System.err.println("INITATE AMOUNT RESPONSE transaction id::::::::::::"+respinitate.getTxnId());

						dto.setTransaction_id(respinitate.getTxnId());
						MeraEventsResponse book = meraEventsApi.eventbook(dto);
						
					dto.setStatus(book.getResponse());
					System.err.println(":::::::::EVENT BOOK  TICKET RESPONSE "+book.getResponse());
					if(book.getResponse().equals("true") )
					{
					
					result = meraEventsApi.eventPayment(dto);
					System.err.println("::::::::: INSIDE PAYMENT APP===============+++++++++++ ");

					result.setMessage("Order placed successfully,Event detail have been sent to your registerd mobile number and email id");
					model.addAttribute("msg", result.getMessage());
					System.err.println(":::::::::EVENT Payment  RESPONSE "+result.getMessage());

						
					} else {
						
						result.setCode("F00");
						result.setMessage(responseDTO.getMessage());
					}
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage(""+respinitate.getDetails());
					model.addAttribute("msg",""+respinitate.getDetails());
					result.setStatus("Failure");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
					} else {
						result.setSuccess(false);
						result.setCode("F00");
						result.setMessage(responseDTO.getMessage());
						model.addAttribute("msg",responseDTO.getMessage());
						result.setStatus("Failure");
						result.setResponse(APIUtils.getFailedJSON().toString());
					}	
					
					
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("User Authority null");
				result.setStatus("Failure");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
				
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("User Authority null");
				result.setStatus("Failure");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("Session null");
			result.setStatus("Failure");
			result.setResponse(APIUtils.getFailedJSON().toString());
			
			
		}
		return "User/MeraEvent/ReviewBooking";
	}

	public static double calculateNetAmount(double totalAmount) {
		double commission = 0/100;
		double commissionOnAmount = totalAmount*commission;
		totalAmount = totalAmount + commissionOnAmount;
		double finalValue = Math.round(totalAmount * 100.0) / 100.0;
		return finalValue;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	@RequestMapping(value = "/ReviewBooking", method = RequestMethod.GET)
	public String reviewbook(@ModelAttribute MeraEventsBookingRequest dto,HttpServletRequest request,
			HttpServletResponse response, HttpSession session, ModelMap model) {
		
		System.err.println("INSIDE ReviewBooking booking Controller");
		MeraEventsResponse result = new MeraEventsResponse();
		String sessionId = (String) session.getAttribute("sessionId");
		if (sessionId != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
					dto.setSessionId(sessionId);
					
					dto.setOrderId((String) session.getAttribute("orderId"));
					dto.setTicketId((String) session.getAttribute("orderId"));
					dto.setQuantity((String) session.getAttribute("noOfTickets"));
					dto.setNoOfAttendees((String) session.getAttribute("noOfTickets"));
					dto.setPrice((String)session.getAttribute("ticketPrice"));
					/////////////////// for review jsp page
					model.addAttribute("eventId",dto.getEventId());
					model.addAttribute("eventName",result1.getTitle());
					model.addAttribute("ticketId",dto.getTicketId());
					model.addAttribute("noOfTicket",dto.getQuantity());
					model.addAttribute("address",result1.getVenueName());
					model.addAttribute("totalamount",netAmount);
			
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("User Authentication Failed");
					result.setStatus("Failure");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("User Authority null");
				result.setStatus("Failure");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("Session null");
			result.setStatus("Failure");
			result.setResponse(APIUtils.getFailedJSON().toString());
			return "redirect:/Home";
		}
		
		return "User/MeraEvent/ReviewBooking";
	}

}
