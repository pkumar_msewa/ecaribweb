package com.payqwikweb.controller.web.thirdparty;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.payqwikweb.app.api.IMerchantApi;
import com.payqwikweb.app.model.request.StatusDTO;
import com.thirdparty.model.ResponseDTO;

@Controller
@RequestMapping("/Api/{version}/{role}/{device}/{language}/Highway/")
public class ThirdPartyTransactionController {
	
	private final IMerchantApi merchantApi;

	public ThirdPartyTransactionController(IMerchantApi merchantApi) {
		super();
		this.merchantApi = merchantApi;
	}

	@RequestMapping(value = "/getStatus", method = RequestMethod.POST)
	public ResponseEntity<ResponseDTO> getStatusByRefNo(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language,
			@RequestHeader(value = "hash", required = false) String hash, @RequestBody StatusDTO dto) 
	{
		ResponseDTO response=merchantApi.getStatus(dto);
		return new ResponseEntity<>(response,HttpStatus.OK);
		
		
}
}