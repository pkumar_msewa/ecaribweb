package com.payqwikweb.controller.web;

import com.payqwikweb.app.model.request.SessionDTO;
import com.sun.management.OperatingSystemMXBean;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.management.ManagementFactory;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

@Controller
public class SystemController {

    ResponseEntity<?> getSystemDetails(@RequestBody SessionDTO session, HttpServletRequest request, HttpServletResponse response) {
        return null;
    }


    public static void main(String... args){
        System.err.println(System.getProperty("os.version"));
        System.err.println(System.getProperty("os.arch"));
        System.err.println(System.getProperty("os.name"));
        System.err.println(System.getenv("PATH"));
        OperatingSystemMXBean operatingSystemMXBean = (com.sun.management.OperatingSystemMXBean)ManagementFactory.getOperatingSystemMXBean();
        double cpuLoad = operatingSystemMXBean.getSystemCpuLoad();
        System.err.println(cpuLoad);
        System.err.println(Math.ceil(cpuLoad*100));
        long freeMemoryBytes = operatingSystemMXBean.getFreePhysicalMemorySize();
        long totalMemoryBytes = operatingSystemMXBean.getTotalPhysicalMemorySize();
        System.err.println(Math.ceil(freeMemoryBytes/(1024*1024)));
        System.err.println(Math.ceil(totalMemoryBytes/(1024*1024)));
        for (Method method : operatingSystemMXBean.getClass().getDeclaredMethods()) {
            method.setAccessible(true);

            if (method.getName().startsWith("get")
                    && Modifier.isPublic(method.getModifiers())) {
                Object value;
//                if(method.getName().equals("getSystemCpuLoad"))
                try {
                    value = method.invoke(operatingSystemMXBean);
                } catch (Exception e) {
                    value = e;
                } // try
                System.out.println(method.getName() + " = " + value);
            } // if
        } //
    }
}
