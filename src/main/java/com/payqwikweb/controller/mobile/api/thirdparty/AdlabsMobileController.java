package com.payqwikweb.controller.mobile.api.thirdparty;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.payqwikweb.api.IAdlabsAPI;
import com.payqwikweb.api.IAuthenticationApi;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.model.app.request.AdlabsAccessTokenRequest;
import com.payqwikweb.model.app.request.AdlabsTicketRequest;
import com.payqwikweb.model.app.response.AdlabOrderCreateRequest;
import com.payqwikweb.model.app.response.AdlabsAccessTokenResponse;
import com.payqwikweb.model.app.response.AdlabsAddOrder;
import com.payqwikweb.model.app.response.AdlabsAmountInitateRequest;
import com.payqwikweb.model.app.response.AdlabsAmountInitateResponse;
import com.payqwikweb.model.app.response.AdlabsAmountsuccessRequest;
import com.payqwikweb.model.app.response.AdlabsOrderCreateResponse;
import com.payqwikweb.model.app.response.AdlabsOrderVoucher;
import com.payqwikweb.model.app.response.AdlabsPaymentRequest;
import com.payqwikweb.model.app.response.AdlabsProceedResponse;
import com.payqwikweb.model.app.response.AdlabsTicketListResponse;
import com.payqwikweb.util.APIUtils;
import com.payqwikweb.util.Authorities;

@RequestMapping("/Api/{version}/{role}/{device}/{language}/Adlabs")
public class AdlabsMobileController implements MessageSourceAware {
	private IAdlabsAPI adLabsApi;
	private final IAuthenticationApi authenticationApi;

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	private MessageSource messageSource;

	public AdlabsMobileController(IAdlabsAPI adLabsApi, IAuthenticationApi authenticationApi) {
		this.authenticationApi = authenticationApi;
		this.adLabsApi = adLabsApi;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	@RequestMapping(value = "/AuthMobile", method = RequestMethod.POST)
	public ResponseEntity<AdlabsTicketListResponse> getAccessToken(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody AdlabsAccessTokenRequest dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws Exception {

		AdlabsTicketListResponse resp = new AdlabsTicketListResponse();
		String sessionId = dto.getSessionId();
		if (sessionId != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {

					AdlabsAccessTokenRequest tokenreq = new AdlabsAccessTokenRequest();
					AdlabsAccessTokenResponse access = adLabsApi.auth(tokenreq);
					// return new ResponseEntity<String>(access.getMessage(),
					// HttpStatus.OK);
					System.err.println(" Api Response :::::::::" + access.getDetails());
					JSONObject jsonObject;
					try {
						jsonObject = new JSONObject(access.getMessage());

						if (jsonObject.has("error")) {

							resp.setSuccess(false);
							resp.setCode("F00");
							resp.setMessage("Client Authentication Failed");
							resp.setResponse(APIUtils.getFailedJSON().toString());

						} else {
							resp.setSuccess(true);
							resp.setCode("S00");
							resp.setMessage("Authinection Success");
							resp.setDetails(access);
						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("User Authentication Failed");

					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("User Authority null");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Session null");
			// resp.setStatus("Failure");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}

		return new ResponseEntity<AdlabsTicketListResponse>(resp, HttpStatus.OK);
	}

	@RequestMapping(value = "/SearchTicketMobile", method = RequestMethod.POST)
	public ResponseEntity<AdlabsTicketListResponse> selectTicket(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody AdlabsTicketRequest dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws Exception {

		AdlabsAccessTokenRequest tokenreq = new AdlabsAccessTokenRequest();
		AdlabsTicketListResponse resp = new AdlabsTicketListResponse();

		String data = "";
		String sessionId = dto.getSessionId();
		if (sessionId != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED))

				{

					String cookie = (dto.getSession_name() + "=" + dto.getSessid());
					dto.setAcessToken(dto.getToken());
					dto.setSession_name(cookie);
					dto.setDate_departure(dto.getDate_departure());
					dto.setDate_visit(dto.getDate_visit());
					dto.setDestination(dto.getDestination());

					AdlabsTicketListResponse result = adLabsApi.ticketList(dto);
					JSONObject jsonObject;
					try {
						jsonObject = new JSONObject(result.getMessage());

						if (jsonObject.has("error")) {

							resp.setSuccess(false);
							resp.setCode("F00");
							resp.setMessage("Client Authentication Failed");
							resp.setResponse(APIUtils.getFailedJSON().toString());

						} else {
							resp.setSuccess(true);
							resp.setCode("S00");
							resp.setMessage("Seach Ticket Successful");
							resp.setDetails(result);
						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("This Ticket not for you Please become a user");
					/// model.addAttribute("msg", resp.getMessage());

					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("User Authority null,Please become a user");
				/// model.addAttribute("msg", resp.getMessage());

				resp.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {

			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Session null");
			// resp.setStatus("Failure");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<AdlabsTicketListResponse>(resp, HttpStatus.OK);

	}

	@RequestMapping(value = "/OrderCreateMobile", method = RequestMethod.POST)
	public ResponseEntity<AdlabsTicketListResponse> postUserDetail(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody AdlabOrderCreateRequest dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws Exception {

		AdlabsTicketListResponse resp = new AdlabsTicketListResponse();
		String cookie = (dto.getSession_name() + "=" + dto.getSessid());
		dto.setSession_name(cookie);
		String sessionId = dto.getSessionId();
		dto.setField_visit_date(dto.getField_visit_date());
		dto.setField_departure_date(dto.getField_departure_date());
		dto.setField_evt_res_business_unit(dto.getField_evt_res_business_unit());
		dto.setCommerce_product(dto.getCommerce_product());
		dto.setAccessToken(dto.getToken());

		if (sessionId != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED))

				{
					String data = "";
					AdlabsOrderCreateResponse order = adLabsApi.ordercreate(dto);

					JSONObject jsonObject;
					try {
						jsonObject = new JSONObject(order.getMessage());

						if (jsonObject.has("error")) {

							resp.setSuccess(false);
							resp.setCode("F00");
							resp.setMessage("Client Authentication Failed");
							resp.setResponse(APIUtils.getFailedJSON().toString());

						} else {
							resp.setSuccess(true);
							resp.setCode("S00");
							resp.setMessage("Order Create Successful");
							resp.setDetails(order);
						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("This card not for you,please become a user");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				
				}
				
			} else {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("User authority null,It's not for you");
				resp.setResponse(APIUtils.getFailedJSON().toString());

				resp.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Session null");
			// resp.setStatus("Failure");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}

		return new ResponseEntity<AdlabsTicketListResponse>(resp, HttpStatus.OK);
	}

	@RequestMapping(value = "/PaymentMobile", method = RequestMethod.POST)
	public ResponseEntity<AdlabsTicketListResponse> postpayment(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody AdlabsPaymentRequest dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws Exception {

		AdlabsTicketListResponse resp = new AdlabsTicketListResponse();
		String cookie = (dto.getSession_name() + "=" + dto.getSessid());
		dto.setSession_name(cookie);
		String sessionId = dto.getSessionId();
		dto.setAccessToken(dto.getToken());

		if (sessionId != null) {
			String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			if (authority != null) {
				if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED))

				{
					AdlabsOrderVoucher ov = new AdlabsOrderVoucher();
					ov.setSessioniId(sessionId);
					AdlabsAmountInitateRequest objinit = new AdlabsAmountInitateRequest();
					AdlabsAmountsuccessRequest objsuucess = new AdlabsAmountsuccessRequest();
					objinit.setAmount(dto.getAmount());
					objinit.setSessioniId(sessionId);

					String data = "";

					AdlabsAmountInitateResponse respinitate = adLabsApi.initate(objinit);

					dto.setTransaction_id(respinitate.getTxnId());

					if (respinitate.getCode().equalsIgnoreCase("S00")) {

						AdlabsOrderCreateResponse addOrder = adLabsApi.payment(dto);
						JSONObject jsonObject = new JSONObject(addOrder.getMessage());
						if (jsonObject.has("error")) {
							ov.setTransactionRefNo(respinitate.getTxnId());
							ov.setCode("F04");
							AdlabsProceedResponse success = adLabsApi.sucees(ov);
							resp.setSuccess(false);
							resp.setCode("F00");
							resp.setMessage("Your order  not placed, please try after some time");
							resp.setResponse(APIUtils.getFailedJSON().toString());

						}

						else {
							try {

								AdlabsAddOrder ao = new AdlabsAddOrder();
								String success_profile = jsonObject.getString("success_profile");
								String success_payment = jsonObject.getString("success_payment");

								ov.setSuccess_payment(success_payment);
								ov.setSuccess_profile(success_profile);
								ov.setTransactionRefNo(respinitate.getTxnId());
								ov.setAmount(dto.getAmount());
								ov.setCode("S00");
								objsuucess.setSessioniId(sessionId);
								try {

									// Amount Success Api Calling
									AdlabsProceedResponse success = adLabsApi.sucees(ov);
									success.setSessioniId(sessionId);
									if (success.getCode().equalsIgnoreCase("S00")) {

										resp.setMessage(
												" Order placed successfully, Detail have been sent to your Enter mobile number and email id");

									} else {
										ov.setTransactionRefNo(respinitate.getTxnId());
										ov.setCode("F04");
										AdlabsProceedResponse success1 = adLabsApi.sucees(ov);
										resp.setSuccess(false);
										resp.setCode("F00");
										resp.setMessage("" + success.getDetails());
										resp.setResponse(APIUtils.getFailedJSON().toString());

									}
								} catch (Exception e) {
									e.printStackTrace();
								}

							} catch (Exception e) {
								e.printStackTrace();
							}

						}

					} else {

						ov.setCode("F04");
						ov.setTransactionRefNo(respinitate.getTxnId());
						AdlabsProceedResponse success = adLabsApi.sucees(ov);
						resp.setSuccess(false);
						resp.setCode("F00");
						resp.setDetails(resp.getDetails());
						resp.setMessage("" + respinitate.getDetails());
						resp.setResponse(APIUtils.getFailedJSON().toString());
					}

				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("User authentication failed,please login again");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("User authority null,please login again");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Session null");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<AdlabsTicketListResponse>(resp, HttpStatus.OK);

	}

}