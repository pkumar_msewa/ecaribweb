package com.payqwikweb.controller.mobile.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.payqwikweb.api.IAuthenticationApi;
import com.payqwikweb.api.IServiceCheckApi;
import com.payqwikweb.app.api.ITravelBusApi;
import com.payqwikweb.app.api.IUserApi;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.ResponseStatus;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.busdto.BookTicketReq;
import com.payqwikweb.app.model.busdto.GetTransactionId;
import com.payqwikweb.app.model.busdto.SaveSeatDetailsDTO;
import com.payqwikweb.app.model.request.SessionDTO;
import com.payqwikweb.app.model.request.UserDetailsRequest;
import com.payqwikweb.app.model.request.bus.GetDestinationCity;
import com.payqwikweb.app.model.request.bus.GetSeatDetails;
import com.payqwikweb.app.model.request.bus.GetSourceCity;
import com.payqwikweb.app.model.request.bus.IsCancellableReq;
import com.payqwikweb.app.model.request.bus.ListOfAvailableTrips;
import com.payqwikweb.app.model.response.UserDetailsResponse;
import com.payqwikweb.app.model.response.bus.ResponseDTO;
import com.payqwikweb.model.error.bus.BusRequestError;
import com.payqwikweb.model.web.Status;
import com.payqwikweb.util.AES;
import com.payqwikweb.util.APIUtils;
import com.payqwikweb.util.TravelBusUtil;
import com.payqwikweb.validation.bus.BusRequestValidation;
import com.thirdparty.model.BusResponseDTO;

@Controller
@RequestMapping("/Api/{version}/{role}/{device}/{language}/Treval/Bus")
public class TravelBusMobileController {


	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private MessageSource messageSource;
	private final ITravelBusApi travelBusApi;
	private final IAuthenticationApi authenticationApi;
	private final BusRequestValidation validation;
	private final IUserApi userApi;
	private final IServiceCheckApi serviceCheckApi;

	public TravelBusMobileController(ITravelBusApi travelBusApi, IAuthenticationApi authenticationApi,
			BusRequestValidation validation,IUserApi userApi,IServiceCheckApi serviceCheckApi) {
		System.err.println("TravelBusMobileController Created");
		this.travelBusApi=travelBusApi;	
		this.authenticationApi = authenticationApi;
		this.validation = validation;
		this.userApi=userApi;
		this.serviceCheckApi=serviceCheckApi;
	}



	@RequestMapping(method = RequestMethod.POST, value = "/GetAllCityList", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> getAllCityList(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody SessionDTO dto,
			@RequestHeader(value = "hash", required = false) String hash) {

		ResponseDTO resp=new ResponseDTO();

		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				String authority = authenticationApi.getAuthorityFromSession(dto.getSessionId(), Role.USER);
				if (authority != null) {
					try {
						resp=travelBusApi.getAllCityList(dto.getSessionId());
						System.err.println("Working");
					} catch (Exception e) {
						System.out.println(e);
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
					}
				}else {
					resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
					resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
					resp.setMessage("Session expired");
				}
			}
			else {
				resp.setCode("F00");
				resp.setMessage("Unknown device");
				resp.setStatus("FAILED");
				resp.setDetails(APIUtils.getFailedJSON().toString());
			}
		}else {
			resp.setCode("F00");
			resp.setMessage("Unauthorised access");
			resp.setStatus("FAILED");
			resp.setDetails(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/GetSourceCityBySourceKey", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> getSourceCity(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody GetSourceCity dto,
			@RequestHeader(value = "hash", required = false) String hash) {

		ResponseDTO resp=new ResponseDTO();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				String authority = authenticationApi.getAuthorityFromSession(dto.getSessionId(), Role.USER);
				if (authority != null) {
					try {
						BusRequestError busError=validation.getAllSourceCityVal(dto);
						if (busError.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
							resp=travelBusApi.getAllSourceCity(dto);
						}
						else {
							resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
							resp.setStatus(ResponseStatus.BAD_REQUEST.getKey());
							resp.setMessage(busError.getMessage());
						}
						return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);

					} catch (Exception e) {
						System.out.println(e);
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
						return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
					}
				}else {
					resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
					resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
					resp.setMessage("Session expired");
				}
			}
			else {
				resp.setCode("F00");
				resp.setMessage("Unknown device");
				resp.setStatus("FAILED");
				resp.setDetails(APIUtils.getFailedJSON().toString());
			}
		}else {
			resp.setCode("F00");
			resp.setMessage("Unauthorised access");
			resp.setStatus("FAILED");
			resp.setDetails(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/GetDestinationCity", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> getDestinationCity(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody GetDestinationCity dto,
			@RequestHeader(value = "hash", required = false) String hash) {

		ResponseDTO resp=new ResponseDTO();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				String authority = authenticationApi.getAuthorityFromSession(dto.getSessionId(), Role.USER);
				if (authority != null) {
					try {
						BusRequestError busError=validation.getAlldestinationCityVal(dto);
						if (busError.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
							resp=travelBusApi.getAllDestinationCity(dto);
						}
						else {
							resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
							resp.setStatus(ResponseStatus.BAD_REQUEST.getKey());
							resp.setMessage(busError.getMessage());
						}
						return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);

					} catch (Exception e) {
						System.out.println(e);
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
						return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
					}
				}else {
					resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
					resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
					resp.setMessage("Session expired");
				}
			}
			else {
				resp.setCode("F00");
				resp.setMessage("Unknown device");
				resp.setStatus("FAILED");
				resp.setDetails(APIUtils.getFailedJSON().toString());
			}
		}else {
			resp.setCode("F00");
			resp.setMessage("Unauthorised access");
			resp.setStatus("FAILED");
			resp.setDetails(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/GetAllAvailableTrips", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> getListOfAvailableTrips(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody ListOfAvailableTrips dto,
			@RequestHeader(value = "hash", required = false) String hash) {

		ResponseDTO resp=new ResponseDTO();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				String authority = authenticationApi.getAuthorityFromSession(dto.getSessionId(), Role.USER);
				if (authority != null) {
					try {
						BusRequestError busError=validation.getAllAvailableTripsVal(dto);
						if (busError.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
							String service=serviceCheckApi.checkServiceActive(dto.getSessionId());
							if (Status.Active.getValue().equalsIgnoreCase(service)) {
								resp=travelBusApi.getAllAvailableTrips(dto);
							}
							else {
								resp.setStatus(ResponseStatus.FAILURE.getKey());
								resp.setCode(ResponseStatus.FAILURE.getValue());
								resp.setMessage("Service is under maintenance");
							}
						}
						else {
							resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
							resp.setStatus(ResponseStatus.BAD_REQUEST.getKey());
							resp.setMessage(busError.getMessage());
						}
						return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);

					} catch (Exception e) {
						System.out.println(e);
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
						return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
					}
				}else {
					resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
					resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
					resp.setMessage("Session expired");
				}
			}
			else {
				resp.setCode("F00");
				resp.setMessage("Unknown device");
				resp.setStatus("FAILED");
				resp.setDetails(APIUtils.getFailedJSON().toString());
			}
		}else {
			resp.setCode("F00");
			resp.setMessage("Unauthorised access");
			resp.setStatus("FAILED");
			resp.setDetails(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
	}


	@RequestMapping(method = RequestMethod.POST, value = "/GetSeatDetails", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> getSeatDetails(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody GetSeatDetails dto,
			@RequestHeader(value = "hash", required = false) String hash) {

		ResponseDTO resp=new ResponseDTO();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				String authority = authenticationApi.getAuthorityFromSession(dto.getSessionId(), Role.USER);
				if (authority != null) {
					try {
						BusRequestError busError=validation.getSeatDetailsVal(dto);
						if (busError.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
							resp=travelBusApi.getSeatDetails(dto);
						}
						else {
							resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
							resp.setStatus(ResponseStatus.BAD_REQUEST.getKey());
							resp.setMessage(busError.getMessage());
						}
						return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);

					} catch (Exception e) {
						System.out.println(e);
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
					}
				}else {
					resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
					resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
					resp.setMessage("Session expired");
				}
			}
			else {
				resp.setCode("F00");
				resp.setMessage("Unknown device");
				resp.setStatus("FAILED");
				resp.setDetails(APIUtils.getFailedJSON().toString());
			}
		}else {
			resp.setCode("F00");
			resp.setMessage("Unauthorised access");
			resp.setStatus("FAILED");
			resp.setDetails(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
	}




	@RequestMapping(method = RequestMethod.POST, value = "/GetTransactionId", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> getTxnId(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody GetTransactionId dto,
			@RequestHeader(value = "hash", required = false) String hash) {

		ResponseDTO resp=new ResponseDTO();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				String authority = authenticationApi.getAuthorityFromSession(dto.getSessionId(), Role.USER);
				if (authority != null) {
					try {
						BusRequestError busError=validation.getTransactionIdVal(dto);
						if (busError.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
							dto.setEmail(dto.getEmailId());
							UserDetailsResponse uDetailsResponse = new UserDetailsResponse();
							UserDetailsRequest userDetails=new UserDetailsRequest();
							userDetails.setSessionId(dto.getSessionId());
							uDetailsResponse=userApi.getUserDetails(userDetails, Role.USER);
							resp=travelBusApi.getTxnId(dto.getSessionId(),dto,uDetailsResponse);
						}
						else {
							resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
							resp.setStatus(ResponseStatus.BAD_REQUEST.getKey());
							resp.setMessage(busError.getMessage());
						}
						return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);

					} catch (Exception e) {
						System.out.println(e);
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
						return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
					}
				}else {
					resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
					resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
					resp.setMessage("Session expired");
				}
			}
			else {
				resp.setCode("F00");
				resp.setMessage("Unknown device");
				resp.setStatus("FAILED");
				resp.setDetails(APIUtils.getFailedJSON().toString());
			}
		}else {
			resp.setCode("F00");
			resp.setMessage("Unauthorised access");
			resp.setStatus("FAILED");
			resp.setDetails(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/BookTicket", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> bookTicket(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody BookTicketReq dto,
			@RequestHeader(value = "hash", required = false) String hash) {

		ResponseDTO resp=new ResponseDTO();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				String authority = authenticationApi.getAuthorityFromSession(dto.getSessionId(), Role.USER);
				if (authority != null) {
					try {
						BusRequestError busError=validation.bookTicketVal(dto);
						if (busError.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
							resp=travelBusApi.bookTicket(dto);

							/*if (!resp.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
								travelBusApi.cancelInitPayment(dto);
							}*/

							if (!(ResponseStatus.SUCCESS.getKey().equalsIgnoreCase(resp.getMdexStatus()))) {
								travelBusApi.cancelInitPayment(dto);
								resp.setMessage("Something went wrong. Please try later");
							}
							else if (ResponseStatus.SUCCESS.getKey().equalsIgnoreCase(resp.getMdexStatus())) {
								if (!(ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(resp.getStatus()))) {
									resp.setMessage("Your Ticket Is Booked.But Payment Not Successful.Please Contact Customer Care");
								}
							}

						}
						else {
							resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
							resp.setStatus(ResponseStatus.BAD_REQUEST.getKey());
							resp.setMessage(busError.getMessage());
						}
						return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);

					} catch (Exception e) {
						System.out.println(e);
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
						return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
					}
				}else {
					resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
					resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
					resp.setMessage("Session expired");
				}
			}
			else {
				resp.setCode("F00");
				resp.setMessage("Unknown device");
				resp.setStatus("FAILED");
				resp.setDetails(APIUtils.getFailedJSON().toString());
			}
		}else {
			resp.setCode("F00");
			resp.setMessage("Unauthorised access");
			resp.setStatus("FAILED");
			resp.setDetails(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/IsCancellable", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> isCancellable(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody IsCancellableReq dto,
			@RequestHeader(value = "hash", required = false) String hash) {

		ResponseDTO resp=new ResponseDTO();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				String authority = authenticationApi.getAuthorityFromSession(dto.getSessionId(), Role.USER);
				if (authority != null) {
					try {
						BusRequestError busError=validation.isCancellableVal(dto);
						if (busError.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
							resp=travelBusApi.isCancellable(dto);
						}
						else {
							resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
							resp.setStatus(ResponseStatus.BAD_REQUEST.getKey());
							resp.setMessage(busError.getMessage());
						}
						return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);

					} catch (Exception e) {
						System.out.println(e);
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
						return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
					}
				}else {
					resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
					resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
					resp.setMessage("Session expired");
				}
			}
			else {
				resp.setCode("F00");
				resp.setMessage("Unknown device");
				resp.setStatus("FAILED");
				resp.setDetails(APIUtils.getFailedJSON().toString());
			}
		}else {
			resp.setCode("F00");
			resp.setMessage("Unauthorised access");
			resp.setStatus("FAILED");
			resp.setDetails(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
	}


	@RequestMapping(method = RequestMethod.POST, value = "/CancelTicket", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> cancelTicket(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody IsCancellableReq dto,
			@RequestHeader(value = "hash", required = false) String hash) {

		ResponseDTO resp=new ResponseDTO();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				String authority = authenticationApi.getAuthorityFromSession(dto.getSessionId(), Role.USER);
				if (authority != null) {
					try {
						BusRequestError busError=validation.isCancellableVal(dto);
						if (busError.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
							resp=travelBusApi.cancelTicket(dto);
						}
						else {
							resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
							resp.setStatus(ResponseStatus.BAD_REQUEST.getKey());
							resp.setMessage(busError.getMessage());
						}
						return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);

					} catch (Exception e) {
						System.out.println(e);
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
						return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
					}
				}else {
					resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
					resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
					resp.setMessage("Session expired");
				}
			}
			else {
				resp.setCode("F00");
				resp.setMessage("Unknown device");
				resp.setStatus("FAILED");
				resp.setDetails(APIUtils.getFailedJSON().toString());
			}
		}else {
			resp.setCode("F00");
			resp.setMessage("Unauthorised access");
			resp.setStatus("FAILED");
			resp.setDetails(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
	}


	@RequestMapping(method = RequestMethod.POST, value = "/cancelInitPayment", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> cancelInitPayment(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody BookTicketReq dto,
			@RequestHeader(value = "hash", required = false) String hash) {

		ResponseDTO resp=new ResponseDTO();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				String authority = authenticationApi.getAuthorityFromSession(dto.getSessionId(), Role.USER);
				if (authority != null) {
					try {

						if (dto.getVpqTxnId()!=null || !dto.getVpqTxnId().isEmpty()) {
							resp=travelBusApi.cancelInitPayment(dto);
						}
						else {
							resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
							resp.setStatus(ResponseStatus.BAD_REQUEST.getKey());
							resp.setMessage("Please Enter Transection No");
						}
						return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);

					} catch (Exception e) {
						System.out.println(e);
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
						return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
					}
				}else {
					resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
					resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
					resp.setMessage("Session expired");
				}
			}
			else {
				resp.setCode("F00");
				resp.setMessage("Unknown device");
				resp.setStatus("FAILED");
				resp.setDetails(APIUtils.getFailedJSON().toString());
			}
		}else {
			resp.setCode("F00");
			resp.setMessage("Unauthorised access");
			resp.setStatus("FAILED");
			resp.setDetails(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
	}


	@RequestMapping(method = RequestMethod.POST, value = "/cronCheck", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> cronCheck(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody SessionDTO dto,
			@RequestHeader(value = "hash", required = false) String hash) {

		ResponseDTO resp=new ResponseDTO();

		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				String authority = authenticationApi.getAuthorityFromSession(dto.getSessionId(), Role.USER);
				if (authority != null) {
					try {
						resp=travelBusApi.cronCheck(dto.getSessionId());
						System.err.println("Working");
					} catch (Exception e) {
						System.out.println(e);
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
					}
				}else {
					resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
					resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
					resp.setMessage("Session expired");
				}
			}
			else {
				resp.setCode("F00");
				resp.setMessage("Unknown device");
				resp.setStatus("FAILED");
				resp.setDetails(APIUtils.getFailedJSON().toString());
			}
		}else {
			resp.setCode("F00");
			resp.setMessage("Unauthorised access");
			resp.setStatus("FAILED");
			resp.setDetails(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
	}


	@RequestMapping(method = RequestMethod.POST, value = "/GetAllCityListCron", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> getAllCityListCron(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody SessionDTO dto,
			@RequestHeader(value = "hash", required = false) String hash) {

		ResponseDTO resp=new ResponseDTO();

		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				String authority = authenticationApi.getAuthorityFromSession(dto.getSessionId(), Role.USER);

				try {
					resp=travelBusApi.getAllCityListByEMT(dto.getSessionId());
					System.err.println("Working");
				} catch (Exception e) {
					System.out.println(e);
					resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
					resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
					resp.setMessage("Service Unavailable");
				}

			}
			else {
				resp.setCode("F00");
				resp.setMessage("Unknown device");
				resp.setStatus("FAILED");
				resp.setDetails(APIUtils.getFailedJSON().toString());
			}
		}else {
			resp.setCode("F00");
			resp.setMessage("Unauthorised access");
			resp.setStatus("FAILED");
			resp.setDetails(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
	}


	@RequestMapping(method = RequestMethod.POST, value = "/getAllTicketsByUser", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> getAllTicketsByUser(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody SessionDTO dto,
			@RequestHeader(value = "hash", required = false) String hash) {

		ResponseDTO resp=new ResponseDTO();

		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				String authority = authenticationApi.getAuthorityFromSession(dto.getSessionId(), Role.USER);
				if (authority != null) {
					try {
						resp=travelBusApi.getAllBookTickets(dto.getSessionId());
						System.err.println("Working");
					} catch (Exception e) {
						System.out.println(e);
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
					}
				}else {
					resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
					resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
					resp.setMessage("Session expired");
				}
			}
			else {
				resp.setCode("F00");
				resp.setMessage("Unknown device");
				resp.setStatus("FAILED");
				resp.setDetails(APIUtils.getFailedJSON().toString());
			}
		}else {
			resp.setCode("F00");
			resp.setMessage("Unauthorised access");
			resp.setStatus("FAILED");
			resp.setDetails(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
	}


	@RequestMapping(method = RequestMethod.POST, value = "/GetTransactionIdUpdated", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> getTxnIdUpdated(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody GetTransactionId dto,
			@RequestHeader(value = "hash", required = false) String hash) {

		ResponseDTO resp=new ResponseDTO();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				String authority = authenticationApi.getAuthorityFromSession(dto.getSessionId(), Role.USER);
				if (authority != null) {
					try {
						BusRequestError busError=validation.getTransactionIdVal(dto);
						if (busError.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
							dto.setEmail(dto.getEmailId());
							UserDetailsResponse uDetailsResponse = new UserDetailsResponse();
							UserDetailsRequest userDetails=new UserDetailsRequest();
							userDetails.setSessionId(dto.getSessionId());
							uDetailsResponse=userApi.getUserDetails(userDetails, Role.USER);
							resp=travelBusApi.getTxnIdUpdated(dto.getSessionId(),dto,uDetailsResponse);
						}
						else {
							resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
							resp.setStatus(ResponseStatus.BAD_REQUEST.getKey());
							resp.setMessage(busError.getMessage());
						}
						return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);

					} catch (Exception e) {
						System.out.println(e);
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
						return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
					}
				}else {
					resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
					resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
					resp.setMessage("Session expired");
				}
			}
			else {
				resp.setCode("F00");
				resp.setMessage("Unknown device");
				resp.setStatus("FAILED");
				resp.setDetails(APIUtils.getFailedJSON().toString());
			}
		}else {
			resp.setCode("F00");
			resp.setMessage("Unauthorised access");
			resp.setStatus("FAILED");
			resp.setDetails(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
	}


	@RequestMapping(method = RequestMethod.POST, value = "/BookTicketUpdated", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<BusResponseDTO> bookTicketUpdated(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody BookTicketReq dto,
			@RequestHeader(value = "hash", required = false) String hash) {

		BusResponseDTO resp=new BusResponseDTO();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				String authority = authenticationApi.getAuthorityFromSession(dto.getSessionId(), Role.USER);
				if (authority != null) {
					try {
						BusRequestError busError=validation.bookTicketValUpdated(dto);
						if (busError.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {

							resp=travelBusApi.initPayment(dto);

							if (resp.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {

								dto.setVpqTxnId(resp.getTransactionRefNo());
								resp=travelBusApi.bookTicketUpdated(dto);

								if (!(ResponseStatus.SUCCESS.getKey().equalsIgnoreCase(resp.getMdexStatus()))) {
									resp.setMessage("Something went wrong. Please try later");
								}
								else if (ResponseStatus.SUCCESS.getKey().equalsIgnoreCase(resp.getMdexStatus())) {
									if (!(ResponseStatus.SUCCESS.getKey().equalsIgnoreCase(resp.getStatus()))) {
										resp.setMessage("Your Ticket Is Booked.But Payment Not Successful.Please Contact Customer Care");
									}
								}
							}else {
								resp.setCode(ResponseStatus.FAILURE.getValue());
								resp.setStatus(ResponseStatus.FAILURE.getKey());
								//resp.setMessage("");
							}

						}
						else {
							resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
							resp.setStatus(ResponseStatus.BAD_REQUEST.getKey());
							resp.setMessage(busError.getMessage());
						}
						return new ResponseEntity<BusResponseDTO>(resp, HttpStatus.OK);

					} catch (Exception e) {
						System.out.println(e);
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
						return new ResponseEntity<BusResponseDTO>(resp, HttpStatus.OK);
					}
				}else {
					resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
					resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
					resp.setMessage("Session expired");
				}
			}
			else {
				resp.setCode("F00");
				resp.setMessage("Unknown device");
				resp.setStatus("FAILED");
				resp.setDetails(APIUtils.getFailedJSON().toString());
			}
		}else {
			resp.setCode("F00");
			resp.setMessage("Unauthorised access");
			resp.setStatus("FAILED");
			resp.setDetails(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<BusResponseDTO>(resp, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/saveSeatDetails", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<BusResponseDTO> saveSeatDetails(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody SaveSeatDetailsDTO dto,
			@RequestHeader(value = "hash", required = false) String hash) {

		BusResponseDTO resp=new BusResponseDTO();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				String authority = authenticationApi.getAuthorityFromSession(dto.getSessionId(), Role.USER);
				if (authority != null) {
					try {

						resp=travelBusApi.saveSeatDetails(dto.getSessionId(), dto);

						return new ResponseEntity<BusResponseDTO>(resp, HttpStatus.OK);

					} catch (Exception e) {
						System.out.println(e);
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
						return new ResponseEntity<BusResponseDTO>(resp, HttpStatus.OK);
					}
				}else {
					resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
					resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
					resp.setMessage("Session expired");
				}
			}
			else {
				resp.setCode("F00");
				resp.setMessage("Unknown device");
				resp.setStatus("FAILED");
				resp.setDetails(APIUtils.getFailedJSON().toString());
			}
		}else {
			resp.setCode("F00");
			resp.setMessage("Unauthorised access");
			resp.setStatus("FAILED");
			resp.setDetails(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<BusResponseDTO>(resp, HttpStatus.OK);
	}





	/************************************** Encryption Request controller *************************************************/


	@RequestMapping(method = RequestMethod.POST, value = "/EncReq/GetAllCityList", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> getAllCityListEncReq(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody SessionDTO dto,
			@RequestHeader(value = "hash", required = false) String hash) throws Exception {

		ResponseDTO resp=new ResponseDTO();


		if (dto.getData()!=null && !dto.getData().isEmpty() && !dto.getData().equalsIgnoreCase("null")) {

			String decData=AES.decrypt(dto.getData());
			String sessionId=TravelBusUtil.getSessionIdDec(decData);
			dto.setSessionId(sessionId);

			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					String authority = authenticationApi.getAuthorityFromSession(dto.getSessionId(), Role.USER);
					if (authority != null) {
						try {
							resp=travelBusApi.getAllCityList(dto.getSessionId());
							System.err.println("Working");
						} catch (Exception e) {
							System.out.println(e);
							resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
							resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
							resp.setMessage("Service Unavailable");
						}
					}else {
						resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
						resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
						resp.setMessage("Session expired");
					}
				}
				else {
					resp.setCode("F00");
					resp.setMessage("Unknown device");
					resp.setStatus("FAILED");
					resp.setDetails(APIUtils.getFailedJSON().toString());
				}
			}else {
				resp.setCode("F00");
				resp.setMessage("Unauthorised access");
				resp.setStatus("FAILED");
				resp.setDetails(APIUtils.getFailedJSON().toString());
			}
		}
		else {
			resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
			resp.setStatus(ResponseStatus.BAD_REQUEST.getKey());
			resp.setMessage("Please pass encrypt data");
		}
		return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
	}


	@RequestMapping(method = RequestMethod.POST, value = "/EncReq/GetAllAvailableTrips", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> getListOfAvailableTripsEncReq(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody ListOfAvailableTrips tripsDto,
			@RequestHeader(value = "hash", required = false) String hash) throws Exception {

		ResponseDTO resp=new ResponseDTO();

		if (tripsDto.getData()!=null && !tripsDto.getData().isEmpty() && !tripsDto.getData().equalsIgnoreCase("null")) {

			String decData=AES.decrypt(tripsDto.getData());
			ListOfAvailableTrips dto=TravelBusUtil.getAllAvailableTripsDec(decData);

			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					String authority = authenticationApi.getAuthorityFromSession(dto.getSessionId(), Role.USER);
					if (authority != null) {
						try {
							BusRequestError busError=validation.getAllAvailableTripsVal(dto);
							if (busError.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
								String service=serviceCheckApi.checkServiceActive(dto.getSessionId());
								if (Status.Active.getValue().equalsIgnoreCase(service)) {
									resp=travelBusApi.getAllAvailableTrips(dto);
								}
								else {
									resp.setStatus(ResponseStatus.FAILURE.getKey());
									resp.setCode(ResponseStatus.FAILURE.getValue());
									resp.setMessage("Service is under maintenance");
								}
							}
							else {
								resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
								resp.setStatus(ResponseStatus.BAD_REQUEST.getKey());
								resp.setMessage(busError.getMessage());
							}

							return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);

						} catch (Exception e) {
							e.printStackTrace();
							resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
							resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
							resp.setMessage("Service Unavailable");
							return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
						}
					}else {
						resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
						resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
						resp.setMessage("Session expired");
					}
				}
				else {
					resp.setCode("F00");
					resp.setMessage("Unknown device");
					resp.setStatus("FAILED");
					resp.setDetails(APIUtils.getFailedJSON().toString());
				}
			}else {
				resp.setCode("F00");
				resp.setMessage("Unauthorised access");
				resp.setStatus("FAILED");
				resp.setDetails(APIUtils.getFailedJSON().toString());
			}
		}
		else {
			resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
			resp.setStatus(ResponseStatus.BAD_REQUEST.getKey());
			resp.setMessage("Please pass encrypt data");
		}
		return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
	}


	@RequestMapping(method = RequestMethod.POST, value = "/EncReq/GetSeatDetails", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> getSeatDetailsEncReq(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody GetSeatDetails getSeatDetails,
			@RequestHeader(value = "hash", required = false) String hash) throws Exception {

		ResponseDTO resp=new ResponseDTO();

		if (getSeatDetails.getData()!=null && !getSeatDetails.getData().isEmpty() && !getSeatDetails.getData().equalsIgnoreCase("null")) {
			String decData=AES.decrypt(getSeatDetails.getData());
			GetSeatDetails dto=TravelBusUtil.getSeatDetailsDec(decData);

			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					String authority = authenticationApi.getAuthorityFromSession(dto.getSessionId(), Role.USER);
					if (authority != null) {
						try {
							BusRequestError busError=validation.getSeatDetailsVal(dto);
							if (busError.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
								resp=travelBusApi.getSeatDetails(dto);
							}
							else {
								resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
								resp.setStatus(ResponseStatus.BAD_REQUEST.getKey());
								resp.setMessage(busError.getMessage());
							}

							return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);

						} catch (Exception e) {
							System.out.println(e);
							resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
							resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
							resp.setMessage("Service Unavailable");
						}
					}else {
						resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
						resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
						resp.setMessage("Session expired");
					}
				}
				else {
					resp.setCode("F00");
					resp.setMessage("Unknown device");
					resp.setStatus("FAILED");
					resp.setDetails(APIUtils.getFailedJSON().toString());
				}
			}else {
				resp.setCode("F00");
				resp.setMessage("Unauthorised access");
				resp.setStatus("FAILED");
				resp.setDetails(APIUtils.getFailedJSON().toString());
			}
		}
		else {
			resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
			resp.setStatus(ResponseStatus.BAD_REQUEST.getKey());
			resp.setMessage("Please pass encrypt data");
		}
		return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
	}


	@RequestMapping(method = RequestMethod.POST, value = "/EncReq/saveSeatDetails", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<BusResponseDTO> saveSeatDetailsEncReq(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody SaveSeatDetailsDTO seatDetailsDTO,
			@RequestHeader(value = "hash", required = false) String hash) throws Exception {

		BusResponseDTO resp=new BusResponseDTO();
		if (seatDetailsDTO.getData()!=null && !seatDetailsDTO.getData().isEmpty() && !seatDetailsDTO.getData().equalsIgnoreCase("null")) {
			
			String decData=AES.decrypt(seatDetailsDTO.getData());
			SaveSeatDetailsDTO dto=TravelBusUtil.saveSeatDetailsDTODec(decData);

			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					String authority = authenticationApi.getAuthorityFromSession(dto.getSessionId(), Role.USER);
					if (authority != null) {
						try {
							resp=travelBusApi.saveSeatDetails(dto.getSessionId(), dto);

							return new ResponseEntity<BusResponseDTO>(resp, HttpStatus.OK);

						} catch (Exception e) {
							System.out.println(e);
							resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
							resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
							resp.setMessage("Service Unavailable");
							return new ResponseEntity<BusResponseDTO>(resp, HttpStatus.OK);
						}
					}else {
						resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
						resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
						resp.setMessage("Session expired");
					}
				}
				else {
					resp.setCode("F00");
					resp.setMessage("Unknown device");
					resp.setStatus("FAILED");
					resp.setDetails(APIUtils.getFailedJSON().toString());
				}
			}else {
				resp.setCode("F00");
				resp.setMessage("Unauthorised access");
				resp.setStatus("FAILED");
				resp.setDetails(APIUtils.getFailedJSON().toString());
			}
		}
		else {
			resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
			resp.setStatus(ResponseStatus.BAD_REQUEST.getKey());
			resp.setMessage("Please pass encrypt data");
		}
		return new ResponseEntity<BusResponseDTO>(resp, HttpStatus.OK);
	}


	@RequestMapping(method = RequestMethod.POST, value = "/EncReq/GetTransactionIdUpdated", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> getTxnIdUpdatedEncReq(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody GetTransactionId txnIdDto,
			@RequestHeader(value = "hash", required = false) String hash) throws Exception {

		ResponseDTO resp=new ResponseDTO();
		if (txnIdDto.getData()!=null && !txnIdDto.getData().isEmpty() && !txnIdDto.getData().equalsIgnoreCase("null")) {
			String decData=AES.decrypt(txnIdDto.getData());
			GetTransactionId dto=TravelBusUtil.getTxnIdDec(decData);

			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					String authority = authenticationApi.getAuthorityFromSession(dto.getSessionId(), Role.USER);
					if (authority != null) {
						try {

							BusRequestError busError=validation.getTransactionIdVal(dto);
							if (busError.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {
								dto.setEmail(dto.getEmailId());
								UserDetailsResponse uDetailsResponse = new UserDetailsResponse();
								UserDetailsRequest userDetails=new UserDetailsRequest();
								userDetails.setSessionId(dto.getSessionId());
								uDetailsResponse=userApi.getUserDetails(userDetails, Role.USER);
								resp=travelBusApi.getTxnIdUpdated(dto.getSessionId(),dto,uDetailsResponse);
							}
							else {
								resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
								resp.setStatus(ResponseStatus.BAD_REQUEST.getKey());
								resp.setMessage(busError.getMessage());
							}

							return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);

						} catch (Exception e) {
							System.out.println(e);
							resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
							resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
							resp.setMessage("Service Unavailable");
							return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
						}
					}else {
						resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
						resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
						resp.setMessage("Session expired");
					}
				}
				else {
					resp.setCode("F00");
					resp.setMessage("Unknown device");
					resp.setStatus("FAILED");
					resp.setDetails(APIUtils.getFailedJSON().toString());
				}
			}else {
				resp.setCode("F00");
				resp.setMessage("Unauthorised access");
				resp.setStatus("FAILED");
				resp.setDetails(APIUtils.getFailedJSON().toString());
			}
		}
		else {
			resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
			resp.setStatus(ResponseStatus.BAD_REQUEST.getKey());
			resp.setMessage("Please pass encrypt data");
		}

		return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
	}


	@RequestMapping(method = RequestMethod.POST, value = "/EncReq/BookTicketUpdated", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<BusResponseDTO> bookTicketUpdatedEncReq(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody BookTicketReq ticketDto,
			@RequestHeader(value = "hash", required = false) String hash) throws Exception {

		BusResponseDTO resp=new BusResponseDTO();

		if (ticketDto.getData()!=null && !ticketDto.getData().isEmpty() && !ticketDto.getData().equalsIgnoreCase("null")) {
			String decData=AES.decrypt(ticketDto.getData());
			BookTicketReq dto=TravelBusUtil.bookTicketDec(decData);

			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					String authority = authenticationApi.getAuthorityFromSession(dto.getSessionId(), Role.USER);
					if (authority != null) {
						try {
							BusRequestError busError=validation.bookTicketValUpdated(dto);
							if (busError.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {

								resp=travelBusApi.initPayment(dto);

								if (resp.getCode().equalsIgnoreCase(ResponseStatus.SUCCESS.getValue())) {

									dto.setVpqTxnId(resp.getTransactionRefNo());
									resp=travelBusApi.bookTicketUpdated(dto);

									if (!(ResponseStatus.SUCCESS.getKey().equalsIgnoreCase(resp.getMdexStatus()))) {
										//									travelBusApi.cancelInitPayment(dto);
										resp.setMessage("Something went wrong. Please try later");
									}
									else if (ResponseStatus.SUCCESS.getKey().equalsIgnoreCase(resp.getMdexStatus())) {
										if (!(ResponseStatus.SUCCESS.getKey().equalsIgnoreCase(resp.getStatus()))) {
											resp.setMessage("Your Ticket Is Booked.But Payment Not Successful.Please Contact Customer Care");
										}
									}
								}else {
									resp.setCode(ResponseStatus.FAILURE.getValue());
									resp.setStatus(ResponseStatus.FAILURE.getKey());
									//resp.setMessage("");
								}

							}
							else {
								resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
								resp.setStatus(ResponseStatus.BAD_REQUEST.getKey());
								resp.setMessage(busError.getMessage());
							}

							return new ResponseEntity<BusResponseDTO>(resp, HttpStatus.OK);

						} catch (Exception e) {
							System.out.println(e);
							resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
							resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
							resp.setMessage("Service Unavailable");
							return new ResponseEntity<BusResponseDTO>(resp, HttpStatus.OK);
						}
					}else {
						resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
						resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
						resp.setMessage("Session expired");
					}
				}
				else {
					resp.setCode("F00");
					resp.setMessage("Unknown device");
					resp.setStatus("FAILED");
					resp.setDetails(APIUtils.getFailedJSON().toString());
				}
			}else {
				resp.setCode("F00");
				resp.setMessage("Unauthorised access");
				resp.setStatus("FAILED");
				resp.setDetails(APIUtils.getFailedJSON().toString());
			}
		}
		else {
			resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
			resp.setStatus(ResponseStatus.BAD_REQUEST.getKey());
			resp.setMessage("Please pass encrypt data");
		}
		return new ResponseEntity<BusResponseDTO>(resp, HttpStatus.OK);
	}


	@RequestMapping(method = RequestMethod.POST, value = "/EncReq/getAllTicketsByUser", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<ResponseDTO> getAllTicketsByUserEncReq(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody SessionDTO dto,
			@RequestHeader(value = "hash", required = false) String hash) throws Exception {

		ResponseDTO resp=new ResponseDTO();

		if (dto.getData()!=null && !dto.getData().isEmpty() && !dto.getData().equalsIgnoreCase("null")) {

			String decData=AES.decrypt(dto.getData());
			String sessionId=TravelBusUtil.getSessionIdDec(decData);
			dto.setSessionId(sessionId);
			

			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					String authority = authenticationApi.getAuthorityFromSession(dto.getSessionId(), Role.USER);
					if (authority != null) {
						try {
							resp=travelBusApi.getAllBookTickets(dto.getSessionId());
							System.err.println("Working");
						} catch (Exception e) {
							System.out.println(e);
							resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
							resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
							resp.setMessage("Service Unavailable");
						}
					}else {
						resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
						resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
						resp.setMessage("Session expired");
					}
				}
				else {
					resp.setCode("F00");
					resp.setMessage("Unknown device");
					resp.setStatus("FAILED");
					resp.setDetails(APIUtils.getFailedJSON().toString());
				}
			}else {
				resp.setCode("F00");
				resp.setMessage("Unauthorised access");
				resp.setStatus("FAILED");
				resp.setDetails(APIUtils.getFailedJSON().toString());
			}
		}
		else {
			resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
			resp.setStatus(ResponseStatus.BAD_REQUEST.getKey());
			resp.setMessage("Please pass encrypt data");
		}
		return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
	}

}
