package com.payqwikweb.controller.mobile.api.thirdparty;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.core.MediaType;

import org.codehaus.jettison.json.JSONObject;
import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.payqwikweb.api.IAuthenticationApi;
import com.payqwikweb.api.IGifCartAPI;
import com.payqwikweb.app.api.IGiftCartAPI;
import com.payqwikweb.app.api.ITransactionApi;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.model.app.request.GCIAddCartRequest;
import com.payqwikweb.model.app.request.GCIAddOrderRequest;
import com.payqwikweb.model.app.request.GCIAmountInitateRequest;
import com.payqwikweb.model.app.request.GCIAmountsuccessRequest;
import com.payqwikweb.model.app.request.GCIOrderProductRequest;
import com.payqwikweb.model.app.request.GCIOrderVoucherRequest;
import com.payqwikweb.model.app.request.GCIProceedRequest;
import com.payqwikweb.model.app.request.GciMobileRequest;
import com.payqwikweb.model.app.request.GetBrandDenominationRequest;
import com.payqwikweb.model.app.response.GCIAddCartResponse;
import com.payqwikweb.model.app.response.GCIAddOrder;
import com.payqwikweb.model.app.response.GCIAddOrderResponse;
import com.payqwikweb.model.app.response.GCIAmountInitateResponse;
import com.payqwikweb.model.app.response.GCIBrandDenominationResponse;
import com.payqwikweb.model.app.response.GCIOrderVoucher;
import com.payqwikweb.model.app.response.GCIOrderVoucherResponse;
import com.payqwikweb.model.app.response.GCIProceedResponse;
import com.payqwikweb.model.app.response.GciMobileResponse;
import com.payqwikweb.util.APIUtils;
import com.payqwikweb.util.Authorities;

@Controller
@RequestMapping("/Api/{version}/{role}/{device}/{language}/GciProducts")
public class GCIMobileController implements MessageSourceAware {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	private MessageSource messageSource;

	private IGiftCartAPI giftCartApi;
	private final IAuthenticationApi authenticationApi;

	public GCIMobileController(IGiftCartAPI giftCartApi, IAuthenticationApi authenticationApi) {
		this.giftCartApi = giftCartApi;
		this.authenticationApi = authenticationApi;

	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	@RequestMapping(value = "/MobileGetBrands", method = RequestMethod.POST)
	public ResponseEntity<GciMobileResponse> GetBrands(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody GciMobileRequest dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws Exception {

		GciMobileResponse resp = new GciMobileResponse();
		String sessionId = dto.getSessionId();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				if (dto.getSessionId() != null || dto.getSessionId().length() > 0) {
					String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
					System.err.println("Authority :: " + authority);
					if (authority != null) {
						if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
							dto.setSessionId(sessionId);

							String accesstoken = giftCartApi.GetaccessToken();
							String getbrands = giftCartApi.GetBrand(accesstoken);

							JSONArray brands = new JSONArray(getbrands);
							String denominationlength = brands.toString();
							System.err.println("::::::::::LENGTHLLLLLLLLLLL::::" + denominationlength.length());
							if (denominationlength.length() > 50) {
								resp.setSuccess(true);
								resp.setCode("S00");
								resp.setMessage("Fetch All Brand");
								resp.setStatus("Sucees");
								resp.setDetails(getbrands);

							} else {
								resp.setSuccess(false);
								resp.setCode("F00");
								resp.setMessage("Client Authentication Failed");
								resp.setStatus("Failure");
								resp.setResponse(APIUtils.getFailedJSON().toString());
							}

						} else {
							resp.setSuccess(false);
							resp.setCode("F00");
							resp.setMessage("User Authentication Failed");
							resp.setStatus("Failure");
							resp.setResponse(APIUtils.getFailedJSON().toString());
						}
					} else {
						resp.setSuccess(false);
						resp.setCode("F00");
						resp.setMessage("User Authority null");
						resp.setStatus("Failure");
						resp.setResponse(APIUtils.getFailedJSON().toString());
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Session null");
					resp.setStatus("Failure");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Unknown device");
				resp.setStatus("Failure");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Un-authorized Role");
			resp.setStatus("Failure");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<GciMobileResponse>(resp, HttpStatus.OK);

	}

	@RequestMapping(value = "/MobileBrandDenominations", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON }, consumes = { MediaType.APPLICATION_JSON })
	public ResponseEntity<GCIBrandDenominationResponse> getdenomination(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody GetBrandDenominationRequest request1,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws Exception {

		System.err.println(":::::::::::::::::::::::MobileBrandDenominations");
		GCIBrandDenominationResponse denomation = new GCIBrandDenominationResponse();
		String sessionId = request1.getSessionId();
		System.err.println(":::::::SESSION ID :::::::::::::" + sessionId);
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue()))

			{
				if (request1.getSessionId() != null) {
					
					request1.setSessionId(sessionId);
					String accesstoken = giftCartApi.GetaccessToken();
					request1.setAccessToken(accesstoken);

					System.err.println(":::::::::::::::HASH:::::::::::" + request1.getHash());
					System.err.println("::::::::::::::::ACCESS TOKEN ::::::" + accesstoken);

					GCIBrandDenominationResponse resp = giftCartApi.denomation(request1);
					JSONObject jsonObject = new JSONObject(resp.getMessage());
					String denominationlength = resp.toString();
					System.err.println("::::::::::LENGTHLLLLLLLLLLL::::" + denominationlength.length());
					if (denominationlength.length() > 50) {
						try {

							denomation.setSuccess(true);
							denomation.setCode("S00");
							denomation.setMessage("Fetch All Brand Denominations");
							denomation.setStatus("Sucees");
							denomation.setDetails(resp.getMessage());
						} catch (Exception e) {
							e.printStackTrace();
						}
					}

					else {
						denomation.setSuccess(false);
						denomation.setCode("F00");
						denomation.setMessage("Client Authentication Failed");
						denomation.setStatus("Failure");
						denomation.setResponse(APIUtils.getFailedJSON().toString());
					}
					
				} else {
					denomation.setSuccess(false);
					denomation.setCode("F00");
					denomation.setMessage("Session null");
					denomation.setStatus("Failure");
					denomation.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				denomation.setSuccess(false);
				denomation.setCode("F00");
				denomation.setMessage("Unknown device");
				denomation.setStatus("Failure");
				denomation.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			denomation.setSuccess(false);
			denomation.setCode("F00");
			denomation.setMessage("Un-authorized Role");
			denomation.setStatus("Failure");
			denomation.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<GCIBrandDenominationResponse>(denomation, HttpStatus.OK);
	}

	@RequestMapping(value = "/Order", method = RequestMethod.POST)
	String order(Model model, HttpServletResponse response, HttpSession session, GCIOrderProductRequest request)
			throws Exception {
		model.addAttribute("quantity", request.getQuantity());
		model.addAttribute("brandHash", request.getBrandHash());
		model.addAttribute("amount", request.getDenomination());
		model.addAttribute("productType", request.getProductType());
		return "User/GCI/Order";
	}

	@RequestMapping(value = "/MobileAddOrder", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON }, consumes = { MediaType.APPLICATION_JSON })
	public ResponseEntity<GciMobileResponse> getaddorder(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody GCIAddOrderRequest request1,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws Exception {

		System.err.println("::::::::::::::::::::::ADD ORDER : MOBILE:::::::::::::::::::::");
		GciMobileResponse resp = new GciMobileResponse();
		String accesstoken = giftCartApi.GetaccessToken();
		GCIOrderVoucher ov = new GCIOrderVoucher();
		GCIAddOrderResponse denomation = new GCIAddOrderResponse();
		GCIAmountInitateRequest objinit = new GCIAmountInitateRequest();
		GCIAmountsuccessRequest objsuucess = new GCIAmountsuccessRequest();
		String sessionId = request1.getSessionId();
		request1.setAccessToken(accesstoken);
		System.err.println(":::::::SESSION ID :::::::::::::" + sessionId);
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue()))

			{
				if (request1.getSessionId() != null) {
					
					objinit.setSessioniId(sessionId);
					objinit.setAmount(request1.getDenomination());

					
					GCIAmountInitateResponse respinitate = giftCartApi.initate(objinit);

					System.err.println(":::::::RESPONSE FROM: INITITAE:::::::" + respinitate.getMessage());
					if (respinitate.getCode().equalsIgnoreCase("S00")) {
						GCIAddOrderResponse addOrder = giftCartApi.addOrder(request1);

						JSONObject jsonObject = new JSONObject(addOrder.getMessage());

						System.err.println(":::::::RESPONSE FROM: ADD ORDER :::::::" + addOrder.getMessage());

						if (jsonObject.has("error")) {

							ov.setSessioniId(sessionId);
							ov.setTransactionRefNo(respinitate.getTxnId());
							ov.setCode("F04");
							GCIProceedResponse success = giftCartApi.sucees(ov);
							resp.setSuccess(false);
							resp.setCode("F00");
							resp.setMessage("No voucher available ");
							resp.setStatus("Failure");
							resp.setResponse(APIUtils.getFailedJSON().toString());
						}

						else {

							try {


								GCIAddOrder ao = new GCIAddOrder();
								String receiptNo = jsonObject.getString("receiptNo");
								ao.setReceiptNo(receiptNo);
								ov.setReceiptno(receiptNo);
								ov.setTransactionRefNo(respinitate.getTxnId());
								ov.setCode("S00");
								objsuucess.setSessioniId(sessionId);
								objsuucess.setReceiptno(receiptNo);
								// voucher Api Calling
								GCIOrderVoucherRequest voucherobj = new GCIOrderVoucherRequest();
								String accesstoken1 = giftCartApi.GetaccessToken();
								voucherobj.setAccessToken(accesstoken);
								voucherobj.setReceiptNo(receiptNo);
								
								GCIOrderVoucherResponse voucher = giftCartApi.voucher(voucherobj);
								System.err.println("VOUCHER API  RESPONSE" + voucher.getMessage());
								JSONArray ja = new JSONArray(voucher.getMessage());
								for (int i = 0; i < ja.length(); i++) {
									String brandName = ja.getJSONObject(i).getString("brandName");
									String denomination = ja.getJSONObject(i).getString("cardPrice");
									String voucherNumber = ja.getJSONObject(i).getString("voucherNumber");
									String voucherPin = ja.getJSONObject(i).getString("voucherPin");
									String expiryDate = ja.getJSONObject(i).getString("expiryDate");
									ov.setBrandName(brandName);
									ov.setDenomination(denomination);
									ov.setVoucherNumber(voucherNumber);
									ov.setVoucherPin(voucherPin);
									ov.setExpiryDate(expiryDate);
									
								}
								
								String denominationlength3 = voucher.toString();
								System.err.println("VOUCHER API  RESPONSE length" + denominationlength3.length());
								if (jsonObject.has("error")) {
									ov.setSessioniId(sessionId);
									ov.setCode("F04");
									ov.setTransactionRefNo(respinitate.getTxnId());
									GCIProceedResponse success = giftCartApi.sucees(ov);
									resp.setSuccess(false);
									resp.setCode("F00");
									resp.setMessage("Your order  not placed, please try after some time voucher");
									resp.setStatus("Failure");
									resp.setResponse(APIUtils.getFailedJSON().toString());
								}
								// changes here 
								
								
								else {
									try {
										GCIOrderVoucher ordervoucher = new GCIOrderVoucher();
										ordervoucher.setSessioniId(sessionId);
										ordervoucher.setTransactionRefNo(respinitate.getTxnId());
										ordervoucher.setBrandName(ov.getBrandName());
										ordervoucher.setAmount(ov.getDenomination());
										ordervoucher.setVoucherNumber(ov.getVoucherNumber());
										ordervoucher.setVoucherPin(ov.getVoucherPin());
										ordervoucher.setExpiryDate(ov.getExpiryDate());
										ordervoucher.setReceiptno(ov.getReceiptno());
										ordervoucher.setCode(ov.getCode());
										
										GCIProceedResponse success = giftCartApi.sucees(ordervoucher);

										System.err.println(
												":::::::RESPONSE FROM: SUCCESS API :::::::" + success.getMessage());

										if (respinitate.getCode().equalsIgnoreCase("S00")) {
											resp.setCode("S00");
											resp.setMessage("Amount Deduct Successful ");
											resp.setSuccess(true);
											resp.setStatus("Success");
											resp.setResponse(respinitate.getResponse());
										} else {
											resp.setSuccess(false);
											resp.setCode("F00");
											resp.setMessage("Insufficient Fund");
											resp.setStatus("Failure");
											resp.setResponse(APIUtils.getFailedJSON().toString());
										}

									} catch (Exception e) {
										e.printStackTrace();
									}
								}

							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					} else {
						resp.setSuccess(false);
						resp.setCode("F00");
						resp.setMessage("InSufficient Fund");
						resp.setStatus("Failure");
						resp.setResponse(APIUtils.getFailedJSON().toString());
					}

					
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Session null");
					resp.setStatus("Failure");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Unknown device");
				resp.setStatus("Failure");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Un-authorized Role");
			denomation.setStatus("Failure");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<GciMobileResponse>(resp, HttpStatus.OK);
	}

	@RequestMapping(value = "/MobileAddCart", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON }, consumes = { MediaType.APPLICATION_JSON })
	public ResponseEntity<GCIAddCartResponse> getcart(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody GCIAddCartRequest request,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request1,
			HttpServletResponse response, HttpSession session) throws Exception {

		System.err.println(":::::::::::::::::::::::MobileADD CART::::: ");
		GCIAddCartResponse denomation = new GCIAddCartResponse();
		String sessionId = request.getSessioniId();
		System.err.println(":::::::SESSION ID :::::::::::::" + sessionId);
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue()))

			{
				if (request.getSessioniId() != null) {
					
					request.setSessioniId(sessionId);
					String accesstoken = giftCartApi.GetaccessToken();

					GCIAddCartResponse resp = giftCartApi.addcart(request);
					JSONObject jsonObject = new JSONObject(resp.getMessage());
					String denominationlength = resp.toString();
					System.err.println("::::::::::LENGTHLLLLLLLLLLL::::" + denominationlength.length());
					if (denominationlength.length() > 50) {
						try {

							denomation.setSuccess(true);
							denomation.setCode("S00");
							denomation.setMessage("Item Added Successfully in Your Cart");
							denomation.setStatus("Sucees");
							denomation.setDetails(resp.getMessage());
						} catch (Exception e) {
							e.printStackTrace();
						}
					}

					else {
						denomation.setSuccess(false);
						denomation.setCode("F00");
						denomation.setMessage("Client Authentication Failed");
						denomation.setStatus("Failure");
						denomation.setResponse(APIUtils.getFailedJSON().toString());
					}
					
				} else {
					denomation.setSuccess(false);
					denomation.setCode("F00");
					denomation.setMessage("Session null");
					denomation.setStatus("Failure");
					denomation.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				denomation.setSuccess(false);
				denomation.setCode("F00");
				denomation.setMessage("Unknown device");
				denomation.setStatus("Failure");
				denomation.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			denomation.setSuccess(false);
			denomation.setCode("F00");
			denomation.setMessage("Un-authorized Role");
			denomation.setStatus("Failure");
			denomation.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<GCIAddCartResponse>(denomation, HttpStatus.OK);
	}

	@RequestMapping(value = "/MobileShowCart", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON }, consumes = { MediaType.APPLICATION_JSON })
	public ResponseEntity<GCIAddCartResponse> getshowcart(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody GCIAddCartRequest request1,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws Exception {

		System.err.println(":::::::::::::::::::::::MOBILE SHOW CART::::::");
		GCIAddCartResponse denomation = new GCIAddCartResponse();
		String sessionId = request1.getSessioniId();
		System.err.println(":::::::SESSION ID :::::::::::::" + sessionId);
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue()))

			{
				if (request1.getSessioniId() != null) {
					
					request1.setSessioniId(sessionId);
					String accesstoken = giftCartApi.GetaccessToken();
					GCIAddCartResponse resp = giftCartApi.showcart(request1);
					System.err.println("::::::::::::::::::Response From SHOW CART ITEM Api" + resp);
					
					String denominationlength = resp.toString();

					System.err.println("::::::::::LENGTHLLLLLLLLLLL::::" + denominationlength.length());
					if (denominationlength.length() > 50) {
						try {

							denomation.setSuccess(true);
							denomation.setCode("S00");
							denomation.setMessage("Your Item Successfully Show");
							denomation.setStatus("Sucees");
							denomation.setDetails(resp.getMessage());
						} catch (Exception e) {
							e.printStackTrace();
						}
					}

					else {
						denomation.setSuccess(false);
						denomation.setCode("F00");
						denomation.setMessage("Client Authentication Failed");
						denomation.setStatus("Failure");
						denomation.setResponse(APIUtils.getFailedJSON().toString());
					}
					
				} else {
					denomation.setSuccess(false);
					denomation.setCode("F00");
					denomation.setMessage("Session null");
					denomation.setStatus("Failure");
					denomation.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				denomation.setSuccess(false);
				denomation.setCode("F00");
				denomation.setMessage("Unknown device");
				denomation.setStatus("Failure");
				denomation.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			denomation.setSuccess(false);
			denomation.setCode("F00");
			denomation.setMessage("Un-authorized Role");
			denomation.setStatus("Failure");
			denomation.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<GCIAddCartResponse>(denomation, HttpStatus.OK);
	}

	@RequestMapping(value = "/MobileRemove", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON }, consumes = { MediaType.APPLICATION_JSON })
	public ResponseEntity<GCIProceedResponse> getremovecart(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody GCIProceedRequest request1,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws Exception {

		System.err.println(":::::::::::::::::::::::MobileRemove::::::::::::");
		GCIProceedResponse denomation = new GCIProceedResponse();
		String sessionId = request1.getSessionId();
		System.err.println(":::::::SESSION ID :::::::::::::" + sessionId);
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue()))

			{
				if (request1.getSessionId() != null) {
					
					request1.setSessionId(sessionId);
					String accesstoken = giftCartApi.GetaccessToken();
					GCIProceedResponse resp = giftCartApi.proceed(request1);

					System.err.println("RESPONSE::::REMOVE CART ITEM :::::::::::::::::::::::" + resp.getMessage());
					
					String denominationlength = resp.toString();
					System.err.println("::::::::::LENGTHLLLLLLLLLLL::::" + denominationlength.length());
					if (denominationlength.length() > 10) {
						try {

							denomation.setSuccess(true);
							denomation.setCode("S00");
							denomation.setMessage("Your Item Successfully Remove From Your Cart");
							denomation.setStatus("Sucees");
							denomation.setDetails(resp.getMessage());
						} catch (Exception e) {
							e.printStackTrace();
						}
					}

					else {
						denomation.setSuccess(false);
						denomation.setCode("F00");
						denomation.setMessage("Client Authentication Failed");
						denomation.setStatus("Failure");
						denomation.setResponse(APIUtils.getFailedJSON().toString());
					}
					
				} else {
					denomation.setSuccess(false);
					denomation.setCode("F00");
					denomation.setMessage("Session null");
					denomation.setStatus("Failure");
					denomation.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				denomation.setSuccess(false);
				denomation.setCode("F00");
				denomation.setMessage("Unknown device");
				denomation.setStatus("Failure");
				denomation.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			denomation.setSuccess(false);
			denomation.setCode("F00");
			denomation.setMessage("Un-authorized Role");
			denomation.setStatus("Failure");
			denomation.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<GCIProceedResponse>(denomation, HttpStatus.OK);
	}

}
