package com.payqwikweb.controller.mobile.api;

import java.net.InetAddress;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.ResponseStatus;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.util.APIUtils;
import com.payqwikweb.util.ConvertUtil;
import com.upi.api.IUPIApi;
import com.upi.model.requet.GenerateDEKRequest;
import com.upi.model.requet.MerchantCheckTxnStatus;
import com.upi.model.requet.MerchantCollectMoneyRequest;
import com.upi.model.requet.MerchantTokenValidRequest;
import com.upi.model.requet.VPARequest;
import com.upi.model.response.GenerateMerchantDEKResponse;
import com.upi.model.response.MerchantMetaVPAResponse;
import com.upi.model.response.MerchantTokenGenResponse;
import com.upi.model.response.MerchantTokenValidResponse;
import com.upi.model.response.MerchantTxnStatusResponse;
import com.upi.util.DesEncryption;

@Controller
@RequestMapping("/Api/{version}/{role}/{device}/{language}/UPI")
public class UPIMobileController {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private final IUPIApi upiApi;

	public UPIMobileController(IUPIApi upiApi) {
		this.upiApi = upiApi;
	}

	@RequestMapping(value = "GenerateKeys", method = RequestMethod.GET)
	ResponseEntity<GenerateMerchantDEKResponse> generateDEK(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language) {
		GenerateMerchantDEKResponse result = new GenerateMerchantDEKResponse();
		try{
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				result = upiApi.getMerchantDEK(ConvertUtil.generatDEKRequest());
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unknown device");
			}
		} else {
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("Unauthorised access");
		}
	} catch (Exception e) {
		e.printStackTrace();
		result.setSuccess(false);
		result.setCode("F00");
		result.setMessage("We are sorry for inconvenience, Please try again later .");
	}
		return new ResponseEntity<GenerateMerchantDEKResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/ValidateMerchantInfo", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<MerchantMetaVPAResponse> genMerchantToken(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody MerchantCollectMoneyRequest dto,
			@RequestHeader(value = "hash", required = false) String hash) {
		MerchantMetaVPAResponse metaVPAResponse = new MerchantMetaVPAResponse();
		try{
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				try {
					System.out.println();
					MerchantTokenGenResponse result = upiApi
							.genMerchantToken(ConvertUtil.generateMerchantTokenRequest(dto));
					if (result.getCode().equals(ResponseStatus.SUCCESS.getValue())) {
						MerchantTokenValidRequest tokenValidRequest = ConvertUtil.tokenValidRequest(result.getToken(), false, null);
						MerchantTokenValidResponse validResponse = upiApi.validateMerchantMetaDataInfo(
								ConvertUtil.merchantMetaInfoRequest(tokenValidRequest.getMsgId(), DesEncryption
										.desEncrypt(tokenValidRequest.getRequest(), tokenValidRequest.getMsgId()), dto.isSdk()));
						if (validResponse.getCode().equals(ResponseStatus.SUCCESS.getValue())) {
							VPARequest vpaRequest = ConvertUtil.vpaRequest(dto);
							metaVPAResponse = upiApi.validateMerchantMetaVPA(
									ConvertUtil.validateMerchantVPARequest(vpaRequest.getMsgId(),
											DesEncryption.desEncrypt(vpaRequest.getRequest(), vpaRequest.getMsgId())));
							if (metaVPAResponse.getCode().equals(ResponseStatus.SUCCESS.getValue())) {
								MerchantCollectMoneyRequest cmRequest = ConvertUtil.collectMoneyRequest(dto);
								metaVPAResponse = upiApi.validateCollectMoney(ConvertUtil
										.validateMerchantCollectMoneyRequest(cmRequest.getMsgId(), DesEncryption
												.desEncrypt(cmRequest.getRequest(), cmRequest.getMsgId())));
								if (metaVPAResponse.getCode().equals(ResponseStatus.SUCCESS.getValue())) {
									MerchantCheckTxnStatus checkTxnStatus = ConvertUtil  
											.merchantStatusRequest(cmRequest.getMsgId());
									MerchantTxnStatusResponse resp = upiApi.maerchantTxnStatus(ConvertUtil
											.merchantTxnStatusRequest(checkTxnStatus.getMsgId(), DesEncryption
													.desEncrypt(checkTxnStatus.getRequest(), checkTxnStatus.getMsgId())));
									if (resp.getCode().equals(ResponseStatus.SUCCESS.getValue())) {
										metaVPAResponse.setStatus(ResponseStatus.SUCCESS);
										return new ResponseEntity<MerchantMetaVPAResponse>(metaVPAResponse, HttpStatus.OK);
									} else {
										metaVPAResponse.setStatus(ResponseStatus.FAILURE);
									}
								} else {
									metaVPAResponse.setStatus(ResponseStatus.FAILURE);
								}
								
							}
						} else {
							metaVPAResponse.setStatus(ResponseStatus.FAILURE);
							metaVPAResponse.setMessage(validResponse.getMessage());
						}
					} else {
						metaVPAResponse.setStatus(ResponseStatus.FAILURE);
						metaVPAResponse.setMessage(result.getMessage());
					}
				} catch (Exception e) {
					e.printStackTrace();
					metaVPAResponse.setStatus(ResponseStatus.FAILURE);
					metaVPAResponse.setMessage("Internal Server Error");
				}
			} else {
				metaVPAResponse.setCode("F00");
				metaVPAResponse.setMessage("Unknown device");
			}
		} else {
			metaVPAResponse.setCode("F00");
			metaVPAResponse.setMessage("Unauthorised access");
		}
		} catch (Exception e) {
			e.printStackTrace();
			metaVPAResponse.setCode("F00");
			metaVPAResponse.setMessage("We are sorry for inconvenience, Please try again later .");
		}
		return new ResponseEntity<MerchantMetaVPAResponse>(metaVPAResponse, HttpStatus.OK);
	}

	@RequestMapping(value = "/UpiStatus", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<MerchantTxnStatusResponse> upiStatus(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody MerchantCollectMoneyRequest dto,
			@RequestHeader(value = "hash", required = false) String hash) {
		MerchantTxnStatusResponse resp = new MerchantTxnStatusResponse();
		try{
			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					try {
						MerchantCheckTxnStatus checkTxnStatus = ConvertUtil
								.merchantStatusRequest(dto.getMsgId());
						 resp = upiApi.maerchantTxnStatus(ConvertUtil
								.merchantTxnStatusRequest(checkTxnStatus.getMsgId(), DesEncryption
										.desEncrypt(checkTxnStatus.getRequest(), checkTxnStatus.getMsgId())));
							return new ResponseEntity<MerchantTxnStatusResponse>(resp, HttpStatus.OK);
					} catch (Exception e) {
						e.printStackTrace();
						resp.setStatus(ResponseStatus.FAILURE);
						resp.setMessage("Internal Server Error");
					}
				} else {
					resp.setCode("F00");
					resp.setMessage("Unknown device");
				}
			} else {
				resp.setCode("F00");
				resp.setMessage("Unauthorised access");
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setCode("F00");
			resp.setMessage("We are sorry for inconvenience, Please try again later .");
		}
		return new ResponseEntity<MerchantTxnStatusResponse>(resp, HttpStatus.OK);
	}
	
	// TODO BESCOM APIs 
	@RequestMapping(value = "GenerateBescomKeys", method = RequestMethod.GET)
	ResponseEntity<GenerateMerchantDEKResponse> generateBescomDEK(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody GenerateDEKRequest dto) {
		GenerateMerchantDEKResponse result = new GenerateMerchantDEKResponse();
		try{
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				result = upiApi.getMerchantDEKBescom(ConvertUtil.generatBescomDEKRequest(dto.getMobileNo()));
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unknown device");
			}
		} else {
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("Unauthorised access");
		}
	} catch (Exception e) {
		e.printStackTrace();
		result.setSuccess(false);
		result.setCode("F00");
		result.setMessage("We are sorry for inconvenience, Please try again later .");
	}
		return new ResponseEntity<GenerateMerchantDEKResponse>(result, HttpStatus.OK);
	}
	
	 /*public static void main(String args[]) throws Exception
	    {
	        // Returns the instance of InetAddress containing
	        // local host name and address
	        InetAddress localhost = InetAddress.getLocalHost();
	        System.out.println("System IP Address : " +
	                      (localhost.getHostAddress()).trim());
	       String ss[]= "https://pmlforexlive.com".split("//");
	       System.err.println(ss[0]);
	       System.err.println(ss[1]);
	        
	        InetAddress ip=InetAddress.getByName("www."+ss[1]);  
	        
	        System.out.println("Host Name: "+ip.getHostName());  
	        System.out.println("IP Address: "+ip.getHostAddress()); 
	 
	        // Find public IP address
	        String systemipaddress = "";
	        try
	        {
	            URL url_name = new URL("http://bot.whatismyipaddress.com");
	 
	            BufferedReader sc =
	            new BufferedReader(new InputStreamReader(url_name.openStream()));
	 
	            // reads system IPAddress
	            systemipaddress = sc.readLine().trim();
	        }
	        catch (Exception e)
	        {
	            systemipaddress = "Cannot Execute Properly";
	        }
	        System.out.println("Public IP Address: " + systemipaddress +"\n");
	    }*/
}
