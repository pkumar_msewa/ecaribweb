package com.payqwikweb.controller.mobile.api.thirdparty;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.imagica.api.IServicesApi;
import com.imagica.model.request.OrderDTO;
import com.imagica.model.request.PaymentRequest;
import com.imagica.model.request.SearchDTO;
import com.imagica.model.response.OrderResponse;
import com.imagica.model.response.PaymentResponse;
import com.imagica.model.response.SearchResponse;
import com.imagica.util.IParkConvertUtil;
import com.payqwikweb.api.IAuthenticationApi;
import com.payqwikweb.app.api.IUserApi;
import com.payqwikweb.app.model.request.SessionDTO;
import com.payqwikweb.model.web.ImagicaAuthDTO;
import com.payqwikweb.model.web.ImagicaOrderRequest;
import com.payqwikweb.model.web.ImagicaOrderResponse;
import com.payqwikweb.model.web.ImagicaPaymentRequest;
import com.payqwikweb.model.web.ImagicaPaymentResponse;
import com.payqwikweb.model.web.LocationRequest;
import com.payqwikweb.model.web.LocationResponse;

@RequestMapping("/Api/{version}/{role}/{device}/{language}/Imagica")
public class ImagicaMobileController implements MessageSourceAware {
	private IServicesApi servicesApi;
	private IUserApi userApi;
	private final IAuthenticationApi authenticationApi;

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	private MessageSource messageSource;

	public ImagicaMobileController(IServicesApi servicesApi,IUserApi userApi, IAuthenticationApi authenticationApi) {
		this.servicesApi = servicesApi;
		this.userApi = userApi;
		this.authenticationApi = authenticationApi;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	@RequestMapping(value = "/SearchTickets", method = RequestMethod.POST)
	public ResponseEntity<SearchResponse> getAccessToken(@PathVariable(value = "version") String version,
														 @PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
														 @PathVariable(value = "language") String language, @RequestBody SearchDTO dto,
														 @RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
														 HttpServletResponse response, HttpSession session) throws Exception {

		SearchResponse resp = new SearchResponse();
		if(version.equalsIgnoreCase("v1")) {
			if(role.equalsIgnoreCase("User")) {
				if(device.equalsIgnoreCase("Android") || device.equalsIgnoreCase("IOS")) {
					if(language.equalsIgnoreCase("EN")) {
						SessionDTO sessionDTO = new SessionDTO();
						sessionDTO.setSessionId(dto.getSessionId());
						ImagicaAuthDTO imagicaAuthDTO = userApi.getImagicaAuth(sessionDTO);
						if(imagicaAuthDTO.getSuccess()) {
							dto.setCookie(imagicaAuthDTO.getCookie());
							dto.setToken(imagicaAuthDTO.getToken());
							resp = servicesApi.getParkSearchResult(dto);
						} else {
							resp.setSuccess(false);
							resp.setMessage(imagicaAuthDTO.getMessage());
						}
					}else {
						resp.setSuccess(false);
						resp.setMessage("Only EN locale available");
					}
				} else {
					resp.setSuccess(false);
					resp.setMessage("Unauthorized Device");
				}
			} else {
				resp.setSuccess(false);
				resp.setMessage("Unauthorized Role");
			}
		}else {
			resp.setSuccess(false);
			resp.setMessage("Invalid Version");
		}
		return new ResponseEntity<>(resp, HttpStatus.OK);
	}


	@RequestMapping(value = {"/PlaceOrder","/AddToCart"}, method = RequestMethod.POST)
	public ResponseEntity<ImagicaOrderResponse> placeOrder(@PathVariable(value = "version") String version,
														   @PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
														   @PathVariable(value = "language") String language, @RequestBody OrderDTO dto,
														   @RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
														   HttpServletResponse response, HttpSession session) throws Exception {

		ImagicaOrderResponse resp = new ImagicaOrderResponse();
		if(version.equalsIgnoreCase("v1")) {
			if(role.equalsIgnoreCase("User")) {
				if(device.equalsIgnoreCase("Android") || device.equalsIgnoreCase("IOS")) {
					if(language.equalsIgnoreCase("EN")) {
						SessionDTO sessionDTO = new SessionDTO();
						sessionDTO.setSessionId(dto.getSessionId());
						ImagicaAuthDTO imagicaAuthDTO = userApi.getImagicaAuth(sessionDTO);
						if(imagicaAuthDTO.getSuccess()) {
							dto.setCookie(imagicaAuthDTO.getCookie());
							dto.setToken(imagicaAuthDTO.getToken());
							OrderResponse orderResponse = servicesApi.placeOrderInCart(dto);
							if(orderResponse.isSuccess()) {
								System.err.println("hi m here.....");
								//resp.setMessage("Oopps!! Ticket is not available at this moment...");
								//resp.setSuccess(false);
								ImagicaOrderRequest orderRequest = IParkConvertUtil.covertFromResponse(orderResponse);
								orderRequest.setSessionId(dto.getSessionId());
								resp = userApi.placeImagicaOrder(orderRequest);
								//return new ResponseEntity<>(resp, HttpStatus.OK); 
							} else {
								resp.setSuccess(false);
								
								resp.setMessage(orderResponse.getMessage());
								resp.setErrorMsgs(orderResponse.getErrorsMsgs());
							}
						} else {
							resp.setSuccess(false);
							resp.setMessage(imagicaAuthDTO.getMessage());
						}
					}else {
						resp.setSuccess(false);
						resp.setMessage("Only EN locale available");
					}
				} else {
					resp.setSuccess(false);
					resp.setMessage("Unauthorized Device");
				}
			} else {
				resp.setSuccess(false);
				resp.setMessage("Unauthorized Role");
			}
		}else {
			resp.setSuccess(false);
			resp.setMessage("Invalid Version");
		}
		return new ResponseEntity<>(resp, HttpStatus.OK);
	}

	@RequestMapping(value = {"/ProcessPayment","/ImagicaPay"}, method = RequestMethod.POST)
	public ResponseEntity<ImagicaPaymentResponse> processPayment(@PathVariable(value = "version") String version,
																 @PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
																 @PathVariable(value = "language") String language, @RequestBody PaymentRequest dto,
																 @RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
																 HttpServletResponse response, HttpSession session) throws Exception {

		ImagicaPaymentResponse resp = new ImagicaPaymentResponse();
		if(version.equalsIgnoreCase("v1")) {
			if(role.equalsIgnoreCase("User")) {
				if(device.equalsIgnoreCase("Android") || device.equalsIgnoreCase("IOS")) {
					if(language.equalsIgnoreCase("EN")) {
						SessionDTO sessionDTO = new SessionDTO();
						sessionDTO.setSessionId(dto.getSessionId());
						ImagicaAuthDTO imagicaAuthDTO = userApi.getImagicaAuth(sessionDTO);
						if(imagicaAuthDTO.getSuccess()) {
							dto.setCookie(imagicaAuthDTO.getCookie());
							dto.setToken(imagicaAuthDTO.getToken());
							PaymentResponse paymentResponse =  servicesApi.processPayment(dto);
							ImagicaPaymentRequest paymentRequest = IParkConvertUtil.convertFromResponse(paymentResponse);
							paymentRequest.setSessionId(dto.getSessionId());
							paymentRequest.setTransctionId(dto.getTransactionId());
							resp = userApi.processPayment(paymentRequest);
						} else {
							resp.setSuccess(false);
							resp.setMessage(imagicaAuthDTO.getMessage());
						}
					}else {
						resp.setSuccess(false);
						resp.setMessage("Only EN locale available");
					}
				} else {
					resp.setSuccess(false);
					resp.setMessage("Unauthorized Device");
				}
			} else {
				resp.setSuccess(false);
				resp.setMessage("Unauthorized Role");
			}
		}else {
			resp.setSuccess(false);
			resp.setMessage("Invalid Version");
		}
		return new ResponseEntity<>(resp, HttpStatus.OK);
	}


	@RequestMapping(value = {"/LocationInfo","/GetLocation"}, method = RequestMethod.POST)
	public ResponseEntity<LocationResponse> getLocationDetails(@PathVariable(value = "version") String version,
																 @PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
																 @PathVariable(value = "language") String language, @RequestBody LocationRequest dto,
																 @RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
																 HttpServletResponse response, HttpSession session) throws Exception {

		LocationResponse resp = new LocationResponse();
		if(version.equalsIgnoreCase("v1")) {
			if(role.equalsIgnoreCase("User")) {
				if(device.equalsIgnoreCase("Android") || device.equalsIgnoreCase("IOS")) {
					if(language.equalsIgnoreCase("EN")) {
						resp = userApi.getLocationByPin(dto);
					}else {
						resp.setSuccess(false);
						resp.setMessage("Only EN locale available");
					}
				} else {
					resp.setSuccess(false);
					resp.setMessage("Unauthorized Device");
				}
			} else {
				resp.setSuccess(false);
				resp.setMessage("Unauthorized Role");
			}
		}else {
			resp.setSuccess(false);
			resp.setMessage("Invalid Version");
		}
		return new ResponseEntity<>(resp, HttpStatus.OK);
	}


}