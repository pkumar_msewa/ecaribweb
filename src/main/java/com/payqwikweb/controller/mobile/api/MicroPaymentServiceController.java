package com.payqwikweb.controller.mobile.api;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.payqwikweb.app.api.impl.MicroPaymentServiceApi;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.request.MicroPaymentInitiateRequest;
import com.payqwikweb.app.model.request.MicroPaymentRequest;
import com.payqwikweb.app.model.response.MicroPaymentResponse;
import com.payqwikweb.util.APIUtils;
import com.payqwikweb.validation.MicroPaymentServiceValidation;

@Controller
@RequestMapping("/Api/{version}/{role}/{device}/{language}/{partner}/User")
public class MicroPaymentServiceController {

	private MicroPaymentServiceApi microPaymentServiceApi;

	public MicroPaymentServiceController(MicroPaymentServiceApi microPaymentServiceApi) {
		this.microPaymentServiceApi = microPaymentServiceApi;
	}

	@RequestMapping(value = "/GetNikkiToken", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<MicroPaymentResponse> getNikkiToken(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @PathVariable(value = "partner") String partner,
			@RequestBody MicroPaymentRequest dto, @RequestHeader(value = "hash", required = false) String hash) {
		MicroPaymentResponse result = new MicroPaymentResponse();
		try {
			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue())
						|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					if (partner.equalsIgnoreCase("Nikki")) {
						// validate
						result = microPaymentServiceApi.getNikkiToken(dto);
						return new ResponseEntity<MicroPaymentResponse>(result, HttpStatus.OK);
					}
					result.setCode("F01");
					result.setDetails("Unauthorised URL Access");
					result.setError("True");
					result.setMessage("Unauthorised URL Access");
					result.setStatus("Failure");
					return new ResponseEntity<MicroPaymentResponse>(result, HttpStatus.OK);
				}
				result.setCode("F01");
				result.setDetails("Unauthorised Access");
				result.setError("True");
				result.setMessage("Unauthorised Access");
				result.setStatus("Failure");
				return new ResponseEntity<MicroPaymentResponse>(result, HttpStatus.OK);
			}
			result.setCode("F01");
			result.setDetails("Unauthorised Role Access");
			result.setError("True");
			result.setMessage("Unauthorised Role Access");
			result.setStatus("Failure");
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<MicroPaymentResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/IntinateChatTransaction", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<MicroPaymentResponse> intinateChatTransaction(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @PathVariable(value = "partner") String partner,
			@RequestBody MicroPaymentInitiateRequest dto,
			@RequestHeader(value = "hash", required = false) String hash) {
		MicroPaymentResponse result = new MicroPaymentResponse();
		try {
			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue())
						|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					if (partner.equalsIgnoreCase("Nikki")) {
						if (MicroPaymentServiceValidation.validateMicroPaymentInitiateRequest(dto)) {
							if (dto.getAmount() > 1) {
								result = microPaymentServiceApi.getIntinateNikkiTransaction(dto);
							} else {
								result.setSuccess(false);
								result.setCode("F00");
								result.setDetails("Invalid Parameters");
								result.setMessage("Invaild Amount");
								result.setStatus("FAILED");
								result.setResponse(APIUtils.getFailedJSON().toString());
							}
						} else {
							result.setSuccess(false);
							result.setCode("F00");
							result.setDetails("Invalid Parameters");
							result.setMessage("Parameters can't be null");
							result.setStatus("FAILED");
							result.setResponse(APIUtils.getFailedJSON().toString());

						}
					} else {
						result.setSuccess(false);
						result.setCode("F00");
						result.setMessage("Unauthorised URL Access");
						result.setStatus("FAILED");
						result.setResponse(APIUtils.getFailedJSON().toString());
					}
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Unknown device");
					result.setStatus("FAILED");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unauthorised access");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<MicroPaymentResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/UpdateChatTransaction", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<MicroPaymentResponse> updateChatTransaction(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @PathVariable(value = "partner") String partner,
			@RequestBody MicroPaymentRequest dto, @RequestHeader(value = "hash", required = false) String hash) {
		MicroPaymentResponse result = new MicroPaymentResponse();
		try {
			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue())
						|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					if (partner.equalsIgnoreCase("Nikki")) {
						try {
							if (MicroPaymentServiceValidation.validateTokenMicroPaymentRequest(dto)) {
								if (dto.getAmount() > 1) {
									result = microPaymentServiceApi.getUpdateNikkiTransaction(dto);
									return new ResponseEntity<MicroPaymentResponse>(result, HttpStatus.OK);
								}
								result.setCode("F01");
								result.setDetails("Invalid Parameters");
								result.setError("True");
								result.setMessage("Invaild Amount");
								result.setStatus("Failure");
								return new ResponseEntity<MicroPaymentResponse>(result, HttpStatus.OK);
							}
						} catch (Exception ex) {
						}
						result.setCode("F01");
						result.setDetails("Invalid Parameters");
						result.setError("True");
						result.setMessage("Parameters can't be null");
						result.setStatus("Failure");

						return new ResponseEntity<MicroPaymentResponse>(result, HttpStatus.OK);

					}
					result.setCode("F01");
					result.setDetails("Unauthorised URL Access");
					result.setError("True");
					result.setMessage("Unauthorised URL Access");
					result.setStatus("Failure");
					return new ResponseEntity<MicroPaymentResponse>(result, HttpStatus.OK);
				}
				result.setCode("F01");
				result.setDetails("Unauthorised Access");
				result.setError("True");
				result.setMessage("Unauthorised Access");
				result.setStatus("Failure");
				return new ResponseEntity<MicroPaymentResponse>(result, HttpStatus.OK);
			}
			result.setCode("F01");
			result.setDetails("Unauthorised Role Access");
			result.setError("True");
			result.setMessage("Unauthorised Role Access");
			result.setStatus("Failure");
			return new ResponseEntity<MicroPaymentResponse>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
			return new ResponseEntity<MicroPaymentResponse>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/RefundChatTransaction", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<MicroPaymentResponse> refundChatTransaction(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @PathVariable(value = "partner") String partner,
			@RequestBody MicroPaymentRequest dto, @RequestHeader(value = "hash", required = false) String hash) {
		MicroPaymentResponse result = new MicroPaymentResponse();
		try{
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				if (partner.equalsIgnoreCase("Nikki")) {
					try {
						result = microPaymentServiceApi.getRefundNikkiTransaction(dto);
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				} else {
					result.setCode("F01");
					result.setDetails("Invalid Parameters");
					result.setError("True");
					result.setMessage("Parameters can't be null");
					result.setStatus("Failure");
				}
			} else {
				result.setCode("F01");
				result.setDetails("Unauthorised URL Access");
				result.setError("True");
				result.setMessage("Unauthorised URL Access");
				result.setStatus("Failure");
			}
		} else {
			result.setCode("F01");
			result.setDetails("Unauthorised Access");
			result.setError("True");
			result.setMessage("Unauthorised Access");
			result.setStatus("Failure");
		}
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		
		return new ResponseEntity<MicroPaymentResponse>(result, HttpStatus.OK);
	}

}
