package com.payqwikweb.controller.mobile.api;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Base64;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import com.gci.model.request.LoginDTO;
import com.payqwik.visa.api.IVisaApi;
import com.payqwik.visa.util.VisaResponseDTO;
import com.payqwikweb.api.IAuthenticationApi;
import com.payqwikweb.app.api.IQwikrPayApi;
import com.payqwikweb.app.api.IUserApi;
import com.payqwikweb.app.api.IVersionApi;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.ResponseStatus;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.request.ChangeMPINRequest;
import com.payqwikweb.app.model.request.ChangePasswordRequest;
import com.payqwikweb.app.model.request.DeleteMPINRequest;
import com.payqwikweb.app.model.request.EditProfileRequest;
import com.payqwikweb.app.model.request.FavouriteRequest;
import com.payqwikweb.app.model.request.FetchMobileList;
import com.payqwikweb.app.model.request.ForgotMpinRequest;
import com.payqwikweb.app.model.request.GetAccountNumber;
import com.payqwikweb.app.model.request.InviteFriendEmailRequest;
import com.payqwikweb.app.model.request.InviteFriendMobileRequest;
import com.payqwikweb.app.model.request.KycVerificationDTO;
import com.payqwikweb.app.model.request.NearByAgentsRequest;
import com.payqwikweb.app.model.request.NearByMerchantsRequest;
import com.payqwikweb.app.model.request.ReceiptsRequest;
import com.payqwikweb.app.model.request.RedeemCodeRequest;
import com.payqwikweb.app.model.request.SessionDTO;
import com.payqwikweb.app.model.request.SetMPINRequest;
import com.payqwikweb.app.model.request.UploadPictureRequest;
import com.payqwikweb.app.model.request.UserDetailsRequest;
import com.payqwikweb.app.model.request.UserMaxLimitDto;
import com.payqwikweb.app.model.request.VerifyMPINRequest;
import com.payqwikweb.app.model.request.VisaRequest;
import com.payqwikweb.app.model.response.ChangePasswordResponse;
import com.payqwikweb.app.model.response.EditProfileResponse;
import com.payqwikweb.app.model.response.InviteFriendEmailResponse;
import com.payqwikweb.app.model.response.InviteFriendMobileResponse;
import com.payqwikweb.app.model.response.KycResponse;
import com.payqwikweb.app.model.response.MPINResponse;
import com.payqwikweb.app.model.response.MerchantResponse;
import com.payqwikweb.app.model.response.NearByAgentResponse;
import com.payqwikweb.app.model.response.NearBymerchantResponse;
import com.payqwikweb.app.model.response.NotificationsResponse;
import com.payqwikweb.app.model.response.ReceiptsResponse;
import com.payqwikweb.app.model.response.SharePointsResponse;
import com.payqwikweb.app.model.response.UploadPictureResponse;
import com.payqwikweb.app.model.response.UserDetailsResponse;
import com.payqwikweb.app.model.response.VisaResponse;
import com.payqwikweb.model.app.request.QwikrPayRequestDTO;
import com.payqwikweb.model.app.request.UpdateReceiptDTO;
import com.payqwikweb.model.app.request.VersionRequestDTO;
import com.payqwikweb.model.app.response.RedeemCodeResponse;
import com.payqwikweb.model.app.response.UpdateReceiptResponse;
import com.payqwikweb.model.app.response.VersionResponseDTO;
import com.payqwikweb.model.error.KycError;
import com.payqwikweb.model.error.MpinError;
import com.payqwikweb.model.web.SharePointDTO;
import com.payqwikweb.util.APIUtils;
import com.payqwikweb.util.Authorities;
import com.payqwikweb.util.ConvertUtil;
import com.payqwikweb.validation.CommonValidation;
import com.payqwikweb.validation.EditProfileValidation;
import com.payqwikweb.validation.KycValidation;
import com.payqwikweb.validation.MpinValidation;
import com.thirdparty.model.ResponseDTO;

@Controller
@RequestMapping("/Api/{version}/{role}/{device}/{language}/User")
public class UserAPIController implements MessageSourceAware {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private static SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
	private static SimpleDateFormat dateFormat = new SimpleDateFormat("YYYY-mm-DD");

	private MessageSource messageSource;
	private final IUserApi userApi;
	private final MpinValidation mpinValidation;
	private final IAuthenticationApi authenticationApi;
	private final KycValidation kycValidation;
	private final IVisaApi visaApi;
	private final IQwikrPayApi qwikrPayApi;
	private final IVersionApi versionApi;
	private final EditProfileValidation editProfileValidation;

	public UserAPIController(IUserApi userApi, MpinValidation mpinValidation, IAuthenticationApi authenticationApi,
			KycValidation kycValidation, IVisaApi visaApi, IQwikrPayApi qwikrPayApi, IVersionApi versionApi,
			EditProfileValidation editProfileValidation) {
		this.userApi = userApi;
		this.mpinValidation = mpinValidation;
		this.authenticationApi = authenticationApi;
		this.kycValidation = kycValidation;
		this.visaApi = visaApi;
		this.qwikrPayApi = qwikrPayApi;
		this.versionApi = versionApi;
		this.editProfileValidation = editProfileValidation;
	}

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	@RequestMapping(value = "/GetUserDetails", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<UserDetailsResponse> getUserDetails(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody UserDetailsRequest dto,
			@RequestHeader(value = "hash", required = false) String hash) {
		UserDetailsResponse result = new UserDetailsResponse();
		try {
			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue())
						|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					result = userApi.getUserDetails(dto, Role.USER);
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Unknown device");
					result.setStatus("FAILED");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else if (role.equalsIgnoreCase(Role.MERCHANT.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue())
						|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					result = userApi.getUserDetails(dto, Role.MERCHANT);
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Unknown device");
					result.setStatus("FAILED");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else if (role.equalsIgnoreCase(Role.DONATEE.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue())
						|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					result = userApi.getUserDetails(dto, Role.DONATEE);
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Unknown device");
					result.setStatus("FAILED");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else if (role.equalsIgnoreCase(Role.AGENT.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue())
						|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					result = userApi.getUserDetails(dto, Role.AGENT);
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Unknown device");
					result.setStatus("FAILED");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unauthorised access");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<UserDetailsResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/GetDetails/{infoName}", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<?> getDetailByName(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @PathVariable(value = "infoName") String info,
			@RequestBody UserDetailsRequest dto, @RequestHeader(value = "hash", required = false) String hash) {
		Object resultObject = null;
		UserDetailsResponse result = new UserDetailsResponse();
		try {
			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue())
						|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					switch (info.toUpperCase()) {
					case "ACCOUNT":
						resultObject = userApi.getAccountDetails(dto, Role.USER);
						break;
					case "BALANCE":
						resultObject = userApi.getBalanceDetails(dto, Role.USER);
						break;
					case "LOCATION":
						resultObject = userApi.getLocationDetails(dto, Role.USER);
						break;
					case "IMAGE":
						resultObject = userApi.getImageDetails(dto, Role.USER);
						break;
					case "LIMITS":
						resultObject = userApi.getLimitDetails(dto, Role.USER);
						break;
					default:
						resultObject = userApi.getBasicDetails(dto, Role.USER);
						break;
					}
					return new ResponseEntity<Object>(resultObject, HttpStatus.OK);
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Unknown device");
					result.setStatus("FAILED");
					result.setResponse(APIUtils.getFailedJSON().toString());
					return new ResponseEntity<Object>(result, HttpStatus.OK);
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unauthorised access");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
				return new ResponseEntity<Object>(result, HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<Object>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/EditProfile", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<EditProfileResponse> editProfile(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody EditProfileRequest dto,
			@RequestHeader(value = "hash", required = false) String hash) {
		EditProfileResponse result = new EditProfileResponse();
		try {
			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue())
						|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					result = userApi.editProfile(dto);
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Unknown device");
					result.setStatus("FAILED");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unauthorised access");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}

		return new ResponseEntity<EditProfileResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/GetMerchants", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<MerchantResponse> listMerchants(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody SessionDTO dto,
			@RequestHeader(value = "hash", required = false) String hash) {
		MerchantResponse result = new MerchantResponse();
		try {
			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue())
						|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					result = userApi.getAllMerchants(dto);
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Unknown device");
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unauthorised access");
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<MerchantResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/SharePoints", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<SharePointsResponse> sharePointsToUser(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody SharePointDTO dto,
			@RequestHeader(value = "hash", required = false) String hash) {
		SharePointsResponse result = new SharePointsResponse();
		try {
			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue())
						|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					result = userApi.sharePoints(dto);
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Unknown device");
					result.setStatus("FAILED");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unauthorised access");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<SharePointsResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/ChangePassword", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ChangePasswordResponse> changePassword(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody ChangePasswordRequest dto,
			@RequestHeader(value = "hash", required = false) String hash) {
		ChangePasswordResponse result = new ChangePasswordResponse();
		try {
			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue())
						|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					result = userApi.changePassword(dto);
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Unknown device");
					result.setStatus("FAILED");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else if (role.equalsIgnoreCase(Role.MERCHANT.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue())
						|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					result = userApi.updateMerchantPassword(dto);
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Unknown device");
					result.setStatus("FAILED");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unauthorised access");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<ChangePasswordResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/UploadPicture", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<UploadPictureResponse> uploadPicture(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @ModelAttribute UploadPictureRequest dto,
			HttpServletRequest request, HttpServletResponse response) {
		UploadPictureResponse result = new UploadPictureResponse();
		try {
			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue())
						|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					MultipartFile file = dto.getProfilePicture();
					boolean validImage = isValidImage(file);
					if (validImage) {
						dto.setSessionId(dto.getSessionId());
						result = userApi.uploadPicture(dto);
					} else {
						result.setMessage("Please enter valid file format and valid size");
					}
				} else {
					result.setCode("F00");
					result.setMessage("Unknown device");
					result.setStatus("FAILED");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unauthorised access");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<UploadPictureResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/DownloadPicture", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public void downloadPicture(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @ModelAttribute SessionDTO dto,
			HttpServletRequest request, HttpServletResponse response) {
		UserDetailsResponse detailResponse = new UserDetailsResponse();
		try {
			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue())
						|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					UserDetailsRequest details = new UserDetailsRequest();
					details.setSessionId(dto.getSessionId());
					detailResponse = userApi.getUserDetails(details, Role.USER);
					if (detailResponse.isSuccess()) {
						ByteArrayOutputStream baos = new ByteArrayOutputStream();
						try {
							byte[] image = Base64.getDecoder().decode(detailResponse.getImageContent());
							response.setContentType(detailResponse.getImage());
							InputStream in = new ByteArrayInputStream(image);
							String name = detailResponse.getImage();
							String[] typeSplit = name.split("/");
							String format = typeSplit[typeSplit.length - 1];
							BufferedImage bImageFromConvert = ImageIO.read(in);
							ImageIO.write(bImageFromConvert, format, baos);
							ServletOutputStream responseOutputStream = response.getOutputStream();
							responseOutputStream.write(baos.toByteArray());
							responseOutputStream.flush();
							responseOutputStream.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			detailResponse.setSuccess(false);
			detailResponse.setCode("F00");
			detailResponse.setMessage("We are sorry for inconvenience, Please try again later .");
			detailResponse.setStatus("FAILED");
			detailResponse.setResponse(APIUtils.getFailedJSON().toString());
		}
	}

	@RequestMapping(value = "/GetReceipts", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ReceiptsResponse> getReceipts(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody ReceiptsRequest dto,
			@RequestHeader(value = "hash", required = false) String hash) {
		ReceiptsResponse result = new ReceiptsResponse();
		try {
			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue())
						|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					result = userApi.getReceipts(dto);
//					JSONArray receiptsArray = (JSONArray) result.getAdditionalInfo();
					// result.setAdditionalInfo(null);
					return new ResponseEntity<ReceiptsResponse>(result, HttpStatus.OK);
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Unknown device");
					result.setStatus("FAILED");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else if (role.equalsIgnoreCase(Role.MERCHANT.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue())
						|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					result = userApi.getMerchantReceipts(dto);
					return new ResponseEntity<ReceiptsResponse>(result, HttpStatus.OK);
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Unknown device");
					result.setStatus("FAILED");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unauthorised access");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<ReceiptsResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/GetFilteredReceipts", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ReceiptsResponse> getFilteredReceipts(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody ReceiptsRequest dto,
			@RequestHeader(value = "hash", required = false) String hash) {
		ReceiptsResponse result = new ReceiptsResponse();
		try {
			if (role.equalsIgnoreCase(Role.MERCHANT.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue())
						|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					result = userApi.getMerchantReceiptsFiltered(dto);
					return new ResponseEntity<ReceiptsResponse>(result, HttpStatus.OK);
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Unknown device");
					result.setStatus("FAILED");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unauthorised access");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<ReceiptsResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/GetDebitReceipts", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ReceiptsResponse> getDebitReceipts(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody ReceiptsRequest dto,
			@RequestHeader(value = "hash", required = false) String hash) {
		ReceiptsResponse result = new ReceiptsResponse();
		try {
			if (role.equalsIgnoreCase(Role.MERCHANT.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue())
						|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					result = userApi.getDebitMerchantReceipts(dto);
					return new ResponseEntity<ReceiptsResponse>(result, HttpStatus.OK);
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Unknown device");
					result.setStatus("FAILED");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unauthorised access");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<ReceiptsResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/GetReceiptsCommision", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ReceiptsResponse> getReceiptsCommsiion(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody ReceiptsRequest dto,
			@RequestHeader(value = "hash", required = false) String hash) {
		ReceiptsResponse result = new ReceiptsResponse();
		try {
			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue())
						|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					result = userApi.getReceipts(dto);
//					JSONArray receiptsArray = (JSONArray) result.getAdditionalInfo();
					// result.setAdditionalInfo(null);
					return new ResponseEntity<ReceiptsResponse>(result, HttpStatus.OK);
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Unknown device");
					result.setStatus("FAILED");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else if (role.equalsIgnoreCase(Role.MERCHANT.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue())
						|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					result = userApi.getMerchantReceiptscommision(dto);
					return new ResponseEntity<ReceiptsResponse>(result, HttpStatus.OK);
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Unknown device");
					result.setStatus("FAILED");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unauthorised access");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<ReceiptsResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/STransactions", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ReceiptsResponse> getTransactions(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody SessionDTO dto,
			@RequestHeader(value = "hash", required = false) String hash) {
		ReceiptsResponse result = new ReceiptsResponse();
		try {
			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue())
						|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					result = userApi.getSuccessfulTransactions(dto);
					return new ResponseEntity<ReceiptsResponse>(result, HttpStatus.OK);
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Unknown device");
					result.setStatus("FAILED");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unauthorised access");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<ReceiptsResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/UpdateFavourite", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ReceiptsResponse> updateFavourite(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody FavouriteRequest dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response) {
		ReceiptsResponse result = new ReceiptsResponse();
		try {
			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue())
						|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					result = userApi.updateFavouriteTransaction(dto);
					return new ResponseEntity<ReceiptsResponse>(result, HttpStatus.OK);
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Unknown device");
					result.setStatus("FAILED");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unauthorised access");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<ReceiptsResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/SetMPIN", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<MPINResponse> setMPIN(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody SetMPINRequest dto,
			@RequestHeader(value = "hash", required = false) String hash) {
		MPINResponse result = new MPINResponse();
		try {
			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue())
						|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					result = userApi.setMPIN(dto);
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Unknown device");
					result.setStatus("FAILED");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unauthorised access");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<MPINResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/CheckMPIN", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<MPINResponse> checkMPIN(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody SetMPINRequest dto,
			@RequestHeader(value = "hash", required = false) String hash) {
		MPINResponse result = new MPINResponse();
		try {
			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue())
						|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					result = userApi.setMPIN(dto);
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Unknown device");
					result.setStatus("FAILED");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unauthorised access");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<MPINResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/ChangeMPIN", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<MPINResponse> changeMPIN(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody ChangeMPINRequest dto,
			@RequestHeader(value = "hash", required = false) String hash) {
		MPINResponse result = new MPINResponse();
		try {
			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue())
						|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					result = userApi.changeMPIN(dto);
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Unknown device");
					result.setStatus("FAILED");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unauthorised access");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<MPINResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/DeleteMPIN", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<MPINResponse> deleteMPIN(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody DeleteMPINRequest dto,
			@RequestHeader(value = "hash", required = false) String hash) {
		MPINResponse result = new MPINResponse();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				result = userApi.deleteMPIN(dto);
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unknown device");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("Unauthorised access");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<MPINResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/ForgotMPIN", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<MPINResponse> deleteMPIN(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody ForgotMpinRequest dto,
			@RequestHeader(value = "hash", required = false) String hash) {
		MPINResponse result = new MPINResponse();
		try {
			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue())
						|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					MpinError error = mpinValidation.checkError(dto);
					if (error.isValid()) {
						result = userApi.forgotMPIN(dto);
					} else {
						result.setSuccess(false);
						result.setCode("F04");
						result.setDetails(error.toString());
					}
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Unknown device");
					result.setStatus("FAILED");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unauthorised access");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<MPINResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/VerifyMPIN", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<MPINResponse> verifyMPIN(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody VerifyMPINRequest dto,
			@RequestHeader(value = "hash", required = false) String hash) {
		MPINResponse result = new MPINResponse();
		try {
			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue())
						|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					result = userApi.verifyMPIN(dto);
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Unknown device");
					result.setStatus("FAILED");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unauthorised access");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<MPINResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/Invite/Email", method = RequestMethod.POST)
	public ResponseEntity<InviteFriendEmailResponse> inviteEmailFriend(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody InviteFriendEmailRequest dto,
			@RequestHeader(value = "hash", required = false) String hash, ModelMap mp, HttpSession session) {
		InviteFriendEmailResponse result = new InviteFriendEmailResponse();
		String sessionId = (String) dto.getSessionId();
		if (sessionId != null && sessionId.length() != 0) {
			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue())
						|| device.equalsIgnoreCase(Device.WINDOWS.getValue())) {
					result = userApi.inviteEmailFriend(dto);
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Unknown device");
					result.setStatus("FAILED");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unauthorised access");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		}
		return new ResponseEntity<InviteFriendEmailResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/Invite/Mobile", method = RequestMethod.POST)
	public ResponseEntity<InviteFriendMobileResponse> inviteMobileFriend(
			@PathVariable(value = "version") String version, @PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody InviteFriendMobileRequest dto, @RequestHeader(value = "hash", required = false) String hash,
			ModelMap mp, HttpSession session) {
		InviteFriendMobileResponse result = new InviteFriendMobileResponse();
		try {
			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue())
						|| device.equalsIgnoreCase(Device.WINDOWS.getValue())) {
					result = userApi.inviteMobileFriend(dto);
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Unknown device");
					result.setStatus("FAILED");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unauthorised access");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}

		return new ResponseEntity<InviteFriendMobileResponse>(result, HttpStatus.OK);
	}

	private boolean isValidImage(MultipartFile file) {
		long length = 2 * 1024 * 1024;
		File imageFile = null;
		try {
			imageFile = convert(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
		boolean isValid = false;
		if (file.getContentType().contains("image")) {
			isValid = true;
		}
		System.err.println("file size ::" + file.getSize());
		System.err.println("file size ::" + length);
		if (file.getSize() <= length) {
			try {
				Image image = ImageIO.read(imageFile);
				if (image == null) {
				} else {
					isValid = true;
				}
			} catch (IOException ex) {
			}
		}
		return isValid;
	}

	private File convert(MultipartFile file) throws IOException {
		File convFile = new File(file.getOriginalFilename());
		convFile.createNewFile();
		FileOutputStream fos = new FileOutputStream(convFile);
		fos.write(file.getBytes());
		fos.close();
		return convFile;
	}

	private String saveImage(String rootDirectory, MultipartFile image) {
		Thread thread = null;
		boolean isSaved = false;
		String filePath = "";
		String seperator = "|";
		String[] format = image.getContentType().split("/");
		String fileFormat = "";
		String[] formats = { "jpeg", "png", "jpg" };
		for (String fmt : formats) {
			if (format[1].equals(fmt)) {
				fileFormat = fmt;
			}
		}
		if (!CommonValidation.isNull(fileFormat)) {
			try {
				String fileName = String.valueOf(System.currentTimeMillis());
				File dirs = new File(rootDirectory + "/resources/profileImages/" + fileName + "." + fileFormat);
				dirs.mkdirs();
				image.transferTo(dirs);
				filePath = "/resources/profileImages/" + fileName + "." + fileFormat;
				isSaved = true;
				return isSaved + seperator + filePath;
			} catch (Exception ex) {
				return "Exception Occurred";
			}
		}
		return "Exception Occurred";
	}

	@RequestMapping(value = "/Redeem/Code", method = RequestMethod.POST)
	public ResponseEntity<RedeemCodeResponse> redeemCode(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody RedeemCodeRequest dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpSession session) {
		RedeemCodeResponse result = new RedeemCodeResponse();
		try {
			String sessionId = dto.getSessionId();
			if (sessionId != null && sessionId.length() != 0) {
				if (role.equalsIgnoreCase(Role.USER.getValue())) {
					if (device.equalsIgnoreCase(Device.ANDROID.getValue())
							|| device.equalsIgnoreCase(Device.WINDOWS.getValue())) {
						result = userApi.redeemPromoCode(dto);
					} else {
						result.setSuccess(false);
						result.setCode("F00");
						result.setMessage("Unknown device");
						result.setStatus("FAILED");
						result.setResponse(APIUtils.getFailedJSON().toString());
					}
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Unauthorised access");
					result.setStatus("FAILED");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<RedeemCodeResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/KycRequest", method = RequestMethod.POST)
	public ResponseEntity<KycResponse> kycRequest(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody KycVerificationDTO dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpSession session) {
		KycResponse result = new KycResponse();
		KycError error = new KycError();
		try {
			String sessionId = dto.getSessionId();
			if (sessionId != null && sessionId.length() != 0) {
				if (version.equalsIgnoreCase("v1")) {
					if (role.equalsIgnoreCase("User")) {
						if (device.equalsIgnoreCase("Android") || device.equalsIgnoreCase("IOS")) {
						error = kycValidation.checkKycError(dto);
						if (error.isValid()) {
							result = userApi.kycVerification(dto);
						} else {
							result.setSuccess(false);
							result.setCode("F04");
							result.setDetails(error);
						 }
						} else {
							result.setSuccess(false);
							result.setMessage("Unauthorized Device");
						}
					} else {
						result.setSuccess(false);
						result.setMessage("Unauthorized Role");
					}
				} else {
					result.setSuccess(false);
					result.setMessage("Invalid Version");
				}
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Unauthorised access");
					result.setStatus("FAILED");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<KycResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/KycRequest/OTPVerification", method = RequestMethod.POST)
	public ResponseEntity<KycResponse> otpVerification(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody KycVerificationDTO dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpSession session) {
		KycResponse result = new KycResponse();
		KycError error = new KycError();
		try {
			String sessionId = dto.getSessionId();
			if (sessionId != null && sessionId.length() != 0) {
				if (version.equalsIgnoreCase("v1")) {
					if (role.equalsIgnoreCase("User")) {
						if (device.equalsIgnoreCase("Android") || device.equalsIgnoreCase("IOS")) {
						error = kycValidation.checkKycOTPError(dto);
						if (error.isValid()) {
							result = userApi.kycOTPVerification(dto);
						} else {
							result.setSuccess(false);
							result.setCode("F04");
							result.setDetails(error);
						 }
						} else {
							result.setSuccess(false);
							result.setMessage("Unauthorized Device");
						}
					} else {
						result.setSuccess(false);
						result.setMessage("Unauthorized Role");
					}
				} else {
					result.setSuccess(false);
					result.setMessage("Invalid Version");
				}
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Unauthorised access");
					result.setStatus("FAILED");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<KycResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/GetAccountNumber", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<VisaResponse> processGetAccountNumber(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody GetAccountNumber req,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response) {
		VisaResponse resp = new VisaResponse();
		try {
			String sessionId = (String) req.getSessionId();
			if (sessionId != null && sessionId.length() != 0) {
				String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
				if (authority != null) {
					if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
						// Send to APP server for Encrption
						VisaRequest visaReq = new VisaRequest();
						visaReq.setSessionId(req.getSessionId());
						visaReq.setMerchantName(req.getFirstName());
						visaReq.setMerchantId(req.getLastName());
						visaReq.setMerchantAddress(req.getUsername());
						VisaResponse res = userApi.prepareGetAccountNumberRequest(visaReq);
						if (res.getCode().equals("S00")) {
							if (res.getMessage() != null) {
								// call to M2P for Account Number in encrypted format
								VisaResponse resp1 = visaApi.getAccountNumberFromM2PEnc(res.getMessage());
								if (resp1.getCode().equals("S00")) {
									if (resp1.getMessage() != null) {
										// calling APP server to decryped the response
										VisaRequest re = new VisaRequest();
										re.setRequest(resp1.getMessage());
										VisaResponse resp2 = userApi.prepareGetAccNoDecrption(re);
										if (resp2.getCode().equals("S00")) {
											resp.setCode("S00");
											resp.setMessage("Success");
											System.out.println("RESPONE :::::: " + resp2.getMessage());
											resp.setResponse(resp2.getMessage());
											return new ResponseEntity<VisaResponse>(resp, HttpStatus.OK);
										} else {
											resp.setCode("F03");
											resp.setMessage("Failure");
											return new ResponseEntity<VisaResponse>(resp, HttpStatus.OK);
										}
									} else {
										resp.setCode("F03");
										resp.setMessage("Failure");
										return new ResponseEntity<VisaResponse>(resp, HttpStatus.OK);
									}
								} else {
									resp.setCode("F03");
									resp.setMessage("Failure");
									return new ResponseEntity<VisaResponse>(resp, HttpStatus.OK);
								}
							} else {
								resp.setCode("F03");
								resp.setMessage("Failure");
								return new ResponseEntity<VisaResponse>(resp, HttpStatus.OK);
							}
						} else {
							resp.setCode("F03");
							resp.setMessage("Invaild Session");
							return new ResponseEntity<VisaResponse>(resp, HttpStatus.OK);
						}
					}
				}
			} else {
				resp.setCode("F00");
				resp.setMessage("Invalid Authority User Role");
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("We are sorry for inconvenience, Please try again later .");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}

		return new ResponseEntity<VisaResponse>(resp, HttpStatus.OK);
	}

	@RequestMapping(value = "/VisaRequest", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<VisaResponseDTO> processVisaMerchantPayment(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody String encRequest,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response) {
		VisaRequest dto = new VisaRequest();
		VisaResponseDTO visaResponseDTO = new VisaResponseDTO();
		VisaResponse res = new VisaResponse();
		try {
			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase("Android")
						|| device.equalsIgnoreCase("Windows")|| device.equalsIgnoreCase("Website")) {
					dto.setRequest(encRequest);
					dto.setDevice(Device.ANDROID);
				    }else if(device.equalsIgnoreCase("Ios")){
				    	dto.setRequest(encRequest);
				    	dto.setDevice(Device.IOS);
				    }
					boolean valid = ConvertUtil.checkVisaRequest(dto);
					if (valid) {
						VisaResponse resp = userApi.visaTransactionInitinated(dto);
						System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! Intinatede ");
						if (resp.isSuccess()) {
							System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! Intinatede in ");
							String m2p = visaApi.visaTransactionM2P(resp.getDetails().toString());
							if (m2p != null) {
								System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! M@P in App server" + m2p);
								String ecryptedResp = visaApi.visaFetchStatusM2P(resp.getTxnId());
								if (ecryptedResp != null) {
									System.out.println(
											"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! M@P in App encryptioin " + encRequest);
									VisaRequest vReq = new VisaRequest();
									vReq.setRequest(m2p);
									vReq.setSessionId(resp.getMessage());
									vReq.setData(resp.getTxnId()); // TransactionId
									vReq.setDetails(ecryptedResp);
									System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! M@P in before");
									res = visaApi.visaM2PStatus(vReq);
									System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! M@P in after is Success " + res.isSuccess());
									System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! M@P in after message "+ res.getMessage());
									return new ResponseEntity<VisaResponseDTO>(ConvertUtil.getFromResponse(res),
											HttpStatus.OK);
								}
								res.setCode("F09");
								res.setMessage("Failed");
								return new ResponseEntity<VisaResponseDTO>(ConvertUtil.getFromResponse(res),
										HttpStatus.OK);
							}
						} else {
							res.setCode(resp.getCode());
							res.setMessage(res.getMessage());
							res.setResponse(String.valueOf(APIUtils.getCustomJSON(resp.getCode(), resp.getMessage())));
							System.out.println("Response form this 3 :: " + res.getCode());
							return new ResponseEntity<VisaResponseDTO>(ConvertUtil.getFromResponse(res), HttpStatus.OK);
						}
					}
			   }
		} catch (Exception e) {
			e.printStackTrace();
			visaResponseDTO.setSuccess(false);
			visaResponseDTO.setCode("F00");
			visaResponseDTO.setMessage("We are sorry for inconvenience, Please try again later .");
			visaResponseDTO.setStatus("FAILED");
			visaResponseDTO.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<VisaResponseDTO>(ConvertUtil.getFromResponse(res), HttpStatus.OK);
	}

	@RequestMapping(value = "/UpdateReceipt", method = RequestMethod.POST)
	public ResponseEntity<UpdateReceiptResponse> updateReceipt(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody UpdateReceiptDTO dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpSession session) {
		UpdateReceiptResponse result = new UpdateReceiptResponse();
		try {
			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue())
						|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					result = userApi.userUpdateReceipts(dto);
				} else {
					result.setSuccess(false);
					result.setCode("F04");
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unknown device");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<UpdateReceiptResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/UpdateReceiptInAjax", method = RequestMethod.POST)
	public ResponseEntity<UpdateReceiptResponse> updateReceiptinAjax(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody UpdateReceiptDTO dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpSession session) {
		UpdateReceiptResponse result = new UpdateReceiptResponse();
		try {
			String sessionId = (String) session.getAttribute("sessionId");
			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue())
						|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					dto.setSessionId(sessionId);
					result = userApi.userUpdateReceiptsinAjax(dto);
				} else {
					result.setSuccess(false);
					result.setCode("F04");
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unknown device");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<UpdateReceiptResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/UpdateCreditAndDebit", method = RequestMethod.GET)
	public ResponseEntity<UpdateReceiptResponse> updateCreditAndDebit(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language,
			@RequestHeader(value = "hash", required = false) String hash, HttpSession session) {
		UpdateReceiptResponse result = new UpdateReceiptResponse();
		try {
			String sessionId = (String) session.getAttribute("sessionId");
			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue())
						|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					UpdateReceiptDTO dto = new UpdateReceiptDTO();
					dto.setSessionId(sessionId);
					result = userApi.userReceipts(dto);
				} else {
					result.setSuccess(false);
					result.setCode("F04");
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unknown device");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}

		return new ResponseEntity<UpdateReceiptResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/getNotifications", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<NotificationsResponse> getNotifications(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody SessionDTO dto,
			@RequestHeader(value = "hash", required = false) String hash) {
		NotificationsResponse result = new NotificationsResponse();
		try {
			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue())
						|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					try {
						result = userApi.getNotifications(dto);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				} else {
					result.setCode("F00");
					result.setMessage("Unknown device");
				}
			} else {
				result.setCode("F00");
				result.setMessage("Unauthorised access");
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<NotificationsResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/InitiateQwikrPay", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> InitializeQwikrPay(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody QwikrPayRequestDTO req,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		ResponseDTO resp = new ResponseDTO();
		try {
			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if ((device.equalsIgnoreCase(Device.ANDROID.getValue()))
						|| (device.equalsIgnoreCase(Device.WINDOWS.getValue()))
						|| (device.equalsIgnoreCase(Device.IOS.getValue()))
						|| (device.equalsIgnoreCase(Device.WEBSITE.getValue()))) {
					ResponseDTO respns = qwikrPayApi.initializeQwikrPay(req);
					if (respns != null) {
						resp.setCode(respns.getCode());
						resp.setMessage(respns.getMessage());
						resp.setTransactionId(respns.getTransactionId());
						return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
					} else {
						resp.setCode("F00");
						resp.setMessage("Unexpected Response...");
						resp.setStatus("Failure");
						resp.setValid(false);
						return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
					}

				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Unauthorised access");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
				return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
			} else {
				resp.setCode("F00");
				resp.setMessage("User not authorized...");
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("We are sorry for inconvenience, Please try again later .");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}

		return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
	}

	@RequestMapping(value = "/SuccessQwikrPay", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> SuccessQwikrPay(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody QwikrPayRequestDTO req,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response) {
		ResponseDTO resp = new ResponseDTO();
		System.out.println("inside success qwikrpay");
		try {
			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if ((device.equalsIgnoreCase(Device.ANDROID.getValue()))
						|| (device.equalsIgnoreCase(Device.WINDOWS.getValue()))
						|| (device.equalsIgnoreCase(Device.IOS.getValue()))
						|| (device.equalsIgnoreCase(Device.WEBSITE.getValue()))) {
					ResponseDTO respns = qwikrPayApi.successQwikrPay(req);
					if (respns != null) {
						resp.setCode(respns.getCode());
						resp.setMessage(respns.getMessage());
						resp.setTransactionId(respns.getTransactionId());
						return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
					} else {
						resp.setCode("F00");
						resp.setMessage("Unexpected Response...");
						resp.setStatus("Failure");
						resp.setValid(false);
						return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
					}

				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Unauthorised access");
					resp.setStatus("FAILED");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
				return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
			} else {
				resp.setCode("F00");
				resp.setMessage("User not authorized...");
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("We are sorry for inconvenience, Please try again later .");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}

		return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
	}

	@RequestMapping(value = "/GetReceiptsMerchantOld", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ReceiptsResponse> getReceiptsMerchant(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody ReceiptsRequest dto,
			@RequestHeader(value = "hash", required = false) String hash) {
		ReceiptsResponse result = new ReceiptsResponse();
		try {
			if (role.equalsIgnoreCase(Role.USER.getValue()) || role.equalsIgnoreCase(Role.MERCHANT.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue())
						|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					result = userApi.getReceipts(dto);
//					JSONArray receiptsArray = (JSONArray) result.getAdditionalInfo();
					// result.setAdditionalInfo(null);
					return new ResponseEntity<ReceiptsResponse>(result, HttpStatus.OK);
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Unknown device");
					result.setStatus("FAILED");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else if (role.equalsIgnoreCase(Role.MERCHANT.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue())
						|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					result = userApi.getMerchantReceipts(dto);
					return new ResponseEntity<ReceiptsResponse>(result, HttpStatus.OK);
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Unknown device");
					result.setStatus("FAILED");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unauthorised access");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<ReceiptsResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/EditName", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<EditProfileResponse> editName(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody EditProfileRequest dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpSession session) {
		EditProfileResponse result = new EditProfileResponse();
		try {
			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue())
						|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					String sessionId = (dto.getSessionId() == null) ? (String) session.getAttribute("sessionId")
							: dto.getSessionId();
					dto.setSessionId(sessionId);
					result = userApi.editName(dto);
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Unknown device");
					result.setStatus("FAILED");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unauthorised access");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}

		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<EditProfileResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/checkVersion", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<VersionResponseDTO> editName(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody VersionRequestDTO dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpSession session) {
		VersionResponseDTO result = new VersionResponseDTO();
		try {
			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue())
						|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					result = versionApi.checkVersion(dto);
				} else {
					result.setCode("F00");
					result.setMessage("Unknown device");
					result.setStatus("FAILED");
				}
			} else {
				result.setCode("F00");
				result.setMessage("Unauthorised access");
				result.setStatus("FAILED");
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<VersionResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/getMobileList", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getListMobile(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody FetchMobileList dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpSession session) {
		ResponseDTO result = new ResponseDTO();
		try {
			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue())
						|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					result = userApi.getMobileListForRefer(dto);
				} else {
					result.setCode("F00");
					result.setMessage("Unknown device");
					result.setStatus("FAILED");
				}
			} else {
				result.setCode("F00");
				result.setMessage("Unauthorised access");
				result.setStatus("FAILED");
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/GetNearByMerchants", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<NearBymerchantResponse> getNearByMerchants(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody NearByMerchantsRequest dto,
			@RequestHeader(value = "hash", required = false) String hash) {
		NearBymerchantResponse result = new NearBymerchantResponse();
		try {
			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue())
						|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					result = userApi.getNearByMerchants(dto, Role.USER);
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Unknown device");
					result.setStatus("FAILED");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else if (role.equalsIgnoreCase(Role.MERCHANT.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue())
						|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					result = userApi.getNearByMerchants(dto, Role.MERCHANT);
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Unknown device");
					result.setStatus("FAILED");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unauthorised access");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}

		return new ResponseEntity<NearBymerchantResponse>(result, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/findNearByMerchants", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<NearBymerchantResponse> findNearByMerchants(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody NearByMerchantsRequest dto,
			@RequestHeader(value = "hash", required = false) String hash) {
		NearBymerchantResponse result = new NearBymerchantResponse();
		try {
			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue())
						|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					result = userApi.findNearByMerchants(dto, Role.USER);
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Unknown device");
					result.setStatus("FAILED");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			} /*else if (role.equalsIgnoreCase(Role.MERCHANT.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue())
						|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					result = userApi.findNearByMerchants(dto, Role.MERCHANT);
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Unknown device");
					result.setStatus("FAILED");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			}*/ else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unauthorised access");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}

		return new ResponseEntity<NearBymerchantResponse>(result, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/findNearByAgents", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<NearByAgentResponse> findNearByAgents(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody NearByAgentsRequest dto,
			@RequestHeader(value = "hash", required = false) String hash) {
		
		NearByAgentResponse nearByAgents = new NearByAgentResponse();
		try {
			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue())
						|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					nearByAgents = userApi.findNearByAgents(dto, Role.USER);
					
				} else {
					nearByAgents.setSuccess(false);
					nearByAgents.setCode("F00");
					nearByAgents.setMessage("Unknown device");
					nearByAgents.setStatus("FAILED");
					nearByAgents.setResponse(APIUtils.getFailedJSON().toString());
				}
				
			} else {
				nearByAgents.setSuccess(false);
				nearByAgents.setCode("F00");
				nearByAgents.setMessage("Unauthorised access");
				nearByAgents.setStatus("FAILED");
				nearByAgents.setResponse(APIUtils.getFailedJSON().toString());
			}
			
		} catch (Exception e) {
			logger.error(e.getMessage());
			nearByAgents.setSuccess(false);
			nearByAgents.setCode("F00");
			nearByAgents.setMessage("We are sorry for inconvenience, Please try again later .");
			nearByAgents.setStatus("FAILED");
			nearByAgents.setResponse(APIUtils.getFailedJSON().toString());
		}
		
		return new ResponseEntity<NearByAgentResponse>(nearByAgents, HttpStatus.OK);
		
	}
	@RequestMapping(value = "/updateUserMaxLimit", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> limitForMerchant(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody UserMaxLimitDto dto,
			@RequestHeader(value = "hash", required = false) String hash) {
		ResponseDTO result = new ResponseDTO();
		try {
			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue())
						|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					result= mpinValidation.validateMaxLimitRequest(dto);
					if (result.isSuccess() && !CommonValidation.isNull(dto.getMobileToken()) ) {
						result = userApi.updateUserMaxLimit(dto);
					}else if (result.isSuccess()) {
						result = userApi.sendMaxLimitOtp(dto);
						result.setInfo(dto);
					}else {
						result.setMessage(result.getMessage());
						result.setCode(ResponseStatus.BAD_REQUEST.getValue());
					}
				} else {
					result.setMessage(ResponseStatus.BAD_REQUEST.getKey());
					result.setCode(ResponseStatus.BAD_REQUEST.getValue());
				}

			} else {
				result.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				result.setMessage(ResponseStatus.UNAUTHORIZED_USER.getKey());
			}

		} catch (Exception e) {
			result.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			result.setMessage(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
			e.printStackTrace();
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getUserMaxLimit", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getUserMaxLimit(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody UserMaxLimitDto dto,
			@RequestHeader(value = "hash", required = false) String hash) {
		ResponseDTO result = new ResponseDTO();
		try {
			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue())
						|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					if (!CommonValidation.isNull(dto.getSessionId())
							&& !CommonValidation.isNull(dto.getTransactionType())) {
						result = userApi.getUserMaxLimit(dto);
					} else {
						result.setMessage(ResponseStatus.BAD_REQUEST.getKey());
						result.setCode(ResponseStatus.BAD_REQUEST.getValue());
					}
				} else {
					result.setMessage(ResponseStatus.BAD_REQUEST.getKey());
					result.setCode(ResponseStatus.BAD_REQUEST.getValue());
				}

			} else {
				result.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				result.setMessage(ResponseStatus.UNAUTHORIZED_USER.getKey());
			}
		} catch (Exception e) {
			result.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			result.setMessage(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
			e.printStackTrace();
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/validatePassword", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> validateUserPassword(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody LoginDTO dto,
			@RequestHeader(value = "hash", required = false) String hash) {
		ResponseDTO result = new ResponseDTO();
		try {
			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue())
						|| device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					if (!CommonValidation.isNull(dto.getSessionId())
							&& !CommonValidation.isNull(dto.getPassword())) {
						result = userApi.validateUserPassword(dto);
					} else {
						result.setMessage(ResponseStatus.BAD_REQUEST.getKey());
						result.setCode(ResponseStatus.BAD_REQUEST.getValue());
					}
				} else {
					result.setMessage(ResponseStatus.BAD_REQUEST.getKey());
					result.setCode(ResponseStatus.BAD_REQUEST.getValue());
				}

			} else {
				result.setCode(ResponseStatus.UNAUTHORIZED_USER.getValue());
				result.setMessage(ResponseStatus.UNAUTHORIZED_USER.getKey());
			}
		} catch (Exception e) {
			result.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
			result.setMessage(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
			e.printStackTrace();
		}
		return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
	}
	
}
