package com.payqwikweb.controller.mobile.api;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.payqwikweb.app.api.ISendMoneyApi;
import com.payqwikweb.app.api.ITransactionApi;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.request.SendMoneyMobileRequest;
import com.payqwikweb.app.model.request.TransactionRequest;
import com.payqwikweb.app.model.response.SendMoneyMobileResponse;
import com.payqwikweb.model.app.request.SendMoneyDonateeRequest;
import com.payqwikweb.model.app.response.SendMoneyDonateeResponse;
import com.payqwikweb.util.APIUtils;
import com.thirdparty.model.ResponseDTO;

@Controller
@RequestMapping("/Api/{version}/{role}/{device}/{language}")
public class SendMoneyToDonateeAPI implements MessageSourceAware{
	
	private MessageSource messageSource;
	private final ITransactionApi transactionApi;
	private final ISendMoneyApi sendMoneyApi;
	
	public SendMoneyToDonateeAPI(ITransactionApi transactionApi, ISendMoneyApi sendMoneyApi) {
		super();
		this.transactionApi = transactionApi;
		this.sendMoneyApi = sendMoneyApi;
	}

	@RequestMapping(value ="/SendMoneyDonatee/Donate", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<SendMoneyDonateeResponse> sendMoneyToDonatee(@PathVariable(value = "version") String version, @PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SendMoneyDonateeRequest dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response) {
			SendMoneyDonateeResponse result = new SendMoneyDonateeResponse();
			try{
			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					TransactionRequest newRequest = new TransactionRequest();
					newRequest.setSessionId(dto.getSessionId());
					newRequest.setAmount(Double.parseDouble(dto.getAmount()));
					newRequest.setTransactionRefNo("D");
					ResponseDTO responseDTO1 = transactionApi.validateTransaction(newRequest);
					if (responseDTO1.isValid()) {
						result = sendMoneyApi.sendMoneyToDonatee(dto);
					} else {
						result.setCode("F00");
						result.setMessage(responseDTO1.getMessage());
					}
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Unknown device");
					result.setStatus("FAILED");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unauthorised access");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			 }
			} catch (Exception e) {
				e.printStackTrace();
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("We are sorry for inconvenience, Please try again later .");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
			return new ResponseEntity<SendMoneyDonateeResponse>(result, HttpStatus.OK);
	}

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

}
