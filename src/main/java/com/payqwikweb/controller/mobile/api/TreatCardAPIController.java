package com.payqwikweb.controller.mobile.api;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.payqwikweb.api.IAuthenticationApi;
import com.payqwikweb.app.api.IUserApi;
import com.payqwikweb.app.model.request.SessionDTO;
import com.payqwikweb.app.model.request.TreatCardDTO;
import com.payqwikweb.app.model.response.TreatCardPlansResponse;
import com.payqwikweb.app.model.response.TreatCardResponse;
import com.payqwikweb.util.TreatCardUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;

@Controller
@RequestMapping("/Api/{version}/{role}/{device}/{language}/TreatCard")
public class TreatCardAPIController  implements MessageSourceAware {
	
	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	private MessageSource messageSource;
	
	private static final String Authorization_value="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdXRoIjoibXNld2F1c2VyIiwiY2hlY2tBdXRoIjoibXNld2EiLCJleHAiOjIzNjk3NTU3ODgsImlhdCI6MTUwNTc1NTc4OH0.LNNa2ELZvH_MQ5miILJ8gqVoh215JFL7xwO_iwuAeog";

	private IUserApi userApi;
	private final IAuthenticationApi authenticationApi;

	public TreatCardAPIController(IUserApi userApi,IAuthenticationApi authenticationApi) {
		this.userApi=userApi;
		this.authenticationApi = authenticationApi;

	}
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	@RequestMapping(value = "/Register/Process", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<String> processVoucher(@RequestBody TreatCardDTO dto) {
		JSONObject payload = new JSONObject();
		try {
			String stringResponse = "";
			payload.put("phoneNumber", dto.getPhoneNumber());
			payload.put("email", dto.getEmailId());
			payload.put("firstName", dto.getFirst());
			payload.put("lastName", dto.getLast());
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(TreatCardUtil.REGISTER_URL);
			ClientResponse clientResponse = webResource.accept(javax.ws.rs.core.MediaType.APPLICATION_JSON).type(javax.ws.rs.core.MediaType.APPLICATION_JSON).header("Authorization",Authorization_value).post(ClientResponse.class, payload);
			stringResponse = clientResponse.getEntity(String.class);
			if (clientResponse.getStatus() == 200) {
				String response = stringResponse;
				return new ResponseEntity<String>(response, HttpStatus.OK);
			} 
			return new ResponseEntity<String>("", HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>("", HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/GetTreatCards", method = RequestMethod.POST)
	public ResponseEntity<TreatCardResponse> getAccessToken(@PathVariable(value = "version") String version,
														 @PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
														 @PathVariable(value = "language") String language, @RequestBody TreatCardDTO dto,
														 @RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
														 HttpServletResponse response, HttpSession session) throws Exception {

		TreatCardResponse resp = new TreatCardResponse();
		try{
		if(version.equalsIgnoreCase("v1")) {
			if(role.equalsIgnoreCase("User")) {
				if(device.equalsIgnoreCase("Android") || device.equalsIgnoreCase("IOS")) {
					if(language.equalsIgnoreCase("EN")) {
						SessionDTO sessionDTO = new SessionDTO();
						sessionDTO.setSessionId(dto.getSessionId());
						resp = userApi.getTreatCardDetails(sessionDTO);
					}else {
						resp.setSuccess(false);
						resp.setMessage("Only EN locale available");
					}
				} else {
					resp.setSuccess(false);
					resp.setMessage("Unauthorized Device");
				}
			} else {
				resp.setSuccess(false);
				resp.setMessage("Unauthorized Role");
			}
		}else {
			resp.setSuccess(false);
			resp.setMessage("Invalid Version");
		 }
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setMessage("We are sorry for inconvenience, Please try again later .");
		}
		return new ResponseEntity<>(resp, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/Update/Process", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<String> updateTreatCard(@RequestBody TreatCardDTO dto) {
		JSONObject payload = new JSONObject();
		try {
			String stringResponse = "";
			Client client = Client.create();
			client.addFilter(new LoggingFilter(System.out));
			WebResource webResource = client.resource(TreatCardUtil.REGISTER_URL+"/"+dto.getPhoneNumber()+"/"+dto.getMembershipCardValidityInDays());
			ClientResponse clientResponse = webResource.accept(javax.ws.rs.core.MediaType.APPLICATION_JSON).type(javax.ws.rs.core.MediaType.APPLICATION_JSON).header("Authorization",Authorization_value).post(ClientResponse.class, payload);
			stringResponse = clientResponse.getEntity(String.class);
			if (clientResponse.getStatus() == 200) {
				String response = stringResponse;
				return new ResponseEntity<String>(response, HttpStatus.OK);
			} 
			return new ResponseEntity<String>("", HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>("", HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/TreatCardPlans", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<TreatCardPlansResponse> getTreatCardPlans(@PathVariable(value = "version") String version,
			 @PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			 @PathVariable(value = "language") String language, @RequestBody SessionDTO dto,
			 @RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			 HttpServletResponse response, HttpSession session) throws Exception {
		TreatCardPlansResponse resp = new TreatCardPlansResponse();
		try{
		if(version.equalsIgnoreCase("v1")) {
			if(role.equalsIgnoreCase("User")) {
				if(device.equalsIgnoreCase("Android") || device.equalsIgnoreCase("IOS")) {
					if(language.equalsIgnoreCase("EN")) {
						SessionDTO sessionDTO = new SessionDTO();
						sessionDTO.setSessionId(dto.getSessionId());
						 resp = userApi.getListPlans(dto);
					}else {
						resp.setSuccess(false);
						resp.setMessage("Only EN locale available");
					}
				} else {
					resp.setSuccess(false);
					resp.setMessage("Unauthorized Device");
				}
			} else {
				resp.setSuccess(false);
				resp.setMessage("Unauthorized Role");
			}
		}else {
			resp.setSuccess(false);
			resp.setMessage("Invalid Version");
		 }
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setMessage("We are sorry for inconvenience, Please try again later .");
		}
		return new ResponseEntity<>(resp, HttpStatus.OK);
	}
	
	
	
	@RequestMapping(value = "/UpdateTreatCard", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<TreatCardPlansResponse> getUpdateTreatCard(@PathVariable(value = "version") String version,
			 @PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			 @PathVariable(value = "language") String language, @RequestBody TreatCardDTO dto,
			 @RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			 HttpServletResponse response, HttpSession session) throws Exception {
		TreatCardPlansResponse resp = new TreatCardPlansResponse();
		try{
		if(version.equalsIgnoreCase("v1")) {
			if(role.equalsIgnoreCase("User")) {
				if(device.equalsIgnoreCase("Android") || device.equalsIgnoreCase("IOS")) {
					if(language.equalsIgnoreCase("EN")) {
						 resp = userApi.getUpdateTreatCard(dto);
					}else {
						resp.setSuccess(false);
						resp.setMessage("Only EN locale available");
					}
				} else {
					resp.setSuccess(false);
					resp.setMessage("Unauthorized Device");
				}
			} else {
				resp.setSuccess(false);
				resp.setMessage("Unauthorized Role");
			}
		}else {
			resp.setSuccess(false);
			resp.setMessage("Invalid Version");
		 }
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setMessage("We are sorry for inconvenience, Please try again later .");
		}
		return new ResponseEntity<>(resp, HttpStatus.OK);
	}

}
