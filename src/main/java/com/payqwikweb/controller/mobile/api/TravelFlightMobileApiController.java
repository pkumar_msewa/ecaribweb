package com.payqwikweb.controller.mobile.api;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.instantpay.api.IValidationApi;
import com.payqwikweb.api.IAuthenticationApi;
import com.payqwikweb.api.IServiceCheckApi;
import com.payqwikweb.app.api.ILoadMoneyApi;
import com.payqwikweb.app.api.ITravelFlightApi;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.FlightPaymentGateWayDTO;
import com.payqwikweb.app.model.ResponseStatus;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.flight.dto.PaymentType;
import com.payqwikweb.app.model.flight.request.FligthBookReq;
import com.payqwikweb.app.model.flight.response.FlightClityList;
import com.payqwikweb.app.model.flight.response.FlightResponseDTO;
import com.payqwikweb.app.model.request.AirBookRQ;
import com.payqwikweb.app.model.request.CreateJsonRequestFlight;
import com.payqwikweb.app.model.request.Fare;
import com.payqwikweb.app.model.request.FlightVnetRequest;
import com.payqwikweb.app.model.request.MobileFlightBookRequest;
import com.payqwikweb.app.model.request.SessionDTO;
import com.payqwikweb.app.model.request.TravelFlightRequest;
import com.payqwikweb.app.model.response.EBSRedirectResponse;
import com.payqwikweb.app.model.response.FlightPriceCheckRequest;
import com.payqwikweb.app.model.response.LoadMoneyResponse;
import com.payqwikweb.app.model.response.VNetResponse;
import com.payqwikweb.app.model.response.VRedirectResponse;
import com.payqwikweb.model.app.request.FlightBookRequest;
import com.payqwikweb.model.app.request.LoadMoneyFlightRequest;
import com.payqwikweb.model.app.request.MobileRoundwayFlightBookRequest;
import com.payqwikweb.model.app.request.TravellerFlightDetails;
import com.payqwikweb.model.error.LoadMoneyError;
import com.payqwikweb.model.web.Status;
import com.payqwikweb.util.AES;
import com.payqwikweb.util.APIUtils;
import com.payqwikweb.util.Authorities;
import com.payqwikweb.util.ModelMapKey;
import com.payqwikweb.util.TravelBusUtil;
import com.payqwikweb.util.TravelFlightUtil;
import com.payqwikweb.validation.LoadMoneyValidation;
import com.thirdparty.model.ResponseDTO;


@Controller
@RequestMapping("/Api/{version}/{role}/{device}/{language}/Treval")
public class TravelFlightMobileApiController {

	private final ITravelFlightApi travelflightapi;
	private final IAuthenticationApi authenticationApi;
	private final IValidationApi validationApi;
	private final ILoadMoneyApi loadMoneyApi;
	private final LoadMoneyValidation loadMoneyValidation;
	private final IServiceCheckApi serviceCheckApi;

	public TravelFlightMobileApiController(ITravelFlightApi travelflightapi, IAuthenticationApi authenticationApi,
			IValidationApi validationApi,ILoadMoneyApi loadMoneyApi,LoadMoneyValidation loadMoneyValidation,IServiceCheckApi serviceCheckApi) {
		this.travelflightapi = travelflightapi;
		this.authenticationApi = authenticationApi;
		this.validationApi = validationApi;
		this.loadMoneyApi = loadMoneyApi;
		this.loadMoneyValidation = loadMoneyValidation;
		this.serviceCheckApi=serviceCheckApi;
	}

	@RequestMapping(value = "/Flight/Source", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<String> processFlightSource(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody TravelFlightRequest req) {
		String source = travelflightapi.getAirlineNames();
		return new ResponseEntity<String>(source, HttpStatus.OK);

	}


	@RequestMapping(value = "/Flight/OneWay", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<String> processFlightOneWay(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody TravelFlightRequest req) {

		System.err.println("Flight Search");
		String source = travelflightapi.getFlightSearchAPI(req);
		return new ResponseEntity<String>(source, HttpStatus.OK);

	}


	@RequestMapping(value = "/Flight/ShowPriceDetail", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<String> processFlightShowPriceDetail(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody FlightPriceCheckRequest req) {
		String airRePriceRQ = travelflightapi.AirRePriceRQ(req);
		return new ResponseEntity<String>(airRePriceRQ, HttpStatus.OK);

	}


	@RequestMapping(value = "/Flight/ConnectingShowPriceDetail", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<String> processConectingFlightShowPriceDetail(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody FlightPriceCheckRequest req) {

		String airRePriceRQ = travelflightapi.mobileAirRePriceRQConnecting(req);
		return new ResponseEntity<String>(airRePriceRQ, HttpStatus.OK);

	}


	@RequestMapping(method = RequestMethod.POST, value = "/Flight/CheckOut", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<String> getFlightCheckOutPostMethod(@RequestBody MobileFlightBookRequest req) {
		try {

			List<TravellerFlightDetails> travellerFlightDetails=new ArrayList<>();

			String flightBookingInitiate = travelflightapi.FlightBookingInitiate(req.getSessionId(), req.getFirstName(),
					req.getGrandtotal(), req.getTicketDetails(), "Wallet");

			JSONObject objflightBookingInitiate = new JSONObject(flightBookingInitiate);
			String codeflightBookingInitiate = objflightBookingInitiate.getString("code");
			if (codeflightBookingInitiate.equalsIgnoreCase("S00")) {
				String transactionRefNo = objflightBookingInitiate.getJSONObject("details")
						.getString("transactionRefNo");
				req.setTransactionId(transactionRefNo);

				String airRePriceRQ = travelflightapi.AirBookRQForMobile(req);
				JSONObject obj = new JSONObject(airRePriceRQ);
				String code = obj.getString("code");
				if (code.equalsIgnoreCase("S00")) {
					String bookingRefId = obj.getJSONObject("details").getString("bookingRefId");
					String transactionRefNomdex = obj.getJSONObject("details").getString("transactionRefNo");
					JSONArray objcc = obj.getJSONObject("details").getJSONObject("bookingDetail")
							.getJSONObject("pnrDetail").getJSONArray("tickets");
					String ticketNumber = "";
					String firstName = "";

					for (int i = 0; i < objcc.length(); i++) {
						ticketNumber += "~" + objcc.getJSONObject(i).getString("ticketNumber");
						firstName += "~" + objcc.getJSONObject(i).getString("firstName");
					}
					JSONArray objccpnrs = obj.getJSONObject("details").getJSONObject("bookingDetail")
							.getJSONObject("pnrDetail").getJSONArray("pnrs");
					String pnrNo = objccpnrs.getJSONObject(0).getString("pnr");

					String flightBookingSucess = travelflightapi.FlightBookingSucess(req.getSessionId(), ticketNumber,
							firstName, bookingRefId, transactionRefNomdex, req.getGrandtotal(), req.getTicketDetails(),
							"Booked", "Success", "Wallet", transactionRefNo, true,req.getEmailAddress(),req.getMobileNumber());

					JSONObject flightBookingSucessobj = new JSONObject(flightBookingSucess);
					String flightBookingSucesscode = flightBookingSucessobj.getString("code");
					if (flightBookingSucesscode.equalsIgnoreCase("S00")) {
						return new ResponseEntity<String>("" + flightBookingSucess, HttpStatus.OK);
					} else {
						travelflightapi.FlightBookingSucess(req.getSessionId(), ticketNumber,
								firstName, bookingRefId, transactionRefNomdex, req.getGrandtotal(), req.getTicketDetails(),
								"Booked", "Success", "Wallet", transactionRefNo, false,req.getEmailAddress(),req.getMobileNumber());
						return new ResponseEntity<String>(airRePriceRQ, HttpStatus.OK);
					}
				} else {
					travelflightapi.FlightBookingSucess(req.getSessionId(), "Flight Not booked", req.getFirstName(),
							"Flight Not booked", "Flight Not booked", req.getGrandtotal(), req.getTicketDetails(),
							"Processing", "Initiated", "Wallet", transactionRefNo, false,req.getEmailAddress(),req.getMobileNumber());	
					return new ResponseEntity<String>(airRePriceRQ, HttpStatus.OK);
				}
			} else {
				return new ResponseEntity<String>(flightBookingInitiate, HttpStatus.OK);
			}

		} catch (Exception e) {
			e.printStackTrace();

			return new ResponseEntity<String>("" + APIUtils.getFailedJSON().toString(), HttpStatus.OK);
		}

	}

	@RequestMapping(value = "/Flight/BookFlight", method = RequestMethod.POST)
	public ResponseEntity<String> bookFlight(@PathVariable(value = "version") String version, @PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language, HttpServletRequest request,
			@RequestBody FlightPaymentGateWayDTO req,  HttpServletResponse response, ModelMap modelMap, HttpSession session) {
		try{
			String airRePriceRQ =null;
			if (req.getFlightType().equalsIgnoreCase("Direct")) {

				airRePriceRQ= travelflightapi.AirBookRQForPaymentGateWayMobile(req);
			}
			else if (req.getFlightType().equalsIgnoreCase("DirectRounway")) {

				airRePriceRQ = travelflightapi.AirBookreturnRQForPaymentGateWayMobile(req);
			}
			else if (req.getFlightType().equalsIgnoreCase("DirectConnecting")) {
				req.setGrandtotal(req.getPaymentDetails().getBookingAmount()+"");
				airRePriceRQ = travelflightapi.AirBookRQForPaymentGateWayMobileConecting(req);
			}
			else if (req.getFlightType().equalsIgnoreCase("RoundwayConnecting")) {
				req.setGrandtotal(req.getPaymentDetails().getBookingAmount()+"");
				airRePriceRQ = travelflightapi.AirBookRQForPaymentGateWayMobileConecting(req);
			}

			JSONObject obj = new JSONObject(airRePriceRQ);
			String code = obj.getString("code");
			if (code.equalsIgnoreCase("S00")) {
				String bookingRefId = obj.getJSONObject("details").getString("bookingRefId");
				String transactionRefNomdex = obj.getJSONObject("details").getString("transactionRefNo");
				JSONArray objcc = obj.getJSONObject("details").getJSONObject("bookingDetail")
						.getJSONObject("pnrDetail").getJSONArray("tickets");
				String ticketNumber = "";
				String firstName = "";

				for (int i = 0; i < objcc.length(); i++) {
					ticketNumber += "~" + objcc.getJSONObject(i).getString("ticketNumber");
					firstName += "~" + objcc.getJSONObject(i).getString("firstName");
				}
				JSONArray objccpnrs = obj.getJSONObject("details").getJSONObject("bookingDetail")
						.getJSONObject("pnrDetail").getJSONArray("pnrs");
				String pnrNo = objccpnrs.getJSONObject(0).getString("pnr");

				/*String flightBookingSucess = travelflightapi.FlightBookingSucess(req.getSessionId(), ticketNumber,
						firstName, bookingRefId, transactionRefNomdex, req.getGrandtotal(), req.getTicketDetails(),
						"Booked", "Success", "Wallet", req.getTxnRefNo(), true,req.getEmailAddress(),req.getMobileNumber());*/


				String flightBookingSucess =travelflightapi.FlightPaymentGatewaySucess(req.getSessionId(), ticketNumber, firstName,
						bookingRefId, transactionRefNomdex, req.getGrandtotal(), req.getTicketDetails(), "Booked", 
						"Success", "Pay with ebs payment gateway", req.getTransactionId(), true,req.getEmailAddress(),req.getMobileNumber());


				JSONObject flightBookingSucessobj = new JSONObject(flightBookingSucess);
				String flightBookingSucesscode = flightBookingSucessobj.getString("code");
				if (flightBookingSucesscode.equalsIgnoreCase("S00")) {
					return new ResponseEntity<String>("" + flightBookingSucess, HttpStatus.OK);
				} else {
					travelflightapi.FlightBookingSucess(req.getSessionId(), ticketNumber,
							firstName, bookingRefId, transactionRefNomdex, req.getGrandtotal(), req.getTicketDetails(),
							"Booked", "Success", "Pay with ebs payment gateway", req.getTransactionId(), false,req.getEmailAddress(),req.getMobileNumber());
					return new ResponseEntity<String>(airRePriceRQ, HttpStatus.OK);
				}
			} else {
				travelflightapi.FlightPaymentGatewaySucess(req.getSessionId(), "Flight Not booked", req.getFirstName(),
						"Flight Not booked", "Flight Not booked", req.getGrandtotal(), req.getTicketDetails(),
						"Processing", "Initiated", "Pay with ebs payment gateway", req.getTransactionId(), false,req.getEmailAddress(),req.getMobileNumber());	
				return new ResponseEntity<String>(airRePriceRQ, HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();

			return new ResponseEntity<String>("" + APIUtils.getFailedJSON().toString(), HttpStatus.OK);
		}
	}


	@RequestMapping(method = RequestMethod.POST, value = "/Flight/RoundwayCheckOut", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<String> getFlightRoundwayCheckOutPostMethod(HttpSession session,
			@RequestBody MobileRoundwayFlightBookRequest req) {

		try {

			List<TravellerFlightDetails> returnTravellerFlightDetails=new ArrayList<>();
			//					getTravellersForRoundTrip(req);

			session.setAttribute("returnTravellerFlightDetails", returnTravellerFlightDetails);

			String flightBookingInitiate = travelflightapi.FlightBookingInitiate(req.getSessionId(), req.getFirstName(),
					req.getGrandtotal(), req.getTicketDetails(), "Wallet");

			JSONObject objflightBookingInitiate = new JSONObject(flightBookingInitiate);
			String codeflightBookingInitiate = objflightBookingInitiate.getString("code");
			if (codeflightBookingInitiate.equalsIgnoreCase("S00"))
			{
				String transactionRefNo = objflightBookingInitiate.getJSONObject("details")
						.getString("transactionRefNo");
				req.setTransactionId(transactionRefNo);

				String airRePriceRQ = travelflightapi.AirBookreturnRQForMobile(req);
				JSONObject obj = new JSONObject(airRePriceRQ);
				String code = obj.getString("code");
				if (code.equalsIgnoreCase("S00")) {

					String bookingRefId = obj.getJSONObject("details").getString("bookingRefId");
					String transactionRefNomdex = obj.getJSONObject("details").getString("transactionRefNo");
					JSONArray objcc = obj.getJSONObject("details").getJSONObject("bookingDetail")
							.getJSONObject("pnrDetail").getJSONArray("tickets");
					String ticketNumber = "";
					String firstName = "";

					for (int i = 0; i < objcc.length(); i++) {
						ticketNumber += "~" + objcc.getJSONObject(i).getString("ticketNumber");
						firstName += "~" + objcc.getJSONObject(i).getString("firstName");
					}


					JSONArray objccpnrs = obj.getJSONObject("details").getJSONObject("bookingDetail")
							.getJSONObject("pnrDetail").getJSONArray("pnrs");
					String pnrNo = objccpnrs.getJSONObject(0).getString("pnr");

					String flightBookingSucess = travelflightapi.FlightBookingSucess(req.getSessionId(), ticketNumber,
							firstName, bookingRefId, transactionRefNomdex, req.getGrandtotal(), req.getTicketDetails(),
							"Booked", "Success", "Wallet", transactionRefNo, true,req.getEmailAddress(),req.getMobileNumber());
					JSONObject flightBookingSucessobj = new JSONObject(flightBookingSucess);
					String flightBookingSucesscode = flightBookingSucessobj.getString("code");
					if (flightBookingSucesscode.equalsIgnoreCase("S00")) {
						travelflightapi.FlightBookingSucess(req.getSessionId(), ticketNumber,
								firstName, bookingRefId, transactionRefNomdex, req.getGrandtotal(), req.getTicketDetails(),
								"Booked", "Success", "Wallet", transactionRefNo, true,req.getEmailAddress(),req.getMobileNumber());
						return new ResponseEntity<String>("" + airRePriceRQ, HttpStatus.OK);
					} else {
						travelflightapi.FlightBookingSucess(req.getSessionId(), "Flight Not booked", req.getFirstName(),
								"Flight Not booked", "Flight Not booked", req.getGrandtotal(), req.getTicketDetails(),
								"Processing", "Initiated", "Wallet", transactionRefNo, false,req.getEmailAddress(),req.getMobileNumber());
						return new ResponseEntity<String>("" + flightBookingSucess, HttpStatus.OK);
					}
				} else {
					travelflightapi.FlightBookingSucess(req.getSessionId(), "Flight Not booked", req.getFirstName(),
							"Flight Not booked", "Flight Not booked", req.getGrandtotal(), req.getTicketDetails(),
							"Processing", "Initiated", "Wallet", transactionRefNo, false,req.getEmailAddress(),req.getMobileNumber());
					return new ResponseEntity<String>(airRePriceRQ, HttpStatus.OK);
				}
			} else {
				return new ResponseEntity<String>(flightBookingInitiate, HttpStatus.OK);
			}

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>("" + APIUtils.getFailedJSON().toString(), HttpStatus.OK);
		}

	}


	@RequestMapping(value = "/Flight/Process", method = RequestMethod.POST)
	public String processLoadMoney(@PathVariable(value = "version") String version, @PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language, @ModelAttribute LoadMoneyFlightRequest dto, HttpServletRequest request,
			HttpServletResponse response, ModelMap modelMap, HttpSession session) {
		String sessionId = dto.getSessionId();
		LoadMoneyError error = loadMoneyValidation.checkErrorFlight(dto);
		LoadMoneyResponse loadMoneyResponse = new LoadMoneyResponse();
		FlightBookRequest req = new FlightBookRequest();
		if (error.isValid()) {
			if (sessionId != null && sessionId.length() != 0) {
				String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
				if (authority != null) {
					if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
						dto.setSessionId(sessionId);
						if (dto.isUseVnet()) {
							FlightVnetRequest vNetRequest = new FlightVnetRequest();
							vNetRequest.setSessionId(sessionId);
							vNetRequest.setReturnURL(dto.getReturnUrl());
							vNetRequest.setAmount(dto.getAmount());
							vNetRequest.setTravellerDetails(dto.getTravellerDetails());
							VNetResponse vNetResponse = loadMoneyApi.initiateVnetBankingFlight(vNetRequest);
							if(vNetResponse.isSuccess()){
								modelMap.addAttribute("vnet",vNetResponse);
								return "User/LoadMoney/VNetPay";
							}
						} else {
							loadMoneyResponse = loadMoneyApi.SplitpaymentMoneyRequest(dto);
							if (loadMoneyResponse.isSuccess()) {
								modelMap.addAttribute("loadmoney", loadMoneyResponse);
								return "User/LoadMoney/Pay";
							} else {
							//	modelMap.addAttribute(ModelMapKey.MESSAGE, loadMoneyResponse.getDescription());
								return "User/LoadMoney";
							}
						}
					}
				}
			}
		}else {
			modelMap.addAttribute(ModelMapKey.ERROR,error.getAmount());
		}
		return "redirect:/";
	}

	@RequestMapping(value = "/Flight/VRedirect",produces = {MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> redirectVNetLoadMoney(VRedirectResponse dto, HttpServletRequest request, HttpServletResponse response, HttpSession session) {
		ResponseDTO responseDTO = loadMoneyApi.handleRedirectRequestFlight(dto);
		return new ResponseEntity<ResponseDTO>(responseDTO,HttpStatus.OK);
	}


	private List<TravellerFlightDetails> getTravellers(MobileFlightBookRequest dto)
	{
		String adultsFname=dto.getFirstName();
		String adultsLname=dto.getLastName();
		String adultsGen=dto.getGender();
		String adultFname[]=adultsFname.split("~");
		String adultLName[]=adultsLname.split("@");
		String adultGen[]=adultsGen.split("#");


		List<TravellerFlightDetails> list=new ArrayList<>();

		for (int i = 1; i < adultFname.length; i++) {
			String gender="";
			if (adultGen[i].trim().equalsIgnoreCase("Mr")) {
				gender="male"; 
			}else{
				gender="female";
			}
			TravellerFlightDetails  travellerDTO=new TravellerFlightDetails();
			travellerDTO.setfName(adultFname[i]);
			travellerDTO.setlName(adultLName[i]);
			travellerDTO.setGender(gender);
			travellerDTO.setType("Adult");
			list.add(travellerDTO);
		}


		String childsFname=dto.getFirstNamechild();
		String childsLname=dto.getLastNamechild();
		String childsGen=dto.getGender();
		String childFname[]=childsFname.split("~");
		String childLName[]=childsLname.split("@");
		String childGen[]=childsGen.split("#");


		for (int i = 1; i < childFname.length; i++) {
			String gender="";
			if (childGen[i].equalsIgnoreCase("Mr")) {
				gender="male"; 
			}else{
				gender="female";
			}
			TravellerFlightDetails  travellerDTO=new TravellerFlightDetails();
			travellerDTO.setfName(childFname[i]);
			travellerDTO.setlName(childLName[i]);
			travellerDTO.setGender(gender);
			travellerDTO.setType("Child");
			list.add(travellerDTO);
		}

		String infantsFname=dto.getFirstNameinfant();
		String infantsLname=dto.getLastNameinfant();
		String infantsGen=dto.getGender();
		String infantFname[]=infantsFname.split("~");
		String infantLName[]=infantsLname.split("@");
		String infantGen[]=infantsGen.split("#");


		for (int i = 1; i < infantFname.length; i++) {
			String gender="";
			if (infantGen[i].equalsIgnoreCase("Mr")) {
				gender="male"; 
			}else{
				gender="female";
			}
			TravellerFlightDetails  travellerDTO=new TravellerFlightDetails();
			travellerDTO.setfName(infantFname[i]);
			travellerDTO.setlName(infantLName[i]);
			travellerDTO.setGender(gender);
			travellerDTO.setType("Infant");
			list.add(travellerDTO);
		}


		return list;
	}


	private List<TravellerFlightDetails> getTravellersForRoundTrip(MobileRoundwayFlightBookRequest dto)
	{
		String adultsFname=dto.getFirstName();
		String adultsLname=dto.getLastName();
		String adultsGen=dto.getGender();
		String adultFname[]=adultsFname.split("~");
		String adultLName[]=adultsLname.split("@");
		String adultGen[]=adultsGen.split("#");


		List<TravellerFlightDetails> list=new ArrayList<>();

		for (int i = 1; i < adultFname.length; i++) {
			String gender="";
			if (adultGen[i].trim().equalsIgnoreCase("Mr")) {
				gender="male"; 
			}else{
				gender="female";
			}
			TravellerFlightDetails  travellerDTO=new TravellerFlightDetails();
			travellerDTO.setfName(adultFname[i]);
			travellerDTO.setlName(adultLName[i]);
			travellerDTO.setGender(gender);
			travellerDTO.setType("Adult");
			list.add(travellerDTO);
		}


		String childsFname=dto.getFirstNamechild();
		String childsLname=dto.getLastNamechild();
		String childsGen=dto.getGender();
		String childFname[]=childsFname.split("~");
		String childLName[]=childsLname.split("@");
		String childGen[]=childsGen.split("#");


		for (int i = 1; i < childFname.length; i++) {
			String gender="";
			if (childGen[i].equalsIgnoreCase("Mr")) {
				gender="male"; 
			}else{
				gender="female";
			}
			TravellerFlightDetails  travellerDTO=new TravellerFlightDetails();
			travellerDTO.setfName(childFname[i]);
			travellerDTO.setlName(childLName[i]);
			travellerDTO.setGender(gender);
			travellerDTO.setType("Child");
			list.add(travellerDTO);
		}

		String infantsFname=dto.getFirstNameinfant();
		String infantsLname=dto.getLastNameinfant();
		String infantsGen=dto.getGender();
		String infantFname[]=infantsFname.split("~");
		String infantLName[]=infantsLname.split("@");
		String infantGen[]=infantsGen.split("#");
		for (int i = 1; i < infantFname.length; i++) {
			String gender="";
			if (infantGen[i].equalsIgnoreCase("Mr")) {
				gender="male"; 
			}else{
				gender="female";
			}
			TravellerFlightDetails  travellerDTO=new TravellerFlightDetails();
			travellerDTO.setfName(infantFname[i]);
			travellerDTO.setlName(infantLName[i]);
			travellerDTO.setGender(gender);
			travellerDTO.setType("Infant");
			list.add(travellerDTO);
		}


		return list;
	}



	@RequestMapping(value = "/Flight/ConnectingPriceRecheck", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<String> processFlightConnectingPriceRecheck(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody TravelFlightRequest req) throws JSONException {
		JSONObject json = new JSONObject(req.getOrigin());
		System.out.println(json);
		System.out.println(req.getOrigin());
		return new ResponseEntity<String>(""+json, HttpStatus.OK);

	}


	@RequestMapping(method = RequestMethod.POST, value = "/Flight/Conecting/CheckOut", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<String> getFlightCheckOutForConnecting(HttpSession session,
			@RequestBody AirBookRQ req) {
		try {

			String firstName="";
			for (int i = 0; i < req.getTravellerDetails().size(); i++) {

				firstName+="~"+req.getTravellerDetails().get(i).getfName();

			}

			String flightBookingInitiate = travelflightapi.FlightBookingInitiate(req.getSessionId(), firstName,
					req.getPaymentDetails().getBookingAmount()+"", req.getTicketDetails(), "Wallet");

			JSONObject objflightBookingInitiate = new JSONObject(flightBookingInitiate);
			String codeflightBookingInitiate = objflightBookingInitiate.getString("code");
			if (codeflightBookingInitiate.equalsIgnoreCase("S00")) {
				String transactionRefNo = objflightBookingInitiate.getJSONObject("details")
						.getString("transactionRefNo");
				req.setTransactionId(transactionRefNo);

				String airRePriceRQ = travelflightapi.AirBookRQForMobileConecting(req);

				JSONObject obj = new JSONObject(airRePriceRQ);
				String code = obj.getString("code");
				if (code.equalsIgnoreCase("S00")) {

					String transactionRefNomdex = obj.getJSONObject("details").getString("transactionRefNo");
					JSONArray objcc = obj.getJSONObject("details").getJSONObject("bookingDetail")
							.getJSONObject("pnrDetail").getJSONArray("tickets");
					String ticketNumber = "";


					for (int i = 0; i < objcc.length(); i++) {
						ticketNumber+= "~" + objcc.getJSONObject(i).getString("ticketNumber");

					}

					String bookingRefId = obj.getJSONObject("details").getString("bookingRefId");

					JSONArray objccpnrs = obj.getJSONObject("details").getJSONObject("bookingDetail")
							.getJSONObject("pnrDetail").getJSONArray("pnrs");
					String pnrNo = objccpnrs.getJSONObject(0).getString("pnr");

					String flightBookingSucess = travelflightapi.FlightBookingSucess(req.getSessionId(), ticketNumber,
							firstName, bookingRefId, transactionRefNomdex, req.getPaymentDetails().getBookingAmount()+"",req.getTicketDetails(),
							"Booked", "Success", "Wallet", transactionRefNo, true,req.getEmailAddress(),req.getMobileNumber());
					JSONObject flightBookingSucessobj = new JSONObject(flightBookingSucess);
					String flightBookingSucesscode = flightBookingSucessobj.getString("code");
					if (flightBookingSucesscode.equalsIgnoreCase("S00")) {
						return new ResponseEntity<String>("" + airRePriceRQ, HttpStatus.OK);
					} else {
						travelflightapi.FlightBookingSucess(req.getSessionId(), "Flight Not booked", firstName,
								"Flight Not booked", "Flight Not booked", req.getPaymentDetails().getBookingAmount()+"", req.getTicketDetails(),
								"Processing", "Initiated", "Wallet", transactionRefNo, false,req.getEmailAddress(),req.getMobileNumber());
						return new ResponseEntity<String>("" + flightBookingSucess, HttpStatus.OK);
					}
				} else {
					travelflightapi.FlightBookingSucess(req.getSessionId(), "Flight Not booked", firstName,
							"Flight Not booked", "Flight Not booked", req.getPaymentDetails().getBookingAmount()+"", req.getTicketDetails(),
							"Processing", "Initiated", "Wallet", transactionRefNo, false,req.getEmailAddress(),req.getMobileNumber());
					return new ResponseEntity<String>(airRePriceRQ, HttpStatus.OK);
				}
			} else {
				return new ResponseEntity<String>(flightBookingInitiate, HttpStatus.OK);
			}

		} catch (Exception e) {
			e.printStackTrace();

			return new ResponseEntity<String>("" + APIUtils.getFailedJSON().toString(), HttpStatus.OK);
		}
	}



	@RequestMapping(method = RequestMethod.POST, value = "/Flight/Roundway/Connecting/CheckOut", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<String> getFlightRoundwayConnectingCheckOut(@RequestBody AirBookRQ req) {

		try {

			String firstName="";
			for (int i = 0; i < req.getTravellerDetails().size(); i++) {

				firstName+="~"+req.getTravellerDetails().get(i).getfName();

			}

			String flightBookingInitiate = travelflightapi.FlightBookingInitiate(req.getSessionId(), firstName,
					req.getPaymentDetails().getBookingAmount()+"", req.getTicketDetails(), "Wallet");

			JSONObject objflightBookingInitiate = new JSONObject(flightBookingInitiate);
			String codeflightBookingInitiate = objflightBookingInitiate.getString("code");
			if (codeflightBookingInitiate.equalsIgnoreCase("S00"))
			{
				String transactionRefNo = objflightBookingInitiate.getJSONObject("details")
						.getString("transactionRefNo");
				req.setTransactionId(transactionRefNo);

				String airRePriceRQ = travelflightapi.AirBookRQForMobileConecting(req);
				JSONObject obj = new JSONObject(airRePriceRQ);
				String code = obj.getString("code");
				if (code.equalsIgnoreCase("S00")) {

					String transactionRefNomdex = obj.getJSONObject("details").getString("transactionRefNo");
					JSONArray objcc = obj.getJSONObject("details").getJSONObject("bookingDetail")
							.getJSONObject("pnrDetail").getJSONArray("tickets");
					String ticketNumber = "";


					for (int i = 0; i < objcc.length(); i++) {
						ticketNumber+= "~" + objcc.getJSONObject(i).getString("ticketNumber");
					}

					String bookingRefId = obj.getJSONObject("details").getString("bookingRefId");

					JSONArray objccpnrs = obj.getJSONObject("details").getJSONObject("bookingDetail")
							.getJSONObject("pnrDetail").getJSONArray("pnrs");
					String pnrNo = objccpnrs.getJSONObject(0).getString("pnr");
					String flightBookingSucess = travelflightapi.FlightBookingSucess(req.getSessionId(), ticketNumber,
							firstName, bookingRefId, transactionRefNomdex, req.getPaymentDetails().getBookingAmount()+"", req.getTicketDetails(),
							"Booked", "Success", "Wallet", transactionRefNo, true,req.getEmailAddress(),req.getMobileNumber());
					JSONObject flightBookingSucessobj = new JSONObject(flightBookingSucess);
					String flightBookingSucesscode = flightBookingSucessobj.getString("code");
					if (flightBookingSucesscode.equalsIgnoreCase("S00")) {
						travelflightapi.FlightBookingSucess(req.getSessionId(), ticketNumber,
								firstName, bookingRefId, transactionRefNomdex, req.getPaymentDetails().getBookingAmount()+"", req.getTicketDetails(),
								"Booked", "Success", "Wallet", transactionRefNo, true,req.getEmailAddress(),req.getMobileNumber());
						return new ResponseEntity<String>("" + airRePriceRQ, HttpStatus.OK);
					} else {
						travelflightapi.FlightBookingSucess(req.getSessionId(), "Flight Not booked", firstName,
								"Flight Not booked", "Flight Not booked", req.getPaymentDetails().getBookingAmount()+"", req.getTicketDetails(),
								"Processing", "Initiated", "Wallet", transactionRefNo, false,req.getEmailAddress(),req.getMobileNumber());
						return new ResponseEntity<String>("" + flightBookingSucess, HttpStatus.OK);
					}
				} else {
					travelflightapi.FlightBookingSucess(req.getSessionId(), "Flight Not booked", firstName,
							"Flight Not booked", "Flight Not booked", req.getPaymentDetails().getBookingAmount()+"", req.getTicketDetails(),
							"Processing", "Initiated", "Wallet", transactionRefNo, false,req.getEmailAddress(),req.getMobileNumber());
					return new ResponseEntity<String>(airRePriceRQ, HttpStatus.OK);
				}
			} else {
				return new ResponseEntity<String>(flightBookingInitiate, HttpStatus.OK);
			}
			//
		} catch (Exception e) {
			e.printStackTrace();

			return new ResponseEntity<String>("" + APIUtils.getFailedJSON().toString(), HttpStatus.OK);
		}

	}


	@RequestMapping(value = "/Flight/BookFlight/Vnet", method = RequestMethod.POST)
	public ResponseEntity<String> bookFlightVnet(@PathVariable(value = "version") String version, @PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language, HttpServletRequest request,
			@RequestBody FlightPaymentGateWayDTO req,  HttpServletResponse response, ModelMap modelMap, HttpSession session) {
		try{
			//			 travelflightapi.getTxnRefNo(req.getSessionId());
			String airRePriceRQ =null;
			if (req.getFlightType().equalsIgnoreCase("Direct")) {
				airRePriceRQ= travelflightapi.AirBookRQForPaymentGateWayMobile(req);
			}
			else if (req.getFlightType().equalsIgnoreCase("DirectRounway")) {
				airRePriceRQ = travelflightapi.AirBookreturnRQForPaymentGateWayMobile(req);
			}
			else if (req.getFlightType().equalsIgnoreCase("DirectConnecting")) {
				req.setGrandtotal(req.getPaymentDetails().getBookingAmount()+"");
				airRePriceRQ = travelflightapi.AirBookRQForPaymentGateWayMobileConecting(req);
			}
			else if (req.getFlightType().equalsIgnoreCase("RoundwayConnecting")) {
				req.setGrandtotal(req.getPaymentDetails().getBookingAmount()+"");
				airRePriceRQ = travelflightapi.AirBookRQForPaymentGateWayMobileConecting(req);
			}

			JSONObject obj = new JSONObject(airRePriceRQ);
			String code = obj.getString("code");
			if (code.equalsIgnoreCase("S00")) {
				String bookingRefId = obj.getJSONObject("details").getString("bookingRefId");
				String transactionRefNomdex = obj.getJSONObject("details").getString("transactionRefNo");
				JSONArray objcc = obj.getJSONObject("details").getJSONObject("bookingDetail")
						.getJSONObject("pnrDetail").getJSONArray("tickets");
				String ticketNumber = "";
				String firstName = "";

				for (int i = 0; i < objcc.length(); i++) {
					ticketNumber += "~" + objcc.getJSONObject(i).getString("ticketNumber");
					firstName += "~" + objcc.getJSONObject(i).getString("firstName");
				}
				JSONArray objccpnrs = obj.getJSONObject("details").getJSONObject("bookingDetail")
						.getJSONObject("pnrDetail").getJSONArray("pnrs");
				String pnrNo = objccpnrs.getJSONObject(0).getString("pnr");

				/*String flightBookingSucess = travelflightapi.FlightBookingSucess(req.getSessionId(), ticketNumber,
						firstName, bookingRefId, transactionRefNomdex, req.getGrandtotal(), req.getTicketDetails(),
						"Booked", "Success", "Wallet", req.getTxnRefNo(), true,req.getEmailAddress(),req.getMobileNumber());*/


				String flightBookingSucess =travelflightapi.FlightPaymentGatewaySucess(req.getSessionId(), ticketNumber, firstName,
						bookingRefId, transactionRefNomdex, req.getGrandtotal(), req.getTicketDetails(), "Booked", 
						"Success", "Pay with Vnet payment gateway", req.getTransactionId(), true,req.getEmailAddress(),req.getMobileNumber());


				JSONObject flightBookingSucessobj = new JSONObject(flightBookingSucess);
				String flightBookingSucesscode = flightBookingSucessobj.getString("code");
				if (flightBookingSucesscode.equalsIgnoreCase("S00")) {
					return new ResponseEntity<String>("" + flightBookingSucess, HttpStatus.OK);
				} else {
					travelflightapi.FlightBookingSucess(req.getSessionId(), ticketNumber,
							firstName, bookingRefId, transactionRefNomdex, req.getGrandtotal(), req.getTicketDetails(),
							"Booked", "Success", "Pay with Vnet payment gateway", req.getTransactionId(), false,req.getEmailAddress(),req.getMobileNumber());
					return new ResponseEntity<String>(airRePriceRQ, HttpStatus.OK);
				}
			} else {
				travelflightapi.FlightPaymentGatewaySucess(req.getSessionId(), "Flight Not booked", req.getFirstName(),
						"Flight Not booked", "Flight Not booked", req.getGrandtotal(), req.getTicketDetails(),
						"Processing", "Initiated", "Pay with Vnet payment gateway", req.getTransactionId(), false,req.getEmailAddress(),req.getMobileNumber());	
				return new ResponseEntity<String>(airRePriceRQ, HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>("" + APIUtils.getFailedJSON().toString(), HttpStatus.OK);
		}
	}


	/* Code Done By Rohit End */


	/*##############################################################################################################*/

	/* Fresh Code Started By Subir */



	@RequestMapping(value ="/Flight/AirLineNames", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<FlightClityList> source(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language,@RequestHeader(value = "hash", required = false) String hash,
			@RequestBody TravelFlightRequest req) {

		FlightClityList resp=new FlightClityList();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				try {
					resp= travelflightapi.getAirLineNames(req.getSessionId());
				} catch (Exception e) {
					System.out.println(e);
					resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
					resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
					resp.setMessage("Service Unavailable");
				}
			}
			else {
				resp.setCode("F00");
				resp.setMessage("Unknown device");
				resp.setStatus("FAILED");
				//resp.setDetails(APIUtils.getFailedJSON().toString());
			}
		}else {
			resp.setCode("F00");
			resp.setMessage("Unauthorised access");
			resp.setStatus("FAILED");
			//resp.setDetails(APIUtils.getFailedJSON().toString());
		}

		return new ResponseEntity<FlightClityList>(resp, HttpStatus.OK);

	}



	@RequestMapping(value ="/Flight/AirLineList", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<FlightResponseDTO> flightList(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language,@RequestHeader(value = "hash", required = false) String hash,
			@RequestBody TravelFlightRequest req) {

		FlightResponseDTO resp=new FlightResponseDTO();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				String authority = authenticationApi.getAuthorityFromSession(req.getSessionId(), Role.USER);
				if (authority != null) {
					try {
						System.err.println("Flight Search");
						String service=serviceCheckApi.checkServiceActive(req.getSessionId());
						if (Status.Active.getValue().equalsIgnoreCase(service)) {
							resp= travelflightapi.searchFlight(req);
						}
						else {
							resp.setStatus(ResponseStatus.FAILURE.getKey());
							resp.setCode(ResponseStatus.FAILURE.getValue());
							resp.setMessage("Service is under maintenance");
						}
					} catch (Exception e) {
						System.out.println(e);
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
					}
				}else {
					resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
					resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
					resp.setMessage("Session expired");
				}
			}
			else {
				resp.setCode("F00");
				resp.setMessage("Unknown device");
				resp.setStatus("FAILED");
				resp.setDetails(APIUtils.getFailedJSON().toString());
			}
		}else {
			resp.setCode("F00");
			resp.setMessage("Unauthorised access");
			resp.setStatus("FAILED");
			resp.setDetails(APIUtils.getFailedJSON().toString());
		}

		return new ResponseEntity<FlightResponseDTO>(resp, HttpStatus.OK);
	}




	@RequestMapping(value = "/Flight/AirPriceRecheck", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<FlightResponseDTO> airPriceRecheck(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody FlightPriceCheckRequest req) {

		FlightResponseDTO resp=new FlightResponseDTO();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				String authority = authenticationApi.getAuthorityFromSession(req.getSessionId(), Role.USER);
				if (authority != null) {
					try {
						resp= travelflightapi.airRePriceRQ(req);
					} catch (Exception e) {
						e.printStackTrace();
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
					}
				}else {
					resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
					resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
					resp.setMessage("Session expired");
				}
			}
			else {
				resp.setCode("F00");
				resp.setMessage("Unknown device");
				resp.setStatus("FAILED");
				resp.setDetails(APIUtils.getFailedJSON().toString());
			}
		}else {
			resp.setCode("F00");
			resp.setMessage("Unauthorised access");
			resp.setStatus("FAILED");
			resp.setDetails(APIUtils.getFailedJSON().toString());
		}

		return new ResponseEntity<FlightResponseDTO>(resp, HttpStatus.OK);

	}


	@RequestMapping(method = RequestMethod.POST, value = "/Flight/BookTicket", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<String> flightCheckOut(HttpSession session, @RequestBody AirBookRQ req) {
		try {
			JSONObject ticketdetails=CreateJsonRequestFlight.createMobilejsonForTicket(req);
			FligthBookReq flightBookReq=new FligthBookReq();
			flightBookReq.setSessionId(req.getSessionId());
			flightBookReq.setPaymentAmount(req.getPaymentDetails().getBookingAmount()+"");
			flightBookReq.setTicketDetails(ticketdetails.toString());
			flightBookReq.setPaymentMethod(req.getPaymentMethod());
			flightBookReq.setEmail(req.getEmailAddress());
			flightBookReq.setMobile(req.getMobileNumber());
			flightBookReq.setSource(req.getFlightSearchDetails().get(0).getOrigin());
			flightBookReq.setDestination(req.getFlightSearchDetails().get(0).getDestination());
            flightBookReq.setBaseFare(ticketdetails.getString("basicFare"));
			String paymentInitcode = null;
			String flightBookingInitiate=null;

			List<TravellerFlightDetails> travellerFlightDetails=req.getTravellerDetails();
			List<TravellerFlightDetails> travellerFlightDetailsReturn=new ArrayList<>();
			if (req.getBookSegments().size()==2) {
				travellerFlightDetailsReturn.addAll(travellerFlightDetails);
			}
			travellerFlightDetailsReturn.addAll(travellerFlightDetails);


			if (req.getPaymentMethod().equalsIgnoreCase(PaymentType.Wallet.getKey())) {
				flightBookingInitiate = travelflightapi.flightBookingInitiate(flightBookReq,travellerFlightDetailsReturn);
				JSONObject objflightBookingInitiate = new JSONObject(flightBookingInitiate);
				String codeflightBookingInitiate=objflightBookingInitiate.getString("code");
				if (codeflightBookingInitiate.equalsIgnoreCase("S00")) {
					String transactionRefNo = objflightBookingInitiate.getJSONObject("details")
							.getString("transactionRefNo");
					req.setTransactionId(transactionRefNo);
				}
				paymentInitcode=codeflightBookingInitiate;
			}
			else if (req.getPaymentMethod().equalsIgnoreCase(PaymentType.EBS.getKey())) {
				if (req.getTransactionId()!=null ||!req.getTransactionId().isEmpty()) {
					paymentInitcode=ResponseStatus.SUCCESS.getValue();
				}

			}
			else if (req.getPaymentMethod().equalsIgnoreCase(PaymentType.VNet.getKey())) {

				if (req.getTransactionId()!=null ||!req.getTransactionId().isEmpty()) {
					paymentInitcode=ResponseStatus.SUCCESS.getValue();
				}
			}

			if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(paymentInitcode)) {
				req.setTravellerDetails(travellerFlightDetails);
				String airRePriceRQ = travelflightapi.AirBookRQForMobileConecting(req);

				JSONObject obj = new JSONObject(airRePriceRQ);
				String code = obj.getString("code");
				if (code.equalsIgnoreCase("S00")) {
					String transactionRefNomdex = obj.getJSONObject("details").getString("transactionRefNo");
					JSONArray tickets = obj.getJSONObject("details").getJSONObject("bookingDetail")
							.getJSONObject("pnrDetail").getJSONArray("tickets");
					String ticketNumber = "";
					for (int i = 0; i < tickets.length(); i++) {
						ticketNumber= tickets.getJSONObject(i).getString("ticketNumber");
						travellerFlightDetailsReturn.get(i).setTicketNo(ticketNumber);
					}
					String bookingRefId = obj.getJSONObject("details").getString("bookingRefId");
					JSONArray objccpnrs = obj.getJSONObject("details").getJSONObject("bookingDetail")
							.getJSONObject("pnrDetail").getJSONArray("pnrs");
					String pnrNo = objccpnrs.getJSONObject(0).getString("pnr");
					flightBookReq.setBookingRefId(bookingRefId);
					flightBookReq.setMdexTxnRefNo(transactionRefNomdex);
					flightBookReq.setStatus(Status.Booked.getValue());
					flightBookReq.setTxnRefno(req.getTransactionId());
					flightBookReq.setSuccess(true);
					flightBookReq.setFlightStatus(Status.Booked.getValue());
					flightBookReq.setPnrNo(pnrNo);
					String flightBookingSucess=null;
					if (req.getPaymentMethod().equalsIgnoreCase(PaymentType.Wallet.getKey())) {
						flightBookingSucess = travelflightapi.flightBookingSucess(flightBookReq,travellerFlightDetailsReturn);
					}
					else {
						flightBookingSucess = travelflightapi.flightPaymentGatewaySucess(flightBookReq,req.getTravellerDetails());
					}
					JSONObject flightBookingSucessobj = new JSONObject(flightBookingSucess);
					String flightBookingSucesscode = flightBookingSucessobj.getString("code");
					if (flightBookingSucesscode.equalsIgnoreCase("S00")) {
						return new ResponseEntity<String>("" + flightBookingSucess, HttpStatus.OK);
					} else {
						/*fligthBookReq.setSuccess(false);
						travelflightapi.flightBookingSucess(fligthBookReq,req.getTravellerDetails());*/
						return new ResponseEntity<String>("" + flightBookingSucess, HttpStatus.OK);
					}
				} else {
					flightBookReq.setTxnRefno(req.getTransactionId());
					flightBookReq.setSuccess(false);
					if (req.getPaymentMethod().equalsIgnoreCase(PaymentType.Wallet.getKey())) {
						travelflightapi.flightBookingSucess(flightBookReq,req.getTravellerDetails());
					}
					else {
						travelflightapi.flightPaymentGatewaySucess(flightBookReq,req.getTravellerDetails());
					}
					return new ResponseEntity<String>(airRePriceRQ, HttpStatus.OK);
				}
			} else {
				return new ResponseEntity<String>(flightBookingInitiate, HttpStatus.OK);
			}

		} catch (Exception e) {
			e.printStackTrace();

			return new ResponseEntity<String>("" + APIUtils.getFailedJSON().toString(), HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/Flight/InitiateLoadMoney", method = RequestMethod.POST,produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<LoadMoneyResponse> initiateLoadMoney(@PathVariable(value = "version") String version, @PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language, @ModelAttribute LoadMoneyFlightRequest dto, HttpServletRequest request,
			HttpServletResponse response, ModelMap modelMap, HttpSession session) {
		LoadMoneyResponse resp = new LoadMoneyResponse();
		if(version.equalsIgnoreCase("v1")) {
			if(role.equalsIgnoreCase("User")) {
				if(device.equalsIgnoreCase("Android") || device.equalsIgnoreCase("IOS")) {
					if(language.equalsIgnoreCase("EN")) {
						resp.setSuccess(false);
						System.err.println("processing for app side");
						resp = loadMoneyApi.SplitpaymentMoneyRequest(dto);
					}else {
						resp.setSuccess(false);
						resp.setMessage("Only EN locale available");
					}
				} else {
					resp.setSuccess(false);
					resp.setMessage("Unauthorized Device");
				}
			} else {
				resp.setSuccess(false);
				resp.setMessage("Unauthorized Role");
			}
		}else {
			resp.setSuccess(false);
			resp.setMessage("Invalid Version");
		}
		return new ResponseEntity<LoadMoneyResponse>(resp,HttpStatus.OK);
	}


	@RequestMapping(value = "/Flight/RedirectSDK", method = RequestMethod.POST,produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<EBSRedirectResponse> redirectLoadMoneySDK(@PathVariable(value = "version") String version, @PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language, @ModelAttribute EBSRedirectResponse redirectResponse,Model model){
		EBSRedirectResponse newResponse = new EBSRedirectResponse();
		ResponseDTO result = null;
		if(version.equalsIgnoreCase("v1")) {
			if(role.equalsIgnoreCase("User")) {
				if(device.equalsIgnoreCase("Android") || device.equalsIgnoreCase("IOS")) {
					if(language.equalsIgnoreCase("EN")) {
						result = loadMoneyApi.verifyEBSTransaction(redirectResponse);
						if(result != null){
							String code = result.getCode();
							if(code.equalsIgnoreCase("S00")){
								redirectResponse.setSuccess(true);
								redirectResponse.setResponseCode("0");
							}else {
								redirectResponse.setSuccess(false);
								redirectResponse.setResponseCode("1");
							}
							newResponse = loadMoneyApi.processRedirectSDKSplitpaymentMoney(redirectResponse);
						}
					}else {
						newResponse.setSuccess(false);
						newResponse.setError("Only EN locale available");
					}
				} else {
					newResponse.setSuccess(false);
					newResponse.setError("Unauthorized Device");
				}
			} else {
				newResponse.setSuccess(false);
				newResponse.setError("Unauthorized Role");
			}
		}else {
			newResponse.setSuccess(false);
			newResponse.setError("Invalid Version");
		}
		return new ResponseEntity<EBSRedirectResponse>(newResponse,HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/Flight/MyTickets", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<FlightResponseDTO> getAllTicketsByUser(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody SessionDTO dto,
			@RequestHeader(value = "hash", required = false) String hash,HttpSession session) {

		FlightResponseDTO resp=new FlightResponseDTO();

		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				String authority = authenticationApi.getAuthorityFromSession(dto.getSessionId(), Role.USER);
				if (authority != null) {
					try {

						resp=travelflightapi.getAllBookTicketsForMobile(dto.getSessionId());
						session.setAttribute("flightMsg", "");
						session.setAttribute("flighterrMsg", "");
						/*System.err.println("Working");*/
					} catch (Exception e) {
						System.out.println(e);
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
					}
				}else {
					resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
					resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
					resp.setMessage("Session expired");
				}
			}
			else {
				resp.setCode("F00");
				resp.setMessage("Unknown device");
				resp.setStatus("FAILED");
				resp.setDetails(APIUtils.getFailedJSON().toString());
			}
		}else {
			resp.setCode("F00");
			resp.setMessage("Unauthorised access");
			resp.setStatus("FAILED");
			resp.setDetails(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<FlightResponseDTO>(resp, HttpStatus.OK);
	}



	@RequestMapping(method = RequestMethod.POST, value = "/Flight/SaveAirLineInDbByCron", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<FlightResponseDTO> getAllCityListCron(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody SessionDTO dto,
			@RequestHeader(value = "hash", required = false) String hash) {

		FlightResponseDTO resp=new FlightResponseDTO();

		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {

				try {
					resp=travelflightapi.saveAirLineNamesInDb();
					System.err.println("Working");
				} catch (Exception e) {
					System.out.println(e);
					resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
					resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
					resp.setMessage("Service Unavailable");
				}

			}
			else {
				resp.setCode("F00");
				resp.setMessage("Unknown device");
				resp.setStatus("FAILED");
				resp.setDetails(APIUtils.getFailedJSON().toString());
			}
		}else {
			resp.setCode("F00");
			resp.setMessage("Unauthorised access");
			resp.setStatus("FAILED");
			resp.setDetails(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<FlightResponseDTO>(resp, HttpStatus.OK);
	}


	@RequestMapping(method = RequestMethod.POST, value = "/Flight/ManualyCronCall", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<FlightResponseDTO> cronCheck(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody SessionDTO dto,
			@RequestHeader(value = "hash", required = false) String hash) {

		FlightResponseDTO resp=new FlightResponseDTO();

		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				String authority = authenticationApi.getAuthorityFromSession(dto.getSessionId(), Role.USER);
				if (authority != null) {
					try {
						resp=travelflightapi.cronCheck(dto.getSessionId());
						System.err.println("Working");
					} catch (Exception e) {
						System.out.println(e);
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
					}
				}else {
					resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
					resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
					resp.setMessage("Session expired");
				}
			}
			else {
				resp.setCode("F00");
				resp.setMessage("Unknown device");
				resp.setStatus("FAILED");
				resp.setDetails(APIUtils.getFailedJSON().toString());
			}
		}else {
			resp.setCode("F00");
			resp.setMessage("Unauthorised access");
			resp.setStatus("FAILED");
			resp.setDetails(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<FlightResponseDTO>(resp, HttpStatus.OK);
	}


	
	
	
	
	

	/***************************************** Encryption Request controller *************************************************/


	@RequestMapping(value ="/EncReq/Flight/AirLineNames", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<FlightClityList> sourceEncReq(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language,@RequestHeader(value = "hash", required = false) String hash,
			@RequestBody TravelFlightRequest req) throws Exception {

		FlightClityList resp=new FlightClityList();

		if (req.getData()!=null && !req.getData().isEmpty() && !req.getData().equalsIgnoreCase("null")) {

			String decData=AES.decrypt(req.getData());
			String sessionId=TravelFlightUtil.getSessionIdDec(decData);
			
			req.setSessionId(sessionId);

			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					try {
						resp= travelflightapi.getAirLineNames(req.getSessionId());
					} catch (Exception e) {

						e.printStackTrace();
						resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
						resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
						resp.setMessage("Service Unavailable");
					}
				}
				else {
					resp.setCode("F00");
					resp.setMessage("Unknown device");
					resp.setStatus("FAILED");
					//resp.setDetails(APIUtils.getFailedJSON().toString());
				}
			}else {
				resp.setCode("F00");
				resp.setMessage("Unauthorised access");
				resp.setStatus("FAILED");
				//resp.setDetails(APIUtils.getFailedJSON().toString());
			}
		}
		else {
			resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
			resp.setStatus(ResponseStatus.BAD_REQUEST.getKey());
			resp.setMessage("Please pass encrypt data");
		}
		return new ResponseEntity<FlightClityList>(resp, HttpStatus.OK);

	}


	@RequestMapping(value ="/EncReq/Flight/AirLineList", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<FlightResponseDTO> flightListEncReq(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language,@RequestHeader(value = "hash", required = false) String hash,
			@RequestBody TravelFlightRequest reqDto) throws Exception {

		FlightResponseDTO resp=new FlightResponseDTO();

		if (reqDto.getData()!=null && !reqDto.getData().isEmpty() && !reqDto.getData().equalsIgnoreCase("null")) {
			String decData=AES.decrypt(reqDto.getData());
			TravelFlightRequest req=TravelFlightUtil.searchFlightDec(decData);	

			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					String authority = authenticationApi.getAuthorityFromSession(req.getSessionId(), Role.USER);
					if (authority != null) {
						try {

							System.err.println("Flight Search");
							String service=serviceCheckApi.checkServiceActive(req.getSessionId());
							if (Status.Active.getValue().equalsIgnoreCase(service)) {
								resp= travelflightapi.searchFlight(req);
							}
							else {
								resp.setStatus(ResponseStatus.FAILURE.getKey());
								resp.setCode(ResponseStatus.FAILURE.getValue());
								resp.setMessage("Service is under maintenance");
							}

						} catch (Exception e) {
							System.out.println(e);
							resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
							resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
							resp.setMessage("Service Unavailable");
						}
					}else {
						resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
						resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
						resp.setMessage("Session expired");
					}
				}
				else {
					resp.setCode("F00");
					resp.setMessage("Unknown device");
					resp.setStatus("FAILED");
					resp.setDetails(APIUtils.getFailedJSON().toString());
				}
			}else {
				resp.setCode("F00");
				resp.setMessage("Unauthorised access");
				resp.setStatus("FAILED");
				resp.setDetails(APIUtils.getFailedJSON().toString());
			}
		}
		else {
			resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
			resp.setStatus(ResponseStatus.BAD_REQUEST.getKey());
			resp.setMessage("Please pass encrypt data");
		}

		return new ResponseEntity<FlightResponseDTO>(resp, HttpStatus.OK);
	}


	@RequestMapping(value = "/EncReq/Flight/AirPriceRecheck", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<FlightResponseDTO> airPriceRecheckEncReq(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody FlightPriceCheckRequest reqDto) throws Exception {

		FlightResponseDTO resp=new FlightResponseDTO();
		if (reqDto.getData()!=null && !reqDto.getData().isEmpty() && !reqDto.getData().equalsIgnoreCase("null")) {
			String decData=AES.decrypt(reqDto.getData());
			FlightPriceCheckRequest req=TravelFlightUtil.airRePriceReqDec(decData);	

			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					String authority = authenticationApi.getAuthorityFromSession(req.getSessionId(), Role.USER);
					if (authority != null) {
						try {
							resp= travelflightapi.airRePriceRQ(req);
						} catch (Exception e) {
							System.out.println(e);
							resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
							resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
							resp.setMessage("Service Unavailable");
						}
					}else {
						resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
						resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
						resp.setMessage("Session expired");
					}
				}
				else {
					resp.setCode("F00");
					resp.setMessage("Unknown device");
					resp.setStatus("FAILED");
					resp.setDetails(APIUtils.getFailedJSON().toString());
				}
			}else {
				resp.setCode("F00");
				resp.setMessage("Unauthorised access");
				resp.setStatus("FAILED");
				resp.setDetails(APIUtils.getFailedJSON().toString());
			}
		}
		else {
			resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
			resp.setStatus(ResponseStatus.BAD_REQUEST.getKey());
			resp.setMessage("Please pass encrypt data");
		}
		return new ResponseEntity<FlightResponseDTO>(resp, HttpStatus.OK);

	}


	@RequestMapping(method = RequestMethod.POST, value = "/EncReq/Flight/BookTicket", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<String> flightCheckOutEncReq(HttpSession session, @RequestBody AirBookRQ reqDto) throws Exception {

		if (reqDto.getData()!=null && !reqDto.getData().isEmpty() && !reqDto.getData().equalsIgnoreCase("null")) {
			String decData=AES.decrypt(reqDto.getData());
			AirBookRQ req=TravelFlightUtil.bookTicketDec(decData);	
			try {
				JSONObject ticketdetails=CreateJsonRequestFlight.createMobilejsonForTicket(req);
				FligthBookReq fligthBookReq=new FligthBookReq();

				fligthBookReq.setSessionId(req.getSessionId());
				fligthBookReq.setPaymentAmount(req.getPaymentDetails().getBookingAmount()+"");
				fligthBookReq.setTicketDetails(ticketdetails.toString());
				fligthBookReq.setPaymentMethod(req.getPaymentMethod());
				fligthBookReq.setEmail(req.getEmailAddress());
				fligthBookReq.setMobile(req.getMobileNumber());
				fligthBookReq.setSource(req.getFlightSearchDetails().get(0).getOrigin());
				fligthBookReq.setDestination(req.getFlightSearchDetails().get(0).getDestination());

				String paymentInitcode = null;
				String flightBookingInitiate=null;

				List<TravellerFlightDetails> travellerFlightDetails=req.getTravellerDetails();
				List<TravellerFlightDetails> travellerFlightDetailsReturn=new ArrayList<>();
				if (req.getBookSegments().size()==2) {
					travellerFlightDetailsReturn.addAll(travellerFlightDetails);
				}
				travellerFlightDetailsReturn.addAll(travellerFlightDetails);

				double bookingAmount=req.getPaymentDetails().getBookingAmount()-200;

				req.getPaymentDetails().setBookingAmount(bookingAmount);


				if (req.getPaymentMethod().equalsIgnoreCase(PaymentType.Wallet.getKey())) {
					flightBookingInitiate = travelflightapi.flightBookingInitiate(fligthBookReq,travellerFlightDetailsReturn);
					JSONObject objflightBookingInitiate = new JSONObject(flightBookingInitiate);
					String codeflightBookingInitiate=objflightBookingInitiate.getString("code");
					if (codeflightBookingInitiate.equalsIgnoreCase("S00")) {
						String transactionRefNo = objflightBookingInitiate.getJSONObject("details")
								.getString("transactionRefNo");
						req.setTransactionId(transactionRefNo);
					}
					paymentInitcode=codeflightBookingInitiate;
				}
				else if (req.getPaymentMethod().equalsIgnoreCase(PaymentType.EBS.getKey())) {
					if (req.getTransactionId()!=null ||!req.getTransactionId().isEmpty()) {
						paymentInitcode=ResponseStatus.SUCCESS.getValue();
					}
				}
				else if (req.getPaymentMethod().equalsIgnoreCase(PaymentType.VNet.getKey())) {
					if (req.getTransactionId()!=null ||!req.getTransactionId().isEmpty()) {
						paymentInitcode=ResponseStatus.SUCCESS.getValue();
					}
				}

				if (ResponseStatus.SUCCESS.getValue().equalsIgnoreCase(paymentInitcode)) {
					req.setTravellerDetails(travellerFlightDetails);
					String airRePriceRQ = travelflightapi.AirBookRQForMobileConecting(req);

					JSONObject obj = new JSONObject(airRePriceRQ);
					String code = obj.getString("code");
					if (code.equalsIgnoreCase("S00")) {

						String transactionRefNomdex = obj.getJSONObject("details").getString("transactionRefNo");
						JSONArray tickets = obj.getJSONObject("details").getJSONObject("bookingDetail")
								.getJSONObject("pnrDetail").getJSONArray("tickets");
						String ticketNumber = "";


						for (int i = 0; i < tickets.length(); i++) {
							ticketNumber= tickets.getJSONObject(i).getString("ticketNumber");
							travellerFlightDetailsReturn.get(i).setTicketNo(ticketNumber);

						}

						String bookingRefId = obj.getJSONObject("details").getString("bookingRefId");

						JSONArray objccpnrs = obj.getJSONObject("details").getJSONObject("bookingDetail")
								.getJSONObject("pnrDetail").getJSONArray("pnrs");
						String pnrNo = objccpnrs.getJSONObject(0).getString("pnr");

						fligthBookReq.setBookingRefId(bookingRefId);
						fligthBookReq.setMdexTxnRefNo(transactionRefNomdex);
						fligthBookReq.setStatus(Status.Booked.getValue());
						fligthBookReq.setTxnRefno(req.getTransactionId());
						fligthBookReq.setSuccess(true);
						fligthBookReq.setFlightStatus(Status.Booked.getValue());
						fligthBookReq.setPnrNo(pnrNo);

						String flightBookingSucess=null;

						System.err.println("Email:: "+req.getEmailAddress());

						if (req.getPaymentMethod().equalsIgnoreCase(PaymentType.Wallet.getKey())) {
							flightBookingSucess = travelflightapi.flightBookingSucess(fligthBookReq,travellerFlightDetailsReturn);
						}
						else {
							flightBookingSucess = travelflightapi.flightPaymentGatewaySucess(fligthBookReq,req.getTravellerDetails());
						}
						JSONObject flightBookingSucessobj = new JSONObject(flightBookingSucess);
						String flightBookingSucesscode = flightBookingSucessobj.getString("code");
						if (flightBookingSucesscode.equalsIgnoreCase("S00")) {
							return new ResponseEntity<String>("" + flightBookingSucess, HttpStatus.OK);
						} else {
							/*fligthBookReq.setSuccess(false);
						travelflightapi.flightBookingSucess(fligthBookReq,req.getTravellerDetails());*/
							return new ResponseEntity<String>("" + flightBookingSucess, HttpStatus.OK);
						}
					} else {
						fligthBookReq.setTxnRefno(req.getTransactionId());
						fligthBookReq.setSuccess(false);
						if (req.getPaymentMethod().equalsIgnoreCase(PaymentType.Wallet.getKey())) {
							travelflightapi.flightBookingSucess(fligthBookReq,req.getTravellerDetails());
						}
						else {
							travelflightapi.flightPaymentGatewaySucess(fligthBookReq,req.getTravellerDetails());
						}
						return new ResponseEntity<String>(airRePriceRQ, HttpStatus.OK);
					}
				} else {
					return new ResponseEntity<String>(flightBookingInitiate, HttpStatus.OK);
				}

			} catch (Exception e) {
				e.printStackTrace();

				return new ResponseEntity<String>("" + APIUtils.getFailedJSON().toString(), HttpStatus.OK);
			}
		}
		else {
			return new ResponseEntity<String>("Please pass encrypt data", HttpStatus.OK);
		}
	}



	@RequestMapping(method = RequestMethod.POST, value = "/EncReq/Flight/MyTickets", consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<FlightResponseDTO> getAllTicketsByUserEncReq(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody SessionDTO req,
			@RequestHeader(value = "hash", required = false) String hash,HttpSession session) throws Exception {

		FlightResponseDTO resp=new FlightResponseDTO();

		if (req.getData()!=null && !req.getData().isEmpty() && !req.getData().equalsIgnoreCase("null")) {

			String decData=AES.decrypt(req.getData());
			String sessionId=TravelFlightUtil.getSessionIdDec(decData);
			
			req.setSessionId(sessionId);

			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					String authority = authenticationApi.getAuthorityFromSession(req.getSessionId(), Role.USER);
					if (authority != null) {
						try {

							resp=travelflightapi.getAllBookTicketsForMobile(req.getSessionId());
							session.setAttribute("flightMsg", "");
							session.setAttribute("flighterrMsg", "");
							/*System.err.println("Working");*/
						} catch (Exception e) {
							System.out.println(e);
							resp.setStatus(ResponseStatus.INTERNAL_SERVER_ERROR.getKey());
							resp.setCode(ResponseStatus.INTERNAL_SERVER_ERROR.getValue());
							resp.setMessage("Service Unavailable");
						}
					}else {
						resp.setStatus(ResponseStatus.INVALID_SESSION.getKey());
						resp.setCode(ResponseStatus.INVALID_SESSION.getValue());
						resp.setMessage("Session expired");
					}
				}
				else {
					resp.setCode("F00");
					resp.setMessage("Unknown device");
					resp.setStatus("FAILED");
					resp.setDetails(APIUtils.getFailedJSON().toString());
				}
			}else {
				resp.setCode("F00");
				resp.setMessage("Unauthorised access");
				resp.setStatus("FAILED");
				resp.setDetails(APIUtils.getFailedJSON().toString());
			}
		}
		else {
			resp.setCode(ResponseStatus.BAD_REQUEST.getValue());
			resp.setStatus(ResponseStatus.BAD_REQUEST.getKey());
			resp.setMessage("Please pass encrypt data");
		}
		return new ResponseEntity<FlightResponseDTO>(resp, HttpStatus.OK);
	}

}