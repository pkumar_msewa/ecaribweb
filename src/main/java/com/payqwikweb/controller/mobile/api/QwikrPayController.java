package com.payqwikweb.controller.mobile.api;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.payqwikweb.app.api.IQwikrPayApi;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.model.app.request.QwikrPayRequestDTO;
import com.payqwikweb.util.APIUtils;
import com.thirdparty.model.ResponseDTO;

@Controller
@RequestMapping("/Api/{version}/{role}/{device}/{language}/User/QwikrPay")
public class QwikrPayController {
	
	private final IQwikrPayApi qwikrPayApi; 
	
	public QwikrPayController(IQwikrPayApi qwikrPayApi) {
		super();
		this.qwikrPayApi = qwikrPayApi;
	}



	@RequestMapping(value = "/InitiateQwikrPay", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> InitializeQwikrPay(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody QwikrPayRequestDTO req,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response,HttpSession session) {
		ResponseDTO resp = new ResponseDTO();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if ((device.equalsIgnoreCase(Device.ANDROID.getValue()))
					|| (device.equalsIgnoreCase(Device.WINDOWS.getValue()))
					|| (device.equalsIgnoreCase(Device.IOS.getValue()))|| (device.equalsIgnoreCase(Device.WEBSITE.getValue()))) {
				ResponseDTO respns=qwikrPayApi.initializeQwikrPay(req);
				if (respns != null) {
					resp.setCode(respns.getCode());
					resp.setMessage(respns.getMessage());
					resp.setTransactionId(respns.getTransactionId());
				}else{
				resp.setCode("F00");
				resp.setMessage("Unexpected Response...");
				resp.setStatus("Failure");
				resp.setValid(false);
			}

			
		} else {
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Unauthorised access");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
	}else{
		resp.setCode("F00");
		resp.setMessage("User not authorized...");
	}
	
		return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
}
	
	@RequestMapping(value = "/SuccessQwikrPay", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> SuccessQwikrPay(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody QwikrPayRequestDTO req,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response) {
		ResponseDTO resp = new ResponseDTO();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if ((device.equalsIgnoreCase(Device.ANDROID.getValue()))
					|| (device.equalsIgnoreCase(Device.WINDOWS.getValue()))
					|| (device.equalsIgnoreCase(Device.IOS.getValue()))|| (device.equalsIgnoreCase(Device.WEBSITE.getValue()))) {
				ResponseDTO respns=qwikrPayApi.successQwikrPay(req);
				if (respns != null) {
					resp.setCode(respns.getCode());
					resp.setMessage(respns.getMessage());
					resp.setTransactionId(respns.getTransactionId());
					return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
				}else{
				resp.setCode("F00");
				resp.setMessage("Unexpected Response...");
				resp.setStatus("Failure");
				resp.setValid(false);
				return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
			}

			
		} else {
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Unauthorised access");
			resp.setStatus("FAILED");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
	}else{
		resp.setCode("F00");
		resp.setMessage("User not authorized...");
	}
	
		return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
}

}
