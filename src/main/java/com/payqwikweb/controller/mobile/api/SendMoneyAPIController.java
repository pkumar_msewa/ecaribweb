package com.payqwikweb.controller.mobile.api;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.payqwikweb.api.IAuthenticationApi;
import com.payqwikweb.app.api.ITransactionApi;
import com.payqwikweb.app.model.request.*;
import com.payqwikweb.app.model.response.SendMoneyBankResponse;
import com.payqwikweb.model.app.request.SendMoneyDonateeRequest;
import com.payqwikweb.model.app.response.SendMoneyDonateeResponse;
import com.payqwikweb.model.error.OfflinePaymentError;
import com.payqwikweb.model.error.SendMoneyBankError;
import com.payqwikweb.model.error.SendMoneyMobileError;
import com.payqwikweb.validation.OfflinePaymentValidation;
import com.payqwikweb.validation.PayAtStoreValidation;
import com.payqwikweb.validation.SendMoneyValidation;
import com.thirdparty.model.ResponseDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import com.payqwikweb.app.api.ISendMoneyApi;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.response.ListStoreApiResponse;
import com.payqwikweb.app.model.response.OfflinePaymentResponse;
import com.payqwikweb.app.model.response.PayAtStoreResponse;
import com.payqwikweb.app.model.response.SendMoneyMobileResponse;
import com.payqwikweb.util.APIUtils;

@Controller
@RequestMapping("/Api/{version}/{role}/{device}/{language}/SendMoney")
public class SendMoneyAPIController implements MessageSourceAware {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private MessageSource messageSource;
	private final ISendMoneyApi sendMoneyApi;
	private final IAuthenticationApi authenticationApi;
	private final SendMoneyValidation sendMoneyValidation;
	private final PayAtStoreValidation payAtStoreValidation;
	private final ITransactionApi transactionApi;
	private final OfflinePaymentValidation offlinePaymentValidation;
	public SendMoneyAPIController(ISendMoneyApi sendMoneyApi, IAuthenticationApi authenticationApi, 
					SendMoneyValidation sendMoneyValidation, PayAtStoreValidation payAtStoreValidation,ITransactionApi transactionApi,OfflinePaymentValidation offlinePaymentValidation) {
		this.sendMoneyApi = sendMoneyApi;
		this.authenticationApi = authenticationApi;
		this.sendMoneyValidation = sendMoneyValidation;
		this.payAtStoreValidation = payAtStoreValidation;
		this.transactionApi = transactionApi;
		this.offlinePaymentValidation = offlinePaymentValidation;
	}

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	@RequestMapping(value = "/Mobile", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<SendMoneyMobileResponse> sendMoneyMobile(@PathVariable(value = "version") String version, @PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SendMoneyMobileRequest dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response) {
			SendMoneyMobileResponse result = new SendMoneyMobileResponse();
			try{
				SendMoneyMobileError error=sendMoneyValidation.checkMobileError(dto);
				if(error.isValid()){
			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
						result = sendMoneyApi.sendMoneyMobileRequest(dto);
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Unknown device");
					result.setStatus("FAILED");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unauthorised access");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			 }
		   }else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Invalid Input");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			 }
			} catch (Exception e) {
				e.printStackTrace();
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("We are sorry for inconvenience, Please try again later .");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
			return new ResponseEntity<SendMoneyMobileResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/Bank", method = RequestMethod.POST)
	ResponseEntity<SendMoneyBankResponse> sendMoneyBankRequest(@PathVariable(value = "version") String version, @PathVariable(value = "role") String role,
                                                               @PathVariable(value = "device") String device, @PathVariable(value = "language") String language,@RequestBody SendMoneyBankRequest dto,
                                                               @RequestHeader(value = "hash", required = false) String hash, ModelMap modelMap, HttpSession session) {
		SendMoneyBankResponse result = new SendMoneyBankResponse();
		try{
		SendMoneyBankError error = sendMoneyValidation.checkBankError(dto);
		if (error.isValid()) {
			TransactionRequest newRequest = new TransactionRequest();
			newRequest.setSessionId(dto.getSessionId());
			newRequest.setAmount(Double.parseDouble(dto.getAmount()));
			newRequest.setTransactionRefNo("D");
				result = sendMoneyApi.sendMoneyBankRequest(dto);
				return new ResponseEntity<SendMoneyBankResponse>(result, HttpStatus.OK);
			} else {
			result.setCode("F04");
			result.setMessage("Invalid Input");
			result.setDetails(error.toJSON().toString());
		  }
	   } catch (Exception e) {
		e.printStackTrace();
		result.setSuccess(false);
		result.setCode("F00");
		result.setMessage("We are sorry for inconvenience, Please try again later .");
		result.setStatus("FAILED");
		result.setResponse(APIUtils.getFailedJSON().toString());
	}
		return new ResponseEntity<SendMoneyBankResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/Store", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<PayAtStoreResponse> sendMoneyStore(
			@PathVariable(value = "version") String version, @PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody PayAtStoreRequest dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response) {
		PayAtStoreResponse result = new PayAtStoreResponse();
		try{
			if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				result = sendMoneyApi.payAtStoreResponseRequest(dto);
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unknown device");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("Unauthorised access");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		 }
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<PayAtStoreResponse>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/ListStore", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ListStoreApiResponse> listStore(
			@PathVariable(value = "version") String version, @PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody ListStoreApiRequest dto,
			@RequestHeader(value = "hash", required = false) String hash) {
		ListStoreApiResponse result = new ListStoreApiResponse();
		try{
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				result = sendMoneyApi.listStoreResponseRequest(dto);
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unknown device");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("Unauthorised access");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		 }
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}	
		return new ResponseEntity<ListStoreApiResponse>(result, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/OfflinePayment", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<OfflinePaymentResponse> paymentAtStore(
			@PathVariable(value = "version") String version, @PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody OfflinePaymentRequest dto,
			@RequestHeader(value = "hash", required = false) String hash) {
		OfflinePaymentResponse result = new OfflinePaymentResponse();
		try{
		OfflinePaymentError error = new OfflinePaymentError();
		if (role.equalsIgnoreCase(Role.MERCHANT.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				error = offlinePaymentValidation.checkPaymentError(dto);
				if(error.isValid()) {
					result = sendMoneyApi.offlinePayment(dto);
				} else {
					result.setCode("F04");
					result.setMessage("Invalid Input");
					result.setDetails(error.toJSON().toString());
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unknown device");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("Unauthorised access");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		  }
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<OfflinePaymentResponse>(result, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/Offline/VerifyOTP", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<OfflinePaymentResponse> verifyOTP(
			@PathVariable(value = "version") String version, @PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody OfflinePaymentRequest dto,
			@RequestHeader(value = "hash", required = false) String hash) {
		OfflinePaymentResponse result = new OfflinePaymentResponse();
		try{
		OfflinePaymentError error = new OfflinePaymentError();
		if (role.equalsIgnoreCase(Role.MERCHANT.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				error = offlinePaymentValidation.checkOTPError(dto);
				if(error.isValid()){
					result = sendMoneyApi.verifyOTP(dto);
				}else {
					result.setCode("F04");
					result.setMessage("Invalid Input");
					result.setDetails(error.toJSON().toString());
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unknown device");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("Unauthorised access");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		 }
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<OfflinePaymentResponse>(result, HttpStatus.OK);
	}
	@RequestMapping(value ="/Donate", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<SendMoneyDonateeResponse> sendMoneyToDonatee(@PathVariable(value = "version") String version, @PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SendMoneyDonateeRequest dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response) {
			SendMoneyDonateeResponse result = new SendMoneyDonateeResponse();
			try{
			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					System.err.println("i am hereeeeeeeee:::");
					TransactionRequest newRequest = new TransactionRequest();
					newRequest.setSessionId(dto.getSessionId());
					newRequest.setAmount(Double.parseDouble(dto.getAmount()));
					newRequest.setTransactionRefNo("D");
					ResponseDTO responseDTO1 = transactionApi.validateTransaction(newRequest);
					System.err.println("the validation response is:::"+responseDTO1);
					if (responseDTO1.isValid()) {
						System.err.println("ransaction Valid");
						result = sendMoneyApi.sendMoneyToDonatee(dto);
					} else {
						result.setCode("F00");
						result.setMessage(responseDTO1.getMessage());
					}
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Unknown device");
					result.setStatus("FAILED");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unauthorised access");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			 }
			} catch (Exception e) {
				e.printStackTrace();
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("We are sorry for inconvenience, Please try again later .");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
			return new ResponseEntity<SendMoneyDonateeResponse>(result, HttpStatus.OK);
	}

	
}
