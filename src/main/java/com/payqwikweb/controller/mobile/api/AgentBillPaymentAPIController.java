package com.payqwikweb.controller.mobile.api;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.instantpay.api.IValidationApi;
import com.instantpay.model.request.ValidationRequest;
import com.instantpay.util.IPayConvertUtil;
import com.payqwikweb.api.IAuthenticationApi;
import com.payqwikweb.app.api.IAgentBillPaymentApi;
import com.payqwikweb.app.api.ITransactionApi;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.request.BillPaymentCommonDTO;
import com.payqwikweb.app.model.request.TransactionRequest;
import com.payqwikweb.app.model.response.BillPaymentCommonResponseDTO;
import com.payqwikweb.util.APIUtils;
import com.payqwikweb.util.ConvertUtil;
import com.payqwikweb.validation.DTHBillPaymentValidation;
import com.payqwikweb.validation.ElectricityBillPaymentValidation;
import com.payqwikweb.validation.GasBillPaymentValidation;
import com.payqwikweb.validation.InsuranceBillPaymentValidation;
import com.payqwikweb.validation.LandlineBillPaymentValidation;
import com.thirdparty.model.ResponseDTO;
import com.thirdparty.model.ValidationResponseDTO;

@Controller
@RequestMapping("/Api/{version}/{role}/{device}/{language}/Agent/BillPayment")
public class AgentBillPaymentAPIController implements MessageSourceAware {
	

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private MessageSource messageSource;
	
	private  IAgentBillPaymentApi billAgentPaymentApi;
	private  IValidationApi validationApi;
	private  ITransactionApi transactionApi;
	public AgentBillPaymentAPIController(IAgentBillPaymentApi billAgentPaymentApi,IValidationApi validationApi,ITransactionApi transactionApi) {
		this.billAgentPaymentApi = billAgentPaymentApi;
		this.validationApi = validationApi;
		this.transactionApi = transactionApi;
	}

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	@RequestMapping(value = "/Electricity", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<BillPaymentCommonResponseDTO> processElectricityBillPayment(
			@PathVariable(value = "version") String version, @PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody BillPaymentCommonDTO dto,
			@RequestHeader(value = "hash", required = false) String hash) {
		BillPaymentCommonResponseDTO result = new BillPaymentCommonResponseDTO();
		if (role.equalsIgnoreCase(Role.AGENT.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				ValidationRequest validationRequest = IPayConvertUtil.convertElectricityBillPaymentRequest(dto);
				ValidationResponseDTO responseDTO = validationApi.validateTransaction(validationRequest);
				if(responseDTO.isSuccess()) {
					TransactionRequest newRequest = new TransactionRequest();
					newRequest.setSessionId(dto.getSessionId());
					newRequest.setAmount(Double.parseDouble(dto.getAmount()));
					newRequest.setTransactionRefNo("D");
					ResponseDTO responseDTO1 = transactionApi.validateTransaction(newRequest);
					if(responseDTO1.isValid()) {
						result = billAgentPaymentApi.electricBill(dto,Role.AGENT);
					}else {
						result.setCode("F00");
						result.setMessage(responseDTO1.getMessage());
					}
				}else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage(responseDTO.getMessage());
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unknown device");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("Unauthorised access");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<BillPaymentCommonResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/Gas", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<BillPaymentCommonResponseDTO> processGasBillPayment(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody BillPaymentCommonDTO dto,
			@RequestHeader(value = "hash", required = false) String hash) {
		BillPaymentCommonResponseDTO result = new BillPaymentCommonResponseDTO();
		if (role.equalsIgnoreCase(Role.AGENT.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				ValidationRequest validationRequest = IPayConvertUtil.convertGasBillPaymentRequest(dto);
				ValidationResponseDTO responseDTO = validationApi.validateTransaction(validationRequest);
				if(responseDTO.isSuccess()) {
					TransactionRequest newRequest = new TransactionRequest();
					newRequest.setSessionId(dto.getSessionId());
					newRequest.setAmount(Double.parseDouble(dto.getAmount()));
					newRequest.setTransactionRefNo("D");
					ResponseDTO responseDTO1 = transactionApi.validateTransaction(newRequest);
					if(responseDTO1.isValid()) {
						result = billAgentPaymentApi.gasBill(dto,Role.AGENT);
					}else {
						result.setCode("F00");
						result.setMessage(responseDTO1.getMessage());
					}
				}else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage(responseDTO.getMessage());
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unknown device");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("Unauthorised access");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<BillPaymentCommonResponseDTO>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/Insurance", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<BillPaymentCommonResponseDTO> processInsuranceBillPayment(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody BillPaymentCommonDTO dto,
			@RequestHeader(value = "hash", required = false) String hash) {
		BillPaymentCommonResponseDTO result = new BillPaymentCommonResponseDTO();
		if (role.equalsIgnoreCase(Role.AGENT.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				ValidationRequest validationRequest = IPayConvertUtil.convertInsuranceBillPaymentRequest(dto);
				ValidationResponseDTO responseDTO = validationApi.validateTransaction(validationRequest);
				if(responseDTO.isSuccess()) {
					TransactionRequest newRequest = new TransactionRequest();
					newRequest.setSessionId(dto.getSessionId());
					newRequest.setAmount(Double.parseDouble(dto.getAmount()));
					newRequest.setTransactionRefNo("D");
					ResponseDTO responseDTO1 = transactionApi.validateTransaction(newRequest);
					if(responseDTO1.isValid()) {
						result = billAgentPaymentApi.insuranceBill(dto,Role.AGENT);
					}else{
						result.setCode("F00");
						result.setMessage(responseDTO1.getMessage());
					}
				}else{
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage(responseDTO.getMessage());
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unknown device");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("Unauthorised access");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<BillPaymentCommonResponseDTO>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/Landline", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<BillPaymentCommonResponseDTO> processLandlineBillPayment(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody BillPaymentCommonDTO dto,
			@RequestHeader(value = "hash", required = false) String hash) {
		BillPaymentCommonResponseDTO result = new BillPaymentCommonResponseDTO();
		if (role.equalsIgnoreCase(Role.AGENT.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				ValidationRequest validationRequest = IPayConvertUtil.convertLandlineBillPaymentRequest(dto);
				ValidationResponseDTO responseDTO = validationApi.validateTransaction(validationRequest);
				if(responseDTO.isSuccess()) {
					TransactionRequest newRequest = new TransactionRequest();
					newRequest.setSessionId(dto.getSessionId());
					newRequest.setAmount(Double.parseDouble(dto.getAmount()));
					newRequest.setTransactionRefNo("D");
					ResponseDTO responseDTO1 = transactionApi.validateTransaction(newRequest);
					if(responseDTO1.isValid()) {
						result = billAgentPaymentApi.landline(dto,Role.AGENT);
					}else {
						result.setCode("F00");
						result.setMessage(responseDTO1.getMessage());
					}
				}else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage(responseDTO.getMessage());
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unknown device");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("Unauthorised access");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<BillPaymentCommonResponseDTO>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/DTH", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<BillPaymentCommonResponseDTO> processDTHBillPayment(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody BillPaymentCommonDTO dto,
			@RequestHeader(value = "hash", required = false) String hash) {
		BillPaymentCommonResponseDTO result = new BillPaymentCommonResponseDTO();
		if (role.equalsIgnoreCase(Role.AGENT.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				ValidationRequest validationRequest = IPayConvertUtil.convertDTHBillPaymentRequest(dto);
				ValidationResponseDTO responseDTO = validationApi.validateTransaction(validationRequest);
				if(responseDTO.isSuccess()) {
					TransactionRequest newRequest = new TransactionRequest();
					newRequest.setSessionId(dto.getSessionId());
					newRequest.setAmount(Double.parseDouble(dto.getAmount()));
					newRequest.setTransactionRefNo("D");
					ResponseDTO responseDTO1 = transactionApi.validateTransaction(newRequest);
					if(responseDTO1.isValid()) {
						result = billAgentPaymentApi.dthBill(dto,Role.AGENT);
					}else {
						result.setCode("F00");
						result.setMessage(responseDTO1.getMessage());
					}
				}else{
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage(responseDTO.getMessage());
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unknown device");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("Unauthorised access");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<BillPaymentCommonResponseDTO>(result, HttpStatus.OK);
	}


//
//	private void sendCoupons(String sessionId,String amount){
//		UserDetailsResponse response = authenticationApi.getUserDetailsFromSession(sessionId);
//		CarrotFryDTO carrotFryDTO = new CarrotFryDTO();
//		carrotFryDTO.setAmount(amount);
//		carrotFryDTO.setCity(" ");
//		carrotFryDTO.setEmail(response.getEmail());
//		carrotFryDTO.setMobileNo(response.getContactNo());
//	}
	@RequestMapping(value = "/checkForSplitPayBillPay", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<BillPaymentCommonResponseDTO> transactionCheckForSplitPayment(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody BillPaymentCommonDTO dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response) {
		BillPaymentCommonResponseDTO result = new BillPaymentCommonResponseDTO();
		if (role.equalsIgnoreCase(Role.AGENT.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				
					TransactionRequest newRequest = new TransactionRequest();
					newRequest.setSessionId(dto.getSessionId());
					newRequest.setAmount(Double.parseDouble(dto.getAmount()));
					newRequest.setTransactionRefNo("D");
					ResponseDTO responseDTO1 = transactionApi.validateTransaction(newRequest);
					if(responseDTO1.getCode().equalsIgnoreCase("T01")) {
						result.setCode(responseDTO1.getCode());
						result.setMessage(responseDTO1.getMessage());
						dto.setSplitAmount(Double.toString(responseDTO1.getAmount()));
						result.setDto(dto);
						System.out.println(dto);
					}else {
						result.setCode(responseDTO1.getCode());
						result.setMessage(responseDTO1.getMessage());
						result.setDto(dto);
					}
				}else{
					result.setCode("F00");
					result.setMessage("not a valid device");
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unknown device");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		
		return new ResponseEntity<BillPaymentCommonResponseDTO>(result, HttpStatus.OK);
	}

}
