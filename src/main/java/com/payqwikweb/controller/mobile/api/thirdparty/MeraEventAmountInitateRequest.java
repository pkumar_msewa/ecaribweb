package com.payqwikweb.controller.mobile.api.thirdparty;

public class MeraEventAmountInitateRequest {

	private String sessioniId;
	private String netAmount;

	public String getSessioniId() {
		return sessioniId;
	}

	public void setSessioniId(String sessioniId) {
		this.sessioniId = sessioniId;
	}

	public String getNetAmount() {
		return netAmount;
	}

	public void setNetAmount(String netAmount) {
		this.netAmount = netAmount;
	}

}
