package com.payqwikweb.controller.mobile.api.thirdparty;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.gci.api.IGciServiceApi;
import com.gci.model.request.BrandDenominationDTO;
import com.gci.model.request.BrandStoreDTO;
import com.gci.model.request.BrandsDTO;
import com.gci.model.request.OrderDTO;
import com.gci.model.response.AddOrderResponse;
import com.gci.model.response.BrandResponse;
import com.gci.model.response.BrandStoreResposne;
import com.gci.model.response.DenominationResponse;
import com.payqwikweb.api.IAuthenticationApi;
import com.payqwikweb.app.api.IUserApi;
import com.payqwikweb.app.model.Version;
import com.payqwikweb.app.model.request.SessionDTO;
import com.payqwikweb.model.web.GciAuthDTO;
import com.payqwikweb.util.APIUtils;

@Controller
@RequestMapping("/Api/{version}/{role}/{device}/{language}/GiftCard")
public class GiftCardMobileController implements MessageSourceAware {

		protected final Logger logger = LoggerFactory.getLogger(this.getClass());
		private MessageSource messageSource;

		private IUserApi userApi;
		private IGciServiceApi gciServiceApi;
		private final IAuthenticationApi authenticationApi;

		public GiftCardMobileController(IUserApi userApi,IGciServiceApi gciServiceApi,IAuthenticationApi authenticationApi) {
			this.userApi=userApi;
			this.gciServiceApi=gciServiceApi;
			this.authenticationApi = authenticationApi;

		}

		public void setMessageSource(MessageSource messageSource) {
			this.messageSource = messageSource;
		}
	
	
	
	@RequestMapping(value = "/GetBrands", method = RequestMethod.POST)
	public ResponseEntity<BrandResponse> getAccessToken(@PathVariable(value = "version") String version,
														 @PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
														 @PathVariable(value = "language") String language, @RequestBody BrandsDTO dto,
														 @RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
														 HttpServletResponse response, HttpSession session) throws Exception {

		BrandResponse resp = new BrandResponse();
		try{
		if(version.equalsIgnoreCase("v1")) {
			if(role.equalsIgnoreCase("User")) {
				if(device.equalsIgnoreCase("Android") || device.equalsIgnoreCase("IOS")) {
					if(language.equalsIgnoreCase("EN")) {
						SessionDTO sessionDTO = new SessionDTO();
						sessionDTO.setSessionId(dto.getSessionId());
						GciAuthDTO gciAuthDTO = userApi.getGciAuth(sessionDTO);
						if(gciAuthDTO.getSuccess()) {
							dto.setToken(gciAuthDTO.getToken());
						resp=gciServiceApi.getGciBrands(dto);
						} else {
							resp.setSuccess(false);
							resp.setMessage(gciAuthDTO.getMessage());
						}
					}else {
						resp.setSuccess(false);
						resp.setMessage("Only EN locale available");
					}
				} else {
					resp.setSuccess(false);
					resp.setMessage("Unauthorized Device");
				}
			} else {
				resp.setSuccess(false);
				resp.setMessage("Unauthorized Role");
			}
		}else {
			resp.setSuccess(false);
			resp.setMessage("Invalid Version");
		 }
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setMessage("We are sorry for inconvenience, Please try again later .");
		}
		return new ResponseEntity<>(resp, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/GetBrandDenominations", method = RequestMethod.POST)
	public ResponseEntity<DenominationResponse> getBrandDenominations(@PathVariable(value = "version") String version,
														 @PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
														 @PathVariable(value = "language") String language, @RequestBody BrandDenominationDTO dto,
														 @RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
														 HttpServletResponse response, HttpSession session) throws Exception {

		DenominationResponse resp = new DenominationResponse();
		try{
		if(version.equalsIgnoreCase("v1")) {
			if(role.equalsIgnoreCase("User")) {
				if(device.equalsIgnoreCase("Android") || device.equalsIgnoreCase("IOS")) {
					if(language.equalsIgnoreCase("EN")) {
						SessionDTO sessionDTO = new SessionDTO();
						sessionDTO.setSessionId(dto.getSessionId());
						GciAuthDTO gciAuthDTO = userApi.getGciAuth(sessionDTO);
						if(gciAuthDTO.getSuccess()) {
						dto.setToken(gciAuthDTO.getToken());
						resp=gciServiceApi.getGciBrandDenominations(dto);
						} else {
							resp.setSuccess(false);
							resp.setMessage(gciAuthDTO.getMessage());
						}
					}else {
						resp.setSuccess(false);
						resp.setMessage("Only EN locale available");
					}
				} else {
					resp.setSuccess(false);
					resp.setMessage("Unauthorized Device");
				}
			} else {
				resp.setSuccess(false);
				resp.setMessage("Unauthorized Role");
			}
		}else {
			resp.setSuccess(false);
			resp.setMessage("Invalid Version");
		}
	} catch (Exception e) {
		e.printStackTrace();
		resp.setSuccess(false);
		resp.setMessage("We are sorry for inconvenience, Please try again later .");
	}	
		return new ResponseEntity<>(resp, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/GetBrandStores", method = RequestMethod.POST)
	public ResponseEntity<BrandStoreResposne> getBrandStores(@PathVariable(value = "version") String version,
														 @PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
														 @PathVariable(value = "language") String language, @RequestBody BrandStoreDTO dto,
														 @RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
														 HttpServletResponse response, HttpSession session) throws Exception {

		BrandStoreResposne resp = new BrandStoreResposne();
		try{
		if(version.equalsIgnoreCase("v1")) {
			if(role.equalsIgnoreCase("User")) {
				if(device.equalsIgnoreCase("Android") || device.equalsIgnoreCase("IOS")) {
					if(language.equalsIgnoreCase("EN")) {
						SessionDTO sessionDTO = new SessionDTO();
						sessionDTO.setSessionId(dto.getSessionId());
						GciAuthDTO gciAuthDTO = userApi.getGciAuth(sessionDTO);
						if(gciAuthDTO.getSuccess()) {
						dto.setToken(gciAuthDTO.getToken());
						resp=gciServiceApi.getGciBrandStores(dto);
						} else {
							resp.setSuccess(false);
							resp.setMessage(gciAuthDTO.getMessage());
						}
					}else {
						resp.setSuccess(false);
						resp.setMessage("Only EN locale available");
					}
				} else {
					resp.setSuccess(false);
					resp.setMessage("Unauthorized Device");
				}
			} else {
				resp.setSuccess(false);
				resp.setMessage("Unauthorized Role");
			}
		}else {
			resp.setSuccess(false);
			resp.setMessage("Invalid Version");
		}
	  } catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setMessage("We are sorry for inconvenience, Please try again later .");
		}	
		return new ResponseEntity<>(resp, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/AddOrder", method = RequestMethod.POST)
	public ResponseEntity<AddOrderResponse> getAddOrder(@PathVariable(value = "version") String version,
														 @PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
														 @PathVariable(value = "language") String language, @RequestBody OrderDTO dto,
														 @RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
														 HttpServletResponse response, HttpSession session) throws Exception {

		AddOrderResponse resp = new AddOrderResponse();
		try{
		if(Version.VERSION_1.getValue().equalsIgnoreCase(version)) {
			if(role.equalsIgnoreCase("User")) {
				if(device.equalsIgnoreCase("Android") || device.equalsIgnoreCase("IOS")) {
					if(language.equalsIgnoreCase("EN")) {
						resp=gciServiceApi.getAddOrder(dto);
					}else {
						resp.setSuccess(false);
						resp.setMessage("Only EN locale available");
					}
				} else {
					resp.setSuccess(false);
					resp.setMessage("Unauthorized Device");
				}
			} else {
				resp.setSuccess(false);
				resp.setMessage("Unauthorized Role");
			}
		}else {
			resp.setSuccess(false);
			resp.setMessage("Invalid Version");
		}
	   } catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setMessage("We are sorry for inconvenience, Please try again later .");
		}	
		return new ResponseEntity<>(resp, HttpStatus.OK);
	}

}
