package com.payqwikweb.controller.mobile.api.thirdparty;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.payqwikweb.api.IAuthenticationApi;
import com.payqwikweb.app.api.ISavaariApi;
import com.payqwikweb.app.api.ITransactionApi;
import com.payqwikweb.app.api.IYupTVApi;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.request.SavaariTokenRequest;
import com.payqwikweb.app.model.request.YupTVRequest;
import com.payqwikweb.app.model.response.SavaariResponse;
import com.payqwikweb.app.model.response.YupTVResponse;
import com.payqwikweb.util.APIUtils;
import com.payqwikweb.util.Authorities;

@Controller
@RequestMapping("/Api/{version}/{role}/{device}/{language}/YuppTV")
public class YuppTVAPIController implements MessageSourceAware {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private MessageSource messageSource;

	private final IAuthenticationApi authenticationApi;
	private final IYupTVApi yupTvApi;

	public YuppTVAPIController(IAuthenticationApi authenticationApi, IYupTVApi yupTvApi) {
		this.authenticationApi = authenticationApi;
		this.yupTvApi = yupTvApi;
	}

	@Override
	public void setMessageSource(MessageSource arg0) {
		this.messageSource = messageSource;
	}

	@RequestMapping(value = "/SaveKey", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<YupTVResponse> saveKeyAndSecret(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody YupTVRequest dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		YupTVResponse resp = new YupTVResponse();
		String sessionId = dto.getSessionId();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				if (dto.getSessionId() != null || dto.getSessionId().length() > 0) {
					String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
					if (authority != null) {
						if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
							dto.setSessionId(sessionId);
							dto.setApiKey(APIUtils.API_VALUE_API_KEY);
							dto.setApiSecret(APIUtils.APIKEY_YUPTV_VALUE);
							// resp = yupTvApi.saveKeySecret(dto);
						} else {
							resp.setSuccess(false);
							resp.setCode("F05");
							resp.setMessage("Failed, Unauthorized user.");
							resp.setDetails("Failed, Unauthorized user.");
							resp.setResponse(APIUtils.getCustomJSON(resp.getCode(), resp.getMessage()).toString());
						}
					} else {
						resp.setSuccess(false);
						resp.setCode("F03");
						resp.setMessage("Please, login and try again.");
						resp.setDetails("Please, login and try again.");
						resp.setResponse(APIUtils.getCustomJSON(resp.getCode(), resp.getMessage()).toString());
					}
				}
			} else {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Unknown device");
				resp.setDetails("Unknown device");
				resp.setResponse(APIUtils.getCustomJSON(resp.getCode(), resp.getMessage()).toString());
			}
		} else {
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Failed, Unauthorized user.");
			resp.setDetails("Failed, Unauthorized user.");
			resp.setResponse(APIUtils.getCustomJSON(resp.getCode(), resp.getMessage()).toString());
		}
		return new ResponseEntity<YupTVResponse>(resp, HttpStatus.OK);
	}

	@RequestMapping(value = "/Register", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<YupTVResponse> getAccessToken(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody YupTVRequest dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		YupTVResponse resp = new YupTVResponse();
		String sessionId = dto.getSessionId();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				if (dto.getSessionId() != null || dto.getSessionId().length() > 0) {
					String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
					if (authority != null) {
						if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
							dto.setSessionId(sessionId);
							YupTVResponse responseFromApp = yupTvApi.CheckRegister(dto);
							if (responseFromApp.getCode().equalsIgnoreCase("S00")) {
								responseFromApp.setSuccess(true);
								responseFromApp.setCode("S00");
								responseFromApp.setMessage(responseFromApp.getMessage());
								responseFromApp.setStatus(responseFromApp.getStatus());
								return new ResponseEntity<YupTVResponse>(responseFromApp, HttpStatus.OK);
							} else if (responseFromApp.getCode().equalsIgnoreCase("S01")
									|| responseFromApp.getCode().equalsIgnoreCase("S02")) {
								resp = yupTvApi.getRegister(dto);
								if (resp.getStatus().equalsIgnoreCase("1")) {
									dto.setMessage(resp.getMessage());
									dto.setStatus(resp.getStatus());
									dto.setToken(resp.getToken());
									dto.setUserId(resp.getUserId());
									dto.setPartnerId(resp.getPartnerId());
									dto.setExpiryDate(resp.getExpiry());
									System.err.println("transactionrefNo::"+responseFromApp.getTxnId());
									dto.setTransactionRefNo(responseFromApp.getTxnId());
									resp = yupTvApi.saveRegister(dto);
								} else if (resp.getStatus().equalsIgnoreCase("0")) {
									dto.setMessage(resp.getMessage());
									dto.setStatus(resp.getStatus());
									dto.setToken(resp.getToken());
									dto.setUserId(resp.getUserId());
									dto.setPartnerId(resp.getPartnerId());
									dto.setExpiryDate(resp.getExpiry());
									System.err.println("transactionrefNo::"+responseFromApp.getTxnId());
									dto.setTransactionRefNo(responseFromApp.getTxnId());
									resp = yupTvApi.existUser(dto);
								}
							} else {
								responseFromApp.setSuccess(false);
								responseFromApp.setCode("F00");
								responseFromApp.setMessage(resp.getMessage());
								responseFromApp.setStatus(resp.getStatus());
								responseFromApp.setToken(resp.getToken());
								responseFromApp.setUserId(resp.getUserId());
								responseFromApp.setPartnerId(resp.getPartnerId());
								responseFromApp.setUserId(resp.getUserId());
								responseFromApp.setResponse(
										APIUtils.getCustomJSON(responseFromApp.getCode(), responseFromApp.getMessage()).toString());
								return new ResponseEntity<YupTVResponse>(responseFromApp, HttpStatus.OK);
							}
						} else {
							resp.setSuccess(false);
							resp.setCode("F05");
							resp.setMessage("Failed, Unauthorized user.");
							resp.setDetails("Failed, Unauthorized user.");
							resp.setResponse(APIUtils.getCustomJSON(resp.getCode(), resp.getMessage()).toString());
						}
					} else {
						resp.setSuccess(false);
						resp.setCode("F03");
						resp.setMessage("Please, login and try again.");
						resp.setDetails("Please, login and try again.");
						resp.setResponse(APIUtils.getCustomJSON(resp.getCode(), resp.getMessage()).toString());
					}
				}
			} else {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Unknown device");
				resp.setDetails("Unknown device");
				resp.setResponse(APIUtils.getCustomJSON(resp.getCode(), resp.getMessage()).toString());
			}
		} else {
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Failed, Unauthorized user.");
			resp.setDetails("Failed, Unauthorized user.");
			resp.setResponse(APIUtils.getCustomJSON(resp.getCode(), resp.getMessage()).toString());
		}
		return new ResponseEntity<YupTVResponse>(resp, HttpStatus.OK);
	}

	@RequestMapping(value = "/Deactivate", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<YupTVResponse> deactivateSubscription(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody YupTVRequest dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		YupTVResponse resp = new YupTVResponse();
		String sessionId = dto.getSessionId();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				if (dto.getSessionId() != null || dto.getSessionId().length() > 0) {
					String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
					if (authority != null) {
						if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
							dto.setSessionId(sessionId);
							resp = yupTvApi.getDeactivated(dto);
						} else {
							resp.setSuccess(false);
							resp.setCode("F05");
							resp.setMessage("Failed, Unauthorized user.");
							resp.setDetails("Failed, Unauthorized user.");
							resp.setResponse(APIUtils.getCustomJSON(resp.getCode(), resp.getMessage()).toString());
						}
					} else {
						resp.setSuccess(false);
						resp.setCode("F03");
						resp.setMessage("Please, login and try again.");
						resp.setDetails("Please, login and try again.");
						resp.setResponse(APIUtils.getCustomJSON(resp.getCode(), resp.getMessage()).toString());
					}
				}
			} else {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Unknown device");
				resp.setDetails("Unknown device");
				resp.setResponse(APIUtils.getCustomJSON(resp.getCode(), resp.getMessage()).toString());
			}
		} else {
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Failed, Unauthorized user.");
			resp.setDetails("Failed, Unauthorized user.");
			resp.setResponse(APIUtils.getCustomJSON(resp.getCode(), resp.getMessage()).toString());
		}
		return new ResponseEntity<YupTVResponse>(resp, HttpStatus.OK);
	}

}
