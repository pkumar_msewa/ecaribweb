package com.payqwikweb.controller.mobile.api;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.payqwikweb.app.model.merchant.MerchantRegisterDTO;
import com.payqwikweb.app.model.request.*;
import com.payqwikweb.app.model.response.*;
import com.payqwikweb.util.JSONParserUtil;
import com.thirdparty.model.ResponseDTO;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.payqwik.visa.util.QRRequestDTO;
import com.payqwik.visa.util.QRResponseDTO;
import com.payqwik.visa.util.VisaMerchantRequest;
import com.payqwikweb.api.IAuthenticationApi;
import com.payqwikweb.app.api.IAdminApi;
import com.payqwikweb.app.api.IRegistrationApi;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.model.error.VisaMerchantError;
import com.payqwikweb.util.APIUtils;
import com.payqwikweb.util.Authorities;
import com.payqwikweb.validation.RegisterValidation;

@Controller
@RequestMapping("/Api/{version}/{role}/{device}/{language}/Merchant")
public class MerchantApiController implements MessageSourceAware {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private MessageSource messageSource;
	private final IRegistrationApi registrationApi;
	private final RegisterValidation registerValidation;
	private final IAdminApi appAdminApi;
	private final IAuthenticationApi authenticationApi;
	
	
	
	
	public MerchantApiController( IRegistrationApi registrationApi,RegisterValidation registerValidation, IAdminApi appAdminApi, IAuthenticationApi authenticationApi) {
		super();
		this.registrationApi = registrationApi;
		this.registerValidation = registerValidation;
		this.appAdminApi = appAdminApi;
		this.authenticationApi = authenticationApi;
	}

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	@RequestMapping(value = "/SignUp", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<RegistrationResponse> processMerchantRegistration(
			@PathVariable(value = "version") String version, @PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody VisaMerchantRequest dto,@RequestHeader(value = "hash", required = false) String hash) {
		RegistrationResponse result = new RegistrationResponse();
		try{
		if (role.equalsIgnoreCase(Role.MERCHANT.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				System.err.println(dto);
				VisaMerchantError error = registerValidation.checkMSignupError(dto);
				if(error.isValid()) {
					result = registrationApi.merchantSignUp(dto);
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Invalid request");
					result.setStatus("FAILED");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unknown device");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("Unauthorised access");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		 }
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<RegistrationResponse>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/addBankDetails", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<RegistrationResponse> addBankDetails(@PathVariable(value = "version") String version, 
			@PathVariable(value = "role") String role,@PathVariable(value = "device") String device, 
			@PathVariable(value = "language") String language,@RequestBody VisaMerchantRequest dto,
			@RequestHeader(value = "hash", required = false) String hash) {
		RegistrationResponse result = new RegistrationResponse();
		try{
		if (role.equalsIgnoreCase(Role.MERCHANT.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				VisaMerchantError error = registerValidation.checkMBankError(dto);
				if(error.isValid()) {
					result = registrationApi.addBankDetails(dto);
					/*if(result.getCode().equalsIgnoreCase("S00")) {
//					VisaMerchantError error1 = registerValidation.checkVisaMerchantRegistrationError(dto);
						if(error.isValid()) {
						  AddMerchantResponse resp = registrationApi.checkExistingVisaMerchant(dto);
						  if (resp.isSuccess()) {
							VisaSignUpResponse m2pResponse = appAdminApi.merchantSignOffAtM2P(dto);
							m2pResponse.setFlag(true);
							m2pResponse.setEntityId("12345");
							m2pResponse.setmVisaId("1234567");
							if (m2pResponse.isFlag()) {
								dto.setEntityId(m2pResponse.getEntityId());
								dto.setMvisaId(m2pResponse.getmVisaId());
								resp = registrationApi.signUpVisaMerchant(dto);
								if (resp.isSuccess()) {
									result.setSuccess(true);
									result.setCode("S00");
									result.setMessage("");
									result.setStatus("SUCCESS");
								} else {
									result.setSuccess(false);
									result.setCode("F00");
									result.setMessage("Error saving Visa merchant details");
									result.setStatus("FAILED");
									result.setResponse(APIUtils.getFailedJSON().toString());
								}
							} else {
								result.setSuccess(false);
								result.setCode("F00");
								result.setMessage("Error Processing Merchant Details...Please Enter All Details");
								result.setStatus("FAILED");
								result.setResponse(APIUtils.getFailedJSON().toString());
							}
						} else {
							result.setSuccess(false);
							result.setCode("F00");
							result.setMessage("Error Processing Merchant Details...Please Enter All Details");
							result.setStatus("FAILED");
							result.setResponse(APIUtils.getFailedJSON().toString());						}
						}
						
						} else {
						result.setSuccess(false);
						result.setCode("F00");
						result.setMessage("Error Processing Merchant Details...Please Enter All Details");
						result.setStatus("FAILED");
						result.setResponse(APIUtils.getFailedJSON().toString());
					}*/
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Invalid request");
					result.setStatus("FAILED");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unknown device");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		  } else {
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("Unauthorised access");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		 }
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<RegistrationResponse>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/MobileOTP", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<MobileOTPResponse> mobileOTP(
			@PathVariable(value = "version") String version, @PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody MobileOTPRequest dto,
			@RequestHeader(value = "hash", required = false) String hash) {
		MobileOTPResponse result = new MobileOTPResponse();
		try{
		if (role.equalsIgnoreCase(Role.MERCHANT.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				result = registrationApi.merchantMobileOTP(dto);
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unknown device");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("Unauthorised access");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		 }
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<MobileOTPResponse>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/ResendMobileOTP", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<String> resendMobileOTP(
			@PathVariable(value = "version") String version, @PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody ResendMobileOTPRequest dto,
			@RequestHeader(value = "hash", required = false) String hash) {
		ResendMobileOTPResponse result = new ResendMobileOTPResponse();
		try{
		if (role.equalsIgnoreCase(Role.MERCHANT.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				result = registrationApi.resendMerchantMobileOTP(dto);
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unknown device");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("Unauthorised access");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		 }
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<String>(result.getResponse(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/addVMerchant", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<RegistrationResponse> addVisaMerchanr(@PathVariable(value = "version") String version, 
			@PathVariable(value = "role") String role,@PathVariable(value = "device") String device, 
			@PathVariable(value = "language") String language,@RequestBody VisaMerchantRequest dto,
			@RequestHeader(value = "hash", required = false) String hash) {
		RegistrationResponse result = new RegistrationResponse();
		try{
		if (role.equalsIgnoreCase(Role.MERCHANT.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
			//		VisaMerchantError error = registerValidation.checkVisaMerchantRegistrationError(dto);
				//		if(error.isValid()) {
						  AddMerchantResponse resp = registrationApi.checkExistingVisaMerchant(dto);
						  if (resp.isSuccess()) {
							  System.err.println("hii m here.......");
							VisaSignUpResponse m2pResponse = appAdminApi.merchantSignOffAtM2P(dto);
							m2pResponse.setFlag(true);
							m2pResponse.setEntityId("12345");
							m2pResponse.setmVisaId("1234567");
							if (m2pResponse.isFlag()) {
								dto.setEntityId(m2pResponse.getEntityId());
								dto.setMvisaId(m2pResponse.getmVisaId());
								resp = registrationApi.signUpVisaMerchant(dto);
								if (resp.isSuccess()) {
									result.setSuccess(true);
									result.setCode("S00");
									result.setMessage("");
									result.setStatus("SUCCESS");
								} else {
									result.setSuccess(false);
									result.setCode("F00");
									result.setMessage("Error saving Visa merchant details");
									result.setStatus("FAILED");
									result.setResponse(APIUtils.getFailedJSON().toString());
								}
							} else {
								result.setSuccess(false);
								result.setCode("F00");
								result.setMessage("Error Processing Merchant Details...Please Enter All Details");
								result.setStatus("FAILED");
								result.setResponse(APIUtils.getFailedJSON().toString());
							}
						} else {
							result.setSuccess(false);
							result.setCode("F00");
							result.setMessage("Error Processing Merchant Details...Please Enter All Details");
							result.setStatus("FAILED");
							result.setResponse(APIUtils.getFailedJSON().toString());						}
				/*}else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Invalid request");
					result.setStatus("FAILED");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}*/
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unknown device");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("Unauthorised access");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		 }
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<RegistrationResponse>(result, HttpStatus.OK);
	}
	
	
	//<<<<copy>>>>>>
	@RequestMapping(value = { "/AddVisaMerchant" }, method = RequestMethod.POST,
			 produces = {
						MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<VisaSignUpResponse>processVisaMerchantRegistration(@RequestBody VisaMerchantRequest merchant,
			 HttpServletRequest request,
			HttpServletResponse response, HttpSession session, Model model,
			@PathVariable(value = "role") String role,@PathVariable(value = "device") String device,@PathVariable(value = "version") String version,
					@PathVariable(value = "language") String language) {
		System.err.println("i am inside controller.......");
		VisaSignUpResponse m2pResponse=new VisaSignUpResponse();
		try{
		//String sessionId = (String) session.getAttribute("adminSessionId");

		//if (sessionId != null && sessionId.length() != 0) {
			//String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
			//if (authority != null) {
			//	if (authority.contains(Authorities.ADMINISTRATOR) && authority.contains(Authorities.AUTHENTICATED)) {
				//	merchant.setSessionId(sessionId);
					VisaMerchantError error = registerValidation.checkVisaMerchantRegistrationError(merchant);
				if (error.isValid()) {
					AddMerchantResponse resp = appAdminApi.checkVisaMerchant(merchant);
					if (resp.isSuccess()) {
						// TODO Merchant Call
						AddMerchantResponse Vresp = appAdminApi.addVisaMerchant(merchant);
						System.err.println("this is resp::" + Vresp);
						if (Vresp.isSuccess()) {
							m2pResponse = appAdminApi.merchantSignOffAtM2P(merchant);
							if (m2pResponse.isFlag()) {
								merchant.setCustomerId(m2pResponse.getCustomerId());
								merchant.setMvisaId(m2pResponse.getmVisaId());
								merchant.setRupayId(m2pResponse.getRupayId());
								merchant.setUserName(merchant.getMobileNo());
								appAdminApi.merchantUpdateM2P(merchant);
								m2pResponse.setMessage("registration details saved..");
								m2pResponse.setCode("S00");
								m2pResponse.setFlag(true);
								return new ResponseEntity<VisaSignUpResponse>(m2pResponse, HttpStatus.OK);
							} else {
								m2pResponse.setCode("F00");
								m2pResponse.setFlag(false);
								m2pResponse.setMessage("Registration failed...please try again..");
								return new ResponseEntity<VisaSignUpResponse>(m2pResponse, HttpStatus.OK);
							}
						} else {
							m2pResponse.setCode("F00");
							m2pResponse.setMessage("merchant already exists....");
							return new ResponseEntity<>(m2pResponse, HttpStatus.OK);
						}
					} else {
						m2pResponse.setCode(resp.getCode());
						m2pResponse.setMessage(resp.getMessage());
					}
				}else {
						m2pResponse.setCode("F00");
						m2pResponse.setMessage("not valid credentials..");
				}
		    } catch (Exception e) {
			e.printStackTrace();
			m2pResponse.setCode("F00");
			m2pResponse.setMessage("We are sorry for inconvenience, Please try again later .");
		}
		 		return new ResponseEntity<>(m2pResponse,HttpStatus.OK) ;
				
	}
	
	
	@RequestMapping(value = { "/GetQRCode" }, method = RequestMethod.POST)
	ResponseEntity<QRResponseDTO>getQRCode(@RequestBody QRRequestDTO req,
			 HttpServletRequest request,HttpServletResponse response, HttpSession session) {
		QRResponseDTO resp=new QRResponseDTO();
		VisaMerchantRequest dto=new VisaMerchantRequest();
		dto.setUserName(req.getUsername());
		 resp=appAdminApi.getQRResponseFromM2P(req);
		 System.err.println("resp::::"+resp);
		 resp.setCode("S00");
		 resp.setMessage("successfully qr code generated..");
		 resp.setEntityId(resp.getEntityId());
		 resp.setQrCode(resp.getQrCode());
		 dto.setQrcode(resp.getQrCode());
		 System.err.println(req.getUsername());
		 System.err.println(resp.getQrCode());
		 appAdminApi.merchantUpdateQR(dto);
		return new ResponseEntity<QRResponseDTO>(resp,HttpStatus.OK);
		
		
	}

	@RequestMapping(value = { "/VBankSignUp" }, method = RequestMethod.POST)
	ResponseEntity<ResponseDTO> verifyKYC(@RequestBody MerchantRegisterDTO req,
										 HttpServletRequest request, HttpServletResponse response, HttpSession session) {

		ResponseDTO result = appAdminApi.verifyKYCRequest(req);
		System.err.println("resp::::"+result);
		return new ResponseEntity<ResponseDTO>(result,HttpStatus.OK);
	}

	@RequestMapping(value = { "/VBankSignUp/OTP" }, method = RequestMethod.POST)
	ResponseEntity<ResponseDTO> processKYC(@RequestBody MerchantRegisterDTO req,
										 HttpServletRequest request, HttpServletResponse response, HttpSession session) {

		ResponseDTO result = appAdminApi.verifyKYCProcess(req);
		System.err.println("resp::::"+result);
		return new ResponseEntity<ResponseDTO>(result,HttpStatus.OK);
	}


	@RequestMapping(value = { "/VBankSignUp/GeneratePassword" }, method = RequestMethod.POST)
	ResponseEntity<ResponseDTO> generatePassword(@RequestBody MerchantRegisterDTO req,
										   HttpServletRequest request, HttpServletResponse response, HttpSession session) {

		ResponseDTO result = appAdminApi.generateMerchantPassword(req);
		System.err.println("resp::::"+result);
		return new ResponseEntity<ResponseDTO>(result,HttpStatus.OK);
	}


	@RequestMapping(value = { "/GetTransactions" }, method = RequestMethod.POST)
	ResponseEntity<QRResponseDTO> getTransactions(@PathVariable("version") String version,
                                                 @PathVariable("role") String role, @PathVariable("device") String device, @RequestBody QRRequestDTO dto, HttpServletRequest request, HttpServletResponse response, HttpSession session) {
		QRResponseDTO resp=new QRResponseDTO();
		try{
        if(role.equalsIgnoreCase(Role.MERCHANT.getValue())) {
            if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
                    || device.equalsIgnoreCase(Device.IOS.getValue())) {
                UserDetailsResponse result = authenticationApi.getUserDetailsFromSession(dto.getSessionId());
                String authority = result.getAuthority();
                if(authority != null) {
                    if(authority.contains(Authorities.MERCHANT) && authority.contains(Authorities.AUTHENTICATED)) {
                        try {
                                JSONObject details = new JSONObject(result.getResponse());
                            System.err.println("details"+details);
                                if (details != null) {
                                    JSONObject merchantDetails = details.getJSONObject("merchantDetail");
                                    System.err.println("merchantDetails::"+merchantDetails);
                                    if (merchantDetails != null) {
                                        String customerid = merchantDetails.getString("customerId");
                                        resp.setEntityId(customerid);
                                        dto.setCustomerId(customerid);
                                        resp = appAdminApi.getTransactions(dto);
                                        resp.setQrCode(JSONParserUtil.getString(merchantDetails,"qrCode"));
                                        resp.setEntityId(customerid);
                                    } else {
                                        resp.setCode("F00");
                                        resp.setMessage("Merchant Details Not Found");
                                    }
                                } else {
                                    resp.setCode("F00");
                                    resp.setMessage("Details Not Found");
                                }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            resp.setCode("F00");
                            resp.setMessage("Please try again later");
                        }
                    }else {
                        resp.setCode("F00");
                        resp.setMessage("Unauthorized User");
                    }
                }else {
                    resp.setCode("F00");
                    result.setMessage("Authority Not Available");
                }
            }else {
                resp.setCode("F00");
                resp.setMessage("Not a valid device");
            }
        }else {
            resp.setCode("F00");
            resp.setMessage("Unauthorized Role");
        }
		} catch (Exception e) {
			e.printStackTrace();
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("We are sorry for inconvenience, Please try again later .");
		}
		return new ResponseEntity<QRResponseDTO>(resp,HttpStatus.OK);
	}


    @RequestMapping(value = { "/GetBalance" }, method = RequestMethod.POST)
    ResponseEntity<String> getBalance(@PathVariable("version") String version,
                                                  @PathVariable("role") String role, @PathVariable("device") String device, @RequestBody SessionDTO dto, HttpServletRequest request, HttpServletResponse response, HttpSession session) {
        String stringResponse = "";
        if(role.equalsIgnoreCase(Role.MERCHANT.getValue())) {
            if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
                    || device.equalsIgnoreCase(Device.IOS.getValue())) {
                UserDetailsResponse result = authenticationApi.getUserDetailsFromSession(dto.getSessionId());
                String authority = result.getAuthority();
                if(authority != null) {
                    if(authority.contains(Authorities.MERCHANT) && authority.contains(Authorities.AUTHENTICATED)) {
                        JSONObject details = null;
                        try {
                        details = new JSONObject(result.getResponse());
                        System.err.println("details"+details);
                        if (details != null) {
                            JSONObject merchantDetails = details.getJSONObject("merchantDetail");
                            System.err.println("merchantDetails::"+merchantDetails);
                            if (merchantDetails != null) {
                                String customerid = merchantDetails.getString("customerId");
                                stringResponse = appAdminApi.getBalanceFrom(customerid);
                                stringResponse = APIUtils.getCustomJSON("S00",stringResponse).toString();
                            } else {
                                stringResponse = APIUtils.getCustomJSON("F00","detail not found").toString();
                            }
                            }else {
                            stringResponse = APIUtils.getCustomJSON("F00","detail not found").toString();
                        }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            stringResponse = APIUtils.getCustomJSON("F00","exception occurred").toString();
                        }
                    }else {
                        stringResponse = APIUtils.getCustomJSON("F00","Unauthorized User").toString();
                    }
                }else {
                    stringResponse = APIUtils.getCustomJSON("F00","Authority Not Available").toString();
                }
            }else {
                stringResponse = APIUtils.getCustomJSON("F00","Not a valid device").toString();
            }
        }else {
            stringResponse = APIUtils.getCustomJSON("F00","Not a valid role").toString();
        }
        return new ResponseEntity<String>(stringResponse,HttpStatus.OK);
    }






}
