package com.payqwikweb.controller.mobile.api.thirdparty;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jettison.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.payqwikweb.api.IAuthenticationApi;
import com.payqwikweb.api.ITravelkhanaApi;
import com.payqwikweb.app.model.ResponseStatus;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.request.SessionDTO;
import com.payqwikweb.model.app.TravelkhanaStationlistRequest;
import com.payqwikweb.model.app.request.TKApplyCouponRequest;
import com.payqwikweb.model.app.request.TKMenuPriceCalRequest;
import com.payqwikweb.model.app.request.TKPlaceOrderRequest;
import com.payqwikweb.model.app.request.TKTrainslistRequest;
import com.payqwikweb.model.app.response.AdlabsAddOrder;
import com.payqwikweb.model.app.response.AdlabsAmountInitateRequest;
import com.payqwikweb.model.app.response.AdlabsAmountInitateResponse;
import com.payqwikweb.model.app.response.AdlabsAmountsuccessRequest;
import com.payqwikweb.model.app.response.AdlabsOrderCreateResponse;
import com.payqwikweb.model.app.response.AdlabsOrderVoucher;
import com.payqwikweb.model.app.response.AdlabsProceedResponse;
import com.payqwikweb.model.app.response.MenuDTO;
import com.payqwikweb.model.app.response.TKMenuPriceCalDTO;
import com.payqwikweb.model.app.response.TKOrderDTO;
import com.payqwikweb.model.app.response.TKMyOrderDetailsDTO;
import com.payqwikweb.model.app.response.TKOrderSuccessDTO;
import com.payqwikweb.model.app.response.StationDTO;
import com.payqwikweb.model.app.response.TKGetMenuListDTO;
import com.payqwikweb.model.app.response.TKGetOutletListDTO;
import com.payqwikweb.model.app.response.TKGetTrainRouteDTO;
import com.payqwikweb.model.app.response.TKTrainslistDTO;
import com.payqwikweb.model.app.response.TkApplyCouponDTO;
import com.payqwikweb.model.app.response.TrainDTO;
import com.payqwikweb.model.app.response.TKMenuListDTO;
import com.payqwikweb.model.app.response.TKOutletListDTO;
import com.payqwikweb.model.app.response.TKPlaceOrderInitiateResp;
import com.payqwikweb.model.app.response.TKTrackUserOrderDTO;
import com.payqwikweb.model.app.response.TKTrainRouteDTO;
import com.payqwikweb.model.web.Status;
import com.payqwikweb.util.AES;
import com.payqwikweb.util.APIUtils;
import com.payqwikweb.util.Authorities;
import com.payqwikweb.util.TravelBusUtil;
import com.payqwikweb.util.TravelkhanaEncUtil;

@RequestMapping("/Api/{version}/{role}/{device}/{language}/Travelkhana")
public class TravelkhanaMobileContoller implements MessageSourceAware {

		private ITravelkhanaApi travelkhanaApi;
		private final IAuthenticationApi authenticationApi;

		protected final Logger logger = LoggerFactory.getLogger(this.getClass());
		private MessageSource messageSource;

		public TravelkhanaMobileContoller(ITravelkhanaApi travelkhanaApi, IAuthenticationApi authenticationApi) {
			this.authenticationApi = authenticationApi;
			this.travelkhanaApi = travelkhanaApi;
		}
		public void setMessageSource(MessageSource messageSource) {
			this.messageSource = messageSource;
		}
		
		/*@RequestMapping(value ="/Trainslist", method = RequestMethod.POST)
		public ResponseEntity<TKTrainslistDTO> getTrainslist(@PathVariable(value = "version") String version,
				@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
				@PathVariable(value = "language") String language, @RequestBody TKTrainslistRequest dto,
				@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
				HttpServletResponse response, HttpSession session) throws Exception {
                
			TKTrainslistDTO resp = new TKTrainslistDTO();
			String sessionId = dto.getSessionId();
			if (sessionId != null) {
				String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
				if (authority != null) {
					if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {

						TKTrainslistRequest trainslistreq = new TKTrainslistRequest();
						TKTrainslistDTO response1 = travelkhanaApi.getTrainslist(trainslistreq);
						ArrayList<TrainDTO>response2= response1.getListOfTrain();
						 if(response2!=null&&response1.isSuccess()){
								resp.setSuccess(true);
								resp.setCode("S00");
								resp.setStatus(Status.Success);
								resp.setMessage("Trains list Successfully");
								resp.setListOfTrain(response2);
							}
						 else{
							    resp.setSuccess(false);
								resp.setCode("F00");
								resp.setMessage("No TrainList available");
						 }
					} else {
						resp.setSuccess(false);
						resp.setCode("F06");
						resp.setMessage("User Authentication Failed");
						resp.setStatus(Status.Failed);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F03");
					resp.setMessage("User Authority null");
					resp.setStatus(Status.Failed);
			     }
			} else {
				resp.setSuccess(false);
				resp.setCode("F03");
				resp.setMessage("Session null");
				resp.setStatus(Status.Failed);
			}

			return new ResponseEntity<TKTrainslistDTO>(resp, HttpStatus.OK);
		}*/
		
		@RequestMapping(value ="/Trainslist", method = RequestMethod.POST)
		public ResponseEntity<TKTrainslistDTO> getTrainslistFromDB(@PathVariable(value = "version") String version,
				@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
				@PathVariable(value = "language") String language, @RequestBody TKTrainslistRequest dto,
				@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
				HttpServletResponse response, HttpSession session) throws Exception {
                
			TKTrainslistDTO resp = new TKTrainslistDTO();
			String sessionId = dto.getSessionId();
			if (sessionId != null) {
				String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
				if (authority != null) {
					if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
						TKTrainslistRequest trainslistreq = new TKTrainslistRequest();
						trainslistreq.setSessionId(sessionId);
						TKTrainslistDTO response1 = travelkhanaApi.getTrainslistFromDB(trainslistreq);
						ArrayList<TrainDTO>response2= response1.getListOfTrain();
						 if(response2!=null&&response1.isSuccess()){
								resp.setSuccess(true);
								resp.setCode("S00");
								resp.setStatus(Status.Success);
								resp.setMessage("Trains list Successfully");
								resp.setListOfTrain(response2);
							}
						 else{
							    resp.setSuccess(false);
								resp.setCode("F00");
								resp.setMessage("No TrainList available");
						 }
					} else {
						resp.setSuccess(false);
						resp.setCode("F03");
						resp.setMessage("User Authentication Failed");
						resp.setStatus(Status.Failed);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F03");
					resp.setMessage("Please, login and try again.");
					resp.setStatus(Status.Failed);
			     }
			} else {
				resp.setSuccess(false);
				resp.setCode("F03");
				resp.setMessage("Session null");
				resp.setStatus(Status.Failed);
			}

			return new ResponseEntity<TKTrainslistDTO>(resp, HttpStatus.OK);
		}
		
		@RequestMapping(value ="/StationlistByTrainNumber", method = RequestMethod.POST)
		public ResponseEntity<TKTrainslistDTO> getStationlist(@PathVariable(value = "version") String version,
				@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
				@PathVariable(value = "language") String language, @RequestBody TKTrainslistRequest dto,
				@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
				HttpServletResponse response, HttpSession session) throws Exception {
				
			TKTrainslistDTO resp = new TKTrainslistDTO();
			String sessionId = dto.getSessionId();
			if (sessionId != null) {
				String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
				if (authority != null) {
					if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
			
						TKTrainslistDTO response1 = travelkhanaApi.getStationlist(dto);
						//ArrayList<StationDTO>response2= response1.getListOfstation();
						 if(response1!=null&&response1.isSuccess()){
								resp.setSuccess(true);
								resp.setCode("S00");
								resp.setStatus(Status.Success);
								resp.setMessage("Get Station list Successfully");
								resp.setStations(response1.getStations());
							   // resp.setListOfstation(response1.getListOfstation());
							}
						 else{
							    resp.setSuccess(false);
								resp.setCode("F00");
								resp.setMessage("No StationList available");
						 }

					} else {
						resp.setSuccess(false);
						resp.setCode("F06");
						resp.setMessage("User Authentication Failed");
						resp.setStatus(Status.Failed);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F03");
					resp.setMessage("User Authority null");
					resp.setStatus(Status.Failed);
				}
			} else {
				resp.setSuccess(false);
				resp.setCode("F03");
				resp.setMessage("Session null");
				resp.setStatus(Status.Failed);
			}

			return new ResponseEntity<TKTrainslistDTO>(resp, HttpStatus.OK);
		}

		@RequestMapping(value ="/OutletInTime", method = RequestMethod.POST)
		public ResponseEntity<TKOutletListDTO> getoutletlist(@PathVariable(value = "version") String version,
				@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
				@PathVariable(value = "language") String language, @RequestBody TKTrainslistRequest dto,
				@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
				HttpServletResponse response, HttpSession session) throws Exception {

			TKOutletListDTO resp = new TKOutletListDTO();
			String sessionId = dto.getSessionId();
			if (sessionId != null) {
				String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
				if (authority != null) {
					if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
			
						TKOutletListDTO response1 = travelkhanaApi.getOuletMenulist(dto);
						 if(response1.isSuccess()){
								resp.setSuccess(true);
								resp.setCode("S00");
								resp.setStatus(Status.Success);
								resp.setMessage("OutletMenu list Successfully");
								resp.setListOfOuletobject(response1.getListOfOuletobject());
							}
						 else{
							    resp.setSuccess(false);
								resp.setCode("F00");
								resp.setMessage(response1.getMessage());
								resp.setStatus(Status.Failed);
						 }

					} else {
						resp.setSuccess(false);
						resp.setCode("F06");
						resp.setMessage("User Authentication Failed");
						resp.setStatus(Status.Failed);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F03");
					resp.setMessage("User Authority null");
					resp.setStatus(Status.Failed);
				}
			} else {
				resp.setSuccess(false);
				resp.setCode("F03");
				resp.setMessage("Session null");
				resp.setStatus(Status.Failed);
			}

			return new ResponseEntity<TKOutletListDTO>(resp, HttpStatus.OK);
		} 
		
		@RequestMapping(value ="/MenuInTime", method = RequestMethod.POST)
		public ResponseEntity<TKMenuListDTO> getMenulist(@PathVariable(value = "version") String version,
				@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
				@PathVariable(value = "language") String language, @RequestBody TKTrainslistRequest dto,
				@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
				HttpServletResponse response, HttpSession session) throws Exception {

			TKMenuListDTO resp = new TKMenuListDTO();
			String sessionId = dto.getSessionId();
			if (sessionId != null) {
				String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
				if (authority != null) {
					if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
			
					  TKMenuListDTO response1 = travelkhanaApi.getMenulist(dto);
					  ArrayList<TKGetMenuListDTO> response2= response1.getListOfMenuobject();
						 if(response2!=null &&response1.isSuccess()){
								resp.setSuccess(true);
								resp.setCode("S00");
								resp.setStatus(Status.Success);
								resp.setMessage("Menu list Successfully");
								resp.setListOfMenuobject(response1.getListOfMenuobject());
							}
						   else{
							    resp.setSuccess(false);
								resp.setCode("F00");
						     	resp.setMessage("No Menu available");
						 }


					} else {
						resp.setSuccess(false);
						resp.setCode("F06");
						resp.setMessage("User Authentication Failed");
						resp.setStatus(Status.Failed);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F03");
					resp.setMessage("User Authority null");
					resp.setStatus(Status.Failed);
				}
			} else {
				resp.setSuccess(false);
				resp.setCode("F03");
				resp.setMessage("Session null");
				resp.setStatus(Status.Failed);
			}

			return new ResponseEntity<TKMenuListDTO>(resp, HttpStatus.OK);
		}
		
		
		@RequestMapping(value ="/TrainRoutesMenu", method = RequestMethod.POST)
		public ResponseEntity<TKTrainRouteDTO> getTrainRoutesMenulist(@PathVariable(value = "version") String version,
				@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
				@PathVariable(value = "language") String language, @RequestBody TKTrainslistRequest dto,
				@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
				HttpServletResponse response, HttpSession session) throws Exception {

			TKTrainRouteDTO resp = new TKTrainRouteDTO();
			String sessionId = dto.getSessionId();
			if (sessionId != null) {
				String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
				/*if (authority != null) {
					if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
			*/
						TKTrainRouteDTO response1 = travelkhanaApi.getTrainRoutesMenulist(dto);
						TKGetTrainRouteDTO response2= response1.getTrainRoutesListResponse();
				//		 if(response1.getCode().equalsIgnoreCase("S00")){
						  if(response1.getCode()!=null && response1.isSuccess()){
								resp.setSuccess(true);
								resp.setCode("S00");
								resp.setStatus(Status.Success);
								resp.setMessage("Menu list Successfully");
								resp.setTrainRoutesListResponse(response1.getTrainRoutesListResponse());
							}
						 else{
							    resp.setSuccess(false);
								resp.setCode(response1.getCode());
								resp.setMessage(response1.getMessage());
						   }
						  
					} else {
						resp.setSuccess(false);
						resp.setCode("F06");
						resp.setMessage("User Authentication Failed");
						resp.setStatus(Status.Failed);
					}
				/*} else {
					resp.setSuccess(false);
					resp.setCode("F03");
					resp.setMessage("User Authority null");
					resp.setStatus(Status.Failed);
				}
			} else {
				resp.setSuccess(false);
				resp.setCode("F03");
				resp.setMessage("Session null");
				resp.setStatus(Status.Failed);
			}*/

			return new ResponseEntity<TKTrainRouteDTO>(resp, HttpStatus.OK);
		} 
		
		@RequestMapping(value ="/MenuPriceCalculation", method = RequestMethod.POST)
		public ResponseEntity<TKMenuPriceCalDTO> getMenuPriceCalculation(@PathVariable(value = "version") String version,
				@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
				@PathVariable(value = "language") String language, @RequestBody TKMenuPriceCalRequest dto,
				@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
				HttpServletResponse response, HttpSession session) throws Exception {

			TKMenuPriceCalDTO resp = new TKMenuPriceCalDTO();
			String sessionId = dto.getSessionId();
			if (sessionId != null) {
				String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
				if (authority != null) {
					if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
						TKMenuPriceCalDTO response1 = travelkhanaApi.getMenuPriceCal(dto);
						 if(response!=null&&response1.isSuccess()){
								resp.setSuccess(true);
								resp.setCode("S00");
								resp.setStatus(Status.Success);
								resp.setMessage("Get Menu Price Successfully");
								resp.setMenuPriceCalDTO(response1.getMenuPriceCalDTO());
							}
						 else{
							    resp.setSuccess(false);
								resp.setCode("F00");
								resp.setMessage(response1.getMessage());
								resp.setStatus(Status.Failed);
						 }

					} else {
						resp.setSuccess(false);
						resp.setCode("F06");
						resp.setMessage("User Authentication Failed");
						resp.setStatus(Status.Failed);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F03");
					resp.setMessage("User Authority null");
					resp.setStatus(Status.Failed);
				}
			} else {
				resp.setSuccess(false);
				resp.setCode("F03");
				resp.setMessage("Session null");
				resp.setStatus(Status.Failed);
			}

			return new ResponseEntity<TKMenuPriceCalDTO>(resp, HttpStatus.OK);
		} 
		
		
		@RequestMapping(value ="/TrackUserOrder", method = RequestMethod.POST)
		public ResponseEntity<TKTrackUserOrderDTO> getUserOrder(@PathVariable(value = "version") String version,
				@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
				@PathVariable(value = "language") String language, @RequestBody TKTrainslistRequest dto,
				@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
				HttpServletResponse response, HttpSession session) throws Exception {

			TKTrackUserOrderDTO resp = new TKTrackUserOrderDTO();
			String sessionId = dto.getSessionId();
			if (sessionId != null) {
				String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
				if (authority != null) {
					if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
				
						TKTrackUserOrderDTO response1 = travelkhanaApi.trackUserOrder(dto);
					//	ArrayList<TravelkhanaMenuListResponse>listOfMenuobject= response1;
						 if(response!=null&&response1.isSuccess()){
								resp.setSuccess(true);
								resp.setCode("S00");
								resp.setStatus(Status.Success);
								resp.setMessage("TrackOrder Successfully");
								resp.settKUserOrderDTO(response1.gettKUserOrderDTO());
							}
						 else{
							    resp.setSuccess(false);
								resp.setCode("F00");
								resp.setMessage(response1.getMessage());
								resp.setStatus(Status.Failed);
						 }

					} else {
						resp.setSuccess(false);
						resp.setCode("F06");
						resp.setMessage("User Authentication Failed");
						resp.setStatus(Status.Failed);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F03");
					resp.setMessage("User Authority null");
					resp.setStatus(Status.Failed);
				}
			} else {
				resp.setSuccess(false);
				resp.setCode("F03");
				resp.setMessage("Session null");
				resp.setStatus(Status.Failed);
			}

			return new ResponseEntity<TKTrackUserOrderDTO>(resp, HttpStatus.OK);
		} 
		@RequestMapping(value ="/ApiOrder", method = RequestMethod.POST)
		public ResponseEntity<TKOrderDTO> getOrder(@PathVariable(value = "version") String version,
				@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
				@PathVariable(value = "language") String language, @RequestBody TKPlaceOrderRequest dto,
				@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
				HttpServletResponse response, HttpSession session) throws Exception {

			TKOrderDTO resp = new TKOrderDTO();
			String sessionId = dto.getSessionId();
			if (sessionId != null) {
				String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
				if (authority != null) {
					if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
					//	TKPlaceOrderRequest objinit = new TKPlaceOrderRequest();
						
						System.err.println("AMOUNT INSIDE  ApiOrder Controller " + dto.getTotalCustomerPayable());

						TKPlaceOrderInitiateResp respinitate = travelkhanaApi.initate(dto);
						
						System.err.println("INITATE AMOUNT RESPONSE::::::::::::" + respinitate.getDetails());
						System.err.println("INITATE AMOUNT RESPONSE transaction id::::::::::::" + respinitate.getTxnId());
						
				//		dto.setTrxn(respinitate.getTxnId());
								if (respinitate.getCode().equalsIgnoreCase("S00")) {
								  			 resp.setSuccess(true);
											 resp.setCode("S00");
											 resp.setMessage(respinitate.getMessage());
											 resp.setUserOrderId(respinitate.getUserOrderId());
											 resp.setStatus(Status.Success);
								  			}
								  		else{
								  			 resp.setSuccess(false);
											 resp.setCode("F00");
											 System.out.println("In foo1");
											 resp.setMessage(respinitate.getMessage());
											 resp.setStatus(Status.Failed);
								  		}
  						 } else {
							resp.setSuccess(false);
							resp.setCode("F06");
							resp.setMessage("User Authentication Failed");
							resp.setStatus(Status.Failed);
						}
				} else {
					resp.setSuccess(false);
					resp.setCode("F03");
					resp.setMessage("User Authority null");
					resp.setStatus(Status.Failed);
				}
			} else {
				resp.setSuccess(false);
				resp.setCode("F03");
				resp.setMessage("Session null");
				resp.setStatus(Status.Failed);
			}
		 
			return new ResponseEntity<TKOrderDTO>(resp, HttpStatus.OK);
     }
	
		@RequestMapping(value ="/ApiOrderSuccess", method = RequestMethod.POST)
		public ResponseEntity<TKOrderDTO>ceateOrder(@PathVariable(value = "version") String version,
				@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
				@PathVariable(value = "language") String language, @RequestBody TKPlaceOrderRequest dto,
				@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
				HttpServletResponse response, HttpSession session) throws Exception {

			TKOrderDTO resp = new TKOrderDTO();
			String sessionId = dto.getSessionId();
			if (sessionId != null) {
				String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
				if (authority != null) {
					if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
							TKOrderDTO resp2 = travelkhanaApi.placeOrder(dto);
							System.err.println("Payment  API RESPONSE:=" + resp2.getMessage());
							
							            if(resp2.isSuccess())	 
							              {
								  			 resp.setSuccess(true);
											 resp.setCode("S00");
											 resp.setMessage("Your Order Successfully Ordered");
											 System.out.println("Successfully ordered");
											 resp.setUserOrderId(resp2.getUserOrderId());
											 resp.setStatus(Status.Success);
								  			}
								  		else{
								  			 resp.setSuccess(false);
											 resp.setCode("F00");
											 System.out.println("In foo1");
											 resp.setMessage(resp2.getMessage());
											 resp.setStatus(Status.Failed);
								  		  }
							
						} else {
							resp.setSuccess(false);
							resp.setCode("F06");
							resp.setMessage("User Authentication Failed");
							resp.setStatus(Status.Failed);
						}
				} else {
					resp.setSuccess(false);
					resp.setCode("F03");
					resp.setMessage("User Authority null");
					resp.setStatus(Status.Failed);
				}
			} else {
				resp.setSuccess(false);
				resp.setCode("F03");
				resp.setMessage("Session null");
				resp.setStatus(Status.Failed);
			}
		 
			return new ResponseEntity<TKOrderDTO>(resp, HttpStatus.OK);
     }
		@RequestMapping(value ="/applycoupon", method = RequestMethod.POST)
		public ResponseEntity<TkApplyCouponDTO> applycoupon(@PathVariable(value = "version") String version,
				@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
				@PathVariable(value = "language") String language, @RequestBody TKApplyCouponRequest dto,
				@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
				HttpServletResponse response, HttpSession session) throws Exception {
                
			TkApplyCouponDTO resp = new TkApplyCouponDTO();
			String sessionId = dto.getSessionId(); 
			if (sessionId != null) {
				String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
				if (authority != null) {
					if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {

				//		TKTrainslistRequest trainslistreq = new TKTrainslistRequest();
						TkApplyCouponDTO response1 = travelkhanaApi.applycoupon(dto);
				//		ArrayList<TrainDTO>response2= response1.getListOfTrain();
						 if(response1!=null&&response1.isSuccess()){
								resp.setSuccess(true);
								resp.setCode("S00");
								resp.setStatus(Status.Success);
								resp.setMessage(response1.getMessage());
								resp.setCartDetails(response1.getCartDetails());
								resp.setPassengerInfo(response1.getPassengerInfo());
							}
						 else{
							    resp.setSuccess(false);
								resp.setCode("F00");
								resp.setMessage(response1.getMessage());
						 }
					} else {
						resp.setSuccess(false);
						resp.setCode("F05");
						resp.setMessage("User Authentication Failed");
						resp.setStatus(Status.Failed);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F03");
					resp.setMessage("User Authority null");
					resp.setStatus(Status.Failed);
			     }
			} else {
				resp.setSuccess(false);
				resp.setCode("F03");
				resp.setMessage("Session null");
				resp.setStatus(Status.Failed);
			}

			return new ResponseEntity<TkApplyCouponDTO>(resp, HttpStatus.OK);
		}
    
		@RequestMapping(value ="/MyOrder", method = RequestMethod.POST)
		public ResponseEntity<TKMyOrderDetailsDTO> myOrder(@PathVariable(value = "version") String version,
				@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
				@PathVariable(value = "language") String language, @RequestBody SessionDTO dto,
				@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
				HttpServletResponse response, HttpSession session) throws Exception {
                
			TKMyOrderDetailsDTO resp = new TKMyOrderDetailsDTO();
			String sessionId = dto.getSessionId(); 
			if (sessionId != null) {
				String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
				if (authority != null) {
					if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {

						TKMyOrderDetailsDTO response1 = travelkhanaApi.myOrder(dto);
						 if(response1!=null&&response1.isSuccess()){
								resp.setSuccess(true);
								resp.setCode("S00");
								resp.setStatus(Status.Success);
								resp.setMessage(response1.getMessage());
								resp.setOrderDetails(response1.getOrderDetails());
							}
						 else{
							    resp.setSuccess(false);
								resp.setCode("F00");
								resp.setMessage(response1.getMessage());
						 }
					} else {
						resp.setSuccess(false);	
						resp.setCode("F05");
						resp.setMessage("User Authentication Failed");
						resp.setStatus(Status.Failed);
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F03");
					resp.setMessage("User Authority null");
					resp.setStatus(Status.Failed);
			     }
			} else {
				resp.setSuccess(false);
				resp.setCode("F03");
				resp.setMessage("Session null");
				resp.setStatus(Status.Failed);
			}

			return new ResponseEntity<TKMyOrderDetailsDTO>(resp, HttpStatus.OK);
		}
		
/****************************ENCRYPTION TRAVELKHANA *******************************************/

				@RequestMapping(value ="/EncReq/Trainslist", method = RequestMethod.POST)
				public ResponseEntity<TKTrainslistDTO> getEncTrainslist(@PathVariable(value = "version") String version,
						@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
						@PathVariable(value = "language") String language, @RequestBody TKTrainslistRequest dto,
						@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
						HttpServletResponse response, HttpSession session) throws Exception {
		                
				TKTrainslistDTO resp = new TKTrainslistDTO();
				if (dto.getData()!=null && !dto.getData().isEmpty() && !dto.getData().equalsIgnoreCase("null")) {

				  String decData=AES.decrypt(dto.getData());
				  String sessionId=TravelkhanaEncUtil.getSessionIdDec(decData);
				  dto.setSessionId(sessionId);					
					if (sessionId != null) {
						String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
						if (authority != null) {
							if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {

								TKTrainslistRequest trainslistreq = new TKTrainslistRequest();
								TKTrainslistDTO response1 = travelkhanaApi.getTrainslist(trainslistreq);
								ArrayList<TrainDTO>response2= response1.getListOfTrain();
								 if(response2!=null&&response1.isSuccess()){
										resp.setSuccess(true);
										resp.setCode("S00");
										resp.setStatus(Status.Success);
										resp.setMessage("Trains list Successfully");
										resp.setListOfTrain(response2);
									}
								 else{
									    resp.setSuccess(false);
										resp.setCode("F00");
										resp.setMessage("No TrainList available");
								 }
							} else {
								resp.setSuccess(false);
								resp.setCode("F06");
								resp.setMessage("User Authentication Failed");
								resp.setStatus(Status.Failed);
							}
						} else {
							resp.setSuccess(false);
							resp.setCode("F03");
							resp.setMessage("User Authority null");
							resp.setStatus(Status.Failed);
					     }
					} else {
						resp.setSuccess(false);
						resp.setCode("F03");
						resp.setMessage("Session null");
						resp.setStatus(Status.Failed);
					}

				}
				return new ResponseEntity<TKTrainslistDTO>(resp, HttpStatus.OK);
		}
				
				@RequestMapping(value ="/EncReq/StationlistByTrainNumber", method = RequestMethod.POST)
				public ResponseEntity<TKTrainslistDTO> getEncStationlist(@PathVariable(value = "version") String version,
						@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
						@PathVariable(value = "language") String language, @RequestBody TKTrainslistRequest dto,
						@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
						HttpServletResponse response, HttpSession session) throws Exception {
						
					TKTrainslistDTO resp = new TKTrainslistDTO();
					if (dto.getData()!=null && !dto.getData().isEmpty() && !dto.getData().equalsIgnoreCase("null")) {

					String decData=AES.decrypt(dto.getData());
					TKTrainslistRequest dto2 = TravelkhanaEncUtil.getTrainNumber(decData);
					String sessionId = dto2.getSessionId();
					if (sessionId != null) {
						String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
						if (authority != null) {
							if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
					
								TKTrainslistDTO response1 = travelkhanaApi.getStationlist(dto2);
								 if(response1!=null&&response1.isSuccess()){
										resp.setSuccess(true);
										resp.setCode("S00");
										resp.setStatus(Status.Success);
										resp.setMessage("Get Station list Successfully");
										resp.setStations(response1.getStations());
									}
								 else{
									    resp.setSuccess(false);
										resp.setCode("F00");
										resp.setMessage("No StationList available");
								 }

							} else {
								resp.setSuccess(false);
								resp.setCode("F06");
								resp.setMessage("User Authentication Failed");
								resp.setStatus(Status.Failed);
							}
						} else {
							resp.setSuccess(false);
							resp.setCode("F03");
							resp.setMessage("User Authority null");
							resp.setStatus(Status.Failed);
						}
					} else {
						resp.setSuccess(false);
						resp.setCode("F03");
						resp.setMessage("Session null");
						resp.setStatus(Status.Failed);
					}
				  }
					return new ResponseEntity<TKTrainslistDTO>(resp, HttpStatus.OK);
			}

				@RequestMapping(value ="/EncReq/OutletInTime", method = RequestMethod.POST)
				public ResponseEntity<TKOutletListDTO> getEncoutletlist(@PathVariable(value = "version") String version,
						@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
						@PathVariable(value = "language") String language, @RequestBody TKTrainslistRequest dto,
						@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
						HttpServletResponse response, HttpSession session) throws Exception {

					TKOutletListDTO resp = new TKOutletListDTO();
					if (dto.getData()!=null && !dto.getData().isEmpty() && !dto.getData().equalsIgnoreCase("null")) {

					String decData=AES.decrypt(dto.getData());
					TKTrainslistRequest dto2 = TravelkhanaEncUtil.getOutletInTime(decData);
					String sessionId = dto2.getSessionId();
					if (sessionId != null) {
						String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
						if (authority != null) {
							if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
					
								TKOutletListDTO response1 = travelkhanaApi.getOuletMenulist(dto2);
								 if(response1.isSuccess()){
										resp.setSuccess(true);
										resp.setCode("S00");
										resp.setStatus(Status.Success);
										resp.setMessage("OutletMenu list Successfully");
										resp.setListOfOuletobject(response1.getListOfOuletobject());
									}
								 else{
									    resp.setSuccess(false);
										resp.setCode("F00");
										resp.setMessage(response1.getMessage());
										resp.setStatus(Status.Failed);
								 }

							} else {
								resp.setSuccess(false);
								resp.setCode("F06");
								resp.setMessage("User Authentication Failed");
								resp.setStatus(Status.Failed);
							}
						} else {
							resp.setSuccess(false);
							resp.setCode("F03");
							resp.setMessage("User Authority null");
							resp.setStatus(Status.Failed);
						}
					} else {
						resp.setSuccess(false);
						resp.setCode("F03");
						resp.setMessage("Session null");
						resp.setStatus(Status.Failed);
					 }
				  }
					return new ResponseEntity<TKOutletListDTO>(resp, HttpStatus.OK);
			 } 
				
				@RequestMapping(value ="/EncReq/MenuInTime", method = RequestMethod.POST)
				public ResponseEntity<TKMenuListDTO> getEncMenulist(@PathVariable(value = "version") String version,
						@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
						@PathVariable(value = "language") String language, @RequestBody TKTrainslistRequest dto,
						@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
						HttpServletResponse response, HttpSession session) throws Exception {

					TKMenuListDTO resp = new TKMenuListDTO();
					if (dto.getData()!=null && !dto.getData().isEmpty() && !dto.getData().equalsIgnoreCase("null")) {

						String decData=AES.decrypt(dto.getData());
						TKTrainslistRequest dto2 = TravelkhanaEncUtil.getMenuInTime(decData);
						String sessionId = dto2.getSessionId();
					if (sessionId != null) {
						String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
						if (authority != null) {
							if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
					
							  TKMenuListDTO response1 = travelkhanaApi.getMenulist(dto2);
							  ArrayList<TKGetMenuListDTO> response2= response1.getListOfMenuobject();
								 if(response2!=null &&response1.isSuccess()){
										resp.setSuccess(true);
										resp.setCode("S00");
										resp.setStatus(Status.Success);
										resp.setMessage("Menu list Successfully");
										resp.setListOfMenuobject(response1.getListOfMenuobject());
									}
								   else{
									    resp.setSuccess(false);
										resp.setCode("F00");
								     	resp.setMessage("No Menu available");
								 }


							} else {
								resp.setSuccess(false);
								resp.setCode("F06");
								resp.setMessage("User Authentication Failed");
								resp.setStatus(Status.Failed);
							}
						} else {
							resp.setSuccess(false);
							resp.setCode("F03");
							resp.setMessage("User Authority null");
							resp.setStatus(Status.Failed);
						}
					} else {
						resp.setSuccess(false);
						resp.setCode("F03");
						resp.setMessage("Session null");
						resp.setStatus(Status.Failed);
					 }
				   }
				    return new ResponseEntity<TKMenuListDTO>(resp, HttpStatus.OK);
			}
				
				
				@RequestMapping(value ="/EncReq/TrainRoutesMenu", method = RequestMethod.POST)
				public ResponseEntity<TKTrainRouteDTO> getEncTrainRoutesMenulist(@PathVariable(value = "version") String version,
						@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
						@PathVariable(value = "language") String language, @RequestBody TKTrainslistRequest dto,
						@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
						HttpServletResponse response, HttpSession session) throws Exception {

					TKTrainRouteDTO resp = new TKTrainRouteDTO();
					if (dto.getData()!=null && !dto.getData().isEmpty() && !dto.getData().equalsIgnoreCase("null")) {

						String decData=AES.decrypt(dto.getData());
						TKTrainslistRequest dto2 = TravelkhanaEncUtil.getTrainRoutesMenu(decData);
						String sessionId = dto2.getSessionId();
					if (sessionId != null) {
						String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
						if (authority != null) {
							if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
					
								TKTrainRouteDTO response1 = travelkhanaApi.getTrainRoutesMenulist(dto2);
								TKGetTrainRouteDTO response2= response1.getTrainRoutesListResponse();
						//		 if(response1.getCode().equalsIgnoreCase("S00")){
								  if(response1.getCode()!=null && response1.isSuccess()){
										resp.setSuccess(true);
										resp.setCode("S00");
										resp.setStatus(Status.Success);
										resp.setMessage("Menu list Successfully");
										resp.setTrainRoutesListResponse(response1.getTrainRoutesListResponse());
									}
								 else{
									   
									    resp.setSuccess(false);
										resp.setCode(response1.getCode());
										resp.setMessage(response1.getMessage());
								 }

							} else {
								resp.setSuccess(false);
								resp.setCode("F06");
								resp.setMessage("User Authentication Failed");
								resp.setStatus(Status.Failed);
							}
						} else {
							resp.setSuccess(false);
							resp.setCode("F03");
							resp.setMessage("User Authority null");
							resp.setStatus(Status.Failed);
						}
					} else {
						resp.setSuccess(false);
						resp.setCode("F03");
						resp.setMessage("Session null");
						resp.setStatus(Status.Failed);
					}
				 }
				return new ResponseEntity<TKTrainRouteDTO>(resp, HttpStatus.OK);
			} 
				
				@RequestMapping(value ="/EncReq/MenuPriceCalculation", method = RequestMethod.POST)
				public ResponseEntity<TKMenuPriceCalDTO> getEncMenuPriceCalculation(@PathVariable(value = "version") String version,
						@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
						@PathVariable(value = "language") String language, @RequestBody TKMenuPriceCalRequest dto,
						@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
						HttpServletResponse response, HttpSession session) throws Exception {

					TKMenuPriceCalDTO resp = new TKMenuPriceCalDTO();

					if (dto.getData()!=null && !dto.getData().isEmpty() && !dto.getData().equalsIgnoreCase("null")) {

						String decData=AES.decrypt(dto.getData());
						TKMenuPriceCalRequest dto2 = TravelkhanaEncUtil.getMenuPriceCalculation(decData);
						String sessionId = dto2.getSessionId();
					if (sessionId != null) {
						String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
						if (authority != null) {
							if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
								TKMenuPriceCalDTO response1 = travelkhanaApi.getMenuPriceCal(dto2);
								 if(response!=null&&response1.isSuccess()){
										resp.setSuccess(true);
										resp.setCode("S00");
										resp.setStatus(Status.Success);
										resp.setMessage("Get Menu Price Successfully");
										resp.setMenuPriceCalDTO(response1.getMenuPriceCalDTO());
									}
								 else{
									    resp.setSuccess(false);
										resp.setCode("F00");
										resp.setMessage(response1.getMessage());
										resp.setStatus(Status.Failed);
								 }

							} else {
								resp.setSuccess(false);
								resp.setCode("F06");
								resp.setMessage("User Authentication Failed");
								resp.setStatus(Status.Failed);
							}
						} else {
							resp.setSuccess(false);
							resp.setCode("F03");
							resp.setMessage("User Authority null");
							resp.setStatus(Status.Failed);
						}
					} else {
						resp.setSuccess(false);
						resp.setCode("F03");
						resp.setMessage("Session null");
						resp.setStatus(Status.Failed);
					}
				  }
					return new ResponseEntity<TKMenuPriceCalDTO>(resp, HttpStatus.OK);
			} 
				
				@RequestMapping(value ="/EncReq/TrackUserOrder", method = RequestMethod.POST)
				public ResponseEntity<TKTrackUserOrderDTO> getEncUserOrder(@PathVariable(value = "version") String version,
						@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
						@PathVariable(value = "language") String language, @RequestBody TKTrainslistRequest dto,
						@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
						HttpServletResponse response, HttpSession session) throws Exception {

					TKTrackUserOrderDTO resp = new TKTrackUserOrderDTO();

					if (dto.getData()!=null && !dto.getData().isEmpty() && !dto.getData().equalsIgnoreCase("null")) {

						String decData=AES.decrypt(dto.getData());
						TKTrainslistRequest dto2 = TravelkhanaEncUtil.TrackUserOrder(decData);
						String sessionId = dto2.getSessionId();
					if (sessionId != null) {
						String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
						if (authority != null) {
							if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
						
								TKTrackUserOrderDTO response1 = travelkhanaApi.trackUserOrder(dto2);
							//	ArrayList<TravelkhanaMenuListResponse>listOfMenuobject= response1;
								 if(response!=null&&response1.isSuccess()){
										resp.setSuccess(true);
										resp.setCode("S00");
										resp.setStatus(Status.Success);
										resp.setMessage("TrackOrder Successfully");
										resp.settKUserOrderDTO(response1.gettKUserOrderDTO());
									}
								 else{
									    resp.setSuccess(false);
										resp.setCode("F00");
										resp.setMessage(response1.getMessage());
										resp.setStatus(Status.Failed);
								 }

							} else {
								resp.setSuccess(false);
								resp.setCode("F06");
								resp.setMessage("User Authentication Failed");
								resp.setStatus(Status.Failed);
							}
						} else {
							resp.setSuccess(false);
							resp.setCode("F03");
							resp.setMessage("User Authority null");
							resp.setStatus(Status.Failed);
						}
					} else {
						resp.setSuccess(false);
						resp.setCode("F03");
						resp.setMessage("Session null");
						resp.setStatus(Status.Failed);
					}
				  }
					return new ResponseEntity<TKTrackUserOrderDTO>(resp, HttpStatus.OK);
			} 
				@RequestMapping(value ="/EncReq/ApiOrder", method = RequestMethod.POST)
				public ResponseEntity<TKOrderDTO> getEncOrder(@PathVariable(value = "version") String version,
						@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
						@PathVariable(value = "language") String language, @RequestBody TKPlaceOrderRequest dto,
						@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
						HttpServletResponse response, HttpSession session) throws Exception {

					TKOrderDTO resp = new TKOrderDTO();
					if (dto.getData()!=null && !dto.getData().isEmpty() && !dto.getData().equalsIgnoreCase("null")) {

						String decData=AES.decrypt(dto.getData());
						TKPlaceOrderRequest dto2 = TravelkhanaEncUtil.ApiOrder(decData);
						String sessionId = dto2.getSessionId();
					if (sessionId != null) {
						String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
						if (authority != null) {
							if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
							//	TKPlaceOrderRequest objinit = new TKPlaceOrderRequest();
								
								System.err.println("AMOUNT INSIDE  ApiOrder Controller " + dto.getTotalCustomerPayable());

								TKPlaceOrderInitiateResp respinitate = travelkhanaApi.initate(dto2);
								
								System.err.println("INITATE AMOUNT RESPONSE::::::::::::" + respinitate.getDetails());
								System.err.println("INITATE AMOUNT RESPONSE transaction id::::::::::::" + respinitate.getTxnId());
								
						//		dto.setTrxn(respinitate.getTxnId());
										if (respinitate.getCode().equalsIgnoreCase("S00")) {
										  			 resp.setSuccess(true);
													 resp.setCode("S00");
													 resp.setMessage(respinitate.getMessage());
													 resp.setUserOrderId(respinitate.getUserOrderId());
													 resp.setStatus(Status.Success);
										  			}
										  		else{
										  			 resp.setSuccess(false);
													 resp.setCode("F00");
													 System.out.println("In foo1");
													 resp.setMessage(respinitate.getMessage());
													 resp.setStatus(Status.Failed);
										  		}
		  						 } else {
									resp.setSuccess(false);
									resp.setCode("F06");
									resp.setMessage("User Authentication Failed");
									resp.setStatus(Status.Failed);
								}
						} else {
							resp.setSuccess(false);
							resp.setCode("F03");
							resp.setMessage("User Authority null");
							resp.setStatus(Status.Failed);
						}
					} else {
						resp.setSuccess(false);
						resp.setCode("F03");
						resp.setMessage("Session null");
						resp.setStatus(Status.Failed);
					}
					}
					return new ResponseEntity<TKOrderDTO>(resp, HttpStatus.OK);
		     }
			
				@RequestMapping(value ="/EncReq/ApiOrderSuccess", method = RequestMethod.POST)
				public ResponseEntity<TKOrderDTO>ceateEncOrder(@PathVariable(value = "version") String version,
						@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
						@PathVariable(value = "language") String language, @RequestBody TKPlaceOrderRequest dto,
						@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
						HttpServletResponse response, HttpSession session) throws Exception {

					TKOrderDTO resp = new TKOrderDTO();
					if (dto.getData()!=null && !dto.getData().isEmpty() && !dto.getData().equalsIgnoreCase("null")) {

						String decData=AES.decrypt(dto.getData());
						TKTrainslistRequest dto2 = TravelkhanaEncUtil.TrackUserOrder(decData);
						String sessionId = dto2.getSessionId();
					if (sessionId != null) {
						String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
						if (authority != null) {
							if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
									TKOrderDTO resp2 = travelkhanaApi.placeOrder(dto);
									System.err.println("Payment  API RESPONSE:=" + resp2.getMessage());
									
									            if(resp2.isSuccess())	 
									              {
										  			 resp.setSuccess(true);
													 resp.setCode("S00");
													 resp.setMessage("Your Order Successfully Ordered");
													 System.out.println("Successfully ordered");
													 resp.setUserOrderId(resp2.getUserOrderId());
													 resp.setStatus(Status.Success);
										  			}
										  		else{
										  			 resp.setSuccess(false);
													 resp.setCode("F00");
													 System.out.println("In foo1");
													 resp.setMessage(resp2.getMessage());
													 resp.setStatus(Status.Failed);
										  		  }
									
								} else {
									resp.setSuccess(false);
									resp.setCode("F06");
									resp.setMessage("User Authentication Failed");
									resp.setStatus(Status.Failed);
								}
						} else {
							resp.setSuccess(false);
							resp.setCode("F03");
							resp.setMessage("User Authority null");
							resp.setStatus(Status.Failed);
						}
					} else {
						resp.setSuccess(false);
						resp.setCode("F03");
						resp.setMessage("Session null");
						resp.setStatus(Status.Failed);
					}
				 } 
					return new ResponseEntity<TKOrderDTO>(resp, HttpStatus.OK);
		     }
				@RequestMapping(value ="/EncReq/applycoupon", method = RequestMethod.POST)
				public ResponseEntity<TkApplyCouponDTO> applyEnccoupon(@PathVariable(value = "version") String version,
						@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
						@PathVariable(value = "language") String language, @RequestBody TKApplyCouponRequest dto,
						@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
						HttpServletResponse response, HttpSession session) throws Exception {
		                
					TkApplyCouponDTO resp = new TkApplyCouponDTO();
					if (dto.getData()!=null && !dto.getData().isEmpty() && !dto.getData().equalsIgnoreCase("null")) {

						String decData=AES.decrypt(dto.getData());
						TKApplyCouponRequest dto2 = TravelkhanaEncUtil.applycoupon(decData);
						String sessionId = dto2.getSessionId();
					if (sessionId != null) {
						String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
						if (authority != null) {
							if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {

						//		TKTrainslistRequest trainslistreq = new TKTrainslistRequest();
								TkApplyCouponDTO response1 = travelkhanaApi.applycoupon(dto);
						//		ArrayList<TrainDTO>response2= response1.getListOfTrain();
								 if(response1!=null&&response1.isSuccess()){
										resp.setSuccess(true);
										resp.setCode("S00");
										resp.setStatus(Status.Success);
										resp.setMessage(response1.getMessage());
										resp.setCartDetails(response1.getCartDetails());
										resp.setPassengerInfo(response1.getPassengerInfo());
									}
								 else{
									    resp.setSuccess(false);
										resp.setCode("F00");
										resp.setMessage(response1.getMessage());
								 }
							} else {
								resp.setSuccess(false);
								resp.setCode("F05");
								resp.setMessage("User Authentication Failed");
								resp.setStatus(Status.Failed);
							}
						} else {
							resp.setSuccess(false);
							resp.setCode("F03");
							resp.setMessage("User Authority null");
							resp.setStatus(Status.Failed);
					     }
					} else {
						resp.setSuccess(false);
						resp.setCode("F03");
						resp.setMessage("Session null");
						resp.setStatus(Status.Failed);
					}
				  }
					return new ResponseEntity<TkApplyCouponDTO>(resp, HttpStatus.OK);
				}
		    
				@RequestMapping(value ="/EncReq/MyOrder", method = RequestMethod.POST)
				public ResponseEntity<TKMyOrderDetailsDTO> myEncOrder(@PathVariable(value = "version") String version,
						@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
						@PathVariable(value = "language") String language, @RequestBody SessionDTO dto,
						@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
						HttpServletResponse response, HttpSession session) throws Exception {
		                
					TKMyOrderDetailsDTO resp = new TKMyOrderDetailsDTO();
					if (dto.getData()!=null && !dto.getData().isEmpty() && !dto.getData().equalsIgnoreCase("null")) {

						String decData=AES.decrypt(dto.getData());
						String sessionId=TravelkhanaEncUtil.getSessionIdDec(decData);
						SessionDTO dto2 = new SessionDTO();
						dto2.setSessionId(sessionId);
					if (sessionId != null) {
						String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
						if (authority != null) {
							if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {

								TKMyOrderDetailsDTO response1 = travelkhanaApi.myOrder(dto2);
								 if(response1!=null&&response1.isSuccess()){
										resp.setSuccess(true);
										resp.setCode("S00");
										resp.setStatus(Status.Success);
										resp.setMessage(response1.getMessage());
										resp.setOrderDetails(response1.getOrderDetails());
									}
								 else{
									    resp.setSuccess(false);
										resp.setCode("F00");
										resp.setMessage(response1.getMessage());
								 }
							} else {
								resp.setSuccess(false);	
								resp.setCode("F05");
								resp.setMessage("User Authentication Failed");
								resp.setStatus(Status.Failed);
							}
						} else {
							resp.setSuccess(false);
							resp.setCode("F03");
							resp.setMessage("User Authority null");
							resp.setStatus(Status.Failed);
					     }
					} else {
						resp.setSuccess(false);
						resp.setCode("F03");
						resp.setMessage("Session null");
						resp.setStatus(Status.Failed);
					}
				  }
					return new ResponseEntity<TKMyOrderDetailsDTO>(resp, HttpStatus.OK);
			}
				@RequestMapping(value ="/getTrainListFromTK", method = RequestMethod.POST)
				public ResponseEntity<TKTrainslistDTO> getTrainslistFromTK(@PathVariable(value = "version") String version,
						@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
						@PathVariable(value = "language") String language,
						@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
						HttpServletResponse response, HttpSession session) throws Exception {
		                
					TKTrainslistDTO resp = new TKTrainslistDTO();
					TKTrainslistRequest dto =new TKTrainslistRequest();
					TKTrainslistDTO response1 = travelkhanaApi.getTrainslist(dto);
					ArrayList<TrainDTO>response2= response1.getListOfTrain();
					 if(response2!=null&&response1.isSuccess()){
							resp.setSuccess(true);
							resp.setCode("S00");
							resp.setStatus(Status.Success);
							resp.setMessage("Trains list Successfully");
							resp.setListOfTrain(response2);
						}
					 else{
						    resp.setSuccess(false);
							resp.setCode("F00");
							resp.setMessage("No TrainList available");
					 }
					
					return new ResponseEntity<TKTrainslistDTO>(resp, HttpStatus.OK);
				}
		  
	  }
				
		