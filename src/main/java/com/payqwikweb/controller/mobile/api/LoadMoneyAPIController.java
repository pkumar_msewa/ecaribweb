package com.payqwikweb.controller.mobile.api;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.ParserConfigurationException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.xml.sax.SAXException;

import com.payqwikweb.api.IAuthenticationApi;
import com.payqwikweb.app.api.ILoadMoneyApi;
import com.payqwikweb.app.model.RazorPayResponse;
import com.payqwikweb.app.model.ResponseStatus;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.request.LoadMoneyRequest;
import com.payqwikweb.app.model.request.RazorPayRequest;
import com.payqwikweb.app.model.request.SessionDTO;
import com.payqwikweb.app.model.request.VNetRequest;
import com.payqwikweb.app.model.response.EBSRedirectResponse;
import com.payqwikweb.app.model.response.EBSStatusResponseDTO;
import com.payqwikweb.app.model.response.LoadMoneyResponse;
import com.payqwikweb.app.model.response.TransactionReportResponse;
import com.payqwikweb.app.model.response.VNetResponse;
import com.payqwikweb.app.model.response.VRedirectResponse;
import com.payqwikweb.model.error.LoadMoneyError;
import com.payqwikweb.model.web.RefundStatusDTO;
import com.payqwikweb.util.APIUtils;
import com.payqwikweb.util.Authorities;
import com.payqwikweb.util.ConvertUtil;
import com.payqwikweb.util.JSONParserUtil;
import com.payqwikweb.util.ModelMapKey;
import com.payqwikweb.validation.LoadMoneyValidation;
import com.thirdparty.model.ResponseDTO;
import com.thirdparty.model.UpiSdkResponseDTO;
import com.thirdparty.model.error.CommonError;
import com.upi.model.requet.UpiMobileRedirectRequest;

@Controller
@RequestMapping("/Api/{version}/{role}/{device}/{language}/LoadMoney")
public class LoadMoneyAPIController implements MessageSourceAware {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

	private MessageSource messageSource;

	private final ILoadMoneyApi loadMoneyApi;
	private final IAuthenticationApi authenticationApi;
	private final LoadMoneyValidation loadMoneyValidation;

	public LoadMoneyAPIController(ILoadMoneyApi loadMoneyApi, IAuthenticationApi authenticationApi,
			LoadMoneyValidation loadMoneyValidation) {
		this.loadMoneyApi = loadMoneyApi;
		this.authenticationApi = authenticationApi;
		this.loadMoneyValidation = loadMoneyValidation;
	}

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/Voucher/Process", produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<LoadMoneyResponse> userAddCurrency(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody LoadMoneyRequest dto,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, ModelMap model) {
		String sessionId = dto.getSessionId();
		LoadMoneyResponse resp = new LoadMoneyResponse();
		CommonError error = loadMoneyValidation.checkLoadMoneyError(dto);
		try {
			if (error.isValid()) {
				if (sessionId != null && sessionId.length() != 0) {
					String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
					if (authority != null) {
						if ((authority.contains(Authorities.USER))&& authority.contains(Authorities.AUTHENTICATED)) {
							dto.setSessionId(sessionId);
							resp = loadMoneyApi.loadMoneyUsingVoucher(dto, role);
						} else {
							resp.setCode("F05");
							resp.setSuccess(false);
							resp.setMessage("Unauthorized User");
						}
					} else {
						resp.setCode("F03");
						resp.setSuccess(false);
						resp.setMessage("Session Expired.Please Login");
					}
				}
			} else {
				resp.setCode("F04");
				resp.setSuccess(false);
				resp.setMessage(error.getMessage());
			}
		} catch (Exception e) {
			System.out.println(e);
			resp.setCode("F04");
			resp.setSuccess(false);
			resp.setMessage(e.getMessage());
		}
		return new ResponseEntity<LoadMoneyResponse>(resp, HttpStatus.OK);
	}

	@RequestMapping(value = "/Process", method = RequestMethod.POST)
	public String processLoadMoney(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @ModelAttribute LoadMoneyRequest dto,
			HttpServletRequest request, HttpServletResponse response, ModelMap modelMap, HttpSession session) {
		String sessionId = dto.getSessionId();
		LoadMoneyError error = loadMoneyValidation.checkError(dto);
		LoadMoneyResponse loadMoneyResponse = new LoadMoneyResponse();
		try {
			if (error.isValid()) {
				if (sessionId != null && sessionId.length() != 0) {
					String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
					if (authority != null) {
						if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
							dto.setSessionId(sessionId);
							if (dto.isUseVnet()) {
								VNetRequest vNetRequest = new VNetRequest();
								vNetRequest.setSessionId(sessionId);
								vNetRequest.setReturnURL(dto.getReturnUrl());
								vNetRequest.setAmount(dto.getAmount());
								VNetResponse vNetResponse = loadMoneyApi.initiateVnetBanking(vNetRequest);
								if (vNetResponse.isSuccess()) {
									modelMap.addAttribute("vnet", vNetResponse);
									return "User/LoadMoney/VNetPay";
								}
							} else {
								loadMoneyResponse = loadMoneyApi.loadMoneyRequest(dto);
								if (loadMoneyResponse.isSuccess()) {
									modelMap.addAttribute("loadmoney", loadMoneyResponse);
									return "User/LoadMoney/Pay";
								} else {
									//modelMap.addAttribute(ModelMapKey.MESSAGE, loadMoneyResponse.getDescription());
									return "User/LoadMoney";
								}
							}
						}
					}
				}
			} else {
				modelMap.addAttribute(ModelMapKey.ERROR, error.getAmount());
			}
		} catch (Exception e) {
			e.printStackTrace();
			loadMoneyResponse.setMessage("We are sorry for inconvenience, Please try again later .");
		}
		return "redirect:/";
	}

	@RequestMapping(value = "/InitiateLoadMoney", method = RequestMethod.POST)
	public ResponseEntity<LoadMoneyResponse> initiateLoadMoney(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @ModelAttribute LoadMoneyRequest dto,
			HttpServletRequest request, HttpServletResponse response, ModelMap modelMap, HttpSession session) {
		String sessionId = dto.getSessionId();
		dto.setReturnUrl("-");
		LoadMoneyError error = loadMoneyValidation.checkError(dto);
		LoadMoneyResponse loadMoneyResponse = new LoadMoneyResponse();
		try {
			if (error.isValid()) {
				if (sessionId != null && sessionId.length() != 0) {
					String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
					if (authority != null) {
						if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
							dto.setSessionId(sessionId);
							loadMoneyResponse = loadMoneyApi.loadMoneyRequest(dto);
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			loadMoneyResponse.setMessage("We are sorry for inconvenience, Please try again later .");
		}
		return new ResponseEntity<LoadMoneyResponse>(loadMoneyResponse, HttpStatus.OK);
	}

	@RequestMapping(value = "/RedirectSDK", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<EBSRedirectResponse> redirectLoadMoneySDK(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @ModelAttribute EBSRedirectResponse redirectResponse,
			Model model) {
		EBSRedirectResponse newResponse = null;
		ResponseDTO result = null;
		try {
			String success = redirectResponse.getResponseCode();
			result = loadMoneyApi.verifyEBSTransaction(redirectResponse);
			if (result != null) {
				String code = result.getCode();
				if (code.equalsIgnoreCase("S00")) {
					redirectResponse.setSuccess(true);
					redirectResponse.setResponseCode("0");
				} else {
					redirectResponse.setSuccess(false);
					redirectResponse.setResponseCode("1");
				}
				newResponse = loadMoneyApi.processRedirectSDK(redirectResponse);
			}
		} catch (Exception e) {
			e.printStackTrace();
			newResponse.setMessage("We are sorry for inconvenience, Please try again later .");
		}
		return new ResponseEntity<EBSRedirectResponse>(newResponse, HttpStatus.OK);
	}

	@RequestMapping(value = "/Redirect", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<EBSRedirectResponse> redirectLoadMoney(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, EBSRedirectResponse redirectResponse,
			HttpServletRequest request, Model model) {
		EBSRedirectResponse newResponse = null;
		ResponseDTO result = null;
		try {
			result = loadMoneyApi.verifyEBSTransaction(redirectResponse);
			if (result != null) {
				String code = result.getCode();
				if (code.equalsIgnoreCase("S00")) {
					redirectResponse.setSuccess(true);
					redirectResponse.setResponseCode("0");
				} else {
					redirectResponse.setSuccess(false);
					redirectResponse.setResponseCode("1");
				}
				newResponse = loadMoneyApi.processRedirectSDK(redirectResponse);
			}
		} catch (Exception e) {
			e.printStackTrace();
			newResponse.setMessage("We are sorry for inconvenience, Please try again later .");
		}
		return new ResponseEntity<EBSRedirectResponse>(newResponse, HttpStatus.OK);
	}

	@RequestMapping(value = "/VRedirect", produces = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> redirectVNetLoadMoney(VRedirectResponse dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		ResponseDTO responseDTO = null;
		try {
			/*
			 * VNetStatusResponse result =
			 * loadMoneyApi.verifyVNetTransaction(dto); if(result.isSuccess()){
			 */
			responseDTO = loadMoneyApi.handleRedirectRequest(dto);
			/*
			 * }else{ dto.setPAID("N"); responseDTO =
			 * loadMoneyApi.handleRedirectRequest(dto); }
			 */
		} catch (Exception e) {
			e.printStackTrace();
			responseDTO.setMessage("We are sorry for inconvenience, Please try again later .");
		}
		return new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.OK);
	}

	@RequestMapping(value = "/refundEBS", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<List<RefundStatusDTO>> refundStatusEBS(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, HttpServletRequest request, HttpServletResponse response,
			HttpSession session) throws JSONException, ParserConfigurationException, SAXException {
		List<RefundStatusDTO> list = new ArrayList<RefundStatusDTO>();
		EBSStatusResponseDTO ebsResp = null;
		try {
			if (role.equalsIgnoreCase("User")) {
				Date currDate = new Date();
				Date oneDayBefore = new Date(currDate.getTime() - (24 * 3600000));
				String dateInString = sdf.format(currDate);
				String beforeString = sdf.format(oneDayBefore);
				RefundStatusDTO dto = new RefundStatusDTO();
				dto.setFrom(beforeString);
				dto.setTo(dateInString);
				// dto.setFrom("2017-03-10");
				// dto.setTo("2017-03-10");

				TransactionReportResponse resp = loadMoneyApi.getRefundStatus(dto);
				JSONArray jarr = resp.getJsonArray();
				for (int i = 0; i < jarr.length(); i++) {
					JSONObject jobject = jarr.getJSONObject(i);
					String transactionRefNo = JSONParserUtil.getString(jobject, "transactionRefNo");
					String ActualTx = transactionRefNo.substring(0, transactionRefNo.length() - 1);
					String status = JSONParserUtil.getString(jobject, "status");
					ebsResp = loadMoneyApi.crossCheckEBSStatus(ActualTx);
					RefundStatusDTO responseEBS = new RefundStatusDTO();
					if (ebsResp != null) {
						if (ebsResp.getErrorMessage() == null) {
							responseEBS.setCode(ebsResp.getCode());
							responseEBS.setAmount(ebsResp.getAmount());
							responseEBS.setTransactionRefNo(ebsResp.getTransactionRefNo());
							responseEBS.setStatus(ebsResp.getStatus());
							responseEBS.setDescription(ebsResp.getDescription());
							responseEBS.setCreated(ebsResp.getCreated());
							list.add(responseEBS);
						} else {
						}
					}
				}
				return new ResponseEntity<List<RefundStatusDTO>>(list, HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			ebsResp.setMessage("We are sorry for inconvenience, Please try again later .");
		}
		return new ResponseEntity<>(list, HttpStatus.OK);
	}

	@RequestMapping(value = "/{transactionRefNo}/Status", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<EBSStatusResponseDTO> checkEBSTransaction(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language,
			@PathVariable("transactionRefNo") String transactionRefNo, HttpServletRequest request,
			HttpServletResponse response, HttpSession session)
					throws JSONException, ParserConfigurationException, SAXException {
		EBSStatusResponseDTO dto = loadMoneyApi.crossCheckEBSStatus(transactionRefNo);
		return new ResponseEntity<EBSStatusResponseDTO>(dto, HttpStatus.OK);
	}

	@RequestMapping(value = "/getTrxTimeDiff", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> getTimeDiff(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody SessionDTO dto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session)
					throws JSONException, ParserConfigurationException, SAXException {
		ResponseDTO resp = null;
		try {
			if (role.equalsIgnoreCase("User") || role.equalsIgnoreCase("Merchant")) {
				resp = loadMoneyApi.getTransactionTym(dto);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setMessage("We are sorry for inconvenience, Please try again later .");
		}
		return new ResponseEntity<ResponseDTO>(resp, HttpStatus.OK);
	}

	// TODO Initiate load money using UPI
	@RequestMapping(value = "/UpiInitiateProcess", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<UpiSdkResponseDTO> UpiInitiateProcess(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody LoadMoneyRequest dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response, ModelMap modelMap, HttpSession session) {
		String sessionId = dto.getSessionId();
		dto.setReturnUrl("-");
		LoadMoneyError error = loadMoneyValidation.checkUpiError(dto);
		UpiSdkResponseDTO result = new UpiSdkResponseDTO();
		try {
			if (error.isValid()) {
				if (sessionId != null && sessionId.length() != 0) {
					String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
					if (authority != null) {
						if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
							dto.setSessionId(sessionId);
							dto.setSdk(true);
							result = loadMoneyApi.loadMoneyUIPRequest(dto);
							if(result.getCode().equals(ResponseStatus.SUCCESS.getValue())){
								result.setInfo(ConvertUtil.getSDKCredentials());
							}
						} else {
							// TODO Un-Authorize Role
							result.setSuccess(false);
							result.setCode("F00");
							result.setMessage("Unauthorised access");
							result.setStatus("FAILED");
							result.setResponse(APIUtils.getFailedJSON().toString());
						}
					} else {
						// TODO Invalid Session
						result.setSuccess(false);
						result.setCode("F00");
						result.setMessage("Invalid Session");
						result.setStatus("FAILED");
						result.setResponse(APIUtils.getFailedJSON().toString());
					}
				} else {
					// TODO Invalid Session
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Invalid Session");
					result.setStatus("FAILED");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage(error.getAmount());
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<UpiSdkResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/UpiSdkRedirect", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<UpiSdkResponseDTO> upiSuccessProcess(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody UpiMobileRedirectRequest dto,
			@RequestHeader(value = "hash", required = false) String hash,
			HttpServletRequest request, HttpServletResponse response, ModelMap modelMap, HttpSession session) {
		UpiSdkResponseDTO result = new UpiSdkResponseDTO();
		try {
			result = loadMoneyApi.handleSdkUPIRedirectRequest(dto);
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<UpiSdkResponseDTO>(result, HttpStatus.OK);
	}
	
	
	// TODO Initiate load money using RazorPay
	
	
	@RequestMapping(value = "/InitiateLoadMoneyRazorPay", method = RequestMethod.POST)
	public ResponseEntity<RazorPayResponse> initiateLoadMoneyRazorPay(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody RazorPayRequest dto,
			HttpServletRequest request, HttpServletResponse response, ModelMap modelMap, HttpSession session) {
		RazorPayResponse result = new RazorPayResponse();
		try {
			String sessionId = dto.getSessionId();
			if (sessionId != null && sessionId.length() != 0) {
				if (version.equalsIgnoreCase("v1")) {
					if (role.equalsIgnoreCase("User")) {
						if (device.equalsIgnoreCase("Android") || device.equalsIgnoreCase("IOS")) {
							dto.setSessionId(sessionId);
							result = loadMoneyApi.initiateRazorPayLoadMoney(dto);
						} else {
							result.setSuccess(false);
							result.setMessage("Unauthorized Device");
						}
					} else {
						result.setSuccess(false);
						result.setMessage("Unauthorized Role");
					}
				} else {
					result.setSuccess(false);
					result.setMessage("Invalid Version");
				}
			} else {
				result.setSuccess(false);
				result.setMessage("Unauthorised access");
				result.setStatus("FAILED");
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
		}
		return new ResponseEntity<RazorPayResponse>(result, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/RedirectLoadMoneyRazorPay", method = RequestMethod.POST)
	public ResponseEntity<RazorPayResponse> redirectLoadMoneyRazorPay(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody RazorPayRequest dto,
			HttpServletRequest request, HttpServletResponse response, ModelMap modelMap, HttpSession session) {
		RazorPayResponse result = new RazorPayResponse();
		try {
			String sessionId = dto.getSessionId();
			if (sessionId != null && sessionId.length() != 0) {
				if (version.equalsIgnoreCase("v1")) {
					if (role.equalsIgnoreCase("User")) {
						if (device.equalsIgnoreCase("Android") || device.equalsIgnoreCase("IOS")) {
							dto.setSessionId(sessionId);
							RazorPayResponse resp = loadMoneyApi.verifyTransactionAtRazorPay(dto);
							if(resp.isSuccess() && resp.isCaptured()){
							result = loadMoneyApi.successRazorPayLoadMoney(dto,resp);
							}
						} else {
							result.setSuccess(false);
							result.setMessage("Unauthorized Device");
						}
					} else {
						result.setSuccess(false);
						result.setMessage("Unauthorized Role");
					}
				} else {
					result.setSuccess(false);
					result.setMessage("Invalid Version");
				}
			} else {
				result.setSuccess(false);
				result.setMessage("Unauthorised access");
				result.setStatus("FAILED");
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
		}
		return new ResponseEntity<RazorPayResponse>(result, HttpStatus.OK);
	}

}
