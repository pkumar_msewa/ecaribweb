package com.payqwikweb.controller.mobile.api;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.payqwikweb.app.api.IAgentApi;
import com.payqwikweb.app.api.IAgentSendMoneyApi;
import com.payqwikweb.app.api.ITransactionApi;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.request.EditProfileRequest;
import com.payqwikweb.app.model.request.LoginRequest;
import com.payqwikweb.app.model.request.MobileOTPRequest;
import com.payqwikweb.app.model.request.ReceiptsRequest;
import com.payqwikweb.app.model.request.RegistrationRequest;
import com.payqwikweb.app.model.request.ResendMobileOTPRequest;
import com.payqwikweb.app.model.request.SendMoneyMobileRequest;
import com.payqwikweb.app.model.request.TransactionRequest;
import com.payqwikweb.app.model.request.UserDetailsRequest;
import com.payqwikweb.app.model.response.EditProfileResponse;
import com.payqwikweb.app.model.response.LoginResponse;
import com.payqwikweb.app.model.response.MobileOTPResponse;
import com.payqwikweb.app.model.response.ReceiptsResponse;
import com.payqwikweb.app.model.response.RegistrationResponse;
import com.payqwikweb.app.model.response.ResendMobileOTPResponse;
import com.payqwikweb.app.model.response.SendMoneyMobileResponse;
import com.payqwikweb.app.model.response.UserDetailsResponse;
import com.payqwikweb.util.APIUtils;
import com.thirdparty.model.ResponseDTO;

@Controller
@RequestMapping("/Api/{version}/{role}/{device}/{language}/Agent")
public class AgentAPIController implements MessageSourceAware {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private MessageSource messageSource;

	private final IAgentApi agentApi;
	private final ITransactionApi transactionApi;
	private final IAgentSendMoneyApi sendAgentMoneyApi;

	public AgentAPIController(IAgentApi agentApi,ITransactionApi transactionApi,IAgentSendMoneyApi sendAgentMoneyApi) {
		this.agentApi = agentApi;
		this.transactionApi=transactionApi;
		this.sendAgentMoneyApi=sendAgentMoneyApi;
	}

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	@RequestMapping(value = "/WebProcess", method = RequestMethod.POST)
	ResponseEntity<LoginResponse> processLogin1(RedirectAttributes model,@ModelAttribute("login") LoginRequest dto, HttpServletRequest request, HttpSession session,
			HttpServletResponse response) {
		System.err.println("inside the login controller");
		LoginResponse result = new LoginResponse();
		try{
	    dto.setIpAddress(request.getRemoteAddr());
		result = agentApi.login(dto, Role.AGENT);
		if (result.getCode().equals("S00")) {
		session.setAttribute("sessionId",result.getSessionId());
		session.setAttribute("balance", result.getBalance());
		session.setAttribute("firstName", result.getFirstName());
		session.setAttribute("userDetails", result);
		session.setAttribute("username", result.getUsername());
		session.setAttribute("emailId", result.getEmail());
		 }
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<LoginResponse>(result, HttpStatus.OK);
	}
	@RequestMapping(value = "Login/Process", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<LoginResponse> login(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody LoginRequest dto,
			@RequestHeader(value = "hash", required = false) String hash) {
		LoginResponse result = new LoginResponse();
		try{
		if (role.equalsIgnoreCase(Role.AGENT.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				result = agentApi.login(dto, Role.AGENT);
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unknown device");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("Unauthorised access");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		 }
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<LoginResponse>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "Registration/Process", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<RegistrationResponse> processRegistration(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody RegistrationRequest dto,
			@RequestHeader(value = "hash", required = false) String hash) {
		RegistrationResponse result = new RegistrationResponse();
		try{
		if (role.equalsIgnoreCase(Role.AGENT.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				result = agentApi.SaveUserToAgent(dto, Role.AGENT);
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unknown device");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("Unauthorised access");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		  }
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}	 
		return new ResponseEntity<RegistrationResponse>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/MobileOTP", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<MobileOTPResponse> mobileOTP(
			@PathVariable(value = "version") String version, @PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody MobileOTPRequest dto,
			@RequestHeader(value = "hash", required = false) String hash) {
		MobileOTPResponse result = new MobileOTPResponse();
		try{
		if (role.equalsIgnoreCase(Role.AGENT.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				result = agentApi.SaveUsertoAgentMobileOTP(dto,Role.AGENT);
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unknown device");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("Unauthorised access");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		  }
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}	
		return new ResponseEntity<MobileOTPResponse>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/ResendMobileOTP", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResendMobileOTPResponse> resendMobileOTP(
			@PathVariable(value = "version") String version, @PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody ResendMobileOTPRequest dto,
			@RequestHeader(value = "hash", required = false) String hash) {
		ResendMobileOTPResponse result = new ResendMobileOTPResponse();
		try{
		if (role.equalsIgnoreCase(Role.AGENT.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				result = agentApi.resendUsertoAgentMobileOTP(dto,Role.AGENT);
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unknown device");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("Unauthorised access");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		 }
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<ResendMobileOTPResponse>(result, HttpStatus.OK);
	}
	

	@RequestMapping(value = "/Receipt", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ReceiptsResponse> receipt(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody ReceiptsRequest dto,
			@RequestHeader(value = "hash", required = false) String hash) {
		ReceiptsResponse result = new ReceiptsResponse();
		try{
		if (role.equalsIgnoreCase(Role.AGENT.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				result = agentApi.getReceipts(dto, Role.AGENT);
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unknown device");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("Unauthorised access");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		 }
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}	
		return new ResponseEntity<ReceiptsResponse>(result, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/EditProfile", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<EditProfileResponse> editProfile(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody EditProfileRequest dto,
			@RequestHeader(value = "hash", required = false) String hash) {
		EditProfileResponse result = new EditProfileResponse();
		try{
		if (role.equalsIgnoreCase(Role.AGENT.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				result = agentApi.editProfile(dto, Role.AGENT);
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unknown device");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("Unauthorised access");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		 }
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<EditProfileResponse>(result, HttpStatus.OK);
	}
	@RequestMapping(value = "/SendMoney/Mobile", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<SendMoneyMobileResponse> sendMoneyMobile(@PathVariable(value = "version") String version, @PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SendMoneyMobileRequest dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response) {
			SendMoneyMobileResponse result = new SendMoneyMobileResponse();
			try{
			if (role.equalsIgnoreCase(Role.AGENT.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
					TransactionRequest newRequest = new TransactionRequest();
					newRequest.setSessionId(dto.getSessionId());
					newRequest.setAmount(Double.parseDouble(dto.getAmount()));
					newRequest.setTransactionRefNo("D");
					ResponseDTO responseDTO1 = transactionApi.validateAgentTransaction(newRequest);
					if (responseDTO1.isValid()) {
						result = sendAgentMoneyApi.sendMoneyMobileRequest(dto,Role.AGENT);
					} else {
						result.setCode("F00");
						result.setMessage(responseDTO1.getMessage());
					}
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Unknown device");
					result.setStatus("FAILED");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unauthorised access");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			 }
			} catch (Exception e) {
				e.printStackTrace();
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("We are sorry for inconvenience, Please try again later .");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
			return new ResponseEntity<SendMoneyMobileResponse>(result, HttpStatus.OK);
	}
	@RequestMapping(value = "/GetUserDetails", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<UserDetailsResponse> getUserDetails(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody UserDetailsRequest dto,
			@RequestHeader(value = "hash", required = false) String hash) {
		UserDetailsResponse result = new UserDetailsResponse();
		try{
		if (role.equalsIgnoreCase(Role.AGENT.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				result = agentApi.getAgentDetails(dto, Role.AGENT);
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unknown device");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
			}else {
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("Unauthorised access");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		 }
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<UserDetailsResponse>(result, HttpStatus.OK);
	}
	
	
	
	
}
