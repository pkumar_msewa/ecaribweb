package com.payqwikweb.controller.mobile.api.thirdparty;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.payqwikweb.api.IAuthenticationApi;
import com.payqwikweb.app.api.IMeraEventsApi;
import com.payqwikweb.app.api.ITransactionApi;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.ResponseStatus;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.request.MeraEventDetailsRequest;
import com.payqwikweb.app.model.request.MeraEventsBookingRequest;
import com.payqwikweb.app.model.request.MeraEventsCommonRequest;
import com.payqwikweb.app.model.request.MeraEventsListRequest;
import com.payqwikweb.app.model.request.MeraEventsTicketDetailsRequest;
import com.payqwikweb.app.model.request.TransactionRequest;
import com.payqwikweb.app.model.response.MeraEventCategoryListResponse;
import com.payqwikweb.app.model.response.MeraEventCityListResponse;
import com.payqwikweb.app.model.response.MeraEventGalleryDetailsResponse;
import com.payqwikweb.app.model.response.MeraEventTicketCalculationResponse;
import com.payqwikweb.app.model.response.MeraEventsAttendeeFormRequest;
import com.payqwikweb.app.model.response.MeraEventsListResponse;
import com.payqwikweb.app.model.response.MeraEventsResponse;
import com.payqwikweb.app.model.response.MeraEventsTicketDetailsResponse;
import com.payqwikweb.util.APIUtils;
import com.payqwikweb.util.Authorities;
import com.payqwikweb.util.LogCat;
import com.thirdparty.model.ResponseDTO;

import static com.payqwikweb.controller.web.thirdparty.MeraEventsController.calculateNetAmount;

@Controller
@RequestMapping("/Api/{version}/{role}/{device}/{language}/MeraEvents")
public class MeraEventsController implements MessageSourceAware {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private MessageSource messageSource;
	
	private final IAuthenticationApi authenticationApi;
	private final IMeraEventsApi meraEventsApi;
	private final ITransactionApi transactionApi;
	public MeraEventsController(IAuthenticationApi authenticationApi,IMeraEventsApi meraEventsApi,
			ITransactionApi transactionApi) {
		this.authenticationApi = authenticationApi;
		this.meraEventsApi = meraEventsApi;
		this.transactionApi = transactionApi;
	}
	
	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	@RequestMapping(value = "/AuthCode", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<MeraEventsResponse> getAccessToken(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody MeraEventsCommonRequest dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		MeraEventsResponse resp = new MeraEventsResponse();
		String sessionId = dto.getSessionId();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				if (dto.getSessionId() != null || dto.getSessionId().length() > 0) {
					String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
					System.err.println("Authority :: " + authority);
					if (authority != null) {
						if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
							dto.setSessionId(sessionId);
							dto.setClientId(APIUtils.ClientID);
							//resp = meraEventsApi.getAuthorizationCode(dto);
							session.setAttribute("authenticationCode", resp.getResponse());
							if (resp.getCode().equalsIgnoreCase("S00")) {
								dto.setAuthorizationCode((String) session.getAttribute("authenticationCode"));
								dto.setClientSecret(APIUtils.ClientSecret);
								//resp = meraEventsApi.getAccessToken(dto);
								if (resp.getResponse().length() > 0) {
									dto.setAccess_token(resp.getResponse());
									//resp = meraEventsApi.saveAccessToken(dto);
								} else {
									resp.setCode("F00");
									resp.setSuccess(false);
									resp.setStatus("Failure");
									resp.setMessage("Unable to generate access Token");
									resp.setResponse(resp.getResponse());
								}
								if (resp.getCode().equalsIgnoreCase("S00")) {
									session.setAttribute("accessToken", resp.getResponse());
									resp.setCode("S00");
									resp.setSuccess(true);
									resp.setStatus("Success");
									resp.setMessage("Access token generated");
									resp.setResponse(resp.getResponse());
								} else {
									resp.setSuccess(false);
									resp.setCode("F00");
									resp.setMessage("Access Denied");
									resp.setStatus("Failure");
									resp.setResponse(APIUtils.getFailedJSON().toString());
								}
							} else {
								resp.setSuccess(false);
								resp.setCode("F00");
								resp.setMessage("Client Authentication Failed");
								resp.setStatus("Failure");
								resp.setResponse(APIUtils.getFailedJSON().toString());
							}
						} else {
							resp.setSuccess(false);
							resp.setCode("F00");
							resp.setMessage("User Authentication Failed");
							resp.setStatus("Failure");
							resp.setResponse(APIUtils.getFailedJSON().toString());
						}
					} else {
						resp.setSuccess(false);
						resp.setCode("F00");
						resp.setMessage("User Authority null");
						resp.setStatus("Failure");
						resp.setResponse(APIUtils.getFailedJSON().toString());
					}
				} else {
					resp.setSuccess(false);
					resp.setCode("F00");
					resp.setMessage("Session null");
					resp.setStatus("Failure");
					resp.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Unknown device");
				resp.setStatus("Failure");
				resp.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Un-authorized Role");
			resp.setStatus("Failure");
			resp.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<MeraEventsResponse>(resp, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/CityList", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<MeraEventsResponse> getCities(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody MeraEventsCommonRequest dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		MeraEventsResponse result = new MeraEventsResponse();
		String sessionId = dto.getSessionId();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				if (dto.getSessionId() != null || dto.getSessionId().length() > 0) {
					String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
					System.err.println("Authority :: " + authority);
					if (authority != null) {
						if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
							dto.setSessionId(sessionId);
							dto.setCountryId(14);
							MeraEventCityListResponse resp = meraEventsApi.getCities(dto);
							JSONArray array = (JSONArray) resp.getJsonArray();
							List<MeraEventCityListResponse> cityList = new ArrayList<>();
							if (resp.getCode().equalsIgnoreCase("S00")) {
								try {
									int count = 0;
									for (int i = 0; i < array.length(); i++) {
										MeraEventCityListResponse list = new MeraEventCityListResponse();
										JSONObject jsonObject = array.getJSONObject(i);
										list.setCityID(jsonObject.getInt("id"));
										list.setCityName(jsonObject.getString("name"));
										list.setOrder(jsonObject.getInt("order"));
										list.setCountryId(jsonObject.getInt("countryId"));
										count++;
										LogCat.print("" + count);
										cityList.add(list);
									}
									result.setCode("S00");
									result.setMessage("City List : ");
									result.setSuccess(true);
									result.setStatus("Success");
									result.setDetails(cityList);
								} catch (Exception e) {
									e.printStackTrace();
								}
							} else {
								result.setSuccess(false);
								result.setCode("F00");
								result.setMessage("Client Authentication Failed");
								result.setStatus("Failure");
								result.setResponse(APIUtils.getFailedJSON().toString());
							}
						} else {
							result.setSuccess(false);
							result.setCode("F00");
							result.setMessage("User Authentication Failed");
							result.setStatus("Failure");
							result.setResponse(APIUtils.getFailedJSON().toString());
						}
					} else {
						result.setSuccess(false);
						result.setCode("F00");
						result.setMessage("User Authority null");
						result.setStatus("Failure");
						result.setResponse(APIUtils.getFailedJSON().toString());
					}
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Session null");
					result.setStatus("Failure");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unknown device");
				result.setStatus("Failure");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("Un-authorized Role");
			result.setStatus("Failure");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<MeraEventsResponse>(result, HttpStatus.OK);
	}	
	
	@RequestMapping(value = "/CategoryList", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<MeraEventsResponse> categoryList(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody MeraEventsCommonRequest dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		MeraEventsResponse result = new MeraEventsResponse();
		String sessionId = dto.getSessionId();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				if (dto.getSessionId() != null || dto.getSessionId().length() > 0) {
					String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
					System.err.println("Authority :: " + authority);
					if (authority != null) {
						if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
							dto.setSessionId(sessionId);
							MeraEventCategoryListResponse resp = meraEventsApi.getEventCategory(dto);
							JSONArray array = (JSONArray) resp.getJsonArray();
							List<MeraEventCategoryListResponse> categoryList = new ArrayList<>();
							if (resp.getCode().equalsIgnoreCase("S00")) {
								try {
									int count = 0;
									for (int i = 0; i < array.length(); i++) {
										MeraEventCategoryListResponse list = new MeraEventCategoryListResponse();
										JSONObject jsonObject = array.getJSONObject(i);
										list.setCategoryID(jsonObject.getInt("id"));
										list.setCategoryName(jsonObject.getString("name"));
										list.setThemeColor(jsonObject.getString("themeColor"));
										list.setTicketSetting(jsonObject.getString("ticketSetting"));
										list.setDefaultBannerPath(jsonObject.getString("defaultBannerPath"));
										list.setDefaultThumbnailPath(jsonObject.getString("defaultThumbnailPath"));
										list.setValue(jsonObject.getString("value"));
										// list.setIconPath(jsonObject.getString("iconPath"));
										count++;
										LogCat.print("" + count);
										categoryList.add(list);
									}
									result.setCode("S00");
									result.setMessage("Category List : ");
									result.setSuccess(true);
									result.setStatus("Success");
									result.setDetails(categoryList);
								} catch (Exception e) {
									e.printStackTrace();
								}
							} else {
								result.setSuccess(false);
								result.setCode("F00");
								result.setMessage("Client Authentication Failed");
								result.setStatus("Failure");
								result.setResponse(APIUtils.getFailedJSON().toString());
							}
						} else {
							result.setSuccess(false);
							result.setCode("F00");
							result.setMessage("User Authentication Failed");
							result.setStatus("Failure");
							result.setResponse(APIUtils.getFailedJSON().toString());
						}
					} else {
						result.setSuccess(false);
						result.setCode("F00");
						result.setMessage("User Authority null");
						result.setStatus("Failure");
						result.setResponse(APIUtils.getFailedJSON().toString());
					}
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Session null");
					result.setStatus("Failure");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unknown device");
				result.setStatus("Failure");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("Un-authorized Role");
			result.setStatus("Failure");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<MeraEventsResponse>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/EventList", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<MeraEventsListResponse> getEventList(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody MeraEventsListRequest dto,
			HttpServletRequest request,HttpServletResponse response, HttpSession session) {
		MeraEventsListResponse result = new MeraEventsListResponse();
		String sessionId = dto.getSessionId();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				if (dto.getSessionId() != null || dto.getSessionId().length() > 0) {
					String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
					System.err.println("Authority :: " + authority);
					if (authority != null) {
						if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
							dto.setSessionId(sessionId);
							dto.setCountryId(14);
							// dto.setAccess_token((String)session.getAttribute("accessToken"));
							result = meraEventsApi.getEventList(dto);
							List<MeraEventsListResponse> eventList = new ArrayList<>();
							JSONArray array = (JSONArray) result.getJsonArray();
							if (result.getCode().equalsIgnoreCase("S00")) {
								try {
									if (array != null) {
										int count = 0;
										for (int i = 0; i < array.length(); i++) {
											MeraEventsListResponse list = new MeraEventsListResponse();
											JSONObject jsonObject = array.getJSONObject(i);
											list.setEventId(jsonObject.getString("id"));
											list.setTitle(jsonObject.getString("title"));
											list.setThumbImage(jsonObject.getString("thumbImage"));
											list.setBannerImage(jsonObject.getString("bannerImage"));
											list.setTimeZone(jsonObject.getString("timeZone"));
											/*
											 * DateFormat formatter = new
											 * SimpleDateFormat(
											 * "dd-MM-yyyy HH:mm"); long
											 * milliSeconds =
											 * Long.parseLong(jsonObject.
											 * getString("startDate")); Calendar
											 * calendar =
											 * Calendar.getInstance();
											 * calendar.setTimeInMillis(
											 * milliSeconds);
											 * list.setStartDate(formatter.
											 * format(calendar.getTime()));
											 */
											list.setStartDate(jsonObject.getString("startDate"));
											list.setEndDate(jsonObject.getString("endDate"));
											list.setVenueName(jsonObject.getString("venueName"));
											list.setEventUrl(jsonObject.getString("eventUrl"));
											list.setCategoryName(jsonObject.getString("categoryName"));
											list.setSubCategoryName(jsonObject.getString("subCategoryName"));
											list.setDefaultBannerImage(jsonObject.getString("defaultBannerImage"));
											list.setDefaultThumbImage(jsonObject.getString("defaultThumbImage"));
											list.setRegistrationType(jsonObject.getString("registrationType"));
											list.setEventMode(jsonObject.getString("eventMode"));
											list.setCityName(jsonObject.getString("cityName"));
											list.setCountryName(jsonObject.getString("countryName"));
											list.setBookmarked(
													Boolean.parseBoolean(jsonObject.getString("bookMarked")));
											list.setLatitude(jsonObject.getString("latitude"));
											list.setLongitude(jsonObject.getString("longitude"));
											list.setBooknowButtonValue(jsonObject.getString("booknowButtonValue"));
											// list.setMinTicketPrice(jsonObject.getInt("minTicketPrice"));
											// list.setMaxTicketPrice(jsonObject.getInt("maxTicketPrice"));
											list.setLimitSingleTicketType(jsonObject.getInt("limitSingleTicketType"));
											list.setIsMobileApiVisible(jsonObject.getInt("isMobileApiVisible"));
											list.setIsStandardApiVisible(jsonObject.getInt("isStandardApiVisible"));
											;
											count++;
											LogCat.print("" + count);
											eventList.add(list);
										}
									}
									result.setCode("S00");
									result.setSuccess(true);
									result.setStatus(ResponseStatus.SUCCESS);
									result.setMessage("Event List : ");
									result.setJsonArray(eventList);
								} catch (Exception e) {
									e.printStackTrace();
								}
							} else {
								result.setSuccess(false);
								result.setCode("F00");
								result.setMessage("Client Authentication Failed");
								result.setStatus(ResponseStatus.FAILURE);
								result.setResponse(APIUtils.getFailedJSON().toString());
							}
						} else {
							result.setSuccess(false);
							result.setCode("F00");
							result.setMessage("User Authentication Failed");
							result.setStatus(ResponseStatus.FAILURE);
							result.setResponse(APIUtils.getFailedJSON().toString());
						}
					} else {
						result.setSuccess(false);
						result.setCode("F00");
						result.setMessage("User Authority null");
						result.setStatus(ResponseStatus.FAILURE);
						result.setResponse(APIUtils.getFailedJSON().toString());
					}
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Session null");
					result.setStatus(ResponseStatus.FAILURE);
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unknown device");
				result.setStatus(ResponseStatus.FAILURE);
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("Un-authorized Role");
			result.setStatus(ResponseStatus.FAILURE);
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<MeraEventsListResponse>(result, HttpStatus.OK);
	}
	
	MeraEventsListResponse eventDetails = new MeraEventsListResponse();

	@RequestMapping(value = "/EventDetails", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<String> getEventDetails(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody MeraEventsCommonRequest dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		String sessionId = dto.getSessionId();
		MeraEventsListResponse result = new MeraEventsListResponse();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				if (dto.getSessionId() != null || dto.getSessionId().length() > 0) {
					String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
					System.err.println("Authority :: " + authority);
					if (authority != null) {
						if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
							dto.setSessionId(sessionId);
							// dto.setAccess_token((String)session.getAttribute("accessToken"));
							eventDetails = meraEventsApi.getEventDetails(dto);
							if (eventDetails.getCode().equalsIgnoreCase("S00")) {
								try {
									// session.setAttribute("eventDetails",
									// eventDetails);
									/*
									 * JSONObject jsonObject = new
									 * JSONObject(resp.getResponse());
									 * resp.setEventId(jsonObject.getJSONObject(
									 * "response").getJSONObject("details").
									 * getInt("id"));
									 * resp.setOwnerId(jsonObject.getJSONObject(
									 * "response").getJSONObject("details").
									 * getInt("ownerId"));
									 * resp.setStartDate(jsonObject.
									 * getJSONObject("response").getJSONObject(
									 * "details").getString("startDate"));
									 * resp.setEndDate(jsonObject.getJSONObject(
									 * "response").getJSONObject("details").
									 * getString("endDate"));
									 * resp.setTitle(jsonObject.getJSONObject(
									 * "response").getJSONObject("details").
									 * getString("title"));
									 * resp.setDetails(jsonObject.getJSONObject(
									 * "response").getJSONObject("details").
									 * getString("description"));
									 * resp.setEventStatus(jsonObject.
									 * getJSONObject("response").getJSONObject(
									 * "details").getInt("status"));
									 * resp.setCategoryName(jsonObject.
									 * getJSONObject("response").getJSONObject(
									 * "details").getString("categoryName"));
									 * resp.setSubCategoryName(jsonObject.
									 * getJSONObject("response").getJSONObject(
									 * "details").getString("subCategoryName"));
									 * resp.setEventUrl(jsonObject.getJSONObject
									 * ("response").getJSONObject("details").
									 * getString("eventUrl"));
									 * resp.setUrl(jsonObject.getJSONObject(
									 * "response").getJSONObject("details").
									 * getString("url"));
									 * resp.setBannerImage(jsonObject.
									 * getJSONObject("response").getJSONObject(
									 * "details").getString("bannerPath"));
									 * resp.setThumbImage(jsonObject.
									 * getJSONObject("response").getJSONObject(
									 * "details").getString("thumbnailPath"));
									 * resp.setBooknowButtonValue(jsonObject.
									 * getJSONObject("response").getJSONObject(
									 * "details").getJSONObject("eventDetails").
									 * getString("bookButtonValue"));
									 * resp.setCountryName(jsonObject.
									 * getJSONObject("response").getJSONObject(
									 * "details").getJSONObject("location").
									 * getString("countryName"));
									 * resp.setStateName(jsonObject.
									 * getJSONObject("response").getJSONObject(
									 * "details").getJSONObject("location").
									 * getString("stateName"));
									 * resp.setCityName(jsonObject.getJSONObject
									 * ("response").getJSONObject("details").
									 * getJSONObject("location").getString(
									 * "cityName"));
									 * resp.setVenueName(jsonObject.
									 * getJSONObject("response").getJSONObject(
									 * "details").getJSONObject("location").
									 * getString("venueName"));
									 * resp.setAddress1(jsonObject.getJSONObject
									 * ("response").getJSONObject("details").
									 * getJSONObject("location").getString(
									 * "address1"));
									 * resp.setAddress2(jsonObject.getJSONObject
									 * ("response").getJSONObject("details").
									 * getJSONObject("location").getString(
									 * "address2")); //
									 * resp.setPincode(jsonObject.getJSONObject(
									 * "response").getJSONObject("details").
									 * getJSONObject("location").getInt(
									 * "pincode"));
									 * resp.setTimeZone(jsonObject.getJSONObject
									 * ("response").getJSONObject("details").
									 * getJSONObject("location").getString(
									 * "timeZone"));
									 * resp.setTimeZoneName(jsonObject.
									 * getJSONObject("response").getJSONObject(
									 * "details").getJSONObject("location").
									 * getString("timeZoneName"));
									 */
									result.setCode("S00");
									result.setMessage("Event Details : ");
									result.setSuccess(true);
									result.setStatus(ResponseStatus.SUCCESS);
									result.setResponse(eventDetails.getResponse());
									return new ResponseEntity<String>(result.getResponse(), HttpStatus.OK);
								} catch (Exception e) {
									e.printStackTrace();
								}
							} else {
								result.setSuccess(false);
								result.setCode("F00");
								result.setMessage("Please try again later..");
								result.setStatus(ResponseStatus.FAILURE);
								result.setResponse(APIUtils.getFailedJSON().toString());
							}
						} else {
							result.setSuccess(false);
							result.setCode("F00");
							result.setMessage("User Authentication Failed");
							result.setStatus(ResponseStatus.FAILURE);
							result.setResponse(APIUtils.getFailedJSON().toString());
						}
					} else {
						result.setSuccess(false);
						result.setCode("F00");
						result.setMessage("User Authentication Failed");
						result.setStatus(ResponseStatus.FAILURE);
						result.setResponse(APIUtils.getFailedJSON().toString());
					}
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("User Authority null");
					result.setStatus(ResponseStatus.FAILURE);
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unknown device");
				result.setStatus(ResponseStatus.FAILURE);
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("Un-authorized Role");
			result.setStatus(ResponseStatus.FAILURE);
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<String>(result.getResponse(), HttpStatus.OK);
	}
	
	

	@RequestMapping(value = "/GalleryDetails", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<MeraEventsResponse> getGalleryDetails(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody MeraEventsTicketDetailsRequest dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		String sessionId = dto.getSessionId();
		MeraEventsResponse result = new MeraEventsResponse();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				if (dto.getSessionId() != null || dto.getSessionId().length() > 0) {
					String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
					System.err.println("Authority :: " + authority);
					if (authority != null) {
						if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
							dto.setSessionId(sessionId);
							// dto.setAccess_token((String)session.getAttribute("accessToken"));
							//MeraEventGalleryDetailsResponse resp = meraEventsApi.getGalleryDetails(dto);
							//JSONArray array = (JSONArray) resp.getJsonArray();
							List<MeraEventGalleryDetailsResponse> galleryList = new ArrayList<>();
							//if (resp.getCode().equalsIgnoreCase("S00")) {/*
								try {
									int count = 0;
									//for (int i = 0; i < array.length(); i++) {
										MeraEventGalleryDetailsResponse list = new MeraEventGalleryDetailsResponse();
									//	JSONObject jsonObject = array.getJSONObject(i);
										/*list.setGalleryId(jsonObject.getInt("id"));
										list.setEventId(jsonObject.getInt("eventId"));
										list.setOrder(jsonObject.getInt("order"));
										list.setImageId(jsonObject.getInt("imageId"));
										list.setThumbnailId(jsonObject.getInt("thumbnailId"));
										list.setImagePath(jsonObject.getString("imagePath"));
										list.setThumbnailPath(jsonObject.getString("thumbnailPath"));*/
										count++;
										LogCat.print("" + count);
										galleryList.add(list);
								//	}
									//result.setCode("S00");
									result.setMessage("Gallery List : ");
									result.setSuccess(true);
									result.setStatus("Success");
									result.setDetails(galleryList);
								} catch (Exception e) {
									e.printStackTrace();
								}
						//	*/} else {
								result.setSuccess(false);
								result.setCode("F00");
								result.setMessage("Sorry, No records found");
								result.setStatus("Failure");
								result.setResponse(APIUtils.getFailedJSON().toString());
							//}
						} else {
							result.setSuccess(false);
							result.setCode("F00");
							result.setMessage("User Authentication Failed");
							result.setStatus("Failure");
							result.setResponse(APIUtils.getFailedJSON().toString());
						}
					} else {
						result.setSuccess(false);
						result.setCode("F00");
						result.setMessage("User Authority null");
						result.setStatus("Failure");
						result.setResponse(APIUtils.getFailedJSON().toString());
					}
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Session null");
					result.setStatus("Failure");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unknown device");
				result.setStatus("Failure");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("Un-authorized Role");
			result.setStatus("Failure");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<MeraEventsResponse>(result, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/TicketDetails", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<MeraEventsResponse> getTicketDetails(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody MeraEventsTicketDetailsRequest dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		MeraEventsResponse result = new MeraEventsResponse();
		String sessionId = dto.getSessionId();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				if (dto.getSessionId() != null || dto.getSessionId().length() > 0) {
					String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
					System.err.println("Authority :: " + authority);
					if (authority != null) {
						if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
							dto.setSessionId(sessionId);
							// dto.setAccess_token((String)session.getAttribute("accessToken"));
							MeraEventsTicketDetailsResponse resp = meraEventsApi.getEventTicketDetails(dto);
							session.setAttribute("eventDetails", resp);
							JSONArray array = (JSONArray) resp.getJsonArray();
							System.err.println("$$$$$$$$$$" + array);
							List<MeraEventsTicketDetailsResponse> ticketList = new ArrayList<>();
							if (resp.getCode().equalsIgnoreCase("S00")) {
								try {
									int count = 0;
									for (int i = 0; i < array.length(); i++) {
										MeraEventsTicketDetailsResponse list = new MeraEventsTicketDetailsResponse();
										JSONObject jsonObject = array.getJSONObject(i);
										list.setTicketId(jsonObject.getInt("id"));
										list.setTicketName(jsonObject.getString("name"));
										list.setDescription(jsonObject.getString("description"));
										list.setEventId(jsonObject.getInt("eventId"));
										list.setTicketPrice(jsonObject.getDouble("price"));
										list.setCurrencyCode(jsonObject.getString("currencyCode"));
										list.setQuantity(jsonObject.getInt("quantity"));
										list.setMinOrderQuantity(jsonObject.getInt("minOrderQuantity"));
										list.setMaxOrderQuantity(jsonObject.getInt("maxOrderQuantity"));
										list.setStartDate(jsonObject.getString("startDate"));
										list.setEndDate(jsonObject.getString("endDate"));
										list.setTicketStatus(jsonObject.getInt("status"));
										list.setTotalSoldTickets(jsonObject.getInt("totalSoldTickets"));
										list.setTicketType(jsonObject.getString("type"));
										list.setDisplayStatus(jsonObject.getInt("displayStatus"));
										list.setSoldout(jsonObject.getInt("soldout"));
										list.setOrder(jsonObject.getInt("order"));
										// list.setTotalSoldTickets(jsonObject.getInt("total"));
										count++;
										LogCat.print("" + count);
										ticketList.add(list);
									}
									result.setCode("S00");
									result.setMessage("Ticket Details : ");
									result.setSuccess(true);
									result.setStatus("Success");
									result.setDetails(ticketList);
								} catch (Exception e) {
									e.printStackTrace();
								}
							} else {
								result.setSuccess(false);
								result.setCode("F00");
								result.setMessage("Client Authentication Failed");
								result.setStatus("Failure");
								result.setResponse(APIUtils.getFailedJSON().toString());
							}
						} else {
							result.setSuccess(false);
							result.setCode("F00");
							result.setMessage("User Authentication Failed");
							result.setStatus("Failure");
							result.setResponse(APIUtils.getFailedJSON().toString());
						}
					} else {
						result.setSuccess(false);
						result.setCode("F00");
						result.setMessage("User Authority null");
						result.setStatus("Failure");
						result.setResponse(APIUtils.getFailedJSON().toString());
					}
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Session null");
					result.setStatus("Failure");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unknown device");
				result.setStatus("Failure");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("Un-authorized Role");
			result.setStatus("Failure");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<MeraEventsResponse>(result, HttpStatus.OK);
	}
	
	MeraEventTicketCalculationResponse totalAmountResp = new MeraEventTicketCalculationResponse();
	List<MeraEventTicketCalculationResponse> ticketList = new ArrayList<>();

	@RequestMapping(value = "/CalculateTotalAmount", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<String> calculateAmount(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody MeraEventsBookingRequest dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		MeraEventsResponse result = new MeraEventsResponse();
		String sessionId = dto.getSessionId();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				if (dto.getSessionId() != null || dto.getSessionId().length() > 0) {
					String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
					System.err.println("Authority :: " + authority);
					if (authority != null) {
						if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
							dto.setSessionId(sessionId);
							// dto.setAccess_token((String)session.getAttribute("accessToken"));
							dto.setCountryId(14);
							totalAmountResp = meraEventsApi.calculateTotalAmount(dto);
							if (totalAmountResp.getCode().equalsIgnoreCase("S00")) {
								try {
									JSONObject json = new JSONObject(totalAmountResp.getResponse());
									session.setAttribute("totalTicketAmount", json.getJSONObject("response")
											.getJSONObject("calculationDetails").getDouble("totalTicketAmount"));
									session.setAttribute("noOfAttendees", json.getJSONObject("response")
											.getJSONObject("calculationDetails").getInt("totalTicketQuantity"));
									System.err.println("%%%%" + json);
									totalAmountResp.setEventId(dto.getEventId());
									totalAmountResp.setTotalTicketAmount(json.getJSONObject("response")
											.getJSONObject("calculationDetails").getDouble("totalTicketAmount"));
									totalAmountResp.setTotalTicketQuantity(json.getJSONObject("response")
											.getJSONObject("calculationDetails").getInt("totalTicketQuantity"));
									totalAmountResp.setTotalCodeDiscount(json.getJSONObject("response")
											.getJSONObject("calculationDetails").getInt("totalCodeDiscount"));
									// totalAmountResp.setTotalBulkDiscount(json.getJSONObject("response").getJSONObject("calculationDetails").getDouble("totalBulkDiscount"));
									totalAmountResp.setTotalTaxAmount(json.getJSONObject("response")
											.getJSONObject("calculationDetails").getInt("totalTaxAmount"));
									totalAmountResp.setTotalReferralDiscount(json.getJSONObject("response")
											.getJSONObject("calculationDetails").getInt("totalReferralDiscount"));
									totalAmountResp.setTotalReferrerDiscount(json.getJSONObject("response")
											.getJSONObject("calculationDetails").getInt("totalReferrerDiscount"));
									totalAmountResp.setRoundofvalue(json.getJSONObject("response")
											.getJSONObject("calculationDetails").getInt("roundofvalue"));
									totalAmountResp.setTotalPurchaseAmount(json.getJSONObject("response")
											.getJSONObject("calculationDetails").getInt("totalPurchaseAmount"));
									totalAmountResp.setCurrencyCode(json.getJSONObject("response")
											.getJSONObject("calculationDetails").getString("currencyCode"));
									totalAmountResp.setDiscountCode(json.getJSONObject("response")
											.getJSONObject("calculationDetails").getString("discountCode"));
									totalAmountResp.setReferralCode(json.getJSONObject("response")
											.getJSONObject("calculationDetails").getString("referralCode"));
									totalAmountResp.setPcode(json.getJSONObject("response")
											.getJSONObject("calculationDetails").getString("pcode"));
									totalAmountResp.setPromoterCode(json.getJSONObject("response")
											.getJSONObject("calculationDetails").getString("promoterCode"));
									JSONArray array = json.getJSONObject("response").getJSONObject("calculationDetails")
											.getJSONArray("ticketsData");
									List<MeraEventTicketCalculationResponse> ticketList = new ArrayList<>();
									for (int i = 0; i < array.length(); i++) {
										MeraEventTicketCalculationResponse list = new MeraEventTicketCalculationResponse();
										JSONObject jsonObject = array.getJSONObject(i);
										list.setTicketId(jsonObject.getString("ticketId"));
										list.setTicketName(jsonObject.getString("ticketName"));
										list.setTicketPrice(jsonObject.getDouble("totalAmount"));
										list.setTicketType(jsonObject.getString("ticketType"));
										list.setSelectedQuantity(jsonObject.getInt("selectedQuantity"));
										ticketList.add(list);
									}
									result.setCode("S00");
									result.setMessage("Total amount to be paid : ");
									result.setSuccess(true);
									result.setStatus("Success");
									result.setResponse(totalAmountResp.getResponse());
								} catch (Exception e) {
									e.printStackTrace();
								}
							} else {
								result.setSuccess(false);
								result.setCode("F00");
								result.setMessage("Please try again later..");
								result.setStatus("Failure");
								result.setResponse(APIUtils.getFailedJSON().toString());
							}
						} else {
							result.setSuccess(false);
							result.setCode("F00");
							result.setMessage("User Authentication Failed");
							result.setStatus("Failure");
							result.setResponse(APIUtils.getFailedJSON().toString());
						}
					} else {
						result.setSuccess(false);
						result.setCode("F00");
						result.setMessage("User Authority null");
						result.setStatus("Failure");
						result.setResponse(APIUtils.getFailedJSON().toString());
					}
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Session null");
					result.setStatus("Failure");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unknown device");
				result.setStatus("Failure");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("Un-authorized Role");
			result.setStatus("Failure");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<String>(result.getResponse(), HttpStatus.OK);
	}
	String ticketId = "";
	@RequestMapping(value = "/InitiateBooking", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<String> initiateBookingProcess(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody MeraEventsBookingRequest dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		MeraEventsResponse result = new MeraEventsResponse();
		String sessionId = dto.getSessionId();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				if (dto.getSessionId() != null || dto.getSessionId().length() > 0) {
					String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
					if (authority != null) {
						if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
							dto.setSessionId(sessionId);
							result = meraEventsApi.initiateBooking(dto);
							if (result.getCode().equalsIgnoreCase("S00")) {
								session.setAttribute("orderId", result.getMessage());
								session.setAttribute("eventId", dto.getEventId());
								ticketId=dto.getTicketId();
								result.setCode("S00");
								result.setMessage("Ticket Booking Initiated ");
								result.setSuccess(true);
								result.setStatus("Success");
								result.setResponse(result.getResponse());
							} else {
								result.setSuccess(false);
								result.setCode("F00");
								result.setMessage("Please try again later..");
								result.setStatus("Failure");
								result.setResponse(APIUtils.getFailedJSON().toString());
							}
						} else {
							result.setSuccess(false);
							result.setCode("F00");
							result.setMessage("User Authentication Failed");
							result.setStatus("Failure");
							result.setResponse(APIUtils.getFailedJSON().toString());
						}
					} else {
						result.setSuccess(false);
						result.setCode("F00");
						result.setMessage("User Authority null");
						result.setStatus("Failure");
						result.setResponse(APIUtils.getFailedJSON().toString());
					}
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Session null");
					result.setStatus("Failure");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unknown device");
				result.setStatus("Failure");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("Un-authorized Role");
			result.setStatus("Failure");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<String>(result.getResponse(), HttpStatus.OK);
	}

	@RequestMapping(value = "/AttendeeForm", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<String> attendeeForm(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody MeraEventsCommonRequest dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		MeraEventsResponse result = new MeraEventsResponse();
		String sessionId = dto.getSessionId();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				if (dto.getSessionId() != null || dto.getSessionId().length() > 0) {
					String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
					System.err.println("Authority :: " + authority);
					if (authority != null) {
						if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
							dto.setSessionId(sessionId);
							// dto.setAccess_token((String)session.getAttribute("accessToken"));
							dto.setOrderId((String) session.getAttribute("orderId"));
						//	result = meraEventsApi.attendeeForm(dto);
							if (result.getCode().equalsIgnoreCase("S00")) {
								result.setCode("S00");
								result.setMessage("Attendee Form For Registration ");
								result.setSuccess(true);
								result.setStatus("Success");
								result.setResponse(result.getResponse());
							} else {
								result.setSuccess(false);
								result.setCode("F00");
								result.setMessage("Please try again later..");
								result.setStatus("Failure");
								result.setResponse(APIUtils.getFailedJSON().toString());
							}
						} else {
							result.setSuccess(false);
							result.setCode("F00");
							result.setMessage("User Authentication Failed");
							result.setStatus("Failure");
							result.setResponse(APIUtils.getFailedJSON().toString());
						}
					} else {
						result.setSuccess(false);
						result.setCode("F00");
						result.setMessage("User Authority null");
						result.setStatus("Failure");
						result.setResponse(APIUtils.getFailedJSON().toString());
					}
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Session null");
					result.setStatus("Failure");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unknown device");
				result.setStatus("Failure");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("Un-authorized Role");
			result.setStatus("Failure");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<String>(result.getResponse(), HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/SaveAttendee", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<String> saveAttendee(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody MeraEventsAttendeeFormRequest dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		MeraEventsResponse result = new MeraEventsResponse();
		MeraEventDetailsRequest event = new MeraEventDetailsRequest();
		MeraEventsCommonRequest req = new MeraEventsCommonRequest();
		String sessionId = dto.getSessionId();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				if (dto.getSessionId() != null || dto.getSessionId().length() > 0) {
					String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
					if (authority != null) {
						if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
							dto.setSessionId(sessionId);
							// dto.setAccess_token((String)session.getAttribute("accessToken"));
							dto.setOrderId((String) session.getAttribute("orderId"));
							dto.setEmailEnable(true);
							dto.setSmsEnable(true);
							result = meraEventsApi.saveAttendeeDetails(dto);
						if (result.getCode().equalsIgnoreCase("S00")) {
						//		session.setAttribute("eventSignUpId", result.getEventSignUpId());
								event.setEventSignUpId(result.getEventSignUpId());
								double commission = calculateCommission(result.getTotalAmount());
								double netAmount = result.getTotalAmount();
								session.getAttribute("netAmount");
								String eventId =  (String) session.getAttribute("eventId");
								//event.setEventSignUpId(1234567);
								if (eventId == eventDetails.getEventId()) {
									event.setCity(eventDetails.getCityName());
									event.setState(eventDetails.getStateName());
									event.setCountry(eventDetails.getCountryName());
									event.setEventCategory(eventDetails.getCategoryName());
									event.setEventName(eventDetails.getTitle());
									event.setEventStartDate(eventDetails.getStartDate());
									event.setEventEndDate(eventDetails.getEndDate());
									event.setEventVanue(eventDetails.getVenueName());
									event.setTotalAmount(netAmount);
									event.setTicketId(ticketId);
									//event.setNoOfAttendees(totalAmountResp.getTotalTicketQuantity());
								} else {
									req.setEventId(eventId);
									MeraEventsListResponse res = meraEventsApi.getEventDetails(req);
									event.setCity(res.getCityName());
									event.setState(res.getStateName());
									event.setCountry(res.getCountryName());
									event.setEventCategory(res.getCategoryName());
									event.setEventName(res.getTitle());
									event.setEventStartDate(res.getStartDate());
									event.setEventEndDate(res.getEndDate());
									event.setEventVanue(res.getVenueName());
									event.setTotalAmount(netAmount);
									event.setTicketId(ticketId);
									//event.setNoOfAttendees(totalAmountResp.getTotalTicketQuantity());
								}
								meraEventsApi.saveEventDetails(event);
								if(result.getCode().equalsIgnoreCase("S00")) {
								result.setCode("S00");
								result.setMessage("Event Details Saved ");
								result.setSuccess(true);
								result.setStatus("Success");
								result.setResponse(result.getResponse());
								} else {
									result.setCode("F00");
									result.setMessage("Unable to save event details");
									result.setSuccess(false);
									result.setStatus("Failure");
									result.setResponse(result.getResponse());
								}
							} else {
								result.setSuccess(false);
								result.setCode("F00");
								result.setMessage("Please try again later..");
								result.setStatus("Failure");
								result.setResponse(APIUtils.getFailedJSON().toString());
							}
						} else {
							result.setSuccess(false);
							result.setCode("F00");
							result.setMessage("User Authentication Failed");
							result.setStatus("Failure");
							result.setResponse(APIUtils.getFailedJSON().toString());
						}
					} else {
						result.setSuccess(false);
						result.setCode("F00");
						result.setMessage("User Authority null");
						result.setStatus("Failure");
						result.setResponse(APIUtils.getFailedJSON().toString());
					}
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Session null");
					result.setStatus("Failure");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unknown device");
				result.setStatus("Failure");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("Un-authorized Role");
			result.setStatus("Failure");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<String>(result.getResponse(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/eventPayment", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<String> eventPayment(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody MeraEventsCommonRequest dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		MeraEventsResponse result = new MeraEventsResponse();
		String sessionId = dto.getSessionId();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				if (dto.getSessionId() != null || dto.getSessionId().length() > 0) {
					String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
					if (authority != null) {
						if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
							dto.setSessionId(sessionId);
					// 				Uncomment this line		
					//		dto.setNetAmount((double)session.getAttribute("netAmount"));
							TransactionRequest newRequest = new TransactionRequest();
							newRequest.setSessionId(dto.getSessionId());
				//			// 		Uncomment this line					
				//			newRequest.setAmount((String)session.getAttribute("netAmount"));
				//			Delete next line

						//	double commission = calculateCommission(dto.getNetAmount());
							newRequest.setAmount(Double.parseDouble(dto.getNetAmount()));
							newRequest.setTransactionRefNo("D");
							ResponseDTO responseDTO = transactionApi.validateTransaction(newRequest);
							if(responseDTO.isValid()) {
								result = meraEventsApi.eventPayment(dto);
							}else {
								result.setCode("F00");
								result.setMessage(responseDTO.getMessage());
							}
						} else {
							result.setSuccess(false);
							result.setCode("F00");
							result.setMessage("User Authentication Failed");
							result.setStatus("Failure");
							result.setResponse(APIUtils.getFailedJSON().toString());
						}
					} else {
						result.setSuccess(false);
						result.setCode("F00");
						result.setMessage("User Authority null");
						result.setStatus("Failure");
						result.setResponse(APIUtils.getFailedJSON().toString());
					}
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Session null");
					result.setStatus("Failure");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unknown device");
				result.setStatus("Failure");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("Un-authorized Role");
			result.setStatus("Failure");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<String>(result.getResponse(), HttpStatus.OK);
	}

	public static double calculateCommission(double totalAmount) {
		double commission = 5.9/100;
		double commissionOnAmount = totalAmount*commission;
		double comm = Math.round( commissionOnAmount * 100.0 ) / 100.0;
//		totalAmount = totalAmount + commissionOnAmount;
//		double finalValue = Math.round( totalAmount * 100.0 ) / 100.0;
		return comm;
	}

//	public static void main(String[] args) {
//		double d = calculateCommission(450.16);
//		System.err.println(d);
//	}

}



