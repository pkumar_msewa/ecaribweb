package com.payqwikweb.controller.mobile.api;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.payqwikweb.app.api.impl.DirectPaymentApi;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.request.DirectPaymentRequest;
import com.payqwikweb.app.model.response.DirectPaymentResponse;
import com.payqwikweb.validation.MicroPaymentServiceValidation;

@Controller
@RequestMapping("/Api/{version}/{role}/{device}/{language}/{partner}/User")
public class DirectPaymentServiceController {

	private DirectPaymentApi directPaymentApi;

	public DirectPaymentServiceController(DirectPaymentApi directPaymentApi) {
		this.directPaymentApi = directPaymentApi;
	}

	@RequestMapping(value = "/GenerateToken", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<DirectPaymentResponse> getNikkiToken(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @PathVariable(value = "partner") String partner,
			@RequestBody DirectPaymentRequest dto, @RequestHeader(value = "hash", required = false) String hash) {
		DirectPaymentResponse result = new DirectPaymentResponse();
		try{
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				if (partner.equalsIgnoreCase("Paylo")) {
					result = directPaymentApi.getPayloToken(dto);
					return new ResponseEntity<DirectPaymentResponse>(result, HttpStatus.OK);
				}
				result.setCode("F01");
				result.setDetails("Unauthorised URL Access");
				result.setError("True");
				result.setMessage("Unauthorised URL Access");
				result.setStatus("Failure");
				return new ResponseEntity<DirectPaymentResponse>(result, HttpStatus.OK);
			}
			result.setCode("F01");
			result.setDetails("Unauthorised Access");
			result.setError("True");
			result.setMessage("Unauthorised Access");
			result.setStatus("Failure");
			return new ResponseEntity<DirectPaymentResponse>(result, HttpStatus.OK);
		}
		result.setCode("F01");
		result.setDetails("Unauthorised Role Access");
		result.setError("True");
		result.setMessage("Unauthorised Role Access");
		result.setStatus("Failure");
		return new ResponseEntity<DirectPaymentResponse>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			return new ResponseEntity<DirectPaymentResponse>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/Payment", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<DirectPaymentResponse> intinateChatTransaction(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @PathVariable(value = "partner") String partner,
			@RequestBody DirectPaymentRequest dto, @RequestHeader(value = "hash", required = false) String hash) {
		DirectPaymentResponse result = new DirectPaymentResponse();
		try{
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				if (partner.equalsIgnoreCase("Paylo")) {
					if (MicroPaymentServiceValidation.validateDirectPaymentInitiateRequest(dto)) {
						if (dto.getAmount() > 1) {
							result = directPaymentApi.getIntinatePayloTransaction(dto);
							return new ResponseEntity<DirectPaymentResponse>(result, HttpStatus.OK);
						}
						result.setCode("F01");
						result.setDetails("Invalid Parameters");
						result.setError("True");
						result.setMessage("Invaild Amount");
						result.setStatus("Failure");
						return new ResponseEntity<DirectPaymentResponse>(result, HttpStatus.OK);
					}
					result.setCode("F01");
					result.setDetails("Invalid Parameters");
					result.setError("True");
					result.setMessage("Parameters can't be null");
					result.setStatus("Failure");
					return new ResponseEntity<DirectPaymentResponse>(result, HttpStatus.OK);

				}
				result.setCode("F01");
				result.setDetails("Unauthorised URL Access");
				result.setError("True");
				result.setMessage("Unauthorised URL Access");
				result.setStatus("Failure");
				return new ResponseEntity<DirectPaymentResponse>(result, HttpStatus.OK);
			}
			result.setCode("F01");
			result.setDetails("Unauthorised Access");
			result.setError("True");
			result.setMessage("Unauthorised Access");
			result.setStatus("Failure");
			return new ResponseEntity<DirectPaymentResponse>(result, HttpStatus.OK);
		}
		result.setCode("F01");
		result.setDetails("Unauthorised Role Access");
		result.setError("True");
		result.setMessage("Unauthorised Role Access");
		result.setStatus("Failure");
		return new ResponseEntity<DirectPaymentResponse>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			return new ResponseEntity<DirectPaymentResponse>(result, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/UpdatePayment", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<DirectPaymentResponse> updateChatTransaction(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @PathVariable(value = "partner") String partner,
			@RequestBody DirectPaymentRequest dto, @RequestHeader(value = "hash", required = false) String hash) {
		DirectPaymentResponse result = new DirectPaymentResponse();
		try{
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				if (partner.equalsIgnoreCase("Paylo")) {
					if (MicroPaymentServiceValidation.validateDirectPaymentUpdateTransaction(dto)) {
						result = directPaymentApi.getUpdatePayloTransaction(dto);
						return new ResponseEntity<DirectPaymentResponse>(result, HttpStatus.OK);
					}
					result.setCode("F01");
					result.setDetails("Invalid Parameters");
					result.setError("True");
					result.setMessage("Parameters can't be null");
					result.setStatus("Failure");
					return new ResponseEntity<DirectPaymentResponse>(result, HttpStatus.OK);

				}
				result.setCode("F01");
				result.setDetails("Unauthorised URL Access");
				result.setError("True");
				result.setMessage("Unauthorised URL Access");
				result.setStatus("Failure");
				return new ResponseEntity<DirectPaymentResponse>(result, HttpStatus.OK);
			}
			result.setCode("F01");
			result.setDetails("Unauthorised Access");
			result.setError("True");
			result.setMessage("Unauthorised Access");
			result.setStatus("Failure");
			return new ResponseEntity<DirectPaymentResponse>(result, HttpStatus.OK);
		}
		result.setCode("F01");
		result.setDetails("Unauthorised Role Access");
		result.setError("True");
		result.setMessage("Unauthorised Role Access");
		result.setStatus("Failure");
		return new ResponseEntity<DirectPaymentResponse>(result, HttpStatus.OK);
	   } catch (Exception e) {
		e.printStackTrace();
		result.setCode("F00");
		result.setMessage("We are sorry for inconvenience, Please try again later .");
		result.setStatus("FAILED");
		return new ResponseEntity<DirectPaymentResponse>(result, HttpStatus.OK);
	}
	}

}
