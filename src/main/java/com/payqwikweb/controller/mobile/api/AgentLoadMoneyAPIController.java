package com.payqwikweb.controller.mobile.api;

import java.text.SimpleDateFormat;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.payqwikweb.api.IAuthenticationApi;
import com.payqwikweb.app.api.IAgentLoadMoneyApi;
import com.payqwikweb.app.api.ILoadMoneyApi;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.request.LoadMoneyRequest;
import com.payqwikweb.app.model.request.VNetRequest;
import com.payqwikweb.app.model.response.EBSRedirectResponse;
import com.payqwikweb.app.model.response.LoadMoneyResponse;
import com.payqwikweb.app.model.response.VNetResponse;
import com.payqwikweb.app.model.response.VRedirectResponse;
import com.payqwikweb.model.error.LoadMoneyError;
import com.payqwikweb.util.Authorities;
import com.payqwikweb.util.ModelMapKey;
import com.payqwikweb.validation.LoadMoneyValidation;
import com.thirdparty.model.ResponseDTO;

@Controller
@RequestMapping("/Api/{version}/{role}/{device}/{language}/Agent/LoadMoney")
public class AgentLoadMoneyAPIController implements MessageSourceAware {
	

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

	private MessageSource messageSource;

	private final IAgentLoadMoneyApi loadAgentMoneyApi;
	private final IAuthenticationApi authenticationApi;
	private final LoadMoneyValidation loadMoneyValidation;

	public AgentLoadMoneyAPIController(IAgentLoadMoneyApi loadAgentMoneyApi, IAuthenticationApi authenticationApi,LoadMoneyValidation loadMoneyValidation) {
		this.loadAgentMoneyApi = loadAgentMoneyApi;
		this.authenticationApi = authenticationApi;
		this.loadMoneyValidation = loadMoneyValidation;
	}

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	@RequestMapping(value = "/Process", method = RequestMethod.POST)
	public String processLoadMoney(@PathVariable(value = "version") String version, @PathVariable(value = "role") String role,
								   @PathVariable(value = "device") String device, @PathVariable(value = "language") String language, @ModelAttribute LoadMoneyRequest dto, HttpServletRequest request,
								   HttpServletResponse response, ModelMap modelMap, HttpSession session) {
		String sessionId = dto.getSessionId();
		LoadMoneyError error = loadMoneyValidation.checkError(dto);
		LoadMoneyResponse loadMoneyResponse = new LoadMoneyResponse();
		if (error.isValid()) {
			if (sessionId != null && sessionId.length() != 0) {
				String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
				if (authority != null) {
					if (authority.contains(Authorities.AGENT) && authority.contains(Authorities.AUTHENTICATED)) {
						dto.setSessionId(sessionId);
						if (dto.isUseVnet()) {
							VNetRequest vNetRequest = new VNetRequest();
							vNetRequest.setSessionId(sessionId);
							vNetRequest.setReturnURL(dto.getReturnUrl());
							vNetRequest.setAmount(dto.getAmount());
							VNetResponse vNetResponse = loadAgentMoneyApi.initiateVnetBanking(vNetRequest,Role.AGENT);
							if(vNetResponse.isSuccess()){
								modelMap.addAttribute("vnet",vNetResponse);
								return "Agent/LoadMoney/VNetPay";
							}
						} else {
							loadMoneyResponse = loadAgentMoneyApi.loadMoneyRequest(dto,Role.AGENT);
							if (loadMoneyResponse.isSuccess()) {
								modelMap.addAttribute("loadmoney", loadMoneyResponse);
								return "Agent/LoadMoney/Pay";
							} else {
							//	modelMap.addAttribute(ModelMapKey.MESSAGE, loadMoneyResponse.getDescription());
								return "Agent/Login/LoadMoney";
							}
						}
					}
				}
			}
		}else {
            modelMap.addAttribute(ModelMapKey.ERROR,error.getAmount());
        }
		return "redirect:/";
	}

	@RequestMapping(value = "/InitiateLoadMoney", method = RequestMethod.POST)
	public ResponseEntity<LoadMoneyResponse> initiateLoadMoney(@PathVariable(value = "version") String version, @PathVariable(value = "role") String role,
								   @PathVariable(value = "device") String device, @PathVariable(value = "language") String language, @ModelAttribute LoadMoneyRequest dto, HttpServletRequest request,
								   HttpServletResponse response, ModelMap modelMap, HttpSession session) {
		String sessionId = dto.getSessionId();
        dto.setReturnUrl("-");
		LoadMoneyError error = loadMoneyValidation.checkError(dto);
		LoadMoneyResponse loadMoneyResponse = new LoadMoneyResponse();
		if (error.isValid()) {
			if (sessionId != null && sessionId.length() != 0) {
				String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.AGENT);
				if (authority != null) {
					if (authority.contains(Authorities.AGENT) && authority.contains(Authorities.AUTHENTICATED)) {
						dto.setSessionId(sessionId);
						loadMoneyResponse = loadAgentMoneyApi.loadMoneyRequest(dto,Role.AGENT);
					}
				}
			}
		}
		return new ResponseEntity<LoadMoneyResponse>(loadMoneyResponse,HttpStatus.OK);
	}


	@RequestMapping(value = "/RedirectSDK", method = RequestMethod.POST,produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<EBSRedirectResponse> redirectLoadMoneySDK(@PathVariable(value = "version") String version, @PathVariable(value = "role") String role,
														  @PathVariable(value = "device") String device, @PathVariable(value = "language") String language, @ModelAttribute EBSRedirectResponse redirectResponse, Model model){
		EBSRedirectResponse newResponse = null;
		ResponseDTO result = null;
		String success = redirectResponse.getResponseCode();
        result = loadAgentMoneyApi.verifyEBSTransaction(redirectResponse,Role.AGENT);
		if(result != null){
			String code = result.getCode();
			if(code.equalsIgnoreCase("S00")){
				redirectResponse.setSuccess(true);
				redirectResponse.setResponseCode("0");
			}else {
				redirectResponse.setSuccess(false);
				redirectResponse.setResponseCode("1");
			}
			newResponse = loadAgentMoneyApi.processRedirectSDK(redirectResponse,Role.AGENT);
		}
		return new ResponseEntity<EBSRedirectResponse>(newResponse,HttpStatus.OK);
	}

	@RequestMapping(value = "/Redirect", method = RequestMethod.POST,produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<EBSRedirectResponse> redirectLoadMoney(@PathVariable(value = "version") String version, @PathVariable(value = "role") String role,
														  @PathVariable(value = "device") String device, @PathVariable(value = "language") String language,EBSRedirectResponse redirectResponse,HttpServletRequest request,  Model model){
        EBSRedirectResponse newResponse = null;
        ResponseDTO result = null;
        String success = redirectResponse.getResponseCode();
        result = loadAgentMoneyApi.verifyEBSTransaction(redirectResponse,Role.AGENT);
        if(result != null){
            String code = result.getCode();
            if(code.equalsIgnoreCase("S00")){
                redirectResponse.setSuccess(true);
                redirectResponse.setResponseCode("0");
            }else {
                redirectResponse.setSuccess(false);
                redirectResponse.setResponseCode("1");
            }
            newResponse = loadAgentMoneyApi.processRedirectSDK(redirectResponse,Role.AGENT);
        }
        return new ResponseEntity<EBSRedirectResponse>(newResponse,HttpStatus.OK);
	}

	@RequestMapping(value = "/VRedirect",produces = {MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ResponseDTO> redirectVNetLoadMoney(VRedirectResponse dto, HttpServletRequest request, HttpServletResponse response, HttpSession session) {
		ResponseDTO responseDTO = loadAgentMoneyApi.handleRedirectRequest(dto,Role.AGENT);
		return new ResponseEntity<ResponseDTO>(responseDTO,HttpStatus.OK);
	}
}
