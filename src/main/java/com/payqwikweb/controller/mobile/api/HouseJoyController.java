package com.payqwikweb.controller.mobile.api;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.payqwikweb.app.api.IHouseJoyApi;
import com.payqwikweb.app.api.IQwikrPayApi;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.response.HouseJoyResponse;
import com.payqwikweb.model.app.request.HouseJoyRequestDTO;
import com.payqwikweb.model.app.request.QwikrPayRequestDTO;
import com.payqwikweb.util.APIUtils;
import com.thirdparty.model.ResponseDTO;

@Controller
@RequestMapping("/Api/{version}/{role}/{device}/{language}/HouseJoy")
public class HouseJoyController {
	
	private final IHouseJoyApi houseJoyApi; 
	
	public HouseJoyController(IHouseJoyApi houseJoyApi) {
		super();
		this.houseJoyApi = houseJoyApi;
	}



	@RequestMapping(value = "/Initiate", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<HouseJoyResponse> initializeHouseJoy(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody HouseJoyRequestDTO dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response,HttpSession session) {
		HouseJoyResponse resp = new HouseJoyResponse();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if ((device.equalsIgnoreCase(Device.ANDROID.getValue()))
					|| (device.equalsIgnoreCase(Device.WINDOWS.getValue()))
					|| (device.equalsIgnoreCase(Device.IOS.getValue()))|| (device.equalsIgnoreCase(Device.WEBSITE.getValue()))) {
				HouseJoyResponse respns=houseJoyApi.initializeHouseJoy(dto);
				if (respns != null) {
					resp.setCode(respns.getCode());
					resp.setMessage(respns.getMessage());
					resp.setStatus(respns.getStatus());
					resp.setTransactionId(respns.getTransactionId());
				}else{
				resp.setCode("F00");
				resp.setMessage("Unexpected Response...");
				resp.setStatus("Failure");
			}

			
		} else {
			resp.setCode("F00");
			resp.setMessage("Unauthorised access");
			resp.setStatus("FAILED");
			resp.setDetails(APIUtils.getFailedJSON().toString());
		}
	}else{
		resp.setCode("F00");
		resp.setMessage("User not authorized...");
	}
	
		return new ResponseEntity<HouseJoyResponse>(resp, HttpStatus.OK);
}
	
	
	@RequestMapping(value = "/Success", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<HouseJoyResponse> successHouseJoy(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody HouseJoyRequestDTO dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response) {
		HouseJoyResponse resp = new HouseJoyResponse();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if ((device.equalsIgnoreCase(Device.ANDROID.getValue()))
					|| (device.equalsIgnoreCase(Device.WINDOWS.getValue()))
					|| (device.equalsIgnoreCase(Device.IOS.getValue()))|| (device.equalsIgnoreCase(Device.WEBSITE.getValue()))) {
				HouseJoyResponse respns=houseJoyApi.successHouseJoy(dto);
				if (respns != null) {
					resp.setCode(respns.getCode());
					resp.setStatus(respns.getStatus());
					resp.setMessage(respns.getMessage());
					resp.setTransactionId(respns.getTransactionId());
					return new ResponseEntity<HouseJoyResponse>(resp, HttpStatus.OK);
				}else{
				resp.setCode("F00");
				resp.setMessage("Unexpected Response...");
				resp.setStatus("Failure");
				return new ResponseEntity<HouseJoyResponse>(resp, HttpStatus.OK);
			}
			
		} else {
			resp.setStatus("Failure");
			resp.setCode("F00");
			resp.setMessage("Unauthorised access");
			resp.setStatus("FAILED");
			resp.setDetails(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<HouseJoyResponse>(resp, HttpStatus.OK);
	}else{
		resp.setCode("F00");
		resp.setMessage("User not authorized...");
	}
	
		return new ResponseEntity<HouseJoyResponse>(resp, HttpStatus.OK);
}

	
	
	@RequestMapping(value = "/cancel", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<HouseJoyResponse> cancelHouseJoy(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody HouseJoyRequestDTO dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response,HttpSession session) {
		HouseJoyResponse resp = new HouseJoyResponse();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if ((device.equalsIgnoreCase(Device.ANDROID.getValue()))
					|| (device.equalsIgnoreCase(Device.WINDOWS.getValue()))
					|| (device.equalsIgnoreCase(Device.IOS.getValue()))|| (device.equalsIgnoreCase(Device.WEBSITE.getValue()))) {
				HouseJoyResponse respns=houseJoyApi.cancelHouseJoy(dto);
				if (respns != null) {
					resp.setCode(respns.getCode());
					resp.setMessage(respns.getMessage());
					resp.setStatus(respns.getStatus());
					resp.setTransactionId(respns.getTransactionId());
				}else{
				resp.setCode("F00");
				resp.setMessage("Unexpected Response...");
				resp.setStatus("Failure");
			}

			
		} else {
			resp.setCode("F00");
			resp.setMessage("Unauthorised access");
			resp.setStatus("FAILED");
			resp.setDetails(APIUtils.getFailedJSON().toString());
		}
	}else{
		resp.setCode("F00");
		resp.setMessage("User not authorized...");
	}
	
		return new ResponseEntity<HouseJoyResponse>(resp, HttpStatus.OK);
}
	
}
