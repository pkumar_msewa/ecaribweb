package com.payqwikweb.controller.mobile.api.thirdparty;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.payqwikweb.api.IAuthenticationApi;
import com.payqwikweb.app.api.ISavaariApi;
import com.payqwikweb.app.api.ITransactionApi;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.request.SavaariTokenRequest;
import com.payqwikweb.app.model.request.SendMoneyMobileRequest;
import com.payqwikweb.app.model.request.TransactionRequest;
import com.payqwikweb.app.model.response.SavaariResponse;
import com.payqwikweb.app.model.response.SendMoneyMobileResponse;
import com.payqwikweb.util.APIUtils;
import com.payqwikweb.util.Authorities;
import com.thirdparty.model.ResponseDTO;

@Controller
@RequestMapping("/Api/{version}/{role}/{device}/{language}/Savaari")
public class SavaariAPIController implements MessageSourceAware {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private MessageSource messageSource;

	private final IAuthenticationApi authenticationApi;
	private final ISavaariApi savaariApi;
	private final ITransactionApi transactionApi;

	public SavaariAPIController(IAuthenticationApi authenticationApi, ISavaariApi savaariApi,
			ITransactionApi transactionApi) {
		this.authenticationApi = authenticationApi;
		this.savaariApi = savaariApi;
		this.transactionApi = transactionApi;
	}

	@Override
	public void setMessageSource(MessageSource arg0) {
		// TODO Auto-generated method stub

	}

	@RequestMapping(value = "/TokenCode", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<SavaariResponse> getAccessToken(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody SavaariTokenRequest dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		SavaariResponse resp = new SavaariResponse();
		String sessionId = dto.getSessionId();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				if (dto.getSessionId() != null || dto.getSessionId().length() > 0) {
					String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
					if (authority != null) {
						if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
							dto.setSessionId(sessionId);
							dto.setAppId(APIUtils.API_VALUE_APP_ID);
							dto.setApiKey(APIUtils.API_VALUE_API_KEY);
							resp = savaariApi.getAuthorizationCode(dto);
							if (resp.getCode().equalsIgnoreCase("S00")) {
								dto.setAccessToken(resp.getToken());
								resp = savaariApi.saveAccessToken(dto);
							} else {
								resp.setSuccess(false);
								resp.setCode("F00");
								resp.setMessage("Client Authentication Failed");
								resp.setStatus("Failure");
								resp.setResponse(APIUtils.getCustomJSON(resp.getCode(),resp.getMessage()).toString());
							}
						} else {
							resp.setSuccess(false);
							resp.setCode("F05");
							resp.setMessage("Failed, Unauthorized user.");
							resp.setDetails("Failed, Unauthorized user.");
							resp.setResponse(APIUtils.getCustomJSON(resp.getCode(),resp.getMessage()).toString());
						}
					} else {
						resp.setSuccess(false);
						resp.setCode("F03");
						resp.setMessage("Please, login and try again.");
						resp.setDetails("Please, login and try again.");
						resp.setResponse(APIUtils.getCustomJSON(resp.getCode(),resp.getMessage()).toString());
					}
				} 
			} else {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Unknown device");
				resp.setDetails("Unknown device");
				resp.setResponse(APIUtils.getCustomJSON(resp.getCode(),resp.getMessage()).toString());
			}
		} else {
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Failed, Unauthorized user.");
			resp.setDetails("Failed, Unauthorized user.");
			resp.setResponse(APIUtils.getCustomJSON(resp.getCode(),resp.getMessage()).toString());
		}
		return new ResponseEntity<SavaariResponse>(resp, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/TripType", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<SavaariResponse> getTripType(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody SavaariTokenRequest dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		SavaariResponse resp = new SavaariResponse();
		String sessionId = dto.getSessionId();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				if (dto.getSessionId() != null || dto.getSessionId().length() > 0) {
					String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
					if (authority != null) {
						if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
							dto.setSessionId(sessionId);
							resp=savaariApi.getAccessToken(dto);
							if(resp.getStatus().equalsIgnoreCase("Success")){
								dto.setAccessToken(resp.getResponse());
							resp = savaariApi.getTripType(dto);
							}else{
								resp.setSuccess(false);
								resp.setCode("F05");
								resp.setMessage("Access Token Not Found.");
								resp.setDetails("Access Token Not Found.");
								resp.setResponse(APIUtils.getCustomJSON(resp.getCode(),resp.getMessage()).toString());	
							}
						} else {
							resp.setSuccess(false);
							resp.setCode("F05");
							resp.setMessage("Failed, Unauthorized user.");
							resp.setDetails("Failed, Unauthorized user.");
							resp.setResponse(APIUtils.getCustomJSON(resp.getCode(),resp.getMessage()).toString());
						}
					} else {
						resp.setSuccess(false);
						resp.setCode("F03");
						resp.setMessage("Please, login and try again.");
						resp.setDetails("Please, login and try again.");
						resp.setResponse(APIUtils.getCustomJSON(resp.getCode(),resp.getMessage()).toString());
					}
				} 
			} else {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Unknown device");
				resp.setDetails("Unknown device");
				resp.setResponse(APIUtils.getCustomJSON(resp.getCode(),resp.getMessage()).toString());
			}
		} else {
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Failed, Unauthorized user.");
			resp.setDetails("Failed, Unauthorized user.");
			resp.setResponse(APIUtils.getCustomJSON(resp.getCode(),resp.getMessage()).toString());
		}
		return new ResponseEntity<SavaariResponse>(resp, HttpStatus.OK);
	}

	@RequestMapping(value = "/SubTripType", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<SavaariResponse> getSubTripType(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody SavaariTokenRequest dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		SavaariResponse resp = new SavaariResponse();
		String sessionId = dto.getSessionId();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				if (dto.getSessionId() != null || dto.getSessionId().length() > 0) {
					String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
					if (authority != null) {
						if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
							dto.setSessionId(sessionId);
							resp=savaariApi.getAccessToken(dto);
							if(resp.getStatus().equalsIgnoreCase("Success")){
								dto.setAccessToken(resp.getResponse());
							resp = savaariApi.getSubTripType(dto);
							}else{
								resp.setSuccess(false);
								resp.setCode("F05");
								resp.setMessage("Access Token Not Found.");
								resp.setDetails("Access Token Not Found.");
								resp.setResponse(APIUtils.getCustomJSON(resp.getCode(),resp.getMessage()).toString());	
							}
						} else {
							resp.setSuccess(false);
							resp.setCode("F05");
							resp.setMessage("Failed, Unauthorized user.");
							resp.setDetails("Failed, Unauthorized user.");
							resp.setResponse(APIUtils.getCustomJSON(resp.getCode(),resp.getMessage()).toString());
						}
					} else {
						resp.setSuccess(false);
						resp.setCode("F03");
						resp.setMessage("Please, login and try again.");
						resp.setDetails("Please, login and try again.");
						resp.setResponse(APIUtils.getCustomJSON(resp.getCode(),resp.getMessage()).toString());
					}
				} 
			} else {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Unknown device");
				resp.setDetails("Unknown device");
				resp.setResponse(APIUtils.getCustomJSON(resp.getCode(),resp.getMessage()).toString());
			}
		} else {
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Failed, Unauthorized user.");
			resp.setDetails("Failed, Unauthorized user.");
			resp.setResponse(APIUtils.getCustomJSON(resp.getCode(),resp.getMessage()).toString());
		}
		return new ResponseEntity<SavaariResponse>(resp, HttpStatus.OK);
	}

	@RequestMapping(value = "/SourceCity", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<SavaariResponse> getSourceCities(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody SavaariTokenRequest dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		SavaariResponse resp = new SavaariResponse();
		String sessionId = dto.getSessionId();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				if (dto.getSessionId() != null || dto.getSessionId().length() > 0) {
					String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
					if (authority != null) {
						if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
							dto.setSessionId(sessionId);
							resp=savaariApi.getAccessToken(dto);
							if(resp.getStatus().equalsIgnoreCase("Success")){
								dto.setAccessToken(resp.getResponse());
							resp = savaariApi.getSourceCity(dto);
							}else{
								resp.setSuccess(false);
								resp.setCode("F05");
								resp.setMessage("Access Token Not Found.");
								resp.setDetails("Access Token Not Found.");
								resp.setResponse(APIUtils.getCustomJSON(resp.getCode(),resp.getMessage()).toString());	
							}
						} else {
							resp.setSuccess(false);
							resp.setCode("F05");
							resp.setMessage("Failed, Unauthorized user.");
							resp.setDetails("Failed, Unauthorized user.");
							resp.setResponse(APIUtils.getCustomJSON(resp.getCode(),resp.getMessage()).toString());
						}
					} else {
						resp.setSuccess(false);
						resp.setCode("F03");
						resp.setMessage("Please, login and try again.");
						resp.setDetails("Please, login and try again.");
						resp.setResponse(APIUtils.getCustomJSON(resp.getCode(),resp.getMessage()).toString());
					}
				} 
			} else {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Unknown device");
				resp.setDetails("Unknown device");
				resp.setResponse(APIUtils.getCustomJSON(resp.getCode(),resp.getMessage()).toString());
			}
		} else {
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Failed, Unauthorized user.");
			resp.setDetails("Failed, Unauthorized user.");
			resp.setResponse(APIUtils.getCustomJSON(resp.getCode(),resp.getMessage()).toString());
		}
		return new ResponseEntity<SavaariResponse>(resp, HttpStatus.OK);
	}

	@RequestMapping(value = "/DestinationCity", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<SavaariResponse> getDestinationCities(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody SavaariTokenRequest dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		SavaariResponse resp = new SavaariResponse();
		String sessionId = dto.getSessionId();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				if (dto.getSessionId() != null || dto.getSessionId().length() > 0) {
					String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
					if (authority != null) {
						if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
							dto.setSessionId(sessionId);
							resp=savaariApi.getAccessToken(dto);
							if(resp.getStatus().equalsIgnoreCase("Success")){
								dto.setAccessToken(resp.getResponse());
							resp = savaariApi.getDestinationCity(dto);
							}else{
								resp.setSuccess(false);
								resp.setCode("F05");
								resp.setMessage("Access Token Not Found.");
								resp.setDetails("Access Token Not Found.");
								resp.setResponse(APIUtils.getCustomJSON(resp.getCode(),resp.getMessage()).toString());	
							}
						} else {
							resp.setSuccess(false);
							resp.setCode("F05");
							resp.setMessage("Failed, Unauthorized user.");
							resp.setDetails("Failed, Unauthorized user.");
							resp.setResponse(APIUtils.getCustomJSON(resp.getCode(),resp.getMessage()).toString());
						}
					} else {
						resp.setSuccess(false);
						resp.setCode("F03");
						resp.setMessage("Please, login and try again.");
						resp.setDetails("Please, login and try again.");
						resp.setResponse(APIUtils.getCustomJSON(resp.getCode(),resp.getMessage()).toString());
					}
				} 
			} else {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Unknown device");
				resp.setDetails("Unknown device");
				resp.setResponse(APIUtils.getCustomJSON(resp.getCode(),resp.getMessage()).toString());
			}
		} else {
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Failed, Unauthorized user.");
			resp.setDetails("Failed, Unauthorized user.");
			resp.setResponse(APIUtils.getCustomJSON(resp.getCode(),resp.getMessage()).toString());
		}
		return new ResponseEntity<SavaariResponse>(resp, HttpStatus.OK);
	}

	@RequestMapping(value = "/Availabilities", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<SavaariResponse> getAvailabilities(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody SavaariTokenRequest dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		SavaariResponse resp = new SavaariResponse();
		String sessionId = dto.getSessionId();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				if (dto.getSessionId() != null || dto.getSessionId().length() > 0) {
					String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
					if (authority != null) {
						if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
							dto.setSessionId(sessionId);
							resp=savaariApi.getAccessToken(dto);
							if(resp.getStatus().equalsIgnoreCase("Success")){
								dto.setAccessToken(resp.getResponse());
							resp = savaariApi.getAvailbilities(dto);
							}else{
								resp.setSuccess(false);
								resp.setCode("F05");
								resp.setMessage("Access Token Not Found.");
								resp.setDetails("Access Token Not Found.");
								resp.setResponse(APIUtils.getCustomJSON(resp.getCode(),resp.getMessage()).toString());	
							}
						} else {
							resp.setSuccess(false);
							resp.setCode("F05");
							resp.setMessage("Failed, Unauthorized user.");
							resp.setDetails("Failed, Unauthorized user.");
							resp.setResponse(APIUtils.getCustomJSON(resp.getCode(),resp.getMessage()).toString());
						}
					} else {
						resp.setSuccess(false);
						resp.setCode("F03");
						resp.setMessage("Please, login and try again.");
						resp.setDetails("Please, login and try again.");
						resp.setResponse(APIUtils.getCustomJSON(resp.getCode(),resp.getMessage()).toString());
					}
				} 
			} else {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Unknown device");
				resp.setDetails("Unknown device");
				resp.setResponse(APIUtils.getCustomJSON(resp.getCode(),resp.getMessage()).toString());
			}
		} else {
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Failed, Unauthorized user.");
			resp.setDetails("Failed, Unauthorized user.");
			resp.setResponse(APIUtils.getCustomJSON(resp.getCode(),resp.getMessage()).toString());
		}
		return new ResponseEntity<SavaariResponse>(resp, HttpStatus.OK);
	}

	@RequestMapping(value = "/Localities", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<SavaariResponse> getlocalities(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody SavaariTokenRequest dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		SavaariResponse resp = new SavaariResponse();
		String sessionId = dto.getSessionId();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				if (dto.getSessionId() != null || dto.getSessionId().length() > 0) {
					String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
					if (authority != null) {
						if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
							dto.setSessionId(sessionId);
							resp=savaariApi.getAccessToken(dto);
							if(resp.getStatus().equalsIgnoreCase("Success")){
								dto.setAccessToken(resp.getResponse());
							resp = savaariApi.getlocalities(dto);
							}else{
								resp.setSuccess(false);
								resp.setCode("F05");
								resp.setMessage("Access Token Not Found.");
								resp.setDetails("Access Token Not Found.");
								resp.setResponse(APIUtils.getCustomJSON(resp.getCode(),resp.getMessage()).toString());	
							}
						} else {
							resp.setSuccess(false);
							resp.setCode("F05");
							resp.setMessage("Failed, Unauthorized user.");
							resp.setDetails("Failed, Unauthorized user.");
							resp.setResponse(APIUtils.getCustomJSON(resp.getCode(),resp.getMessage()).toString());
						}
					} else {
						resp.setSuccess(false);
						resp.setCode("F03");
						resp.setMessage("Please, login and try again.");
						resp.setDetails("Please, login and try again.");
						resp.setResponse(APIUtils.getCustomJSON(resp.getCode(),resp.getMessage()).toString());
					}
				} 
			} else {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Unknown device");
				resp.setDetails("Unknown device");
				resp.setResponse(APIUtils.getCustomJSON(resp.getCode(),resp.getMessage()).toString());
			}
		} else {
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Failed, Unauthorized user.");
			resp.setDetails("Failed, Unauthorized user.");
			resp.setResponse(APIUtils.getCustomJSON(resp.getCode(),resp.getMessage()).toString());
		}
		return new ResponseEntity<SavaariResponse>(resp, HttpStatus.OK);
	}

	@RequestMapping(value = "/CarTypes", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<SavaariResponse> getCarTypes(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody SavaariTokenRequest dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		SavaariResponse resp = new SavaariResponse();
		String sessionId = dto.getSessionId();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				if (dto.getSessionId() != null || dto.getSessionId().length() > 0) {
					String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
					if (authority != null) {
						if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
							dto.setSessionId(sessionId);
							resp=savaariApi.getAccessToken(dto);
							if(resp.getStatus().equalsIgnoreCase("Success")){
								dto.setAccessToken(resp.getResponse());
							resp = savaariApi.getCarTypes(dto);
							}else{
								resp.setSuccess(false);
								resp.setCode("F05");
								resp.setMessage("Access Token Not Found.");
								resp.setDetails("Access Token Not Found.");
								resp.setResponse(APIUtils.getCustomJSON(resp.getCode(),resp.getMessage()).toString());	
							}
						} else {
							resp.setSuccess(false);
							resp.setCode("F05");
							resp.setMessage("Failed, Unauthorized user.");
							resp.setDetails("Failed, Unauthorized user.");
							resp.setResponse(APIUtils.getCustomJSON(resp.getCode(),resp.getMessage()).toString());
						}
					} else {
						resp.setSuccess(false);
						resp.setCode("F03");
						resp.setMessage("Please, login and try again.");
						resp.setDetails("Please, login and try again.");
						resp.setResponse(APIUtils.getCustomJSON(resp.getCode(),resp.getMessage()).toString());
					}
				} 
			} else {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Unknown device");
				resp.setDetails("Unknown device");
				resp.setResponse(APIUtils.getCustomJSON(resp.getCode(),resp.getMessage()).toString());
			}
		} else {
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Failed, Unauthorized user.");
			resp.setDetails("Failed, Unauthorized user.");
			resp.setResponse(APIUtils.getCustomJSON(resp.getCode(),resp.getMessage()).toString());
		}
		return new ResponseEntity<SavaariResponse>(resp, HttpStatus.OK);
	}

	@RequestMapping(value = "/Booking", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<SavaariResponse> getCarBooking(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody SavaariTokenRequest dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws JSONException {
		SavaariResponse resp = new SavaariResponse();
		SavaariResponse successResponse=new SavaariResponse();
		String sessionId = dto.getSessionId();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				if (dto.getSessionId() != null || dto.getSessionId().length() > 0) {
					String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
					if (authority != null) {
						if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
							dto.setSessionId(sessionId);
							resp=savaariApi.getAccessToken(dto);
							if(resp.getStatus().equalsIgnoreCase("Success")){
								dto.setAccessToken(resp.getResponse()); 
								long transactionRefNo=System.currentTimeMillis();
								dto.setTransactionRefNo(String.valueOf(transactionRefNo));
								SavaariResponse respFromApp=savaariApi.saveSavaariUserBookingDetails(dto);
							if(respFromApp.getStatus().equalsIgnoreCase("Success")){
								dto.setAccessToken(resp.getResponse());
								SavaariResponse	respFromClient = savaariApi.getBooking(dto);
							if(respFromClient.getStatus().equalsIgnoreCase("success")){
								dto.setBookingId(respFromClient.getBookingId());
								SavaariResponse	detailsFromClient=savaariApi.getBookingDetails(dto);
								String parsedResponse=detailsFromClient.getResponse();
								JSONObject obj=new JSONObject(parsedResponse);
								JSONObject jsonObj=obj.getJSONObject("data");
								String bookingId=jsonObj.getString("bookingId");
								String reservationId=jsonObj.getString("reservationId");
								String status=jsonObj.getString("status");
								String carType=jsonObj.getString("carType");
								String carName=jsonObj.getString("carName");
								String amount=jsonObj.getString("totalFare");
								JSONObject jsonObj1=jsonObj.getJSONObject("tripDetails");
								String locality=jsonObj1.getString("locality");
								String tripType=jsonObj1.getString("package");
								String sourceCity=jsonObj1.getString("sourceCity");
								String pickupDate=jsonObj1.getString("pickupDate");
								String pickupTime=jsonObj1.getString("pickupTime");
								String pickupLocation=jsonObj1.getString("pickupLocation");
								String returnDate=jsonObj1.getString("returnDate");
								dto.setBookingId(bookingId);
								dto.setCarName(carName);
								dto.setCarType(carType);
								dto.setLocality(locality);
								dto.setTripType(tripType);
								dto.setSourceCity(sourceCity);
								dto.setPickupDate(pickupDate);
								dto.setPickupTime(pickupTime);
								dto.setPickupLocation(pickupLocation);
								dto.setReturnDate(returnDate);
								dto.setPrePayment(amount);
								dto.setReservationId(reservationId);
								if(status.equalsIgnoreCase("Booked")){
									successResponse=savaariApi.getSuccessTikcet(dto);
									return new ResponseEntity<SavaariResponse>(successResponse, HttpStatus.OK);
								}else{
									successResponse.setSuccess(false);
									successResponse.setCode("F00");
									successResponse.setMessage("Ticket not Booked");
									successResponse.setDetails("Ticket not Booked");
									return new ResponseEntity<SavaariResponse>(successResponse, HttpStatus.OK);
								}
							}else{
								respFromClient.setSuccess(false);
								respFromClient.setCode("F00");
								respFromClient.setMessage("Booking Not Happen This Time");
								respFromClient.setDetails("Booking Not Happen This Time");
								return new ResponseEntity<SavaariResponse>(respFromClient, HttpStatus.OK);
							  }
							}else{
								respFromApp.setSuccess(false);
								respFromApp.setCode("F00");
								respFromApp.setMessage(respFromApp.getMessage());
								respFromApp.setDetails(respFromApp.getDetails());
								respFromApp.setResponse(APIUtils.getCustomJSON(respFromApp.getCode(),respFromApp.getMessage()).toString());
								return new ResponseEntity<SavaariResponse>(respFromApp, HttpStatus.OK);
							}
						}else{
							resp.setSuccess(false);
							resp.setCode("F00");
							resp.setMessage("Access Token Not Found.");
							resp.setDetails("Access Token Not Found.");
							resp.setResponse(APIUtils.getCustomJSON(resp.getCode(),resp.getMessage()).toString());	
						 }
						} else {
							resp.setSuccess(false);
							resp.setCode("F05");
							resp.setMessage("Failed, Unauthorized user.");
							resp.setDetails("Failed, Unauthorized user.");
							resp.setResponse(APIUtils.getCustomJSON(resp.getCode(),resp.getMessage()).toString());
						}
					} else {
						resp.setSuccess(false);
						resp.setCode("F03");
						resp.setMessage("Please, login and try again.");
						resp.setDetails("Please, login and try again.");
						resp.setResponse(APIUtils.getCustomJSON(resp.getCode(),resp.getMessage()).toString());
					}
				} 
			} else {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Unknown device");
				resp.setDetails("Unknown device");
				resp.setResponse(APIUtils.getCustomJSON(resp.getCode(),resp.getMessage()).toString());
			}
		} else {
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Failed, Unauthorized user.");
			resp.setDetails("Failed, Unauthorized user.");
			resp.setResponse(APIUtils.getCustomJSON(resp.getCode(),resp.getMessage()).toString());
		}
		return new ResponseEntity<SavaariResponse>(resp, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/CancelTicket", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<SavaariResponse> cancelTicket(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody SavaariTokenRequest dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws JSONException {
		SavaariResponse resp = new SavaariResponse();
		String sessionId = dto.getSessionId();
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				if (dto.getSessionId() != null || dto.getSessionId().length() > 0) {
					String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
					if (authority != null) {
						if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
							dto.setSessionId(sessionId);
							resp=savaariApi.getAccessToken(dto);
							if(resp.getStatus().equalsIgnoreCase("Success")){
								SavaariResponse respFromApp = savaariApi.checkBookingId(dto);
							if(respFromApp.getStatus().equalsIgnoreCase("Success")){
								dto.setAccessToken(resp.getResponse());
								SavaariResponse clientResponse=savaariApi.CancelDetails(dto);
								if(clientResponse.getStatus().equalsIgnoreCase("success")){
									dto.setAccessToken(resp.getResponse());
									SavaariResponse cancellResponse=savaariApi.getBookingDetails(dto);
									String parsedResponse=cancellResponse.getResponse();
									JSONObject obj=new JSONObject(parsedResponse);
									JSONObject jsonObj=obj.getJSONObject("data");
									String bookingId=jsonObj.getString("bookingId");
									String reservationId=jsonObj.getString("reservationId");
									String status=jsonObj.getString("status");
									dto.setBookingId(bookingId);
									dto.setReservationId(reservationId);
									dto.setStatus(status);
									if(cancellResponse!=null){
										SavaariResponse cancellTicket=savaariApi.cancelTicket(dto);
										return new ResponseEntity<SavaariResponse>(cancellTicket, HttpStatus.OK);
									}
								}else{
									resp.setSuccess(false);
									resp.setCode("F05");
									resp.setMessage("Access Token Not Found.");
									resp.setDetails("Access Token Not Found.");
									resp.setResponse(APIUtils.getCustomJSON(clientResponse.getCode(),clientResponse.getMessage()).toString());
								}
							}
							}else{
								resp.setSuccess(false);
								resp.setCode("F05");
								resp.setMessage("Access Token Not Found.");
								resp.setDetails("Access Token Not Found.");
								resp.setResponse(APIUtils.getCustomJSON(resp.getCode(),resp.getMessage()).toString());	
							}
						} else {
							resp.setSuccess(false);
							resp.setCode("F05");
							resp.setMessage("Failed, Unauthorized user.");
							resp.setDetails("Failed, Unauthorized user.");
							resp.setResponse(APIUtils.getCustomJSON(resp.getCode(),resp.getMessage()).toString());
						}
					} else {
						resp.setSuccess(false);
						resp.setCode("F03");
						resp.setMessage("Please, login and try again.");
						resp.setDetails("Please, login and try again.");
						resp.setResponse(APIUtils.getCustomJSON(resp.getCode(),resp.getMessage()).toString());
					}
				} 
			} else {
				resp.setSuccess(false);
				resp.setCode("F00");
				resp.setMessage("Unknown device");
				resp.setDetails("Unknown device");
				resp.setResponse(APIUtils.getCustomJSON(resp.getCode(),resp.getMessage()).toString());
			}
		} else {
			resp.setSuccess(false);
			resp.setCode("F00");
			resp.setMessage("Failed, Unauthorized user.");
			resp.setDetails("Failed, Unauthorized user.");
			resp.setResponse(APIUtils.getCustomJSON(resp.getCode(),resp.getMessage()).toString());
		}
		return new ResponseEntity<SavaariResponse>(resp, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/GetBookingDetails", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<SavaariResponse> sendMoneyMobile(@PathVariable(value = "version") String version, @PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody SavaariTokenRequest dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response) {
		SavaariResponse result = new SavaariResponse();
			if (role.equalsIgnoreCase(Role.USER.getValue())) {
				if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
						|| device.equalsIgnoreCase(Device.IOS.getValue())) {
						result = savaariApi.getTicketDetails(dto);
				} else {
					result.setSuccess(false);
					result.setCode("F00");
					result.setMessage("Unknown device");
					result.setStatus("FAILED");
					result.setResponse(APIUtils.getFailedJSON().toString());
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unauthorised access");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
			return new ResponseEntity<SavaariResponse>(result, HttpStatus.OK);
	}

}
