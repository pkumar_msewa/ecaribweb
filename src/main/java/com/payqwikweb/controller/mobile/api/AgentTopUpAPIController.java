package com.payqwikweb.controller.mobile.api;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.instantpay.api.IValidationApi;
import com.instantpay.model.request.ValidationRequest;
import com.instantpay.util.IPayConvertUtil;
import com.payqwikweb.api.IAuthenticationApi;
import com.payqwikweb.app.api.IAgentTopupApi;
import com.payqwikweb.app.api.ITopupApi;
import com.payqwikweb.app.api.ITransactionApi;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.request.BrowsePlansRequest;
import com.payqwikweb.app.model.request.GetOperatorAndCircleForMobRequest;
import com.payqwikweb.app.model.request.GetOperatorAndCircleRequest;
import com.payqwikweb.app.model.request.RechargeRequestDTO;
import com.payqwikweb.app.model.request.TransactionRequest;
import com.payqwikweb.app.model.response.BrowsePlansResponse;
import com.payqwikweb.app.model.response.GetOperatorAndCircleForMobResponse;
import com.payqwikweb.app.model.response.GetOperatorAndCircleResponse;
import com.payqwikweb.app.model.response.RechargeResponseDTO;
import com.payqwikweb.app.model.response.UserDetailsResponse;
import com.payqwikweb.model.web.CarrotFryDTO;
import com.payqwikweb.util.APIUtils;
import com.payqwikweb.util.ConvertUtil;
import com.payqwikweb.validation.TopupValidation;
import com.thirdparty.model.ResponseDTO;

@Controller
@RequestMapping("/Api/{version}/{role}/{device}/{language}/Agent/Topup")
public class AgentTopUpAPIController implements MessageSourceAware {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private MessageSource messageSource;

	private final IAgentTopupApi appAgentTopupApi;
	private final IAuthenticationApi authenticationApi;
	private final ITransactionApi transactionApi;
	private final IValidationApi validationApi;

	public AgentTopUpAPIController(IAgentTopupApi appAgentTopupApi, IAuthenticationApi authenticationApi,
			ITransactionApi transactionApi, IValidationApi validationApi) {
		this.appAgentTopupApi = appAgentTopupApi;
		this.authenticationApi = authenticationApi;
		this.transactionApi = transactionApi;
		this.validationApi = validationApi;
	}

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	
	@RequestMapping(value = "/GetOperatorAndCircle", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<GetOperatorAndCircleResponse> getOperatorAndCircle(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody GetOperatorAndCircleRequest dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response) {
		GetOperatorAndCircleResponse result = new GetOperatorAndCircleResponse();
		if (role.equalsIgnoreCase(Role.AGENT.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				result = appAgentTopupApi.operatorAndcircle(dto,Role.AGENT);
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unknown device");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("Unauthorised access");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<GetOperatorAndCircleResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/GetOperatorAndCircleForMobile", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<GetOperatorAndCircleForMobResponse> getOperatorAndCircleForMobile(
			@PathVariable(value = "version") String version, @PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody GetOperatorAndCircleForMobRequest dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response) {
		GetOperatorAndCircleForMobResponse result = new GetOperatorAndCircleForMobResponse();
		if (role.equalsIgnoreCase(Role.AGENT.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				result = appAgentTopupApi.operatorAndcircleForMob(dto,Role.AGENT);
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unknown device");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("Unauthorised access");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<GetOperatorAndCircleForMobResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/GetPlans", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<BrowsePlansResponse> getPlans(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody BrowsePlansRequest dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response) {
		BrowsePlansResponse result = new BrowsePlansResponse();
		if (role.equalsIgnoreCase(Role.AGENT.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				result = appAgentTopupApi.getPlansForMobile(dto,Role.AGENT);
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unknown device");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("Unauthorised access");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}

		return new ResponseEntity<BrowsePlansResponse>(result, HttpStatus.OK);
	}


	@RequestMapping(value = "/checkForSplitPay", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<RechargeResponseDTO> transactionCheckForSplitPayment(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody RechargeRequestDTO dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response) {
		RechargeResponseDTO result = new RechargeResponseDTO();
		if (role.equalsIgnoreCase(Role.AGENT.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				
					TransactionRequest newRequest = new TransactionRequest();
					newRequest.setSessionId(dto.getSessionId());
					newRequest.setAmount(Double.parseDouble(dto.getAmount()));
					newRequest.setTransactionRefNo("D");
					ResponseDTO responseDTO1 = transactionApi.validateTransaction(newRequest);
					if(responseDTO1.getCode().equalsIgnoreCase("T01")) {
						result.setCode(responseDTO1.getCode());
						result.setMessage(responseDTO1.getMessage());
						dto.setSplitAmount(Double.toString(responseDTO1.getAmount()));
						result.setDto(dto);
						System.out.println(dto);
					}else {
						result.setCode(responseDTO1.getCode());
						result.setMessage(responseDTO1.getMessage());
						result.setDto(dto);
					}
				}else{
					result.setCode("F00");
					result.setMessage("not a valid device");
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unknown device");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		
		return new ResponseEntity<RechargeResponseDTO>(result, HttpStatus.OK);
	}

}

