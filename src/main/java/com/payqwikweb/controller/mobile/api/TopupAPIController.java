package com.payqwikweb.controller.mobile.api;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.payqwikweb.api.IAuthenticationApi;
import com.payqwikweb.app.api.ITopupApi;
import com.payqwikweb.app.api.ITransactionApi;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.request.BrowsePlansRequest;
import com.payqwikweb.app.model.request.GetOperatorAndCircleForMobRequest;
import com.payqwikweb.app.model.request.GetOperatorAndCircleRequest;
import com.payqwikweb.app.model.request.RechargeRequestDTO;
import com.payqwikweb.app.model.request.TransactionRequest;
import com.payqwikweb.app.model.response.BrowsePlansResponse;
import com.payqwikweb.app.model.response.GetOperatorAndCircleForMobResponse;
import com.payqwikweb.app.model.response.GetOperatorAndCircleResponse;
import com.payqwikweb.app.model.response.RechargeResponseDTO;
import com.payqwikweb.app.model.response.ServiceProviderResponse;
import com.payqwikweb.util.APIUtils;
import com.payqwikweb.util.Authorities;
import com.payqwikweb.util.ServiceProviderUtil;
import com.thirdparty.model.ResponseDTO;

@Controller
@RequestMapping("/Api/{version}/{role}/{device}/{language}/Topup")
public class TopupAPIController implements MessageSourceAware {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private MessageSource messageSource;

	private final ITopupApi appTopupApi;
	private final ITransactionApi transactionApi;
	private final IAuthenticationApi authenticationApi;
	public TopupAPIController(ITopupApi appTopupApi,ITransactionApi transactionApi,IAuthenticationApi authenticationApi) {
		this.appTopupApi = appTopupApi;
		this.transactionApi = transactionApi;
		this.authenticationApi=authenticationApi;
	}

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	@RequestMapping(value = "/PrePaid", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<RechargeResponseDTO> prePaid(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody RechargeRequestDTO dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response) {
		RechargeResponseDTO result = new RechargeResponseDTO();
		try{
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
						result = appTopupApi.prePaid(dto);
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unknown device");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("Unauthorised access");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		 }
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<RechargeResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/PostPaid", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<RechargeResponseDTO> postPaid(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody RechargeRequestDTO dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response) {
		RechargeResponseDTO result = new RechargeResponseDTO();
		try{
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
						result = appTopupApi.prePaid(dto);
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unknown device");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("Unauthorised access");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		 }
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<RechargeResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/DataCard", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<RechargeResponseDTO> dataCard(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody RechargeRequestDTO dto,
			@RequestHeader(value = "hash", required = false) String hash) {
		RechargeResponseDTO result = new RechargeResponseDTO();
		try{
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
						result = appTopupApi.prePaid(dto);
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unknown device");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("Unauthorised access");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		 }
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<RechargeResponseDTO>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/GetOperatorAndCircle", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<GetOperatorAndCircleResponse> getOperatorAndCircle(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody GetOperatorAndCircleRequest dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response) {
		GetOperatorAndCircleResponse result = new GetOperatorAndCircleResponse();
		try{
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				result = appTopupApi.operatorAndcircle(dto);
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unknown device");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("Unauthorised access");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		 }
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<GetOperatorAndCircleResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/GetOperatorAndCircleForMobile", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<GetOperatorAndCircleForMobResponse> getOperatorAndCircleForMobile(
			@PathVariable(value = "version") String version, @PathVariable(value = "role") String role,
			@PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
			@RequestBody GetOperatorAndCircleForMobRequest dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response) {
		GetOperatorAndCircleForMobResponse result = new GetOperatorAndCircleForMobResponse();
		try{
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				result = appTopupApi.operatorAndcircleForMob(dto);
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unknown device");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("Unauthorised access");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		 }
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<GetOperatorAndCircleForMobResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/GetPlans", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<BrowsePlansResponse> getPlans(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody BrowsePlansRequest dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response) {
		BrowsePlansResponse result = new BrowsePlansResponse();
		try{
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				result = appTopupApi.getPlansForMobile(dto);
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unknown device");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} else {
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("Unauthorised access");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		 }
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<BrowsePlansResponse>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/checkForSplitPay", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<RechargeResponseDTO> transactionCheckForSplitPayment(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody RechargeRequestDTO dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response) {
		RechargeResponseDTO result = new RechargeResponseDTO();
		try{
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				
					TransactionRequest newRequest = new TransactionRequest();
					newRequest.setSessionId(dto.getSessionId());
					newRequest.setAmount(Double.parseDouble(dto.getAmount()));
					newRequest.setTransactionRefNo("D");
					ResponseDTO responseDTO1 = transactionApi.validateTransaction(newRequest);
					if(responseDTO1.getCode().equalsIgnoreCase("T01")) {
						result.setCode(responseDTO1.getCode());
						result.setMessage(responseDTO1.getMessage());
						dto.setSplitAmount(Double.toString(responseDTO1.getAmount()));
						result.setDto(dto);
						System.out.println(dto);
					}else {
						result.setCode(responseDTO1.getCode());
						result.setMessage(responseDTO1.getMessage());
						result.setDto(dto);
					}
				}else{
					result.setCode("F00");
					result.setMessage("not a valid device");
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unknown device");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		  } catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<RechargeResponseDTO>(result, HttpStatus.OK);
	}
	
}
