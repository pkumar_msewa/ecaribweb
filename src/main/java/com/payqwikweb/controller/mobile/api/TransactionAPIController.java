package com.payqwikweb.controller.mobile.api;

import com.payqwikweb.api.IAuthenticationApi;
import com.payqwikweb.app.api.*;
import com.payqwikweb.app.model.Device;
import com.payqwikweb.app.model.Role;
import com.payqwikweb.app.model.request.*;
import com.payqwikweb.app.model.response.*;
import com.payqwikweb.model.error.OneClickPayError;
import com.payqwikweb.model.web.MobileTopupDTO;
import com.payqwikweb.util.APIUtils;
import com.payqwikweb.util.Authorities;
import com.payqwikweb.util.JSONParserUtil;
import com.payqwikweb.validation.OneClickPayqwikValidation;
import com.thirdparty.model.ResponseDTO;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("/Api/{version}/{role}/{device}/{language}")
public class TransactionAPIController {

    private IAuthenticationApi authenticationApi;
    private ITransactionApi transactionApi;
    private final ISendMoneyApi sendMoneyApi;
    private final ILoadMoneyApi loadMoneyApi;
    private final ITopupApi topupApi;
    private final IBillPaymentApi billPaymentApi;


    public TransactionAPIController(IAuthenticationApi authenticationApi, ITransactionApi transactionApi, ISendMoneyApi sendMoneyApi, ILoadMoneyApi loadMoneyApi, ITopupApi topupApi, IBillPaymentApi billPaymentApi) {
        this.authenticationApi = authenticationApi;
        this.transactionApi = transactionApi;
        this.sendMoneyApi = sendMoneyApi;
        this.loadMoneyApi = loadMoneyApi;
        this.topupApi = topupApi;
        this.billPaymentApi = billPaymentApi;
    }

    @RequestMapping(value = {"/getAllBanks", "/listBanks"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<ResponseDTO> listAllBanks(@PathVariable(value = "version") String version, @PathVariable(value = "role") String role,
                                             @PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
                                             HttpServletRequest request, HttpServletResponse response) {
        ResponseDTO jsonResponse = transactionApi.getAllBanks();
        return new ResponseEntity<ResponseDTO>(jsonResponse, HttpStatus.OK);
    }

    @RequestMapping(value = {"/getIFSC/{bankCode}"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<ResponseDTO> listIFSC(@PathVariable(value = "version") String version, @PathVariable(value = "role") String role,
                                         @PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
                                         @PathVariable(value = "bankCode") String bankCode, HttpServletRequest request, HttpServletResponse response) {
        ResponseDTO jsonResponse = null;
        if (bankCode != null) {
            jsonResponse = transactionApi.getIFSCByBank(bankCode);
            return new ResponseEntity<ResponseDTO>(jsonResponse, HttpStatus.OK);
        } else {
            jsonResponse = new ResponseDTO();
            jsonResponse.setSuccess(false);
            jsonResponse.setCode("F04");
            jsonResponse.setMessage("Please enter bankCode");
            return new ResponseEntity<ResponseDTO>(jsonResponse, HttpStatus.OK);
        }
    }


    @RequestMapping(value = {"/validateTransactionRequest", "/checkTransactionLimit", "/isValidTransaction"}, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<ResponseDTO> validateTransactionRequest(@PathVariable(value = "version") String version, @PathVariable(value = "role") String role,
                                                           @PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
                                                           @ModelAttribute TransactionRequest dto, HttpServletRequest request, HttpServletResponse response) {
        ResponseDTO jsonResponse = new ResponseDTO();
        dto.setTransactionRefNo("C");
        jsonResponse = transactionApi.validateTransaction(dto);
        return new ResponseEntity<ResponseDTO>(jsonResponse, HttpStatus.OK);
    }

    @RequestMapping(value = {"/OneClickPay"}, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<?> validateOneClickPay(@PathVariable(value = "version") String version, @PathVariable(value = "role") String role,
                                          @PathVariable(value = "device") String device, @PathVariable(value = "language") String language,
                                          @RequestBody OnePayRequest dto) throws JSONException {
        ResponseDTO jsonResponse = new ResponseDTO();
        try{
        String sessionId = dto.getSessionId();
            String authority = authenticationApi.getAuthorityFromSession(sessionId, Role.USER);
            int choice = 0;
            if (authority != null) {
                if (authority.contains(Authorities.USER) && authority.contains(Authorities.AUTHENTICATED)) {
                    OnePayResponse resp = transactionApi.getOnePayResponse(dto);
                    String serviceCode = resp.getServiceCode();
                    String amount = dto.getAmount();
                    if (amount != null) {
                        double parsedAmount = Double.parseDouble(amount);
                        if (parsedAmount >= 10 && parsedAmount <= 10000) {
                            JSONObject json = new JSONObject(resp.getJson());
                            if (serviceCode != null) {
                                choice = transactionApi.getChoiceByServiceCode(serviceCode);
                                if (choice == 1) {
                                    ResponseDTO result = new ResponseDTO();
                                    result.setSuccess(false);
                                    result.setCode("F04");
                                    result.setMessage("Service Not Yet Active");
                                    return new ResponseEntity<ResponseDTO>(result, HttpStatus.OK);
                                } else if (choice == 2) {
                                    SendMoneyMobileRequest sendMoneyMobileRequest = new SendMoneyMobileRequest();
                                    sendMoneyMobileRequest.setSessionId(sessionId);
                                    sendMoneyMobileRequest.setAmount(dto.getAmount());
                                    sendMoneyMobileRequest.setMobileNumber(JSONParserUtil.getString(json, "mobileNumber"));
                                    sendMoneyMobileRequest.setMessage(JSONParserUtil.getString(json, "message"));
                                    SendMoneyMobileResponse sendMoneyMobileResponse = sendMoneyApi.sendMoneyMobileRequest(sendMoneyMobileRequest);
                                    return new ResponseEntity<SendMoneyMobileResponse>(sendMoneyMobileResponse, HttpStatus.OK);
                                } else if (choice == 3) {
                                    RechargeRequestDTO topupRequest = new RechargeRequestDTO();
                                    topupRequest.setSessionId(sessionId);
                                    topupRequest.setAmount(dto.getAmount());
                                    topupRequest.setMobileNo(JSONParserUtil.getString(json, "mobileNo"));
                                    topupRequest.setTopupType("Prepaid");
                                    topupRequest.setServiceProvider(JSONParserUtil.getString(json, "serviceProvider"));
                                    topupRequest.setArea(JSONParserUtil.getString(json, "area"));
                                    RechargeResponseDTO prepaidTopupResponse = topupApi.prePaid(topupRequest);
                                    return new ResponseEntity<RechargeResponseDTO>(prepaidTopupResponse, HttpStatus.OK);
                                } else if (choice == 4) {
                                    RechargeRequestDTO postpaidRequest = new RechargeRequestDTO();
                                    postpaidRequest.setSessionId(sessionId);
                                    postpaidRequest.setAmount(dto.getAmount());
                                    postpaidRequest.setMobileNo(JSONParserUtil.getString(json, "mobileNo"));
                                    postpaidRequest.setTopupType("Postpaid");
                                    postpaidRequest.setServiceProvider(JSONParserUtil.getString(json, "serviceProvider"));
                                    postpaidRequest.setArea(JSONParserUtil.getString(json, "area"));
                                    RechargeResponseDTO postpaidTopupResponse = topupApi.prePaid(postpaidRequest);
                                    return new ResponseEntity<RechargeResponseDTO>(postpaidTopupResponse, HttpStatus.OK);
                                } else if (choice == 5) {
                                    BillPaymentCommonDTO dthBillPaymentRequest = new BillPaymentCommonDTO();
                                    dthBillPaymentRequest.setServiceProvider(JSONParserUtil.getString(json, "serviceProvider"));
                                    dthBillPaymentRequest.setAmount(dto.getAmount());
                                    dthBillPaymentRequest.setSessionId(sessionId);
                                    dthBillPaymentRequest.setDthNo(JSONParserUtil.getString(json, "dthNo"));
                                    BillPaymentCommonResponseDTO dthBillPaymentResponse = billPaymentApi.dthBill(dthBillPaymentRequest);
                                    return new ResponseEntity<BillPaymentCommonResponseDTO>(dthBillPaymentResponse, HttpStatus.OK);
                                } else if (choice == 6) {
                                    BillPaymentCommonDTO landlineBillPaymentRequest = new BillPaymentCommonDTO();
                                    landlineBillPaymentRequest.setSessionId(sessionId);
                                    landlineBillPaymentRequest.setServiceProvider(JSONParserUtil.getString(json, "serviceProvider"));
                                    landlineBillPaymentRequest.setStdCode(JSONParserUtil.getString(json, "stdCode"));
                                    landlineBillPaymentRequest.setLandlineNumber(JSONParserUtil.getString(json, "landlineNumber"));
                                    landlineBillPaymentRequest.setAccountNumber(JSONParserUtil.getString(json, "accountNumber"));
                                    landlineBillPaymentRequest.setAmount(dto.getAmount());
                                    BillPaymentCommonResponseDTO landlineBillPaymentResponse = billPaymentApi.landline(landlineBillPaymentRequest);
                                    return new ResponseEntity<BillPaymentCommonResponseDTO>(landlineBillPaymentResponse, HttpStatus.OK);
                                } else if (choice == 7) {
                                    BillPaymentCommonDTO electricityBillPaymentRequest = new BillPaymentCommonDTO();
                                    electricityBillPaymentRequest.setSessionId(sessionId);
                                    electricityBillPaymentRequest.setAmount(dto.getAmount());
                                    electricityBillPaymentRequest.setServiceProvider(JSONParserUtil.getString(json, "serviceProvider"));
                                    electricityBillPaymentRequest.setAccountNumber(JSONParserUtil.getString(json, "accountNumber"));
                                    electricityBillPaymentRequest.setCycleNumber(JSONParserUtil.getString(json, "cycleNumber"));
                                    BillPaymentCommonResponseDTO electricityBillPaymentResponse = billPaymentApi.electricBill(electricityBillPaymentRequest);
                                    return new ResponseEntity<BillPaymentCommonResponseDTO>(electricityBillPaymentResponse, HttpStatus.OK);
                                } else if (choice == 8) {
                                    BillPaymentCommonDTO gasBillPaymentRequest = new BillPaymentCommonDTO();
                                    gasBillPaymentRequest.setSessionId(sessionId);
                                    gasBillPaymentRequest.setAccountNumber(JSONParserUtil.getString(json, "accountNumber"));
                                    gasBillPaymentRequest.setServiceProvider(JSONParserUtil.getString(json, "serviceProvider"));
                                    gasBillPaymentRequest.setAmount(dto.getAmount());
                                    BillPaymentCommonResponseDTO gasBillPaymentResponse = billPaymentApi.gasBill(gasBillPaymentRequest);
                                    return new ResponseEntity<BillPaymentCommonResponseDTO>(gasBillPaymentResponse, HttpStatus.OK);
                                } else if (choice == 9) {
                                    BillPaymentCommonDTO insuranceBillPaymentRequest = new BillPaymentCommonDTO();
                                    insuranceBillPaymentRequest.setSessionId(sessionId);
                                    insuranceBillPaymentRequest.setAmount(dto.getAmount());
                                    insuranceBillPaymentRequest.setServiceProvider(JSONParserUtil.getString(json, "serviceProvider"));
                                    insuranceBillPaymentRequest.setPolicyDate(JSONParserUtil.getString(json, "policyDate"));
                                    insuranceBillPaymentRequest.setPolicyNumber(JSONParserUtil.getString(json, "policyNumber"));
                                    BillPaymentCommonResponseDTO insuranceBillPaymentResponse = billPaymentApi.insuranceBill(insuranceBillPaymentRequest);
                                    return new ResponseEntity<BillPaymentCommonResponseDTO>(insuranceBillPaymentResponse, HttpStatus.OK);
                                }
                            }
                        }
                    }
                }
                jsonResponse.setCode("F00");
                jsonResponse.setDetails("entered amount is not valid");
                try {
                    jsonResponse.setAmount(Double.parseDouble(dto.getAmount()));
                } catch (Exception e) {
                    jsonResponse.setMessage("please enter a valid number");
                    jsonResponse.setSessionId(sessionId);
                    jsonResponse.setMessage("invalid input please check and try again");
                    jsonResponse.setSuccess(false);
                    jsonResponse.setValid(false);
                    jsonResponse.setStatus("failed");
                    return new ResponseEntity<>(jsonResponse, HttpStatus.OK);
                }
                jsonResponse.setSessionId(sessionId);
                jsonResponse.setMessage("invalid input please check and try again");
                jsonResponse.setSuccess(false);
                jsonResponse.setValid(false);
                jsonResponse.setStatus("failed");
                return new ResponseEntity<>(jsonResponse, HttpStatus.OK);
            }
    	} catch (Exception e) {
			e.printStackTrace();
			jsonResponse.setSuccess(false);
			jsonResponse.setCode("F00");
			jsonResponse.setMessage("We are sorry for inconvenience, Please try again later .");
			jsonResponse.setStatus("FAILED");
			jsonResponse.setResponse(APIUtils.getFailedJSON().toString());
		}
        return new ResponseEntity<>(jsonResponse, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/checkForSplitOneClickPay", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<OneClickPayResponse> transactionCheckForSplitPayment(@PathVariable(value = "version") String version,
			@PathVariable(value = "role") String role, @PathVariable(value = "device") String device,
			@PathVariable(value = "language") String language, @RequestBody OnePayRequest dto,
			@RequestHeader(value = "hash", required = false) String hash, HttpServletRequest request,
			HttpServletResponse response) {
		OneClickPayResponse result = new OneClickPayResponse();
		try{
		if (role.equalsIgnoreCase(Role.USER.getValue())) {
			if (device.equalsIgnoreCase(Device.ANDROID.getValue()) || device.equalsIgnoreCase(Device.WINDOWS.getValue())
					|| device.equalsIgnoreCase(Device.IOS.getValue())) {
				
					TransactionRequest newRequest = new TransactionRequest();
					newRequest.setSessionId(dto.getSessionId());
					newRequest.setAmount(Double.parseDouble(dto.getAmount()));
					newRequest.setTransactionRefNo("D");
					ResponseDTO responseDTO1 = transactionApi.validateTransaction(newRequest);
					if(responseDTO1.getCode().equalsIgnoreCase("T01")) {
						result.setCode(responseDTO1.getCode());
						result.setMessage(responseDTO1.getMessage());
						dto.setSplitAmount(Double.toString(responseDTO1.getAmount()));
						result.setDto(dto);
						System.out.println(dto);
					}else {
						result.setCode(responseDTO1.getCode());
						result.setMessage(responseDTO1.getMessage());
						result.setDto(dto);
					}
				}else{
					result.setCode("F00");
					result.setMessage("not a valid device");
				}
			} else {
				result.setSuccess(false);
				result.setCode("F00");
				result.setMessage("Unknown device");
				result.setStatus("FAILED");
				result.setResponse(APIUtils.getFailedJSON().toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setCode("F00");
			result.setMessage("We are sorry for inconvenience, Please try again later .");
			result.setStatus("FAILED");
			result.setResponse(APIUtils.getFailedJSON().toString());
		}
		return new ResponseEntity<OneClickPayResponse>(result, HttpStatus.OK);
	}

}
