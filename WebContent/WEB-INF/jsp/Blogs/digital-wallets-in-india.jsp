<!DOCTYPE html>
<html>
<head>
<title>Vpayqwik | Blogs</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="${pageContext.request.contextPath}/resources/css/blogcss/bootstrap.css" rel='stylesheet' type='text/css' />
<link href="${pageContext.request.contextPath}/resources/css/blogcss/style.css" rel='stylesheet' type='text/css' />
<script src="${pageContext.request.contextPath}/resources/js/Blogs/jquery.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
</head>
<body>
<jsp:include page="/WEB-INF/jsp/Blogs/BlogHeader.jsp" />
	<div class="container">
		<a href="#"><img class="img-responsive" src="/resources/images/blogs/single-13.jpg" alt=" "></a>
			<div class=" single-grid">
				<h4>Digital wallets in India- a match made in heaven or hell?</h4>	
				<p>
						"Indian digital payment system will be worth 500 billion dollars by 2020", read a headline. Top news channels were breaking news like it was the apocalypse. The shift to a cashless economy was given a major boost the day demonetisation kicked in. Now, almost a year later, India presents one of the largest opportunities for digital payments and digital wallet companies are licking their lips.</p>
						<p>But with India heading towards the milestone 2020 mark, where exactly is the digital payment industry moving? For starters, just like every other measure, one has to factor in the fact that India has a population of more than 1.3 billion. This is a country where a large mass of people possess mobile phones but not bank accounts or even passports for that matter. Yes, our government has always provided access to banks and technology-driven financial services but somehow the reach of these facilities has been limited. In this scenario, digital payment systems offer a breath of fresh air.</p>
						<p>More than 50% of India's population is under the age of 25 and more than 80% of them consume content online. With the age of the internet in full swing, everything happens online. From subscriptions to entertainment services like Netflix and Amazon Prime to payments ranging from general groceries to even the last-minute birthday cake. Not only that, even payment of bills and recharging is done online. What do all of these services require? The answer is money, more specifically, digital money. Ask the average college-goer or worker these days and you'll find their physical wallets having just loose change, such is the extent to which digital wallets have influenced consumer behaviour. With smartphones getting smarter, internet's reach getting wider, and the user experience getting friendlier, this is the golden period for digital wallets.</p>
						<p>
						On the government front, digital wallets like PayTM, Mobikwik, VPayQwik, etc., have been offered the chance to experiment for a better understanding of how to maximise digital finance and it's distribution reach. The government has also supported the move towards a cashless economy by allowing wallets to be interoperable with its own Unified Payments Interface system. Banks too have caught onto the trend and have launched their own digital apps to make online transactions smoother.</p>
	</div>	
	<!--start-single-->
	<div class="single">
		<div class="container">
		<hr>
				<div class="single-top">
					<!--	<a href="#"><img class="img-responsive" src="images/single-1.jpg" alt=" "></a>-->
					<div class=" single-grid">
						<h4>So what's going to happen in the future?</h4>
						<p>Every single consumer is going to be spoilt for choice with various payment applications on their phone. Each of these will have their own unique user-friendly interfaces and will offer a variety of cash back options, coupons and other incentives for making payments. The digital wallet as a tool has the potential to not only understand a customer's usage patterns but also help the user make efficient purchases thus leading to it becoming a complete financial management tool. Artificial intelligence, Big Data and any other advancement in technology will only make the use of a digital wallet more of a need than a want.</p>
					</div>			
    				
				</div>	
				<hr>
				<div class="single-top">
						
					<div class=" single-grid">
						<h4>So is there a flip side to this?</h4>			
						<p>Yes, the digital wallet industry is still in its experimental phase. The margin in the payment business is still considerably small and does not expand beyond the country's borders.Furthermore, ultimately any digital wallet is device-based and hence cannot beat the use of tangible credit/debit cards. If you happen to lose your phone to theft, there is nothing that can stop the user from transacting with your digital money. (Though full-fledged security features are being added with constant correction)</p>
							<p>If in today's date, the general mobile-cart vegetable vendor accepts digital payments, one can only imagine how the reach of digital wallets is going to grow in the coming years. Until now, we lived in a world where consumption drove payments. But the tables have turned. Such is the predicted change in India's payment landscape that the cash to non-cash ratio is estimated to reverse by 2026. </p>
							
							<p>Think about it. Being a modern day consumer, how often do you see yourself going to the supermarket nearby or purchasing a ticket for a concert at the physical venue or even going Dutch when it comes to a bill, and paying with cash? Just like how a world without Google Maps seems a nightmare, we're close to hitting that same point with digital wallets.</p>
							
							<p>If you still haven't gone cashless- download the VPayQwik app for your Android or iOS device today. It is the complete payment app for the 21st century Indian!</p>
					</div>			
    				
				</div>	
				<hr>
			</div>					
	</div>
	</div>
</body>
</html>