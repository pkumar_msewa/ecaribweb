<!DOCTYPE html>
<html>
<head>
<title>Vpayqwik | Blogs</title>
<title>vpayqwik blog</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<link href="css/style.css" rel='stylesheet' type='text/css' />
<script src="js/jquery.min.js"></script>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<style>
	.single-grid h4 {
		font-size: 22px;
	    font-weight: 600;
	    color: #0c9eaf;
	    text-decoration: underline;
	}
</style>
</head>
<body>

<jsp:include page="/WEB-INF/jsp/Blogs/BlogHeader.jsp" />
	
	<div class="container">
		<a href=""><img class="img-responsive" src="/resources/images/blogs/single-17.jpg" alt=" "></a>
			<div class=" single-grid">
				<h4>Top 5 Restaurants in Bengaluru to Dine In this Valentine's Day</h4>	
				<p style="text-align: justify;">We've all heard the saying, "food is the way to someone's heart", (or maybe stomach in this case). With Valentine's Day advancing rapidly upon us, VpayQwik shows you how to earn brownie points with your better half. Here are the top 5 Bengaluru restaurant recommendations to woo your Valentine.
				</p>
	</div>	
	<!--start-single-->
	<div class="single">
		<div class="container">
		<hr>
				<div class="single-top">
					<div class="row">
						<div class="col-sm-7">
							<div class="single-grid" style="text-align: justify;">
								<h4>The Tao Terraces, MG Road</h4>
								<p>Asian food is in a league of its own; everything from ambience to spices, to the actual dining experience, is a soulful journey. The Tao Terraces takes fine dining to a whole new level - quite literally. This multi-storeyed restaurant with classy interiors and a massive rooftop lounge, can transport you to a different country. If you are the sort who enjoys a round of food and drinks over loud music and DJ nights, The Tao Terraces is a must visit.</p>

								<p>Treatcard members can avail 50% off on any 1 starter, any 1 main-course and 2 drinks, from the menu.</p>
							</div>	
						</div>
						<div class="col-sm-5">
							<img class="img-responsive" src="/resources/images/blogs/Tao-Terraces.JPG" alt="Tao Terraces">
						</div>
					</div>
				</div>	
				<hr>
				<div class="single-top">
					<div class="row">
						<div class="col-sm-7">
							<div class=" single-grid" style="text-align: justify;">
								<h4>The Humming Tree, Indiranagar</h4>
								<p>If you are someone who loves live music and rustic interiors, then The Humming Tree is the place for you. The lounge regularly hosts musical nights, with artists of multiple genres performing on stage; you are guaranteed to have a humming good time! Don't forget to try their Sangria and Chicken Satay.</p>

								<p style="font-weight: 600;">Treatcard members can avail any 1 of the two offers:</p>
								<ul>
									<li>50% off on any 1 starter and any 1 main-course (Monday - Thursday).</li>
									<li>Happy Hours extended till closing time.</li>
								</ul>
							</div>	
						</div>
						<div class="col-sm-5">
							<img class="img-responsive" src="/resources/images/blogs/Humming tree.JPG" alt="Humming tree">
						</div>
					</div>
				</div>	
				<hr>
				<div class="single-top">
					<div class="row">
						<div class="col-sm-7">
							<div class=" single-grid" style="text-align: justify;">
								<h4>Murphy's Brewhouse, Domlur</h4>
								<p>If there is one country that knows all there is to know about alcohol, it is Ireland, and Murphy's BrewHouse captures Ireland in all its drinking glory. The brewhouse is unique for its multi-flavoured craft beers and an assortment of liquors (reading the menu itself will make you tipsy). If pubbing is on your Valentine's Day list, Murphy's Brewhouse is the place to be. You are even treated to a huge menu of gourmet pizzas and the occasional music nights.</p>

								<p style="font-weight: 600;">Treatcard members can avail any 1 of the two offers: </p>
								<ul>
									<li>50% off on any 1 starter and any 1 main-course (Sunday - Thursday).</li>
									<li>Buy 1 get 1 free offer on drinks (Sunday - Thursday).</li>
								</ul>
							</div>	
						</div>
						<div class="col-sm-5">
							<img class="img-responsive" src="/resources/images/blogs/Murphy's Brewhouse.JPG" alt="Murphy's Brewhouse">
						</div>
					</div>
				</div>	
				<hr>
				<div class="single-top">
					<div class="row">
						<div class="col-sm-7">
							<div class=" single-grid" style="text-align: justify;">
								<h4>Fava, UB City </h4>
								<p>A good dining experience isn't just about the food; presentation and ambience also matter. Award winning chef and owner of Fava, Abhijit Saha, has gone the extra mile with the choice of location. Diners at Fava will be treated to a spectacular view of Cubbon Park. Their Mediterranean-inspired menu, with local and artisanal ingredients sourced from their place of origin, will make your tongues waggle. The entire experience at Fava makes it a perfect first-date (or 100th date) spot.</p>

								<p style="font-weight: 600;">Treatcard members can avail any 1 of the two offers:</p>
								<ul>
									<li>50% off on 1 main-course.</li>
									<li>Happy Hours until closing time.</li>
								</ul>
							</div>	
						</div>
						<div class="col-sm-5">
							<img class="img-responsive" src="/resources/images/blogs/Fava UB city.JPG" alt="Fava UB city">
						</div>
					</div>
				</div>	
				<hr>
				<div class="single-top">
					<div class="row">
						<div class="col-sm-7">
							<div class=" single-grid" style="text-align: justify;">
								<h4>Sotally Tober, Koramangala</h4>
								<p>With a name like this, you know that you and your valentine are going to have a wonderful time. Sotally Tober will delight your senses with its impressive menu of alcohol infused treats like Sour Chai and shot drinks like the Chocoholic. Once inside, you are greeted with a dizzy setting of uniquely placed furniture and lighting effects that set a quirky yet romantic mood. Their cuisine ranges from an impressive European to American, and Asian to North Indian.</p>

								<strong>Treatcard members can avail a buy 1 get 1 free offer on drinks.</strong>
							</div>	
						</div>
						<div class="col-sm-5">
							<img class="img-responsive" src="/resources/images/blogs/Sotally tober.JPG" alt="Tao Terraces">
						</div>
					</div>
				</div>	
				<hr>
				<div class="single-top">
					<div class=" single-grid">
						<p style="font-weight: 600;">Avail all these exciting offers and more by getting a Treatcard annual membership worth 1500 for just Rs 300, when purchasing your membership through the <a href="https://play.google.com/store/apps/details?id=in.msewa.vpayqwik&hl=en">VPayQwik App</a>. Other membership plans available as well.</p>

						<em style="font-weight: 600;">So, which restaurant will you choose this Valentine's Day?!</em>
					</div>
				</div>
				<hr>
				</div>	
			</div>					
	</div>
	
</body>
</html>