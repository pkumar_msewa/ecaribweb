<nav class="navbar navbar-default" style="margin-bottom: 0;">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
       <a class="navbar-brand hidden-lg visible-xs" target="_blank" href="http://www.msspayments.com"><img src="${pageContext.request.contextPath}/resources/images/new_img/mlogo.png" alt="Msewa Softwate Pvt. Ltd." style="width: 30px;margin-top: -5px;"></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a href="/Blogs" class="active">NEWS & INFORMATION</a></li>
		<li><a href="/Blogs/what-if-money-gets-deducted-from-your-bank">HELP/INFORMATION</a></li>
		<li><a href="/Blogs/know-more-about-your-Vpayqwik-wallet">SECURITY</a></li>      
      </ul>
      
      <ul class="nav navbar-nav navbar-right">
			<li><a href="https://www.facebook.com/vpayqwik/"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
			<li><a href="https://twitter.com/V_PayQwik"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
	   </ul>       
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

		<div class="container">
        	<div class="row">
               	<img src="${pageContext.request.contextPath}/resources/images/blogs/toolbart.png" style="width: 305px; margin-top: 25px;">
			</div>               
        </div>
