<%@taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html>
<head>
<title>VPayQwik-Blog | What if money gets deducted from your bank</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="${pageContext.request.contextPath}/resources/css/blogcss/bootstrap.css" rel='stylesheet' type='text/css' />
<link href="${pageContext.request.contextPath}/resources/css/blogcss/style.css" rel='stylesheet' type='text/css' />
<script src="${pageContext.request.contextPath}/resources/js/Blogs/jquery.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
</head>
<body>
<jsp:include page="/WEB-INF/jsp/Blogs/BlogHeader.jsp" />
	<div class="container">
    	<div class="col-md-7 col-md-offset-1">
        	<h3><b>What if money gets deducted from your bank/ card, but does not reflect in Vpayqwik Wallet Balance immediately?</b></h3>
        	<p>The next time you receive a notification from the bank saying money has been deducted, but the amount doesn't reflect in your Vpayqwik account, don't worry. Your money is absolutely safe in the banking system.</p>
        	<p>Transaction goes in pending when we don't receive the final confirmation from your bank or payment gateway. These exceptions happen when banking payment gateways are facing some technical outages or load spikes.</p>
        	<p>In such cases, our systems continue to follow-up with the bank/gateway for the next 4 hrs. In case we don't get confirmation for transaction success from your bank/gateway within the next 4 hrs, the transaction fails. In case money was deducted from your account, your bank could take 7–14 days to credit the refund in your account. Since this time is taken by your bank, Vpayqwik has little capability in expediting it. Hence, we suggest you to please wait for this duration. You may also contact your bank for any delays.</p>
       		<p>We hope that we are able to address your concern through the concern above. If you have a different concern, you can go through our blogs on other topics as well. In case your problem is still not resolved, you can write to us on <b>care@vpayqwik.com</b> or register your complaint by calling <b>080 25011300.<b></p>
        </div>
        <jsp:include page="/WEB-INF/jsp/Blogs/BlogRightMenu.jsp" />
		</div>
	<!--about-end-->
    
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>