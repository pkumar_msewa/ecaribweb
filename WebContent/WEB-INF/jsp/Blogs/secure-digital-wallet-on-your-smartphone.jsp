<!DOCTYPE html>
<html>
<head>
<title>Vpayqwik | Blogs - Secure digital wallet on your smartphone</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="${pageContext.request.contextPath}/resources/css/blogcss/bootstrap.css" rel='stylesheet' type='text/css' />
<link href="${pageContext.request.contextPath}/resources/css/blogcss/style.css" rel='stylesheet' type='text/css' />
<script src="${pageContext.request.contextPath}/resources/js/Blogs/jquery.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
</head>
<body>
<jsp:include page="/WEB-INF/jsp/Blogs/BlogHeader.jsp" />
	
	<div class="container">
		<a href="#"><img class="img-responsive" src="/resources/images/blogs/single-15.jpg" alt=" "></a>
			<div class=" single-grid">
				<h4>How secure is a Digital Wallet on your smartphone?</h4>	
				<p>The year is 2017 and our smartphones have replaced maps, health trackers, cameras, and yoga instructors. Well, the list actually goes on. It's safe to say that the Earth is round ( there's actually an App that shows a live video feed of the Earth from the International Space Station ) and that we are living in a digital age. So why haven't smartphones completely taken over the role of credit cards too? The answer lies in the second stage of Maslow's hierarchy of needs theory- the need for safety. Yes, digital wallets do have security measures, but it's basic human instinct to be uncomfortable with something that's relatively new (Basically, change) and the digital wallet industry in India is still in its infancy stage. So the best way to get over the "uneasiness" is to actually read up and understand the what, how, where, when and why of digital wallets.
				</p>
				<p>Digital wallets are what equip a smartphone to replace the leather-bound wallets you keep in the pockets of your jeans. They normally connect bank accounts, debit cards, credit cards, etc. of the consumers, thus enabling them to transact at the touch of a few buttons. All this information is stored in a unique form. The money stored can be used to pay your mobile bills, electricity bills, book a movie ticket, transfer funds to other wallets, etc. So a digital wallet is essentially a virtual account.</p>
	</div>	
	<!--start-single-->
	<div class="single">
		<div class="container">
		<hr>
				<div class="single-top">
					<!--	<a href="#"><img class="img-responsive" src="images/single-1.jpg" alt=" "></a>-->
					<div class=" single-grid">
						<p><b>But is it safer than a plastic debit/credit card?</b></p>
						<p>Digital wallets provide more security than a plastic card that can be stolen from your back pocket at any point of time and unlike a plastic card, the data in digital wallets is transmitted wirelessly and is also encrypted. Digital wallets provide an extra layer of protection too as every purchase requires a passcode, unique ID or number. Furthermore, your smartphones are protected by pin-codes and pattern locks too. Whereas, if your physical wallet is stolen, the thief can run your tab at a gazillion stores before you realise that you're missing your card. Despite its supremacy, the fact remains that consumers need an incentive to shift to mobile payment. They have a sense of comfort when they're signing a receipt and entering a PIN. </p>
					</div>			
    				
				</div>	
				<hr>
				<div class="single-top">
						
					<div class=" single-grid">						
					<p><b>So what are the vulnerabilities of a digital wallet?</b></p>
						<p>In theory, there are very few ways that your digital wallet can be abused and most of these are inherent to the risks associated with any mobile transaction. For example, during registration, a fraudulent user can register a digital wallet under your mobile number. Digital wallets do not identify fraud verification of the user's information card. There is also a risk of losing your phone and jeopardising your personal and financial information.  
</p>
					</div>			
    				
				</div>	
				<hr>
				
				<div class="single-top">
					<div class=" single-grid">
						<p>But perhaps the biggest risk associated with a digital wallet has to be the personal liability of the user arising in case of fraud. Most credit card/debit card companies protect their consumers against fraud, unlike a digital wallet. They do not hold the cardholder liable for any fraudulent purchases made on their cards. This insurance feature is currently lacking when it comes to the use of a digital wallet.</p>
					</div>			
    				<hr> 
					<div class=" single-grid">
						
						<p>At the end of the day, the reality is that as long as you are broadcasting your data across a mobile network, you're aware of and acknowledging the fact that there is a possibility of data interception and theft by a third party. Yes, the encryption of data makes it harder for a hacker to steal your information but there is always a possibility. </p>
					</div>	
<hr>					
<div class=" single-grid">
						<p>Remember, there was a point in time where e-commerce sites like Flipkart had just kicked off and people had an aversion towards them. "How do I know the product is authentic? What surety do I have that what I see online is what I receive?". Such questions floated around for a while but soon disappeared. Why? Because in today's world, any business/tool that manages to save people time, will click. Be it a chat-based personal management platform like Dunzo that runs all your errands for you or a simple digital wallet tool integrated with your Uber or Ola app that makes the payment as soon as you complete your trip and step out of the cab. Gone are the days where your cab/auto driver runs around asking nearby shopkeeper's for change while you just wait in the cab, kicking yourself for not having the exact amount and wasting precious time.</p>
					</div>
					<div class=" single-grid">
						
						<p>Get on-board with the use of a digital wallet and download the <a href="https://play.google.com/store/apps/details?id=in.msewa.vpayqwik&hl=en">VPayQwik app for your Android </a>or <a href="https://itunes.apple.com/in/app/vpayqwik/id1207326500?mt=8">iOS device</a> today. The 3-tier architecture security system ensures that your digital wallet is as safe as your bank account. After all, soon, physical wallets will be as outdated as the use of encyclopaedias!</p>
					</div>	
<hr>					
					
					
					
				</div>	
			</div>					
	</div>
	</div>
</body>
</html>