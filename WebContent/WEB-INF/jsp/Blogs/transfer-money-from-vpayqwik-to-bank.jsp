<%@taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html>
<html>
<head>
<title>VPayQwik Blog | Transfer Money from VPayQwik to bank instantly</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="${pageContext.request.contextPath}/resources/css/blogcss/bootstrap.css" rel='stylesheet' type='text/css' />
<link href="${pageContext.request.contextPath}/resources/css/blogcss/style.css" rel='stylesheet' type='text/css' />
<script src="${pageContext.request.contextPath}/resources/js/Blogs/jquery.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
</head>
<body>

<jsp:include page="/WEB-INF/jsp/Blogs/BlogHeader.jsp" />

        
		
	<div class="container">
    	<div class="col-md-7 col-md-offset-1">
        	<h3><b>When you transfer money from Vpayqwik to Bank account, it is initiated instantly</b></h3><img src="/resources/images/blogs/9_9.jpg" alt="" class="img-responsive"/></a><br>                     
			<p>Vpayqwik customers can transfer money from their Vpayqwik Wallets to a bank account at any time. We process this transaction instantly and in most cases money is reflected in user's Bank accounts within few minutes. However, when the banking systems are going through stress due load spikes, banks could take typically upto <b>7 working days</b> to credit the amount in your bank account. Please read below to better understand why banks delay when you 'Send Money to Bank Account' from your Vpayqwik Wallet.</p>
				<i class="fa fa-university fa-5x"  style="float:left;color: #00adef;"></i>
			<p>The respective bank's network may not respond to our request due to downtime or network fluctuation at the their end. Banks typically take upto <b>7 working days</b> to settle such transactions. Where banks are not able to settle, the money is rolled back into the customer's Vpayqwik Wallet with a notification.</p>
        		<div style="float:left;margin-right: 10px;color: #00adef;"><i class="fa fa-clock-o fa-5x"></i><i class="fa fa-times fa-2x" style="margin-left: -18px;background: white;"></i></div>
        	<p>There may be occasional delays while processing IMPS transactions owing to a Time out at Bank/NPCI's (National Payment Council of India) end. NPCI could take up to <b>7 working days</b> to update the final status, and we send an SMS to the user immediately when we get an updated status.</p>
       		<img class="img-responsive"  src="/resources/images/blogs/ewallet-transfer-bank-01.jpg" alt="">
        </div>
        
      <jsp:include page="/WEB-INF/jsp/Blogs/BlogRightMenu.jsp" />
		</div>
	<!--about-end-->
    
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>