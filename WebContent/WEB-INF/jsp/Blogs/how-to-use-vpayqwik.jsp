<%@taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html>
<head>
<title>VPayQwik Blog | How To Use VPayQwik</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="${pageContext.request.contextPath}/resources/css/blogcss/bootstrap.css" rel='stylesheet' type='text/css' />
<link href="${pageContext.request.contextPath}/resources/css/blogcss/style.css" rel='stylesheet' type='text/css' />
<script src="${pageContext.request.contextPath}/resources/js/Blogs/jquery.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
</head>
<body>

<jsp:include page="/WEB-INF/jsp/Blogs/BlogHeader.jsp" />
 
	<div class="container">
    	<div class="col-md-7 col-md-offset-1">
        	<h3><b>How to use Vpayqwik</b></h3><img src="${pageContext.request.contextPath}/resources/images/blogs/9_10.jpg" alt="" class="img-responsive"/><br>                     
			<p>Vpayqwik has become one of the most popular payment solutions today and we are only adding to our incredible userbase daily. We felt a simple step-by-step guide would help this vast user-base familiarize themselves with us.</p> 
			<p>Welcome to Vpayqwik. Here is a basic guide to get you started.</p>
        	<h4>What is Vpayqwik?</h4>
       		<p>Vpayqwik is India's largest mobile payments platform. We help you transfer money instantly to anyone at zero cost using the Vpayqwik Wallet. This money can then be used to pay seamlessly at several places like taxi and autos, petrol pumps, grocery shops, restaurants, coffee shops, multiplexes, parking, pharmacies, hospitals and kirana shops among others.</p>
			<p>You could also use it to pay for online recharges, utility bill payments, book movie or travel tickets among other things on the Vpayqwik app or website.</p>
     		<b><p>Here is a step-by-step guide to use Vpayqwik:</p></b>
            <iframe width="100%" height="350px" src="https://www.youtube.com/embed/-2m1mXso4oo" frameborder="0" allowfullscreen></iframe><br><br><br>
        </div>
           <jsp:include page="/WEB-INF/jsp/Blogs/BlogRightMenu.jsp" />
		</div>
	<!--about-end-->
    
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>