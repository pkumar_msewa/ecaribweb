<%@taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html>
<head>
<title>VPayQwik Blog | Transfer money using VPayQwik</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="${pageContext.request.contextPath}/resources/css/blogcss/bootstrap.css" rel='stylesheet' type='text/css' />
<link href="${pageContext.request.contextPath}/resources/css/blogcss/style.css" rel='stylesheet' type='text/css' />
<script src="${pageContext.request.contextPath}/resources/js/Blogs/jquery.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
</head>
<body>

<jsp:include page="/WEB-INF/jsp/Blogs/BlogHeader.jsp" />
 
	<div class="container">
    	<div class="col-md-7 col-md-offset-1">
        	<h3><b>Your identity is safe when you transfer money using Vpayqwik</b></h3>                     
			<p>Over the last couple of days, we received queries regarding what we do to ensure our customer's data is absolutely safe. Here is a simple example.</p>
            <center><img src="/resources/images/blogs/home_1.jpg" class="img-responsive" width=" 30%"></center>
            <p>When you transfer your money using Vpayqwik, we ensure your mobile number is only partially revealed to the recipient. Here, have a look.</p>
            <p>Only the first two and the last three digits of your mobile number would appear in the recipient's Vpayqwik Passbook. This not only guarantees our customers full security against any fraud, but also protects them from possible harassment and spam calls.</p>
            <center><img src="/resources/images/blogs/home_12.jpg" class="img-responsive" width=" 40%"></center><br>
        </div>
         <jsp:include page="/WEB-INF/jsp/Blogs/BlogRightMenu.jsp" />
    </div>
    
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>