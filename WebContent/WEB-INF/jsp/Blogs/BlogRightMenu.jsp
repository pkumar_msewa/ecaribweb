<%@taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<div class="col-md-4 about-right heading">
					<div class="abt-2">
						<h3>Latest</h3>
							<div class="might-grid">
								<div class="grid-might">
									<a href="${pageContext.request.contextPath}/Blogs/how-to-use-vpayqwik"><img src="${pageContext.request.contextPath}/resources/images/blogs/9_10.jpg" class="img-responsive" alt=""> </a>
								</div>
								<div class="might-top">
									<h4><a href="${pageContext.request.contextPath}/Blogs/how-to-use-vpayqwik">How to use Vpayqwik</a></h4>
									<p>Vpayqwik has become one of the most popular payment solutions today and we are only adding to our incredible userbase daily. We felt a simple step-by-step guide would help...</p> 
								</div>
								<div class="clearfix"></div>
							</div>	
							<div class="might-grid">
								<div class="grid-might">
									<a href="${pageContext.request.contextPath}/Blogs/transfer-money-using-vpayqwik"><img src="${pageContext.request.contextPath}/resources/images/blogs/home_12.jpg" class="img-responsive" alt=""> </a>
								</div>
								<div class="might-top">
									<h4><a href="${pageContext.request.contextPath}/Blogs/transfer-money-using-vpayqwik">Your identity is safe when you transfer money using Vpayqwik</a></h4>
									<p>Over the last couple of days, we received queries regarding what we do to ensure our customer's data is absolutely safe. Here is a simple example.</p> 
								</div>
								<div class="clearfix"></div>
							</div>
						
					<div class="abt-2">
						<h3>ARCHIVES</h3>
						<ul>
							<li><a href="${pageContext.request.contextPath}/Blogs/introduction-to-mobile-money">INTRODUCTION TO MOBILE MONEY </a></li>
							<li><a href="${pageContext.request.contextPath}/Blogs/transfer-money-from-vpayqwik-to-bank">When you transfer money from Vpayqwik to Bank account, it is initiated instantly</a></li>
							<li><a href="${pageContext.request.contextPath}/Blogs/know-more-about-your-Vpayqwik-wallet">Know more about your Vpayqwik Wallet: Security Features </a> </li>
							<li><a href="${pageContext.request.contextPath}/Blogs/accept-payment-through-vpayqwik">Self-declared Vpayqwik merchants can now accept up to Rs. 50,000 in their bank account</a> </li>
							<li><a href="${pageContext.request.contextPath}/Blogs/accept-vpayqwik-in-your-store">How to accept Vpayqwik in your Store: Process & FAQs</a> </li>
							<li><a href="${pageContext.request.contextPath}/Blogs/how-we-authenticate-a-user">Your Vpayqwik ID & Password is not enough to access Vpayqwik Wallet Account </a> </li>
							<li><a href="${pageContext.request.contextPath}/Blogs/what-if-money-gets-deducted-from-your-bank">What if money gets deducted from your bank/ card, but does not reflect in Vpayqwik Wallet Balance immediately?</a> </li>
						</ul>	
					</div>
					
				</div>
				<div class="clearfix"></div>			
			</div>