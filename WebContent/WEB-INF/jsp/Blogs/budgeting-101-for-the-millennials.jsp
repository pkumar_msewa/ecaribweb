<!DOCTYPE html>
<html>
<head>
<title>Vpayqwik | Blogs - Budgeting 101 for the millennials</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="${pageContext.request.contextPath}/resources/css/blogcss/bootstrap.css" rel='stylesheet' type='text/css' />
<link href="${pageContext.request.contextPath}/resources/css/blogcss/style.css" rel='stylesheet' type='text/css' />
<script src="${pageContext.request.contextPath}/resources/js/Blogs/jquery.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
</head>
<body>
<jsp:include page="/WEB-INF/jsp/Blogs/BlogHeader.jsp" />
	
	<div class="container">
		<a href="#"><img class="img-responsive" src="/resources/images/blogs/single-14.jpg" alt=" "></a>
			<div class=" single-grid">
				<h4>Budgeting 101 for the Millennials</h4>	
				<p>In the simplest terms, a budget is an estimation of revenue and expenses over a certain period of time. But in a millennial's books, a budget is a friend who turns enemy number one as the calendar counts down to the end of the month. Admit it, we've all have had that "Damn, I wish college taught me how to budget rather than teaching me a concept that has no relevance in the 21st century" moment.
				</p>
	</div>	
	<!--start-single-->
	<div class="single">
		<div class="container">
		<hr>
				<div class="single-top">
					<!--	<a href="#"><img class="img-responsive" src="images/single-1.jpg" alt=" "></a>-->
					<div class=" single-grid">
						
						<p>The first thing you must do is stop attaching a negative emotion towards the word "budget". It isn't the same as the generally unspoken, gruesome word "diet". It definitely does not restrict you to do what you love or spend on what you love. Budget essentially means more efficient spending rather than restricted spending.</p>
					</div>			
    				
				</div>	
				<hr>
				<div class="single-top">
						
					<div class=" single-grid">						
						<p>Next, go through your emails, your dusty bedside drawers, etc. and gather every financial statement that you can find. The idea behind this is to collect every single piece of data relating to the source of your incomes and expenses that will help you make a more accurate estimate. 
</p>
					</div>			
    				
				</div>	
				<hr>
				
				<div class="single-top">
					<div class=" single-grid">
							
						<p>Most college-goers or freshers earn/receive an average of Rs. 20,000 a month. Since it's a personal budget, the source of your money does not really matter. You may be self-employed and earning on your own or you may be receiving a constant inflow every month from your parents if you're living by yourself. (Yes, that doesn't count as income technically, but hey, living independently is work on its own!) . Either way, you have only a fixed sum to juggle with the whole month.</p>
					</div>			
    				<hr> 
					<div class=" single-grid">
						
						<p>Once you've estimated that, list down all your possible expenses to a tee. Right from the monthly rent, cook and maid expenses, groceries cost to the Netflix subscriptions, Recharge expenses and the cost of your late-night food cravings. Now comes the tricky part. You'll have to classify your expenses into fixed and variable. Fixed expenses are basically those that you will incur and are absolutely necessary considering your way of living. (No, they do not include your binge-watching expenses no matter how necessary you feel it is to keep you going on the weekends). They include the rent you pay, the expenses for the cook who keeps you fuelled up, ready to face the day ahead and the internet, for college and work assignments, of course. Variable expenses are those that will change from month to month. For example, if it's exam month you'll probably end up spending more on groceries than usual. All-nighters aren't possible without coffee shots, are they? Hence variable expenses are those that you can constantly adjust based on your circumstances.</p>
					</div>	
<hr>					
<div class=" single-grid">
						<p>It's time for the moment of truth. Total your incomes and expenses, and you'll find yourself either jumping with joy or standing dumbfound. If your incomes exceed your expenses, you're off to a great start. This means that you have savings that you can accumulate and set off with any unplanned expenses that may occur in any other month. If your expenses exceed your income, it means that you really need to sacrifice certain things that you spend on. Bring out your list of variable expenses and strike off any expense that you can do without. Personal budgets aren't dealing with high income flows hence it shouldn't really be difficult to strike a balance between your outflows and inflows.</p>
					</div>
					<div class=" single-grid">
						
						<p>As unexciting as it may seem, preparing a budget will not only help you manage your funds wisely but also make you better at making financial decisions. Review your budget at the end of every month and account for seasonal expenses that may arise. The more detailed your budget is, the more likely you are to match the estimation by the month end. Preparing your budget also reduces the headache of deciding when to pay your bills. With everything accounted for beforehand, end of the months are a lot less shocking.</p>
					</div>	
<hr>					
<div class=" single-grid">
						
						<p>In today's world, budgeting can be made easier by making all your payments through a digital wallet. Be it your mobile recharge, gas bill, electricity bill or groceries bill, payments can be made at the touch of a button. With all your expenses paid for online, you won't find yourself running around searching for bills at the end of the month. Not only that, digital wallets offer loyalty points and discounts that will make your purchases more valuable. So download the <a href="https://play.google.com/store/apps/details?id=in.msewa.vpayqwik&hl=en">VPayQwik app for your Android</a> or <a href="https://itunes.apple.com/in/app/vpayqwik/id1207326500?mt=8">iOS device</a> today and make personal budgeting much easier!</p>
					</div>	
<hr>					
					
					
					
				</div>	
			</div>					
	</div>
	</div>
</body>
</html>