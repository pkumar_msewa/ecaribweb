<!DOCTYPE html>
<html>
<head>
<title>VPayQwik | Blogs - Money tips for millennial college graduate</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="${pageContext.request.contextPath}/resources/css/blogcss/bootstrap.css" rel='stylesheet' type='text/css' />
<link href="${pageContext.request.contextPath}/resources/css/blogcss/style.css" rel='stylesheet' type='text/css' />
<script src="${pageContext.request.contextPath}/resources/js/Blogs/jquery.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
</head>
<body>
<jsp:include page="/WEB-INF/jsp/Blogs/BlogHeader.jsp" />
	
	<div class="container">
		<a href="#"><img class="img-responsive" src="/resources/images/blogs/single-16.jpg" alt=" "></a>
			<div class=" single-grid">
				<h4>Money Tips for the Millennial College Graduate</h4>	
				<p>We are the Millennials, the so-called Gen-Y. The generation of people that are narcissistic and lazy but at the same time positive as well as idea-driven. As we enter our 20's and 30's, we practically dominate the workforce. We are the current movers and shakers of the 21st century. The greatest advantage that we hold is the abundance of time and with plenty of that on our hands, there's this fresh sense of optimism that comes as we graduate. In this stage of massive student loan debts, low wages and lack of financial security, here are a few money tips to help you make the best of what you have.
				</p>
	</div>	
	<!--start-single-->
	<div class="single">
		<div class="container">
		<hr>
				<div class="single-top">
					<div class=" single-grid">
						<p>1. There's this four-letter word that can change your fortunes in the long-run and no it isn't "love". It's "save". People are often confused as to how much of their monthly paycheck they should save, and the right answer is - Save as much as you can! If you're of the opinion that saving money is a sacrifice, well, you're doomed in the long run. Saving should not be an afterthought. You should first begin saving and then spend out of what you have left. This will not only help you build a reserve but you'll also feel guilt-free every time you spend the remainder of your money. Ideally, you must reach a stage where you save 20-25% of your monthly paycheck.</p>
					</div>			
    				
				</div>	
				<hr>
				<div class="single-top">
						
					<div class=" single-grid">						
					<p>2.  So now the question arises, what do you do with the saved money? Leave it idle? Most certainly not! Start investing (even if you have the debt of student loan). What many youngsters fail to realise is that the biggest advantage of investing is time. Investing isn't as complicated as it is made to be and your excuses will remain excuses until you actually open your Demat account. Your money will grow over time due to compounding interest and the joy of seeing your investment portfolio build over time is indescribable. The safest options in today's date for investments are mutual funds. Yes, it will take you a while to learn more about investing but over time you'll realise that you've gained confidence when it comes to making investment choices.</p>
					</div>			
    				
				</div>	
				<hr>
				
				<div class="single-top">
					<div class=" single-grid">
						<p>3. Once you've reached the stage where your inflows and outflows are extensive, it becomes crucial to collect data, constantly review and keep track of your money. At this point, it is important to start tracking your money and investments in one place. Digital wallets and various financial applications help track your savings, account balances and investment performances. For example, digital wallets serve as a one-stop solution for all your payments, be it your mobile bill, electricity bill, rent, etc. <a href="https://play.google.com/store/apps/details?id=in.msewa.vpayqwik&hl=en">Download the VPayQwik app for your Android</a> or <a href="https://itunes.apple.com/in/app/vpayqwik/id1207326500?mt=8">iOS device</a> and make money management an easier task for yourself in today's cash-free world.</p>
					</div>			
    				<hr> 
					<div class=" single-grid">
						<p>4. Yes, we are millennials and we spend on experiences rather than things. But, as much as sharing our experiences on social media makes us happier, we must realise that our experiences sometimes, take a huge chunk out of our monthly budget. Try minimising your biggest expense and you'll see how much of a difference it can make. Be it your housing, eating or commuting expenses or even the religiously constant weekend bar-hopping; find a way to optimise your outflow. Self-learning is the best way and the habit of reading a few personal finance books combined with tried-and-tested tweaks in your budget will only make your piggy-bank fuller in the long-run.</p>
					</div>	
<hr>					
                  <div class=" single-grid">
						<p>5. According to research, anything done for 66 days continuously becomes a habit. Why is that piece of information important? Well, as spontaneous we millennials are, it still is important to follow certain habits that are old-school. Build certain money habits like tracking your net-worth on a constant basis, reading up on the current trends in the finance world, saving at least 5% of your post-tax income and gradually increasing your savings rate by 1%, refinancing your student loans at a lower rate, and searching for ways to optimize your payments.</p>
					</div>
					<hr>
					<div class=" single-grid">
						
						<p>6. Last but not the least, it is crucial to for every college graduate to understand that waiting for the perfect job never works. You need to get out there, network, and hustle. The ideal job won't fall into your lap; you'll get closer to it as time progresses. Also, it is vital to diversify your income streams as you build your portfolio. We live in a fast-paced world where a full-time job may not be enough. A side-income stream will also help you build skills that your regular job may not offer and at the same it, it offers a sense of security.</p>
						<p>There are a million ways to make money in today's world. There are a plethora of opportunities available. Even ideas that were deemed as the "silliest" at first have made young college graduates successful. The same college graduates who dealt with the similar obstacles of student loans, lack of funds and lack of financial experience. At the end of the day, what you have to ask yourself is how badly do you want it?</p>
					</div>	
<hr>					
					
					
					
				</div>	
			</div>					
	</div>
	</div>
</body>
</html>