<%@taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html>
<html>
<head>
<title>VPayQwik Blog | Accept Payment Through VPayQwik</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="${pageContext.request.contextPath}/resources/css/blogcss/bootstrap.css" rel='stylesheet' type='text/css' />
<link href="${pageContext.request.contextPath}/resources/css/blogcss/style.css" rel='stylesheet' type='text/css' />
<script src="${pageContext.request.contextPath}/resources/js/Blogs/jquery.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
</head>
<body>

<jsp:include page="/WEB-INF/jsp/Blogs/BlogHeader.jsp" />
        
    
	<div class="container">
    	<div class="col-md-7 col-md-offset-1">
        	<h3><b>Self-declared Vpayqwik merchants can now accept up to Rs. 50,000 in their bank account</b></h3><p>We are launching a new feature on Vpayqwik that would enable self-declared merchants to transfer up to Rs 50,000 directly in their bank accounts. The amount would be settled directly to the merchant's bank account. The fee to transfer money to your bank account.</p>
        	<img src="${pageContext.request.contextPath}/resources/images/blogs/c-3.jpg" alt="" class="img-responsive"/><br>                     
			<h4><b>FAQs:</b></h4>
			<h5><b>1. Is every merchant across India eligible for this?</b></h5>	
			<p>Exactly. Any and every merchant across India can now self-declare himself or herself as a merchant and accept up to Rs. 50,000 directly in their bank account.</p>
			<hr>
            <h5><b>2. When does this money transferred to the bank?</b></h5>
			<p>The day's balance is sent to your bank account at midnight.</p>
			<hr>
            <h5><b>3. Is this feature available on Android and iOS both?</b></h5>
			<p>We are currently launching this feature on Android. Going forward, we'll also launch it on iOS soon.</p>
			<hr>
            <h5><b>4. How long does it take to connect Vpayqwik to my bank account?</b></h5>
			<p>It typically takes anywhere between 30 mins to an hour. The maximum time can go up to 24 hours.</p>
			<hr>
            <h5><b>5. What are the charges for this upgrade?</b></h5>
			<p>There are no charges for this upgrade. It's absolutely free for all.</p>
           </div>
           
        <jsp:include page="/WEB-INF/jsp/Blogs/BlogRightMenu.jsp" />
		</div>
	<!--about-end-->
    
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>