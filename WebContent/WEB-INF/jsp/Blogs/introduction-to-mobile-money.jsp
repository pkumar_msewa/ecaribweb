<%@taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html>
<head>
<title>VPayQwik Blog | Introduction to mobile money</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="${pageContext.request.contextPath}/resources/css/blogcss/bootstrap.css" rel='stylesheet' type='text/css' />
<link href="${pageContext.request.contextPath}/resources/css/blogcss/style.css" rel='stylesheet' type='text/css' />
<script src="${pageContext.request.contextPath}/resources/js/Blogs/jquery.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
</head>
<body>
<jsp:include page="/WEB-INF/jsp/Blogs/BlogHeader.jsp" />
	<div class="container">
    	<div class="col-md-7 col-md-offset-1">
        	<h3><b>INTRODUCTION TO MOBILE MONEY</b></h3><img src="${pageContext.request.contextPath}/resources/images/blogs/7_1.jpg" alt="" class="img-responsive"/></a><br>                     
			<p>Digital Wallet, specializes in Secure Real-Time transaction processing service and support, and is a payment gateway that enables online payment for transactions and value added services at the point of sales. Digital Wallet performs payment authorization and authentication, and makes settlement of funds between Financial Institutions (FIs), Merchants and Users (Customers).</p>		        				
        	<p>Msewa's mobile financial platform has been uniquely designed to provide multiple services to banked economy as well as unbanked economy. We provide multiple cash in/out channels to our customers so that they can easily fulfill their needs related to financial transactions. The image below explains the various features provided by our platform</p>
       		<br><img src="${pageContext.request.contextPath}/resources/images/blogs/chart2.jpg" class="img-responsive" alt=""><br>
        </div>
         <jsp:include page="/WEB-INF/jsp/Blogs/BlogRightMenu.jsp" />		
		</div>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>