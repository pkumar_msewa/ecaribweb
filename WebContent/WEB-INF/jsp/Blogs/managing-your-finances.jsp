<%@taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html>
<head>
<title>vpayqwik blog | Managing Your Finances</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="${pageContext.request.contextPath}/resources/css/blogcss/bootstrap.css" rel='stylesheet' type='text/css' />
<link href="${pageContext.request.contextPath}/resources/css/blogcss/style.css" rel='stylesheet' type='text/css' />
<script src="${pageContext.request.contextPath}/resources/js/Blogs/jquery.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
</head>
<body>

<jsp:include page="/WEB-INF/jsp/Blogs/BlogHeader.jsp" />
	<div class="container">
		<a href="#"><img class="img-responsive" src="/resources/images/blogs/single-11.jpg" alt=" "></a>
			<div class=" single-grid">
				<!-- <h4>Managing​ ​your​ ​Finances​ ​like​ ​a​ ​Superhero! </h4> -->	
				<h4>Managing your finances like a superhero</h4>
				<p>Be it Marvel or DC, who hasn't walked out of a theater after watching a superhero movie and not felt fiercely inspired? How many of us felt superhero movies are more interesting towards the climax even after the closing credits? No matter how cliched it gets, we are always filled with awe when superheroes rise to take action. These extraordinary gentlemen and women can teach us a thing or two about managing one's personal finances.</p>
			</div>
	</div>	
	<!--start-single-->
	<div class="single">
		<div class="container">
		<hr>
				<div class="single-top">
					<!--	<a href="#"><img class="img-responsive" src="images/single-1.jpg" alt=" "></a>-->
					<div class=" single-grid">
						<h4>Managing Finance is a skill only if you choose to learn. </h4>
						<p>When Spider Man woke up with mysterious powers, he had no clue how to use it. All superheroes are extraordinary not only because they have been gifted with special powers but also because they have trained themselves to harness it. The same applies to us while managing finances. One needs to learn how to save and spend their cash inflows wisely.</p>
						
						<p>Most of our knowledge is from 'friends turned finance gurus', from one's own experiences with banking products and many times from the INTERNET.</p>
						
						<p>Visiting an accountant or an auditor to learn the basics from them is one way to increase knowledge. Reading and learning from finance related books is another option - Top 5 money management books and articles -<b>http://lifehacker.com/five-best-personal-finance-books-1682899925.</b> These books will help in understanding the numbers & the process behind taxes, techniques like passive investments, reducing debts, refinancing and much more. Learning & studying shouldn't be left behind after school & college. After all, our superheroes were also good with technology, physics, biology and what not. They not only got physically stronger but also smarter. So do not take it easy if you have an accountant or auditor to take care of your annual investment & taxes. Take an active interest and put in an effort to get into the depth of the subject.</p>
					</div>			
    				
				</div>	
				<hr>
				<div class="single-top">
						
					<div class=" single-grid">
						<h4>Stick to your Moral Codes</h4>			
						<p>The one thing that differentiates a superhero from the villains is his ultimate commitment to his moral codes. "With great power comes great responsibility" In our case 'Money is Power' thus we need to act responsibly with it.</p>
						
						<p>Make it a habit to split your cash flows. Eg:
60 % - Essentials - Your long term savings, loan EMIs or deposits, your household expenses & bills.
20 % - Upcoming expenses - your short term goals like repairing a broken laptop, paying for an online course or planning that vacation you wanted to take in the next two months.
20% - Splurges - a fancy pair of shoes, your favourite movie, a dinner date etc.
</p>
						
						<p>Never spend more than what you have set aside. Do not binge on credit cards or borrow temporary funds. When you feel like crossing over to the other side of temptation, remind yourself of the financial mistakes you might have committed in the past and have always regretted. Like our superheroes felt responsible and protected people from pain or suffering similar to what they had gone through in their troubled past, we too must strive to not repeat mistakes.</p>
					</div>			
    				
				</div>	
				<hr>
				
				<div class="single-top">
					<div class=" single-grid">
						<h4>It's okay to fail</h4>			
						<p>Superheroes are not an exception to failure. Sometimes, despite getting badly kicked ,they did not stop from showing up on the call of duty. It's okay to feel deflated about your finances sometimes. Bad financial decisions or unplanned expenses might make you feel powerless but it shouldn't deter you from bouncing back. Keep an open mind to learn and resurrect yourself from mistakes. Check if the interest you pay on your debts can cut down your tax, try building another stream of income, review your interest rates and EMIs. Most importantly do not give up saving money, no matter how small it is. You can do it the 'Ant Man' way. Shrink your yearly investments into monthly EMIs. While you might not be able to invest One Lakh per year in a single go, Rs. 8,300/- per month sounds extremely doable.</p>
					</div>			
    				<hr> 
					<div class=" single-grid">
						<h4>Prepare yourself.</h4>				
						<p>If you're still thinking nothing can go wrong, remember what Kryptonite did to the Man of Steel who thought he was indestructible? You need to buckle up and be prepared to deal against all the unexpected speed bumps on the road to your financial freedom. Always make sure your long term or short term investments as well as bank deposits are covered with the right insurance. Have a flexible wealth plan that ensures smooth transitions at times of crisis. Like the untimely death of his parents did not affect the Wayne Enterprises or Batman's financial stability, you too need to be prepared for the worst.</p>
						
						<p>So as Batman says "You must become your fear in order to conquer it". Don't be afraid of complicated banking terminologies. They are not that difficult to learn or understand. Learn to save and invest smartly. Wear your invisible cape and get going to "Save The Money"!</p>
					</div>	
<hr>					
				</div>	
			</div>					
	</div>
	
</body>
</html>