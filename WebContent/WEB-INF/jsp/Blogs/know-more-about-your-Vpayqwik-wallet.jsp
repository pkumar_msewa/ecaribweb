<%@taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html>
<html>
<head>
<title>VPayQwik Blog | Know more about your VPayQwik Wallet</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="${pageContext.request.contextPath}/resources/css/blogcss/bootstrap.css" rel='stylesheet' type='text/css' />
<link href="${pageContext.request.contextPath}/resources/css/blogcss/style.css" rel='stylesheet' type='text/css' />
<script src="${pageContext.request.contextPath}/resources/js/Blogs/jquery.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
</head>
<body>

<jsp:include page="/WEB-INF/jsp/Blogs/BlogHeader.jsp" />
        
        
    
	<div class="container">
    	<div class="col-md-7 col-md-offset-1">
        	<h3><b>Know more about your Vpayqwik Wallet: Security Features</b></h3><p>Vpayqwik wallet is the most preferred payment option for users. Vpayqwik is accepted at merchant locations online & offline. We are committed to ensure your account's security at all times and provide you with a good experience every time you pay using Vpayqwik.</p><img src="${pageContext.request.contextPath}/resources/images/blogs/c-1.jpg" alt="" class="img-responsive"/></a><br><p>Here is a quick overview on the security features & various fees related to your Vpayqwik Wallet</p><br>                     
			<h4><b>Security Features:</b></h4>
            <p>1. Your Vpayqwik ID + Password alone are not enough to access your Vpayqwik Wallet. Even if someone takes your Vpayqwik ID & Password, it is not possible to login as OTP is delivered to your registered mobile number when logging from a new device. So the only way to access your account is to use the combination of your Vpayqwik ID/Password + Your own device or New Device with OTP to your registered mobile number.</p>
            <p>2. App Lock Password: You can now enable your phone's default screen lock/Pin/Password/Pattern/Fingerprint as your 2nd level of security to access Vpayqwik. You will be asked to reconfirm you Pin/Password/Pattern/Fingerprint before you can make a payment using Vpayqwik Wallet. This deters any unauthorized use of your Wallet Account. More details on how to enable this feature</p>
            <p>The respective bank's network may not respond to our request due to downtime or network fluctuation at the their end. Banks typically take upto 7 working days to settle such transactions. Where banks are not able to settle, the money is rolled back into the customer's Vpayqwik Wallet with a notification.</p>
        </div>
        
        <jsp:include page="/WEB-INF/jsp/Blogs/BlogRightMenu.jsp" />
		</div>
    
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>