<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage=""%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<script>
function fgPass(){
		$("#agentModal").modal("hide");
		$("#forgot_password_agent").modal("show");
		$("#forgot_password_agent").modal({backdrop: false});
	}
</script>
<!--header-->

<div class="navbar">
  <nav class="navbar" data-spy="affix" data-offset-top="500" style="z-index: 9999;border-bottom: 1px solid white; border-radius:0; background: #005cac;">
  			<c:if test="${error ne null}">
				<div class="alert alert-danger col-md-12" id="alertdanger">
					<center>
						<c:out value="${error}" escapeXml="true" default="" />
					</center>
				</div>
			</c:if>
			<c:if test="${msg ne null}">
				<div class="alert alert-success col-md-12" id="successalert">
					<center>
						<c:out value="${msg}" escapeXml="true" default="" />
					</center>
				</div>
			</c:if>
			
  <div class="container-fluid" style="background-color:#e7be3e;">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle clpse_btn" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      
      <a id="_brand3" itemprop="brand" itemscope itemtype="http://schema.org/Brand" href="${pageContext.request.contextPath}/Home">
    <img itemprop="logo" src="${pageContext.request.contextPath}/resources/images/logo.png" style="width:100px;" id="brand"></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right" style="padding: 10px; margin-left: 2%;">
        <li class="nav_link"><a style="color: white;" itemprop="url" href="${pageContext.request.contextPath}/Home" class="smoothScroll">HOME</a></li>
        <li class="nav_link"><a style="color: white;" href="#works" class="smoothScroll">WHY US?</a></li>
        <li class="nav_link"><a style="color: white;" href="#about" class="smoothScroll">FEATURES</a></li>
        <li class="nav_link"><a style="color: white;" href="#testimonial" class="smoothScroll">TESTIMONIAL</a></li>        
        <li><a style="color: white;" href="#contact" class="smoothScroll">CONTACT US</a></li>

        <li class="dropdown">
          <a href="#" style="color: white;" class="dropdown-toggle" data-toggle="dropdown">SIGNUP | LOGIN <span class="caret"></span></a>
          <ul class="dropdown-menu" id="menu2">
            <li><a href="#" onclick="bodyfreezeScroll('userModal')">User</a></li>
            <li><a href="#" onclick="bodyfreezeScroll('merchntModal')">Merchant</a></li>            
            <li><a href="#" onclick="bodyfreezeScroll('agentModal')">Agent</a></li>
          </ul>
        </li>

      </ul>
    </div>
  </div>
</nav>
</div>
<!---end header--->
<!--Login modal for user-->
        <div class="modal fade" id="userModal" tabindex="-1" role="dialog" 
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" onclick="bodyUnfreezeScroll('userModal')" aria-hidden="true">
                            &times;</button>
                        <h4 class="modal-title" id="myModalLabel">
                            Login/Signup as User!</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12 extra-w3layouts">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#Login" data-toggle="tab">Login</a></li>
                                    <li><a href="#Registration" data-toggle="tab">Register</a></li>
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div class="tab-pane active" id="Login">
                                        <!-- <form  class="form-horizontal" action="#" method="post" id="login"> --> 
                                        <form class="form-horizontal"  action="<c:url value='${pageContext.request.contextPath}/User/Login'/>"  method="post">
                                          <div class="form-group">
                                              <!-- <label for="mobile" class="col-sm-3 control-label">
                                                  Mobile</label> -->
                                              <div class="col-md-12 col-sm-12 col-xs-12">
                                                  <input type="text" class="form-control numeric" placeholder="Enter Mobile Number"  name="username" id="username"
									onkeypress="clearvalue('error_username')" autocomplete="off" maxlength="10" required> 
								<p class="error" id="error_username" class="error"></p>
                                              </div>
                                          </div>
                                          <div class="form-group">
                                              <!-- <label for="exampleInputPassword1" class="col-sm-3 control-label">
                                                  Password</label> -->
                                              <div class="col-md-12 col-sm-12 col-xs-12">
                                                  <input type="password" name="password" class="form-control" id="password_login"
									onkeypress="clearvalue('error_login_password')" placeholder="Password"
									autocomplete="off" minlength="6" maxlength="6" required><i
									class="fa fa-eye-slash" id="login_pwd_eye"></i>
								<p class="error" id="error_login_password" class="error"></p>
                                              <a href="#" class="agileits-forgot" data-toggle="modal" onclick="bodyUnfreezeScroll('userModal')" data-target="#forgotPassword"
									id="forgot_password_modal">Forgot Password?</a>
                                              </div>
                                          </div>
                                          <div class="row">
                                              <div class="col-md-12 col-sm-12 col-xs-12">
                                                  <center><button type="submit" class="submit btn btn-primary btn-sm">
                                                      Submit</button></center>
                                              </div>
                                          </div>
                                        </form>
                                    </div>
                                    <div class="tab-pane" id="Registration">
                                        <form  class="form-horizontal" action="#" method="POST">
                                          <div class="form-group">
                                              <div class="col-md-12 col-sm-12 col-xs-12">
                                              <div class="row">
                                                  <div class="col-md-6 col-sm-6 col-xs-6">
                                                     <input type="text" class="form-control" placeholder="First Name" id="first_name"
							                                   onkeypress="return lettersOnly(event)" required>
						                             <p id="error_first_name" style="color: red;"></p>
						                          </div>
						                           <div class="col-md-6 col-sm-6 col-xs-6">
                                                     <input type="text" class="form-control" placeholder="Last Name" id="last_name"
							                                   onkeypress="return lettersOnly(event)" required>
						                             <p id="error_last_name" style="color: red;"></p>
						                          </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="form-group">
                                              <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="row">
                                                      <div class="col-md-6 col-sm-6 col-xs-6">
                                                          <input type="email" class="form-control"  name="email" id="email" placeholder="Email" required="required" aria-hidden="true" "@gmail.com"/>
                                                        <p id="error_email"  style="color: red;"></p>
                                                      </div>
                                                      <div class="col-md-6 col-sm-6 col-xs-6">
                                                          <input type="tel" class="form-control numeric" name="contactNo" id="contact_no" placeholder="Mobile" required="required" maxlength="10" />
                                                          <p  id="error_contact_no" style="color: red;"></p>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="form-group">
                                              <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="row">
                                                      <div class="col-md-6 col-sm-6 col-xs-6">
                                                          <input type="password" class="form-control" name="password" id="password" minlength="6" maxlength="6" placeholder="Password" required="required" />
                                                      <i class="fa fa-eye-slash" id="pwd_eye" style="float: right; margin-top: -25px; margin-left: 40px;"></i>
						                               <p class="error" id="error_password" class="error"  style="color: red;"></p>
                                                      </div>
                                                     <!--  <div class="col-md-6 col-sm-6 col-xs-6">
                                                          <input type="text" class="form-control numeric" name="locationCode" id="pincode" minlength="6" maxlength="6" placeholder="Pincode" required="required"/>
                                                      <p class="error" id="error_pincode" class="error" style="color: red;"></p>
                                                      </div> -->
                                                       <div class="col-md-6 col-sm-6 col-xs-6">
                                                	<input type="text" class="form-control numeric" id="dob" placeholder="Date of Birth" required="required"  readonly="readonly"/>
                                          			 <p class="error" id="error_dob" class="error" style="color: red;"></p>
                                            </div>
                                                  </div>
                                              </div>
                                          </div>
                                      <%--   <div class="form-group">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <select class="form-control"  id="secquestions">
                                                    <option value="#">Select your Security Question</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <input type="text" class="form-control" id="sec_ans" placeholder="Security Answer"/>
                                           <p class="error" id="error_sec_ans" class="error" style="color: red;"></p>
                                            </div>
                                        </div>
                                        <div style="z-index: 99; position: absolute; margin-left: 78%;">
						              <a class="captcha_link"><span class="glyphicon glyphicon-refresh" aria-hidden="true" style="float: right; margin-top: -10px; margin-left: 20px;"></span></a>
					                   </div>
                                        <div class="form-group">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <img src="<c:url value="/Captcha" />"
							                 class="img-responsive captcha_image" style="margin-top: -15px; position: absolute; width: 130px;" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <input type="text" class="form-control" id="g-recaptcha-response" placeholder="Enter text shown in image" required="required" />
                                            <p class="error" id="error_captcha" class="error" style="color: red;"></p>
                                            </div>
                                        </div> --%>
                                        <div class="form-group">
                                            <div class="checkbox-inline">
                                                <label><input type="checkbox" class="check" value="termsConditions" id="termsConditions"> 
                                               By clicking submit I hereby agree all the <a href="Terms&Conditions" target="_blank">terms and conditions</a></label>
                                            </div>
                                        <sec:csrfMetaTags />
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <center><button type="button" class="btn btn-success btn-md disabled" id="registerButton">
                                                    Save &amp; Continue</button></center>
                                            </div>
                                        </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <!--//Login modal-->

    <!--Login modal for Merchant-->
        <div class="modal fade" id="merchntModal" tabindex="-1" role="dialog" 
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" onclick="bodyUnfreezeScroll('merchntModal')" aria-hidden="true">
                            &times;</button>
                        <h5 class="modal-title" id="myModalLabel">
                            Login as Merchant</h5>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12 extra-w3layouts">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs">
                                    <!-- <li class="active"><a href="#mLogin" data-toggle="tab">Login</a></li> -->
                                    <!-- <li><a href="#mRegd" data-toggle="tab">Register</a></li> -->
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div class="tab-pane active" id="mLogin">
                                     <form  class="form-horizontal" action="#" method="post" id="merchantLogin">
                                        <!-- <form  class="form-horizontal" method="post" action="/Merchant/Home"> -->
                                        <div class="form-group">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <input type="text" class="form-control" name="username" id="merchant_username" placeholder="Email" required="required"  />
                                             <p class="error" id="merchant_error_username" class="error" style="color:red;"></p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <input type="password" class="form-control" name="password" id="merchant_password_login" placeholder="Password" required="required" />
                                                <!-- <a href="javascript:;" class="agileits-forgot" data-toggle="modal" data-target="#forgot_modal">Forgot password?</a> -->
                                                  <p class="error" id="merchant_error_password" class="error" style="color:red;"></p>
                                            </div>
                                        </div>
                                         <div class="row">
                                              <div class="col-md-12 col-sm-12 col-xs-12">
                                                  <center><button type="button"  id="merchant_loginButton" class="submit btn btn-primary btn-sm">
                                                      Submit</button></center>
                                              </div>
                                          </div>
                                        </form>
                                    </div>
                                    <div class="tab-pane" id="mRegd">
                                        <form  class="form-horizontal" action="#" method="POST">
                                        <div class="form-group">
                                            <div class="col-md-3 col-sm-3 col-xs-3">
                                                <input type="tel" class="form-control" id="mobile" value="+91" readonly="" />
                                            </div>
                                            <div class="col-md-9 col-sm-9 col-xs-9">
                                                <input type="tel" class="form-control" id="mobile" placeholder="Mobile" required="required" maxlength="10" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                                        <input type="text" class="form-control" placeholder="First Name" required="required" />
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                                        <input type="text" class="form-control" placeholder="Last Name" required="required" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                                        <input type="email" class="form-control" id="email" placeholder="Email" required="required" />
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                                        <input type="password" class="form-control" id="password" placeholder="Password" required="required" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <input type="text" class="form-control" id="address" placeholder="Address" required="required" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                                        <input type="text" class="form-control" id="city" placeholder="City" required="required" />
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                                        <input type="text" class="form-control" id="state" placeholder="State" required="required" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                                        <input type="text" class="form-control" id="pin" placeholder="Pincode" required="required" />
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                                        <input type="text" class="form-control" id="pan" placeholder="Pan No." required="required" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                                        <input type="text" class="form-control" id="adhar" placeholder="Aadhar No." required="required" />
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                                        <input type="text" class="form-control" id="accname" placeholder="Account Name" required="required" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                                        <input type="text" class="form-control" id="bankname" placeholder="Bank Name" required="required" />
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                                        <input type="text" class="form-control" id="accno" placeholder="Account Number" required="required" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <input type="text" class="form-control" id="ifsc" placeholder="IFSC Code" required="required" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="checkbox-inline">
                                                <label><input type="checkbox" name="Policy"> By clicking submit i hereby agree all the <a href="#">terms &amp; condition.</a></label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <center><button type="submit"  class="submit btn btn-primary btn-sm">
                                                    Continue</button></center>
                                            </div>
                                        </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <!--//Login modal merchant-->
    

    <!--Login modal for Agent-->
        <div class="modal fade" id="agentModal" tabindex="-1" role="dialog" 
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" onclick="bodyUnfreezeScroll('agentModal')" aria-hidden="true">
                            &times;</button>
                        <h5 class="modal-title" id="myModalLabel">
                            Login as Agent</h5>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12 extra-w3layouts">
                                <!-- Nav tabs -->
                                <!-- <ul class="nav nav-tabs">
                                    <li class="active"><a href="#Login" data-toggle="tab">Login</a></li>
                                </ul> -->
                                <!-- Tab panes -->
                                <div class="tab-content">
                            <div class="tab-pane active" id="Login">
                                         <!-- <form  class="form-horizontal" action="#" method="post" id="login">  -->
          
          <%-- <form id="login" action="<c:url value='/Agent/Login'/>" method="post">
				<div class="group">
					<input type="text"  class="form-control numeric" placeholder="Enter Mobile Number" name="username" autocomplete="off"
						 maxlength="10" required>
				</div>

				<div class="group">
					<input type="password" name="password" id="password_login" class="form-control"
						placeholder="Enter Password" autocomplete="off" minlength="6"
						maxlength="9" required> 
					<sec:csrfInput />
					<!-- <a href="#" data-toggle="modal"  id="forgot_password_agent"
						id="forgot_password_modal">Forgot Password? </a> -->
				 <a href="#" class="agileits-forgot" data-toggle="modal" onclick = "fgPass()" 
									>Forgot Password</a>		
				</div>
				<center>
					<button type="submit" class="btn">
						Login <i class="fa fa-sign-in fa-1x"></i>
					</button>
				</center>
				<br>
				<br>
			</form> --%>
			
			
			
			            <form class="form-horizontal"  action="<c:url value='/Agent/Home/Login'/>"  method="post">
            <div class="form-group">
           <div class="col-md-12 col-sm-12 col-xs-12">
              
				<input type="text"  class="form-control numeric" placeholder="Enter Mobile Number" name="username" autocomplete="off"
						 maxlength="10" required>
				<p class="error" id="agent_error_username" class="error" style="color:red;"></p>
          </div>
          </div>
                       <div class="form-group">
                     
                       <div class="col-md-12 col-sm-12 col-xs-12">
			   
			   <input type="password" name="password" id="password_login" class="form-control"
						placeholder="Enter Password" autocomplete="off" 
						maxlength="6" required> 
								<p class="error" id="agent_error_login_password" class="error" style="color:red;"></p>
         		<a href="#" class="agileits-forgot" data-toggle="modal" onclick = "fgPass()">Forgot Password</a>
                       </div>
                        </div>
                        <div class="row">
                              <div class="col-md-12 col-sm-12 col-xs-12">
                                   <center><button type="submit"  class="submit btn btn-primary btn-sm">
                                             Submit</button></center>
                              </div>
                        </div>
     </form>
			
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<!--//Login modal Admin-->
<!-- Modal after successful verification -->
<%-- <div id="forgot_password_agent" role="dialog" class="modal fade">
	<div class="modal-dialog" >
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5>Please contact to customer care number(8025535857)</h5>
			</div>
			<div class="modal-body">
				<center id=""
					class="alert alert-success"></center>
			</div>
		</div>
	</div>
</div> --%>

<div id="forgot_password_agent" role="dialog" class="modal fade">
		<div class="modal-dialog modal-sm">
			<center>
				<div class="modal-content" style="margin-top: 100px;">
					
					<img src="${pageContext.request.contextPath}/resources/images/agent.png" style="width: 30%;    margin-top: -32px;">
					<div class="modal-body" style="margin-top: -20px;">
						<%-- <center id="success_alert" class="alert alert-success"></center> --%>
						<h5><b>Please contact to customer care Number (8025535857)</b></h5>
					</div>
				</div>
			</center>
		</div>
</div>
	
<!--Forgot Password modal-->
        <div class="modal fade" id="forgotPassword" role="dialog">
            <div class="modal-dialog modal-sm" style="margin-top: 6em;">
                
                <!-- Modal Content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" onclick="bodyUnfreezeScroll('forgotPassword')">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Forgot Password?</h4>
                    </div>
                    <form  class="form-horizontal" action="#" method="POST" id="forgotForm">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12 extra-w3layouts">
                                        <div class="form-group">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <input type="text" class="form-control numeric" name="username" id="fp_username" onkeypress="clearvalue('fp_error_username')"
							placeholder="Mobile Number" maxlength="10" 
							autofocus />
							<p class="error" id="fp_error_username" style="color: red"></p>
                                            </div>
                                        </div>
                                        
                     <%--                    <div style="z-index: 99; position: absolute; margin-left: 78%;">
						<a class="captcha_link"><span
							class="glyphicon glyphicon-refresh" aria-hidden="true"
							style="margin-left: 80%;"></span></a>
					</div>
					<br> <img src="${pageContext.request.contextPath}/Captcha" height="80" class="captcha_image" />
					<br />
					<div class="form-group">
						<div class="col-md-12 col-sm-12 col-xs-12">
              <input id="g-recaptcha-response-2" class="form-control"
              onkeypress="clearvalue('error_captcha2')"
              placeholder="Enter text shown in image" type="text"/>
              <p class="error" id="error_captcha2" style="color: red"></p>
            </div>
					</div> --%>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <center><button type="button" class="submit btn btn-primary btn-sm" id="forgot_password_request">
                                                    Submit</button></center>
                                            </div>
                                        </div>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
<!--//Forgot Password modal-->

<!-- modal OTP for forget password -->
        <div class="modal fade" id="fpOTP" role="dialog">
            <div class="modal-dialog modal-sm" style="margin-top: 5em;">
                
                <!-- Modal Content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" onclick="bodyUnfreezeScroll('fpOTP')">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Change Password</h4>
                    </div>
                    <form  class="form-horizontal" action="#" method="POST" id="">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12 extra-w3layouts">
                                        <div class="form-group">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <input type="hidden" name="mobileNumber" class="numeric" id="fpusername_forgot" class="form-control" value="" required />
                                                <p class="error" id="fp_error_username" style="color: red"></p>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <input type="text" name=key class="form-control numeric" id="fpOTP_key" onkeypress="clearvalue('error_otp')" 
                                                placeholder="OTP" maxLength="6"  onkeydown="limit(this, 6);" onkeyup="limit(this, 6);"/>
                                                <p class="error" id="error_otp" class="error" style="color: red;margin-left:10px;"></p>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <input type="password" name=key class="form-control" id="fpnewPassword_key" onkeypress="clearvalue('error_new_password')"
              minlength="6" maxlength="6" placeholder="New Password" />
              <p class="error" id="error_new_password" class="error" style="color: red;margin-left:10px;"></p>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                          <div class="col-md-12 col-sm-12 col-xs-12">
                                            <input type="password" name=key class="form-control" id="fpconfirmPassword_key" onkeypress="clearvalue('error_confirm_password')"
                minlength="6" maxlength="6" placeholder="Confirm Password"
                required />
            <p class="error" id="error_confirm_password" class="error" style="color: red;margin-left:10px;"></p>
                                          </div>
                                      </div>

                                      <div class="form-group">
                                          <div class="col-md-12 col-sm-12 col-xs-12">
                                            <center>
            <button class="submit btn btn-primary btn-sm" 
              type="button" id="process_forgot_password_request">Continue</button>
          </center>
                                          </div>
                                      </div>

                                      <br>
                                     <%--  <div style="z-index: 99; position: absolute; margin-left: 78%;">
                                        <a class="captcha_link"><span
                                          class="glyphicon glyphicon-refresh" aria-hidden="true"
                                          style="margin-left: 80%;"></span></a>
                                      </div>
                                      <br> <img src="${pageContext.request.contextPath}/Captcha" height="50" class="captcha_image" />
                                      <br />

                                      <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                          <input id="g-recaptcha-response-3" type="text" class="form-control"
              placeholder="Enter text shown in image" required />
            <p class="error" id="error_captcha3" class="error"></p>
                                        </div>
                                      </div> --%>

                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <center>
            <button class="submit btn btn-primary btn-sm" 
              type="button" id="forgot_password_resend_otp" >Resend OTP</button>
          </center>
                                            </div>
                                        </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
          <center id="fpOTP_message"></center>
        </div>
                    </form>
                </div>
            </div>
        </div>
        
<!-- modal OTP for forget password -->
		
		<div id="successNotification" role="dialog" class="modal fade">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h5></h5>
					</div>
					<div class="modal-body">
						<center id="success_alert" class="alert alert-success"></center>
					</div>
				</div>
			</div>
		</div>

                    <div id="common_error" role="dialog" class="modal fade">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
								</div>
								<div class="modal-body">
									<center id="common_success" class="alert alert-danger"></center>
								</div>
							</div>
						</div>
					</div>
	
	<div id="common_success_true" role="dialog" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<center id="common_success_msg" class="alert alert-success"></center>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Modal after error verification -->
	<div id="common_success_message" role="dialog" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<center id="common_success_true" class="alert alert-danger"></center>
				</div>
			</div>
		</div>
	</div>

<div id="common_modal" role="dialog" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<p class="error" id="common_error" class="error"></p>
				</div>
			</div>
		</div>
	</div>
	
			<!-- Modal after successful verification -->
		<div id="verifiedMessage" role="dialog" class="modal fade">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h5>Verification Successful</h5>
					</div>
					<div class="modal-body">
						<center id="success_verification_message"
							class="alert alert-success"></center>
					</div>
				</div>
			</div>
		</div>

		<!-- Modal after error verification -->
		<div id="errorMessage" role="dialog" class="modal fade" style="z-index: 99999;margin-top: 5em;">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-body">
						<center id="error_message" class="alert alert-danger"></center>
					</div>
				</div>
			</div>
		</div>

