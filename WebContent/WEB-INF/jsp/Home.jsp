<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage=""%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html lang="en">
<head>
 <meta charset="utf-8">
 <sec:csrfMetaTags/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="X-Frame-Options" content="deny">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>eCarib – Digital &amp; Utility Payment, Entertainment, Travel &amp; more Online !</title>
    <meta name="title" content="eCarib.com – Digital &amp; Utility Payment, Entertainment, Travel &amp; more Online !">
    <meta name="description" content="eCarib.com - Make Instant Digital &amp; Utility Payment, grab latest Entertainment Tickets Online with Great Offers and Discounts and let your Travel be much easier &amp; fun-filled!">
    
    <link rel="icon" href='<c:url value="/resources/images/favicon.png"/>' type="image/png"/>
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/new-css/custome.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/new-css/style.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/new-css/animate.min.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/new-css/et-line-font.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/new-css/font-awesome.min.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/new-css/dashlogin.css">
  <link rel="stylesheet" type="text/css" href="http://propeller.in/components/button/css/button.css">
  <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="stylesheet" type="text/css" href="http://propeller.in/components/icons/css/google-icons.css">
  
  <link href="http://fonts.googleapis.com/css?family=Asap" rel="stylesheet"><!--Fonts -->
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/header.js"></script>
	<script src="<c:url value='/resources/js/jquery.js'/>"></script>
   <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/signup.js"></script>
   <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/Login.js"></script>
   <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/MerchantLogin.js"></script>
   <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/AgentLogin.js"></script>
  <script src="<c:url value='/resources/js/reset.js'/>"></script>
<script src="<c:url value='/resources/js/modernizr.js'/>"></script>

   <script type="text/javascript">
	$(window).load(function() {
		$(".se-pre-con").fadeOut("slow");
		start();
	});
   </script>

<script>
	function getQuestions() {
		$.ajax({
			url : "${pageContext.request.contextPath}/Api/v1/User/Website/en/WebRegistration/Questions",
			type : "GET",
			datatype : 'json',
			contentType : "application/json",
			success : function(response) {
				var a = response + " ";
				var y = new Array();
				y = a.split(",");
				document.getElementById("secquestions").innerHTML = "";
				var option = document.createElement("option");
				var select = document.getElementById("secquestions");
				var j;
				option.text = "Select your Security Question";
				option.value = "#";
				select.add(option);
				for (j = 0; j < y.length; j++) {
					var z = Array();
					var option = document.createElement("option");
					z = y[j].split("#");
					option.text = z[1];
					option.value = z[0];
					select.add(option);
				}
			}
		});
	}
	     function successAlert(){
        $("#successalert").fadeTo(2000, 500).slideUp(500, function(){
       $("#successalert").slideUp(500);
        });   
    }
	     function dangerAlert(){
	         $("#alertdanger").fadeTo(2000, 500).slideUp(500, function(){
	        $("#alertdanger").slideUp(500);
	         });   
	     }
	     function dateOfBirth(){ 
	     $("#dob").datepicker({
				format : "yyyy-mm-dd"
			});
	     }
	 	function start(){
	 		successAlert();
	 		dangerAlert();
	 	//	getQuestions();
	 		dateOfBirth();
		}
</script>
<link rel="stylesheet"
	href="<c:url value="/resources/css/datepicker.css"/>">
<script src="<c:url value="/resources/js/datepicker.js"/>"></script>
<!-- <script>
	$(function() {
		$("#dob").datepicker({
			format : "yyyy-mm-dd"
		});
	});
</script> -->
   
</head>


<body>

	<!-- Navbar -->
	<jsp:include page="/WEB-INF/jsp/Header.jsp" />
	<!-- /Navbar -->
	<div class="container-fluid" id="header">
<!--slider-->
<script>
    $(function() {
      $(".anim_image").show();      
    });
</script>
<c:if test="${error ne null}">
			<%-- <div class="alert alert-danger col-md-12">
				<center>
					<c:out value="${error}" escapeXml="true" default="" />
				</center>
			</div> --%>
</c:if>
 <div class="container">
    <!--header img-->
        <div class="col-md-12 adv_wrap">
          <div class="col-md-3 col-sm-3 col-xs-4 it_wrap1">
            <div class="img_text">
              <img src="${pageContext.request.contextPath}/resources/images/secure.png" class="img-responsive adv_img">
              <h4>Secure</h4>
              <p>Your wallet is as secure as your Bank account.</p>
            </div><br>
            <div class="img_text">
              <img src="${pageContext.request.contextPath}/resources/images/convenient.png" class="img-responsive adv_img">
              <h4>Convenient</h4>
              <p>Use your wallet from anywhere and any time.</p>
            </div>
          </div>
          <div class="col-md-3 col-sm-3 col-xs-4 it_wrap2" style="">
            <div class="img_text">
              <img src="${pageContext.request.contextPath}/resources/images/easy-use.png" class="img-responsive adv_img">
              <h4>Hassle Free</h4>
              <p>Very easy and quick option to make your various payments.</p>
            </div><br>
            <div class="img_text">
              <img src="${pageContext.request.contextPath}/resources/images/one_step.png" class="img-responsive adv_img">
              <h4>One Stop Solution</h4>
              <p>No need to download lot of apps in your mobile. Use eCarib for all type of transactions.</p>
            </div>
          </div>
        	<div class="col-md-8 col-sm-5">
            <img src="${pageContext.request.contextPath}/resources/images/e_carib_splash.gif" style="width:60%;margin-left:100%;" class="anim_image">
			<div style="margin-top: -90px; padding-bottom: 70px;">
				<%-- <a href="#" target="_blank"><img src="${pageContext.request.contextPath}/resources/images/google_play.png" class="img-responsive dwn_icon-g"></a>
				<a href="#" target="_blank"><img src="${pageContext.request.contextPath}/resources/images/ios.png" class="img-responsive dwn_icon-a"></a> --%>
				<a href="#"><img src="${pageContext.request.contextPath}/resources/images/google_play.png" class="img-responsive dwn_icon-g"></a>
				<a href="#"><img src="${pageContext.request.contextPath}/resources/images/ios.png" class="img-responsive dwn_icon-a"></a>
			</div>
          </div>
        </div>
    <!--end header img-->
  </div>
</div>
<!--end slider-->

<!---WHY US?-->
	<%-- <div class="container-fluid" id="works" style=" padding: 4%;">
    	<div class="container">
        <h2><b>WHY US?</b></h2>
					<hr>
        
        	<div class="col-lg-4 col-md-4 col-sm-4 wow fadeInUp" data-wow-delay="0.3s">
				<center><img src="${pageContext.request.contextPath}/resources/images/4.png" class="img-responsive whyimg" alt=""></center>
					<h3>USER FRIENDLY &amp; SIMPLE</h3>
					<hr>
					<p>eCarib offers a very user friendly interface that is simple to understand and use. Its home page layout with easy to recognise icons make life easy for our users.</p>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 wow fadeInUp" data-wow-delay="0.5s">
				<center><img src="${pageContext.request.contextPath}/resources/images/5.png" class="img-responsive whyimg" alt=""></center>
					<h3>3-TIER ARCHITECTURE SECURITY</h3>
					<hr>
					<p>Trust is the way we work. Our 3-Tier Architecture Security System built by our team makes sure that customer data and balance is as well protected as in any bank. </p>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 wow fadeInUp" data-wow-delay="0.7s">
				<center><img src="${pageContext.request.contextPath}/resources/images/cp-points-for-stays-icon.png" class="img-responsive whyimg" alt=""></center>
					<h3>LOYALITY POINTS FOR OUR LOYAL CUSTOMERS</h3>
					<hr>
					<p>Our loyal customers are awarded with loyalty points and cash back offers for continuous use of our services which they can use to make valuable purchases from our wallet. </p>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 wow fadeInUp" data-wow-delay="0.7s">
				<center><img src="${pageContext.request.contextPath}/resources/images/2.png" class="img-responsive whyimg" alt=""></center>
					<h3>WIDE RANGE OF PRODUCTS &amp; SERVICES</h3>
					<hr>
					<p>eCarib offers a multitude of services and products like Top-up, Fund Transfer, Gift Cards, Tickting services, etc to make our customers lives very easy.</p>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 wow fadeInUp" data-wow-delay="0.7s">
				<center><img src="${pageContext.request.contextPath}/resources/images/3.png" class="img-responsive whyimg" alt=""></center>
					<h3>EXCELLENT CUSTOMER SUPPORT</h3>
					<hr>
					<p>We have 24 x 7 Customer Support in multiple languages that will be available to help and guide our customers at any stage of their transaction or should they face issues. </p>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 wow fadeInUp" data-wow-delay="0.7s">
				<center><img src="${pageContext.request.contextPath}/resources/images/6.png" class="img-responsive whyimg" alt=""></center>
					<h3>BHARAT QR COMPATIBLE</h3>
					<hr>
					<p>eCarib is proud to be one of the first wallets that have enabled the Bharat QR into their wallets whereby our users can even pay with Merchants that do not have a eCarib account. </p>
			</div>
        </div>
    </div> --%>
<!--end WHY US?-->
<div></div>
<!-- =========================
    VIDEO SECTION   
============================== -->
<%-- <section id="video" class="parallax-section">
  

      <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="false">
            <!-- Indicators -->
            <!-- <ol class="carousel-indicators sr-only">
              <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
              <li data-target="#myCarousel" data-slide-to="1"></li>
              <li data-target="#myCarousel" data-slide-to="2"></li>
            </ol> -->

            <!-- Wrapper for slides -->
            <div class="carousel-inner video_carousel">
              <div class="item active">
                <div class="wow fadeInUp text-center" data-wow-delay="0.5s"><br>
                  <h3>eCarib The Complete Payment App</h3>
                  <P>Going cashless is easy! Pay bills, transfer money, book travel tickets<br> and perform a host of other functions through the eCarib app. </p><br>
                    <div class="embed-responsive embed-responsive-16by9 intro_vdo">
                      <center><iframe width="560" height="315" src="https://www.youtube.com/embed/OqHGnTUzWbc?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe></center>
                    </div>
                </div>
              </div>
            </div>

            <!-- Left and right controls -->
            <!-- <a class="left carousel-control" href="#myCarousel" data-slide="prev">
              <i class="glyphicon-chevron-left fa fa-arrow-circle-left" style="font-size:30px"></i>
        
            </a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">
              <i class="glyphicon-chevron-right fa fa-arrow-circle-right" style="font-size:30px"></i>
            </a> -->
        </div>
</section>
 --%>

<!-- about section
============================================================================= -->
<%-- <section id="about">
  <div class="container" id="function">
    <div class="row">
      <div class="col-md-12 col-sm-12 text-center">
        <div class="section-title">
          <!-- <strong>02</strong> -->
          <!-- Feature Travel -->
          <h1 class="heading bold trvl_head">Travels</h1>
          <!-- <hr> -->
        </div>
      </div>
    </div>
        <div class="row">
            <div class="col-md-6 col-sm-6" style="padding-top: 30px;">
                <img class="img-travel" src="${pageContext.request.contextPath}/resources/images/webside-animation_travel.gif">
            </div>
            <div class="col-md-6 col-sm-6" id="fea-travl" style="padding: 50px 0 50px 10px;">
                <div class="col-md-6 col-sm-6" style="text-align: center; padding: 20px 0;">
                <center><img src="${pageContext.request.contextPath}/resources/images/4.png" class="img-responsive whyimg" alt=""></center>
                    <h4>Easy to Book</h4>
                    <p>Book Flight/Bus tickets with relative ease.</p>
                </div>
                <div class="col-md-6 col-sm-6" style="text-align: center; padding: 20px 0;">
                <center><img src="${pageContext.request.contextPath}/resources/images/66.png" class="img-responsive whyimg" alt=""></center>
                    <h4>Instant Pay</h4>
                    <p>Easy to make payment directly from the wallet.</p>
                </div>
                <div class="col-md-6 col-sm-6" style="text-align: center; padding: 20px 0;">
                <center><img src="${pageContext.request.contextPath}/resources/images/25.png" class="img-responsive whyimg" alt=""></center>
                    <h4>Travel Anywhere</h4>
                    <p>Travel to any destination within the country and internationally.</p>
                </div>
                <div class="col-md-6 col-sm-6" style="text-align: center; padding: 20px 0;">
                <center><img src="${pageContext.request.contextPath}/resources/images/77.png" class="img-responsive whyimg" alt=""></center>
                    <h4>Get Discounts</h4>
                    <p>Frequent users are given loyalty points to get discounts on bookings.</p>
                </div>
            </div>
        </div>
  </div><hr class="feature_division">
  <br>

  <!-- Frature Gift Cards -->
    <div class="container featre_pad">
        <div class="row">
            <div class="col-md-12 col-sm-12 text-center">
                <div class="section-title">
                    <!-- <strong>02</strong> -->
                    <h1 class="heading bold">Gift Cards</h1>
                    <!-- <hr> -->
                </div>
            </div>
        </div>
        <div class="row" style="padding-top: 30px;">
            <div class="col-md-6 col-sm-6">
                <div class="col-md-6 col-sm-6 gift_1" style="text-align: center; padding: 20px 0;">
                <center><img src="${pageContext.request.contextPath}/resources/images/6.png" class="img-responsive whyimg" alt=""></center>
                    <h4>All Major Brands</h4>
                    <p>All gift cards available are from major international and national brands.</p>
                </div>
                <div class="col-md-6 col-sm-6 gift_2" style="text-align: center; padding: 20px 0;">
                <center><img src="${pageContext.request.contextPath}/resources/images/55.png" class="img-responsive whyimg" alt=""></center>
                    <h4>Easy to use</h4>
                    <p>Gift Cards will be sent to the registered E-mail ID and can be used conveniently.</p>
                </div>
                <div class="col-md-6 col-sm-6 gift_3" style="text-align: center;margin-left: 135px; padding: 20px 0;">
                <center><img src="${pageContext.request.contextPath}/resources/images/88.png" class="img-responsive whyimg" alt=""></center>
                    <h4>Gifting Made Easier</h4>
                    <p>Send gift cards to your loved ones to make their life exciting.</p>
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
                <img class="img-gift" src="${pageContext.request.contextPath}/resources/images/gift.gif">
            </div>
        </div>
    </div>
    <hr class="feature_division">
    <br>

    <!-- Feature Bharat QR -->
    <div class="container featre_pad">
        <div class="row">
            <div class="col-md-12 col-sm-12 text-center">
                <div class="section-title">
                    <!-- <strong>02</strong> -->
                    <h1 class="heading bold">Bharat QR</h1>
                    <!-- <hr> -->
                </div>
            </div>
        </div>
        <div class="row" style="padding-top: 30px;">
            <div class="col-md-6 col-sm-6">
                <img class="img-bqr" src="${pageContext.request.contextPath}/resources/images/qr-code.gif">
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="col-md-6 col-sm-6 gift_1" style="text-align: center; padding: 20px 0;">
                <center><img src="${pageContext.request.contextPath}/resources/images/6.png" class="img-responsive whyimg" alt=""></center>
                    <h4>One QR for all types of Cards</h4>
                    <p>Be it any type of card, just scan and make payment at the store.</p>
                </div>
                <div class="col-md-6 col-sm-6 gift_2" style="text-align: center; padding: 20px 0;">
                <center><img src="${pageContext.request.contextPath}/resources/images/store.png" class="img-responsive whyimg store_img" alt=""></center>
                    <h4>Thousands of merchants under Bharat QR</h4>
                    <p>Experience seamless cash free transactions using Bharat QR over a hundred thousand locations.</p>
                </div>
                <div class="col-md-6 col-sm-6 gift_3" style="text-align: center; margin-left: 135px; padding: 20px 0;">
                <center><img src="${pageContext.request.contextPath}/resources/images/purchase.png" class="img-responsive whyimg" alt=""></center>
                    <h4>Cheaper cashless payments</h4>
                    <p>Go cashless via Bharat QR with no transaction charges.</p>
                </div>
            </div>
        </div>
    </div>
</section> --%>

<!-- portfolio section
=============================================================================================== -->
<%-- <section id="testimonial">
    <div class="testimonials-container section-container section-container-image-bg">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 testimonials section-description">
                        <h1 class="heading bold">TESTIMONIAL</h1><hr>
                        <!-- <p class="medium-paragraph">Take a look below to learn what our customer are saying about us:</p> -->
                    </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="embed-responsive embed-responsive-16by9 testi_vdeo">
                      <center><iframe width="560" height="315" src="https://www.youtube.com/embed/0bL-UL9t9Tg?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe></center>
                    </div>
                  </div>
                </div>
      <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="carousel slide" data-ride="carousel" id="quote-carousel">
                    <!-- Carousel Slides / Quotes -->
                    <div class="carousel-inner text-center">
                        <!-- Quote 1 -->
                        <div class="item active">
                            <blockquote>
                                <div class="row">
                                    <div class="col-sm-8 col-sm-offset-2">
                                        <p><i class="fa fa-twitter" style="color: #00aced; font-size: 25px;"></i> Tried eCarib. Love the secure and quick functionality.</p>
                                        <!-- <small>Name</small> -->
                                    </div>
                                </div>
                            </blockquote>
                        </div>
                        <!-- Quote 2 -->
                        <div class="item">
                            <blockquote>
                                <div class="row">
                                    <div class="col-sm-8 col-sm-offset-2">
                                        <p><i class="fa fa-facebook" style="color: #3b5998; font-size: 25px;"></i> eCarib, only wallet with Bharat QR for easy and quick transactions.</p>
                                        <!-- <small>Kiru Ramachandra</small> -->
                                    </div>
                                </div>
                            </blockquote>
                        </div>
                        <!-- Quote 3 -->
                        <div class="item">
                            <blockquote>
                                <div class="row">
                                    <div class="col-sm-8 col-sm-offset-2">
                                        <p>Nice app given by Vijaya bank. Wonderful app. Quick n fast. Must have it.</p>
                                        <!-- <small>Jyoti Agrawalla</small> -->
                                    </div>
                                </div>
                            </blockquote>
                        </div>
                    </div>
                    <!-- Bottom Carousel Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#quote-carousel" data-slide-to="0" class="active"><!-- <img class="img-responsive" src="images/user-male.png" alt=""> -->
                        </li>
                        <li data-target="#quote-carousel" data-slide-to="1"><!-- <img class="img-responsive" src="images/user-male.png" alt="">
                        </li> -->
                        <li data-target="#quote-carousel" data-slide-to="2"><!-- <img class="img-responsive" src="images/user-female.png" alt="">
                        </li> -->
                    </ol>

                    <!-- Carousel Buttons Next/Prev -->
                    <a data-slide="prev" href="#quote-carousel" class="left carousel-control"><i class="fa fa-chevron-left"></i></a>
                    <a data-slide="next" href="#quote-carousel" class="right carousel-control"><i class="fa fa-chevron-right"></i></a>
                </div>
            </div>
        </div>
    </div>
        </div>
</section> --%>
<!--OTP modal-->
		<div id="newOTPModal" class="modal fade" role="dialog" style="z-index: 99999;margin-top: 5em;">
			<div class="modal-dialog modal-sm">
				<div class="modal-content text-center">
					<button type="button" onclick="bodyUnfreezeScroll('newOTPModal')" class="close">&times;</button>
					<div class="icon">
						<span class="fa fa-unlock-alt fa-2x" aria-hidden="true"></span>
					</div>
					<h4>OTP Verification</h4>
					<hr style="margin-top: 20px;">

					<div class="group_1" style="margin-left: 30px;">
						<input type="text" class="numeric" name="key" id="verify_reg_otp_key" maxlength="6"
							required="required"> <span class="highlight"></span> <label>OTP</label>
					</div>
					<div class="group_1">
						<input type="hidden" name="mobileNumber"
							class="form-control input-sm" id="reg_otp_username"
							required="required" />
					</div>
					<div class="col-md-6"
						style="float: none; margin-left: auto; margin-right: auto;">
						<button class="btn btn-md btn-block btncu"
							style="margin-bottom: 5px;width: 50%;margin-left:20%;" id="register_verify_mobile">Verify
							Mobile</button>
					</div>
					<br>
					
					<%-- <div class="group_1">
						<div style="z-index: 99; position: absolute; margin-left: 78%;">
							<a href="#" class="captcha_link"><span
								class="glyphicon glyphicon-refresh" aria-hidden="true"
								style="margin-left: 80%;"></span></a>
						</div>
						<br> <img src="<c:url value="/Captcha"/>"
							class="captcha_image" height="50" />
					</div>
					<div class="group_1">
						<input id="g-recaptcha-response-1" type="text" required
							style="margin-top: -25px;" />
						<p class="error" id="error_captcha1" class="error"></p>
						<!-- <label>Enter text shown in image</label> -->
					</div> --%>
					
					<div class="group_1">
						<button class="btn btn-md btncu" id="register_resend_otp"
							style="margin-bottom: 5px; margin-left: -20px;">Resend
							OTP</button>
					</div>

					<div class="modal-footer">
						<div class="alert alert-success" id="regMessage_success"></div>
					</div>
				</div>
			</div>
		</div>
<!--//OTP modal-->


<!--device binding OTP modal-->
	<div id="loginOTPVerification" data-backdrop="static" class="modal fade" role="dialog">
		<div class="modal-dialog modal-sm" style="margin-top: 80px;">
			<div class="modal-content text-center">
				<div class="modal-header">
					<button type="button" onclick="bodyUnfreezeScroll('loginOTPVerification')" class="close">&times;</button>
				<div class="icon">
					<span class="fa fa-unlock-alt fa-2x" aria-hidden="true"></span>
				</div>
				<h4 style="color:white;">OTP Verification</h4>
				</div>
				
				<div class="modal-body" style="height: 161px;">
					<div class="row">
						<div class="col-md-12 extra-w3layouts">
					<input type="text" class="numeric" name="key" id="verify_otp_key" maxlength="6" placeholder="OTP"
						    onkeypress="clearvalue1('error_otp','logMessage_success')"> <span class="highlight"></span>
						   <p id="error_otp1" style="color:Red"></p>
				</div><br><br>
				<div class="group_1">
					<input type="hidden" name="mobileNumber"
						   class="form-control in/put-sm" id="login_otp_username"
						   required="required" />
				</div>
				<div class="group_1">
					<input type="hidden" name="password"
						   class="form-control inp/ut-sm" id="login_otp_password"
						   required="required" />
				</div>
				<div class="group_1">
					<input type="hidden" name="ipAddress"
						   class="form-control in/put-sm" id="login_otp_ip_addrss"
						   required="required" />
				</div>
				<center style="padding: 10px;" id="logMessage_success"></center> 
				<div class="row">
                	<div class="col-md-12">
                		<button class="submit btn resend-otp" id="login_resend_OTP"
							style="margin-bottom: 5px;margin-left: 0px;">Resend OTP</button>
						&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                        <button type="button" class="submit btn"
							style="margin-bottom: 5px; margin-left: -10px; margin-right:10px;" id="login_verify_mobile">Continue</button>
                	</div>
				  </div>
						</div>
					</div>
				</div>
				<br>
		
				<!-- div class="modal-footer">
					<div class="alert alert-success text-center" id="logMessage_success"></div>
				</div> -->
			</div>
		</div>
		</div>
<!--//OTP modal-->

<!-- Footer -->
	<jsp:include page="/WEB-INF/jsp/Footer.jsp" />
  
<!-- /Footer -->


<!------script------>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	 <script type="text/javascript"
			 src='<c:url value="/resources/js/wow.js"/>' />
	 <script>
		 new WOW().init();
	 </script>
<script src="${pageContext.request.contextPath}/resources/js/smoothscroll.js"></script>
<!-- Datepicker script -->
<script>
    $( function() {
    $( "#date_birth" ).datepicker(new Date());
} );
</script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>

<!-- Navbar toggle animation -->
<script>
  $(document).ready(function () {
        $(".navbar-toggle").on("click", function () {
            $(this).toggleClass("active");
        });
    });
   function limit(element, max) {    
	    var max_chars = max;
	    if(element.value.length > max_chars) {
	        element.value = element.value.substr(0, max_chars);
	    } 
	}
</script>
<script>
	$(document).ready(function() {
		function disableBack() {
			window.history.forward();
		}
		window.onload = disableBack();
		window.onpageshow = function(evt) {
			if (evt.persisted)
				disableBack();
		}
	});
</script>



<!-- <script type="text/javascript">
    window.history.forward();
    function noBack() { window.history.forward(); }
</script> -->

<script>
	$("#forgotPassword").click(function(){
		$("#userModal").modal("hide");
	});
	</script>

<script>
	function clearvalue(val) {
		$("#" + val).text("");

	}

	function clearvalue1(val, val1) {
		$("#" + val).text("");
		$("#" + val1).text("");
	}
	
	function lettersOnly(evt) {
	    evt = (evt) ? evt : event;
	    var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode :((evt.which) ? evt.which : 0));
	    if (charCode > 31 && (charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122)) {
	     return false;
	    }
	    return true;
	}
</script>

<!-- Scrip for hide scrollbar when modal open -->
<script>
      function bodyfreezeScroll(modelid) {
          
         $('body').css('overflow','hidden');
         $('body').css('position','fixed');

         $("#"+modelid).modal('show');
      }

function bodyUnfreezeScroll(modelid) {
          
         $('body').css('overflow','');
         $('body').css('position','');

         $("#"+modelid).modal('hide');
      }
      
  </script>

</body>
</html>