<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="_csrf_parameter" content="_csrf" /><meta name="_csrf_header" content="X-CSRF-TOKEN" /><meta name="_csrf" content="890f6f82-b3cd-4149-98e4-463b3dda2490" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Welcome to VPayQwik</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

 <style>
        /*404 page*/
/* Import fonts */

@import url(https://fonts.googleapis.com/css?family=Lato:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic);

@import url(line-icons.css);

body {
    color: #797979;
    background: #eeeeee;
    font-family: 'Lato', sans-serif;
    padding: 0px !important;
    margin: 0px !important;
    font-size:14px !important;    
}

h1,h2,h3,h4,h5{
    font-weight: 300;
}

label{
    font-weight: 300;   
}

ul li {
    list-style: none;
}

a, a:hover, a:focus {
    text-decoration: none;
    outline: none;
    outline: 0;
}

p {
    margin: 0 0 10px;
}

input:focus, textarea:focus { outline: none; }

table tr th { color: #688a7e;}

*:focus {outline: none;}

::selection {
    background: #688a7e;
    color: #fff;
}

::-moz-selection {
    background: #688a7e;
    color: #fff;
}

#container {
    width: 100%;
    height: 100%;
}

.page-404{
  position:relative;
  width:350px;
  height:200px;    
  margin:100px auto;
  text-align: center;
}

.text-404{    
  font-size:138px;
  background:#e9e3dd;
  background-image: url('https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcQTdWeWNteABnbzguiXSBxmfc8Z8nh_HHoWoK35QL81x4HnpVmgvQ');
  background-repeat: no-repeat;
  background-size: 200% 200%;
  background-position: 100% 100%;
  -webkit-animation: square 3s linear infinite;
  -ms-animation: square 3s linear infinite;
  animation: square 3s linear infinite;
  -webkit-background-clip: text;
  color:transparent;  
  text-align:center;
  line-height:200px;
  position:relative;
}

.text-404:nth-of-type(2){
  text-shadow:none;
  z-index:2;
  position:absolute;
  top:-139px;
  left:-1px;
}

@-webkit-keyframes square {
  0% { background-position: 0 0; }
  25% { background-position: 100% 0; }
  50% { background-position: 100% 100%; }
  75% { background-position: 0 100%; }
  100% { background-position: 0 0; }
}

@-ms-keyframes square {
  0% { background-position: 0 0; }
  25% { background-position: 100% 0; }
  50% { background-position: 100% 100%; }
  75% { background-position: 0 100%; }
  100% { background-position: 0 0; }
}

@keyframes square {
  0% { background-position: 0 0; }
  25% { background-position: 100% 0; }
  50% { background-position: 100% 100%; }
  75% { background-position: 0 100%; }
  100% { background-position: 0 0; }
}
    </style>

</head>
<body style="background: rgba(205,205,205,1.00);">



 <div class="page-404">
    <p class="text-404">403</p>

    <h2>ACCESS DENIED</h2>
    <p>You do not have the permission to view this resource. <br><a href="https://tripay.in/">Return Home</a></p>
  </div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
</body>
</html>