<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page isELIgnored="false"%>
<%@ page import="org.springframework.web.util.HtmlUtils" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html lang="en">
<%-- <%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%> --%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicontripays, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<sec:csrfMetaTags/>

<title>eCarib | Users List</title>
<!-- Bootstrap core CSS -->
<!-- for pagination -->
<%-- <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/flaviusmatis-simplePagination.js-07c3728/simplePagination.css" type="text/css"> --%>
<%--   <script src="${pageContext.request.contextPath}/resources/flaviusmatis-simplePagination.js-07c3728/jquery.simplePagination.js" ></script> --%>
<!-- for pagination ends -->
<link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'
	type="image/png" />
<link
	href="${pageContext.request.contextPath}/resources/admin/css/bootstrap.min.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/admin/fonts/css/font-awesome.min.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/admin/css/animate.min.css"
	rel="stylesheet">
<!-- Custom styling plus plugins -->
<link
	href="${pageContext.request.contextPath}/resources/admin/css/custom.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/admin/css/icheck/flat/green.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/fixedHeader.bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/responsive.bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/scroller.bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<script	src="${pageContext.request.contextPath}/resources/admin/js/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/Admin/jquery.twbsPagination.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/Admin/jquery.twbsPagination.min.js"></script>


<script>
    var handleDataTableButtons = function() {
        "use strict";
        0 !== $("#datatable-buttons").length
        && $("#datatable-buttons").DataTable({
            columnDefs: [
                {
                    type: 'date-dd-mmm-yyyy',
                    targets: 3
                }
            ],
            dom : "Bfrtip",
            buttons : [ {
                extend : "copy",
                className : "btn-sm"
            }, {
                extend : "csv",
                className : "btn-sm"
            }, {
                extend : "excel",
                className : "btn-sm"
            }, {
                extend : "pdf",
                className : "btn-sm"
            }, {
                extend : "print",
                className : "btn-sm"
            } ],
            responsive : !0
        })
    }, TableManageButtons = function() {
        "use strict";
        return {
            init : function() {
                handleDataTableButtons()
            }
        }
    }();
</script>



	<script type="text/javascript">
// 	var flag=0;
// 	function searchIt(){
// 		console.log("inside search it");
// 		var num=document.getElementById("mobile").value;
// 		alert(num);
// 		$.ajax({
// 			type:"POST",
// 			url:"${pageContext.request.contextPath}/Admin/UserListOnSearch?search="+num,
// 			dataType:"json",
// 			success:function(data){
// 				var trHTML='';
// 				$(".testingg").empty();
// 				$(data.jsonArray).each(function(i,item){trHTML += '<tr class="testingg"><td>'+i+'</td><td>UserName:' + data.jsonArray[i].username + '<br>Mobileno.:'+ data.jsonArray[i].contactNo+'<br>email id:'+ data.jsonArray[i].email+'</td>'+'<td>user type:'+ data.jsonArray[i].kycNonkyc+'<br>DOB:'+data.jsonArray[i].dob+'<br>Gender:'+data.jsonArray[i].gender+'<br>Authority:'+data.jsonArray[i].authority+'</td>'+'<td>Balance:'+data.jsonArray[i].balance+'<br>points:'+data.jsonArray[i].points+'</td>'+'<td>'+data.jsonArray[i].dateOfAccountCreation+'</td></tr>';});
// 				$('#editedtable').append(trHTML);
// 				console.log("search completed wit display");
// 			}
				
// 			});
		
// 	}	
// 		$(function () {
//         var obj = $('#paginationn').twbsPagination({
//             visiblePages: 10,
//             onPageClick: function (event, page) {
//                 console.info(page);
//             }
//         });
//     });
	
	 function fetchMe(value){
		//  var tag = $(this);
		//  var paging=document.getElementById('kick2').innerHTML;
		var paging=value;
		
		 // paging=tag.;
		console.log("paging == " + paging);
		
	$.ajax({
	type:"POST",
	url:"${pageContext.request.contextPath}/Admin/WalletBalOustanding",
	data:{page:paging,size:'10'},
	dataType:"json",
	success:function(data){
	var trHTML='';
		if(trHTML==''){
		$(".testingg").empty();
		$(data.jsonArray).each(function(i,item){
			
			var mStatus;
			if (data.jsonArray[i].mobileStatus==="null" ) {
				console.log("Null");
				mStatus=data.jsonArray[i].mobileStatus;
				}
			else if (data.jsonArray[i].mobileStatus!=null) {
			mStatus=data.jsonArray[i].mobileStatus;
			console.log("Not Empty");
			}
			else {
				mStatus="NA";
				console.log("Empty");
			}
			
			trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td><a href="${pageContext.request.contextPath}/Admin/User/'+data.jsonArray[i].mobile+'">'+ data.jsonArray[i].mobile+'</a></td><td>'+ data.jsonArray[i].name+'</td>'+'<td>'+data.jsonArray[i].email+'</td>'+'<td>'+data.jsonArray[i].balance+'</td>'+'<td>'+data.jsonArray[i].date+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'<td>'+'NA'+'</td></tr>';});
		  $('#editedtable').append(trHTML);
		}
		else
		{
			$(data.jsonArray).each(function(i,item){
				console.log("i == " + i);
				var mStatus;
				if (data.jsonArray[i].mobileStatus==="null" ) {
					mStatus=data.jsonArray[i].mobileStatus;
					console.log("Null");
					}
				else if (data.jsonArray[i].mobileStatus!=null) {
				mStatus=data.jsonArray[i].mobileStatus;
				console.log("Not Empty");
				}
				else {
					mStatus="NA";
					console.log("Empty");
				}
				trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td><a href="${pageContext.request.contextPath}/Admin/User/'+data.jsonArray[i].mobile+'">'+ data.jsonArray[i].mobile+'</a></td><td>'+ data.jsonArray[i].name+'</td>'+'<td>'+data.jsonArray[i].email+'</td>'+'<td>'+data.jsonArray[i].balance+'</td>'+'<td>'+data.jsonArray[i].date+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'<td>'+'NA'+'</td></tr>';});
			  $('#editedtable').append(trHTML);
		}
	}
	});
			 }
	 $(document).ready(function() {
		 var paging='0';
		 var size='';
		 console.log("under ready...");
		 $.ajax({
				type:"POST",
				url:"${pageContext.request.contextPath}/Admin/WalletBalOustanding",
				data:{page:paging,size:'10'},
			dataType:"json",
			success:function(data){
				console.log("DATA  :: :: :: :: "  + data);
				var trHTML='';
					if(trHTML==''){
					$(".testingg").empty();
					$(data.jsonArray).each(function(i,item){
						 trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td><a href="${pageContext.request.contextPath}/Admin/User/'+data.jsonArray[i].mobile+'">'+ data.jsonArray[i].mobile+'</a></td><td>'+ data.jsonArray[i].name+'</td>'+'<td>'+data.jsonArray[i].email+'</td>'+'<td>'+data.jsonArray[i].balance+'</td>'+'<td>'+data.jsonArray[i].date+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'<td>'+'NA'+'</td></tr>';});
					$('#editedtable').append(trHTML);
					}
					else
					{
						$(data.jsonArray).each(function(i,item){
							trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td><a href="${pageContext.request.contextPath}/Admin/User/'+data.jsonArray[i].mobile+'">'+ data.jsonArray[i].mobile+'</a></td><td>'+ data.jsonArray[i].name+'</td>'+'<td>'+data.jsonArray[i].email+'</td>'+'<td>'+data.jsonArray[i].balance+'</td>'+'<td>'+data.jsonArray[i].date+'</td>'+'<td>'+data.jsonArray[i].status+'</td>'+'<td>'+'NA'+'</td></tr>';});
						  $('#editedtable').append(trHTML);
					}
					
					 $(function () {
							console.log("inside funt...");
						 $('#paginationn').twbsPagination({
							 totalPages: data.totalPages,
							 visiblePages: 10,
				         onPageClick: function (event, page) {
				        	 console.log("Page: "+page);
				        	 page = page - 1;
				        	fetchMe(page);
				         }
						 });
						});
			}
		 });
		
		
	 });
	 
	 
	
	
</script>	

	
		<style>
		.no-js #loader {
			display: none;
		}

		.js #loader {
			display: block;
			position: absolute;
			left: 100px;
			top: 0;
		}

		.se-pre-con {
			position: fixed;
			left: 0px;
			top: 0px;
			width: 100%;
			height: 100%;
			z-index: 9999;
			background: url(${pageContext.request.contextPath}/images/pq_large.gif) center no-repeat #fff;
		}
	</style>
	<script type="text/javascript">
		$(window).load(function() {
			$(".se-pre-con").fadeOut("slow");
		});
	</script>
	<link rel="stylesheet" href="<c:url value="/resources/css/datepicker.css"/>">
	<script src="<c:url value="/resources/js/datepicker.js"/>"></script>
	<script>
		$(function() {
			$( "#toDate" ).datepicker({
				format:"yyyy-mm-dd"
			}).on('change', function() {
				$('.datepicker').hide();
			});
			$( "#fromDate" ).datepicker({
				format:"yyyy-mm-dd"
			}).on('change', function() {
				$('.datepicker').hide();
			});
		});
	</script>
	<!-- bootpag -->
<!--     <script src="//code.jquery.com/jquery-2.1.3.min.js"></script> -->
<!--     <script src="//netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script> -->
<!--     <script src="//raw.github.com/botmonster/jquery-bootpag/master/lib/jquery.bootpag.min.js"></script> -->
<!-- bootpag -->
	

	<script src="<c:url value='/resources/js/modernizr.js'/>"></script>


</head>

<body class="nav-md">
	<!-- <div class="se-pre-con"></div> -->
	<div class="container body">
		<div class="main_container">
				<jsp:include page="/WEB-INF/jsp/Admin/LeftMenu.jsp"/>
				<jsp:include page="/WEB-INF/jsp/Admin/TopNavigation.jsp"/>
 
			<!-- page content -->
			<div class="right_col">
				<div class="">
					<div class="page-title">
						<div class="title_left">
							<h3>All Users</h3>
						</div>
						<!-- <div class="title_right">
							<div
								class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
								<div class="input-group">
									<input type="text" class="form-control"
										placeholder="Search for..."> <span
										class="input-group-btn">
										<button class="btn btn-default" type="button">Go!</button>
									</span>
								</div>
							</div>
						</div> -->
					</div>
					<div class="clearfix"></div>
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<!--    <h2>Default Example <small>Users</small></h2> -->
								<form  method="post" action="${pageContext.request.contextPath}/Admin/WalletBalanceOnSearch" onsubmit=" return validateMobile();">
								Search:<input type="text" name="search" id="mobile"  placeholder="enter mobile no./FirstName">
								
								<input type="hidden" id="mobile2" name="search"/>
								<input type="submit" value="search">
								 <p id="error_msg" style="color:red; margin-left: 15%;"></p>
								 <p id="error_msg2" style="color:red; margin-left: 15%;"></p>
								
								</form>
								<!-- search bar -->
											<div class="x_content">
                    <p class="text-muted font-13 m-b-30">
					  <form action="<c:url value="/Admin/filteredUserWalletResults"/>" method="post" class="form form-inline" onsubmit=" return validateDate();">
						  From <input type="text" id="fromDate"  class="form-control" readonly/>
						   To <input type="text" id="toDate"  class="form-control" readonly/>
						  
						   <input type="hidden" id="fromDate2" name="startDate"/>
						   <input type="hidden" id="toDate2" name="endDate"/>
						  
						  <sec:csrfInput/>
						  <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-filter"></span></button>
					      <p id="error_msg" style="color:red; margin-left: 15%;"></p>
					 
					  </form>
                    </p>			 
									<div class="clearfix"></div>
								</div>
								<div class="x_content">
									<p class="text-muted font-13 m-b-30">
										<!--  DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: <code>$().DataTable();</code> -->
									</p>
									<table id="editedtable"
										class="table table-striped table-bordered">
											<tr id="xyz">
												<th>S.No</th>
												<th>Mobile Number</th>
												<th>Customer Name</th>
												<th>Customer Email ID</th>
												<th>Balance On wallet</th>
												<th>Date of Creation</th>
												<th>Status</th>
												<th>Date of Expiry</th>
											</tr>
									</table>
								</div>
							</div>
						</div>
						<nav>
							<ul class="pagination" id="paginationn">
							  </ul>
							  </nav>
					</div>
				</div>


				<!-- footer content -->
				<jsp:include page="/WEB-INF/jsp/Admin/Footer.jsp"/>
				<!-- /footer content -->

			</div>
			<!-- /page content -->
		</div>

	</div>

	<div id="custom_notifications" class="custom-notifications dsp_none">
		<ul class="list-unstyled notifications clearfix"
			data-tabbed_notifications="notif-group">
		</ul>
		<div class="clearfix"></div>
		<div id="notif-group" class="tabbed_notifications"></div>
	</div>
	
<div class="container" align="center">

</div>

	<script
		src="${pageContext.request.contextPath}/resources/admin/js/bootstrap.min.js"></script>

	<!-- bootstrap progress js -->
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/progressbar/bootstrap-progressbar.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/nicescroll/jquery.nicescroll.min.js"></script>
	<!-- icheck -->
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/icheck/icheck.min.js"></script>

	<script
		src="${pageContext.request.contextPath}/resources/admin/js/custom.js"></script>


	<!-- Datatables-->
<!-- 	<script -->
<%-- 		src="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.js"></script> --%>
<!-- 	<script -->
<%-- 		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.bootstrap.js"></script> --%>
<!-- 	<script -->
<%-- 		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.buttons.min.js"></script> --%>
<!-- 	<script -->
<%-- 		src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.js"></script> --%>
<!-- 	<script -->
<%-- 		src="${pageContext.request.contextPath}/resources/admin/js/datatables/jszip.min.js"></script> --%>
<!-- 	<script -->
<%-- 		src="${pageContext.request.contextPath}/resources/admin/js/datatables/pdfmake.min.js"></script> --%>
<!-- 	<script -->
<%-- 		src="${pageContext.request.contextPath}/resources/admin/js/datatables/vfs_fonts.js"></script> --%>
<!-- 	<script -->
<%-- 		src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.html5.min.js"></script> --%>
<!-- 	<script -->
<%-- 		src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.print.min.js"></script> --%>
<!-- 	<script -->
<%-- 		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.fixedHeader.min.js"></script> --%>
<!-- 	<script -->
<%-- 		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.keyTable.min.js"></script> --%>
<!-- 	<script -->
<%-- 		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.responsive.min.js"></script> --%>
<!-- 	<script -->
<%-- 		src="${pageContext.request.contextPath}/resources/admin/js/datatables/responsive.bootstrap.min.js"></script> --%>
<!-- 	<script -->
<%-- 		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.scroller.min.js"></script> --%>
<!--     <script -->
<!--             src="//cdn.datatables.net/plug-ins/1.10.11/sorting/date-dd-MMM-yyyy.js"></script> -->

	<!-- pace -->
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/pace/pace.min.js"></script>

<script>
		function validateDate(){
			var fromDate=$('#fromDate').val();
			var toDate=$('#toDate').val();
			if (fromDate.length <=0 || toDate.length <=0) {
				$('#error_msg').html("Please select start & end date");
				return false;
			}
			var date1=Date.parse(fromDate);
			var date2= Date.parse(toDate);
			var timeDiff=date2-date1;
			daysDiff = Math.floor(timeDiff / (1000 * 60 * 60 * 24));
			/* console.log("days diff"+daysDiff); */
			if (daysDiff <0  || daysDiff >30) {
			$('#error_msg').html("Date should be between 30 days")
			return false;
			}
			/* console.log(date1+":"+date2+" sub: "+(date2-date1));
			console.log(date1+":"+date2); */
			 var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9\+\/\=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/\r\n/g,"\n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}
			 $('#fromDate2').val(Base64.encode(fromDate));
			 $('#toDate2').val(Base64.encode(toDate));
			return true;
		}
	</script>
	
   <script>
		function validateMobile(){
		  var Mobile=$('#mobile').val();
		  if (Mobile == "") {
				$('#error_msg').html("The field cannot be blank");
				return false;
			}
		  
		  if (Mobile.length < 10) {
				$('#error_msg2').html("Please enter a valid mobile number");
				return false;
			}
		  var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9\+\/\=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/\r\n/g,"\n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}
		  $('#mobile2').val(Base64.encode(Mobile));
		  return true;
		}
	</script> 
</body>
</html>
