<!DOCTYPE html>
<html lang="en">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<sec:csrfMetaTags/>
<title>eCarib | Agent List</title>

<!-- Bootstrap core CSS -->
<link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'
	type="image/png" />
<link
	href="${pageContext.request.contextPath}/resources/admin/css/bootstrap.min.css"
	rel="stylesheet">

<link
	href="${pageContext.request.contextPath}/resources/admin/fonts/css/font-awesome.min.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/admin/css/animate.min.css"
	rel="stylesheet">

<!-- Custom styling plus plugins -->
<link
	href="${pageContext.request.contextPath}/resources/admin/css/custom.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/admin/css/icheck/flat/green.css"
	rel="stylesheet">

<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/fixedHeader.bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/responsive.bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/scroller.bootstrap.min.css"
	rel="stylesheet" type="text/css" />

<script
	src="${pageContext.request.contextPath}/resources/admin/js/jquery.min.js"></script>

	<style>
		.no-js #loader {
			display: none;
		}

		.js #loader {
			display: block;
			position: absolute;
			left: 100px;
			top: 0;
		}
.error{
margin-left:0px;
}
		.se-pre-con {
			position: fixed;
			left: 0px;
			top: 0px;
			width: 100%;
			height: 100%;
			z-index: 9999;
			background: url(/images/pq_large.gif) center no-repeat #fff;
		}
	</style>
	<script src="<c:url value='/resources/js/modernizr.js'/>"></script>

	<script type="text/javascript">
		$(window).load(function() {
			$(".se-pre-con").fadeOut("slow");
		});
	</script>

</head>
<body class="nav-md">
 
	<div class="se-pre-con"></div>
	<div class="container body">
		<div class="main_container">
				<jsp:include page="/WEB-INF/jsp/Admin/LeftMenu.jsp"/>
				<jsp:include page="/WEB-INF/jsp/Admin/TopNavigation.jsp"/>

			<!-- page content -->
			<div class="right_col" role="main">
				<div class="">
					<div class="page-title">
						<div class="title_left">
							<h3>VPayQwik Agents</h3>
						</div>

					</div>
					<div class="clearfix"></div>

					<div class="row">

						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<!--    <h2>Default Example <small>Users</small></h2> -->
									<ul class="nav navbar-right panel_toolbox">
										
									</ul>
									<div class="clearfix"></div>
								</div>
								<div class="x_content">
									<p class="text-muted font-13 m-b-30">
										<!--  DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: <code>$().DataTable();</code> -->
									</p>
									<table id="datatable-buttons"
										class="table table-striped table-bordered date_sorted">
									 <thead>
                                             <tr>
                                                <th>S.No</th>
												<th>Agent Account Details</th>
												<th>Agent BankAccount Details</th>
												<th>Personal Details</th>
												<th>Account</th>
												<th>Date</th>
												<th>Reset Password</th>
												<th>PanCard Image</th>
												<th>Edit</th>
											</tr>
											 	<tbody id=userList>
											<c:forEach items="${agentList}" var="slist" varStatus="loopCount">
											<tr>
											<td>${loopCount.count}</td>
											<td><b>UserName:</b><c:out value="${slist.username}"></c:out><br>
											<b>MobileNo:<a href="${pageContext.request.contextPath}/Admin/User/${slist.contactNo}"></b><c:out value="${slist.contactNo}"></c:out></a><br>
											<b>Email Id:</b><c:out value="${slist.email}"/><br>
											<b>Pincode:</b><c:out value="${slist.pinCode}"/><br>
											<b>CircleName:</b><c:out value="${slist.circleName}"/><br>
											<b>VijayaBankAccount:</b><c:out value="${slist.accountNumber}"/>
											
											</td>
											<td>
											<b>AgentBankName:</b><c:out value="${slist.agentBankName}"/><br>
											<b>AgentBranchName</b><c:out value="${slist.agentBranchName}"></c:out><br>
											<b>AgentBankAccountType:</b><c:out value="${slist.agentBankAccountName}"/><br>
											<b>AgentBankAccountNo:</b><c:out value="${slist.agentBankAccountNo}"/><br>
											<b>Agency Name:</b><c:out value="${slist.agentName}"/><br>
											<b>AgentIfscCode:</b><c:out value="${slist.agentIfscCode}"/><br>
											<b>AgentPanCardNo:</b><c:out value="${slist.agentPanCardNo}"/>
											</td>
											<td>
											<b>UserType:</b><c:out value="${slist.userType}"/><br>
											<b>Authority:</b><c:out value="${slist.authority}"/><br>
											<b>Mobile Status:<b><c:out value="${slist.mobileStatus}"/></b></b>
											</td>
											<td><c:out value="${slist.balance}"/><br></td>
											<td><c:out value="${slist.dateOfAccountCreation}"/></td>
											<td>
											<button id=" " type="button" onclick="viewfp('${slist.contactNo}')" 
												style="background-color: #5cb85c; color: #fff;">Reset
											</button>
											</td>
											<td>
												<button type="button" onclick="panCardImg('${slist.image}','${slist.imageContent}')" style="background-color: #5cb85c; color: #fff;">View</button>
											</td>
											<td><a href="${pageContext.request.contextPath}/Admin/AddAgent?aId=<c:out value="${slist.agentId}"/>">
													<i class="fa fa-pencil-square" aria-hidden="true"></i></a></td>
											</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
							</div>
						</div>

						</div>
				</div>


				<!-- footer content -->
					<jsp:include page="/WEB-INF/jsp/Admin/Footer.jsp"/>
			<!-- /footer content -->

			</div>
			<!-- /page content -->
		</div>

	</div>
	
<!-- Modal after successful verification -->
<div id="verifiedMessageagent" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5></h5>
			</div>
			<div class="modal-body">
				<center id="success_alert"
					class="alert alert-success"></center>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="panCardImage" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content" style="width:80%;margin-left:110px;margin-top: 100px;">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" style="margin-left:150px;">PanCard Image</h4>
        </div>
        <div class="modal-body">
          <img  class="panCardImg" src="" class="img-circle"
							style="width:80%; margin-left:50px;margin-top: -7px;"/>
        </div>
        <!-- <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div> -->
      </div>
      
    </div>
  </div>

<!-- modal OTP for forget password -->
        <div class="modal fade" id="fpMob" role="dialog">
            <div class="modal-dialog modal-sm" style="margin-top: 5em;">
                
                <!-- Modal Content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" onclick="bodyUnfreezeScroll('fpOTP')">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Change Password</h4>
                    </div>
                    <form  class="form-horizontal" action="#" method="POST" id="">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12 extra-w3layouts">
                                        <div class="form-group">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <input type="hidden"  class="numeric" id="fpusername_forgot" name="fpusername_forgot" class="form-control" required />
                                                <!-- <p class="error" id="fp_error_username" style="color: red"></p> -->
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <input type="text" name=key class="form-control" id="fpnewPassword_key" onkeypress="clearvalue('error_new_password')"
              minlength="6" maxlength="9" autocomplete="off" placeholder="New Password" />
              <p class="error" id="error_new_password" class="error" style="color: red;margin-left:0px;"></p>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                          <div class="col-md-12 col-sm-12 col-xs-12">
                                            <input type="password" name=key class="form-control" id="fpconfirmPassword_key" onkeypress="clearvalue('error_confirm_password')"
                minlength="6" maxlength="9"  autocomplete="off" placeholder="Confirm Password"
                required /><i class="fa fa-eye-slash" id="pwd_eye" style="float: right; margin-top: -25px; margin-left: 40px;"></i>
            <p class="error" id="error_confirm_password" class="error" style="color: red;margin-left:0px;"></p>
                                          </div>
                                      </div>

                                      <div class="form-group">
                                          <div class="col-md-12 col-sm-12 col-xs-12">
                                            <center>
            								<button class="submit btn btn-primary btn-sm" 
              							type="button" id="agent_forgot_password">Continue</button>
          									</center>
                                          </div>
                                      </div>

                                      <br>
                            </div>
                        </div>
                    </div>
                    	<div class="modal-footer">
          					<center id="fpMob_message"></center>
        				<div>
                    </form>
                </div>
            </div>
        </div>
       </div>
</div>	   
<!-- modal OTP for forget password -->

<div id="successNotification" role="dialog" class="modal fade">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h5></h5>
					</div>
					<div class="modal-body">
						<center id="success_alert" class="alert alert-success"></center>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="myModal" role="dialog">
            	<div class="modal-dialog modal-sm" style="margin-top: 5em;">
                
                <!-- Modal Content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" onclick="bodyUnfreezeScroll('fpOTP')">&times;</button>
                        <h4 class="modal-title" id="success_alert">Change Password</h4>
                    </div>
                </div>
            </div>
        </div>
        
 <%-- <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
       
        <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" id="close3">&times;</button>
          <center>
        <div style="background: white; padding: 40px;">
          <h3 id="success_alert"><b><br><small></b></small></h3>
        </div></center>
        </div>
      </div>
    </div>
  </div>
 --%>



		<div id="custom_notifications" class="custom-notifications dsp_none">
		<ul class="list-unstyled notifications clearfix"
			data-tabbed_notifications="notif-group">
		</ul>
		<div class="clearfix"></div>
		<div id="notif-group" class="tabbed_notifications"></div>
	</div>

<script type="text/javascript">
$(document).ready(function(){
	$("#pwd_eye").click(function(){
		var value = $("#fpconfirmPassword_key").attr("type");
		if(value == "password") {
			$(this).attr("class", "fa fa-eye");
			$("#fpconfirmPassword_key").attr("type", "text");
		}
		if(value == "text"){
			$(this).attr("class", "fa fa-eye-slash");
			$("#fpconfirmPassword_key").attr("type", "password");
		}

	});
	
	
});
</script>

<script>

function viewfp(username){
	console.log("username="+username);
	$("#fpMob").modal("show");
	
	$("#fpusername_forgot").val(username);
	console.log("mobile number="+$("#fpusername_forgot").val());
 }
 
 function panCardImg(type,imageContent){
	 var srcImage = "data:"+type+";base64,"+imageContent
		$(".panCardImg").attr('src',srcImage);
	 $("#panCardImage").modal("show");
 }
 
$("#agent_forgot_password").click(function() {
	$("#fpMob_message").text("");
	var username = $("#fpusername_forgot").val();
	var newPassword = $("#fpnewPassword_key").val();
	var confirmPassword =  $('#fpconfirmPassword_key').val();
	var mobilePattern="^(?=(?:[7-9]){1})(?=[0-9]{10}).*";
	var valid = true;
	console.log("username="+username);
	console.log("newPassword="+newPassword);
	console.log("confirmPassword="+confirmPassword);
	/* if(username.length ==0){
		$("#fp_error_username").html("Enter mobile number");
		  valid = false;
	}
	else if(!username.match(mobilePattern)){
		$("#fp_error_username").html("Enter valid mobile number");
		valid = false;
	} */
	if(newPassword.length <=0){
		$("#error_new_password").html("Enter New Password.");
		  valid = false;
	}
	if(confirmPassword.length <=0){
		$("#error_confirm_password").html("Enter Confirm Password.");
		  valid = false;
	}
	if(valid == true) {
	if(newPassword == confirmPassword) {
		console.log("confirmPassword");
	$.ajax({
		type : "POST",
		contentType : "application/json",
		url : "/Admin/Agent/ChangePasswordWithMobile",
		dataType : 'json',
		data : JSON.stringify({
			"username" : "" + username + "",
			"newPassword" : "" + newPassword + "",
			"confirmPassword" : "" + confirmPassword + ""
		}),
		success : function(response) {
			console.log("CODE="+response.code);
			if (response.code.includes("S00")) {
			//	$("#fpMob").modal("hide");
			//	$('.modal-backdrop').remove();
				$("#fpMob").modal("hide")
				$("#verifiedMessageagent").modal("show");
				$("#success_alert").html(response.message);
				var timeout = setTimeout(function(){
		            $("#verifiedMessageagent").modal('hide');
		            $("#fpusername_forgot").val('');
		            $("#fpnewPassword_key").val('');
		            $('#fpconfirmPassword_key').val('');
		          }, 6000);
			} else {
				console.log("In else")
				$("#fpMob_message").css("color", "red");
				$("#fpMob_message").text(response.status);
			}
		}
	});
	}else {
//		$('#fpnewPassword_key').val("Please enter same password in both fields");
		$("#error_confirm_password").text("Password does not match");
	}
  }	
	var timeout = setTimeout(function(){
		$("#error_new_password").html("");
		$("#error_confirm_password").html("");
	}, 3000);

});



</script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/bootstrap.min.js"></script>

	<!-- bootstrap progress js -->
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/progressbar/bootstrap-progressbar.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/nicescroll/jquery.nicescroll.min.js"></script>
	<!-- icheck -->
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/icheck/icheck.min.js"></script>

	<script
		src="${pageContext.request.contextPath}/resources/admin/js/custom.js"></script>


	<!-- Datatables -->
	<!-- <script src="js/datatables/js/jquery.dataTables.js"></script>
  <script src="js/datatables/tools/js/dataTables.tableTools.js"></script> -->

	<!-- Datatables-->
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.bootstrap.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.buttons.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/jszip.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/pdfmake.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/vfs_fonts.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.html5.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.print.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.fixedHeader.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.keyTable.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.responsive.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/responsive.bootstrap.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.scroller.min.js"></script>
    <script
            src="//cdn.datatables.net/plug-ins/1.10.11/sorting/date-dd-MMM-yyyy.js"></script>

    <!-- pace -->
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/pace/pace.min.js"></script>
	<script type="text/javascript">
		var handleDataTableButtons = function() {
            console.log("inside datatable buttons");
			"use strict";
			0 !== $("#datatable-buttons").length
					&& $("#datatable-buttons").DataTable({
                columnDefs: [
                    {
                        type: 'date-dd-mmm-yyyy',
                        targets: 1
                    }
                ],
						dom : "Bfrtip",
						buttons : [ {
							extend : "copy",
							className : "btn-sm"
						}, {
							extend : "csv",
							className : "btn-sm"
						}, {
							extend : "excel",
							className : "btn-sm"
						}, {
							extend : "pdf",
							className : "btn-sm"
						}, {
							extend : "print",
							className : "btn-sm"
						} ],



						responsive : !0
					})
		}, TableManageButtons = function() {
			"use strict";
			return {
				init : function() {
					handleDataTableButtons()
				}
			}
		}();
	</script>
	
	<script type="text/javascript">
		$(document).ready(function() {

			$('#datatable-keytable').DataTable({
				keys : true
			});
			$('#datatable-responsive').DataTable();
			$('#datatable-scroller').DataTable({
				ajax : "js/datatables/json/scroller-demo.json",
				deferRender : true,
				scrollY : 380,
				scrollCollapse : true,
				scroller : true
			});
			var table = $('#datatable-fixed-header').DataTable({
				fixedHeader : true
			});
		});
		TableManageButtons.init();
	</script>
	
</body>
</html>