<!DOCTYPE html>
<html lang="en">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <sec:csrfMetaTags />
    <title>eCarib |Agent Transactions Report</title>

    <!-- Bootstrap core CSS -->
    <link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'
          type="image/png" />

    <link
            href="${pageContext.request.contextPath}/resources/admin/css/bootstrap.min.css"
            rel="stylesheet">

    <link
            href="${pageContext.request.contextPath}/resources/admin/fonts/css/font-awesome.min.css"
            rel="stylesheet">
    <link
            href="${pageContext.request.contextPath}/resources/admin/css/animate.min.css"
            rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link
            href="${pageContext.request.contextPath}/resources/admin/css/custom.css"
            rel="stylesheet">
    <link
            href="${pageContext.request.contextPath}/resources/admin/css/icheck/flat/green.css"
            rel="stylesheet">

    <link
            href="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.css"
            rel="stylesheet" type="text/css" />
    <link
            href="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.css"
            rel="stylesheet" type="text/css" />
    <link
            href="${pageContext.request.contextPath}/resources/admin/js/datatables/fixedHeader.bootstrap.min.css"
            rel="stylesheet" type="text/css" />
    <link
            href="${pageContext.request.contextPath}/resources/admin/js/datatables/responsive.bootstrap.min.css"
            rel="stylesheet" type="text/css" />
    <link
            href="${pageContext.request.contextPath}/resources/admin/js/datatables/scroller.bootstrap.min.css"
            rel="stylesheet" type="text/css" />

    <script
            src="${pageContext.request.contextPath}/resources/admin/js/jquery.min.js"></script>
 <style>
table {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
    border: 1px solid #ddd;
}

th, td {
    padding: 10px;
}

th:first-child, td:first-child {
    text-align: left;
}

tr:nth-child(even) {
    background-color: #f2f2f2
}

        .no-js #loader {
            display: none;
        }

        .js #loader {
            display: block;
            position: absolute;
            left: 100px;
            top: 0;
        }

        .se-pre-con {
            position: fixed;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: url(/images/pq_large.gif) center no-repeat #fff;
        }
    </style>
    <script src="<c:url value='/resources/js/modernizr.js'/>"></script>
    <link rel="stylesheet" href="<c:url value="/resources/css/datepicker.css"/>">
    <script src="<c:url value="/resources/js/datepicker.js"/>"></script>
    <script>
        $(function() {
            $( "#toDate" ).datepicker({
                format:"yyyy-mm-dd"
            });
            $( "#fromDate" ).datepicker({
                format:"yyyy-mm-dd"
            });
        });
    </script>

    <script type="text/javascript">
        $(window).load(function() {
            $(".se-pre-con").fadeOut("slow");
            
            var timeout = setTimeout(function(){
			    $("#error_msg").html(" ");
				
			}, 1000);
        });
    </script>

</head>
<body class="nav-md">
<div class="se-pre-con"></div>
<div class="container body">
    <div class="main_container">
        <jsp:include page="/WEB-INF/jsp/Admin/LeftMenu.jsp" />
        <jsp:include page="/WEB-INF/jsp/Admin/TopNavigation.jsp" />



        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3>Agent NEFT Requests</h3>
                    </div>

                </div>
                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <!--  <h2>Button Example <small>Users</small></h2> -->
                                <ul class="nav navbar-right panel_toolbox">
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <p class="text-muted font-13 m-b-30">
<!--                                                          The Buttons extension for DataTables provides a common set of options, API methods and styling to display buttons on a page that will interact with a DataTable. The core library provides the based framework upon which plug-ins can built. -->
							<form action="<c:url value="/Admin/AgentNEFTListInJSON"/>" method="post" class="form form-inline" onsubmit="return validateDate();">
                               From <input type="text" id="fromDate"  class="form-control" readonly/>
										To <input type="text" id="toDate"  class="form-control" readonly/>
										
										<input type="hidden" id="fromDate2" name="fromDate"/>
									    <input type="hidden" id="toDate2" name="toDate"/>
                                <sec:csrfInput/>
                                <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-filter"></span></button> 
                                <p id="error_msg" style="color:red; margin-left: 15%;"></p>
                            </form> 
                            </p>
                                <table id="datatable-buttons"
                                       class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>User Details</th>
                                        <th>Transaction ID</th>
                                        <th>Transaction Date</th>
                                        <th>Bank Name</th>
                                        <th>IFSC Code</th>
                                        <th>Account Number</th>
                                        <th>Account Holder Name</th>
                                        <th>User Account</th>
                                        <th>Bank Account</th>
                                        <th>Amount</th>
                                        <th>Status</th>
                                        <th>ViewDetails</th>
                                    </tr>
                                    </thead>
                                    <tbody id="userList">
                                    <c:forEach items="${neftList}" var="neft"
                                               varStatus="loopCounter">
                                        <tr>
                                            <td>${loopCounter.count}</td>
                                            <td><b>Name:</b><c:out value="${neft.name}" default=""
                                                       escapeXml="true" /><br/>
                                                <b>Email:</b><c:out value="${neft.email}" default=""
                                                                   escapeXml="true" /><br/>
                                            </td>
                                            <td><c:out value="${neft.transactionID}"
                                                       default="" escapeXml="true" /></td>
                                            <td><c:out value="${neft.transactionDate}"
                                                       default="" escapeXml="true" /></td>
                                            <td><c:out value="${neft.bankName}"
                                                       escapeXml="true" default="" /></td>
                                            <td><c:out value="${neft.ifscCode}"
                                                       escapeXml="true" default="" /></td>
                                            <td><c:out value="${neft.accountNumber}"
                                                       escapeXml="true" default="" /></td>
                                            <td><c:out value="${neft.accountName}" default=""
                                                       escapeXml="true" /></td>
                                            <td><c:out value="${neft.userAccount}" default=""
                                                       escapeXml="true" /></td>
                                            <td><c:out value="${neft.bankAccount}" default=""
                                                       escapeXml="true" /></td>
                                            <td><c:out value="${neft.amount}" default=""
                                                       escapeXml="true" /></td>
                                            <td><c:out value="${neft.status}" default=""
                                                       escapeXml="true" /></td>
                                            <td id="userinfo2"><button type="button" style="background-color: #5cb85c; color: #fff;" class="btn btn-primary" 
   onclick="userinfo('${neft.senderName}','${neft.accountName}','${neft.senderEmailId}','${neft.receiverEmailId}','${neft.senderMobileNo}',
     '${neft.receiverMobileNo}','${neft.idProofNo}','${neft.image}','${neft.imageContent}','${neft.description}',
   				'${neft.addImageType}','${neft.addressImage}')">view</button></td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>


            <jsp:include page="/WEB-INF/jsp/Admin/Footer.jsp" />
        </div>
        <!-- /page content -->
    </div>

</div>

 <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" style="margin-left:200px;">Sender/Receiver Info</h4>
        </div>
        <div class="modal-body">
     <div class="row">
		<div class="col-sm-6"> 
			<table>
  				<tr>
    				<th style="width:50%; margin-left:3000px;">Sender Info</th>
    				<th></th>
  				</tr>
  				<tr>
    				<td>Sender Name</td>
    				<td id="senderName"></td>
  				</tr>
  				<tr>
    				<td>Sender Email id</td>
    				<td id="senderEmailId"></td>
  				</tr>
 	 			<tr>
    				<td>Sender Number</td>
    				<td id="senderMobileNo"></td>
  				</tr>
  				<tr>
    				<td>Sender Id Proof Number</td>
    				<td id="idProofNo"></td>
  				</tr>
  				<tr>
  					<td>Sender Id Proof Image</td>
  					<!-- <td><img  class="idProofImg" src="" class="img-circle"
							style="width:250%; margin-top: -7px;"/>
					</td> -->
					<td>
					   <button type="button" onclick="viewImg()" style="background-color: #5cb85c; color: #fff;">View</button>
					</td>
  				</tr> 
	         </table>
		</div>

		<div class="col-sm-6"> 
			<table>
  				<tr>
    				<th style="width:50%;margin-left:10px;">Receiver Info</th>
    				<th></th>
  				</tr>
  				<tr>
    				<td>Receiver Name</td>
   					<td id="receiverName"></td>
  			   </tr>
  			    <tr>
    				 <td>Receiver Email id</td>
    				 <td id="receiverEmailId"></td>
 		 	    </tr>
  				<tr>
   		 			<td>Receiver Number</td>
   		 			<td id="receiverMobileNo"></td>
  				</tr>
  				<tr>
  					<td>Description</td>
  					<td id="description"></td>
  				</tr>
			</table>
	  </div>
</div>
        </div>
        
      </div>
      
    </div>
  </div>

<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix"
        data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>

<div class="modal fade" id="idProofImg" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content" style="width:80%;margin-left:60px;margin-top: 100px;">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" style="margin-left:150px;">ID Proof Image</h4>
        </div>
        <label style="margin-left:170px; color:green;"><b>Front side Image</b></label>
        <div class="modal-body">
          <img  class="fImg" src="" class="img-circle"
							style="width:80%; margin-left:50px;margin-top: -7px;"/>
        </div>
        <label style="margin-left:170px; color:green;"><b>Address Proof Image</b></label>
        <div class="modal-body">
          <img  class="bImg" src="" class="img-circle"
							style="width:80%; margin-left:50px;margin-top: -7px;"/>
        </div>
        <!-- <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div> -->
      </div>
      
    </div>
  </div>
  
<script type="text/javascript">
function userinfo(senderName,receiverName,senderEmailId,receiverEmailId,senderMobileNo,receiverMobileNo,idProofNo,type,imageContent,desc,type2,imgContent){
	$("#senderName").html(senderName);
	$("#senderEmailId").html(senderEmailId);
	$("#senderMobileNo").html(senderMobileNo);
	$("#idProofNo").html(idProofNo);
	$("#receiverName").html(receiverName);
	$("#receiverEmailId").html(receiverEmailId);
	$("#receiverMobileNo").html(receiverMobileNo);
	$("#description").html(desc);
	/* var srcImage = "data:"+type+";base64,"+imageContent
	$(".idProofImg").attr('src',srcImage); */
	 var srcImage = "data:"+type+";base64,"+imageContent
	 var srcImage2 = "data:"+type2+";base64,"+imgContent
	 console.log("srcImage=="+srcImage);
	 console.log("srcImage2=="+srcImage2);
		$(".fImg").attr('src',srcImage);
	    $(".bImg").attr('src',srcImage2);
	$("#myModal").modal("show");
}

function viewImg(){
	/*  var srcImage = "data:"+type+";base64,"+imageContent
	 var srcImage2 = "data:"+type2+";base64,"+imgContent
	 console.log("srcImage=="+srcImage);
	 console.log("srcImage2=="+srcImage2);
		$(".fImg").attr('src',srcImage);
	    $(".bImg").attr('src',srcImage2); */
	 $("#idProofImg").modal("show");
}

</script>
<script
        src="${pageContext.request.contextPath}/resources/admin/js/bootstrap.min.js"></script>

<!-- bootstrap progress js -->
<script
        src="${pageContext.request.contextPath}/resources/admin/js/progressbar/bootstrap-progressbar.min.js"></script>
<script
        src="${pageContext.request.contextPath}/resources/admin/js/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<script
        src="${pageContext.request.contextPath}/resources/admin/js/icheck/icheck.min.js"></script>

<script
        src="${pageContext.request.contextPath}/resources/admin/js/custom.js"></script>


<!-- Datatables -->
<!-- <script src="js/datatables/js/jquery.dataTables.js"></script>
<script src="js/datatables/tools/js/dataTables.tableTools.js"></script> -->

<!-- Datatables-->
<script
        src="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.js"></script>
<script
        src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.bootstrap.js"></script>
<script
        src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.buttons.min.js"></script>
<script
        src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.js"></script>
<script
        src="${pageContext.request.contextPath}/resources/admin/js/datatables/jszip.min.js"></script>
<script
        src="${pageContext.request.contextPath}/resources/admin/js/datatables/pdfmake.min.js"></script>
<script
        src="${pageContext.request.contextPath}/resources/admin/js/datatables/vfs_fonts.js"></script>
<script
        src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.html5.min.js"></script>
<script
        src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.print.min.js"></script>
<script
        src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.fixedHeader.min.js"></script>
<script
        src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.keyTable.min.js"></script>
<script
        src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.responsive.min.js"></script>
<script
        src="${pageContext.request.contextPath}/resources/admin/js/datatables/responsive.bootstrap.min.js"></script>
<script
        src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.scroller.min.js"></script>


<!-- pace -->
<script
        src="${pageContext.request.contextPath}/resources/admin/js/pace/pace.min.js"></script>
<script>
    var handleDataTableButtons = function() {
        "use strict";
        0 !== $("#datatable-buttons").length
        && $("#datatable-buttons").DataTable({
            columnDefs: [
                {
                    type: 'date-dd-mmm-yyyy',
                    targets: 3
                }
            ],
            dom : "Bfrtip",
            buttons : [ {
                extend : "copy",
                className : "btn-sm"
            }, {
                extend : "csv",
                className : "btn-sm"
            }, {
                extend : "excel",
                className : "btn-sm"
            }, {
                extend : "pdf",
                className : "btn-sm"
            }, {
                extend : "print",
                className : "btn-sm"
            } ],
            responsive : !0
        })
    }, TableManageButtons = function() {
        "use strict";
        return {
            init : function() {
                handleDataTableButtons()
            }
        }
    }();
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#datatable').dataTable();
        $('#datatable-keytable').DataTable({
            keys : true
        });
        $('#datatable-responsive').DataTable();
        $('#datatable-scroller').DataTable({
            ajax : "js/datatables/json/scroller-demo.json",
            deferRender : true,
            scrollY : 380,
            scrollCollapse : true,
            scroller : true
        });
        var table = $('#datatable-fixed-header').DataTable({
            fixedHeader : true
        });
    });
    TableManageButtons.init();
</script>
<script type="text/javascript">
    $(document)
            .ready(
                    function() {

                        var cb = function(start, end, label) {
                            console.log(start.toISOString(), end
                                    .toISOString(), label);
                            $('#reportrange span').html(
                                    start.format('MMMM D, YYYY') + ' - '
                                    + end.format('MMMM D, YYYY'));
                            //alert("Callback has fired: [" + start.format('MMMM D, YYYY') + " to " + end.format('MMMM D, YYYY') + ", label = " + label + "]");
                        }

                        var optionSet1 = {
                            startDate : moment().subtract(29, 'days'),
                            endDate : moment(),
                            minDate : '01/01/2012',
                            maxDate : '12/31/2015',
                            dateLimit : {
                                days : 60
                            },
                            showDropdowns : true,
                            showWeekNumbers : true,
                            timePicker : false,
                            timePickerIncrement : 1,
                            timePicker12Hour : true,
                            ranges : {
                                'Today' : [ moment(), moment() ],
                                'Yesterday' : [
                                    moment().subtract(1, 'days'),
                                    moment().subtract(1, 'days') ],
                                'Last 7 Days' : [
                                    moment().subtract(6, 'days'),
                                    moment() ],
                                'Last 30 Days' : [
                                    moment().subtract(29, 'days'),
                                    moment() ],
                                'This Month' : [ moment().startOf('month'),
                                    moment().endOf('month') ],
                                'Last Month' : [
                                    moment().subtract(1, 'month')
                                            .startOf('month'),
                                    moment().subtract(1, 'month')
                                            .endOf('month') ]
                            },
                            opens : 'left',
                            buttonClasses : [ 'btn btn-default' ],
                            applyClass : 'btn-small btn-primary',
                            cancelClass : 'btn-small',
                            format : 'MM/DD/YYYY',
                            separator : ' to ',
                            locale : {
                                applyLabel : 'Submit',
                                cancelLabel : 'Clear',
                                fromLabel : 'From',
                                toLabel : 'To',
                                customRangeLabel : 'Custom',
                                daysOfWeek : [ 'Su', 'Mo', 'Tu', 'We',
                                    'Th', 'Fr', 'Sa' ],
                                monthNames : [ 'January', 'February',
                                    'March', 'April', 'May', 'June',
                                    'July', 'August', 'September',
                                    'October', 'November', 'December' ],
                                firstDay : 1
                            }
                        };
                        $('#reportrange span').html(
                                moment().subtract(29, 'days').format(
                                        'MMMM D, YYYY')
                                + ' - '
                                + moment().format('MMMM D, YYYY'));
                        $('#reportrange').daterangepicker(optionSet1, cb);
                        $('#reportrange').on('show.daterangepicker',
                                function() {
                                    console.log("show event fired");
                                });
                        $('#reportrange').on('hide.daterangepicker',
                                function() {
                                    console.log("hide event fired");
                                });
                        $('#reportrange')
                                .on(
                                        'apply.daterangepicker',
                                        function(ev, picker) {
                                            console
                                                    .log("apply event fired, start/end dates are "
                                                            + picker.startDate
                                                                    .format('MMMM D, YYYY')
                                                            + " to "
                                                            + picker.endDate
                                                                    .format('MMMM D, YYYY'));
                                        });
                        $('#reportrange').on('cancel.daterangepicker',
                                function(ev, picker) {
                                    console.log("cancel event fired");
                                });
                        $('#options1').click(
                                function() {
                                    $('#reportrange').data(
                                            'daterangepicker').setOptions(
                                            optionSet1, cb);
                                });
                        $('#options2').click(
                                function() {
                                    $('#reportrange').data(
                                            'daterangepicker').setOptions(
                                            optionSet2, cb);
                                });
                        $('#destroy').click(
                                function() {
                                    $('#reportrange').data(
                                            'daterangepicker').remove();
                                });
                    });
</script>

   <script>
		function validateDate(){
			var fromDate=$('#fromDate').val();
			var toDate=$('#toDate').val();
			if (fromDate.length <=0 || toDate.length <=0) {
				$('#error_msg').html("Please select start & end date");
				return false;
			}
			var date1=Date.parse(fromDate);
			var date2= Date.parse(toDate);
			var timeDiff=date2-date1;
			daysDiff = Math.floor(timeDiff / (1000 * 60 * 60 * 24));
			
			if (daysDiff <0  || daysDiff >30) {
			$('#error_msg').html("Date should be between 30 days")
			return false;
			}
			var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9\+\/\=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/\r\n/g,"\n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}
			 $('#fromDate2').val(Base64.encode(fromDate));
			 $('#toDate2').val(Base64.encode(toDate));
			return true;
		}
	</script>

</body>
</html>

