<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage=""%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<sec:csrfInput/>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>eCarib | Login</title>
<!-- Latest compiled and minified CSS -->
<link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'
	type="image/png" />
<link href='<c:url value="/resources/css/new_css/cloud-admin.css"/>'
	rel='stylesheet' type='text/css'>
	
	<link href='<c:url value="/resources/new-css/font-awesome.min.css"/>'
	rel='stylesheet' type='text/css'>
	
	<!-- DATE RANGE PICKER -->
	
	<link href='<c:url value="/resources/new-css/daterangepicker-bs3.css"/>'
	rel='stylesheet' type='text/css'>
	<!-- UNIFORM -->
	<link href='<c:url value="/resources/css/new_css/uniform.default.min.css"/>'
	rel='stylesheet' type='text/css'>
	<!-- ANIMATE -->
	<link href='<c:url value="/resources/new-css/animate.min.css"/>'
	rel='stylesheet' type='text/css'>
	<!-- FONTS -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
<script
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript"
	src="<c:url value='/resources/js/jquery.js'/>"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<style>
		.no-js #loader {
			display: none;
		}

		.js #loader {
			display: block;
			position: absolute;
			left: 100px;
			top: 0;
		}

		.se-pre-con {
			position: fixed;
			left: 0px;
			top: 0px;
			width: 100%;
			height: 100%;
			z-index: 9999;
			background: url(/images/pq_large.gif) center no-repeat #fff;
		}
	</style>
	<script src="<c:url value='/resources/js/modernizr.js'/>"></script>

	<script type="text/javascript">
		$(window).load(function() {
			$(".se-pre-con").fadeOut("slow");
			start();
		});
	</script>

</head>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/header.js"></script>
<body class="login">
<c:if test="${error ne null}">
				<div class="alert alert-danger col-md-12" id="alertDanger">
					<center>
						<c:out value="${error}" escapeXml="true" default="" />
					</center>
				</div>
			</c:if>
			<c:if test="${msg ne null}">
				<div class="alert alert-success col-md-12" id="successAlert">
					<center>
						<c:out value="${msg}" escapeXml="true" default="" />
					</center>
				</div>
			</c:if>	
	<!-- PAGE -->
	<section id="page">
			<!-- HEADER -->
			<header>
				<!-- NAV-BAR -->
				<div class="container">
					<div class="row">
						<div class="col-md-4 col-md-offset-4">
							<div id="logo">
								<a href="#"><img src="${pageContext.request.contextPath}/resources/images/new_img/logo1.png" height="50" style="width:35%;height:40%;"alt="logo name" /></a>
							</div>
						</div>
					</div>
				</div>
				<!--/NAV-BAR -->
			</header>
			<!--/HEADER -->
			
			<!-- LOGIN -->
			<section id="login_bg" class="visible">
				<div class="container">
					<div class="row">
						<div class="col-md-4 col-md-offset-4">
							<div class="login-box">
								<h2 class="bigintro">Sign In</h2>
								<div class="divide-40"></div>
								<form role="form" action="${pageContext.request.contextPath}/Admin/Home" method="post" onsubmit=" return validatePassword();">
								  <div class="form-group">
									<label>Username</label>
									<i class="fa fa-mobile"></i>
									<input type="text" name="username" class="form-control" id="username" value=" ">
								  </div>
								  <div class="form-group"> 
									<label>Password</label>
									<i class="fa fa-lock"></i>
									<input type="password" name="password" class="form-control" id="password" value="">
								  </div>
								  <div>
									<!-- <label class="checkbox"> <input type="checkbox" class="uniform" value=""> Remember me</label> -->
									<button type="submit" class="btn btn-danger">Submit</button>
								  </div>
								</form>
								<!-- <div class="login-helpers">
									<a href="#" onclick="swapScreen('forgot_bg');return false;">Forgot Password?</a> <br>
									
								</div> -->
							</div>
						</div>
					</div>
				</div>
			</section>
			<!--/LOGIN -->
			<!-- CHANGE PASSWORD -->
			<section id="register_bg" class="font-400">
				<div class="container">
					<div class="row">
						<div class="col-md-4 col-md-offset-4">
							<div class="login-box">
								<h2 class="bigintro">Change Password</h2>
								<div class="divide-40"></div>
								<form role="form">
								  <div class="form-group">
									<label for="exampleInputName">OTP</label>
									<i class="fa fa-font"></i>
									<input type="text" class="form-control" id="exampleInputName" >
								  </div>
								  <div class="form-group">
									<label for="exampleInputUsername">New Password</label>
									<i class="fa fa-lock"></i>
									<input type="password" class="form-control" id="exampleInputUsername" >
								  </div>
								  <div class="form-group">
									<label for="exampleInputEmail1">Confirm Password</label>
									<i class="fa fa-lock"></i>
									<input type="password" class="form-control" id="exampleInputEmail1" >
								  </div>
								  <div class="form-group"> 
									<center><img src="img/captcha.png" class="img-responsive"></center>
								  </div>
								  <div class="form-group"> 
									<i class="fa fa-font"></i>
									<input type="text" class="form-control" id="exampleInputPassword2" placeholder="Enter text shown in image" >
								  </div>
								  <div>
									<div class="col-md-6">
										<center><button type="submit" class="btn btn-success">Resend OTP</button></center>
									</div>
									<div class="col-md-6">
										<center><button type="submit" class="btn btn-success">Continue</button></center>
									</div>
								  </div>
								</form>
								<div class="divide-40"></div>
								<div class="login-helpers">
									<a href="#" onclick="swapScreen('login_bg');return false;"> Back to Login</a> <br>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!--/CHANGE PASSWORD -->
			<!-- FORGOT PASSWORD -->
			<section id="forgot_bg">
				<div class="container">
					<div class="row">
						<div class="col-md-4 col-md-offset-4">
							<div class="login-box">
								<h2 class="bigintro">Reset Password</h2>
								<div class="divide-40"></div>
								<form role="form">
								  <div class="form-group">
									<label for="exampleInputEmail1">Enter your Mobile Number</label>
									<i class="fa fa-mobile"></i>
									<input type="text" class="form-control textfield" id="exampleInputEmail1" maxlength="10" onkeypress="return onlyNos(event,this);">
								  </div>
								  <div>
									<button type="submit" class="btn btn-info" onclick="swapScreen('register_bg');return false;">Continue</button>
								  </div>
								</form>
								<div class="login-helpers">
									<a href="#" onclick="swapScreen('login_bg');return false;">Back to Login</a> <br>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- FORGOT PASSWORD -->
			<!-- FOOTER -->
			<section id="footer">
				<div class="container">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12 foot_box" style="text-align: center; padding: 15px; font-size: 15px; color: #ffffff;">
							<p><img src="${pageContext.request.contextPath}/resources/images/new_img/mlogo.png" style="width: 5%;"> &copy; <a href="http://www.msewa.com/">Msewa Software</a> Pvt Ltd. | 2017 All Rights Reserved.</p>
						</div>
					</div>
				</div>
			</section>
			<!-- /FOOTER -->


	</section>
	<!--/PAGE -->
	<!-- JAVASCRIPTS -->
	<!-- Placed at the end of the document so the pages load faster -->
	<!-- JQUERY -->
	<script type="text/javascript"
			src="${pageContext.request.contextPath}/resources/js/jquery-2.0.3.min.js"></script>
	<!-- JQUERY UI-->
	<script type="text/javascript"
			src="${pageContext.request.contextPath}/resources/js/new_js/jquery-ui-1.10.3.custom.min.js"></script>
	<!-- BOOTSTRAP -->
	<script type="text/javascript"
			src="${pageContext.request.contextPath}/resources/js/new_js/bootstrap.min.js"></script>
	
	
	<!-- UNIFORM -->
	<script type="text/javascript"
			src="${pageContext.request.contextPath}/resources/js/new_js/jquery.uniform.min.js"></script>
	<!-- BACKSTRETCH -->
	<script type="text/javascript"
			src="${pageContext.request.contextPath}/resources/js/new_js/jquery.backstretch.min.js"></script>
	<!-- CUSTOM SCRIPT -->
	<script type="text/javascript"
			src="${pageContext.request.contextPath}/resources/js/new_js/script.js"></script>
	<script>
		$(document).ready(function() {		
			App.setPage("login_bg");  //Set current page
			App.init(); //Initialise plugins and elements
		});
	</script>
	<script type="text/javascript">
		function swapScreen(id) {
			$('.visible').removeClass('visible animated fadeInUp');
			$('#'+id).addClass('visible animated fadeInUp');
		}
	</script>

<!-- Script for only number input -->
	<script>
		function onlyNos(e, t) {
            try {
                if (window.event) {
                    var charCode = window.event.keyCode;
                }
                else if (e) {
                    var charCode = e.which;
                }
                else { return true; }
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    return false;
                }
                return true;
            }
            catch (err) {
                alert(err.Description);
            }
        }
	
	</script>
	
	<script type="text/javascript">
    function successAlert(){
        $("#successAlert").fadeTo(2000, 500).slideUp(500, function(){
       $("#successAlert").slideUp(500);
        });   
       }
	     function dangerAlert(){
	         $("#alertDanger").fadeTo(2000, 500).slideUp(500, function(){
	        $("#alertDanger").slideUp(500);
	         });   
	     }
	     
		function start(){
	 		successAlert();
	 		dangerAlert();
		}
	
	</script>
	
	<script>
		function validatePassword(){
			 var password=$('#password').val();
             var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9\+\/\=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/\r\n/g,"\n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}
			 $('#password').val(Base64.encode(password));
			return true;
		}
	</script>
	
<!-- /Script for only number input -->

	<!-- /JAVASCRIPTS -->
</body>
</html>