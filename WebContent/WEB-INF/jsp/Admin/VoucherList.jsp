<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page isELIgnored="false"%>
<%@ page import="org.springframework.web.util.HtmlUtils" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html lang="en">
<%-- <%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%> --%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<sec:csrfMetaTags/>

<title>eCarib | Voucher List</title>
<!-- Bootstrap core CSS -->

<link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'
	type="image/png" />
<link
	href="${pageContext.request.contextPath}/resources/admin/css/bootstrap.min.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/admin/fonts/css/font-awesome.min.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/admin/css/animate.min.css"
	rel="stylesheet">
<!-- Custom styling plus plugins -->
<link
	href="${pageContext.request.contextPath}/resources/admin/css/custom.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/admin/css/icheck/flat/green.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/fixedHeader.bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/responsive.bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/scroller.bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<script	src="${pageContext.request.contextPath}/resources/admin/js/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/Admin/jquery.twbsPagination.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/Admin/jquery.twbsPagination.min.js"></script>



<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/header.js"></script>
<link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'
	type="image/png" />


	<script type="text/javascript">
	 function fetchMe(value){
		var paging=value;
		console.log(paging);
		console.log("inside function");
	$.ajax({
	type:"POST",
	url:"${pageContext.request.contextPath}/Admin/VoucherList",
	data:{page:paging,size:'10'},
	dataType:"json",
	success:function(data){
	var trHTML='';
		if(trHTML==''){
		$(".testingg").empty();
		$(data.jsonArray).each(function(i,item){
			trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>' + data.jsonArray[i].createdDate + '</td><td>' + data.jsonArray[i].voucherNo + '</td><td>' + data.jsonArray[i].amount + '</td><td>' + data.jsonArray[i].expiryDate + '</td><td>' + data.jsonArray[i].expired + '</td><td>' + data.jsonArray[i].redeemed + '</td></tr>';});
		  $('#editedtable').append(trHTML);
		}
		else
		{
			$(data.jsonArray).each(function(i,item){
				trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>' + data.jsonArray[i].createdDate + '</td><td>' + data.jsonArray[i].voucherNo + '</td><td>' + data.jsonArray[i].amount + '</td><td>' + data.jsonArray[i].expiryDate + '</td><td>' + data.jsonArray[i].expired + '</td><td>' + data.jsonArray[i].redeemed + '</td></tr>';});
			  $('#editedtable').append(trHTML);
		}
	}
	});
			 }
	  $(document).ready(function() {
		 var paging='0';
		 var size='10';
		 console.log("under ready...");
		 $.ajax({
				type:"POST",
				url:"${pageContext.request.contextPath}/Admin/VoucherList",
				data:{page:paging,size:size},
				dataType:"json",
				success:function(data){
				var trHTML='';
					if(trHTML==''){
					$(".testingg").empty();
					$(data.jsonArray).each(function(i,item){
						trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>' + data.jsonArray[i].createdDate + '</td><td>' + data.jsonArray[i].voucherNo + '</td><td>' + data.jsonArray[i].amount + '</td><td>' + data.jsonArray[i].expiryDate + '</td><td>' + data.jsonArray[i].expired + '</td><td>' + data.jsonArray[i].redeemed + '</td></tr>';});
					  $('#editedtable').append(trHTML);
					}
					else
					{
						$(data.jsonArray).each(function(i,item){
							trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>' + data.jsonArray[i].createdDate + '</td><td>' + data.jsonArray[i].voucherNo + '</td><td>' + data.jsonArray[i].amount + '</td><td>' + data.jsonArray[i].expiryDate + '</td><td>' + data.jsonArray[i].expired + '</td><td>' + data.jsonArray[i].redeemed + '</td></tr>';});
						  $('#editedtable').append(trHTML);
					}
					 $(function () {
							console.log("inside funt...");
						 $('#paginationn').twbsPagination({
							 totalPages: data.totalPages,
							 visiblePages: 10,
				         onPageClick: function (event, page) {
				        	 fetchMe(page-1);
				         }
						 });
						});
					 
			}
		 });
		
		
	 }); 
	 
	 
	
	
</script>	
		<style>
		.no-js #loader {
			display: none;
		}

		.js #loader {
			display: block;
			position: absolute;
			left: 100px;
			top: 0;
		}

		.se-pre-con {
			position: fixed;
			left: 0px;
			top: 0px;
			width: 100%;
			height: 100%;
			z-index: 9999;
			background: url(${pageContext.request.contextPath}/images/claro.gif) center no-repeat #fff;
		}
	</style>
	<script type="text/javascript">
		$(window).load(function() {
			$(".se-pre-con").fadeOut("slow");
		});
	</script>
	<link rel="stylesheet" href="<c:url value="/resources/css/datepicker.css"/>">
	<script src="<c:url value="/resources/js/datepicker.js"/>"></script>
	<script>
		$(function() {
			$( "#toDate" ).datepicker({
				format:"yyyy-mm-dd"
			}).on('change', function(){
		        $('.datepicker').hide();
		    });
			$( "#fromDate" ).datepicker({
				format:"yyyy-mm-dd"
			}).on('change', function(){
		        $('.datepicker').hide();
		    });
		});
	</script>
	<!-- bootpag -->
<!--     <script src="//code.jquery.com/jquery-2.1.3.min.js"></script> -->
<!--     <script src="//netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script> -->
<!--     <script src="//raw.github.com/botmonster/jquery-bootpag/master/lib/jquery.bootpag.min.js"></script> -->
<!-- bootpag -->
	

	<script src="<c:url value='/resources/js/modernizr.js'/>"></script>


</head>

<body class="nav-md">
	<!-- <div class="se-pre-con"></div> -->
	<div class="container body">
		<div class="main_container">
				<jsp:include page="/WEB-INF/jsp/Admin/LeftMenu.jsp"/>
				<jsp:include page="/WEB-INF/jsp/Admin/TopNavigation.jsp"/>
 
			<!-- page content -->
			<div class="right_col">
				<div class="">
					<div class="page-title">
						<div class="title_left">
							<h3>Voucher List</h3>
						</div>
						<!-- <div class="title_right">
							<div
								class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
								<div class="input-group">
									<input type="text" class="form-control"
										placeholder="Search for..."> <span
										class="input-group-btn">
										<button class="btn btn-default" type="button">Go!</button>
									</span>
								</div>
							</div>
						</div> -->
					</div>
					<div class="clearfix"></div>
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<!--    <h2>Default Example <small>Users</small></h2> -->
								<%-- <form  method="get" action="${pageContext.request.contextPath}/Admin/UserListOnSearch">
								Search:<input type="text" name="search" id="mobile"  placeholder="enter mobile no./FirstName" >
								<input type="submit" value="search">
								</form> --%>
								<!-- search bar -->
							<div class="x_content">
                    <p class="text-muted font-13 m-b-30">
					  <form action="<c:url value="/Admin/filteredVoucherlist"/>" method="post" class="form form-inline">
						  From <input type="text" id="fromDate" name="fromDate" class="form-control" readonly/>
						  To <input type="text" id="toDate" name="toDate" class="form-control" readonly/>
						  <sec:csrfInput/>
						  <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-filter"></span></button>
					  </form>
                    </p>			 
									<div class="clearfix"></div>
								</div>
								<div class="x_content">
									<p class="text-muted font-13 m-b-30">
									</p>
									<table id="editedtable"
										class="table table-striped table-bordered">
											<tr id="xyz">
											<th>S.No</th>
												<th>Created Date</th>
												<th>Voucher Number</th>
												<th>Amount</th>
												<th>Expiry Date</th>
												<th>Expired</th>
												<th>Redeemed</th>
											</tr>
									</table>
								</div>
							</div>
						</div>
						<nav>
							<ul class="pagination" id="paginationn">
							  </ul>
							  </nav>
					</div>
				</div>


				<!-- footer content -->
				<jsp:include page="/WEB-INF/jsp/Admin/Footer.jsp"/>
				<!-- /footer content -->

			</div>
			<!-- /page content -->
		</div>

	</div>

	<div id="custom_notifications" class="custom-notifications dsp_none">
		<ul class="list-unstyled notifications clearfix"
			data-tabbed_notifications="notif-group">
		</ul>
		<div class="clearfix"></div>
		<div id="notif-group" class="tabbed_notifications"></div>
	</div>
	
<div class="container" align="center">

</div>

	<script
		src="${pageContext.request.contextPath}/resources/admin/js/bootstrap.min.js"></script>

	<!-- bootstrap progress js -->
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/progressbar/bootstrap-progressbar.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/nicescroll/jquery.nicescroll.min.js"></script>
	<!-- icheck -->
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/icheck/icheck.min.js"></script>

	<script
		src="${pageContext.request.contextPath}/resources/admin/js/custom.js"></script>



<script type="text/javascript">
function viewAccounts(userId,uContact) {
	
	var uId=userId;
	var contact=uContact;
	console.log(contact);
	console.log("inside View btn Ajax calling");
		  $.ajax({
			type : "POST",
			url : "${pageContext.request.contextPath}/Admin/SingleUserAccDetails",
			data : {
				userId : uId,
			},
			success : function(response) {
				var trHTML='';
				$("#user_details").html("User Mobile No: "+contact);
				$("#acc_table").html($("#acc_table_structure"));
				console.log(response);
				console.log(response.success);
				$(response.jsonArray).each(function(i,item){
					console.log("Acc Bal:: "+response.jsonArray[i].balance);
					trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>' + response.jsonArray[i].countryName +'</td><td>'+response.jsonArray[i].accNo+'</td><td>'+response.jsonArray[i].balance+'</td></tr>';
				});
				$("#acc_table").append(trHTML);
				$("#acc_model").append($("#acc_table"));
				$("#msg_resp").modal("show");

				
			}
		}); 
}
</script>

	<!-- pace -->
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/pace/pace.min.js"></script>

</body>
</html>
