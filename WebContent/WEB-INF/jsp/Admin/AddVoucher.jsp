<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<sec:csrfMetaTags/>
<title>Add Voucher</title>

<!-- Bootstrap core CSS -->
<link rel="icon" href='<c:url value="/resources/images/favicon.png"/>' type="image/png" />

<link
	href="${pageContext.request.contextPath}/resources/admin/css/bootstrap.min.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/admin/fonts/css/font-awesome.min.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/admin/css/animate.min.css"
	rel="stylesheet">
<!-- Custom styling plus plugins -->
<link
	href="${pageContext.request.contextPath}/resources/admin/css/custom.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/admin/css/icheck/flat/green.css"
	rel="stylesheet">
<script
	src="${pageContext.request.contextPath}/resources/admin/js/jquery.min.js"></script>
<script type="text/javascript">
var contextPath="${pageContext.request.contextPath}"
</script>
	<style>
		.no-js #loader {
			display: none;
		}

		.js #loader {
			display: block;
			position: absolute;
			left: 100px;
			top: 0;
		}

#example1 {
    border: 1px solid #e5e5e5;
    padding: 30px 20px;
}
		.se-pre-con {
			position: fixed;
			left: 0px;
			top: 0px;
			width: 100%;
			height: 100%;
			z-index: 9999;
			background: url(${pageContext.request.contextPath}/images/claro.gif) center no-repeat #fff;
		}
	</style>
	<script src="<c:url value='/resources/js/modernizr.js'/>"></script>

	<script type="text/javascript">
		$(window).load(function() {
			$(".se-pre-con").fadeOut("slow");
		});
	</script>

	<link rel="stylesheet" href="<c:url value="/resources/css/datepicker.css"/>">
	<script src="<c:url value="/resources/js/datepicker.js"/>"></script>
	<script>
		$(function() {
			$( "#expDate" ).datepicker({
				startDate: '-0m',
				format:"yyyy-mm-dd"
			}).on('change', function(){
		        $('.datepicker').hide();
		    	$("#error_admin_voucher_expDate").html("");
		    });
		});
	</script>
</head>
<body class="nav-md">
<div class="se-pre-con"></div>
<div class="container body">
		<div class="main_container">
				<jsp:include page="/WEB-INF/jsp/Admin/LeftMenu.jsp"/>
				<jsp:include page="/WEB-INF/jsp/Admin/TopNavigation.jsp"/>
			

			<!-- page content -->
			<div class="right_col" role="main">

				<div class="">
					<div class="page-title">
						<div class="title_left">
							<h3>Add Voucher</h3>
						</div>
					</div>
					<div class="clearfix"></div>

					<div class="row">

						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_content">
								 <div class="row">
									<div class="col-md-4 col-md-offset-4">
										<div id="example1">
											<!-- Smart Wizard -->
		                                    ${msg}
		                                    <sf:form modelAttribute="addMerchant" action="#" method="post" enctype="multipart/form-data">
											<div class="group_1">
											 <input type="text" class="form-control"  id="voucher_qntity" name="voucherQty" placeholder="Enter Voucher Quantity" onkeypress="$('#error_admin_voucher_qty').empty();" required />
											 <p id="error_admin_voucher_qty" style="color: red"></p>
										   </div>
										  <div class="group_1">
											<select name="voucherAmount" id="voucher_amt" class="form-control"  onchange="$('#error_admin_voucher_amt').empty();">
												<option value="#">SELECT AMOUNT</option>
												<option value="10">10</option>
												<option value="20">20</option>
												<option value="50">50</option>
												<option value="100">100</option>
											</select>
											<p id="error_admin_voucher_amt" style="color: red"></p>
										  </div>
										  <div class="group_1">
										  <input type="text" id="expDate" name="expiryDate" class="form-control" placeholder="Expiry Date" readonly/>
										  <p id="error_admin_voucher_expDate" style="color: red"></p>
										  </div><p id="msg"></p>
										  <div class="text-center">
										  	<button type="button" class="btn btn-success" id="voucher_add">Submit</button>
										  </div>
											</sf:form>
											</div>										
									</div>								 	
								 </div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
		<!-- Modal after error verification -->
<div id="failure_model" role="dialog" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content" style="width: 60%; margin-left: 25%;">
			<!-- <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div> -->
			<div class="modal-body">
				<center id="error_message" class="alert alert-danger"></center>
			</div>
		</div>
	</div>
</div>

<!-- Modal after success verification -->
<div id="success_model" role="dialog" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content" style="width: 60%; margin-left: 25%;">
			<!-- <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div> -->
			<div class="modal-body">
				<center id="success_message" class="alert alert-success"></center>
			</div>
		</div>
	</div>
</div>

	<jsp:include page="/WEB-INF/jsp/Admin/Footer.jsp"/>
			
			<!-- /page content -->
		

	<div id="custom_notifications" class="custom-notifications dsp_none">
		<ul class="list-unstyled notifications clearfix"
			data-tabbed_notifications="notif-group">
		</ul>
		<div class="clearfix"></div>
		<div id="notif-group" class="tabbed_notifications"></div>
	</div>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/bootstrap.min.js"></script>

	<!-- bootstrap progress js -->
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/progressbar/bootstrap-progressbar.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/nicescroll/jquery.nicescroll.min.js"></script>
	<!-- icheck -->
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/icheck/icheck.min.js"></script>

	<script
		src="${pageContext.request.contextPath}/resources/admin/js/custom.js"></script>
	<!-- form wizard -->
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/resources/admin/js/wizard/jquery.smartWizard.js"></script>
	<!-- pace -->
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/pace/pace.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			// Smart Wizard
			$('#wizard').smartWizard();

			function onFinishCallback() {
				$('#wizard').smartWizard('showMessage', 'Finish Clicked');
				//alert('Finish Clicked');
			}
		});

		$(document).ready(function(){
			// Smart Wizard
			$('#wizard_verticle').smartWizard({
				transitionEffect : 'slide'
			});
		});
	</script>
	
		<script type="text/javascript">
		$(document).ready(function(){
			
		 $("#voucher_qntity").keyup(function(){
			 this.value = this.value.replace(/[^0-9]/g,'');
		 });
		 
		$("#voucher_add").click(function () {
			var spinnerUrl = "Please wait <img src="+contextPath+"'/resources/images/spinner.gif' height='20' width='20'>"
			var voucher_qty=$('#voucher_qntity').val();
			var voucher_amt=$('#voucher_amt').val();
			var voucher_expDate=$('#expDate').val();
		
			console.log("amount is: "+voucher_amt);
			
			var valid=true;
			if (voucher_qty.length<=0) {
				valid=false;
				console.log("Invalid");
				$("#error_admin_voucher_qty").html("Please Enter Voucher Quantity");
			}else if (isNaN(voucher_qty)) {
				valid=false;
				console.log("Invalid");
				$("#error_admin_voucher_qty").html("Please Enter Numeric Value");
			}
			
			if (voucher_amt=='#') {
				valid=false;
				console.log("Invalid amount");
				$("#error_admin_voucher_amt").html("Please Select Voucher Amount");
			}
			if (voucher_expDate.length<=0) {
				valid=false;
				console.log("Invalid");
				$("#error_admin_voucher_expDate").html("Please Select Voucher Expiry Date");
			}
			console.log("Voucher length is :: "+voucher_qty.length);
			
			if (valid==true) {
				console.log("valid");
				$("#voucher_add").addClass("disabled");
				$("#voucher_add").html(spinnerUrl);
			$("#error_voucher").html("");
			$.ajax({
				type : "POST",
				contentType : "application/json",
				url : contextPath+"/Admin/AddVoucher",
				dataType : 'json',
				data : JSON.stringify({
					"voucherQty" :  voucher_qty,
					"voucherAmount" :  voucher_amt,
					"expiryDate" :  voucher_expDate,
				}),
				success : function(response) {
					$("#voucher_add").removeClass("disabled");
					console.log("Response is:: "+response);
					if (response.code.includes("S00")) {
						console.log("Inside valid");
						$("#success_message").html(response.message);
						$("#success_model").modal("show");
						setTimeout(function(){
							window.location = "/Admin/AddVoucher";
						},2000);
					}else{
						$("#error_message").html(response.message);
						$("#failure_model").modal("show");
					}
				}
			  });
			}
		});
		
		});
		</script>
	
</body>
</html>
