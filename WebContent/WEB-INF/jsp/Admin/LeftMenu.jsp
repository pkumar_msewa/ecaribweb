<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="col-md-3 left_col">
	<div class="left_col scroll-view">

		<div class="navbar nav_title" style="border: 0;">
			<a href="<c:url value="/Admin/Home"/>" class="site_title"><img
				src="<c:url value="/resources/images/vijayalogo.png"/>" alt=""
				style="width:75px; margin-left: 6PX;"></a>
		</div>
		<div class="clearfix"></div>
		<div class="profile">
			<div class="profile_pic">
				<img
					src="${pageContext.request.contextPath}/resources/admin/images/pic.png"
					alt="..." class="img-circle profile_img">
			</div>
			<div class="profile_info">
				<span>Welcome,</span>
				<h2>
					<c:out value="${UserName}" default="" escapeXml="true" />
				</h2>
			</div>
		</div>
		<!-- /menu prile quick info -->
		<br />
		<!-- sidebar menu -->
		<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
			<div class="menu_section">
				<h3>Admin Panel</h3>
				<ul class="nav side-menu">
					<li><a><i class="fa fa-home"></i> Users <span
							class="fa fa-chevron-down"></span></a>
						<ul class="nav child_menu" style="display: none">
							<li><a href="<c:url value="/Admin/UserList"/>">All</a></li>
							<li><a href="<c:url value="/Admin/GetUserByLocation"/>">Get User By Location</a></li>
							<li><a href="<c:url value="/Admin/VerifiedUsers"/>">Verified</a></li>
							<li><a href="<c:url value="/Admin/UnverifiedUsers"/>">Unverified</a></li>
							<li><a href="<c:url value="/Admin/BlockedUsers"/>">Blocked</a></li>
							<li><a href="<c:url value="/Admin/LockedUsers"/>">Locked</a></li>
							<%-- <li><a href="<c:url value="/Admin/PayAtStoreList"/>">PayAtStoreUsers</a></li> --%>
							
						
							<li><a href="<c:url value="/Admin/ActiveUsers"/>">Online</a></li>
							<li><a href="<c:url value="/Admin/MaleUsers"/>">Male
									User</a></li>
							<li><a href="<c:url value="/Admin/FemaleUsers"/>">Female
									User</a></li>
							<li><a href="<c:url value="/Admin/KYCusers"/>">KYC Users</a></li>
							<%-- <li><a href="<c:url value="/Admin/PredictAndWin"/>">predict And Win</a></li>
							<li><a href="<c:url value="/Admin/PredictWinners"/>">Send Money To Predict Winners</a></li> --%>
							<li><a href="<c:url value="/Admin/ShowAnalytics"/>">Analytics</a></li>
							<%-- <li><a href="<c:url value="/Admin/ReconcileEBS"/>">Reconcile</a></li> --%>
							<li><a href="<c:url value="/Admin/PartnerDetails"/>">Partner Details</a></li>
							
						</ul>
					</li>
					<li><a><i class="fa fa-edit"></i> Reports <span
							class="fa fa-chevron-down"></span></a>
						<ul class="nav child_menu" style="display: none">
							<li><a href="<c:url value="/Admin/TransactionReport"/>">Transactions
									Report</a></li>
							<li><a href="<c:url value="/Admin/PoolAccount"/>">Pool
									Account Reports</a></li>
							<li><a href="<c:url value="/Admin/CommissionAccount"/>">Commission
									Reports</a></li>
									<li><a href="<c:url value="/Admin/NikkiTransaction"/>">Nikki Chat
									Reports</a></li>
									<li><a href="<c:url value="/Admin/ImagicaTransaction"/>">Imagica Report
							</a></li>
									<%-- <li><a href="<c:url value="/Admin/TransactionReportNikki"/>">Nikki Chat
									Reports</a></li> --%>
							<li><a href="<c:url value="/Admin/SettlementAccount"/>">Settlement
									Reports</a></li>
									
									<li><a href="<c:url value="/Admin/DebitTransaction"/>">Debit
									Reports</a></li>
									<li><a href="<c:url value="/Admin/CreditTransaction"/>">Credit
									Reports</a></li>
							<li><a href="<c:url value="/Admin/NEFTList"/>">NEFT
									Requests</a></li>
							<li><a href="<c:url value="/Admin/refundEBS"/>">EBSRecon Report</a></li>
							<li><a href="<c:url value="/Admin/ReconciliationReport"/>">Reconciliation Reports</a></li>
							<%-- <li><a href="<c:url value="/Admin/BulkUpload"/>">Bulk Payment</a></li> --%>
							<li><a href="<c:url value="/Admin/upiTxn"/>">UPI Transaction</a></li>
							
						</ul></li>
							<li><a><i class="fa fa-edit"></i>Bulk Upload<span
							class="fa fa-chevron-down"></span></a>
						<ul class="nav child_menu" style="display: none">
							<li><a href="<c:url value="/Admin/BulkUpload"/>">Canteen Bulk Payment</a></li>
							<li><a href="<c:url value="/Admin/PredictAndWinUpload"/>">Predict and win Payment</a></li>
						</ul>
					</li>
						
						<li><a><i class="fa fa-edit"></i> RBI Reports <span
							class="fa fa-chevron-down"></span></a>
						<ul class="nav child_menu" style="display: none">
							<li><a href="<c:url value="/Admin/WalletBalOustanding"/>">User's Wallet Balance</a></li>
							<li><a href="<c:url value="/Admin/CustomerTransactionReport"/>">Transactions Report</a></li>
							<li><a href="<c:url value="/Admin/BankTransferList"/>">IMPS-NEFT Debit Report</a></li>
							<li><a href="<c:url value="/Admin/SummaryReport"/>">Summary Report</a></li>
							<li><a href="<c:url value="/Admin/MISReport"/>">Wallet MIS Report</a></li>
							<li><a href="<c:url value="/Admin/WalletLoadReport"/>">Wallet Load Report</a></li>
							<li><a href="<c:url value="/Admin/ListMerchantUpdated"/>" >Merchant On boarding Report</a></li>
							<%-- <li><a href="<c:url value="/Admin/InstantPayRefundReport"/>" href="#">Merchant Refunded Report</a></li>--%>
							<li><a href="<c:url value="/Admin/KYCusers"/>">KYC Users</a></li>
						</ul>
					</li>
						
					<li><a><i class="fa fa-desktop"></i> Logs <span
							class="fa fa-chevron-down"></span></a>
						<ul class="nav child_menu" style="display: none">
							<li><a href="<c:url value="/Admin/SMSLog"/>">Message
									Logs</a></li>
							<li><a href="<c:url value="/Admin/EmailLog"/>">Email
									Logs</a></li>
						</ul></li>
						
						<%-- <li><a><i class="fa fa-plane"></i> Travel <span
							class="fa fa-chevron-down"></span></a>
						<ul class="nav child_menu" style="display: none">
								<li><a href="<c:url value="/Admin/FlightdeatilList"/>">Flight Detail</a></li>
							    <li><a href="<c:url value="/Admin/BusDeatilsList"/>">Bus Details</a></li>
						</ul></li>
						
						<li><a><i class="fa fa-credit-card" aria-hidden="true"></i> Treat Card <span
							class="fa fa-chevron-down"></span></a>
						<ul class="nav child_menu" style="display: none">
								<li><a href="<c:url value="/Admin/TreatCardPlansList"/>">TreatCard Plans</a></li>
							    
						</ul>
						<ul class="nav child_menu" style="display: none">
								<li><a href="<c:url value="/Admin/TreatCardReport"/>">TreatCard Register List</a></li>
							    
						</ul>
						<ul class="nav child_menu" style="display: none">
								<li><a href="<c:url value="/Admin/TreatCardCount"/>">TreatCard Count List</a></li>
							    
						</ul></li>
						
								
						<li><a><i class="fa fa-credit-card" aria-hidden="true"></i> Woohoo GiftCard <span
							class="fa fa-chevron-down"></span></a>
						<ul class="nav child_menu" style="display: none">
								<li><a href="<c:url value="/Admin/WooHooCardList"/>">Woohoo Details</a></li>
						</ul>
						<ul class="nav child_menu" style="display: none">
								<li><a href="<c:url value="/Admin/WooHooCardList"/>">Woohoo Report</a></li>
						</ul>
						
						<ul class="nav child_menu" style="display: none">
								<li><a href="<c:url value="/Admin/Offers"/>">Add Offers</a></li>
						</ul>
						</li>
						
							
							<li><a><i class="fa fa-home"></i> HouseJoy<span
							class="fa fa-chevron-down"></span></a>
						<ul class="nav child_menu" style="display: none">
								<li><a href="<c:url value="/Admin/HouseJoyDetails"/>">HouseJoy Details</a></li>
						</ul></li>
							
						<li><a><i class="fa fa-train" aria-hidden="true"></i></i>Travelkhana<span
							class="fa fa-chevron-down"></span></a>
						<ul class="nav child_menu" style="display: none">
			          <li><a href="<c:url value="/Admin/TKOrderDeatilsList"/>">Order Details</a></li>
						</ul></li> --%>
				</ul>

			</div>
			<div class="menu_section">
				<h3>Others</h3>
				<ul class="nav side-menu">
				
				    <li><a><i class="fa fa-windows"></i> Voucher <span
							class="fa fa-chevron-down"></span></a>
						<ul class="nav child_menu" style="display: none">
							<li><a href="<c:url value="/Admin/AddVoucher"/>">Add
									Voucher</a></li>
							<li><a href="<c:url value="/Admin/VoucherList"/>">All
									Voucher List</a></li>
						</ul></li>
						
					<li><a><i class="fa fa-bug"></i> GCM Notification <span
							class="fa fa-chevron-down"></span></a>
						<ul class="nav child_menu" style="display: none">
							<li><a href="<c:url value='/Admin/SendNotification'/>">Send
									Notification</a></li>
						</ul></li>

					<li><a><i class="fa fa-windows"></i> Service <span
							class="fa fa-chevron-down"></span></a>
						<ul class="nav child_menu" style="display: none">
							<li><a href="#">Email</a></li>
							<li><a href="#">Bulk Email</a></li>
							<li><a href="<c:url value="/Admin/SendPromotionalSMS"/>">SMS</a></li>
							<li><a href="#">Bulk SMS</a></li>
							<li><a href="Version">Version</a></li>
						</ul></li>
					<li><a><i class="fa fa-bar-chart-o"></i> Merchant Service
							<span class="fa fa-chevron-down"></span></a>
						<ul class="nav child_menu" style="display: none">
							<li><a href="<c:url value="/Admin/ListMerchant"/>">Merchant
									List</a></li>
							<li><a href="<c:url value="/Admin/Merchant"/>">Add
									Merchant</a></li>
									<li><a href="<c:url value="/Admin/Donatee"/>">Add
									Donatee</a></li>
							<li><a href="<c:url value="/Admin/MerchantNEFTList"/>">Merchant
									NEFT Requests</a></li>
						</ul></li>

					<li><a><i class="fa fa-bar-chart-o"></i> Agent Service <span class="fa fa-chevron-down"></span></a>
						<ul class="nav child_menu" style="display: none">
							<li><a href="<c:url value="/Admin/ListAgent"/>">Agent List</a></li>
							<li><a href="<c:url value="/Admin/AddAgent"/>">Add Agent</a></li>
							<li><a><i class="fa fa-plane"></i> Travel <span class="fa fa-chevron-down"></span></a>
						<ul class="nav child_menu" style="display: none">
							<li><a href="<c:url value="/Admin/Agent/FlightdeatilList"/>">Flight Detail</a></li>
						    <li><a href="<c:url value="/Admin/Agent/BusDeatilsList"/>">Bus Details</a></li>
						</ul>
					</li>
							<li><a href="<c:url value="/Admin/AgentNEFTList"/>">Agent NEFT Requests</a>
							</li>
						</ul></li>

					<li><a><i class="fa fa-windows"></i> Promo Code <span
							class="fa fa-chevron-down"></span></a>
						<ul class="nav child_menu" style="display: none">
							<li><a href="GeneratePromoCode">Generate Promo Code</a></li>
							<li><a href="PromoCodeList">List Promo Codes</a></li>
							<li><a href="<c:url value='/Admin/PromoTransactions'/>">Transactions</a></li>
						</ul></li>

					<%-- <li><a><i class="fa fa-cc-visa"></i> Visa <span
							class="fa fa-chevron-down"></span></a>
						<ul class="nav child_menu" style="display: none">
							<li><a href="<c:url value="/Admin/AddVisaMerchant"/>">Add
									Merchants</a></li>
							<li><a
								href="<c:url value="/Merchant/VisaMerchantTransaction"/>">Merchant
									Transactions</a></li>
							<li><a href="<c:url value="/Merchant/VisaMerchantBalance"/>">Merchants
									Balance</a></li>
						</ul></li> --%>
				</ul>
			</div>
		</div>
		<!-- /sidebar menu -->

		<!-- /menu footer buttons -->
		<div class="sidebar-footer hidden-small">
			<a data-toggle="tooltip" data-placement="top" title="Settings"> <span
				class="glyphicon glyphicon-cog" aria-hidden="true"></span>
			</a> <a data-toggle="tooltip" data-placement="top" title="FullScreen">
				<span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
			</a> <a data-toggle="tooltip" data-placement="top" title="Lock"> <span
				class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
			</a> <a data-toggle="tooltip" data-placement="top" title="Logout"> <span
				class="glyphicon glyphicon-off" aria-hidden="true"></span>
			</a>
		</div>
		<!-- /menu footer buttons -->
	</div>
</div>