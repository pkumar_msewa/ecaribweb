<!DOCTYPE html>
<html lang="en">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<sec:csrfMetaTags/>
<title>eCarib | Active Users</title>

<!-- Bootstrap core CSS -->
<link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'
	type="image/png" />

<link
	href="<c:url value="/resources/admin/css/bootstrap.min.css"/>"
	rel="stylesheet">

<link
	href="<c:url value="/resources/admin/fonts/css/font-awesome.min.css"/>"
	rel="stylesheet">
<link
	href="<c:url value="/resources/admin/css/animate.min.css" />"
	rel="stylesheet">

<!-- Custom styling plus plugins -->
<link
	href="${pageContext.request.contextPath}/resources/admin/css/custom.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/admin/css/icheck/flat/green.css"
	rel="stylesheet">

<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/fixedHeader.bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/responsive.bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/scroller.bootstrap.min.css"
	rel="stylesheet" type="text/css" />

<script
	src="${pageContext.request.contextPath}/resources/admin/js/jquery.min.js"></script>

	<style>
		.no-js #loader {
			display: none;
		}

		.js #loader {
			display: block;
			position: absolute;
			left: 100px;
			top: 0;
		}

		.se-pre-con {
			position: fixed;
			left: 0px;
			top: 0px;
			width: 100%;
			height: 100%;
			z-index: 9999;
			background: url(/images/pq_large.gif) center no-repeat #fff;
		}
	</style>
	<script src="<c:url value='/resources/js/modernizr.js'/>"></script>
<script	src="${pageContext.request.contextPath}/resources/admin/js/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/Admin/jquery.twbsPagination.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/Admin/jquery.twbsPagination.min.js"></script>

	<script type="text/javascript">
	$(window).load(function() {
			$(".se-pre-con").fadeOut("slow");
		});
	</script>
<script type="text/javascript" >
function fetchMe(value){
	//  var tag = $(this);
	//  var paging=document.getElementById('kick2').innerHTML;
	var paging=(value-1);
	
	console.log(paging);
	 // paging=tag.;
	console.log("inside function");
	
$.ajax({
type:"POST",
url:"${pageContext.request.contextPath}/Admin/ActiveUsers",
data:{page:paging,size:'20'},
dataType:"json",
success:function(data){
var trHTML='';
	if(trHTML==''){
	$(".testingg").empty();
	
	 $(data.jsonArray).each(function(i,item){trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>UserName:' + data.jsonArray[i].username +'</td>'+'<td><a href="${pageContext.request.contextPath}/Admin/User'+data.jsonArray[i].contactNo+'">'+data.jsonArray[i].contactNo+'</a>'+'</td><td>Balance:'+data.jsonArray[i].balance+'</td></tr>'});
	  $('#editedtable').append(trHTML);
	 
	 

	}
	else
	{   $(data.jsonArray).each(function(i,item){trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>UserName:' + data.jsonArray[i].username +'</td>'+'<td><a href="${pageContext.request.contextPath}/Admin/User'+data.jsonArray[i].contactNo+'">'+data.jsonArray[i].contactNo+'</a>'+'</td><td>Balance:'+data.jsonArray[i].balance+'</td></tr>'});
	  $('#editedtable').append(trHTML);
	}
	 
}
});
		 }
$(document).ready(function() {
	 var paging='0';
	 var size='';
	 console.log("under ready...");
	 $.ajax({
			type:"POST",
			url:"${pageContext.request.contextPath}/Admin/ActiveUsers",
			data:{page:paging,size:'20'},
		dataType:"json",
		success:function(data){
			var trHTML='';
				if(trHTML==''){
				$(".testingg").empty();
				 $(data.jsonArray).each(function(i,item){trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>UserName:' + data.jsonArray[i].username +'</td>'+'<td><a href="${pageContext.request.contextPath}/Admin/User'+data.jsonArray[i].contactNo+'">'+data.jsonArray[i].contactNo+'</a></td><td>Balance:'+data.jsonArray[i].balance+'</td></tr>'});
				  $('#editedtable').append(trHTML);
				 
				 
			
				}
				else
					{
					 $(data.jsonArray).each(function(i,item){trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>UserName:' + data.jsonArray[i].username +'</td>'+'<td><a href="${pageContext.request.contextPath}/Admin/User'+data.jsonArray[i].contactNo+'">'+data.jsonArray[i].contactNo+'</a></td><td>Balance:'+data.jsonArray[i].balance+'</td></tr>'});
					  $('#editedtable').append(trHTML);
					 
				}
				
				$(function () {
					console.log("inside funt...");
				 $('#paginationn').twbsPagination({
					 totalPages: data.totalPages,
					 visiblePages: 10,
		         onPageClick: function (event, page) {
		        	 fetchMe(page);
				
		         }
				 });
				});
			 
					  
				
		}
	 });
	
	
});
</script>

</head>
<body class="nav-md">
<div class="se-pre-con"></div>
<div class="container body">
		<div class="main_container">
				<jsp:include page="/WEB-INF/jsp/Admin/LeftMenu.jsp"/>
				<jsp:include page="/WEB-INF/jsp/Admin/TopNavigation.jsp"/>

			
			<!-- page content -->
			<div class="right_col" role="main">
				<div class="">
					<div class="page-title">
						<div class="title_left">
							<h3>Active Users</h3>
						</div>
					</div>
					<div class="clearfix"></div>

					<div class="row">

						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<!--    <h2>Default Example <small>Users</small></h2> -->
									<form  method="post" action="${pageContext.request.contextPath}/Admin/UserListOnSearch" onsubmit="return validateMobile();">
								Search:<input type="text" id="mobile"  placeholder="enter mobile no." maxlength="10" onkeypress="return isNumberKey(event)">
								       <input type="hidden" id="mobile2" name="search"/>
								       <p id="error_msg" style="color:red; margin-left: 4.5%;"></p>
								       <p id="error_msg2" style="color:red; margin-left: 4.5%;"></p>
								       
								<input type="submit" value="search">
								</form>
									<div class="clearfix"></div>
								</div>
								<div class="x_content">
									<p class="text-muted font-13 m-b-30">
										<!--  DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: <code>$().DataTable();</code> -->
									</p>
									<table id="editedtable"
										class="table table-striped table-bordered">
										<thead>
											<tr>
												<th>S.No</th>
												<th>User Name</th>
												<th>Mobile Number</th>
												<th>Account</th>
												<!-- <th>Expense</th> -->
											</tr>
										</thead>
										
									</table>
								</div>
							</div>
						</div>
							<nav>
							<ul class="pagination" id="paginationn">
							  </ul>
							  </nav>
				</div>
				</div>

	
	<jsp:include page="/WEB-INF/jsp/Admin/Footer.jsp"/>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/bootstrap.min.js"></script>

	<!-- bootstrap progress js -->
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/progressbar/bootstrap-progressbar.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/nicescroll/jquery.nicescroll.min.js"></script>
	<!-- icheck -->
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/icheck/icheck.min.js"></script>

	<script
		src="${pageContext.request.contextPath}/resources/admin/js/custom.js"></script>


	<!-- Datatables -->
	<!-- <script src="js/datatables/js/jquery.dataTables.js"></script>
  <script src="js/datatables/tools/js/dataTables.tableTools.js"></script> -->

	<!-- Datatables-->
<!-- 	<script -->
<%-- 		src="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.js"></script> --%>
<!-- 	<script -->
<%-- 		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.bootstrap.js"></script> --%>
<!-- 	<script -->
<%-- 		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.buttons.min.js"></script> --%>
<!-- 	<script -->
<%-- 		src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.js"></script> --%>
<!-- 	<script -->
<%-- 		src="${pageContext.request.contextPath}/resources/admin/js/datatables/jszip.min.js"></script> --%>
<!-- 	<script -->
<%-- 		src="${pageContext.request.contextPath}/resources/admin/js/datatables/pdfmake.min.js"></script> --%>
<!-- 	<script -->
<%-- 		src="${pageContext.request.contextPath}/resources/admin/js/datatables/vfs_fonts.js"></script> --%>
<!-- 	<script -->
<%-- 		src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.html5.min.js"></script> --%>
<!-- 	<script -->
<%-- 		src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.print.min.js"></script> --%>
<!-- 	<script -->
<%-- 		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.fixedHeader.min.js"></script> --%>
<!-- 	<script -->
<%-- 		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.keyTable.min.js"></script> --%>
<!-- 	<script -->
<%-- 		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.responsive.min.js"></script> --%>
<!-- 	<script -->
<%-- 		src="${pageContext.request.contextPath}/resources/admin/js/datatables/responsive.bootstrap.min.js"></script> --%>
<!-- 	<script -->
<%-- 		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.scroller.min.js"></script> --%>


	<!-- pace -->
<script src="${pageContext.request.contextPath}/resources/admin/js/pace/pace.min.js"></script>
		
<script>
		function validateMobile(){
		  var Mobile=$('#mobile').val();
		  if (Mobile == "") {
				$('#error_msg').html("Mobile value cannot be blank");
				return false;
			}
		  
		  if (Mobile.length < 10) {
				$('#error_msg2').html("Please enter a valid mobile number");
				return false;
			}
		  var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9\+\/\=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/\r\n/g,"\n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}
			 $('#mobile2').val(Base64.encode(Mobile));
			return true;
		}
</script>
		
<script type="text/javascript"> 
    function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if ((charCode < 48 || charCode > 57))
        return false;
   return true;
   }
</script> 
		
<!-- <div class="container" align="center"> -->
<!-- <nav> -->
<!-- <ul class="pagination" id="paginationn"> -->
<!--   </ul> -->
<!--   </nav> -->
<!-- </div> -->
</body>

</html>