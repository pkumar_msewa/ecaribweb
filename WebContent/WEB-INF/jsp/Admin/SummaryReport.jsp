<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page isELIgnored="false"%>
<%@ page import="org.springframework.web.util.HtmlUtils" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html lang="en">
<%-- <%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%> --%>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicontripays, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<sec:csrfMetaTags/>
	<title>eCarib | Summary Report</title>

	<!-- Bootstrap core CSS -->
	<link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'
		  type="image/png" />
	<link
			href="${pageContext.request.contextPath}/resources/admin/css/bootstrap.min.css"
			rel="stylesheet">
	<link
			href="${pageContext.request.contextPath}/resources/admin/fonts/css/font-awesome.min.css"
			rel="stylesheet">
	<link
			href="${pageContext.request.contextPath}/resources/admin/css/animate.min.css"
			rel="stylesheet">
	<!-- Custom styling plus plugins -->
	<link
			href="${pageContext.request.contextPath}/resources/admin/css/custom.css"
			rel="stylesheet">
	<link
			href="${pageContext.request.contextPath}/resources/admin/css/icheck/flat/green.css"
			rel="stylesheet">
	<link
			href="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.css"
			rel="stylesheet" type="text/css" />
	<link
			href="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.css"
			rel="stylesheet" type="text/css" />
	<link
			href="${pageContext.request.contextPath}/resources/admin/js/datatables/fixedHeader.bootstrap.min.css"
			rel="stylesheet" type="text/css" />
	<link
			href="${pageContext.request.contextPath}/resources/admin/js/datatables/responsive.bootstrap.min.css"
			rel="stylesheet" type="text/css" />
	<link
			href="${pageContext.request.contextPath}/resources/admin/js/datatables/scroller.bootstrap.min.css"
			rel="stylesheet" type="text/css" />
	<script
			src="${pageContext.request.contextPath}/resources/admin/js/jquery.min.js"></script>
	<style>
		.no-js #loader {
			display: none;
		}

		.js #loader {
			display: block;
			position: absolute;
			left: 100px;
			top: 0;
		}

		.se-pre-con {
			position: fixed;
			left: 0px;
			top: 0px;
			width: 100%;
			height: 100%;
			z-index: 9999;
			background: url(${pageContext.request.contextPath}/images/pq_large.gif) center no-repeat #fff;
		}
	</style>
	<script src="<c:url value='/resources/js/modernizr.js'/>"></script>

	<link rel="stylesheet" href="<c:url value="/resources/css/datepicker.css"/>">
	<script src="<c:url value="/resources/js/datepicker.js"/>"></script>

	<script>
		$(function() {
			$("#toDate").datepicker({
				format : "yyyy-mm-dd"
			});
			$("#fromDate").datepicker({
				format : "yyyy-mm-dd"
			});
		});
	</script>

	<script type="text/javascript">
		$(window).load(function() {
			$(".se-pre-con").fadeOut("slow");
		});
	</script>


	



</head>

<body class="nav-md">
<div class="se-pre-con"></div>
<div class="container body">
	<div class="main_container">
		<jsp:include page="/WEB-INF/jsp/Admin/LeftMenu.jsp"/>
		<jsp:include page="/WEB-INF/jsp/Admin/TopNavigation.jsp"/>

		<!-- page content -->
		<div class="right_col" role="main">
			<div class="">
				<div class="page-title">
					<div class="title_left">
						<!-- <h3>Summary Report</h3> -->
					</div>
					<!-- <div class="title_right">
                        <div
                            class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                            <div class="input-group">
                                <input type="text" class="form-control"
                                    placeholder="Search for..."> <span
                                    class="input-group-btn">
                                    <button class="btn btn-default" type="button">Go!</button>
                                </span>
                            </div>
                        </div>
                    </div> -->
				</div>
				<div class="clearfix"></div>
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="x_panel">
							<div class="x_title">
								<h3>Summary Report</h3>
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
								<p class="text-muted font-13 m-b-30">
								<form id="formId" action="${pageContext.request.contextPath}/Admin/SummaryReport" method="get" class="form form-inline">
						  <input type="text" id="fromDate" name="startDate" value="${curDate}" class="form-control" readonly/>
						  <!-- 						  <sec:csrfInput/> -->
						  <button type="submit" class="btn btn-primary" title="Search"><span class="glyphicon glyphicon-filter"></span></button>
<!-- 					 onclick="getAllData();"  -->
					  </form><p></p>
									<!--  DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: <code>$().DataTable();</code> -->
								</p>
								
								<table id="datatable-buttons" class="table table-striped table-bordered date_sorted">
									<thead>
									<tr>
										<th style="text-align: center; background: #79B3C6; color: #fff;">S.No</th>
										<th style="text-align: center; background: #79B3C6; color: #fff;">Description</th>
										<th style="text-align: center; background: #79B3C6; color: #fff;">Date</th>
										<th style="text-align: center; background: #79B3C6; color: #fff;">Count</th>
										<th style="text-align: center; background: #79B3C6; color: #fff;">Amount</th>
									</tr>
									</thead>
									<tbody>
										<tr align="center">
											<td></td>
											<td>Wallet Op Balance</td>
											<td>${todaysDate}</td>
											<td></td>
											<td>${walletBalance}</td>
										</tr>

										<tr align="center">
											<th style="text-align: center;"><b>1</b></th>
											<th style="text-align: center;"><b><u>Credit Transactions</u></b></th>
											<th></th>
											<th></th>
											<th></th>
										</tr>

										<tr align="center">
											<td></td>
											<td>Load/Reload</td>
											<td></td>
											<td>${totalDailyLoadMoneyCount}</td>
											<td>${totalDailyLoadMoneyAmount}</td>
										</tr>

										<tr align="center">
											<td></td>
											<td>Cash Backs</td>
											<td></td>
											<td>${totalCountPromoCode}</td>
											<td>${totalPromoCode}</td>
										</tr>

										<tr align="center">
											<td></td>
											<td>Merchant Refunds</td>
											<td></td>
											<td>${totalCountMerchantRefund}</td>
											<td>${totalMerchantRefund}</td>
										</tr>
										<tr align="center">
											<td></td>
											<td>Wallet Transfer Cr</td>
											<td></td>
											<td>${countSendMoneyCredit}</td>
											<td>${totalSendMoneyCredit}</td>
										</tr>

										<tr align="center">
											<td style="background: #F7C7D8"></td>
											<td style="background: #F7C7D8"></td>
											<td style="background: #F7C7D8">Total</td>
											<td style="background: #F7C7D8"></td>
											<td style="background: #F7C7D8"><b>${totalCredit}</b></td>
										</tr>
										
										<tr>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
										</tr>

										<tr align="center">
											<td><b>2</b></td>
											<td><b><u>Debit Transactions</u></b></td>
											<td></td>
											<td></td>
											<td></td>
										</tr>

										<tr align="center">
											<td></td>
											<td>Merchant Payout</td>
											<td></td>
											<td>${topupTotalCountNow}</td>
											<td>${topupTotalNow}</td>
										</tr>

										<tr align="center">
											<td></td>
											<td>IMPS_NEFT Dr</td>
											<td></td>
											<td>${totalCountBankTranferNow}</td>
											<td>${totalBankTranferNow}</td>
										</tr>
										
										<tr align="center">
											<td></td>
											<td>Wallet Transfer Dr</td>
											<td></td>
											<td>${countSendMoneyDebit}</td>
											<td>${totalSendMoneyDebit}</td>
										</tr>

										<tr align="center">
											<td></td>
											<td>Exception/Wallet Closed</td>
											<td></td>
											<td>${walletCloseExcepCount}</td>
											<td>${walletCloseExcepBalNow}</td>
										</tr>

										<tr align="center">
											<td style="background: #F7C7D8"></td>
											<td style="background: #F7C7D8"></td>
											<td style="background: #F7C7D8"><b>Total</b></td>
											<td style="background: #F7C7D8"></td>
											<td style="background: #F7C7D8"><b>${totalDebit}</b></td>
										</tr>

										<tr>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
										</tr>

										<tr align="center">
											<td><b>3</b></td>
											<td><b><u>Wallet Closing Balance</u></b></td>
											<td>${todaysDate}</td>
											<td>Total</td>
											<td style="background: #F7C7D8">${walletClose}</td>
										</tr>
										<tr>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
										</tr>

										<tr align="center">
											<td><b>4</b></td>
											<td><u><b>Pool Account Closing Balance</b></u></td>
											<td>${todaysDate}</td>
											<td></td>
											<td style="background: #F7C7D8">${poolBalance}</td>
										</tr>

										<tr align="center">
											<td></td>
											<td></td>
											<td>Difference</td>
											<td></td>
											<td>${differece}</td>
										</tr>

									</tbody>
								</table>
								
							</div>
						</div>
					</div>
				</div>
			</div>


			<!-- footer content -->
			<jsp:include page="/WEB-INF/jsp/Admin/Footer.jsp"/>
			<!-- /footer content -->

		</div>
		<!-- /page content -->
	</div>

</div>

<div id="custom_notifications" class="custom-notifications dsp_none">
	<ul class="list-unstyled notifications clearfix"
		data-tabbed_notifications="notif-group">
	</ul>
	<div class="clearfix"></div>
	<div id="notif-group" class="tabbed_notifications"></div>
</div>

<script
		src="${pageContext.request.contextPath}/resources/admin/js/bootstrap.min.js"></script>

<!-- bootstrap progress js -->
<script
		src="${pageContext.request.contextPath}/resources/admin/js/progressbar/bootstrap-progressbar.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<script
		src="${pageContext.request.contextPath}/resources/admin/js/icheck/icheck.min.js"></script>

<script
		src="${pageContext.request.contextPath}/resources/admin/js/custom.js"></script>


<!-- Datatables-->
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.bootstrap.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.buttons.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/jszip.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/pdfmake.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/vfs_fonts.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.html5.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.print.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.fixedHeader.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.keyTable.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.responsive.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/responsive.bootstrap.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.scroller.min.js"></script>
<script
		src="//cdn.datatables.net/plug-ins/1.10.11/sorting/date-dd-MMM-yyyy.js"></script>

<!-- pace -->
<script
		src="${pageContext.request.contextPath}/resources/admin/js/pace/pace.min.js"></script>
<script>
	var handleDataTableButtons = function() {
		"use strict";
		0 !== $("#datatable-buttons").length
		&& $("#datatable-buttons").DataTable({
			
			columnDefs : [ {
							type : 'date-dd-mmm-yyyy',
							targets : 3
						} ],
			dom : "Bfrtip",
			buttons : [ {
				extend : "copy",
				className : "btn-sm"
			}, {
				extend : "csv",
				className : "btn-sm"
			}, {
				extend : "excel",
				className : "btn-sm"
			}, {
				extend : "pdf",
				className : "btn-sm"
			}, {
				extend : "print",
				className : "btn-sm"
			} ],
			ordering : false,
    		paging: false,
			responsive : !0
		})
	}, TableManageButtons = function() {
		"use strict";
		return {
			init : function() {
				handleDataTableButtons()
			}
		}
	}();
</script>

<!-- <script>
	$(document).ready(function() {
    $('#datatable-buttons').DataTable( {
        "paging":   false,
        "ordering": false,
        "info":     false
    } );
} );
</script> -->

<script type="text/javascript">
		$(document).ready(function() {
			$('#datatable').dataTable();
			$('#datatable-keytable').DataTable({
				keys : true,
			});
			$('#datatable-responsive').DataTable();
			$('#datatable-scroller').DataTable({
				ajax : "js/datatables/json/scroller-demo.json",
				deferRender : true,
				scrollY : 300,
				scrollCollapse : true,
				scroller : true
			});
			var table = $('#datatable-fixed-header').DataTable({
				fixedHeader : true
			});
		});
		TableManageButtons.init();
	</script>
	<script type="text/javascript">
		$(document)
				.ready(
						function() {

							var cb = function(start, end, label) {
								console.log(start.toISOString(), end
										.toISOString(), label);
								$('#reportrange span').html(
										start.format('MMMM D, YYYY') + ' - '
												+ end.format('MMMM D, YYYY'));
								//alert("Callback has fired: [" + start.format('MMMM D, YYYY') + " to " + end.format('MMMM D, YYYY') + ", label = " + label + "]");
							}

							var optionSet1 = {
								startDate : moment().subtract(29, 'days'),
								endDate : moment(),
								minDate : '01/01/2012',
								maxDate : '12/31/2015',
								dateLimit : {
									days : 60
								},
								showDropdowns : true,
								showWeekNumbers : true,
								timePicker : false,
								timePickerIncrement : 1,
								timePicker12Hour : true,
								ranges : {
									'Today' : [ moment(), moment() ],
									'Yesterday' : [
											moment().subtract(1, 'days'),
											moment().subtract(1, 'days') ],
									'Last 7 Days' : [
											moment().subtract(6, 'days'),
											moment() ],
									'Last 30 Days' : [
											moment().subtract(29, 'days'),
											moment() ],
									'This Month' : [ moment().startOf('month'),
											moment().endOf('month') ],
									'Last Month' : [
											moment().subtract(1, 'month')
													.startOf('month'),
											moment().subtract(1, 'month')
													.endOf('month') ]
								},
								opens : 'left',
								buttonClasses : [ 'btn btn-default' ],
								applyClass : 'btn-small btn-primary',
								cancelClass : 'btn-small',
								format : 'MM/DD/YYYY',
								separator : ' to ',
								locale : {
									applyLabel : 'Submit',
									cancelLabel : 'Clear',
									fromLabel : 'From',
									toLabel : 'To',
									customRangeLabel : 'Custom',
									daysOfWeek : [ 'Su', 'Mo', 'Tu', 'We',
											'Th', 'Fr', 'Sa' ],
									monthNames : [ 'January', 'February',
											'March', 'April', 'May', 'June',
											'July', 'August', 'September',
											'October', 'November', 'December' ],
									firstDay : 1
								}
							};
							$('#reportrange span').html(
									moment().subtract(29, 'days').format(
											'MMMM D, YYYY')
											+ ' - '
											+ moment().format('MMMM D, YYYY'));
							$('#reportrange').daterangepicker(optionSet1, cb);
							$('#reportrange').on('show.daterangepicker',
									function() {
										console.log("show event fired");
									});
							$('#reportrange').on('hide.daterangepicker',
									function() {
										console.log("hide event fired");
									});
							$('#reportrange')
									.on(
											'apply.daterangepicker',
											function(ev, picker) {
												console
														.log("apply event fired, start/end dates are "
																+ picker.startDate
																		.format('MMMM D, YYYY')
																+ " to "
																+ picker.endDate
																		.format('MMMM D, YYYY'));
											});
							$('#reportrange').on('cancel.daterangepicker',
									function(ev, picker) {
										console.log("cancel event fired");
									});
							$('#options1').click(
									function() {
										$('#reportrange').data(
												'daterangepicker').setOptions(
												optionSet1, cb);
									});
							$('#options2').click(
									function() {
										$('#reportrange').data(
												'daterangepicker').setOptions(
												optionSet2, cb);
									});
							$('#destroy').click(
									function() {
										$('#reportrange').data(
												'daterangepicker').remove();
									});
						});
	</script>

</body>

</html>
