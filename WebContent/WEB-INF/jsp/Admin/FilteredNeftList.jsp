<!DOCTYPE html>
<html lang="en">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<sec:csrfMetaTags/>
<title>eCarib | Neft Transaction</title>

<!-- Bootstrap core CSS -->
<link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'
	type="image/png" />

<link
	href="${pageContext.request.contextPath}/resources/admin/css/bootstrap.min.css"
	rel="stylesheet">

<link
	href="${pageContext.request.contextPath}/resources/admin/fonts/css/font-awesome.min.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/admin/css/animate.min.css"
	rel="stylesheet">

<!-- Custom styling plus plugins -->
<link
	href="${pageContext.request.contextPath}/resources/admin/css/custom.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/admin/css/icheck/flat/green.css"
	rel="stylesheet">

<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/fixedHeader.bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/responsive.bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<!-- <link -->
<%-- 	href="${pageContext.request.contextPath}/resources/admin/js/datatables/scroller.bootstrap.min.css" --%>
<!-- 	rel="stylesheet" type="text/css" /> -->

<script
	src="${pageContext.request.contextPath}/resources/admin/js/jquery.min.js"></script>
	<style>
		.no-js #loader {
			display: none;
		}

		.js #loader {
			display: block;
			position: absolute;
			left: 100px;
			top: 0;
		}

		.se-pre-con {
			position: fixed;
			left: 0px;
			top: 0px;
			width: 100%;
			height: 100%;
			z-index: 9999;
			background: url(/images/pq_large.gif) center no-repeat #fff;
		}
	</style>
	<script src="<c:url value='/resources/js/modernizr.js'/>"></script>

<link rel="stylesheet" href="<c:url value="/resources/css/datepicker.css"/>">
	<script src="<c:url value="/resources/js/datepicker.js"/>"></script>
	<script>
		$(function() {
			$( "#toDate" ).datepicker({
				format:"yyyy-mm-dd"
			});
			$( "#fromDate" ).datepicker({
				format:"yyyy-mm-dd"
			});
		});
	</script>

	<script type="text/javascript">
		$(window).load(function() {
 			$(".se-pre-con").fadeOut("slow");
 		});
	</script>
<script src="${pageContext.request.contextPath}/resources/js/Admin/jquery.twbsPagination.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/Admin/jquery.twbsPagination.min.js"></script>
<script type="text/javascript" >

</script>
</head>
<body class="nav-md">
	<div class="se-pre-con"></div>
	<div class="container body">
		<div class="main_container">

				<jsp:include page="/WEB-INF/jsp/Admin/LeftMenu.jsp"/>
				<jsp:include page="/WEB-INF/jsp/Admin/TopNavigation.jsp"/>
			<!-- page content -->
			<div class="right_col" role="main">
				<div class="">
					<div class="page-title">
						<div class="title_left">
							<h3>UPI Transactions</h3>
						</div>
					</div>
					<div class="clearfix"></div>

					<div class="row">

						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<ul class="nav navbar-right panel_toolbox">
										<li><a href="#"><i class="fa fa-chevron-up"></i></a></li>
										<li class="dropdown"><a href="#" class="dropdown-toggle"
											data-toggle="dropdown" role="button" aria-expanded="false"><i
												class="fa fa-wrench"></i></a> 
                      </li>
										<li><a href="#"><i class="fa fa-close"></i></a></li>
									</ul>
									<div class="clearfix"></div>
								</div>
								<div class="x_content">
								<form action="#" method="post" class="form form-inline">
                                From <input type="text" id="fromDate" name="fromDate" class="form-control" readonly/> 
                                 To <input type="text" id="toDate" name="toDate" class="form-control" readonly/> 
                                <sec:csrfInput/>
                                <button type="submit" class="btn btn-primary filtered_neft_Request"><span class="glyphicon glyphicon-filter"></span></button> 
                            </form> 
									<p class="text-muted font-13 m-b-30">
										<!--  DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: <code>$().DataTable();</code> -->
									</p>
									<table id="editedtable"
										class="table table-striped table-bordered">
										<thead>
									<tr>
                                        <th>S.No</th>
                                        <th>created</th>
                                        <th>amount</th>
                                        <th>beneficiaryAccountNumber</th>
                                        <th>beneficiaryName</th>
                                        <th>transactionRefNo</th>
                                        <th>IFSC Code</th>
                                        <th>userName</th>
                                        <th>firstName</th>
                                        <th>AccountType</th>
                                        <th>VpayQwik Debit Pool Account</th>
                                        <th>Status</th>
                                    </tr>
										</thead>

									</table>
								</div>
							</div>
						</div>
	<nav>
<ul class="pagination" id="paginationn">
  </ul>
  </nav>
					</div>
				</div>


               <jsp:include page="/WEB-INF/jsp/Admin/Footer.jsp"/>
			</div>
			<!-- /page content -->
		</div>

	</div>

	<div id="custom_notifications" class="custom-notifications dsp_none">
		<ul class="list-unstyled notifications clearfix"
			data-tabbed_notifications="notif-group">
		</ul>
		<div class="clearfix"></div>
		<div id="notif-group" class="tabbed_notifications"></div>
	</div>
	
	<script src="https://github.com/btechco/btechco_excelexport"></script>

	<script
		src="${pageContext.request.contextPath}/resources/admin/js/bootstrap.min.js"></script>

	<!-- bootstrap progress js -->
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/progressbar/bootstrap-progressbar.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/nicescroll/jquery.nicescroll.min.js"></script>
	<!-- icheck -->
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/icheck/icheck.min.js"></script>

	<script
		src="${pageContext.request.contextPath}/resources/admin/js/custom.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/pace/pace.min.js"></script>
		
		<script src="js/datatables/js/jquery.dataTables.js"></script>
		
<script src="js/datatables/tools/js/dataTables.tableTools.js"></script>
		<script
        src="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.js"></script>
<script
        src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.bootstrap.js"></script>
<script
        src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.buttons.min.js"></script>
<script
        src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.js"></script>
<script
        src="${pageContext.request.contextPath}/resources/admin/js/datatables/jszip.min.js"></script>
<script
        src="${pageContext.request.contextPath}/resources/admin/js/datatables/pdfmake.min.js"></script>
<script
        src="${pageContext.request.contextPath}/resources/admin/js/datatables/vfs_fonts.js"></script>
<script
        src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.html5.min.js"></script>
<script
        src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.print.min.js"></script>
<script
        src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.fixedHeader.min.js"></script>
<script
        src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.keyTable.min.js"></script>
<script
        src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.responsive.min.js"></script>
<script
        src="${pageContext.request.contextPath}/resources/admin/js/datatables/responsive.bootstrap.min.js"></script>
<script
        src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.scroller.min.js"></script>


<!-- pace -->
<script
        src="${pageContext.request.contextPath}/resources/admin/js/pace/pace.min.js"></script>
<script>

function fetchMe(value){
	
$.ajax({
type:"POST",
url:"${pageContext.request.contextPath}/Admin/FilteredNeftList",
data:{
	page:value,
	size:'10'
	},
dataType:"json",
success:function(data){
	console.log("data:"+data)
var trHTML='';
	if(trHTML==''){
	$(".testingg").empty();
	$(data.jsonArray).each(function(i,item){trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>' + data.jsonArray[i].transactionDate + '</td><td>' + data.jsonArray[i].amount +'</td>'+'<td>'+data.jsonArray[i].accountNumber+'</td><td>'+data.jsonArray[i].accountName+'</td><td>'+data.jsonArray[i].transactionID+'</td><td>'+data.jsonArray[i].ifscCode+'</td><td>'+data.jsonArray[i].mobileNo+'</td><td>'+data.jsonArray[i].name+'</td><td>'+'KYC'+'</td><td>'+"133100037672007"+'</td><td>'+data.jsonArray[i].status+'</td></tr>'});
	  $('#editedtable').append(trHTML);
	}
	else
	{
		$(data.jsonArray).each(function(i,item){trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>' + data.jsonArray[i].transactionDate + '</td><td>' + data.jsonArray[i].amount +'</td>'+'<td>'+data.jsonArray[i].accountNumber+'</td><td>'+data.jsonArray[i].accountName+'</td><td>'+data.jsonArray[i].transactionID+'</td><td>'+data.jsonArray[i].ifscCode+'</td><td>'+data.jsonArray[i].mobileNo+'</td><td>'+data.jsonArray[i].name+'</td><td>'+'KYC'+'</td><td>'+"133100037672007"+'</td><td>'+data.jsonArray[i].status+'</td></tr>'});
		  $('#editedtable').append(trHTML);
	}
}
});
		 }
$(document).ready(function() {
	$(".filtered_neft_Request").click(function() {
		console.log("inside this function");
		var fromDate = $("#fromDate").val();
		var toDate =  $('#toDate').val();
	 $.ajax({
			type:"POST",
			url:"${pageContext.request.contextPath}/Admin/FilteredNeftList",
			data:{
				page:'0',
				size:'10',
				fromDate:fromDate,
				toDate:toDate
				},
		dataType:"json",
		success:function(data){
			var trHTML='';
				if(trHTML==''){
				$(".testingg").empty();
				$(data.jsonArray).each(function(i,item){trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>' + data.jsonArray[i].transactionDate + '</td><td>' + data.jsonArray[i].amount +'</td>'+'<td>'+data.jsonArray[i].accountNumber+'</td><td>'+data.jsonArray[i].accountName+'</td><td>'+data.jsonArray[i].transactionID+'</td><td>'+data.jsonArray[i].ifscCode+'</td><td>'+data.jsonArray[i].mobileNo+'</td><td>'+data.jsonArray[i].name+'</td><td>'+'KYC'+'</td><td>'+"133100037672007"+'</td><td>'+data.jsonArray[i].status+'</td></tr>'});
				  $('#editedtable').append(trHTML);
				}
				else
				{
					$(data.jsonArray).each(function(i,item){trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>' + data.jsonArray[i].transactionDate + '</td><td>' + data.jsonArray[i].amount +'</td>'+'<td>'+data.jsonArray[i].accountNumber+'</td><td>'+data.jsonArray[i].accountName+'</td><td>'+data.jsonArray[i].transactionID+'</td><td>'+data.jsonArray[i].ifscCode+'</td><td>'+data.jsonArray[i].mobileNo+'</td><td>'+data.jsonArray[i].name+'</td><td>'+'KYC'+'</td><td>'+"133100037672007"+'</td><td>'+data.jsonArray[i].status+'</td></tr>'});
					  $('#editedtable').append(trHTML);
					 
				}
				$(function () {
				 $('#paginationn').twbsPagination({
					 totalPages: data.totalPages,
					 visiblePages: 10,
		         onPageClick: function (event, page) {
		        	 page = page - 1;
		        	 fetchMe(page);
		         }
				 });
				});
			    TableManageButtons.init();
		}
	 });
	});
});
    var handleDataTableButtons = function() {
        "use strict";
        0 !== $("#editedtable").length
        && $("#editedtable").DataTable({
            dom : "Bfrtip",
            buttons : [ {
                extend : "copy",
                className : "btn-sm"
            }, {
                extend : "csv",
                className : "btn-sm"
            }, {
                extend : "excel",
                className : "btn-sm"
            }, {
                extend : "pdf",
                className : "btn-sm"
            }, {
                extend : "print",
                className : "btn-sm"
            } ],
            responsive : !0
        })
    }, TableManageButtons = function() {
        "use strict";
        return {
            init : function() {
                handleDataTableButtons()
            }
        }
    }();
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#datatable').dataTable();
        $('#datatable-keytable').DataTable({
            keys : true
        });
        $('#datatable-responsive').DataTable();
        $('#datatable-scroller').DataTable({
            ajax : "js/datatables/json/scroller-demo.json",
            deferRender : true,
            scrollY : 380,
            scrollCollapse : true,
            scroller : true
        });
        var table = $('#datatable-fixed-header').DataTable({
            fixedHeader : true
        });
    });
    
</script>
		
		
<div class="container" align="center">

</div>

</body>

</html>