<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<sec:csrfMetaTags />
<title>eCarib | Add Agent</title>

<!-- Bootstrap core CSS -->
<link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'
	type="image/png" />

<link
	href="${pageContext.request.contextPath}/resources/admin/css/bootstrap.min.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/admin/fonts/css/font-awesome.min.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/admin/css/animate.min.css"
	rel="stylesheet">
<!-- Custom styling plus plugins -->
<link
	href="${pageContext.request.contextPath}/resources/admin/css/custom.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/admin/css/icheck/flat/green.css"
	rel="stylesheet">
<script
	src="${pageContext.request.contextPath}/resources/admin/js/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script src="https://harvesthq.github.io/chosen/chosen.jquery.js"></script> 
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
<style>
.no-js #loader {
	display: none;
}

.js #loader {
	display: block;
	position: absolute;
	left: 100px;
	top: 0;
}

.se-pre-con {
	position: fixed;
	left: 0px;
	top: 0px;
	width: 100%;
	height: 100%;
	z-index: 9999;
	background: url(/images/pq_large.gif) center no-repeat #fff;
}
</style>
<script src="<c:url value='/resources/js/modernizr.js'/>"></script>

<script type="text/javascript">
	$(window).load(function() {
		$(".se-pre-con").fadeOut("slow");
		$("#firstName").val(" ");
		$("#lastName").val(" ");
		$("#agencyName").val(" ");
		$("#emailAddress").val(" ");
		$("#password").val("");
		$("#address").val(" ");
		$("#pinCode").val(" ");
		$("#city").val(" ");
		$("#state").val(" ");
		$("#panNo").val(" ");
		$("#agentBranch_Name").val(" ");
		$("#agentBankIfscCode").val(" ");
		$("#mobileNo").val(" ");
	});
	$(".js-example-tags").select2({
		  tags: true
	});
</script>


</head>
<body class="nav-md">
	<div class="se-pre-con"></div>
	<div class="container body">
		<div class="main_container">
			<jsp:include page="/WEB-INF/jsp/Admin/LeftMenu.jsp" />
			<jsp:include page="/WEB-INF/jsp/Admin/TopNavigation.jsp" />


			<!-- page content -->
			<div class="right_col" role="main">

				<div class="">
					<div class="page-title">
						<div class="title_left">
							<h3>Add Agent</h3>
						</div>

						<div class="title_right">
							<div
								class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
								<div class="input-group">
									<input type="text" class="form-control"
										placeholder="Search for..."> <span
										class="input-group-btn">
										<button class="btn btn-default" type="button">Go!</button>
									</span>
								</div>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>

					<div class="row">

						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_content">
								 <div class="col-md-5">
									<h2>Basic Info</h2>
								 <form action="<c:url value="/Admin/SaveAgentDetail"/>" enctype="multipart/form-data" 
					                 onSubmit = "return validateForm()" method="post">
										<div class="form-group">
											<label>Enter First Name </label>
											 
											<input type="text" name="firstName" class="form-control" id="firstName" maxlength="25">
											<span class="error" id="error_first_name" class="error"></span>
										</div>
										
										<div class="form-group">
											<label>Enter Last Name</label>
											
											<input type="text" name="lastName" class="form-control" id="lastName" maxlength="25">
										    <p class="error" id="error_lastName" class="error"></p>
										</div>
										
										<div class="form-group">
											<label>Enter Agency Name</label>
											<input type="text" name="agencyName" class="form-control" id="agencyName" maxlength="25">
										    <p class="error" id="error_agencyName" class="error"></p>
										</div>
										
										<div class="form-group">
											<label>Enter Email</label>
											<input type="text" name="emailAddress" class="form-control" id="emailAddress" >
											<p class="error" id="error_emailAddress" class="error"></p>
										</div>
										
										<div class="form-group">
											<label>Enter Mobile No.</label>
												<input type="text" name="mobileNo" id="mobileNo" minlength="10" maxlength="10" class="form-control">
											<p class="error" id="error_mobileNo" class="error"></p>
										</div>
										
										<div class="form-group">
											<label>Enter password</label>
											<input type="password" name="password" class="form-control" minlength ="6" id="password" >
											<p class="error" id="error_password" class="error"></p>
										</div>
										
										<div class="form-group">
											<label>Address</label>
											<input type="text" name="address" class="form-control" id="address">
										    <p class="error" id="error_address" class="error"></p> 
										</div>
										
										<div class="form-group">
											<label>Enter PinCode Number</label>
											<input type="text" name="pinCode" class="form-control" id="pinCode" maxlength="6">
											<p class="error" id="error_pincode" class="error"></p>
										</div>
										
										<div class="form-group">
											<label>Enter City</label>
											<input type="text" name="city" class="form-control" maxlength="25" id="city" readonly>
										    <p class="error" id="error_city" class="error"></p> 
										</div>
										
										<div class="form-group">
											<label>Enter State</label>
												<input type="text" name="state" class="form-control" maxlength="25" id="state" readonly>
												<p class="error" id="error_state" class="error"></p>
										</div>
										
										<input type="hidden" name="country" value="India"/>
    									<div class="form-group">
											<label>Enter Pan No.</label>
											<input type="text" name="panNo" class="form-control" id="panNo" >
											<p class="error" id="error_panno" class="error"></p>
										</div>
    									
    									<label>Enter PanCard Image</label><br>
    									<div class="form-group">
    										<input type="file" name="panCardImg" accept="image/*" onchange="return imgchange(this)" id="panCardImg" placeholder="Enter Pan Card Img"/>
    										<img id="imgs" style="margin-left:10px;" width="90%" height="200px" ></img>
    										<p class="error" id="error_panCardImg"></p>
    									</div><br/>
										<div class="form-group">
											<label>Enter Account Type</label>
											
										<select id="agentAccountName"  name="agentAccountName" class="form-control" style="width:410px;" >
												<option value="saving" selected>saving</option>
												<option value="current">current</option>
										</select>
											<p class="error" id="error_agentAccountName" class="error"></p>
										</div>
										
										<div class="form-group">
											<!-- <label>Enter Bank Name</label> -->
											<select id="agentBankName" name="agentBankName" class="form-control" style="width:410px; height:50px;">
												<option value="#">Select bank</option>
											</select>
											<p class="error" id="error_agentBankName" class="error"></p>
										</div>
								
										<div class="form-group">
											<label>Enter Account Number</label>
												<input type="text" name="agentAccountNumber" id="agentAccountNumber" maxlength="16" class="form-control">
												<p class="error" id="error_agentAccountNumber" class="error"></p>
										</div>
										
										<div class="form-group">
											<label>Enter Branch Name</label>
												<input type="text" name="agentbranchname" id="agentBranch_Name" class="form-control">
											<p class="error" id="error_Branch_Name" class="error"></p>
										</div>
										
										<div class="form-group">
											<label>Enter IFSC Code</label>
												<input type="text" name="agentBankIfscCode" id="agentBankIfscCode" maxlength="25" class="form-control">
										    <p class="error" id="error_agentBankIfscCode" class="error"></p>
										</div>
										
										<button type="submit" class="btn"  value="Add Agent" >Add Agent</button>
									</form>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="/WEB-INF/jsp/Admin/Footer.jsp" />
	</div>
	<!-- /page content -->
	</div>

	</div>

	<div id="custom_notifications" class="custom-notifications dsp_none">
		<ul class="list-unstyled notifications clearfix"
			data-tabbed_notifications="notif-group">
		</ul>
		<div class="clearfix"></div>
		<div id="notif-group" class="tabbed_notifications"></div>
	</div>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/bootstrap.min.js"></script>

	<!-- bootstrap progress js -->
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/progressbar/bootstrap-progressbar.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/nicescroll/jquery.nicescroll.min.js"></script>
	<!-- icheck -->
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/icheck/icheck.min.js"></script>

	<script
		src="${pageContext.request.contextPath}/resources/admin/js/custom.js"></script>
	<!-- form wizard -->
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/resources/admin/js/wizard/jquery.smartWizard.js"></script>
	<!-- pace -->
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/pace/pace.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			// Smart Wizard
			$('#wizard').smartWizard();

			function onFinishCallback() {
				$('#wizard').smartWizard('showMessage', 'Finish Clicked');
				//alert('Finish Clicked');
			}
		});

		$(document).ready(function() {
			// Smart Wizard
			$('#wizard_verticle').smartWizard({
				transitionEffect : 'slide'
			});
			
		});

	 $(document).ready(function() {
		 
		  var agentRespCode = $("#agentRespCode").val();
	      var agentResp = $("#agentResp").val();
	      
	      if(agentRespCode.includes("S00")){
	    	    $('#firstName').val("");
				$('#lastName').val("");
				$('#agencyName').val("");
				$('#emailAddress').val("");
				$('#address').val("");
				$('#city').val("");
				$('#state').val("");

				$('#pinCode').val("");
				$('#password').val("");
				$('#panNo').val("");
				$("#panCardImg").val("");
				$('#mobileNo').val("");
			//	$('#agentBankName').val("");
				$('#agentAccountNumber').val("");
				$('#agentBranch_Name').val("");
				$('#agentBankIfscCode').val("");
				$('#error_password').html(" ");
				
				$("#verifiedMessageagent").modal('show');
				$("#success_verification_message_agent").html(agentResp+" Please login to continue");
					var timeout = setTimeout(function(){
			            $("#verifiedMessageagent").modal('hide');
			          }, 3000);
					$("#agentRespCode").html("");
	       }
	      else if(agentRespCode.includes("F00")){
	    	  $("#errorMessage").modal('show');
			  $("#error_message").html(agentResp);
			  $("#agentRespCode").html("");
		  }
	      else if(agentRespCode.includes("F04")){
	    	  $("#agentRespCode").html("");
	    	  $("#errorMessage").modal('show');
			  $("#error_message").html(agentResp);
		  }
		 
		 $("#agentBankName").select2();
			$.ajax({
				type : "POST",
				contentType : "application/json",
				url : "/Admin/Agent/BankList",
				dataType : 'json',
				data : JSON.stringify({
				}),
				success : function(response) {
				if(response.code.includes("S00")){
					for(var i=0;i<response.details2.length;i++){
						$('#agentBankName').append('<option value="' + response.details2[i].bankName + '">' + response.details2[i].bankName + '</option>');
					  }
					}
				}
			});
		 
		 
			$("#error_first_name").html("");
			$("#error_lastName").html("");
			$("#error_agencyName").html("");
			$("#error_emailAddress").html("");
			$("#error_address").html("");
			$("#error_city").html("");
			$("#error_state").html("");
			$("#error_pincode").html("");
			$("#error_password").html("");
			$("#error_panno").html("");
		
			$("#error_mobileNo").html("");
			$("#error_agentAccountName").html("");
			$("#error_Branch_Name").html("");
			$("#error_agentAccountNumber").html("");
			$("#error_agentBankIfscCode").html("");
			var valid = true;
			var pattern = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2}|com|org|net|edu|gov|mil|biz|info|mobi|name|aero|asia|jobs|museum|co|in)"
			var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
			
			var passwordPattern = "[a-zA-z0-9]"; //pattern for password
			var namePattern = /^[a-zA-Z-,]+(\s{0,1}[a-zA-Z-, ])*$/;
			var whitespace = /^\s+$/;
			var regpan = /^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/;
			var addressPattern = /^[a-zA-Z0-9-\/] ?([a-zA-Z0-9-\/]|[a-zA-Z0-9-\/] )*[a-zA-Z0-9-\/]$/;
			
			var firstName = $('#firstName').val();
			var lastName = $('#lastName').val();
			var emailAddress = $('#emailAddress').val();
			var address1 = $('#address').val();
			var city = $('#city').val();
			var state = $('#state').val();

			var pinCode = $('#pinCode').val();
			var password = $('#password').val();
			var panNo = $('#panNo').val();
			var mobileNo = $('#mobileNo').val();
			var agentAccountName = $('#agentAccountName').val();
			var agentBankName = $('#agentBankName').val();
			var agentAccountNumber = $('#agentAccountNumber').val();
			
			var agentbranchname = $('#agentBranch_Name').val();

			var agentBankIfscCode = $('#agentBankIfscCode').val();
 			
 		   		$("#firstName").bind("keypress", function (event) {
 		            if (event.charCode!=0) {
 		            	var regex = new RegExp("^[a-zA-Z \b-]+$");
 		                var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
 		                if (!regex.test(key)) {
 		                    event.preventDefault();
 		                    return false;
 		                }
 		            }
 		        });
 			 $("#lastName").bind("keypress", function (event) {
		            if (event.charCode!=0) {
		            	var regex = new RegExp("^[a-zA-Z \b-]+$");
		                var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
		                if (!regex.test(key)) {
		                    event.preventDefault();
		                    return false;
		                }
		            }
		        });
 			 
 			 $("#agencyName").bind("keypress", function (event) {
		            if (event.charCode!=0) {
		            	var regex = new RegExp("^[a-zA-Z \b-]+$");
		                var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
		                if (!regex.test(key)) {
		                    event.preventDefault();
		                    return false;
		                }
		            }
		        });
 			 
 			
 			 $("#firstName").on("blur", function(){
				var firstName = $('#firstName').val();
				if(firstName.length<=0){
					console.log("inside 0 length");
					$('#error_first_name').html("Please enter name");
				}
			}); 
			
			 $("#lastName").on("blur", function(){
				var lastName = $('#lastName').val();
				if(lastName.length<=0){
					$('#error_lastName').html("Please enter name");
				}
			});

			 $("#agencyName").on("blur", function(){
					var lastName = $('#lastName').val();
					if(lastName.length<=0){
						$('#error_agencyName').html("Please enter agency name");
					}
				});

		 	$("#emailAddress").on("blur", function(){
				var emailAddress = $('#emailAddress').val();
				if(emailAddress.length<=0){
					$('#error_emailAddress').html("Please enter email-Id");
				}
			   else if(!emailAddress.match(pattern)){
				   console.log("inside the pattern");
					$('#error_emailAddress').html("Please enter valid email-Id");
				}
				else if(!emailAddress.match(filter)){
					console.log("inside the email pattern filter");
					$('#error_emailAddress').html("Please enter valid email-Id")
				} 
			}); 
		
		     $("#address").on("blur", function(){
				var address = $('#address').val();
				if(address.length<=0){
					$('#error_address').html("Please enter address");
				}
				else if(!address.match(addressPattern)){
					console.log("In error_address");
					$('#error_address').html("Please enter valid address");
				}
				else{
					$('#error_address').html(" ");
				}
			});
			$("#agentBankName").on("blur", function(){
				var agentBankName = $('#agentBankName').val();
				if(agentBankName.length<=0){
					$('#error_agentBankName').html("Please enter bank name");
				}
				else if(!agentBankName.match(namePattern)){
					$('#error_agentBankName').html("Please enter valid bank name");
				}
				else{
					$('#error_agentBankName').html(" ");
				}
			});
			
			 $("#agentBranch_Name").bind("keypress", function (event) {
		            if (event.charCode!=0) {
		                var regex = new RegExp("^[a-zA-Z]+$");
		                var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
		                if (!regex.test(key)) {
		                    event.preventDefault();
		                    return false;
		                }
		            }
		        });
			$("#agentBranch_Name").on("blur", function(){
				var agentBranch_Name = $('#agentBranch_Name').val();
				if(agentBranch_Name.length<=0){
					$('#error_Branch_Name').html("Please enter branch name");
				}
				else if(!agentBranch_Name.match(namePattern)){
					$('#error_Branch_Name').html("Please enter valid branch name");
				}
				else{
					$('#error_Branch_Name').html(" ");
				}
			});
			
	         $("#panCardImg").on("blur",function(){
				var panCardImg = $("#panCardImg").val();
				if(panCardImg.length<=0){
					$("#error_panCardImg").html("Upload PanCard Image");
			 	 }
				else{
					$("#error_panCardImg").html("");
				 }
			}); 
			
			$("#agentBankIfscCode").on("blur", function(){
				var agentBankName = $("#agentBankName").val();
				console.log("agentBankName="+agentBankName);
				var ifscPattern = "^[A-Za-z]{4}[a-zA-Z0-9]{7}$";
				
				var agentBankIfscCode = $('#agentBankIfscCode').val();
				if(agentBankIfscCode.length<=0){
					$('#error_agentBankIfscCode').html("Please enter bank IFSC code");
				}
				else if(!agentBankIfscCode.match(ifscPattern)){
					$('#error_agentBankIfscCode').html("Please enter valid IFSC code");
				}
				else{
					$('#error_agentBankIfscCode').html(" ");
				}
			});
			
			 $("#agentAccountNumber").on("blur", function(){
				var agentAccountNumber = $('#agentAccountNumber').val();
				if(agentAccountNumber.length<=0){
					$("#error_agentAccountNumber").html("Please enter Account number");
				}
				else if(agentAccountNumber.length>16){
					$("#error_agentAccountNumber").html("Please enter valid Account number");
				}
				else{
					$('#error_agentAccountNumber').html(" ");
				}
			});
			 
			$('#agentAccountNumber').keyup(function () { 
			    this.value = this.value.replace(/[^0-9\.]/g,'');
			});
			
			$('#mobileNo').keyup(function () { 
			    this.value = this.value.replace(/[^0-9\.]/g,'');
			});
			
			$("#mobileNo").on("blur",function(){
				var mobile = $("#mobileNo").val();
				if(mobile.length<=0){
					$("#error_mobileNo").html("Please enter mobile number");
				}
				else if(mobile.length!=10){
					$("#error_mobileNo").html("Please enter valid mobile number");
				}
				else{
					$('#error_mobileNo').html(" ");
				}
			});
			var mobilePattern="^(?=(?:[7-9]){1})(?=[0-9]{10}).*";
			$('#verify_agent_otp_key').keyup(function () { 
				console.log("verify_agent_otp_key=");
			    this.value = this.value.replace(/[^0-9\.]/g,'');
			});
			$("#verify_agent_otp_key").on("blur",function(){
				var value = $("#verify_agent_otp_key").val();
				if(value.length!=6){
					$("#regMessage_success_agent").html("Please enter min and max");
				}
				if(!value.match(mobilePattern)){
					$("#regMessage_success_agent").html("Please enter valid key");
				}
			});
			
			$("#panNo").on("blur",function(){
				var panNo = $("#panNo").val();
				if(panNo.length<=0){
					$("#error_panno").html("Please enter pancard number");
				}
				else if(!panNo.match(regpan)){
					$("#error_panno").html("Please enter valid pancard number");
				}
				else{
					$('#error_panno').html(" ");
				}
			});
			
			$('#pinCode').keyup(function () { 
			    this.value = this.value.replace(/[^0-9\.]/g,'');
			});
			$("#pinCode").on("blur",function(){
				var valid=true;
				var pinCode = $("#pinCode").val();
				console.log("pinCode.length="+pinCode.length);
				if(pinCode.length<=0){
					valid=false;
					$("#error_pincode").html("Please enter pincode number");
					$("#city").val("");
					$("#state").val("");
				}else if(pinCode.length!=6){
					console.log("In pincode");
					valid=false;
					$("#error_pincode").html("Please enter valid pincode number");
					$("#city").val("");
					$("#state").val("");
				}
				else{
					$('#error_pincode').html(" ");
				}
				
				if (valid == true) {
					//alert("ss");
				//	$("#Agentregister").addClass("disabled");
					//$("#Agentregister").html(spinnerUrl);
					$.ajax({
						type : "POST",
						contentType : "application/json",
						url : "/Admin/GetCityAndState",
						dataType : 'json',
						data : JSON.stringify({
							"pinCode":" "+pinCode+""
						}),
						success : function(response) {
						if(response.code.includes("S00")){
							$("#city").val(response.details);		
							$("#state").val(response.response);
						}
						else{
							console.log()
							$("#error_pincode").html("Please enter valid pincode number");
						}
					}
			 	});
			 }
			});
			$("#password").on("blur",function(){
				var password = $("#password").val();
				if(password.length<=0){
					$("#error_password").html("Please enter password");
				}
				else if(password.length<6){
					$("#error_password").html("Please enter minimum 6 digit password");
				}
				else{
					$('#error_password').html(" ");
				}
			});
		}); 
	 
	  function imgchange(f) {
	     var panCardImg = $('#panCardImg').val();
	     if(panCardImg != null){
	     	$("#error_panCardImg").html("");
		     var FileUploadPath = panCardImg.value;
				var MAX_SIZE=500000;
				    var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();
				    if (Extension == "gif" || Extension == "png"
				                || Extension == "jpeg" || Extension == "jpg") {
				            if (panCardImg.files && panCardImg.files[0]) {
				                var size = panCardImg.files[0].size;
				                if(size > MAX_SIZE){
				                	$('#error_panCardImg').html("File size can not exceed 500kb");
				                	$('#panCardImg').val('');
				                    return false;
				                }
				            }
				    } 
				else {
						$('#error_panCardImg').html("Only GIF,JPG,JPEG,PNG file types are allowed");
						$('#panCardImg').val('');
				    }
	     }
	     var reader = new FileReader();
	     reader.onload = function (e) {
	         $('#imgs').attr('src',e.target.result);
	     };
	     reader.readAsDataURL(f.files[0]);
	  }
	  
		function saveAgentdetail(){
			$("#error_first_name").html("");
			$("#error_lastName").html("");
			$("#error_agencyName").html("");
			$("#error_emailAddress").html("");
			$("#error_address").html("");
			$("#error_city").html("");
			$("#error_state").html("");
			$("#error_pincode").html("");
			$("#error_password").html("");
			$("#error_panno").html("");
		
			$("#error_mobileNo").html("");
			$("#error_agentAccountName").html("");
			$("#error_Branch_Name").html("");
			$("#error_agentAccountNumber").html("");
			$("#error_agentBankIfscCode").html("");
			var valid = true;
			var pattern = "[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$"; //pattern for email
			var passwordPattern = "[a-zA-z0-9]"; //pattern for password

			var firstName = $('#firstName').val();
			var lastName = $('#lastName').val();
			var agencyName = $('#agencyName').val();
			var emailAddress = $('#emailAddress').val();
			var address1 = $('#address').val();
			var city = $('#city').val();
			var state = $('#state').val();

			var pinCode = $('#pinCode').val();
			var password = $('#password').val();
			var panNo = $('#panNo').val();
			var mobileNo = $('#mobileNo').val();
	//		var agentAccountName = $('#agentAccountName').val();
			var agentBankName = $('#agentBankName').val();
			var agentAccountNumber = $('#agentAccountNumber').val();
			var agentAccountName =  $( "#agentAccountName option:selected" ).text();
			var agentbranchname = $('#agentBranch_Name').val();
			var agentBankIfscCode = $('#agentBankIfscCode').val();

			if (firstName.length <= 0) {
				$("#error_first_name").html("enter name");
				valid = false;
			}

			if (lastName.length <= 0) {
				$("#error_lastName").html("enter last name");
				valid = false;
			}
			
			if (agencyName.length <= 0) {
				$("#error_agencyName").html("enter agency name");
				valid = false;
			}

			if (!emailAddress.match(pattern)) {
				$("#error_emailAddress").html("enter valid email");
				valid = false;
			}
			if (address1.length <= 0) {
				$("#error_address").html("enter adress");
				valid = false;
			}

			if (city.length <= 0) {
				$("#error_city").html("enter city");
				valid = false;
			}

			if (state.length <= 0) {
				$("#error_state").html("enter state");
				valid = false;
			}

			if (pinCode.length != 6) {
				$("#error_pincode").html("enter pincode");
				valid = false;
			}
			if (password.length <= 0) {
				$("#error_password").html("enter password");
				valid = false;
			}
			else if(password.length!=6){
				$("#error_password").html("Please enter minimum 6 digit password");
			}
			if (panNo.length <= 0) {
				$("#error_panno").html("enter pan number");
				valid = false;
			}
			if (mobileNo.length != 10) {
				$("#error_mobileNo").html("enter valid mobile number");
				valid = false;
			}
			
			if (agentAccountName.length<= 0) {
				$("#error_agentAccountName").html("enter agent Bank Type");
				valid = false;
			}
			if (agentBankName.length <= 0) {
				$("#error_Branch_Name").html("enter agent bank Name");
				valid = false;
			}
			if (agentAccountNumber.length <= 0) {
				$("#error_agentAccountNumber").html("enter agent bank ac. number");
				valid = false;
			}
			if (agentBankIfscCode.length<= 0) {
				$("#error_agentBankIfscCode").html("enter bank IFSC code");
				valid = false;
			}
			if (agentbranchname.length<= 0) {
				$("#error_Branch_Name").html("enter agent bank location");
				valid = false;
			}
			console.log("valid=="+valid);
			return valid;
			/* if (valid == true) {
				//alert("ss");
			//	$("#Agentregister").addClass("disabled");
				//$("#Agentregister").html(spinnerUrl);
				$.ajax({
					type : "POST",
					contentType : "application/json",
					url : "/Admin/SaveAgentDetail",
					dataType : 'json',
					data : JSON.stringify({

						"firstName" : "" + firstName + "",
						"lastName" : "" + lastName + "",
						"emailAddress" : "" + emailAddress + "",
						"address1" : "" + address1 + "",
						"city" : "" + city + "",
						"state" : "" + state + "",
						"country" :"India" ,
						"pinCode" : "" + pinCode + "",
						"password" : "" + password + "",
						"panNo" : "" + panNo + "",
						"mobileNo" : "" + mobileNo + "",
						"agentAccountName":""+agentAccountName+"",
						"agentBankName" : "" + agentBankName + "",
						"agentAccountNumber" : "" + agentAccountNumber + "",
					//	"agentBankLocation" : "" + agentBankLocation + "",
						"agentBankIfscCode":""+ agentBankIfscCode +"",
						"agentbranchname":""+agentbranchname+""
					}),
					success : function(response) {

						$("#Agentregister").removeClass("disabled");
						$("#Agentregister").html("Sign Up");
						if (response.code.includes("S00")) {
					
						$('#firstName').val("");
						$('#lastName').val("");
						$('#emailAddress').val("");
						$('#address').val("");
						$('#city').val("");
						$('#state').val("");

						$('#pinCode').val("");
						$('#password').val("");
						$('#panNo').val("");
						$('#mobileNo').val("");
					//	$('#agentBankName').val("");
						$('#agentAccountNumber').val("");
						$('#agentBranch_Name').val("");
						$('#agentBankIfscCode').val("");
						$('#error_password').html(" ");
						
						$("#verifiedMessageagent").modal('show');
						$("#success_verification_message_agent").html(response.details
											+" Please login to continue");
							var timeout = setTimeout(function(){
					            $("#verifiedMessageagent").modal('hide');
					          }, 3000);
						}
						else if (response.code.includes("F04")) {

							$("#errorMessage").modal('show');
							$("#error_message").html(response.message);
						}
						else
							{
							  console.log("In invalid session");
							  window.locaion ="Admin/Home";
						    }
					  }
				});
			} */

		}
		
		
		function verify_mobile_Agent()
		{
			$.ajax({
						type : "POST",
						contentType : "application/json",
						url : "/Admin/AgentMobileOTP",
						dataType : 'json',
						data : JSON.stringify({
							"mobileNumber" : ""
									+ $('#agent_otp_username').val() + "",
							"key" : "" + $('#verify_agent_otp_key').val() + ""
						}),
						success : function(response) {

							console.log("response.code"+response.code);
							console.log("response.details"+response.details);
							if (response.code.includes("S00")) {

								$("#AgnetregMessage").modal('hide');
								$("#verifiedMessageagent").modal('show');
								$("#success_verification_message_agent").html(
										response.details
												+ " Please login to continue");
								var timeout = setTimeout(function(){
						            $("#verifiedMessageagent").modal('hide');
						          }, 3000);
							} 
							else if(response.code.includes("F03")){
								console.log("In false F03");
								window.location ="/Admin/Home";
							}
							else {
								console.log("In false key");
								$("#regMessage_success_agent").html(response.details);
							}
						}
					});
			 }
		
		function verify_resend_otp() {
			var valid=true;
			console.log("In resend otp");
			if(valid==true){
			$.ajax({
				type : "POST",
				contentType : "application/json",
				url : "/Admin/AgentResendOTP",
				dataType : 'json',
				data : JSON.stringify({
					"mobileNumber" : "" + $('#agent_otp_username').val() + ""
				}),
				success : function(response) {
					$("#register_resend_otp").html("Resend OTP");
					$("#register_resend_otp").removeClass("disabled");
					if (response.code.includes("S00")) {
						$("#regMessage_success_agent").html(response.details);
					//	$("#fpOTP_message").html(response.details);
					}
					else if(response.code.includes("F03")){
						window.location ="/Admin/Home";
					}
					else {
						$("#regMessage_success").html(response.details);
					}
				}
			});	
			}
		}
		
		
	</script>
	<script>
	function validateForm(){
			$("#error_first_name").html("");
			$("#error_lastName").html("");
			$("#error_agencyName").html("");
			$("#error_emailAddress").html("");
			$("#error_address").html("");
			$("#error_city").html("");
			$("#error_state").html("");
			$("#error_pincode").html("");
			$("#error_password").html("");
			$("#error_panno").html("");
			$("#error_panCardImg").html();
			$("#error_mobileNo").html("");
			$("#error_agentAccountName").html("");
			$("#error_Branch_Name").html("");
			$("#error_agentAccountNumber").html("");
			$("#error_agentBankIfscCode").html("");
			
			var valid = true;
			var pattern = "[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$"; //pattern for email
			var passwordPattern = "[a-zA-z0-9]"; //pattern for password

			var firstName = $('#firstName').val();
			var lastName = $('#lastName').val();
			var agencyName = $('#agencyName').val();
			var emailAddress = $('#emailAddress').val();
			var address1 = $('#address').val();
			var city = $('#city').val();
			var state = $('#state').val();

			var pinCode = $('#pinCode').val();
			var password = $('#password').val();
			var panNo = $('#panNo').val();
			var panCardImg = $("#panCardImg").val();
			var mobileNo = $('#mobileNo').val();
	//		var agentAccountName = $('#agentAccountName').val();
			var agentBankName = $('#agentBankName').val();
			var agentAccountNumber = $('#agentAccountNumber').val();
			var agentAccountName =  $( "#agentAccountName option:selected" ).text();
			
			var agentbranchname = $('#agentBranch_Name').val();

			var agentBankIfscCode = $('#agentBankIfscCode').val();

			if (firstName.length <= 0) {
				$("#error_first_name").html("enter name");
				valid = false;
			}

			if (lastName.length <= 0) {
				$("#error_lastName").html("enter last name");
				valid = false;
			}
			
			if (agencyName.length <= 0) {
				$("#error_agencyName").html("enter last name");
				valid = false;
			}

			if (!emailAddress.match(pattern)) {
				$("#error_emailAddress").html("enter valid email");
				valid = false;
			}
			if (address1.length <= 0) {
				$("#error_address").html("enter adress");
				valid = false;
			}

			if (city.length <= 0) {
				$("#error_city").html("enter city");
				valid = false;
			}

			if (state.length <= 0) {
				$("#error_state").html("enter state");
				valid = false;
			}

			if (pinCode.length != 6) {
				$("#error_pincode").html("enter pincode");
				valid = false;
			}
			if (password.length <= 0) {
				$("#error_password").html("enter password");
				valid = false;
			}
			else if(password.length!=6){
				valid = false;
				$("#error_password").html("Please enter minimum 6 digit password");
			}
			if (panNo.length <= 0) {
				$("#error_panno").html("enter pan number");
				valid = false;
			}
			if(panCardImg.length<=0){
				valid = false;
				$("#error_panCardImg").html("Upload PanCard Image");
		 	 }
			if (mobileNo.length != 10) {
				$("#error_mobileNo").html("enter valid mobile number");
				valid = false;
			}
			
			if (agentAccountName.length<= 0) {
				$("#error_agentAccountName").html("enter agent Bank Type");
				valid = false;
			}
			if (agentBankName.length <= 0) {
				$("#error_Branch_Name").html("enter agent bank Name");
				valid = false;
			}
			if (agentAccountNumber.length <= 0) {
				$("#error_agentAccountNumber").html("enter agent bank ac. number");
				valid = false;
			}
			if (agentBankIfscCode.length<= 0) {
				$("#error_agentBankIfscCode").html("enter bank IFSC code");
				valid = false;
			}
			if (agentbranchname.length<= 0) {
				$("#error_Branch_Name").html("enter agent bank location");
				valid = false;
			}
			return valid;
	}
	</script>
</body>
	 <input type="hidden" name="agentRespCode" value="${agentRespCode}" id="agentRespCode"/>
     <input type="hidden" name="agentResp" value="${agentResp}" id="agentResp"/>
     
<!-----modal after Registration Successful -->
<div id="AgnetregMessage" class="modal fade" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content text-center">
			<button type="button" data-dismiss="modal" class="close">&times;</button>
			<div class="icon">
				<span class="fa fa-unlock-alt fa-2x" aria-hidden="true"></span>
			</div>
			<h4>OTP Verification</h4>
			<hr style="margin-top: 20px;">

			<div class="group_1">
				<input type="text" name="key" id="verify_agent_otp_key"
					required="required" maxlength="6"> <span class="highlight"></span> <label>OTP</label>
			</div>
			<div class="group_1">
				<input type="hidden" name="mobileNumber"
					class="form-control input-sm" id="agent_otp_username" value=""
					required="required" />
			</div>
			<div class="col-md-6"
				style="float: none; margin-left: auto; margin-right: auto;">
				<button class="btn btn-md btn-block btncu" type="button"
					style="margin-bottom: 5px" onclick="verify_mobile_Agent()">Verify
					Mobile</button>
			</div>
			<div class="group_1">
				<button class="btn btn-md btncu" id="register_resend_otp" onclick="verify_resend_otp()" style="margin-left:-20px; margin-bottom:5px;">
				 Resend Otp</button>
			</div>
			<br>
			<%-- 	<div class="group_1">
						<div style="z-index: 99; position: absolute; margin-left: 78%;">
							<a href="#" class="captcha_link"><span
								class="glyphicon glyphicon-refresh" aria-hidden="true"
								style="margin-left: 80%;"></span></a>
						</div>
						<br> <img src="<c:url value="/Captcha"/>"
							class="captcha_image" height="50" />
					</div>
					<div class="group_1">
						<input id="g-recaptcha-response-1" type="text" required
							style="margin-top: -25px;" />
						<p class="error" id="error_captcha1" class="error"></p>
						<!-- <label>Enter text shown in image</label> -->
					</div>
					<div class="group_1">
						<button class="btn btn-md btncu" id="register_resend_otp"
							style="margin-bottom: 5px; margin-left: -20px;">Resend
							OTP</button>
					</div> --%>

			<div class="modal-footer">
				<div class="alert alert-success" id="regMessage_success_agent"></div>
			</div>
		</div>
	</div>
</div>
<!-- Modal after successful verification -->
<div id="verifiedMessageagent" role="dialog" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5>Verification Successful</h5>
			</div>
			<div class="modal-body">
				<center id="success_verification_message_agent"
					class="alert alert-success"></center>
			</div>
		</div>
	</div>
</div>

<!-- Modal after error verification -->
<div id="errorMessage" role="dialog" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				<center id="error_message" class="alert alert-danger"></center>
			</div>
		</div>
	</div>
</div>
</html>
