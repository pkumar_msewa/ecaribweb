<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<title>eCarib | ServiceStatus</title>
<!-- Bootstrap core CSS -->
<link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'
	type="image/png" />

<link
	href="${pageContext.request.contextPath}/resources/admin/css/bootstrap.min.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/admin/fonts/css/font-awesome.min.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/admin/css/animate.min.css"
	rel="stylesheet">

<!-- Custom styling plus plugins -->
<link
	href="${pageContext.request.contextPath}/resources/admin/css/custom.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/admin/css/icheck/flat/green.css"
	rel="stylesheet">
<script
	src="${pageContext.request.contextPath}/resources/admin/js/jquery.min.js"></script>


<script>
	$(document).ready();
	function updateAccount() {

		$.ajax({
			url : "${pageContext.request.contextPath}/Admin/UpdateLimits",
			type : "POST",
			datatype : 'json',
			contentType : "application/json",
			data : {
				mobileNumber:mobile,
				amount:amount,
				message:message
			},
			success : function(response) {
				console.log(response);
				}
		});
	}
</script>
</head>
<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			 <jsp:include page="/WEB-INF/jsp/Admin/LeftMenu.jsp"/>
			<jsp:include page="/WEB-INF/jsp/Admin/TopNavigation.jsp"/>
			<!-- /top navigation -->

			<!-- page content -->
			<div class="right_col" role="main">

				<div class="">
					<div class="page-title">
						<div class="title_left">
							<h3>Service Status</h3>
						</div>

					</div>
					<div class="clearfix"></div>

					<div class="row">
						<div class="col-md-12">
							<div class="x_panel">
								<div class="x_title">
									<ul class="nav navbar-right panel_toolbox">
									</ul>
									<div class="clearfix"></div>
								</div>
								<div class="x_content">

									<!-- start project list -->
									 <table id="datatable-buttons"
                                       class="table table-striped table-bordered">
                                       <thead>
                                             <tr>
                                                <th>S.No</th>
												<th >Acccount Details</th>
												<th >Monthly Limit</th>
												<th >Daily Limit</th>
												<th >Balance Limit</th>
												<th >Transaction Limit</th>
												<th >Status</th>
											</tr>
										</thead>
										
										 <tbody id="userList">
                                               <c:forEach items="${list}" var="limitList"varStatus="loopCounter">
                                                    <tr>
													<td>${loopCounter.count}</td>
													<td><b>Name : </b>${limitList.name}<br/> 
												        <b>Description :</b>${limitList.description}<br/>
														<b>Code : </b>${limitList.code}</td>
													<td ><a>${limitList.monthlyLimit}</a></td>
													<td ><a>${limitList.dailyLimit} </a> </td>
													<td ><a>${limitList.balanceLimit} </a>  </td>
													<td ><a>${limitList.transactionLimit} </a> </td>
													<td><a href="${pageContext.request.contextPath}/Admin/EditLimit" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i>Update</a></td>
												</tr>
											 </c:forEach> 
										</tbody>
									</table>
									<!-- end project list -->

								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- footer content -->
				<footer>
					<div class="copyright-info"></div>
					<div class="clearfix"></div>
				</footer>
				<!-- /footer content -->

			</div>
		</div>
	</div>

	<!-- footer content -->
	<footer>
		<div class="copyright-info"></div>
		<div class="clearfix"></div>
	</footer>
	<!-- /footer content -->
	</div>
	<!-- /page content -->
	</div>
	</div>

	<div id="custom_notifications" class="custom-notifications dsp_none">
		<ul class="list-unstyled notifications clearfix"
			data-tabbed_notifications="notif-group">
		</ul>
		<div class="clearfix"></div>
		<div id="notif-group" class="tabbed_notifications"></div>
	</div>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/bootstrap.min.js"></script>

	<!-- bootstrap progress js -->
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/progressbar/bootstrap-progressbar.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/nicescroll/jquery.nicescroll.min.js"></script>
	<!-- icheck -->
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/icheck/icheck.min.js"></script>

	<script
		src="${pageContext.request.contextPath}/resources/admin/js/custom.js"></script>
	<!-- form wizard -->
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/resources/admin/js/wizard/jquery.smartWizard.js"></script>
	<!-- pace -->
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/pace/pace.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			// Smart Wizard
			$('#wizard').smartWizard();

			function onFinishCallback() {
				$('#wizard').smartWizard('showMessage', 'Finish Clicked');
				//alert('Finish Clicked');
			}
		});

		$(document).ready(function() {
			// Smart Wizard
			$('#wizard_verticle').smartWizard({
				transitionEffect : 'slide'
			});

		});
	</script>
</body>
</html>
