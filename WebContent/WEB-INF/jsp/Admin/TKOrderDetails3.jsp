<!DOCTYPE html>
<html lang="en">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<sec:csrfMetaTags/>
<title>eCarib | UPI Transaction</title>

<!-- Bootstrap core CSS -->
<link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'
	type="image/png" />

<link
	href="${pageContext.request.contextPath}/resources/admin/css/bootstrap.min.css"
	rel="stylesheet">

<link
	href="${pageContext.request.contextPath}/resources/admin/fonts/css/font-awesome.min.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/admin/css/animate.min.css"
	rel="stylesheet">

<!-- Custom styling plus plugins -->
<link
	href="${pageContext.request.contextPath}/resources/admin/css/custom.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/admin/css/icheck/flat/green.css"
	rel="stylesheet">

<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/fixedHeader.bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/responsive.bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<!-- <link -->
<%-- 	href="${pageContext.request.contextPath}/resources/admin/js/datatables/scroller.bootstrap.min.css" --%>
<!-- 	rel="stylesheet" type="text/css" /> -->

<script
	src="${pageContext.request.contextPath}/resources/admin/js/jquery.min.js"></script>
	<style>
		.no-js #loader {
			display: none;
		}

		.js #loader {
			display: block;
			position: absolute;
			left: 100px;
			top: 0;
		}

		.se-pre-con {
			position: fixed;
			left: 0px;
			top: 0px;
			width: 100%;
			height: 100%;
			z-index: 9999;
			background: url(/images/pq_large.gif) center no-repeat #fff;
		}
	</style>
	<script src="<c:url value='/resources/js/modernizr.js'/>"></script>

 <link rel="stylesheet" href="<c:url value="/resources/css/datepicker.css"/>">
	<script src="<c:url value="/resources/js/datepicker.js"/>"></script>
	<script>
	$(document).ready(function(){
		
	var today = new Date();
		
		$(function() {
			$( "#toDate" ).datepicker({
				format:"yyyy-mm-dd",
				endDate: today,
			}).on('change', function() {
				$('.datepicker').hide();
			});
			$( "#fromDate" ).datepicker({
				format:"yyyy-mm-dd",
				endDate: today,
			}).on('change', function() {
				$('.datepicker').hide();
			});  
		});
	});
	</script>

	<script type="text/javascript">
		$(window).load(function() {
 			$(".se-pre-con").fadeOut("slow");
 		});
	</script>
<script src="${pageContext.request.contextPath}/resources/js/Admin/jquery.twbsPagination.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/Admin/jquery.twbsPagination.min.js"></script>
<script type="text/javascript" >


function fetchMe(value){
	console.log("page:"+value);
	console.log("inside function");
	
$.ajax({
type:"POST",
url:"${pageContext.request.contextPath}/Admin/TKOrderDeatilsList",
data:{
	page:value,
	size:'10'
	},
dataType:"json",
success:function(data){
	console.log("data:"+data)
var trHTML='';
	if(trHTML==''){
	$(".testingg").empty();
	$(data.jsonArray).each(function(i,item){trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>' + data.jsonArray[i].userDetail.firstName + '</td><td>UserName:' + data.jsonArray[i].userDetail.username +'</td>'+'<td>'+data.jsonArray[i].transactionRefNo+'</td><td>'+data.jsonArray[i].txnDate+'</td><td>'+data.jsonArray[i].userOrderId+'</td><td>'+data.jsonArray[i].orderStatus+
		 '</td><td>'+data.jsonArray[i].txnStatus+'</td><td>'+data.jsonArray[i].totalCustomerPayable+'</td><td>'+data.jsonArray[i].commsissionAmount
		 +'</td><td><button type="button" class="btn btn-info btn-sm" style="background-color: #5cb85c; color: #fff;"onclick="'+viewTicketDetails+'('+ data.jsonArray[i].name,data.jsonArray[i].pnr,data.jsonArray[i].seat,data.jsonArray[i].station_code,data.jsonArray[i].totalCustomerPayable,data.jsonArray[i].train_number,data.jsonArray[i].creationTime,data.jsonArray[i].coach +
		 ')">view</button></td>'+'</tr>'});
	$('#editedtable3').append(trHTML);
	}
	else
	{
		$(data.jsonArray).each(function(i,item){trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>' + data.jsonArray[i].userDetail.firstName + '</td><td>UserName:' + data.jsonArray[i].userDetail.username +'</td>'+'<td>'+data.jsonArray[i].transactionRefNo+'</td><td>'+data.jsonArray[i].txnDate+'</td><td>'+data.jsonArray[i].userOrderId+'</td><td>'+data.jsonArray[i].orderStatus+
			 '</td><td>'+data.jsonArray[i].txnStatus+'</td><td>'+data.jsonArray[i].totalCustomerPayable+'</td><td>'+data.jsonArray[i].commsissionAmount
			 +'</td><td><button type="button" class="btn btn-info btn-sm" style="background-color: #5cb85c; color: #fff;"onclick="'+viewTicketDetails+'('+ data.jsonArray[i].name,data.jsonArray[i].pnr,data.jsonArray[i].seat,data.jsonArray[i].station_code,data.jsonArray[i].totalCustomerPayable,data.jsonArray[i].train_number,data.jsonArray[i].creationTime,data.jsonArray[i].coach +
			 ')">view</button></td>'+'</tr>'});
		$('#editedtable3').append(trHTML);
		
	}
}
});
		 }
$(document).ready(function() {
// 	 var paging='0';
// 	 var size='10';
	 console.log("under ready...");
	 $.ajax({
			type:"POST",
			url:"${pageContext.request.contextPath}/Admin/TKOrderDeatilsList",
			data:{
				page:'0',
				size:'10'
				},
		dataType:"json",
		success:function(data){
			console.log("totaldata:"+data+" total pages:"+data.totalPages);
			console.log("JSON ARRAY="+data.jsonArray);
			var trHTML='';
				if(trHTML==''){
				$(".testingg").empty();
				$(data.jsonArray).each(function(i,item){trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>' + data.jsonArray[i].userDetail.firstName + '</td><td>UserName:' + data.jsonArray[i].userDetail.username +'</td>'+'<td>'+data.jsonArray[i].transactionRefNo+'</td><td>'+data.jsonArray[i].txnDate+'</td><td>'+data.jsonArray[i].userOrderId+'</td><td>'+data.jsonArray[i].orderStatus+
					 '</td><td>'+data.jsonArray[i].txnStatus+'</td><td>'+data.jsonArray[i].totalCustomerPayable+'</td><td>'+data.jsonArray[i].commsissionAmount
					 +'</td><td><button type="button" class="btn btn-info btn-sm" style="background-color: #5cb85c; color: #fff;"onclick="'+viewTicketDetails+'('+ data.jsonArray[i].name,data.jsonArray[i].pnr,data.jsonArray[i].seat,data.jsonArray[i].station_code,data.jsonArray[i].totalCustomerPayable,data.jsonArray[i].train_number,data.jsonArray[i].creationTime,data.jsonArray[i].coach +
					 ')">view</button></td>'+'</tr>'});
				  $('#editedtable3').append(trHTML);
				}
				
				else
				{
					$(data.jsonArray).each(function(i,item){trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>' + data.jsonArray[i].userDetail.firstName + '</td><td>UserName:' + data.jsonArray[i].userDetail.username +'</td>'+'<td>'+data.jsonArray[i].transactionRefNo+'</td><td>'+data.jsonArray[i].txnDate+'</td><td>'+data.jsonArray[i].userOrderId+'</td><td>'+data.jsonArray[i].orderStatus+
						 '</td><td>'+data.jsonArray[i].txnStatus+'</td><td>'+data.jsonArray[i].totalCustomerPayable+'</td><td>'+data.jsonArray[i].commsissionAmount
						 +'</td><td><button type="button" class="btn btn-info btn-sm" style="background-color: #5cb85c; color: #fff;"onclick="'+viewTicketDetails+'('+ data.jsonArray[i].name,data.jsonArray[i].pnr,data.jsonArray[i].seat,data.jsonArray[i].station_code,data.jsonArray[i].totalCustomerPayable,data.jsonArray[i].train_number,data.jsonArray[i].creationTime,data.jsonArray[i].coach +
						 ')>view</button></td>'+'</tr>'});
					$('#editedtable3').append(trHTML);
				}
				$(function () {
					console.log("inside funt...");
				 $('#paginationn').twbsPagination({
					 totalPages: data.totalPages,
					 visiblePages: 10,
		         onPageClick: function (event, page) {
		        	 page = page - 1;
		        	 fetchMe(page);
		           }
				});
			});
		}
	 });
});

</script>
</head>
<body class="nav-md">
	<div class="se-pre-con"></div>
	<div class="container body">
		<div class="main_container">

				<jsp:include page="/WEB-INF/jsp/Admin/LeftMenu.jsp"/>
				<jsp:include page="/WEB-INF/jsp/Admin/TopNavigation.jsp"/>
			<!-- page content -->
			<div class="right_col" role="main">
				<div class="">
					<div class="page-title">
						<div class="title_left">
							<h3>VPayQwik TravelkhanaOrder Details</h3>
						</div>
					</div>
					<div class="clearfix"></div>

					<div class="row">

						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<ul class="nav navbar-right panel_toolbox">
										<li><a href="#"><i class="fa fa-chevron-up"></i></a></li>
										<li class="dropdown"><a href="#" class="dropdown-toggle"
											data-toggle="dropdown" role="button" aria-expanded="false"><i
												class="fa fa-wrench"></i></a> 
                      </li>
										<li><a href="#"><i class="fa fa-close"></i></a></li>
									</ul>
									<div class="clearfix"></div>
								</div>
								<div class="x_content">
								<form action="${pageContext.request.contextPath}/Admin/TKOrderDeatilsListByDate" method="post" class="form form-inline" >
									 From <input type="text" id="fromDate"  class="form-control" readonly/>
										To <input type="text" id="toDate"  class="form-control" readonly/>
										
										<input type="hidden" id="fromDate2" name="startDate"/>
									    <input type="hidden" id="toDate2" name="endDate"/>
									<sec:csrfInput/>
									<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-filter"></span></button>
								 <p id="error_msg" style="color:red; margin-left: 15%;"></p>
								
								</form>
									<p class="text-muted font-13 m-b-30">
										<!--  DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: <code>$().DataTable();</code> -->
									</p>
									<table id="editedtable3"
										class="table table-striped table-bordered">
										<thead>
											<tr>
												<th>S.No</th>
												<th>User Details</th>
												<th>User Contact Details</th>
												<th>Transaction ID</th>
												<th>Transaction Date</th>
												<th>UserOrderId</th>
												<th>Order Status</th>
												<th>Transaction Status</th>
												<th>Account Details</th>
												<th>Commission</th>
												<th>Order Details</th>
											</tr>
										</thead>

									</table>
								</div>
							</div>
						</div>
	<nav>
<ul class="pagination" id="paginationn">
  </ul>
  </nav>
					</div>
				</div>


               <jsp:include page="/WEB-INF/jsp/Admin/Footer.jsp"/>
			</div>
			<!-- /page content -->
		</div>

	</div>

  <script type="text/javascript">
	 function viewTicketDetails(name,pnr,seat,station_code,totalCustomerPayable,train_number,creationTime,coach){
    $("#o_name").html(name);
	$("#o_train_number").html(train_number);
	$("#o_pnr").html(pnr);
	$("#o_seat").html(seat);
	$("#o_station_code").html(station_code);
	$("#o_totalCustomerPayable").html(totalCustomerPayable);
	$("#o_coach").html(coach);
//	$("#o_Date").html(orderDate);
	$("#o_creation_time").html(creationTime);
    $("#myModal").modal('show');
 }
	</script>

  <!-- Modal for orderDetails -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Order Details</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <!-- <span style="font-weight: 700;">Order Date:</span>&nbsp;<span id="o_Date"></span><br> -->
              <span style="font-weight: 700;">Order Date and Time:</span>&nbsp;<span id="o_creation_time"></span>
            </div>
          </div>
          <hr>
          <div class="row">
            <div class="col-md-12">
              <table class="table table-condensed" id="editedtable">
                <thead>
                  <tr>
                    <th><center>Sl. No</center></th>
                    <td><center>Name</center></th>
                    <th><center>Train No</center></th>
                    <th><center>PNR No</center></th>
                    <th><center>Seat</center></th>
                    <th><center>coach</center></th>
                    <th><center>Station Code</center></th>
                    <th><center>Total Customer Payable</center></th>
                  </tr>
                </thead>
                <tbody>
                <tr id="editedtable"></tr>
                  <tr>
                    <td><center>1</center></td>
              		<td id="o_name"><center></center></td>
                	<td id="o_train_number"><center></center></td>
                    <td id="o_pnr"><center></center></td>
                    <td id="o_seat"><center></center></td>
                     <td id="o_coach"><center></center></td>
                    <td id="o_station_code"><center></center></td>
                    <td id="o_totalCustomerPayable"><center></center></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      
    </div>
  </div>


<div class="container">  
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
       
        <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" id="close3">&times;</button>
          <center>
        <div style="background: white; padding: 40px;">
          <h3 id="cnclMsgVal"><b><br><small></b></small></h3>
        </div></center>
        </div>
      </div>
    </div>
  </div>
</div>
	<div id="custom_notifications" class="custom-notifications dsp_none">
		<ul class="list-unstyled notifications clearfix"
			data-tabbed_notifications="notif-group">
		</ul>
		<div class="clearfix"></div>
		<div id="notif-group" class="tabbed_notifications"></div>
	</div>

	<script
		src="${pageContext.request.contextPath}/resources/admin/js/bootstrap.min.js"></script>

	<!-- bootstrap progress js -->
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/progressbar/bootstrap-progressbar.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/nicescroll/jquery.nicescroll.min.js"></script>
	<!-- icheck -->
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/icheck/icheck.min.js"></script>

	<script
		src="${pageContext.request.contextPath}/resources/admin/js/custom.js"></script>


	<!-- Datatables -->
	<script src="js/datatables/js/jquery.dataTables.js"></script>
  <script src="js/datatables/tools/js/dataTables.tableTools.js"></script>

	<!-- Datatables-->
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.bootstrap.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.buttons.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/jszip.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/pdfmake.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/vfs_fonts.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.html5.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.print.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.fixedHeader.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.keyTable.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.responsive.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/responsive.bootstrap.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.scroller.min.js"></script>
	<script
		src="//cdn.datatables.net/plug-ins/1.10.11/sorting/date-dd-MMM-yyyy.js"></script>

	<!-- pace -->
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/pace/pace.min.js"></script>
	<script type="text/javascript">
		var handleDataTableButtons = function() {
            console.log("inside datatable buttons");
			"use strict";
			0 !== $("#datatable-buttons").length
					&& $("#datatable-buttons").DataTable({
                columnDefs: [
                    {
                        type: 'date-dd-mmm-yyyy',
                        targets: 1
                    }
                ],
						dom : "Bfrtip",
						buttons : [ {
							extend : "copy",
							className : "btn-sm"
						}, {
							extend : "csv",
							className : "btn-sm"
						}, {
							extend : "excel",
							className : "btn-sm"
						}, {
							extend : "pdf",
							className : "btn-sm"
						}, {
							extend : "print",
							className : "btn-sm"
						} ],



						responsive : !0
					})
		}, TableManageButtons = function() {
			"use strict";
			return {
				init : function() {
					handleDataTableButtons()
				}
			}
		}();
	</script>
	<script type="text/javascript">
		$(document).ready(function() {

			$('#datatable-keytable').DataTable({
				keys : true
			});
			$('#datatable-responsive').DataTable();
			$('#datatable-scroller').DataTable({
				ajax : "js/datatables/json/scroller-demo.json",
				deferRender : true,
				scrollY : 380,
				scrollCollapse : true,
				scroller : true
			});
			var table = $('#datatable-fixed-header').DataTable({
				fixedHeader : true
			});
			
			$("#cnclTckt").click(function () {
			$("#cancel_confirm").modal('show');
			});
			
		});
		TableManageButtons.init();
	</script>
		
		
		<script>
		function validateDate(){
			var fromDate=$('#fromDate').val();
			var toDate=$('#toDate').val();
			if (fromDate.length <=0 || toDate.length <=0) {
				$('#error_msg').html("Please select start & end date");
				return false;
			}
			var date1=Date.parse(fromDate);
			var date2= Date.parse(toDate);
			var timeDiff=date2-date1;
			daysDiff = Math.floor(timeDiff / (1000 * 60 * 60 * 24));
			/* console.log("days diff"+daysDiff); */
			if (daysDiff <0  || daysDiff >30) {
			$('#error_msg').html("Date should be between 30 days")
			return false;
			}
			/* console.log(date1+":"+date2+" sub: "+(date2-date1));
			console.log(date1+":"+date2); */
			 var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9\+\/\=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/\r\n/g,"\n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}
			 $('#fromDate2').val(Base64.encode(fromDate));
			 $('#toDate2').val(Base64.encode(toDate));
			return true;
		}
	</script>
		
<div class="container" align="center">

</div>

</body>

</html>
