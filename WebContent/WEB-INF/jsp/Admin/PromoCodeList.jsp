<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<sec:csrfMetaTags/>
<title>eCarib | Promo Code List</title>

<!-- Bootstrap core CSS -->
<link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'
	type="image/png" />

<link
	href="${pageContext.request.contextPath}/resources/admin/css/bootstrap.min.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/admin/fonts/css/font-awesome.min.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/admin/css/animate.min.css"
	rel="stylesheet">

<!-- Custom styling plus plugins -->
<link
	href="${pageContext.request.contextPath}/resources/admin/css/custom.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/admin/css/icheck/flat/green.css"
	rel="stylesheet">
<script
	src="${pageContext.request.contextPath}/resources/admin/js/jquery.min.js"></script>
<link rel="stylesheet" href="<c:url value="/resources/css/datepicker.css"/>">
	<script src="<c:url value="/resources/js/datepicker.js"/>"></script>
<!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
	<style>
		.no-js #loader {
			display: none;
		}

		.js #loader {
			display: block;
			position: absolute;
			left: 100px;
			top: 0;
		}

		.se-pre-con {
			position: fixed;
			left: 0px;
			top: 0px;
			width: 100%;
			height: 100%;
			z-index: 9999;
			background: url(/images/pq_large.gif) center no-repeat #fff;
		}
	</style>
	<script src="<c:url value='/resources/js/modernizr.js'/>"></script>

	<script type="text/javascript">
		$(window).load(function() {
			$(".se-pre-con").fadeOut("slow");
		});
	</script>
	
	<script>
		$(function() {
			$( "#startDate" ).datepicker({
				format:"yyyy-mm-dd"
			});
			$( "#endDate" ).datepicker({
				format:"yyyy-mm-dd"
			});


		});
	</script>
	<script type="text/javascript">
	
	$(document).ready(function() {

	   var respcode = $("#respCode").val();
	   var errorMsg = $("#errorMsg").val();
	   console.log("In respCode"+respcode);
	   console.log("In errorMsg"+errorMsg);
	    if(respcode.includes("S00")){
	 	  $("#respMessage").modal("show");
	 	 /* document.getElementById("respCode").innerHTML="F00";
	 	  $("#respCode").html("F00"); */
	 	  $("#respCode").val("");	
	 	 console.log("In respCode2="+$("#respCode").val());
	    }
	    
	    if(errorMsg.length>0){
	    	$("#ErrModal").modal("show");
	    	document.getElementById("errorMsg").innerHTML="";
	    	$("#errorMsg").val("");
	    	$("#errorMsg").html("");
	    	console.log("In errorMsg2="+$("#errorMsg").val());
	    }
	    
	    $('#terms').keyup(function () { 
		    this.value = this.value.replace(/[^0-9\.]/g,'');
		});
	    
	    $('#value').keyup(function () { 
		    this.value = this.value.replace(/[^0-9\.]/g,'');
		});
	    
	    $('#cashBackValue').keyup(function () { 
		    this.value = this.value.replace(/[^0-9\.]/g,'');
		});
	    
	    var onlyNumber = "[0-9]+";
		$("#promoCode").on("blur",function(){
			var promoCode = $("#promoCode").val();
			if(promoCode.length<=0){
		    	$("#error_promoCode").html("Enter Promo Code");
		    }else if(promoCode.length!=6){
		    	$("#error_promoCode").html("Promo Code must be at least 6 characters long");
		    }
		    else{
		    	$("#error_promoCode").html(" ");
		    }
		});
		
		$("#terms").on("blur",function(){
			var terms = $("#terms").val();
			 if(terms.length<=0){
			    	valid = false;
			    	$("#error_terms").html("Enter min amount to redeem that coupon");
			    }else if(!terms.match(onlyNumber)){
			    	$("#error_terms").html("Amount must be in numeric form");
			 }
			 else{
			      $("#error_terms").html(" ");
			 }
		});
		
		
		$("#startDate").on("blur",function(){
			var startDate = $("#startDate").val();
			if(startDate.length<=0){
		    	valid = false;
		    	$("#error_startDate").html("Enter start date");
		    }
			 else{
			    	$("#error_startDate").html(" ");
			    }
		});
		
		$("#endDate").on("blur",function(){
			var endDate = $("#endDate").val();
			 if(endDate.length<=0){
			    	valid = false;
			    	$("#error_endDate").html("Enter end date");
			    }
			 else{
			    	$("#error_endDate").html(" ");
			    }
		});
		
		$("#desc").on("blur",function(){
			var desc = $("#desc").val();
			 if(desc.length<=0){
			    	valid = false;
		            $("#error_desc").html("Enter valid description");
			    }
			 else{
			    	$("#error_desc").html(" ");
			    }
		});
		
		$("#value").on("blur",function(){
			var value = $("#value").val();
		 if(!(value > 0)){
	            valid = false;
	            $("#error_value").html("Enter value of coupon");
	        }
		 else{
		    	$("#error_value").html(" ");
		    }
		});
	});
	
	</script>
</head>
<body class="nav-md">
	<div class="se-pre-con"></div>
	<div class="container body">
		<div class="main_container">

			<jsp:include page="/WEB-INF/jsp/Admin/LeftMenu.jsp"/>
			<jsp:include page="/WEB-INF/jsp/Admin/TopNavigation.jsp"/>
			<input type="hidden" value="${respCode}" name= "respCode" id="respCode"/>
			<input type="hidden" value="${errorMsg}" name= "errorMsg" id="errorMsg"/>
			<div class="right_col" role="main">

				<div class="">
					<div class="page-title">
						<div class="title_left">
							<h3>Promo Code List</h3>
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="row">
						<div class="col-md-12">
							<div class="x_panel">
								<div class="x_title">

									<div class="clearfix"></div>
								</div>
								<div class="x_content">

									<!-- start project list -->
									<table class="table table-striped projects">
										<thead>
											<tr>
												<th>S.N.</th>
												<th>Promo Code</th>
												<th>Terms& Conditions</th>
												<th>Start Date</th>
												<th>End Date</th>
												<th>Description</th>
												<th>Fixed</th>
												<th>Value</th>
												<th>Cash Back</th>
												<th>Edit</th>
											</tr>
										</thead>
										<tbody id=userList>
											<c:forEach items="${list}" var="list" varStatus = "loopCount">
												<tr>
													<!-- <td><i class="fa fa-chevron-circle-right" aria-hidden="true"></i></td> -->
													<td><c:out value="${loopCount.count}" escapeXml="true"/></td>
													<td><c:out value="${list.promoCode}" default="" escapeXml="true"/></td>
													<td><c:out value="${list.terms}" default="" escapeXml="true"/> <br /> </td>
													<td><c:out value="${list.startDate}" default="" escapeXml="true"/> <br /> </td>
													<td><c:out value="${list.endDate}" default="" escapeXml="true"/><br /> </td>
													<td><c:out value="${list.description}" default="" escapeXml="true"/><br /> </td>
													<td><c:out value="${list.fixed}" default="" escapeXml="true"/> <br /> </td>
													<td><c:out value="${list.value}" default="" escapeXml="true"/> <br/> </td>
													<td><c:out value="${list.cashBackValue}" default="" escapeXml="true"/> <br/></td>
													<td><a href="${pageContext.request.contextPath}/Admin/GeneratePromoCode?pId=<c:out value="${list.promoCodeId}"/>">
													<i class="fa fa-pencil-square" aria-hidden="true"></i></a></td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
									<!-- end project list -->

								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- footer content -->
<jsp:include page="/WEB-INF/jsp/Admin/Footer.jsp"/>
				<!-- /footer content -->

			</div>
		</div>
	</div>

	<!-- footer content -->
	<footer>
		<div class="copyright-info"></div>
		<div class="clearfix"></div>
	</footer>
	<!-- /footer content -->
	
	<!--Login modal for user (promoCode,terms,startDate,endDate,desc,fixed,value) -->
        <div class="modal fade" id="userModal" tabindex="-1" role="dialog" 
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content" style="width: 88%;">
                    <div class="modal-header">
                       <!--  <button type="button" class="close" onclick="bodyUnfreezeScroll('userModal')" aria-hidden="true">
                            &times;</button> -->
                        <h4 class="modal-title" id="myModalLabel" style="text-align:  center;">
                            Edit PromCode Details!</h4>
                    </div>
                    <div class="modal-body" style="width: 100%;">
                        <div class="row" style=" width: 97%;padding-right:  0%;">
                            <div class="col-md-12 extra-w3layouts" style=" padding-left:  0%; padding-right: 10%;">
                                <div class="tab-content" style="width: 113%;padding-left: 11%;">
                                  <div class="tab-pane active" id="addPromoCode">
                                      <form class="form-horizontal" action="/Admin/EditPromoCode" method="POST" onsubmit="return validate()" style="width: 120%; ">
                                          <div class="form-group">
                                              <label class="control-label col-md-3 col-sm-3 col-xs-12" style="margin-left:-40px;">
														Promo Code </label>
												 <div class="col-md-7 col-sm-7 col-xs-12" style=" width: 59%;">
                                                        <input type="text" class="form-control" placeholder="PromoCode" id="promoCode" name="promoCode">
							                            <p class="error"></p>
							                             <p class="error" id="error_promoCode" style="color: red;margin-right: 200px;"></p>
                                              	 </div>
                                          </div>
                                        
                                        <div class="form-group">
                                           <label class="control-label col-md-3 col-sm-3 col-xs-12" style="margin-left:-40px;margin-top: -11px;">
														Terms &amp; Conditions</label>
											 <div class="col-md-7 col-sm-7 col-xs-12">
                                                <input type="text" class="form-control numeric" id="terms" placeholder="Min Amount for Transaction" name="terms">
                                           		<p class="error"></p>
                                           		 <p class="error" id="error_terms" style="color: red;margin-right:90px;"></p>
                                            </div>
                                        </div>
                                     
                                        <div class="form-group">
                                        	<label class="control-label col-md-3 col-sm-3 col-xs-12" style="margin-left:-40px;">
														StartDate</label>
											 <div class="col-md-7 col-sm-7 col-xs-12">
                                        		<input type="text" class="form-control" id="startDate" name="startDate" placeholder="StartDate">
                                        		<p class="error"></p>
                                        		 <p class="error" id="error_startDate" style="color: red;margin-right:210px;"></p>
                                        	</div>
                                        </div>
                                        
                                         <div class="form-group">
                                        	<label class="control-label col-md-3 col-sm-3 col-xs-12" style="margin-left:-40px;">
														EndDate</label>
											 <div class="col-md-7 col-sm-7 col-xs-12">
                                        		<input type="text" class="form-control" id="endDate" name="endDate" placeholder="EndDate">
                                        		<p class="error"></p>
                                        		 <p class="error" id="error_endDate" style="color: red;margin-right:210px;"></p>
                                        	</div>
                                        </div>
                                         <div class="form-group">
                                        	<label class="control-label col-md-3 col-sm-3 col-xs-12" style="margin-left:-40px;">
														Description</label>
											 <div class="col-md-7 col-sm-7 col-xs-12">
                                        		<input type="text" class="form-control" id="desc" name="description" placeholder="Description">
                                        		<p class="error"></p>
                                        		 <p class="error" id="error_desc" style="color: red;margin-right:180px;"></p>
                                        	</div>
                                        </div>
                                         <div class="form-group">
                                        	<label class="control-label col-md-3 col-sm-3 col-xs-12" style="margin-left:-40px;">
														Fixed</label>
											 <div class="col-md-7 col-sm-7 col-xs-12">
											 <!-- spring:checkbox path="fixed" class="form-control form-ds1"/> -->
                                        		<input type="checkbox" class="form-control" id="fixed" name="fixed" placeholder="Fixed">
                                        	</div>
                                        	
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" style="margin-left:-40px;">
														Amount</label>
											 <div class="col-md-7 col-sm-7 col-xs-12">
                                                <input type="number" class="form-control" id="value" name="value" placeholder="Value">
                                             <p class="error"></p>
                                              <p class="error" id="error_value" style="color: red;margin-right:180px;"></p>
                                            </div>
                                        </div>
                                        
                                         <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" style="margin-left:-40px;">
														CashBackValue</label>
											 <div class="col-md-7 col-sm-7 col-xs-12">
                                                 <input type="text" class="form-control" id="cashBackValue" name="cashBackValue" placeholder="Value">
                                            </div>
                                        </div>
                                        
                                         <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <center><button type="submit" class="btn btn-primary btn-sm disabled" id="updatePromoCode">
                                                    Update</button></center>
                                            </div>
                                         </div>
                                      </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<div id="respMessage" role="dialog" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<!-- <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5></h5>
			</div> -->
			<div class="modal-body" style="margin-top:10px;">
				<center class="alert alert-success">${respMsg}</center>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="ErrModal" role="dialog">
     <div class="modal-dialog">
       <div class="modal-content" >
         <div class="modal-body" style="margin-top:10px;">
				<center class="alert alert-danger">${errorMsg}</center>
		 </div>
       </div>
     </div>
</div>
  
	<div id="custom_notifications" class="custom-notifications dsp_none">
		<ul class="list-unstyled notifications clearfix"
			data-tabbed_notifications="notif-group">
		</ul>
		<div class="clearfix"></div>
		<div id="notif-group" class="tabbed_notifications"></div>
	</div>
	
	<script>
	
		function validate(){
			
			var promoCode = $("#promoCode").val();
			var terms = $("#terms").val();
			var startDate = $("#startDate").val();
			var endDate = $("#endDate").val();
			var desc = $("#desc").val();
		//	$("#fixed").val(fixed);
			var value = $("#value").val();
			var onlyNumber = "[0-9]+";
			var valid = true;
			console.log("valid=="+valid);
		    if(promoCode.length<=0){
		    	valid = false;
		    	$("#error_promoCode").html("Enter Promo Code");
		    }else if(promoCode.length!=6){
		    	valid = false;
		    	$("#error_promoCode").html("Promo Code must be at least 6 characters long");
		    }
			
		    if(terms.length<=0){
		    	valid = false;
		    	$("#error_terms").html("Enter min amount to redeem that coupon");
		    }else if(!terms.match(onlyNumber)){
		    	$("#error_terms").html("Amount must be in numeric form");
		    }
		    
		    if(!(value > 0)){
	            valid = false;
	            $("#error_value").html("Enter value of coupon");
	        }
		    
		    if(desc.length<=0){
		    	valid = false;
	            $("#error_desc").html("Enter valid description");
		    }
		    
		    if(startDate.length<=0){
		    	valid = false;
		    	$("#error_startDate").html("Enter start date");
		    }
		    
		    if(endDate.length<=0){
		    	valid = false;
		    	$("#error_endDate").html("Enter end date");
		    } 
		    
	    	return valid;	 
	     }
	     
	     
		function editPromoCode(promoCode,terms,startDate,endDate,desc,fixed,value,cashBackValue){
			
			if(fixed.includes("true")){
				$("#fixed").prop( "checked", true );
			}
			$("#promoCode").val(promoCode);
			$("#terms").val(terms);
			$("#startDate").val(startDate);
			$("#endDate").val(endDate);
			$("#desc").val(desc);
			$("#endDate").val(endDate);
		//	$("#fixed").val(fixed);
			$("#value").val(value);
			$("#cashBackValue").val(cashBackValue);
			$("#userModal").modal("show");
	   }
	</script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/bootstrap.min.js"></script>

	<!-- bootstrap progress js -->
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/progressbar/bootstrap-progressbar.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/nicescroll/jquery.nicescroll.min.js"></script>
	<!-- icheck -->
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/icheck/icheck.min.js"></script>

	<script
		src="${pageContext.request.contextPath}/resources/admin/js/custom.js"></script>
	<!-- form wizard -->
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/resources/admin/js/wizard/jquery.smartWizard.js"></script>
	<!-- pace -->
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/pace/pace.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			// Smart Wizard
			$('#wizard').smartWizard();

			function onFinishCallback() {
				$('#wizard').smartWizard('showMessage', 'Finish Clicked');
				//alert('Finish Clicked');
			}
		});

		$(document).ready(function() {
			// Smart Wizard
			$('#wizard_verticle').smartWizard({
				transitionEffect : 'slide'
			});

		});
	</script>
</body>
</html>
