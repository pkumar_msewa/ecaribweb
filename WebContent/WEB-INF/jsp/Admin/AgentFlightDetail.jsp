<!DOCTYPE html>
<html lang="en">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<sec:csrfMetaTags/>
<title>eCarib | Flight Detail</title>

<!-- Bootstrap core CSS -->
<link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'
	type="image/png" />
<link
	href="${pageContext.request.contextPath}/resources/admin/css/bootstrap.min.css"
	rel="stylesheet">

<link
	href="${pageContext.request.contextPath}/resources/admin/fonts/css/font-awesome.min.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/admin/css/animate.min.css"
	rel="stylesheet">

<!-- Custom styling plus plugins -->
<link
	href="${pageContext.request.contextPath}/resources/admin/css/custom.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/admin/css/icheck/flat/green.css"
	rel="stylesheet">

<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/fixedHeader.bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/responsive.bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/scroller.bootstrap.min.css"
	rel="stylesheet" type="text/css" />

<script
	src="${pageContext.request.contextPath}/resources/admin/js/jquery.min.js"></script>
	<link rel="stylesheet" href="<c:url value="/resources/css/datepicker.css"/>">
	<script src="<c:url value="/resources/js/datepicker.js"/>"></script>
	<style>
		.no-js #loader {
			display: none;
		}

		.js #loader {
			display: block;
			position: absolute;
			left: 100px;
			top: 0;
		}

		.se-pre-con {
			position: fixed;
			left: 0px;
			top: 0px;
			width: 100%;
			height: 100%;
			z-index: 9999;
			background: url(/images/pq_large.gif) center no-repeat #fff;
		}
	</style>
	<script src="<c:url value='/resources/js/modernizr.js'/>"></script>

	<script type="text/javascript">
		$(window).load(function() {
			$(".se-pre-con").fadeOut("slow");
			var timeout = setTimeout(function(){
			    $("#error_msg").html(" ");
				
			}, 1000);
		});
	</script>
	
		<script>
		var today = new Date();
		
		$(function() {
			$( "#toDate" ).datepicker({
				format:"yyyy-mm-dd",
				endDate: today,
			}).on('change', function() {
				$('.datepicker').hide();
			});
			$( "#fromDate" ).datepicker({
				format:"yyyy-mm-dd"
			}).on('change', function() {
				$('.datepicker').hide();
			});  
		});
	</script>
	
	
	<script>
		$(document).ready(function(){
		  $("#flgt_dtls").on("hide.bs.collapse", function(){
// 		    $("#collaps").html('<span class="glyphicon glyphicon-collapse-down"></span> Open');
		    $("#collaps").attr('class', 'fa fa-plus-square-o');
		  });
		  $("#flgt_dtls").on("show.bs.collapse", function(){
// 		    $("#collaps").html('<span class="glyphicon glyphicon-collapse-up"></span> Close');
		    $("#collaps").attr('class', 'fa fa-minus-square-o');
		  });
		});
	</script>

</head>
<body class="nav-md">
	<div class="se-pre-con" id="axz"></div>
	<div class="container body">
		<div class="main_container">
				<jsp:include page="/WEB-INF/jsp/Admin/LeftMenu.jsp"/>
				<jsp:include page="/WEB-INF/jsp/Admin/TopNavigation.jsp"/>

			<!-- page content -->
			<div class="right_col" role="main">
				<div class="">
					<div class="page-title">
						<div class="title_left">
							<h3>VPayQwik Flight Details</h3>
						</div>

					</div>
					<div class="clearfix"></div>

					<div class="row">

						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<!--<h2>Default Example <small>Users</small></h2> -->
									<ul class="nav navbar-right panel_toolbox">
										
									</ul>
									<div class="clearfix"></div>
								</div>
								<div class="x_content">
									  <p class="text-muted font-13 m-b-30">
									  <form action="<c:url value="/Admin/Agent/FlightdeatilListByDate"/>" method="post" class="form form-inline" onsubmit="return validateDate();">
										From <input type="text" id="fromDate"  class="form-control" readonly/>
										To <input type="text" id="toDate"  class="form-control" readonly/>
										
										<input type="hidden" id="fromDate2" name="startDate"/>
									    <input type="hidden" id="toDate2" name="endDate"/>
										  <sec:csrfInput/>
										  <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-filter"></span></button>
									  <p id="error_msg" style="color:red; margin-left: 15%;"></p>
									  
									  </form>
                				    </p>
									<table id="datatable-buttons"
										class="table table-striped table-bordered date_sorted">
									 <thead>
                                             <tr>
                                               <th>S.No</th>
												<th>User Details</th>
												<th>User Contact Details</th>
												<th>Transaction ID</th>
												<th>Transaction Date</th>
												<th>Booking Ref No.</th>
												<th>Booking Status</th>
												<th>Transaction Status</th>
												<th>Amount</th>
												<th>Base Fare</th>
												<th>Agent Commission Amount</th>
												<th>Account No.</th>
												<th>Flight Onward</th>
												<th>Flight Return</th>
												<th>Trip Type</th>
												<th>Commission</th>
												<th>Ticket Details</th>
												<th>Ticket PDF</th>
											</tr>
											 	<tbody id=userList>
											<c:forEach items="${FlightDeatils}" var="slist" varStatus="loopCount">
											<tr>
											<td>${loopCount.count}</td>
											<td> ${slist.userDetails}</td>
											<td><a href="${pageContext.request.contextPath}/Admin/User/${slist.userName}"><c:out value="${slist.userName}"></c:out></a>
											</td>
											<td> <c:choose><c:when test="${slist.transactionRefNo != 'null' }">
											${slist.transactionRefNo}
											</c:when>
											<c:otherwise>
											 NA
										    </c:otherwise>
											</c:choose></td>
											<td>${slist.txnDate}</td>
											<td><c:choose><c:when test="${slist.bookingRefId != 'null' }">
											${slist.bookingRefId}
											</c:when>
											<c:otherwise>
											 NA
										    </c:otherwise>
											</c:choose></td>
											<td><c:choose><c:when test="${slist.bookingStatus != 'null' }">
											${slist.bookingStatus}
											</c:when>
											<c:otherwise>
											 NA
										    </c:otherwise>
											</c:choose></td>
											<td><c:choose><c:when test="${slist.transactionStatus != 'null' }">
											${slist.transactionStatus}
											</c:when>
											<c:otherwise>
											 NA
										    </c:otherwise>
											</c:choose></td>
											<td>${slist.paymentAmount}</td>
											<td>${slist.baseFare}</td>
											<td>${slist.agentAmount}</td>
											<td>
											${slist.accountNumber}
										   </td>
										   <td>${slist.flightNameOnward}</td>
										   <td>${slist.flightNameReturn}</td>
										   <td>${slist.tripType}</td>
											<td>
											<c:choose>
											<c:when test="${slist.commsissionAmount>0}">
												${slist.commsissionAmount}
											</c:when>
											<c:otherwise>
												0
											</c:otherwise>
											</c:choose>
											
											</td>
					 						<td>
											<button id="view_ticket" type="button" style="background-color: #5cb85c; color: #fff;"
											onclick="viewTicketDetails('${slist.flightTicketId}')">View</button>
											</td>
											<td>
											<c:choose>
											<c:when test="${slist.bookingStatus== 'Booked'}">
											<form action="${pageContext.request.contextPath}/Admin/VpayQwikAgentFlightTicket" target="new"  method="post">
											<input type="hidden" id="flightTicketId" name="flightTicketId" value="${slist.flightTicketId}"/>
											<button id="Pdf_ticket${loopCount.count}"  type="submit"  style="background-color: #5cb85c; color: #fff;">Download</button>
											</form>	
											</c:when>
											<c:otherwise>
												NA
											</c:otherwise>
											</c:choose>
											</td>
											
											</c:forEach>
										</tbody>
									</table>
								</div>
							</div>
						</div>

						</div>
				</div>


				<!-- footer content -->
					<jsp:include page="/WEB-INF/jsp/Admin/Footer.jsp"/>
			<!-- /footer content -->

			</div>
			<!-- /page content -->
		</div>

	</div>

		<div id="custom_notifications" class="custom-notifications dsp_none">
		<ul class="list-unstyled notifications clearfix"
			data-tabbed_notifications="notif-group">
		</ul>
		<div class="clearfix"></div>
		<div id="notif-group" class="tabbed_notifications"></div>
	</div>


  <!-- Modal -->
  <div class="modal fade" id="ErrmyModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content" style="margin-top: 40%;">
       
        <div class="modal-body">
          <center>
<%-- 		  <img src="${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/search.gif" style="width: 150px; margin-top: -91px;"> --%>
        <div style="background: white; margin-top: -20px; padding: 2.5% 2%; padding-top: 32px;">
          <h3 id="payErrMsgVal"><b><br><small></b></small></h3>
        </div></center>
        </div>
       
      </div>
      
    </div>
  </div>

<!-- Flight -->

	<!-- ========================= Modal For Traveler Details ==================================== -->
	<div id="travellerDetails" class="modal fade" role="dialog"
		hidden="hidden">
		<div class="modal-dialog modal-lg">
			<!-- Modal content-->
			<div class="modal-content">

				<div class="modal-body">
					<button type="button" class="close close2" data-dismiss="modal"
						id="clsesits">&times;</button>
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<img src="${pageContext.request.contextPath}/resources/images/User/travel/img/airline/plane.png" style="width: 35px;">
							<span class="modal-title bookinghead" id="m_flight" style="font-size: 18px;"><b>Flight Details</b>&nbsp;<i id="collaps" class="fa fa-plus-square-o" data-toggle="collapse" data-target="#flgt_dtls"></i></span>

						</div>
						
						<div id="flgt_dtls" class="collapse">
    						<div id="flDtls"></div>
  						</div>
						
					</div>
					
					
					<hr style="margin-top: 5px;">
                     <h4>Traveler Details</h4>
					<div class="row">
						<div class="row">
							<div class="col-md-12">
								<!-- TABLE HOVER -->
								<!-- page content -->
								<table id="editedtable" class="table table-striped ">
									<tr id="xyz">
										<th>SL.NO</th>
										<th>First Name</th>
										<th>Last Name</th>
										<th>Age</th>
										<th>Gender</th>
										<th>Seat No.</th>
										<th>Fare</th>
									</tr>
								</table>
								<nav>
									<ul class="pagination" id="paginationn">
									</ul>
								</nav>
								<!-- End Page Content -->
								<!-- END TABLE HOVER -->

								<!-- <span style="color: red; padding-left: 696px;">Refunded Amount: </span><span
										id="refAmt"></span><br>
										<span style="color: red; padding-left: 696px;">Cancellation  Charge: </span><span
										id="cnclAmt"></span> -->
										
								<%-- <center>
									<br><input type="hidden" id="bookingRefNo">
									<input type="hidden" id="email">
									<button type="button" id="flDetails"  style="background-color: #d43f3a; border-color: #d43f3a;color: #FFFFFF;">Get Flight Details</button>
								</center> --%>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>


	<script
		src="${pageContext.request.contextPath}/resources/admin/js/bootstrap.min.js"></script>

	<!-- bootstrap progress js -->
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/progressbar/bootstrap-progressbar.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/nicescroll/jquery.nicescroll.min.js"></script>
	<!-- icheck -->
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/icheck/icheck.min.js"></script>

	<script
		src="${pageContext.request.contextPath}/resources/admin/js/custom.js"></script>


	<!-- Datatables -->
	<!-- <script src="js/datatables/js/jquery.dataTables.js"></script>
  <script src="js/datatables/tools/js/dataTables.tableTools.js"></script> -->

	<!-- Datatables-->
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.bootstrap.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.buttons.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/jszip.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/pdfmake.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/vfs_fonts.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.html5.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.print.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.fixedHeader.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.keyTable.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.responsive.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/responsive.bootstrap.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.scroller.min.js"></script>
    <script
            src="//cdn.datatables.net/plug-ins/1.10.11/sorting/date-dd-MMM-yyyy.js"></script>

    <!-- pace -->
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/pace/pace.min.js"></script>
	<script type="text/javascript">
		var handleDataTableButtons = function() {
            console.log("inside datatable buttons");
			"use strict";
			0 !== $("#datatable-buttons").length
					&& $("#datatable-buttons").DataTable({
                columnDefs: [
                    {
                        type: 'date-dd-mmm-yyyy',
                        targets: 1
                    }
                ],
						dom : "Bfrtip",
						buttons : [ {
							extend : "copy",
							className : "btn-sm"
						}, {
							extend : "csv",
							className : "btn-sm"
						}, {
							extend : "excel",
							className : "btn-sm"
						}, {
							extend : "pdf",
							className : "btn-sm"
						}, {
							extend : "print",
							className : "btn-sm"
						} ],



						responsive : !0
					})
		}, TableManageButtons = function() {
			"use strict";
			return {
				init : function() {
					handleDataTableButtons()
				}
			}
		}();
	</script>
	<script type="text/javascript">
		$(document).ready(function() {

			$('#datatable-keytable').DataTable({
				keys : true
			});
			$('#datatable-responsive').DataTable();
			$('#datatable-scroller').DataTable({
				ajax : "js/datatables/json/scroller-demo.json",
				deferRender : true,
				scrollY : 380,
				scrollCollapse : true,
				scroller : true
			});
			var table = $('#datatable-fixed-header').DataTable({
				fixedHeader : true
			});
		});
		TableManageButtons.init();
	</script>
	
	
	<script type="text/javascript">
    function viewTicketDetails(flightTicketId) {
    	 $('#flDtls').html('');
    	$("#axz").show();
    	 $("#cncl_msg").hide();
    	 $("#cnclTckt").hide();
    	 $('#editedtable').html('<tr class="testingg"><th>SL.NO</th><th>First Name</th><th>Last Name</th><th>Gender</th><th>Ticket No.</th><th>Passenger Type</th></tr>');
    	var contextPath = "${pageContext.request.contextPath}";
    	var spinnerUrl = "Please wait <img src="+contextPath+"'/resources/images/spinner.gif' style='width:20px; hight:10px;'>"
    	$("#view_ticket").addClass("disabled");
    	$("#view_ticket").html(spinnerUrl);
     	 $.ajax({
   	       type : "POST",
   	        contentType : "application/json",
   	        url : "${pageContext.request.contextPath}/Admin/getSingleAgentFlightTicketDetails",
   			dataType : 'json',
   			data :JSON.stringify({
   				"flightTicketId" : flightTicketId,
   			}),
   	       success : function(response) {
   	    	$(".se-pre-con").fadeOut("slow");
   	       console.log("Response :: "+response.code);
   	       
   	       if (response.code.includes("S00")) {
		
   	  	   var trHTML='';
   	  	   
   	       for (var i = 0; i < response.details.travellerDetails.length; i++) {
   	    	   
   	    	console.log("First Name: "+response.details.travellerDetails[i].fName);
   	    	var trHTML='';
   	    	 trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>' + response.details.travellerDetails[i].fName + '</td><td>' + response.details.travellerDetails[i].lName + '</td><td>' + response.details.travellerDetails[i].gender + '</td><td>'+ response.details.travellerDetails[i].ticketNo +'</td><td>' + response.details.travellerDetails[i].type + '</td></tr>';
   	    	 
   	    	$('#bookingRefNo').val(response.details.bookingRefNo);
   	    	/* console.log("abc:: "+response.details.bookingRefNo); */
   	    	$('#email').val(response.details.email);
   	    	 
   	    	$('#editedtable').append(trHTML);
   	    	
		  }
   	      
   	   		 console.log("First Name: "+response.details.ticketsResp.Tickets.Oneway[0].origin); 
   	   	    console.log("First Name: "+response.details.ticketsResp.Tickets.Oneway[0].sourceName);
   	   	   for (var i = 0; i < response.details.ticketsResp.Tickets.Oneway.length; i++) {
	    	   
   	   		
		    $('#flDtls').append('<br><div class="row" style="margin-right: 0px; margin-left: 0px; background: #fff; margin-top: 10px; width: 100%;">'
		   	   		+'<div class="lftside" style="width: 45%; float: left; padding-left: 20px; text-align: left;">'
					+'<span style="font-size: 15px; font-weight: 600; color: #737373;">'+response.details.ticketsResp.Tickets.Oneway[i].origin+'('+response.details.ticketsResp.Tickets.Oneway[i].sourceName+')'+'</span><br>'
					+'<span style="font-size: 12px;">'+response.details.ticketsResp.Tickets.Oneway[i].departureDate+'</span><br>'
					+'<span style="font-size: 12px;">'+response.details.ticketsResp.Tickets.Oneway[i].cabin+'</span><br>'
				    +'<span style="font-size: 14px;">'+response.details.ticketsResp.Tickets.Oneway[i].airlineName+ '-'+response.details.ticketsResp.Tickets.Oneway[i].flightNumber+'</span><br>'
				    +'<span style="font-size: 12px;">'+ response.details.ticketsResp.Tickets.Oneway[i].departureTime+'</span><br>'
					+'</div>'
					+'<div class="cntr" style="width: 10%; float: left; padding-left: 20px;">'
					+'<span style="font-size: 13px;">'+response.details.ticketsResp.Tickets.Oneway[i].duration+'</span>'
					+'</div>'
					+'<div class="r8side" style="width: 45%; float: left; text-align: right; padding-right: 20px; padding-bottom: 15px;">'
					+'<span style="font-size: 15px; font-weight: 600; color: #737373;">'+response.details.ticketsResp.Tickets.Oneway[i].destination+'('+response.details.ticketsResp.Tickets.Oneway[i].destinationName+')'+'</span><br>'
					+'<span style="font-size: 12px;">'+response.details.ticketsResp.Tickets.Oneway[i].departureDate+'</span><br>'
					+'<span style="font-size: 12px;">'+ response.details.ticketsResp.Tickets.Oneway[i].cabin+'</span><br>'
				    +'<span style="font-size: 14px;">'+response.details.ticketsResp.Tickets.Oneway[i].airlineName+ '-' +response.details.ticketsResp.Tickets.Oneway[i].flightNumber+'</span><br>'
				    +'<span style="font-size: 12px;">'+response.details.ticketsResp.Tickets.Oneway[i].arrivalTime+'</span><br>'
				    +'</div><br><br> </div>'); 
		   } 
   	   	   
   	   	   if (response.details.ticketsResp.Tickets.Roundway.length>0) {
   	   		$('#flDtls').append('<center><h4><b>Return</b></h4></center>');
			}
   	   	   
   	   	   
			for (var i = 0; i < response.details.ticketsResp.Tickets.Roundway.length; i++) {
   	   		
		    $('#flDtls').append('<br><div class="row" style="margin-right: 0px; margin-left: 0px; background: #fff; margin-top: 10px; width: 100%;">'
			   		+'<div class="lftside" style="width: 45%; float: left; padding-left: 20px; text-align: left;">'
					+'<span style="font-size: 15px; font-weight: 600; color: #737373;">'+response.details.ticketsResp.Tickets.Roundway[i].origin+'('+response.details.ticketsResp.Tickets.Oneway[i].sourceName+')'+'</span><br>'
					+'<span style="font-size: 12px;">'+response.details.ticketsResp.Tickets.Roundway[i].departureDate+'</span><br>'
					+'<span style="font-size: 12px;">'+response.details.ticketsResp.Tickets.Roundway[i].cabin+'</span><br>'
				    +'<span style="font-size: 14px;">'+response.details.ticketsResp.Tickets.Roundway[i].airlineName+ '-'+response.details.ticketsResp.Tickets.Roundway[i].flightNumber+'</span><br>'
				    +'<span style="font-size: 12px;">'+ response.details.ticketsResp.Tickets.Roundway[i].departureTime+'</span><br>'
					+'</div>'
					+'<div class="cntr" style="width: 10%; float: left; padding-left: 20px;">'
					+'<span style="font-size: 13px;">'+response.details.ticketsResp.Tickets.Roundway[i].duration+'</span>'
					+'</div>'
					+'<div class="r8side" style="width: 45%; float: left; text-align: right; padding-right: 20px; padding-bottom: 15px;">'
					+'<span style="font-size: 15px; font-weight: 600; color: #737373;">'+response.details.ticketsResp.Tickets.Roundway[i].destination+'('+response.details.ticketsResp.Tickets.Oneway[i].destinationName+')'+'</span><br>'
					+'<span style="font-size: 12px;">'+response.details.ticketsResp.Tickets.Roundway[i].departureDate+'</span><br>'
					+'<span style="font-size: 12px;">'+ response.details.ticketsResp.Tickets.Roundway[i].cabin+'</span><br>'
				    +'<span style="font-size: 14px;">'+response.details.ticketsResp.Tickets.Roundway[i].airlineName+ '-' +response.details.ticketsResp.Tickets.Roundway[i].flightNumber+'</span><br>'
				    +'<span style="font-size: 12px;">'+response.details.ticketsResp.Tickets.Roundway[i].arrivalTime+'</span><br>'
				    +'</div><br><br> </div>'); 
		   }
   	   	   
   	 		$("#view_ticket").addClass("removeClass");
    		$("#view_ticket").html("View");
   	 	    $("#travellerDetails").modal('show'); 
   	       }
   	       else{
   	    	   
   	        $('#payErrMsgVal').html("Ticket details not available");	
   	    	$("#ErrmyModal").modal('show'); 
   	    	location.reload();	 
   	       }
   	 	   
   	       } 
   	   }); 
      }
    
    
    
    $("#flDetails").click(function () {
   	 $.ajax({
 	       type : "POST",
 	        contentType : "application/json",
 	        url : "${pageContext.request.contextPath}/Admin/getAgentFlightDetails",
 			dataType : 'json',
 			data :JSON.stringify({
 				"bookingRefId" : flightTicketId,
 				"email" : flightTicketId,
 			}),
 	       success : function(response) {
 	    	
 	       console.log("Response :: "+response.code);
 	       
 	       if (response.code.includes("S00")) {
 	  	   
 	       }
 	       } 
 	   }); 
		
	});  
    
	</script>
	
	<script>
		function validateDate(){
			var fromDate=$('#fromDate').val();
			var toDate=$('#toDate').val();
			if (fromDate.length <=0 || toDate.length <=0) {
				$('#error_msg').html("Please select start & end date");
				return false;
			}
			var date1=Date.parse(fromDate);
			var date2= Date.parse(toDate);
			var timeDiff=date2-date1;
			daysDiff = Math.floor(timeDiff / (1000 * 60 * 60 * 24));
		
			if (daysDiff <0  || daysDiff >30) {
			$('#error_msg').html("Date should be between 30 days")
			return false;
			}
			
			 var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9\+\/\=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/\r\n/g,"\n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}
			 $('#fromDate2').val(Base64.encode(fromDate));
			 $('#toDate2').val(Base64.encode(toDate));
			return true;
		}
	</script>
	
</body>
</html>