<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
 <%@ taglib prefix="taq" uri="http://www.springframework.org/tags"%> 
 <%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form"%>


<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<sec:csrfMetaTags />
<title>eCarib | Generate Promo Code</title>

<!-- Bootstrap core CSS -->
<link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'
	type="image/png" />
<link
	href="${pageContext.request.contextPath}/resources/admin/css/bootstrap.min.css"
	rel="stylesheet">

<link
	href="${pageContext.request.contextPath}/resources/admin/fonts/css/font-awesome.min.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/admin/css/animate.min.css"
	rel="stylesheet">

<link
	href="${pageContext.request.contextPath}/resources/admin/css/custom.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/admin/css/icheck/flat/green.css"
	rel="stylesheet">

<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/fixedHeader.bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/responsive.bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/scroller.bootstrap.min.css"
	rel="stylesheet" type="text/css" />

<script
	src="${pageContext.request.contextPath}/resources/admin/js/jquery.min.js"></script>

<!-- <link
	href="http://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css"
	rel="stylesheet">
<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script> -->
	<link rel="stylesheet" href="<c:url value="/resources/css/datepicker.css"/>">
	<script src="<c:url value="/resources/js/datepicker.js"/>"></script>
	
	<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
	<script>
		$(function() {
			var date = new Date();
			date.setDate(date.getDate());
			$( "#startDate" ).datepicker({
				format:"yyyy-mm-dd",
				startDate: date
			});
			$( "#endDate" ).datepicker({
				format:"yyyy-mm-dd",
				startDate: date
			});


		});
	</script>


	<style>
		.no-js #loader {
			display: none;
		}

		.js #loader {
			display: block;
			position: absolute;
			left: 100px;
			top: 0;
		}

		.se-pre-con {
			position: fixed;
			left: 0px;
			top: 0px;
			width: 100%;
			height: 100%;
			z-index: 9999;
			background: url(/images/pq_large.gif) center no-repeat #fff;
		}
	</style>
	<script src="<c:url value='/resources/js/modernizr.js'/>"></script>

	<script type="text/javascript">
		$(window).load(function() {
			$(".se-pre-con").fadeOut("slow");
		});
		
	</script>
	<script type="text/javascript">
	function selectServiceOptions(jsonObj){
// 		var servicesArr='${selectedService}';
// 		 var jsonObj=JSON.parse(servicesArr);
// 		 console.log(jsonObj);
// 		for(var i=0; i<jsonObj.length; i++){
// 			var code=jsonObj[i].code;
// 			console.log(code);
// 			$('#id1').html("<option value=\"abc\" selected>ABC</option>")
// 		}
	}
	function getServiceData(){
		var valid = true;
		 var serviceTypeId = $("#serviceType").val();
			console.log("serviceType=="+serviceTypeId);
		 if(serviceTypeId.includes("#")){
			 $("#serviceType_error").html("Select serviceType");
			 valid=false;
		 }
		 else{
			 $("#serviceType_error").html(" ");
			 $('#services').children('option').remove();
			 $("#services").empty();
			 $("#services").val(null).trigger("change");
			 $.ajax({
				type:"post",
				contentType:"application/json",
				url:"/Admin/GetServices",
				dataType:'json',
				data:JSON.stringify({
					"serviceId":serviceTypeId
				}),
				success:function(response){
					 $("#loadingMesg").html("");
					console.log("code="+response.code);
					if(response.code.includes("S00")){
						var servicesArr='${selectedService}';
						var jsonArray=null;
						if(servicesArr !=null && servicesArr!=''){
 						var jsonArray=JSON.parse(servicesArr);
						}
						 console.log("json array : "+jsonArray);
						 var code=''; var codeSelect=false; var selected='';
						 if(response.serviceList!=null){
					 for(var i=0;i<response.serviceList.length;i++){
						 if(jsonArray!=null && jsonArray!=''){
						 for(var j=0; j<jsonArray.length; j++){
							 if(response.serviceList[i].code == jsonArray[j].code){
							 $("#services").append('<option  value="'+response.serviceList[i].code+'" selected>'
									 +response.serviceList[i].description+'</option>');
							codeSelect=true;
							 break;
							 }
						}
					}
						 if(!codeSelect){
							 $("#services").append('<option value="'+response.serviceList[i].code+'">'+response.serviceList[i].description+'</option>')
						 }
						 codeSelect=false;
					 }
						 }
					 console.log(jsonArray);
					 console.log(response.serviceList)
					 $("#services").select2();
					}
				}
				 
			  }); 

		 }
		 		 selectServiceOptions();
	}
	
	$(document).ready(function() {
	    $("#services").select2();
	   var respcode = $("#respCode").val();
	   console.log("In respCode"+respcode);
	    if(respcode.includes("S00")||respcode.includes("F00")){
	 	  $("#respMessage").modal("show");
	 	  $("#respCode").val("");
	    }
	    $('#services').children('option').remove();
	    $("#serviceType").append('<option value="#">Select serviceType</option>');
	    $.ajax({
			type:"post",
			contentType:"application/json",
			url:"/Admin/GetServiceType",
			dataType:'json',
			data:JSON.stringify({
			}),
			success:function(response){
				console.log("GetServiceType code="+response.code);
				if(response.code.includes("S00")){
					 var serviceType='${promoCodeRequest.serviceTypeId}';
					 var seleted=''; var temp=false;
				 for(var i=0;i<response.serviceDTOs.length;i++){
					 if(serviceType!=null && serviceType!='' && serviceType == response.serviceDTOs[i].id ){
            			 seleted="selected";
            			 temp=true;
            		 }
                     $("#serviceType").append('<option value=" '+ response.serviceDTOs[i].id+ '" '+seleted+'>'+response.serviceDTOs[i].description+'</option>')
                     seleted=''; 
				 }
				 if(temp){
                	 getServiceData();
                 }
				  }
			    }
			});
	    
	    checkSelectedFields();
	});
	
	function checkSelectedFields(){
		 var serviceType='${promoCodeRequest.serviceTypeId}';
		    console.log(serviceType);
		    if(serviceType!=null && serviceType!=''){
		    	document.getElementById("serviceType").value = serviceType;
//	 	    $('#serviceType').val(value);
		    }
		    var check='${promoCodeRequest.fixed}';
		    console.log("check:"+check);
		    if(check ==="true"){
		    	  $("#checkId").prop("checked", true);
		    }else{
		    	 $("#checkId").prop("checked", false);
		    }
	}
	</script>

</head>
<body class="nav-md">
<div class="se-pre-con"></div>
	<div class="container body">
		<div class="main_container">


			<jsp:include page="/WEB-INF/jsp/Admin/LeftMenu.jsp" />
			<jsp:include page="/WEB-INF/jsp/Admin/TopNavigation.jsp" />
			<input type="hidden" value="${respCode}" id="respCode"/>
			<!-- page content -->
			<div class="right_col" role="main">
				<div class="col-md-6 col-xs-12">
					<div class="x_panel">
						<div class="x_title">
							<c:choose>
								<c:when test="${promoCodeId !=null && promoCodeId !='' }">
									<h2>Edit Promo Code</h2>
								</c:when>
								<c:otherwise>
									<h2>Add Promo Code</h2>
								</c:otherwise>
							</c:choose>
							<div class="clearfix"></div>
						</div>
						<div class="x_content">
							<br />
							<spring:form method="post" action="/Admin/GeneratePromoCode"
								modelAttribute="promoCodeRequest" onsubmit="return validatePromo();"
								class="form-horizontal form-label-left">
								<div class="form-group">
								<input type="hidden" name="promoCodeId" value="${promoCodeId}"></input>
									<label class="control-label col-md-3 col-sm-3 col-xs-12">
										Promo Code </label>
									<div class="col-md-9 col-sm-9 col-xs-12">
										<spring:input path="promoCode" class="form-control form-ds1" name="number" value="${promoCodeRequest.promoCode}" id="number"
										placeholder="Promo Code" maxlength="6" />
<%-- 										<p class="error" id="error_promoCode2">${error.promoCode}</p> --%>
										<p class="error" id="error_promoCode" style="color: red;"></p>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12">Terms
										& Conditions <span class="required">*</span>
									</label>
									<div class="col-md-9 col-sm-9 col-xs-12">
										<spring:input path="terms" class="form-control form-ds1"
											id="condition" value="${promoCodeRequest.terms}"
											maxlength="6" placeholder='Min Amount for Transaction' onkeypress="return isNumberKey(event)" />
										<%-- 										<p class="error" id= "error_terms2">${error.terms}</p> --%>
										<p class="error" id="error_terms"
											style="color: red;"></p>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12">Start
										Date <span class="required">*</span>
									</label>
									<div class="col-md-9 col-sm-9 col-xs-12">
										<input type="text" class="form-control" id="startDate" value="${promoCodeRequest.startDate}" name="startDate" placeholder="StartDate" readonly>
<%-- 									<p class="error" id="error_startDate2">${error.startDate}</p> --%>
									<p class="error" id="error_startDate" style="color: red;"></p>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12">End
										Date <span class="required">*</span>
									</label>
									<div class="col-md-9 col-sm-9 col-xs-12">
										<input type="text" class="form-control" id="endDate" value="${promoCodeRequest.endDate}" name="endDate" placeholder="EndDate" readonly>
<%-- 										<p class="error" id="error_endDate2">${error.endDate}</p> --%>
										 <p class="error" id="error_endDate" style="color: red;"></p>
									</div>
									<sec:csrfInput/>
								</div>

								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12">Description <span class="required">*</span>
									</label>
									<div class="col-md-9 col-sm-9 col-xs-12">
										<spring:input path="description" id="desc" value="${promoCodeRequest.description}" maxlength="100" class="form-control form-ds1"/>
<%-- 										<p class="error" id="error_desc2">${error.description}</p> --%>
										<p class="error" id="error_desc" style="color: red;"></p>
									</div>
									<sec:csrfInput/>
								</div>


								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12">Fixed
										<span class="required">*</span>
									</label>
									<div class="col-md-9 col-sm-9 col-xs-12">
										<spring:checkbox path="fixed" value="${promoCodeRequest.fixed}" id="checkId" class="form-control form-ds1"
													  />
										<%-- <p class="error">${error.value}</p> --%>
									</div>
								</div>


								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12">Amount
										 <span class="required">*</span>
									</label>
									<div class="col-md-9 col-sm-9 col-xs-12">
										<spring:input path="value" id="amount" value="${promoCodeRequest.value}" class="form-control form-ds1" rows="3"
											 placeholder='Amount' maxlength="6" onkeypress="return isNumberKey(event)"/>
<%-- 										<p class="error" id="error_value2">${error.value}</p> --%>
										 <p class="error" id="error_value" style="color: red;"></p>
									</div>
								</div>
								
									<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12">Upto CashBack
										 <span class="required">*</span>
									</label>
									<div class="col-md-9 col-sm-9 col-xs-12">
										<spring:input path="cashBackValue" id="cashBackValue" value="${promoCodeRequest.cashBackValue}" class="form-control form-ds1" rows="3"
											 placeholder='UptoCashBack' maxlength="6" onkeypress="return isNumberKey(event)"/>
<%-- 										<p class="error">${error.cashBackValue}</p> --%>
										<p class="error" id="error_cashBackValue" style="color: red;"></p>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12">
										Service Type<span class="required">*</span>
									</label>
									<div class="col-md-9 col-sm-9 col-xs-12">
									<select id="serviceType" name="serviceType"  class="form-control" style="width:333px; height:40px;">
									</select>
									<p class="error" id="serviceType_error">${error.serviceType}</p><br>
									
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12">
										Services<span class="required">*</span>
									</label>
									<div class="col-md-9 col-sm-9 col-xs-12">
										<select class="js-example-basic-multiple" multiple="multiple"
											id="services" name="services" class="form-control"
											style="width: 333px; height: 60px;">
										</select>
										<p id="loadingMesg"></p>
										<br>
										<p class="error" id="service_error"></p>
										<br>
									</div>
								</div>
								<div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
									<button type="submit" class="btn btn-success btn-ds1" id="btnId">Submit</button>
								</div>
							</spring:form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- footer content -->
	<jsp:include page="/WEB-INF/jsp/Admin/Footer.jsp" />
	<!-- /footer content -->

<div id="respMessage" role="dialog" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<!-- <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5></h5>
			</div> -->
			<div class="modal-body" style="margin-top:10px;">
				<center class="alert alert-success">${resp.message}</center>
			</div>
		</div>
	</div>
</div>

	<div id="custom_notifications" class="custom-notifications dsp_none">
		<ul class="list-unstyled notifications clearfix"
			data-tabbed_notifications="notif-group">
		</ul>
		<div class="clearfix"></div>
		<div id="notif-group" class="tabbed_notifications"></div>
	</div>
	<script>
	var spinnerUrl = "Loading please wait...<img src='/resources/images/spinner.gif' height='20' width='20'>"
		$("#serviceType").change(function(){
			$("#loadingMesg").html(spinnerUrl);
			getServiceData();
		});
		
		
		$("#services").on("blur",function(){
			var serviceTypeId = $("#serviceType").val();
			var service = $("#services option:selected").val();
			
//			console.log($("#services").text());
			console.log("SERVICE="+service);
			console.log("serviceType=="+serviceTypeId);
			
			/* var hexvalues = [];
			var labelvalues = [];

			$('#services :selected').each(function(i, selectedElement) {
			 hexvalues[i] = $(selectedElement).val();
			 labelvalues[i] = $(selectedElement).text();
			});
			console.log("hexvalues="+hexvalues); */
			
		});
		
	</script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/bootstrap.min.js"></script>

	<!-- bootstrap progress js -->
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/progressbar/bootstrap-progressbar.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/nicescroll/jquery.nicescroll.min.js"></script>
	<!-- icheck -->
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/icheck/icheck.min.js"></script>

	<script
		src="${pageContext.request.contextPath}/resources/admin/js/custom.js"></script>


	<!-- Datatables -->
	<!-- <script src="js/datatables/js/jquery.dataTables.js"></script>
  <script src="js/datatables/tools/js/dataTables.tableTools.js"></script> -->

	<!-- Datatables-->
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.bootstrap.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.buttons.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/jszip.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/pdfmake.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/vfs_fonts.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.html5.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.print.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.fixedHeader.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.keyTable.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.responsive.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/responsive.bootstrap.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.scroller.min.js"></script>


	<!-- pace -->
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/pace/pace.min.js"></script>
	<script>
		var handleDataTableButtons = function() {
			"use strict";
			0 !== $("#datatable-buttons").length
					&& $("#datatable-buttons").DataTable({
						dom : "Bfrtip",
						buttons : [ {
							extend : "copy",
							className : "btn-sm"
						}, {
							extend : "csv",
							className : "btn-sm"
						}, {
							extend : "excel",
							className : "btn-sm"
						}, {
							extend : "pdf",
							className : "btn-sm"
						}, {
							extend : "print",
							className : "btn-sm"
						} ],
						responsive : !0
					})
		}, TableManageButtons = function() {
			"use strict";
			return {
				init : function() {
					handleDataTableButtons()
				}
			}
		}();
	</script>
	<script type="text/javascript">
		$(document).ready(function() {
			var promoId='${promoCodeId}';
			console.log("promocodeId"+promoId);
			if(promoId !=null && promoId!=''){
				document.getElementById("number").readOnly  = true;
			}			
			$('#datatable').dataTable();
			$('#datatable-keytable').DataTable({
				keys : true
			});
			$('#datatable-responsive').DataTable();
			$('#datatable-scroller').DataTable({
				ajax : "js/datatables/json/scroller-demo.json",
				deferRender : true,
				scrollY : 380,
				scrollCollapse : true,
				scroller : true
			});
			var table = $('#datatable-fixed-header').DataTable({
				fixedHeader : true
			});
		});
		TableManageButtons.init();
		
		function validatePromo(){
			var valid=true;
			var number=$('#number').val();
			var condition=$('#condition').val();
			var startDate=$('#startDate').val();
			var endDate=$('#endDate').val();
			var desc=$('#desc').val();
			var cashBackValue=$('#cashBackValue').val();
			var amount=$('#amount').val();
			var services=$('#services').val();
			
			removeErrorMesg();
			
			console.log("ser:"+services);
			if(number.length != 6){
				valid=false;
				$('#error_promoCode').html("Enter 6 character promo code");
			}if(!condition.length>0){
				valid=false;
				$('#error_terms').html("Enter Terms&Condition value");
			}if(!startDate.length>0){
				valid=false;
				$('#error_startDate').html("Select start date");
			}if(!endDate.length>0){
				valid=false;
				$('#error_endDate').html("Select end date");
			}
			if(endDate.length > 0 && startDate.length > 0){
				if(!validateDate(startDate,endDate)){
					valid=false;
					$('#error_endDate').html("End date should be greater or equal to start Date");
				}
			}
			if(!desc.length>0){
				valid=false;
				$('#error_desc').html("Enter description");
			}
			if(!amount.length>0 || amount <=0 ){
				valid=false;
				$('#error_value').html("Enter amount");
			}if(!cashBackValue.length>0){
				valid=false;
				$('#error_cashBackValue').html("Enter cashback amount");
			}if(services == null || services=='' ){
				valid=false;
				$('#service_error').html("Select atleast one service");
			}
			if(valid){
				$('#btnId').addClass('disabled');
			}
			return valid ;
		}
		
		function isNumberKey(evt)
	     {
	        var charCode = (evt.which) ? evt.which : evt.keyCode;
	        if (charCode != 46 && charCode > 31  && (charCode < 48 || charCode > 57))
	           return false;
	        return true;
	     }

		function validateDate(startDate,endDate){
			console.log("start"+startDate+": end"+endDate);
			var start= new Date(startDate);
			var end = new Date(endDate);
			console.log("startDate"+start+": endDate"+end);
			var startTim= start.getTime();
			var endTim= end.getTime();
			console.log("startime"+startTim+": endtime"+endTim);
			
			if( startTim > endTim ){
			return false	
			}
			return true;
		}
		
		function removeErrorMesg(){
			$('#error_promoCode').html("");
			$('#error_terms').html("");
			$('#error_startDate').html("");
			$('#error_endDate').html("");
			$('#error_desc').html("");
			$('#error_value').html("");
			$('#error_cashBackValue').html("");
			$('#service_error').html("");
		}
	</script>
</body>

</html>
