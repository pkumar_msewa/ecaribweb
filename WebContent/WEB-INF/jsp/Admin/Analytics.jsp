
<!DOCTYPE html>
<html lang="en">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<sec:csrfMetaTags />
<title>eCarib | Analytics</title>

<!-- Bootstrap core CSS -->
<link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'
	type="image/png" />

<link
	href="${pageContext.request.contextPath}/resources/admin/css/bootstrap.min.css"
	rel="stylesheet">

<link
	href="${pageContext.request.contextPath}/resources/admin/fonts/css/font-awesome.min.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/admin/css/animate.min.css"
	rel="stylesheet">

<!-- Custom styling plus plugins -->
<link
	href="${pageContext.request.contextPath}/resources/admin/css/custom.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/admin/css/icheck/flat/green.css"
	rel="stylesheet">

<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/fixedHeader.bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/responsive.bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/scroller.bootstrap.min.css"
	rel="stylesheet" type="text/css" />

<script
	src="${pageContext.request.contextPath}/resources/admin/js/jquery.min.js"></script>

	<style>
		.no-js #loader {
			display: none;
		}

		.js #loader {
			display: block;
			position: absolute;
			left: 100px;
			top: 0;
		}

		.se-pre-con {
			position: fixed;
			left: 0px;
			top: 0px;
			width: 100%;
			height: 100%;
			z-index: 9999;
			background: url(/images/pq_large.gif) center no-repeat #fff;
		}
	</style>
	<script src="<c:url value='/resources/js/modernizr.js'/>"></script>

	<link rel="stylesheet" href="<c:url value="/resources/css/datepicker.css"/>">
	<script src="<c:url value="/resources/js/datepicker.js"/>"></script>
	<script>
		$(function() {
			$( "#toDate" ).datepicker({
				format:"yyyy-mm-dd"
			});
			$( "#fromDate" ).datepicker({
				format:"yyyy-mm-dd"
			});
		});
	</script>

	<script type="text/javascript">
		$(window).load(function() {
			$(".se-pre-con").fadeOut("slow");
		});
	</script>
    

	<script src="/resources/js/canvasjs.min.js"></script>
	<script>
		
	</script>
</head>
<body class="nav-md">
	<div class="se-pre-con"></div>
	<div class="container body">
		<div class="main_container">
			<jsp:include page="/WEB-INF/jsp/Admin/LeftMenu.jsp" />
			<jsp:include page="/WEB-INF/jsp/Admin/TopNavigation.jsp" />
		<jsp:include page="/WEB-INF/jsp/Admin/filter.jsp" />
			<!-- page content -->
			
			<div class="right_col" role="main" >
			
				<div class="">
					<div class="page-title">
						<div class="title_left">
							<h3>eCarib Analytics</h3>
						</div>

					</div>
					<div class="clearfix"></div>

					<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									   <!-- <h4>Demographics</h4> -->
								
								<!-- search bar -->
						<div class="x_content">
                    		 
									<div class="clearfix"></div>
								</div>
								

						<div class="col-md-5 col-sm-12 col-xs-12">
                       
							<div class="x_panel" style="	background: white;">
								<div class="x_title">
									
									<h4><b>Demographics</b></h4>
									</ul>
									<div class="clearfix"></div>
								</div><br>
							 <div id="chartContainer1" style="    margin-bottom: 12%;
    margin-top: 11%; height: 300px; width: 100%;"></div>									
								</div>
								<script>
								window.onload = function () {
									var chart = new CanvasJS.Chart("chartContainer1", {				
										animationEnabled: true,
										theme: "theme2",
										data: [
										{
											type: "doughnut",
											indexLabelFontFamily: "Helvetica",
											indexLabelFontSize: 15,
											startAngle: 0,
											indexLabelFontColor: "black",
											indexLabelLineColor: "darkgrey",
											toolTipContent: "{y} %",

											dataPoints: [
											{ y: 50.04, indexLabel: "karnataka" },
											{ y: 30.83, indexLabel: "Andhra Pradesh" },
											{ y: 3.20, indexLabel: "Kerala " },
											{ y: 20.11, indexLabel: "Tamil Nadu" },
											{ y: 2.29, indexLabel: "Haryana" },
											{ y: 15.53, indexLabel: "Maharashtra" }

											]
										}
										]
									});

									chart.render();
			
			var chart = new CanvasJS.Chart("chartContainer", {
				data: [{
					type: "column",
					click:onClick,
					dataPoints: [
             <c:forEach items="${LoadMoney}" var="loadmoney" >
						{ y: ${loadmoney.amount}, label: "${loadmoney.serviceName}" },
						 </c:forEach>
					]
				}]
			});
			chart.render();
			
			var chart = new CanvasJS.Chart("chartContainer2", {
				data: [{
					type: "column",
					click:onClick,
					dataPoints: [
             <c:forEach items="${Analytics}" var="analytics" >
						{ y: ${analytics.amount}, label: "${analytics.serviceName}" },
						 </c:forEach>
					]
				}]
			});
			chart.render();
			
			function onClick(f) {
				var d=f.dataPoint.label;
				 $.ajax({
						type:"POST",
						url:"${pageContext.request.contextPath}/Admin/ShowUserListAnalytics",
						data:{serviceName:d},
					dataType:"json",
					success:function(data){
						 var username;
						var created;
						var transactionRefNo;
						var amount;
						var status;
						var description;
						
						

						/* for (i in data.info) {
						    var username = data.info[i].username;
						    var created=data.info[i].created;
						    var transactionRefNo=data.info[i].transactionRefNo;
						    var amount=data.info[i].amount;
						    var status=data.info[i].status;
						    var description=data.info[i].description;
						    $('#editedtable').DataTable( {
						        "columns": [
						            { "data": "username" },
						            { "data": "created" },
						            { "data": "transactionRefNo" },
						            { "data": "amount" },
						            { "data": "status" },
						            { "data": "description" }
						        ]
						    } );
						   
						} */ 
						
						
						
						
						$.each(JSON.parse(data), function(idx, obj){
							
						 alert('the datas are:'+username+created+transactionRefNo+amount+status+description);
							var rowNode= [ item.username, item.created, item.transactionRefNo,amaount,status,description]
					        dataTable.row.add(rowNode).draw();
						 
						})
							}
				 });
				
			}
		}
			
			
	</script>
							</div>
							
						<div class="col-md-7 col-sm-12 col-xs-12">                       
							<div class="x_panel" style="	background: white;">
								<div class="x_title">
								<h4><b>Load Money &amp; Promo Transactions</b></h4>
								
									</ul>
									<div class="clearfix"></div>
								</div><br>
						 
								<div class="col-md-12 col-sm-12 col-xs-12">
                       
							
						<div id="chartContainer" style="height: 400px; width: 100%;">
	</div>

						 
 
								</div>
							
							</div>
						</div>
						<div class="col-md-12 col-sm-12 col-xs-12">                       
							<div class="x_panel" style="	background: white;">
								<div class="x_title">
								<h4><b>Payments</b></h4>
									</ul>
									<div class="clearfix"></div>
								</div><br>
						 
								<div class="col-md-12 col-sm-12 col-xs-12">
                       
							
						<div id="chartContainer2" style="height: 400px; width: 100%;">
	</div>

						 
 
								</div>
							
							</div>
						</div>
					</div>
					</div>
			<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
								
								<div class="x_content">
									<p class="text-muted font-13 m-b-30">
										<!--  DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: <code>$().DataTable();</code> -->
									</p>
									<table id="editedtable"
										class="table table-striped table-bordered">
											<tr id="xyz" style="background: #0182c4;color: white;">
											<th>username</th>
												<th>created</th>
												<th>transactionRefNo</th>
												<th>amount</th>
												<th>status</th>
												<th>description</th>
											</tr>
									</table>
								</div>
							</div>
						</div>
						<nav>
							<ul class="pagination" id="paginationn">
							  </ul>
							  </nav>
					</div>
				</div>
					
				</div>

</div>
				<jsp:include page="/WEB-INF/jsp/Admin/Footer.jsp" />
			</div>
			<!-- /page content -->
		</div>

	</div>

	<div id="custom_notifications" class="custom-notifications dsp_none">
		<ul class="list-unstyled notifications clearfix"
			data-tabbed_notifications="notif-group">
		</ul>
		<div class="clearfix"></div>
		<div id="notif-group" class="tabbed_notifications"></div>
	</div>

	<script
		src="${pageContext.request.contextPath}/resources/admin/js/bootstrap.min.js"></script>

	<!-- bootstrap progress js -->
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/progressbar/bootstrap-progressbar.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/nicescroll/jquery.nicescroll.min.js"></script>
	<!-- icheck -->
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/icheck/icheck.min.js"></script>

	<script
		src="${pageContext.request.contextPath}/resources/admin/js/custom.js"></script>


	<!-- Datatables -->
	<!-- <script src="js/datatables/js/jquery.dataTables.js"></script>
  <script src="js/datatables/tools/js/dataTables.tableTools.js"></script> -->

	<!-- Datatables-->
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.bootstrap.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.buttons.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/jszip.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/pdfmake.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/vfs_fonts.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.html5.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.print.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.fixedHeader.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.keyTable.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.responsive.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/responsive.bootstrap.min.js"></script>
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.scroller.min.js"></script>
    <script
            src="//cdn.datatables.net/plug-ins/1.10.11/sorting/date-dd-MMM-yyyy.js"></script>

    <!-- pace -->
    <script
            src="${pageContext.request.contextPath}/resources/admin/js/pace/pace.min.js"></script>
	<script type="text/javascript">
		var handleDataTableButtons = function() {
            console.log("inside datatable buttons");
			"use strict";
			0 !== $("#datatable-buttons").length
					&& $("#datatable-buttons").DataTable({
                columnDefs: [
                    {
                        type: 'date-dd-mmm-yyyy',
                        targets: 1
                    }
                ],
						dom : "Bfrtip",
						buttons : [ {
							extend : "copy",
							className : "btn-sm"
						}, {
							extend : "csv",
							className : "btn-sm"
						}, {
							extend : "excel",
							className : "btn-sm"
						}, {
							extend : "pdf",
							className : "btn-sm"
						}, {
							extend : "print",
							className : "btn-sm"
						} ],



						responsive : !0
					})
		}, TableManageButtons = function() {
			"use strict";
			return {
				init : function() {
					handleDataTableButtons()
				}
			}
		}();
	</script>
	<script type="text/javascript">
		$(document).ready(function() {

			$('#datatable-keytable').DataTable({
				keys : true
			});
			$('#datatable-responsive').DataTable();
			$('#datatable-scroller').DataTable({
				ajax : "js/datatables/json/scroller-demo.json",
				deferRender : true,
				scrollY : 380,
				scrollCollapse : true,
				scroller : true
			});
			var table = $('#datatable-fixed-header').DataTable({
				fixedHeader : true
			});
		});
		TableManageButtons.init();
	</script>
</body>
</html>
