<!DOCTYPE html>
<html>
<head>
<title>VPayQwik Blog</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="${pageContext.request.contextPath}/resources/css/blogcss/bootstrap.css" rel='stylesheet' type='text/css' />
<link href="${pageContext.request.contextPath}/resources/css/blogcss/style.css" rel='stylesheet' type='text/css' />
<script src="${pageContext.request.contextPath}/resources/js/Blogs/jquery.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
</head>
<body>

<jsp:include page="/WEB-INF/jsp/Blogs/BlogHeader.jsp"/>
   
	<div class="about">
		<div class="container">
			<div class="about-main">
				<div class="col-md-8 about-left">
				
		         <div class="about-two">
                     <a href="/Blogs/5-Ways-to-Celebrate-this-Women's-Day">
                        <h3><b>5 Ways to Celebrate this Women's Day </b></h3><img src="/resources/images/blogs/single-18.jpg" alt="" class="img-responsive" /></a>
                       <p>We know fully well that 24 hours doesn't do justice to celebrate the true spirit of womanhood, but to make the most out of this special day, we've compiled a list of five amusing and productive things you can do to make this day even more special - after all, it's your day!</p>
                       <label class="pull-right" style="color: #989898;">Mar 8, 2018</label>
                        <div class="about-btn">
                           <a href="/Blogs/5-Ways-to-Celebrate-this-Women's-Day">Read More</a>
                     </div>
                    <ul>
                         <li><p>Share : </p></li>
                         <li><a href="https://www.facebook.com/vpayqwik/"><span class="fb"> </span></a></li>
                         <li><a href="https://twitter.com/V_PayQwik"><span class="twit"> </span></a></li>
                   </ul>
               </div>
               
				<div class="about-two">
						<a href="${pageContext.request.contextPath}/Blogs/top-5-resturants-in-bengaluru">
                        <h3><b>Top 5 Restaurants in Bengaluru to Dine In this Valentine's Day</b></h3><img src="/resources/images/blogs/single-17.jpg" alt="" class="img-responsive" /></a>
						<p>We've all heard the saying, "food is the way to someone's heart", (or maybe stomach in this case). With Valentine's Day advancing rapidly upon us, VpayQwik shows you how to earn brownie points with your better half. Here are the top 5 Bengaluru restaurant recommendations to woo your Valentine.</p>
						<label class="pull-right" style="color: #989898;">Feb 14, 2018</label>
        				<div class="about-btn">
							<a href="${pageContext.request.contextPath}/Blogs/top-5-resturants-in-bengaluru">Read More</a>
						</div>
						<ul>
							<li><p>Share : </p></li>
							<li><a href="https://www.facebook.com/vpayqwik/"><span class="fb"> </span></a></li>
							<li><a href="https://twitter.com/V_PayQwik"><span class="twit"> </span></a></li>
						</ul>
					</div>
				
					<div class="about-two">
						<h3><a href="${pageContext.request.contextPath}/Blogs/money-tips-for-millennial-college-graduate">
                        <b>Money Tips for the Millennial College Graduate</b>
                        <img src="${pageContext.request.contextPath}/resources/images/blogs/single-16.jpg" alt="money-tips-for-millennial-college-graduate" class="img-responsive" />
                        </a></h3>
						<p>We are the Millennials, the so-called Gen-Y. The generation of people that are narcissistic and lazy but at the same time positive as well as idea-driven. As we enter our 20's and 30's, we practically dominate the workforce. We are the current movers and shakers of the 21st century.</p>
						<label class="pull-right" style="color: #989898;">Dec 21, 2017</label>
        				<div class="about-btn">
							<a href="${pageContext.request.contextPath}/Blogs/money-tips-for-millennial-college-graduate">Read More</a>
						</div>
						<ul>
							<li><p>Share : </p></li>
							<li><a href="https://www.facebook.com/vpayqwik/"><span class="fb"> </span></a></li>
							<li><a href="https://twitter.com/V_PayQwik"><span class="twit"> </span></a></li>
						</ul>
					</div>
				
					<div class="about-two">
						 <h3><a href="/Blogs/secure-digital-wallet-on-your-smartphone">
                       <b>How secure is a Digital Wallet on your smartphone?</b><img src="/resources/images/blogs/single-15.jpg" alt="" class="img-responsive" /></a></h3>
						<p>The year is 2017 and our smartphones have replaced maps, health trackers, cameras, and yoga instructors. Well, the list actually goes on. It's safe to say that the Earth is round ( there's actually an App that shows a live video feed of the Earth from the International Space Station ) and that we are living in a digital age.</p>
						<label class="pull-right" style="color: #989898;">Dec 19, 2017</label>
        				<div class="about-btn">
							<a href="${pageContext.request.contextPath}/Blogs/secure-digital-wallet-on-your-smartphone">Read More</a>
						</div>
						<ul>
							<li><p>Share : </p></li>
							<li><a href="https://www.facebook.com/vpayqwik/"><span class="fb"> </span></a></li>
							<li><a href="https://twitter.com/V_PayQwik"><span class="twit"> </span></a></li>
						</ul>
					</div>
				
					<div class="about-two">
						<h3><a href="/Blogs/budgeting-101-for-the-millennials">
                        <b>Budgeting 101 for the Millennials</b><img src="/resources/images/blogs/single-14.jpg" alt="" class="img-responsive" /></a></h3>
						<p>In the simplest terms, a budget is an estimation of revenue and expenses over a certain period of time. But in a millennial's books, a budget is a friend who turns enemy number one as the calendar counts down to the end of the month. Admit it, we've all have had that "Damn, I wish college taught me how to budget rather than teaching me a concept that has no relevance in the 21st century" moment.</p>
						<label class="pull-right" style="color: #989898;">Dec 12, 2017</label>
        				<div class="about-btn">
							<a href="${pageContext.request.contextPath}/Blogs/budgeting-101-for-the-millennials">Read More</a>
						</div>
						<ul>
							<li><p>Share : </p></li>
							<li><a href="https://www.facebook.com/vpayqwik/"><span class="fb"> </span></a></li>
							<li><a href="https://twitter.com/V_PayQwik"><span class="twit"> </span></a></li>
						</ul>
					</div>	
				
					<div class="about-two">
						<h3><a href="/Blogs/digital-wallets-in-india">
                        <b>Digital wallets in India- a match made in heaven or hell?</b><img src="${pageContext.request.contextPath}/resources/images/blogs/single-13.jpg" alt="digital-wallets-in-india" class="img-responsive" /></a></h3>
						<p>
						"Indian digital payment system will be worth 500 billion dollars by 2020", read a headline. Top news channels were breaking news like it was the apocalypse. The shift to a cashless economy was given a major boost the day demonetisation kicked in. Now, almost a year later, India presents one of the largest opportunities for digital payments and digital wallet companies are licking their lips.</p>
						<label class="pull-right" style="color: #989898;">Nov 13, 2017</label>
        				<div class="about-btn">
							<a href="${pageContext.request.contextPath}/Blogs/digital-wallets-in-india">Read More</a>
						</div>
						<ul>
							<li><p>Share : </p></li>
							<li><a href="https://www.facebook.com/vpayqwik/"><span class="fb"> </span></a></li>
							<li><a href="https://twitter.com/V_PayQwik"><span class="twit"> </span></a></li>
						</ul>
					</div>
				
					<div class="about-two">
						<h3><a href="${pageContext.request.contextPath}/Blogs/before-you-download-a-mobile-wallet-app">
                        <b>Five Things to Know Before You Download a Mobile Wallet App</b><img src="${pageContext.request.contextPath}/resources/images/blogs/single-12.jpg" alt="before-you-download-a-mobile-wallet-app" class="img-responsive" /></a></h3>
						<p><b>Bye bye wallet</b>
						In human history, money has evolved several times; from the barter system to coins, paper and now smartphones. Today several companies have revolutionized the way we shop and pay by replacing our wallets with smartphones.</p>
						<label class="pull-right" style="color: #989898;">Nov 13, 2017</label>
        				<div class="about-btn">
							<a href="${pageContext.request.contextPath}/Blogs/before-you-download-a-mobile-wallet-app">Read More</a>
						</div>
						<ul>
							<li><p>Share : </p></li>
							<li><a href="https://www.facebook.com/vpayqwik/"><span class="fb"> </span></a></li>
							<li><a href="https://twitter.com/V_PayQwik"><span class="twit"> </span></a></li>
						</ul>
					</div>				

				<div class="about-two">
						<h3><a href="/Blogs/managing-your-finances">
                        <b>Managing your Finances like a Superhero! </b><img src="/resources/images/blogs/single-11.jpg" alt="" class="img-responsive" /></a></h3>                        
						<p>Be it Marvel or DC, who hasn't walked out of a theater after watching a superhero movie and not felt fiercely inspired? How many of us felt superhero movies are more interesting towards the climax even after the closing credits? No matter how cliched it gets, we are always filled with awe when superheroes rise to take action. These extraordinary gentlemen and women can teach us a thing or two about managing one's personal finances.</p>		
						<label class="pull-right" style="color: #989898;">Nov 13, 2017</label>
        				<div class="about-btn">
							<a href="${pageContext.request.contextPath}/Blogs/managing-your-finances">Read More</a>
						</div>
						<ul>
							<li><p>Share : </p></li>
							<li><a href="https://www.facebook.com/vpayqwik/"><span class="fb"> </span></a></li>
							<li><a href="https://twitter.com/V_PayQwik"><span class="twit"> </span></a></li>
						</ul>
					</div><!---end about-two-->
				
					<div class="about-two">
						<h3><a href="/Blogs/introduction-to-mobile-money">
                        <b>INTRODUCTION TO MOBILE MONEY</b><img src="/resources/images/blogs/1-1.jpg" alt="" class="img-responsive" /></a></h3>                      
						<p>Digital Wallet, specializes in Secure Real-Time transaction processing service and support, and is a payment gateway that enables online payment for transactions and value added services at the point of sales. Digital Wallet performs payment authorization and authentication, and makes settlement of funds between Financial Institutions (FIs), Merchants and Users (Customers).</p>	
						<label class="pull-right" style="color: #989898;">Oct 30, 2017</label>						
        				<div class="about-btn">
							<a href="${pageContext.request.contextPath}/Blogs/introduction-to-mobile-money">Read More</a>
						</div>
						<ul>
							<li><p>Share : </p></li>
							<li><a href="https://www.facebook.com/vpayqwik/"><span class="fb"> </span></a></li>
							<li><a href="https://twitter.com/V_PayQwik"><span class="twit"> </span></a></li>
						</ul>
					</div><!---end about-two-->
                    	
					<div class="about-tre">
						<div class="a-1">
							<div class="col-md-6 abt-left">
								<h4><a href="${pageContext.request.contextPath}/Blogs/transfer-money-from-vpayqwik-to-bank"><img src="${pageContext.request.contextPath}/resources/images/blogs/9_9.jpg" alt="" />
								When you transfer money to Bank account from Vpayqwik, it is initiated instantly</a></h4>
								<p>Vpayqwik customers can transfer money from their Vpayqwik Wallets to a bank account at any time. We process this transaction instantly and in most cases money is reflected in user's Bank accounts within few minutes. However, when the banking systems are going through stress due load spikes, banks could take typically upto 7 working days to credit the amount in your bank account.</p>
								<label>Oct 10, 2017</label>
							</div>
							<div class="col-md-6 abt-left">
								<h4><a href="${pageContext.request.contextPath}/Blogs/know-more-about-your-Vpayqwik-wallet"><img src="${pageContext.request.contextPath}/resources/images/blogs/c-1.jpg" alt="" />
								Know more about your Vpayqwik Wallet: Security Features</a></h4>
								<p>Vpayqwik wallet is the most preferred payment option for users. Vpayqwik is accepted at merchant locations online &amp; offline. We are committed to ensure your account's security at all times and provide you with a good experience every time you pay using Vpayqwik.</p>
								<label>Sep 27, 2017</label>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="a-1">
							<div class="col-md-6 abt-left">
								<h4><a href="${pageContext.request.contextPath}/Blogs/accept-payment-through-vpayqwik"><img src="${pageContext.request.contextPath}/resources/images/blogs/c-3.jpg" alt="" />
								Self-declared Vpayqwik merchants can now accept money in their bank account</a></h4>
								<p>We are launching a new feature on Vpayqwik that would enable self-declared merchants to transfer up to Rs 50,000 directly in their bank accounts. The amount would be settled directly to the merchant's bank account. The fee to transfer money to your bank account.</p>
								<label>Sep 02, 2017</label>
							</div>
							<div class="col-md-6 abt-left">
								<h4><a href="${pageContext.request.contextPath}/Blogs/accept-vpayqwik-in-your-store"><img src="${pageContext.request.contextPath}/resources/images/blogs/c-4.jpg" alt="" />
								How to accept Vpayqwik in your Store: Process &amp; FAQs</a></h4>
								<p>Its extremely easy to accept Vpayqwik. There is zero setup fee, zero annual fee and no hardware involved. The best part is, you can go live instantly.</p>
								<label>Aug 15, 2017</label>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="a-1">
							<div class="col-md-6 abt-left">
								<h4><a href="${pageContext.request.contextPath}/Blogs/how-we-authenticate-a-user"><img src="${pageContext.request.contextPath}/resources/images/blogs/c-5.jpg" alt="" />
								Your Vpayqwik ID &amp; Password is not enough to access Vpayqwik Wallet Account</a></h4>
								<p>We take security very seriously at Vpayqwik while keeping the user experience intact. With users trusting us &amp; sharing their feedback with us, we have several foolproof measures to save any misuse of your Vpayqwik Wallet.</p>
								<label>July 31, 2017</label>
							</div>
							<div class="col-md-6 abt-left">
								<h4><a href="${pageContext.request.contextPath}/Blogs/what-if-money-gets-deducted-from-your-bank"><img src="" alt="" />
								What if money gets deducted from your bank/ card, but does not reflect in Vpayqwik Wallet Balance immediately?</a></h4>
								<p>The next time you receive a notification from the bank saying money has been deducted, but the amount doesn't reflect in your Vpayqwik account, don't worry. Your money is absolutely safe in the banking system.</p>
								<label>July 12, 2017</label>
							</div>
							<div class="clearfix"></div>
						</div>
					</div><!--end about-tre-->
				</div><!--end col-md-8-->
                
				 <jsp:include page="/WEB-INF/jsp/Blogs/BlogRightMenu.jsp" />		
		</div>
	</div>
	</div>
	<!--about-end-->
   
    
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>