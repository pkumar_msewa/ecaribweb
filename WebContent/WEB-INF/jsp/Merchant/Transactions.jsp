<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<sec:csrfMetaTags />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>eCarib | Transactions Report</title>

    <!-- Bootstrap core CSS -->
    <link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'
	type="image/png" />

    <link
            href="${pageContext.request.contextPath}/resources/admin/css/bootstrap.min.css"
            rel="stylesheet">

    <link
            href="${pageContext.request.contextPath}/resources/admin/fonts/css/font-awesome.min.css"
            rel="stylesheet">
    <link
            href="${pageContext.request.contextPath}/resources/admin/css/animate.min.css"
            rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link
            href="${pageContext.request.contextPath}/resources/admin/css/custom.css"
            rel="stylesheet">
    <link
            href="${pageContext.request.contextPath}/resources/admin/css/icheck/flat/green.css"
            rel="stylesheet">

    <link
            href="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.css"
            rel="stylesheet" type="text/css" />
    <link
            href="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.css"
            rel="stylesheet" type="text/css" />
    <link
            href="${pageContext.request.contextPath}/resources/admin/js/datatables/fixedHeader.bootstrap.min.css"
            rel="stylesheet" type="text/css" />
    <link
            href="${pageContext.request.contextPath}/resources/admin/js/datatables/responsive.bootstrap.min.css"
            rel="stylesheet" type="text/css" />
    <link
            href="${pageContext.request.contextPath}/resources/admin/js/datatables/scroller.bootstrap.min.css"
            rel="stylesheet" type="text/css" />
<script
	src="${pageContext.request.contextPath}/resources/admin/js/jquery.min.js"></script>
	
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>
	
	<script src="<c:url value='/resources/super_admin/js/header.js'/>"></script>
    <style>
        .no-js #loader {
            display: none;
        }

        .js #loader {
            display: block;
            position: absolute;
            left: 100px;
            top: 0;
        }

        .se-pre-con {
            position: fixed;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: url(/images/pq_large.gif) center no-repeat #fff;
        }
        
        body{
          padding-right:0 !important;
        }
    </style>
    <script src="<c:url value='/resources/js/modernizr.js'/>"></script>
    <link rel="stylesheet" href="<c:url value="/resources/css/datepicker.css"/>">
    <script src="<c:url value="/resources/js/datepicker.js"/>"></script>
    <script>
        $(function() {
            $( "#toDate" ).datepicker({
                format:"yyyy-mm-dd"
            });
            $( "#fromDate" ).datepicker({
                format:"yyyy-mm-dd"
            });
        });
    </script>

    <script type="text/javascript">
        $(window).load(function() {
            $(".se-pre-con").fadeOut("slow");
        });
    </script>
    
     
</head>
<body class="nav-md">
<div class="se-pre-con"></div>
<div class="container body">
    <div class="main_container">
        <jsp:include page="/WEB-INF/jsp/Merchant/LeftMenu.jsp" />
        <jsp:include page="/WEB-INF/jsp/Merchant/TopNavigation.jsp" />

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3>eCarib Merchants</h3>
                    </div>
                </div>
                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <!--  <h2>Button Example <small>Users</small></h2> -->
                                <ul class="nav navbar-right panel_toolbox">
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <p class="text-muted font-13 m-b-30">
                                    <!--                       The Buttons extension for DataTables provides a common set of options, API methods and styling to display buttons on a page that will interact with a DataTable. The core library provides the based framework upon which plug-ins can built.
-->
                             <form action="<c:url value="/Merchant/TransactionFiltered"/>" method="post" class="form form-inline" >
                                From <input type="text" id="fromDate" name="fromDate" class="form-control" readonly/> 
                                  To <input type="text" id="toDate" name="toDate" class="form-control" readonly/> 
                                <sec:csrfInput/>
                                 <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-filter"></span></button>
                             </form> 
                                </p>
                               <table id="datatable-buttons"
										class="table table-striped table-bordered date_sorted">
                                    <thead>
                                    
                                    <tr>
                                        <th>S.No</th>
                                        <th>Transaction Date</th>
                                        <th>User Details</th>
                                        <th>Transaction ID</th>
                                        <th>Order ID</th>
                                        <th>Amount</th>
                                        <th>Status</th>
                                        <th>Refund</th>
                                    </tr>
                                    
                                    </thead>
                                   	<tbody id=userList>
											<c:forEach items="${transactionList}" var="list" varStatus="loopCounter">
												<tr>
												    <td id="o_count">${loopCounter.count}</td>
												    
													<td><c:out value="${list.date}" default="" escapeXml="true"/></td>
													
													<td id="o_mobile-${loopCounter.count}"><b>MobileNumber:</b><c:out value="${list.contactNo}" default="" escapeXml="true"/> <br />
													<b>Email Id:</b><c:out value="${list.email}" default="" escapeXml="true"/> <br>
													 <b>First Name:</b><c:out value="${list.firstName}" default="" escapeXml="true" /> <br /> </td>
													
													<td id="o_txnid-${loopCounter.count}"><c:out value="${list.transactionRefNo}" default="" escapeXml="true"/><br /> </td>
													
													 <td><c:out value="${list.orderId}" default="" escapeXml="true"/><br /> </td>
													 
													<td id="o_amount-${loopCounter.count}"><c:out value="${list.amount}" default="" escapeXml="true"/><br /> </td>
													
													<td><c:out value="${list.status}" default="" escapeXml="true"/> <br /></td>
													
													<td><center>
											<c:if test='${list.email eq "bikram.rout@patanjaliayurved.org"}'>
											
										     <span id="refund_status-${loopCounter.count}"><a onclick="openRefundModal('${list.contactNo}','${list.transactionRefNo}','${list.amount}','${list.orderId}')"><i class="fa fa-retweet fa-2x update-refundAmount" data-toggle="modal" data-target="#partialRefundModal"></i></a></span>
											
											</c:if>
										</center>
										</td>
										
												</tr>
											</c:forEach>
										</tbody>
                                </table>
                            </div>
                         </div>
                          	<div class="modal fade" id="success_refundNotification" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
								<div class="modal-dialog">
									<!-- Modal content-->
									<div class="modal-content"
										style="margin-top: 40%; margin-left: 100px;">

										<div class="modal-body">
											<center>
												<img
													src="${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/source-(tick).gif"
													style="width: 140px; margin-top: -91px; margin-bottom: 10px;">
												<div
													style="background: white; margin-top: -20px; padding: 2.5% 2%;">
														<h4 class="refund_body"></h4><br>
													<center>
														<button type="button" class="btn" data-dismiss="modal">OK</button> <!-- onclick="javascript:window.location.reload()" -->
													</center>
												</div>
											</center>
										</div>

									</div>
								</div>
							</div> 
							
							<div class="modal fade" id="error_refundNotification" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
								<div class="modal-dialog">
									<!-- Modal content-->
									<div class="modal-content"
										style="margin-top: 40%; margin-left: 100px;">

										<div class="modal-body">
											<center>
												<img
													src="${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/x.gif"
													style="width: 140px; margin-top: -91px; margin-bottom: 10px;">
												<div
													style="background: white; margin-top: -20px; padding: 2.5% 2%;">
														<h4 class="refund_body"></h4><br>
													<center>
														<button type="button" class="btn" data-dismiss="modal">OK</button>
													</center>
												</div>
											</center>
										</div>

									</div>
								</div>
							</div>  
                        
                        
                       <div id="partialRefundModal" role="dialog" class="modal fade" data-backdrop="static" data-keyboard="false">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                     <center> <h3 class="modal-title">Refund Amount</h3></center>
                                        <button type="button" class="close" data-dismiss="modal">�</button>
                                   </div>
        
                                    <!-- Modal body -->
                                    	<div class="modal-body">
									<table class="table table-condensed">
										<tr>
											<td><b>Transaction RefNo</b></td>
											<td id="o_transactionId"></td>
										</tr>
										<tr>
											<td><b>Order Id</b></td>
											<td id="o_orderId"></td>
										</tr>
										<tr>
											<td><b>Net Amount</b></td>
											<td id="o_amount"></td>
										</tr>
										<tr>
											<td><b>Refund Amount</b></td>
											<td><input id="o_netamount" type="text" placeholder="0.0" /></td>
										</tr>
									</table>
									<center><button type="button" class="btn" id="refundProcess">Confirm</button><center>
								</div>
                             </div>
                          </div>
                        </div>
                    </div>
<nav>
							<ul class="pagination" id="paginationn">
							  </ul>
							  </nav>
                </div>
            </div>


            <jsp:include page="/WEB-INF/jsp/Admin/Footer.jsp" />
        </div>
        <!-- /page content -->
    </div>

</div>

<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix"
        data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>
  
<script
        src="${pageContext.request.contextPath}/resources/admin/js/bootstrap.min.js"></script>

<!-- bootstrap progress js -->
<script
        src="${pageContext.request.contextPath}/resources/admin/js/progressbar/bootstrap-progressbar.min.js"></script>
<script
        src="${pageContext.request.contextPath}/resources/admin/js/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<script
        src="${pageContext.request.contextPath}/resources/admin/js/icheck/icheck.min.js"></script>

<script
        src="${pageContext.request.contextPath}/resources/admin/js/custom.js"></script>


<!-- Datatables -->
<script
        src="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.js"></script>
<script
        src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.bootstrap.js"></script>
<script
        src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.buttons.min.js"></script>
<script
        src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.js"></script>
<script
        src="${pageContext.request.contextPath}/resources/admin/js/datatables/jszip.min.js"></script>
<script
        src="${pageContext.request.contextPath}/resources/admin/js/datatables/pdfmake.min.js"></script>
<script
        src="${pageContext.request.contextPath}/resources/admin/js/datatables/vfs_fonts.js"></script>
<script
        src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.html5.min.js"></script>
<script
        src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.print.min.js"></script>
<script
        src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.fixedHeader.min.js"></script>
<script
        src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.keyTable.min.js"></script>
<script
        src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.responsive.min.js"></script>
<script
        src="${pageContext.request.contextPath}/resources/admin/js/datatables/responsive.bootstrap.min.js"></script>
<script
        src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.scroller.min.js"></script>

<!-- pace -->
       
<script>
    var handleDataTableButtons = function() {
        "use strict";
        0 !== $("#datatable-buttons").length
        && $("#datatable-buttons").DataTable({
            dom : "Bfrtip",
            buttons : [ {
                extend : "copy",
                className : "btn-sm"
            }, {
                extend : "csv",
                className : "btn-sm"
            }, {
                extend : "excel",
                className : "btn-sm"
            }, {
                extend : "pdf",
                className : "btn-sm"
            }, {
                extend : "print",
                className : "btn-sm"
            } ],
            responsive : !0
        })
    }, TableManageButtons = function() {
        "use strict";
        return {
            init : function() {
                handleDataTableButtons()
            }
        }
    }();
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#datatable').dataTable();
        $('#datatable-keytable').DataTable({
            keys : true
        });
        $('#datatable-responsive').DataTable();
        $('#datatable-scroller').DataTable({
            ajax : "js/datatables/json/scroller-demo.json",
            deferRender : true,
            scrollY : 380,
            scrollCollapse : true,
            scroller : true
        });
        var table = $('#datatable-fixed-header').DataTable({
            fixedHeader : true
        });
    });
    TableManageButtons.init();
</script>
</body>
</html>
