<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Service List</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<sec:csrfMetaTags/>
	<!-- CSS -->
	<script src="<c:url value='/resources/js/jquery.js'/>"></script>
	<%-- <link href="${pageContext.request.contextPath}/resources/admin/css/bootstrap.min.css" rel="stylesheet"> --%>
	<link rel="stylesheet" href="<c:url value='/resources/super_admin/css/bootstrap.min.css'/>">
	<link rel="stylesheet" href="<c:url value='/resources/super_admin/css/vendor/icon-sets.css'/>">
	<link rel="stylesheet" href="<c:url value='/resources/super_admin/css/main.min.css'/>">
	<link rel="stylesheet" href="<c:url value='/resources/super_admin/css/demo.css'/>">
	<link rel="stylesheet" href="<c:url value='/resources/css/font-awesome.min.css'/>">
	<link href="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.css"
			rel="stylesheet" type="text/css" />
	<link href="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.css"
			rel="stylesheet" type="text/css" />
	<link href="${pageContext.request.contextPath}/resources/admin/js/datatables/fixedHeader.bootstrap.min.css"
			rel="stylesheet" type="text/css" />
	<link href="${pageContext.request.contextPath}/resources/admin/js/datatables/responsive.bootstrap.min.css"
			rel="stylesheet" type="text/css" />
	<link href="${pageContext.request.contextPath}/resources/admin/js/datatables/scroller.bootstrap.min.css"
			rel="stylesheet" type="text/css" />

	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="<c:url value = '/resources/super_admin/img/apple-icon.png'/>">
	<link rel="icon" type="image/png" sizes="96x96" href="<c:url value = '/resources/images/favicon.png'/>">
	<script src="<c:url value='/resources/super_admin/js/jquery/jquery-2.1.0.min.js'/>"></script>
	<link rel="stylesheet" href="<c:url value="/resources/css/datepicker.css"/>">
	<script src="<c:url value="/resources/js/datepicker.js"/>"></script>
	   <script type="text/javascript">
$(document).ready(function() {
	
         		var errorMsg=document.getElementById("errorMsg").value;
        		if(!errorMsg.length == 0){
        			$("#common_error_true").modal("show");
        			$("#common_error_msg").html(errorMsg);
        			var timeout = setTimeout(function(){
        				$("#common_error_true").modal("hide");
        				$("#errorMsg").val("");
        	          }, 5000);
        		}
        		
        		var successMsg=document.getElementById("successMsg").value;
        		if(!successMsg.length == 0){
        			$("#common_success_true").modal("show");
        			$("#common_success_msg").html(successMsg);
        			var timeout = setTimeout(function(){
        				$("#common_success_true").modal("hide");
        				$("#successMsg").val("");
        	          }, 5000); 
        		}
         	});
   </script>      
<style>
.name-input:valid { text-transform: uppercase; }

</style>	         
	
</head>

<body>
	<!-- WRAPPER -->
<div id="wrapper">
	<!-- SIDEBAR -->
	<jsp:include page="/WEB-INF/jsp/SuperAdmin/Sidebar.jsp"/>
	<!-- END SIDEBAR -->
	<!-- MAIN -->
	<input type="text" name="successMsg" id="successMsg" value="${msg}">
	<input type="hidden" name="errorMsg" id="errorMsg" value="${error}">
	<div class="main">
		<!-- NAVBAR -->
		<jsp:include page="/WEB-INF/jsp/SuperAdmin/Navbar.jsp"/>
		<!-- END NAVBAR -->
		<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
							<div class="panel panel-headline">
								<div class="panel-heading">
							<h3 class="panel-title">Add Service Type</h3></div>
						<div class="panel-body" style="background: #eaeaea;">
							<div class="row">
								<div class="col-md-4 col-md-offset-4"><br>
									<!-- ADD MERCHANT FORM START -->
									<div align="center">
										<form
											action="${pageContext.request.contextPath}/SuperAdmin/AddServiceType"
											method="post">
											<div align="center">
												<div class="form-group">
													<input type="text" class="form-control" required="required"
														placeholder="Service Name" id="name" name="name"
														onkeypress="clearvalue('error_name')">
													<p id="error_name" style="color: red; margin-top: 2px;"></p>
												</div>

												<div class="form-group">
													<input type="text" class="form-control" required="required"
														placeholder="Description" id="description"
														name="description" onkeypress="clearvalue('error_description')">
													<p id="error_description"
														style="color: red; margin-top: 2px;"></p>
												</div>

												<div class="form-group">
													<button type="submit" id="loadMId"
														onclick="return loadValidate();" name="singlebutton"
														class="btn btn-default">ADD SERVICE TYPE</button>
												</div>
											</div>
										</form>
									</div>
								<!-- ADD MERCHANT FORM END -->
								</div>
							</div>
						</div>
					</div> 
					<!-- END OVERVIEW -->
				</div>
			</div>
			<!-- END MAIN CONTENT -->
			
		<div id="common_success_true" role="dialog" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<center id="common_success_msg" class="alert alert-success"></center>
				</div>
			</div>
		</div>
	</div>
	
	<div id="common_error_true" role="dialog" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<center id="common_error_msg" class="alert alert-danger"></center>
				</div>
			</div>
		</div>
	</div>
		</div>
			<!-- END MAIN -->
			<%-- <div class="clearfix"></div>
			<jsp:include page="/WEB-INF/jsp/Admin/Footer.jsp" /> --%>
		</div>
		<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="<c:url value='/resources/super_admin/js/jquery/jquery-2.1.0.min.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/search.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/header.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/notifications.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/bootstrap/bootstrap.min.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/plugins/jquery-slimscroll/jquery.slimscroll.min.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/plugins/jquery-easypiechart/jquery.easypiechart.min.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/plugins/chartist/chartist.min.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/vpqadmin.min.js'/>"></script>

<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.bootstrap.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.buttons.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/jszip.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/pdfmake.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/vfs_fonts.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.html5.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.print.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.fixedHeader.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.keyTable.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.responsive.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/responsive.bootstrap.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.scroller.min.js"></script>


<!-- pace -->
<script
		src="${pageContext.request.contextPath}/resources/admin/js/pace/pace.min.js"></script>
		
		<script type="text/javascript">
		function loadValidate() {
			
			var spinnerUrl = "Please wait <img src='/resources/admin/images/spinner.gif' height='20' width='20'>"
			var alphaExp="/^[a-zA-Z]+$/";
			var valid = true;
			var name = $('#name').val();
			var description = $('#description').val();
			 if(name == "" || name == "0"){
					valid = false;
					$("#error_name").html("Name should not be blank .");	
			 }else if(!name.match(alphaExp)){
				 valid = false;
					$("#error_name").html("Name only allow alphabets."); 
			 }else if(name.length > 15){
				 valid = false;
					$("#error_name").html("please enter maximum 15 character name."); 
			 }
			 if(description == "" || description == "0"){
					valid = false;
					$("#error_description").html("Description should not be blank .");
			 } else if(description.length > 30){
				 valid = false;
					$("#error_description").html("please enter maximum 30 character description."); 
			 }
			 
			/*  var timeout = setTimeout(function(){
				 $("#error_name").html("");
				 $("#error_description").html(""); 
 	          }, 4000);
			  */
			if(valid == true) {
			     //$("#loadMId").attr("disabled","disabled");
				 $("#loadMId").html(spinnerUrl);  
				return valid;
			}else{
				 $("#loadMId").html("ADD SERVICE TYPE"); 
			}
			return valid;
		}
		</script>
		<script>
		function clearvalue(val) {
			$("#" + val).text("");
		}
		</script>
	<script>
	
	
		$(document).ready(function() {
			$('#editedtable').DataTable({
				 "bLengthChange": false,
				 "bPaginate" : true,	
		 		    "bFilter" : true,
		 		    "bInfo" : true,
				"order" : [],
				columnDefs : [ {
					orderable : true,
					targets : [ 0 ]
				}, {
					orderable : true,
					targets : [ 1 ]
				}, {
					orderable : true,
					targets : [ 2 ]
				}, {
					orderable : true,
					targets : [ 3 ]
				}, {
					orderable : true,
					targets : [ 4 ]
				}, {
					orderable : true,
					targets : [ 5 ]
				} ],
			});
		});
	</script>
	<script>
		$('#userId').change(function(){
			$("#formId").submit();
		})
	</script>
</body>

</html>
