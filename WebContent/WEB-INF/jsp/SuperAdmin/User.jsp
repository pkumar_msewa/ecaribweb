<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!doctype html>
<html lang="en">
<head>
	<title>All Users</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<sec:csrfMetaTags/>
	<!-- CSS -->
	<script src="<c:url value='/resources/js/jquery.js'/>"></script>
	<link rel="stylesheet" href="<c:url value='/resources/super_admin/css/bootstrap.min.css'/>">
	<link rel="stylesheet" href="<c:url value='/resources/super_admin/css/vendor/icon-sets.css'/>">
	<link rel="stylesheet" href="<c:url value='/resources/super_admin/css/main.min.css'/>">
	<link rel="stylesheet" href="<c:url value='/resources/super_admin/css/demo.css'/>">
	<link rel="stylesheet" href="<c:url value='/resources/css/font-awesome.min.css'/>">
	<link
			href="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.css"
			rel="stylesheet" type="text/css" />
	<link
			href="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.css"
			rel="stylesheet" type="text/css" />
	<link
			href="${pageContext.request.contextPath}/resources/admin/js/datatables/fixedHeader.bootstrap.min.css"
			rel="stylesheet" type="text/css" />
	<link
			href="${pageContext.request.contextPath}/resources/admin/js/datatables/responsive.bootstrap.min.css"
			rel="stylesheet" type="text/css" />
	<link
			href="${pageContext.request.contextPath}/resources/admin/js/datatables/scroller.bootstrap.min.css"
			rel="stylesheet" type="text/css" />

	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="<c:url value = '/resources/super_admin/img/apple-icon.png'/>">
	<link rel="icon" type="image/png" sizes="96x96" href="<c:url value = '/resources/images/favicon.png'/>">
	<script src="<c:url value='/resources/super_admin/js/jquery/jquery-2.1.0.min.js'/>"></script>
	
</head>

<body>
<!-- WRAPPER -->
<div id="wrapper">
	<!-- SIDEBAR -->
	<jsp:include page="/WEB-INF/jsp/SuperAdmin/Sidebar.jsp"/>
	<!-- END SIDEBAR -->
	<!-- MAIN -->
	<div class="main">
		<!-- NAVBAR -->
		<jsp:include page="/WEB-INF/jsp/SuperAdmin/Navbar.jsp"/>
		<!-- END NAVBAR -->
		<!-- MAIN CONTENT -->
		<div class="main-content">
			<div class="container-fluid">
				<div class="panel panel-profile">
					<div class="clearfix">
					<h4><c:out value="${msg}" escapeXml="true"/></h4>
						<!-- LEFT COLUMN -->
						<div class="profile-left">
							<!-- PROFILE HEADER -->
							<div class="profile-header">
								<div class="overlay"></div>
								<div class="profile-main">
									<img src="<c:url value='${basic.image}'/>" class="img-circle" width="200" height="200" id="profileImage" alt="profileImage">
									<h3 class="name"><c:out value="${basic.name}" default=""/></h3>
									<c:if test="${online eq true}">
									<span class="online-status status-available">Available</span>
									</c:if>
									<c:if test="${online eq false}">
										<span class="online-status status-offline">Offline</span>
									</c:if>

								</div>
								<div class="profile-stat">
									<div class="row">
										<div class="col-md-4 stat-item">
											<span>Balance</span> <c:out value="${account.balance}" default=""/>
										</div>
										<div class="col-md-4 stat-item">
											<span>Account Type</span>	<c:out value="${account.code}" default="" />
										</div>
										<div class="col-md-4 stat-item">
											<span>Points</span>	<c:out value="${account.points}" default=""/>
										</div>
									</div>
								</div>
							</div>
							<!-- END PROFILE HEADER -->
							<!-- PROFILE DETAIL -->
							<div class="profile-detail">
								<div class="profile-info">
									<h4 class="heading">Basic Info</h4>
									<ul class="list-unstyled list-justify">
										<li>Registration Date <span><c:out value="${basic.registrationDate}" default=""/></span></li>
										<li>Mobile <span><c:out value="${basic.mobile}" default=""/></span></li>
										<li>Email <span><a href="mailto:${basic.email}"><c:out value="${basic.email}" default=""/></a></span></li>
										<li>Gender <span id="gender"><c:out value="${basic.gender}" default=""/></span></li>
										<li>DOB <span><c:out value="${basic.dateOfBirth}" default=""/></span></li>
										<li>Authority <span><c:out value="${basic.authority}" default=""/></span></li>
										<li>Mobile Status <span><c:out value="${basic.mobileStatus}" default=""/></span></li>
										<li>Username <span><c:out value="${basic.username}" default=""/></span></li>
										<c:if test="${basic.pinCode ne 'null'}">
										<li>Pincode <span><c:out value="${basic.pinCode}" default=""/></span></li>
										<li>Circle <span><c:out value="${basic.circleName}" default=""/></span></li>
										<br/><br/><br/><br/>
										</c:if>
										<c:if test="${fn:containsIgnoreCase(basic.authority,'ROLE_USER')}">
										<c:if test="${fn:containsIgnoreCase( basic.authority, 'ROLE_AUTHENTICATED')}">
											<a class="btn btn-danger btn-block"
											   href='<c:url value="/SuperAdmin/User/Block/${basic.username}" />'>Lock <i class="fa fa-lock" aria-hidden="true"></i></a>
										</c:if>
										<c:if test="${fn:containsIgnoreCase( basic.authority, 'ROLE_LOCKED')}">
											<a class="btn btn-success btn-block"
											   href='<c:url value="/SuperAdmin/User/Unblock/${basic.username}" />'>Unlock<i class="fa fa-unlock" aria-hidden="true"></i></a>
										</c:if>
										<c:if test="${fn:containsIgnoreCase( basic.authority, 'ROLE_BLOCKED')}">
											<a class="btn btn-info btn-block"
											   href='<c:url value="/SuperAdmin/User/Unblock/${basic.username}" />'>Unblock <i class="fa fa-ban" aria-hidden="true"></i></a>
										</c:if>
											</c:if>
											<c:choose>
											<c:when test="${fn:containsIgnoreCase(account.code,'NONKYC')}">
											<a class="btn btn-info btn-block" href='<c:url value="/SuperAdmin/User/KycUpdate/${basic.username}" />'>KYC <i class="fa fa-user-plus" aria-hidden="true"></i></a>
										</c:when>
										<c:when test="${fn:containsIgnoreCase(account.code,'KYC')}">
											<a class="btn btn-warning btn-block"
											   href='<c:url value="/SuperAdmin/User/Non-KycUpdate/${basic.username}" />'>Non-KYC <i class="fa fa-user-times" aria-hidden="true"></i></a>
										</c:when>
										</c:choose>
									</ul>
								</div>

							</div>
							<!-- END PROFILE DETAIL -->
						</div>
						<!-- END LEFT COLUMN -->
						<!-- RIGHT COLUMN -->
						<div class="profile-right">
							<h4 class="heading">Account Info</h4>
							<!-- AWARDS -->
							<div class="awards">
								<div class="row">
									<div class="col-md-3 col-sm-3">
										<div class="award-item">
												<p><c:out value="${account.accountNumber}" default=""/></p><br/>
											<span><b>Account Number</b></span>
										</div>
									</div>
									<div class="col-md-3 col-sm-3">
										<div class="award-item">
											<p>Rs. <c:out value="${totalCredit}" default=""/></p><br/>
											<span><b>Total Credited</b></span>
										</div>
									</div>
									<div class="col-md-3 col-sm-3">
										<div class="award-item">
											<p>Rs. <c:out value="${totalDebit}" default=""/></p><br/>
											<span><b>Total Debited</b></span>
										</div>
									</div>
									<div class="col-md-3 col-sm-3">
										<div class="award-item">
											<p>Rs. <c:out value="${account.monthlyLimit}" default=""/></p><br/>
											<span><b>Monthly Limit</b></span>
										</div>
									</div>
								</div>
							</div>
							<!-- END AWARDS -->
							<!-- TABBED CONTENT -->
							<div class="custom-tabs-line tabs-line-bottom left-aligned">
								<ul class="nav" role="tablist">
									<%--<li class="active"><a href="#tab-bottom-left1" role="tab" data-toggle="tab">Transactions <span class="badge"><c:out value="${transactions.size()}"/></span> </a></li>--%>
									<li><a href="#tab-bottom-left2" role="tab" data-toggle="tab">Login Logs </a></li>
								</ul>
							</div>
							<div class="tab-content">
								<div class="tab-pane fade in active" id="tab-bottom-left1">
									<table  class="table table-striped table-bordered dt-buttons">
										<thead>
										<tr>
											<th>Date</th>
											<th>Amount</th>
											<th>Current Balance</th>
											<th>Description</th>
											<th>Ref #</th>
											<th>Status</th>
										</tr>
										</thead>
										<tbody id="transactionList">
										<c:forEach items="${transactions}" var="transaction"
												   varStatus="loopCounter">
											<tr>
												<td> <c:out value="${transaction.date}"
															default="" escapeXml="true" /></td>
												<td>
													<c:out value="${transaction.amount}"
														   default="" escapeXml="true" />
												</td>
												<td>
													<c:out value="${transaction.currentBalance}"
														   default="" escapeXml="true" />
												</td>
												<td>
													<c:out value="${transaction.description}"
														   default="" escapeXml="true" />
												</td>
												<td> <c:out value="${transaction.transactionRefNo}"
																default="" escapeXml="true" /></td>
												<td>
													<c:out value="${transaction.status}"
														   default="" escapeXml="true" />
												</td>
											</tr>
										</c:forEach>
										</tbody>

									</table>
								</div>
								<div class="tab-pane fade" id="tab-bottom-left2">
									<div class="table-responsive">
										<table class="table table-striped table-bordered dt-buttons">
											<thead>
											<tr>
												<th>Date and Time</th>
												<th>Device ID</th>
												<th>Status</th>
											</tr>
											</thead>
											<tbody>
											<c:forEach items="${loginLogs}" var="login">
												<tr>
													<td><c:out value="${login.loginTime}"/></td>
													<td><c:out value="${login.deviceId}"/></td>
													<td><c:out value="${login.status}"/></td>
												</tr>
											</c:forEach>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<!-- END TABBED CONTENT -->
						</div>
						<!-- END RIGHT COLUMN -->
					</div>
				</div>

			</div>
		</div>
		<!-- END MAIN CONTENT -->
		<jsp:include page="/WEB-INF/jsp/SuperAdmin/Footer.jsp"/>
	</div>
	<!-- END MAIN -->
</div>
<!-- END WRAPPER -->
<!-- Javascript -->
<script src="<c:url value='/resources/super_admin/js/jquery/jquery-2.1.0.min.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/search.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/header.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/notifications.js'/>"></script>
<script>
	$(function() {
		console.log("ajax");
		var img = "";
		var gender = $("#gender").html();
		if(gender == "F") {
			img = "/resources/images/main/f_default.png";
		}else {
			img = "/resources/images/main/m_default.png";
		}
		$.ajax(
				{
					url:$("#profileImage").attr("src"),
					error:$("#profileImage").attr("src",img)
				}
				);


	});
</script>

<script src="<c:url value='/resources/super_admin/js/bootstrap/bootstrap.min.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/plugins/jquery-slimscroll/jquery.slimscroll.min.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/plugins/jquery-easypiechart/jquery.easypiechart.min.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/plugins/chartist/chartist.min.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/vpqadmin.min.js'/>"></script>

<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.bootstrap.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.buttons.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/jszip.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/pdfmake.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/vfs_fonts.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.html5.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.print.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.fixedHeader.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.keyTable.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.responsive.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/responsive.bootstrap.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.scroller.min.js"></script>


<!-- pace -->
<script
		src="${pageContext.request.contextPath}/resources/admin/js/pace/pace.min.js"></script>
<script>
	var handleDataTableButtons = function() {
		"use strict";
		0 !== $(".dt-buttons").length
		&& $(".dt-buttons").DataTable({
			"pageLength":5,
			columnDefs: [

			],
			dom : "Bfrtip",
			buttons : [ {
				extend : "copy",
				className : "btn-sm"
			}, {
				extend : "csv",
				className : "btn-sm"
			}, {
				extend : "excel",
				className : "btn-sm"
			}, {
				extend : "pdf",
				className : "btn-sm"
			}, {
				extend : "print",
				className : "btn-sm"
			} ],

		})
	}, TableManageButtons = function() {
		"use strict";
		return {
			init : function() {
				handleDataTableButtons()
			}
		}
	}();
</script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#datatable').dataTable({
			"pageLength":5
		});
		$('#datatable-keytable').DataTable({
			keys : true
		});
		$('#datatable-responsive').DataTable();
		$('#datatable-scroller').DataTable({
			ajax : "js/datatables/json/scroller-demo.json",
			deferRender : true,
			scrollY : 380,
			scrollCollapse : true,
			scroller : true
		});
		var table = $('#datatable-fixed-header').DataTable({
			fixedHeader : true
		});
	});
	TableManageButtons.init();
</script>

</body>

</html>
