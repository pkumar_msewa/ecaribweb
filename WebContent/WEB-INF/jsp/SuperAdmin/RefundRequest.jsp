<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!doctype html>
<html lang="en">
<head>
	<title>Merchant Refund Request</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<sec:csrfMetaTags/>
	<!-- CSS -->
	<script src="<c:url value='/resources/js/jquery.js'/>"></script>
	<link rel="stylesheet" href="<c:url value='/resources/super_admin/css/bootstrap.min.css'/>">
	<link rel="stylesheet" href="<c:url value='/resources/super_admin/css/vendor/icon-sets.css'/>">
	<link rel="stylesheet" href="<c:url value='/resources/super_admin/css/main.min.css'/>">
	<link rel="stylesheet" href="<c:url value='/resources/super_admin/css/demo.css'/>">
	<link rel="stylesheet" href="<c:url value='/resources/css/font-awesome.min.css'/>">
	<link
			href="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.css"
			rel="stylesheet" type="text/css" />
	<link
			href="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.css"
			rel="stylesheet" type="text/css" />
	<link
			href="${pageContext.request.contextPath}/resources/admin/js/datatables/fixedHeader.bootstrap.min.css"
			rel="stylesheet" type="text/css" />
	<link
			href="${pageContext.request.contextPath}/resources/admin/js/datatables/responsive.bootstrap.min.css"
			rel="stylesheet" type="text/css" />
	<link
			href="${pageContext.request.contextPath}/resources/admin/js/datatables/scroller.bootstrap.min.css"
			rel="stylesheet" type="text/css" />

	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="<c:url value = '/resources/super_admin/img/apple-icon.png'/>">
	<link rel="icon" type="image/png" sizes="96x96" href="<c:url value = '/resources/images/favicon.png'/>">
	<script src="<c:url value='/resources/super_admin/js/jquery/jquery-2.1.0.min.js'/>"></script>
	<link rel="stylesheet" href="<c:url value="/resources/css/datepicker.css"/>">

</head>

<body>
<!-- WRAPPER -->
<div id="wrapper">
	<!-- SIDEBAR -->
	<jsp:include page="/WEB-INF/jsp/SuperAdmin/Sidebar.jsp"/>
	<!-- END SIDEBAR -->
	<!-- MAIN -->
	<div class="main">
		<!-- NAVBAR -->
		<jsp:include page="/WEB-INF/jsp/SuperAdmin/Navbar.jsp"/>
		<!-- END NAVBAR -->
		<!-- MAIN CONTENT -->
		<div class="main-content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">
						<b>${msg}</b><br/>
						<form action="<c:url value="/SuperAdmin/TransactionReport"/>" method="post" class="form form-inline">
							From <input type="text" id="fromDate" name="startDate" class="form-control" readonly/>
							To <input type="text" id="toDate" name="endDate" class="form-control" readonly/>
							<sec:csrfInput/>
							<button type="submit" class="btn btn-primary"><i class="fa fa-filter" aria-hidden="true"></i></button>
						<a href="<c:url value="/SuperAdmin/FilterTransactions"/>">Advanced Filter</a>
						</form><br/>

						<table id="datatable-buttons"
							   class="table table-striped table-bordered">
							<thead>
							<tr>
								<th>S.No</th>
								<th>Date and Time</th>
								<th>Ref No.</th>
								<th>Order Id</th>
								<th>Amount</th>
								<th>Refund Amount</th>
								<th>Status</th>
								<th>Refund</th>
							</tr>
							</thead>
							<tbody id="transactionLists">
							<c:forEach items="${refundTransactionLists}" var="transaction"
									   varStatus="loopCounter">
								<tr>
									<td>${loopCounter.count}</td>
									
									<td >
										<c:out value="${transaction.dateOfTrasnaction}" default="" escapeXml="true" />
									</td>
									<td>
										<a class="transactionRefNo" onclick='getTransactionDetails("${transaction.transactionRefNo}")'><c:out value="${transaction.transactionRefNo}" default="" escapeXml="true" /></a>
									</td>
									<td>
										<c:out value="${transaction.orderId}" default="" escapeXml="true" /><br/>
									</td>
									<td>
										<c:out value="${transaction.netAmount}" default="" escapeXml="true" /><br/>
									</td>
									<td>
										<c:out value="${transaction.refundAmount}" default="" escapeXml="true" /><br/>
									</td>
									<td>
										<c:out value="${transaction.status}" default="" escapeXml="true" /><br/>
									</td>
									<td >
									  <a onclick="processMerchantRefund('${transaction.transactionRefNo}','${loopCounter.count}')"><i class="fa fa-retweet fa-2x" id="refundreport${loopCounter.count}"></i> </a></td>
									</td>
								</tr>
							</c:forEach>
							</tbody>

						</table>
						<div id="successNotification" role="dialog" class="modal fade">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<center><h2>Transaction Details</h2></center>
									</div>
									<div id="tran_alert" class="modal-body">

										<center><b>IPay Transaction ID</b> : <span id="ipay_id"></span><br></center>
										<center><b>IPay Agent ID</b> :<span id="agent_id"></span><br></center>
										<center><b>Operator Ref</b> :<span id="opr_id"></span><br></center>
										<center><b>Account No.</b> :<span id="account_no"></span><br></center>
										<center><b>Service Provider Key</b> :<span id="sp_key"></span><br></center>
										<center><b>Amount</b> :<span id="trans_amt"></span><br></center>
										<center><b>Charged Amount</b> :<span id="charged_amt"></span><br></center>
										<center><b>Opening Balance</b> :<span id="opening_bal"></span><br></center>
										<center><b>Date & Time</b> :<span id="req_dt"></span><br></center>
										<center><b>Response Code</b> :<span id="res_code"></span><br></center>
										<center><b>Response Message</b> :<span id="res_msg"></span><br></center>
										<center><b>Status</b> :<span id="status"></span><br></center>
									</div>
								</div>
							</div>
						</div>
                      <!--   <div id="refundNotification" role="dialog" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>

                                    </div>
                                    <div id = "refund_body" class="modal-body">

                                    </div>
                                </div>
                            </div>
                        </div> -->
                      		<div class="modal fade" id="success_refundNotification" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
								<div class="modal-dialog">
									<!-- Modal content-->
									<div class="modal-content"
										style="margin-top: 40%; margin-left: 100px;">

										<div class="modal-body">
											<center>
												<img
													src="${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/source-(tick).gif"
													style="width: 140px; margin-top: -91px; margin-bottom: 10px;">
												<div
													style="background: white; margin-top: -20px; padding: 2.5% 2%;">
														<h4 class="refund_body"></h4><br>
													<center>
														<button type="button" class="btn" data-dismiss="modal" onclick="javascript:window.location.reload()">OK</button>
													</center>
												</div>
											</center>
										</div>

									</div>
								</div>
							</div>  
							
								<div class="modal fade" id="error_refundNotification" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
								<div class="modal-dialog">
									<!-- Modal content-->
									<div class="modal-content"
										style="margin-top: 40%; margin-left: 100px;">

										<div class="modal-body">
											<center>
												<img
													src="${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/x.gif"
													style="width: 140px; margin-top: -91px; margin-bottom: 10px;">
												<div
													style="background: white; margin-top: -20px; padding: 2.5% 2%;">
														<h4 class="refund_body"></h4><br>
													<center>
														<button type="button" class="btn" data-dismiss="modal" onclick="javascript:window.location.reload()">OK</button>
													</center>
												</div>
											</center>
										</div>

									</div>
								</div>
							</div>  
                        
                        


					</div>
				</div>

			</div>
		</div>
		<!-- END MAIN CONTENT -->
		<jsp:include page="/WEB-INF/jsp/SuperAdmin/Footer.jsp"/>
	</div>
	<!-- END MAIN -->
</div>
<!-- END WRAPPER -->
<!-- Javascript -->
<script src="<c:url value='/resources/super_admin/js/jquery/jquery-2.1.0.min.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/search.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/header.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/notifications.js'/>"></script>
<script src="<c:url value="/resources/js/datepicker.js"/>"></script>
<script>
	$(function() {
		$( "#toDate" ).datepicker({
			format:"yyyy-mm-dd"
		});
		$( "#fromDate" ).datepicker({
			format:"yyyy-mm-dd"
		});
	});
</script>

<script src="<c:url value='/resources/super_admin/js/bootstrap/bootstrap.min.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/plugins/jquery-slimscroll/jquery.slimscroll.min.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/plugins/jquery-easypiechart/jquery.easypiechart.min.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/plugins/chartist/chartist.min.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/vpqadmin.min.js'/>"></script>

<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.bootstrap.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.buttons.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/jszip.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/pdfmake.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/vfs_fonts.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.html5.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.print.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.fixedHeader.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.keyTable.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.responsive.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/responsive.bootstrap.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.scroller.min.js"></script>


<!-- pace -->
<script
		src="${pageContext.request.contextPath}/resources/admin/js/pace/pace.min.js"></script>
<script>
	var handleDataTableButtons = function() {
		"use strict";
		0 !== $("#datatable-buttons").length
		&& $("#datatable-buttons").DataTable({
			columnDefs: [
				{
					type: 'date-dd-mmm-yyyy',
					targets: 3
				}
			],
			dom : "Bfrtip",
			buttons : [ {
				extend : "copy",
				className : "btn-sm"
			}, {
				extend : "csv",
				className : "btn-sm"
			}, {
				extend : "excel",
				className : "btn-sm"
			}, {
				extend : "pdf",
				className : "btn-sm"
			}, {
				extend : "print",
				className : "btn-sm"
			} ],
			responsive : !0
		})
	}, TableManageButtons = function() {
		"use strict";
		return {
			init : function() {
				handleDataTableButtons()
			}
		}
	}();
</script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#datatable').dataTable();
		$('#datatable-keytable').DataTable({
			keys : true
		});
		$('#datatable-responsive').DataTable();
		$('#datatable-scroller').DataTable({
			ajax : "js/datatables/json/scroller-demo.json",
			deferRender : true,
			scrollY : 380,
			scrollCollapse : true,
			scroller : true
		});
		var table = $('#datatable-fixed-header').DataTable({
			fixedHeader : true
		});
	});
	TableManageButtons.init();
</script>

</body>

</html>