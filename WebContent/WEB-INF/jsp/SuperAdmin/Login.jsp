<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage=""%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>eCarib | Login</title>
<!-- Latest compiled and minified CSS -->
<link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'
	type="image/png" />
  <link rel='stylesheet prefetch' href='https://fonts.googleapis.com/icon?family=Material+Icons'>
<link rel='stylesheet prefetch' href='https://fonts.googleapis.com/css?family=Roboto:300'>

      <link href='<c:url value='/resources/css/Superadmin/style.css'/>'
	rel='stylesheet' type='text/css'>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<script type="text/javascript"
			src="${pageContext.request.contextPath}/resources/js/signup.js"></script>
	<style>
		.no-js #loader {
			display: none;
		}

		.js #loader {
			display: block;
			position: absolute;
			left: 100px;
			top: 0;
		}

		.se-pre-con {
			position: fixed;
			left: 0px;
			top: 0px;
			width: 100%;
			height: 100%;
			z-index: 9999;
			background: url(/images/pq_large.gif) center no-repeat #fff;
		}
		#loginsub{
			margin-right: 40%;
			background: white;
			padding: 10px 0px 12px 10px;
			margin-top: 8%;
		}

	</style>
	<script src="<c:url value='/resources/js/modernizr.js'/>"></script>

	<script type="text/javascript">
		$(window).load(function() {
			$(".se-pre-con").fadeOut("slow");
			start();
		});
	</script>

</head>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/header.js"></script>
<body>

<div class="se-pre-con"></div>

<main role="main">
            <c:if test="${error ne null}">
				<div class="alert alert-danger col-md-12" id="alertDanger">
				
					<center>
						<c:out value="${error}" escapeXml="true" default="" />
					</center>
				</div>
			</c:if>
			
			<c:if test="${msg ne null}">
				<div class="alert alert-success col-md-12" id="successAlert">
					<center>
						<c:out value="${msg}" escapeXml="true" default="" />
					</center>
				</div>
			</c:if>
			
  <div class="mediahawkz-login">
    <div class="div-left">
      <center><img src="${pageContext.request.contextPath}/resources/images/new_img/logo.png" class="img-responsive admin_logo"></center>
    </div>
    <div class="div-right">
      <div class="rkmd-form login">
        <h2 class="form-title">Login</h2>

	<form id="login"action="${pageContext.request.contextPath}/SuperAdmin/Home" method="post">
		 <div class="form-field">
            <label class="field-label" for="username">Username</label>
            <input  type="text" name="username" class="field-input" autocomplete="off">
            <i class="material-icons md-18">error_outline</i>
          </div>
          <div class="form-field">
            <label class="field-label" for="password">Password</label>
            <input id="pass" type="password" name="password" class="field-input" autocomplete="off"
				   maxlength="50">
            <i class="material-icons md-18">error_outline</i>
            <sec:csrfInput/>
          </div>
		<!-- <div class="group_1">
		<button type="submit" class="btn"
				style=" background: #ec2029; color: #FFFFFF; border-radius: 0">Submit</button>
	</div> -->
	 <div class="form-row clearfix">
            <div class="remember float-left">
              <input type="checkbox" name="rem" id="rem">
              <label for="rem">Stay Signed in</label>
            </div>
            <button id="submit" class="rkmd-btn btn-lightBlue ripple-effect float-right">LOGIN</button>
          </div>
          <div class="form-row clearfix">
            <!-- <a href="#"
			   data-toggle="modal" style="color: white;"
			   data-target="#forgotPassword">Forgot Password? </a> -->
          </div>
	</form>
</div>

 <div class="copyright">
        <p><img src="${pageContext.request.contextPath}/resources/images/new_img/mlogo.png" style="width: 5%;margin-bottom: -22px;"> &copy; <a href="http://www.msewa.com/">Msewa Software</a> Pvt Ltd. | 2017 All Rights Reserved.</p>
      </div>
    </div>
  </div>
</main>
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

    <script type="text/javascript"
			src="${pageContext.request.contextPath}/resources/js/SuperAdmin/index.js"></script>

</body>
	<script type="text/javascript">
    function successAlert(){
        $("#successAlert").fadeTo(2000, 500).slideUp(500, function(){
       $("#successAlert").slideUp(500);
        });   
       }
	     function dangerAlert(){
	         $("#alertDanger").fadeTo(2000, 500).slideUp(500, function(){
	        $("#alertDanger").slideUp(500);
	         });   
	     }
	     
		function start(){
	 		successAlert();
	 		dangerAlert();
		}
	
	</script>
</html>
