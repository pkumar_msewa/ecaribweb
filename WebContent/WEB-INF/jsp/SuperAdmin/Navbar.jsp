<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>	
		<nav class="navbar navbar-default">
				<div class="container-fluid">
					<div class="navbar-btn">
						<button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
					</div>
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-menu">
							<span class="sr-only">Toggle Navigation</span>
							<i class="fa fa-bars icon-nav"></i>
						</button>
					</div>
					<div id="navbar-menu" class="navbar-collapse collapse">
						<form class="navbar-form navbar-left hidden-xs" method="post" action="<c:url value="/SuperAdmin/SearchUser"/> ">
							<input type="text" name="mobile" id="search" autocomplete="off" value="" class="form-control" placeholder="Search by Mobile No.">
							<button type="submit"  class="btn btn-primary">Go</button></span><br>
								<div class="input-group">
									<p id="list"></p>
									<span class="input-group-btn"></span>
							</div>
						</form>
						<ul class="nav navbar-nav navbar-right">
							<li class="dropdown">
								<a href="#" class="dropdown-toggle icon-menu" data-toggle="dropdown">
									<i class="lnr lnr-alarm"></i>
									<span class="badge bg-danger">1</span>
								</a>
								<ul class="dropdown-menu notifications">


								</ul>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="<c:url value='/resources/images/favicon.png'/>" class="img-circle" alt="Avatar"> <span>SuperAdmin</span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>
								<ul class="dropdown-menu">
									<li><a href="<c:url value='/SuperAdmin/ChangePassword'/>"><i class="lnr lnr-cog"></i> <span>Change Password</span></a></li>
									<li><a href="<c:url value='/SuperAdmin/Logout'/>"><i class="lnr lnr-exit"></i> <span>Logout</span></a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</nav>

<style>
	#list{
		max-height: 150px;
		overflow-y: auto;
		position: absolute;
		width: 265px;
		background: white;
		z-index: 999;
		-webkit-box-shadow: 1px 16px 97px -23px rgba(0,0,0,0.54);
		-moz-box-shadow: 1px 16px 97px -23px rgba(0,0,0,0.54);
		box-shadow: 1px 16px 97px -23px rgba(0,0,0,0.54);
		}
		.input-group li{
			color: white;
			padding: 5px;
			}
</style>