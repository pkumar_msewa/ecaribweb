<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<!doctype html>
<html lang="en">
<head>
	<title>Generate Promo Code</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<sec:csrfMetaTags/>
	<!-- CSS -->
	<script src="<c:url value='/resources/js/jquery.js'/>"></script>
	<link rel="stylesheet" href="<c:url value='/resources/super_admin/css/bootstrap.min.css'/>">
	<link rel="stylesheet" href="<c:url value='/resources/super_admin/css/vendor/icon-sets.css'/>">
	<link rel="stylesheet" href="<c:url value='/resources/super_admin/css/main.min.css'/>">
	<link rel="stylesheet" href="<c:url value='/resources/super_admin/css/demo.css'/>">
	<link rel="stylesheet" href="<c:url value='/resources/css/font-awesome.min.css'/>">
	<link
			href="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.css"
			rel="stylesheet" type="text/css" />
	<link
			href="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.css"
			rel="stylesheet" type="text/css" />
	<link
			href="${pageContext.request.contextPath}/resources/admin/js/datatables/fixedHeader.bootstrap.min.css"
			rel="stylesheet" type="text/css" />
	<link
			href="${pageContext.request.contextPath}/resources/admin/js/datatables/responsive.bootstrap.min.css"
			rel="stylesheet" type="text/css" />
	<link
			href="${pageContext.request.contextPath}/resources/admin/js/datatables/scroller.bootstrap.min.css"
			rel="stylesheet" type="text/css" />

	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="<c:url value = '/resources/super_admin/img/apple-icon.png'/>">
	<link rel="icon" type="image/png" sizes="96x96" href="<c:url value = '/resources/images/favicon.png'/>">
	<script src="<c:url value='/resources/super_admin/js/jquery/jquery-2.1.0.min.js'/>"></script>
	<link rel="stylesheet" href="<c:url value="/resources/css/datepicker.css"/>">
	<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
	
</head>

<body>
<!-- WRAPPER -->
<div id="wrapper">
	<!-- SIDEBAR -->
	<jsp:include page="/WEB-INF/jsp/SuperAdmin/Sidebar.jsp"/>
	<!-- END SIDEBAR -->
	<!-- MAIN -->
	<div class="main">
		<!-- NAVBAR -->
		<jsp:include page="/WEB-INF/jsp/SuperAdmin/Navbar.jsp"/>
		<!-- END NAVBAR -->
		<!-- MAIN CONTENT -->
		<div class="main-content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-4"></div>
					<div class="col-md-4">
						<input type="hidden" value="${respCode}" id="respCode"/>
						<spring:form method="post" action="/SuperAdmin/GeneratePromoCode"
								modelAttribute="promoCodeRequest"
								class="form-horizontal form-label-left" id="promocodeform">
								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12">
										Promo Code </label>
									<div class="col-md-9 col-sm-9 col-xs-12">
										<spring:input path="promoCode" class="form-control form-ds1" name="number" id="number"
										placeholder="Promo Code" maxlength="6"/>
										<p class="error">${error.promoCode}</p>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12">Terms
										& Conditions <span class="required">*</span>
									</label>
									<div class="col-md-9 col-sm-9 col-xs-12">
										<spring:input path="terms" class="form-control form-ds1" placeholder='Min Amount for Transaction'/>
										<p class="error">${error.terms}</p>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12">Start
										Date <span class="required">*</span>
									</label>
									<div class="col-md-9 col-sm-9 col-xs-12">
										<spring:input path="startDate" readonly="true" class="form-control form-ds1"/>
									<p class="error">${error.startDate}</p>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12">End
										Date <span class="required">*</span>
									</label>
									<div class="col-md-9 col-sm-9 col-xs-12">
										<spring:input path="endDate" readonly="true" class="form-control form-ds1"/>
										<p class="error">${error.endDate}</p>
									</div>
									<sec:csrfInput/>
								</div>

								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12">Description <span class="required">*</span>
									</label>
									<div class="col-md-9 col-sm-9 col-xs-12">
										<spring:input path="description" class="form-control form-ds1"/>
										<p class="error">${error.description}</p>
									</div>
									<sec:csrfInput/>
								</div>


								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12">Fixed
										<span class="required">*</span>
									</label>
									<div class="col-md-9 col-sm-9 col-xs-12">
										<spring:checkbox path="fixed" class="form-control form-ds1"
													  />
										<p class="error">${error.value}</p>
									</div>
								</div>


								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12">Amount
										 <span class="required">*</span>
									</label>
									<div class="col-md-9 col-sm-9 col-xs-12">
										<spring:input path="value" id="value" class="form-control form-ds1" rows="3"
											 placeholder='Amount'/>
										<p class="error">${error.value}</p>
									</div>
								</div>
								
									<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12">Upto CashBack
										 <span class="required">*</span>
									</label>
									<div class="col-md-9 col-sm-9 col-xs-12">
										<spring:input path="cashBackValue" id="cashBackValue" class="form-control form-ds1" rows="3"
											 placeholder='UptoCashBack'/>
										<p class="error">${error.cashBackValue}</p>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12">
										Service Type<span class="required">*</span>
									</label>
									<div class="col-md-9 col-sm-9 col-xs-12">
									<select id="serviceType" name="serviceType" class="form-control" style="width:249px; height:40px;">
									</select>
									<p class="error" id="serviceType_error" >${error.serviceType}</p><br>
									
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12">
										Services<span class="required">*</span>
									</label>
									<div class="col-md-9 col-sm-9 col-xs-12">
									<select class="js-example-basic-multiple"  multiple="multiple"  id="services" name="services" class="form-control" style="width:333px; height:60px;">
									</select>
									<p class="error" id="service_error"></p><br>
									</div>
								</div>
								<div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
									<button type="submit" class="btn btn-success btn-ds1" id="submitForm">Submit</button>
								</div>
							</spring:form>
					</div>
					<div class="col-md-4"></div>
				</div>

			</div>
		</div>
		<!-- END MAIN CONTENT -->
		<jsp:include page="/WEB-INF/jsp/SuperAdmin/Footer.jsp"/>
	</div>
	<!-- END MAIN -->
</div>
<!-- END WRAPPER -->

      	<div class="modal fade" id="respMessage" role="dialog" data-backdrop="static" data-keyboard="false">
								<div class="modal-dialog">
									<!-- Modal content-->
		 							<div class="modal-content"
										style="margin-top: 40%; margin-left: 100px;">

										<div class="modal-body">
											<center>
												<img
													src="${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/source-(tick).gif"
													style="width: 140px; margin-top: -91px; margin-bottom: 10px;">
												<div
													style="background: white; margin-top: -20px; padding: 2.5% 2%;">
														<h4>${resp.message}</h4>
													<center>
														<button type="button" class="btn" data-dismiss="modal">OK</button>
													</center>
												</div>
											</center>
										</div>

									</div>
								</div>
							</div> 
 
<div id="pay_loader" role="dialog" class="modal fade" data-backdrop="static" data-keyboard="false">
 <div class="modal-dialog">
   <div class="modal-body">
    <center style="width:20%;margin-top: 150px; margin-left:300px;">
     <span><h4 style="color:green;font-weight:1000;">Please Wait for 2 Min...</h4><img src="/resources/images/spinner.gif"></span>
    </center>
   </div>
 </div>
</div>

	<div id="custom_notifications" class="custom-notifications dsp_none">
		<ul class="list-unstyled notifications clearfix"
			data-tabbed_notifications="notif-group">
		</ul>
		<div class="clearfix"></div>
		<div id="notif-group" class="tabbed_notifications"></div>
	</div>
	<script>
	
		$('#terms').keyup(function () { 
	    	this.value = this.value.replace(/[^0-9\.]/g,'');
		});
		
		$('#value').keyup(function () { 
	    	this.value = this.value.replace(/[^0-9\.]/g,'');
		});
		
		$("#serviceType").on("blur",function(){
			var valid = true;
			 var serviceTypeId = $("#serviceType").val();
			 if(serviceTypeId.includes("#")){
				 $("#serviceType_error").html("Select service Type");
				 valid=false;
			 }
			 else{
				 $("#serviceType_error").html(" ");
				 $('#services').children('option').remove();
				 $("#services").empty();
				 $("#services").val(null).trigger("change");
				  $.ajax({
					type:"post",
					contentType:"application/json",
					url:"/SuperAdmin/GetServices",
					dataType:'json',
					data:JSON.stringify({
						"serviceId":serviceTypeId
					}),
					success:function(response){
						if(response.code.includes("S00")){
						 for(var i=0;i<response.serviceList.length;i++){
                          $("#services").append('<option value=" '+ response.serviceList[i].code+'">'+response.serviceList[i].description+'</option>')
						   }
						}
					}
					 
				  }); 
			 }
			 
		});
		
		$("#services").on("blur",function(){
			var serviceTypeId = $("#serviceType").val();
			var service = $("#services option:selected").val();
			 if(serviceTypeId.includes("#")||service.length<=0){
				 $("#serviceType_error").html("Select serviceType");
			 }
			 else  if(service.includes("#")){
				 $("#service_error").html("Select services");
			 }
			 else{
				 $("#service_error").html("");
			 }
		});
		
	</script>
<!-- Javascript -->
<script src="<c:url value='/resources/super_admin/js/jquery/jquery-2.1.0.min.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/search.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/header.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/notifications.js'/>"></script>

<script src="<c:url value="/resources/js/datepicker.js"/>"></script>
<script>
	$(function() {
		$( "#startDate" ).datepicker({
			format:"yyyy-mm-dd"
		});
		$( "#endDate" ).datepicker({
			format:"yyyy-mm-dd"
		});
	});
</script>
<script src="<c:url value='/resources/super_admin/js/bootstrap/bootstrap.min.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/plugins/jquery-slimscroll/jquery.slimscroll.min.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/plugins/jquery-easypiechart/jquery.easypiechart.min.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/plugins/chartist/chartist.min.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/vpqadmin.min.js'/>"></script>

<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.bootstrap.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.buttons.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/jszip.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/pdfmake.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/vfs_fonts.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.html5.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.print.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.fixedHeader.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.keyTable.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.responsive.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/responsive.bootstrap.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.scroller.min.js"></script>


<!-- pace -->
<script
		src="${pageContext.request.contextPath}/resources/admin/js/pace/pace.min.js"></script>
		
        <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
<script>
	var handleDataTableButtons = function() {
		"use strict";
		0 !== $("#datatable-buttons").length
		&& $("#datatable-buttons").DataTable({
			columnDefs: [
				{
					type: 'date-dd-mmm-yyyy',
					targets: 3
				}
			],
			dom : "Bfrtip",
			buttons : [ {
				extend : "copy",
				className : "btn-sm"
			}, {
				extend : "csv",
				className : "btn-sm"
			}, {
				extend : "excel",
				className : "btn-sm"
			}, {
				extend : "pdf",
				className : "btn-sm"
			}, {
				extend : "print",
				className : "btn-sm"
			} ],
			responsive : !0
		})
	}, TableManageButtons = function() {
		"use strict";
		return {
			init : function() {
				handleDataTableButtons()
			}
		}
	}();
</script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#datatable').dataTable();
		$('#datatable-keytable').DataTable({
			keys : true
		});
		$('#datatable-responsive').DataTable();
		$('#datatable-scroller').DataTable({
			ajax : "js/datatables/json/scroller-demo.json",
			deferRender : true,
			scrollY : 380,
			scrollCollapse : true,
			scroller : true
		});
		var table = $('#datatable-fixed-header').DataTable({
			fixedHeader : true
		});
	});
	TableManageButtons.init();
</script>

<script type="text/javascript">
	    $(document).ready(function() {
	    $("#services").select2();
	   var respcode = $("#respCode").val();
	    if(respcode.includes("S00")||respcode.includes("F00")){
	 	  $("#respMessage").modal("show");
	 	  $("#respCode").val("");
	    }
	    $('#services').children('option').remove();
	    $("#serviceType").append('<option value="#">Select serviceType</option>');
	    $.ajax({
			type:"post",
			contentType:"application/json",
			url:"/SuperAdmin/GetServiceType",
			dataType:'json',
			data:JSON.stringify({
			}),
			success:function(response){
				if(response.code.includes("S00")){
				 for(var i=0;i<response.serviceDTOs.length;i++){
                  $("#serviceType").append('<option value=" '+ response.serviceDTOs[i].id+'">'+response.serviceDTOs[i].description+'</option>')
				   }
				  }
			    }
			});
	    });

	</script>

</body>

</html>
