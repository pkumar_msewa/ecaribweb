<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!doctype html>
<html lang="en">
<head>
	<title>All BusList</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<sec:csrfMetaTags/>
	<!-- CSS -->
	<script src="<c:url value='/resources/js/jquery.js'/>"></script>
	<link rel="stylesheet" href="<c:url value='/resources/super_admin/css/bootstrap.min.css'/>">
	<link rel="stylesheet" href="<c:url value='/resources/super_admin/css/vendor/icon-sets.css'/>">
	<link rel="stylesheet" href="<c:url value='/resources/super_admin/css/main.min.css'/>">
	<link rel="stylesheet" href="<c:url value='/resources/super_admin/css/demo.css'/>">
	<link rel="stylesheet" href="<c:url value='/resources/css/font-awesome.min.css'/>">
	<link
			href="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.css"
			rel="stylesheet" type="text/css" />
	<link
			href="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.css"
			rel="stylesheet" type="text/css" />
	<link
			href="${pageContext.request.contextPath}/resources/admin/js/datatables/fixedHeader.bootstrap.min.css"
			rel="stylesheet" type="text/css" />
	<link
			href="${pageContext.request.contextPath}/resources/admin/js/datatables/responsive.bootstrap.min.css"
			rel="stylesheet" type="text/css" />
	<link
			href="${pageContext.request.contextPath}/resources/admin/js/datatables/scroller.bootstrap.min.css"
			rel="stylesheet" type="text/css" />

	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="<c:url value = '/resources/super_admin/img/apple-icon.png'/>">
	<link rel="icon" type="image/png" sizes="96x96" href="<c:url value = '/resources/images/favicon.png'/>">
	<script src="<c:url value='/resources/js/jquery.js'/>"></script>
	<link rel="stylesheet" href="<c:url value="/resources/css/datepicker.css"/>">
</head>

<body>
<!-- WRAPPER -->
<div id="wrapper">
	<!-- SIDEBAR -->
	<jsp:include page="/WEB-INF/jsp/SuperAdmin/Sidebar.jsp"/>
	<!-- END SIDEBAR -->
	<!-- MAIN -->
	<div class="main">
		<!-- NAVBAR -->
		<jsp:include page="/WEB-INF/jsp/SuperAdmin/Navbar.jsp"/>
		<!-- END NAVBAR -->
		<!-- MAIN CONTENT -->
		<div class="main-content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">
						<b>${msg}</b><br/>
									  <form action="<c:url value="/SuperAdmin/BusDeatilsListByDate"/>" method="get" class="form form-inline">
										  From <input type="text" id="fromDate" name="startDate" class="form-control" readonly/>
										  To <input type="text" id="toDate" name="endDate" class="form-control" readonly/>
										  <sec:csrfInput/>
										  <button type="submit" class="btn btn-primary"><i class="fa fa-filter" aria-hidden="true"></i></button>
									  </form>
                				    </p>
									<table id="datatable-buttons"
							              class="table table-striped table-bordered">
										<thead>
											<tr>
												<th>S.No</th>
												<th>User Details</th>
												<th>User Contact Details</th>
												<th>Transaction ID</th>
												<th>Transaction Date</th>
												<th>Ticket PNR No.</th>
												<th>Operator PNR No.</th>
												<th>Booking Status</th>
												<th>Transaction Status</th>
												<th>Source</th>
												<th>Destination</th>
												<th>Account Details</th>
												<th>Transaction Amount</th>
												<th>Commission</th>
												<th>Ticket Details</th>
												<th>Ticket PDF</th>
											</tr>
										<tbody id=userList>
											<c:forEach items="${BusDetails}" var="slist"
												varStatus="loopCount">
												<tr>
													<td>${loopCount.count}</td>
													<td>
													<c:out value="${slist.userDetail.firstName} ${slist.userDetail.lastName}"></c:out></td>
														<td><a href="${pageContext.request.contextPath}/SuperAdmin/${slist.userDetail.userName}/Info">
													<c:out value="${slist.userDetail.username}"></c:out></a></td>
													<td>${slist.transactionRefNo}</td>
													<td>${slist.txnDate}</td>
													<td><c:choose>
													<c:when test="${slist.ticketPnr != 'null' }">
													${slist.ticketPnr}
													</c:when>
													<c:otherwise>
													  NA
													</c:otherwise>
													</c:choose></td>
													<td><c:choose>
													<c:when test="${slist.operatorPnr != 'null' }">
													 ${slist.operatorPnr}
													</c:when>
													<c:otherwise>
													  NA
													</c:otherwise>
													</c:choose></td>
													<td> ${slist.status}</td> 
													<td>${slist.txnStatus}</td>
													<td>${slist.source}</td>
													<td>${slist.destination}</td>
													<td><b>Acc No:</b>${slist.userDetail.accountNumber}</td>
													<td><b>Total Txn Amount: </b>${slist.totalFare}<br></td>
													<td>${slist.commsissionAmount}</td>
													<td>
													<button id="view_ticket" type="button" style="background-color: #5cb85c; color: #fff;" onclick="viewTicketDetails('${slist.emtTxnId}')">View</button>
													</td>
															
													<td>
													<c:choose>
													<c:when test="${slist.status== 'Booked'}">
													<form action="${pageContext.request.contextPath}/SuperAdmin/singleTicketPdfForBus" target="new"  method="post">
													<input type="hidden" id="emtTxnId" name="emtTxnId" value="${slist.emtTxnId}"/>
													<button id="Pdf_ticket${loopCount.count}"  type="submit"  style="background-color: #5cb85c; color: #fff;">Download</button>
													</form>	
													</c:when>
													<c:otherwise>
														NA
													</c:otherwise>
													</c:choose>
													</td>
													
											</c:forEach>
										</tbody>
									</table>
									
					</div>
				</div>

			</div>
		</div>
		<!-- END MAIN CONTENT -->
		<jsp:include page="/WEB-INF/jsp/SuperAdmin/Footer.jsp"/>
	</div>
	<!-- END MAIN -->
</div>

<div id="custom_notifications" class="custom-notifications dsp_none">
		<ul class="list-unstyled notifications clearfix"
			data-tabbed_notifications="notif-group">
		</ul>
		<div class="clearfix"></div>
		<div id="notif-group" class="tabbed_notifications"></div>
	</div>




	<!-- ========================= Modal For Traveler Details ==================================== -->
	<div id="travellerDetails" class="modal fade" role="dialog"
		hidden="hidden">
		<div class="modal-dialog modal-lg">
			<!-- Modal content-->
			<div class="modal-content">

				<div class="modal-body">
					<button type="button" class="close close2" data-dismiss="modal"
						id="clsesits">&times;</button>
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<img
								src="${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/Bus.png"
								style="width: 4%;"> <span class="modal-title bookinghead"
								id="m_bus_nm"></span> <br> &nbsp;&nbsp;<b>Journey Date :</b><span
								id="m_bus_doj"> </span> <span id="m_bus_dept">
								</span> &nbsp<span id="m_bus_arr"></span> &nbsp<b>Source :</b><span id="src"></span> &nbsp<b>Destination :</b><span id="dest"></span>

						</div>
					</div>
					<hr style="margin-top: 5px;">

					<br>

					<div class="row">
						<div class="row">
							<div class="col-md-12">
								<!-- TABLE HOVER -->
								<!-- page content -->
								<table id="editedtable" class="table table-striped ">
									<tr id="xyz">
										<th>SL.NO</th>
										<th>First Name</th>
										<th>Last Name</th>
										<th>Age</th>
										<th>Gender</th>
										<th>Seat No.</th>
										<th>Fare</th>
									</tr>
								</table>
								<nav>
									<ul class="pagination" id="paginationn">
									</ul>
								</nav>
								<!-- End Page Content -->
								<!-- END TABLE HOVER -->

								<span style="color: red; padding-left: 696px;">Refunded Amount: </span><span
										id="refAmt"></span><br>
										<span style="color: red; padding-left: 696px;">Cancellation  Charge: </span><span
										id="cnclAmt"></span>
										
								<center>
									<br><input type="hidden" id="txnRefNo">
									<button type="button" id="cnclTckt"  style="background-color: #d43f3a; border-color: #d43f3a;color: #FFFFFF;" hidden="hidden">Cancel Ticket</button>
								<p style="color: #d43f3a; font-size: large;" id="cncl_msg" hidden="hidden">Ticket is Cancelled.</p>
								</center>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>


	<!-- End of Modal for Cancel Ticket Confirmation -->
	<div id="cancel_confirm" class="modal fade" role="dialog" hidden="hidden">
	  <div class="modal-dialog">

	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
	        <center><h3 class="modal-title">Confirmation!</h3></center>
	      </div>
	      <input type="hidden" name="prodId"  class="form-control input-sm" id="rejProdId" required="required" />
	      <div class="modal-body">
	        <div class="row">
	        	<center><img src="${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/delete.png" class="img-responsive" style="width: 20%;"></center>
	        </div>
	        <div class="row">
	        	<center><h2>Are you sure!</h2></center>
	        </div>
	        <div class="row">
	        	<center><p>Are you sure that you want to cancel this ticket?</p></center>
	        </div>
	        <br>
	        <div class="row">
	        	<div class="col-md-6">
	        		<button class="btn btn-success" data-dismiss="modal" style="float: right;">No</button>
	        	</div>
	        	<div class="col-md-6">
	        		<button class="btn btn-danger" id="ticketConfCancel">Yes, Cancel it!</button>
	        	</div>
	        </div>
	      </div>
	    </div>

	  </div>
	</div>

<div class="container">  
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
       
        <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" id="close3">&times;</button>
          <center>
        <div style="background: white; padding: 40px;">
          <h3 id="cnclMsgVal"><b><br><small></b></small></h3>
        </div></center>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- END WRAPPER -->
<!-- Javascript -->
<script src="<c:url value='/resources/super_admin/js/jquery/jquery-2.1.0.min.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/search.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/header.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/notifications.js'/>"></script>
<script src="<c:url value="/resources/js/datepicker.js"/>"></script>
<script>
	$(function() {
		$( "#toDate" ).datepicker({
			format:"yyyy-mm-dd"
		});
		$( "#fromDate" ).datepicker({
			format:"yyyy-mm-dd"
		});
	});
</script>

<script src="<c:url value='/resources/super_admin/js/bootstrap/bootstrap.min.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/plugins/jquery-slimscroll/jquery.slimscroll.min.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/plugins/jquery-easypiechart/jquery.easypiechart.min.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/plugins/chartist/chartist.min.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/vpqadmin.min.js'/>"></script>

<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.bootstrap.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.buttons.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/jszip.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/pdfmake.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/vfs_fonts.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.html5.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.print.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.fixedHeader.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.keyTable.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.responsive.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/responsive.bootstrap.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.scroller.min.js"></script>


<!-- pace -->
<script
		src="${pageContext.request.contextPath}/resources/admin/js/pace/pace.min.js"></script>
<script>
	var handleDataTableButtons = function() {
		"use strict";
		0 !== $("#datatable-buttons").length
		&& $("#datatable-buttons").DataTable({
			columnDefs: [
				{
					type: 'date-dd-mmm-yyyy',
					targets: 3
				}
			],
			dom : "Bfrtip",
			buttons : [ {
				extend : "copy",
				className : "btn-sm"
			}, {
				extend : "csv",
				className : "btn-sm"
			}, {
				extend : "excel",
				className : "btn-sm"
			}, {
				extend : "pdf",
				className : "btn-sm"
			}, {
				extend : "print",
				className : "btn-sm"
			} ],
			responsive : !0
		})
	}, TableManageButtons = function() {
		"use strict";
		return {
			init : function() {
				handleDataTableButtons()
			}
		}
	}();
</script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#datatable').dataTable();
		$('#datatable-keytable').DataTable({
			keys : true
		});
		$('#datatable-responsive').DataTable();
		$('#datatable-scroller').DataTable({
			ajax : "js/datatables/json/scroller-demo.json",
			deferRender : true,
			scrollY : 380,
			scrollCollapse : true,
			scroller : true
		});
		var table = $('#datatable-fixed-header').DataTable({
			fixedHeader : true
		});
	});
	TableManageButtons.init();
</script>

<script type="text/javascript">
		$(document).ready(function() {

			$('#datatable-keytable').DataTable({
				keys : true
			});
			$('#datatable-responsive').DataTable();
			$('#datatable-scroller').DataTable({
				ajax : "js/datatables/json/scroller-demo.json",
				deferRender : true,
				scrollY : 380,
				scrollCollapse : true,
				scroller : true
			});
			var table = $('#datatable-fixed-header').DataTable({
				fixedHeader : true
			});
			
			$("#cnclTckt").click(function () {
			$("#cancel_confirm").modal('show');
			});
			
			
			$("#ticketConfCancel").click(function () {
				var txnNo=$('#txnRefNo').val();
				var contextPath = "${pageContext.request.contextPath}";
		    	var spinnerUrl = "Please wait <img src="+contextPath+"'/resources/images/spinner.gif' style='width:20px; hight:10px;'>"
		    	$("#ticketConfCancel").addClass("disabled");
		    	$("#ticketConfCancel").html(spinnerUrl);
			   	 $.ajax({
			   	       type : "POST",
			   	        contentType : "application/json",
			   	        url : "${pageContext.request.contextPath}/SuperAdmin/cancelBookedTicket",
			   			dataType : 'json',
			   			data :JSON.stringify({
			   				"vPqTxnId":txnNo,
			   			}),
			   	       success : function(response) {
			   	       console.log("Response :: "+response.code);
			   	       if (response.code.includes("S00")) {
			   	    	$("#cnclMsgVal").html('Ticket Cancel Successfully');
					}else{
						$("#cnclMsgVal").html('Unable To Cancel Ticket');
					}
			   	  	    $("#cancel_confirm").modal('hide');
			   	 		$("#travellerDetails").modal('hide'); 
			   	    	$("#myModal").modal('show');
			   	    	$("#myModal").on('hidden.bs.modal', function ()  {
							 location.reload();		
						});	
			   	       } 
			   	   }); 
			});
			
		});
		TableManageButtons.init();
	</script>


	<script type="text/javascript">
    function viewTicketDetails(emtTxnId) {
    	$("#axz").show();
    	 $("#cncl_msg").hide();
    	 $("#cnclTckt").hide();
    	$('#editedtable').html('<tr class="testingg"><th>SL.NO</th><th>First Name</th><th>Last Name</th><th>Age</th><th>Gender</th><th>Seat No.</th><th>Fare</th></tr>');
    	var contextPath = "${pageContext.request.contextPath}";
    	var spinnerUrl = "Please wait <img src="+contextPath+"'/resources/images/spinner.gif' style='width:20px; hight:10px;'>"
    	$("#view_ticket").addClass("disabled");
    	$("#view_ticket").html(spinnerUrl);
     	 $.ajax({
   	       type : "POST",
   	        contentType : "application/json",
   	        url : "${pageContext.request.contextPath}/SuperAdmin/getSingleTicketDetails",
   			dataType : 'json',
   			data :JSON.stringify({
   				"emtTxnId" : emtTxnId,
   			}),
   	       success : function(response) {
   	    	$(".se-pre-con").fadeOut("slow");
   	       console.log("Response :: "+response.code);
   	       
   	       if (response.code.includes("S00")) {
		
   	  	   console.log("Extra Info :: "+response.extraInfo.busOperator);
   	  	   var trHTML='';
   	  	   
   	       for (var i = 0; i < response.details.length; i++) {
   	    	var trHTML='';
   	    	trHTML += '<tr class="testingg"><td>'+(i+1)+'</td><td>' + response.details[i].fName + '</td><td>' + response.details[i].lName + '</td><td>' + response.details[i].age + '</td><td>' + response.details[i].gender + '</td><td>'+ response.details[i].seatNo +'</td><td>' + response.details[i].fare + '</td></tr>';
   	    	$('#editedtable').append(trHTML);
   	    	console.log("refAmount: "+response.refAmt);
   	    
   	    	$('#txnRefNo').val(response.transactionRefNo);
   	    	
		  }
   	       if ( response.extraInfo.status.includes("Booked")) {
   	    	$('#refAmt').html(response.refAmt);
   	    	$('#cnclAmt').html(response.cancellationCharges);
   	    	console.log("isCancelable: "+response.cancelable);
   	    	if (response.cancelable==true) {
   	    		$("#cnclTckt").show();
			}
		}
   	       else if( response.extraInfo.status.includes("Cancelled")) {
   	    	 $("#cncl_msg").show();
		}
   	 	    $("#m_bus_nm").html(response.extraInfo.busOperator);
   	 	    $("#src").html(response.extraInfo.source);
   	 	    $("#dest").html(response.extraInfo.destination);
   	 	    $("#m_bus_doj").html("Date: "+response.extraInfo.journeyDate);	
   	 	    $("#m_bus_dept").html("Departure:"+response.extraInfo.deptTime);
   	 		$("#m_bus_arr").html("Arrival:"+response.extraInfo.arrTime);
   	 		$("#view_ticket").addClass("removeClass");
    		$("#view_ticket").html("View");
   	 	    $("#travellerDetails").modal('show'); 
   	       }
   	       else{
   	    	$("#cnclMsgVal").html('Ticket Details not found');
   	    	$("#myModal").modal('show');
//    	    	location.reload();	 
   	       }
   	 	   
   	       } 
   	   }); 
      }
    
	</script>


<script type="text/javascript">
    function openPdf(emtTxnId) {
    	$("#axz").show();
    	
    	var contextPath = "${pageContext.request.contextPath}";
    
     	 $.ajax({
   	       type : "POST",
   	        contentType : "application/json",
   	        url : "${pageContext.request.contextPath}/SuperAdmin/singleTicketPdfForBus",
   			dataType : 'json',
   			data :JSON.stringify({
   				"emtTxnId" : emtTxnId,
   			}),
   	       success : function(response) {
   	    	$(".se-pre-con").fadeOut("slow");
   	       console.log("Response :: "+response.code);

   	       if (response.code.includes("F03")) {
			
   	    	window.location.href='${pageContext.request.contextPath}/SuperAdmin/Home';
		}
   	       
   	       
   	       } 
   	   }); 
      }
    function pdfSpinner(val)
    {
    	var contextPath = "${pageContext.request.contextPath}";
    	var spinnerUrl = "Please wait <img src="+contextPath+"'/resources/images/spinner.gif' style='width:20px; hight:10px;'>"
    	
    	$('#'+val).append(spinnerUrl);
//     	$('#'+val).addClass("dissable");
    	
    }
	</script>

</body>

</html>
