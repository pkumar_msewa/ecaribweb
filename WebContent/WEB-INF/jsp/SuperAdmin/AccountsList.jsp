<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!doctype html>
<html lang="en">
<head>
	<title>Account List</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<sec:csrfMetaTags/>
	<!-- CSS -->
	<script src="<c:url value='/resources/js/jquery.js'/>"></script>
	<link rel="stylesheet" href="<c:url value='/resources/super_admin/css/bootstrap.min.css'/>">
	<link rel="stylesheet" href="<c:url value='/resources/super_admin/css/vendor/icon-sets.css'/>">
	<link rel="stylesheet" href="<c:url value='/resources/super_admin/css/main.min.css'/>">
	<link rel="stylesheet" href="<c:url value='/resources/super_admin/css/demo.css'/>">
	<link rel="stylesheet" href="<c:url value='/resources/css/font-awesome.min.css'/>">
	<link
			href="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.css"
			rel="stylesheet" type="text/css" />
	<link
			href="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.css"
			rel="stylesheet" type="text/css" />
	<link
			href="${pageContext.request.contextPath}/resources/admin/js/datatables/fixedHeader.bootstrap.min.css"
			rel="stylesheet" type="text/css" />
	<link
			href="${pageContext.request.contextPath}/resources/admin/js/datatables/responsive.bootstrap.min.css"
			rel="stylesheet" type="text/css" />
	<link
			href="${pageContext.request.contextPath}/resources/admin/js/datatables/scroller.bootstrap.min.css"
			rel="stylesheet" type="text/css" />

	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="<c:url value = '/resources/super_admin/img/apple-icon.png'/>">
	<link rel="icon" type="image/png" sizes="96x96" href="<c:url value = '/resources/images/favicon.png'/>">
	<script src="<c:url value='/resources/super_admin/js/jquery/jquery-2.1.0.min.js'/>"></script>
	<link rel="stylesheet" href="<c:url value="/resources/css/datepicker.css"/>">
	<script src="<c:url value="/resources/js/datepicker.js"/>"></script>
	<script>
		$(function() {
			$(".update-limit").hide();
			$(".server-load").hide();
			$(".numeric").keydown(function(event){
				if(event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || (event.keyCode == 65 && event.ctrlKey == true) || (event.keyCode >= 35 && event.keyCode<=39)){
					return;
				}else{
					if(event.shiftKey || (event.keyCode < 48 || event.keyCode >57) && (event.keyCode < 96 || event.keyCode > 105)) {
						event.preventDefault();
					}
				}
			});
			$(".edit-limit").click(function(){
				var code = $(this).attr("id");
				$(this).hide();
				$("#upload-"+code).show();
				$("#tlimit-"+code).removeAttr("readonly");
				$("#dlimit-"+code).removeAttr("readonly");
				$("#mlimit-"+code).removeAttr("readonly");
				$("#blimit-"+code).removeAttr("readonly");

			});

			$(".update-limit").click(function(){
				$("#res_msg").html("");
				$(this).hide();
			console.log("update limit");
			var csrfHeader = $("meta[name='_csrf_header']").attr("content");
			var csrfToken = $("meta[name='_csrf']").attr("content");
			var headers = {};
			headers[csrfHeader] = csrfToken;
			var arr = $(this).attr("id").split("-")
				var code = arr[1];
			var tLimit = $("#tlimit-" + code).val();
			var dLimit = $("#dlimit-" + code).val();
			var mLimit = $("#mlimit-" + code).val();
			var bLimit = $("#blimit-" + code).val();
				$("#spin-"+code).show();

			$.ajax({
				type: "POST",
				headers: headers,
				contentType: "application/x-www-form-urlencoded",
				url: "/SuperAdmin/EditLimit",
				data: {
					code: code,
					monthlyLimit: mLimit,
					dailyLimit: dLimit,
					transactionLimit: tLimit,
					balanceLimit: bLimit
				},
				success: function (response) {

					console.log(response);
					if(response.success) {
						$("#res_msg").html(response.message);
						$("#tlimit-"+code).attr("readonly","readonly");
						$("#dlimit-"+code).attr("readonly","readonly");
						$("#mlimit-"+code).attr("readonly","readonly");
						$("#blimit-"+code).attr("readonly","readonly");
						$("#tlimit-" + code).val(response.transactionLimit);
						$("#dlimit-" + code).val(response.dailyLimit);
						$("#mlimit-" + code).val(response.monthlyLimit);
						$("#blimit-" + code).val(response.balanceLimit);
						$("#spin-"+code).hide();
						$("#upload-"+code).hide();
						$("#"+code).show();
					}
				}
			});
			});

		});
	</script>
</head>

<body>
<!-- WRAPPER -->
<div id="wrapper">
	<!-- SIDEBAR -->
	<jsp:include page="/WEB-INF/jsp/SuperAdmin/Sidebar.jsp"/>
	<!-- END SIDEBAR -->
	<!-- MAIN -->
	<div class="main">
		<!-- NAVBAR -->
		<jsp:include page="/WEB-INF/jsp/SuperAdmin/Navbar.jsp"/>
		<!-- END NAVBAR -->
		<!-- MAIN CONTENT -->
		<div class="main-content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">
						<h5><span class="label label-info" id="res_msg"></span></h5><br/>

						<table id="datatable-buttons"
							   class="table table-condensed">
							<thead>
							<tr>
								<th>S.No</th>
								<th>Name</th>
								<th>Transaction Limit</th>
								<th>Daily Limit</th>
								<th>Monthly Limit</th>
								<th>Balance Limit</th>
								<th></th>
								<th></th>
								<th></th>
							</tr>
							</thead>
							<tbody id="userList">
							<c:forEach items="${list}" var="account"
									   varStatus="loopCounter">
								<tr>
									<td>${loopCounter.count}</td>
									<td> <c:out value="${account.name}" default="" escapeXml="true" />
									</td>
									<td>
										<input type="number" id="tlimit-${account.code}" class="tlimit numeric" value="${account.transactionLimit}" readonly="readonly"/>

									</td>
									<td>
										<input type="number" id="dlimit-${account.code}" class="dlimit numeric" value="${account.dailyLimit}" readonly="readonly"/>

									</td>
									<td>
										<input type="number" id="mlimit-${account.code}" class="mlimit numeric" value="${account.monthlyLimit}" readonly="readonly"/>

									</td>
									<td>
									
									    <input type="number" id="blimit-${account.code}" class="blimit numeric" value="${account.balanceLimit}" readonly="readonly"/>

									</td>
									
									<td><i class="fa fa-pencil-square-o edit-limit" id="${account.code}" aria-hidden="true"></i></td>
									
									<td><i class="fa fa-upload update-limit" id="upload-${account.code}" aria-hidden="true"></i></td>
									
									<td><i class="fa fa-refresh fa-spin server-load" id="spin-${account.code}" aria-hidden="true"></i></td>

								</tr>
							</c:forEach>
							</tbody>

						</table>


                    </div>
				</div>

			</div>
		</div>
		<!-- END MAIN CONTENT -->
		<jsp:include page="/WEB-INF/jsp/SuperAdmin/Footer.jsp"/>
	</div>
	<!-- END MAIN -->
</div>
<!-- END WRAPPER -->
<!-- Javascript -->
<script src="<c:url value='/resources/super_admin/js/jquery/jquery-2.1.0.min.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/search.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/header.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/notifications.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/bootstrap/bootstrap.min.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/plugins/jquery-slimscroll/jquery.slimscroll.min.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/plugins/jquery-easypiechart/jquery.easypiechart.min.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/plugins/chartist/chartist.min.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/vpqadmin.min.js'/>"></script>

<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.bootstrap.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.buttons.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/jszip.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/pdfmake.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/vfs_fonts.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.html5.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.print.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.fixedHeader.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.keyTable.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.responsive.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/responsive.bootstrap.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.scroller.min.js"></script>


<!-- pace -->
<script
		src="${pageContext.request.contextPath}/resources/admin/js/pace/pace.min.js"></script>
<script>
	var handleDataTableButtons = function() {
		"use strict";
		0 !== $("#datatable-buttons").length
		&& $("#datatable-buttons").DataTable({
			columnDefs: [
				{
					type: 'date-dd-mmm-yyyy',
					targets: 3
				}
			],
			dom : "Bfrtip",
			buttons : [ {
				extend : "copy",
				className : "btn-sm"
			}, {
				extend : "csv",
				className : "btn-sm"
			}, {
				extend : "excel",
				className : "btn-sm"
			}, {
				extend : "pdf",
				className : "btn-sm"
			}, {
				extend : "print",
				className : "btn-sm"
			} ],
			responsive : !0
		})
	}, TableManageButtons = function() {
		"use strict";
		return {
			init : function() {
				handleDataTableButtons()
			}
		}
	}();
</script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#datatable').dataTable();
		$('#datatable-keytable').DataTable({
			keys : true
		});
		$('#datatable-responsive').DataTable();
		$('#datatable-scroller').DataTable({
			ajax : "js/datatables/json/scroller-demo.json",
			deferRender : true,
			scrollY : 380,
			scrollCollapse : true,
			scroller : true
		});
		var table = $('#datatable-fixed-header').DataTable({
			fixedHeader : true
		});
	});
	TableManageButtons.init();
</script>
</body>

</html>
