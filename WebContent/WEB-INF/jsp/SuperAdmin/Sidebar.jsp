<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="sidebar">
	<div class="brand">
		<a href="<c:url value="/SuperAdmin/Home"/>"  class="site_title">
		<img src="<c:url value='/resources/images/vpaylogo.png'/>" style="width:50%;margin-top:-20px;margin-bottom:-15px;" alt="SuperAdmin Logo" class="img-responsive logo"></a>
	</div>
	<div class="sidebar-scroll">
		<nav>
			<ul class="nav">
				<li>
					<a href="#subUsers" data-toggle="collapse" class="collapsed"><i class="fa fa-users fa-2x"></i> <span>Users</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
					<div id="subUsers" class="collapse">
						<ul class="nav">
							<li><a href="<c:url value="/SuperAdmin/UserList"/>">All</a></li>
							<li><a href="<c:url value="/SuperAdmin/VerifiedUsers"/>">Verified</a></li>
							<li><a href="<c:url value="/SuperAdmin/UnverifiedUsers"/>">Unverified</a></li>
							<li><a href="<c:url value="/SuperAdmin/BlockedUsers"/>">Blocked</a></li>
							<li><a href="<c:url value="/SuperAdmin/OnlineUsers"/>">Online</a></li>
							<li><a href="<c:url value="/SuperAdmin/LockedUsers"/>">Locked</a></li>
							<li><a href="<c:url value="/SuperAdmin/MaleUsers"/>">Male</a></li>
							<li><a href="<c:url value="/SuperAdmin/FemaleUsers"/>">Female</a></li>
							<li><a href="<c:url value="/SuperAdmin/KYCUsers"/>">KYC</a></li>
							<li><a href="<c:url value="/SuperAdmin/AadharDetails"/>">Aadhar Details</a></li>
						</ul>
					</div>
				</li>

				<li>
					<a href="#subReports" data-toggle="collapse" class="collapsed"><i class="fa fa-files-o fa-2x"></i> <span>Reports</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
					<div id="subReports" class="collapse ">
						<ul class="nav">
							<li><a href="<c:url value="/SuperAdmin/TransactionReport"/>">Transactions</a></li>
							<li><a href="<c:url value="/SuperAdmin/SettlementAccount"/>">Settlement</a></li>
							<li><a href="<c:url value="/SuperAdmin/BankTransfer"/>">Bank Transfer</a></li>
						</ul>
					</div>
				</li>
				
			<%-- 			
				<li>
					<a href="#subTravels" data-toggle="collapse" class="collapsed"><i class="fa fa-plane"></i> <span>Travel</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
					<div id="subTravels" class="collapse ">
						<ul class="nav">
						    <li><a href="<c:url value="/SuperAdmin/FlightdeatilList"/>">Flight Detail</a></li>
							    <li><a href="<c:url value="/SuperAdmin/BusDeatilsList"/>">Bus Details</a></li>
						</ul>
					</div>
				</li>
				
					<li>
					<a href="#subCards" data-toggle="collapse" class="collapsed"><i class="fa fa-credit-card"></i> <span>Treat Card</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
					<div id="subCards" class="collapse ">
						<ul class="nav">
						    <li><a href="<c:url value="/SuperAdmin/TreatCardPlansList"/>">TreatCard Plans</a></li>
						    <li><a href="<c:url value="/SuperAdmin/TreatCardReport"/>">TreatCard Register List</a></li>
						    <li><a href="<c:url value="/SuperAdmin/TreatCardCount"/>">TreatCard Count List</a></li>
						</ul>
					</div>
				</li> --%>

				<li><a href="<c:url value="/SuperAdmin/SendNotification"/>" class=""><i class="fa fa-bullhorn fa-2x"></i> <span>Send Notifications</span></a></li>

				<li>
					<a href="#subLogs" data-toggle="collapse" class="collapsed"><i class="fa fa-file-archive-o fa-2x"></i><span>Logs</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
					<div id="subLogs" class="collapse ">
						<ul class="nav">
							<li><a href="<c:url value="/SuperAdmin/SMSLogs"/>">Message Logs</a></li>
							<li><a href="<c:url value="/SuperAdmin/MailLogs"/>">Email Logs</a></li>
						</ul>
					</div>
				</li>
				
				<li><a href="<c:url value="/SuperAdmin/BulkFileList"/>" class=""><i class="fa fa-retweet fa-2x"></i> <span>Bulk Report</span></a></li>

				<%-- <li><a href="<c:url value="/SuperAdmin/MdexSwitch"/>" class=""><i class="fa fa-cogs fa-2x"></i> <span>Mdex/Instantpay</span></a></li> --%>
				
				<li>
					<a href="#switchMenu" data-toggle="collapse" class="collapsed"><i class="fa fa-file-archive-o fa-2x"></i><span>Services</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
					<div id="switchMenu" class="collapse ">
						<ul class="nav">
							<%-- <li><a href="<c:url value="/Admin/AddOperator"/>"> <span>Add Operator</span></a></li> --%>
						    <li><a href="<c:url value="/SuperAdmin/AddServiceType"/>"> <span>Add Service Type</span></a></li> 
							<li><a href="<c:url value="/SuperAdmin/AddServices"/>"><span>Add Services</span></a></li>
							<li><a href="<c:url value="/SuperAdmin/Services"/>"><span>Services List</span></a></li>
						</ul>
					</div>
				</li>
				
				
				
				<li><a href="<c:url value="/SuperAdmin/Version"/>" class=""><i class="fa fa-cogs fa-2x"></i><span>Versions</span></a></li>
				<li><a href="<c:url value="/SuperAdmin/AccountsList"/>" class=""><i class="fa fa-tasks fa-2x"></i> <span>Update Limits</span></a></li>
				<li><a href="<c:url value="/SuperAdmin/AccessList"/>" class=""><i class="fa fa-users fa-2x"></i><i class="fa fa-plus fa-2x"></i> <span>Update Access</span></a></li>
				<li><a href="<c:url value="/SuperAdmin/commissionUpdate"/>" class=""><i class="fa fa-inr"></i><span>Update Commission</span></a></li>

				<li>
					<a href="#subEmail" data-toggle="collapse" class="collapsed"><i class="fa fa-envelope fa-2x"></i><span>Email</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
					<div id="subEmail" class="collapse ">
						<ul class="nav">
							<li><a href="<c:url value="/SuperAdmin/SendMail"/>">Send Email</a></li>
							<li><a href="<c:url value="/SuperAdmin/SendBulkMail"/>">Send Bulk Email</a></li>
						</ul>
					</div>
				</li>
				
				<li><a href="<c:url value="/SuperAdmin/getMerchantRefundRequest"/>" class=""><i class="fa fa-retweet fa-2x"></i> <span>Merchant Refund Transactions</span></a></li>
				
				<li><a href="<c:url value="/SuperAdmin/FilterRefundTransactions"/>" class=""><i class="fa fa-retweet fa-2x"></i> <span>Refund Transactions</span></a></li>
				
				<li><a href="<c:url value="/SuperAdmin/FilterRefundTransactionsOfBillPayment"/>" class=""><i class="fa fa-retweet fa-2x"></i> <span>Refund BillPayment</span></a></li>
				<li>
					<a href="#subSMS" data-toggle="collapse" class="collapsed"><i class="fa fa-commenting fa-2x"></i><span>SMS</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
					<div id="subSMS" class="collapse ">
						<ul class="nav">
							<li><a href="<c:url value="/SuperAdmin/SendSMS"/>">Send SMS</a></li>
							<li><a href="<c:url value="/SuperAdmin/SendBulkSMS"/>">Send Bulk SMS</a></li>
						</ul>
					</div>
				</li>

				<li>
					<a href="#subMerchant" data-toggle="collapse" class="collapsed"><i class="fa fa-user fa-2x"></i><span>Merchant</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
					<div id="subMerchant" class="collapse ">
						<ul class="nav">
							<li><a href="<c:url value="/SuperAdmin/ListMerchant"/>">List</a></li>
							<%--<li><a href="<c:url value="/SuperAdmin/Merchant"/>">Add</a></li>--%>
							<%--<li><a href="<c:url value="/SuperAdmin/MerchantNEFTList"/>">NEFT Requests</a></li>--%>

						</ul>
					</div>
				</li>


				<li>
					<a href="#subPromo" data-toggle="collapse" class="collapsed"><i class="fa fa-clipboard fa-2x"></i><span>Promo Code</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
					<div id="subPromo" class="collapse ">
						<ul class="nav">
							<li><a href="<c:url value='/SuperAdmin/GeneratePromoCode'/>">Generate </a></li>
							<li><a href="<c:url value='/SuperAdmin/PromoCodeList'/>">List </a></li>
							<li><a href="<c:url value='/SuperAdmin/PromoTransactions'/>">Transactions</a></li>
						</ul>
					</div>
				</li>
			</ul>
		</nav>
	</div>
	<a class="footer" style="background-color: #0084b4" href="#" title="Twitter share" target="_blank"><i class="fa fa-twitter"></i> Follow us <span></span></a>
</div>
