<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Service List</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<sec:csrfMetaTags/>
	<!-- CSS -->
	<script src="<c:url value='/resources/js/jquery.js'/>"></script>
	<%-- <link href="${pageContext.request.contextPath}/resources/admin/css/bootstrap.min.css" rel="stylesheet"> --%>
	<link rel="stylesheet" href="<c:url value='/resources/super_admin/css/bootstrap.min.css'/>">
	<link rel="stylesheet" href="<c:url value='/resources/super_admin/css/vendor/icon-sets.css'/>">
	<link rel="stylesheet" href="<c:url value='/resources/super_admin/css/main.min.css'/>">
	<link rel="stylesheet" href="<c:url value='/resources/super_admin/css/demo.css'/>">
	<link rel="stylesheet" href="<c:url value='/resources/css/font-awesome.min.css'/>">
	<link href="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.css"
			rel="stylesheet" type="text/css" />
	<link href="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.css"
			rel="stylesheet" type="text/css" />
	<link href="${pageContext.request.contextPath}/resources/admin/js/datatables/fixedHeader.bootstrap.min.css"
			rel="stylesheet" type="text/css" />
	<link href="${pageContext.request.contextPath}/resources/admin/js/datatables/responsive.bootstrap.min.css"
			rel="stylesheet" type="text/css" />
	<link href="${pageContext.request.contextPath}/resources/admin/js/datatables/scroller.bootstrap.min.css"
			rel="stylesheet" type="text/css" />

	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="<c:url value = '/resources/super_admin/img/apple-icon.png'/>">
	<link rel="icon" type="image/png" sizes="96x96" href="<c:url value = '/resources/images/favicon.png'/>">
	<script src="<c:url value='/resources/super_admin/js/jquery/jquery-2.1.0.min.js'/>"></script>
	<link rel="stylesheet" href="<c:url value="/resources/css/datepicker.css"/>">
	<script src="<c:url value="/resources/js/datepicker.js"/>"></script>
	<script type="text/javascript">
$(document).ready(function() {
	
         		var errorMsg=document.getElementById("errorMsg").value;
        		if(!errorMsg.length == 0){
        			$("#common_error_true").modal("show");
        			$("#common_error_msg").html(errorMsg);
        			var timeout = setTimeout(function(){
        				$("#common_error_true").modal("hide");
        				$("#errorMsg").val("");
        	          }, 5000);
        		}
        		
        		var successMsg=document.getElementById("successMsg").value;
        		if(!successMsg.length == 0){
        			$("#common_success_true").modal("show");
        			$("#common_success_msg").html(successMsg);
        			var timeout = setTimeout(function(){
        				$("#common_success_true").modal("hide");
        				$("#successMsg").val("");
        	          }, 5000); 
        		}
         	});
   
  // Calling Api for set the exiting operator      	
$.ajax({
	    type : "GET",
	     url : "${pageContext.request.contextPath}/SuperAdmin/GetOperatorList",
	       success:function(response){
				if(response.code.includes("S00")){
				 for(var i=0;i<response.serviceDTOs.length;i++){
               $("#operator").append('<option value="'+ response.serviceDTOs[i].name+'">'+response.serviceDTOs[i].name+'</option>')
				   }
				  }
			    }
		 });
  
	$.ajax({
	    type : "GET",
	     url : "${pageContext.request.contextPath}/SuperAdmin/GetServiceTypeList",
	       success:function(response){
				if(response.code.includes("S00")){
				 for(var i=0;i<response.serviceDTOs.length;i++){
               $("#serviceType").append('<option value="'+ response.serviceDTOs[i].name+'">'+response.serviceDTOs[i].name+'</option>')
				   }
				  }
			    }
		 });
	         </script>
	         
<style>
.name-input:valid { text-transform: uppercase; }

</style>	         
	
</head>

<body>
	<!-- WRAPPER -->
<div id="wrapper">
	<!-- SIDEBAR -->
	<jsp:include page="/WEB-INF/jsp/SuperAdmin/Sidebar.jsp"/>
	<!-- END SIDEBAR -->
	<!-- MAIN -->
	<input type="text" name="successMsg" id="successMsg" value="${msg}">
	<input type="hidden" name="errorMsg" id="errorMsg" value="${error}">
	<div class="main">
		<!-- NAVBAR -->
		<jsp:include page="/WEB-INF/jsp/SuperAdmin/Navbar.jsp"/>
		<!-- END NAVBAR -->
		<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
							<div class="panel panel-headline">
								<div class="panel-heading">
							<h3 class="panel-title">Add Services</h3></div>
						<div class="panel-body" style="background: #eaeaea;">
							<div class="row">
								<div class="col-md-4 col-md-offset-4"><br>
									<!-- ADD MERCHANT FORM START -->
									<div align="center">
										<form action="${pageContext.request.contextPath}/SuperAdmin/AddServices" method="post">
											<div align="center">
												<div class="form-group">
													<input type="text" class="form-control name-input" required="required"
														placeholder="Service Code" id="code" name="code" maxlength="10"
														onkeypress="return lettersOnly(event)">
													<p id="error_code" style="color:red; margin-top: 2px;"></p>
												</div>
												<div class="form-group">
													<input type="text" class="form-control" required="required"
														placeholder="Service Name" id="name" name="name"
														onkeypress="return lettersOnly(event)">
													<p id="error_name" style="color:red; margin-top: 2px;"></p>
												</div>

												<div class="form-group">
													<input type="text" class="form-control" required="required"
														placeholder="Min Amount" id="minAmount" name="minAmount"
														onkeypress="return isNumber(event)">
													<p class="error" id="error_minAmount"
														style="color: red; margin-top: 2px;"></p>
												</div>

												<div class="form-group">
													<input type="text" class="form-control" required="required"
														placeholder="Max Amount" id="maxAmount" name="maxAmount"
														onkeypress="return isNumber(event)">
													<p class="error" id="error_maxAmount"
														style="color: red; margin-top: 2px;"></p>
												</div>

												<div class="form-group">
													<input type="text" class="form-control" required="required"
														placeholder="Commission Value" id="commission"
														name="value" onkeypress="return isNumber(event)">
													<p class="error" id="error_commission"
														style="color: red; margin-top: 2px;"></p>
												</div>

												<div class="form-group">
													<input type="text" class="form-control" required="required"
														placeholder="Description" id="description"
														name="description" onkeypress="return lettersOnly(event)">
														<p  id="error_description" style="color:red; margin-top: 2px;"></p>
												</div>

												<label style="margin-right: 61%;">Fixed</label>
													<div class="form-group col-md-3 col-sm-3 col-xs-6">
														<input type="checkbox" name="fixed" class="form-control"
															style="margin-left: 218%;">
													</div>
												<label style="margin-right: 61%;">Bbps Enable</label>
													<div class="form-group col-md-3 col-sm-3 col-xs-6">
														<input type="checkbox" name="bbpsEnabled" class="form-control"
															style="margin-left: 218%;">
													</div>	
												
												<div class="form-group">
									                <select name="type" id="type" class="form-control">
									                <option value="0" label="Select Type"/>
										            <option value="PRE" label="PRE"/>
										            <option value="POST" label="POST"/>
									                </select>
									                <p class="error" id="error_type"></p>
								              </div>

												<div class="form-group">
													<select class="form-control" name="operator" id="operator"
														onchange="clearvalue('error_operator')">
														<option value="0">Select Operator</option>
													</select>
													<p id="error_operator" style="color: red; margin-top: 2px;"></p>
												</div>
												<div class="form-group">
													<select class="form-control" name="serviceType"
														id="serviceType"
														onchange="clearvalue('error_serviceType')">
														<option value="0">Select Service Type</option>
													</select>
													<p id="error_serviceType"
														style="color: red; margin-top: 2px;"></p>
												</div>

												<div class="form-group">
													<button type="submit" id="loadMId"
														onclick="return loadValidate();" name="singlebutton"
														class="btn btn-default">ADD SERVICE</button>
												</div>
											</div>
										</form>
									</div>
								<!-- ADD MERCHANT FORM END -->
								</div>
							</div>
						</div>
					</div> 
					<!-- END OVERVIEW -->
				</div>
			</div>
			<!-- END MAIN CONTENT -->
			
		<div id="common_success_true" role="dialog" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<center id="common_success_msg" class="alert alert-success"></center>
				</div>
			</div>
		</div>
	</div>
	
	<div id="common_error_true" role="dialog" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<center id="common_error_msg" class="alert alert-danger"></center>
				</div>
			</div>
		</div>
	</div>
		</div>
			<!-- END MAIN -->
			<%-- <div class="clearfix"></div>
			<jsp:include page="/WEB-INF/jsp/Admin/Footer.jsp" /> --%>
		</div>
		<!-- END WRAPPER -->
		<script type="text/javascript">
		function loadValidate() {
			
			var spinnerUrl = "Please wait <img src='/resources/admin/images/spinner.gif' height='20' width='20'>"
			var valid = true;
			var operator = $('#operator').val();
			var code = $('#code').val();
			var name = $('#name').val();
			var description = $('#description').val();
			var commissionValue = $('#commission').val();
			var minValue = parseInt($('#minAmount').val());
			var maxValue = parseInt($('#maxAmount').val());
			var serviceType = $('#serviceType').val();
			
			 if(operator == "" || operator == "0"){
				valid = false;
				$("#error_operator").html("Select Operator");	
			}
			 if(serviceType == "" || serviceType == "0"){
					valid = false;
					$("#error_serviceType").html("Select Service Type");	
			 }
			 if(code == "" || code == "0"){
					valid = false;
					$("#error_code").html("Code should not be blank .");	
			 }else if(code.length > 8){
				     valid = false;
					$("#error_code").html("Enter minimum 8 character code .");
			 }
			 if(name == "" || name == "0"){
					valid = false;
					$("#error_name").html("Name should not be blank .");	
			 }
			 if(description == "" || description == "0"){
					valid = false;
					$("#error_description").html("Description should not be blank .");
			 } else if(description.length > 30){
				 valid = false;
					$("#error_description").html("please enter maximum 30 character description."); 
			 }
			 if(commissionValue == ""){
					valid = false;
					$("#error_commission").html("Commission value should not be blank.");	
			 }
			 if(minValue == "" || minValue == 0){
					valid = false;
					$("#error_minAmount").html("Minimum amount value should not be blank or zero.");	
			 }
			 
			 if(maxValue == "" || maxValue == 0){
					valid = false;
					$("#error_maxAmount").html("Maximum amount value should not be blank or zero.");	
			 }
			 if(minValue > maxValue){
				 valid=false;
				 $("#error_minAmount").html("Min amount should always be less than Max amount .");
				 $("#error_maxAmount").html("Max amount should always be greater than Min amount .");
			 }
			 
			 
			 var timeout = setTimeout(function(){
				 $("#error_operator").html("");
				 $("#error_serviceType").html("");
				 $("#error_name").html("");
				 $("#error_code").html("");
				 $("#error_commission").html("");
				 $("#error_description").html(""); 
				 $("#error_minAmount").html(""); 
				 $("#error_maxAmount").html(""); 
 	          }, 4000);
			 
			 
			if(valid == true) {
			     //$("#loadMId").attr("disabled","disabled");
				 $("#loadMId").html(spinnerUrl);  
				return valid;
			}else{
				 $("#loadMId").html("ADD SERVICE"); 
			}
			return valid;
		}
		
		</script>
		<script type="text/javascript">
		function clearvalue(val){
			$("#"+val).text("");
		}
		
	    function isNumber(evt) {
	    	$("#error_msg").html("");
			$("#error_msg2").html("");
			$("#error_msg3").html("");
			 evt = (evt) ? evt : window.event;
			    var charCode = (evt.which) ? evt.which : evt.keyCode;
			    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
			        return false;
			    }
			    return true;
	     }
	    
	    function lettersOnly(evt) {
	        evt = (evt) ? evt : event;
	        var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode :((evt.which) ? evt.which : 0));
	        if (charCode > 31 && (charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122)) {
	           return false;
	        }
	        return true;
	      }
		 
		 $("#commission").keyup(function(){
			 var value= $('#commission').val();
		        if(value > 20){
		        	$("#error_commission").html("Commission value should be 0 to 20 .");
		        }else{
		        	$("#error_commission").html(" ");
		        }
		    });
		 $("#maxAmount").keyup(function(){
			 var value= $('#maxAmount').val();
		        if(value > 100000){
		        	$("#error_maxAmount").html("Max amount value should be 1 to 1000000.");
		        }else{
		        	$("#error_maxAmount").html(" ");
		        }
		    });
		 $("#minAmount").keyup(function(){
			 var value= $('#minAmount').val();
		        if(value > 100){
		        	$("#error_minAmount").html("Min amount value should be 1 to 100.");
		        }else{
		        	$("#error_minAmount").html(" ");
		        }
		    });
		</script>
		
	<!-- Javascript -->
	<script src="<c:url value='/resources/super_admin/js/jquery/jquery-2.1.0.min.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/search.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/header.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/notifications.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/bootstrap/bootstrap.min.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/plugins/jquery-slimscroll/jquery.slimscroll.min.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/plugins/jquery-easypiechart/jquery.easypiechart.min.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/plugins/chartist/chartist.min.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/vpqadmin.min.js'/>"></script>

<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.bootstrap.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.buttons.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/jszip.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/pdfmake.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/vfs_fonts.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.html5.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.print.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.fixedHeader.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.keyTable.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.responsive.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/responsive.bootstrap.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.scroller.min.js"></script>


<!-- pace -->
<script
		src="${pageContext.request.contextPath}/resources/admin/js/pace/pace.min.js"></script>
	<script>
	
	
		$(document).ready(function() {
			$('#editedtable').DataTable({
				 "bLengthChange": false,
				 "bPaginate" : true,	
		 		    "bFilter" : true,
		 		    "bInfo" : true,
				"order" : [],
				columnDefs : [ {
					orderable : true,
					targets : [ 0 ]
				}, {
					orderable : true,
					targets : [ 1 ]
				}, {
					orderable : true,
					targets : [ 2 ]
				}, {
					orderable : true,
					targets : [ 3 ]
				}, {
					orderable : true,
					targets : [ 4 ]
				}, {
					orderable : true,
					targets : [ 5 ]
				} ],
			});
		});
	</script>
	<script>
		$('#userId').change(function(){
			$("#formId").submit();
		})
	</script>
</body>

</html>
