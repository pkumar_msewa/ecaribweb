<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!doctype html>
<html lang="en">
<head>
	<title>Commission Update</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<sec:csrfMetaTags/>
	<!-- CSS -->
	<script src="<c:url value='/resources/js/jquery.js'/>"></script>
	<link rel="stylesheet" href="<c:url value='/resources/super_admin/css/bootstrap.min.css'/>">
	<link rel="stylesheet" href="<c:url value='/resources/super_admin/css/vendor/icon-sets.css'/>">
	<link rel="stylesheet" href="<c:url value='/resources/super_admin/css/main.min.css'/>">
	<link rel="stylesheet" href="<c:url value='/resources/super_admin/css/demo.css'/>">
	<link rel="stylesheet" href="<c:url value='/resources/css/font-awesome.min.css'/>">
	<link
			href="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.css"
			rel="stylesheet" type="text/css" />
	<link
			href="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.css"
			rel="stylesheet" type="text/css" />
	<link
			href="${pageContext.request.contextPath}/resources/admin/js/datatables/fixedHeader.bootstrap.min.css"
			rel="stylesheet" type="text/css" />
	<link
			href="${pageContext.request.contextPath}/resources/admin/js/datatables/responsive.bootstrap.min.css"
			rel="stylesheet" type="text/css" />
	<link
			href="${pageContext.request.contextPath}/resources/admin/js/datatables/scroller.bootstrap.min.css"
			rel="stylesheet" type="text/css" />

	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="<c:url value = '/resources/super_admin/img/apple-icon.png'/>">
	<link rel="icon" type="image/png" sizes="96x96" href="<c:url value = '/resources/images/favicon.png'/>">
	<script src="<c:url value='/resources/super_admin/js/jquery/jquery-2.1.0.min.js'/>"></script>
	<link rel="stylesheet" href="<c:url value="/resources/css/datepicker.css"/>">
</head>

<body>
<!-- WRAPPER -->
<div id="wrapper">
	<!-- SIDEBAR -->
	<jsp:include page="/WEB-INF/jsp/SuperAdmin/Sidebar.jsp"/>
	<!-- END SIDEBAR -->
	<!-- MAIN -->
	<div class="main">
		<!-- NAVBAR -->
		<jsp:include page="/WEB-INF/jsp/SuperAdmin/Navbar.jsp"/>
		<!-- END NAVBAR -->
		<!-- MAIN CONTENT -->
		<div class="main-content">
			<div class="container-fluid">
				<div class="panel panel-headline">
						<div class="panel-heading">

							<div class="row">
								<div class="col-md-12">
									<div class="col-md-4">
										<h3 class="panel-title">Update Commission</h3>
									</div>
									<div class="col-md-8">
										<p style="color:green;"><c:out value="${updateMsg}"></c:out></p>
									</div>

								</div>
							</div>
							<!-- <p class="panel-subtitle">Period: Oct 14, 2016 - Oct 21, 2016</p> -->
						</div>
						<div class="row">
							<div class="col-md-12">
								<input type="hidden" id="daterange" name="daterange" value=""
									class="form-control" readonly />
								<div class="col-md-3 col-sm-3 col-xs-3">
									<label>Service Type*</label> <select id="userId" name="val"
										class="form-control">
										<option value="" hidden>Select Service Type</option>
										<c:if
											test="${requestScope.allService !=null && !empty allService}">
											<c:forEach var="allService"
												items="${requestScope.allService}">
												<option value="${allService.id}">
													<c:out value="${allService.name}" /></option>
											</c:forEach>
										</c:if>
									</select>
									<p id="userMsg"></p>
								</div>
								<form id="formId"action="" method="post">
									<div class="col-md-3 col-sm-3 col-xs-3">
										<label>Services*</label> <select id="serviceId" name="code"
											class="form-control">
											<option value="" hidden>Select Service</option>
										</select>
										<p id="serviceMsg" style="color:red;"></p>
									</div>
									<div class="col-md-2 col-sm-2 col-xs-2">
										<button type="button" id="findCom"class="btn btn-primary" title="Search"
											style="margin-top: 25px;" onclick="getAllCommission();">Find Commission
										</button>
									</div>
								</form>
							</div>
						</div>
						<div class="panel-body" style="background: #eaeaea;">
							<div class="row">
								<div class="col-md-12">
									<!-- TABLE HOVER -->
									<!-- page content -->
									<table id="editedtable"
										class="table table-striped table-bordered date_sorted">
										<thead>
											<tr>
												<th>Sr.No</th>
												<th>Min Amount</th>
												<th>Max Amount</th>
												<th>Value</th>
												<th>Type</th>
												<th>Identifier</th>
												<th>Is Fixed</th>
											</tr>
										</thead>
										<%-- <tbody>
											<c:if test="${allCommission !=null && !empty allCommission}">
												<c:forEach var="commission"
													items="${requestScope.allCommission}">
													<tr>
														<td><c:out value="${commission.minAmount}"></c:out></td>
														<td><c:out value="${commission.maxAmount}"></c:out></td>
														<td><c:out value="${commission.type}"></c:out></td>
														<td><c:out value="${commission.value}"></c:out></td>
														<td><c:out value="${commission.identifier}"></c:out></td>
														<td><c:out value="${commission.fixed}"></c:out></td>
													</tr>
												</c:forEach>
											</c:if>
											<c:if test="${allCommission == null">
												<c:out value="NO Data Found"></c:out>
											</c:if>
										</tbody> --%>
									</table>
									<div id="errorId"></div>
									<nav>
										<ul class="pagination" id="paginationn"></ul>
									</nav>
									<!-- End Page Content -->
									<!-- END TABLE HOVER -->
								</div>
							</div>
						</div>

<!-- 	Upate commission model -->

							<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
								aria-labelledby="exampleModalLabel" aria-hidden="true" style="padding-top: 4%;">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<div class="col-md-12" >
												<div class="col-md-6"  class="form-control">
											<h4 class="modal-title" id="exampleModalLabel"><c:out value="${clientName}"></c:out></h4>
											</div>
												<div class="col-md-6"  class="form-control" align="right">
											<button type="button" class="close" data-dismiss="modal"
												aria-label="Close">
												<span><i title ="Close"class="fa fa-times" aria-hidden="true"   style="color:red"></i></span>
											</button>	</div></div>
										</div>
										<div class="modal-body">

											<form class="form-auth-small" action="${pageContext.request.contextPath}/SuperAdmin/commissionUpdate"
											 method="post" onsubmit="return validate();">
												<div class="row">
														<input type="hidden" id="typeValId" value="">
														<input type="hidden" id="servId" value="" name="serviceId">
														<input type="hidden" id="code" value="" name="code">
														<input type="hidden" id="minId" value="" name="minAmount"> 
														<input type="hidden" id="maxId" value="" name="maxAmount"> 
													<div class="col-md-6">
														<label>Service Name</label> <input type="text"
														style="cursor:not-allowed"	name="serviceName" class="form-control" id="sId" value=""
															placeholder="" readonly>
													</div>
													<div class="col-md-6">
														<label id="lableId">Commission*</label> <input type="text"
															name="value" class="form-control" id="cId" value="" maxlength="10"
															placeholder="Enter Commission"  onkeypress="return isNumberKey(event)"><p id="errorMsg"></p>
													</div>
												</div>
												<br/>
												<div class="row">
													<div class="col-md-6">
														<label>Type</label><br /> <select name="type" id="typeId"
															class="form-control"  >
															<option value="POST">POST</option>
															<option value="PRE">PRE</option>
														</select>
													</div>
													<div class="col-md-6">
														<label>Is Fixed</label><br /> <select name="fixed"
															id="fixeId" class="form-control" >
															<option value="true" >True</option>
															<option value="false">False</option>
														</select>
													</div>
													
												</div>
												<br/>
													<div class="row" align="center">
														<button type="submit" class="btn btn-primary" id="buttonId">Submit</button>
													</div>
											</form>
										</div>
									</div>
								</div>
							</div>
					</div>
			</div>
		</div>
		<!-- END MAIN CONTENT -->
		<jsp:include page="/WEB-INF/jsp/SuperAdmin/Footer.jsp"/>
	</div>
	<!-- END MAIN -->
</div>
<!-- END WRAPPER -->
<!-- Javascript -->
<script src="<c:url value='/resources/super_admin/js/search.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/header.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/notifications.js'/>"></script>
<script src="<c:url value="/resources/js/datepicker.js"/>"></script>
<script src="<c:url value='/resources/super_admin/js/bootstrap/bootstrap.min.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/plugins/jquery-slimscroll/jquery.slimscroll.min.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/plugins/jquery-easypiechart/jquery.easypiechart.min.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/plugins/chartist/chartist.min.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/vpqadmin.min.js'/>"></script>

<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.bootstrap.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.buttons.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/jszip.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/pdfmake.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/vfs_fonts.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.html5.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.print.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.fixedHeader.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.keyTable.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.responsive.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/responsive.bootstrap.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.scroller.min.js"></script>


<!-- pace -->
<script
		src="${pageContext.request.contextPath}/resources/admin/js/pace/pace.min.js"></script>
<script type="text/javascript">
		function validate() {
		var	spinnerUrl = "Please wait <img src='${pageContext.request.contextPath}/resources/images/spinner.gif' height='20' width='20'>"
			var commission = $('#cId').val();
			$('#errorMsg').html('<p id="errorMsg"></p>');
			console.log(commission);
			if (commission == null || commission == '') {
				$('#errorMsg').html('<p id="errorMsg" style="color:red;">Please enter commission</p>');
				return false;
			}
			$('#buttonId').addClass('disabled');
			$('#buttonId').html(spinnerUrl);
			return true;
		}
	</script>

	<script type="text/javascript">
	var spinnerUrl = null;
		$('#userId').change(function() {
			spinnerUrl = "Please wait <img src='${pageContext.request.contextPath}/resources/images/spinner.gif' height='20' width='20'>"				
			var userId = $('#userId').val();
							console.log("id value:" + userId);
							if (userId == 'All') {
								$('#serviceId')
										.find('option')
										.remove()
										.end()
										.append(
												'<option id="optId2" value="" hidden selected>Select Service</option>')
										.val('');
							} else {
								$('#serviceId')
										.find('option')
										.remove()
										.end()
										.append(
												'<option id="optId2" value="" selected>Loading...</option>')
										.val('');
							}
							console.log(userId);
							$.ajax({
										type : "post",
										contentType : "application/json",
										url : "${pageContext.request.contextPath}/SuperAdmin/GetServices",
										dataType : 'json',
										data : JSON.stringify({
											"serviceId" : userId
										}),
										dataType : "json",
										success : function(data) {
											if (data != '') {
												$("#serviceId").removeAttr(
														"disabled");
												$("#optId2").remove();
												$('#serviceId')
														.find('option')
														.remove()
														.end()
														.append(
																'<option id="optId2" value="" hidden>Select Service</option>')
												$(data.serviceList).each(function(i,item) {
																		var div_data = "<option value="+data.serviceList[i].code+">"
																				+ data.serviceList[i].description
																				+ "</option>";
																		$(div_data).appendTo('#serviceId');
																});
											}
											if (data == '') {
												$("#optId2").remove();
												$('#serviceId')
														.find('option')
														.remove()
														.end()
														.append(
																'<option id="optId2" value="" selected >No Data Found</option>')
												$("#serviceId").prop(
														"disabled", "disabled");
											}
										}
									});
						});
		
		function getAllCommission() {
			var valid=true;
			var serviceId= $('#serviceId').val();
			if(serviceId.length <= 0){
				$("#serviceMsg").html("Please select service");
				valid=false;
			}
			if(valid){
			$("#findCom").addClass("disabled");
			$("#findCom").html(spinnerUrl);
			$('#editedtable tbody').html('');
			 $.ajax({
					 type : "post",
					contentType : "application/json",
					url : "${pageContext.request.contextPath}/SuperAdmin/CommissionList",
					dataType : 'json',
					data : JSON.stringify({
						"code" : serviceId
					}),
				dataType:"json",
				success:function(data){
					$("#findCom").removeClass("disabled");
					$("#findCom").html("Find Commission");
					$("#serviceMsg").html("");
					var trHTML='';
					$('#editedtable').append(trHTML);
					console.log("resp"+data.success);
					 if(data!='' && data.success==true){
						if(trHTML==''){
						$(data.commList).each(function(i,item){
							console.log("type: " + data.commList[i].type)
							total = data.commList[i].totalPages;
							trHTML += '<tr id="tbodyId"  onclick="setDataIntoModel('+data.commList[i].minAmount+','+data.commList[i].maxAmount
									+ ','+data.commList[i].value+','+data.commList[i].fixed+','+data.commList[i].serviceId+','+i+')" ><td>'
							+(i+1)+'</td><td>' + data.commList[i].minAmount + 
							'</td><td>' + data.commList[i].maxAmount + '</td><td>' + data.commList[i].value +
							'</td> <td id="tId'+i+'">'+ data.commList[i].type +
							'</td><td  id="identifierId'+i+'">' + data.commList[i].identifier + 
							'</td><td>' + data.commList[i].fixed +'</td></tr>';
						$('#typeValId').val(data.commList[i].type);	
						$('#sId').val(data.commList[i].serviceName);	
						$('#code').val(serviceId);
						});
						  $('#editedtable').append(trHTML);
						}
				}else{
					$('#typeValId').val('');	
					$('#sId').val('');
					trHTML +='<tr id="noDataId"><td>No Data found</td></tr>';
					 $('#editedtable').append(trHTML);
				}
				}
			 });
			}
		}
	</script>
	<script>
		function setDataIntoModel(min, max,ComValue,fixed,serviceId,rowNo){
		console.log(min+":"+ max+"::"+ComValue+"::"+fixed+":"+serviceId+":"+rowNo)
		$('#cId').val(ComValue);
		$('#servId').val(serviceId);
		$('#maxId').val(max);
		$('#minId').val(min);
		var typeVal=$("#typeValId").val();
		if(typeVal =='PRE'){
			document.getElementById("typeId").selectedIndex = "1";
		}else{
			document.getElementById("typeId").selectedIndex = "0";
		}
		if( fixed == false){
			document.getElementById("fixeId").selectedIndex = "1";
		}else{
			document.getElementById("fixeId").selectedIndex = "0";
		}
		$('#myModal').modal('show');
		}
		
		function isNumberKey(evt)
	     {
	        var charCode = (evt.which) ? evt.which : evt.keyCode;
	        if (charCode != 46 && charCode > 31  && (charCode < 48 || charCode > 57))
	           return false;
	        return true;
	     }
		
	</script>  
</body>

</html>
