<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Access List</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<sec:csrfMetaTags/>
	<!-- CSS -->
	<script src="<c:url value='/resources/js/jquery.js'/>"></script>
	<link rel="stylesheet" href="<c:url value='/resources/super_admin/css/bootstrap.min.css'/>">
	<link rel="stylesheet" href="<c:url value='/resources/super_admin/css/vendor/icon-sets.css'/>">
	<link rel="stylesheet" href="<c:url value='/resources/super_admin/css/main.min.css'/>">
	<link rel="stylesheet" href="<c:url value='/resources/super_admin/css/demo.css'/>">
	<link rel="stylesheet" href="<c:url value='/resources/css/font-awesome.min.css'/>">
	<link rel="stylesheet" href="<c:url value='/resources/css/toggle.css'/>">
	<link href="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.css"
			rel="stylesheet" type="text/css" />
	<link href="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.css"
			rel="stylesheet" type="text/css" />
	<link href="${pageContext.request.contextPath}/resources/admin/js/datatables/fixedHeader.bootstrap.min.css"
			rel="stylesheet" type="text/css" />
	<link href="${pageContext.request.contextPath}/resources/admin/js/datatables/responsive.bootstrap.min.css"
			rel="stylesheet" type="text/css" />
	<link href="${pageContext.request.contextPath}/resources/admin/js/datatables/scroller.bootstrap.min.css"
			rel="stylesheet" type="text/css" />

	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="<c:url value = '/resources/super_admin/img/apple-icon.png'/>">
	<link rel="icon" type="image/png" sizes="96x96" href="<c:url value = '/resources/images/favicon.png'/>">
	<script src="<c:url value='/resources/super_admin/js/jquery/jquery-2.1.0.min.js'/>"></script>
	<link rel="stylesheet" href="<c:url value="/resources/css/datepicker.css"/>">
	<script src="<c:url value="/resources/js/datepicker.js"/>"></script>
	<script src="<c:url value="/resources/js/toggle.js"/>"></script>
	<script>
        $(document).ready(function() {
            var csrfHeader = $("meta[name='_csrf_header']").attr("content");
            var csrfToken = $("meta[name='_csrf']").attr("content");
            var headers = {};
            headers[csrfHeader] = csrfToken;
            $(".edit").click(function(){
                var username = $(this).attr("id");

                $.ajax({
                    type : "GET",
                    headers: headers,
                    contentType : "application/x-www-form-urlencoded",
                    url : "/SuperAdmin/"+username+"/GetAccessListByUser/",
                    success : function(response) {
                        console.log(response);
						console.log(response.username)
						$("#username_modal").val(response.username);
                        if(response.updateAuthAdmin){
							$("#updateAuthAdmin").attr("checked","checked");
						}
						if(response.updateAuthUser){
							$("#updateAuthUser").attr("checked","checked");
						}
						if(response.updateAuthMerchants) {
							$("#updateAuthMerchants").attr("checked","checked");
						}
						if(response.updateAuthAgents){
							$("#updateAuthAgents").attr("checked","checked");
						}
						if(response.updateLimits){
							$("#updateLimits").attr("checked","checked");
						}
						if(response.sendSMS){
							$("#sendSMS").attr("checked","checked");

						}
						if(response.sendGCM) {
							$("#sendGCM").attr("checked","checked");
						}
						if(response.sendMail) {
							$("#sendMail").attr("checked","checked");
						}
						if(response.sendMoney){
							$("#sendMoney").attr("checked","checked");
						}
						if(response.requestMoney) {
							$("#requestMoney").attr("checked","checked");
						}
						if(response.loadMoney){
							$("#loadMoney").attr("checked","checked");
						}
						$("#updateAccess").modal('show');
                    }
                });


            });




        });
    </script>
</head>

<body>
<!-- WRAPPER -->
<div id="wrapper">
	<!-- SIDEBAR -->
	<jsp:include page="/WEB-INF/jsp/SuperAdmin/Sidebar.jsp"/>
	<!-- END SIDEBAR -->
	<!-- MAIN -->
	<div class="main">
		<!-- NAVBAR -->
		<jsp:include page="/WEB-INF/jsp/SuperAdmin/Navbar.jsp"/>
		<!-- END NAVBAR -->
		<!-- MAIN CONTENT -->
		<div class="main-content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">
						<b>${msg}</b><br/>
						<b>${error}</b><br/>

						<table class="table table-striped table-bordered">
							<thead>
							<tr>
								<th>S.No</th>
								<th>User Details</th>
								<th>Update User Authority</th>
								<th>Update Merchant Authority</th>
								<th>Update Admin Authority</th>
								<th>Update Agent Authority</th>
								<th>Send GCM</th>
								<th>Send SMS</th>
								<th>Send Mail</th>
								<th>Load Money</th>
								<th>Request Money</th>
								<th>Send Money</th>
								<th>Update Limits</th>
								<th></th>
							</tr>
							</thead>
							<tbody id="userList">
							<c:forEach items="${access}" var="a"
									   varStatus="loopCounter">
								<tr>
									<td>${loopCounter.count}</td>
									<td>
										<b>Username</b> <c:out value="${a.username}" default="" escapeXml="true" /><br/>
										<b>Authority</b> <c:out value="${a.authority}" default="" escapeXml="true" /><br/>
									</td>
									<td><c:if test="${a.updateAuthUser eq true}"><i class="fa fa-toggle-on  service "  style="color: green;" aria-hidden="true"></i></c:if><c:if test="${a.updateAuthUser eq false}"><i class="fa fa-toggle-off  service"  style="color: green;"  aria-hidden="true"></i></c:if></td>
									<td><c:if test="${a.updateAuthMerchants eq true}"><i class="fa fa-toggle-on service"  style="color: green;" aria-hidden="true"></i></c:if><c:if test="${a.updateAuthMerchants eq false}"><i class="fa fa-toggle-off  service"  style="color: green;"  aria-hidden="true"></i></c:if></td>
									<td><c:if test="${a.updateAuthAdmin eq true}"><i class="fa fa-toggle-on service"  style="color: green;" aria-hidden="true"></i></c:if><c:if test="${a.updateAuthAdmin eq false}"><i class="fa fa-toggle-off  service"  style="color: green;"  aria-hidden="true"></i></c:if></td>
									<td><c:if test="${a.updateAuthAgents eq true}"><i class="fa fa-toggle-on service"  style="color: green;" aria-hidden="true"></i></c:if><c:if test="${a.updateAuthAgents eq false}"><i class="fa fa-toggle-off  service"  style="color: green;"  aria-hidden="true"></i></c:if></td>
                                    <td><c:if test="${a.sendGCM eq true}"><i class="fa fa-toggle-on  service"  style="color: green;" aria-hidden="true"></i></c:if><c:if test="${a.sendGCM eq false}"><i class="fa fa-toggle-off  service"  style="color: green;"  aria-hidden="true"></i></c:if></td>
									<td><c:if test="${a.sendSMS eq true}"><i class="fa fa-toggle-on service"  style="color: green;" aria-hidden="true"></i></c:if><c:if test="${a.sendSMS eq false}"><i class="fa fa-toggle-off  service"  style="color: green;"  aria-hidden="true"></i></c:if></td>
									<td><c:if test="${a.sendMail eq true}"><i class="fa fa-toggle-on service"  style="color: green;" aria-hidden="true"></i></c:if><c:if test="${a.sendMail eq false}"><i class="fa fa-toggle-off  service"  style="color: green;"  aria-hidden="true"></i></c:if></td>
									<td><c:if test="${a.loadMoney eq true}"><i class="fa fa-toggle-on service"  style="color: green;" aria-hidden="true"></i></c:if><c:if test="${a.loadMoney eq false}"><i class="fa fa-toggle-off  service"  style="color: green;"  aria-hidden="true"></i></c:if></td>
									<td><c:if test="${a.requestMoney eq true}"><i class="fa fa-toggle-on service"  style="color: green;" aria-hidden="true"></i></c:if><c:if test="${a.requestMoney eq false}"><i class="fa fa-toggle-off  service"  style="color: green;"  aria-hidden="true"></i></c:if></td>
									<td><c:if test="${a.sendMoney eq true}"><i class="fa fa-toggle-on service"  style="color: green;" aria-hidden="true"></i></c:if><c:if test="${a.sendMoney eq false}"><i class="fa fa-toggle-off  service"  style="color: green;"  aria-hidden="true"></i></c:if></td>
									<td><c:if test="${a.updateLimits eq true}"><i class="fa fa-toggle-on service"  style="color: green;" aria-hidden="true"></i></c:if><c:if test="${a.updateLimits eq false}"><i class="fa fa-toggle-off  service"  style="color: green;"  aria-hidden="true"></i></c:if></td>
									<td><a href="#" class="edit" id="${a.username}"><i class="fa fa-pencil-square-o edit-limit"  aria-hidden="true"></i></a></td>
								</tr>
							</c:forEach>
							</tbody>
						</table>


                    </div>
				</div>

			</div>
		</div>
		<!-- END MAIN CONTENT -->
		<jsp:include page="/WEB-INF/jsp/SuperAdmin/Footer.jsp"/>
	</div>
	<!-- END MAIN -->
	<div id="updateAccess" role="dialog" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h5></h5>
				</div>
				<div class="modal-body">
					<form action="<c:url value="/SuperAdmin/UpdateAccess"/>" method="post">
						Username : <input type="text" readonly="readonly" size="50"  name="username" id="username_modal"/><br/>
						Admin Authority : <input type="checkbox" name="updateAuthAdmin" id="updateAuthAdmin" class="form-control form-ds1"  />
						Merchants Authority : <input type="checkbox" name="updateAuthMerchants" id="updateAuthMerchants" class="form-control form-ds1" />
						Agents Authority : <input type="checkbox" name="updateAuthAgents" id="updateAuthAgents" class="form-control form-ds1"  />
						User Authority : <input type="checkbox" name="updateAuthUser" id="updateAuthUser" class="form-control form-ds1"  />
						Send SMS : <input type="checkbox" name="sendSMS" id="sendSMS" class="form-control form-ds1"  />
						Send GCM : <input type="checkbox" name="sendGCM" id="sendGCM" class="form-control form-ds1"  />
						Send Mail : <input type="checkbox" name="sendMail" id="sendMail" class="form-control form-ds1" />
						Update Limits : <input type="checkbox" name="updateLimits" id="updateLimits" class="form-control form-ds1" />
						Load Money : <input type="checkbox" name="loadMoney" id="loadMoney" class="form-control form-ds1"  />
						Send Money : <input type="checkbox" name="sendMoney" id="sendMoney" class="form-control form-ds1"  />
						<button type="submit"><i class="fa fa-upload fa-2x update-limit"  aria-hidden="true"></i></button>
					</form>
				</div>
			</div>
		</div>
	</div>

</div>

<script>
</script>

<!-- END WRAPPER -->
<!-- Javascript -->
<script src="<c:url value='/resources/super_admin/js/jquery/jquery-2.1.0.min.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/search.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/header.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/notifications.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/bootstrap/bootstrap.min.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/plugins/jquery-slimscroll/jquery.slimscroll.min.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/plugins/jquery-easypiechart/jquery.easypiechart.min.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/plugins/chartist/chartist.min.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/vpqadmin.min.js'/>"></script>

<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.bootstrap.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.buttons.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/jszip.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/pdfmake.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/vfs_fonts.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.html5.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.print.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.fixedHeader.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.keyTable.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.responsive.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/responsive.bootstrap.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.scroller.min.js"></script>


<!-- pace -->
<script
		src="${pageContext.request.contextPath}/resources/admin/js/pace/pace.min.js"></script>
<script>
	var handleDataTableButtons = function() {
		"use strict";
		0 !== $("#datatable-buttons").length
		&& $("#datatable-buttons").DataTable({
			columnDefs: [
				{
					type: 'date-dd-mmm-yyyy',
					targets: 3
				}
			],
			dom : "Bfrtip",
			buttons : [ {
				extend : "copy",
				className : "btn-sm"
			}, {
				extend : "csv",
				className : "btn-sm"
			}, {
				extend : "excel",
				className : "btn-sm"
			}, {
				extend : "pdf",
				className : "btn-sm"
			}, {
				extend : "print",
				className : "btn-sm"
			} ],
			responsive : !0
		})
	}, TableManageButtons = function() {
		"use strict";
		return {
			init : function() {
				handleDataTableButtons()
			}
		}
	}();
</script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#datatable').dataTable();
		$('#datatable-keytable').DataTable({
			keys : true
		});
		$('#datatable-responsive').DataTable();
		$('#datatable-scroller').DataTable({
			ajax : "js/datatables/json/scroller-demo.json",
			deferRender : true,
			scrollY : 380,
			scrollCollapse : true,
			scroller : true
		});
		var table = $('#datatable-fixed-header').DataTable({
			fixedHeader : true
		});
	});
	TableManageButtons.init();
</script>

</body>

</html>
