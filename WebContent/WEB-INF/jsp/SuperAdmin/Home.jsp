<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!doctype html>
<html lang="en">
<head>
	<title>Dashboard | eCarib SuperAdmin</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<sec:csrfMetaTags/>
	<!-- CSS -->
	<link rel="stylesheet" href="<c:url value='/resources/super_admin/css/bootstrap.min.css'/>">
	<link rel="stylesheet" href="<c:url value='/resources/super_admin/css/vendor/icon-sets.css'/>">
	<link rel="stylesheet" href="<c:url value='/resources/super_admin/css/main.min.css'/>">
	<link rel="stylesheet" href="<c:url value='/resources/super_admin/css/demo.css'/>">
	<link rel="stylesheet" href="<c:url value='/resources/css/font-awesome.min.css'/>">
	<link rel="stylesheet" href="<c:url value='/resources/super_admin/css/suggestion-box.min.css'/>">

	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="<c:url value = '/resources/super_admin/img/apple-icon.png'/>">
	<link rel="icon" type="image/png" sizes="96x96" href="<c:url value = '/resources/images/favicon.png'/>">
</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- SIDEBAR -->
		<jsp:include page="/WEB-INF/jsp/SuperAdmin/Sidebar.jsp"/>
		<!-- END SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
			<!-- NAVBAR -->
			<jsp:include page="/WEB-INF/jsp/SuperAdmin/Navbar.jsp"/>
				<!-- END NAVBAR -->
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<!-- OVERVIEW -->
					<div class="panel panel-headline">
						<div class="panel-heading">
							<h3 class="panel-title">Overview</h3>
							<p class="panel-subtitle"></p>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-md-3">
									<div class="metric">
										<span class="icon"><i class="fa fa-users"></i></span>
										<p>
											<span class="number" id="total_users"><i class="fa fa-refresh fa-spin" style="font-size:24px"></i></span>
											<span class="title">Users</span>
										</p>
									</div>
								</div>
								<div class="col-md-3">
									<div class="metric">
										<span class="icon"><i class="fa fa-bank"></i></span>
										<p>
											<span class="number" id="pool_amount"><i class="fa fa-refresh fa-spin" style="font-size:24px"></i></span>
											<span class="title">Pool Account Balance</span>
										</p>
									</div>
								</div>
								<div class="col-md-3">
									<div class="metric">
										<span class="icon"><i class="fa fa-lightbulb-o fa-3x"></i><i class="fa fa-mobile"></i></span>
										<p>
											<span class="number" id="total_payable"><i class="fa fa-refresh fa-spin" style="font-size:24px"></i></span>
											<span class="title">Topup and Bill Payment</span>
										</p>
									</div>
								</div>
								<div class="col-md-3">
									<div class="metric">
										<span class="icon"><i class="fa fa-bar-chart"></i></span>
										<p>
											<span class="number" id="total_transactions"><i class="fa fa-refresh fa-spin" style="font-size:24px"></i></span>
											<span class="title">Total Transactions</span>
										</p>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-9">
									<div id="headline-chart" class="ct-chart"></div>
								</div>
								<div class="col-md-3">
									<div class="weekly-summary text-right">
										<span class="number" id="total_ebs"><i class="fa fa-refresh fa-spin" style="font-size:24px"></i></span>
										<span class="info-label">Load Money(EBS)</span>
									</div>
									<div class="weekly-summary text-right">
										<span class="number" id="total_vnet"><i class="fa fa-refresh fa-spin" style="font-size:24px"></i></span>
										<span class="info-label">Load Money(VNET)</span>
									</div>
									<div class="weekly-summary text-right">
										<span class="number" id="total_commission"><i class="fa fa-refresh fa-spin" style="font-size:24px"></i></span>
										<span class="info-label">Total Commission</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- END OVERVIEW -->
					<div class="row">
						<div class="col-md-6">
							<!-- RECENT TRANSACTIONS -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Recent Transactions</h3>
									<div class="right">
										<button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
										<button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
									</div>
								</div>
								<div class="panel-body no-padding">
									<table class="table table-striped">
										<thead>
											<tr>
												<th>Transaction Ref No.</th>
												<th>Name</th>
												<th>Amount</th>
												<th>Date &amp; Time</th>
												<th>Status</th>
											</tr>
										</thead>
										<tbody id="transactionList">
										<tr>
											<td><i class="fa fa-refresh fa-spin"></i></td>
											<td><i class="fa fa-refresh fa-spin"></i></td>
											<td><i class="fa fa-refresh fa-spin"></i></td>
											<td><i class="fa fa-refresh fa-spin"></i></td>
											<td><i class="fa fa-refresh fa-spin"></i></td>
										</tr>
										</tbody>
									</table>
								</div>
								<div class="panel-footer">
									<div class="row">
										<div class="col-md-6"><span class="panel-note"><i class="fa fa-clock-o"></i> Last 5 transactions </span></div>
										<div class="col-md-6 text-right"><a href="<c:url value='/SuperAdmin/TransactionReport'/>" class="btn btn-primary">View All Transactions</a></div>
									</div>
								</div>
							</div>
							<!-- END RECENT PURCHASES -->
						</div>
						<div class="col-md-6">
							<!-- MULTI CHARTS -->
                            <div class="panel">
                                <div class="panel-heading">
                                    <h3 class="panel-title">System Info</h3>
                                    <div class="right">
                                        <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                                        <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <ul class="list-unstyled list-justify">
                                        <li>CPU:
                                            <div class="progress">
                                                <div id="cpu" class="progress-bar progress-bar-striped active" role="progressbar"
                                                     aria-valuemin="0" aria-valuemax="100" style="width:1%">
                                                    0%
                                                </div>
                                            </div>
                                        </li>
                                        <li>Free Space:
                                            <div class="progress">
                                                <div id="free_space" class="progress-bar progress-bar-striped active" role="progressbar"
                                                     aria-valuemin="0" aria-valuemax="100" style="width:1%">
                                                    0%
                                                </div>
                                            </div>
                                        </li>
                                        <li>OS Name: <span id="os_name"><i class="fa fa-refresh fa-spin" style="font-size:24px"></i></span></li>
                                        <li>OS Version: <span id="os_version"><i class="fa fa-refresh fa-spin" style="font-size:24px"></i></span></li>
                                        <li>OS Type: <span id="os_type"><i class="fa fa-refresh fa-spin" style="font-size:24px"></i></span></li>
                                    </ul>
                                </div>
                            </div>
                            <!-- END MULTI CHARTS -->
						</div>
					</div>

				</div>
			</div>
			<!-- END MAIN CONTENT -->
			<jsp:include page="/WEB-INF/jsp/SuperAdmin/Footer.jsp"/>
		</div>
		<!-- END MAIN -->
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="<c:url value='/resources/super_admin/js/jquery/jquery-2.1.0.min.js'/>"></script>
	<script src="<c:url value='/resources/super_admin/js/search.js'/>"></script>
	<script src="<c:url value='/resources/super_admin/js/header.js'/>"></script>
	<script src="<c:url value='/resources/super_admin/js/notifications.js'/>"></script>

	<script src="<c:url value='/resources/super_admin/js/bootstrap/bootstrap.min.js'/>"></script>
	<script src="<c:url value='/resources/super_admin/js/plugins/jquery-slimscroll/jquery.slimscroll.min.js'/>"></script>
	<script src="<c:url value='/resources/super_admin/js/plugins/jquery-easypiechart/jquery.easypiechart.min.js'/>"></script>
	<script src="<c:url value='/resources/super_admin/js/plugins/chartist/chartist.min.js'/>"></script>
	<script src="<c:url value='/resources/super_admin/js/vpqadmin.min.js'/>"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			var csrfHeader = $("meta[name='_csrf_header']").attr("content");
			var csrfToken = $("meta[name='_csrf']").attr("content");
			var headers = {};
			headers[csrfHeader] = csrfToken;

			$.ajax({
				type:"POST",
				url:"/SuperAdmin/HomeInAjax",
				dataType:"json",
				success:function(data){
					console.log(data);
					$('#mpayable').html("<i class='fa fa-inr' aria-hidden='true'></i>"+data.merchantPayable);
					$('#total_users').html(data.totalUser);
					$('#total_commission').html("<i class='fa fa-inr fa-2x' aria-hidden='true'></i>"+data.totalCommission);
					$('#total_transactions').html(data.totalTrans);
					$('#total_ebs').html("<i class='fa fa-inr fa-2x' aria-hidden='true'></i>"+data.totalLoadMoneyEBS);
					$('#total_vnet').html("<i class='fa fa-inr fa-2x' aria-hidden='true'></i>"+data.totalLoadMoneyVNet);
					$('#total_payable').html("<i class='fa fa-inr' aria-hidden='true'></i>"+data.totalPayable);
					$('#bank_amount').html("<i class='fa fa-inr' aria-hidden='true'></i>"+data.bankAmount);
					$('#pool_amount').html("<i class='fa fa-inr' aria-hidden='true'></i>"+data.pool);
				}
			});
            $.ajax({
                type:"GET",
                url:"/SuperAdmin/SystemInfo",
                dataType:"json",
                success:function(data){
                        console.log(data);
                    var cpu_usage = data.cpuUsage;
                    var total_space = data.totalSpace;
                    var free_space = data.freeSpace;
                    var perFree = ((total_space-free_space)/total_space)*100;

                    if(cpu_usage < 40){
                        $("#cpu").addClass("progress-bar-success");
                    }else if(cpu_usage >= 40 && cpu_usage < 50){
                        $("#cpu").addClass("progress-bar-info");
                    }else if(cpu_usage >= 50 && cpu_usage < 60){
                        $("#cpu").addClass("progress-bar-warning");
                    }else{
                        $("#cpu").addClass("progress-bar-danger");
                    }


                    if(perFree < 40){
                        $("#free_space").addClass("progress-bar-success");
                    }else if(perFree >= 40 && perFree < 50){
                        $("#free_space").addClass("progress-bar-info");
                    }else if(perFree >= 50 && perFree < 60){
                        $("#free_space").addClass("progress-bar-warning");
                    }else{
                        $("#free_space").addClass("progress-bar-danger");
                    }
                        $("#os_name").html(data.osName);
                        $("#os_type").html(data.osType)
                        $("#os_version").html(data.osVersion);
                        $("#cpu").attr("style","width:"+data.cpuUsage+"%");
                        $("#cpu").html(data.cpuUsage+"%");
                    $("#free_space").attr("style","width:"+perFree+"%");
                    $("#free_space").html(free_space+" MB");

                    }
            });

			$.ajax({
				type:"GET",
				url:"/SuperAdmin/TransactionReportJSON",
				dataType:"json",
				headers:headers,
				success:function(data){
					console.log(data);
					var success = data.success;
					var list = data.list;
					if(success) {
						$("#transactionList").html("");
						$.each(list,function(index,value){
							var statusClass = '';
							var statusValue = value.status;
							if(statusValue == "Initiated") {
								statusClass = 'label label-warning';
							}else if(statusValue == "Success"){
								statusClass = 'label label-success';
							}else if(statusValue == "Processing"){
								statusClass = 'label label-primary';
							}else if(statusValue == "Failed"){
								statusClass = 'label label-danger';
							}else {
								statusClass = 'label label-default';
							}
							console.log(statusClass);
							$("#transactionList").append("<tr id="+index+"><td>"+value.transactionRefNo+"</td><td>"+value.username+"</td><td>"+value.amount+"</td><td>"+value.dateOfTransaction+"</td><td><span class='"+statusClass+"'>"+value.status+"</span></td></tr>");
						});
					}
				}
			});



		});

	</script>
</body>

</html>
