<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>BulkFile List</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<sec:csrfMetaTags/>
	<!-- CSS -->
	<script src="<c:url value='/resources/js/jquery.js'/>"></script>
	<link rel="stylesheet" href="<c:url value='/resources/super_admin/css/bootstrap.min.css'/>">
	<link rel="stylesheet" href="<c:url value='/resources/super_admin/css/vendor/icon-sets.css'/>">
	<link rel="stylesheet" href="<c:url value='/resources/super_admin/css/main.min.css'/>">
	<link rel="stylesheet" href="<c:url value='/resources/super_admin/css/demo.css'/>">
	<link rel="stylesheet" href="<c:url value='/resources/css/font-awesome.min.css'/>">
	<link rel="stylesheet" href="<c:url value='/resources/css/toggle.css'/>">
	<link href="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.css"
			rel="stylesheet" type="text/css" />
	<link href="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.css"
			rel="stylesheet" type="text/css" />
	<link href="${pageContext.request.contextPath}/resources/admin/js/datatables/fixedHeader.bootstrap.min.css"
			rel="stylesheet" type="text/css" />
	<link href="${pageContext.request.contextPath}/resources/admin/js/datatables/responsive.bootstrap.min.css"
			rel="stylesheet" type="text/css" />
	<link href="${pageContext.request.contextPath}/resources/admin/js/datatables/scroller.bootstrap.min.css"
			rel="stylesheet" type="text/css" />

	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="<c:url value = '/resources/super_admin/img/apple-icon.png'/>">
	<link rel="icon" type="image/png" sizes="96x96" href="<c:url value = '/resources/images/favicon.png'/>">
	<script src="<c:url value='/resources/super_admin/js/jquery/jquery-2.1.0.min.js'/>"></script>
	<link rel="stylesheet" href="<c:url value="/resources/css/datepicker.css"/>">
	<script src="<c:url value="/resources/js/datepicker.js"/>"></script>
	<script src="<c:url value="/resources/js/toggle.js"/>"></script>
	<script>
        $(document).ready(function() {
        	var spinnerUrl = "Please Wait <img src='/resources/images/spinner.gif' height='20' width='20'>"
            var csrfHeader = $("meta[name='_csrf_header']").attr("content");
            var csrfToken = $("meta[name='_csrf']").attr("content");
            var headers = {};
            headers[csrfHeader] = csrfToken;
            $("#submit_button").click(function(){
            	$("#submit_button").addClass("disabled");
            	$("#submit_button").html(spinnerUrl);
            	var userList = generateUserList();
            	 $.ajax({
           			type : "POST",
                    headers: headers,
                	url:"${pageContext.request.contextPath}/SuperAdmin/BulkFileRecord",
                	dataType:"json",
           			contentType : "application/json",
           			data : JSON.stringify({data : userList}),
                    success : function(response) {
                    	if(response.code.includes("S00")){
                    		window.location = "/SuperAdmin/BulkFileList";
                    	}
                    }
                 
                }); 


            });




        });
        function generateUserList() {
        	var userListArr = new Array();
        	$('#userList tr').each(function() {
        		var $tr = $(this);
        		var obj = {
        				'mobileNumber' : $tr.find('#mobileNumber').text(),
        				'amount' : $tr.find('#amount').text(),
        				'status' : $tr.find('#status').text()
        		};
        		$tr.find('input').is(':checked') ? userListArr.push(obj) : null;
        	});
        	return userListArr;
        }
        
        
    </script>
</head>

<body>
<!-- WRAPPER -->
<div id="wrapper">
	<!-- SIDEBAR -->
	<jsp:include page="/WEB-INF/jsp/SuperAdmin/Sidebar.jsp"/>
	<!-- END SIDEBAR -->
	<!-- MAIN -->
	<div class="main">
		<!-- NAVBAR -->
		<jsp:include page="/WEB-INF/jsp/SuperAdmin/Navbar.jsp"/>
		<!-- END NAVBAR -->
		<!-- MAIN CONTENT -->
		<div class="main-content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">
						<b>${msg}</b><br/>
						<b>${error}</b><br/>

						<table class="table table-striped table-bordered">
							<thead>
							<tr>
								<th>S.No</th>
								<th>Mobile Number</th>
								<th>Amount</th>
								<th>Status</th>
								<th>Select All ::<input name="select_all" id="checkAll" type="checkbox"></th>
							</tr>
							</thead>
							<tbody id="userList">
							<c:forEach items="${bulkFile}" var="a" varStatus="loopCounter">
								<tr id="values">
									<td>${loopCounter.count}</td>
									<td id="mobileNumber"><c:out value="${a.mobileNumber}" default="" escapeXml="true" /></td>
									<td id="amount"><c:out value="${a.amount}" default="" escapeXml="true" /></td>
									<td id="status"><c:out value="${a.status}" default="" escapeXml="true" /></td>
									<td><input type="checkbox" class="checkBoxClass"/></td>
								</tr>
							</c:forEach>
							</tbody>
						</table>
                      <center><input type="submit" value="Submit" id="submit_button" style="padding: 6px 30px; border: 0; background: #73879C; color: white;"/></center>

                    </div>
				</div>

			</div>
		</div>
		<!-- END MAIN CONTENT -->
		<jsp:include page="/WEB-INF/jsp/SuperAdmin/Footer.jsp"/>
	</div>
	<!-- END MAIN -->

</div>

<script>
$("#checkAll").click(function () {
    $(".checkBoxClass").prop('checked', $(this).prop('checked'));
});
</script>

<!-- END WRAPPER -->
<!-- Javascript -->
<script src="<c:url value='/resources/super_admin/js/jquery/jquery-2.1.0.min.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/search.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/header.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/notifications.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/bootstrap/bootstrap.min.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/plugins/jquery-slimscroll/jquery.slimscroll.min.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/plugins/jquery-easypiechart/jquery.easypiechart.min.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/plugins/chartist/chartist.min.js'/>"></script>
<script src="<c:url value='/resources/super_admin/js/vpqadmin.min.js'/>"></script>

<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.bootstrap.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.buttons.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/jszip.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/pdfmake.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/vfs_fonts.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.html5.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.print.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.fixedHeader.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.keyTable.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.responsive.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/responsive.bootstrap.min.js"></script>
<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.scroller.min.js"></script>


<!-- pace -->
<script
		src="${pageContext.request.contextPath}/resources/admin/js/pace/pace.min.js"></script>
<script>
	var handleDataTableButtons = function() {
		"use strict";
		0 !== $("#datatable-buttons").length
		&& $("#datatable-buttons").DataTable({
			columnDefs: [
				{
					type: 'date-dd-mmm-yyyy',
					targets: 3
				}
			],
			dom : "Bfrtip",
			buttons : [ {
				extend : "copy",
				className : "btn-sm"
			}, {
				extend : "csv",
				className : "btn-sm"
			}, {
				extend : "excel",
				className : "btn-sm"
			}, {
				extend : "pdf",
				className : "btn-sm"
			}, {
				extend : "print",
				className : "btn-sm"
			} ],
			responsive : !0
		})
	}, TableManageButtons = function() {
		"use strict";
		return {
			init : function() {
				handleDataTableButtons()
			}
		}
	}();
</script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#datatable').dataTable();
		$('#datatable-keytable').DataTable({
			keys : true
		});
		$('#datatable-responsive').DataTable();
		$('#datatable-scroller').DataTable({
			ajax : "js/datatables/json/scroller-demo.json",
			deferRender : true,
			scrollY : 380,
			scrollCollapse : true,
			scroller : true
		});
		var table = $('#datatable-fixed-header').DataTable({
			fixedHeader : true
		});
	});
	TableManageButtons.init();
</script>

</body>

</html>
