<%@taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<sec:csrfMetaTags/>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>VpayQwik | Verify Mobile</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet"
		  href="<c:url value="/resources/css/font-awesome.min.css"/>">

	<link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'
		  type="image/png" />

	<script src="<c:url value='/resources/js/jquery.js'/>"></script>
	<link rel="stylesheet"
		  href="<c:url value="/resources/css/datepicker.css"/>">
	<script src="<c:url value="/resources/js/datepicker.js"/>"></script>
	<script>
		$(function() {
			$("#dob").datepicker({
				format : "yyyy-mm-dd"
			});
		});
	</script>
	<!-- Optional theme -->
	<link rel="stylesheet"
		  href="<c:url value='/resources/css/bootstrap-theme.min.css'/>"
		  type='text/css'>
	<link href="<c:url value="/resources/css/style_main.css"/>"
		  rel='stylesheet' type='text/css'>

	<link rel="stylesheet"
		  href="<c:url value='/resources/css/bootstrap.min.css'/>"
		  type='text/css'>
	<%--
        <link href="<c:url value="/resources/css/css_style.css"/>"
              rel='stylesheet' type='text/css'>
    --%>
	<script src="<c:url value='/resources/js/bootstrap.js'/>"></script>
	<script type="text/javascript"
			src="${pageContext.request.contextPath}/resources/js/header.js"></script>
	<script type="text/javascript"
			src="${pageContext.request.contextPath}/resources/js/signup.js"></script>

	<style>
		.no-js #loader {
			display: none;
		}

		.js #loader {
			display: block;
			position: absolute;
			left: 100px;
			top: 0;
		}

		.se-pre-con {
			position: fixed;
			left: 0px;
			top: 0px;
			width: 100%;
			height: 100%;
			z-index: 9999;
			background: url(/images/pq_large.gif) center no-repeat #fff;
		}

		.short {
			font-weight: normal;
			color: #FF0000;
			font-size: 13px;
		}

		.weak {
			font-weight: normal;
			color: orange;
			font-size: 13px;
		}

		.good {
			font-weight: normal;
			color: #2D98F3;
			font-size: 13px;
		}

		.strong {
			font-weight: normal;
			color: limegreen;
			font-size: 13px;
		}
	</style>

	<script src="<c:url value='/resources/js/modernizr.js'/>"></script>

	<script type="text/javascript">
		$(window).load(function() {
			$(".se-pre-con").fadeOut("slow");
		});
	</script>
		<script>
        $(document).ready(function() {
            var csrfHeader = $("meta[name='_csrf_header']").attr("content");
            var csrfToken = $("meta[name='_csrf']").attr("content");
            var headers = {};
            headers[csrfHeader] = csrfToken;
            $("#logMessage_success").html("");
    		$("#error_otp1").html("");
    		$('#verify_otp_key').val("");
            $("#login_resend_otp").click(function(){
    		var userNam = $('#username').val();
       			 $.ajax({
                        type: "POST",
                        headers: headers,
                        contentType : "application/json",
                        url: "/Api/v1/User/Windows/en/Login/ResendDeviceBindingOTP",
                		dataType : 'json',
                		data : JSON.stringify({
                            "mobileNumber": "" + userNam + "",
                        }),
                        success: function (response) {
                            if(response.code.includes("S00")){
         						$("#logMessage_success").css("color", "green");
         						$(".logMessage_success").html("OTP sent to :"+ $('#username').val());
         						$("#loginButton").removeAttr("disabled");
         	                	$("#loginButton").html("Login");
         						$("#login1").hide();
         					}
                        }
                    });
            });
            
            var count = 0;
            $(".resend-otp").click(function () {
                if (count >= 3) {
                	 $("#common_error_div").modal("show");
                     $("#common_error_message").html("Please Login Again .");
                } else count++

            });
            
            $('#common_error_div').on('hidden.bs.modal', function () {
            	window.location = "/Home";
            	})
        });
    </script>

</head>
<body>
<div class="se-pre-con"></div>
<nav class="navbar navbar-default"
	 style="min-height: 80px; margin-bottom: 0">
	<div class="container">
		<div class="navbar-header">
			<a class="navbar-brand" href="#"><img
					src="<c:url value='/resources/images/vijayalogo.png'/>" alt="logo" style="width: 230px; margin-top: 8px;" ></a>
		</div>
	</div>
	<!-- /.container-fluid -->
</nav>
<div class="line" style="height: 4px; background: #17bcc8;"></div>
<!-----------------end navbar---------------------->
<div class="container">
	<div class="container" id="aboutus">
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-info"><center><b class="logMessage_success">${msg}</b></center></div>
				<hr>
				<div class="col-md-offset-4 col-md-3 text-center">
					<spring:form modelAttribute="loginDTO" action="/User/Login/VerifyMobile" method="post" cssClass="form form-horizontal">
						<spring:hidden path="username" />
						<spring:hidden path="password"/>

						<div class="group">
							<spring:input path="mobileToken" class="numeric" id="verify_log_otp_key" maxlength="6" size="6" required="required" autocomplete="false" width="100%"/>
							<span class="highlight"></span>
							<span class="bar"></span> <label style="margin-left: 11px;">Enter OTP</label>
						</div>
						<br>
						<div class="group_1">
                      <!--  <button class="btn btn-md btncu" id="login_resend_otp"
							style="margin-bottom: 5px;margin-left: 0px;">Resend OTP</button> -->
							 <button type="button" class="btn btn-md btncu resend-otp" data-toggle="modal" data-target="resendOTP" id="login_resend_otp"
									style="margin-bottom: 5px;margin-left: 0px;">Resend OTP</button>
						&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                        <button class="btn btn-md btncu"
							style="margin-bottom: 5px; margin-left: -10px; margin-right:10px;" id="login_verify_mobile">Continue</button>
				</div>

					</spring:form>
				</div>	
				     <div id="common_error_div" role="dialog" class="modal fade">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
								</div>
								<div class="modal-body">
									<center id="common_error_message" class="alert alert-danger"></center>
								</div>
							</div>
						</div>
					</div>
				
							</div>
			<!----end col-md-12------>
		</div>
		<!----end row----->
		<hr>
	</div>
	<!------end accolades-->

	<%--		<div class="row">
                <div class="Accolades">
                    <h1>Accolades</h1>
                    <img src="resources/images/main/mobile.jpg" class="img-responsive"
                        alt="chart">
                </div>
            </div>--%>
	<!---end row-->

</div>
<!---------end About Us------------->

<%-- 		<div id="resendOTP" role="dialog" class="modal fade">
			<div class="modal-dialog modal-sm">
				<div class="modal-content text-center" style="width: 120%;">
					<button type="button" data-dismiss="modal" class="close">&times;</button>
					<div class="icon">
						<i class="fa fa-unlock-alt fa-2x" aria-hidden="true"></i>
					</div>
					<h4>Resend OTP</h4>
					<hr style="margin-top: 20px;">
					<div class="modal-body">
						<div class="group">
								<input type="text" class="numeric"  id="verify_log_otp_key" onkeypress="clearvalue('error_otp')"
									autocomplete="off" maxlength="6" required> <span
									class="highlight"></span> <span class="bar"></span> <label>Enter OTP</label>
								<p class="error" id="error_otp" class="error"></p>
							</div>
					</div>
				</div>
			</div>
		</div>
 --%>
</body>

	<script type="text/javascript">
	$(document).ready(function() {
		function disableBack() {
			window.history.forward();
		}
		window.onload = disableBack();
		window.onpageshow = function(evt) {
			if (evt.persisted)
				disableBack();
		}
	});
	
</script>
</html>