<html><head>
    <meta charset="utf-8">
    <title>Verify OTP</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/resources/css/merchant_css.css">
    <link rel="icon" href="/resources/images/favicon.png" type="image/png">
    <link rel="stylesheet" href="/resources/css/font-awesome.min.css">
    <link href="/resources/css/style_main.css">
    <link rel="stylesheet" href="/resources/css/bootstrap.min.css">
    <script src="/resources/js/jquery.js"></script>
    <script src="/resources/js/bootstrap.js"></script>
    
  <script>
    $(document).ready(function(){
    	/* $("#verifyOtp").modal("show");
    	$("#verifyOtp").modal({backdrop: false}); */
    	$('#verifyOtp').modal({
	        backdrop: 'static',
	        keyboard: true, 
	        show: true
		});
    	
    	var otpUsername = $("#otpUsername").val();
    	if(otpUsername.length>=0){
    		$("#otp_username").val(otpUsername);
    	}
    	
    	var errorMsg = $("#errorMsg").val();
		console.log("errorMsg="+errorMsg);
		if(errorMsg.length>0){
			$("#otp_error").html(errorMsg);
			$("#otp_error").val("");
		}
		$(".numeric").keydown(function(event){
			if(event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || (event.keyCode == 65 && event.ctrlKey == true) || (event.keyCode >= 35 && event.keyCode<=39)){
				return;
			}else{
				if(event.shiftKey || (event.keyCode < 48 || event.keyCode >57) && (event.keyCode < 96 || event.keyCode > 105)) {
					event.preventDefault();
				}
			}
		});
    });
  </script>
</head>
<body background="/resources/images/merchantbg.jpg">
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="payqwik">
                <img src="/resources/images/vpaylogo.png">
            </div>
        </div>
    </div>
</div>
<input type="hidden" value="${error.otp}" id="errorMsg">
<input type="hidden" value="8742068862" id="otpUsername">
<!-----modal after Registration Successful -->

<div class="col-md-3 col-md-offset-4 text-center" style="margin-top: 10%;border:1px solid;padding: 10;">
			<h4 style="/* margin-left:600px; */">OTP Verification</h4>
			<hr style="margin-top: 20px;">
		<form action="/ws/api/VerifyUserOTP" method="post">
		
			<div class="group_1">
				<input type="text" name="otp" id="verify_otp_key" required="required" maxlength="6" class="numeric" style="/* margin-left: 580px; */"> <span class="highlight"></span> <label>OTP</label>
			</div>
			<div class="group_1">
				<input type="hidden" name="contactNo" class="form-control input-sm" id="otp_username" value="8742068862">
			</div>
			<p class="error" style="color:red;/* margin-left:600px; */" id="otp_error"></p>
			<div class="row">
			<div class="col-md-6" style="margin-top: 10px;">
				<button class="btn pull-right" type="submit" style="margin-bottom: 5px;">Verify
					Mobile</button>
			</div>
		
			</div>
			
		</form>	
		
		<!-- <form action="/ws/api/VerifyResendOTP" method="post">
		
		    <div class="group_1">
				<input type="hidden" name="contactNo_resend_otp" class="form-control input-sm" id="otp_username_resend_otp" value="8742068862">
			</div>
			   <p class="error" style="color:red;/* margin-left:600px; */" id="resend_otp_error"></p>
			
		    <div class="row">
			   <div class="col-md-6" style="margin-top: 10px;margin-right:100px;">
				  <button class="btn pull-left" type="submit" style="margin-bottom: 5px;margin-top: -62px;margin-left: 171px;">Resend Otp</button>
			   </div>
		    </div>
		    
		</form> -->
	</div>		
			<!-- <div class="group_1">
				<button class="btn btn-md btncu" id="register_resend_otp" onclick="verify_resend_otp()" style="margin-left:-20px; margin-bottom:5px;">
				 Resend Otp</button>
			</div> -->
			<br>

			<!-- <div class="modal-footer">
				<div class="alert alert-success" id="success_otp"></div>
			</div> -->

<div class="navbar navbar-fixed-bottom" id="footer">
    <div class="container-fluid">

        <div class="span pull-left">
            <a href="#"><img src="/resources/images/vijaya_logo.png" style="height: 30px;"></a>
        </div>
        <div class="span pull-right">
            <p>� Copyright MSewa Software Pvt. Ltd.</p>
        </div>
    </div>
</div>


</body></html>