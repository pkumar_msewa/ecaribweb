<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage=""%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
    <link rel="icon" type="image/png" href='<c:url value="/resources/admin/upi/favicon.ico"/>'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>UPI Payment</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />

    <!--     Fonts and icons     -->
     <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

    <!-- Custom CSS -->
	<link rel="stylesheet" href='<c:url value="/resources/admin/upi/custom.css"/>'>
	
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href='<c:url value="/resources/admin/upi/bootstrap.css"/>'>
	<!-- jQuery library -->
	<script src='<c:url value="/resources/admin/upi/jquery.3.2.1.min.js"/>'></script>
	<!-- Latest compiled JavaScript -->
	<script src='<c:url value="/resources/admin/upi/bootstrap.min.js"/>'></script>
	
	 <style>
        	.error {
        		font-size: 12px;
        		color: red;
        		margin-top: 0;
        		margin-bottom: 0;
        	}
        </style>
</head>

<body>
	<div class="top-line">
		<div class="wrapper">
			<div class="top-link">
				<ul>
					<li>Customer Care: 1800 425 04066/9992/5885</li>
				</ul>
			</div>
		</div>
	</div>
	<nav class="navbar navbar-default">
	  <div class="container">
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span> 
	      </button>
	      <span class="navbar-brand"><img src='<c:url value="/resources/admin/upi/upi.png"/>'></span>
	    </div>
	    <div class="collapse navbar-collapse" id="myNavbar">
	      <ul class="nav navbar-nav">
	        <!-- <li class="active"><a href="#">Home</a></li>
	        <li><a href="#">Page 1</a></li>
	        <li><a href="#">Page 2</a></li> 
	        <li><a href="#">Page 3</a></li>  -->
	      </ul>
	      <ul class="nav navbar-nav navbar-right">
	        <!-- <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
	        <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li> -->
	        <span class="navbar-brand"><img src='<c:url value="/resources/admin/upi/logo.png"/>'></span>
	      </ul>
	    </div>
	  </div>
	</nav>
	<div class="second-line"></div>
	<div class="container">
		<div class="col-md-12">
			<div class="col-md-10 col-md-offset-1">
				<div class="card card-plain">
					<div class="card-heading">
						<div class="row">
							<%-- <div class="col-md-6">
								<div class="order-wrp">
									<span class="ord-nme">Order Ref No:</span>&nbsp;<span class="ord-num">${txnid}</span>
								</div>
							</div> --%>
							<div class="col-md-6">
								<!-- <div class="totAmt-wrp pull-right">
									<span class="amt-nme">Total Amount:</span>&nbsp;<span class="inr">INR</span>&nbsp;<span class="tot-amt">115</span>
								</div> -->
							</div>
						</div>
					</div>
					<div class="card-body">
						<div class="row">
							<div class="col-md-12">
								<div class="frm">
									<form class="form-inline" method="Post" action="/ws/api/upiValidate">
									<!-- <form class="form-inline" method="Post" action="#"> -->
									  <div class="form-group">
										 <button id ="loadMId" type="submit" onclick="return loadValidate();" class="btn btn-sm btn-default">Click here to initiate the Payment</button>
										  </div>
									  		<input type="hidden" class="form-control" name="key" id="" placeholder="" title="Merchant Id" value="${key}">  </br>
											<input type="hidden" class="form-control" name="merchanttxnid" id="" placeholder="" title="Txns Id" value="${merchanttxnid}"></br>
											<input type="hidden" class="form-control" name="billamount" id="" placeholder="" title="Amount" value="${billamount}">
											<input type="hidden" class="form-control" name="additionalcharges" id="" placeholder="" title="Amount" value="${additionalcharges}"></br>
											<input type="hidden" class="form-control" name="hash" id="" placeholder="" title="Hash" value="${hash}"></br>
											<input type="hidden" class="form-control" name="accountid" id="" placeholder="" title="accountid" value="${accountid}"></br>
											<input type="hidden" class="form-control" name="firstname" id="" placeholder="" title="Hash" value="${firstname}"></br>
											<input type="hidden" class="form-control" name="email" id="" placeholder="" title="Hash" value="${email}"></br>
											<input type="hidden" class="form-control" name="phone" id="" placeholder="" title="Hash" value="${phone}"></br>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<div class="footer-line">
<div class="col-md-12" style="text-align: center;">
                        <p><img src="${pageContext.request.contextPath}/resources/images/new_img/mlogo.png" style="width: 32px;"> &copy; <a href="http://www.msewa.com/">Msewa Software</a> Pvt Ltd. | 2017 All Rights Reserved.</p>
                      </div>
</div>
</body>
<script type="text/javascript">
function clearvalue(val) {
	$("#" + val).text("");
}
</script>
<!-- <script type="text/javascript">
	function loadValidate() {
		var spinnerUrl = "Please wait <img src='/resources/images/spinner.gif' height='20' width='20'>"
		var valid = true;
		var vpAddress = $('#vpAddress').val();
		console.log("vpAddress : " + vpAddress);
		if(vpAddress.length == 0){
			$("#error_vpAddress").html("Enter Virtual Address");
			valid = false;
		} 
		if(valid == true) {
		     //$("#loadMId").attr("disabled","disabled");
			 $("#loadMId").html(spinnerUrl);  
			return valid;
		}
		return valid;
	}
</script>
 -->

<!-- 
<script type="text/javascript">
	$(document).ready(function(){
		
		//Add Client Services
		$("#loadMId").click(function() {
			var spinnerUrl = "Please wait <img src='/resources/images/spinner.gif' height='20' width='20'>"
				var valid = true;
				var vpAddress = $('#vpAddress').val();
				console.log("vpAddress : " + vpAddress);
				if(vpAddress.length == 0){
					$("#error_vpAddress").html("Enter Virtual Address");
					valid = false;
				} 
			if(valid == true){
				$("#commonSpinner").modal("show");
				var contextPath = "${pageContext.request.contextPath}"; 
				console.log("ContexPath : : " + contextPath)
					$("#loadMId").attr("disabled","disabled");
			 		$("#loadMId").html(spinnerUrl);
				$.ajax({
					type : "POST",
					contentType : "application/x-www-form-urlencoded",
					url : contextPath+"/ws/api/UpiPaymentProcess",
					data : {
						merchantVpa : vpAddress
					},
					success : function(response) {
						$("#commonSpinner").modal("hide");
						console.log(response);
						console.log("tid : : " + response.transactionRefNO);
						console.log("tdate : : " + response.transactionDate);
						if(response.code == "S00"){
							/* window.location = "UpiSuccess"; */
						}
						if(response.code == "F00"){
							/* window.location = "UpiFailed"; */
					   }
					}
				});
			}
		});
	});
</script>
     -->


<script type="JavaScript">
		$(function(){
			$('#username').editable({
			    type: 'text',
			    pk: 1,
			    url: '/post',
			    title: 'Enter username'
			});
		})
	</script>
	
	<script>
	  $("#eicon_link").click(function(){
	  		console.log("xxxxxxxxxxxxxxx");
		     $("#email_id").css('.border-bottom ','1px solid #000');
		});
		$("#eicon_link").click(function(){
		    document.getElementById("email_id").style.border="none";
		});
	  </script>
	
<script>
	$(document).ready(function() {
		function disableBack() {
			window.history.forward()
		}
		window.onload = disableBack();
		window.onpageshow = function(evt) {
			if (evt.persisted)
				disableBack()
		}
	});
</script>

</html>