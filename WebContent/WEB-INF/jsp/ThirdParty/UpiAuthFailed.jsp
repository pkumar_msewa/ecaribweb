<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage=""%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
    <link rel="icon" type="image/png" href='<c:url value="/resources/admin/upi/favicon.ico"/>'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>UPI Auth Failed</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />

    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

    <!-- Custom CSS -->
	<link rel="stylesheet" href='<c:url value="/resources/admin/upi/custom.css"/>'>
	
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href='<c:url value="/resources/admin/upi/bootstrap.css"/>'>
	<!-- jQuery library -->
	<script src='<c:url value="/resources/admin/upi/jquery.3.2.1.min.js"/>'></script>
	<!-- Latest compiled JavaScript -->
	<script src='<c:url value="/resources/admin/upi/bootstrap.min.js"/>'></script>

	<style>
		.err-msg .err-hed .err-img img {
			width: 160px;
		}
		.err-msg .err-bdy span {
			font-size: 20px;
		    font-weight: 600;
		    color: #f50a0a;
		    line-height: 1.92233;
		    letter-spacing: 0.3pt;
		}
		.err-msg {
			padding-top: 60px;
		}
	</style>

</head>
<body>
	<div class="top-line">
		<div class="wrapper">
			<div class="top-link">
				<ul>
					<li>Customer Care: 1800 425 04066/9992/5885</li>
				</ul>
			</div>
		</div>
	</div>
	<nav class="navbar navbar-default">
	  <div class="container">
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span> 
	      </button>
	     <span class="navbar-brand"><img src='<c:url value="/resources/admin/upi/upi.png"/>'></span>
	    </div>
	    <div class="collapse navbar-collapse" id="myNavbar">
	      <ul class="nav navbar-nav">
	        <!-- <li class="active"><a href="#">Home</a></li>
	        <li><a href="#">Page 1</a></li>
	        <li><a href="#">Page 2</a></li> 
	        <li><a href="#">Page 3</a></li>  -->
	      </ul>
	      <ul class="nav navbar-nav navbar-right">
	        <!-- <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
	        <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li> -->
	       <span class="navbar-brand"><img src='<c:url value="/resources/admin/upi/logo.png"/>'></span>
	      </ul>
	    </div>
	  </div>
	</nav>
	<div class="second-line"></div>

	<div class="container">
		<div class="col-md-12">
			<div class="col-md-8 col-md-offset-2">
				<center>
					<div class="err-msg">
						<div class="err-hed">
							<div class="err-img">
								<img src='<c:url value="/resources/admin/upi/emoji.png"/>' class="img-responsive">
							</div>
							
						</div>
						<div class="err-bdy">
							<span>Merchant Authentication Failed: </span><span>${error}</span><br>
						</div>
						<div class="col-md-12">
									<center><a href="https://www.bescom.co.in/SCP/Myhome.aspx" class="btn btn-info noprint">Go to Home</a></center>
								</div>
					</div>
				</center>
			</div>
		</div>
	</div>
	<div class="footer-line">
	<div class="col-md-12" style="text-align: center;">
                        <p><img src="${pageContext.request.contextPath}/resources/images/new_img/mlogo.png" style="width: 32px;"> &copy; <a href="http://www.msewa.com/">Msewa Software</a> Pvt Ltd. | 2017 All Rights Reserved.</p>
                      </div>
	</div>
</body>
</html>