<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage=""%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
    <link rel="icon" type="image/png" href="<c:url value="/resources/admin/upi/favicon.ico"/>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Receipt Success</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />

    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

    <!-- Custom CSS -->
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/admin/upi/custom.css">

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/admin/upi/bootstrap.css">

	<style>
		h5 {
            font-weight: 600;
        }
        .top-tablle tbody tr td {
            padding: 5px;
        }
        .main_head {
            box-shadow: 0 2px 4px 0 rgba(0,0,0,0.16),0 2px 10px 0 rgba(0,0,0,0.12)!important;
            -webkit-box-shadow: 0 2px 4px 0 rgba(0,0,0,0.16),0 2px 10px 0 rgba(0,0,0,0.12)!important;
            padding: 10px 15px;
            border-bottom: 4px solid #5dbcc8;
        }
        .receipt_head {
            box-shadow: 0 2px 4px 0 rgba(0,0,0,0.16),0 2px 10px 0 rgba(0,0,0,0.12)!important;
            -webkit-box-shadow: 0 2px 4px 0 rgba(0,0,0,0.16),0 2px 10px 0 rgba(0,0,0,0.12)!important;
            padding: 10px 15px;
        }
        .receipt_body {
            border: 3px solid #efefef;
            box-shadow: 0 2px 4px 0 rgba(0,0,0,0.16),0 2px 10px 0 rgba(0,0,0,0.12)!important;
            -webkit-box-shadow: 0 2px 4px 0 rgba(0,0,0,0.16),0 2px 10px 0 rgba(0,0,0,0.12)!important;
        }
        .receipt_wrp {
            background: #ffffff;
            border-radius: 4px;
            border: 1px solid transparent;
            color: #737373;
        }
        .footer-line {
        	position: initial;
        }
        .mt-10 {
        	margin-top: 10px;
        }
	</style>

</head>
<body>
	<div class="top-line">
		<div class="wrapper">
			<div class="top-link">
				<ul>
					<li>Customer Care: 1800 425 04066/9992/5885</li>
				</ul>
			</div>
		</div>
	</div>
	<nav class="navbar navbar-default">
	  <div class="container">
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span> 
	      </button>
	      <span class="navbar-brand"><img src='<c:url value="/resources/admin/upi/upi.png"/>'></span>
	    </div>
	    <div class="collapse navbar-collapse" id="myNavbar">
	      <ul class="nav navbar-nav navbar-right">
	        <%-- <span class="navbar-brand"><img src='<c:url value="/resources/admin/upi/logo.png"/>'></span> --%>
	      </ul>
	    </div>
	  </div>
	</nav>
	<div class="second-line"></div>

	<div class="container-fluid">
		<div class="col-md-12">
			<div class="col-md-8 col-md-offset-2">
				<div class="receipt_wrp mb-20">
					<div id="successReceipt">
						<div style="margin:0 auto; padding: 17px;" class="receipt_wrp">
					        <div>
					            <table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin: 0 auto; background-color: #fff; margin-top: 2%" class="top-tablle">
					                <tbody>
					                    <tr class="main_head">
					                        <td id="">
					                            <span style="font-size: 18px; font-weight: 600; color: #395cb1;">Bangalore Electricity Supply Company Limited</span>
					                        </td>
					                        <%-- <td id="" style="text-align: right">
					                            <img src='<c:url value="/resources/admin/upi/logo.png"/>' id="" alt="Vijaya Bank" class="CToWUd" style="width: 150px;" />
					                        </td> --%>
					                    </tr>
					                    <tr><td colspan="2" height="5"></td></tr>
					                    <tr class="receipt_head">
					                        <td colspan="2">
					                            <table>
					                                <tbody>
					                                    <tr>
					                                        <td colspan="2">
					                                            <center><span style="font-size: 19px; font-weight: 600; color: #395cb1;">Thank You!</span></center>
					                                        </td>
					                                    </tr>
					                                    <tr><td colspan="2" height="10" style="border-top: 2px solid #efefef;"></td></tr>
					                                    <tr>
					                                        <td colspan="2">
					                                            <p style="font-weight: 600; color: #737473;">Your payment has been successfully received with the following details. Please quote your transaction reference number for any queries relating to this request.</p>
					                                        </td>
					                                    </tr>
					                                </tbody>
					                            </table>
					                        </td>
					                    </tr>
					                    <tr><td colspan="2" height="5"></td></tr>
					                </tbody>
					            </table>
					            <table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin: 0 auto; background-color: #fff; font-size: 14px; font-family: Arial,Helvetica,sans-serif" class="receipt_body">
					                <tbody>
					                    <tr>
					                        <td colspan="2" align="left" style="padding: 10px; background: #efefef; color: #000000; font-size: 14px; font-weight: 600;">
					                            <span style="font-size: 19px; letter-spacing: 0.5px;">Transaction Acknowledgment</span>
					                        </td>
					                    </tr>
					                    <tr>
					                        <td colspan="2" align="left" style="padding: 0 10px">
					                            <table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin: 0 auto; background-color: #fff; font-size: 14px; font-family: Arial,Helvetica,sans-serif">
					                                <tbody>
					                                    <tr>
					                                        <td><h5>Transaction Status: </h5></td>
					                                        <td><h5 style="color: #00cc00;">Success</h5></td>
					                                    </tr>
					                                    <tr>
					                                        <td><h5>Transaction Id: </h5></td>
					                                         <td><h5>${paymentId}</h5></td>
					                                        
					                                    </tr>
					                                    <tr>
					                                        <td><h5>Transaction Date and Time: </h5></td>
					                                        <td><h5>${transactionDate}</h5></td>
					                                    </tr>
					                                    <tr>
					                                        <td><h5>Transaction Reference Number: </h5></td>
					                                       <td><h5>${merchantRefNo}</h5></td>
					                                    </tr>
					                                    <tr>
					                                        <td><h5>Account ID: </h5></td>
					                                        <td><h5>${accountid}</h5></td>
					                                    </tr>
					                                    <tr>
					                                        <td><h5>Email Address: </h5></td>
					                                        <td><h5>${email}</h5></td>
					                                    </tr>
					                                    <tr>
					                                        <td><h5>Transaction Amount: </h5></td>
					                                        <td><h5>Rs. <span>${amount}</span></h5></td>
					                                    </tr>
					                                </tbody>
					                            </table>
					                        </td>
					                    </tr>
					                    
					                </tbody>
					            </table>
					        </div>
					        <div class="print_btn mt-10">
								<div class="row">
									<div class="col-md-12">
										<div class="col-md-6" style="text-align: right;">
											<a href="https://www.bescom.co.in/SCP/Myhome.aspx" class="btn btn-info" id="lnkBckTOHme"><i class="fa fa-chevron-circle-left "></i> Initiate another payment</a>
										</div>
										<div class="col-md-6">
											<a href="javascript:PrintDoc()" class="btn btn-info" id="lnkPrint"><i class="fa fa-print"></i> Print </a>
										</div>
									</div>
								</div>
							</div>
					    </div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="footer-line">
	<div class="col-md-12" style="text-align: center;">
                        <%-- <p><img src="${pageContext.request.contextPath}/resources/images/new_img/mlogo.png" style="width: 32px;"> &copy; <a href="http://www.msewa.com/">Msewa Software</a> Pvt Ltd. | 2017 All Rights Reserved.</p> --%>
                      </div>
	<!-- jQuery library -->
	<script src='<c:url value="/resources/admin/upi/jquery.3.2.1.min.js"/>'></script>
	<!-- Latest compiled JavaScript -->
	<script src='<c:url value="/resources/admin/upi/bootstrap.min.js"/>'></script>
	
<script>
	$(document).ready(function() {
		function disableBack() {
			window.history.forward()
		}
		window.onload = disableBack();
		window.onpageshow = function(evt) {
			if (evt.persisted)
				disableBack()
		}
	});
</script>
	<script type="text/javascript" language="javascript">
        function PrintDoc() {
            document.getElementById("lnkBckTOHme").style.display = 'none';
            document.getElementById("lnkPrint").style.display = 'none';
            print();
        }
    </script>
</body>
</html>
	<!-- Latest compiled JavaScript -->
	<script src='<c:url value="/resources/admin/upi/bootstrap.min.js"/>'></script>
	
<script>
	$(document).ready(function() {
		function disableBack() {
			window.history.forward()
		}
		window.onload = disableBack();
		window.onpageshow = function(evt) {
			if (evt.persisted)
				disableBack()
		}
	});
</script>
</body>
</html>