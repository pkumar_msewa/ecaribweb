<tr>
	<td><input type="text" name="title" placeholder="Title" id="titleId"
		maxlength="50" style="width: 100%; padding: 6px; margin-bottom: 10px;">
		<p id="title_error" style="color:red; margin-top: -8px;" >${error.title}</p> <br> <br></td>
</tr>
<tr>
	<td><textarea rows="4" cols="50" name="message" maxlength="159" id="mesgId"
			style="width: 100%; margin-bottom: 10px;"></textarea>
		<p  id="mesg_error" style="color:red; margin-top: -8px;">${error.message}</p> <br> <br></td>
</tr>
<tr>
	<td><input type="checkbox" name="imageGCM" id="imageGCM" /> With
		Image</td>
</tr>
<tr>
	<td><input type="file" accept="image/*" name="gcmImage"
		onchange="return ValidateFileUpload()" id="pic"
		style="margin-bottom: 10px;">
		<p class="error">${error.gcmImage}</p> <br>
		<p id="imgErrorId" style="margin-top: -30px; color: red;"></p></td>
</tr>

<script>
$(document).ready(function(){
	$("#pic").hide();
});

var isChecked=false;
var isAll=false;
$("#imageGCM").click(function(){
	isChecked = $(this).is(":checked");
	if(isChecked){
		$("#pic").show();
	}else {
		$('#imgErrorId').html("");
		$("#pic").hide();
	}
});

$('input[type=radio][name=notificationType]').change(function() {
	 if (this.value == 'SingleUser') {
		 isAll=false;
		 $("#username").show();
      }
       else{
    	   isAll=true;
    	   $('#user_error').html("");
       	$("#username").hide();
       }
   });
   
function  validateRequest(){
			var title = $('#titleId').val();
			var mesg= $('#mesgId').val();
			var type= $('#singleUser').val();
			var fuData = document.getElementById('pic');
			var FileUploadPath = fuData.value;
			var value = typeof type;
			console.log("type: "+type);
			$('#title_error').html("");
			$('#mesg_error').html("");
			$('#user_error').html("")
			var valid=true;
			if(title.length < 10){
				valid =false;
				$('#title_error').html("Please enter title of 10 characters ");
			}if(mesg.length < 20){
				valid =false;
				$('#mesg_error').html("Please enter message of 20 characters ");
			}if(value != "undefined" &&  type == "SingleUser" && !isAll){
				var username= $('#username').val();
				if(username.length < 10){
					valid =false;
					$('#user_error').html("Please enter valid mobile number");	
				}
			}if(isChecked && FileUploadPath.length <= 0){
				valid =false;
				$('#imgErrorId').html("Please select an image");
			}
			return valid;
	}
</script>
<script>
	function ValidateFileUpload() {
		var fuData = document.getElementById('pic');
		$('#imgErrorId').html("");
		var FileUploadPath = fuData.value;
		var MAX_SIZE=500000;
		console.log(FileUploadPath);
		    var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();
		    if (Extension == "gif" || Extension == "png"
		                || Extension == "jpeg" || Extension == "jpg") {
		            if (fuData.files && fuData.files[0]) {
		                var size = fuData.files[0].size;
		                console.log("size:"+size);
		                if(size > MAX_SIZE){
		                	$('#imgErrorId').html("File size can not exceed 500kb");
		                	$('#pic').val('');
		                    return false;
		                }
		            }
		    }else {
				$('#imgErrorId').html("Only GIF,JPG,JPEG,PNG file types are allowed");
				$('#pic').val('');
		    }
		}
	
	
	 function isNumberKey(evt)
     {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if ((charCode == 46 || charCode > 31 ) && (charCode < 48 || charCode > 57))
           return false;
        return true;
     }
	
	</script>