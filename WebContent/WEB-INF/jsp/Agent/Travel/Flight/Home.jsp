<!DOCTYPE HTML>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<html>
<head>
<title>VPayQwik | Travel</title>


<meta content="text/html;charset=utf-8" http-equiv="Content-Type">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'
	type="image/png" />
<!--<link rel="stylesheet" href="/resources/css/new_css/cloud-admin.css">-->
<link rel="stylesheet" href="/resources/css/font-awesome.min.css">
<link rel="stylesheet" href="/resources/css/new_css/default.css">
<!--<link rel="stylesheet" href="/resources/css/new_css/uniform.default.min.css">-->
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap-chosen.css">

<!--- GOOGLE FONTS - -->
<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600' rel='stylesheet' type='text/css'>
<!-- /GOOGLE FONTS -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/travel_css/bootstrap.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<!--<link rel="stylesheet" href="css/font-awesome.css">-->
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/travel_css/icomoon.css">
</head>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css" />
 
 <script src="/resources/js/new_js/select2.min.js"></script>
 <script type="text/javascript" 
    src="<c:url value="/resources/js/Agent/Agentdetail.js"/>"></script>
		
<!-- <link rel="stylesheet" href="/resources/css/new_css/newstyles.css"> -->
 <link rel="stylesheet" rel="stylesheet" href="/resources/css/new_css/select2.1.css">
  	
	<style>
		.rederror {
			font-size: 12px;
    		color: red;
		}
	</style>
	<style>
.hide{ visibility : hidden; }
</style>
<script>
   
  $(document).ready(function(){
   var date_input=$('input[name="date"]');
   var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
   date_input.datepicker({
    format: 'dd/mm/yyyy',
    container: container,
    startDate: '+1d',
    todayHighlight: true,
    autoclose: true,
   })
  })
  
  
   $(document).ready(function(){
   var date_input=$('input[name="returndate"]');
   var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
   date_input.datepicker({
    format: 'dd/mm/yyyy',
    startDate: '+1d',
    container: container,
    todayHighlight: true,
    autoclose: true,
   })
  })
 </script>
 

<body onload="setsrcanddestination()"> 
	<input type="hidden" value="${sourcecode}" id="traveldataval">
	
<jsp:include page="/WEB-INF/jsp/Agent/AHeader.jsp" />
	
	<!-- ====================== TOP AREA ================== -->
  <style>
        .select2-container {
            box-sizing: border-box;
            display: inline-block;
            margin: 0;
            position: relative;
            vertical-align: middle;
            width: 50% !important;
            float: right;
            background: #fff;
        }
        .inner_tab_content_Hotel > .select2-container {
            box-sizing: border-box;
            display: inline-block;
            margin: 0;
            position: relative;
            vertical-align: middle;
            width: 100% !important;
            float: right;
            background: #fff;
        }
        .inner_tab_content_Holiday > .select2-container {
            box-sizing: border-box;
            display: inline-block;
            margin: 0;
            position: relative;
            vertical-align: middle;
            width: 100% !important;
            float: right;
            background: #fff;
        }

    
        .dropdown1 {
            background: #fff;
            border: 1px solid #ccc;
            border-radius: 4px;
            width: 280px;
            padding: 5px;
            left: 55%;
           top: 343px; 
            position: absolute;
            z-index: 99; 
        }
        
        .dropdown1 .t_menu {
            border-radius:4px;
            box-shadow:none;
            margin-top:10px;
            wi/dth:300px;
        }
        .dropdown1 .t_menu:before {
            content: "";
            border-bottom: 10px solid #fff;
            border-right: 10px solid transparent;
            border-left: 10px solid transparent;
            position: absolute;
            top: -10px;
            right: 16px;
            z-index: 10;
        }
        .dropdown1 .t_menu:after {
            content: "";
            border-bottom: 12px solid #ccc;
            border-right: 12px solid transparent;
            border-left: 12px solid transparent;
            position: absolute;
            top: -12px;
            right: 14px;
            z-index: 9;
        }
        .t_divider {
            margin-top: 15px;
            margin-bottom: 15px;
            border: 0;
            border-top: 1px solid #eee;
        }
        
        .incr-btn {
  margin-left: 0px;
}
@media screen and (max-width: 1440px) {
.dropdown1 {
			top: 388px;
	}
}
@media screen and (max-width: 1024px) {
.dropdown1 {
			top: 343px;
	}
}
@media screen and (max-width: 768px) {
	.dropdown1 {
		    left: 12%;
			top: 407px;
	}
}
@media screen and (max-width: 425px) {
	.dropdown1 {
		    left: 12%;
			top: 454px;
	}
	.dropdown1 .t_menu:before{
		    right: auto;
	}
	.dropdown1 .t_menu:after{
		right: auto;
	}
}
    </style>
</head>
<script>
    $(".beSwapCity").on('click', function() {
      var fromcurrency =  $('#flight_src_list').val();
      var tocurrency = $('#flight_des_list').val();
      $('#flight_src_list').val(tocurrency).trigger('change');
      $('#flight_des_list').val(fromcurrency).trigger('change');
});
    function closedivtrevalinformation()
   	{
   		$("#trav_menu").hide();
   	}
</script>


<body>
       
       	 <!-- Model for flight gif -->
	<div id="loading_flight" class="modal fade" role="dialog" style="margin-top: 7%;">
		<div class="modal-dialog" style="background: ">

			<!-- Modal content-->
			<div class="modal-content" style="background: transparent; box-shadow: none; border: none;">
					<center><img src="${pageContext.request.contextPath}/resources/images/User/travel/img/Flight/flight.gif" class="img-responsive"></center>
			</div>

			</div>
		</div>
       <!-- Model for flight gif -->
<!-- ====================== TOP AREA ================== -->
        <div class="top-area show-onload" style="margin-top: -21px;">
            <div class="bg-holder full">
                <div class="bg-mask"></div>
                <div class="bg-parallax" style="background-image:url(/resources/images/Travel/bg3.gif);"></div>
                <div class="bg-content" style="margin-top: 6%;">
                    <div class="container">
                        <div class="bking-box" style=" margin-top: 10%;">
                            <div class="row">
                              
                                <div class="col-md-12">
                                    <div class="tggle-cont" >
								
								<div class="tab-content" style="background: rgba(0,0,0,0.6) !important; background: rgba(0,0,0,0.4); border-radius: 3px; padding: 15px; min-height: 155px; -webkit-transition: all .2s ease; -moz-transition: all .2s ease; -ms-transition: all .2s ease; -o-transition: all .2s ease; transition: all .2s ease;">
                                            <!-- ============================ Flight Tab ============================= -->
                                           
	  							<div class="top-toggle">
                                    <ul class="nav nav-tabs ">
                                          <li class="active">
                                            <a href="#tab_flight" data-toggle="tab">
                                            Flights</a>
                                        </li> 
                                            <li >
                                            <a href="#tab_bus" data-toggle="tab" id="bus_travel">
                                            Buses</a>
                                        </li>
                                       <li>
                                            <a href="#tab_hotel" data-toggle="tab">
                                            Hotels</a>
                                        </li>
                                        <!-- <li>
                                            <a href="#tab_holiday" data-toggle="tab">
                                            Holidays</a>
                                        </li> -->
                                        
                                    </ul>
                                </div>
                                
										   <div class="tab-pane active" id="tab_flight" style="margin-top: 9px;">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="top-list" style="margin-bottom: 5px;">
                                                            <ul class="nav nav-pills">
                                                              
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4" style="padding-left: 0px; padding-right: 0px;">
                                                    
                                                    <!-- =============== -->
                                                    <div class="inner_tab_content_Flight">
                                                        <select id="flightdest" class="js-example-basic-multiple" style="width: 350px;" name="sourceId"  >
                                                                
                                                        </select>
                                                        
                                                         <select id="flightsrc" class="js-example-basic-multiple" style="width: 350px;" name="sourceId"  >
                                                                 
                                                        </select>
                                                         <span class="rederror" id="errormsgsrc"></span>
                                                        <span class="rederror" id="errormsgdest" style="padding-left: 71px;"></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-4" style="padding-left: 0px; padding-right: 0px;">
                                                    <input type="text" class="form-control" placeholder="Depart Date" name="date" style="width: 50%; float: left; height: 40px;" id="date">
                                                    <i class="fa fa-calendar D-font-icon icon-inside icon-calendar"></i>
                                                    <input type="text" class="form-control"   placeholder="Return Date" name="returndate" style="width: 50%; height: 40px; cursor: default;" id="returndate" >
                                                    <i class="fa fa-calendar R-font-icon icon-inside icon-calendar"></i>
                                                </div>
                                                <div class="col-md-4" style="padding-left: 0px; padding-right: 0px; height: 40px;">
                                                    <!-- <input type="text" class="form-control" placeholder="Traveller Information" name="" style="width: 50%; float: left; height: 40px; cursor: pointer; background: #fff; readonly="readonly"> -->
                                                    <div class="pax-details" id="selTrav">
                                                        <span class="txt-ellipses">
                                                            <span class="totalCount" id="totalCounttrevaler">1</span> Traveller(s)<span class="flight_cls" id="flight_cls">, Economy</span>
                                                        </span>
                                                       <!-- <i class="sprite-booking-engine ico-be-arrow-down-grey-v2 icon-inside" id=""> </i>-->
                                                    </div>
                                                    <!-- <i class="sprite-booking-engine ico-be-arrow-down-grey-v2 icon-inside" id=""> </i> -->
                                                    <button class="btn btn-block search-btn" style="width: 50%; height: 40px;" onclick="getvalue()" id="flght_srch">Search</button>
                                                    <!-- <div style="display: none;" id="loader">
													<img src="/resources/images/spinner.gif" style="width: 3%;">
													</div> -->
													<span class="rederror"   id="responsemessage"> </span>
                                                </div>
                                            </div>

                                            <!-- ============================ Flight Tab End ============================= -->

                                            <!-- ============================ Hotel Tab ============================= -->
                                            <div class="tab-pane" id="tab_hotel">
                                                <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="top-list" style="margin-bottom: 5px;">
                                                                <ul class="nav nav-pills">
                                                                    <li class="active">
                                                                        <a href="#" data-toggle="pills">Domestic &amp; International Hotels</a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <div class="col-md-4" style="padding-left: 0px; padding-right: 0px;">
                                                    <!-- =============== -->
                                                    <div class="inner_tab_content_Hotel">
                                                        <select id="location_list" class="js-example-basic-multiple" style="width: 350px;" name="sourceId" onchange="$('#errSrcCity').empty();">
                                                                <option value="#">Select City, Location or Hotel Name</option>
                                                                <option value="#">Bangalore</option>
                                                                <option value="#">Bangalore</option>
                                                                <option value="#">Bangalore</option>
                                                                <option value="#">Bangalore</option>
                                                                <option value="#">Bangalore</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4" style="padding-left: 0px; padding-right: 0px;">
                                                    <input type="text" class="form-control" placeholder="Check-in" name="" style="width: 50%; float: left; height: 40px;">
                                                    <i class="fa fa-calendar D-font-icon icon-inside icon-calendar"></i>
                                                    <input type="text" class="form-control" placeholder="Check-out" name="" style="width: 50%; height: 40px;">
                                                    <i class="fa fa-calendar R-font-icon icon-inside icon-calendar"></i>
                                                </div>
                                                <div class="col-md-4" style="padding-left: 0px; padding-right: 0px;">
                                                    <!-- <input type="text" class="form-control" placeholder="People&Room" name="" style="width: 50%; float: left; height: 40px;"> -->
                                                    <div class="pax-details" id="rm_ppl">
                                                        <span class="txt-ellipses">
                                                            <span class="pplCount">1</span> People in <span class="rmCount">1</span><span class="room"> Room</span>
                                                        </span>
                                                       <!-- <i class="sprite-booking-engine ico-be-arrow-down-grey-v2 icon-inside" id=""> </i>-->
                                                    </div>
                                                    <button class="btn btn-block search-btn" style="width: 50%; height: 40px;">Search</button>
                                                </div>
                                            </div>
                                            <!-- ============================ Hotel Tab End ============================= -->

                                            <!-- ============================ Holiday Tab ============================= -->
                                            <div class="tab-pane" id="tab_holiday">
                                                <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="top-list" style="margin-bottom: 5px;">
                                                                <ul class="nav nav-pills">
                                                                    <li class="active">
                                                                        <a href="#one-way" data-toggle="pills">Search your Favorite Destination</a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <div class="col-md-6 col-md-offset-1" style="padding-left: 0px; padding-right: 0px;">
                                                    <!-- =============== -->
                                                    <div class="inner_tab_content_Holiday">
                                                        <select id="holiday_list" class="js-example-basic-multiple" style="width: 350px;" name="sourceId" onchange="$('#errSrcCity').empty();">
                                                                <option value="#">Where you want to go?</option>
                                                                <option value="#">Bangalore</option>
                                                                <option value="#">Bangalore</option>
                                                                <option value="#">Bangalore</option>
                                                                <option value="#">Bangalore</option>
                                                                <option value="#">Bangalore</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4" style="padding-left: 0px; padding-right: 0px;">
                                                    <button class="btn btn-block search-btn" style="width: 100%; height: 40px;">Search</button>
                                                </div>
                                            </div>
                                            <!-- ============================ Holiday Tab End============================= -->

                                            <!-- ============================ Bus Tab ============================= -->
                                           
												<jsp:include page="/WEB-INF/jsp/Agent/Travel/Bus/Home.jsp" />
                                            <!-- ============================ Bus Tab End ============================= -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- ===================== end main row =============== -->
                    </div><!-- =============end container============== -->
                </div><!-- =================end bg-content================= -->
            </div><!-- ==============end bg-holder full================= -->
        </div>
        <!-- END TOP AREA  -->

        <!-- TRAVELLERS DETAILS STARTS HERE  -->
      	 <div class="dropdown1" id="trav_menu" style="display: none;">
            <div class="t_menu">
                <div class="row">
                    <div class="col-md-7">
                        <input class="quantity" type="text" id="adult" name="quantity" value="1"  hidden="hidden" />
                        <span class="quantity" id="adult1" >1</span>&nbsp;<span>Adult(s)</span>
                    </div>
                    <div class="col-md-5">
                        <button class="incr-btn btn btn-sm" style="background: #0d30e8; color: #fff;" onclick="decreaseValue('adult','adult1');" ><i class="fa fa-minus" aria-hidden="true"></i></button>
                        <button class="incr-btn btn btn-sm" style="background: #0d30e8; color: #fff;" onclick="increaseValue('adult','adult1');" ><i class="fa fa-plus" aria-hidden="true"></i></button>
                    </div>
                </div>
                <hr class="t_divider">
                <div class="row">
                    <div class="col-md-7">
                        <input class="quantity" type="text" id="childs" name="quantity" value="0"  hidden="hidden" />
                        <span class="quantity" id="childs1">0</span>&nbsp;<span>Child</span>&nbsp;<small>(2-12 YRS)</small>
                    </div>
                    <div class="col-md-5">
                        <button class="incr-btn btn btn-sm" style="background: #0d30e8; color: #fff;" onclick="decreaseValue('childs','childs1');" ><i class="fa fa-minus" aria-hidden="true"></i></button>
                        <button class="incr-btn btn btn-sm" style="background: #0d30e8; color: #fff;" onclick="increaseValue('childs','childs1');" ><i class="fa fa-plus" aria-hidden="true"></i></button>
                     </div>
                </div>
                <hr class="t_divider">
                <div class="row">
                    <div class="col-md-7">
                        <input class="quantity" type="text" name="quantity" value="0" id="infants" hidden="hidden" />
                        <span class="quantity" id="infants1">0</span>&nbsp;<span>Infant</span>&nbsp;<small>(Below 2 YRS)</small>
                    </div>
                    <div class="col-md-5">
                        <button class="incr-btn btn btn-sm" style="background: #0d30e8; color: #fff;" onclick="decreaseValue('infants','infants1');" ><i class="fa fa-minus" aria-hidden="true"></i></button>
                        <button class="incr-btn btn btn-sm" style="background: #0d30e8; color: #fff;" onclick="increaseValue('infants','infants1');" ><i class="fa fa-plus" aria-hidden="true"></i></button>
                    </div>
                </div>
                <hr class="t_divider">
                <div class="row">
                    <div class="col-md-12">
                   <label>Class</label>
               <select name="flight_pax" id="cabin"  tabindex="" onchange="changesclass()"class="form-control">
				<option selected="selected" value="Economy">Economy</option>
				<option value="ER">Premium Economy</option>
				<option value="B">Business</option>
			   </select>
                 
                    </div>
                </div>
            <br>
                <div class="row">
                    <div class="col-md-12">
                         <button class="btn btn-sm btn-danger pull-right" onclick="closedivtrevalinformation()">Done</button> 
                    </div>
                </div>
            </div>
        </div>
        <!-- TRAVELLERS DETAILS END HERE  -->
	
	<footer>
		<div class="container-fluid">
			<a href="AboutUs">About Us</a> <a href="#">Partner
				with us</a> <a href="#">Terms &amp; Conditions</a> <a
				href="#">Customer service</a> <a href="#">Grievance
				policy</a> <a href="#">Recharge Partners</a>
				   <div  class="span pull-left" "> 
				<!-- <a href="#"><img src="img/v-logo123.png" style="width: 100px;"></a>    -->
    	   </div>
    	   <div  class="span pull-right"> 
				<p>&copy; Copyright MSewa Software Pvt. Ltd.</p>  	
    	   </div>
		</div>
	</footer>
  
        <script>
            $( "#flightsrc" ).select2({
                theme: "bootstrap"
            });
            $( "#flightdest" ).select2({
                theme: "bootstrap"
            });
            $( "#location_list" ).select2({
                theme: "bootstrap"
            });
            $( "#bus_src_list" ).select2({
                theme: "bootstrap"
            });
            $( "#bus_des_list" ).select2({
                theme: "bootstrap"
            });
            $( "#holiday_list" ).select2({
                theme: "bootstrap"
            });
        </script>
        
        <!-- ================ Script for Flight traveller details ============================ -->
        <script>
            $(document).ready(
                function(){
                    $("#selTrav").click(function () {
                        $("#trav_menu").fadeToggle();
                    });

                });
        </script>

        <!-- ================ Script for Room traveller details ============================ -->
        <script>
            $(document).ready(
                function(){
                    $("#rm_ppl").click(function () {
                        $("#htl_menu").fadeToggle();
                    });

                });
        </script>



        <!-- <script src="js/jquery.js"></script> -->
      <!--   <script src="js/bootstrap.js"></script>
        <script src="js/slimmenu.js"></script>
     	<script src="js/bootstrap-datepicker.js"></script>
      	<script src="js/bootstrap-timepicker.js"></script>
     	<script src="js/nicescroll.js"></script>    
       	<script src="js/typeahead.js"></script>
        <script src="js/custom.js"></script>
        <script src="js/travel.js"></script> -->

      <!-- <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
      <script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script> -->
  
<%-- 
	<script src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/bootstrap.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/slimmenu.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/bootstrap-datepicker.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/bootstrap-timepicker.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/nicescroll.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/typeahead.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/custom.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/travel.js"></script> --%>
		<script src="https://harvesthq.github.io/chosen/chosen.jquery.js"></script>
</body>



<script>
	function setsrcanddestination() {

		var traveldata = document.getElementById("traveldataval").value;
		var travel_code = new Array();

		$(".js-example-basic-multiple").select2();
		travel_code = traveldata.split("!");

		var travel_name = new Array();
		travel_name = travel_code[1].split("@");

		var sourcecode = new Array();
		sourcecode = travel_code[0].split("#");
		
		var airportName = new Array();
		airportName = travel_code[0].split("&");
		
		document.getElementById("flightsrc").innerHTML = "";
		for (i = 0; i < travel_name.length; i++) {
			var option = document.createElement("option");

			option.text = travel_name[i] + ' ('+sourcecode[i]+')';
			option.value = sourcecode[i];

			var select = document.getElementById("flightsrc");
			if (i == 0) {
				option.text = "Select Source";
				option.value = "#";
				select.add(option);

			}

			select.add(option);
			console.log("dd");
		}

		document.getElementById("flightdest").innerHTML = "";
		for (i = 0; i < travel_name.length; i++) {
			var option = document.createElement("option");

			option.text = travel_name[i]+ ' ('+sourcecode[i]+')';
			option.value = sourcecode[i];

			var select = document.getElementById("flightdest");
			if (i == 0) {
				option.text = "Select Destination";
				option.value = "#";
				select.add(option);

			}

			select.add(option);

		}

		$('.chosen-select').chosen();

		 var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1; //January is 0!

		var yyyy = today.getFullYear();
		if(dd<10){
		    dd='0'+dd;
		} 
		if(mm<10){
		    mm='0'+mm;
		} 
		var today = dd+'/'+mm+'/'+yyyy;
		document.getElementById("date").value = today; 
	}
	
	
	
	function getvalue() 
	{
		var spinnerUrl = "Please wait <img src='/resources/images/spinner.gif' style='width:25px;'>";
		
		
		$("#trav_menu").hide();
		document.getElementById("errormsgsrc").innerHTML="";
		document.getElementById("responsemessage").innerHTML="";
	    document.getElementById("errormsgdest").innerHTML="";
		var flightdest = document.getElementById("flightdest");
		var flightdestselect = flightdest.options[flightdest.selectedIndex].value;

		var flightsrc = document.getElementById("flightsrc");
		var flightsrcselect = flightsrc.options[flightsrc.selectedIndex].value;
		var date = document.getElementById("date").value;
		
	//	var tripType = document.getElementById("tripType").value;
		var cabin = document.getElementById("cabin").value;
		var adults = document.getElementById("adult").value;
		var childs = document.getElementById("childs").value;
		var infants = document.getElementById("infants").value;
		var returndate = document.getElementById("returndate").value;
		 
		var valid=true;
		
		 if(flightsrcselect.length <=1)
			{
// 			$("#loader").hide();
			document.getElementById("errormsgsrc").innerHTML="Please Select Source";
			valid=false;
			}
		  if(flightdestselect.length <=1)
			{
// 			$("#loader").hide();
			document.getElementById("errormsgdest").innerHTML="Please Select Destination";
			valid=false;
			}
	 
			if(valid==true)
				{
				$("#flght_srch").addClass("disabled");
// 				$("#flght_srch").html(spinnerUrl);
				$("#loading_flight").modal({backdrop: false});
				if(returndate.length <=1)
				{
					
					$.ajax({
						type : "POST",
						contentType : "application/json",
						url : "${pageContext.request.contextPath}/Agent/Travel/Flight/OneWay",
						data : JSON.stringify({
							"origin" : "" +flightsrcselect + "",

							"destination" : "" +flightdestselect + "",

							"beginDate" : "" +date + "",

							"engineIDs" : "" +"AYTM00011111111110002" + "",

							"tripType" : "" +"OneWay" + "",

							"cabin" : "" +cabin + "",

							"adults" : "" +adults + "",

							"childs" : "" + childs+ "",

							"infants" : "" +infants + "",
							"endDate":"" +date + "",
							"traceId" : "" + "AYTM00011111111110002"+ "",
 
						}),
						success : function(response) {
							 
					 
							 var d=response.split("#");
							 if(d[0]==="S00")
							 {
						 window.location = "OneWay";
					 }
				 else
				 {
					 $("#loader").hide();
					 $("#loading_flight").hide();
					 $("#flght_srch").removeClass("disabled");
					 
					 document.getElementById("responsemessage").innerHTML=""+d[1];	
					 
							
				 }
			}
					});
				}
				else
					{

					$.ajax({
						type : "POST",
						contentType : "application/json",
						url : "${pageContext.request.contextPath}/Agent/Travel/Flight/OneWay",
						data : JSON.stringify({
							"origin" : "" +flightsrcselect + "",

							"destination" : "" +flightdestselect + "",

							"beginDate" : "" +date + "",

							"engineIDs" : "" +"AYTM00011111111110002" + "",

							"tripType" : "" +"RoundTrip" + "",

							"cabin" : "" +cabin + "",

							"adults" : "" +adults + "",

							"childs" : "" + childs+ "",

							"infants" : "" +infants + "",
							"endDate":"" +returndate + "",
							"traceId" : "" + "AYTM00011111111110002"+ "",
 
						}),
						success : function(response) {
 
							 var d=response.split("#");
					 if(d[0]==="S00")
					 {
						 if(d[1]==="DomesticRoundway")
							 {
							 window.location = "RoundWay";
							 }
						 else
							 {
							 window.location = "internationalRoundWay";
							 }
						
					 }
				 else
				 {
					 $("#loader").hide();
					$("#loading_flight").hide();
					 $("#flght_srch").removeClass("disabled");
					
					 document.getElementById("responsemessage").innerHTML=""+d[1];	
					 
							
				 }
			  }
					});
					
					}

	}
	}
	
	function returndate()
	{
		 $("#returntrip").show();
	}

	 function increaseValue(id,idset)
   	 {
        	  var value = parseInt(document.getElementById(""+id).value, 10);
        	  if(value<=4)
        		  {
        		 var d = parseInt(document.getElementById("totalCounttrevaler").innerHTML);
        		 var c=parseInt(d);
            	 c++;
            	document.getElementById("totalCounttrevaler").innerHTML=""+c;
        	  value = isNaN(value) ? 0 : value;
        	  value++;
        	  document.getElementById(""+idset).innerHTML = ""+value;
        	  document.getElementById(""+id).value = ""+value;
        	
        	 
        	
        		  }
        	}

        	function decreaseValue(id,idset) 
        	{
        	  var value = parseInt(document.getElementById(""+id).value, 10);
        	 if(value>0)
   		  {
   		 
   	
      		var d = parseInt(document.getElementById("totalCounttrevaler").innerHTML);
     		 var c=parseInt(d);
         	 c--;
         	document.getElementById("totalCounttrevaler").innerHTML=""+c;
   	
   		  }
        	  value = isNaN(value) ? 0 : value;
        	  value < 1 ? value = 1 : '';
        	  value--;
        	 
        	  document.getElementById(""+idset).innerHTML = ""+value;
        	  document.getElementById(""+id).value = ""+value;
        	
        	 
        	}
        	function changesclass() 
        	{
        		var d =document.getElementById("cabin").value;
        	document.getElementById("flight_cls").innerHTML=", "+d;
        	}
</script>
<script src="https://harvesthq.github.io/chosen/chosen.jquery.js"></script>
</html>