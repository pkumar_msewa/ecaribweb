<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="tab-content">
	<div id="tab_bustkt" class="tab-pane fade in active">
		<div class="all-tkt-content">
			<div class="tkt-head">
				<div class="row">
					<div class="col-md-12">
						
						<table class="table table-responsive">
							<thead>
								<tr>
									<th>Ticket Description</th>
									<th>Booking Date</th>
									<th><center>Flight Status</center></th>
									<th>Payment Status</th>
									<th>Transaction Ref No.</th>
									<th>Booking Ref No.</th>
									<th>Price</th>
									<th>View Ticket</th>
									
								</tr>
							</thead>
							<tbody>
							<c:forEach items="${details}" var="var">
								<tr>
										<td><span class="fa fa-plane fa-2x"></span>&nbsp;
											${var.source} - ${var.destination} <c:if
												test="${var.tripType=='Roundway'}">
												<br>
												<span class="fa fa-plane fa-rotate-180 fa-2x"></span>&nbsp;${var.destination} - ${var.source}
									</c:if></td>
										<td>${var.txnDate}</td>
										<td><center>
												<span class="label label-success">${var.status}</span><br>You
												will receive the ticket in your email.
											</center></td>
										<td><center>
												<span class="label label-success">${var.txnStatus}</span>
											</center></td>
										<td>${var.transactionRefNo}</td>
										<td>${var.bookingRefId}</td>
										<td>Rs. ${var.totalFare}</td>
										<td><c:choose>
												<c:when test="${var.status == 'Booked'}">
													<form action="${pageContext.request.contextPath}/Admin/VpayQwikAgentFlightTicketPdf" target="new" method="post">
														<input type="hidden" id="flightTicketId"
															name="flightTicketId" value="${var.flightTicketId}" />
														<button id="Pdf_ticket${loopCount.count}" type="submit"
															style="background-color: #5cb85c; color: #fff;">Download</button>
													</form>
												</c:when>
												<c:otherwise>
												NA
											</c:otherwise>
											</c:choose></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<%-- <div class="all-tkt-body">
				<c:forEach items="${details}" var="var">
					<div class="row user-tickets">
						<div class="col-md-12">
							<div class="col-md-5">
								<div class="col-md-3" style="padding-left: 5px;">
									<div class="col-md-2" style="padding-left: 0px;">
										<img
											src="${pageContext.request.contextPath}/resources/images/User/travel/img/airline/plane.png">
									</div>
									<div class="col-md-10" style="padding: 0px;">
										<span class="trv trv-src" style="font-size: 20px;">${var.source}</span> <span>-</span> <span
											class="trv trv-dest" style="font-size: 20px;">${var.destination}</span><br> <span
											class="trv trv-doj">${var.oneWay.departureDate}</span>
									</div>
								</div>
								<div class="col-md-3">
										<span class="label label-success">${var.txnDate}</span><br>
										<span class="trv trv-dobk"></span> <span class="trv trv-tim"></span>
								</div>
								<div class="col-md-3">
										<span class="label label-success">${var.status}</span><br>
										<span class="trv bked-msg">You will receive the ticket
											in your email.</span>
								</div>
							</div>
							<div class="col-md-5">
								<div class="col-md-3">
									<span class="label label-success">${var.txnStatus}</span><br>
									<span class="trv trv-dobk"></span> <span class="trv trv-tim"></span>
								</div>
								<div class="col-md-4">
										<span class="trv"><b>${var.transactionRefNo}</b></span><br>
										<span class="trv trv-dobk"></span> <span class="trv trv-tim"></span>
								</div>
								
								<div class="col-md-4">
										<span class="trv"><b>${var.bookingRefId}</b></span><br>
										<span class="trv trv-dobk"></span> <span class="trv trv-tim"></span>
								</div>
							</div>
							<div class="col-md-2">
									<span class="trv trv-prc">Rs.</span> <span class="trv trv-prc">${var.totalFare}</span>
									<form
										action="${pageContext.request.contextPath}/User/Travel/Bus/getSingleTicketTravellerDetails"
										method="get">

										<input type="hidden" name="emtTxnId" value="${var.mdexTxnRefNo}">
<!-- 										<button type="submit">View</button> -->
									</form>

							</div>
						</div>
					</div>
				</c:forEach>
				<hr>
			</div> --%>
		</div>
		<br>
	</div>
</div>
<style>
.fa-plane{
    color: #1388cc;
    }
</style>