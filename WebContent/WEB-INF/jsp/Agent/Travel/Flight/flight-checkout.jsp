<!DOCTYPE HTML>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<html>
<head>
<title>Check Out</title>
 <sec:csrfMetaTags />
<meta content="text/html;charset=utf-8" http-equiv="Content-Type">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'
	type="image/png" />
<link rel="stylesheet" href="/resources/css/new_css/cloud-admin.css">
<link rel="stylesheet" href="/resources/css/font-awesome.min.css">
<link rel="stylesheet" href="/resources/css/new_css/default.css">
<link rel="stylesheet" href="/resources/css/new_css/responsive.css">
<link rel="stylesheet"
	href="/resources/css/new_css/uniform.default.min.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/bootstrap-chosen.css">
<!--- GOOGLE FONTS - -->
<link
	href='https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700'
	rel='stylesheet' type='text/css'>
<link
	href='https://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600'
	rel='stylesheet' type='text/css'>
<!-- /GOOGLE FONTS -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/travel_css/bootstrap.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<!--<link rel="stylesheet" href="css/font-awesome.css">-->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/travel_css/icomoon.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/travel_css/styles.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/travel_css/mystyles.css">
<script type="text/javascript" 
    src="<c:url value="/resources/js/Agent/Agentdetail.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/Agent/Agentdetails.js"/>"></script>
<!-- <link rel="stylesheet" href="css/mystyles.css">   -->

<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css" />


<style>
.rederror {
	font-size: 12px;
	color: red;
}

.age {
	width: 52px;
}
.modal-content {
			border-radius: 0;
			border: 2px solid #efefef;
			width:380px;
		}
		#payModal .modal-body h4 {
			font-size: 19px;
		}
		.modal.fade .modal-dialog {
	      -webkit-transform: scale(0.1);
	      -moz-transform: scale(0.1);
	      -ms-transform: scale(0.1);
	      transform: scale(0.1);
	      top: 300px;
	      opacity: 0;
	      -webkit-transition: all 0.3s;
	      -moz-transition: all 0.3s;
	      transition: all 0.3s;
	  }
	
	  .modal.fade.in .modal-dialog {
	      -webkit-transform: scale(1);
	      -moz-transform: scale(1);
	      -ms-transform: scale(1);
	      transform: scale(1);
	      -webkit-transform: translate3d(0, -300px, 0);
	      transform: translate3d(0, -300px, 0);
	      opacity: 1;
	  }

</style>

<script type="text/javascript">
$(function() {
		  $("input[name='date']").datepicker({
			  format : "yyyy/mm/dd",
			 
		}).on('change', function() {
			$('.datepicker').hide();
		});  
	});
	

</script>

</head>


<body style="background: #efefef;" onload="setDetail()">
	<input type="hidden" value="${checkoutDeatil}" id="checkoutDeatil">

	<input type="hidden" value="${adults}" id="adults">
	<input type="hidden" value="${childs}" id="childs">
	<input type="hidden" value="${infants}" id="infants">

	<jsp:include page="/WEB-INF/jsp/Agent/AHeader.jsp" />

	<br>

	<div class="container-fluid chkout-wrp">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="col-md-8 col-sm-8">

					<!-- Customer Details =========================-->
					<div class="panel panel-default">
						<div class="panel-heading">
							<img
								src="${pageContext.request.contextPath}/resources/images/new_img/bag.png"
								class="chkout-img"> <span><b>Travellers Details</b></span>
						</div>
						<div class="panel-body">
							<div id="container">
								<!-- Adult -->
								<form action="">

									<c:forEach items="${adultsarr}" varStatus="loopCount">
										<div class='dtls-wrap' style='margin-bottom: 20px;'>
											<div class='col-md-2 col-sm-2'>
												<span><b>Adult ${loopCount.count} </b></span><span> </span>
											</div>
											<div class='col-md-2 col-sm-2'>
												<div class='form-group'>
													<select class='form-control title'
														id="useradultsgender${loopCount.count}">
														<option>Mr</option>
														<option>Ms</option>
														<option>Mrs</option>
													</select>
													<p class="rederror" id="erra_title${loopCount.count}"></p>
												</div>
											</div>
											<div class='col-md-8 col-sm-8'>
												<div class='col-md-6 col-sm-6'>
													<div class='form-group'>
														<input type='text' class='form-control' value=""
															id='userfirstnameadults${loopCount.count}'
															placeholder='Enter First Name'
															onkeypress="$('#erra_fName${loopCount.count}').empty();" maxlength="25">
														<p class="rederror" id="erra_fName${loopCount.count}"></p>
													</div>
												</div>
												<div class='col-md-6 col-md-6'>
													<div class='form-group'>
														<input type='text' class='form-control' value=""
															id='userlastnameadults${loopCount.count}'
															placeholder='Enter Last Name'
															onkeypress="$('#erra_lName${loopCount.count}').empty();" maxlength="25">
														<p class="rederror" id="erra_lName${loopCount.count}"></p>
													</div>
												</div>
											</div>
										</div>
									</c:forEach>

									<c:forEach items="${childsarr}" varStatus="loopCount">
										<div class='dtls-wrap' style='margin-bottom: 20px;'>
											<div class='col-md-2 col-sm-2'>
												<span><b>Child ${loopCount.count}</b></span><span> </span>
											</div>
											<div class='col-md-2 col-sm-2'>
												<div class='form-group'>
													<select class='form-control title'
														id="userchildsgender${loopCount.count}">
														<option>Mr</option>
														<option>Ms</option>
														<option>Mrs</option>
													</select>
													<p class="rederror" id="errc_title${loopCount.count}"></p>
												</div>
											</div>
											<div class='col-md-8 col-sm-8'>
												<div class='col-md-6 col-sm-6'>
													<div class='form-group'>
														<input type='text' class='form-control'
															id='userfirstnamechilds${loopCount.count}' value=""
															placeholder='Enter First Name'
															onkeypress="$('#errc_fName${loopCount.count}').empty();" maxlength="25">
														<p class="rederror" id="errc_fName${loopCount.count}"></p>
													</div>
												</div>
												<div class='col-md-6 col-md-6'>
													<div class='form-group'>
														<input type='text' class='form-control' value=""
															id='userlastnamechilds${loopCount.count}'
															placeholder='Enter Last Name'
															onkeypress="$('#errc_lName${loopCount.count}').empty();" maxlength="25">
														<p class="rederror" id="errc_lName${loopCount.count}"></p>
													</div>
												</div>
											</div>
										</div>
									</c:forEach>

									<c:forEach items="${infantsarr}" varStatus="loopCount">
										<div class='dtls-wrap' style='margin-bottom: 20px;'>
											<div class='col-md-2 col-sm-2'>
												<span><b>Infant ${loopCount.count}</b></span><span> </span>
											</div>
											<div class='col-md-2 col-sm-2'>
												<div class='form-group'>
													<select class='form-control title'
														id="userinfantsgender${loopCount.count}">
														<option>Mr</option>
														<option>Ms</option>
														<option>Mrs</option>
													</select>
													<p class="rederror" id="erri_title${loopCount.count}"></p>
												</div>
											</div>
											<div class='col-md-8 col-sm-8'>
												<div class='col-md-6 col-sm-6'>
													<div class='form-group'>
														<input type='text' class='form-control' value=""
															id='userfirstnameinfants${loopCount.count}'
															placeholder='Enter First Name'
															onkeypress="$('#erri_fName${loopCount.count}').empty();" maxlength="25">
														<p class="rederror" id="erri_fName${loopCount.count}"></p>
													</div>
												</div>
												<div class='col-md-6 col-md-6'>
													<div class='form-group'>
														<input type='text' class='form-control' value=""
															id='userlastnameinfants${loopCount.count}'
															placeholder='Enter Last Name'
															onkeypress="$('#erri_lName${loopCount.count}').empty();" maxlength="25">
														<p class="rederror" id="erri_lName${loopCount.count}"></p>
													</div>
												</div>
												<div class='col-md-6 col-md-6'>
													<div class='form-group'>
														<input type="text" class="form-control"
															placeholder="Date of birth" name="date" readonly style="cursor:pointer;"
															id="infantsdob${loopCount.count}"
															onchange="$('#erri_dob${loopCount.count}').empty();">
														<p class="rederror" id="erri_dob${loopCount.count}"></p>
													</div>
												</div>
											</div>
										</div>
									</c:forEach>

									<div class='row'>
										<div class='col-md-12 col-sm-12'>
											<span><b>Contact Details</b></span> <br>
											<div class='col-md-6 col-sm-6'>
												<div class='form-group'>
													<input type='email' class='form-control' id='useremailid'
														value="" placeholder='Enter Your Email ID'
														onkeypress="$('#pesengerdeatilemail').html('');" maxlength="75">
													<p style='color: red;' id='pesengerdeatilemail'></p>
													<span class='msg'>Your ticket will be sent to this
														email address</span>
												</div>
											</div>
											<div class='col-md-6 col-sm-6'>
												<div class='form-group'>
													<input type='text' class='form-control numeric'
														id='usermobilenumber' value=""
														onkeypress="$('#pesengerdeatilmobile').html(''); return isNumberKey(event);"
														placeholder='Enter Your Mobile Number' maxlength="10">
													<p style='color: red;' id='pesengerdeatilmobile'></p>
													<span class='msg'>Your Mobile number will be used
														only for sending flight related communication.</span>

													<p class='rederror' id='responsemessage'></p>

													<div style='display: none;' id='loader'>
														<img src='/resources/images/spinner.gif'
															style='width: 10%;'>
													</div>

												</div>
											</div>

										</div>
									</div>

									<div class='panel-group'>
										<div class='panel panel-default'>
											<div class='panel-heading'>
												<h4 class='panel-title'>
													<a data-toggle='collapse' href='#collapse1'>Payment</a>
												</h4>
											</div>
											<div id='collapse1' class='panel-collapse collapse'>
												<div class='panel-body'>
													<div class='row'>
														<div class='col-md-12 col-sm-12'>
															<div class='row' style='margin-top: 50px;'>
																<div class='col-md-12'>
																	<%--  <label class='radio-inline'><input type='radio' id='vnetId' name='optradio' value='vnet'><img src='${pageContext.request.contextPath}/resources/images/User/travel/img/Flight/VNET.png' alt='Vnet' style='width: 120px; margin-top: -10px;'></label>
					 <label class='radio-inline'><input type='radio' id='ebsId' name='optradio' checked='checked' value='ebs'><img src='${pageContext.request.contextPath}/resources/images/User/travel/img/Flight/EBS.png' alt='Others' style='width: 120px; margin-top: -10px;'></label> --%>
																	<label class='radio-inline'><input type='radio'
																		id='walletId' name='optradio' value='wallet'><img
																		src='${pageContext.request.contextPath}/resources/images/User/travel/img/Flight/Wallet.png'
																		alt='Wallet' style='width: 120px; margin-top: -10px;'></label>
																</div>
																<br>
																<!-- <button class='btn btn-primary contnu' style='margin-left: 173px; margin-top: 19px; width: 119px; height: 43px;' onclick='openpaymentgateselection()' id='paymentbtn' type="button">Continue</button> -->
																<button type="button" class="btn btn-primary" onclick='payAmount()' style='margin-left: 173px; margin-top: 19px; width: 119px; height: 43px;'>
																Continue</button>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>

						</div>
					</div>

				</div>

<%-- 
	<div id="payModal" role="dialog" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<center id="loadAmt" class="alert alert-success"></center>
					<center id="aAmount" class="alert alert-success"></center>
					<center><label id="common_error_msg" class="alert alert-danger"></label></center>
				</div>
				<div>
				<button class='btn btn-primary contnu' style='margin-left: 173px; margin-top: 19px; width: 119px; height: 43px;' onclick='openpaymentgateselection()' id='paymentbtn' type="button">
				Continue</button>
				</div>
			</div>
		</div>
	</div> --%>
	
<div id="payModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-sm">
  <!-- Modal content-->
    	<div class="modal-content">
	      <div class="modal-header" style=" height: 61px;">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        
	       <center><h4><img src="${pageContext.request.contextPath}/resources/images/new_img/payment.png" class="chkout-img-price">&nbsp;<b>Payment</b></h4></center>
	      </div>
	      <div class="modal-body panel-heading" >
	        <div class="row">
	        	<div class="col-md-12">
	        		<div class="col-md-6">
	        			<div>
	        				<h4><b>Wallet Balance:</b></h4>
	        			</div>
	        			<div id="loadBal">
	        				<h4 ><b>Amount to load:</b></h4>
	        			</div>
	        			<div>
	        				<h4><b>Payable Amount:</b></h4>
	        			</div>
	        		</div>
	        		<div class="col-md-6">
	        			<div>
	        				<h4><i class="fa fa-inr"></i>&nbsp;<span  id="aAmount"></span></h4>
	        			</div>
	        			<div>
	        				<span id="loadAmt2"><h4 ><i  class="fa fa-inr"></i>&nbsp;<span id="loadAmt"></span></h4></span>
	        			</div>
	        			<div>
	        				<h4><i class="fa fa-inr"></i>&nbsp;<span id="totalAmt" ></span></h4>
	        			</div>
	        		</div>
	        	</div>
	        </div>
	        <hr>
	        <div class="row">
	        		<div class="col-md-6">
	        			<center><button class='btn btn-primary contnu' style='width: 119px;' onclick='openpaymentgateselection()' id='paymentbtn' type="button">
				Continue</button></center>
	        			<!-- <button class="btn btn-primary">Continue</button> -->
	        		</div>
	        		<div class="col-md-6">
	        			<button class="btn btn-danger" style="width: 119px;" data-dismiss="modal">Cancel</button>
	        		</div>
	        	</div>
	      </div>
	    </div>
	    
  </div>
</div>
				<!-- CheckOut Receipt ============================= -->
				<div class="col-md-4 col-sm-4">
					<div class="panel panel-default">
						<div class="panel-heading">
							<img
								src="${pageContext.request.contextPath}/resources/images/new_img/price.png"
								class="chkout-img-price"> <span><b>Trip Summary</b></span>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="hpadding30 margtop30"
									style="padding: 0 30px; margin-top: 30px !important;">
									<!-- GOING TO -->

									<c:forEach items="${checkoutDeatilForLGS}" var="datas">

										<div>
											<div class="wh33percent left size12 bold dark"
												style="width: 67%; color: #333; font-size: 12px; font-weight: 700; float: left;">
												<span class="chkout-city" id="source">${datas.origin}
												</span>
											</div>

											<div class="wh33percent right textright size12 bold dark"
												style="width: 33%; color: #333; font-size: 12px; font-weight: 700; margin: 0 auto; text-align: right; float: left;">
												<span class="chkout-place" id="destination">${datas.destination}
												</span>
											</div>
											<div class="clearfix" style="float: none; clear: both;"></div>

											<div class="wh33percent left"
												style="width: 33%; float: left;">
												<div class="fcircle"
													style="width: 39px; height: 39px; border: 2px solid #ebebeb; background: #fff; -webkit-border-radius: 100px; -moz-border-radius: 100px; border-radius: 100px; position: relative; z-index: 100;">
													<span class="fdeparture"
														style="width: 25px; height: 21px; background: url(${pageContext.request.contextPath}/resources/images/new_img/departure.png) no-repeat; display: block; margin: 6px 0px 0px 4px;"></span>
												</div>
											</div>

											<div class="wh33percent left"
												style="width: 33%; float: left; text-align: center; text-align: -webkit-center; margin: 0 auto;">
												<div class="fcircle center"
													style="width: 39px; height: 39px; border: 2px solid #ebebeb; background: #fff; -webkit-border-radius: 100px; -moz-border-radius: 100px; border-radius: 100px; position: relative; z-index: 100;">
													<span class="fstop"
														style="width: 25px; height: 21px; background: url(${pageContext.request.contextPath}/resources/images/new_img/clock1.png) no-repeat; display: block; margin: 5px 0px 0px 5px;"></span>
												</div>
											</div>

											<div class="wh33percent right"
												style="width: 13%; float: right;">
												<div class="fcircle right"
													style="width: 39px; height: 39px; border: 2px solid #ebebeb; background: #fff; -webkit-border-radius: 100px; -moz-border-radius: 100px; border-radius: 100px; position: relative; z-index: 100;">
													<span class="farrival"
														style="width: 25px; height: 21px; background: url(${pageContext.request.contextPath}/resources/images/new_img/arrival.png) no-repeat; display: block; margin: 6px 0px 0px 4px;"></span>
												</div>
											</div>

											<div class="clearfix" style="float: none; clear: both;"></div>
											<div class="fline2px"
												style="width: 100%; height: 2px; background: #ebebeb; display: block; position: relative; top: -20px; z-index: 10;"></div>

											<div class="wh33percent left size12"
												style="width: 33%; font-size: 12px; float: left;">
												<span class="chkout-time" id="departuretime">${datas.departureTime}</span>
											</div>
											<div class="wh33percent left center size12"
												style="width: 33%; font-size: 12px; margin: 0 auto; text-align: center; float: left;">
												<span class="chkout-dur" id="durationflight">${datas.duration}
												</span>
											</div>
											<div class="wh33percent right textright size12"
												style="width: 33%; font-size: 12px; text-align: right; float: right;">
												<span class="chkout-time" id="arrivaltime">${datas.arrivalTime}</span>
											</div>
											<div class="clearfix" style="float: none; clear: both;"></div>
										</div>
										<br>
									</c:forEach>
									<!-- END OF GOING TO -->

									<br> <br> <span class="dark" style="color: #333;">Tickets:
										OneWay</span>


								</div>
								<div class="line3"></div>
								
								<div class="col-md-12">
									<div class="col-md-8"
										style="float: left; font-size: 14px; color: #333;">
										Trip Total :</div>

									<div class="col-md-4" align="right">
										<span class="right lred2 bold size18"
											style="float: right; font-size: 18px; font-weight: 800; color: #ff5a00;"
											id="grandtotal"> </span>
									</div>

								</div>
								<div class="col-md-12">
								<div class="col-md-8" style="float: left; font-size: 14px;  color: #333;">
								Convenience Fee :
								</div>
								
								<div class="col-md-4" align="right">
										<input type="text" name="convFee" id="convId" value="200"
											maxlength="3"
											style="width: 60%; margin-left: 40%;  font-size: 18px; font-weight: 800; color: #ff5a00;"
											onkeypress="return isNumberKey(event)" onkeyup="addConvFee()" />
									</div>
								</div>
								
								<!-- <div class="padding30" style="padding: 30px;">
									<span class="left size14 dark"
										style="float: left; font-size: 14px; color: #333;">Trip
										Total:</span> <span class="right lred2 bold size18"
										style="float: right; font-size: 18px; font-weight: 800; color: #ff5a00;"
										id="grandtotal"></span>
									<div class="clearfix" style="float: none; clear: both;"></div>
								</div>
								<div>
									<span class="left size14 dark"
										style="float: left; font-size: 14px; margin-top:  -11%;margin-left:  10%; color: #333;">
										Convenience Fee: </span>
										<span class="right lred2 bold size18"
										style="float: right; font-size: 18px; font-weight: 800; color: #ff5a00;"
										 ><input type="text" name="convFee" id="convId" value="200" maxlength="3"
										 style="width: 16%; margin-left: 66%;margin-top: -27%;"
										 onkeypress="return isNumberKey(event)"
 						            	onkeyup="addConvFee()"/></span>
								</div> -->
							</div>

						</div>
					</div>


					<div class="panel panel-default">
						<div class="panel-body">
							<div class="row">
								<div class="cpadding1" style="padding: 0px 30px 10px 50px;">
									<span class="icon-help"
										style="width: 24px; height: 24px; background: url(${pageContext.request.contextPath}/resources/images/new_img/icon-phone.png) no-repeat; display: block; position: absolute; float: left; left: 37px;  margin-top: 7px;"></span>
									<h3 class="opensans">Need Assistance?</h3>
									<p class="size14 grey" style="color: #999; font-size: 14px;">Our
										team is 24/7 at your service to help you with your booking
										issues or answer any related questions</p>
									<p class="ass_number">8025011300</p>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>



		</div>
	</div>

	<div class="modal fade" id="flightModal" role="dialog" hidden="hidden">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content" style="margin-top: 40%;margin-left: 100px;">

				<div class="modal-body">
					<center>
						<img
							src="${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/source-(tick).gif"
							style="width: 140px; margin-top: -91px;    margin-bottom: 10px;">
						<div
							style="background: white; margin-top: -20px; padding: 2.5% 2%;">
							<h4>
								<b>Your ticket is successfully booked.<br>
								<small></b></small>
							</h4>
							<center>
								<button type="button" id="payment_ok">OK</button>
							</center>
						</div>
					</center>
				</div>

			</div>

		</div>
	</div>


	<div id="errModel" role="dialog" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<center id="common_error_msg" class="alert alert-danger"></center>
					<%-- <center><label id="common_error_msg" class="alert alert-danger"></label></center> --%>
				</div>
			</div>
		</div>
	</div>


	<%-- 		<form method="POST"
			action="${pageContext.request.contextPath}/User/Travel/Flight/Process"
			id="myform">
			<input type="hidden" class="form-control numeric" id="amountgrand"
				name="amount" min="1" max="9999" size="10" /> <input type="hidden"
				class="form-control numeric" id="phone" name="phone" /> <input
				type="hidden" class="form-control numeric" id="email" name="email" />
			<input type="hidden" class="form-control numeric" id="address"
				name="address" value="dsfsdfsdfgsd" /> <input type="hidden"
				class="form-control numeric" id="name" name="name"
				value="${firstName}" /> <input type="hidden"
				class="form-control numeric" id="firstName" name="firstName"
				value="" /> <input type="hidden" class="form-control numeric"
				id="lastName" name="lastName" value="" /> <input type="hidden"
				class="form-control numeric" id="firstNamechild"
				name="firstNamechild" value="" /> <input type="hidden"
				class="form-control numeric" id="lastNamechild"
				name="lastNamechild" value="" /> <input type="hidden"
				class="form-control numeric" id="firstNameinfant"
				name="firstNameinfant" value="" /> <input type="hidden"
				class="form-control numeric" id="lastNameinfant"
				name="lastNameinfant" value="" />

		</form> --%>




	<form method="POST"
		action="${pageContext.request.contextPath}/Agent/Travel/Flight/ProcessSplitpayment"
		id="ProcessSplitpayment">
		<input type="hidden" class="form-control numeric" id="returnamount"
			name="returnamount" min="1" max="9999" size="10" /> <input
			type="hidden" class="form-control numeric" id="phone1" name="phone" />
		<input type="hidden" class="form-control numeric" id="email1"
			name="email" /> <input type="hidden" class="form-control numeric"
			id="address1" name="address" value="dsfsdfsdfgsd" /> <input
			type="hidden" class="form-control numeric" id="firstName1"
			name="firstName" /> <input type="hidden"
			class="form-control numeric" id="lastName1" name="lastName" /> <input
			type="hidden" class="form-control numeric" id="firstNamechild1"
			name="firstNamechild" /> <input type="hidden"
			class="form-control numeric" id="lastNamechild1" name="lastNamechild" />
		<input type="hidden" class="form-control numeric"
			id="firstNameinfant1" name="firstNameinfant" /> <input
			type="hidden" class="form-control numeric" id="lastNameinfant1"
			name="lastNameinfant" /> <input type="hidden"
			class="form-control numeric" id="useradultsgender1"
			name="useradultsgender" /> <input type="hidden"
			class="form-control numeric" id="userchildsgender1"
			name="userchildsgender" /> <input type="hidden"
			class="form-control numeric" id="userinfantsgender1"
			name="userinfantsgender" /> <input type="hidden"
			class="form-control numeric" id="infantDateOfBirth1"
			name="infantDateOfBirth" />
	</form>



	<div class="modal-body">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<form method="post" action="/Agent/Travel/Flight/PaymentProcess"
					id="myform">
					<div class="col-md-12">
						<!-- <form method="post"
																action="/User/LoadMoney/Process" class="form-inline" /> -->
						<div class="form-group">
							<c:out value="${msg}" default="" escapeXml="true" />
							<!-- <div class="col-md-12 col-sm-12 col-xs-12">
															<input type="number" id="amountVal" class="form-control numeric"
																 placeholder="Enter Amount" name="amount" onkeypress="clearvalue('error_amount')"
																min="1" max="9999" size="10" />
																<p class="error" id="error_amount" class="error"></p>
														</div> -->
						</div>
						<div class="form-group">
							<input id="vnetrd" type="hidden" class="form-control"
								name="useVnetWeb"
								style="width: 15px; float: left; margin-top: 12px;">
						</div>
						<input type="hidden" class="form-control numeric" id="amountgrand"
							name="amount" min="1" max="9999" size="10" /> <input
							type="hidden" class="form-control numeric" id="phone"
							name="phone" /> <input type="hidden"
							class="form-control numeric" id="email" name="email" /> <input
							type="hidden" class="form-control numeric" id="address"
							name="address" value="dsfsdfsdfgsd" /> <input type="hidden"
							class="form-control numeric" id="name" name="name"
							value="${firstName}" /> <input type="hidden"
							class="form-control numeric" id="firstName" name="firstName"
							value="" /> <input type="hidden" class="form-control numeric"
							id="lastName" name="lastName" value="" /> <input type="hidden"
							class="form-control numeric" id="firstNamechild"
							name="firstNamechild" value="" /> <input type="hidden"
							class="form-control numeric" id="lastNamechild"
							name="lastNamechild" value="" /> <input type="hidden"
							class="form-control numeric" id="firstNameinfant"
							name="firstNameinfant" value="" /> <input type="hidden"
							class="form-control numeric" id="lastNameinfant"
							name="lastNameinfant" value="" /> <input type="hidden"
							class="form-control numeric" id="useradultsgender"
							name="useradultsgender" /> <input type="hidden"
							class="form-control numeric" id="userchildsgender"
							name="userchildsgender" /> <input type="hidden"
							class="form-control numeric" id="userinfantsgender"
							name="userinfantsgender" /> <input type="hidden"
							id="flightType" name="flightType" value="Oneway" />
							 <input type="hidden"
							id="convFeeId" name="convFee" value="" />
							<input type="hidden"
							id="baseFareId" name="baseFare" value="" />
							<input type="hidden"
							id="totalAmtId" name="baseFare" value="" />
						<div class="row" style="margin-top: 40px;">
							<input type="hidden" class="form-control numeric"
								id="adultDateOfBirth" name="adultDateOfBirth" /> <input
								type="hidden" class="form-control numeric" id="childDateOfBirth"
								name="childDateOfBirth" /> <input type="hidden"
								class="form-control numeric" id="infantDateOfBirth"
								name="infantDateOfBirth" />


							<%-- <center>
															<div class="form-group">
																<button  onclick="bookflightwithpaymentgateway()" type="button"  class="btn"
																	style="background: #ff0000; color: #FFFFFF;">Continue</button>
															</div>
														</center> --%>
						</div>
						<!-- </form>  onclick="this.disabled=true"  -->
					</div>
				</form>
			</div>
		</div>
	</div>
	</div>

	<%-- <center>
						<!-- Modal for Add Money -->
						<div class="modal fade" id="loadMoneyModal" role="dialog">
							<div class="modal-dialog">

								<!-- Modal content-->
								<div class="modal-content rchrg_modal">
									<div class="modal-header addm_head">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<center>
											<h2 class="modal-title">
												<b>Add Money</b>
											</h2>
										</center>
									</div>
									<div class="modal-body">
										<div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12">
												
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>
						<!-- Modal for Add Money -->
					</center> --%>

	</div>

	<script type="text/javascript">
	var today = new Date();
	
	$(function() {
	$("#adultDateOfBirth").datepicker({
		startDate: today,
		format : "dd/mm/yyyy"
	}).on('change', function() {
		$('.datepicker').hide();
	});
	});
	
	$(function() {
		$("#childDateOfBirth").datepicker({
			startDate: today,
			format : "dd/mm/yyyy"
		}).on('change', function() {
			$('.datepicker').hide();
		});
		});
	
	$(function() {
		$("#infantDateOfBirth").datepicker({
			startDate: today,
			format : "dd/mm/yyyy"
		}).on('change', function() {
			$('.datepicker').hide();
		});
		});
</script>


	<script type="text/javascript">


		function bookflight()
		{
			var contextPath = "${pageContext.request.contextPath}";
			var spinnerUrl = "Please wait <img src="+contextPath+"'/resources/images/spinner.gif' style='width:25px;'>"
			
			document.getElementById("pesengerdeatilmobile").innerHTML="";
			document.getElementById("pesengerdeatilemail").innerHTML="";
			 document.getElementById("responsemessage").innerHTML="";
 
			var emailAddress=document.getElementById("useremailid").value;
			var mobileNumber=document.getElementById("usermobilenumber").value;
			
			var firstName="";
			var lastName="";
			
			var firstNamechild="";
			var lastNamechild="";
 
			var firstNameinfant="";
			var lastNameinfant="";
			
			
			
			var useradultsgender="";
			var userchildsgender="";
		    var userinfantsgender="";
			

			
			
			var adults = document.getElementById("adults").value;
			 var childs = document.getElementById("childs").value;
			 var infants = document.getElementById("infants").value;
		 
		    for (i=0;i<adults;i++){
				firstName+="~"+document.getElementById("userfirstnameadults"+i).value;
				 lastName+="@"+document.getElementById("userlastnameadults"+i).value; 
				 useradultsgender+="#"+document.getElementById("useradultsgender"+i).value; 
		    }

		    for (i=0;i<childs;i++){

		    	firstNamechild+="~"+document.getElementById("userfirstnamechilds"+i).value;
		    	lastNamechild+="@"+document.getElementById("userlastnamechilds"+i).value;  
		    	userchildsgender+="#"+document.getElementById("userchildsgender"+i).value; 
		    }

		    for (i=0;i<infants;i++){
		    	firstNameinfant+="~"+document.getElementById("userfirstnameinfants"+i).value;
		    	lastNameinfant+="@"+document.getElementById("userlastnameinfants"+i).value;  
		    	userinfantsgender+="#"+document.getElementById("userinfantsgender"+i).value; 
		    }
			
			 
			
			 
			var grandtotal=document.getElementById("grandtotal").innerHTML;
		
		 
					var atpos = emailAddress.indexOf("@");
				    var dotpos = emailAddress.lastIndexOf(".");
				   if(mobileNumber.length <=9)
						{
						 $("#loader").hide();
						document.getElementById("pesengerdeatilmobile").innerHTML="Please Enter 10 Digit Mobile Number";
						}
				     else if(atpos<1 || dotpos<atpos+2 || dotpos+2 >= emailAddress.length) {
				    	 $("#loader").hide(); 
						document.getElementById("pesengerdeatilemail").innerHTML="Please Enter Your Email";
				       
				    } else if(firstName.length<=4 || lastName.length<=4 )
				     {
				    	 $("#loader").hide(); 
						document.getElementById("pesengerdeatilemail").innerHTML="Please Enter Your Name Detail";
				       
				    } else{
				    	$("#walletPay").html(spinnerUrl);
						$("#walletPay").addClass("disabled");
			$.ajax({
						type : "POST",
						contentType : "application/json",
						url : "${pageContext.request.contextPath}/Agent/Travel/Flight/CheckOut",
						data : JSON.stringify({
							 "emailAddress" : "" + emailAddress+ "",
								"firstName" : "" + firstName+ "",
								"lastName" : "" + lastName+ "",
								
								"firstNamechild" : "" + firstNamechild+ "",
								"lastNamechild" : "" + lastNamechild+ "",
								"firstNameinfant" : "" + firstNameinfant+ "",
								"lastNameinfant" : "" + lastNameinfant+ "",
								"mobileNumber" : "" + mobileNumber+ "",
							    "useradultsgender" : "" + useradultsgender+ "",
							    "userchildsgender" : "" + userchildsgender+ "",
								"userinfantsgender" : "" + userinfantsgender+ "",
								"grandtotal" : "" + grandtotal+ "",
								
						}),
						success : function(response)
						{
							$("#walletPay").html("Pay With Wallet");
							$("#walletPay").removeClass("disabled");
							document.getElementById("pesengerdeatilmobile").innerHTML="";
							document.getElementById("pesengerdeatilemail").innerHTML="";
							 document.getElementById("responsemessage").innerHTML="";
				 
							 var d=response.split("@");
							 if(d[0]==="S00")
							 {
								 $("#loader").hide();
								 document.getElementById("responsemessage").innerHTML=""+d[1];	
							 }
							 else if(d[0]==="T01")
								 {
								 document.getElementById("firstName1").value=""+firstName;
								 document.getElementById("lastName1").value=""+lastName;
								 document.getElementById("returnamount").value=""+d[2];
								 /* document.getElementById("firstNamechild1").value=""+firstNamechild;
								 document.getElementById("lastNamechild1").value=""+lastNamechild;
								 document.getElementById("firstNameinfant1").value=""+firstNameinfant;
								 document.getElementById("lastNameinfant1").value=""+lastNameinfant; */
								 
								        document.getElementById("phone1").value=""+mobileNumber;
										 document.getElementById("email1").value=""+emailAddress;
								 $('#ProcessSplitpayment').submit();
								 }
						 else
						 {
							 $("#loader").hide();
							
							 document.getElementById("responsemessage").innerHTML=""+d[1];	
							 
									
						 }
		 

						}
					});
				    }
		}


		function bookflightwithpaymentgateway()
		{
			$("#loader").show();
			 
			document.getElementById("pesengerdeatilmobile").innerHTML="";
			document.getElementById("pesengerdeatilemail").innerHTML="";
			 document.getElementById("responsemessage").innerHTML="";
 
			var emailAddress=document.getElementById("useremailid").value;
			var mobileNumber=document.getElementById("usermobilenumber").value;
			
			var firstName="";
			var lastName="";
			
			var firstNamechild="";
			var lastNamechild="";
 
			var firstNameinfant="";
			var lastNameinfant="";

			var useradultsgender="";
			var userchildsgender="";
		    var userinfantsgender="";

		    var adults = document.getElementById("adults").value;
			 var childs = document.getElementById("childs").value;
			 var infants = document.getElementById("infants").value;
		 
		    for (i=0;i<adults;i++){
				firstName+="~"+document.getElementById("userfirstnameadults"+i).value;
				 lastName+="@"+document.getElementById("userlastnameadults"+i).value; 
				 useradultsgender+="#"+document.getElementById("useradultsgender"+i).value; 
		    }

		    for (i=0;i<childs;i++){

		    	firstNamechild+="~"+document.getElementById("userfirstnamechilds"+i).value;
		    	lastNamechild+="@"+document.getElementById("userlastnamechilds"+i).value;  
		    	userchildsgender+="#"+document.getElementById("userchildsgender"+i).value; 
		    }

		    for (i=0;i<infants;i++){
		    	firstNameinfant+="~"+document.getElementById("userfirstnameinfants"+i).value;
		    	lastNameinfant+="@"+document.getElementById("userlastnameinfants"+i).value;  
		    	userinfantsgender+="#"+document.getElementById("userinfantsgender"+i).value; 
		    }
			
			 
			 
			var grandtotal=document.getElementById("grandtotal").innerHTML;
		
		 
					var atpos = emailAddress.indexOf("@");
				    var dotpos = emailAddress.lastIndexOf(".");
				   if(mobileNumber.length <=9)
						{
						 $("#loader").hide();
						document.getElementById("pesengerdeatilmobile").innerHTML="Please Enter 10 Digit Mobile Number";
						}
				     else if(atpos<1 || dotpos<atpos+2 || dotpos+2 >= emailAddress.length) {
				    	 $("#loader").hide(); 
						document.getElementById("pesengerdeatilemail").innerHTML="Please Enter Your Email";
				       
				    }else if(firstName.length<=4 || lastName.length<=4 )
				     {
				    	 $("#loader").hide(); 
						document.getElementById("pesengerdeatilemail").innerHTML="Please Enter Your Name Detail";
				       
				    } else{
	 
				    	 $("#loader").hide();
							 document.getElementById("firstName").value=""+firstName;
							 document.getElementById("lastName").value=""+lastName;
							 document.getElementById("firstNamechild").value=""+firstNamechild;
							 document.getElementById("lastNamechild").value=""+lastNamechild;
							 document.getElementById("firstNameinfant").value=""+firstNameinfant;
							 document.getElementById("lastNameinfant").value=""+lastNameinfant;
							 document.getElementById("phone").value=""+mobileNumber;
							 document.getElementById("email").value=""+emailAddress;
							 document.getElementById("useradultsgender1").value=""+useradultsgender;
							 document.getElementById("userchildsgender1").value=""+userchildsgender;
							 document.getElementById("userinfantsgender1").value=""+userinfantsgender;
							 
								  $('#myform').submit();
				    }
		}
		
	</script>
	<%-- <center><button class="btn btn-continue"onclick="bookflightwithpaymentgateway()">Continue</button>
 --%>

	<script src="https://harvesthq.github.io/chosen/chosen.jquery.js"></script>
	
</body>
</html>

<script>

function setDetail() {

	var traveldata = document.getElementById("checkoutDeatil").value;
	var fares = new Array();
	fares = traveldata.split("#");
	console.log("fares:"+fares);
	document.getElementById("source").innerHTML = fares[0];
	
	document.getElementById("destination").innerHTML = fares[1];
	
	document.getElementById("durationflight").innerHTML = fares[4];
	document.getElementById("arrivaltime").innerHTML = fares[5];
	document.getElementById("departuretime").innerHTML = fares[6];


	document.getElementById("grandtotal").innerHTML = fares[11];
	document.getElementById("amountgrand").value = fares[11];
	var grandTotal= parseInt(fares[11])+200;
	console.log("grand total..:"+grandTotal)
	$('#convFeeId').val(fares[12]);	
	$('#baseFareId').val(fares[13]);
	$('#totalAmtId').val(fares[11]);
	
	document.getElementById("grandtotal").innerHTML=grandTotal;
// 	$('#grandtotal').val(grandTotal);

	var adults = document.getElementById("adults").value;
	 var childs = document.getElementById("childs").value;
	 var infants = document.getElementById("infants").value;
	 
	 
/* var c="";
    for (i=0;i<adults;i++){

        c+=  "<div class='dtls-wrap' style='margin-bottom: 20px;'>"

        
        +"<div class='col-md-2 col-sm-2'>"
          +"<span><b>Adult </b></span><span> </span>"
       +"</div>"
        +"<div class='col-md-2 col-sm-2'>"
          +"<div class='form-group'>"
            +"<select class='form-control title' id='useradultsgender"+i+"'>"
              +"<option>Mr</option>"
              +"<option>Ms</option>"
              +"<option>Mrs</option>"
            +"</select>"
          +"</div>"
        +"</div>"
        +"<div class='col-md-8 col-sm-8'>"
          +"<div class='col-md-6 col-sm-6'>"
            +"<div class='form-group'>"
              +"<input type='text' class='form-control' id='userfirstnameadults"+i+"' placeholder='Enter First Name'>"
            +"</div>"
          +"</div>"
          +"<div class='col-md-6 col-md-6'>"
            +"<div class='form-group'>"
              +"<input type='text' class='form-control' id='userlastnameadults"+i+"' placeholder='Enter Last Name'>"
            +"</div>"
         +"</div>"
         +"<div class='col-md-6 col-md-6'>"
         +"<div class='form-group'>"
           +"<input type='text' class='form-control' id='adultDateOfBirth"+i+"' placeholder='Date Of Birth'>"
           +"<span class='msg'> As 11-06-1993</span>"
           +"</div>"
     	 +"</div>"
        +"</div>"
    
  +"</div>";
    }
    
    
    
    
    for (i=0;i<childs;i++){

        c+=  "<div class='dtls-wrap' style='margin-bottom: 20px;'>"

        
        +"<div class='col-md-2 col-sm-2'>"
          +"<span><b>Child </b></span><span> </span>"
       +"</div>"
        +"<div class='col-md-2 col-sm-2'>"
          +"<div class='form-group'>"
            +"<select class='form-control title' id='userchildsgender"+i+"'>"
              +"<option>Mr</option>"
              +"<option>Ms</option>"
              +"<option>Mrs</option>"
            +"</select>"
          +"</div>"
        +"</div>"
        +"<div class='col-md-8 col-sm-8'>"
          +"<div class='col-md-6 col-sm-6'>"
            +"<div class='form-group'>"
              +"<input type='text' class='form-control' id='userfirstnamechilds"+i+"' placeholder='Enter First Name'>"
            +"</div>"
          +"</div>"
          +"<div class='col-md-6 col-md-6'>"
            +"<div class='form-group'>"
              +"<input type='text' class='form-control' id='userlastnamechilds"+i+"' placeholder='Enter Last Name'>"
            +"</div>"
         +"</div>"
         +"<div class='col-md-6 col-md-6'>"
         +"<div class='form-group'>"
           +"<input type='text' class='form-control' id='childDateOfBirth"+i+"' placeholder='Date Of Birth'>"
           +"<span class='msg'> As 11-06-1993</span>"
         +"</div>"
     	 +"</div>"
        +"</div>"
    
  +"</div>";
    }
    
    
    
    for (i=0;i<infants;i++){

        c+=  "<div class='dtls-wrap' style='margin-bottom: 20px;'>"

        
        +"<div class='col-md-2 col-sm-2'>"
          +"<span><b>Infant </b></span><span> </span>"
       +"</div>"
        +"<div class='col-md-2 col-sm-2'>"
          +"<div class='form-group'>"
            +"<select class='form-control title' id='userinfantsgender"+i+"'>"
              +"<option>Mr</option>"
              +"<option>Ms</option>"
              +"<option>Mrs</option>"
            +"</select>"
          +"</div>"
        +"</div>"
        +"<div class='col-md-8 col-sm-8'>"
          +"<div class='col-md-6 col-sm-6'>"
            +"<div class='form-group'>"
              +"<input type='text' class='form-control' id='userfirstnameinfants"+i+"' placeholder='Enter First Name'>"
            +"</div>"
          +"</div>"
          +"<div class='col-md-6 col-md-6'>"
            +"<div class='form-group'>"
              +"<input type='text' class='form-control' id='userlastnameinfants"+i+"' placeholder='Enter Last Name'>"
            +"</div>"
         +"</div>"
        +"</div>"
        +"<div class='col-md-6 col-md-6'>"
        +"<div class='form-group'>"
          +"<input type='text' class='form-control' id='infantDateOfBirth"+i+"' placeholder='Date Of Birth'>"+ "<span class='msg'> As 11-06-1993</span>"
          
        +"</div>"
    	 +"</div>"
  +"</div>";
    }
    var d="<div class='row'>"
        +"<div class='col-md-12 col-sm-12'>"
        +"<span><b>Contact Details</b></span>"
      +"<br>"
        +"<div class='col-md-6 col-sm-6'>"
          +"<div class='form-group'>"
            +"<input type='email' class='form-control' id='useremailid' placeholder='Enter Your Email ID'>"
            +"<p style='color: red;' id='pesengerdeatilemail'></p>"
            +"<span class='msg'>Your ticket will be sent to this email address</span>"
         +"</div>"
        +"</div>"
        +"<div class='col-md-6 col-sm-6'>"
          +"<div class='form-group'>"
            +"<input type='text' class='form-control' id='usermobilenumber' placeholder='Enter Your Mobile Number'>"
        	+"<p style='color: red;' id='pesengerdeatilmobile'></p>"
            +"<span class='msg'>Your Mobile number will be used only for sending flight related communication.</span>"
            
            +"<p class='rederror'   id='responsemessage'> </p>"
            
          
            +"<div style='display: none;' id='loader'>"
			+"<img src='/resources/images/spinner.gif' style='width: 10%;'>"
	         +"</div>"
             
            
          +"</div>"
        +"</div>"
      +"</div>"
    +"</div>"
   
	 document.getElementById("container").innerHTML=""+c+d+"<div class='panel-group'>"
	    +"<div class='panel panel-default'>"
	    +"<div class='panel-heading'>"
	    +"<h4 class='panel-title'>"
	     +"<a data-toggle='collapse' href='#collapse1'>Payment</a>"
	     +"</h4>"
	    +"</div>"
	    +"<div id='collapse1' class='panel-collapse collapse'>"
	     +"<div class='panel-body'>"
	      +"<div class='row'>"
	       +"<div class='col-md-12 col-sm-12'>"
		+"<div class='row' style='margin-top: 50px;'>"
		 +"<div class='col-md-12'>"
		 +"<label class='radio-inline'><input type='radio' id='vnetId' name='optradio' value='vnet'><img src='${pageContext.request.contextPath}/resources/images/User/travel/img/Flight/VNET.png' alt='Vnet' style='width: 120px; margin-top: -10px;'></label>"
		 +"<label class='radio-inline'><input type='radio' id='ebsId' name='optradio' checked='checked' value='ebs'><img src='${pageContext.request.contextPath}/resources/images/User/travel/img/Flight/EBS.png' alt='Others' style='width: 120px; margin-top: -10px;'></label>"
		+"<label class='radio-inline'><input type='radio' id='walletId' name='optradio' value='wallet'><img src='${pageContext.request.contextPath}/resources/images/User/travel/img/Flight/Wallet.png' alt='Wallet' style='margin-top: -19px;'></label>"
		+ "</div>"
		+"<br>"
		+"<button class='btn btn-primary contnu' style='margin-left: 173px; margin-top: 19px; width: 119px; height: 43px;' onclick='openpaymentgateselection()' id='paymentbtn'>Continue</button>"
		+ "</div>"
		+ "</div>"
	        + "</div>"
	     + "</div>"
	    + "</div>"
	   + "</div>"
	   + "</div>"
	   + "</div>"; */
	   
}
function payAmount(){
	var amount=document.getElementById("grandtotal").innerHTML;
	console.log("amount="+amount);
	
	var payment_type=$("input[name='optradio']:checked").val();
	
	console.log("ABC:: "+payment_type);

	var contextPath = "${pageContext.request.contextPath}";
	var spinnerUrl = "Please wait <img src="+contextPath+"'/resources/images/spinner.gif' style='width:25px;'>"
	$("#gatewayPay").html(spinnerUrl);
	$("#gatewayPay").addClass("disabled");
	
// 	$("#loader").show();
	document.getElementById("pesengerdeatilmobile").innerHTML="";
	document.getElementById("pesengerdeatilemail").innerHTML="";
	 document.getElementById("responsemessage").innerHTML="";

	var emailAddress=document.getElementById("useremailid").value;
	var mobileNumber=document.getElementById("usermobilenumber").value;
	
	console.log("email :: "+emailAddress.length);
	
	var firstName="";
	var lastName="";
	
	var firstNamechild="";
	var lastNamechild="";

	var firstNameinfant="";
	var lastNameinfant="";
	
	var useradultsgender="";
	var userchildsgender="";
    var userinfantsgender="";
	
    var infantDateOfBirth="";
    var childDateOfBirth="";
    var adultDateOfBirth="";
    var userinfantsdob="";
	var adults = document.getElementById("adults").value;
	 var childs = document.getElementById("childs").value;
	 var infants = document.getElementById("infants").value;
	 var valid=true;
//	 var pattern = "[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$";
	 var pattern = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2}|com|org|net|edu|gov|mil|biz|info|mobi|name|aero|asia|jobs|museum|co|in)";
	 var namePattern = /^[a-zA-Z ]{2,30}$/;
	 var email_pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
	 var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
		
    for (i=1;i<=adults;i++){

    	if (document.getElementById("useradultsgender"+i).value=='#') {
    		$('#erra_title'+i).text("Please select title");
    		valid=false;
    	}
    	if (document.getElementById("userfirstnameadults"+i).value=='') {
    		valid=false;
    		$('#erra_fName'+i).text("Please enter first name");
    	}
    	else if(!(document.getElementById("userfirstnameadults"+i).value.match(namePattern))){
    		valid=false;
    		$('#erra_fName'+i).text("Please enter valid first name");
    	}
    	if (document.getElementById("userlastnameadults"+i).value=='') {
    		valid=false;
    		$('#erra_lName'+i).text("Please enter last name");
    	}
    	else if(!(document.getElementById("userlastnameadults"+i).value.match(namePattern))){
    		valid=false;
    		$('#erra_lName'+i).text("Please enter valid last name");
    	}
    	
		 firstName+="~"+document.getElementById("userfirstnameadults"+i).value;
		 lastName+="@"+document.getElementById("userlastnameadults"+i).value; 
		 useradultsgender+="#"+document.getElementById("useradultsgender"+i).value; 
// 		 adultDateOfBirth+="~"+document.getElementById("adultDateOfBirth"+i).value;
    }

    for (i=1;i<=childs;i++){
    	
    	if (document.getElementById("userchildsgender"+i).value=='#') {
    		$('#errc_title'+i).text("Please select title");
    		valid=false;
    	}
    	if (document.getElementById("userfirstnamechilds"+i).value=='') {
    		valid=false;
    		$('#errc_fName'+i).text("Please enter first name");
    	}
    	else if(!(document.getElementById("userfirstnamechilds"+i).value.match(namePattern))){
    		valid=false;
    		$('#errc_fName'+i).text("Please enter valid first name");
    	}
    	if (document.getElementById("userlastnamechilds"+i).value=='') {
    		valid=false;
    		$('#errc_lName'+i).text("Please enter last name");
    	}
    	else if(!(document.getElementById("userlastnamechilds"+i).value.match(namePattern))){
    		valid=false;
    		$('#errc_lName'+i).text("Please enter valid last name");
    	}
    	
    	
    	firstNamechild+="~"+document.getElementById("userfirstnamechilds"+i).value;
    	lastNamechild+="@"+document.getElementById("userlastnamechilds"+i).value;  
    	userchildsgender+="#"+document.getElementById("userchildsgender"+i).value; 
//     	childDateOfBirth+="~"+document.getElementById("childDateOfBirth"+i).value;
    }

    for (i=1;i<=infants;i++){
    	
    	if (document.getElementById("userinfantsgender"+i).value=='#') {
    		$('#erri_title'+i).text("Please select title");
    		valid=false;
    	}
    	if (document.getElementById("userfirstnameinfants"+i).value=='') {
    		valid=false;
    		$('#erri_fName'+i).text("Please enter first name");
    	}
    	else if(!(document.getElementById("userfirstnameinfants"+i).value.match(namePattern))){
    		valid=false;
    		$('#erri_fName'+i).text("Please enter valid first name");
    	}
    	if (document.getElementById("userlastnameinfants"+i).value=='') {
    		valid=false;
    		$('#erri_lName'+i).text("Please enter last name");
    	}
    	else if(!(document.getElementById("userlastnameinfants"+i).value.match(namePattern))){
    		valid=false;
    		$('#erri_lName'+i).text("Please enter valid last name");
    	}
    	
    	var infantDob=$('#infantsdob'+i).val();
    	if (document.getElementById("infantsdob"+i).value=='') {
    		valid=false;
    		$('#erri_dob'+i).text("Please Select Date of birth");
    	}
    	else if(!vaidateInfantDob(infantDob)){
    		$('#erri_dob'+i).text("Infant age should be 2 year or less");
    		valid=false;
    	}
    	firstNameinfant+="~"+document.getElementById("userfirstnameinfants"+i).value;
    	lastNameinfant+="@"+document.getElementById("userlastnameinfants"+i).value;  
    	userinfantsgender+="#"+document.getElementById("userinfantsgender"+i).value; 
    	userinfantsdob+="~"+document.getElementById("infantsdob"+i).value;
    	
    }
    
    var convFee=$("#convId").val();
	var baseFare=$("#baseFareId").val();
	console.log("base fare: "+baseFare);
	console.log("convFee: "+convFee);
	if(convFee.length > 0){
		var convFeeInt=parseFloat(convFee);
		if(convFeeInt < 200 || convFeeInt > 400 ){
			valid=false
			alert("Convenience fee should be between 200 to 400");
		}
	}
	else{
		valid=false
		alert("Convenience fee could not be empty");
	}
	 
	var grandtotal=document.getElementById("grandtotal").innerHTML;
	
		$('#usermobilenumber').keyup(function () { 
	    	this.value = this.value.replace(/[^0-9\.]/g,'');
		});
	   if(mobileNumber.length!=10)
		{
	   		$("#gatewayPay").html("Pay With Payment Gateway");
			$("#gatewayPay").removeClass("disabled");
		 	$("#loader").hide();
		 	valid=false;
			document.getElementById("pesengerdeatilmobile").innerHTML="Please Enter 10 Digit Mobile Number";
		}
 
	    if (emailAddress=='' || emailAddress==' ') {
	    	 console.log("email Invalid:: ");
			valid=false;
			document.getElementById("pesengerdeatilemail").innerHTML="Please enter email";
		}
		else if(!(emailAddress.match(pattern))){
			document.getElementById("pesengerdeatilemail").innerHTML="Please enter valid email";
			valid = false;
			console.log("email Invalid:: ");
		}
		else if(!(emailAddress.match(email_pattern))){
			document.getElementById("pesengerdeatilemail").innerHTML="Please enter valid email";
			valid = false;
			console.log("email Invalid:: ");
		}
		else if(!(emailAddress.match(filter))){
			document.getElementById("pesengerdeatilemail").innerHTML="Please enter valid email";
			valid = false;
			console.log("email Invalid:: ");
		}
	 if(valid==true){
	  $.ajax({
		type : "POST",
		contentType :"application/json",
		url : "${pageContext.request.contextPath}/Agent/Travel/Flight/PayAmount",
		data : JSON.stringify({ 
			"grandtotal" : "" + amount+ "",
		}),
		success : function(response)
		{
			console.log("Length="+response.amount);
			console.log("Length="+response.loadAmt);
			
			
		//	$("#payModal").modal("show");
			/* $('#payModal').modal({
		        backdrop: 'static',
		        keyboard: true, 
		        show: true
			}); */
			
			$("#payModal").modal({backdrop: false});
			$("#aAmount").html(response.amount);
			var amt = response.loadAmt;
			if(amt>1){
				$("#loadAmt").html(amt);
			//	$("#loadAmt2").show();
			}
			else{
				console.log("In else");
				$("#loadAmt").hide();
				$("#loadAmt2").hide();
				$("#loadBal").hide();
			}
			$("#totalAmt").html(amount);
		}
	});
  } 
}

function openpaymentgateselection()
{
	
	/* #vnetId").html(spinnerUrl);
	$("#ebsId").html(spinnerUrl);
	$("#walletId").html(spinnerUrl); */ 
	
	var payment_type=$("input[name='optradio']:checked").val();
	
	console.log("ABC:: "+payment_type);

	var contextPath = "${pageContext.request.contextPath}";
	var spinnerUrl = "Please wait <img src="+contextPath+"'/resources/images/spinner.gif' style='width:25px;'>"
	$("#gatewayPay").html(spinnerUrl);
	$("#gatewayPay").addClass("disabled");
	
// 	$("#loader").show();
	document.getElementById("pesengerdeatilmobile").innerHTML="";
	document.getElementById("pesengerdeatilemail").innerHTML="";
	 document.getElementById("responsemessage").innerHTML="";

	var emailAddress=document.getElementById("useremailid").value;
	var mobileNumber=document.getElementById("usermobilenumber").value;
	
	console.log("email :: "+emailAddress.length);
	
	var firstName="";
	var lastName="";
	
	var firstNamechild="";
	var lastNamechild="";

	var firstNameinfant="";
	var lastNameinfant="";
	
	var useradultsgender="";
	var userchildsgender="";
    var userinfantsgender="";
	
    var infantDateOfBirth="";
    var childDateOfBirth="";
    var adultDateOfBirth="";
    var userinfantsdob="";
	var adults = document.getElementById("adults").value;
	 var childs = document.getElementById("childs").value;
	 var infants = document.getElementById("infants").value;
	 var valid=true;
	 var pattern = "[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$";
	 var namePattern = /^[a-zA-Z ]{2,30}$/;
	 var convFee= $('#convId').val();
	 var baseFare= $('#baseFareId').val();
    for (i=1;i<=adults;i++){

    	if (document.getElementById("useradultsgender"+i).value=='#') {
    		$('#erra_title'+i).text("Please select title");
    		valid=false;
    	}
    	if (document.getElementById("userfirstnameadults"+i).value=='') {
    		valid=false;
    		$('#erra_fName'+i).text("Please enter first name");
    	}
    	else if(!(document.getElementById("userfirstnameadults"+i).value.match(namePattern))){
    		valid=false;
    		$('#erra_fName'+i).text("Please enter valid name");
    	}
    	if (document.getElementById("userlastnameadults"+i).value=='') {
    		valid=false;
    		$('#erra_lName'+i).text("Please enter last name");
    	}
    	else if(!(document.getElementById("userlastnameadults"+i).value.match(namePattern))){
    		valid=false;
    		$('#erra_lName'+i).text("Please enter valid name");
    	}
    	
		 firstName+="~"+document.getElementById("userfirstnameadults"+i).value;
		 lastName+="@"+document.getElementById("userlastnameadults"+i).value; 
		 useradultsgender+="#"+document.getElementById("useradultsgender"+i).value; 
// 		 adultDateOfBirth+="~"+document.getElementById("adultDateOfBirth"+i).value;
    }

    for (i=1;i<=childs;i++){
    	
    	if (document.getElementById("userchildsgender"+i).value=='#') {
    		$('#errc_title'+i).text("Please select title");
    		valid=false;
    	}
    	if (document.getElementById("userfirstnamechilds"+i).value=='') {
    		valid=false;
    		$('#errc_fName'+i).text("Please enter first name");
    	}
    	else if(!(document.getElementById("userfirstnamechilds"+i).value.match(namePattern))){
    		valid=false;
    		$('#errc_fName'+i).text("Please enter valid name");
    	}
    	if (document.getElementById("userlastnamechilds"+i).value=='') {
    		valid=false;
    		$('#errc_lName'+i).text("Please enter last name");
    	}
    	else if(!(document.getElementById("userlastnamechilds"+i).value.match(namePattern))){
    		valid=false;
    		$('#errc_lName'+i).text("Please enter valid name");
    	}
    	
    	
    	firstNamechild+="~"+document.getElementById("userfirstnamechilds"+i).value;
    	lastNamechild+="@"+document.getElementById("userlastnamechilds"+i).value;  
    	userchildsgender+="#"+document.getElementById("userchildsgender"+i).value; 
//     	childDateOfBirth+="~"+document.getElementById("childDateOfBirth"+i).value;
    }

    for (i=1;i<=infants;i++){
    	
    	if (document.getElementById("userinfantsgender"+i).value=='#') {
    		$('#erri_title'+i).text("Please select title");
    		valid=false;
    	}
    	if (document.getElementById("userfirstnameinfants"+i).value=='') {
    		valid=false;
    		$('#erri_fName'+i).text("Please enter first name");
    	}
    	else if(!(document.getElementById("userfirstnameinfants"+i).value.match(namePattern))){
    		valid=false;
    		$('#erri_lName'+i).text("Please enter valid name");
    	}
    	if (document.getElementById("userlastnameinfants"+i).value=='') {
    		valid=false;
    		$('#erri_lName'+i).text("Please enter last name");
    	}
    	else if(!(document.getElementById("userlastnameinfants"+i).value.match(namePattern))){
    		valid=false;
    		$('#erri_lName'+i).text("Please enter valid name");
    	}
    	
    	if (document.getElementById("infantsdob"+i).value=='') {
    		valid=false;
    		$('#erri_dob'+i).text("Please Select Date of birth");
    	}
    	firstNameinfant+="~"+document.getElementById("userfirstnameinfants"+i).value;
    	lastNameinfant+="@"+document.getElementById("userlastnameinfants"+i).value;  
    	userinfantsgender+="#"+document.getElementById("userinfantsgender"+i).value; 
    	userinfantsdob+="~"+document.getElementById("infantsdob"+i).value;
    	
    }
	
	 
	var grandtotal=document.getElementById("grandtotal").innerHTML;
		
	$('#usermobilenumber').keyup(function () { 
    	this.value = this.value.replace(/[^0-9\.]/g,'');
	});
	   if(mobileNumber.length <=9)
		{
	   $("#gatewayPay").html("Pay With Payment Gateway");
		$("#gatewayPay").removeClass("disabled");
		 $("#loader").hide();
		document.getElementById("pesengerdeatilmobile").innerHTML="Please Enter 10 Digit Mobile Number";
		}
 
	    if (emailAddress=='' || emailAddress==' ') {
	    	 console.log("email Invalid:: ");
			valid=false;
			document.getElementById("pesengerdeatilemail").innerHTML="Please enter email";
		}
		else if(!(emailAddress.match(pattern))){
			document.getElementById("pesengerdeatilemail").innerHTML="Please enter valid email";
			valid = false;
			console.log("email Invalid:: ");
		}
			/* var atpos = emailAddress.indexOf("@");
		    var dotpos = emailAddress.lastIndexOf(".");
		   if(mobileNumber.length <=9)
				{
			   $("#gatewayPay").html("Pay With Payment Gateway");
				$("#gatewayPay").removeClass("disabled");
				 $("#loader").hide();
				document.getElementById("pesengerdeatilmobile").innerHTML="Please Enter 10 Digit Mobile Number";
				}
		     else if(atpos<1 || dotpos<atpos+2 || dotpos+2 >= emailAddress.length) {
		    	 $("#gatewayPay").html("Pay With Wallet");
				$("#gatewayPay").removeClass("disabled");
		    	 $("#loader").hide(); 
				document.getElementById("pesengerdeatilemail").innerHTML="Please Enter Your Email";
		       
		    }
		     else if(firstName.length<=4 || lastName.length<=4 )
		     {
		    	 $("#gatewayPay").html("Pay With Payment Gateway");
				$("#gatewayPay").removeClass("disabled");
		    	 $("#loader").hide(); 
				document.getElementById("pesengerdeatilemail").innerHTML="Please Enter Your Name Detail";
		       
		    }  */
		   
		     
		   
// 		     else{
		    	 document.getElementById("useradultsgender").value=""+useradultsgender;
				 document.getElementById("userchildsgender").value=""+userchildsgender;
				 document.getElementById("userinfantsgender").value=""+userinfantsgender;
				
				 $("#gatewayPay").html("Pay With Payment Gateway");
				 $("#gatewayPay").removeClass("disabled");
		    	 $("#loader").hide();
// //    	 $("#loadMoneyModal").modal();
		    	 
		    	 if (valid==true) {
		    	 if (payment_type.includes("wallet")) {
		    		 
		    		 $("#paymentbtn").html(spinnerUrl);
		    		 $("#paymentbtn").addClass("disabled");
		    		 
		    		$.ajax({
						type : "POST",
						contentType : "application/json",
						url : "${pageContext.request.contextPath}/Agent/Travel/Flight/CheckOut",
						data : JSON.stringify({
							 "emailAddress" : "" + emailAddress+ "",
								"firstName" : "" + firstName+ "",
								"lastName" : "" + lastName+ "",
								
								"firstNamechild" : "" + firstNamechild+ "",
								"lastNamechild" : "" + lastNamechild+ "",
								"firstNameinfant" : "" + firstNameinfant+ "",
								"lastNameinfant" : "" + lastNameinfant+ "",
								"mobileNumber" : "" + mobileNumber+ "",
							    "useradultsgender" : "" + useradultsgender+ "",
							    "userchildsgender" : "" + userchildsgender+ "",
								"userinfantsgender" : "" + userinfantsgender+ "",
								"grandtotal" : "" + grandtotal+ "",
								"infantDateOfBirth":"" +userinfantsdob +"", 
								"convFee":"" +convFee+"", 
								"baseFare":"" +baseFare+"", 
								/* "adultDateOfBirth":"" +adultDateOfBirth +"",
								"childDateOfBirth":"" +childDateOfBirth +"",
								"infantDateOfBirth":"" +infantDateOfBirth +"", */
								
						}),
						success : function(response)
						{
							$("#paymentbtn").html("Pay With Wallet");
							$("#paymentbtn").removeClass("disabled");
							document.getElementById("pesengerdeatilmobile").innerHTML="";
							document.getElementById("pesengerdeatilemail").innerHTML="";
							 document.getElementById("responsemessage").innerHTML="";
				 				$("#payModal").modal("hide");
							 var d=response.split("@");
							 if(d[0]==="S00")
							 {
								 
								 $('#flightModal').modal({
								        backdrop: 'static',
								        keyboard: true, 
								        show: true
									});
								 
								 $("#payment_ok").click(function() {
										window.location.href='${pageContext.request.contextPath}/Agent/Travel/Flight/MyTickets'; 
									});
								 
								 $("#loader").hide();
// 								 document.getElementById("responsemessage").innerHTML=""+d[1];	
								 
							 }
							 else if(d[0]==="T01")
								 {
								 document.getElementById("firstName1").value=""+firstName;
								 document.getElementById("lastName1").value=""+lastName;
								 document.getElementById("returnamount").value=""+d[2];
								 /* document.getElementById("firstNamechild1").value=""+firstNamechild;
								 document.getElementById("lastNamechild1").value=""+lastNamechild;
								 document.getElementById("firstNameinfant1").value=""+firstNameinfant;
								 document.getElementById("lastNameinfant1").value=""+lastNameinfant; */
								 
								  document.getElementById("phone1").value=""+mobileNumber;
								  document.getElementById("email1").value=""+emailAddress;
								 $('#ProcessSplitpayment').submit();
								 }
							 else if(d[0]==="F03")
							 {
								 window.location.href='${pageContext.request.contextPath}/Home'; 
							 }
						 else
						 {
							 $("#loader").hide();
							
							 $("#common_error_msg").html(""+d[1]);
							 $("#errModel").modal("show"); 
							 
							 
// 							 document.getElementById("responsemessage").innerHTML=""+d[1];	
							 
									
						 }
		 

						}
					});
		    	 }
	
		    	 
		    	  else if ((payment_type.includes("vnet")) || (payment_type.includes("ebs"))) {
		         
		    		 if (payment_type.includes("vnet")) {
		    		  document.getElementById("vnetrd").value="vnet";	
					}
		    		 if (payment_type.includes("ebs")) {
			    		  document.getElementById("vnetrd").value="others";	
						}
		    		 
		    	 document.getElementById("firstName").value=""+firstName;
				 document.getElementById("lastName").value=""+lastName;
				 document.getElementById("firstNamechild").value=""+firstNamechild;
				 document.getElementById("lastNamechild").value=""+lastNamechild;
				 document.getElementById("firstNameinfant").value=""+firstNameinfant;
				 document.getElementById("lastNameinfant").value=""+lastNameinfant;
				 document.getElementById("phone").value=""+mobileNumber;
				 document.getElementById("email").value=""+emailAddress;
				 document.getElementById("useradultsgender1").value=""+useradultsgender;
				 document.getElementById("userchildsgender1").value=""+userchildsgender;
				 document.getElementById("userinfantsgender1").value=""+userinfantsgender;
				 document.getElementById("infantDateOfBirth").value=""+userinfantsdob;
				 
				 console.log("Infant DOB:: "+userinfantsdob);
					  $('#myform').submit();

		    	 }
		    } 
		     }
	 
		   function vaidateInfantDob(value){
			   if(value!=null && value!=""){
			var curDate= new Date();
			var prevDate=parseInt(new Date(curDate.getFullYear()-2+"/"+curDate.getMonth()+"/"+curDate.getDate()).getTime());
			var dob= parseInt(new Date(value).getTime());
			curDate=parseInt(curDate.getTime());
			console.log("cur="+curDate+":pre"+prevDate+":dob"+dob+"://"+value);
			if(dob >= prevDate && dob  <= curDate)
			return true;
			return false;
			   }
		   }
		   
		   
		   function isNumberKey(evt)
     	     {
     	        var charCode = (evt.which) ? evt.which : evt.keyCode;
     	        if (charCode > 31  && (charCode < 48 || charCode > 57)){
     	         return false;
     	        }else{
     	        	return true;
     	        }
     	     }
            function addConvFee() {
				var convFee= $('#convId').val();
				var total= $('#totalAmtId').val();
          	  console.log("convFee:"+convFee);
				if(convFee!=null && convFee !=""){
					var totalNum=parseFloat(total);
					var convFeeNum=parseFloat(convFee);
					totalNum = totalNum + convFeeNum;
					console.log("grand total"+totalNum);
					 document.getElementById("grandtotal").innerHTML = '<td id="grandtotal">'+totalNum+'</td>';
				}else{
					 document.getElementById("grandtotal").innerHTML = '<td id="grandtotal">'+total+'</td>';
				}
            }
		   
// }
</script>






