<!DOCTYPE HTML>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<html>
<head>
    <title>International Round Trip</title>
 <link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/bootstrap-chosen.css">
<link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'
	type="image/png" />
<link rel="stylesheet" href="/resources/css/new_css/cloud-admin.css">
<link rel="stylesheet" href="/resources/css/font-awesome.min.css">
<link rel="stylesheet" href="/resources/css/new_css/default.css">
<link rel="stylesheet" href="/resources/css/new_css/responsive.css">
<link rel="stylesheet"
	href="/resources/css/new_css/uniform.default.min.css">
<!--- GOOGLE FONTS - -->
<link
	href='https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700'
	rel='stylesheet' type='text/css'>
<link
	href='https://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600'
	rel='stylesheet' type='text/css'>
<!-- /GOOGLE FONTS -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/travel_css/bootstrap.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<!--<link rel="stylesheet" href="css/font-awesome.css">-->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/travel_css/icomoon.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/travel_css/styles.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/travel_css/mystyles.css">
</head>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css" />
 
 <script src="/resources/js/new_js/select2.min.js"></script>
 
 <script type="text/javascript" 
    src="<c:url value="/resources/js/Agent/Agentdetail.js"/>"></script>
		
		   <link rel="stylesheet" href="/resources/css/new_css/newstyles.css">    
    <link rel="stylesheet" rel="stylesheet" href="/resources/css/new_css/select2.1.css">
    
    
    
     <link rel="stylesheet" href="/resources/css/new_css/jquery.ui.theme.css" type="text/css">	 
	<link rel="stylesheet" href="/resources/css/new_css/jquery.ui.slider.css" type="text/css">
<!-- 	<script type="text/javascript" src="/resources/js/new_js/jquery-1.7.1.js"></script> -->
	<script type="text/javascript" src="/resources/js/new_js/jquery.ui.widget.js"></script>
	<script type="text/javascript" src="/resources/js/new_js/jquery.ui.mouse.js"></script>
	<script type="text/javascript" src="/resources/js/new_js/jquery.ui.slider.js"></script>
	<script src="/resources/js/new_js/select2.min.js"></script>
	
	
    <style>
    	h6 {
    		margin-bottom: 0px;
    	}
        .rederror {
            font-size: 14px;
            color: red;
        }
    
        .select2-container {
            box-sizing: border-box;
            display: inline-block;
            margin: 0;
            position: relative;
            vertical-align: middle;
            width: 50% !important;
            float: right;
            background: #fff;
        }
        .inner_tab_content_Hotel > .select2-container {
            box-sizing: border-box;
            display: inline-block;
            margin: 0;
            position: relative;
            vertical-align: middle;
            width: 100% !important;
            float: right;
            background: #fff;
        }
        .inner_tab_content_Holiday > .select2-container {
            box-sizing: border-box;
            display: inline-block;
            margin: 0;
            position: relative;
            vertical-align: middle;
            width: 100% !important;
            float: right;
            background: #fff;
        }

    
        .dropdown1 {
            background: #fff;
            border: 1px solid #ccc;
            border-radius: 4px;
            width: 280px;
            padding: 5px;
            left: 62%;
        /*     top: 413px; */
            position: absolute;
            z-index: 99; 
        }
        
        .dropdown1 .t_menu {
            border-radius:4px;
            box-shadow:none;
            margin-top:10px;
            wi/dth:300px;
        }
        .dropdown1 .t_menu:before {
            content: "";
            border-bottom: 10px solid #fff;
            border-right: 10px solid transparent;
            border-left: 10px solid transparent;
            position: absolute;
            top: -10px;
            right: 16px;
            z-index: 10;
        }
        .dropdown1 .t_menu:after {
            content: "";
            border-bottom: 12px solid #ccc;
            border-right: 12px solid transparent;
            border-left: 12px solid transparent;
            position: absolute;
            top: -12px;
            right: 14px;
            z-index: 9;
        }
        .t_divider {
            margin-top: 15px;
            margin-bottom: 15px;
            border: 0;
            border-top: 1px solid #eee;
        }
        
        .incr-btn {
          margin-left: 0px;
        }
        #errormsgdest {
          margin-left: 263px;
        }
        
        #errormsgdate {
          position: absolute;
		  margin-top:  38px;
		  margin-left: -200px;
        }
      
        #errormsgadult {
          position: absolute;
		  margin-top:  30px;
		  margin-left: -136px;
		  }
		  hr {
	        margin-top: 14px;
	        margin-bottom: 10px;
	        border: 0;
	        border-top: 2px dotted #eee;
	      }
	      .rou-arrw {
	        width: 75%;
	        margin-left: -20px;
	    }
        .rou-arrw2 {
            width: 100%;
            margin-left: -10px;
        }
        input[type=radio] {
        	width: 17px;
        }
    </style>
    
    <script>
    $(".beSwapCity").on('click', function() {
      var fromcurrency =  $('#flight_src_list').val();
      var tocurrency = $('#flight_des_list').val();
      $('#flight_src_list').val(tocurrency).trigger('change');
      $('#flight_des_list').val(fromcurrency).trigger('change');
});
</script>

   <script>
            $( "#flight_src_list" ).select2({
                theme: "bootstrap"
            });
            $( "#flight_des_list" ).select2({
                theme: "bootstrap"
            });
    
        </script>
        
        
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
  
  <!-- <script>
   
  $(document).ready(function(){
   var date_input=$('input[name="date"]');
   var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
   date_input.datepicker({
    format: 'dd/mm/yyyy',
    container: container,
  
    todayHighlight: true,
    autoclose: true,
   })
  })
  
  
   $(document).ready(function(){
   var date_input=$('input[name="returndate"]');
   var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
   date_input.datepicker({
    format: 'dd/mm/yyyy',
    container: container,
    todayHighlight: true,
    autoclose: true,
   })
  })
 </script> -->
 
 <script>
  $(document).ready(function(){
	  
	  $( "#date" ).focus(function() {
		  $( this ).blur();
		});
	  $( "#returndate" ).focus(function() {
		  $( this ).blur();
		});
	  
	  var flightdata=$('#flightdata').val(); 
	  console.log("flightdata: "+flightdata.length);
	  var minAmt=$('#minAmt').val();     
		var maxAmt=$('#maxAmt').val();    
		
		console.log("minAmt:: "+minAmt);
		console.log("maxAmt:: "+maxAmt);
		
		$("input[name='df']").attr('checked', false);
		
	$(function () {
	      $('#slider-container').slider({
	          range: true,
	          min: parseInt(minAmt),
	          max: parseInt(maxAmt),
	          values: [parseInt(minAmt), parseInt(maxAmt)],
	          create: function() {
	              $("#amount").val("Rs "+ parseInt(minAmt) + "- RS " + parseInt(maxAmt));
	          },
	          slide: function (event, ui) {
	              $("#amount").val("Rs" + ui.values[0] + " - Rs" + ui.values[1]);
	              var mi = ui.values[0];
	              var mx = ui.values[1];
	              filterSystem(mi, mx);
	          }
	          
	      })
	});

	  function filterSystem(minPrice, maxPrice) {
	      
		  $(".computers div.system").hide().filter(function () {
	          var price = parseInt($(this).data("price"), 10);
	          return price >= minPrice && price <= maxPrice;
	      }).show();
	  }
	  

var contextPath = "${pageContext.request.contextPath}";
var session=$("#session").val();
    $(".js-example-basic-multiple").select2();
//     $("#loading_flight").modal({backdrop: false});
    $("#flght_srch").addClass("disabled");
 	$('#flightsrc').empty();
 	$('#flightdest').empty(); 
 	$("#returndate").val("");
 	
//  	$('#flightsrc').append('<option value="#" selected="selected"><span><img src="${pageContext.request.contextPath}/resources/images/User/travel/img/Flight/Ellipsis.gif"></span></option>');
	$('#flightsrc').append('<option value="#" selected="selected">Getting Origin.....</option>');
 	$('#flightdest').append('<option value="#" selected="selected">Getting Destination.....</option>');
 	
 	var org=$('#sourceinternational').val();
    var dst=$('#destinationinternational').val();
    console.log("org:: "+org);
    console.log("dst:: "+dst);
					$.ajax({
						type : "POST",
						contentType : "application/json",
						url :contextPath+"/Agent/Travel/Flight/AirLineNames",
						dataType : 'json',
						data : JSON.stringify({
							"sessionId": session,
					   }),
            			success : function(response) {
            				
            				for(var i=0;i<response.details.length;i++){
            					
            					if (response.details[i].cityCode.includes(org)) {
            						console.log(response.details[i].cityCode);
            					$('#flightsrc').append('<option value="' + response.details[i].cityCode + '" selected="selected">' + response.details[i].cityName + ' ('+response.details[i].cityCode+')'+ '</option>');
            					}
            					else{
            						$('#flightsrc').append('<option value="' + response.details[i].cityCode + '" >' + response.details[i].cityName + ' ('+response.details[i].cityCode+')'+ '</option>');
            					}
            					if (response.details[i].cityCode.includes(dst)) {
            					$('#flightdest').append('<option value="' + response.details[i].cityCode + '" selected="selected">' + response.details[i].cityName + ' ('+response.details[i].cityCode+')'+ '</option>');	
            					}else{
            						$('#flightdest').append('<option value="' + response.details[i].cityCode + '">' + response.details[i].cityName + ' ('+response.details[i].cityCode+')'+ '</option>');	
            					}
            				
            				}
//             				 $("#loading_flight").modal("hide");
            				 
            				 $("#flght_srch").removeClass("disabled");
            				 
            					var today = new Date();
            					var dd = today.getDate();
            					var mm = today.getMonth()+1; //January is 0!

            					var yyyy = today.getFullYear();
            					if(dd<10){
            					    dd='0'+dd;
            					} 
            					if(mm<10){
            					    mm='0'+mm;
            					} 
            					var today = dd+'/'+mm+'/'+yyyy;
//             					document.getElementById("date").value = today; 
            					var end=$("#endDate").val();
            					console.log("End date : "+end);
            					var bDate=$("#beginDate").val();
            					var nbDate=bDate.split("-");
            					var nbeDate=end.split("-");
            					nwDate=nbDate[2]+"/"+nbDate[1]+"/"+nbDate[0];
            					nweDate=nbeDate[2]+"/"+nbeDate[1]+"/"+nbeDate[0];
            					$("#returndate").val(nweDate);
            					console.log("DAte:: "+nwDate);
//             					var nbDate=bDate.replace("-", "/");
            					$("#date").val(nwDate);
		            			}
			            		});
			 				 });
  
  
            		var today = new Date();
            		  
            	  	$(function() {
            			$("#date").datepicker({
            				startDate: today,
            				format : "dd/mm/yyyy"
            			}).on('change', function() {
            				$('.datepicker').hide();
            			});
            		});
            		
            	  	$(function() {
            			$("#returndate").datepicker({
            				startDate: today,
            				format : "dd/mm/yyyy"
            			}).on('change', function() {
            				$('.datepicker').hide();
            			});
            		});
            		
            	  	/* $(function() {
            			$("#returndatecal").datepicker({
            				startDate: '+1d',
            				format : "dd/mm/yyyy"
            			}).on('change', function() {
            				$('.datepicker').hide();
            			});
            		}); */
            	  
            	  	
            	  	$(function() {
            			$("#datecal").datepicker({
            				startDate: today,
            				format : "dd/mm/yyyy"
            			}).on('change', function() {
            				$('.datepicker').hide();
            			});
            		});
  
            		
            		function srcdest() {
            			console.log("Compare");
            		$('#errormsgdest').empty();
            		var sr=$('#flightsrc').val();
            		var ds=$('#flightdest').val();
            			
            			if (sr.includes(ds)) {
            			$('#errormsgdest').html("Please select source & destation different.");
//            	 		alert("src dest mis match");
//            	 		document.getElementById("flightdestination").selectedIndex = 0;
            			}
            		}
            		
 </script>
 
</head>


<body style="background: #efefef;">
<input type="hidden" value="${sourcecode}" id="traveldataval">
     <input type="hidden" value="${flightnumberdata}" id="filterdata1" >
     <input type="hidden" value="${source}" id="sourceinternational" >
     <input type="hidden" value="${destination}" id="destinationinternational" >
     <input type="hidden" value="${bDate}" id="beginDate">
     <input type="hidden" value="${eDate}" id="endDate">
     <input type="hidden" value="${flightdatareturn}" id="fldatareturn">
     <input type="hidden" value="${minAmt}" id="minAmt">
	 <input type="hidden" value="${maxAmt}" id="maxAmt">
	<input type="hidden" value="${flightdata}" id="flightdata">
	
<jsp:include page="/WEB-INF/jsp/Agent/AHeader.jsp" />

         <div class="bg-holder full" style="    height: auto;">
            
                <div class="bg-content">
                    <div class="container">
                        <div class="bking-box">
                            <div class="row">                                
                                <div class="col-md-12">
                                    <div class="tggle-cont">
                                        <div class="tab-content" style="background: rgba(0,0,0,0.6) !important; background: rgba(0,0,0,0.4); border-radius: 3px; padding: 15px; min-height: 74px; -webkit-transition: all .2s ease; -moz-transition: all .2s ease; -ms-transition: all .2s ease; -o-transition: all .2s ease; transition: all .2s ease;">
                                            <!-- ============================ Flight Tab ============================= -->
                                            <div class="tab-pane active" id="tab_flight">                                                
                                                <div class="col-md-4" style="padding-left: 0px; padding-right: 0px;">
                                                    <div class="inner_tab_content_Flight">
                                                        <select id="flightdest" class="js-example-basic-multiple" style="width: 350px;" name="sourceId"  >
                                                                
                                                        </select>
                                                        <a href="javascript:void(0);" class="beSwapCity" data-swap="true">
                                                            <i class="sprite-booking-engine ico-be-leftRightArow" title="Swap Origin City and Destination City">&nbsp;</i> 
                                                        </a>
                                                         <select id="flightsrc" class="js-example-basic-multiple" style="width: 350px;" name="sourceId"  onchange="changeval()">
                                                                 
                                                        </select>
                                                        <span class="rederror" id="errormsgsrc"></span>
                                                        <span class="rederror" id="errormsgdest"></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-4" style="padding-left: 0px; padding-right: 0px;">
                                                    <input type="text" class="form-control" placeholder="Depart Date" name="date" style="width: 50%; float: left; height: 40px;" id="date" onchange="$('#errormsgdate').empty();">
                                                     <span class="rederror" id="errormsgdate"></span>
                                                    <i class="fa fa-calendar D-font-icon icon-inside icon-calendar"></i>
                                                    <input type="text" class="form-control"   placeholder="Return Date" name="returndate" style="width: 50%; height: 40px; cursor: default;" id="returndate" >
                                                    <i class="fa fa-calendar R-font-icon icon-inside icon-calendar"></i>
                                                </div>
                                                <div class="col-md-4" style="padding-left: 0px; padding-right: 0px; height: 40px;">
                                                    <!-- <input type="text" class="form-control" placeholder="Traveller Information" name="" style="width: 50%; float: left; height: 40px; cursor: pointer; background: #fff; readonly="readonly"> -->
                                                    <div class="pax-details" id="selTrav">
                                                        <span class="txt-ellipses">
                                                            <span class="totalCount" id="totalCounttrevaler">1</span> Traveller(s)<span class="flight_cls" id="flight_cls">, Economy</span>
                                                              <span class="rederror" id="errormsgadult"></span>
                                                        </span>
                                                        <i class="sprite-booking-engine ico-be-arrow-down-grey-v2 icon-inside" id=""> </i>
                                                    </div>
                                                    <!-- <i class="sprite-booking-engine ico-be-arrow-down-grey-v2 icon-inside" id=""> </i> -->
                                                    <button class="btn btn-block search-btn" style="width: 50%; height: 40px;" id="loader" onclick="getvalue()">Modify Search</button>
                                                    <div style="display: none;" id="loader">
													<span class="rederror" id="responsemessage"> </span>
													</div>
												   
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- ===================== end main row =============== -->
                    </div><!-- =============end container============== -->
                </div><!-- =================end bg-content================= -->
            </div><!-- ==============end bg-holder full================= -->
        </div>




	 <div class="dropdown1" id="trav_menu" style="display: none;">
            <div class="t_menu">
                <div class="row">
                    <div class="col-md-7">
                        <input class="quantity" type="text" id="adult" name="quantity" value="1"  hidden="hidden" />
                        <span class="quantity" id="adult1" >1</span>&nbsp;<span>Adult(s)</span>
                    </div>
                    <div class="col-md-5">
                        <button class="incr-btn btn btn-sm" style="background: #0d30e8; color: #fff;" onclick="decreaseValue('adult','adult1');" ><i class="fa fa-minus" aria-hidden="true"></i></button>
                        <button class="incr-btn btn btn-sm" style="background: #0d30e8; color: #fff;" onclick="increaseValue('adult','adult1');" ><i class="fa fa-plus" aria-hidden="true"></i></button>
                    </div>
                </div>
                <hr class="t_divider">
                <div class="row">
                    <div class="col-md-7">
                        <input class="quantity" type="text" id="childs" value="0" name="quantity"   hidden="hidden" />
                        <span class="quantity" id="childs1">0</span>&nbsp;<span>Child</span>&nbsp;<small>(2-12 YRS)</small>
                    </div>
                    <div class="col-md-5">
                        <button class="incr-btn btn btn-sm" style="background: #0d30e8; color: #fff;" onclick="decreaseValue('childs','childs1');" ><i class="fa fa-minus" aria-hidden="true"></i></button>
                        <button class="incr-btn btn btn-sm" style="background: #0d30e8; color: #fff;" onclick="increaseValue('childs','childs1');" ><i class="fa fa-plus" aria-hidden="true"></i></button>
                     </div>
                </div>
                <hr class="t_divider">
                <div class="row">
                    <div class="col-md-7">
                        <input class="quantity" type="text" name="quantity" value="0" id="infants" hidden="hidden" />
                        <span class="quantity" id="infants1">0</span>&nbsp;<span>Infant</span>&nbsp;<small>(Below 2 YRS)</small>
                    </div>
                    <div class="col-md-5">
                        <button class="incr-btn btn btn-sm" style="background: #0d30e8; color: #fff;" onclick="decreaseValue('infants','infants1');" ><i class="fa fa-minus" aria-hidden="true"></i></button>
                        <button class="incr-btn btn btn-sm" style="background: #0d30e8; color: #fff;" onclick="increaseValue('infants','infants1');" ><i class="fa fa-plus" aria-hidden="true"></i></button>
                    </div>
                </div>
                <hr class="t_divider">
                <div class="row">
                    <div class="col-md-12">
                    	<div class="col-md-4">
                    		<label>Class</label>
                    	</div>
                    	<div class="col-md-8">
                    		<select name="flight_pax" id="flightcabin" tabindex="" onchange="changesclass()" class="form-control">
								<option selected="selected" value="Economy">Economy</option>
								<!--<option value="Premium Economy">Premium Economy</option> -->
								<option value="Business">Business</option>
							</select>
                    	</div>
                    </div>
                </div>
            
                <div class="row">
                    <div class="col-md-12">
                       <button class="btn btn-sm btn-danger pull-right" onclick="closedivtrevalinformation();">Done</button>  	
                    </div>
                </div>
            </div>
        </div>
        <!-- TRAVELLERS DETAILS END HERE  -->
	
    <div class="container-fluid round-trip">
       
       <div class="col-md-3 hidden-sm hidden-xs" style="padding-left: 0px;">
            <div class="float-panel panel panel-default">
                <div class="panel-heading">
                    <span style="font-size: 20px; font-weight: 800;">Filter By:</span>
                    <a style="padding-left: 65px;" href="#" id="clr_filtr">Clear all filter</a>
                </div>
                <div class="panel-body">
                    <div class="row" style="margin-bottom: 5px;">
                        <div class="col-md-12">
                            <span style="font-size: 15px; font-weight: 600;">Stops</span>
                        </div>
                    </div>
                    <!-- <hr style="margin-top: 6px; margin-bottom: 6px;"> -->
                    <div class="row">

                        <div class="checkbox">
                            <label style="font-size: 12px;"><input type="checkbox"
                                class="filter_link_stopage" data-filter="all" value=""
                                name="stopageall">Show-All</label>
                        </div>

                        <c:forEach items="${flightNumbersotpage}" var="u">
                            <div class="checkbox">
                                <label style="font-size: 12px;"><input type="checkbox"
                                    class="filter_link_stopage" data-filter="${u.stopagereturn}" value=""
                                    name="${u.stopagereturn}">${u.stopagereturn}</label>
                            </div>
                        </c:forEach>

                    </div>
                    <hr style="margin-top: 6px; margin-bottom: 6px;">


                    <div class="row">
                        <div class="col-md-12">
                            <span style="font-size: 15px; font-weight: 600;">Price
                                range</span>
                            <div class="checkbox">

                                <p>
                                    <input type="text" id="amount"
                                        style="border: 0; color: #f6931f; font-weight: bold;" />
                                </p>

                                <div id="slider-container"></div>
                                <div id="slider-range"></div>

                            </div>


                        </div>
                    </div>


                    <hr style="margin-top: 6px; margin-bottom: 6px;">

                    <span style="font-size: 15px; font-weight: 600;">Airlines</span>
                    <c:forEach items="${flightnumberdata}" var="u">
                        <div class="row">
                            <div class="col-md-12">

                                <div class="checkbox" style="float: left;">
                                    <label><input type="checkbox" class="filter_link"
                                        data-filter="${u.airlineName}" value="" name="flnamefilture">${u.flightName}</label>
                                </div>
                            </div>
                        </div>
                    </c:forEach>

                </div>
            </div>
        </div>
        
      <div class="col-md-9" style="padding-left: 0px; padding-right: 0px;">
      	<div class="panel panel-default">
        <div class="panel-body">
          <div class="row">
            <div class="col-md-12 col-sm-12">

<div>
  		<div class="col-md-12 col-sm-12" style="padding-left: 0; padding-right: 5px;">
                <%-- <div class="banner">
                  <div class="row">
                  
                   <div class="col-md-12 col-sm-12">
                      <div class="col-md-8 col-sm-8">
                      <span>${srcHtml}</span>&nbsp;→&nbsp;<span>${destHtml}</span>
                      </div>
                      <div class="col-md-4 col-sm-4">

                      </div>
                      <div class="col-md-4 col-sm-4" style="text-align: right;"> <span class="date">${beginDate}</span>
                        <!-- <p><a href="#">Previous</a> | <a href="#">Next</a></p> -->
                      </div>
                    </div>
                    
                  </div>
                </div> --%>

                <!-- Flight Title =============================================== -->
                <div class="flight-wrap">
                  <div class="row wrap-title">
                    <div class="col-md-1 col-sm-1">
                      
                    </div>
                    <div class="col-md-5">
                    	<div class="col-md-6 col-sm-6" style="text-align: center;">
	                      <a href="#">Airline</a>
	                    </div>
	                    <div class="col-md-6 col-sm-6">
	                      <a href="#">Departure</a>
	                    </div>
                    </div>
                    <div class="col-md-4" style="padding-left: 0;">
                    	<div class="col-md-5 col-sm-5">
	                      <a href="#">Duration</a>
	                    </div>
	                    <div class="col-md-7 col-sm-7" style="text-align: center;">
	                      <a href="#">Arrival</a>
	                    </div>
                    </div>
                    <div class="col-md-2 col-sm-2" style="text-align: center;">
                      <a href="#">Price</a>
                    </div>
                  </div>
                </div>

                <!-- Flight =============================================== -->
      
               

         <c:forEach items="${flightdata}"  var="u" varStatus="loopCount">
               
                <span class="media ${u.airlineName}">
               <span class="stopage ${u.stopage}">
               <div class="computers">
				<div class="system" data-price="${u.amount}">
               
                <div class="row wrap-dtls">
                   <label>
                     <div class="col-md-1 col-sm-1" style="text-align: right; text-align: -webkit-right; padding-top: 98px;">
                      <input type="radio" class="form-control" onclick="destinationdetail('${u.engineID}','${u.flightNumber}','${u.arrivalTime}','${u.origin}','${u.duration}','${u.departureTime}','${u.destination}','${u.amount}','${u.searchId}','${u.number}','${u.stopage}','${loopCount.count}','${u.flightName}','${u.journeyTime}','${cabinType}')" name="df">
                      <p class="rederror"   id="responsemessagereturn${u.number}"> </p>
                    </div>
                    <div class="col-md-9">
                    	<div class="col-md-12" style="margin-bottom: 20px; padding-left: 0; padding-right: 0;">
	                    	<div class="col-md-6" style="padding-right: 0;">
		                    	<div class="col-md-12">
			                      	<div class="col-md-6 col-sm-6">
			                      	  <img src="${pageContext.request.contextPath}/resources/images/User/travel/img/airline/${u.airlineName}.jpg" alt="Not Found" onerror="this.src='${pageContext.request.contextPath}/resources/images/User/travel/img/airline/plane.png';" class="img-responsive" style="width: 60px;margin-left: -6px;">
				                        <!-- <img src="img/airline/indigo.jpg" class="img-responsive"> -->
				                          <h6>${u.flightName}</h6>
				                          <span class="air-num">${u.airlineName}-${u.flightNumber}</span>
				                      </div>
				                      <div class="col-md-6 col-sm-6"> <i class="fa fa-plane" style="font-size: 20px;"></i>
				                        <span class="dep">${source}</span>
				                        <br>
				                        <span class="rou-time">${u.departureTime}</span>
				                      </div>
			                      </div>
			                      
		                    </div>
		                    <div class="col-md-6" style="padding-right: 0;">
		                    	<div class="col-md-12">
			                      	<div class="col-md-7 col-sm-7">
			                      	
			                      <span class="air-num" id="json${u.number}" style="display: none;"> ${u.json} </span>
			                        <span class="air-num" id="jsonpaxfare${u.number}" style="display: none;"> ${u.jsonpaxfare} </span>
				                        <span class="dur">${u.journeyTime}</span>
				                        <br>
				                        <img class="rou-arrw" src="${pageContext.request.contextPath}/resources/images/arrow.svg">
				                        <br>
				                        <span class="stops">${u.stopage}</span>
				                      </div>
				                      <div class="col-md-5 col-sm-5">	<i class="fa fa-plane fa-flip-vertical" style="font-size: 20px;"></i>
				                        <span class="dep">${destination}</span>
				                        <br>
				                        <span class="rou-time">${u.arrivalTime}</span>
				                      </div>
			                      </div>
		                    </div>
	                    </div>
	                    
	                    <!-- ====return trip ===== -->
	                    <div class="col-md-12" style="padding-left: 0; padding-right: 0;">
	                    	<div class="col-md-6"  style="padding-right: 0;">
		                    	<div class="col-md-12">
			                      	<div class="col-md-6 col-sm-6">
			                      	  <img src="${pageContext.request.contextPath}/resources/images/User/travel/img/airline/${u.airlineName}.jpg" alt="Not Found" onerror="this.src='${pageContext.request.contextPath}/resources/images/User/travel/img/airline/plane.png';" class="img-responsive" style="width: 60px;margin-left: -6px;">
				                        <!-- <img src="img/airline/indigo.jpg" class="img-responsive"> -->
				                         <h6 id="flNamereturn${loopCount.count}">${u.flightName}</h6>
			                          <span class="air-num">${u.airlineNamereturn}-${u.flightNumberreturn}</span>
				                      </div>
				                      <div class="col-md-6 col-sm-6"> <i class="fa fa-plane" style="font-size: 20px;"></i>
				                       <span class="dep">${destination}</span>
				                        <br>
				                         <span class="rou-time" id="depreturn${loopCount.count}">${u.departureTimereturn}</span>
				                      </div>
			                      </div>
			                      
		                    </div>
		                    <div class="col-md-6" style="padding-right: 0;">
		                    	<div class="col-md-12">
			                      	<div class="col-md-7 col-sm-7">
			                      	
			                      <span class="air-num" id="json${u.number}" style="display: none;"> ${u.json} </span>
			                        <span class="air-num" id="jsonpaxfare${u.number}" style="display: none;"> ${u.jsonpaxfare} </span>
				                        <span class="dur" id="journeyTimeReturn${loopCount.count}">${u.journeyTimeReturn}</span>
				                        <br>
				                        <img class="rou-arrw" src="${pageContext.request.contextPath}/resources/images/arrow.svg">
				                        <br>
				                         <span class="stops" id="stopagereturn${loopCount.count}">${u.stopagereturn}</span>
				                      </div>
				                      <div class="col-md-5 col-sm-5">	<i class="fa fa-plane fa-flip-vertical" style="font-size: 20px;"></i>
				                        <span class="dep">${source}</span>
				                        <br>
				                        <span class="rou-time" id="arrreturn${loopCount.count}">${u.arrivalTimereturn}</span>
				                      </div>
			                      </div>
		                    </div>
	                    </div>
                    </div>
                    
                    
                    <div class="col-md-2">
                    	<div class="inter-round" style="padding-top: 72px;">
                    		<span class="fa fa-inr"></span>
                        	<span class="rou-fare-inter" id="amt${loopCount.count}">${u.amount}</span>
                    	</div>
                    </div>
                      
                  </label>  
                   
                </div>
                <hr>
				</div></div>
				</span></span>
                </c:forEach>
                <br><br>

              </div>


            <%-- <!-- Flight First Way Trip =============================================== -->
              <div class="col-md-6 col-sm-6" style="padding-right: 0; padding-left: 5px;">
                <div class="banner">
                  <div class="row">
                    <div class="col-md-12 col-sm-12">
                      <div class="col-md-8 col-sm-8">
                        <span>${destHtml}</span>&nbsp;→&nbsp;<span>${srcHtml}</span>
                      </div>
                      <div class="col-md-4 col-sm-4">

                       
                      </div>
                      <div class="col-md-4 col-sm-4" style="text-align: right;">
                       <span class="date">${endDate}</span>
                        <!-- <p><a href="#">Previous</a> | <a href="#">Next</a></p> -->
                      </div>
                    </div>
                  </div>
                </div>

                <!-- Flight Title =============================================== -->
	
                <div class="flight-wrap">
                  <div class="row wrap-title" style="">
                    
                    <div class="col-md-5">
                    	<div class="col-md-6 col-sm-6">
	                      <a href="#">Airline</a>
	                    </div>
	                    <div class="col-md-6 col-sm-6">
	                      <a href="#">Departure</a>
	                    </div>
                    </div>
                    <div class="col-md-5">
                    	<div class="col-md-6 col-sm-6">
	                      <a href="#">Duration</a>
	                    </div>
	                    <div class="col-md-6 col-sm-6">
	                      <a href="#">Arrival</a>
	                    </div>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-2">
                      <a href="#">Price</a>
                    </div>
                    
                  </div>
                </div>

                <!-- Flights =============================================== -->
                
                <c:forEach items="${flightdatareturn}" var="u" varStatus="loopCount">
                <span class="media ${u.airlineNamereturn}">
               <span class="stopage ${u.stopagereturn}">
               <div class="computers">
				<div class="system" data-price="${u.amount}">
                <div class="row wrap-dtls">
                  <label>
                      <div class="col-md-5">
                      	<div class="col-md-6 col-sm-6">
	                        <h6 id="flNamereturn${loopCount.count}">${u.flightName}</h6>
	                           
	                          <span class="air-num">${u.airlineNamereturn}-${u.flightNumberreturn}</span>
	                      </div>
	                      <div class="col-md-6 col-sm-6"><i class="fa fa-plane" style="font-size: 20px;"></i>
	                        <span class="dep">${destination}</span>
	                        <br>
	                        <span class="rou-time" id="depreturn${loopCount.count}">${u.departureTimereturn}</span>
	                      </div>
                      </div>
                      <div class="col-md-5">
                      	<div class="col-md-7 col-sm-7">
	                        <span class="dur">${u.journeyTime}</span>
	                        <br>
	                        <img class="rou-arrw2" src="${pageContext.request.contextPath}/resources/images/arrow.svg">
	                        <br>
	                        <span class="stops">${u.stopagereturn}</span>
	                      </div>
	                      <div class="col-md-5 col-sm-5">	<i class="fa fa-plane fa-flip-vertical" style="font-size: 20px;"></i>
	                        <span class="dep">${source}</span>
	                        <br>
	                        <span class="rou-time" id="arrreturn${loopCount.count}">${u.arrivalTimereturn}</span>
	                      </div>
                      </div>
                      <div class="col-md-2 col-sm-2 col-xs-4">
                        <span class="fa fa-inr"></span>
                        <span class="rou-fare" id="amt${loopCount.count}">${u.amount}</span>
                      </div> 
                  </label>  
                </div>
                <hr>
                </div></div></span></span>
			</c:forEach> --%>
			<br><br>
                
              </div>
</div>
              <!-- Flight Return Trip =============================================== -->
            
            </div>
          </div>
        </div>
      </div>
      </div>
  
    </div>
  </div>

  <!-- Bottom Navbar -->
 <nav class="navbar navbar-fixed-bottom navbar-default" id="flightdetailapishow" style="display: none;">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <div class="col-md-10 col-sm-10">
            <div class="row">
              <div class="col-md-12 col-sm-12">
             
                <div class="col-md-5 col-sm-5" style="border-right: 2px solid #000;">
                  <div class="row">
                    <div class="col-md-12 col-sm-12">
                     <div class="row">
                        <div class="col-md-3 col-sm-3">
                          <div class="row">
                             
                            <div class="col-md-8 col-sm-7">
                             <img id="flpriceimg" src="${pageContext.request.contextPath}/resources/images/User/travel/img/airline/${u.airlineName}.jpg" alt="Not Found" onerror="this.src='${pageContext.request.contextPath}/resources/images/User/travel/img/airline/plane.png';" class="airline-img" style="width: 45px;">
                              <span class="air-nme"><b id="flightname"> </b></span>
                              <br>
                              <span class="air-num" id="flightnumber"> </span>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-3 col-sm-3">
                          <span class="dept" id="departure"> </span>
                          <br>
                          <span class="dept" id="origin"> </span>
                        </div>
                        <div class="col-md-3 col-sm-3">
                          <span id="journeyTime"></span>
                          <br>
                          <span class="arr">→</span>
                          <br>
                          <span id="stopage"></span>
                        </div>
                        <div class="col-md-3 col-sm-3">
                          <span class="ariv" id="arrival"> </span>
                          <br>
                          <span class="ariv" id="destionation"> </span>
                        </div>
                        
                      </div>
                    </div>
                  </div>
                </div>
            
                <div class="col-md-5 col-sm-5" style="border-right: 2px solid #000;">
                  <div class="row">
                    <div class="col-md-12 col-sm-12">
                      <div class="row">
                        <div class="col-md-3 col-sm-3">
                          <div class="row">
                             
                            <div class="col-md-8 col-sm-8">
                             <img id="flpriceimgrtn" src="${pageContext.request.contextPath}/resources/images/User/travel/img/airline/${u.airlineName}.jpg" alt="Not Found" onerror="this.src='${pageContext.request.contextPath}/resources/images/User/travel/img/airline/plane.png';" class="airline-img" style="width: 45px;">
                              <span class="air-nme"><b id="flightnamereturn"> </b></span>
                              <br>
                              <span class="air-num" id="flightnumberreturn"> </span>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-3 col-sm-3">
                          <span class="dept" id="departurereturn"> </span>
                          <br>
                          <span class="dept" id="originreturn"> </span>
                        </div>
                        <div class="col-md-3 col-sm-3">
                          <span id="journeyTimereturn"></span>
                          <br>
                          <span class="arr">→</span>
                          <br>
                          <span id="stopagereturn"></span>
                        </div>
                        <div class="col-md-3 col-sm-3">
                          <span class="ariv" id="arrivalreturn"> </span>
                          <br>
                          <span class="ariv" id="destionationreturn"> </span>
                        </div>
                       
                      </div>
                    </div>
                  </div>
                </div>
               
                <div class="col-md-2 col-sm-2">
                 <span style="font-size: 12px;" id="amtflt"></span>
                 <span style="font-size: 12px;">Convenience Fee: 200</span>
                  <span class="fa fa-inr tot-sym"></span>
                  <span class="tot-fare" id="grandtotal"> </span>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-2 col-sm-2">
            <button type="submit" class="btn btn-continue" id="checkoutbutton" style="display: none; width: 42%;margin-left: 20px; margin-top: 20px;" onclick="checkoutFlight()">Book</button>
          </div>
        </div>
      </div>
    </div>
  </nav>

   <div id="errrepriceModel" role="dialog" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				<center id="common_error_msg" class="alert alert-danger"></center>
				<%-- <center><label id="common_error_msg" class="alert alert-danger"></label></center> --%>
			</div>
		</div>
	</div>
</div>
    
    <!-- Model for flight gif -->
	<div id="loading_flight" class="modal fade" role="dialog"
		style="margin-top: 7%;">
		<div class="modal-dialog" style="background:">

			<!-- Modal content-->
			<div class="modal-content" style="background: transparent; box-shadow: none; border: none;">
			<img src="${pageContext.request.contextPath}/resources/images/User/travel/img/Flight/flight.gif" class="img-responsive">
			</div>

		</div>
	</div>
	<!-- Model for flight gif -->
    
    
</body>

<script src="https://harvesthq.github.io/chosen/chosen.jquery.js"></script>

<script type="text/javascript">
 
function destinationdetail(airlineName,flightNumber,arrivalTime,origin,duration,departureTime,destination1,amount,searchId,id,stopage,colNo,flightName,journeyTime,cabinType)
{
	 var d=document.getElementById("json"+id).innerHTML;
	 var jsonpaxfare=document.getElementById("jsonpaxfare"+id).innerHTML;
	 
	 var deptreturn= $("#depreturn"+colNo).html();
	 var arrreturn= $("#arrreturn"+colNo).html();
	 var flNamereturn=$("#flNamereturn"+colNo).html();
	 var journeyTimeReturn=$("#journeyTimeReturn"+colNo).html();
	 var stopagereturn=$("#stopagereturn"+colNo).html();
	 
	 console.log("colNo: "+colNo);
	 
	 $("#flightdetailapishow").hide();
	 
	 $('#loading_flight').modal({
		  backdrop: 'static',
		  keyboard: true, 
	      show: true
		});
	 
// 	 console.log(jsonpaxfare);
// 	 ${u.number}
		$.ajax({
			type : "POST",
			contentType : "application/json",
			url : "${pageContext.request.contextPath}/Agent/Travel/Flight/internationalConnectingShowPriceDetail",
			data : JSON.stringify({
				
				"engineID" : "" +airlineName + "",
				"flightNumber" : "" +flightNumber + "",
				"onewayflight" : "" +"Firstfligt" + "",
				"searchId" : "" +searchId + "",
				"jsonbonds" : "" +d + "",
				"jsonpaxfare" : "" +jsonpaxfare + "",
				"colNo":""+colNo+"",
				"cabin":""+cabinType+""
			}),
			success : function(response) {
			
				var totalFare = new Array();

				totalFare = response.split("@");

				
				console.log("respis:: "+totalFare[0]);
				
		 if(totalFare[0]==="S00")
		 {
		    var source=document.getElementById("sourceinternational").value;
		    var destination=document.getElementById("destinationinternational").value;
		    
			 var amountupdate = new Array();
			 amountupdate=totalFare[1].split("#");
			 
			 document.getElementById("flightname").innerHTML=""+flightName;
			 
			 
			 document.getElementById("origin").innerHTML=""+source;
			 
			 document.getElementById("destionation").innerHTML=""+destination; 

			 
			 document.getElementById("flightnamereturn").innerHTML=""+flNamereturn;
			 
			 
			 document.getElementById("originreturn").innerHTML=""+destination;
			 
			 document.getElementById("destionationreturn").innerHTML=""+source; 
			  
			 document.getElementById("journeyTime").innerHTML=""+journeyTime;  
			 document.getElementById("stopage").innerHTML=""+stopage;   
			 document.getElementById("grandtotal" ).innerHTML=""+amountupdate[3];
			 
			 document.getElementById("departure").innerHTML=""+departureTime;
			 document.getElementById("arrival").innerHTML=""+arrivalTime; 
			 
			 document.getElementById("amtflt").innerHTML="Amount: "+amount;  
			 document.getElementById("journeyTimereturn").innerHTML=""+journeyTimeReturn;  
			 document.getElementById("stopagereturn").innerHTML=""+stopagereturn;  
			 
			 document.getElementById("departurereturn").innerHTML=""+deptreturn;
			 document.getElementById("arrivalreturn").innerHTML=""+arrreturn; 
			 
			/*  if(d==0) 
			 {
				 console.log("Price recheck not show");
				 $("#loading_flight").modal('hide');
				 $('.modal-backdrop').remove();
			 }
		 else
			 { */
			 console.log("Price recheck show");
			 $("#flightdetailapishow").show();
			 $("#checkoutbutton").show();
			 $("#loading_flight").modal('hide');
			 $('.modal-backdrop').remove();
			
		 }
		 else if(totalFare[0]==="F03")
		 {
			 window.location.href='${pageContext.request.contextPath}/Home'; 
		 }
	 else 
	 {
		 /* $("#loader"+id).hide();
		 $("#"+id).collapse('hide'); */
		 $("#loading_flight").modal('hide');
		 $('.modal-backdrop').remove();
		 $("#common_error_msg").html("Flight sold out. Please try another");
		 $("#errrepriceModel").modal("show");
		 
// 		 document.getElementById("responsemessagereturn"+id).innerHTML="This Flight Not Available.";	
				
	 } 
	 }
		});
	 
}


function checkoutFlight()
{
	 window.location = "InternationalCheckOut";
}


/* function setsrcanddestination() {

	var traveldata = document.getElementById("traveldataval").value;
	var travel_code = new Array();

	travel_code = traveldata.split("!");

	var travel_name = new Array();
	travel_name = travel_code[1].split("@");

	var sourcecode = new Array();
	sourcecode = travel_code[0].split("#");

	document.getElementById("flightsrc").innerHTML = "";
	for (i = 0; i < travel_name.length; i++) {
		var option = document.createElement("option");

		option.text = travel_name[i];
		option.value = sourcecode[i];

		var select = document.getElementById("flightsrc");
		if (i == 0) {
			option.text = "Select Source";
			option.value = "#";
			select.add(option);

		}

		select.add(option);
		console.log("dd");
	}
 
} */


</script>

<script>
    $( "#flightsrc" ).select2({
        theme: "bootstrap"
    });
    $( "#flightdest" ).select2({
        theme: "bootstrap"
    });
    $( "#location_list" ).select2({
        theme: "bootstrap"
    });
    $( "#bus_src_list" ).select2({
        theme: "bootstrap"
    });
    $( "#bus_des_list" ).select2({
        theme: "bootstrap"
    });
    $( "#holiday_list" ).select2({
        theme: "bootstrap"
    });
</script>

<!-- ================ Script for Flight traveller details ============================ -->
<script>
    $(document).ready(
        function(){
            $("#selTrav").click(function () {
                $("#trav_menu").fadeToggle();
            });

        });
</script>

<!-- ================ Script for Room traveller details ============================ -->
<script>
    $(document).ready(
        function(){
            $("#rm_ppl").click(function () {
                $("#htl_menu").fadeToggle();
            });

        });
    
    

  	 function increaseValue(id,idset)
  	 {
       	  var value = parseInt(document.getElementById(""+id).value, 10);
       	  if(value<=4)
       		  {
       		 var d = parseInt(document.getElementById("totalCounttrevaler").innerHTML);
       		 var c=parseInt(d);
           	 c++;
           	document.getElementById("totalCounttrevaler").innerHTML=""+c;
       	  value = isNaN(value) ? 0 : value;
       	  value++;
       	  document.getElementById(""+idset).innerHTML = ""+value;
       	  document.getElementById(""+id).value = ""+value;
       	
       		  }
       	}

       	function decreaseValue(id,idset) 
       	{
       	  var value = parseInt(document.getElementById(""+id).value, 10);
       	 if(value>0)
  		  {
  		 
  	
     		var d = parseInt(document.getElementById("totalCounttrevaler").innerHTML);
    		 var c=parseInt(d);
        	 c--;
        	document.getElementById("totalCounttrevaler").innerHTML=""+c;
  	
  		  }
       	  value = isNaN(value) ? 0 : value;
       	  value < 1 ? value = 1 : '';
       	  value--;
       	 
       	  document.getElementById(""+idset).innerHTML = ""+value;
       	  document.getElementById(""+id).value = ""+value;
       	
       	 
       	}
       	function changesclass() 
       	{
       		var d =document.getElementById("flightcabin").value;
       	document.getElementById("flight_cls").innerHTML=", "+d;
       	}
      	function closedivtrevalinformation()
      	{
      		$("#trav_menu").hide();
      	}
    	
    	
    	
    	function getvalue() 
    	{
    		$("#loader").show();
    		$("#trav_menu").hide();
    		
    		var contextPath = "${pageContext.request.contextPath}";
    		var spinnerUrl = "Please wait <img src="+contextPath+"'/resources/images/spinner.gif' style='width:25px;'>";
    		
    		$("#loader").html(spinnerUrl);
    		$("#loader").addClass("disabled");
    		
    		document.getElementById("errormsgsrc").innerHTML="";
    		document.getElementById("responsemessage").innerHTML="";
    	    document.getElementById("errormsgdest").innerHTML="";
    		var flightdest = document.getElementById("flightdest");
    		var flightdestselect = flightdest.options[flightdest.selectedIndex].value;

    		var flightsrc = document.getElementById("flightsrc");
    		var flightsrcselect = flightsrc.options[flightsrc.selectedIndex].value;
    		var date = document.getElementById("date").value;
    		
    	//	var tripType = document.getElementById("tripType").value;
    		var cabin = document.getElementById("flightcabin").value;
    		var adults = document.getElementById("adult1").innerHTML;
    		var childs = document.getElementById("childs1").innerHTML;
    		var infants = document.getElementById("infants1").innerHTML;
    		var returndate = document.getElementById("returndate").value;
    		 
    		var srchtml=flightsrc.options[flightsrc.selectedIndex].text;
    		var desthtml=flightdest.options[flightdest.selectedIndex].text;
    		
    		
    		 if(flightsrcselect.length <=1)
    			{
//     			$("#loader").hide();
				$("#loader").html("Modify Search");
	            $("#loader").removeClass("disabled");
    			document.getElementById("errormsgsrc").innerHTML="Please Select Source";
    			}
    		 else if(flightdestselect.length <=1)
    			{
//     			$("#loader").hide();
				$("#loader").html("Modify Search");
	            $("#loader").removeClass("disabled");
    			document.getElementById("errormsgdest").innerHTML="Please Select Destination";
    			}
    		 else if(date.length <=1)
 			{
//     			 $("#loader").hide();
				$("#loader").html("Modify Search");
	            $("#loader").removeClass("disabled");
 			document.getElementById("errormsgdate").innerHTML="Please Select Date";
 			}
    		 else if(adults<1)
  			{
//      			 $("#loader").hide();
				$("#loader").html("Modify Search");
	            $("#loader").removeClass("disabled");
  			document.getElementById("errormsgadult").innerHTML="Please Add One Adult";
  			}
    			else
    				{
    				if(returndate.length <=1)
    				{
    					$.ajax({
    						type : "POST",
    						contentType : "application/json",
    						url : "${pageContext.request.contextPath}/Agent/Travel/Flight/OneWay",
    						data : JSON.stringify({
    							"origin" : "" +flightsrcselect + "",

    							"destination" : "" +flightdestselect + "",

    							"beginDate" : "" +date + "",

    							"engineIDs" : "" +"AYTM00011111111110002" + "",

    							"tripType" : "" +"OneWay" + "",

    							"cabin" : "" +cabin + "",

    							"adults" : "" +adults + "",

    							"childs" : "" + childs+ "",

    							"infants" : "" +infants + "",
    							"endDate":"" +date + "",
    							"traceId" : "" + "AYTM00011111111110002"+ "",
    							"srcFullName":""+srchtml+"",
     							"destFullName":""+desthtml+"",
    							
     
    						}),
    						success : function(response) {

    							 var d=response.split("#");
    							 if(d[0]==="S00")
    							 {
    							  window.location = "OneWay";
    					 		 }
    							 else if(d[0]==="F03")
    							 {
    								 window.location.href='${pageContext.request.contextPath}/Home'; 
    							 }
    							  else if (d[0]==="F02") {
//      								 $("#loader").hide();
 									$("#loader").html("Modify Search");
                    				$("#loader").removeClass("disabled");
     								 $("#loading_flight").hide();
     								 $("#flght_srch").removeClass("disabled");
     								 document.getElementById("responsemessage").innerHTML="Server Down.Please Try Again Later";	
     							 }
    				 else
    				 {
    					 $("#loader").html("Modify Search");
    			         $("#loader").removeClass("disabled");
//     					 $("#loader").hide();
    					 $("#loading_flight").hide();
    					 $("#flght_srch").removeClass("disabled");
    					 console.log("undefined"+d[1]);
    					 document.getElementById("responsemessage").innerHTML=""+d[1];	

    				 }
    				}
    					});
    				}
    				else
    					{

    					$.ajax({
    						type : "POST",
    						contentType : "application/json",
    						url : "${pageContext.request.contextPath}/Agent/Travel/Flight/OneWay",
    						data : JSON.stringify({
    							"origin" : "" +flightsrcselect + "",

    							"destination" : "" +flightdestselect + "",

    							"beginDate" : "" +date + "",

    							"engineIDs" : "" +"AYTM00011111111110002" + "",

    							"tripType" : "" +"RoundTrip" + "",

    							"cabin" : "" +cabin + "",

    							"adults" : "" +adults + "",

    							"childs" : "" + childs+ "",

    							"infants" : "" +infants + "",
    							"endDate":"" +returndate + "",
    							"traceId" : "" + "AYTM00011111111110002"+ "",
    							
    							"srcFullName":""+srchtml+"",
     							"destFullName":""+desthtml+"",
     
    						}),
    						success : function(response) {
    					 
    							 var d=response.split("#");
    							 if(d[0]==="S00")
    							 {
    								 if(d[1]==="DomesticRoundway")
    									 {
    									 window.location = "RoundWay";
    									 }
    								 else
    									 {
    									 window.location = "internationalRoundWay";
    									 }
    								
    							 }
    							 else if(d[0]==="F03")
    							 {
    								 window.location.href='${pageContext.request.contextPath}/Home'; 
    							 }
    						 else
    						 {
//     							 $("#loader").hide();
								 $("#loader").html("Modify Search");
 			   			         $("#loader").removeClass("disabled");
    							 $("#loading_flight").hide();
    							 $("#flght_srch").removeClass("disabled");
    							 console.log("udefined: "+d[1]);
    							 document.getElementById("responsemessage").innerHTML=""+d[1];	
    									
    						 }
    							
    						}
    					});
    					
    					}

    	}
    	}
    	
    	
    	function changeval() {
    		var traveldata = document.getElementById("traveldataval").value;
    		var travel_code = new Array();

    		travel_code = traveldata.split("!");

    		var travel_name = new Array();
    		travel_name = travel_code[1].split("@");

    		var sourcecode = new Array();
    		sourcecode = travel_code[0].split("#");

    		var flightsrc = document.getElementById("flightsrc");
    		var flightsrcselect = flightsrc.options[flightsrc.selectedIndex].value;

    		document.getElementById("flightdest").innerHTML = "";
    		for (i = 0; i < travel_name.length; i++) {
    			
    			if (flightsrcselect === "" + sourcecode[i]) {

    			} else {
    				var option = document.createElement("option");

    				option.text = travel_name[i];
    				option.value = sourcecode[i];
    				var select = document.getElementById("flightdest");
    				if (i == 0) {
    					option.text = "Select Destination";
    					option.value = "#";
    					select.add(option);
    				}
    				select.add(option);
    			}

    		}
    	}
    	
    	
    	
    	
      	 var tmp=[];
    	 $('.filter_link').click(function(e){
    		 
    		 $("input[id='destCheck']").attr('checked', false);
    		 $("input[id='srcCheck']").attr('checked', false);
    		  
    	var mediaElements = $('.media');
    	// get the category from the attribute
    	var filterVal = $(this).data('filter');

    	if (tmp.indexOf(filterVal)==-1) {
    		
    		tmp.push(filterVal);
    		console.log("tmp :: if"+filterVal);
    	}
    	else {
    		var a=tmp.indexOf(filterVal);
    		tmp.splice(a,1);
    		console.log("tmp :: else"+tmp);
    	}
    	
    	console.log("tmp length"+tmp.length);
    	
    	mediaElements.hide();
    	
    	if (tmp.length==0) {
    		console.log("Show all");
    		 mediaElements.show();
    	}
    	
    	
    	for (var i = 0; i <tmp.length; i++) {
    		console.log("filterVal:: "+tmp[i]);
    	 mediaElements.filter('.' + tmp[i]).show();
    	}
    	
    	
//     	mediaElements.hide().filter('.' + filterVal).show();

    });
    	 
    	 
    	 var tmpStop=[];
    	 
    	 $('.filter_link_stopage').click(function(e){
    		 
    		 $("input[id='destCheck']").attr('checked', false);
    		  $("input[id='srcCheck']").attr('checked', false);
    			
    		 var mediaElements = $('.stopage');
    			// get the category from the attribute
    			
    			var filterVal = $(this).data('filter');

    			mediaElements.hide();
    		
    			 if(filterVal === 'all'){
    			   mediaElements.show();
//     			 $("input[name='stopage']").attr('checked', false);
    			      
    			  for (var i = 0; i <tmpStop.length; i++) {
    				  console.log("tmpStop "+i+": "+tmpStop[i]);
    				  $("input[name='"+tmpStop[i]+"']").attr('checked', false);
    			   }  
    			  tmpStop=[];
    			    }else{
    			    	mediaElements.hide();
    			    	 $("input[name='stopageall']").attr('checked', false);
    			    	if (tmpStop.indexOf(filterVal)==-1) {
    			    		
    			    		tmpStop.push(filterVal);
    			    		console.log("tmp Stop:: if"+filterVal);
    					}
    			    	else {
    						var a=tmpStop.indexOf(filterVal);
    						tmpStop.splice(a,1);
    						console.log("tmp Stop:: else"+tmpStop);
    					}

    			    	// hide all then filter the ones to show
    			       
    			    	console.log("tmp Stop length"+tmpStop.length);
    			    	if (tmpStop.length==0) {
    			    		mediaElements.show();
    					}
    			    	
    			       for (var i = 0; i <tmpStop.length; i++) {
    			    	   
    					console.log("filterVal::"+i+" "+tmpStop[i]);
    					
    	 				mediaElements.filter('.' + tmpStop[i]).show();
    				  }
    			    }
    		});


    	 
    	 $('#clr_filtr').click(function(e){
    		 
    		 $("input[name='df']").attr('checked', false);
    		
    		 
    			var mediaElements = $('.stopage');
    			var mediaElement = $('.media');
    			// get the category from the attribute
    			
    			 mediaElements.show();
    			 mediaElement.show();
    			 $("input[name='flnamefilture']").attr('checked', false);
    			 
    			 $("input[name='stopageall']").attr('checked', false);
    			 $("input[name='stopage']").attr('checked', false);
    			 
    			 for (var i = 0; i <tmpStop.length; i++) {
    				  $("input[name='"+tmpStop[i]+"']").attr('checked', false);
    				   }  
    			 
    			 
    			 tmp=[];
    			 tmpStop=[];
    			 
    		});
    	
    	
</script>
	 
</html>