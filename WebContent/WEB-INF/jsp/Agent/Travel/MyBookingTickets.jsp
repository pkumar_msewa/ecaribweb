<!DOCTYPE HTML>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<html>
<head>
    <title>My Trips</title>

    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'type="image/png" />
    <!--- GOOGLE FONTS - -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600' rel='stylesheet' type='text/css'>
    <!-- /GOOGLE FONTS -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/travel_css/bootstrap.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script type="text/javascript" 
    src="<c:url value="/resources/js/Agent/Agentdetail.js"/>"></script>
    <!--<link rel="stylesheet" href="css/font-awesome.css">-->
   <link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/travel_css/icomoon.css">
  <link rel="stylesheet" href="/resources/css/new_css/newstyles.css">   
    
 <%--    <link rel="stylesheet" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/select2.1.css">
    <script src="js/select2.min.js"></script> --%>

<script type="text/javascript" src="<c:url value="/resources/js/userdetails.js"/>"></script>

    <link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
      <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/new_css/default.css">
   <style>
        .all-tkts .nav-tabs > li > a {
            color: black;
        }
       .all-tkts .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus {
            color: blue;
       }
       .all-tkts .nav-tabs > li > a:hover {
            color: red;
       }
       .nav-tabs {
        border-bottom: 1px solid #ddd;
       }
       .table > thead > tr > th {
            border-bottom: none;
            background: #efefef;
       }
       .table {
        margin-top: 10px;
       }
       
        .trv {
            font-size: 12px;
        }
        .trv-prc {
            font-weight: bold;
        }
        a {
            color: #808080;
        }
        a:hover {
            color: #808080;
        }
        hr{
            margin-top: 0;
            margin-bottom: 0;
        }
        .user-tickets {
            margin-top: 10px;
            height: 100px;
        }
        .lod-btn {
            color: blue;
            background: transparent;
            border: 1px solid blue;
            transition: all 600ms ease-in-out;
            -webkit-transition: all 600ms ease-in-out;
            -moz-transition: all 600ms ease-in-out;
            -o-transition: all 600ms ease-in-out;
        }
        .lod-btn:hover {
            color: #fff;
            background: blue;
            bor/der: 1px solid blue;
        }
   </style>
   
</head>

<body style="background: lightblue;">
<jsp:include page="/WEB-INF/jsp/Agent/AHeader.jsp" />
 <input type="hidden" value="${val}" id="value">
<br>
<!-- ====================== TOP AREA ================== -->
        <div class="show-onload">
            <div class="bg-holder full">
                <div class="bg-mask"></div>
                
                <div class="bg-content">
                    <div class="container-fluid">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="all-tkts">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="top-toggle">
                                                <ul class="nav nav-tabs">
                                                    
                                                   <li class="" id="flightTicket">
                                                        <a href="#tab_flighttkt" data-toggle="tab" >
                                                        <b>Flight Tickets</b></a>
                                                    </li> 
                                                    <li id="busTicket">
                                                        <a href="#" data-toggle="tab" >
                                                        <b>Bus Tickets</b></a>
                                                    </li>
                                                </ul>
                                                
                                                <c:choose>
                                                <c:when test="${val == 'flight'}">
                                                 <jsp:include page="/WEB-INF/jsp/Agent/Travel/Flight/MyBookingTickets.jsp" />
                                                </c:when>
                                                <c:when test="${val == 'bus'}">
                                                 <jsp:include page="/WEB-INF/jsp/Agent/Travel/Bus/MyBookingTickets.jsp" />
                                                </c:when>
                                                </c:choose>
                                                
                                                 
                                            </div>
                                        </div>
                                    </div>
         
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
       
        <!-- END TOP AREA  -->
</body>


<script type="text/javascript">
var val=$("#value").val();

if (val=="bus") {
	 $("#busTicket").addClass('active');
}
else if (val=="flight") {
	 $("#flightTicket").addClass('active');
}
$("#busTicket").click(function() {
	var contextPath = "${pageContext.request.contextPath}";
	 
	window.location.href='${pageContext.request.contextPath}/Agent/Travel/Bus/MyTickets'; 
	 
	});
	
$("#flightTicket").click(function() {
	var contextPath = "${pageContext.request.contextPath}";
	
	window.location.href='${pageContext.request.contextPath}/Agent/Travel/Flight/MyTickets'; 
		
	});
</script>



</html>