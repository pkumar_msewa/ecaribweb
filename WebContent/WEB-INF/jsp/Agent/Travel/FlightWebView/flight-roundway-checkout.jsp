<!DOCTYPE HTML>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<html>
<head>
<title>Check Out</title>


<meta content="text/html;charset=utf-8" http-equiv="Content-Type">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'
	type="image/png" />
<link rel="stylesheet" href="/resources/css/new_css/cloud-admin.css">
<link rel="stylesheet" href="/resources/css/font-awesome.min.css">
<link rel="stylesheet" href="/resources/css/new_css/default.css">
<link rel="stylesheet" href="/resources/css/new_css/responsive.css">
<link rel="stylesheet"
	href="/resources/css/new_css/uniform.default.min.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/bootstrap-chosen.css">
<!--- GOOGLE FONTS - -->
<link
	href='https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700'
	rel='stylesheet' type='text/css'>
<link
	href='https://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600'
	rel='stylesheet' type='text/css'>
<!-- /GOOGLE FONTS -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/travel_css/bootstrap.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<!--<link rel="stylesheet" href="css/font-awesome.css">-->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/travel_css/icomoon.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/travel_css/styles.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/travel_css/mystyles.css">
	
	<script type="text/javascript" src="<c:url value="/resources/js/userdetails.js"/>"></script>
<!-- <link rel="stylesheet" href="css/mystyles.css">   -->

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css" />


<style>
		.rederror {
			font-size: 12px;
    		color: red;
		}
		.age{
			width: 52px;
		}
	</style>
<script type="text/javascript">
$(function() {
		  $("input[name='date']").datepicker({
			  format : "yyyy/mm/dd",
			 
		}).on('change', function() {
			$('.datepicker').hide();
		});  
	});
	

</script>

</head>


<body style="background: #efefef;" onload="setDetail()">
	<input type="hidden" value="${adults}" id="adults">
	<input type="hidden" value="${childs}" id="childs">
	<input type="hidden" value="${infants}" id="infants">

	<input type="hidden" value="${checkoutDeatil}" id="checkoutDeatil">

<input type="hidden" value="${returncheckoutDeatil}" id="returncheckoutDeatil">

		<jsp:include page="/WEB-INF/jsp/User/Header.jsp" />
	
	<br>

	<div class="container-fluid chkout-wrp">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="col-md-8 col-sm-8">
					

					<!-- Customer Details =========================-->
					<div class="panel panel-default">
						<div class="panel-heading">
							<img
								src="${pageContext.request.contextPath}/resources/images/new_img/bag.png"
								class="chkout-img"> <span><b>Travellers Details</b></span>
						</div>
						<div class="panel-body">
								 <div id="container">
								 <!-- Adult -->
							<form action="">
				  		 <c:forEach items="${adultsarr}" varStatus="loopCount">
				  		<div class='dtls-wrap' style='margin-bottom: 20px;'>
				        <div class='col-md-2 col-sm-2'>
				          <span><b>Adult ${loopCount.count} </b></span><span> </span>
				       </div>
				        <div class='col-md-2 col-sm-2'>
				          <div class='form-group'>
				            <select class='form-control title'  id="useradultsgender${loopCount.count}">
				              <option>Mr</option>
				              <option>Ms</option>
				              <option>Mrs</option>
				            </select>
				             <p class="rederror" id="erra_title${loopCount.count}"></p>
				          </div>
				       </div>
				        <div class='col-md-8 col-sm-8'>
				          <div class='col-md-6 col-sm-6'>
				            <div class='form-group'>
				             <input type='text' class='form-control' value="" id='userfirstnameadults${loopCount.count}' placeholder='Enter First Name' onkeypress="$('#erra_fName${loopCount.count}').empty();">
				             <p class="rederror" id="erra_fName${loopCount.count}"></p>
				            </div>
				          </div>
				          <div class='col-md-6 col-md-6'>
				            <div class='form-group'>
				             <input type='text' class='form-control'  value="" id='userlastnameadults${loopCount.count}' placeholder='Enter Last Name' onkeypress="$('#erra_lName${loopCount.count}').empty();">
				            <p class="rederror" id="erra_lName${loopCount.count}"></p>
				            </div>
				         </div>
				        </div>
				  		</div>
				  		</c:forEach>

				  		<c:forEach items="${childsarr}" varStatus="loopCount">
				  		<div class='dtls-wrap' style='margin-bottom: 20px;'>
				        <div class='col-md-2 col-sm-2'>
				          <span><b>Child ${loopCount.count}</b></span><span> </span>
				       </div>
				        <div class='col-md-2 col-sm-2'>
				          <div class='form-group'>
				            <select class='form-control title' id="userchildsgender${loopCount.count}">
				              <option>Mr</option>
				              <option>Ms</option>
				              <option>Mrs</option>
				            </select>
				            <p class="rederror" id="errc_title${loopCount.count}"></p>
				          </div>
				       </div>
				        <div class='col-md-8 col-sm-8'>
				          <div class='col-md-6 col-sm-6'>
				            <div class='form-group'>
				             <input type='text' class='form-control' id='userfirstnamechilds${loopCount.count}' value="" placeholder='Enter First Name' onkeypress="$('#errc_fName${loopCount.count}').empty();">
				             <p class="rederror" id="errc_fName${loopCount.count}"></p>
				            </div>
				          </div>
				          <div class='col-md-6 col-md-6'>
				            <div class='form-group'>
				             <input type='text' class='form-control' value="" id='userlastnamechilds${loopCount.count}' placeholder='Enter Last Name' onkeypress="$('#errc_lName${loopCount.count}').empty();">
				             <p class="rederror" id="errc_lName${loopCount.count}"></p>
				            </div>
				         </div>
				        </div>
				  		</div>
				  		</c:forEach>

				  		<c:forEach items="${infantsarr}" varStatus="loopCount">
				  		<div class='dtls-wrap' style='margin-bottom: 20px;'>
				        <div class='col-md-2 col-sm-2'>
				          <span><b>Infant ${loopCount.count}</b></span><span> </span>
				       </div>
				        <div class='col-md-2 col-sm-2'>
				          <div class='form-group'>
				            <select class='form-control title' id="userinfantsgender${loopCount.count}">
				              <option>Mr</option>
				              <option>Ms</option>
				              <option>Mrs</option>
				            </select>
				             <p class="rederror" id="erri_title${loopCount.count}"></p>
				          </div>
				       </div>
				        <div class='col-md-8 col-sm-8'>
				          <div class='col-md-6 col-sm-6'>
				            <div class='form-group'>
				             <input type='text' class='form-control' value="" id='userfirstnameinfants${loopCount.count}' placeholder='Enter First Name' onkeypress="$('#erri_fName${loopCount.count}').empty();">
				            <p class="rederror" id="erri_fName${loopCount.count}"></p>
				            </div>
				          </div>
				          <div class='col-md-6 col-md-6'>
				            <div class='form-group'>
				             <input type='text' class='form-control' value="" id='userlastnameinfants${loopCount.count}' placeholder='Enter Last Name' onkeypress="$('#erri_lName${loopCount.count}').empty();">
				            <p class="rederror" id="erri_lName${loopCount.count}"></p>
				            </div>
				         </div>
				         <div class='col-md-6 col-md-6'>
				            <div class='form-group'>
				              <input type="text" class="form-control" placeholder="Date of birth" name="date"
								  id="infantsdob${loopCount.count}" onchange="$('#erri_dob${loopCount.count}').empty();"> 
					            <p class="rederror" id="erri_dob${loopCount.count}"></p>
				            </div>
				         </div>
				        </div>
				  		</div>
				  		</c:forEach>
						
							<div class='row'>
								<div class='col-md-12 col-sm-12'>
									<span><b>Contact Details</b></span> <br>
									<div class='col-md-6 col-sm-6'>
										<div class='form-group'>
											<input type='email' class='form-control' id='useremailid' value=""
												placeholder='Enter Your Email ID' onkeypress="$('#pesengerdeatilemail').html('');">
											<p style='color: red;' id='pesengerdeatilemail'></p>
											<span class='msg'>Your ticket will be sent to this
												email address</span>
										</div>
									</div>
									<div class='col-md-6 col-sm-6'>
										<div class='form-group'>
											<input type='text' class='form-control numeric' id='usermobilenumber' value=""
												onkeypress="$('#pesengerdeatilmobile').html('');"
												placeholder='Enter Your Mobile Number'>
											<p style='color: red;' id='pesengerdeatilmobile'></p>
											<span class='msg'>Your Mobile number will be used only
												for sending flight related communication.</span>

											<p class='rederror' id='responsemessage'></p>

											<div style='display: none;' id='loader'>
												<img src='/resources/images/spinner.gif' style='width: 10%;'>
											</div>

										</div>
									</div>
									
								</div>
							</div>
							
					<div class='panel-group'>
				    <div class='panel panel-default'>
				    <div class='panel-heading'>
				    <h4 class='panel-title'>
				     <a data-toggle='collapse' href='#collapse1'>Payment</a>
				     </h4>
				    </div>
				    <div id='collapse1' class='panel-collapse collapse'>
				     <div class='panel-body'>
				      <div class='row'>
				       <div class='col-md-12 col-sm-12'>
					<div class='row' style='margin-top: 50px;'>
					 <div class='col-md-12'>
					 <label class='radio-inline'><input type='radio' id='vnetId' name='optradio' value='vnet'><img src='${pageContext.request.contextPath}/resources/images/User/travel/img/Flight/VNET.png' alt='Vnet' style='width: 120px; margin-top: -10px;'></label>
					 <label class='radio-inline'><input type='radio' id='ebsId' name='optradio' checked='checked' value='ebs'><img src='${pageContext.request.contextPath}/resources/images/User/travel/img/Flight/EBS.png' alt='Others' style='width: 120px; margin-top: -10px;'></label>
					<label class='radio-inline'><input type='radio' id='walletId' name='optradio' value='wallet'><img src='${pageContext.request.contextPath}/resources/images/User/travel/img/Flight/Wallet.png' alt='Wallet' style='width: 120px;margin-top: -10px;'></label>
					</div>
					<br>
					<button class='btn btn-primary contnu' style='margin-left: 173px; margin-top: 19px; width: 119px; height: 43px;' onclick='openpaymentgateselection()' id='paymentbtn' type="button">Continue</button>
					</div>
					</div>
				     </div>
				     </div>
				    </div>
				   </div>
				  </div>
				  </form>
				 </div>
						</div>
					</div>
				</div>

				<!-- CheckOut Receipt ============================= -->
				<div class="col-md-4 col-sm-4">
            <div class="panel panel-default">
              <div class="panel-heading" style="height: 61px;">
                <img src="${pageContext.request.contextPath}/resources/images/new_img/price.png" class="chkout-img-price">
                <span><b>Trip Summary</b></span>
              </div>
              <div class="panel-body">
                <div class="row">
                  <div class="hpadding30 margtop30" style="padding: 0 30px; margin-top: 30px!important;">
                  <!-- GOING TO -->
                  
                  <c:forEach items="${arrivalLGS}" var="datas" >
                  
                  <div>
                    <div class="wh33percent left size12 bold dark" style="width: 67%; color: #333; font-size: 12px; font-weight: 700; float: left;">
                     <span class="chkout-city" id="source">${datas.origin} </span>  
                    </div>
                   
                    <div class="wh33percent right textright size12 bold dark" style="width: 33%; color: #333; font-size: 12px; font-weight: 700;margin: 0 auto; text-align: right; float: left;">
                    <span class="chkout-place" id="destination">${datas.destination} </span>
                    </div>
                    <div class="clearfix" style="float: none; clear: both;"></div>
                    
                    <div class="wh33percent left" style="width: 33%; float: left;">
                      <div class="fcircle" style="width: 39px; height: 39px; border: 2px solid #ebebeb; background: #fff; -webkit-border-radius: 100px; -moz-border-radius: 100px; border-radius: 100px; position: relative; z-index: 100;">
                        <span class="fdeparture" style="width: 25px; height: 21px; background: url(${pageContext.request.contextPath}/resources/images/new_img/departure.png) no-repeat; display: block; margin: 6px 0px 0px 4px;"></span>
                      </div>
                    </div>

                    <div class="wh33percent left" style="width: 33%; float: left; text-align: center; text-align: -webkit-center; margin: 0 auto;">
                      <div class="fcircle center" style="width: 39px; height: 39px; border: 2px solid #ebebeb; background: #fff; -webkit-border-radius: 100px; -moz-border-radius: 100px; border-radius: 100px; position: relative; z-index: 100;">
                        <span class="fstop" style="width: 25px; height: 21px; background: url(${pageContext.request.contextPath}/resources/images/new_img/clock1.png) no-repeat; display: block; margin: 6px 0px 0px 4px;"></span>
                      </div>
                    </div>

                    <div class="wh33percent right" style="width: 13%; float: right;">
                      <div class="fcircle right" style="width: 39px; height: 39px; border: 2px solid #ebebeb; background: #fff; -webkit-border-radius: 100px; -moz-border-radius: 100px; border-radius: 100px; position: relative; z-index: 100;">
                        <span class="farrival" style="width: 25px; height: 21px; background: url(${pageContext.request.contextPath}/resources/images/new_img/arrival.png) no-repeat; display: block; margin: 6px 0px 0px 4px;"></span>
                      </div>
                    </div>

                    <div class="clearfix" style="float: none; clear: both;"></div>
                    <div class="fline2px" style="width: 100%; height: 2px; background: #ebebeb; display: block; position: relative; top: -20px; z-index: 10;"></div>
                    
                    <div class="wh33percent left size12" style="width: 33%; font-size: 12px; float: left;">
                     <span class="chkout-time" id="departuretime">${datas.departureTime}</span> 
                    </div>
                    <div class="wh33percent left center size12" style="width: 33%; font-size: 12px; margin: 0 auto; text-align: center; float: left;">
                     <span class="chkout-dur" id="durationflight">${datas.duration} </span>
                    </div>
                    <div class="wh33percent right textright size12" style="width: 33%; font-size: 12px; text-align: right; float: right;">
                    <span class="chkout-time" id="arrivaltime"> ${datas.arrivalTime}</span>
                    </div>
                    <div class="clearfix" style="float: none; clear: both;"></div>
                  </div>
                  </c:forEach>
                  <!-- END OF GOING TO -->
                  
                  <br>
                  <p><b>Return</b></p>
                  <!-- RETURNING -->
                  <c:forEach items="${deptLGS}" var="datas" >
                  <div>
                    <div class="wh33percent left size12 bold dark" style="width: 67%;  color: #333; font-size: 12px; font-weight: 700; float: left;">
                     <span class="chkout-city" id="returnsource">${datas.origin} </span> 
                    </div>
                     
                    <div class="wh33percent right textright size12 bold dark" style="width: 33%; color: #333; font-size: 12px; font-weight: 700;margin: 0 auto; text-align: right; float: left;">
                      <span class="chkout-place" id="returndestination">${datas.destination} </span>
                    </div>
                    <div class="clearfix" style="float: none; clear: both;"></div>
                    
                    <div class="wh33percent left" style="width: 33%; float: left;">
                      <div class="fcircle" style="width: 39px; height: 39px; border: 2px solid #ebebeb; background: #fff; -webkit-border-radius: 100px; -moz-border-radius: 100px; border-radius: 100px; position: relative; z-index: 100;">
                        <span class="fdeparture" style="width: 25px; height: 21px; background: url(${pageContext.request.contextPath}/resources/images/new_img/departure.png) no-repeat; display: block; margin: 6px 0px 0px 4px;"></span>
                      </div>
                    </div>
                    <div class="wh33percent left" style="width: 33%; float: left; text-align: center; text-align: -webkit-center; margin: 0 auto;">
                      <div class="fcircle center" style="width: 39px; height: 39px; border: 2px solid #ebebeb; background: #fff; -webkit-border-radius: 100px; -moz-border-radius: 100px; border-radius: 100px; position: relative; z-index: 100;">
                        <span class="fstop" style="width: 25px; height: 21px; background: url(${pageContext.request.contextPath}/resources/images/new_img/clock1.png) no-repeat; display: block; margin: 6px 0px 0px 4px;"></span>
                      </div>
                    </div>
                    <div class="wh33percent right" style="width: 13%; float: right;">
                      <div class="fcircle right" style="width: 39px; height: 39px; border: 2px solid #ebebeb; background: #fff; -webkit-border-radius: 100px; -moz-border-radius: 100px; border-radius: 100px; position: relative; z-index: 100;">
                        <span class="farrival" style="width: 25px; height: 21px; background: url(${pageContext.request.contextPath}/resources/images/new_img/arrival.png) no-repeat; display: block; margin: 6px 0px 0px 4px;"></span>
                      </div>
                    </div>
                    <div class="clearfix" style="float: none; clear: both;"></div>
                    <div class="fline2px" style="width: 100%; height: 2px; background: #ebebeb; display: block; position: relative; top: -20px; z-index: 10;"></div>
                    
                    <div class="wh33percent left size12" style="width: 33%; font-size: 12px; float: left;">
                     <span class="chkout-time" id="returndeparturetime"> ${datas.departureTime}</span>
                    </div>
                    <div class="wh33percent left center size12" style="width: 33%; font-size: 12px; margin: 0 auto; text-align: center; float: left;">
                  <span class="chkout-dur" id="returndurationflight">${datas.duration} </span>
                    </div>
                    <div class="wh33percent right textright size12" style="width: 33%; font-size: 12px; text-align: right; float: right;">
                       <span class="chkout-time" id="returnarrivaltime"> ${datas.arrivalTime}</span>
                    </div>
                    <div class="clearfix" style="float: none; clear: both;"></div>
                  </div>
                  </c:forEach>
                  <!-- END OF RETURNING -->
                  
                  <br>
                  
                  <span class="dark" style="color: #333;">Tickets: Roundtrip</span>
                  
                  </div>
                  <div class="line3"></div>
                  <div class="padding30" style="padding: 30px;">         
                    <span class="left size14 dark" style="float: left; font-size: 14px; color: #333;">Trip Total:</span>
                    <span class="right lred2 bold size18" style="float: right; font-size: 18px; font-weight: 800; color: #ff5a00;" id="grandtotal"> </span>
                    <div class="clearfix" style="float: none; clear: both;"></div>
                  </div>
                </div>

              </div>
            </div>

            <div class="panel panel-default">
              <div class="panel-body">
                <div class="row">
                  <div class="cpadding1" style="padding: 0px 30px 10px 50px;">
                    <span class="icon-help" style="width: 24px; height: 24px; background: url(${pageContext.request.contextPath}/resources/images/new_img/icon-phone.png) no-repeat; display: block; position: absolute; float: left; left: 37px;  margin-top: 7px;"></span>
                    <h3 class="opensans">Need Assistance?</h3>
                    <p class="size14 grey" style="color: #999; font-size: 14px;">Our team is 24/7 at your service to help you with your booking issues or answer any related questions</p>
                    <p class="ass_number">8025011300</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        
 	<div class="modal fade" id="flightModal" role="dialog" hidden="hidden">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content" style="margin-top: 40%;">
       
        <div class="modal-body">
          <center>
		  <img src="${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/source-(tick).gif" style="width: 150px; margin-top: -91px;">
		<div style="background: white; margin-top: -20px;
		    padding: 2.5% 2%;">
		<h3><b>Your ticket is successfully booked.<br><small></b></small></h3>
		<center><button type="button" id="payment_ok">OK</button></center>
		</div></center>
        </div>
       
      </div>
      
    </div>
  </div>
		
		<div id="errModel" role="dialog" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<center id="common_error_msg" class="alert alert-danger"></center>
					<%-- <center><label id="common_error_msg" class="alert alert-danger"></label></center> --%>
				</div>
			</div>
		</div>
	</div>
		
		 
		    <form method="POST"
			action="${pageContext.request.contextPath}/User/Travel/Flight/ReturnProcessSplitpayment"
			id="ReturnProcessSplitpayment">
			<input type="hidden" class="form-control numeric" id="returnamount"
				name="returnamount" value="" />  <input type="hidden"
				class="form-control numeric" id="phone1" name="phone" value="" /> <input
				type="hidden" class="form-control numeric" id="email1" name="email" />
			<input type="hidden" class="form-control numeric" id="address"
				name="address" value="dsfsdfsdfgsd" /> <input type="hidden"
				class="form-control numeric" id="firstName1" name="firstName"
				  /> <input type="hidden" class="form-control numeric"
				id="lastName1" name="lastName"  /> <input type="hidden"
				class="form-control numeric" id="firstNamechild1"
				name="firstNamechild"  /> <input type="hidden"
				class="form-control numeric" id="lastNamechild1"
				name="lastNamechild"  /> <input type="hidden"
				class="form-control numeric" id="firstNameinfant1"
				name="firstNameinfant"   /> <input type="hidden"
				class="form-control numeric" id="lastNameinfant1"
				name="lastNameinfant"   />
				
				<input type="hidden"
				class="form-control numeric" id="useradultsgender1"
				name="useradultsgender"   />
				<input type="hidden"
				class="form-control numeric" id="userchildsgender1"
				name="userchildsgender"   />
				<input type="hidden"
				class="form-control numeric" id="userinfantsgender1"
				name="userinfantsgender"   />

		</form>  
		<center>
						<!-- Modal for Add Money -->
						<div class="modal fade" id="loadMoneyModal" role="dialog">
							<div class="modal-dialog">

			<%-- 					<!-- Modal content-->
								<div class="modal-content rchrg_modal">
									<div class="modal-header addm_head">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<center>
											<h2 class="modal-title">
												<b>Add Money</b>
											</h2>
										</center>
									</div> --%>
									<div class="modal-body">
										<div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12">
												 <form method="POST" action="${pageContext.request.contextPath}/User/Travel/Flight/ReturnProcess" id="myform">
												<div class="col-md-12">
													<!-- <form method="post"
																action="/User/LoadMoney/Process" class="form-inline" /> -->
													<div class="form-group">
														<c:out value="${msg}" default="" escapeXml="true" />
														<!-- <div class="col-md-12 col-sm-12 col-xs-12">
															<input type="number" id="amountVal" class="form-control numeric"
																 placeholder="Enter Amount" name="amount" onkeypress="clearvalue('error_amount')"
																min="1" max="9999" size="10" />
																<p class="error" id="error_amount" class="error"></p>
														</div> -->
													</div>
													<div class="form-group">
													<input id ="vnetrd" type="hidden" class="form-control" name="useVnetWeb"
																style="width: 15px; float: left; margin-top: 12px;"> 
														<br>
													</div>
									             <input type="hidden" class="form-control numeric" id="amountgrandtotal"
													name="amount" value="" />
												<input type="hidden"
													class="form-control numeric" id="phone" name="phone" value="" /> <input
													type="hidden" class="form-control numeric" id="email" name="email" />
												<input type="hidden" class="form-control numeric" id="address"
													name="address" value="dsfsdfsdfgsd" /> <input type="hidden"
													class="form-control numeric" id="name" name="name"
													value="${firstName}" /> <input type="hidden"
													class="form-control numeric" id="firstName" name="firstName"
													value="" /> <input type="hidden" class="form-control numeric"
													id="lastName" name="lastName" value="" /> <input type="hidden"
													class="form-control numeric" id="firstNamechild"
													name="firstNamechild" value="" /> <input type="hidden"
													class="form-control numeric" id="lastNamechild"
													name="lastNamechild" value="" /> <input type="hidden"
													class="form-control numeric" id="firstNameinfant"
													name="firstNameinfant" value="" /> <input type="hidden"
													class="form-control numeric" id="lastNameinfant"
													name="lastNameinfant" value="" />
													<input type="hidden"
													class="form-control numeric" id="useradultsgender"
													name="useradultsgender"   />
													<input type="hidden"
													class="form-control numeric" id="userchildsgender"
													name="userchildsgender"   />
													<input type="hidden"
													class="form-control numeric" id="userinfantsgender"
													name="userinfantsgender"/>
												  <input type="hidden" class="form-control numeric" id="infantDateOfBirth" name="infantDateOfBirth"/>
												  
												  <input type="hidden"  id="flightType" name="flightType" value="RoundTrip"/>
													
													<%-- <div class="row" style="margin-top: 40px;">
														<center>
															<div class="form-group">
																<button  onclick="bookflightwithpaymentgateway()"  type="button" class="btn"
																	style="background: #ff0000; color: #FFFFFF;">Continue</button>
															</div>
														</center>
													</div> --%> 
													<!-- </form>  onclick="this.disabled=true"  -->
												</div>
												</form>
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>
						<!-- Modal for Add Money -->
					</center>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		function setDetail() {

			var traveldata = document.getElementById("checkoutDeatil").value;
			var fares = new Array();
			fares = traveldata.split("#");
			 
			document.getElementById("source").innerHTML = fares[0];
  
			document.getElementById("destination").innerHTML = fares[1];
  
			document.getElementById("durationflight").innerHTML = fares[4];
			document.getElementById("arrivaltime").innerHTML = fares[5];
			document.getElementById("departuretime").innerHTML = fares[6];
 
			var traveldatareturn = document.getElementById("returncheckoutDeatil").value;
			
			var faresreturn = new Array();
			faresreturn = traveldatareturn.split("#");
		 
			document.getElementById("returnsource").innerHTML = faresreturn[0];
			 
			document.getElementById("returndestination").innerHTML = faresreturn[1];
			 
			document.getElementById("returndurationflight").innerHTML = faresreturn[4];
			document.getElementById("returnarrivaltime").innerHTML = faresreturn[5];
			document.getElementById("returndeparturetime").innerHTML = faresreturn[6];
			
			var grandtotal=parseInt(fares[11])+parseInt(faresreturn[11])+parseInt(200);
			document.getElementById("grandtotal").innerHTML = ""+grandtotal;
			document.getElementById("amountgrandtotal").value = ""+grandtotal;

			 var adults = document.getElementById("adults").value;
			 var childs = document.getElementById("childs").value;
			 var infants = document.getElementById("infants").value;

		}

		function bookflight()
		{
			$("#loader").show();
			document.getElementById("pesengerdeatilmobile").innerHTML="";
			document.getElementById("pesengerdeatilemail").innerHTML="";
			document.getElementById("responsemessage").innerHTML="";
			var emailAddress=document.getElementById("useremailid").value;
			var mobileNumber=document.getElementById("usermobilenumber").value;
			
			var firstName="";
			var lastName="";
			
			var firstNamechild="";
			var lastNamechild="";
 
			var firstNameinfant="";
			var lastNameinfant="";
			
			var useradultsgender="";
			var userchildsgender="";
		    var userinfantsgender="";
			
			var adults = document.getElementById("adults").value;
			 var childs = document.getElementById("childs").value;
			 var infants = document.getElementById("infants").value;
		 
		    for (i=0;i<adults;i++){
				firstName+="~"+document.getElementById("userfirstnameadults"+i).value;
				 lastName+="@"+document.getElementById("userlastnameadults"+i).value; 
				 useradultsgender+="#"+document.getElementById("useradultsgender"+i).value; 
		    }

		    for (i=0;i<childs;i++){

		    	firstNamechild+="~"+document.getElementById("userfirstnamechilds"+i).value;
		    	lastNamechild+="@"+document.getElementById("userlastnamechilds"+i).value;  
		    	userchildsgender+="#"+document.getElementById("userchildsgender"+i).value; 
		    }

		    for (i=0;i<infants;i++){
		    	firstNameinfant+="~"+document.getElementById("userfirstnameinfants"+i).value;
		    	lastNameinfant+="@"+document.getElementById("userlastnameinfants"+i).value;  
		    	userinfantsgender+="#"+document.getElementById("userinfantsgender"+i).value; 
		    }
			
			 
			var grandtotal=document.getElementById("grandtotal").innerHTML;
					var atpos = emailAddress.indexOf("@");
				    var dotpos = emailAddress.lastIndexOf(".");
				   if(mobileNumber.length <=9)
						{
						 $("#loader").hide();
						document.getElementById("pesengerdeatilmobile").innerHTML="Please Enter 10 Digit Mobile Number";
						}
				     else if(atpos<1 || dotpos<atpos+2 || dotpos+2 >= emailAddress.length) {
				    	 $("#loader").hide(); 
						document.getElementById("pesengerdeatilemail").innerHTML="Please Enter Your Email";
				       
				    }else if(firstName.length<=4 || lastName.length<=4 )
				     {
				    	 $("#loader").hide(); 
						document.getElementById("pesengerdeatilemail").innerHTML="Please Enter Your Name Detail";
				       
				    } else{
			$.ajax({
						type : "POST",
						contentType : "application/json",
						url : "${pageContext.request.contextPath}/User/Travel/Flight/ReturnCheckOut",
						data : JSON.stringify({
							 "emailAddress" : "" + emailAddress+ "",
								"firstName" : "" + firstName+ "",
								"lastName" : "" + lastName+ "",
								"mobileNumber" : "" + mobileNumber+ "",
								"firstNamechild" : "" + firstNamechild+ "",
								"lastNamechild" : "" + lastNamechild+ "",
								"firstNameinfant" : "" + firstNameinfant+ "",
								"lastNameinfant" : "" + lastNameinfant+ "",
								"useradultsgender" : "" + useradultsgender+ "",
								"userchildsgender" : "" + userchildsgender+ "",
								"userinfantsgender" : "" + userinfantsgender+ "",
								"grandtotal" : "" + grandtotal+ "",
						}),
						success : function(response)
						{
							 
							document.getElementById("pesengerdeatilmobile").innerHTML="";
							document.getElementById("pesengerdeatilemail").innerHTML="";
							 var d=response.split("@");
							 if(d[0]==="S00")
							 {
								 
								 $('#flightModal').modal({
								        backdrop: 'static',
								        keyboard: true, 
								        show: true
									});
								 
								 $("#payment_ok").click(function() {
										window.location.href='${pageContext.request.contextPath}/User/Travel/Flight/MyTickets'; 
									});
								 
								 
								/*  $("#loader").hide();
								 document.getElementById("responsemessage").innerHTML=""+d[1];	 */
							 }
							 else if(d[0]==="T01")
							 {
								 document.getElementById("firstName1").value=""+firstName;
								 document.getElementById("lastName1").value=""+lastName;
								 document.getElementById("returnamount").value=""+d[2];
								document.getElementById("firstNamechild1").value=""+firstNamechild;
								 document.getElementById("lastNamechild1").value=""+lastNamechild;
								 document.getElementById("firstNameinfant1").value=""+firstNameinfant;
								 document.getElementById("lastNameinfant1").value=""+lastNameinfant; 
							        document.getElementById("phone1").value=""+mobileNumber;
									 document.getElementById("email1").value=""+emailAddress;
							 $('#ReturnProcessSplitpayment').submit();
							 }
							 else if(d[0]==="F03")
							 {
								 window.location.href='${pageContext.request.contextPath}/Home'; 
							 }
						 else
						 {
							 $("#loader").hide();
							
							 document.getElementById("responsemessage").innerHTML=""+d[1];	
									
						 }
		 

						}
					});
				    }
		}
	
		

		function bookflightwithpaymentgateway()
		{
			$("#loader").show();
			 
			document.getElementById("pesengerdeatilmobile").innerHTML="";
			document.getElementById("pesengerdeatilemail").innerHTML="";
			 document.getElementById("responsemessage").innerHTML="";
 
			var emailAddress=document.getElementById("useremailid").value;
			var mobileNumber=document.getElementById("usermobilenumber").value;
			
			var firstName="";
			var lastName="";
			
			var firstNamechild="";
			var lastNamechild="";
 
			var firstNameinfant="";
			var lastNameinfant="";
			
			var useradultsgender="";
			var userchildsgender="";
		    var userinfantsgender="";

		    var adults = document.getElementById("adults").value;
			 var childs = document.getElementById("childs").value;
			 var infants = document.getElementById("infants").value;
		 
		    for (i=0;i<adults;i++){
				firstName+="~"+document.getElementById("userfirstnameadults"+i).value;
				 lastName+="@"+document.getElementById("userlastnameadults"+i).value; 
				 useradultsgender+="#"+document.getElementById("useradultsgender"+i).value; 
		    }

		    for (i=0;i<childs;i++){

		    	firstNamechild+="~"+document.getElementById("userfirstnamechilds"+i).value;
		    	lastNamechild+="@"+document.getElementById("userlastnamechilds"+i).value;  
		    	userchildsgender+="#"+document.getElementById("userchildsgender"+i).value; 
		    }

		    for (i=0;i<infants;i++){
		    	firstNameinfant+="~"+document.getElementById("userfirstnameinfants"+i).value;
		    	lastNameinfant+="@"+document.getElementById("userlastnameinfants"+i).value;  
		    	userinfantsgender+="#"+document.getElementById("userinfantsgender"+i).value; 
		    }
			
			 
			var grandtotal=document.getElementById("grandtotal").innerHTML;
		
		 
					var atpos = emailAddress.indexOf("@");
				    var dotpos = emailAddress.lastIndexOf(".");
				   if(mobileNumber.length <=9)
						{
						 $("#loader").hide();
						document.getElementById("pesengerdeatilmobile").innerHTML="Please Enter 10 Digit Mobile Number";
						}
				     else if(atpos<1 || dotpos<atpos+2 || dotpos+2 >= emailAddress.length) {
				    	 $("#loader").hide(); 
						document.getElementById("pesengerdeatilemail").innerHTML="Please Enter Your Email";
				       
				    }else if(firstName.length<=4 || lastName.length<=4 )
				     {
				    	 $("#loader").hide(); 
						document.getElementById("pesengerdeatilemail").innerHTML="Please Enter Your Name Detail";
				       
				    } else{
	 
				    	 $("#loader").hide();
							 document.getElementById("firstName").value=""+firstName;
							 document.getElementById("lastName").value=""+lastName;
							 document.getElementById("firstNamechild").value=""+firstNamechild;
							 document.getElementById("lastNamechild").value=""+lastNamechild;
							 document.getElementById("firstNameinfant").value=""+firstNameinfant;
							 document.getElementById("lastNameinfant").value=""+lastNameinfant;
							 document.getElementById("phone").value=""+mobileNumber;
							 document.getElementById("email").value=""+emailAddress;
							 document.getElementById("useradultsgender1").value=""+useradultsgender;
							 document.getElementById("userchildsgender1").value=""+userchildsgender;
							 document.getElementById("userinfantsgender1").value=""+userinfantsgender;
							 
								   $('#myform').submit();
				    }
		}
		
		
		function openpaymentgateselection()
		{
// 			$("#loader").show();

		    var payment_type=$("input[name='optradio']:checked").val();
			
			console.log("ABC:: "+payment_type);
		
			var contextPath = "${pageContext.request.contextPath}";
			var spinnerUrl = "Please wait <img src="+contextPath+"'/resources/images/spinner.gif' style='width:25px;'>";
			$("#gatewayPay").html(spinnerUrl);
			$("#gatewayPay").addClass("disabled");
	
			document.getElementById("pesengerdeatilmobile").innerHTML="";
			document.getElementById("pesengerdeatilemail").innerHTML="";
			 document.getElementById("responsemessage").innerHTML="";
 
			var emailAddress=document.getElementById("useremailid").value;
			var mobileNumber=document.getElementById("usermobilenumber").value;
			var pattern = "[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$";
			
			var firstName="";
			var lastName="";
			
			var firstNamechild="";
			var lastNamechild="";
 
			var firstNameinfant="";
			var lastNameinfant="";
			
			var useradultsgender="";
			var userchildsgender="";
		    var userinfantsgender="";
			
			var adults = document.getElementById("adults").value;
			 var childs = document.getElementById("childs").value;
			 var infants = document.getElementById("infants").value;
			 var valid=true;
			 var userinfantsdob="";
			 
		    for (i=1;i<=adults;i++){
		    	
		    	if (document.getElementById("useradultsgender"+i).value=='#') {
		    		$('#erra_title'+i).text("Please select title");
		    		valid=false;
		    	}
		    	if (document.getElementById("userfirstnameadults"+i).value=='') {
		    		valid=false;
		    		$('#erra_fName'+i).text("Please enter first name");
		    	}
		    	if (document.getElementById("userlastnameadults"+i).value=='') {
		    		valid=false;
		    		$('#erra_lName'+i).text("Please enter last name");
		    	}
		    	
		    	
				firstName+="~"+document.getElementById("userfirstnameadults"+i).value;
				 lastName+="@"+document.getElementById("userlastnameadults"+i).value; 
				 useradultsgender+="#"+document.getElementById("useradultsgender"+i).value; 
		    }

		    for (i=1;i<=childs;i++){

		    	if (document.getElementById("userchildsgender"+i).value=='#') {
		    		$('#errc_title'+i).text("Please select title");
		    		valid=false;
		    	}
		    	if (document.getElementById("userfirstnamechilds"+i).value=='') {
		    		valid=false;
		    		$('#errc_fName'+i).text("Please enter first name");
		    	}
		    	if (document.getElementById("userlastnamechilds"+i).value=='') {
		    		valid=false;
		    		$('#errc_lName'+i).text("Please enter last name");
		    	}
		    	
		    	firstNamechild+="~"+document.getElementById("userfirstnamechilds"+i).value;
		    	lastNamechild+="@"+document.getElementById("userlastnamechilds"+i).value;  
		    	userchildsgender+="#"+document.getElementById("userchildsgender"+i).value; 
		    }

		    for (i=1;i<=infants;i++){
		    	
		    	if (document.getElementById("userinfantsgender"+i).value=='#') {
		    		$('#erri_title'+i).text("Please select title");
		    		valid=false;
		    	}
		    	if (document.getElementById("userfirstnameinfants"+i).value=='') {
		    		valid=false;
		    		$('#erri_fName'+i).text("Please enter first name");
		    	}
		    	if (document.getElementById("userlastnameinfants"+i).value=='') {
		    		valid=false;
		    		$('#erri_lName'+i).text("Please enter last name");
		    	}
		    	if (document.getElementById("infantsdob"+i).value=='') {
		    		valid=false;
		    		$('#erri_dob'+i).text("Please Select Date of birth");
		    	}
		    	
		    	firstNameinfant+="~"+document.getElementById("userfirstnameinfants"+i).value;
		    	lastNameinfant+="@"+document.getElementById("userlastnameinfants"+i).value;  
		    	userinfantsgender+="#"+document.getElementById("userinfantsgender"+i).value; 
		    	userinfantsdob+="~"+document.getElementById("infantsdob"+i).value;
		    }
			 
			var grandtotal=document.getElementById("grandtotal").innerHTML;
		
			if (emailAddress=='' || emailAddress==' ') {
		    	 console.log("email Invalid:: ");
				valid=false;
				document.getElementById("pesengerdeatilemail").innerHTML="Please enter email";
			}
			else if(!(emailAddress.match(pattern))){
				document.getElementById("pesengerdeatilemail").innerHTML="Please enter valid email";
				valid = false;
				console.log("email Invalid:: ");
			}
			
			  if(mobileNumber.length <=9)
				{
				 $("#loader").hide();
				document.getElementById("pesengerdeatilmobile").innerHTML="Please Enter 10 Digit Mobile Number";
				}
			  
					/* var atpos = emailAddress.indexOf("@");
				    var dotpos = emailAddress.lastIndexOf(".");
				   if(mobileNumber.length <=9)
						{
						 $("#loader").hide();
						document.getElementById("pesengerdeatilmobile").innerHTML="Please Enter 10 Digit Mobile Number";
						}
				     else if(atpos<1 || dotpos<atpos+2 || dotpos+2 >= emailAddress.length) {
				    	 $("#loader").hide(); 
						document.getElementById("pesengerdeatilemail").innerHTML="Please Enter Your Email";
				       
				    }else if(firstName.length<=4 || lastName.length<=4 )
				     {
				    	 $("#loader").hide(); 
						document.getElementById("pesengerdeatilemail").innerHTML="Please Enter Your Name Detail";
				       
				    }  */
				    
// 				    else{
				    	document.getElementById("useradultsgender").value=""+useradultsgender;
						 document.getElementById("userchildsgender").value=""+userchildsgender;
						 document.getElementById("userinfantsgender").value=""+userinfantsgender; 
						 
				    /* 	 $("#loader").hide();
				    	 $("#loadMoneyModal").modal(); */
				    	 
				     if (valid==true) {
				    if (payment_type.includes("wallet")) {
		    		 
		    		 $("#paymentbtn").html(spinnerUrl);
		    		 $("#paymentbtn").addClass("disabled");
		    		 
		    		$.ajax({
						type : "POST",
						contentType : "application/json",
						url : "${pageContext.request.contextPath}/User/Travel/Flight/ReturnCheckOut",
						data : JSON.stringify({
							 "emailAddress" : "" + emailAddress+ "",
								"firstName" : "" + firstName+ "",
								"lastName" : "" + lastName+ "",
								
								"firstNamechild" : "" + firstNamechild+ "",
								"lastNamechild" : "" + lastNamechild+ "",
								"firstNameinfant" : "" + firstNameinfant+ "",
								"lastNameinfant" : "" + lastNameinfant+ "",
								"mobileNumber" : "" + mobileNumber+ "",
							    "useradultsgender" : "" + useradultsgender+ "",
							    "userchildsgender" : "" + userchildsgender+ "",
								"userinfantsgender" : "" + userinfantsgender+ "",
								"grandtotal" : "" + grandtotal+ "",
								"infantDateOfBirth":"" +userinfantsdob +"",
								/* "adultDateOfBirth":"" +adultDateOfBirth +"",
								"childDateOfBirth":"" +childDateOfBirth +"",
								"infantDateOfBirth":"" +infantDateOfBirth +"", */
						}),
						success : function(response)
						{
							$("#paymentbtn").html("Continue");
							$("#paymentbtn").removeClass("disabled");
							document.getElementById("pesengerdeatilmobile").innerHTML="";
							document.getElementById("pesengerdeatilemail").innerHTML="";
							 document.getElementById("responsemessage").innerHTML="";
				 
							 var d=response.split("@");
							 if(d[0]==="S00")
							 {
								 $('#flightModal').modal({
								        backdrop: 'static',
								        keyboard: true, 
								        show: true
									});
								 
								 $("#payment_ok").click(function() {
										window.location.href='${pageContext.request.contextPath}/User/Travel/Flight/MyTickets'; 
									});
								 
								 $("#loader").hide();
								 document.getElementById("responsemessage").innerHTML=""+d[1];	
								 
							 }
							 else if(d[0]==="T01")
								 {
								 document.getElementById("firstName1").value=""+firstName;
								 document.getElementById("lastName1").value=""+lastName;
								 document.getElementById("returnamount").value=""+d[2];
								 /* document.getElementById("firstNamechild1").value=""+firstNamechild;
								 document.getElementById("lastNamechild1").value=""+lastNamechild;
								 document.getElementById("firstNameinfant1").value=""+firstNameinfant;
								 document.getElementById("lastNameinfant1").value=""+lastNameinfant; */
								 
						        document.getElementById("phone1").value=""+mobileNumber;
							    document.getElementById("email1").value=""+emailAddress;
								 $('#ReturnProcessSplitpayment').submit();
								 }
							 else if(d[0]==="F03")
							 {
								 window.location.href='${pageContext.request.contextPath}/Home'; 
							 }
						 else
						 {
							 $("#loader").hide();
							 $("#common_error_msg").html(""+d[1]);
							 $("#errModel").modal("show"); 
							 
// 							 document.getElementById("responsemessage").innerHTML=""+d[1];	
									
						 }
		 

						}
					});
		    	 }
		    	 
	
		    	 
		    	  else if ((payment_type.includes("vnet")) || (payment_type.includes("ebs"))) {
		         
		    		 if (payment_type.includes("vnet")) {
		    		  document.getElementById("vnetrd").value="vnet";	
					}
		    		 if (payment_type.includes("ebs")) {
			    		  document.getElementById("vnetrd").value="others";	
						}
		    		 
		    	 document.getElementById("firstName").value=""+firstName;
				 document.getElementById("lastName").value=""+lastName;
				 document.getElementById("firstNamechild").value=""+firstNamechild;
				 document.getElementById("lastNamechild").value=""+lastNamechild;
				 document.getElementById("firstNameinfant").value=""+firstNameinfant;
				 document.getElementById("lastNameinfant").value=""+lastNameinfant;
				 document.getElementById("phone").value=""+mobileNumber;
				 document.getElementById("email").value=""+emailAddress;
				 document.getElementById("useradultsgender1").value=""+useradultsgender;
				 document.getElementById("userchildsgender1").value=""+userchildsgender;
				 document.getElementById("userinfantsgender1").value=""+userinfantsgender;
				 document.getElementById("infantDateOfBirth").value=""+userinfantsdob;
				 
					  $('#myform').submit();

		    	 }
				    }
				    	 
		}
	</script>
	 
	<script src="https://harvesthq.github.io/chosen/chosen.jquery.js"></script>
</body>
</html>