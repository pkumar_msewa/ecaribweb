<div class="tab-pane active" id="tab_flight" style="margin-top: 9px;">
	<div class="row">
		<div class="col-md-12">
			<div class="top-list" style="margin-bottom: 5px;">
				<ul class="nav nav-pills">
					<li class="active"><a href="#" id="one-way"
						data-toggle="pills" title="One Way">One Way</a></li>
					<li><a href="#" id="round" data-toggle="pills"
						title="Round Trip">Round trip</a></li>
				</ul>
			</div>
		</div>
	</div>

	<div class="col-md-4"
		style="padding-left: 0px; padding-right: 0px;">
		<div class="inner_tab_content_Flight">
			<select id="flightdestination"
				class="js-example-basic-multiple" style="width: 350px;"
				name="destinationId">
				<option value="#">Select Destination</option>
			</select> <select id="flightsource"
				class="js-example-basic-multiple" style="width: 350px;"
				name="sourceId">
				<option value="#">Select Origin</option>
			</select> <span class="rederror" id="errormsgsrc"></span> <span
				class="rederror" id="errormsgdest"
				style="padding-left: 71px;"></span>
		</div>
	</div>

	<div class="col-md-4"
		style="padding-left: 0px; padding-right: 0px;">
		<input type="text" class="form-control"
			placeholder="Depart Date" name="date"
			style="width: 50%; float: left; height: 40px;" id="date"
			onchange="$('#errDeptdate').empty();"> <span 
			id="returndatecal"> <i 
			class="fa fa-calendar D-font-icon icon-inside icon-calendar"></i></span>

		<input type="text" class="form-control"
			placeholder="Return Date" name="returndate"
			style="width: 50%; height: 40px; cursor: default;"
			id="returndate" onchange="$('#errormsgdest').empty();">
		<span id=""> <i
			class="fa fa-calendar R-font-icon icon-inside icon-calendar"></i></span>
		<span class="rederror" id="errDeptdate"></span>
	</div>

	<div class="col-md-4"
		style="padding-left: 0px; padding-right: 0px; height: 40px;">
		<div class="pax-details" id="selTrav">
			<span class="txt-ellipses"> <span class="totalCount"
				id="totalCounttrevaler">1</span> Traveller(s)<span
				class="flight_cls" id="flight_cls">, Economy</span>
			</span>
		</div>
		<button class="btn btn-block search-btn"
			style="width: 50%; height: 40px;" 
			id="flght_srch">Search</button>
		<span class="rederror" id="responsemessage"> </span>
	</div>
 </div>