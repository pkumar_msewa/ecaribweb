<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <title>Payment Successful</title>
  <link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'
	type="image/png" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/new_css/default.css">
  </head>
 <body>
<jsp:include page="/WEB-INF/jsp/Agent/AHeader.jsp" />

<div class="container">  
  <!-- Trigger the modal with a button -->
<!--   <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button> -->

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content" style="margin-top: 40%;">
       
        <div class="modal-body">
          <center>
		  <img src="${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/source-(tick).gif" style="width: 150px; margin-top: -91px;">
<div style="background: white; margin-top: -20px;
    padding: 2.5% 2%;">
<h3><b>Your Payment is successful.<br><small></b></small></h3>
</div></center>
        </div>
       
      </div>
      
    </div>
  </div>
  
</div>
</body>
<script type="text/javascript">
$(document).ready(function() {
	$('#myModal').modal({
        backdrop: 'static',
        keyboard: true, 
        show: true
}); 
	
});

</script>
</html>