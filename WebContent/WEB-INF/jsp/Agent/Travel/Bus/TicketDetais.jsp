<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>VPayQwik | Trip Summary</title>

    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
	<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
	<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
	<%@ taglib prefix="sec"
		uri="http://www.springframework.org/security/tags"%>
	<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
    <!--- GOOGLE FONTS - -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600' rel='stylesheet' type='text/css'>
    <!-- /GOOGLE FONTS -->
 <link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/travel_css/bootstrap.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <!--<link rel="stylesheet" href="css/font-awesome.css">-->
   <link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/travel_css/icomoon.css">
  <link rel="stylesheet" href="/resources/css/new_css/newstyles.css">   
 <link rel="icon" href='<c:url value="/resources/images/favicon.png"/>' type="image/png" />
 <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/new_css/default.css">
 <script type="text/javascript" 
    src="<c:url value="/resources/js/Agent/Agentdetail.js"/>"></script>

    <link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
    
   <style>
        a {
            color: #808080;
        }
        a:hover {
            color: #808080;
        }
        
        .ordr {
            font-size: 12px;
        }
        .ordr-success {
            background: green;
            border-radius: 23px;
            color: #fff;
            width: 180px;
            padding: 8px;
            text-align: center;
        }
        .ordr-success-in {
            font-size: 15px;
            font-weight: bold;
        }
        .promo-cd {
            color: green;
            font-weight: bold;
        }
        .ordr-src {
            font-size: 14px;
            font-weight: bold;
        }
        .ordr-dest {
            font-size: 14px;
            font-weight: bold;
        }
        .ordr-ope {
            color: #0abbc7;
        }
        .ordr-busnme {
            font-size: 14px;
            font-weight: bold;
            text-transform: uppercase;
        }
        .ordr-bustyp {
            font-size: 11px;
        }
        .trip-dtls {

        }
        .par-city {
            text-transform: uppercase;
            font-size: 15px;
            font-weight: bold;
            letter-spacing: 1px;
        }
        .par-time {
            text-transform: uppercase;
            font-size: 14px;
            font-weight: bold;
            letter-spacing: 1px;
        }
        .partre {
            color: #0abbc7;
        }
        
   </style>
   
</head>

<body style="background: lightblue;">
<jsp:include page="/WEB-INF/jsp/Agent/AHeader.jsp" />
<div class="topline"></div>
  <!--   <nav class="navbar" style="background:rgba(255,255,255,1.00); min-height: 60px; margin-bottom: 0px;">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="#"><img src="img/vij/aya pay logo.png"/></a>
        </div>    
        <ul class="nav navbar-nav navbar-right">
            <li><a href="">Travel</a></li>
                 <li><a href="">Profile</a></li>
                 <li><a href="">+91 44 45455010/20</a></li>
                 <li><a href="">care@payqwik.in</a></li>
        </ul>
      </div>
    </nav> -->
       <br>
<!-- ====================== TOP AREA ================== -->
        <div class="show-onload">
            <div class="bg-holder full">
                <div class="bg-mask"></div>
                
                <div class="bg-content">
                    <div class="container">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <span class="ord-summ">Order Summary</span>
                                <hr>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <span class="ordr">Your Order No is</span>
                                            <span class="ordr"><b>${extraInfo.transactionRefNo}D</b></span><br>
                                            <span class="ordr">Date</span>&nbsp;<span>:</span>&nbsp;<span class="ordr"><b>${extraInfo.txnDate}</b></span>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="ordr-success pull-right">
                                                <span class="ordr-success-in">Booking Successful</span>
                                            </div>
                                        </div>
                                         <div class="col-md-3">
                                            <div class="ordr-success pull-right">
                                                <span class="ordr-success-in" onclick="openPdf('${extraInfo.emtTxnId}')" style="cursor: pointer;">Download Ticket</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="panel-footer">
                                <span class="ordr">PromoCode Applied Successfully</span><br>
                                <span class="ordr promo-cd">VBUS50</span>
                            </div> -->
                        </div>
                        <br>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <span class="ord-summ">Trip Details</span>
                                        <!-- <span class="ordr-src">Bengaluru</span> → <span class="ordr-dest">Chennai</span>&nbsp; -->
                                        <!-- <span class="ordr">Sun</span>,<span class="ordr">15 Oct 2017</span> -->
                                        </div>
                                        
                                        <div class="col-md-6" style="text-align: right;">
                                            <span class="ordr" style="font-weight: bold;">Date Of Journey: </span>&nbsp;
                                            <span class="ordr"></span><span class="ordr">${extraInfo.journeyDate}</span>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-8">
                                            <div class="col-md-1">
                                                <img src="${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/bus-dtls.png" style="width: 20px;">
                                            </div>
                                            <div class="col-md-11" style="padding-left: 0px;">
                                                <span class="ordr ordr-ope">Bus Operator Name</span><br>
                                                <span class="ordr-busnme"id="bus_opt">${extraInfo.busOperator}</span><br>
<!--                                                 <span class="ordr-bustyp">Multiaxle B9R VOLVO AC Semi Sleeper</span> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="trip-dtls-box">
                                        <div class="trip-dtls">
                                            <div class="col-md-5" style="text-align: center;">
                                                <span class="ordr partre">Departure</span><br>
                                                <img src="${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/loc.png" style="width: 10px;">&nbsp;
                                                <span class="par-city" id="src">${extraInfo.source}</span><br>
<!--                                                 <span class="par-time" id="arrTime">11:25 PM</span> -->
                                            </div>
                                            <div class="col-md-2" style="text-align: center;">
                                                <img src="${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/duration.png" style="width: 50px;">
                                            </div>
                                            <div class="col-md-5" style="text-align: center;">
                                                <span class="ordr partre">Arrival</span><br>
                                                <img src="${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/loc.png" style="width: 10px;">
                                                <span class="par-city" id="dest">${extraInfo.destination}</span><br>
<!--                                                 <span class="par-time">6:30 AM</span> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <img src="${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/loc.png" style="width: 10px;">
                                            <span class="ordr partre">Boarding Point</span><br>
                                            <span class="ordr" id="bdPoint">${extraInfo.boardingAddress}</span>
                                        </div>
                                        
                                         <div class="col-md-6">
<!--                                             <span class="ord-summ">Trip Details</span> -->
                                        <!-- <span class="ordr-src">Bengaluru</span> → <span class="ordr-dest">Chennai</span>&nbsp; -->
                                        <!-- <span class="ordr">Sun</span>,<span class="ordr">15 Oct 2017</span> -->
                                        
<!--                                           <div class="col-md-8" style="border-right: 1px dotted;"> -->
						                  <h5>Traveller Details</h5>
						                  <div class="">
<%-- 						                    <span>Number of Passengers: </span>&nbsp;<span>${noOfPas}</span> --%>
						                  </div>
						                  <br>
						                  <div class="tabble">
						                    <table class="table table-bordered">
						                      <thead>
						                        <tr>
						                        <th>Sl. No</th>
						                          <th>Firstname</th>
						                          <th>Lastname</th>
						                          <th>Gender</th>
						                          <th>Seat No.</th>
						                          <th>Fare</th>
						                        </tr>
						                      </thead>
						                      <tbody>
						                      <c:forEach items="${details}" var="details" varStatus="loopCounter">
						                        <tr >
						                          <td>${loopCounter.count}</td>
						                          <td>${details.fName}</td>
						                          <td>${details.lName}</td>
						                          <td>${details.gender}</td>
						                          <td>${details.seatNo}</td>
						                          <td>${details.fare}</td>
						                        </tr>
						                        </c:forEach>
						                      </tbody>
						                    </table>
						                  </div>
						                </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END TOP AREA  -->

        <!-- <div class="gap"></div> -->
         
        <!-- <div class="gap gap-small"></div> -->
        
        <!-- <script src="js/jquery.js"></script> -->
        	<!-- <script src="js/bootstrap-datepicker.js"></script>
      	<script src="js/bootstrap-timepicker.js"></script> -->
  <!--       <script src="js/bootstrap.js"></script>
        <script src="js/slimmenu.js"></script> -->
     
     <!-- 	<script src="js/nicescroll.js"></script>    
       	<script src="js/typeahead.js"></script>
        <script src="js/custom.js"></script>
        <script src="js/travel.js"></script> -->
  
</body>

<script type="text/javascript">
    function openPdf(emtTxnId) {
    	$("#axz").show();
    	
    	var contextPath = "${pageContext.request.contextPath}";
    
     	 $.ajax({
   	       type : "POST",
   	        contentType : "application/json",
   	        url : "${pageContext.request.contextPath}/Agent/Travel/Bus/singleTicketPdfForBus",
   			dataType : 'json',
   			data :JSON.stringify({
   				"emtTxnId" : emtTxnId,
   			}),
   	       success : function(response) {
   	    	$(".se-pre-con").fadeOut("slow");
   	       console.log("Response :: "+response.code);

   	       if (response.code.includes("F03")) {
			
   	    	window.location.href='${pageContext.request.contextPath}/Home';
		}
   	       
   	       
   	       } 
   	   }); 
      }
    
	</script>
</html>