<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage=""%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<sec:csrfMetaTags/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Receipts</title>
  
<link rel="stylesheet" href="/resources/css/cuscss.css">
  
<!-- Optional theme -->
<link rel="stylesheet" href="<c:url value='/resources/css/bootstrap-theme.min.css'/>" type='text/css'>
<link rel="stylesheet" href="<c:url value='/resources/css/bootstrap.min.css'/>" type='text/css'>
<script src="<c:url value='/resources/js/jquery.js'/>"></script>
<script src="<c:url value='/resources/js/bootstrap.js'/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/Agent/Agentdetail.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/header.js"/>"></script>
<!-- Latest compiled and minified CSS -->
<link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'
	type="image/png" />
<script type="text/javascript"
	src="<c:url value="/resources/js/AgentBillpay.js"/>"></script>
  <link rel="stylesheet" href="/resources/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="/resources/css/Pe-icon-7-stroke.css">  
  <link rel="stylesheet" href="/resources/css/AdminLTE.min.css">
  <link rel="stylesheet" href="/resources/css/_all-skins.min.css">
  
	<link rel="stylesheet" href="<c:url value="/resources/css/datepicker.css"/>">
	<script src="<c:url value="/resources/js/datepicker.js"/>"></script>
	<script>
		$(function() {
			$( "#ins_policy_date" ).datepicker({
				format:"yyyy-mm-dd"
			});
		});
	</script>

<style>
.no-js #loader {
	display: none;
}

.js #loader {
	display: block;
	position: absolute;
	left: 100px;
	top: 0;
}

.se-pre-con {
	position: fixed;
	left: 0px;
	top: 0px;
	width: 100%;
	height: 100%;
	z-index: 9999;
	background: url(/images/pq_large.gif) center no-repeat #fff;
}
@media only screen and (max-width: 1024px) {
	 #table{
		margin-left: 0px; width: 110.5% !important;
	}
}
@media only screen and (max-width: 768px) {
	 #table{
		margin-left: -15px !important; width: 108% !important;
	}
}
</style>
	<script src="<c:url value='/resources/js/modernizr.js'/>"></script>
	<script type="text/javascript">
		$(window).load(function() {
			$(".se-pre-con").fadeOut("slow");
		});
	</script>

</head>
<body class="hold-transition skin-blue sidebar-mini">

	
<jsp:include page="/WEB-INF/jsp/Agent/Login/Header.jsp" />
	
<div class="wrapper">
<jsp:include page="/WEB-INF/jsp/Agent/Login/sidenav.jsp" />

<div class="content-wrapper" style="height: 1000px;">
    <!-- Content Header (Page header) -->
     
     <div class="col-md-11"><br>
			<div class="row" id="table" style="background: white; margin-left: 0px; width: 109%;">
				<table class="table table-hover">
					<thead>
						<tr>
							<th><center>DATE</center></th>
							<th><center>DESCRIPTION</center></th>
							<th><center>AMOUNT</center></th>
							<th><center>STATUS</center></th>
						</tr> 
					</thead>
					<tbody style="color: gray"> 
						<c:forEach items="${transactions}" var="transaction">
							<tr>
								<td><center><c:out value="${transaction.date}" escapeXml="true"/></center></td>
								<td><center><c:out value="${transaction.description}" escapeXml="true"/>
										<b>Transaction ID:</b> <c:out value="${transaction.transactionRefNo}" escapeXml="true"/>
									</center></td>
								<td><center>
									<c:if test="${transaction.debit eq true}"><i class="fa fa-minus" style="color:red"></i></c:if><c:if test="${transaction.debit eq false}"><i class="fa fa-plus" style="color:limegreen"></i></c:if>
									<c:out value="${transaction.amount}" escapeXml="true"/>
								</center></td>
								<td><center>
								<c:if test='${transaction.status eq "Initiated"}'>
								<span class="label label-warning"><c:out value="${transaction.status}" escapeXml="true"/></span>
								</c:if>
								<c:if test='${transaction.status eq "Success"}'>
								<span class="label label-success"><c:out value="${transaction.status}" escapeXml="true"/></span>
								</c:if>
								<c:if test='${transaction.status eq "Processing" }'>
								<span class="label label-primary"><c:out value="${transaction.status}" escapeXml="true"/></span>
								</c:if>
								<c:if test='${transaction.status eq "Failed"}'>
								<span class="label label-danger"><c:out value="${transaction.status}" escapeXml="true"/></span>
								</c:if>
								<c:if test='${transaction.status eq "Reversed"}'>
                                <span class="label label-default"><c:out value="${transaction.status}" escapeXml="true"/></span>
								</c:if>
								</center></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
                </div>
                <jsp:include page="/WEB-INF/jsp/Agent/Login/Footer.jsp"/>
			</div>
			<!---col-md-12-->
		</div>
		<!--row-->
	</div>
	<!-------END BOX----------->

<script src="/resources/js/jquery-2.2.3.min.js"></script>
<script src="/resources/js/bootstrap.min.js"></script>
<script src="/resources/js/app.min.js"></script>
<script src="/resources/js/Chart.min.js"></script>
<script src="/resources/js/dashboard2.js"></script>
</body>
</html>



