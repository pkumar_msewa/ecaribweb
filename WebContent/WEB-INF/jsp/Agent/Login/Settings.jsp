<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage=""%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<sec:csrfMetaTags/>
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>VPayQwik | Settings</title>
  
<link rel="stylesheet" href="/resources/css/cuscss.css">
  
<!-- Optional theme -->
<link rel="stylesheet" href="<c:url value='/resources/css/bootstrap-theme.min.css'/>" type='text/css'>
<link rel="stylesheet" href="<c:url value='/resources/css/bootstrap.min.css'/>" type='text/css'>
<script src="<c:url value='/resources/js/jquery.js'/>"></script>
<script src="<c:url value='/resources/js/bootstrap.js'/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/Agentdetail.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/header.js"/>"></script>
<!-- Latest compiled and minified CSS -->
<link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'
	type="image/png" />
<script type="text/javascript"
	src="<c:url value="/resources/js/AgentBillpay.js"/>"></script>
  <link rel="stylesheet" href="/resources/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="/resources/css/Pe-icon-7-stroke.css">  
  <link rel="stylesheet" href="/resources/css/AdminLTE.min.css">
  <link rel="stylesheet" href="/resources/css/_all-skins.min.css">
  
	<link rel="stylesheet" href="<c:url value="/resources/css/datepicker.css"/>">
	<script type="text/javascript" src="/resources/js/new_js/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>
<script src="/resources/js/new_js/script.js"></script>
<script type="text/javascript" src="<c:url value="/resources/js/Agent/Agentdetail.js"/>"></script>
	<script src="<c:url value="/resources/js/datepicker.js"/>"></script>
	<script>
		$(function() {
			$( "#ins_policy_date" ).datepicker({
				format:"yyyy-mm-dd"
			});
		});
	</script>

<style>
.no-js #loader {
	display: none;
}

.js #loader {
	display: block;
	position: absolute;
	left: 100px;
	top: 0;
}

.se-pre-con {
	position: fixed;
	left: 0px;
	top: 0px;
	width: 100%;
	height: 100%;
	z-index: 9999;
	background: url(/images/pq_large.gif) center no-repeat #fff;
}
</style>
	<script src="<c:url value='/resources/js/modernizr.js'/>"></script>
	<script type="text/javascript">
		$(window).load(function() {
			$(".se-pre-con").fadeOut("slow");
		});
	</script>

</head>
<body class="hold-transition skin-blue sidebar-mini">
	
<jsp:include page="/WEB-INF/jsp/Agent/Login/Header.jsp" />
	
<div class="wrapper">
<jsp:include page="/WEB-INF/jsp/Agent/Login/sidenav.jsp" />

<div class="content-wrapper" style="height: 1000px;">
    <!-- Content Header (Page header) -->
     
     <div class="col-md-11"><br>
     <div class="row">
			<div class="col-md-6">
				<div class="tab-content" style="background: white;"><br>
                	<h3 style="margin-top: auto; margin-left: 4%;"><span style="border-bottom: solid #17bcc8;">Edit Profile</span></h3><br>
                          <%--   <center><code>${msg}</code></center> --%>
							<c:out value="${message}" default="" escapeXml="true" />
							<%-- <form action="<c:url value="/Agent/AgentEditProfile/Process"/>" method="post" class="form form-inline"> --%>
							<form method="post" action="#">
							<div class="group_1">
                           		 <i class="pe-7s-user" id="inputico"></i>                           
								<input name="firstName" type="text" class="agent_first_name" placeholder="Enter Name"> <span class="highlight"></span><span class="bar"></span>
								<p class="error" id="error_first_name"></p>
							</div>

						<!-- 	<div class="group_1">
                            <i class="pe-7s-user" id="inputico"></i>
								<input name="lastName" type="text" id="last_name_ep" placeholder="Last Name"> <span class="highlight"></span> <span
									class="bar"></span>
									<p id="error_last_name" class="error"></p>
							</div> -->
                            
                            <div class="group_1">
                           		 <i class="pe-7s-map-marker" id="inputico"></i>   
                           		 <sec:csrfInput/>                        
								<input type="text" name="address"  id="address"  placeholder="Address"> <span class="highlight"></span><span class="bar"></span>
								<p class="error" id="error_address"></p>
							</div>

							<div class="group_1">
                            <i class="pe-7s-global email_logo" id="inputico"></i>
								<input type="email" name="email"  id="agent_email_ep" placeholder="Email"> <span class="highlight"></span> <span
									class="bar"></span>
									<p id="error_email" class="error"></p>
							</div>
                            <sec:csrfInput/>
							<center><button type="button" id="editprofile_validate" class="btn"
								style="width: 80%; background: #ff0000; margin-top: 10px; color: #FFFFFF;">Update</button></center>
						</form><br><br>
					</div>					
				</div>
                
                <div class="col-md-6">
				<div class="tab-content" style="background: white;"><br>
                	<h3 style="margin-top: auto; margin-left: 4%;"><span style="border-bottom: solid #17bcc8;">Change Password</span></h3><br>
						<form method="post" action="#">
							<div class="group_1">
                           		 <i class="pe-7s-lock" id="inputico"></i>                           
								<input type="password" id="agent_current_password" name="oldPassword"  minlength="6" maxlength="6" placeholder="Current Password"> <span class="highlight"></span><span class="bar"></span>
								<p id="agent_error_current_password" class="error"></p>
							</div>

							<div class="group_1">
                            <i class="pe-7s-lock" id="inputico"></i>
								<input type="password" id="agent_new_password" name="newpassword" minlength="6" maxlength="6" placeholder="New Password"> <span class="highlight"></span> <span
									class="bar"></span>
									<p id="agent_error_new" class="error"></p>
							</div>
                            
                            <div class="group_1">
                           		 <i class="pe-7s-lock" id="inputico"></i>                           
								<input type="password" id="agent_confirm_new_password" name="confirmPassword" minlength="6" maxlength="6" placeholder="Confirm New Password"> <span class="highlight"></span><span class="bar"></span>
								<p id="agent_error_confirm" class="error"></p>
							</div>
							<sec:csrfInput/>
							<center><button type="button" id="agent_update_password" class="btn"
							style="width: 80%; background: #ff0000; margin-top: 10px; color: #FFFFFF;">Update</button></center>
						</form><br><br><br><br><br>
					</div>					
				</div>
				
				<div id="successNotification" role="dialog" class="modal fade">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
								</div>
								<div class="modal-body">
									<center id="success_alert" class="alert alert-success"></center>
								</div>
							</div>
						</div>
					</div>
					
						<div id="errorNotification" role="dialog" class="modal fade">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
								</div>
								<div class="modal-body">
									<center id="error_alert" class="alert alert-danger"></center>
								</div>
							</div>
						</div>
					</div>
           </div>     
                <jsp:include page="/WEB-INF/jsp/Agent/Login/Footer.jsp"/>
	</div>
    </div>
    </div>
	<script src="http://code.jquery.com/jquery-2.2.1.min.js"></script>
	<script type="text/javascript"
		src='<c:url value="/resources/js/wow.js"/>' />
	<script>
		new WOW().init();
	</script>
<script
	src="<c:url value='/resources/js/bootstrap.js'/>"></script>
<script src="/resources/js/jquery-2.2.3.min.js"></script>
<script src="/resources/js/bootstrap.min.js"></script>
<script src="/resources/js/app.min.js"></script>
<script src="/resources/js/Chart.min.js"></script>
<script src="/resources/js/dashboard2.js"></script>
</body>
</html>
