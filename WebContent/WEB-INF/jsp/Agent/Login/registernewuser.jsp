<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage=""%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<sec:csrfMetaTags/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Add New User</title>
  
<link rel="stylesheet" href="/resources/css/cuscss.css">
  <link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'
	type="image/png" />
<!-- Optional theme -->
<link rel="stylesheet" href="<c:url value='/resources/css/bootstrap-theme.min.css'/>" type='text/css'>
<link rel="stylesheet" href="<c:url value='/resources/css/bootstrap.min.css'/>" type='text/css'>
<script src="<c:url value='/resources/js/jquery.js'/>"></script>
<script src="<c:url value='/resources/js/bootstrap.js'/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/Agent/Agentdetail.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/header.js"/>"></script>
<!-- Latest compiled and minified CSS -->

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/AgentToUserSignup.js"></script>

  <link rel="stylesheet" href="/resources/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="/resources/css/Pe-icon-7-stroke.css">  
  <link rel="stylesheet" href="/resources/css/AdminLTE.min.css">
  <link rel="stylesheet" href="/resources/css/_all-skins.min.css">
  <script src="<c:url value='/resources/js/jquery.js'/>"></script>
	<link rel="stylesheet" href="<c:url value="/resources/css/datepicker.css"/>">
	<script src="<c:url value="/resources/js/datepicker.js"/>"></script>

<script>
	$(function() {
		$("#dob").datepicker({
			format : "yyyy-mm-dd"
		});
	});
</script>

<style>
.no-js #loader {
	display: none;
}

.js #loader {
	display: block;
	position: absolute;
	left: 100px;
	top: 0;
}

.se-pre-con {
	position: fixed;
	left: 0px;
	top: 0px;
	width: 100%;
	height: 100%;
	z-index: 9999;
	background: url(/images/pq_large.gif) center no-repeat #fff;
}

</style>
	<script src="<c:url value='/resources/js/modernizr.js'/>"></script>

<script type="text/javascript">
	$(window).load(function() {
		$(".se-pre-con").fadeOut("slow");
		getQuestions();
	});
</script>
<script type="text/javascript">
/* 
$(document).ready(function(){
	
}); */

</script>
<script>
	$(document).ready();
	function getQuestions() {

		$.ajax({
			url : "/Api/v1/User/Website/en/WebRegistration/Questions",
			type : "GET",
			datatype : 'json',
			contentType : "application/json",
			success : function(response) {
				var a = response + " ";
				var y = new Array();
				y = a.split(",");
				document.getElementById("secquestions").innerHTML = "";
				var option = document.createElement("option");
				var select = document.getElementById("secquestions");
				var j;
				option.text = "Select your Security Question";
				option.value = "#";
				select.add(option);
				for (j = 0; j < y.length; j++) {
					var z = Array();
					var option = document.createElement("option");
					z = y[j].split("#");
					option.text = z[1];
					option.value = z[0];
					select.add(option);
				}
			}
		});
	}
</script>
</head>
<body class="hold-transition skin-blue sidebar-mini">

	
<jsp:include page="/WEB-INF/jsp/Agent/Login/Header.jsp" />
	
<div class="wrapper">
<jsp:include page="/WEB-INF/jsp/Agent/Login/sidenav.jsp" />

<div class="content-wrapper">
<div class="container">
    <!-- Content Header (Page header) -->
  <style>
  	input{
		width: 95%;
		}
  </style>   
     <div class="col-md-11"><br>
     <div class="row">
     	<div class="col-md-5" style="background: white; height:90%; padding: 40px;">
     	
     	 <span id="error_registerAgent" class="label label-danger"></span>
                           
				<form action="#" method="post">
					<div class="group">
						<input type="text" class="numeric" name="contactNo"
							id="contact_no" minlength="10" maxlength="10" placeholder="Mobile Number"> <span
							class="highlight"></span>
						<p class="error" id="error_contact_no" class="error"></p>						
					</div><br>
					<div class="group" style="margin-bottom: 18px;">
						<input type=password name="password" id="password" minlength="6"
							maxlength="6" placeholder="Password"> <i class="fa fa-eye-slash"
							id="pwd_eye"
							style="float: right; margin-top: -25px; margin-right: 26px;"></i>
						<p class="error" id="error_password" class="error"></p><br>
						<p id="strength_password"></p>
						<span class="highlight"></span> 
					</div>
					<div class="group">
						<input type="text" name="firstName" id="firstName" placeholder="First Name">
						<p class="error" id="error_first_name" class="error"></p>
						<span class="highlight"></span> 
					</div><br>
					<div class="group">
						<input type="text" name="lastName" id="lastName" placeholder="Last Name">
						<p class="error" id="error_last_name" class="error"></p>
						<span class="highlight"></span> 
					</div><br>
					<div class="group">
						<input type="email" name="email" id="email" placeholder="Email Address"> <span
							class="highlight"></span>
						<p class="error" id="error_email" class="error"></p>
					</div><br>
					<div class="group">
						<input type="text" name="locationCode" id="pincode" minlength="6" class="numeric"
							maxlength="6" placeholder="Pincode"> <span class="highlight"></span>
						<p class="error" id="error_pincode" class="error"></p>						
					</div><br>
					<div class="group">
						<input id="dob" name="dob" type="text" required="required" placeholder="Date Of Birth" />
						<p class="error" id="error_dob" class="error"></p>						
					</div><br>
					<div class="group">
						<select id="secquestions" class="form-control"
							style="width: 95%; border: transparent; border-bottom: gray; border-style: solid; border-width: 1.8px; font-family: 'Ubuntu', sans-serif; padding-left: 0; height: 55px; margin-top: -10px; font-weight: bold; color: #928F8F; box-shadow: none; border-radius: 0;">
							<option value="#">Select your Security Question</option>
						</select>
						<p class="error" id="error_sec_ques" class="error"></p><br>
					</div>
					
					<div class="group">
						<input type="text" name="sec_ans" id="sec_ans" placeholder="Security Answer">
						<p class="error" id="error_sec_ans" class="error"></p>
						<span class="highlight"></span><br>
					</div><br>
					
					<div style="z-index: 99; position: absolute; margin-left: 60%;">
						<a class="captcha_link"><span
							class="glyphicon glyphicon-refresh" aria-hidden="true"
							style="margin-left: 400%;"></span></a>
					</div>
					<div class="group" style="margin-top: 0px;">
						<img src="<c:url value="/Captcha"  />"
							class="img-responsive captcha_image"
							style="margin-top: -15px; position: absolute; width: 130px;" />
					</div>
					<div class="group" style="margin-top: 20px;">
						<input id="g-recaptcha-response" type="text" placeholder="Enter text shown in image"/>
						<p class="error" id="error_captcha" class="error"></p><br>
					</div>
					<input type="checkbox" class="check" value="termsConditions"
						id="termsConditions"
						style="width: 10%; text-align: left; margin-top:15px; float: left;">
					<p style="font-size: 12px; margin-top: 12px; margin-bottom: -22px;">
						By clicking submit I hereby agree all the <a
							href="<c:url  value='/Terms&Conditions'/>">terms and
							conditions</a>.
					</p>

					<br><br>
					<button type="button" class="btn disabled" id="registerButton"
						style="margin-top: 12px; margin-bottom: 8%; margin-left: 8%; background: #ec2029; color: #FFFFFF; width: 80%;">Sign
						Up</button>
				</form>
			</div>
			
			<div class="col-md-6 hidden-sm" style="margin-left: 10px;">

                        <div class="slider" id="slider" style="margin-right: -15px; margin-left: -15px;">	
                            <div class="carousel slide hidden-xs" data-ride="carousel" id="mycarousel">
                                <ol class="carousel-indicators">
                                    <li class="active" data-slide-to="0" data-target="#mycarousel"></li>
                                    <li data-slide-to="1" data-target="#mycarousel" class=""></li>
                                </ol>

                                <div class="carousel-inner">        
                                    <div class="item active" id="slide1">        
                                        <img src="<c:url value="/resources/images/adlapbs_gcm.jpg"/>">
                                    </div>
                                    <!---end item---->
        
                                    <div class="item">
                                        <img src="<c:url value="/resources/images/slider_61.jpg"/>">
                                    </div>
        
                                    <!---end item---->
                                </div>
                                <!--end carousel inner------>
                            </div>
                        </div>
					<!---end caeousel slider---->
                    </div> <br>
           </div>
           <jsp:include page="/WEB-INF/jsp/Agent/Login/Footer.jsp"/>         
	</div></div>
</div>
	<div id="order_confirmation" class="modal fade" role="dialog">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h5 class="modal-title">Order Confirmation</h5>
				</div>
				<div class="modal-body">
					<table class="table table-condensed">
						<tr>
							<td><b>Service </b> </td> <td id="o_service"> </td>
						</tr>
						<tr>
							<td><b id="o_account_name">Account Number </b> </td> <td id="o_account_number"> </td>
						</tr>
						<tr>
							<td><b>Bill Amount</b> </td><td id="o_bill_amount"></td>
						</tr>
						<tr>
							<td><b>Service Charge</b> </td><td id="o_service_amount"></td>
						</tr>
						<tr>
							<td><b>Net Amount</b></td> <td id="o_net_amount"></td>
						</tr>
					</table>
					<button type="button" class="btn btn-block btn-info" id="confirm_order">Confirm</button>
				</div>
			</div>
		</div>
	</div>


	<!-------end container------------>
		<!-----modal after Registration Successful -->
		<div id="regMessage" class="modal fade" role="dialog">
			<div class="modal-dialog modal-sm">
				<div class="modal-content text-center">
					<button type="button" data-dismiss="modal" class="close">&times;</button>
					<div class="icon">
						<span class="fa fa-unlock-alt fa-2x" aria-hidden="true"></span>
					</div>
					<h4>OTP Verification</h4>
					<hr style="margin-top: 20px;">

					<div class="group_1">
						<input type="text" name="key" id="verify_reg_otp_key"
							required="required"> <span class="highlight"></span> <label>OTP</label>
					</div>
					<div class="group_1">
						<input type="hidden" name="mobileNumber"
							class="form-control input-sm" id="reg_otp_username"
							required="required" />
					</div>
					<div class="col-md-6"
						style="float: none; margin-left: auto; margin-right: auto;">
						<button class="btn btn-md btn-block btncu"
							style="margin-bottom: 5px" id="register_verify_mobile">Verify
							Mobile</button>
					</div>
					<br>
					<div class="group_1">
						<div style="z-index: 99; position: absolute; margin-left: 78%;">
							<a href="#" class="captcha_link"><span
								class="glyphicon glyphicon-refresh" aria-hidden="true"
								style="margin-left: 80%;"></span></a>
						</div>
						<br> <img src="<c:url value="/Captcha"/>"
							class="captcha_image" height="50" />
					</div>
					<div class="group_1">
						<input id="g-recaptcha-response-1" type="text" required
							style="margin-top: -25px;" />
						<p class="error" id="error_captcha1" class="error"></p>
						<!-- <label>Enter text shown in image</label> -->
					</div>
					<div class="group_1">
						<button class="btn btn-md btncu" id="register_resend_otp"
							style="margin-bottom: 5px; margin-left: -20px;">Resend
							OTP</button>
					</div>

					<div class="modal-footer">
						<div class="alert alert-success" id="regMessage_success"></div>
					</div>
				</div>
			</div>
		</div>
		<!-- Modal after successful verification -->
		<div id="verifiedMessage" role="dialog" class="modal fade">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h5>Verification Successful</h5>
					</div>
					<div class="modal-body">
						<center id="success_verification_message"
							class="alert alert-success"></center>
					</div>
				</div>
			</div>
		</div>

		<!-- Modal after error verification -->
		<div id="errorMessage" role="dialog" class="modal fade">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
					<div class="modal-body">
						<center id="error_message" class="alert alert-danger"></center>
					</div>
				</div>
			</div>
		</div>
<script src="/resources/js/bootstrap.min.js"></script>
<script src="/resources/js/app.min.js"></script>
<script src="/resources/js/Chart.min.js"></script>
<script src="/resources/js/dashboard2.js"></script>
</body>
</html>