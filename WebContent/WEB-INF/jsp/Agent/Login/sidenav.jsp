<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
  </a>
  <!-- Left side column. contains the logo and sidebar -->
  
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
     
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="/resources/images/sample.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
        <a href="#">Welcome</a><br>
          <p id="first_name_user"></p>
        </div>
      </div>
      <!-- sidebar menu -->
      <ul class="sidebar-menu">
      
       <li class="treeview">
          <a href="/Agent/RegisterUser">
            <i class="fa fa-suitcase menufa2"></i>
            <p class="menutx">Add User</p>
          </a>
        </li> 
      <li class="treeview">
          <a href="">
            <i class="fa fa-money menufa2"></i> <p class="menutx">Send Money</p>
            <span class="pull-right-container">
              <i id="hide" class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<c:url value='/Agent/SendMoney'/>"><i class="fa fa-mobile"></i> <span class="hide1">Mobile</span></a></li>
           <li><a href="<c:url value="/Agent/SendMoney/Bank"/>"><i class="fa fa-credit-card"></i> Bank Transfer</a></li>
          </ul>
      </li> 
    <!--     <li class="treeview">
          <a href="/Agent/RequestMoneyToAdmin">
            <i class="fa fa-suitcase menufa2"></i>
           <p class="menutx">Request Money</p>           
          </a>          
        </li> -->
        <li class="treeview">
          <a href="#">
            <i class="fa fa-mobile fa-2x menufa1"></i> <p class="menutx">Mobile Topup</p>
            <span class="pull-right-container">
              <i id="hide" class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="/Agent/Prepaid"><i class="fa fa-mobile"></i><span class="hide1"> Prepaid</span></a></li>
            <li><a href="/Agent/Postpaid"><i class="fa fa-mobile"></i> <span class="hide1">Postpaid</span></a></li>
            <li><a href="/Agent/DataCard"><i class="fa fa-rocket"></i> <span class="hide1">DataCard</span></a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-lightbulb-o fa-2x menufa1"></i>
            <p class="menutx">Bill Payment</p>
            <span class="pull-right-container">
              <i id="hide" class="fa fa-angle-left pull-right"></i>
            </span>            
          </a>
          <ul class="treeview-menu">
            <li><a href="/Agent/DTH"><i class="fa fa-television" aria-hidden="true"></i> <span class="hide1">DTH</span></a></li>
            <!-- <li><a href="/Agent/landline"><i class="fa fa-phone" aria-hidden="true"></i> <span class="hide1">Landline</span></a></li> -->
            <li><a href="/Agent/Electricity"><i class="fa fa-lightbulb-o" aria-hidden="true"></i> <span class="hide1">Electricity</span></a></li>
            <li><a href="/Agent/Gas"><i class="fa fa-fire" aria-hidden="true"></i> <span class="hide1">Gas</span></a></li>
            <li><a href="/Agent/Insurance"><i class="fa fa-medkit" aria-hidden="true"></i> <span class="hide1">Insurance</span></a></li>
          </ul>
        </li>
        
        <li class="treeview">
          <a href="/Agent/Travel/Flight/Source">
            <i class="fa fa-suitcase menufa2"></i>
            <p class="menutx">Travel Tickets</p>
          </a>
       
        </li>
          
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

<%-- <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
  </a>
  <!-- Left side column. contains the logo and sidebar -->
  
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
     
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="/resources/images/sample.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
        <a href="#">Welcome</a>
          <p>Rohit Manhas</p>          
        </div>
      </div>
      <!-- sidebar menu -->
      <ul class="sidebar-menu">
      <li class=" active treeview">
          <a href="">
            <i class="fa fa-money menufa2"></i> <p class="menutx">Send Money</p>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<c:url value='/Agent/SendMoney'/>"><i class="fa fa-mobile"></i> Mobile</a></li>
          <!--  <li><a href=""><i class="fa fa-credit-card"></i> Bank Account</a></li>-->
          </ul>
        </li>
    <!--     <li class="treeview">
          <a href="/Agent/RequestMoneyToAdmin">
            <i class="fa fa-suitcase menufa2"></i>
           <p class="menutx">Request Money</p>           
          </a>          
        </li> -->
        <li class="treeview">
          <a href="#">
            <i class="fa fa-mobile fa-2x menufa1"></i> <p class="menutx">Mobile Topup</p>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="/Agent/Prepaid"><i class="fa fa-mobile"></i> Prepaid</a></li>
            <li><a href="/Agent/Postpaid"><i class="fa fa-mobile"></i> Postpaid</a></li>
            <li><a href="/Agent/DataCard"><i class="fa fa-rocket"></i> DataCard</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-lightbulb-o fa-2x menufa1"></i>
            <p class="menutx">Bill Payment</p>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>            
          </a>
          <ul class="treeview-menu">
            <li><a href="/Agent/DTH"><i class="fa fa-television" aria-hidden="true"></i> DTH</a></li>
            <li><a href="/Agent/landline"><i class="fa fa-phone" aria-hidden="true"></i> Landline</a></li>
            <li><a href="/Agent/Electricity"><i class="fa fa-lightbulb-o" aria-hidden="true"></i> Electricity</a></li>
            <li><a href="/Agent/Gas"><i class="fa fa-fire" aria-hidden="true"></i> Gas</a></li>
            <li><a href="/Agent/Insurance"><i class="fa fa-medkit" aria-hidden="true"></i> Insurance</a></li>
          </ul>
        </li>
        
        <li class="treeview">
          <a href="http://travel.vpayqwik.com/" target="_blank">
            <i class="fa fa-suitcase menufa2"></i>
            <p class="menutx">Travel Tickets</p>
          </a>
        </li>
        
         <li class="treeview">
          <a href="/Agent/RegisterUser">
            <i class="fa fa-suitcase menufa2"></i>
            <p class="menutx">Add User</p>
          </a>
        </li>   
          
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside> --%>