<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage=""%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<sec:csrfMetaTags/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Electricity Payment</title>
  
<link rel="stylesheet" href="/resources/css/cuscss.css">
  <link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'
	type="image/png" />
<!-- Optional theme -->
<link rel="stylesheet" href="<c:url value='/resources/css/bootstrap-theme.min.css'/>" type='text/css'>
<link rel="stylesheet" href="<c:url value='/resources/css/bootstrap.min.css'/>" type='text/css'>
<script src="<c:url value='/resources/js/jquery.js'/>"></script>
<script src="<c:url value='/resources/js/bootstrap.js'/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/Agent/Agentdetail.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/header.js"/>"></script>
<!-- Latest compiled and minified CSS -->

<%-- <script type="text/javascript"
	src="<c:url value="/resources/js/AgentBillpay.js"/>"></script> --%>
  <link rel="stylesheet" href="/resources/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="/resources/css/Pe-icon-7-stroke.css">  
  <link rel="stylesheet" href="/resources/css/AdminLTE.min.css">
  <link rel="stylesheet" href="/resources/css/_all-skins.min.css">
  
	<link rel="stylesheet" href="<c:url value="/resources/css/datepicker.css"/>">
	<script src="<c:url value="/resources/js/datepicker.js"/>"></script>
	<script>
		$(function() {
			$( "#ins_policy_date" ).datepicker({
				format:"yyyy-mm-dd"
			});
		});
	</script>

<style>
.no-js #loader {
	display: none;
}

.js #loader {
	display: block;
	position: absolute;
	left: 100px;
	top: 0;
}

.se-pre-con {
	position: fixed;
	left: 0px;
	top: 0px;
	width: 100%;
	height: 100%;
	z-index: 9999;
	background: url(/images/pq_large.gif) center no-repeat #fff;
}

</style>
	<script src="<c:url value='/resources/js/modernizr.js'/>"></script>
	<script type="text/javascript">
		$(window).load(function() {
			$(".se-pre-con").fadeOut("slow");
		});
	</script>

</head>
<body class="hold-transition skin-blue sidebar-mini">

	
<jsp:include page="/WEB-INF/jsp/Agent/Login/Header.jsp" />
	
<div class="wrapper">
<jsp:include page="/WEB-INF/jsp/Agent/Login/sidenav.jsp" />

<div class="content-wrapper" style="height: 1000px;">
    <!-- Content Header (Page header) -->
     
     <div class="col-md-11"><br>
     <div class="row">
     	<div class="col-md-6" id="left">
				<div class="tab-content" style="background: white;"><br>
						<p  id="error_ecity_bill" class="title_error"></p>
						<p  id="sucess_ecity_bill" class="title_error"></p><br>
						<form method="post"
							action="#">
							<div class="group_1">
                            <i class="pe-7s-filter" id="inputico"></i>  
							<select name="serviceProvider" id="ecity_provider"
									class="form-control" style="border-radius: 0; width: 86%; border: transparent;border-bottom: gray;border-style: solid;border-width: 1.8px; font-family: 'Ubuntu', sans-serif; padding-left: 0; height: 55px; margin-top: -19px; font-weight: bold; color: #928F8F; box-shadow: none;">
									<option value="#">Select Electricity Provider</option>
									<option value="VTTE">TSECL - TRIPURA</option>
									<option value="VTPE">Torrent Power</option>
									<option value="VNDE">Tata Power - DELHI</option>
									<option value="VSTE">Southern Power - TELANGANA</option>
									<option value="VSAE">Southern Power - ANDHRA PRADESH </option>
									<option value="VREE">Reliance Energy - MUMBAI</option>
									<option value="VMPE">Paschim Kshetra Vitaran - MADHYA PRADESH</option>
									<option value="VDOE">Odisha Discoms - ODISHA</option>
									<option value="VNUE">Noida Power - NOIDA</option>
									<option value="VMDE">MSEDC - MAHARASHTRA</option>
									<option value="VMME">Madhya Kshetra Vitaran - MADHYA PRADESH</option>
									<option value="VDRE">Jodhpur Vidyut Vitran Nigam - RAJASTHAN</option>
									<option value="VJUE">Jamshedpur Utilities & Services (JUSCO)</option>
									<option value="VJRE">Jaipur Vidyut Vitran Nigam - RAJASTHAN</option>
									<option value="VIPE">India Power</option>
									<option value="VDNE">DNHPDCL - DADRA & NAGAR HAVELI</option>
									<option value="VDHE">DHBVN - HARYANA</option>
									<option value="VCCE">CSEB - CHHATTISGARH</option>
									<option value="VCWE">CESC - WEST BENGAL</option>
									<option value="VBYE">BSES Yamuna - DELHI</option>
									<option value="VBRE">BSES Rajdhani - DELHI</option>
									<option value="VBME">BEST Undertaking - MUMBAI</option>
									<option value="VBBE">BESCOM - BENGALURU</option>
									<option value="VAAE">APDCL - ASSAM</option>
									<option value="VARE">Ajmer Vidyut Vitran Nigam - RAJASTHAN</option>
								</select>
								<p class="error" id="error_ecity_provider"></p>
							</div>

							<div class="group_1">
                            <i class="pe-7s-phone" id="inputico"></i>
								<input type="text" name="accountNumber" id="ecity_account_number" placeholder="Ecity Account Number"> <span class="highlight"></span><span class="bar"></span>
								<label id="dth_number_label"></label>								
								<p class="error" id="error_landline_std"></p>
							</div>

							<div class="group_1" id="cycle_number">
                            <i class="pe-7s-phone" id="inputico"></i>
								<input type="text" name="cycleNumber" class="numeric" id="ecity_cycle_number" placeholder="Cycle No"> <span class="highlight"></span> <span
									class="bar"></span>
									<p class="error" id="error_landline_ca"></p>
							</div>
                            
                            <div class="group_1" id="billing_unit">
                            <i class="pe-7s-phone" id="inputico"></i>
                            <sec:csrfInput/>
								<input type="text" type="text" name="cycleNumber" class="numeric" id="ecity_billing_unit" placeholder="Billing Unit"> <span
									class="highlight"></span> <span class="bar"></span>
								<p class="error" id="error_ecity_billing_unit"></p>
							</div>
                            
                            <div class="group_1" id="processing_cycle">
                            <i class="pe-7s-phone" id="inputico"></i>
								<input type="text" name="cycleNumber" id="ecity_processing_cycle" placeholder="Processing Cycle"> <span
									class="highlight"></span> <span class="bar"></span>
								<p class="error" id="error_ecity_processing_cycle"></p>
							</div>

							<div class="group_1" id="city_name">
                            <i class="pe-7s-phone" id="inputico"></i>
								<input type="text" name="cycleNumber" id="ecity_city_name" placeholder="City Name"> <span
									class="highlight"></span> <span class="bar"></span>
									<p class="error" id="error_ecity_city_name"></p>
							</div>
                            
                            <div class="group_1" id="city_name">
                            <i class="fa fa-inr rup" id="inputico"></i>
								<input type="number" name="amount" min="10" class="numeric" id="ecity_amount"  placeholder="Enter Exact Amount"> <span
									class="highlight"></span> <span class="bar"></span>
									<p class="error" id="error_ecity_amount"></p>
                                <a href="#" id="ecity_get_amount" class="links">Get Due Amount</a>
							</div>
                            
							<center><button type="button" class="btn" id="ecity_submit"
								style="width: 80%; background: #ff0000; margin-top: 10px; color: #FFFFFF;">Coming Soon</button></center>
						</form><br><br>
					</div>					
				</div>
			
            <div class="col-md-6 hidden-sm">
				<div class="slider" id="slider"
					style="margin-right: -15px; margin-left: -15px;">
					<div class="carousel slide hidden-xs" data-ride="carousel"
						id="mycarousel">
						<ol class="carousel-indicators">
							<li class="active" data-slide-to="0" data-target="#mycarousel"></li>
							<li data-slide-to="1" data-target="#mycarousel"></li>
						</ol>

						<div class="carousel-inner">

						<div class="item active" id="slide1">

								<img src='<c:url value="/resources/images/adlapbs_gcm.jpg"/>' />
							</div>
							<!---end item---->

							<div class="item">

								<img src='<c:url value="/resources/images/slider_61.jpg"/>'>
							</div>

							<!---end item---->
						</div>
						<!--end carousel inner------>

					</div>

					<!---end caeousel slider---->
				</div>
				<!---end slider----->
				<table class="table table-fixed" id="plan_table">

				</table>
			</div>
			<div class="item"></div>
			<!---end item---->
			
		</div>
		  <!---end item---->	            	
            <jsp:include page="/WEB-INF/jsp/Agent/Login/Footer.jsp"/>
		</div>
		<!--end carousel inner------>
	</div>
</div>
	<div id="order_confirmation" class="modal fade" role="dialog">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h5 class="modal-title">Order Confirmation</h5>
				</div>
				<div class="modal-body">
					<table class="table table-condensed">
						<tr>
							<td><b>Service </b> </td> <td id="o_service"> </td>
						</tr>
						<tr>
							<td><b id="o_account_name">Account Number </b> </td> <td id="o_account_number"> </td>
						</tr>
						<tr>
							<td><b>Bill Amount</b> </td><td id="o_bill_amount"></td>
						</tr>
						<tr>
							<td><b>Service Charge</b> </td><td id="o_service_amount"></td>
						</tr>
						<tr>
							<td><b>Net Amount</b></td> <td id="o_net_amount"></td>
						</tr>
					</table>
					<button type="button" class="btn btn-block btn-info" id="confirm_order">Confirm</button>
				</div>
			</div>
		</div>
	</div>

<script src="/resources/js/jquery-2.2.3.min.js"></script>
<script src="/resources/js/bootstrap.min.js"></script>
<script src="/resources/js/app.min.js"></script>
<script src="/resources/js/Chart.min.js"></script>
<script src="/resources/js/dashboard2.js"></script>
</body>
</html>