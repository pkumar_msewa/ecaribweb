<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage=""%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<sec:csrfMetaTags/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>LoadMoney</title>
  
<link rel="stylesheet" href="/resources/css/cuscss.css">
  <link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'
	type="image/png" />
<!-- Optional theme -->
<link rel="stylesheet" href="<c:url value='/resources/css/bootstrap-theme.min.css'/>" type='text/css'>
<link rel="stylesheet" href="<c:url value='/resources/css/bootstrap.min.css'/>" type='text/css'>
<script src="<c:url value='/resources/js/jquery.js'/>"></script>
<script src="<c:url value='/resources/js/bootstrap.js'/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/Agent/Agentdetail.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/header.js"/>"></script>
<!-- Latest compiled and minified CSS -->

<script type="text/javascript"
	src="<c:url value="/resources/js/AgentBillpay.js"/>"></script>
  <link rel="stylesheet" href="/resources/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="/resources/css/Pe-icon-7-stroke.css">  
  <link rel="stylesheet" href="/resources/css/AdminLTE.min.css">
  <link rel="stylesheet" href="/resources/css/_all-skins.min.css">
  
	<link rel="stylesheet" href="<c:url value="/resources/css/datepicker.css"/>">
	<script src="<c:url value="/resources/js/datepicker.js"/>"></script>
	
		<script type="text/javascript">
$(document).ready(function() {
	
         		var errorMsg=document.getElementById("errorMsg").value;
        		if(!errorMsg.length == 0){
        			$("#common_error_true").modal("show");
        			$("#common_error_msg").html(errorMsg);
        			var timeout = setTimeout(function(){
        				$("#common_error_true").modal("hide");
        				$("#errorMsg").val("");
        	          }, 5000);
        		}
        		
        		var successMsg=document.getElementById("successMsg").value;
        		if(!successMsg.length == 0){
        			$("#common_success_true").modal("show");
        			$("#common_success_msg").html(successMsg);
        			var timeout = setTimeout(function(){
        				$("#common_success_true").modal("hide");
        				$("#successMsg").val("");
        	          }, 5000); 
        		}
         	});
	<script>
		$(function() {
			$( "#ins_policy_date" ).datepicker({
				format:"yyyy-mm-dd"
			});
		});
	</script>

<style>
.no-js #loader {
	display: none;
}

.js #loader {
	display: block;
	position: absolute;
	left: 100px;
	top: 0;
}

.se-pre-con {
	position: fixed;
	left: 0px;
	top: 0px;
	width: 100%;
	height: 100%;
	z-index: 9999;
	background: url(/images/pq_large.gif) center no-repeat #fff;
}

</style>
	<script src="<c:url value='/resources/js/modernizr.js'/>"></script>
	<script type="text/javascript">
		$(window).load(function() {
			$(".se-pre-con").fadeOut("slow");
		});
	</script>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<jsp:include page="/WEB-INF/jsp/Agent/Login/Header.jsp" />
	
<div class="wrapper">
<jsp:include page="/WEB-INF/jsp/Agent/Login/sidenav.jsp" />
<input type="hidden" name="successMsg" id="successMsg" value="${msg}">
	<input type="hidden" name="errorMsg" id="errorMsg" value="${error}">
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
     
     <div class="col-md-11"><br>

			<div class="container" style="background: white;">
				<div class="col-md-1"></div>
				<div class="col-md-8">
					<div class="form-group">
						<h1>Load Money</h1>
						<hr>


					</div>
					<form method="post" name="customerData" action="<c:url value='/Agent/LoadMoney/AgentProcess' />" class="form-inline">
						<div class="form-group">
							<c:out value="${msg}" default="" escapeXml="true"/>
							<div class="input-group">
								<div class="input-group-addon"><i class="fa fa-rupee"></i></div>
								<input type="number" class="form-control numeric" name="amount"
									 min="10" max="100000" placeholder="<spring:message code="page.user.loadMoney.form.input.amount"/>"
									 size="10" required />
									<sec:csrfInput/>
							</div>&nbsp&nbsp&nbsp&nbsp
						</div>
						<div class="form-group">
							<div style="float: left;"><input type="radio" name="useVnet"  value="true" style="float:left; width:20px; margin-top: 20px;" checked/><img src="<c:url value="/resources/images/net.png"/>"></div>
							<input type="radio" name="useVnet" value="false" style="float:left; width:20px; margin-top: 20px;"/><img src="<c:url value="/resources/images/hd_Others.png"/>">
						</div>
                           <input type="hidden" id="loadmoney_username" name="name" />
						<input type="hidden" id="loadmoney_phone" name="phone" />
						<input type="hidden" id="loadmoney_email" name="email" />
						<input type="hidden" id="loadmoney_address" name="address" />
						
						<!-- Button -->
						<div class="form-group">
								<input type="submit"
									value="<spring:message code="page.user.loadMoney.form.button"/>"  class="btn btn-block"
									style="background: #ff0000; color: #FFFFFF;"/>
						</div><br><br><br>
					</form>
				</div>
				<div class="col-md-2"></div>
			</div>
            <jsp:include page="/WEB-INF/jsp/Agent/Login/Footer.jsp"/>
		</div>
	</div>
</div>
</body>
<script src="/resources/js/jquery-2.2.3.min.js"></script>
<script src="/resources/js/bootstrap.min.js"></script>
<script src="/resources/js/app.min.js"></script>
<script src="/resources/js/Chart.min.js"></script>
<script src="/resources/js/dashboard2.js"></script>
</html>
<!-- ========================================================================================================================== -->

