<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*"  isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<link rel="stylesheet"
	href="<c:url value='/resources/css/font-awesome.min.css'/>" type='text/css'>
<link href='<c:url value='/resources/css/font-family.css'/>' rel='stylesheet' type='text/css'>

	<div class="topline"></div>
	<nav class="navbar navbar-default"
		style="background: rgba(255, 255, 255, 1.00); min-height: 40px; margin-bottom: 0px;">
		<div class="container">
			<div class="navbar-header col-md-4 col-xs-8">
				<a class="navbar-brand" href="<c:out value="/User/Home"/>"><img
					src='<c:url value="/resources/images/vijaya pay logo.png"/>' alt="logo" style="width: 200px;margin-left: -100px;" ></a>
			</div>
			<ul class="nav navbar-nav navbar-right" style="float: left;">
			
				<li id="wallets"><a href="<c:url value="/Agent/LoadMoney/Process"/>"><div style="float:left;"><i class="fa fa-dollar"></i> <h id="account_balance">${balance}</h> <p>Add Money</p></div>
                <img src='<c:url value="/resources/images/main/wallet.png"/>' alt="wallet" style="margin-left:10px;width: 40px; "> </a>
                </li>
						 
				<div class="dropdown">
				   <li style="margin-top: -11px;"><a href="#"><p id="first_name"></p>
				   <img id="small_display_pic" src='<c:url value="/resources/images/sample.jpg"/>' class="img-circle"
							style=" margin-top: 15px;" width="40" height="40"/>
                   </a></li>
					<div class="dropdown-content">
						<a href="<c:url value="/Agent/Login"/>"><i class="fa fa-tachometer" style="color: #17bcc8"></i> Dashboard</a>
				        <a href="<c:url value="/Agent/Settings"/>"><i class="fa fa-cogs" aria-hidden="true" style="color: #17bcc8"></i> Settings</a>
						<a href='<c:url value="/Agent/AgentReceipts"/>'><i class="fa fa-files-o" aria-hidden="true" style="color: #17bcc8"></i> My Receipts</a>
					    <a href="${pageContext.request.contextPath}/Agent/Travel/Flight/MyTickets"><i class="fa fa-users" style="color: #17bcc8"></i> Travel Tickets</a>
					    <a href="<c:url value="/Agent/Logout"/>"><i class="fa fa-sign-out" aria-hidden="true" style="color: #17bcc8"></i> Logout Agent</a>
					</div>
				</div>
			</ul>
		</div>
		<!-- /.container-fluid -->
	</nav>
	
	
	 <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
          <p>Some text in the modal.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>