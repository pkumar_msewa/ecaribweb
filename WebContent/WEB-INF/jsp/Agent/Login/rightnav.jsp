   <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-md-12">
          <!-- small box -->
          <!--  <div class="col-md-3">
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>150</h3>

              <p>Total Credit</p>
            </div>
            <div class="icon">
              <i class="fa fa-line-chart" aria-hidden="true"></i>
            </div>
          </div>
          </div> -->
          <!-- small box -->
           <div class="col-md-3">
          <div class="small-box bg-green">
            <div class="inner" style="height: 101px">
              <h3 class="debit_Amount_Agent"></h3>
              <p>Total Debit</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
          </div>
          </div>
          <!-- small box -->
           <div class="col-md-3">
          <div class="small-box bg-yellow">
            <div class="inner" style="height: 101px">
              <h3 class="user_Register_Agent"></h3>
              <p>User Registrations</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
          </div>
          </div>
          <!-- small box -->
       <!--     <div class="col-md-3">
          <div class="small-box bg-red">
            <div class="inner">
              <h3>65</h3>

              <p>Total Commission</p>
            </div>
            <div class="icon">
              <i class="fa fa-money" aria-hidden="true"></i>
            </div>
          </div>
        </div> -->
        <!-- ./col -->
        </div>
      </div>
    </section>
    </div>
    <!-- /.content -->
