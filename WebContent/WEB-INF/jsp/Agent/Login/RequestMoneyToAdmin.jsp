<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage=""%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
 <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title></title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <sec:csrfMetaTags/>
<link rel="stylesheet" href="/resources/css/cuscss.css">
<link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'
	type="image/png" />
<!-- Optional theme -->
<link rel="stylesheet" href="<c:url value='/resources/css/bootstrap-theme.min.css'/>" type='text/css'>
<link rel="stylesheet" href="<c:url value='/resources/css/bootstrap.min.css'/>" type='text/css'>
<script src="<c:url value='/resources/js/jquery.js'/>"></script>
<script src="<c:url value='/resources/js/bootstrap.js'/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/Agentdetail.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/header.js"/>"></script>
<!-- Latest compiled and minified CSS -->

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/AgentSendMoeny.js"></script>

  <link rel="stylesheet" href="/resources/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="/resources/css/Pe-icon-7-stroke.css">  
  <link rel="stylesheet" href="/resources/css/AdminLTE.min.css">
  <link rel="stylesheet" href="/resources/css/_all-skins.min.css">
   

    
 <script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/AgentSendMoeny.js"></script>

<style>
.no-js #loader {
	display: none;
}

.js #loader {
	display: block;
	position: absolute;
	left: 100px;
	top: 0;
}

.se-pre-con {
	position: fixed;
	left: 0px;
	top: 0px;
	width: 100%;
	height: 100%;
	z-index: 9999;
	background: url(/images/pq_large.gif) center no-repeat #fff;
}
</style>
<script src="<c:url value='/resources/js/modernizr.js'/>"></script>

<script type="text/javascript">
	$(window).load(function() {
		$(".se-pre-con").fadeOut("slow");
	});
</script>

</head>

<body class="hold-transition skin-blue sidebar-mini">

	<jsp:include page="/WEB-INF/jsp/Agent/Login/Header.jsp" />
	
<div class="wrapper">
<jsp:include page="/WEB-INF/jsp/Agent/Login/sidenav.jsp" />

<div class="content-wrapper" style="height: 1000px;">
    <!-- Content Header (Page header) -->
     
     <div class="col-md-11"><br>

	<div id="banktransfer_submit" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<a
				href="https://play.google.com/store/apps/details?id=in.msewa.vpayqwik&hl=en">
				<img src="<c:url value='/resources/images/popup_bank.jpg'/>"
				class="img-responsive" width="100%" alt="">
			</a>

		</div>
	</div>
	<!--end modal-->
		<div class="row">
			<div class="col-md-6" id="left">
				<!---form---->
				
				<div class="tab-content" style="background: white;">
					<span id="error_mobile_sm" class="label label-danger"></span> <span
						id="success_mobile_sm" class="label label-success"></span>
					<div id="home" class="tab-pane fade in active">

						<form method="post" action="#"><br>
                         <h3 style="margin-top: auto; margin-left: 4%;"><span style="border-bottom: solid #17bcc8;">Request Money</span></h3><br><br><br>
							<div class="group_1">
                            <i class="fa fa-inr rup" id="inputico"></i>
								<input type="number" name="amount" min="10" id="lma_amount"	placeholder="Amount"> <span class="highlight"></span> <span
									class="bar"></span>
								<p id="error_lma_amount" class="error"></p>
							</div>

							<div class="group_1">
                            <i class="pe-7s-filter" id="inputico"></i> 
                            <sec:csrfInput/>
								<input type="text" name="amount" min="10" id="lma_message" placeholder="Message"> <span class="highlight"></span> <span
									class="bar"></span>
								<p id="error_lma_message" class="error"></p>
							</div>

							<center><button type="button" class="btn" id="lma_submit"
								style="width: 80%; background: #ff0000; margin-top: 10px; color: #FFFFFF;">Send</button></center>
						</form><br><br><br>
					</div>


				</div>

			</div>
			<!----end col-md-4-->

			<div class="col-md-6 hidden-sm">
				<div class="slider" style="margin-right: -15px; margin-left: -15px;">
					<div class="carousel slide hidden-xs" data-ride="carousel"
						id="mycarousel">
						<ol class="carousel-indicators">
							<li class="active" data-slide-to="0" data-target="#mycarousel"></li>
							<li data-slide-to="1" data-target="#mycarousel"></li>
						</ol>

						<div class="carousel-inner">

						<div class="item active" id="slide1">

								<img src='<c:url value="/resources/images/adlapbs_gcm.jpg"/>' />
							</div>
							<!---end item---->

							<div class="item">

								<img src='<c:url value="/resources/images/slider_61.jpg"/>'>
							</div>
							<!---end item---->
						</div>
						<!--end carousel inner------>

					</div>
					<!---end caeousel slider---->
				</div>
				<!---end slider----->
			</div>
		</div>
		<!-----end col-md-8-->
		 <jsp:include page="/WEB-INF/jsp/Agent/Login/Footer.jsp"/>
	</div>
	<!---end row-->
	<!----end container-->
	
	<div id="order_confirmation" class="modal fade" role="dialog">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h5 class="modal-title">Order Confirmation</h5>
				</div>
				<div class="modal-body">
					<table class="table table-condensed">
						<tr>
							<td><b>Mobile No. </b></td>
							<td id="o_mobile_number"></td>
						</tr>
						<tr>
							<td><b>Amount</b></td>
							<td id="o_amount"></td>
						</tr>
						<tr>
							<td><b>Commission</b></td>
							<td id="o_commission"></td>
						</tr>
						<tr>
							<td><b>Net Amount</b></td>
							<td id="o_net_amount"></td>
						</tr>
					</table>
					<button type="button" class="btn btn-block btn-info"
						id="confirm_order">Confirm</button>
				</div>
			</div>
		</div>
	</div>

	</div>
    </div>
    </div>
    
	<script type="text/javascript">
	$("#lma_submit").click(function() {
		 
        var spinnerUrl = "Please Wait <img src='/resources/images/spinner.gif' height='20' width='20'>"
        var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");

        var csrfHeader = $("meta[name='_csrf_header']").attr("content");
        var csrfToken =$("meta[name='_csrf']").attr("content");
        var hash_key = "hash";
        var default_hash = "123456";
        var headers = {};
        headers[hash_key] = default_hash;
        headers[csrfHeader] = csrfToken;

        var valid = true;
        $("#error_lma_mobile").html("");
        $("#error_lma_amount").html("");
       
        var amount = $("#lma_amount").val();
        var message = $("#lma_message").val();
        var commission = calculateSendMoneyCommission(amount);
        var numericAmount =parseFloat(amount);
    
        if (amount < 10) {
            valid = false;
            $("#error_lma_amount").html("Amount must begreater than or equal to 10 ");
            } if (message.length <= 0) { valid = false;
                $("#error_lma_message").html("Please enter message");
            }
            if (valid == true) {
               
                $("#o_amount").html("<i class='fafa - rupee '></i> " + amount);
                $("#o_commission").html( "<i class='fa fa - rupee '></i> " + commission);
                $("#o_net_amount").html( "<i class='fa fa - rupee '></i> " + (numericAmount + commission));
               
                  
                           
                            $("#lma_submit").attr("disabled", "disabled");
                            $("#lma_submit").html(spinnerUrl);
                            $.ajax({
                                type: "POST",
                                headers: headers,
                                contentType: "application/x-www-form-urlencoded",
                                url: "/Agent/SendMoney/sendMoneyRequestSuperAgent",
                                data: {
                                    mobileNumber: "",
                                    amount: amount,
                                    message: message
                                },
                                success: function(response) {
                                    $("#lma_submit").removeAttr("disabled", "disabled");
                                    $("#lma_submit").html("Send");
                                    var parsedResponse = JSON.parse(response.response);
                                    
                                    //alert(parsedResponse);
                                if (parsedResponse.code == "F00") {
                                        $("#error_mobile_sm").html(parsedResponse.details);
                                    }
                                    if (parsedResponse.code == "S00") {
                                        $("#account_balance").html(parsedResponse.balance);
                                        $("#success_mobile_sm").html(parsedResponse.details);
                                        $("#lma_mobile").val("");
                                        $("#lma_amount").val("");
                                        $("#lma_message").val("");
                                    } 
                                }
                            });

                       
                }
            });
	</script>
    
<script src="/resources/js/jquery-2.2.3.min.js"></script>
<script src="/resources/js/bootstrap.min.js"></script>
<script src="/resources/js/app.min.js"></script>
<script src="/resources/js/Chart.min.js"></script>
<script src="/resources/js/dashboard2.js"></script>
</body>
</html>
