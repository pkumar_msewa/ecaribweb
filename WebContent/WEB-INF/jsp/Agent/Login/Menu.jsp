<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<!--  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
--> <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<style>.wrap {
	position: relative;
	-webkit-transition: all 0.3s ease-out;
	-moz-transition: all 0.3s ease-out;
	-ms-transition: all 0.3s ease-out;
	-o-transition: all 0.3s ease-out;
	transition: all 0.3s ease-out;
}
.wrap.active {
	left: 16em;
}
a.menu-link {
	display: block;
	padding: 1em;
	z-index: 999;
}
nav[role=navigation] {
	background: #17bcc8;
	clear: both;
	overflow: hidden;
}
.js nav[role=navigation] {
	width: 16em;
	height: 100%;
	position: absolute;
	top: 0;
	left: -16em;
}
nav[role=navigation] ul {
	margin: 0;
	padding: 0;
	border-top: 1px solid #808080;
}
nav[role=navigation] li a {
	display: block;
	padding: 0.8em;
	color: #fff;
	border-bottom: 1px solid #fff;
}
.lorem {
	clear: both;
}

@media screen and (min-width: 41.25em) {
	a.menu-link {
		display: none;
	}
	.js nav[role=navigation] {
		max-width: none;
		position: static;
		width: auto;
	}
	.wrap.active {
		left: 0;
	}
	nav[role=navigation] ul {
		margin: 0;
		border: 0;
	}

	nav[role=navigation]  li {
		display: inline-block;
		margin: 0 0.25em;
	}
	nav[role=navigation] li a {
		border: 0;
	}

}</style>
<div class="wrap" id="wrap">
	<a href="#menu" class="menu-link" style="background: #17bcc8;"><i class="fa fa-bars fa-2x" style="color: white;"></i></a>
	<nav id="menu" role="navigation">
		<center><ul>
		<li><a href="<c:url value="/Agent/MobileTopup"/>"> <i class="fa fa-mobile fa-2x menufa"></i> <spring:message
					code="page.user.menu.topup" /></a></li>
			<li> <a
					href="<c:url value="/Agent/BillPayment"/>"> <i class="fa fa-lightbulb-o fa-2x menufa"></i> <spring:message
					code="page.user.menu.billPayment" /></a></li> 
			<li> <a
					href="<c:url value="/Agent/SendMoney"/>"><i class="fa fa-money fa-2x menufa"></i>
				<spring:message code="page.user.menu.sendMoney" /></a></li>
				
					<li>
				<a href="/Agent/RequestMoneyToAdmin" > <i class="fa fa-suitcase fa-2x menufa"></i>Request Money
				</a></li>
			<li>
				<%-- <a href="<c:url value="/User/Travel/BusTravel"/>"><i class="fa fa-suitcase fa-2x menufa" aria-hidden="true"style="font-size:25px;color:white"></i>Travel</a> --%>
				<a href="http://travel.vpayqwik.com/" target="_blank"> <i class="fa fa-suitcase fa-2x menufa"></i>
					<spring:message	code="page.user.menu.travel" /></a></li>
			<%-- <li> <a href='<spring:url value="/User/RedeemCoupon"/>'> <i class="fa fa-ticket fa-2x menufa"></i> <spring:message code="page.user.menu.coupon" /></a></li>
			<li> <a href="<spring:url value="/User/PayAtStore"/>"> <i class="fa fa-university fa-2x menufa"></i> <spring:message	code="page.user.menu.payStore" /></a></li>
 --%>	<%-- 		<li class="dropdown" style="position: absolute;"> <a href="<spring:url value="/User/Others"/>"> <i class="fa fa-university fa-2x menufa"></i> <spring:message	code="page.user.menu.others" /></a>
			  <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="fa fa-university fa-2x menufa"></i> Others <span class="caret"></span></a>
				<ul class="dropdown-menu" style="background:#17bcc8;">
					  <li><a href="<spring:url value="/MeraEvents/AuthCode"/>">Book Events</a></li>

					<li><a href=" <c:url value="/User/GciProducts/GetBrands"/>">Gift Card</a></li>
					<!-- <li><a href="#">Page 1-3</a></li> -->
				</ul>
			</li> --%>
		</ul></center>
	</nav>
	<script>$(document).ready(function() {
		$('body').addClass('js');

		var $menu = $('#menu'),
				$menulink = $('.menu-link'),
				$wrap = $('#wrap');

		$menulink.click(function() {
			$menulink.toggleClass('active');
			$wrap.toggleClass('active');
			return false;
		});
	});</script>
</div>
