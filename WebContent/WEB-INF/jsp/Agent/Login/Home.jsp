<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage=""%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<sec:csrfMetaTags/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>eCarib | Agent Home</title>
<link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'
	type="image/png" />
<link rel="stylesheet" href="/resources/css/cuscss.css">
<link rel="stylesheet" href="/resources/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<link rel="stylesheet" href="/resources/css/Pe-icon-7-stroke.css">
<link rel="stylesheet" href="/resources/css/AdminLTE.min.css">
<link rel="stylesheet" href="/resources/css/_all-skins.min.css">
<script type="text/javascript" src="/resources/js/new_js/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>
<script src="/resources/js/new_js/script.js"></script>
<script type="text/javascript" src="<c:url value="/resources/js/Agent/Agentdetail.js"/>"></script>

<script type="text/javascript">
	$(window).load(function() {
		
		
	});
</script>

<script>

$(document).ready(function() {

var sc=	$('#flightSusMsg').val();
var fl=	$('#flightflMsg').val();

var successMsg = document.getElementById("flightSusMsg").value;
var fsucc = document.getElementById("fSuccess").value;
var str = "Transasction Successful.Your Flight Is Booked.";

console.log("successMsg :: " + successMsg);
if (successMsg.includes(str)) {
	/* $("#flightSusMsg").val(""); */
	console.log("In success"+successMsg);
	document.getElementById("flightSusMsg").value="";
	document.getElementById("fSuccess").value="";
	
	$('#flightModal').modal({
        backdrop: 'static',
        keyboard: true, 
        show: true
	});
	$("#paySuccessMsgVal").html(successMsg);
}

if(!successMsg.includes(str) && successMsg!=""){
	/* $("#flightSusMsg").val(""); */
	console.log("In else");
	$('#payErrMsgVal').html(successMsg);	
	$('#ErrmyModal').modal("show");
	document.getElementById("flightSusMsg").value="";
}
if (fl!="") {
	$('#flightflMsg').val("");
	document.getElementById("flightflMsg").value="";
	$('#payErrMsgVal').html(fl);	
	$('#ErrmyModal').modal("show");
}

var logout = $("#logoutmsg").val();
console.log("logout=="+logout);
if(successMsg.length==0){
	console.log("In flight logout");
	$("#flightModal").modal("hide");
}
 $("#payment_ok").click(function() {
		window.location.href='${pageContext.request.contextPath}/Agent/Travel/Flight/MyTickets'; 
	});
 
 $("#payment_fail").click(function() {
		window.location.href='${pageContext.request.contextPath}/Agent/Travel/Flight/MyTickets';  
	}); 
 
});
 </script>

</script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<input type="hidden" name="flightSusMsg" id="flightSusMsg" value="${msg}">
<input type="hidden" name="flightflMsg" id="flightflMsg" value="${flighterrMsg}">
<input type="hidden" name="logoutmsg" id="logoutmsg" value="${Alogout}">
<input type="hidden" name="fSuccess" id="fSuccess" value="${fsuccess}">
	<jsp:include page="/WEB-INF/jsp/Agent/Login/Header.jsp" />

	<div class="wrapper">
		<jsp:include page="/WEB-INF/jsp/Agent/Login/sidenav.jsp" />

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->

			<div class="col-md-12" id="left">
				<br>
				<jsp:include page="/WEB-INF/jsp/Agent/Login/rightnav.jsp" />

				<!-- Content Header (Page header) -->
				<div class="row">
					<div class="col-md-12">
						<div class="box">
							<div class="box-header with-border">
								<h3 class="box-title">Monthly Recap Report</h3>
							</div>
							<!-- /.box-header -->
							<div class="box-body">
								<div class="row">
									<div class="col-md-8" id="left">
										<p class="text-center"></p>

										<div class="chart">
											<!-- Sales Chart Canvas -->
											<canvas id="salesChart" style="height: 180px;"></canvas>
										</div>
										<!-- /.chart-responsive -->
									</div>
									<!-- /.col -->

								</div>
								<!-- /.row -->
							</div>

						</div>
						<!-- /.box -->
						<div class="col-sm-11">
							<jsp:include page="/WEB-INF/jsp/Agent/Login/Footer.jsp" />
						</div>
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row -->
			</div>



			<!-- /.content-wrapper -->
			<footer class="main-footer">
				<strong>Msewa Copyright &copy; 2017-2018</strong>
			</footer>



			<div class="control-sidebar-bg"></div>
		</div>
		<!-- ./wrapper -->
</div>


	<div class="modal fade" id="flightModal" role="dialog" hidden="hidden">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content" style="margin-top: 40%;margin-left: 100px;">

				<div class="modal-body">
					<center>
						<img
							src="${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/source-(tick).gif"
							style="width: 140px; margin-top: -91px;    margin-bottom: 10px;">
						<div
							style="background: white; margin-top: -20px; padding: 2.5% 2%;">
							<h4>
								<h4 id="paySuccessMsgVal"><b><br><small></b></small></h4>
								<small></b></small>
							</h4>
							<center>
								<button type="button" id="payment_ok">OK</button>
							</center>
						</div>
					</center>
				</div>

			</div>
		</div>
	</div>

	<%-- <div id="errModel" role="dialog" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<center id="common_error_msg" class="alert alert-danger"></center>
					<center><label id="common_error_msg" class="alert alert-danger"></label></center>
				</div>
			</div>
		</div>
	</div> --%>
	
	
   <div class="modal fade" id="ErrmyModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content" style="margin-top: 40%;">
       
        <div class="modal-body">
          <center>
        <div style="background: white; padding: 2.5% 2%; color: #bb181f;">
          <h3 id="payErrMsgVal"></h3>
         <%--  <center><button type="button" id="payment_fail">OK</button></center> --%>
        </div></center>
        </div>
      </div>
      
    </div>
  </div>
	<script type="text/javascript">
	
	</script>
		<!-- script-->
		<script src="/resources/js/jquery-2.2.3.min.js"></script>
		<script type="text/javascript" src="<c:url value="/resources/js/Agent/Agentdetail.js"/>"></script>
		<script src="/resources/js/bootstrap.min.js"></script>
		<script src="/resources/js/app.min.js"></script>
		<script src="/resources/js/Chart.min.js"></script>
		<script src="/resources/js/dashboard2.js"></script>
</body>
</html>