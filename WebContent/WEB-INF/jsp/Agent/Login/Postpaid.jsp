<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage=""%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
 <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Postpaid</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <sec:csrfMetaTags/>
<link rel="stylesheet" href="/resources/css/cuscss.css">
  <link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'
	type="image/png" />
<!-- Optional theme -->
<link rel="stylesheet" href="<c:url value='/resources/css/bootstrap-theme.min.css'/>" type='text/css'>
<link rel="stylesheet" href="<c:url value='/resources/css/bootstrap.min.css'/>" type='text/css'>
<script src="<c:url value='/resources/js/jquery.js'/>"></script>
<script src="<c:url value='/resources/js/bootstrap.js'/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/Agent/Agentdetail.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/header.js"/>"></script>
<!-- Latest compiled and minified CSS -->

<%-- <script type="text/javascript" src="<c:url value="/resources/js/AgentTopup.js"/>" ></script> --%>
	
  <link rel="stylesheet" href="/resources/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="/resources/css/Pe-icon-7-stroke.css">  
  <link rel="stylesheet" href="/resources/css/AdminLTE.min.css">
  <link rel="stylesheet" href="/resources/css/_all-skins.min.css">
  
	<style>
		.no-js #loader {
			display: none;
		}

		.js #loader {
			display: block;
			position: absolute;
			left: 100px;
			top: 0;
		}

		.se-pre-con {
			position: fixed;
			left: 0px;
			top: 0px;
			width: 100%;
			height: 100%;
			z-index: 9999;
			background: url(/images/pq_large.gif) center no-repeat #fff;
		}
	</style>
	<script src="<c:url value='/resources/js/modernizr.js'/>"></script>

	<script type="text/javascript">
		$(window).load(function() {
			$(".se-pre-con").fadeOut("slow");
		});
	</script>

</head>

<body class="hold-transition skin-blue sidebar-mini">

<jsp:include page="/WEB-INF/jsp/Agent/Login/Header.jsp" />
	
<div class="wrapper">
<jsp:include page="/WEB-INF/jsp/Agent/Login/sidenav.jsp" />

<div class="content-wrapper" style="height:1000px;">
    <!-- Content Header (Page header) -->
     
     <div class="col-md-11"><br>
     
        <a data-toggle="modal" data-target="#order_confirmation"></a>
        <div class="row">

			<div class="col-md-6">

				<div class="tab-content" style="background: white;">
                    <span id="error_post_topup" class="label label-danger"></span>
                    <span id="success_post_topup" class="label label-success"></span>

						<form action="#" method="post">
						<br> <h3 style="margin-top: auto; margin-left: 4%;"><span style="border-bottom: solid #17bcc8;">Postpaid</span></h3>

							<div class="group_1">
	                            <i class="pe-7s-phone" id="inputico"></i>
	                            <sec:csrfInput/>
								<input type="text" name="mobileNo" id="post_mobile" placeholder="Postpaid Mobile No"> <span
									class="highlight"></span><span class="bar"></span>
								<p id="error_pre_mobile" class="error"></p>								
								<a data-toggle="modal" data-target="#order_confirmation"></a>
							</div>

							<div class="group_1">
                            <i class="pe-7s-filter" id="inputico"></i>    
							<select name="serviceProvider" id="post_operator"
									class="form-control" style="border-radius: 0; width: 80%; border: transparent;border-bottom: gray;border-style: solid;border-width: 1.8px; font-family: 'Ubuntu', sans-serif; padding-left: 0; height: 55px; margin-top: -19px; font-weight: bold; color: #928F8F; box-shadow: none;">
									<option value="#">Select Operator</option>
									<option value="VACC">Aircel</option>
									<option value="VATC">Airtel</option>
									<option value="VBGC">BSNL</option>
									<option value="VIDC">Idea</option>
									<option value="VMTC">MTS</option>
									<option value="VRGC">Reliance</option>
									<option value="VTDC">Tata Docomo</option>
									<option value="VVFC">Vodafone</option>
								</select>
								<p id="error_post_operator" class="error"></p>
							</div>

							<div class="group_1">
                            <i class="pe-7s-filter" id="inputico"></i>    
								<select name="serviceProvider" id="post_circle"
									class="form-control" style="border-radius: 0; width: 80%; border: transparent;border-bottom: gray;border-style: solid;border-width: 1.8px; font-family: 'Ubuntu', sans-serif; padding-left: 0; height: 55px; margin-top: -19px; font-weight: bold; color: #928F8F; box-shadow: none;">
									<option value="">Select Area</option>
									<option value="AP">Andhra Pradesh</option>
									<option value="AS">Assam</option>
									<option value="BR">Bihar and Jharkhand</option>
									<option value="CH">Chennai</option>
									<option value="DL">Delhi</option>
									<option value="GJ">Gujarat</option>
									<option value="HR">Haryana</option>
									<option value="HP">Himachal Pradesh</option>
									<option value="JK">Jammu and Kashmir</option>
									<option value="KN">Karnataka</option>
									<option value="KL">Kerala</option>
									<option value="KO">Kolkata</option>
									<option value="MP">Madhya Pradesh/Chattisgarh</option>
									<option value="MH">Maharashtra</option>
									<option value="MU">Mumbai</option>
									<option value="NE">North East</option>
									<option value="OR">Orissa</option>
									<option value="PB">Punjab</option>
									<option value="RJ">Rajasthan</option>
									<option value="TN">Tamil Nadu</option>
									<option value="UW">Uttar Pradesh(W)/Uttranchal</option>
									<option value="UE">Uttar Pradesh(E)</option>
									<option value="WB">West Bengal</option>
								</select>
								<p id="error_post_circle" class="error"></p>
							</div>
							
							<div class="group_1">
                            <i class="pe-7s-phone" id="inputico"></i>
								<input type="number" id="post_amount" name="amount" class="numeric" min="10" placeholder="Enter Amount"> <span
									class="highlight"></span> <span class="bar"></span>
									<p id="error_pre_amount" class="error"></p>
							</div>

							<center><button type="button" class="btn" id="post_submit" 
								style="width: 80%; background: #ff0000; color: #FFFFFF;">Coming Soon</button></center>
                                <br>
						</form>
					</div><!-- end prepaid -->
                    
                    
					</div>



			<div class="col-md-6 hidden-xs">
				<div class="slider" id="slider"
					style="margin-right: -15px; margin-left: -15px;">
					<div class="carousel slide hidden-xs" data-ride="carousel"
						id="mycarousel">
						<ol class="carousel-indicators">
							<li class="active" data-slide-to="0" data-target="#mycarousel"></li>
							<li data-slide-to="1" data-target="#mycarousel"></li>
						</ol>

						<div class="carousel-inner">

							<div class="item active" id="slide1">

								<img src='<c:url value="/resources/images/adlapbs_gcm.jpg"/>' />
							</div>
							<!---end item---->

							<div class="item">

								<img src='<c:url value="/resources/images/slider_61.jpg"/>'>
							</div>

							<!---end item---->
						</div>
						<!--end carousel inner------>

					</div>

					<!---end caeousel slider---->
				</div>
				<!---end slider----->
			</div>
		</div>
		<!-----end col-md-8-->
        <jsp:include page="/WEB-INF/jsp/Agent/Login/Footer.jsp"/>
	</div>
    
	<!---end row-->
	<div id="order_confirmation" class="modal fade" role="dialog">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h5 class="modal-title">Order Confirmation</h5>
				</div>
				<div class="modal-body">
						<table class="table table-condensed">
							<tr>
						<td><b>Topup </b> </td> <td id="o_topup"> </td>
						</tr>
                            <tr>
                                <td><b>Mobile </b> </td> <td id="o_mobile"> </td>
                            </tr>

                            <tr>
								<td><b>Operator </b> </td> <td id="o_operator"> </td>
							</tr>
							<tr>
								<td><b>Area </b> </td> <td id="o_area"></td>
							</tr>
							<tr>
								<td><b>Amount</b> </td> <td id="o_amount"></td>
							</tr>


						</table>
					<button type="button" class="btn btn-block btn-info" id="confirm_order">Confirm</button>
				</div>
			</div>
		</div>
	</div>
	<div id="browse_plans_modal" role="dialog" class="modal fade">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h2><b>Choose Plan</b></h2>
				</div>
				<div class="modal-body" id="browse_plans_body">
 			<div class="row">
 			<div class="col-md-12">
 			<ul class="nav nav-tabs">
    			<li class="active"><a data-toggle="tab" href="#3g" class="size">3G</a></li>
    			<li><a data-toggle="tab" href="#2g" class="size">2G</a></li>
 	  			<li><a data-toggle="tab" href="#special" class="size">Special</a></li>
 	  			<li><a data-toggle="tab" href="#regular" class="size">Regular</a></li>
  			</ul>
  			</div>
			</div>
          <div class="tab-content">
    			<div id="3g" class="tab-pane fade in active">
    				<table id="3g_plans" class="table table-hover">
    				</table>
    			</div>
    			<div id="2g" class="tab-pane fade">
    				<table id="2g_plans" class="table table-hover">
    				</table>
    			</div>
    			<div id="special" class="tab-pane fade">
    				<table id="special_plans" class="table table-hover">
    				</table>
    			</div>
    			<div id="regular" class="tab-pane fade">
    			    <table id="regular_plans" class="table table-hover">
    				</table>
    			</div>
    			

  </div>							
							
											
				</div>
			</div>
		</div>
	</div>

    <div id="browse_dc_plans_modal" role="dialog" class="modal fade">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h2><b>Choose Plan</b></h2>
                </div>
                <div class="modal-body" id="browse_dc_plans_body">
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#3g_dc" class="size">3G</a></li>
                                <li><a data-toggle="tab" href="#2g_dc" class="size">2G</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="tab-content">
                        <div id="3g_dc" class="tab-pane fade in active">
                            <table id="3g_dc_plans" class="table table-hover">
                            </table>
                        </div>
                        <div id="2g_dc" class="tab-pane fade">
                            <table id="2g_dc_plans" class="table table-hover">
                            </table>
                        </div>


                    </div>


                </div>
            </div>
        </div>
    </div>

<script src="/resources/js/jquery-2.2.3.min.js"></script>
<script src="/resources/js/bootstrap.min.js"></script>
<script src="/resources/js/app.min.js"></script>
<script src="/resources/js/Chart.min.js"></script>
<script src="/resources/js/dashboard2.js"></script>
</body>
</html>
