<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage=""%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
 <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SendMoney Mobile</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <sec:csrfMetaTags/>
<link rel="stylesheet" href="/resources/css/cuscss.css">

<link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'
	type="image/png" />
  
<!-- Optional theme -->
<link rel="stylesheet" href="<c:url value='/resources/css/bootstrap-theme.min.css'/>" type='text/css'>
<link rel="stylesheet" href="<c:url value='/resources/css/bootstrap.min.css'/>" type='text/css'>
<script src="<c:url value='/resources/js/jquery.js'/>"></script>
<script src="<c:url value='/resources/js/bootstrap.js'/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/Agent/Agentdetail.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/header.js"/>"></script>
<!-- Latest compiled and minified CSS -->

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/AgentSendMoeny.js"></script>

  <link rel="stylesheet" href="/resources/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="/resources/css/Pe-icon-7-stroke.css">  
  <link rel="stylesheet" href="/resources/css/AdminLTE.min.css">
  <link rel="stylesheet" href="/resources/css/_all-skins.min.css">
  

    
 <script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/AgentSendMoeny.js"></script>

	<style>
		.no-js #loader {
			display: none;
		}

		.js #loader {
			display: block;
			position: absolute;
			left: 100px;
			top: 0;
		}

		.se-pre-con {
			position: fixed;
			left: 0px;
			top: 0px;
			width: 100%;
			height: 100%;
			z-index: 9999;
			background: url(/images/pq_large.gif) center no-repeat #fff;
		}
	</style>
	<script src="<c:url value='/resources/js/modernizr.js'/>"></script>

	<script type="text/javascript">
		$(window).load(function() {
			$(".se-pre-con").fadeOut("slow");
		});
	</script>

</head>

<body class="hold-transition skin-blue sidebar-mini">
<div id="banktransfer_submit" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
        <a href="https://play.google.com/store/apps/details?id=in.msewa.vpayqwik&hl=en">
		<img src="<c:url value='/resources/images/popup_bank.jpg'/>" class="img-responsive" width="100%"  alt=""></a>

	</div>
</div><!--end modal-->
<jsp:include page="/WEB-INF/jsp/Agent/Login/Header.jsp" />
	
<div class="wrapper">
<jsp:include page="/WEB-INF/jsp/Agent/Login/sidenav.jsp" />

<div class="content-wrapper" style="height: 1000px;">
    <!-- Content Header (Page header) -->
     
     <div class="col-md-11"><br>
    	<div class="row">
            <div class="col-md-6" id="left">
             <div class="tab-content" style="background: white;">  
             
                <span id="error_mobile_sm" class="label label-danger"></span>
                            <span id="success_mobile_sm" class="label label-success"></span>
                                                                             
                                    <div id="Prepaid" class="tab-pane fade in active"><br> 
                                        <h3 style="margin-top: auto; margin-left: 4%;"><span style="border-bottom: solid #17bcc8;">Send Money</span></h3><br>
                            <form method="post" action="#">
                             
                                <div class="group_1">
                                   <i class="pe-7s-phone" id="inputico"></i>
                                    <input type="text" name="mobileNumber" maxlength="10" id="smm_mobile" placeholder="Mobile No">
                                    <span class="highlight"></span> <span class="bar"></span>
                                    <p class="error" id="error_smm_mobile"></p>
                                </div>
    
                                <div class="group_1">
                                    <i class="fa fa-inr rup" id="inputico"></i>
                                    <sec:csrfInput/>
                                    <input type="number" name="amount" min="10" id="smm_amount" placeholder="Amount"> <span
                                        class="highlight"></span> <span class="bar"></span>
                                    <p id="error_smm_amount" class="error"></p>
                                </div>
    
                                <div class="group_1">
                                 <i class="pe-7s-filter" id="inputico"></i>
                                    <input type="text" name="amount" min="10" id="smm_message" placeholder="Message" > <span
                                        class="highlight"></span> <span class="bar"></span>
                                    <p id="error_smm_message" class="error"></p>
                                </div>
                                <div class="group_1">
                                  <center><button type="button" class="btn" id="smm_submit"
                                    style="width: 80%; background: #ff0000; margin-top: 10px; color: #FFFFFF;">Send</button></center>
                                 </div>
                            </form><br>
                            </div><!--end Prepaid-->
                         </div><!---end tab-content-->
                    </div><!----end col-md-6-->
                    
                    <div class="col-md-6 hidden-sm">

                        <div class="slider" id="slider" style="margin-right: -15px; margin-left: -15px;">	
                            <div class="carousel slide hidden-xs" data-ride="carousel" id="mycarousel">
                                <ol class="carousel-indicators">
                                    <li class="active" data-slide-to="0" data-target="#mycarousel"></li>
                                    <li data-slide-to="1" data-target="#mycarousel" class=""></li>
                                </ol>

                                <div class="carousel-inner">        
                                    <div class="item active" id="slide1">        
                                        <img src="<c:url value="/resources/images/adlapbs_gcm.jpg"/>">
                                    </div>
                                    <!---end item---->
        
                                    <div class="item">
                                        <img src="<c:url value="/resources/images/slider_61.jpg"/>">
                                    </div>
        
                                    <!---end item---->
                                </div>
                                <!--end carousel inner------>
                            </div>
                        </div>
					<!---end caeousel slider---->
                    </div> <br>
   				</div>
               <jsp:include page="/WEB-INF/jsp/Agent/Login/Footer.jsp"/>
    	</div><!--end main-col-8-->
        
         
      
<div id="order_confirmation" class="modal fade" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Order Confirmation</h5>
			</div>
			<div class="modal-body">
				<table class="table table-condensed">
					<tr>
						<td><b>Mobile No. </b></td> <td id="o_mobile_number"> </td>
					</tr>
					<tr>
						<td><b>Amount</b> </td><td id="o_amount"></td>
					</tr>
					<tr>
						<td><b>Commission</b> </td><td id="o_commission"></td>
					</tr>
					<tr>
						<td><b>Net Amount</b></td> <td id="o_net_amount"></td>
					</tr>
				</table>
				<button type="button" class="btn btn-block btn-info" id="confirm_order">Confirm</button>
			</div>
		</div>
	</div>
</div>


</div>
</div>



<script src="/resources/js/jquery-2.2.3.min.js"></script>
<script src="/resources/js/bootstrap.min.js"></script>
<script src="/resources/js/app.min.js"></script>
<script src="/resources/js/Chart.min.js"></script>
<script src="/resources/js/dashboard2.js"></script>
</body>
</html>
