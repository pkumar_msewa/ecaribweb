<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage=""%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
 <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SendMoney Account</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <sec:csrfMetaTags/>
<link rel="stylesheet" href="/resources/css/cuscss.css">

<link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'
	type="image/png" />
  
<!-- Optional theme -->
<link rel="stylesheet" href="<c:url value='/resources/css/bootstrap-theme.min.css'/>" type='text/css'>
<link rel="stylesheet" href="<c:url value='/resources/css/bootstrap.min.css'/>" type='text/css'>
<script src="<c:url value='/resources/js/jquery.js'/>"></script>
<script src="<c:url value='/resources/js/bootstrap.js'/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/Agent/Agentdetail.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/header.js"/>"></script>
<!-- Latest compiled and minified CSS -->

  <link rel="stylesheet" href="/resources/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="/resources/css/Pe-icon-7-stroke.css">  
  <link rel="stylesheet" href="/resources/css/AdminLTE.min.css">
  <link rel="stylesheet" href="/resources/css/_all-skins.min.css">
  
<script
	src="${pageContext.request.contextPath}/resources/admin/js/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>  
<script src="https://harvesthq.github.io/chosen/chosen.jquery.js"></script> 
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<!-- <script type="text/javascript" href="/resources/js/AgentBankTransfer.js"></script> -->
	<style>
		.no-js #loader {
			display: none;
		}

		.js #loader {
			display: block;
			position: absolute;
			left: 100px;
			top: 0;
		}
		
		.se-pre-con {
			position: fixed;
			left: 0px;
			top: 0px;
			width: 100%;
			height: 100%;
			z-index: 9999;
			background: url(/images/pq_large.gif) center no-repeat #fff;
		}
		#paySuccessModal .modal-body h4 {
			font-size: 19px;
		}
	</style>
	<script src="<c:url value='/resources/js/modernizr.js'/>"></script>

	<script type="text/javascript">
		$(window).load(function() {
			$(".se-pre-con").fadeOut("slow");
		});
		$(".js-example-tags").select2({
			  tags: true
		});
	</script>
<%-- <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/AgentBankTransfer.js"></script> --%>
</head>
<script type="text/javascript">
$(document).ready(function(){
	
   	var spinnerUrl = "Please Wait <img src='/resources/images/spinner.gif'  height='20' width='20'>"
    var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");
    var csrfHeader = $("meta[name='_csrf_header']").attr("content");
    var csrfToken = $("meta[name='_csrf']").attr("content");
   	var hash_key = "hash";
    var default_hash = "123456";
    var headers = {};
    headers[hash_key] = default_hash;
    headers[csrfHeader] = csrfToken;
    var ifscPattern = "^[A-Za-z]{4}[a-zA-Z0-9]{7}$";
    var namePattern = /^[a-zA-Z-,]+(\s{0,1}[a-zA-Z-, ])*$/;
    /*Pattern for email*/
    var pattern = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2}|com|org|net|edu|gov|mil|biz|info|mobi|name|aero|asia|jobs|museum|co|in)"
	var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
	var email_pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
	var mobilePattern="^(?=(?:[7-9]){1})(?=[0-9]{10}).*";
	var specialChar = "[!@#$%&*()_+=|<>?{}\\[\\]~-]";
	var alphaNumeric = "[A-Za-z0-9]+";
	var onlyNumber = "[0-9]+";
	var onlyAlpha = "[A-Za-z]+";
	
 //   $(':input','#rform').attr("disabled",true);
      $("#infocheck").click(function(){
    	  console.log("In infocheck");
    	  	if($('input[type=checkbox]').is(':checked')) {
    		    $("#rform :input").prop("disabled", true);
       	  		$(this).prop('checked',true);
       	  		 $("#smm_account_holder_name").val($("#sName").val());
       	  		 $("#rlName").val($("#slName").val());
       	  		 $("#rEmailId").val($("#sEmailId").val());
       	  		 $("#rMobileNumber").val($("#sMobileNumber").val());
       	  		 
       	  		 $("#error_smm_account_holder_name").html("");
  		 		 $("#error_rEmailId").html("");
  		 		 $("#error_rMobileNumber").html("");
  		 		 $("#error_rlName").html("");
   	   		} 
    	    else {
    	    	$("#rform :input").prop("disabled", false);
       		    $(this).prop('checked',false);
       		 	$("#smm_account_holder_name").html("");
   	  		 	$("#rEmailId").html("");
   	  		 	$("#rMobileNumber").html("");
   	  			$("#error_smm_account_holder_name").html("");
	  		 	$("#error_rEmailId").html("");
	  		 	$("#error_rMobileNumber").html("");
	  		 	$("#error_rlName").html("");
   	   		}
      });
 	  
      var bankRespCode = $("#bankRespCode").val();
      var bankResp = $("#bankResp").val();
      
      console.log("bankRespCode=="+bankRespCode);
      console.log("bankResp=="+bankResp);
      
      if(bankRespCode.includes("S00")){
    	     $("#sName").html(" ");
    		 $("#slName").html(" ");
    		 $("#sEmailId").html(" ");
    		 $("#sMobileNumber").html(" ");
    		 $("#idProofNo").html(" ");
    		 $("#idProofImage").html(" ");
    		 $("#imgs").html(" ");
    		 $("#idProofBImage").html(" ");
    		 $("#backImg").html(" ");
    		 $("#description").html(" ");
    		 
    		$("#smm_account_holder_name").html(" ");
    		$("#rlName").html(" ");
    		$("#rEmailId").html(" ");
    		$("#rMobileNumber").html(" ");
    		 
    		$("#smm_ifsc_code").html(" ");
    		$("#smm_account_number").html(" ");
    		$("#confirm_account_number").html(" ");
    		$("#smm_bank_amount").html(" ");
    	     
    	    $("#pay_loader").modal("hide");
			$("#smm_submit").removeAttr("disabled","disabled");
			$("#smm_submit").html("Send");
			$("#paySuccessModal").modal("show");
			$("#bank_success_msg").html(bankResp);
      }
      else if(bankRespCode.includes("F03"))
      {
    	  $("#pay_loader").modal("hide");
    	  $("#errorMessage").modal("show");
		  $("#error_message").html(bankResp);
		  window.location.href='${pageContext.request.contextPath}/Home'; 
      }
      else if(bankRespCode.includes("F00")){
		  $("#pay_loader").modal("hide");
		  $("#errorMessage").modal("show");
		  $("#error_message").html(bankResp);
	  }
     
    	$("#smm_bankName").select2();
	 	console.log("In agent BanList");
		$.ajax({
			type : "POST",
			headers:headers,
			contentType : "application/json",
//			contentType: "application/x-www-form-urlencoded",
			url : "/Agent/BankList",
			dataType : 'json',
			data : JSON.stringify({
			}),
			success : function(response) {
			console.log("CODE=="+response.code);
			if(response.code.includes("S00")){
				for(var i=0;i<response.details2.length;i++){
					$('#smm_bankName').append('<option value="' + response.details2[i].bankName + '">' + response.details2[i].bankName + '</option>');
				  }
				}
			}
		});
		
		
		/* $('#smm_bankName').on('change', function () {
			if(bankName.includes("#")){
				console.log("error_smm_bankName onvlur");
				$('#error_smm_bankName').html("Select bank name");
 			}
	  	}); */
	  	
		/***********Sender Validation*******************/
		
		$("#sName").bind("keypress", function (event) {
 		            if (event.charCode!=0) {
 		                var regex = new RegExp("^[a-zA-Z]+$");
 		                var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
 		                if (!regex.test(key)) {
 		                    event.preventDefault();
 		                    return false;
 		                }
 		            }
 		   });
	  	$("#sName").on("blur", function(){
			var name = $("#sName").val();
			if(name.length<=0){
			    $("#error_sName").html("Enter name");
			}
			else{
				$("#error_sName").html("");
			}
			
		});
	  	$("#slName").bind("keypress", function (event) {
	            if (event.charCode!=0) {
	                var regex = new RegExp("^[a-zA-Z]+$");
	                var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
	                if (!regex.test(key)) {
	                    event.preventDefault();
	                    return false;
	                }
	            }
	   });
	   $("#slName").on("blur", function(){
			var name = $("#slName").val();
			if(name.length<=0){
	    		$("#error_slName").html("Enter last name");
			}
			else{
				$("#error_slName").html("");
			}
	
	 });
	  	
	  	$("#sEmailId").on("blur", function(){
			var emailAddress = $('#sEmailId').val();
			console.log("In emailAddress:");
			if(emailAddress.length<=0){
				$('#error_sEmailId').html("Enter email-Id");
			}
		    else if(!emailAddress.match(pattern)){
				console.log("In email pattern");
				$('#error_sEmailId').html("Enter valid email-Id");
			}
		    else if(!emailAddress.match(email_pattern)){
				console.log("In email pattern2");
				$('#error_sEmailId').html("Enter valid email-Id");
			}
			else if(!emailAddress.match(filter)){
				console.log("In email filter");
				$('#error_sEmailId').html("Please enter valid email-Id")
			} 
			else{
				$('#error_sEmailId').html(" ");
			}
		}); 
	  	
		$("#sMobileNumber").keyup(function(){
			this.value = this.value.replace(/[^0-9\.]/g,'');
		});
		$("#sMobileNumber").on("blur",function(){
			var mobileNo = $("#sMobileNumber").val();
			if(mobileNo.length<=0){
			   $("#error_sMobileNumber").html("Enter mobile number");
		    }
			else if(!mobileNo.match(mobilePattern)){
		    	$("#error_sMobileNumber").html("Enter valid mobile number");
		    }	
			else if(mobileNo.length!=10){
				$("#error_sMobileNumber").html("Enter valid mobile number");
			}
			else{
				$('#error_sMobileNumber').html(" ");
			}
		});
		
		$("#idProof").on("blur",function(){
			var idProof = $("#idProof").val();
			if(idProof.length<=0){
				valid=false;
				$("#error_idProof").html("Select idProof");
			}
			else if(idProof.includes("#")){
				valid=false;
				$("#error_idProof").html("Select idProof");
			}
			else{
				$('#error_idProof').html("");
			 }
		});
		
		$("#idProofA").on("blur",function(){
			var idProof = $("#idProofA").val();
			if(idProof.length<=0){
				valid=false;
				$("#error_idProofA").html("Select idProof");
			}
			else if(idProof.includes("#")){
				valid=false;
				$("#error_idProofA").html("Select idProof");
			}
			else{
				$('#error_idProofA').html("");
			 }
		});
		
		$("#idProofNo").on("blur",function(){
			var valid = true;
			var voterId_pattern = "@\b\d{5}-\d{7}-\d$";
			var aadharCard_pattern = /^\d{4}\s\d{4}\s\d{4}$/;
			var panCard_pattern = /^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/;
			var drivingLicence_pattern = /[0-7][0-9]{2}[\W\s-][0-9]{2}[\W\s-][0-9]{4}|[\s\W][0-7][0-9]{8}[\s\W]/;
		//	(input, @"\b\d{5}-\d{7}-\d$");
			var idProof = $("#idProof").val();
			if(idProof.length<=0){
				valid=false;
				$("#error_idProof").html("Select idProof");
			}
			else if(idProof.includes("#")){
				valid=false;
				$("#error_idProof").html("Select idProof");
			}
			else{
				$('#error_idProof').html("");
			 }
			
			var idProofNo = $("#idProofNo").val();
			if(idProofNo.length<=0){
				$("#error_idProofNo").html("Enter Id number");
			}
			else{
				$('#error_idProofNo').html("");
			 }
			
		 if(valid==true)
		 {
			if(idProof.includes("1")){
				if(!idProofNo.match(panCard_pattern)){
					$("#error_idProofNo").html("Enter valid Id number");
				}
			}
			else if(idProof.includes("2")){
				if(!idProofNo.match(aadharCard_pattern)){
					$("#error_idProofNo").html("Enter valid Id number");
				}
			}
		    else if(idProof.includes("3")){
				if(idProofNo.match(specialChar)){
					$("#error_idProofNo").html("Enter valid Id number");
				}
				else if(!idProofNo.match(alphaNumeric)){
					$("#error_idProofNo").html("Enter valid Id number");
				}
				else if(!idProofNo.match(onlyNumber)){
					$("#error_idProofNo").html("Enter valid Id number");
				}
				else if(!idProofNo.match(onlyAlpha)){
					$("#error_idProofNo").html("Enter valid Id number");
				}
				else if(idProofNo.length>16){
					$("#error_idProofNo").html("Enter valid Id number");
				}
				else{
					$("#error_idProofNo").html("");
				}
			} 
			else if(idProof.includes("4")){
				if(!idProofNo.match(drivingLicence_pattern)){
					$("#error_idProofNo").html("Enter valid Id number");
				}
			}
		 } 
			
		});
		
		$("#idProofImage").on("blur",function(){
			var idProofImage = $("#idProofImage").val();
			if(idProofImage.length<=0){
				$("#error_idProofImage").html("Upload Id Image");
		 	 }
			else{
				$("#error_idProofImage").html("");
			 }
		});
		
		$("#idProofBImage").on("blur",function(){
			var idProofBImage = $("#idProofBImage").val();
			if(idProofBImage.length<=0){
				$("#error_idProofBImage").html("Upload Image of address");
		 	 }
			else{
				$("#error_idProofBImage").html("");
			 }
		});
		
		
		/***********Receiver Validation*******************/
		
		$("#smm_account_holder_name").bind("keypress", function (event) {
	            if (event.charCode!=0) {
	                var regex = new RegExp("^[a-zA-Z]+$");
	                var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
	                if (!regex.test(key)) {
	                    event.preventDefault();
	                    return false;
	                }
	            }
	     });
		$("#smm_account_holder_name").on("blur", function(){
			var name = $("#smm_account_holder_name").val();
			if(name.length<=0){
			    $("#error_smm_account_holder_name").html("Enter name");
			}else{
				$("#error_smm_account_holder_name").html("");
			}
			
		});
		$("#rlName").bind("keypress", function (event) {
            if (event.charCode!=0) {
                var regex = new RegExp("^[a-zA-Z]+$");
                var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
                if (!regex.test(key)) {
                    event.preventDefault();
                    return false;
                }
            }
       });
		
	$("#rlName").on("blur", function(){
		var name = $("#rlName").val();
		if(name.length<=0){
		    $("#error_rlName").html("Enter last name");
		}else{
			$("#error_rlName").html("");
		}
	});
		
	  	$("#rEmailId").on("blur", function(){
			var emailAddress = $('#rEmailId').val();
			if(emailAddress.length<=0){
				$('#error_rEmailId').html("Enter email-Id");
			}
		    else if(!emailAddress.match(pattern)){
				$('#error_rEmailId').html("Enter valid email-Id");
			}
		    else if(!emailAddress.match(email_pattern)){
				$('#error_rEmailId').html("Enter valid email-Id");
			}
			else if(!emailAddress.match(filter)){
				$('#error_rEmailId').html("Please enter valid email-Id")
			} 
			else{
				$('#error_rEmailId').html(" ");
			}
		}); 
	  	
		$("#rMobileNumber").keyup(function(){
			this.value = this.value.replace(/[^0-9\.]/g,'');
		});
		$("#rMobileNumber").on("blur",function(){
			var mobileNo = $("#rMobileNumber").val();
			if(mobileNo.length<=0){
			   $("#error_rMobileNumber").html("Enter mobile number");
		    }
			else if(!mobileNo.match(mobilePattern)){
		    	$("#error_rMobileNumber").html("Enter valid mobile number");
		    }
			else if(mobileNo.length!=10){
				$("#error_rMobileNumber").html("Enter valid mobile number");
			}
			else{
				$('#error_rMobileNumber').html(" ");
			}
		});
		
		$("#smm_bankName").on("click", function(){
			
		 });
		
		
	 	$("#smm_ifsc_code").on("blur", function(){
			var ifsc_Code = $("#smm_ifsc_code").val();
			
			var ifsc_Code = $('#smm_ifsc_code').val();
			if(ifsc_Code.length<=0){
				$('#error_smm_ifsc_code').html("Enter bank IFSC code");
			}
			else if(!ifsc_Code.match(ifscPattern)){
				$('#error_smm_ifsc_code').html("Enter valid IFSC code");
			}
			else{
				$('#error_smm_ifsc_code').html(" ");
			}
		});
		
		$('#smm_account_number').keyup(function () { 
		    this.value = this.value.replace(/[^0-9\.]/g,'');
		});
		
		 $("#smm_account_number").on("blur", function(){
			var accountNumber = $('#smm_account_number').val();
			if(accountNumber.length<=0){
				$("#error_smm_account_number").html("Enter Account number");
			}
			else if(accountNumber.length>16){
				$("#error_smm_account_number").html("Enter valid Account number");
			}
			else{
				$('#error_smm_account_number').html(" ");
			}
		});
		 
		
		
		$('#confirm_account_number').keyup(function () { 
		    this.value = this.value.replace(/[^0-9\.]/g,'');
		});
		 $("#confirm_account_number").on("blur", function(){
				var accountNumber = $('#confirm_account_number').val();
				if(accountNumber.length<=0){
					$("#error_confirm_account_number").html("Enter Account number");
				}
				else if(accountNumber.length>16){
					$("#error_confirm_account_number").html("Enter valid Account number");
				}
				else{
					$('#error_confirm_account_number').html(" ");
				}
			});

			
			$('#smm_bank_amount').keyup(function () { 
			    this.value = this.value.replace(/[^0-9\.]/g,'');
			});
			
			$("#smm_bank_amount").on("blur",function(){
				 var amount = $("#smm_bank_amount").val();
				 if(amount.length<=0){
					 $("#error_smm_bank_amount").html("Enter amount");
				 }
				 else if(amount<500 || amount>5000){
					 $("#error_smm_bank_amount").html("Enter min 500 and max 5000 amount");
				 }
				 else{
					   $('#error_smm_bank_amount').html(" ");
					}
			});
			
			/* $('#bank_form').submit(function() {
				
			}); */
			
			
	

});
</script>

<script type="text/javascript">

function validateForm(){
	
	console.log("in bank_form");
	 var ifscPattern = "^[A-Za-z]{4}[a-zA-Z0-9]{7}$";
	 var namePattern = /^[a-zA-Z-,]+(\s{0,1}[a-zA-Z-, ])*$/;
	    /*Pattern for email*/
	 var pattern = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2}|com|org|net|edu|gov|mil|biz|info|mobi|name|aero|asia|jobs|museum|co|in)"
	 var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
	 var email_pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
	 var mobilePattern="^(?=(?:[7-9]){1})(?=[0-9]{10}).*";
	 var valid=true;
	 console.log("in validateForm");
	 var sName = $("#sName").val();
	 var slName = $("#slName").val();
	 var sEmailId = $("#sEmailId").val();
	 var sMobileNumber = $("#sMobileNumber").val();
	 var idProofNo = $("#idProofNo").val();
	 var idProofImage = $("#idProofImage").val();
	 var idProofBImage = $("#idProofBImage").val();
	 var description = $("#description").val();
	 console.log("idProofBImage="+idProofBImage);
	 var acoountName = $("#smm_account_holder_name").val();
	 var receiverLastName = $("#rlName").val();
	 var rEmailId = $("#rEmailId").val();
	 var rMobileNumber = $("#rMobileNumber").val();
	 
	 var bankName = $("#smm_bankName").val();
	 var ifscCode = $("#smm_ifsc_code").val();
	 var accountNumber = $("#smm_account_number").val();
	 var confAccountNum = $("#confirm_account_number").val();
	 var amount = $("#smm_bank_amount").val();
	 var bank="";
	 
	 $("#accountName").val(acoountName);
	 $("#receiverEmailId").val(rEmailId);
	 $("#receiverMobileNo").val(rMobileNumber);
	 $("#receiverLastName").val(receiverLastName);
	 
	 console.log("receiverLastName=="+$("#receiverLastName").html());
	 
	 $("#error_sName").html(" ");
	 $("#error_slName").html(" ");
	 $("#error_sEmailId").html(" ");
	 $("#error_sMobileNumber").html(" ");
	 $("#error_idProofNo").html(" ");
	 
	 $("#error_rEmailId").html(" ");
	 $("#error_rMobileNumber").html(" ");
	 
	 $("#error_smm_ifsc_code").html(" ");
	 $("#error_smm_account_holder_name").html(" ");
	 $("#error_rlName").html(" ");
	 $("#error_smm_account_number").html(" ");
	 $("#error_confirm_account_number").html(" ");
	 $("#error_smm_bank_amount").html(" ");
	 
	 
//	 var bankName = $("#smm_bankName option:selected").val();

  /***********Sender Validation*******************/
    if(sName.length<=0){
   	 valid=false;
   	 $("#error_sName").html("Enter name");
    }
    else if(!sName.match(namePattern)){
   	 $("#error_sName").html("Enetr valid name");
    }
      if(slName.length<=0){
      	 valid=false;
      	 $("#error_slName").html("Enter last name");
       }
       else if(!slName.match(namePattern)){
      	 $("#error_slName").html("Enetr valid last name");
       }
  
   if(sEmailId.length<=0){
   	valid=false;
   	$("#error_sEmailId").html("Enter emailId");
   }
   else if(!sEmailId.match(email_pattern)){
   	valid=false;
   	$("#error_sEmailId").html("Enter valid emailId");
   }
   else if(!sEmailId.match(filter)){
   	valid=false;
   	$("#error_sEmailId").html("Enter valid emailId");
   }
   else if(!sEmailId.match(pattern)){
   	valid=false;
   	$("#error_sEmailId").html("Enter valid emailId");
   }
   
   if(sMobileNumber.length<=0){
   	valid=false;
   	$("#error_sMobileNumber").html("Enter mobile number");
   }else if(!sMobileNumber.match(mobilePattern)){
   	valid=false;
   	$("#error_sMobileNumber").html("Enter valid mobile number");
   }
   
   if(idProofNo.length<=0){
   	valid = false;
   	$("#error_idProofNo").html("Enter Id Proof Number");
   }

   if(idProofImage.length<=0){
   	valid = false;
   	$("#error_idProofImage").html("Upload Id Proof Image");
   }
   if(idProofBImage.length<=0){
	   	valid = false;
	   	$("#error_idProofBImage").html("Upload Id Proof address Image");
   }
   
   
   var idProof = $("#idProof").val();
	if(idProof.length<=0){
		valid=false;
		$("#error_idProof").html("Select IdProof");
	}
	else if(idProof.includes("#")){
		valid=false;
		$("#error_idProof").html("Select IdProof");
	}
	else{
		$('#error_idProof').html("");
	 }
	
	    var idProofA = $("#idProofA").val();
		if(idProofA.length<=0){
			valid=false;
			$("#error_idProofA").html("Select IdProof");
		}
		else if(idProofA.includes("#")){
			valid=false;
			$("#error_idProofA").html("Select IdProof");
		}
		else{
			$('#error_idProofA').html("");
		 }
 /***********Receiver Validation*******************/
 
   if(acoountName.length<=0){
		 valid=false;
		 $("#error_smm_account_holder_name").html("Enter name");
	 }else if(!acoountName.match(namePattern)){
		 valid=false;
		 $("#error_smm_account_holder_name").html("Enter valid name");
	 }
 
   if(receiverLastName.length<=0){
		 valid=false;
		 $("#error_rlName").html("Enter last name");
	 }else if(!receiverLastName.match(namePattern)){
		 valid=false;
		 $("#error_rlName").html("Enter valid last name");
	 }
   
   if(rEmailId.length<=0){
   	valid=false;
   	$("#error_rEmailId").html("Enter emailId");
   }
   else if(!rEmailId.match(email_pattern)){
   	valid=false;
   	$("#error_rEmailId").html("Enter valid emailId");
   }
   else if(!rEmailId.match(filter)){
   	valid=false;
   	$("#error_rEmailId").html("Enter valid emailId");
   }
   else if(!rEmailId.match(pattern)){
   	valid=false;
   	$("#error_rEmailId").html("Enter valid emailId");
   }
   
   console.log("rMobileNumber=="+rMobileNumber);
   if(rMobileNumber.length<=0){
   	valid=false;
   	$("#error_rMobileNumber").html("Enter mobile number");
   }else if(!rMobileNumber.match(mobilePattern)){
   	valid=false;
   	$("#error_rMobileNumber").html("Enter valid mobile number");
   }
    
	 if(bankName.length<=0){
		 valid=false;
		 $("#error_smm_bankName").html("Select bankName");
	 }
    else if(bankName.includes("#")){
   	    valid=false;
   	    $("#error_smm_bankName").html("Select bankName");
	 }
	 
	 if(ifscCode.length<=0){
		 valid=false;
		 $("#error_smm_ifsc_code").html("Enter ifsc code");
	 }
	 else if(!ifscCode.match(ifscPattern)){
		 	valid=false;
			$('#error_smm_ifsc_code').html("Enter valid IFSC code");
	 }
	 
	 if(accountNumber.length<=0){
		 valid=false;
		 $("#error_smm_account_number").html("Enter account number");
	 }
	 if(confAccountNum.length<=0){
		 valid=false;
		 $("#error_confirm_account_number").html("Enter account number");
	 }
	 if(!(accountNumber==confAccountNum)){
		 valid=false;
		 $("#error_smm_account_number").html("Account number must be match");
	  }
	 
	 if(amount.length<=0){
		 valid=false;
		 $("#error_smm_bank_amount").html("Enter amount");
	 }
	 else if(amount<500 || amount>5000){
		 valid=false;
		 $("#error_smm_bank_amount").html("Enter min 500 and max 5000 amount");
	 }
	if (valid) {
		console.log("In pay_loader");
	   $("#pay_loader").modal({backdrop: false});
	}

	 return valid;
}
</script>

<script type="text/javascript">
function selectBank(){
	var bankName = $("#smm_bankName").val();
	if(bankName.length<=0){
	 	$("#error_smm_bankName").html("Select bankName");
 	}
	 else if(bankName.includes("#")){
	    $("#error_smm_bankName").html("Select bankName");
 	 }
	 else{
		 $("#error_smm_bankName").html("");
	 }
}
</script>

<script>
function imgchange(f) {
	
    var image = $('#idProofImage').val();
    if(image != null){
    	$("#error_idProofImage").html("");
    }
    console.log("In idProofImage 2333");
    var reader = new FileReader();
    reader.onload = function (e) {
        $('#imgs').attr('src',e.target.result);
    };
    reader.readAsDataURL(f.files[0]);   
 }
 
function backImgchange(f) {
	
    var image = $('#idProofBImage').val();
    if(image!= null){
    	$("#error_idProofBImage").html("");
    }
    console.log("In idProofBImage 2333");
    var reader = new FileReader();
    reader.onload = function (e) {
        $('#backImg').attr('src',e.target.result);
    };
    reader.readAsDataURL(f.files[0]);   
 }
</script>

<body class="hold-transition skin-blue sidebar-mini">

<!-- <div id="pay_loader" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<img src='/resources/images/spinner.gif' style='width: 10%;'>
	</div>
</div> -->

<!--end modal-->

<jsp:include page="/WEB-INF/jsp/Agent/Login/Header.jsp" />
	
<div class="wrapper">
<jsp:include page="/WEB-INF/jsp/Agent/Login/sidenav.jsp" />

<div class="content-wrapper">
<div class="container">
    <!-- Content Header (Page he ader) -->
     <input type="hidden" name="bankCode" id="bankCode"/>
     
     <input type="hidden" name="bankRespCode" value="${bankRespCode}" id="bankRespCode"/>
     <input type="hidden" name="bankResp" value="${bankResp}" id="bankResp"/>
     
     <div class="col-md-11"><br>
    	<div class="row">
    	 <form  action="<c:url value="/Agent/SendMoney/BankTransfer"/>" id="bank_form" enctype="multipart/form-data" onSubmit = "return validateForm()" method="post" >
            <div class="col-md-6" id="left">
              <div class="tab-content" style="background: white;">  
             <!-- 
                <span id="error_mobile_sm" class="label label-danger"></span>
                            <span id="success_mobile_sm" class="label label-success"></span> -->
                                                                             
                          <div id="bankTransfer" class="tab-pane fade in active"><br> 
                                        <h3 style="margin-top: auto; margin-left:25%;"><span style="border-bottom: solid #17bcc8;">Sender Info</span></h3><br>
                            
 								<div class="group_1">
    								<i class="" id="inputico"></i>
    								<input type="text" name="senderName" id="sName" placeholder="Enter First Name" maxlength="25"/>
    								<span class="highlight"></span><span class="bar"></span>
    								<p class="error" id="error_sName"></p>
    							</div>
    							<div class="group_1">
    								<i class="" id="inputico"></i>
    								<input type="text" name="senderLastName" id="slName" placeholder="Enter Last Name" maxlength="25"/>
    								<span class="highlight"></span><span class="bar"></span>
    								<p class="error" id="error_slName"></p>
    							</div>
    							<div class="group_1">
    								<i class="" id="inputico"></i>
    								<input type="text" name="senderEmailId" id="sEmailId" placeholder="Enter Email Id"/>
    								<span class="highlight"></span><span class="bar"></span>
    								<p class="error" id="error_sEmailId"></p>
    							</div>
    							
    							<div class="group_1">
    								<i class="" id="inputico"></i>
    								<input type="text" name="senderMobileNo" id="sMobileNumber" maxlength="10" placeholder="Enter Mobile Number"/>
    								<span class="highlight"></span><span class="bar"></span>
    								<p class="error" id="error_sMobileNumber"></p>
    							</div>
    							
    							<div class="group_1" >
    								<select id="idProof" name="idProof" class="form-control" style="width:360px;height:30px; margin-left:12px;">
    									<option value="#">Select Government ID Card</option>
    									<option value="1">PAN card</option>
    									<option value="2">Aadhaar card </option>
    									<option value="3">Voter Id card</option>
    									<option value="4">Driving licence</option>
    								</select>
    								<p class="error" id="error_idProof"></p>
    							</div>
    							
    							<div class="group_1">
    								<i class="" id="inputico"></i>
    								<input type="text" name="idProofNo" id="idProofNo" placeholder="Enter ID Proof Number"/>
    								<span class="highlight"></span><span class="bar"></span>
    								<p class="error" id="error_idProofNo"></p>
    							</div>
    							
    							<div class="group_1">
    								<i class="" id="inputico"></i>
    								<label>Upload IdProof front side image</label>
    								<input type="file" name="idProofImage" accept="image/*" onchange="imgchange(this)" id="idProofImage" placeholder="Enter ID Proof Image"/>
    								<img id="imgs" style="margin-left:0px;" width="81%" height="150px"></img>
    								<p class="error" id="error_idProofImage"></p>
    							</div>
    							
    							<div class="group_1" >
    								<select id="idProofA" name="idProofA" class="form-control" style="width:360px;height:30px; margin-left:12px;">
    									<option value="#">Select ID Card for address proof image</option>
    									<option value="2">Aadhaar card </option>
    									<option value="3">Voter Id card</option>
    									<option value="4">Driving licence</option>
    								</select>
    								<p class="error" id="error_idProofA"></p>
    							</div>
    							
    							<div class="group_1">
    								<i class="" id="inputico"></i>
    								<label>Upload IdProof back side image with address</label>
    								<input type="file" name="idProofBImage" accept="image/*" onchange="backImgchange(this)" id="idProofBImage" placeholder="Enter ID Proof Back side Image"/>
    								<img id="backImg" style="margin-left:0px;" width="81%" height="150px"></img>
    								<p class="error" id="error_idProofBImage"></p>
    							</div>
    							
    							<div class="group_1">
    								<i class="" id="inputico"></i>
    								<input type="text" name="description" id="description" placeholder="Enter Description"/>
    								<span class="highlight"></span><span class="bar"></span>
    								<p class="error" id="error_description"></p>
    							</div>
    							
    							<div class="group_1">
    							  <h5 style="margin-left: 80px;"><b>Same as receiver info</h5></b>
    							  <input type="checkbox" name="checkbox" id="infocheck" style="margin-left:40px;margin-top:-23px;"/>
    							</div>
                          <!--  </form> -->
                            <br>
                            </div><!--end Prepaid-->
                         </div><!---end tab-content-->
                         
                    </div><!----end col-md-6-->
                   
                  <div class="col-md-6" >
                   <div class="tab-content" style="background: white;">  
                                                                             
                          <div id="bankTransfer" class="tab-pane fade in active"><br> 
                                        <h3 style="margin-top: auto; margin-left:25%;"><span style="border-bottom: solid #17bcc8;">Receiver Info</span></h3><br>
 							
 							<input type="hidden" name="accountName" id="accountName"/>
 							<input type="hidden" name="receiverEmailId" id="receiverEmailId"/>
 							<input type="hidden" name="receiverMobileNo" id="receiverMobileNo"/>
 							<input type="hidden" name="receiverLastName" id="receiverLastName"/>
 							<div id="rform">
 								<div class="group_1">
    								<!-- <i class="material-icons" id="inputico">&#xe7fd;</i> -->
    								<input type="text" name="accName" id="smm_account_holder_name" style="margin-left: 11px;"  maxlength="25" placeholder="Enter Name"/>
    								<span class="highlight"></span><span class="bar"></span>
    								<p class="error" id="error_smm_account_holder_name"></p>
    							</div>
    							
 								<div class="group_1">
    								<i class="" id="inputico"></i>
    								<input type="text" name="receiverLName" id="rlName" placeholder="Enter Last Name" maxlength="25"/>
    								<span class="highlight"></span><span class="bar"></span>
    								<p class="error" id="error_rlName"></p>
    							</div>
    							
 								<div class="group_1">
    								<i class="" id="inputico"></i>
    								<input type="text" name="receEId" id="rEmailId" placeholder="Enter Email Id"/>
    								<span class="highlight"></span><span class="bar"></span>
    								<p class="error" id="error_rEmailId"></p>
    							</div>
   
    							<div class="group_1">
    								<i class="" id="inputico"></i>
    								<input type="text" name="receMobNo" id="rMobileNumber" maxlength="10" placeholder="Enter Mobile Number"/>
    								<span class="highlight"></span><span class="bar"></span>
    								<p class="error" id="error_rMobileNumber"></p>
    							</div>
    						 </div>
    					  <!--  <form> -->
                                <div  class="group_1" style="margin-left:30px;">
                                   <!-- <i class="material-icons" id="inputico">&#xe84f;</i> -->
                                   <select id="smm_bankName"  name="bankName" onChange="selectBank();" class="form-control" style="width:363px;height:30px; margin-left:40px;" >
                                   		<option value="#">Select bank</option>
                                   </select>
                                    <span class="highlight"></span> <span class="bar"></span>
                                    <p class="error" id="error_smm_bankName"></p>
                                </div>
    							
    							<div class="group_1">
    								<i class="" id="inputico"></i>
    								<input type="text" name="ifscCode" id="smm_ifsc_code" placeholder="IFSC Code"/>
    								<span class="highlight"></span><span class="bar"></span>
    								<p class="error" id="error_smm_ifsc_code"></p>
    							</div>
    						
    							<div class="group_1">
    								<i class="" id="inputico"></i>
    								<input type="text" name="accountNumber" id="smm_account_number" maxlength="16" placeholder="Account number"/>
    								<span class="highlight"></span><span class="bar"></span>
    								<p class="error" id="error_smm_account_number"></p>
    							</div>
    							
    							<div class="group_1">
    								<i class="" id="inputico"></i>
    								<input type="text" name="confirm_accountNumber" id="confirm_account_number" maxlength="16" placeholder="Confirm Account number"/>
    								<span class="highlight"></span><span class="bar"></span>
    								<p class="error" id="error_confirm_account_number"></p>
    							</div>
    							
                                <div class="group_1">
                                   <!--  <i class="fa fa-inr rup" id="inputico"></i> -->
                                    <i class="" id="inputico"></i>
                                    <sec:csrfInput/>
                                    <input type="number" name="amount" min="10" id="smm_bank_amount" placeholder="Amount"> <span
                                        class="highlight"></span> <span class="bar"></span>
                                    <p id="error_smm_bank_amount" class="error"></p>
                                </div>
    
                                <div class="group_1">
                                  <center><button type="submit" class="btn" 
                                style="width:20%; margin-left: -70px;background: #ff0000; margin-top: 10px; color: #FFFFFF;">Send</button></center>
                                </div>
                           <!--  </form><br> -->
                            </div><!--end Prepaid-->
                         </div><!---end tab-content-->
                    </div><!----end col-md-6-->
                    <br>
                 </form>
   			</div>
   			
               <jsp:include page="/WEB-INF/jsp/Agent/Login/Footer.jsp"/>
    	</div><!--end main-col-8-->
        
      </div>
</div>
</div>

<!-- Modal after error verification -->
<div id="errorMessage" role="dialog" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				<center id="error_message" class="alert alert-danger"></center>
			</div>
		</div>
	</div>
</div>

<div id="pay_loader" role="dialog" class="modal fade">
	<div class="modal-dialog">
			<div class="modal-body">
				<center style="width:20%;margin-top: 150px; margin-left:300px;">
					<span><h4 style="color:green;font-weight:1000;">Please Wait for 2 Min...</h4><img src="/resources/images/spinner.gif"></span>
				</center>
			</div>
	</div>
</div>

<!--  <div style='display: none;' id='pay_loader'>
		
				</div> -->

<div class="modal fade" id="paySuccessModal" role="dialog" hidden="hidden">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content" style="margin-top: 40%;margin-left:100px;">

				<div class="modal-body">
					<center>
						<img
							src="${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/source-(tick).gif"
							style="width: 140px; margin-top: -91px;    margin-bottom: 10px;">
						<div
							style="background: white; margin-top: -20px; padding: 2.5% 2%;">
							<h4>
								<b id="bank_success_msg"> <br>
								<small></b></small>
							</h4>
						</div>
					</center>
				</div>

			</div>

		</div>
	</div>

<!-- <script src="/resources/js/jquery-2.2.3.min.js"></script> -->
<script src="/resources/js/bootstrap.min.js"></script>
<script src="/resources/js/app.min.js"></script>
<script src="/resources/js/Chart.min.js"></script>
<script src="/resources/js/dashboard2.js"></script>
</body>
</html>
