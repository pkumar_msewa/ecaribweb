<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*"  isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<link rel="stylesheet"
	href="<c:url value='/resources/css/font-awesome.min.css'/>" type='text/css'>
<link href='<c:url value='/resources/css/font-family.css'/>'
	  rel='stylesheet' type='text/css'>

	<!-- HEADER -->
	<header class="navbar clearfix" id="header">
		<div class="container-fluid">
				<div class="navbar-brand">
					<!-- COMPANY LOGO -->
					<a href="/User/Home">
						<img src="/resources/images/new_img/logo/logo.png" alt="VPayQwik Logo"  style="margin-left: -30px;" class="img-responsive">
					</a>					
				</div>
				<ul class="nav navbar-nav pull-right" style="margin-top: 12px;">				
					<!-- BEGIN USER LOGIN DROPDOWN -->
					<li class="user " id="" style="margin-left:100px;">
						<a href="<c:url value="/Agent/LoadMoney/Process"/>" class="">
							<span class="fa fa-inr" style="color: #fff; font-weight: 700;"></span>
							<span class="username" id="account_balance" style="font-size: 15px; font-weight: 700; color: #fff;">${balance}</span>
							<img  class="small_pic" src="<c:url value="/resources/images/main/wallet.png"/>" class="img-circle"
							style="width: 33px; margin-top: -7px;"/>
						</a>
					</li>
					<li class="dropdown user pull-right" id="header-user" style=>
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<img  class="small_pic" src="<c:url value="/resources/images/sample.jpg"/>" class="img-circle"
							style="" width="40" height="40"/>
							<span class="username" id="first_name_user"></span>
							<i class="fa fa-angle-down"></i>
						</a>
						<ul class="dropdown-menu">
							<li><a href="<c:url value="/User/Home"/>"><i class="fa fa-user"></i> Dashboard</a></li>
							<!-- <li><a data-toggle="modal" data-target="#invite-frnds"><i class="fa fa-users"></i> Invite Friends</a></li> -->
							<%-- <li><a href="<c:url value="/User/Settings"/>"><i class="fa fa-cog"></i> Account Settings</a></li> --%>
							<!-- <li><a href="#"><i class="fa fa-eye"></i> Privacy Settings</a></li> -->
							<li><a href="${pageContext.request.contextPath}/Agent/Travel/Flight/MyTickets"><i class="fa fa-users"></i> Travel Tickets</a></li>
							<li><a href="<c:url value="/User/Home"/>"><i class="fa fa-power-off"></i> Log Out</a></li>
						</ul>
					</li>
					<!-- END USER LOGIN DROPDOWN -->
				</ul>
				<!-- END TOP NAVIGATION MENU -->
		</div>
	</header>
	<!--/HEADER -->
	
	<!-- FONTS -->
 <!-- <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'> -->

                    <center>
						<!-- Modal for Invite Friends -->
						<div class="modal fade" id="invite-frnds" role="dialog">
							<div class="modal-dialog">

								<!-- Modal content-->
								<div class="modal-content rchrg_modal">
									<div class="modal-header dth_head">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<center>
											<h2 class="modal-title">
												<b>Invite Friends</b>
											</h2>
										</center>
									</div>
									<div class="modal-body">
										<div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<br> <br>
												<div class="form-group" style="margin-top: -46px;">
												<p id="error_ife" class="label label-danger" ></p>
				                                <p id="success_ife" class="label label-success"></p>
													<div class="col-md-12">
														<form method="post" action="#">
															<div class="group_1">
																<input type="text" name="mobileNo" id="ife_mobileNo" autocomplete="off" maxlength="10"
																	class="form-control numeric" style="margin-right: 50px;" maxlength="10" required="required" placeholder="Mobile Number">
															<p id="error_ife_mobileNo" class="error" style="color: red"></p>
															</div>

															<div class="group_1">
																<input type="text" name="receiversName" id="ife_receiver"
									                         required="required" class="form-control" placeholder="Invitee's Name">
															<p id="error_ife_receivername" class="error" style="color: red"></p>
															</div>
															<button type="button" class="btn" id="ife_submit"
																style="background: #8cc84e; color: #FFFFFF;">Invite</button>
														</form>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

							</div>

						</div>
						<!-- Modal for Invite Friends -->
					</center>