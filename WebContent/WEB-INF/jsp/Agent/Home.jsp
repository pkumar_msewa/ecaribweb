
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage=""%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<sec:csrfMetaTags />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="google-site-verification"
	content="Gqjxvc7DzSWRpY3nEn9I-hXs1znjtPi7prk3P7PF7Fo" />
<title>Welcome to eCarib</title>

<link rel="stylesheet" href="<c:url value="/resources/css/font-awesome.min.css"/>">

<link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'
	type="image/png" />

<script src="<c:url value='/resources/js/jquery.js'/>"></script>
<link rel="stylesheet"
	href="<c:url value="/resources/css/datepicker.css"/>">
<script src="<c:url value="/resources/js/datepicker.js"/>"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/AgentLogin.js"></script>
<script>
	$(function() {
		$("#dob").datepicker({
			format : "yyyy-mm-dd"
		});
	});
</script>
<!-- Optional theme -->
<link rel="stylesheet"
	href="<c:url value='/resources/css/bootstrap-theme.min.css'/>"
	type='text/css'>
<link href="<c:url value="/resources/css/style_main.css"/>"
	rel='stylesheet' type='text/css'>

<link rel="stylesheet"
	href="<c:url value='/resources/css/bootstrap.min.css'/>"
	type='text/css'>
<%--
	<link href="<c:url value="/resources/css/css_style.css"/>"
		  rel='stylesheet' type='text/css'>
--%>
<script src="<c:url value='/resources/js/bootstrap.js'/>"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/header.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/AgentToUserSignup.js"></script>

<style>
.no-js #loader {
	display: none;
}

.js #loader {
	display: block;
	position: absolute;
	left: 100px;
	top: 0;
}

.se-pre-con {
	position: fixed;
	left: 0px;
	top: 0px;
	width: 100%;
	height: 100%;
	z-index: 9999;
	background: url(/images/pq_large.gif) center no-repeat #fff;
}

.short {
	font-weight: normal;
	color: #FF0000;
	font-size: 13px;
}

.weak {
	font-weight: normal;
	color: orange;
	font-size: 13px;
}

.good {
	font-weight: normal;
	color: #2D98F3;
	font-size: 13px;
}

.strong {
	font-weight: normal;
	color: limegreen;
	font-size: 13px;
}

#login input {
	width: 260px !important;
}

#login .bar {
	width: 260px !important;
}

#login .group {
	margin-left: 40px;
}

@media screen and (max-width: 320px) {
	#login .group {
		margin-left: 0px !important;
	}
}
</style>
<script src="<c:url value='/resources/js/modernizr.js'/>"></script>

<script type="text/javascript">
	$(window).load(function() {
		$(".se-pre-con").fadeOut("slow");
		//getQuestions();
	});
</script>

<script>
	$(document).ready();
	function getQuestions() {

		$.ajax({
			url : "/Api/v1/User/Website/en/WebRegistration/Questions",
			type : "GET",
			datatype : 'json',
			contentType : "application/json",
			success : function(response) {
				var a = response + " ";
				var y = new Array();
				y = a.split(",");
				document.getElementById("secquestions").innerHTML = "";
				var option = document.createElement("option");
				var select = document.getElementById("secquestions");
				var j;
				option.text = "Select your Security Question";
				option.value = "#";
				select.add(option);
				for (j = 0; j < y.length; j++) {
					var z = Array();
					var option = document.createElement("option");
					z = y[j].split("#");
					option.text = z[1];
					option.value = z[0];
					select.add(option);
				}
			}
		});
	}
</script>
 <script type="text/javascript">
function fgPass(){
		$("#forgot_password_agent").modal("show");
		$("#forgot_password_agent").modal({backdrop: false});
	}
</script>
</head>
<body style="overflow-x: hidden;">

 			<c:if test="${error ne null}">
				<div class="alert alert-danger col-md-12" id="alertdanger">
					<center>
						<c:out value="${error}" escapeXml="true" default="" />
					</center>
				</div>
			</c:if>
	
	<!-- Modal -->
	<div id="myModal" class="modal fade" role="dialog"
		style="background: transparent; border-color: transparent">
		<div class="modal-dialog modal-lg">

			<!-- Modal content-->
			<!-- <a
				href="https://play.google.com/store/apps/details?id=in.msewa.vpayqwik&hl=en"> -->
			<img src="/resources/images/banner.jpg" class="img-responsive"
				width="100%" usemap="#banner" alt="">

			<map name="banner">
				<a
					href="https://itunes.apple.com/us/app/vpayqwik/id1207326500?ls=1&mt=8"
					target="_blank">
					<area shape="rect" coords="55,412,233,464">
				</a>
				<a
					href="https://play.google.com/store/apps/details?id=in.msewa.vpayqwik&amp;hl=en"
					target="_blank">
					<area shape="rect" coords="248,413,425,464">
				</a>
			</map>
		</div>
	</div>
	
	<!--end modal-->

	<div>
		<div class="se-pre-con"></div>
		<!----slider----->
		<!--<div id="myCarousel" class="carousel slide"
			style="margin-top: -14px; position: fixed; z-index: -1; height: 100%; width: 100%;">
			<!-- Indicators -->

		<!-- Wrapper for Slides -->
		<!--			<div class="carousel-inner">
				<div class="item active">
					<!-- Set the first background image using inline CSS below. -->
		<!--				<div class="fill"
						style="background-image: url('/resources/images/bg_vpayqwik.jpg');"></div>
				</div>
			</div>

			<!-- Controls -->
	</div>

	<!-- /.container -->

	<!-- jQuery -->
	<script src="js/jquery.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="js/bootstrap.min.js"></script>

	<!-- Script to Activate the Carousel -->
	<script>
		$('.carousel').carousel({
			interval : 5000
		//changes the speed
		})
	</script>
	<!-------end slider---------->


	<div class="topline"></div>
	<img src="/resources/images/bg.jpg" class="img-responsive" style="position: absolute;">
    
<c:if test="${error ne null}">
			<div class="alert alert-danger col-md-12">
				<center>
					<c:out value="${error}" escapeXml="true" default="" />
				</center>
			</div>
</c:if>
		<c:if test="${msg ne null}">
			<div class="alert alert-success col-md-12">
				<center>
					<c:out value="${msg}" escapeXml="true" default="" />
				</center>
			</div>
		</c:if>
	<div class="container">
		
		<div class="col-md-4 col-md-offset-7"
			style="margin-top: 10%;margin-left: -50px;padding: 20px 20px; background: white;">
			
			<center>
				<a href="#"><img src="/resources/images/vijayalogo.png" alt=""
					style="width: 250px; margin-bottom: 20px;"></a>
			</center>

			<form id="login" action="<c:url value='/Agent/Login'/>" method="post">
				<div class="group">
					<input type="text" name="username" autocomplete="off"
						placeholder="Mobile No" maxlength="10" required> <span
						class="highlight"></span> <span class="bar"></span>
				</div>

				<div class="group">
					<input type="password" name="password" id="password_login"
						placeholder="Password" autocomplete="off" minlength="6"
						maxlength="6" required> <span class="highlight"></span> <span
						class="bar"></span> <i class="fa fa-eye-slash" id="login_pwd_eye"
						style="float: right; margin-top: -25px;"></i>
					<sec:csrfInput />
					<!-- <a href="#" data-toggle="modal" data-target="#forgotPassword"
						id="forgot_password_modal">Forgot Password? </a> -->
						<p class="error" id="agent_error_login_password" class="error" style="color:red;"></p>
         		<a href="#" class="agileits-forgot" data-toggle="modal" onclick = "fgPass()">Forgot Password</a>
				</div>
				<center>
					<button type="submit" class="btn"
						style="border-radius: 0; width: 50%; margin-top: 19px; margin-left: -23px;">
						Login <i class="fa fa-sign-in fa-1x"></i>
					</button>
				</center>
				<br>
				<br>
			</form>
		</div>
	</div>

	<!-----modal after Registration Successful -->
	<div id="regMessage" class="modal fade" role="dialog">
		<div class="modal-dialog modal-sm">
			<div class="modal-content text-center">
				<button type="button" data-dismiss="modal" class="close">&times;</button>
				<div class="icon">
					<span class="fa fa-unlock-alt fa-2x" aria-hidden="true"></span>
				</div>
				<h4>OTP Verification</h4>
				<hr style="margin-top: 20px;">

				<div class="group_1">
					<input type="text" name="key" id="verify_reg_otp_key"
						required="required"> <span class="highlight"></span> <label>OTP</label>
				</div>
				<div class="group_1">
					<input type="hidden" name="mobileNumber"
						class="form-control input-sm" id="reg_otp_username"
						required="required" />
				</div>
				<div class="col-md-6"
					style="float: none; margin-left: auto; margin-right: auto;">
					<button class="btn btn-md btn-block btncu"
						style="margin-bottom: 5px" id="register_verify_mobile">Verify
						Mobile</button>
				</div>
				<br>
				<div class="group_1">
					<div style="z-index: 99; position: absolute; margin-left: 78%;">
						<a href="#" class="captcha_link"><span
							class="glyphicon glyphicon-refresh" aria-hidden="true"
							style="margin-left: 80%;"></span></a>
					</div>
					<br> <img src="<c:url value="/Captcha"/>"
						class="captcha_image" height="50" />
				</div>
				<div class="group_1">
					<input id="g-recaptcha-response-1" type="text" required
						style="margin-top: -25px;" />
					<p class="error" id="error_captcha1" class="error"></p>
					<!-- <label>Enter text shown in image</label> -->
				</div>
				<div class="group_1">
					<button class="btn btn-md btncu" id="register_resend_otp"
						style="margin-bottom: 5px; margin-left: -20px;">Resend
						OTP</button>
				</div>

				<div class="modal-footer">
					<div class="alert alert-success" id="regMessage_success"></div>
				</div>
			</div>
		</div>
	</div>
	<!-- Modal after successful verification -->
	<div id="verifiedMessage" role="dialog" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h5>Verification Successful</h5>
				</div>
				<div class="modal-body">
					<center id="success_verification_message"
						class="alert alert-success"></center>
				</div>
			</div>
		</div>
	</div>

	<!-- Modal after error verification -->
	<div id="errorMessage" role="dialog" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<center id="error_message" class="alert alert-danger"></center>
				</div>
			</div>
		</div>
	</div>


	<!-- Modal for forgot password -->
	<div class="modal fade" role="dialog" id="forgotPassword">
		<div class="modal-dialog modal-sm">
			<div class="modal-content text-center" style="width: 120%;">
				<button type="button" data-dismiss="modal" class="close">&times;</button>
				<div class="icon">
					<i class="fa fa-unlock-alt fa-2x" aria-hidden="true"></i>
				</div>
				<h4>Forgot Password</h4>
				<hr style="margin-top: 20px;">

				<form style="margin-left: 30px;">
					<div class="group_1">
						<input type="text" name="username" id="fp_username"
							placeholder="Mobile Number" required="required" autofocus>
						<p class="error" id="fp_error_username"></p>
					</div>
					<div style="z-index: 99; position: absolute; margin-left: 78%;">
						<a class="captcha_link"><span
							class="glyphicon glyphicon-refresh" aria-hidden="true"
							style="margin-left: 80%;"></span></a>
					</div>
					<br> <img src="/Captcha" height="80" class="captcha_image" />
					<br />
					<div class="group_1">
						<input id="g-recaptcha-response-2"
							placeholder="Enter text shown in image" type="text" required />
						<p class="error" id="error_captcha2" class="error"></p>
					</div>
				</form>
				<div class="col-md-6"
					style="float: none; margin-left: auto; margin-right: auto;">
					<button class="btn btn-sm btn-block btncu" type="button"
						style="margin-bottom: 20px; margin-top: 10px;"
						id="forgot_password_request">Continue</button>
				</div>

			</div>
		</div>
	</div>


	<div id="successNotification" role="dialog" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h5></h5>
				</div>
				<div class="modal-body">
					<center id="success_alert" class="alert alert-success"></center>
				</div>
			</div>
		</div>
	</div>



	<!-- modal OTP for forget password -->
	<div class="modal fade" role="dialog" id="fpOTP">
		<div class="modal-dialog modal-sm">
			<div class="modal-content text-center">
				<button type="button" data-dismiss="modal" class="close">&times;</button>
				<div class="icon">
					<i class="fa fa-unlock-alt fa-2x" aria-hidden="true"></i>
				</div>
				<h4>Change Password</h4>
				<hr style="margin-top: 20px;">
				<input type="hidden" name="mobileNumber" id="fpusername_forgot"
					class="form-control input-sm" value="" required />

				<div class="group_1" style="margin-left: 20px;">
					<input type="text" name=key id="fpOTP_key" placeholder="OTP"
						required />
				</div>
				<div class="group_1" style="margin-left: 20px;">
					<input type="password" name=key id="fpnewPassword_key"
						minlength="6" maxlength="6" placeholder="New Password" required />
				</div>
				<div class="group_1" style="margin-left: 20px;">
					<input type="password" name=key id="fpconfirmPassword_key"
						minlength="6" maxlength="6" placeholder="Confirm Password"
						required />
				</div>
				<br />
				<div class="group_1">
					<center>
						<button class="btn  btn-md  btncu" style="float: left"
							type="button" id="process_forgot_password_request">Continue</button>
					</center>
					<br>
				</div>
				<br>
				<div style="z-index: 99; position: absolute; margin-left: 78%;">
					<a class="captcha_link"><span
						class="glyphicon glyphicon-refresh" aria-hidden="true"
						style="margin-left: 80%;"></span></a>
				</div>
				<br> <img src="/Captcha" height="50" class="captcha_image" />
				<br />

				<div class="group_1" style="margin-left: 20px;">
					<input id="g-recaptcha-response-3" type="text"
						placeholder="Enter text shown in image" required />
					<p class="error" id="error_captcha3" class="error"></p>
				</div>

				<div class="form-group col-md-6" style="float: none;">
					<button type="button" class="btn  btn-md btn-block btncu"
						id="forgot_password_resend_otp" style="margin-bottom: 5px">Resend
						OTP</button>
				</div>
			</div>
			<div class="modal-footer">
				<center id="fpOTP_message"></center>
			</div>
		</div>
	</div>
	</div>


<div id="forgot_password_agent" role="dialog" class="modal fade">
		<div class="modal-dialog modal-sm">
			<center>
				<div class="modal-content" style="margin-top: 100px;">
					
					<img src="${pageContext.request.contextPath}/resources/images/agent.png" style="width: 30%;    margin-top: -32px;">
					<div class="modal-body" style="margin-top: -20px;">
						<%-- <center id="success_alert" class="alert alert-success"></center> --%>
						<h5><b>Please contact to customer care Number (8025535857)</b></h5>
					</div>
				</div>
			</center>
		</div>
</div>

	<div class="navbar navbar-fixed-bottom" id="footer">
		<div class="container-fluid">
			<a href="<c:url value='/AboutUs'/>">About Us</a> <a
				href="<c:url value='/PartnersWithUs'/>">Partnerwith us</a> <a
				href="<c:url value='/Campaign'/>"> Campaign</a> <a
				href="<c:url value='/Terms&Conditions'/>">Terms & Conditions</a> <a
				href="<c:url value='/Grievance'/>">Grievancepolicy</a>
			<!-- <div class="span pull-left">
				<a href="#"><img src="/resources/images/vijaya_logo.png"
					style="height: 30px;"></a> -->
			</div>
			<div class="span pull-right">
				<p>&copy; Copyright MSewa Software Pvt. Ltd.</p>
			</div>
		</div>
</body>
</html>