<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">

<title>Vijaya | Bulk Upload</title>
<link rel="icon" type="image/ico"
	href="${pageContext.request.contextPath}/resources/bescom/assets/images/favicon.ico" />
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- vendor css files -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/bescom/assets/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bescom/assets/css/animsition.min.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/bescom/assets/css/morris.css">
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/bescom/assets/css/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/bescom/assets/dropify/css/dropify.min.css">
<!-- project main css files -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/bescom/assets/css/main.css">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<style>
table.table-bordered.dataTable tbody th, table.table-bordered.dataTable tbody td
	{
	font-size: 12px;
}
</style>

<style>
.error {
	font-size: 12px;
	color: red;
	margin-top: 0;
	margin-bottom: 0;
 }
.loader {
		position: fixed;
		left: 0px;
		top: 0px;
		width: 100%;
		height: 100%;
		z-index: 9999;
		background: url(/resources/bescom/img/vijayabank.gif) center no-repeat #fff;
		}
	</style>
	<script src="<c:url value='/resources/js/modernizr.js'/>"></script>

	<script type="text/javascript">
	$(document).ready(function() {
		$(".loader").fadeOut("slow");
		});
	</script>
</head>
<body id="falcon" class="main_Wrapper">
<div class="loader"></div>
	<div id="wrap" class="animsition">
		<!-- HEADER Content -->
		<jsp:include page="/WEB-INF/jsp/Vijaya/Header.jsp" />
		<jsp:include page="/WEB-INF/jsp/Vijaya/LeftMenu.jsp" />
		<input type="hidden" name="successMsg" id="successMsg" value="${msg}">
		<input type="hidden" name="errorMsg" id="errorMsg" value="${error}">
		<!-- CONTENT -->
		<section id="content">
			<div class="page dashboard-page">
				<!-- bradcome -->
				<div class="b-b mb-20">
					<div class="row">
						<div class="col-sm-6 col-xs-12">
							<h1 class="h3 m-0">Bulk Report</h1>
							<!-- <small class="text-muted">Welcome to Falcon application</small> -->
						</div>
					</div>
				</div>
                 <b>${message}</b>
				<form action="<c:url value="/Vijaya/Merchant/BulkUpload"/>"
					method="Post" enctype="multipart/form-data">
					<div class="row clearfix">
						<div class="col-md-6">
							<input type="file" name="file" id="file" class="dropify">
						</div>
						<div class="col-md-6">
							<input type="file" name="otherFile" id="other_file"
								class="dropify">
						</div>
					</div>
					<div class="row clearfix">
						<div class="col-md-12">
							<center>
								<button class="btn btn-raised btn-info">Submit</button>
							</center>
						</div>
					</div>
				</form>
			</div>
			<!-- FOOTER -->
			<jsp:include page="/WEB-INF/jsp/Vijaya/Footer.jsp" />
			<!-- FOOTER -->
		</section>

	</div>
	<!-- Vendor JavaScripts -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/bescom/assets/bootstrap/js/bootstrap.min.js"></script>
	<!-- <script src="assets/js/libscripts.bundle.js"></script> -->
	<script
		src="${pageContext.request.contextPath}/resources/bescom/assets/js/vendorscripts.bundle.js"></script>

	<!--/ vendor javascripts -->
	<script
		src="${pageContext.request.contextPath}/resources/bescom/assets/js/flotscripts.bundle.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/bescom/assets/js/d3cripts.bundle.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/bescom/assets/js/sparkline.bundle.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/bescom/assets/js/raphael.bundle.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/bescom/assets/js/morris.bundle.js"></script>
	<!-- <script src="assets/js/loadercripts.bundle.js"></script> -->

	<!-- page Js -->
	<script
		src="${pageContext.request.contextPath}/resources/bescom/assets/js/mainscripts.bundle.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/bescom/assets/js/index.js"></script>

	<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>

	<script
		src="${pageContext.request.contextPath}/resources/bescom/assets/dropify/js/dropify.min.js"></script>

	<script>
		$(function() {
			$('.dropify').dropify();
		});
	</script>
	
	<script type="text/javascript">
	$(document).ready(function() {
		/* $("#commonSpinner").modal("show");
		var timeout = setTimeout(function() {
				$("#commonSpinner").modal("hide");
			}, 2000); */
		
		console.log("Alert ID :: " + errorMsg);
		if (!errorMsg.length == 0) {
			$("#common_error_true").modal("show");
			$("#common_error_msg").html(errorMsg);
			var timeout = setTimeout(function() {
				$("#common_error_true").modal("hide");
				$("#errorMsg").val("");
			}, 3000);
		}

		var successMsg = document.getElementById("successMsg").value;
		console.log("successMsg :: " + successMsg);
		if (!successMsg.length == 0) {
			$("#common_success_true").modal("show");
			$("#common_success_msg").html(successMsg);
			var timeout = setTimeout(function() {
				$("#common_success_true").modal("hide");
				$("#successMsg").val("");
			}, 2000);
		}
	});
</script>
</body>
</html>