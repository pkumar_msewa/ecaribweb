<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">

<title>Vijaya | Home</title>
<link rel="icon" type="image/ico" href="${pageContext.request.contextPath}/resources/bescom/assets/images/favicon.ico" />
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- vendor css files -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bescom/assets/bootstrap/css/bootstrap.min.css">    
<%-- <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bescom/assets/css/animsition.min.css"> --%>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bescom/assets/css/morris.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bescom/assets/css/font-awesome/css/font-awesome.min.css">    
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> -->
<!-- project main css files -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bescom/assets/css/main.css">

<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
	/* 	$("#commonSpinner").modal("show"); */
		 /* var timeout = setTimeout(function() {
				$("#commonSpinner").modal("hide");
			}, 1000); */
		
	});
</script>


<style>
	.loader {
	    position: fixed;
		left: 0px;
		top: 0px;
		width: 100%;
		height: 100%;
		z-index: 9999;
		background: url(/resources/bescom/img/vijayabank.gif) center no-repeat #fff;
}
</style>

<script>
$(document).ready(function() {
	   $(".loader").fadeOut("slow");
	  });
</script> 


</head>
<body id="falcon" class="main_Wrapper">
<div class="loader"></div>
    <div id="wrap" class="animsition">
        <!-- HEADER Content -->
        <jsp:include page="/WEB-INF/jsp/Vijaya/Header.jsp"/>
        <jsp:include page="/WEB-INF/jsp/Vijaya/LeftMenu.jsp"/>
        <!--/ HEADER Content  -->
        <!-- CONTENT -->
        <section id="content">
            <div class="page dashboard-page">
			   <div style="border-top: 1px solid #5dbcc7; border-bottom: 1px solid #5dbcc7;padding: 13px; background: #fff;">
			   	<h2 style="margin: 0;">Welcome ${user.firstName} ${user.lastName}</h2>
			   </div>
                <!-- bradcome -->
                <!-- <div class="b-b mb-20">
                    <div class="row">
                        <div class="col-sm-6 col-xs-12">
                            <h1 class="h3 m-0">Dashboard</h1>
                            <small class="text-muted">Welcome to Falcon application</small>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-3 col-sm-6 col-md-6 col-xs-12">
                        <div class="boxs top_report_chart l-blue">
                            <div class="boxs-body">
                                <h3 class="mt-0">52</h3>
                                <p>TOTAL TRANSACTIONS</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6 col-md-6 col-xs-12">
                        <div class="boxs top_report_chart l-parpl">
                            <div class="boxs-body">
                                <h3 class="mt-0">31867</h3>
                                <p>TRANSACTIONS AMOUNT</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6 col-md-6 col-xs-12">
                        <div class="boxs top_report_chart l-seagreen">
                            <div class="boxs-body">
                                <h3 class="mt-0"><i class="fa fa-inr"></i> 21,501</h3>
                                <p>POOL BALANCE</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6 col-md-6 col-xs-12">
                        <div class="boxs top_report_chart l-amber">
                            <div class="boxs-body">
                                <h3 class="mt-0">1,195</h3>
                                <p>Total Feedbacks</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <section class="boxs">
                            <div class="boxs-header">
                                <h3 class="custom-font hb-cyan">
                                    <strong>Annual </strong>Report</h3>
                            </div>
                            <div class="boxs-body">
                                <div id="chartContainer" style="height: 290px"></div>
                            </div>
                        </section>
                    </div>
                    
                </div> -->
            </div>
            <!-- FOOTER -->
            <jsp:include page="/WEB-INF/jsp/Vijaya/Footer.jsp"/>
          <!-- FOOTER -->
        </section>
    </div>
    
     
<!-- Common model for the transaction processing -->
	<div class="modal fade" role="dialog" id="commonSpinner" data-backdrop="static" style="margin-top:10%;">
		<div class="modal-dialog modal-sm">
			<div class="modal-body text-center" style="width: 120%; background:white; padding:15px;">
				<button type="button" data-dismiss="modal" class="close">&times;</button>			
					<center><img alt="" src="${pageContext.request.contextPath}/resources/admin/upi/please-wait.gif" class="img-responsive"></center>
					<!-- <br>
					<h4><b>Please wait...</b></h4> -->
			</div>
		</div>
	</div>
    
    
    <!-- Vendor JavaScripts -->
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> -->
    <script src="${pageContext.request.contextPath}/resources/bescom/assets/bootstrap/js/bootstrap.min.js"></script>
    <!-- <script src="assets/js/libscripts.bundle.js"></script> -->
    <script src="${pageContext.request.contextPath}/resources/bescom/assets/js/vendorscripts.bundle.js"></script>

    <!--/ vendor javascripts -->
    <script src="${pageContext.request.contextPath}/resources/bescom/assets/js/flotscripts.bundle.js"></script>    
    <script src="${pageContext.request.contextPath}/resources/bescom/assets/js/d3cripts.bundle.js"></script>
    <script src="${pageContext.request.contextPath}/resources/bescom/assets/js/sparkline.bundle.js"></script>
    <script src="${pageContext.request.contextPath}/resources/bescom/assets/js/raphael.bundle.js"></script>
    <script src="${pageContext.request.contextPath}/resources/bescom/assets/js/morris.bundle.js"></script>
    <!-- <script src="assets/js/loadercripts.bundle.js"></script> -->
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/bescom/assets/js/canvasjs.min.js"></script>

    <!-- page Js -->
    <script src="${pageContext.request.contextPath}/resources/bescom/assets/js/mainscripts.bundle.js"></script>
    <script src="${pageContext.request.contextPath}/resources/bescom/assets/js/index.js"></script>     

    <script>
    window.onload = function () {

    var chart = new CanvasJS.Chart("chartContainer", {
        animationEnabled: true,  
        
        axisY: {
            title: "Revenue in INR",
            valueFormatString: "#0,,.",
            suffix: "mn",
            prefix: "$"
        },
        data: [{
            type: "splineArea",
            color: "rgba(54,158,173,.7)",
            markerSize: 5,
            xValueFormatString: "YYYY",
            yValueFormatString: "$#,##0.##",
            dataPoints: [
                { x: new Date(2000, 0), y: 3289000 },
                { x: new Date(2001, 0), y: 3830000 },
                { x: new Date(2002, 0), y: 2009000 },
                { x: new Date(2003, 0), y: 2840000 },
                { x: new Date(2004, 0), y: 2396000 },
                { x: new Date(2005, 0), y: 1613000 },
                { x: new Date(2006, 0), y: 2821000 },
                { x: new Date(2007, 0), y: 2000000 },
                { x: new Date(2008, 0), y: 1397000 },
                { x: new Date(2009, 0), y: 2506000 },
                { x: new Date(2010, 0), y: 2798000 },
                { x: new Date(2011, 0), y: 3386000 },
                { x: new Date(2012, 0), y: 6704000},
                { x: new Date(2013, 0), y: 6026000 },
                { x: new Date(2014, 0), y: 2394000 },
                { x: new Date(2015, 0), y: 1872000 },
                { x: new Date(2016, 0), y: 2140000 }
            ]
        }]
        });
    chart.render();

    }
    </script>
</body>
</html>