<%@ page contentType="text/html; charset=utf-8" language="java"
         import="java.sql.*" errorPage=""%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Vijaya | Login</title>
    <link rel="icon" type="image/ico" href="${pageContext.request.contextPath}/resources/bescom/assets/images/favicon.ico" />
   <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bescom/vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bescom/vendor/font-awesome/css/font-awesome.min.css">
    <!-- Custom Font Icons CSS-->
    <link rel="stylesheet" href="https://file.myfontastic.com/BQ5rqoUxsfQGHC35jWa5Ub/icons.css">
    <!-- Google fonts - Open Sans-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800">
    
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bescom/css/style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bescom/css/custom.css">
    <!-- Favicon-->
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/bescom/img/favicon.png">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
        
        <style>
        	.error {
        		font-size: 12px;
        		color: red;
        		margin-top: 0px !important;
			    margin-bottom: 0 !important;
			    text-align: right;
        	}
        </style>
  </head>
  <body>
    <!-- navbar-->
    <header class="header">
      <nav class="navbar navbar-expand-lg fixed-top">
        <a href="index.html" class="navbar-brand">
          <img src="${pageContext.request.contextPath}/resources/bescom/img/logo.png" class="img-responsive" style="width: 180px;">
        </a>
        <button type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler navbar-toggler-right"><span></span><span></span><span></span></button>
      </nav>
    </header>
    
    <section id="hero" class="hero hero-home bg-gray">
      <div class="container">
        <div class="row d-flex">
          <div class="col-lg-4 text order-2 order-lg-1 pad_45">
            <div class="card card-plain card-login"  style="left: 393px;">
              <form class="form" action="${pageContext.request.contextPath}/Vijaya/Merchant/Home" method="post" >
                <div class="content">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <input type="text" id="username" name="username" class="form-control" placeholder="Username">
                        <p class="error" id="error_username"></p>
                      </div>
                      <div class="form-group">
                        <input type="Password" id="password" name="password" placeholder="Password" class="form-control" />
                        <p class="error" id="error_password"></p>
                      </div>
                      <!-- <div class="checkbox">
                        <label><input type="checkbox"> Remember me</label>
                      </div> -->
                    </div>
                  </div>
                </div>
                <div class="footer">
                  <!-- <a href="#" class="btn btn-primary btn-gradient btn-md btn-block" style="border-radius: 5px;">Login</a> -->
                  <button type="submit" onclick="return loadValidate();" class="btn btn-primary btn-gradient btn-md btn-block" style="border-radius: 5px;">Submit</button>
                </div>
              </form>
            </div>
          </div>
          <!-- <div class="col-lg-6 order-1 order-lg-2"><img src="img/Macbook.png" alt="..." class="img-fluid"></div> -->
        </div>
      </div>
    </section>
    
    <div id="scrollTop">
      <div class="d-flex align-items-center justify-content-end"><i class="fa fa-long-arrow-up"></i>To Top</div>
    </div>

    <footer class="main-footer">
                <div class="copyrights">
                  <div class="container">
                    <div class="row">
                      <div class="col-md-12" style="text-align: center;">
                        <p><img src="${pageContext.request.contextPath}/resources/images/new_img/mlogo.png" style="width: 32px;"> &copy; <a href="http://www.msewa.com/">Msewa Software</a> Pvt Ltd. | 2017 All Rights Reserved.</p>
                      </div>
                      
                    </div>
                  </div>
                </div>
              </footer>
    <!-- Javascript files-->
    
    
    
    <script type="text/javascript">
	function loadValidate() {
		$("#error_password").html();
		//var spinnerUrl = "Please wait <img src='/resources/images/spinner.gif' height='20' width='20'>"
		
		var valid = true;
		var username = $('#username').val();
		var password = $('#password').val();
		/* var expDate = $('#upi_exp_dt').val(); */
	
		if(username.length == 0){
			$("#error_username").html();
			$("#error_username").html("Enter username");
			valid = false;
		}
		
		if(password.length == 0){
			$("#error_password").html("Enter password");
			valid = false;
		} 
		
// 		/* if(valid == true) {
			
			if(radioValue === "Upi"){
		       //$("#loadMId").attr("disabled","disabled");
			   $("#loadMId").html(spinnerUrl);  
			} 
			return valid;
		} */
		return valid;
	}
	
</script>
    
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"> </script>
    <script src="${pageContext.request.contextPath}/resources/bescom/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/bescom/vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="${pageContext.request.contextPath}/resources/bescom/js/front.js"></script>
  </body>
</html>