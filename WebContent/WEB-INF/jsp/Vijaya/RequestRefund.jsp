<!DOCTYPE html>
<html lang="en">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<sec:csrfMetaTags />
<title>Vijaya | Request Refund</title>

<!-- Bootstrap core CSS -->
<link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'
	type="image/png" />
<link
	href="${pageContext.request.contextPath}/resources/admin/css/bootstrap.min.css"
	rel="stylesheet">

<link
	href="${pageContext.request.contextPath}/resources/admin/fonts/css/font-awesome.min.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/admin/css/animate.min.css"
	rel="stylesheet">

<!-- Custom styling plus plugins -->
<link
	href="${pageContext.request.contextPath}/resources/admin/css/custom.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/admin/css/icheck/flat/green.css"
	rel="stylesheet">

<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/fixedHeader.bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/responsive.bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resources/admin/js/datatables/scroller.bootstrap.min.css"
	rel="stylesheet" type="text/css" />

<script
	src="${pageContext.request.contextPath}/resources/admin/js/jquery.min.js"></script>
<style>
.no-js #loader {
	display: none;
}

.js #loader {
	display: block;
	position: absolute;
	left: 100px;
	top: 0;
}

.se-pre-con {
	position: fixed;
	left: 0px;
	top: 0px;
	width: 100%;
	height: 100%;
	z-index: 9999;
	background: url(/images/pq_large.gif) center no-repeat #fff;
}
</style>
<script src="<c:url value='/resources/js/modernizr.js'/>"></script>

<script type="text/javascript">
	$(window).load(function() {
		$(".se-pre-con").fadeOut("slow");
	});
</script>
<script src="<c:url value="/resources/js/datepicker.js"/>"></script>
<script type="text/javascript">
	$(document).ready(function() {
		/*  $("#commonSpinner").modal("show"); */
		var errorMsg = document.getElementById("errorMsg").value;
		console.log("Alert ID :: " + errorMsg);
		if (!errorMsg.length == 0) {
			$("#common_error_true").modal("show");
			$("#common_error_msg").html(errorMsg);
			var timeout = setTimeout(function() {
				$("#common_error_true").modal("hide");
				$("#errorMsg").val("");
			}, 3000);
		}

		var successMsg = document.getElementById("successMsg").value;
		console.log("successMsg :: " + successMsg);
		if (!successMsg.length == 0) {
			$("#common_success_true").modal("show");
			$("#common_success_msg").html(successMsg);
			var timeout = setTimeout(function() {
				$("#common_success_true").modal("hide");
				$("#successMsg").val("");
			}, 2000);
		}
	});
	
	// Calling Api for set the exiting ServiceType	
	/* var csrfHeader = $("meta[name='_csrf_header']").attr("content");
    var csrfToken = $("meta[name='_csrf']").attr("content");
    var headers = {};
    headers[csrfHeader] = csrfToken;
    $("#tid").keypress(function(event) {
		var txnid = $("#tid").val();
		console.log("Tid : : " + txnid);
		$.ajax({
			type : "POST",
			headers : headers,
			url : "${pageContext.request.contextPath}/Vijaya/Merchant/RequestRefund",
			contentType : "application/json",
			datatype : 'json',
			data:JSON.stringify({
				"tid":""+txnid+"",
			}),
			success : function(response) {
				console.log("response : :  " + response);
			},
	    }); 
	});*/

	// Call PQ Services based on Service Typw
</script>


</head>
<body class="nav-md">
	<div class="se-pre-con"></div>
	<div class="container body">
		<div class="main_container">
			<jsp:include page="/WEB-INF/jsp/Merchant/LeftMenu.jsp" />
			<jsp:include page="/WEB-INF/jsp/Merchant/TopNavigation.jsp" />
			<input type="hidden" name="successMsg" id="successMsg" value="${msg}">
			<input type="hidden" name="errorMsg" id="errorMsg" value="${error}">

			<!-- page content -->
			<div class="right_col" role="main">
				<div class="">
					<div class="page-title">
						<div class="title_left">
							<h3>Request Refund</h3>
						</div>

					</div>
					<div class="clearfix"></div>

					<div class="row">

						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<!--    <h2>Default Example <small>Users</small></h2> -->
									<ul class="nav navbar-right panel_toolbox">
										<li><a href="#"><i class="fa fa-chevron-up"></i></a></li>
										<li class="dropdown"><a href="#" class="dropdown-toggle"
											data-toggle="dropdown" role="button" aria-expanded="false"><i
												class="fa fa-wrench"></i></a></li>
										<li><a href="#"><i class="fa fa-close"></i></a></li>
									</ul>
									<div class="clearfix"></div>
								</div>
								<div class="x_content">
									<p class="text-muted font-13 m-b-30">
										<!--  DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: <code>$().DataTable();</code> -->
									</p>
									<b>${message}</b>

									<form action="#" method="Post">
										<center>
											<div class="form-group">
												<input type="text" class="form-control"
													placeholder="Transaction ID*" id="tid" name="tid"
													onkeypress="clearvalue('error_tid')">
												<p class="error" id="error_tid"></p>
											</div>

											<div class="form-group">
												<input type="text" class="form-control"
													placeholder="Amount*" id="amount" name="amount"
													onkeypress="clearvalue('error_amount')">
												<p class="error" id="error_amount"></p>
											</div>

											<input id="refund_id" type="button" value="Request"
												style="padding: 6px 30px; border: 0; background: #73879C; color: white;" />
										</center>
									</form>

								</div>
							</div>
						</div>

					</div>
				</div>
				
	<div id="common_success_true" role="dialog" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<center id="common_success_msg" class="alert alert-success"></center>
				</div>
			</div>
		</div>
	</div>
	
	<div id="common_error_true" role="dialog" data-backdrop="static"  class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<center id="common_error_msg" class="alert alert-danger"></center>
					<%-- <center><label id="common_error_msg" class="alert alert-danger"></label></center> --%>
				</div>
			</div>
		</div>
	</div>
				
				<!-- footer content -->
				<jsp:include page="/WEB-INF/jsp/Merchant/Footer.jsp" />
				<!-- /footer content -->
			</div>
			<!-- /page content -->
		</div>
	</div>

	<div id="custom_notifications" class="custom-notifications dsp_none">
		<ul class="list-unstyled notifications clearfix"
			data-tabbed_notifications="notif-group">
		</ul>
		<div class="clearfix"></div>
		<div id="notif-group" class="tabbed_notifications"></div>
	</div>



<!-- Common model for the transaction processing -->
	<div class="modal fade" role="dialog" id="commonSpinner" data-backdrop="static" style="margin-top:10%;">
		<div class="modal-dialog modal-sm">
			<div class="modal-body text-center" style="width: 120%; background:white; padding:15px;">
				<button type="button" data-dismiss="modal" class="close">&times;</button>			
					<center><img alt="" src="${pageContext.request.contextPath}/resources/admin/upi/please-wait.gif" class="img-responsive"></center>
					<!-- <br>
					<h4><b>Please wait...</b></h4> -->
			</div>
		</div>
	</div>


<script type="text/javascript">
		function clearvalue(val){
			$("#"+val).text("");
		}
		</script>

<script type="text/javascript">
	$(document).ready(function(){
		var csrfHeader = $("meta[name='_csrf_header']").attr("content");
        var csrfToken = $("meta[name='_csrf']").attr("content");
        var headers = {};
        headers[csrfHeader] = csrfToken;
		console.log("inside bill payment");
		
		
		//Add Client Services
		$("#refund_id").click(function() {
			var valid = true;
			$("#error_tid").html("");
			$("#error_amount").html("");
			
			var txnid = $("#tid").val();
			var t_amount = $("#amount").val();
			
			if(txnid.length == 0){
				valid = false;
				$("#error_tid").html("Enter Min Amount");
			}
			
			if(t_amount.length == 0){
				valid = false;
				$("#error_amount").html("Enter Max Amount");
			}
			

			if(valid == true){
				$("#commonSpinner").modal("show");
				var contextPath = "${pageContext.request.contextPath}"; 
				console.log("ContexPath : : " + contextPath)
				/*
	            $("#dth_submit").html(spinnerUrl);
	            $("#dth_submit").attr("disabled","disabled");*/

				$.ajax({
					type : "POST",
					contentType : "application/x-www-form-urlencoded",
					url : contextPath+"/Vijaya/Merchant/RequestRefund",
					data : {
						tid : txnid,
						amount : t_amount
					},
					success : function(response) {
						$("#commonSpinner").modal("hide");
						console.log(response);
						if(response.code == "F00"){
						    $("#common_error_true").modal("show");
		        			$("#common_error_msg").html(response.message);
		        			var timeout = setTimeout(function(){
		        				$("#common_error_true").modal("hide");
		        				$("#errorMsg").val("");
		        	          }, 4000);
						}
						if(response.code == "S00"){
							$("#common_success_true").modal("show");
		        			$("#common_success_msg").html(response.message);
		        			var timeout = setTimeout(function(){
		        				$("#common_success_true").modal("hide");
		        	          }, 4000); 
					   }
						if(response.code == "F03"){
						    $("#common_error_true").modal("show");
		        			$("#common_error_msg").html(response.message);
		        			
		        			
							setTimeout(location.reload.bind(location), 3000);
		        			
		        			/* var timeout = setTimeout(function(){
		        				$("#common_error_true").modal("hide");
		        				$("#errorMsg").val("");
		        	          }, 4000); */
		        			
		        			
						}
					}
				});
			}
		});
	});
</script>










	<script
		src="${pageContext.request.contextPath}/resources/admin/js/bootstrap.min.js"></script>

	<!-- bootstrap progress js -->
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/progressbar/bootstrap-progressbar.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/nicescroll/jquery.nicescroll.min.js"></script>
	<!-- icheck -->
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/icheck/icheck.min.js"></script>

	<script
		src="${pageContext.request.contextPath}/resources/admin/js/custom.js"></script>


	<!-- Datatables -->
	<!-- <script src="js/datatables/js/jquery.dataTables.js"></script>
  <script src="js/datatables/tools/js/dataTables.tableTools.js"></script> -->

	<!-- Datatables-->
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/jquery.dataTables.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.bootstrap.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.buttons.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.bootstrap.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/jszip.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/pdfmake.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/vfs_fonts.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.html5.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/buttons.print.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.fixedHeader.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.keyTable.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.responsive.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/responsive.bootstrap.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/datatables/dataTables.scroller.min.js"></script>


	<!-- pace -->
	<script
		src="${pageContext.request.contextPath}/resources/admin/js/pace/pace.min.js"></script>
	<script>
		var handleDataTableButtons = function() {
			"use strict";
			0 !== $("#datatable-buttons").length
					&& $("#datatable-buttons").DataTable({
						dom : "Bfrtip",
						buttons : [ {
							extend : "copy",
							className : "btn-sm"
						}, {
							extend : "csv",
							className : "btn-sm"
						}, {
							extend : "excel",
							className : "btn-sm"
						}, {
							extend : "pdf",
							className : "btn-sm"
						}, {
							extend : "print",
							className : "btn-sm"
						} ],
						responsive : !0
					})
		}, TableManageButtons = function() {
			"use strict";
			return {
				init : function() {
					handleDataTableButtons()
				}
			}
		}();
	</script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#datatable').dataTable();
			$('#datatable-keytable').DataTable({
				keys : true
			});
			$('#datatable-responsive').DataTable();
			$('#datatable-scroller').DataTable({
				ajax : "js/datatables/json/scroller-demo.json",
				deferRender : true,
				scrollY : 380,
				scrollCollapse : true,
				scroller : true
			});
			var table = $('#datatable-fixed-header').DataTable({
				fixedHeader : true
			});
		});
		TableManageButtons.init();
	</script>
</body>

</html>
