<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div id="controls">
            <aside id="leftmenu">
                <div id="leftmenu-wrap">
                    <div class="panel-group slim-scroll" role="tablist">
                        <div class="panel panel-default">
                            <div id="leftmenuNav" class="panel-collapse collapse in" role="tabpanel">
                                <div class="panel-body">
                                    <!--  NAVIGATION Content -->
                                    <ul id="navigation">
                                        <li class="active open">
                                            <a href="${pageContext.request.contextPath}/Vijaya/Merchant/Home">
                                                <i class="fa fa-dashboard"></i>
                                                <span>Dashboard</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="${pageContext.request.contextPath}/Vijaya/Merchant/Transactions">
                                                <i class="fa fa-file-text-o"></i>
                                                <span>Transaction Report</span>
                                            </a>
                                        </li>
                                        
                                        <li>
                                            <a role="button" tabindex="0">
                                                <i class="fa fa-file-text"></i>
                                                <span>Reconciliation Report</span>
                                            </a>
                                            <ul>
                                                <li>
                                                    <a href="${pageContext.request.contextPath}/Vijaya/Merchant/BulkUpload">
                                                        <i class="fa fa-angle-right"></i> Reconciliation Report</a>
                                                </li>
                                                 <li>
                                                    <a href="${pageContext.request.contextPath}/Vijaya/Merchant/MerchantReport">
                                                        <i class="fa fa-angle-right"></i> Upload Report</a>
                                                </li>
                                               <!--  <li>
                                                    <a href="mail-single.html">
                                                        <i class="fa fa-angle-right"></i> Single Mail</a>
                                                </li> -->
                                            </ul>
                                        </li>
                                        
                                     <%--    <li>
                                            <a role="button" tabindex="0">
                                                <i class="fa fa-file-text"></i>
                                                <span>Refund Transactions</span>
                                            </a>
                                            <ul>
                                                <li>
                                                    <a href="<c:url value="/Vijaya/Merchant/RequestRefund"/>">
                                                        <i class="fa fa-angle-right"></i> Refund Request</a>
                                                </li>
                                                <!-- <li>
                                                    <a href="mail-compose.html">
                                                        <i class="fa fa-angle-right"></i> Compose Mail</a>
                                                </li>
                                                <li>
                                                    <a href="mail-single.html">
                                                        <i class="fa fa-angle-right"></i> Single Mail</a>
                                                </li> -->
                                            </ul>
                                        </li> --%>
                                        
                                       <%--  <li>
                                            <a href="<c:url value="/Vijaya/Merchant/RequestRefund"/>">
                                                <i class="fa fa-refresh"></i>
                                                <span>Refund Request</span>
                                            </a>
                                        </li> --%>
                                        
                                    </ul>
                                    <!--/ NAVIGATION Content -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </aside>
            
        </div>
























