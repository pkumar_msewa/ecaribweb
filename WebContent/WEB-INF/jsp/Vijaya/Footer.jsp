<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	<footer class="main-footer">
                <div class="copyrights">
                  <div class="container">
                    <div class="row">
                      <div class="col-md-12">
                        <p><img src="${pageContext.request.contextPath}/resources/images/new_img/mlogo.png" style="width: 32px;"> &copy; <a href="http://www.msewa.com/">Msewa Software</a> Pvt Ltd. | 2017 All Rights Reserved.</p>
                      </div>
                      
                    </div>
                  </div>
                </div>
              </footer>
