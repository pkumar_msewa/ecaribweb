<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags"%>
<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">

<title>Vijaya | Transaction List</title>
<link rel="icon" type="image/ico" href="${pageContext.request.contextPath}/resources/bescom/assets/images/favicon.ico" />
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- vendor css files -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bescom/assets/bootstrap/css/bootstrap.min.css">   
<%-- <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bescom/assets/css/animsition.min.css"> --%>
<!-- <link rel="stylesheet" href="assets/css/animsition.min.css"> -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bescom/assets/css/morris.css">    
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- project main css files -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bescom/assets/css/main.css">

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.1/css/responsive.bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" type="text/css">
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />

<style>
    table.table-bordered.dataTable tbody th, table.table-bordered.dataTable tbody td {
        font-size: 12px;
    }
</style>

</head>
<body id="falcon" class="main_Wrapper">
    <div id="wrap" class="animsition">
        <!-- HEADER Content -->
        <jsp:include page="/WEB-INF/jsp/Vijaya/Header.jsp"/>
        <jsp:include page="/WEB-INF/jsp/Vijaya/LeftMenu.jsp"/>
        <!--/ HEADER Content  -->
        
        <!-- CONTENT -->
        <section id="content">
            <div class="page dashboard-page">
                <!-- bradcome -->
                <div class="b-b mb-20">
                    <div class="row">
                        <div class="col-sm-6 col-xs-12">
                            <h1 class="h3 m-0">Reconciliation Report</h1>
                            <!-- <small class="text-muted">Welcome to Falcon application</small> -->
                        </div>
                    </div>
                </div>
                <!-- DATE PICKER -->
                       <!--  <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-8 col-md-offset-4">
                                    <div class="col-md-2 col-md-offset-3">
                                        <div style="text-align: right;">
                                            <p>Filter by:</p>
                                        </div>
                                    </div>
                                    <div class="col-md-7">
                                        <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc;">
                                            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                                            <span></span> <b class="caret"></b>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                        <!-- DATE PICKER -->
                <div class="row clearfix">
                    <div class="col-md-12">
                        <div class="trxn_table">
                            <table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Transaction Date</th>
                                        <th>Transaction ID</th>
                                        <th>Reference ID</th>
                                        <th>Amount</th>
                                        <th>Status</th>
                                        <th>Fss Txn Date</th>
                                        <th>Fss Ref ID</th>
                                        <th>Fss Status</th>
                                    </tr>
                                </thead>
                               <tbody id=userList>
											<c:forEach items="${transactionList}" var="list" varStatus="loopCounter">
												<tr>
												    <td>${loopCounter.count}</td>
													<!-- <td><i class="fa fa-chevron-circle-right" aria-hidden="true"></i></td> -->
													<td><c:out value="${list.tdate}" default="" escapeXml="true"/></td>
													<td><c:out value="${list.tid}" default="" escapeXml="true"/><br /> </td>
													<td><c:out value="${list.refid}" default="" escapeXml="true"/><br /> </td>
													<td><c:out value="${list.amount}" default="" escapeXml="true"/><br /> </td>
													<td><c:out value="${list.status}" default="" escapeXml="true"/> <br /> </td>
													<%-- <td><c:out value="${list.fdate}" default="" escapeXml="true"/><br /> </td>
													<td><c:out value="${list.frefid}" default="" escapeXml="true"/><br /> </td>
													<td><c:out value="${list.fstatus}" default="" escapeXml="true"/> <br /> </td> --%>
													
												</tr>
											</c:forEach>
										</tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
              <!-- FOOTER -->
            <jsp:include page="/WEB-INF/jsp/Vijaya/Footer.jsp"/>
          <!-- FOOTER -->
            
            
        </section>

    </div>
    <!-- Vendor JavaScripts -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/bescom/assets/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/bescom/assets/js/vendorscripts.bundle.js"></script>
    <!-- Include Date Range Picker -->
    <script type="text/javascript" src="https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
    

    <!--/ vendor javascripts -->
    <script src="${pageContext.request.contextPath}/resources/bescom/assets/js/flotscripts.bundle.js"></script>    
    <script src="${pageContext.request.contextPath}/resources/bescom/assets/js/d3cripts.bundle.js"></script>
    <script src="${pageContext.request.contextPath}/resources/bescom/assets/js/sparkline.bundle.js"></script>
    <script src="${pageContext.request.contextPath}/resources/bescom/assets/js/raphael.bundle.js"></script>
    <script src="${pageContext.request.contextPath}/resources/bescom/assets/js/morris.bundle.js"></script>
    <!-- <script src="assets/js/loadercripts.bundle.js"></script> -->

    <!-- page Js -->
    <script src="${pageContext.request.contextPath}/resources/bescom/assets/js/mainscripts.bundle.js"></script>
    <script src="${pageContext.request.contextPath}/resources/bescom/assets/js/index.js"></script>     

    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.1/js/responsive.bootstrap.min.js"></script>

    <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>

    <script>
        $(document).ready(function() {
            $('#example').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            } );
        } );
        </script>

        <script type="text/javascript">
        $(function() {

            var start = moment().subtract(29, 'days');
            var end = moment();

            function cb(start, end) {
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            }

            $('#reportrange').daterangepicker({
                startDate: start,
                endDate: end,
                ranges: {
                   'Today': [moment(), moment()],
                   'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                   'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                   'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                   'This Month': [moment().startOf('month'), moment().endOf('month')],
                   'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, cb);

            cb(start, end);
            
        });
        </script>
</body>
</html>