<!-- HEADER Content -->
        <div id="header">
            <header class="clearfix">
                <!-- Branding -->
                <div class="branding">
                    <a class="brand" href="${pageContext.request.contextPath}/Vijaya/Merchant/Home">
                        <!-- <span>Falcon</span> -->
                        <img src="${pageContext.request.contextPath}/resources/bescom/img/logo.png" alt="Vijaya Bank" title="Vijaya Bank" style="width: 190px; padding-top: 6px; padding-left: 40px; padding-bottom: 3px;">
                    </a>
                    <a role="button" tabindex="0" class="offcanvas-toggle visible-xs-inline">
                        <i class="fa fa-bars"></i>
                    </a>
                </div>
                <!-- Branding end -->

                <!-- Left-side navigation -->
                <ul class="nav-left pull-left list-unstyled list-inline">
                    <li class="leftmenu-collapse">
                        <a role="button" tabindex="0" class="collapse-leftmenu">
                            <i class="fa fa-outdent"></i>
                        </a>
                    </li>
                </ul>
                <!-- Left-side navigation end -->
                <!-- <div class="search" id="main-search">
                    <input type="text" class="form-control underline-input" placeholder="Explore Falcon...">
                </div> -->
                <!-- Search end -->

                <!-- Right-side navigation -->
                <ul class="nav-right pull-right list-inline">
                    
                    <li class="dropdown nav-profile">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="${pageContext.request.contextPath}/resources/bescom/assets/images/profile-photo.jpg" alt="" class="0 size-45x45"> </a>
                        <ul class="dropdown-menu pull-right" role="menu">
                            <!-- <li>
                                <div class="user-info">
                                    <div class="user-name">Alexander</div>
                                    <div class="user-position online">Available</div>
                                </div>
                            </li> -->
                            <!-- <li>
                                <a href="profile.html" role="button" tabindex="0">
                                    <span class="label label-success pull-right">80%</span>
                                    <i class="fa fa-user"></i>Profile</a>
                            </li> -->
                            <!-- <li>
                                <a role="button" tabindex="0">
                                    <span class="label label-info pull-right">new</span>
                                    <i class="fa fa-check"></i>Tasks</a>
                            </li> -->
                           <!--  <li>
                                <a role="button" tabindex="0">
                                    <i class="fa fa-cog"></i>Settings</a>
                            </li> -->
                            <li class="divider"></li>
                            <!-- <li>
                                <a href="locked.html" role="button" tabindex="0">
                                    <i class="fa fa-lock"></i>Lock</a>
                            </li> -->
                            <li>
                                <a href="${pageContext.request.contextPath}/Vijaya/Merchant/Logout" role="button" tabindex="0">
                                <i class="fa fa-sign-out"></i>Logout</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <!-- Right-side navigation end -->
            </header>
        </div>
        <!--/ HEADER Content  -->