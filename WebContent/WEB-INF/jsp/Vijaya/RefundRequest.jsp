<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags"%>
<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">

<title>Vijaya | Refund Request</title>
<link rel="icon" type="image/ico" href="${pageContext.request.contextPath}/resources/bescom/assets/images/favicon.ico" />
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- vendor css files -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bescom/assets/bootstrap/css/bootstrap.min.css">  
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bescom/assets/css/animsition.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bescom/assets/css/morris.css">    
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bescom/assets/css/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bescom/assets/dropify/css/dropify.min.css">
<!-- project main css files -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bescom/assets/css/main.css">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->

<style>
        	.error {
        		font-size: 12px;
        		color: red;
        		margin-top: 0;
        		margin-bottom: 0;
        	}
        </style>
<script type="text/javascript">
	$(document).ready(function() {
		$("#commonSpinner").modal("show");
		console.log("Alert ID :: " + errorMsg);
		if (!errorMsg.length == 0) {
			$("#common_error_true").modal("show");
			$("#common_error_msg").html(errorMsg);
			var timeout = setTimeout(function() {
				$("#common_error_true").modal("hide");
				$("#errorMsg").val("");
			}, 3000);
		}

		var successMsg = document.getElementById("successMsg").value;
		console.log("successMsg :: " + successMsg);
		if (!successMsg.length == 0) {
			$("#common_success_true").modal("show");
			$("#common_success_msg").html(successMsg);
			var timeout = setTimeout(function() {
				$("#common_success_true").modal("hide");
				$("#successMsg").val("");
			}, 2000);
		}
	});
</script>

</head>
<body id="falcon" class="main_Wrapper">
    <div id="wrap" class="animsition">
        <!-- HEADER Content -->
        <jsp:include page="/WEB-INF/jsp/Vijaya/Header.jsp"/>
        <jsp:include page="/WEB-INF/jsp/Vijaya/LeftMenu.jsp"/>
        <input type="hidden" name="successMsg" id="successMsg" value="${msg}">
			<input type="hidden" name="errorMsg" id="errorMsg" value="${error}">
        <!-- CONTENT -->
        <section id="content">
            <div class="page dashboard-page">
                <!-- bradcome -->
                <div class="b-b mb-20">
                    <div class="row">
                        <div class="col-sm-6 col-xs-12">
                            <h1 class="h3 m-0">Refund Request</h1>
                            <!-- <small class="text-muted">Welcome to Falcon application</small> -->
                        </div>
                    </div>
                </div>
                
                <div class="row clearfix">
                    <div class="col-md-4 col-md-offset-4">
                        <!-- REFUND REQUEST -->
                        <div class="boxs" style="margin-top: 50px;">
                            <div class="boxs-body">
                                <form action="#" method="Post">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="tid" name="tid"
													onkeypress="clearvalue('error_tid')" placeholder="Transaction ID" style="background: aliceblue;">
                                    </div>
                                    <p class="error" id="error_tid"></p>
                                   <!--  <div class="form-group">
                                        <input type="text" class="form-control" id="contact-email" placeholder="VPA">
                                    </div> -->
                                    <div class="form-group">
                                        <input type="number" class="form-control" id="amount" name="amount"
													onkeypress="clearvalue('error_amount')" placeholder="Amount" style="background: aliceblue;">
                                        <p class="error" id="error_amount"></p>
                                    </div>
                                    <center>
                                        <button id="refund_id" type="button" class="btn btn-raised btn-info">Submit</button>
                                    </center>
                                </form>
                            </div>
                        </div>
                        <!-- END REFUND REQUEST -->
                    </div>
                </div>
            </div>
             <!-- FOOTER -->
            <jsp:include page="/WEB-INF/jsp/Vijaya/Footer.jsp"/>
          <!-- FOOTER -->
        </section>
    </div>
    		
	<div id="common_success_true" role="dialog" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<center id="common_success_msg" class="alert alert-success"></center>
				</div>
			</div>
		</div>
	</div>
	
	<div id="common_error_true" role="dialog" data-backdrop="static"  class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<center id="common_error_msg" class="alert alert-danger"></center>
					<%-- <center><label id="common_error_msg" class="alert alert-danger"></label></center> --%>
				</div>
			</div>
		</div>
	</div>
    
    
    
<!-- Common model for the transaction processing -->
	<div class="modal fade" role="dialog" id="commonSpinner" data-backdrop="static" style="margin-top:10%;">
		<div class="modal-dialog modal-sm">
			<div class="modal-body text-center" style="width: 120%; background:white; padding:15px;">
				<button type="button" data-dismiss="modal" class="close">&times;</button>			
					<center><img alt="" src="${pageContext.request.contextPath}/resources/admin/upi/please-wait.gif" class="img-responsive"></center>
					<!-- <br>
					<h4><b>Please wait...</b></h4> -->
			</div>
		</div>
	</div>

    <!-- Vendor JavaScripts -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/bescom/assets/bootstrap/js/bootstrap.min.js"></script>
    <!-- <script src="assets/js/libscripts.bundle.js"></script> -->
    <script src="${pageContext.request.contextPath}/resources/bescom/assets/js/vendorscripts.bundle.js"></script>

    <!--/ vendor javascripts -->
    <script src="${pageContext.request.contextPath}/resources/bescom/assets/js/flotscripts.bundle.js"></script>    
    <script src="${pageContext.request.contextPath}/resources/bescom/assets/js/d3cripts.bundle.js"></script>
    <script src="${pageContext.request.contextPath}/resources/bescom/assets/js/sparkline.bundle.js"></script>
    <script src="${pageContext.request.contextPath}/resources/bescom/assets/js/raphael.bundle.js"></script>
    <script src="${pageContext.request.contextPath}/resources/bescom/assets/js/morris.bundle.js"></script>
    

    <!-- page Js -->
    <script src="${pageContext.request.contextPath}/resources/bescom/assets/js/mainscripts.bundle.js"></script>
    <script src="${pageContext.request.contextPath}/resources/bescom/assets/js/index.js"></script>  
    
    <script type="text/javascript">
		function clearvalue(val){
			$("#"+val).text("");
		}
		</script>

<script type="text/javascript">
	$(document).ready(function(){
		var csrfHeader = $("meta[name='_csrf_header']").attr("content");
        var csrfToken = $("meta[name='_csrf']").attr("content");
        var headers = {};
        headers[csrfHeader] = csrfToken;
		console.log("inside bill payment");
		
		
		//Add Client Services
		$("#refund_id").click(function() {
			var valid = true;
			$("#error_tid").html("");
			$("#error_amount").html("");
			
			var txnid = $("#tid").val();
			var t_amount = $("#amount").val();
			
			if(txnid.length == 0){
				valid = false;
				$("#error_tid").html("Enter Min Amount");
			}
			
			if(t_amount.length == 0){
				valid = false;
				$("#error_amount").html("Enter Max Amount");
			}
			

			if(valid == true){
				$("#commonSpinner").modal("show");
				var contextPath = "${pageContext.request.contextPath}"; 
				console.log("ContexPath : : " + contextPath)
				/*
	            $("#dth_submit").html(spinnerUrl);
	            $("#dth_submit").attr("disabled","disabled");*/

				$.ajax({
					type : "POST",
					contentType : "application/x-www-form-urlencoded",
					url : contextPath+"/Vijaya/Merchant/RequestRefund",
					data : {
						tid : txnid,
						amount : t_amount
					},
					success : function(response) {
						$("#commonSpinner").modal("hide");
						console.log(response);
						if(response.code == "F00"){
						    $("#common_error_true").modal("show");
		        			$("#common_error_msg").html(response.message);
		        			var timeout = setTimeout(function(){
		        				$("#common_error_true").modal("hide");
		        				$("#errorMsg").val("");
		        	          }, 4000);
						}
						if(response.code == "S00"){
							$("#common_success_true").modal("show");
		        			$("#common_success_msg").html(response.message);
		        			var timeout = setTimeout(function(){
		        				$("#common_success_true").modal("hide");
		        	          }, 4000); 
					   }
						if(response.code == "F03"){
						    $("#common_error_true").modal("show");
		        			$("#common_error_msg").html(response.message);
							setTimeout(location.reload.bind(location), 3000);
		        			
						}
					}
				});
			}
		});
	});
</script>   
    
</body>
</html>