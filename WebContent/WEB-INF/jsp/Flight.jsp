<!DOCTYPE HTML>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec"	uri="http://www.springframework.org/security/tags"%>
 
<html>
<head>
	 

	<title>Thank You for booking</title>

</head>
<body style="font-family: Helvetica,Helvetica,Arial,sans-serif">
	<input type="hidden" value="${dataflight}" id="traveldataval">
	<div style="margin-top: 24px; padding-right: 15px; padding-left: 15px; margin-right: auto; margin-left: auto; box-sizing: border-box; display: table;">
		<div style="margin-right: -15px; margin-left: -15px; width: 700px; border: 2px solid #efefef; padding: 20px 20px 13px 20px; background: #efefef; box-sizing: border-box; display: table;">
			<div style="width: 657px;">

				<!-- ==================================== LOGO ============================== -->
				<div class="e-head" style="background: #2d8bc5; box-shadow: 0 0 10px 0px #8b8b8b; padding: 15px;">
					<center><img src="vpaylogo.png" style="width: 180px;"></center>
				</div>
				<div style="margin-right: 0px; margin-left: 0px; background: #fff; padding: 20px 41px 13px 20px; margin-top: 10px;">
					<span style="font-size: 17px; font-weight: 700; color: #069efb;">Hi</span>&nbsp;<span style="font-size: 17px; font-weight: 700; color: #069efb;">Pankaj Kureel</span><br>
					<span style="font-size: 13px;">Your Flight booking has been confirmed. </span>
					<span style="font-size: 13px;">Booking Ref ID</span>&nbsp;<span>#</span>&nbsp;<span style="font-size: 13px;">3531415712</span>
				</div>

				<!-- ==================================== TRAVELLERS AND PNR DETAILS ============================== -->
				<div style="margin-right: 0px; margin-left: 0px; background: #fff; padding: 20px 41px 13px 20px; margin-top: 10px; box-sizing: border-box; display: table; width: 100%;">
					<h4 style="font-size: 18px; margin-top: 0px; margin-bottom: 10px; font-family: inherit; font-weight: 500; line-height: 1.1; color: inherit; box-sizing: border-box;">Travellers Details</h4>
					<hr style="margin-top: 20px; margin-bottom: 20px; border: 0; border-top: 1px solid #eee; height: 0; box-sizing: content-box; display: block; -webkit-margin-before: 0.5em; -webkit-margin-after: 0.5em; -webkit-margin-start: auto; -webkit-margin-end: auto;">
					<div style="margin-right: 0px; margin-left: -20px; background: #fff; padding: 0px 41px 13px 20px; margin-top: 10px; width: 100%;">
						<table style="width: 100%; background-color: transparent; border-spacing: 0; border-collapse: collapse; box-sizing: border-box; display: table;">
							<tr style="background: #efefef;">
							   <th style="padding: 5px; text-align: center;">Passenger Name</th>
							 
							   <th style="padding: 5px; text-align: center;">Ticket No.</th>
							</tr>
								<c:forEach items="${dataflight}" var="u">
							
							<tr>
													
								<td style="padding: 5px; text-align: center; font-size: 13px; color: #737373;"> ${u.cabin} </td>

								<td style="padding: 5px; text-align: center; font-size: 13px; color: #737373;">${u.journeyTime} </td>
								
							</tr>
							 </c:forEach>
						</table>
					</div>
				</div>

				<!-- ==================================== TRIP DETAILS ============================== -->
				
				<div style="margin-right: 0px; margin-left: 0px; background: #fff; padding: 20px 41px 13px 20px; margin-top: 10px; box-sizing: border-box; display: table; width: 100%;">
					
<!-- markup... -->
<h4 style="font-size: 18px; margin-top: 0px; margin-bottom: 10px; font-family: inherit; font-weight: 500; line-height: 1.1; color: inherit; box-sizing: border-box;">Trip Details</h4>
					<hr style="margin-top: 20px; margin-bottom: 20px; border: 0; border-top: 1px solid #eee; height: 0; box-sizing: content-box; display: block; -webkit-margin-before: 0.5em; -webkit-margin-after: 0.5em; -webkit-margin-start: auto; -webkit-margin-end: auto;">


										<c:forEach items="${dataflight}" var="u">
							<span style="font-size: 18px; color: #808080; font-weight: 300;">${u.origin}</span style="font-size: 18px; color: #808080; font-weight: 300;"> <span style="font-size: 18px; color: #808080; font-weight: 300;">→</span> <span style="font-size: 18px; color: #808080; font-weight: 300;">${u.destination}</span><br> 
							<span style="font-size: 12px;">  ${u.departureDate}</span><br>
					<span style="font-size: 12px;"> ${u.cabin}</span><br>
					<span style="font-size: 14px;">${u.airlineName}-${u.flightNumber}</span><br>
<br><br>							</c:forEach>
					
					<br><br>
					<div class="row" style="margin-right: 0px; margin-left: 0px; background: #fff; padding: 0px 10px 13px 10px; margin-top: 10px; width: 100%;">
					<c:forEach items="${dataflight}" var="u">
						<div class="lftside" style="width: 40%; float: left;">
							<span style="font-size: 15px; font-weight: 600; color: #737373;"> ${u.origin}</span>&nbsp;<span style="font-size: 15px; font-weight: 600; color: #737373;">${u.departureTime}</span><br>
							<span style="font-size: 12px; color: #737373;">${u.departureDate}</span><br>
							
						</div>
						<div class="cntr" style="width: 20%; float: left;">
							<span style="font-size: 13px;"> ${u.duration}</span>
						</div>
						<div class="r8side" style="width: 40%; float: left; text-align: right;">
							<span style="font-size: 15px; font-weight: 600; color: #737373;">${u.destination}</span>&nbsp;<span style="font-size: 15px; font-weight: 600; color: #737373;">${u.arrivalTime}</span><br>
							<span style="font-size: 12px; color: #737373;">${u.arrivalDate}</span><br>
							
						</div>
						</c:forEach>
					</div>
					 
				</div>

<!-- ==================================== TRIP DETAILS return============================== -->
<c:if test="${fn:length(dataflightreturn) gt 1}">
	<div style="margin-right: 0px; margin-left: 0px; background: #fff; padding: 20px 41px 13px 20px; margin-top: 10px; box-sizing: border-box; display: table; width: 100%;">
					<h4 style="font-size: 18px; margin-top: 0px; margin-bottom: 10px; font-family: inherit; font-weight: 500; line-height: 1.1; color: inherit; box-sizing: border-box;">Trip Details</h4>
					<hr style="margin-top: 20px; margin-bottom: 20px; border: 0; border-top: 1px solid #eee; height: 0; box-sizing: content-box; display: block; -webkit-margin-before: 0.5em; -webkit-margin-after: 0.5em; -webkit-margin-start: auto; -webkit-margin-end: auto;">
					<c:forEach items="${dataflightreturn}" var="u">
							<span style="font-size: 18px; color: #808080; font-weight: 300;">${u.originreturn}</span style="font-size: 18px; color: #808080; font-weight: 300;"> <span style="font-size: 18px; color: #808080; font-weight: 300;">→</span> <span style="font-size: 18px; color: #808080; font-weight: 300;">${u.destinationreturn}</span><br>
 
							<span style="font-size: 12px;">  ${u.departureDatereturn}</span><br>
					<span style="font-size: 12px;">${u.cabinreturn}</span><br>
					<span style="font-size: 14px;">${u.airlineNamereturn}-${u.flightNumberreturn}</span><br>
					<br><br>
							</c:forEach>
					
					<div class="row" style="margin-right: 0px; margin-left: 0px; background: #fff; padding: 0px 10px 13px 10px; margin-top: 10px; width: 100%;">
					<c:forEach items="${dataflightreturn}" var="u">
						<div class="lftside" style="width: 40%; float: left;">
							<span style="font-size: 15px; font-weight: 600; color: #737373;"> ${u.originreturn}</span>&nbsp;<span style="font-size: 15px; font-weight: 600; color: #737373;">${u.departureTimereturn}</span><br>
							<span style="font-size: 12px; color: #737373;">${u.departureDatereturn}</span><br>
							
						</div>
						<div class="cntr" style="width: 20%; float: left;">
							<span style="font-size: 13px;"> ${u.durationreturn}</span>
						</div>
						<div class="r8side" style="width: 40%; float: left; text-align: right;">
							<span style="font-size: 15px; font-weight: 600; color: #737373;">${u.destinationreturn}</span>&nbsp;<span style="font-size: 15px; font-weight: 600; color: #737373;">${u.arrivalTimereturn}</span><br>
							<span style="font-size: 12px; color: #737373;">${u.arrivalDatereturn}</span><br>
							
						</div>
						</c:forEach>
					</div>
					 
				</div>
				</c:if>
				<!-- ==================================== FARE DETAILS ============================== -->
				<div style="margin-right: 0px; margin-left: 0px; background: #fff; padding: 20px 41px 13px 20px; margin-top: 10px; box-sizing: border-box; display: table; width: 100%;">
					<h4 style="font-size: 18px; margin-top: 0px; margin-bottom: 10px; font-family: inherit; font-weight: 500; line-height: 1.1; color: inherit; box-sizing: border-box;">Fare Details</h4>
					<hr style="margin-top: 20px; margin-bottom: 20px; border: 0; border-top: 1px solid #eee; height: 0; box-sizing: content-box; display: block; -webkit-margin-before: 0.5em; -webkit-margin-after: 0.5em; -webkit-margin-start: auto; -webkit-margin-end: auto;">
					<div class="row" style="margin-right: 0px; margin-left: 0px; background: #fff; padding: 0px 10px 13px 10px; margin-top: 10px; width: 100%;">
						<div class="lftside" style="width: 80%; float: left; text-align: left;">
					
						
							<span><b>Total</b></span>
						</div>
						<div class="r8side" style="width: 20%; float: left; text-align: right;">
							
							<span style="font-size: 13px;">Rs:</span>&nbsp;<span style="font-size: 13px;">3991</span>
						</div>
					</div>
				</div>
				
				
				<br>
				<div class="cmpy-logo" style="float: left; width: 52px; border-right: 2px solid; margin-left: 210px; border-right-color: #bfbfbf;">
					<a href="www.msewa.com"><img src="logo.png" style="width: 40px;"></a><br>
				</div>
				<div class="cmpy-add" style="font-size: 10px; letter-spacing: 0.5pt; margin-left: 9px; float: left;">
					<span>MSewa Software Solution Pvt. Ltd.</span><br>
					<span>Koramangala, Bangalore, India</span><br>
					<a href="www.msewa.com">www.msewa.com</a>
				</div>
			</div>
		</div>
	</div>
</body>
</html>