<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	errorPage=""%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec"	uri="http://www.springframework.org/security/tags"%>
<html lang="en">
<head>
<meta charset="utf-8">
<sec:csrfMetaTags />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<title>VPayQwik | Adlab Order</title>
<link rel="stylesheet"
	href="<c:url value="/resources/css/font-awesome.min.css"/>">
<link href='<c:url value='/resources/css/font-family.css'/>'
	rel='stylesheet' type='text/css'>
<link rel="stylesheet"
	href="<c:url value='/resources/css/font-awesome.css'/>" type='text/css'>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!-- Optional theme -->
<link rel="stylesheet"
	href="<c:url value='/resources/css/bootstrap-theme.min.css'/>"
	type='text/css'>

<link href="<c:url value="/resources/css/css_style.css"/>"
	rel='stylesheet' type='text/css'>
<link rel="stylesheet"
	href="<c:url value='/resources/css/bootstrap.css'/>" type='text/css'>
<script src="<c:url value='/resources/js/jquery.js'/>"></script>
<script src="<c:url value='/resources/js/bootstrap.min.js'/>"></script>

<script type="text/javascript"
	src="<c:url value="/resources/js/userdetails.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/header.js"/>"></script>
<link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'
	type="image/png" />
<link href='<c:url value="/resources/css/css_style.css"/>'
	rel='stylesheet' type='text/css'>
	
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/> 

<style>
.no-js #loader {
	display: none;
}

.js #loader {
	display: block;
	position: absolute;
	left: 100px;
	top: 0;
}

.se-pre-con {
	position: fixed;
	left: 0px;
	top: 0px;
	width: 100%;
	height: 100%;
	z-index: 9999;
	background: url(/images/pq_large.gif) center no-repeat #fff;
}
</style>
<script src="<c:url value='/resources/js/modernizr.js'/>"></script>

<script type="text/javascript">
	$(window).load(function() {
		$(".se-pre-con").fadeOut("slow");
	});
	
	
</script> 

</head>

<body>
	<div class="se-pre-con"></div>
	<jsp:include page="/WEB-INF/jsp/User/Header.jsp" />
	
	<script type="text/javascript">
	$(document).ready(function(){
		$("#myModal").modal('show');
	});
</script>
	
	

	<div class="container" id="aboutus" style="margin-top: -80px;">
		
			<div class="shopper-informations">
					 
						<div class="bill-to" style="margin-top: 19px;">
							<h4><b>User Detail <b></b></h4>
						
							<div class="form-one">
									
<form action="${pageContext.request.contextPath}/User/Adlabs/Payment" method="post" name="myForm" onsubmit="return validateForm();">
								<div class="col-md-2">
									<input type="text" style="width:100%;" placeholder="First Name *"name="first_name"required id="a">
                                </div>
                                <div class="col-md-2">
									<input type="text" style="width:100%;" placeholder="last Name *"name="last_name" required id="c">
                                </div>
								<div class="col-md-2">
                                	<input type="email" style="width:100%;"  placeholder="Email*" onkeyup="getbusseatandamount()" name="email_id"required id="email">
                                 </div>      
				                  <sec:csrfInput/>  
									<span class="error"  style="padding:0px" id="error_usergender_name"></span>
								<div class="col-md-2">	
									<input type="number" style="width:100%;" placeholder="Mobile Number *"name="mob_num" required id="co">
                                </div>
                                <div class="col-md-2">
									<input type="text" style="width:100%;" placeholder="Address *"name="address" required id="s">
								</div>
                                <div class="col-md-2">
									<input type="text" style="width:100%;" placeholder="City *"name="city" required>
                                 </div>
                                 <div class="col-md-2">
				            <input type="hidden" style="width:100%;" value="${uid}" escapeXml="true" name="uid">
                            </div>
                            <div class="col-md-2">
				          <input type="hidden" style="width:100%;" value="${order_id}" escapeXml="true" name="order_id">
                          </div><br>
                          <div class="col-md-2 pull-right" style="    margin-top: 10px;">
				       <input type="submit"   style="width: 100%; color:white;background:red; border-bottom: aliceblue;" id="myModalcsds" />
				             </div><hr>
								</form>
                                
			 <span style="color:red;"> ${msg}</span><br>
							
							</div><br>
							
						</div>					
				</div>
							 		
				</div>
				
			</div>
					
					
		</div>
	<!-- Modal  -->

	

	
<script>
 $(document).ready(function(){
  var date_input=$('input[name="orderDate"]');
  var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
  date_input.datepicker({
	  minDate: 0,
   format: 'dd/mm/yyyy',
   container: container,
   todayHighlight: true,
   autoclose: true,
  })
 })
</script>
	
	
	<script>
	
	function validateForm() {
	    var x = document.forms["myForm"]["email"].value;
	    var atpos = x.indexOf("@");
	    var dotpos = x.lastIndexOf(".");
	    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length) {
	        alert("Not a valid e-mail address");
	        return false;
	    }
	}
    
    
    </script>
<jsp:include page="/WEB-INF/jsp/User/Footer.jsp" />

    <script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
</body>

	
<jsp:include page="/WEB-INF/jsp/User/Footer.jsp" />

<script src="http://code.jquery.com/jquery-2.2.1.min.js"></script>
</html>













