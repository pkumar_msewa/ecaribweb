<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
  <title></title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  </head>
  <body style="background: url("/resources/images/main/banner-3")no-repeat; background-size: 100%">
 <center style="margin-top: 10%;"><div style="width: 100px; margin-top: 10px">
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 46.1 46.6" enable-background="new 0 0 46.1 46.6" xml:space="preserve">
<circle fill="#FFFFFF" cx="23" cy="23.3" r="22.8"/>
<circle fill="#2EB379" cx="23" cy="23.3" r="20.6"/>
<polygon fill="#FFFFFF" points="16.9,20.5 21.2,24.8 29.5,17 31.4,19.1 21.2,29.6 14.6,23 "/>
</svg></div>
<div style="width: 500px; background: white; margin-top: -50px;
    padding: 2.5% 2%;">
<h3><b>Your order is successfully placed.<br><small>You will receive the ticket in your email.</b></small></h3>
</div></center>
</body>
</html>
