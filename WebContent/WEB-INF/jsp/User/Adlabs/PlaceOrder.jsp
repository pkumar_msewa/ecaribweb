<!DOCTYPE html>
<html lang="en">
<head>
  <title></title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="/resources/css/new_css/default.css">
  <link rel="stylesheet" href="/resources/css/new_css/responsive.css">
  <link rel="stylesheet" href="/resources/css/new_css/uniform.default.min.css">
  
  
</head>
<style>
.quantity{
    border: none;
    padding: 7px;
    margin-left: -3px;
    margin-right: -3px;
    width: 56px;
	text-align: center;
	}
	.btn{
		background: #f35835;
    	color: white;
   		border-radius: 0;
		}
		#pri1{
		    float: left;
    		margin-left: 90px;
			}
			#box{
				padding: 14px;
				display:none;
				background: #e2f0ff;
				}
				#quen{
					float:right; 
					margin-right: 31px;
					}
					#line1{
						margin-bottom: 65px;
						}
						#new{
							padding: 7px;
    						background: white;
							}
							#btn{
								margin-top: 50px;
								}
								#top_form{
								    background: white;
									padding: 13px;
									border-bottom-left-radius: 17px;
									border-bottom-right-radius: 17px;
								    -webkit-box-shadow: 0px 25px 42px -23px rgb(210, 210, 210);
								    -moz-box-shadow: 0px 25px 42px -23px rgb(210, 210, 210);
								    box-shadow: 0px 25px 42px -23px rgb(210, 210, 210);
									}
									#tick{
										background: white;
										-webkit-box-shadow: 0px 11px 33px 1px rgba(210, 210, 210, 0.68);
										-moz-box-shadow: 0px 11px 33px 1px rgba(210, 210, 210, 0.68);
										box-shadow: 0px 11px 33px 1px rgba(210, 210, 210, 0.68);
										}
										.boxtt i{
											font-size: 30pt;
											}
											.boxtt{
											    float: left;
											    color: #d6d6d6;										   
												}
												.1111a{
													color: #d6d6d6;
													}
							@media screen and (max-width: 991px) {
								#newtext{
									text-align: right;
									}
									#btn{
										float:right;
									    margin-right: 25px;
									    margin-top: auto;
										}
										.dis{
											font-size: 9pt;
											}
											#pri1{
												margin-left: 20px;
												}											
								}
								@media screen and (max-width: 650px) {
										#quen{
											float:left;
										}
									}
									@media screen and (max-width: 550px) {
										#pri1{
											float: right;
											}
										}
										@media screen and (max-width: 397px) {
										.cartbtn{
										margin-left: 0px !important;
										}
										}
</style>

<body style="background: aliceblue;">


<jsp:include page="/WEB-INF/jsp/User/Header.jsp" />

<div class="container-fluid" id="tick">
	<div style="-webkit-box-shadow: inset -1px 11px 24px -10px rgba(0,0,0,0.54);-moz-box-shadow: inset -1px 11px 24px -10px rgba(0,0,0,0.54);box-shadow: inset -1px 11px 24px -10px rgba(0,0,0,0.54);margin-left: -15px;margin-right: -15px;">

	<div class="container" style="padding: 10px;">
    	<div class="row">
        	<div class="col-sm-2 boxtt text-center" style="color: #f35835;"><i class="fa fa-ticket" aria-hidden="true"></i><br> TICKETS </div><i class="fa fa-chevron-right" style="color: #0a9f8c; position: absolute;font-size: 34px;margin-top: 11px;"></i>
            <div class="col-sm-2 boxtt text-center" style="color: #8cc84e;"><i class="fa fa-puzzle-piece" aria-hidden="true"></i><br> Addons</div><i class="fa fa-chevron-right" style="color: #0a9f8c; position: absolute;font-size: 34px;margin-top: 11px;"></i>
            <div class="col-sm-2 boxtt text-center" style="color: #0a9f8c;"><i class="fa fa-user" aria-hidden="true"></i><br> Your Details</div><i class="fa fa-chevron-right" style="color: #0a9f8c; position: absolute;font-size: 34px;margin-top: 11px;"></i>
            <div class="col-sm-2 boxtt text-center" style="color: #f35835;"><i class="fa fa-newspaper-o" aria-hidden="true"></i><br> Review Order</div><i class="fa fa-chevron-right" style="color:#0a9f8c; position: absolute;font-size: 34px;margin-top: 11px;"></i>
            <div class="col-sm-2 boxtt text-center"><i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 60px;"></i></div>
        </div>
    </div>
</div>
</div>




<div class="container" id="new" style="margin-top: 2%;">
	<div><span style="color: #005cac;font-size: 17pt;"><b>CART SUMMARY</b> </span><div class="pull-right" style="color: #005cac;font-size: 12pt;"><span><b>Cart Total:</span> <span>Cart Total:</span> <span>
Visit Date: 27/05/2017</span></div></div>
    <hr style="margin-top:auto;"> 
	
	
	<br>
	
	
	<div class="row">
	<div class="col-md-6 col-md-offset-3">
	<h4 style="color:#005cac;margin-left: 2%;"><b>BILLING INFORMATION</b></h4>
		<form action="${pageContext.request.contextPath}/Imagica/confirmPayment" method="get">
		<table class="table">
			<tr>
				<td>First Name: </td>
				<td>${firstN}</td>				
			</tr>
			<tr>
				<td>Last Name: </td>
				<td>${lastName}</td>
			</tr>
			<tr>
				<td>Order Id:  </td>
				<td>${orderId}</td>
			</tr>
			<tr>
				<td>Transaction Id: </td>
				<td>${transactionId}</td>
			</tr>
			<tr>
				<td>Amount: </td>
				<td>${amount}</td>
			</tr>
			
			<tr>
				<td>Address: </td>
				<td>${address}</td>
			</tr>
			<tr>
				<td>Mobile No: </td>
				<td>${mobileNo}</td>
			</tr>
			<tr><td></td><td><button class="btn" type="submit">Confirm Payment</button></td></tr>
		</table>
		</form>
	</div>
	
	</div>
	
		
	
</div>


<div class="container">
  
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
          <p><i>${Trxmessage}</i></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
</div>



<script>
	$("#hidecart").click(function(){
		$("#showcard").toggle();
	});
</script>
<script>
$( document ).ready(function() {
	var txcode='"'+${Trxcode}+'"';
	if(txcode=="F00"){
		  $("#myModal").modal();
	}
	
});
</script>
</body>
</html>
