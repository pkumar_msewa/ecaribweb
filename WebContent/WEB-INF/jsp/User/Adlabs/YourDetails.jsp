<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
  <title></title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="/resources/css/new_css/default.css">
  <link rel="stylesheet" href="/resources/css/new_css/responsive.css">
  <link rel="stylesheet" href="/resources/css/new_css/uniform.default.min.css">
  
  <!-- Include Date Range Picker -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
</head>
<style>
.quantity{
    border: none;
    padding: 7px;
    margin-left: -3px;
    margin-right: -3px;
    width: 56px;
	text-align: center;
	}
	.btn{
		background: #f35835;
    	color: white;
   		border-radius: 0;
		}
		#pri1{
		    float: left;
    		margin-left: 90px;
			}
			#box{
				padding: 14px;
				display:none;
				background: #e2f0ff;
				}
				#quen{
					float:right; 
					margin-right: 31px;
					}
					#line1{
						margin-bottom: 65px;
						}
						#new{
							padding: 7px;
    						background: white;
							}
							#btn{
								margin-top: 50px;
								}
								#top_form{
								    background: white;
									padding: 13px;
									border-bottom-left-radius: 17px;
									border-bottom-right-radius: 17px;
								    -webkit-box-shadow: 0px 25px 42px -23px rgb(210, 210, 210);
								    -moz-box-shadow: 0px 25px 42px -23px rgb(210, 210, 210);
								    box-shadow: 0px 25px 42px -23px rgb(210, 210, 210);
									}
									#tick{
										background: white;
										-webkit-box-shadow: 0px 11px 33px 1px rgba(210, 210, 210, 0.68);
										-moz-box-shadow: 0px 11px 33px 1px rgba(210, 210, 210, 0.68);
										box-shadow: 0px 11px 33px 1px rgba(210, 210, 210, 0.68);
										}
										.boxtt i{
											font-size: 30pt;
											}
											.boxtt{
											    float: left;
											    color: #d6d6d6;										   
												}
												.1111a{
													color: #d6d6d6;
													}
													.table>tbody>tr>td{
														    border-top: none;
														}
							@media screen and (max-width: 991px) {
								#newtext{
									text-align: right;
									}
									#btn{
										float:right;
									    margin-right: 25px;
									    margin-top: auto;
										}
										.dis{
											font-size: 9pt;
											}
											#pri1{
												margin-left: 20px;
												}											
								}
								@media screen and (max-width: 650px) {
										#quen{
											float:left;
										}
									}
									@media screen and (max-width: 550px) {
										#pri1{
											float: right;
											}
										}
										@media screen and (max-width: 397px) {
										.cartbtn{
										margin-left: 0px !important;
										}
										}
</style>

<script>
	var i=1;
		function a(){
		if(i==1){
		$("#btn").html("hide <i class='fa fa-arrow-up'></i>");
			document.getElementById("new").style.backgroundColor = "#e2f0ff";
			i=2;
			$("#box").toggle();}
			else{
			document.getElementById("new").style.backgroundColor = "white";
			i=1;
			$("#box").toggle();
	 		$("#btn").html("show <i class='fa fa-arrow-down'></i>");	
		}
	}
</script>
<body style="background: aliceblue;">

<jsp:include page="/WEB-INF/jsp/User/Header.jsp" />

<div class="container-fluid" id="tick">
<div style="-webkit-box-shadow: inset -1px 11px 24px -10px rgba(0,0,0,0.54);-moz-box-shadow: inset -1px 11px 24px -10px rgba(0,0,0,0.54);box-shadow: inset -1px 11px 24px -10px rgba(0,0,0,0.54);margin-left: -15px;margin-right: -15px;">

	<div class="container" style="padding: 10px;">
    	<div class="row">
        	<div class="col-sm-2 boxtt text-center" style="color: #f35835;"><i class="fa fa-ticket" aria-hidden="true"></i><br> TICKETS </div><i class="fa fa-chevron-right" style="color: #0a9f8c; position: absolute;font-size: 34px;margin-top: 11px;"></i></a>
            <div class="col-sm-2 boxtt text-center"><i class="fa fa-puzzle-piece" aria-hidden="true"></i><br> Addons</div><i class="fa fa-chevron-right" style="color: #d6d6d6; position: absolute;font-size: 34px;margin-top: 11px;"></i>
            <div class="col-sm-2 boxtt text-center"><i class="fa fa-user" aria-hidden="true"></i><br> Your Details</div><i class="fa fa-chevron-right" style="color:#d6d6d6; position: absolute;font-size: 34px;margin-top: 11px;"></i>
            <div class="col-sm-2 boxtt text-center"><i class="fa fa-newspaper-o" aria-hidden="true"></i><br> Review Order</div><i class="fa fa-chevron-right" style="color:#d6d6d6; position: absolute;font-size: 34px;margin-top: 11px;"></i>
            <div class="col-sm-2 boxtt text-center"><i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 60px;" id=""></i><p style="    font-size: 13pt;
    color: #f35835; font-weight: 800;" id="shopping_cart"></p></div>
        </div>
    </div>
</div>
</div>


<div class="container" style="margin-top: 3%; background: white;">
	<h3><b>CART SUMMARY</b></h3>
    <hr style="margin-top:auto;"> 
    <input type="text" style="padding: 4.8px;"><button class="btn">USE COUPON CODE</button>   
	<hr>
    
    <h3><b>Add Your Details</b></h3>
  <form action="${pageContext.request.contextPath}/Imagica/processPayment" method="post">
    <div class="row">
    	<div class="col-md-6">
    	
    	  
		<table class="table">
			<tr>
				<td><b>Title</b></td>
				<td><select class="form-control" name="salutation" required>
						<option value="">--Select--</option>
						<option value="Mr">Mr.</option>
						<option value="Mrs">Mrs.</option>
						<option value="Ms">Ms.</option>
					</select>
				</td>				
			</tr>
			<tr>
				<td><b>First Name</b></td>
				<td><input type="text" name="firstName" class="form-control" required></td>
			</tr>
            <tr>
				<td><b>Last Name</b></td>
				<td><input type="text" name="lastName" class="form-control" required></td>
			</tr>
            <tr>
				<td><b>Email Address</b></td>
				<td><input type="text" name="email_id" class="form-control" required></td>
			</tr>
            <tr>
				<td><b>Date of Birth</b></td>
				<td><input type="text" name="dob" class="form-control" required></td>
			</tr>
            
             <tr>
				<td><b>Phone Number</b></td>
				<td><input type="text" name="contact_no" class="form-control" required></td>
			</tr>
        </table>
		</div>
        
        <div class="col-md-6">
		<table class="table">
			<tr>
				<td><b>Address</b></td>
				<td><textarea class="form-control" rows="4" id="comment" name="address" required></textarea>
				</td>				
			</tr>
			<tr>
				<td><b>Pin Code</b></td>
				<td><input type="text" name="pinCode" class="form-control" required></td>
			</tr>
            <tr>
				<td><b>Country</b></td>
				<td><input type="text" name="country" class="form-control" placeholder="india" required></td>
			</tr>
            <tr>
				<td><b>State</b></td>
				<td><input type="text" name="state" class="form-control" required></td>
			</tr>           
             <tr>
				<td><b>City</b></td>
				<td><input type="text" name="city" class="form-control" required></td>
			</tr>
        </table>
        
		</div>
        <hr>
    </div>
        <button id="btn" class="btn btn-success pull-right" style="margin-top: auto; margin-right: 2%; margin-bottom: 2%" type="submit">Proceed to Order Review</button>
  </form>
    </div>

<script>
 $(document).ready(function(){
  var date_input=$('input[name="dob"]');
  var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
  date_input.datepicker({
   format: 'mm-dd-yyyy',
   container: container,
   todayHighlight: true,
   autoclose: true,
  })
 })
</script>
<script>
    $(".incr-btn").on("click", function (e) {
        var $button = $(this);
        var oldValue = $button.parent().find('.quantity').val();
        $button.parent().find('.incr-btn[data-action="decrease"]').removeClass('inactive');
        if ($button.data('action') == "increase") {
            var newVal = parseFloat(oldValue) + 1;
        } else {
            if (oldValue > 1) {
                var newVal = parseFloat(oldValue) - 1;
            } else {
                newVal = 1;
                $button.addClass('inactive');
            }
        }
        $button.parent().find('.quantity').val(newVal);
        e.preventDefault();
    });
</script>
</body>
</html>
    