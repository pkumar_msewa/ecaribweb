<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	errorPage=""%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<html lang="en">
<head>
<meta charset="utf-8">
<sec:csrfMetaTags />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
	
<title>VPayQwik | Event Lists</title>
<link rel="stylesheet"
	href="<c:url value="/resources/css/font-awesome.min.css"/>">
<link href='<c:url value='/resources/css/font-family.css'/>'
	rel='stylesheet' type='text/css'>

<!-- Optional theme -->
<link rel="stylesheet"
	href="<c:url value='/resources/css/bootstrap-theme.min.css'/>"
	type='text/css'>

<link href="<c:url value="/resources/css/css_style.css"/>"
	rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    
<script type="text/javascript"
	src="<c:url value="/resources/js/userdetails.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/header.js"/>"></script>
<link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'
	type="image/png" />
<link href='<c:url value="/resources/css/css_style.css"/>'
	rel='stylesheet' type='text/css'>
<style>
.no-js #loader {
	display: none;
}

.js #loader {
	display: block;
	position: absolute;
	left: 100px;
	top: 0;
}

.se-pre-con {
	position: fixed;
	left: 0px;
	top: 0px;
	width: 100%;
	height: 100%;
	z-index: 9999;
	background: url(/images/pq_large.gif) center no-repeat #fff;
}
</style>
<script src="<c:url value='/resources/js/modernizr.js'/>"></script>

<script type="text/javascript">
		$(window).load(function() {
			$(".se-pre-con").fadeOut("slow");
		});
	</script>

</head>

<body onload="start()">
	<div class="se-pre-con"></div>
	<jsp:include page="/WEB-INF/jsp/User/Header.jsp" />

	<!-----------------end navbar---------------------->

	<!------------- end main-------------------->

	<!---blue box---->
	<span style="color: red;"> ${msg}</span>
	<br>

	<div class="container" style="		background: #FFFFFF;
		margin-top: -80px;
		-webkit-box-shadow: -1px 1px 47px -7px rgba(0, 0, 0, 0.67);
		-moz-box-shadow: -1px 1px 47px -7px rgba(0, 0, 0, 0.67);
		box-shadow: -1px 1px 47px -7px rgba(0, 0, 0, 0.67);
	">
	<div class="col-md-12">
		<c:forEach items="${ticketlist}" var="gl" varStatus="loopCounter">
					<div class="col-md-6" style="background: white; padding: 2%; border: solid #f9f8f8;     margin-top: 15px;">
				<div><center><img src="/resources/images/waterpark1.jpg" class="img-responsive" /></center></div>
					<hr><h2 style="margin-top: 5%; background: cadetblue; padding: 7px 17px; margin-top: 5%;"><i style="color: white;"><b><c:out value="${gl.title}" escapeXml="true" default="" /></b></i></h2>
						${gl.ticket_des}
					<button  onclick="btnsho()" class="btn btn-success pull-right" >Show</button>
				</div>

			<!-- -----end for 1 ticket----- -->

			<!-- for   Second Ticket Imagica Express   -->
<br>

			<div class="col-md-6" style="background: white; padding: 2%; border: solid #f9f8f8; margin-top: -5px;">
				<div><center><img src="/resources/images/waterpark2.jpg" class="img-responsive" /></center></div>
				<hr><h2 style="margin-top: 5%; background: cadetblue; padding: 7px 17px; margin-top: 5%;"><i style="color: white;"><b><c:out value="${gl.etitle}" escapeXml="true" default="" /></b></i></h2>
						${gl.eticket_des}
					<button onclick="btnsho1()" class="btn btn-success pull-right">Show</button>
			</div>
		
	</div>	
			<!-- -----for 1 ticket----- -->
			<div class="row" id="btn2"
					style="background: #fff6f6; display: none; padding: 19px 62px;">
				<div >
					<!-- for Adult  -->
					<div class="row">
						<div class="col-md-2">
							<h4 style="float: left; margin-top: 26px;">
								<b><i class="fa fa-inr"></i> <c:out value="${gl.acost_per2}"
										escapeXml="true" default="" /></b>
							</h4>
						</div>
						<div class="col-md-3 text-center">
							<b><c:out value="${gl.atype}" escapeXml="true" default="" /></b>

							<select name="quantity" id="${loopCounter.count}quantity"onchange="calculateTotalAmount(${loopCounter.count},${gl.acost_per2});"
								class="form-control">
								<option value="0">0</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
							</select>
						</div>
						<div class="col-md-5">
							<b><i class="fa fa-inr"
								style="float: left; margin-top: 21px; font-size: 16pt;"></i><input
								type="text" id="1totalamount" value="${gl.acost_per2}"
								style="margin-top: 10px; background: transparent; border-bottom: none; font-size: 20pt; width: 130px; margin-top: auto;" /></b>
						
						<div style="display: none;" id="eventspiner">
							<img src="/resources/images/s.gif" width=" 50px" />
						</div>
						</div>
						<button type="button" class="btn btn-warning"
							style="background: red; color: white; margin-left: 4%; border-radius: 0; margin-top: 16px;"
							onclick="detail('${gl.aproduct_id}','${gl.acost_per2}')">Proceed
						</button>
					</div>
					<hr>
				</div>
			</div>
			<!-- -----end for 1 ticket----- -->

	<!-- -----for 2 ticket----- -->
			<div class="row" id="btn5"
					style="background: #fff6f6; display: none; padding: 19px 62px;">
				<div >
					<!-- for Adult  -->
					<div class="row">
						<div class="col-md-2">
							<h4 style="float: left; margin-top: 26px;">
								<b><i class="fa fa-inr"></i> <c:out value="${gl.t3acost_per5}"
										escapeXml="true" default="" /></b>
							</h4>
						</div>
						<div class="col-md-3 text-center">
							<b><c:out value="${gl.eatype}" escapeXml="true" default="" /></b>

							<select name="quantity" id="${loopCounter.count+3}quantity"onchange="calculateTotalAmount(${loopCounter.count+3},${gl.t3acost_per5});"
								class="form-control">
								<option value="0">0</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
							</select>
						</div>
						<div class="col-md-5">
							<b><i class="fa fa-inr"
								style="float: left; margin-top: 21px; font-size: 16pt;"></i><input
								type="text" id="4totalamount" value="${gl.t3acost_per5}"
								style="margin-top: 10px; background: transparent; border-bottom: none; font-size: 20pt; width: 130px; margin-top: auto;" /></b>
						</div>
						<div style="display: none;" id="eventspiner">
							<img src="/resources/images/s.gif" style="width: 15%;" />
						</div>
						<button type="button" class="btn btn-warning"
							style="background: red; color: white; margin-left: 4%; border-radius: 0; margin-top: 16px;"
							onclick="detail('${gl.eaproduct_id}','${gl.t3acost_per5}')">Proceed
						</button>
					</div>
					<hr>
				</div>
			</div>
			<!-- -----end for 4 ticket----- -->
		</c:forEach>
	</div>
	
	<!-- END EVENTS DETAILS -->
<script>
	function btnsho() {	
			$("#btn2").show();
			$("#btn5").hide();
		}
	function btnsho1(){	
		$("#btn2").hide();
			$("#btn5").show();
		}
 </script>
 
 

	</div>
	<!-----end col-md-8-->

	<!---end row-->
	<!----end container-->
	<jsp:include page="/WEB-INF/jsp/User/Footer.jsp" />
	<script src="http://code.jquery.com/jquery-2.2.1.min.js"></script>
	<script src="<c:url value='/resources/js/bootstrap.js'/>"></script>
	<script>

  var myBox1="";
	 function calculateTotalAmount(x,myBox2)
	
	{
		  myBox1 = document.getElementById(x+"quantity").value;
			//var totalamount = document.getElementById('totalamount');	
			var myResult = myBox1 * myBox2;
			//alert(totalamount);
			
			document.getElementById(x+"totalamount").value = myResult;
			var textbox3 = document.getElementById('textbox3');
			textbox3.value=myResult;
			document.getElementById(x+"totalamount").innerHTML =""+myResult;
	}
		// DETAIL 
		
		function detail(id,amount) {
		
		 $("#eventspiner").show();
			$.ajax({
				type : "POST",
				contentType : "application/json",
				url : "/User/Adlabs/AdlabsUserDetail",
				data : JSON.stringify({
					"product_id" : "" +id + "",
					"quantity"  :""+myBox1+"",
					"amount"  :""+amount+""
					
				}),
				success : function(response) {
					//window.location("EventDetail");
					location.href = "AdlabsUserDetail";
					
					

				}
			});

		}
	</script>
	
	

</body>
</html>



