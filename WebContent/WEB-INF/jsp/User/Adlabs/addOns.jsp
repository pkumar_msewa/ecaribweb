<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
  <title></title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  
</head>
<style>
.quantity{
    border: none;
    padding: 7px;
    margin-left: -3px;
    margin-right: -3px;
    width: 56px;
	text-align: center;
	}
	.btn{
		background: deepskyblue;
    	color: white;
   		border-radius: 0;
		}
		#pri1{
		    float: left;
    		margin-left: 90px;
			}
			#box{
				padding: 14px;
				display:none;
				background: #e2f0ff;
				}
				#quen{
					float:right; 
					margin-right: 31px;
					}
					#line1{
						margin-bottom: 65px;
						}
						#new{
							padding: 7px;
    						background: white;
							}
							#btn{
								margin-top: 50px;
								}
								#top_form{
								    background: white;
									padding: 13px;
									border-bottom-left-radius: 17px;
									border-bottom-right-radius: 17px;
								    -webkit-box-shadow: 0px 25px 42px -23px rgb(210, 210, 210);
								    -moz-box-shadow: 0px 25px 42px -23px rgb(210, 210, 210);
								    box-shadow: 0px 25px 42px -23px rgb(210, 210, 210);
									}
									#tick{
										background: white;
										-webkit-box-shadow: 0px 11px 33px 1px rgba(210, 210, 210, 0.68);
										-moz-box-shadow: 0px 11px 33px 1px rgba(210, 210, 210, 0.68);
										box-shadow: 0px 11px 33px 1px rgba(210, 210, 210, 0.68);
										}
										.boxtt i{
											font-size: 30pt;
											}
											.boxtt{
											    float: left;
											    color: #d6d6d6;										   
												}
												.1111a{
													color: #d6d6d6;
													}
							@media screen and (max-width: 991px) {
								#newtext{
									text-align: right;
									}
									#btn{
										float:right;
									    margin-right: 25px;
									    margin-top: auto;
										}
										.dis{
											font-size: 9pt;
											}
											#pri1{
												margin-left: 20px;
												}											
								}
								@media screen and (max-width: 650px) {
										#quen{
											float:left;
										}
									}
									@media screen and (max-width: 550px) {
										#pri1{
											float: right;
											}
										}
										@media screen and (max-width: 397px) {
										.cartbtn{
										margin-left: 0px !important;
										}
										}
</style>

<script>
	var i=1;
		function a(){
		if(i==1){
		$("#btn").html("hide <i class='fa fa-arrow-up'></i>");
			document.getElementById("new").style.backgroundColor = "#e2f0ff";
			i=2;
			$("#box").toggle();}
			else{
			document.getElementById("new").style.backgroundColor = "white";
			i=1;
			$("#box").toggle();
	 		$("#btn").html("show <i class='fa fa-arrow-down'></i>");	
		}
	}
</script>

<script>
$(document).ready(function() {
	 console.log("under ready...");
	 var date='${date}';
	 var destination='${destination}';
	 $.ajax({
			type : "GET",
			contentType : 'application/json;charset=UTF-8',
			url : "/Imagica/SearchTickets",
		 data: "dateVisit=" + date + "&destination=" + destination,
		 	dataType : 'json',
			contentType : "application/json",
			success:function(data,textStatus,jqXHR){
				
				var visitDate=data.visitDate;
				var businessUnit=data.businessUnit;
				for(i=0;i<data.addOns.carAddonDetailList.length;i++){
					var typeId=data.addOns.carAddonDetailList[i].typeId;
					var type=data.addOns.carAddonDetailList[i].type;
					var currency=data.addOns.carAddonDetailList[i].currency;
					var quantity=data.addOns.carAddonDetailList[i].quantity;
					var totalAddOnCost=data.addOns.carAddonDetailList[i].totalAddOnCost;
					var addOnCost=data.addOns.carAddonDetailList[i].addOnCost;
					var title=data.addOns.carAddonDetailList[i].title;
					var aid=data.addOns.carAddonDetailList[i].aid;
					var imageUrl=data.addOns.carAddonDetailList[i].imageUrl;
					console.log(typeId);
					console.log(type);
					console.log(currency);
					console.log(totalAddOnCost);
					console.log(addOnCost);
					console.log(title);
					console.log(aid);
					console.log(imageUrl);
					
				}
				var photoTypeId=data.addOns.photoAddonDetail.typeId;
				var photoType=data.addOns.photoAddonDetail.type;
				var photoCurrency=data.addOns.photoAddonDetail.currency;
				var photoquantity=data.addOns.photoAddonDetail.quantity;
				var photototalAddOnCost=data.addOns.photoAddonDetail.totalAddOnCost;
				var photoaddOnCost=data.addOns.photoAddonDetail.addOnCost;
				var phototitle=data.addOns.photoAddonDetail.title;
				var photoaid=data.addOns.photoAddonDetail.aid;
				var snowDescription=data.addOns.snowMagicaAddOn.ticketDescription;
				var snowTitle=data.addOns.snowMagicaAddOn.title;
				var snowimageUrl=data.addOns.snowMagicaAddOn.imageUrl;
				console.log(photoTypeId);
				console.log(photoType);
				console.log(photoTypeId);
				console.log(photoCurrency);
				console.log(photototalAddOnCost);
				console.log(photoaddOnCost);
				console.log(snowDescription);
				console.log(snowTitle);
				console.log(snowimageUrl);
				for(i=0;i<data.addOns.snowMagicaAddOn.sessionList.length;i++){
					var sessionName=data.addOns.snowMagicaAddOn.sessionList[i].sessionName;
					console.log(sessionName);
				for(j=0;j<data.addOns.snowMagicaAddOn.sessionList[i].timeSlots.length;j++){
					var slotName=data.addOns.snowMagicaAddOn.sessionList[i].timeSlots[j].slotName;
					console.log(slotName);
					
				}	
				}
				var ticketType=data.addOns.snowMagicaAddOn.ticketType.type;
				var ticketSnowQuantity=data.addOns.snowMagicaAddOn.ticketType.quantity;
				var ticketSnowcostPerQty=data.addOns.snowMagicaAddOn.ticketType.costPerQty;
				var ticketSnowtotalcost=data.addOns.snowMagicaAddOn.ticketType.totalCost;
				console.log(ticketType);
				console.log(ticketSnowQuantity);
				console.log(ticketSnowcostPerQty);
				console.log(ticketSnowtotalcost);
				
			} 
	 });
});
</script>


<body style="background: aliceblue;">


<div class="container-fluid" id="tick">
	<div class="container" style="padding: 10px;">
    	<div class="row">
        	<a href=""><div class="col-sm-2 boxtt text-center" style="color: deepskyblue;"><i class="fa fa-ticket" aria-hidden="true"></i><br> TICKETS </div><i class="fa fa-chevron-right" style="color: deepskyblue; position: absolute;font-size: 34px;margin-top: 11px;"></i></a>
            <a href=""><div class="col-sm-2 boxtt text-center" style="color: deepskyblue;"><i class="fa fa-puzzle-piece" aria-hidden="true"></i><br> Addons</div><i class="fa fa-chevron-right" style="color: deepskyblue; position: absolute;font-size: 34px;margin-top: 11px;"></i></a>
            <a href=""><div class="col-sm-2 boxtt text-center"><i class="fa fa-user" aria-hidden="true"></i><br> Your Details</div><i class="fa fa-chevron-right" style="color:#d6d6d6; position: absolute;font-size: 34px;margin-top: 11px;"></i></a>
            <a href=""><div class="col-sm-2 boxtt text-center"><i class="fa fa-newspaper-o" aria-hidden="true"></i><br> Review Order</div><i class="fa fa-chevron-right" style="color:#d6d6d6; position: absolute;font-size: 34px;margin-top: 11px;"></i></a>
            <a href=""><div class="col-sm-2 boxtt text-center"><i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 60px;"></i></div></a>
        </div>
    </div>
</div>



<div class="container" id="top_form">
	<center>
    	<div class="row">
            <input type="text" placeholder="Book For" style="padding: 5px 7px;">
            <input type="text" placeholder="Date of Visit" style="padding: 5px 7px;">
            <button class="btn">Continue</button>    
    	</div>
    </center>
</div>
<br>

<div class="container" id="new">
	<a href="#" id="hidecart"><div><span style="font-size: 17pt;"><b>CART SUMMARY</b> <i class="fa fa-sort-desc" aria-hidden="true"></i></span><div class="pull-right" style="font-size: 12pt;"><span><b>Cart Total:</span> <span>Cart Total:</span> <span>
Visit Date: 27/05/2017</span></div></div> </a> 
    <hr style="margin-top:auto;"> 
	<div id="showcard" style="display:none;">
			<div class="row">
				<div class="col-sm-5 col-xs-4">
				<h4>Adult<small>(Above 12 Years)</small></h4>
				</div>
				<div id="pri1"><h4>1899.00 INR</h4></div>
				<div id="pri1"><h4>1899.00 INR</h4></div>
				<div id="quen">
					<button class="incr-btn btn" data-action="decrease"><i class="fa fa-minus" aria-hidden="true"></i></button>
						 <input class="quantity" type="text" name="quantity" value="1"/>
					<button class="incr-btn btn" data-action="increase"><i class="fa fa-plus" aria-hidden="true"></i></button>
				</div>
			</div>
			
			<div class="pull-right">
				<span style="font-size: 24px;">Cart total</span>&nbsp&nbsp&nbsp <span style="font-size: 24px;">999.00 INR</span><br>
				<span><i>Plus taxes as applicable.</i></span>
			</div>
	</div>
</div>

<br>
<div class="container">
	<div class="row" id="new">
    	<div class="col-md-2 col-sm-4 col-xs-6"><img src="bbe_3.png" width="100%" class="img-responsive" alt=""></div>
        <div class="col-md-8 col-sm-8">
        	<div class="col-md-8" id="newtext">
            	<h4><b>FAMILY OF 6</b></h4>
            <p class="dis">Money saver package! Get Regular Tickets for 6, Chauffeur driven A/C car travel for 6 from Mumbai/Pune to Imagica & back and All-day meal (lunch+snacks+dinner) at Imagica.</p>
            </div>
        </div>
        <div class="col-md-2">
    		<button id="btn" class="btn btn-success" onClick="a()">Show <i class="fa fa-arrow-down"></i></button>
        </div>
    </div>
    <br><br>
    <div class="row" id="new">
    	<div class="col-md-2 col-sm-4 col-xs-6"><img src="bbe_3.png" width="100%" class="img-responsive" alt=""></div>
        <div class="col-md-8 col-sm-8">
        	<div class="col-md-8" id="newtext">
            	<h4><b>FAMILY OF 6</b></h4>
            <p class="dis">Money saver package! Get Regular Tickets for 6, Chauffeur driven A/C car travel for 6 from Mumbai/Pune to Imagica & back and All-day meal (lunch+snacks+dinner) at Imagica.</p>
            </div>
        </div>
        <div class="col-md-2">
    		<button id="btn" class="btn btn-success" onClick="a()">Show <i class="fa fa-arrow-down"></i></button>
        </div>
    </div>
    <br><br>
    <div class="row" id="new">
    	<div class="col-md-2 col-sm-4 col-xs-6"><img src="bbe_3.png" width="100%" class="img-responsive" alt=""></div>
        <div class="col-md-8 col-sm-8">
        	<div class="col-md-8" id="newtext">
            	<h4><b>FAMILY OF 6</b></h4>
            <p class="dis">Money saver package! Get Regular Tickets for 6, Chauffeur driven A/C car travel for 6 from Mumbai/Pune to Imagica & back and All-day meal (lunch+snacks+dinner) at Imagica.</p>
            </div>
        </div>
        <div class="col-md-2">
    		<button id="btn" class="btn btn-success" onClick="a()">Show <i class="fa fa-arrow-down"></i></button>
        </div>
    </div>
    
    <div class="row" id="box" >
        <div id="line1">
		<div class="row">
        	<div class="col-sm-5 col-xs-4">
            <h4>Adult<small>(Above 12 Years)</small></h4>
            </div>
            <div id="pri1"><h4>1899.00 INR</h4></div>
            <div id="pri1"><h4>1899.00 INR</h4></div>
            <div id="quen">
                <button class="incr-btn btn" data-action="decrease"><i class="fa fa-minus" aria-hidden="true"></i></button>
                     <input class="quantity" type="text" name="quantity" value="1"/>
                <button class="incr-btn btn" data-action="increase"><i class="fa fa-plus" aria-hidden="true"></i></button>
            </div>
		</div>
		<div class="row">
        	<div class="col-sm-5 col-xs-4">
            <h4>Adult<small>(Above 12 Years)</small></h4>
            </div>
            <div id="pri1"><h4>1899.00 INR</h4></div>
            <div id="pri1"><h4>1899.00 INR</h4></div>
            <div id="quen">
                <button class="incr-btn btn" data-action="decrease"><i class="fa fa-minus" aria-hidden="true"></i></button>
                     <input class="quantity" type="text" name="quantity" value="1"/>
                <button class="incr-btn btn" data-action="increase"><i class="fa fa-plus" aria-hidden="true"></i></button>
            </div>
		</div>
		<div class="row">
        	<div class="col-sm-5 col-xs-4">
            <h4>Adult<small>(Above 12 Years)</small></h4>
            </div>
            <div id="pri1"><h4>1899.00 INR</h4></div>
            <div id="pri1"><h4>1899.00 INR</h4></div>
            <div id="quen">
                <button class="incr-btn btn" data-action="decrease"><i class="fa fa-minus" aria-hidden="true"></i></button>
                     <input class="quantity" type="text" name="quantity" value="1"/>
                <button class="incr-btn btn" data-action="increase"><i class="fa fa-plus" aria-hidden="true"></i></button>
            </div>
		</div>
		<div class="row">
        	<div class="col-sm-5 col-xs-4">
            <h4>Adult<small>(Above 12 Years)</small></h4>
            </div>
            <div id="pri1"><h4>1899.00 INR</h4></div>
            <div id="pri1"><h4>1899.00 INR</h4></div>
            <div id="quen">
                <button class="incr-btn btn" data-action="decrease"><i class="fa fa-minus" aria-hidden="true"></i></button>
                     <input class="quantity" type="text" name="quantity" value="1"/>
                <button class="incr-btn btn" data-action="increase"><i class="fa fa-plus" aria-hidden="true"></i></button>
            </div>
		</div><br>
			<div class="pull-right">
				<div style="float: left;"><h4 style="margin-bottom: auto; margin-right: 10px;"><b>Sub total : 4598.00 INR</b></h4>
				<span>Plus taxes as applicable.</span></div>
				<button type="button" class="btn cartbtn" style="margin-left: 30px; margin-right: 30px; margin-top: 10px;">ADD TO CART</button>
			</div>
        </div>
    </div>
</div>



<script>
    $(".incr-btn").on("click", function (e) {
        var $button = $(this);
        var oldValue = $button.parent().find('.quantity').val();
        $button.parent().find('.incr-btn[data-action="decrease"]').removeClass('inactive');
        if ($button.data('action') == "increase") {
            var newVal = parseFloat(oldValue) + 1;
        } else {
            if (oldValue > 1) {
                var newVal = parseFloat(oldValue) - 1;
            } else {
                newVal = 1;
                $button.addClass('inactive');
            }
        }
        $button.parent().find('.quantity').val(newVal);
        e.preventDefault();
    });
	
	$("#hidecart").click(function(){
		$("#showcard").toggle();
	});
</script>
</body>
</html>
    