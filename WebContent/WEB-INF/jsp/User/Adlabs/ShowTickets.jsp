<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en">
<head>
  <title></title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="/resources/css/new_css/default.css">
  <link rel="stylesheet" href="/resources/css/new_css/responsive.css">
  <link rel="stylesheet" href="/resources/css/new_css/uniform.default.min.css">
  
   <!-- Include Date Range Picker -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

<script>
	$(document).ready(function(){	 
	var date = new Date();
    var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
	var date_input=$('input[name="dateVisit"]');
	var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
	date_input.datepicker({
	   format: 'dd-mm-yyyy',
	   container: container,
	   maxDate: '0',
	   startDate: today,
	   todayHighlight: true,
	   autoclose: true,
	})
	 })
</script>

</head>
<style>
.quantity{
    border: none;
    padding: 7px;
    margin-left: -3px;
    margin-right: -3px;
    width: 56px;
	text-align: center;
	}
	.btn{
		background: #f35835;
    	color: white;
   		border-radius: 0;
		}
		.btn a{
    	color: white;
		}
		#pri1{
		    float: left;
    		margin-left: 90px;
			}
		
				#quen{
					float:right; 
					margin-right: 31px;
					}
					#line1{
					/*	margin-bottom: 65px;*/
						}
						.new{
							padding: 7px;
    						background: white;
    						    margin-bottom: 20px;
							}
							#btn{
								margin-top: 50px;
								}
								#top_form{
								    background: white;
									padding: 13px;
									border-bottom-left-radius: 17px;
									border-bottom-right-radius: 17px;
								    -webkit-box-shadow: 0px 25px 42px -23px rgb(210, 210, 210);
								    -moz-box-shadow: 0px 25px 42px -23px rgb(210, 210, 210);
								    box-shadow: 0px 25px 42px -23px rgb(210, 210, 210);
									}
									#tick{
										background: white;
										-webkit-box-shadow: 0px 11px 33px 1px rgba(210, 210, 210, 0.68);
										-moz-box-shadow: 0px 11px 33px 1px rgba(210, 210, 210, 0.68);
										box-shadow: 0px 11px 33px 1px rgba(210, 210, 210, 0.68);
										}
										.boxtt i{
											font-size: 30pt;
											}
											.boxtt{
											    float: left;
											    color: #d6d6d6;										   
												}
												.1111a{
													color: #d6d6d6;
													}
							@media screen and (max-width: 991px) {
								#newtext{
									text-align: right;
									}
									#btn{
										float:right;
									    margin-right: 25px;
									    margin-top: auto;
										}
										.dis{
											font-size: 9pt;
											}
											#pri1{
												margin-left: 20px;
												}											
								}
								@media screen and (max-width: 650px) {
										#quen{
											float:left;
										}
									}
									@media screen and (max-width: 550px) {
										#pri1{
											float: right;
											}
										}
										@media screen and (max-width: 397px) {
										.cartbtn{
										margin-left: 0px !important;
										}
										}
										
</style>

<script>
	var i=1;
		function a(id){
		if(i==1){
		$("#btn").html("hide <i class='fa fa-arrow-up'></i>");
			document.getElementById("new"+id).style.backgroundColor = "#e2f0ff";
			i=2;
			$("#"+id).show();}
			else{
			document.getElementById("new"+id).style.backgroundColor = "white";
			i=1;
			$("#"+id).hide();
	 		$("#btn").html("show <i class='fa fa-arrow-down'></i>");	
		}
	}
</script>
<script>
$(function() {
	$( "#datepicker" ).datepicker({
		dateFormat:'dd-mm-yyyy',
			 minDate: 0
	});
});
</script>

<script>

var d45="";
$(document).ready(function() {
	 console.log("under ready...");
	 var date='${date}';
	 var f="";
	 var destination='${destination}';
	 $.ajax({
			type : "GET",
			contentType : 'application/json;charset=UTF-8',
			url : "/Imagica/SearchTickets",
		 data: "dateVisit=" + date + "&destination=" + destination,
			dataType : 'json',
			contentType : "application/json",
			success:function(data,textStatus, jqXHR){
				console.log('i am in success');	 
			 for(i=0;i<data.packages.length;i++){
				var img=data.packages[i].imageUrl;
				var title=data.packages[i].title;
				var description=data.packages[i].description;
				var costper=data.packages[i].costPer;
				var currency=data.packages[i].currency;
				var passenger=data.packages[i].ticketDetail.passengerType;
				var qtyhh=data.packages[i].quantity;
				
				//console.log(passenger);
				$('#im_image').attr('src', img);
				$('#im_title').html(title);
				$('#im_description').html(description);
				$('#im_costper').html(costper);
				$('#im_currency').html(currency);
			//	$('#im_passenger_type').html(passenger);
			
			$('#packages_lol').append('<div class="row new" id="new">'+
			    	'<div class="col-md-2 col-sm-4 col-xs-6"><img src="'+img+'" width="100%" class="img-responsive" alt=""></div>'+
			       '<div class="col-md-8 col-sm-8">'+
			        	'<div class="col-md-8" id="newtext">'+
			            	'<h4><b>'+title+'</b></h4>'+
			            '<p class="dis">'+description+'</p>'+
			           ' </div>'+
			       '</div>'+
			        '<div class="col-md-2">'+
			    		'<button id="btn'+i+'" class="btn btn-success" onClick="a()">Show <i class="fa fa-arrow-down"></i></button>'+
			       '</div>'+
			    '</div>'+'<div class="row" id="'+j+'" style="padding: 14px;display:none;" >'+
			    '<br><br>');
			} 
			
			 var c="";
			 var amount="";
			 var userid="";
			 var contity="";
			 console.log(data.tickets.length);
			for(var j=0;j<data.tickets.length;j++){
				console.log(data.tickets.length);
				
				var image=data.tickets[j].image;
				var title_tic=data.tickets[j].title;
				var description_tic=data.tickets[j].description;
				var costper=data.tickets[j].costPer;
				var currency=data.tickets[j].currency;
				
				$('#hereonly').append('<div class="row new" id="new'+j+'"><div class="row" >'+
						'<div class="col-md-2 col-sm-4 col-xs-6"><img src="'+image+'"width="100%" class="img-responsive" alt="" id="image_tic'+j+'" value="'+image+'"></div>'+
				        '<form id="myForm'+j+'" method="post" action="Imagica/AddToCartTickets"><div class="col-md-8 col-sm-8">'+
				        	'<div class="col-md-8" id="newtext">'+
				            	'<h4><b id="title_tic'+j+'">'+title_tic+'</b></h4>'+  
				           ' <p class="dis" id="tic_description'+j+'">'+description_tic+'</p>'+
				            '</div>'+
				        '</div>'+
				       ' <div class="col-md-2">'+
				    		'<button id="btn"'+j+' type="button" class="btn btn-success" onClick="a('+j+')">Show <i class="fa fa-arrow-down"></i></button>'+
				       ' </div>'+
				   '</div><div>'+ '<div class="row" id="'+j+'" style="padding: 14px;display:none;" >'
			        		

			        	);
				
				for(k=0;k<data.tickets[j].ticketTypeList.length;k++){
					var tic_type=data.tickets[j].ticketTypeList[k].type;
					console.log(tic_type);
					var quantity=data.tickets[j].ticketTypeList[k].quantity;
					var cost_per_qty=data.tickets[j].ticketTypeList[k].costPerQty;
					var total_cost=data.tickets[j].ticketTypeList[k].totalCost;
					var product_id=data.tickets[j].ticketTypeList[k].productId;
					console.log(product_id);
					console.log(cost_per_qty);
					
			
			var cgst=data.tickets[j].ticketTypeList[k].taxDetailsList[0].pricePerQty;
			var prod_price=(cost_per_qty+cgst+cgst).toFixed(2);
			console.log('the total price is:'+prod_price);
			var forIncrDecr=Math.random()+Math.random();
			amount+=""+prod_price+"!";
			userid+=""+product_id+"@"
		   
		   
		c+='<div id="line1">'+
		'<div class="row">'	+ '<div class="col-sm-5 col-xs-4">'+
        '<h4>'+tic_type+'<small></small></h4>'+
       '</div>'+
        '<div class="col-md-4" id="pri"'+k+'><h4 id="cost_per_Qty'+k+'">'+prod_price+'</h4></div>'+
        '<input type="hidden" id="product_id'+k+'" value="'+product_id+'"/>'+
        '<input type="hidden" id="tax_prod'+k+'" value="'+cgst+'"/>'+
        '<div class="col-md-2" id="quen">'+
            '<button class="incr-btn btn" data-action="decrease" id="decr" onclick="decrement('+forIncrDecr+')"><i class="fa fa-minus" aria-hidden="true"></i></button>'+
               '  <input class="quantity" type="number" name="quantity" id="quantity'+forIncrDecr+'" value='+quantity+'>'+
            '<button class="incr-btn btn"  onclick="increment('+forIncrDecr+')" id="incr"><i class="fa fa-plus" aria-hidden="true"></i></button>'+'<br><br>'+
        '</div>'+
	'</div></div>';
	
			 contity+="quantity"+forIncrDecr+"#";
			
				}
				var imagicadata12="~"+amount+"~"+userid;
				d45+="<input type='button' class='btn pull-right' style='margin-right:6%;' id='myButton"+j+"' value='ADD TO CART' onclick=takemyvalues('"+j+"','"+k+"','"+imagicadata12+"','"+contity+"')>";
				 $("#"+j).append(c).append(d45);
				 c="";
				 d45="";
				 amount="";
					userid="";
				    contity="";
			}
				}
			});
			
				});

</script>

<script type="text/javascript">

    function takemyvalues(value1,value2,imagicadata12,contity){
    		console.log(value1);
    		console.log(imagicadata12);
    		var contity1=new Array();
    		contity1=contity.split("#");
    		
    		var c="";
    		for (var i = 0; i < contity1.length-1; i++)
    		{
    			
    			var d=document.getElementById(""+contity1[i]).value;
				c+=d+"#"
			}
    		var alldata=imagicadata12+"~"+c;
    		var prod="";//document.getElementById("product_id"+value2-1).value;
    		var prod_cst="";//document.getElementById("cost_per_Qty"+value2-1).innerHTML;
    		/* console.log(prod);
    		console.log(prod_cst); */
    		var qty="";//document.getElementById("quantity"+value2-1).value;
    		var cgst="";//document.getElementById("tax_prod"+value2-1).value;
    		var image="";//document.getElementById("image_tic"+value1).src;
    		var title="";//document.getElementById("title_tic"+value1).innerHTML;
    		var descr="";//document.getElementById("tic_description"+value1).innerHTML;
    		/* console.log("the descr:"+descr);
    		console.log("the title:"+title);
    		console.log(image); */
    		
    		console.log("all datas" + alldata)
    		
    		
    		
    		$.ajax({
    	        url:'/Imagica/AddToCartTickets',
    	        type:'post',
    	        dataType : 'json',
    	        contentType : "application/json",
    			data :JSON.stringify({"productId":""+alldata+"","productPrice":""+prod_cst+"","productQuantity":""+qty+"","title":""+title+"","imageUrl":""+image+""}),
    	        success:function(data,textStatus, jqXHR){
    	            //whatever// you wanna do after the form is successfully submitted
    	           document.getElementById("shopping_cart").innerHTML=data.message; 
    	          document.getElementById('myButton'+value1).disabled=true;
    	            
    	        }
    	    });
    		//document.getElementById("myForm"+value).submit();
    		
     
    }
</script>
<body style="background: aliceblue;">

<!--------->
<jsp:include page="/WEB-INF/jsp/User/Header.jsp" />

<!--------->


<div class="container-fluid" id="tick">
<div style="-webkit-box-shadow: inset -1px 11px 24px -10px rgba(0,0,0,0.54);-moz-box-shadow: inset -1px 11px 24px -10px rgba(0,0,0,0.54);box-shadow: inset -1px 11px 24px -10px rgba(0,0,0,0.54);margin-left: -15px;margin-right: -15px;">
	<div class="container" style="padding: 10px;">
    	<div class="row">
        	<div class="col-sm-2 boxtt text-center" style="color: #f35835;"><i class="fa fa-ticket" aria-hidden="true"></i><br> TICKETS </div><i class="fa fa-chevron-right" style="color: #0a9f8c; position: absolute;font-size: 34px;margin-top: 11px;"></i></a>
            <div class="col-sm-2 boxtt text-center"><i class="fa fa-puzzle-piece" aria-hidden="true"></i><br> Addons</div><i class="fa fa-chevron-right" style="color: #d6d6d6; position: absolute;font-size: 34px;margin-top: 11px;"></i>
            <div class="col-sm-2 boxtt text-center"><i class="fa fa-user" aria-hidden="true"></i><br> Your Details</div><i class="fa fa-chevron-right" style="color:#d6d6d6; position: absolute;font-size: 34px;margin-top: 11px;"></i>
            <div class="col-sm-2 boxtt text-center"><i class="fa fa-newspaper-o" aria-hidden="true"></i><br> Review Order</div><i class="fa fa-chevron-right" style="color:#d6d6d6; position: absolute;font-size: 34px;margin-top: 11px;"></i>
            <div class="col-sm-2 boxtt text-center"><i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 60px;" id=""></i><p style="    font-size: 13pt;
    color: #f35835; font-weight: 800;" id="shopping_cart"></p></div>
        </div>
    </div>
</div>

</div>

<div class="container" id="top_form">
	<center>
    	<div class="row">
    	<form class="form-inline"  action="${pageContext.request.contextPath}/Imagica/SearchITickets" method="GET">
             <select required aria-required="true" class="form-control" style="padding: 5px 7px;" name="destination" id="destination">
                         <option value="THEME_PARK">Theme Park</option>
                         <option value="WATER_PARK">Water Park</option>									
                        </select>
            <input  id="datepicker"  name="dateVisit"  value="${date}" type="text" placeholder="Enter Date" style="padding: 5px 7px;" readonly/>
            <button class="btn" type="submit">Continue</button>    
            </form>
    	</div>
    </center>
</div>

<div class="container">
	<h3><b>SELECT YOUR TICKETS</b></h3>
    <hr style="margin-top:auto;">    
	
	<div id="packages_lol"></div>
    <div id="hereonly"></div>

 
 
          <center>  <button class="incr-btn btn"><a href="<c:url value="/Imagica/ShowDetails"/>">PROCEED WITHOUT ADDONS</a></button></center>
    
<!-- <script>

    function increment(id){
    console.log('hi m here');
    	alert(id);
      
        var oldValue =document.getElementById(""+id).value;
        alert("get value"+oldValue);
            var newVal = parseInt(""+oldValue) + 1;
            alert("incrementval"+newVal);
            $("#"+id).val(""+newVal);
       
    }
</script> -->
<!-- <script>
function increment(id) {
    var $button = $(this);
    var oldValue = $button.parent().find('.quantity').val();
    $button.parent().find('.incr-btn[data-action="decrease"]').removeClass('inactive');
    if ($button.data('action') == "increase") {
        var newVal = parseFloat(oldValue) + 1;
    } else {
        if (oldValue > 1) {
            var newVal = parseFloat(oldValue) - 1;
        } else {
            newVal = 1;
            $button.addClass('inactive');
        }
    }
    $button.parent().find('.quantity').val(newVal);
    e.preventDefault();
}
</script> -->
<script>
function increment(value1) {
	  var value = parseInt(document.getElementById('quantity'+value1).value, 10);
	  value = isNaN(value) ? 0 : value;
	  value++;
	  document.getElementById('quantity'+value1).value = value;
	}

	function decrement(value2) {
	  var value = parseInt(document.getElementById('quantity'+value2).value, 10);
	  value = isNaN(value) ? 0 : value;
	  value < 1 ? value = 1 : '';
	  value--;
	  document.getElementById('quantity'+value2).value = value;
	}
</script>
</body>
</html>
