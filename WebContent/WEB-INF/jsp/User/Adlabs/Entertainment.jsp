<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage=""%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html lang="en">
<head>
<meta charset="utf-8">
<sec:csrfMetaTags/>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>VPayQwik |Entertainment</title>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>

	<link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'
	type="image/png" />
<link rel="stylesheet" href="/resources/css/new_css/cloud-admin.css">
<link rel="stylesheet" href="/resources/css/font-awesome.min.css">
<link rel="stylesheet" href="/resources/css/new_css/default.css">
<link rel="stylesheet" href="/resources/css/new_css/responsive.css">
<link rel="stylesheet"
	href="/resources/css/new_css/uniform.default.min.css">
<link rel='stylesheet' href="/resources/css/css_style.css">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>

<script type="text/javascript"
	src="/resources/js/new_js/jquery.min.js"></script>
	<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>

	<link rel="icon" href='<c:url value="/resources/images/favicon.png"/>' type="image/png" />

	<script type="text/javascript" src="<c:url value="/resources/js/userdetails.js"/>"></script>

	<style>
		.cstmfrm {
			width: 255px;
		    float: right;
		    background: rgba(245,245,245,0.8);
		    color: #FFFFFF;
		    display: block;
		    padding: 20px 15px 20px 15px;
		    border-radius: 5px;
		    -moz-border-radius: 5px;
		    -webkit-border-radius: 5px;
		    -khtml-border-radius: 5px;
		    box-shadow: 0 0 20px 2px #8b8b8b;
		}
		#searchOfferSection {
			position: absolute;
		    top: 235px;
		    left: 0;
		    right: 0;
		    z-index: 0;
		    padding: 0;
		}
		#myCarousel {
			margin-top: -19px;
		    margin-left: -14px;
		    margin-right: -14px;
		}
		.carousel-inner {
			height: 100vh;
		}
		.positionAbsolute {
			position: absolute;
			top: 4%;
			right: 6%;
		}
		.positionAbsolute h3 {
			font-size: 26px;
		    color: orange;
		    font-weight: 600;
		}
		/*.positionAbsolute p {
			background: yellow;
			color: blue;
		}*/
		.left-ctrl {
			height: 43px;
		    width: 43px;
		    margin-top: 20%;
		    background: black;
		    border-radius: 50%;
		}
		.right-ctrl {
			height: 43px;
		    width: 43px;
		    margin-top: 20%;
		    background: black;
		    border-radius: 50%;
		}
		.left-ctrl .glyphicon-chevron-left {
			font-size: 20px;
    		margin-left: -15px;
		}
		.right-ctrl .glyphicon-chevron-right {
			font-size: 20px;
    		margin-right: -15px;
		}
	</style>

	<script type="text/javascript">
		$(window).load(function() {
			$(".se-pre-con").fadeOut("slow");
		});
	</script>
	
</head>
<body style="overflow-x: hidden;">
	<div class="se-pre-con"></div>
	<jsp:include page="/WEB-INF/jsp/User/Header.jsp" />

	<div class="container-fluid">
		  <div id="myCarousel" class="carousel slide" data-ride="carousel">
		    <!-- Indicators -->
		    <!-- <ol class="carousel-indicators">
		      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
		      <li data-target="#myCarousel" data-slide-to="1"></li>
		      <li data-target="#myCarousel" data-slide-to="2"></li>
		    </ol> -->

		    <!-- Wrapper for slides -->
		    <div class="carousel-inner">

		      <div class="item active">
		        <img src="/resources/images/images/banner_1.jpg" alt="" style="width:100%;">
		        <div class="positionAbsolute">
		          <h3>Enjoy the #Longest#Long#Weekend-12th-20th-Aug</h3>
		          <!-- <p>Save on GST and get flat 28%off</p> -->
		        </div>
		      </div>

		      <div class="item">
		        <img src="/resources/images/images/banner_2.jpg" alt="" style="width:100%;">
		        <div class="positionAbsolute">
		          <h3>Vacation Ka Naya Formula</h3>
		          <!-- <p>Experience Thematic Show,Thirlls rides, Multi-cuisine Resturants</p> -->
		        </div>
		      </div>
		    
		      <div class="item">
		        <img src="/resources/images/images/banner_3.jpg" alt="New York" style="width:100%;">
		        <div class="positionAbsolute">
		          <h3>Travel to imagica for free every Sunday</h3>
		          <!-- <p>We love the Big Apple!</p> -->
		        </div>
		      </div>
		  
		    </div>

		    <!-- Left and right controls -->
		    <!-- <a class="left carousel-control left-ctrl" href="#myCarousel" data-slide="prev">
		      <span class="glyphicon glyphicon-chevron-left"></span>
		      <span class="sr-only">Previous</span>
		    </a>
		    <a class="right carousel-control right-ctrl" href="#myCarousel" data-slide="next">
		      <span class="glyphicon glyphicon-chevron-right"></span>
		      <span class="sr-only">Next</span>
		    </a> -->
		  </div>

		</div>

		<div id="searchOfferSection" class="container">
			<div class="row">
				<div class="col-md-4">
					<div class="cstmfrm">
					  	<form action="${pageContext.request.contextPath}/Imagica/SearchITickets" method="GET">
					  		<div class="form-group">
							  <select class="form-control" id="sel1" name="destination" id="destination">
							    <option value="THEME_PARK">Theme Park</option>
							    <option value="THEME_PARK">Water Park</option>
							  </select>
							</div>
					  		<div class="form-group">
					  			<input type="text" class="form-control" placeholder="Enter Date" name="dateVisit">
					  		</div>
					  		<button class="btn btn-primary btn-block" type="submit">Book</button>
					  	</form>
					  </div>
				</div>
			</div>
		</div>

	<jsp:include page="/WEB-INF/jsp/User/Footer.jsp" />	


	
	<script>
	$(document).ready(function(){	 
	var date = new Date();
    var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
	var date_input=$('input[name="dateVisit"]');
	var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
	date_input.datepicker({
	   format: 'dd-mm-yyyy',
	   container: container,
	   maxDate: '0',
	   startDate: today,
	   todayHighlight: true,
	   autoclose: true,
	})
	 })
</script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
</body>
</html>