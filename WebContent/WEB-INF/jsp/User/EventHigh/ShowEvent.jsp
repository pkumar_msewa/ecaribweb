<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	errorPage=""%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<html lang="en">
<head>
<meta charset="utf-8">
<sec:csrfMetaTags />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>VPayQwik | EVENT Lists</title>
<link rel="stylesheet"
	href="<c:url value="/resources/css/font-awesome.min.css"/>">
<link href='<c:url value='/resources/css/font-family.css'/>'
	rel='stylesheet' type='text/css'>
<link rel="stylesheet"
	href="<c:url value='/resources/css/font-awesome.css'/>" type='text/css'>

<!-- Optional theme -->
<link rel="stylesheet"
	href="<c:url value='/resources/css/bootstrap-theme.min.css'/>"
	type='text/css'>

<link href="<c:url value="/resources/css/css_style.css"/>"
	rel='stylesheet' type='text/css'>
<link rel="stylesheet"
	href="<c:url value='/resources/css/bootstrap.css'/>" type='text/css'>
<script src="<c:url value='/resources/js/jquery.js'/>"></script>
<script src="<c:url value='/resources/js/bootstrap.min.js'/>"></script>

<script type="text/javascript"
	src="<c:url value="/resources/js/userdetails.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/header.js"/>"></script>
<link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'
	type="image/png" />
<link href='<c:url value="/resources/css/css_style.css"/>'
	rel='stylesheet' type='text/css'>
<style>
.no-js #loader {
	display: none;
}

.js #loader {
	display: block;
	position: absolute;
	left: 100px;
	top: 0;
}

.se-pre-con {
	position: fixed;
	left: 0px;
	top: 0px;
	width: 100%;
	height: 100%;
	z-index: 9999;
	background: url(/images/pq_large.gif) center no-repeat #fff;
}
</style>
<script src="<c:url value='/resources/js/modernizr.js'/>"></script>

<script type="text/javascript">
	$(window).load(function() {
		$(".se-pre-con").fadeOut("slow");
	});
</script>

</head>

<body>
	<div class="se-pre-con"></div>
	<jsp:include page="/WEB-INF/jsp/User/Header.jsp" />

	<!-----------------end navbar---------------------->


	<!------------- end main-------------------->


	<div class="container" id="aboutus" style="margin-top: -80px;">
	<div class="col-md-12">
				<div class="slider" id="slider"
					style="margin-right: -15px; margin-left: -15px;  margin-top: 15px;"">
					<div class="carousel slide hidden-xs" data-ride="carousel"
						id="mycarousel">
						<ol class="carousel-indicators">
							<li class="active" data-slide-to="0" data-target="#mycarousel"></li>
							<li data-slide-to="1" data-target="#mycarousel"></li>
						</ol>

						<div class="carousel-inner">

							<div class="item active" id="slide1">

								<img src='<c:url value="/resources/images/slider2.jpg"/>' />
							</div>
							<!---end item---->

							<div class="item">
								<img src='<c:url value="/resources/images/slider1.jpg"/>' />
							</div>

							<!---end item---->
						</div>
						<!--end carousel inner------>

					</div>

					<!---end caeousel slider---->
				</div>
	
	<center><H2><b>Choose And Click on your Favourite Gift Cards</b></H2></center>
            <h3 style="margin-left: 400px; color:red;">${msg}</h3>
	
		
		<c:forEach items="${ShowEvent}" var="gl" varStatus="loopCounter">
			<!-- <div class="col-sm-3 col-xs-6">  -->
	
				<%-- <form action="${pageContext.request.contextPath}/User/Eventhigh/Events" method="get">
				 --%>
				
			
				
			 	<img src="${gl.img_url}"  style="width:350px; height:200px; margin-top: 60px;" />
			 	<div class="info">
			 		<p> ${gl.title} </p>
			 		<c:forEach items="${gl.eventClass}" var="gll" varStatus="loopCounter">
			 		<c:out value="${gll.date}" />
			 		
			 		</c:forEach>
			 		
			 		
			 	</div>
			 	 <button type="button" class="btn btn-warning"
						style="background: red; color: white; border-radius: 0; margin-top: 16px;"
						onclick="detail('${gl.id}')">SHow Detail
						</button> 
	
<!-- </div> -->
				
	
		</c:forEach>
	</div>
	</div>
	
  
 
	 <script>

 	
 	

 
 	
 
 
</script>
	</div>

	 <script>
		// DETAIL 
		function detail(id) {
			$.ajax({
				type : "POST",
				contentType : "application/json",
				url : "/User/Eventhigh/EventDetail",
				data : JSON.stringify({
					"id" : "" +id + "",
					
				}),
				success : function(response) {
					// alert(response);
					
					//window.location("EventDetail");
					location.href = "EventDetail";
					
					

				}
			});

		}
	
	
	
	
	
	
	</script>
	
	
	
	
	
<jsp:include page="/WEB-INF/jsp/User/Footer.jsp" />

    <script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
</body>
</html>

