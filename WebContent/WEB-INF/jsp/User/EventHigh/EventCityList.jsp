<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	errorPage=""%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<html lang="en">
<head>
<meta charset="utf-8">
<sec:csrfMetaTags />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>VPayQwik | EVENT Detail</title>
<link rel="stylesheet"
	href="<c:url value="/resources/css/font-awesome.min.css"/>">
<link href='<c:url value='/resources/css/font-family.css'/>'
	rel='stylesheet' type='text/css'>
<link rel="stylesheet"
	href="<c:url value='/resources/css/font-awesome.css'/>" type='text/css'>

<!-- Optional theme -->
<link rel="stylesheet"
	href="<c:url value='/resources/css/bootstrap-theme.min.css'/>"
	type='text/css'>

<link href="<c:url value="/resources/css/css_style.css"/>"
	rel='stylesheet' type='text/css'>
<link rel="stylesheet"
	href="<c:url value='/resources/css/bootstrap.css'/>" type='text/css'>
<script src="<c:url value='/resources/js/jquery.js'/>"></script>
<script src="<c:url value='/resources/js/bootstrap.min.js'/>"></script>

<script type="text/javascript"
	src="<c:url value="/resources/js/userdetails.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/header.js"/>"></script>
<link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'
	type="image/png" />
<link href='<c:url value="/resources/css/css_style.css"/>'
	rel='stylesheet' type='text/css'>
<style>
.no-js #loader {
	display: none;
}

.js #loader {
	display: block;
	position: absolute;
	left: 100px;
	top: 0;
}

.se-pre-con {
	position: fixed;
	left: 0px;
	top: 0px;
	width: 100%;
	height: 100%;
	z-index: 9999;
	background: url(/images/pq_large.gif) center no-repeat #fff;
}
</style>
<script src="<c:url value='/resources/js/modernizr.js'/>"></script>

<script type="text/javascript">
	$(window).load(function() {
		$(".se-pre-con").fadeOut("slow");
	});
</script>

</head>

<body>
	<div class="se-pre-con"></div>
	<jsp:include page="/WEB-INF/jsp/User/Header.jsp" />

	<!-----------------end navbar---------------------->


	<!------------- end main-------------------->

<div class="container" id="box">
	<div class="row">
		<div class="col-md-4" style="margin-top: 10%;">
            <form action="${pageContext.request.contextPath}/User/Eventhigh/Events" method="get">
                                  
                          <div class="group_1">
							<select name="city" id="pre_operator"
									class="form-control" style="width: 86%; border: transparent;border-bottom: gray;border-style: solid;border-width: 1.8px; font-family: 'Ubuntu', sans-serif; padding-left: 0; height: 55px; margin-top: -19px; font-weight: bold; color: #928F8F; box-shadow: none; border-radius: 0;">
									<option value="#">Select City</option>
									<option value="bangalore">bangalore</option>
                                        <option value="delhi">delhi</option>
                                        <option value="mumbai">mumbai</option>
								</select>
							</div>
							
							<div class="group_1">     
                                  <button type="submit" class="btn btn-primary">Search</button>
                                  </div>
                                </form>
                                
                                
                         
	</div>
	
	<div class="col-md-7 hidden-xs">
				<div class="slider" id="slider"
					style="margin-right: -15px; margin-left: -15px;  margin-top: 15px;"">
					<div class="carousel slide hidden-xs" data-ride="carousel"
						id="mycarousel">
						<ol class="carousel-indicators">
							<li class="active" data-slide-to="0" data-target="#mycarousel"></li>
							<li data-slide-to="1" data-target="#mycarousel"></li>
						</ol>

						<div class="carousel-inner">

							<div class="item active" id="slide1">

								<img src='<c:url value="/resources/images/gift2.jpg"/>' />
							</div>
							<!---end item---->

							<div class="item">
								<img src='<c:url value="/resources/images/gift2.jpg"/>' />
							</div>

							<!---end item---->
						</div>
						<!--end carousel inner------>

					</div>

					<!---end caeousel slider---->
				</div>
				<!---end slider----->
			</div>
</div>
</div>


