<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage=""%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<sec:csrfMetaTags />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta http-equiv="X-Frame-Options" content="deny">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <title>UPI Failed</title>
    
	<link rel="icon" href='<c:url value="/resources/images/favicon.png"/>' type="image/png" />
	<link rel="stylesheet" href="/resources/css/new_css/cloud-admin.css">
	<link rel="stylesheet" href="/resources/css/font-awesome.min.css">
	<link rel="stylesheet" href="/resources/css/new_css/default.css">
	<link rel="stylesheet" href="/resources/css/new_css/responsive.css">
	<link rel="stylesheet" href="/resources/css/new_css/uniform.default.min.css">
	<link rel='stylesheet' href="/resources/css/css_style.css">
	<!-- <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'> -->

	<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> -->
	<!-- <script type="text/javascript" src="/resources/js/new_js/jquery.min.js"></script> -->
		
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>
	<script type="text/javascript" src="/resources/js/new_js/bootstrap.min.js"></script>
	<!-- <script type="text/javascript" src="/resources/js/new_js/jquery.easypiechart.min.js"></script> -->
	<!-- <script type="text/javascript" src="/resources/js/new_js/jquery.sparkline.min.js"></script> -->
		
	<script src="/resources/js/new_js/script.js"></script>
	<script type="text/javascript" src="<c:url value="/resources/js/aj.js"/>"></script>
	<!-- <script type="text/javascript" src="<c:url value="/resources/js/topup.js"/>"></script> -->
	<script type="text/javascript" src="<c:url value="/resources/js/header.js"/>"></script>
	<%-- <script type="text/javascript" src="<c:url value="/resources/js/userdetails.js"/>"></script> --%>
	<!-- <script type="text/javascript" src="<c:url value="/resources/js/sendmoney.js"/>"></script> -->
	<!-- <script type="text/javascript" src="<c:url value="/resources/js/billpay.js"/>"></script> -->
	<!-- <script type="text/javascript" src="<c:url value="/resources/js/invitefriends.js"/>"></script> -->

	<!-- <script src="${pageContext.request.contextPath}/resources/js/Admin/jquery.twbsPagination.js"></script> -->
	<!-- <script src="${pageContext.request.contextPath}/resources/js/Admin/jquery.twbsPagination.min.js"></script> -->
	<!-- <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/datepicker.css"> -->
		
	<!-- <script src="${pageContext.request.contextPath}/resources/js/datepicker.js"></script> -->

	<!-- <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/new_css/datetimepicker.css"> -->
	<!-- <script src="${pageContext.request.contextPath}/resources/js/new_js/datetimepicker.js"></script> -->
</head>
	<style>
	.footer_title {
		bottom: 0;
	    position: absolute;
	    width: 100%;
	}
		.err-msg .err-bdy span {
			font-size: 18px;
		    font-weight: 600;
		    color: #6d6e72;
		    line-height: 1.92233;
		    letter-spacing: 0.3pt;
		}
		.err-msg {
			padding-top: 110px;
		}
		p {
			color: #6d6e72;
		    font-size: 18px;
		    margin-top: 15px;
		    font-weight: 500;
		}
		.transition10s {
		    transition: 10s ease-in-out;
		    -moz-transition: 10s ease-in-out;
		    -ms-transition: 10s ease-in-out;
		    -webkit-transition: 10s ease-in-out;
		    -o-transition: 10s ease-in-out;
		}
		.transition300 {
		    transition: 300ms ease-in-out;
		    -moz-transition: 300ms ease-in-out;
		    -ms-transition: 300ms ease-in-out;
		    -webkit-transition: 300ms ease-in-out;
		    -o-transition: 300ms ease-in-out;
		}
		.fLeft {
		    float: left !important;
		}
		/*Track Timer*/
		.timerCont{width: 15%;display: inline-block;vertical-align: middle;}
		.loader{position: relative;width: 150px; height:150px; margin:auto;-webkit-user-select: none;-moz-user-select: none;-o-user-select: none; user-select: none;-webkit-box-sizing: border-box;-moz-box-sizing: border-box;-o-box-sizing: border-box;box-sizing: border-box;padding:15px; background: #f0f0f0;border-radius: 100px;}
		.loader-bg{width: 100%;height: 100%;border-radius: 50%;-webkit-box-sizing: border-box;-moz-box-sizing: border-box;-o-box-sizing: border-box;box-sizing: border-box;background: #fff;box-shadow: 0 0 15px 0 rgba(0,0,0,0.2)}
		.spiner-holder-1{position: absolute; top:0;left:0; overflow: hidden;width: 50%;height: 50%;background: transparent;-webkit-box-sizing: border-box;-moz-box-sizing: border-box;-o-box-sizing: border-box;box-sizing: border-box;}
		.spiner-holder-2{position: absolute;top:0;left:0;overflow: hidden;width: 100%;height: 100%;background: transparent;-webkit-box-sizing: border-box;-moz-box-sizing: border-box;-o-box-sizing: border-box;box-sizing: border-box;}
		.loader-spiner{width: 200%;height: 200%;border-radius: 50%;border: 15px solid #cf2929;-webkit-box-sizing: border-box;-moz-box-sizing: border-box;-o-box-sizing: border-box;box-sizing: border-box;}
		.animate-0-25-a{-webkit-transform:rotate(90deg);-moz-transform:rotate(90deg);-o-transform:rotate(90deg);transform: rotate(90deg);-webkit-transform-origin:100% 100%;-moz-transform-origin:100% 100%;-o-transform-origin:100% 100%;transform-origin: 100% 100%;}
		.animate-0-25-b{-webkit-transform:rotate(-90deg);-moz-transform:rotate(-90deg);-o-transform:rotate(-90deg);transform: rotate(-90deg);-webkit-transform-origin:100% 100%;-moz-transform-origin:100% 100%;-o-transform-origin:100% 100%;transform-origin: 100% 100%;}
		.animate-25-50-a{-webkit-transform:rotate(180deg);-moz-transform:rotate(180deg);-o-transform:rotate(180deg);transform: rotate(180deg);-webkit-transform-origin:100% 100%;-moz-transform-origin:100% 100%;-o-transform-origin:100% 100%;transform-origin: 100% 100%;}
		.animate-25-50-b{-webkit-transform:rotate(-90deg);-moz-transform:rotate(-90deg);-o-transform:rotate(-90deg);transform: rotate(-90deg);-webkit-transform-origin:100% 100%;-moz-transform-origin:100% 100%;-o-transform-origin:100% 100%;transform-origin: 100% 100%;}
		.animate-50-75-a{-webkit-transform:rotate(270deg);-moz-transform:rotate(270deg);-o-transform:rotate(270deg);transform: rotate(270deg);-webkit-transform-origin:100% 100%;-moz-transform-origin:100% 100%;-o-transform-origin:100% 100%;transform-origin: 100% 100%;}
		.animate-50-75-b{-webkit-transform:rotate(-90deg);-moz-transform:rotate(-90deg);-webkit-transform:rotate(-90deg);-o-transform:rotate(-90deg);transform: rotate(-90deg);-webkit-transform-origin:100% 100%; -moz-transform-origin:100% 100%;-o-transform-origin:100% 100%; transform-origin:100% 100%;}
		.animate-75-100-a{-webkit-transform:rotate(0deg);-moz-transform:rotate(0deg);-o-transform:rotate(0deg);transform: rotate(0deg);-webkit-transform-origin:100% 100%;-moz-transform-origin:100% 100%;-o-transform-origin:100% 100%;transform-origin: 100% 100%;}
		.animate-75-100-b{-webkit-transform:rotate(-90deg);-moz-transform:rotate(-90deg);-o-transform:rotate(-90deg);transform: rotate(-90deg);-webkit-transform-origin:100% 100%;-moz-transform-origin:100% 100%;-o-transform-origin:100% 100%;transform-origin: 100% 100%;}
		.text{padding-top: 38%;text-align: center;color: #cf2929;font-size: 26px;line-height: 20px; font-weight: 700;}

		.border0{border-color: #cf2929;}
		.border25{border-color: #cf2929;}
		.border50{border-color: #cf2929;}
		.border75{border-color: #cf2929;}
		.timetitle{width:85%;line-height: 30px;padding:5px 20px;    display: inline-block;vertical-align: middle;}
	</style>

<script type="text/javascript">
$(document).ready(function(){
	window.location = "UpiRedirectSuccess";
		});
	</script>
<body style="overflow-x: hidden;">
	<jsp:include page="/WEB-INF/jsp/User/Header.jsp" />
	
	
	<div class="container">
		<div class="col-md-12">
			<div class="col-md-8 col-md-offset-2">
				<center>
					<div class="err-msg">
						<div class="err-hed">
							<div class="err-img">
								<div class="timerCont transition300" id="timer_div">
								  <div class="loader fLeft">
								    <div class="loader-bg">
								      <div id="timer" class="text fmUbuntuL transition10s"></div>
								    </div>
								    <div class="spiner-holder-1 animate-0-25-a">
								      <div class="spiner-holder-2 animate-0-25-b" style="transform: rotate(-90deg);">
								        <div class="loader-spiner transition10s border75">
								        </div>
								      </div>
								    </div>
								    <div class="spiner-holder-1 animate-25-50-a">
								      <div class="spiner-holder-2 animate-25-50-b" style="transform: rotate(-90deg);">
								        <div class="loader-spiner transition10s border75"></div>
								      </div>
								    </div>
								    <div class="spiner-holder-1 animate-50-75-a">
								      <div class="spiner-holder-2 animate-50-75-b" style="transform: rotate(-90deg);">
								        <div class="loader-spiner transition10s border75"></div>
								      </div>
								    </div>
								    <div class="spiner-holder-1 animate-75-100-a">
								      <div class="spiner-holder-2 animate-75-100-b" style="transform: rotate(-90deg);">
								        <div class="loader-spiner transition10s border75"></div>
								      </div>
								    </div>
								  </div>
								</div>
							</div>
						</div>
						<div class="err-bdy">
							<span>Please login to your UPI app and approve the payment request for <br> <span style="font-weight: 800; font-size: 20px; color: #5dbcc8;"><i class="fa fa-inr"></i>${netAmount}</span></span>
							<p>Please be patience, awaiting to get response.</p>
						</div>
					</div>
				</center>
			</div>
		</div>
	</div>
	
	<jsp:include page="/WEB-INF/jsp/User/Footer.jsp" />
	<script>
	var time = 300; // seconds
	var originalRotate = -90;
	var cntDwn = 59;
	var rotatePerSec = 360 / time;
	var rotateQuarter = time / 4;
	// console.log("rr..."+rotateQuarter);
	function myTimer() {
	    // console.log('time...' + time + 'cntDwn...' + cntDwn + 'originalRotate...' + originalRotate);
	    console.log('time...' + time + 'cntDwn...' + cntDwn + 'originalRotate...' + originalRotate);
	    if (cntDwn == 0) {
	        // console.log("cdn zero");
	        cntDwn = 59;
	    }else{
	        cntDwn--;
	        if (cntDwn < 10) {
	            cntDwn = "0" + cntDwn;
	        }
	        // console.log("cdn --");
	    }

	    if (time >= 240 && time < 300) {
	            // console.log("test1");
	            $("#timer").html("04:"+cntDwn);
        }else if (time >= 180 && time < 240) {
            $("#timer").html("03:"+cntDwn);
        }else if (time >= 120 && time < 180) {
            $("#timer").html("02:"+cntDwn);
        }else if (time >= 60 && time < 120) {
            $("#timer").html("01:"+cntDwn);
        }else if (time >= 0 && time < 60) {
        	if (time > 0) {
        		$("#timer").html("00:"+cntDwn);
        	}else{
        		console.log("time is zero");
        		$("#timer").html("00:00");
        	}            
        }else if(time < 0){
        	
            $(".animate-75-100-b").css("transform","rotate(0deg)");
        }

	    

	    if (originalRotate <= 0) {
	    	originalRotate = originalRotate + 1.2;
	    }else{
	        originalRotate = -88.8;
	    }

	    // console.log("rotate..."+ originalRotate + "....." + time);
        if (time > 225 && time <= 300) {
        	if (originalRotate <= 0) {
        		// console.log("A11 "+time);
        		$(".animate-0-25-b").css("transform","rotate("+originalRotate+"deg)");
        	}else{
        		// console.log("A22 "+time);
        		$(".animate-0-25-b").css("transform","rotate(0deg)");
        		$(".loader-spiner").removeClass("border75").addClass("border50");
        	}            
        }else if (time > 150 && time <= 225 ) {
        	if (originalRotate <= 0) {
        		// console.log("B11 "+time);
        		// $(".animate-0-25-b").css("transform","rotate(0deg)");
        		$(".animate-25-50-b").css("transform","rotate("+originalRotate+"deg)");
        	}else{
        		// console.log("B22 "+time);
        		$(".animate-25-50-b").css("transform","rotate(0deg)");
        		$(".loader-spiner").removeClass("border50").addClass("border25");
        	}
        }else if (time > 75 && time <= 150 ) {
            if (originalRotate <= 0) {
        		// console.log("C11 "+time);
        		// $(".animate-25-50-b").css("transform","rotate(0deg)");
        		$(".animate-50-75-b").css("transform","rotate("+originalRotate+"deg)");
        	}else{
        		// console.log("C22 "+time);
        		$(".animate-50-75-b").css("transform","rotate(0deg)");
        		$(".loader-spiner").removeClass("border25").addClass("border0");
        	}
        }else if (time > 0 && time <= 75 ) {
        	if (originalRotate <= 0) {
        		// console.log("D11 "+time);
        		// $(".animate-50-75-b").css("transform","rotate(0deg)");
        		$(".animate-75-100-b").css("transform","rotate("+originalRotate+"deg)");
        	}else{
        		// console.log("D22 "+time);
        		$(".animate-75-100-b").css("transform","rotate(0deg)");
        	}
            // $(".animate-0-25-b").css("transform","rotate(0deg)");
            // $(".animate-25-50-b").css("transform","rotate(0deg)");
            // $(".animate-50-75-b").css("transform","rotate(0deg)");
            
            
        }else if(time <= 0){
            $(".animate-75-100-b").css("transform","rotate(0deg)");
        }

	    time--;
	    
	}
	var myVar = setInterval(function(){ 
	    myTimer();
	    if (time < 1) {
	        console.log("Stop");
	        clearInterval(myVar);
	        $(".animate-75-100-b").css("transform","rotate(0deg)");
	        $("#timer").html("00:00");
	    }
	}, 1000);
	</script>
	
</body>
</html>