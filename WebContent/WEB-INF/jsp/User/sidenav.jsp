<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
  </a>
  <!-- Left side column. contains the logo and sidebar -->
  
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
     
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
        <a href="#">Welcome</a>
          <p>Rohit Manhas</p>          
        </div>
      </div>
      <!-- sidebar menu -->
      <ul class="sidebar-menu">
        <li class="active treeview">
          <a href="#">
            <i class="fa fa-mobile fa-2x menufa1"></i> <p class="menutx">Mobile Topup</p>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            	<li id="PrepaidSubMenu" class="active"><a data-toggle="pill"
						href="#PrepaidFadeIn"> <i class="fa fa-mobile"></i> <span>Prepaid</span></a></li>
					<li id="PostpaidSubMenu"><a data-toggle="pill"
						href="#PostpaidFadeIn"> <i class="fa fa-mobile"></i> <span>Postpaid</span></a></li>
					<li id="DataCardSubMenu"><a data-toggle="pill"
						href="#DataCardFadeIn"><i class="fa fa-rocket"></i> <span>DataCard</span></a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-lightbulb-o fa-2x menufa1"></i>
            <p class="menutx">Bill Payment</p>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>            
          </a>
          <ul class="treeview-menu">
            <li><a href=""><i class="fa fa-television" aria-hidden="true"></i> DTH</a></li>
            <li><a href=""><i class="fa fa-phone" aria-hidden="true"></i> Landline</a></li>
            <li><a href=""><i class="fa fa-lightbulb-o" aria-hidden="true"></i> Electricity</a></li>
            <li><a href=""><i class="fa fa-fire" aria-hidden="true"></i> Gas</a></li>
            <li><a href=""><i class="fa fa-medkit" aria-hidden="true"></i> Insurance</a></li>
          </ul>
        </li>        
        <li class="treeview">
          <a href="#">
            <i class="fa fa-money menufa2"></i>
            <p class="menutx">Send Money</p>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href=""><i class="fa fa-mobile"></i> Mobile</a></li>
            <li><a href=""><i class="fa fa-credit-card"></i> Bank Account</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-suitcase menufa2"></i>
            <p class="menutx">Travel Tickets</p>
          </a>
        </li>       
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>