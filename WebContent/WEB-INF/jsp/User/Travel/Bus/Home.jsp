<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%-- <link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/select2.css">
	<script src="${pageContext.request.contextPath}/resources/js/select2.min.js"></script>
	 <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/styles.css"> --%>
<link rel="stylesheet" href="/resources/css/new_css/newstyles.css">

<!-- Model for Bus gif -->
<div id="loading" class="modal fade" role="dialog"
	style="margin-top: 7%;">
	<div class="modal-dialog" style="background:">

		<!-- Modal content-->
		<div class="modal-content"
			style="background: transparent; box-shadow: none; border: none;">
				<img
					src="${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/bus1.gif"
					class="img-responsive">
		</div>
	</div>
</div>
<!-- ================= bus ======================= -->

<div class="tab-pane" id="tab_bus">

	<form
		action="${pageContext.request.contextPath}/User/Travel/Bus/GetAllAvailableTrips"
		onsubmit="return validateForm()" method="get">

		<div class="col-md-5" style="padding-left: 0px; padding-right: 0px;">

			<!-- =============== -->
			<div class="inner_tab_content_Bus">

				<select id="bus_dest_list" class="js-example-basic-multiple"
					style="width: 350px;" name="destinationId"
					onchange="$('#errDestCity').empty();">
					<option value="#">Select Destination City</option>
				</select>

				<select id="bus_src_list" class="js-example-basic-multiple"
					style="width: 350px;" name="sourceId"
					onchange="$('#errSrcCity').empty();">
					<option value="#">Select Origin</option>
				</select>

				<span class="rederror" id="errSrcCity" style="margin-left: 19px;"></span>
				<span class="rederror" id="errDestCity" style="margin-left: 132px;"></span>

			</div>
		</div>
		<div class="col-md-3" style="padding-left: 0px; padding-right: 0px;">
			<input class="form-control" name="date" type="text" id="depDate"
				onchange="$('#errDeptdate').empty();"
				style="width: 100%; float: left; height: 40px;" /> <span
				id="depDateicon"> <i
				class="fa fa-calendar Bus-font-icon icon-inside icon-calendar"></i></span>
			<span class="rederror" id="errDeptdate"></span>
		</div>
		<div class="col-md-4" style="padding-left: 0px; padding-right: 0px;">
			<button class="btn btn-block search-btn"
				style="width: 100%; height: 40px;" id="submit_Bus_Search"
				type="submit">Search</button>
			<input type="hidden" name="successMsg" id="sucMsg"
				value="${nobusmsg}"> 
			<input type="hidden" name="bserrMsg" id="bserrMsg" value="${bserrMsg}">
		</div>
	</form>
</div>

<!-- ================= end bus =============== -->


<!-- Model for Message response -->
<div class="container">
	<!-- Trigger the modal with a button -->
	<!--   <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button> -->

	<!-- Modal -->
	<div class="modal fade" id="notFound" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content" style="margin-top: 40%;">

				<div class="modal-body">
					<center>
						<img
							src="${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/search.gif"
							style="width: 150px; margin-top: -91px;">
						<div
							style="background: white; margin-top: -20px; padding: 2.5% 2%;">
							<h3 id="txnErrMsgVal">
								<b>No Buses Found.<br> <small></b></small>
							</h3>
						</div>
					</center>
				</div>

			</div>

		</div>
	</div>
</div>
<!--End  Model for Message response -->

<div id="errModel" role="dialog" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				<center id="common_error_msg" class="alert alert-danger"></center>
				<%-- <center><label id="common_error_msg" class="alert alert-danger"></label></center> --%>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
	$(function() {
		var successMsg = $("#sucMsg").val();
		var bsfailMsg = $("#bserrMsg").val();
// 		console.log("successMsg :: " + successMsg);
		if (!successMsg.length == 0) {
			$("#notFound").modal({
				backdrop : false
			});
			$('#sucMsg').val("");
			var timeout = setTimeout(
					function() {
						$("#notFound").modal({
							show : false
						});
						window.location.href = '${pageContext.request.contextPath}/User/Travel/Flight/Source';
					}, 6000);
		}
		
		if (!bsfailMsg.length == 0) {
			
			$("#common_error_msg").html(bsfailMsg);
			$("#errModel").modal("show");
			$('#bserrMsg').val("");
			var timeout = setTimeout(
					function() {
						$("#errModel").modal({
							show : false
						});
						window.location.href = '${pageContext.request.contextPath}/User/Travel/Flight/Source';
					}, 6000);
		}
	
	});
</script>

<script type="text/javascript">
	$(function() {
		var successMsg = $("#sucMsg").val();
// 		console.log("successMsg :: " + successMsg);
		if (!successMsg.length == 0) {
			$("#notFound").modal({
				backdrop : false
			});
			$('#sucMsg').val("");
			var timeout = setTimeout(
					function() {
						$("#notFound").modal({
							show : false
						});
						window.location.href = '${pageContext.request.contextPath}/User/Travel/Flight/Source';
					}, 6000);

		}
	});
</script>
<!-- Script for page context -->
<script type="text/javascript">
	var contextPath = "${pageContext.request.contextPath}";
</script>

<!-- Script for date picker -->
<script>
	$(function() {
		$("#depDate").datepicker({
			startDate : new Date(),
			format : "dd-mm-yyyy"
		}).on('change', function() {
			$('.datepicker').hide();
		});
	});
</script>
<script>
	$(function() {
		$("#depDateicon").datepicker({
			startDate : new Date(),
			format : "dd-mm-yyyy"
		}).on('change', function() {
			$('.datepicker').hide();
		});
	});
</script>


<!-- Ajax Calling for Get city List -->
<script>
	$("#bus_travel")
			.click(
					function() {
						//    $(document).ready(function(){
						$("#loading").modal({
							backdrop : false
						});
						$('#bus_src_list').empty();
						$('#bus_dest_list').empty();
						$('#bus_src_list')
								.append(
										'<option value="#"> Select Source City </option>');
						$('#bus_dest_list')
								.append(
										'<option value="#"> Select Destination City </option>');

						$(".js-example-basic-multiple").select2();
						$
								.ajax({
									type : "POST",
									contentType : "application/json",
									url : contextPath
											+ "/User/Travel/Bus/GetAllCityList",
									dataType : 'json',
									data : JSON.stringify({

									}),
									success : function(response) {
										console.log("response is "
												+ response.code);
										console.log("response is "
												+ response.details.length);
										//console.log("response is "+response.details[0].cityName);

										for (var i = 0; i < response.details.length; i++) {

											$('#bus_src_list')
													.append(
															'<option value="' + response.details[i].cityId + '">'
																	+ response.details[i].cityName
																	+ '</option>');
											$('#bus_dest_list')
													.append(
															'<option value="' + response.details[i].cityId + '">'
																	+ response.details[i].cityName
																	+ '</option>');

										}
										$("#loading").modal("hide");
									}
								});

						var today = new Date();
						var dd = today.getDate();
						var mm = today.getMonth() + 1; //January is 0!

						 $( "#depDate" ).focus(function() {
							  $( this ).blur();
							});
						
						var yyyy = today.getFullYear();
						if (dd < 10) {
							dd = '0' + dd;
						}
						if (mm < 10) {
							mm = '0' + mm;
						}
						var today = dd + '/' + mm + '/' + yyyy;
						document.getElementById("date").value = today;

						$("#depDateicon").val(today);
						$("#depDate").val(today);

						$("#submit_Bus_Search").click(function() {

						});

						window.onbeforeunload = function(e) {
							alert("Not able to close it");
						};

					});
</script>


<script type="text/javascript">
	function validateForm() {

		console.log("Valid form");

		var sCity = $('#bus_src_list').val();
		var dCity = $('#bus_dest_list').val();
		var dDate = $('#depDate').val();

		var valid = true;

		if (sCity == '#') {
			$('#errSrcCity').text("Please select Source city");
			valid = false;
		}
		if (dCity == '#') {
			valid = false;
			$('#errDestCity').text("Please select Destination city");
		}
		if (dDate == '') {
			valid = false;
			$('#errDeptdate').text("Please select Departure Date");
		}

		if (valid == true) {
			console.log("Spinner Added");

			$("#loading").modal({
				backdrop : false
			});
			$("#submit_Bus_Search").addClass("disabled");
			$("#submit_Bus_Search").html(spinnerUrl);
		}
		return valid;
	}
</script>

