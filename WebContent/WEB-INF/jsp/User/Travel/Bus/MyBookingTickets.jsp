<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="tab-content">
	<div id="tab_bustkt" class="tab-pane fade in active">
		<div class="all-tkt-content">
			<div class="tkt-head">
				<div class="row">
					<div class="col-md-12">
						<table class="table table-responsive">
							<thead>
								<tr>
									<th>Ticket Describe</th>
									<th>Status</th>
									<th><center>Order Number</center></th>
									<th><center>Price</center></th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
			<div class="all-tkt-body">
				<c:forEach items="${details}" var="var">
					<div class="row user-tickets">
						<div class="col-md-12">
							<div class="col-md-3" style="padding-left: 5px;">
								<div class="col-md-2" style="padding-left: 0px;">
									<img
										src="${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/bus-dtls.png">
								</div>
								<div class="col-md-10" style="padding: 0px;">
									<span class="trv trv-src">${var.source}</span> <span>-</span> <span
										class="trv trv-dest">${var.destination}</span><br> <span
										class="trv trv-doj">${var.journeyDate}</span>
								</div>
							</div>
							<div class="col-md-3">
								<center>
									<span class="label label-success">${var.status}</span><br>
									<span class="trv bked-msg">You will receive the ticket
										in your email.</span>
								</center>
							</div>
							<div class="col-md-4">
								<center>
									<span class="trv"><b>${var.transactionRefNo}</b></span><br>
									<span class="trv trv-dobk"></span> <span class="trv trv-tim"></span>
								</center>
							</div>
							<div class="col-md-2">
								<center>
									<span class="trv trv-prc">Rs.</span> <span class="trv trv-prc">${var.totalFare}</span>
									<form
										action="${pageContext.request.contextPath}/User/Travel/Bus/getSingleTicketTravellerDetails"
										method="get">

										<input type="hidden" name="emtTxnId" value="${var.emtTxnId}">
										<button type="submit">View</button>
									</form>

								</center>
							</div>
						</div>
					</div>
				</c:forEach>
				<hr>
			</div>
		</div>
		<br>
	</div>
</div>