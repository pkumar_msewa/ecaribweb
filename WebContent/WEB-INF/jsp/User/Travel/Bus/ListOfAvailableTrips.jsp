<!DOCTYPE HTML>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width" />
<title>VPayQwik | Travel</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/styles.css">

<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap-chosen.css">
<link rel="icon" href='<c:url value="/resources/images/favicon.png"/>' type="image/png" /> 
<link rel="stylesheet" href="/resources/css/new_css/cloud-admin.css">
<link rel="stylesheet" href="/resources/css/font-awesome.min.css">
<link rel="stylesheet" href="/resources/css/new_css/default.css">
<link rel="stylesheet" href="/resources/css/new_css/responsive.css">
<link rel="stylesheet" href="/resources/css/new_css/uniform.default.min.css">
<!--- GOOGLE FONTS - -->
<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600' rel='stylesheet' type='text/css'>
<!-- /GOOGLE FONTS -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/travel_css/bootstrap.css">
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<!--<link rel="stylesheet" href="css/font-awesome.css">-->
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/travel_css/icomoon.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/travel_css/styles.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/travel_css/mystyles.css"><%-- 
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/new_css/jquery.ui.all.css"> --%>
<!-- <link rel="stylesheet" href="css/mystyles.css">   -->
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/9.8.1/bootstrap-slider.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/9.8.1/css/bootstrap-slider.min.css">

<link rel="stylesheet" href="/resources/css/new_css/jquery.ui.theme.css" type="text/css">	 
	<link rel="stylesheet" href="/resources/css/new_css/jquery.ui.slider.css" type="text/css">

		<script type="text/javascript" src="/resources/js/new_js/jquery.ui.widget.js"></script>
		<script type="text/javascript" src="/resources/js/new_js/jquery.ui.mouse.js"></script>
		<script type="text/javascript" src="/resources/js/new_js/jquery.ui.slider.js"></script>
		 <script src="/resources/js/new_js/select2.min.js"></script>
		 <script type="text/javascript" src="<c:url value="/resources/js/userdetails.js"/>"></script>
		
		   <link rel="stylesheet" href="/resources/css/new_css/newstyles.css">
    
    <link rel="stylesheet" rel="stylesheet" href="/resources/css/new_css/select2.1.css">
	<style>
		.rederror {
			font-size: 12px;
    		color: red;
		}
		
		
		.abc {
		position: fixed;
		left: 0px;
		top: 0px;
		width: 100p%;
		height: 100%;
		z-index: 9999;
		background: url(${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/bus.gif) center no-repeat #fff;
	}
	#lower_show ul li {
		
	}
	#lower_show ul li a {
		font-size: 15px;
	    padding: 4px;
	    background: #dfe0e0;
	    font-weight: 700;
    	letter-spacing: 1px;
    	border-radius: 0;
	}
	/* #lower_show ul li:active {
		font-size: 15px;
	    padding: 4px;
	    background: #105796;
	    font-weight: 700;
    	letter-spacing: 1px;
    	border-radius: 0;
	} */
	</style>

<style>
#slider-container {
    width:100%;
}
</style>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/demos/style.css">
 
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
  

</head>

<body style="background: #efefef;">
<jsp:include page="/WEB-INF/jsp/User/Header.jsp" />

	
	
	 <!--<div class="top-area show-onload">-->
     
<!-- Model for Bus gif -->
<div id="loading" class="modal fade" role="dialog" style="margin-top: 5%;">
		<div class="modal-dialog" style="background: ">

			<!-- Modal content-->
			<div class="modal-content" style="background: transparent; box-shadow: none; border: none;">
					<center><img src="${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/bus1.gif" class="img-responsive"></center>
			</div>

		</div>
	</div>

<div class="container">  
  <!-- Trigger the modal with a button -->
<!--   <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button> -->

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
       
        <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" id="close3">&times;</button>
          <center>
<%-- 		  <img src="${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/search.gif" style="width: 150px; margin-top: -91px;"> --%>
        <div style="background: white; padding: 40px;">
          <h3 id="txnErrMsgVal"><b><br><small></b></small></h3>
        </div></center>
        </div>
       
      </div>
      
    </div>
  </div>
</div>

	<div class="blank" style="height: 50px;"></div>

	<div class="container-fluid ">
		<div class="container">
			<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12 col-sm-12 col-xs-12">
								<form action="${pageContext.request.contextPath}/User/Travel/Bus/GetAllAvailableTrips" onsubmit="return validateForm()" method="get">
									<div class="col-md-4 col-sm-4">
									<input type="hidden" id="txnErrMsg" value="${txnErrMsg}">
									<input type="hidden" id="srcvalue" value="${details.source}">
									<input type="hidden" id="destvalue" value="${details.destination}">
									<input type="hidden" id="srcvalueId" value="${details.sourceId}">
									<input type="hidden" id="destvalueId" value="${details.destinationId}">
									<input type="hidden" value="${minAmt}" id="minAmt">
									<input type="hidden" value="${maxAmt}" id="maxAmt">
										<!-- <input type="text" name="" class="form-control" placeholder="From"> -->
										<select id="bus_src_list" class="js-example-basic-multiple" style="width: 350px;" name="sourceId" onchange="$('#errSrcCity').empty();">
										<option value="${details.sourceId}" selected="selected">${details.source}</option>
									
									</select>
<!-- 								 <a class="tooltips" href="#"><i class="fa fa-exclamation-circle errnme"></i><span class="ad1" id="axz"></span></a> -->
								 <p class="rederror" id="errSrcCity"></p>
									</div>
									
									<div class="col-md-4 col-sm-4">
										<!-- <input type="text" name="" class="form-control" placeholder="To"> -->
										<select id="bus_dest_list" class="js-example-basic-multiple" style="width: 350px;" name="destinationId" onchange="$('#errDestCity').empty();">
									<option value="${details.destinationId}" selected="selected">${details.destination}</option>
									
									</select>
							<!-- <a class="tooltips" id="errscr" href="#"><i class="fa fa-exclamation-circle errnme"></i><span class="ad1"><p id="asxc"></p></span></a> -->
								<p class="rederror" id="errDestCity"></p>
									</div>
									<div class="col-md-2 col-sm-2">
									<input class="form-control" name="date" type="text" placeholder="Departure date" id="depDate" value="${depDate}" onchange="$('#errDeptdate').empty();"/>
									<p class="rederror" id="errDeptdate"></p>
									</div>
									<div class="col-md-2 col-sm-2">
										<button type="submit" class="btn btn-primary" id="submit_Bus_Search">Modify
											Search</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>

		<div class="row">
			<div class="col-md-12 col-sm-12">
				<%--         <input type="hidden" value="${details.availableTripsDTOs.size()}"> --%>
				<span class="sho" > Showing
					${fn:length(details.availableTripsDTOs)} buses from
					${details.source} to ${details.destination}</span>
			</div>
		</div>
		<br>
		
<div class="loader" hidden="hidden" id="spinner"></div>


		<div class="row">
			<div class="col-md-12 col-md-12 col-xs-12">
				 <div class="col-md-3 col-sm-3 filter">
            <div class="panel panel-default">
              
<!--  abc -->
              
              <div class="panel-body">
                
                 <div class="row">
                <div class="col-md-12">
      <span style="font-size: 15px; font-weight: 600;">Price range</span>
                  <div class="checkbox">
               
 <p>
   
    <input type="text" id="amount" style="border: 0; color: #f6931f; font-weight: bold;" />
</p>
<div id="slider-container"></div>

 
<div id="slider-range"></div>
 
                  </div>
                 
                </div>
              </div>
                 <hr>
                 
                <!-- div class="col-md-12">
                	<div class="row" style="margin-bottom: 14px;">
	                  <div class="col-md-12">
	                    <span style="font-size: 15px; font-weight: 600;">Departure Time</span>
	                  </div>
	                </div>
	                <div class="row" style="font-size: 12px;">
	                  <div class="col-md-12 filter-dep">
	                    <ul style="">
	                      <li ng-click="filterTime('MORNING')" id="morning">
	                        <div class="morning_icon"></div>
	                        <div class="bef-time"><span>Before 6AM</span></div>
	                      </li>
	                      <li ng-class="" ng-click="filterTime('AFTERNOON')" id="aft6Am">
	                        <div class="noon_icon"></div>
	                        <div class="bef-time"><span>6AM-12PM</span></div>
	                      </li>
	                      <li ng-class="" ng-click="filterTime('EVENING')" id="aft12pm">
	                        <div class="evening_icon"></div>
	                        <div class="bef-time"><span>12PM-6PM</span></div>
	                      </li>
	                      <li ng-class="" ng-click="filterTime('NIGHT')" id="aft6pm">
	                        <div class="night_icon"></div>
	                        <div class="bef-time"><span>After 6PM</span></div>
	                      </li>
	                    </ul>
	                  </div>
	                </div>
	                <hr>
                </div> -->
                

                <div class="col-md-12">
                	<div class="row" style="margin-bottom: 5px;">
	                  <div class="col-md-12">
	                    <span style="font-size: 15px; font-weight: 600;"><b>BUS TYPE</b></span>
	                  </div>
	                </div>
	                <!-- <hr style="margin-top: 6px; margin-bottom: 6px;"> -->
	                <div class="row">
	                  <div class="col-md-12">
	                   <label style="font-size: 12px;"><input type="checkbox" value="" id="show_all">&nbsp;Show All</label>
	                    <label style="font-size: 12px;"><input type="checkbox" value="" id="ac_filter">&nbsp;AC</label>
	                    <label style="font-size: 12px;"><input type="checkbox" value="" id="non_ac_filter">&nbsp;Non AC</label>
	                    <label style="font-size: 12px;"><input type="checkbox" value="" id="sleeper_filter">&nbsp;Sleeper</label>
	                    <label style="font-size: 12px;"><input type="checkbox" value="" id="seater_filter">&nbsp;Seater</label>
	                  </div>
	                </div>
                </div>
                
                <%-- <div class="col-md-12">
                	<div class="row" style="margin-bottom: 5px;">
	                  <div class="col-md-12">
	                    <span style="font-size: 15px; font-weight: 600;"><b>BOARDING POINTS</b></span>
	                  </div>
	                </div>
	                <!-- <hr style="margin-top: 6px; margin-bottom: 6px;"> -->
	                <div class="row">
	                  <div class="col-md-12">
	                  <c:forEach items="${details.boardingFilter}" var="u">
	                   <label style="font-size: 12px;"><input type="checkbox" value="" id="show_all">&nbsp;${u}</label>
	                   </c:forEach>
	                  </div>
	                </div>
	                <hr>
                </div> --%>
                
                 <%-- <div class="col-md-12">
                	<div class="row" style="margin-bottom: 5px;">
	                  <div class="col-md-12">
	                    <span style="font-size: 15px; font-weight: 600;"><b>DROPPING POINTS</b></span>
	                  </div>
	                </div>
	                <!-- <hr style="margin-top: 6px; margin-bottom: 6px;"> -->
	                <div class="row">
	                  <div class="col-md-12">
	                  <c:forEach items="${demo}" var="u">
	                   <label style="font-size: 12px;"><input type="checkbox" value="" id="show_all">&nbsp;Show All</label>
	                   </c:forEach>
	                  </div>
	                </div>
	                <hr>
                </div> --%>
                
                <%-- <div class="col-md-12">
                	<div class="row" style="margin-bottom: 5px;">
	                  <div class="col-md-12">
	                    <span style="font-size: 15px; font-weight: 600;"><b>BUS OPERATOR</b></span>
	                  </div>
	                </div>
	                <!-- <hr style="margin-top: 6px; margin-bottom: 6px;"> -->
	                <div class="row">
	                  <div class="col-md-12">
	                  <c:forEach items="${demo}" var="u">
	                   <label style="font-size: 12px;"><input type="checkbox" value="" id="show_all">&nbsp;Show All</label>
	                   </c:forEach>
	                  </div>
	                </div>
	                <hr>
                </div> --%>
                
              </div>
            </div>
          </div> 
				<div class="col-md-9">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="panel panel-default">
								<div class="panel-body">
									<div class="row">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<div class="col-md-4 col-sm-4 col-xs-4">
												<span>Bus Operator</span>
											</div>
											<div class="col-md-2 col-sm-2 col-xs-2">
												<span>Departure</span>
											</div>
											<div class="col-md-2 col-sm-2 col-xs-2">
												<span>Arrival</span>
											</div>
											<div class="col-md-2 col-sm-2 col-xs-2">
												<span>Duration</span>
											</div>
											<div class="col-md-2 col-sm-2 col-xs-2">
												<span>Price</span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>


					<!-- ===================================== Bus List Starts ================================ -->

					<c:forEach items="${details.availableTripsDTOs}" var="trips">
					<span class="bustype ${trips.ac}"> 
					<span class="busSeattype ${trips.sleeper}"> 
					<span class="busDepTime ${trips.departureTime}"> 
<%-- 					<span class="type ${trips.sleeper}">  --%>
					 <div id="computers">
   					 <div class="system" data-price="${trips.price}">
						<div class="panel panel-default">
							<div class="panel-body">
								<div class="row">
									<div class="col-md-12">
										<div class="col-md-4 col-sm-4 col-xs-12">
											<img
												src="${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/Bus.png"
												style="width: 8%;"> <span class="bus-ope">&nbsp;${trips.travels}</span><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<span class="bus-type">${trips.busType}</span> <br> <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<span class="savail">Seats Available : </span> <span
												class="sqty">${trips.availableSeats}</span>
										</div>
										<div class="col-md-2 col-sm-2 col-xs-6">

											<span class="tme">${trips.departureTime}</span>
										</div>
										<div class="col-md-2 col-sm-2 col-xs-6">
											<span class="tme">${trips.arrivalTime}</span>
										</div>
										<div class="col-md-2 col-sm-2 col-xs-6"
											style="te /xt-align: center;">
											<img
												src="${pageContext.request.contextPath}/resources/images/User/travel/img/airline/clock.png"
												style="width: 20%; margin-left: 15px;"><br> <span
												class="bus-dur">${trips.duration}</span>
										</div>
										<div class="col-md-2 col-sm-2 col-xs-6">
											<span class="fa fa-inr bus-fa"></span> <span class="prc">${trips.price}</span>
											<span class="ppp">/ Person</span><br>
											<button class="btn btn-primary sel-seat" style="margin-top: 10px;" data-toggle="modal" id="getSeatDetails_submit"
												data-target="#"
												onclick="getSeatDetails('${trips.id}','${trips.seater}','${trips.sleeper}','${trips.engineId}','${trips.doj}','${trips.travels}',
												'${trips.departureTime}','${trips.arrivalTime}','${trips.duration}','${details.source}','${details.destination}','${trips.busType}',
												'${details.sourceId}','${details.destinationId}','${trips.routeId}',
												'${trips.commission}','${trips.markup}','${trips.bpId}','${trips.dpId}','${trips.bpDpLayout}')">Select
												Seats</button>
															
											<a href="#" data-toggle="modal" data-target="#cncelPlcy"
												class="cp">Cancellation Policy</a>
										</div>
									</div>
								</div>
							</div>
						</div>
				     </div>
				 </div>
				 </span></span></span>

					</c:forEach>
					<!-- ===================================== Bus List End Here ================================ -->

				</div>
			</div>
		</div>
	</div>

	<!-- ========================= Modal For Cancellation Policy ==================================== -->
	<div id="cncelPlcy" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close close1" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Cancellation Policy</h4>
				</div>
				<div class="modal-body">
					<div class="tblBox">
						<table class="table table-bordered table-responsive">
							<thead>
								<tr>
									<th>Before Departure</th>
									<th>Cancellation Charges</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Cancellation between departure time and 3 hour(s)
										before departure time</td>
									<td>100%</td>
								</tr>
								<tr>
									<td>Cancellation between 3 hour(s) before departure time
										and 24 hour(s) before departure time</td>
									<td>50%</td>
								</tr>
								<tr>
									<td>Cancellation between 24 hour(s) before departure time
										and 72 hour(s) before departure time</td>
									<td>25%</td>
								</tr>
								<tr>
									<td>Cancellation between 72 hour(s) before departure time
										and 168 hour(s) before departure time</td>
									<td>10%</td>
								</tr>
							</tbody>
						</table>
						<p class="note">Note: Cancellation policy mentioned is liable
							to change at bus operator's end. Partial cancellation is not
							allowed.</p>
					</div>
				</div>
			</div>

		</div>
	</div>

	<!-- ========================= Modal For Seat Booking ==================================== -->
	<div id="seatBooking" class="modal fade" role="dialog" style="    overflow-y: auto;">
		<div class="modal-dialog modal-lg" style="margin-top: auto;">

			<!-- Modal content-->
			<div class="modal-content">

				<div class="modal-body">
					<form action="${pageContext.request.contextPath}/User/Travel/Bus/takeSeatDetails" onsubmit="return checkForm()">
					<button type="button" class="close close2" data-dismiss="modal" id="clsesits">&times;</button>
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<img
								src="${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/Bus.png"
								style="width: 4%;"> <span class="modal-title bookinghead" id="m_bus_nm">HMS
								Jabbar Travels</span> <input type="hidden" id="m_bus_nm_val" name="travelName"><br> <span class="bookingtype" id="m_bus_type">2+1,
								Sleeper/Seater, Non-AC</span> <input type="hidden" id="m_bus_type_val" name="busType"> &nbsp;&nbsp;<span id="m_bus_doj" >7/27/2017 </span><input type="hidden" id="m_bus_doj_val" name="doj"> <span id="m_bus_toj">12:00:00
								AM</span><input type="hidden" id="m_bus_dtime_val" name="departureTime">
								<input type="hidden" id="m_bus_atime_val" name="arrivalTime">
								<input type="hidden" id="m_bus_source_val" name="source">
								<input type="hidden" id="m_bus_destination_val" name="destination">
								<input type="hidden" id="m_bus_duration_val" name="duration">
								<input type="hidden" id="m_bus_boarding_val" name="boarding">
								<input type="hidden" id="m_bus_Bus_Id" name="busId">
								<input type="hidden" id="m_bus_Bus_Type" name="busType">
								
								<input type="hidden" id="m_bus_sourceId_val" name="sourceId">
								<input type="hidden" id="m_bus_destinationId_val" name="destinationId">
								<input type="hidden" id="m_bus_routId_val" name="routId">
								<input type="hidden" id="m_bus_commission" name="commission">
								<input type="hidden" id="m_bus_markup" name="markup">
								<input type="hidden" id="m_bus_engine_Id" name="engineId">
								<input type="hidden" id="m_bus_fares" name="fares">
						</div>
					</div>
					<hr style="margin-top: 5px;">

					<div class="row">
						<div class="col-md-12 col-sm-12">
							<div class="col-md-3 col-sm-3">
								<img class="avail"
									src="${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/seats2.png">
								<span>Available</span>
							</div>
							<div class="col-md-3 col-sm-3">
								<img class="booked"
									src="${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/seats4.png">
								<span>Booked</span>
							</div>
							<div class="col-md-3 col-sm-3">
								<img class="slcted"
									src="${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/seats3.png">
								<span>Selected</span>
							</div>
							<div class="col-md-3 col-sm-3">
								<img class="rsvrd-lady"
									src="${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/seats1.png">
								<span>Reserved for Ladies</span>
							</div>
						</div>
					</div>
					<br>
					
					<div class="row">
						<div class="col-md-12 col-sm-12" id="lower_show" hidden="hidden">
							<div class="row">
								<div class="col-md-3 col-sm-4 col-md-offset-4 col-sm-offset-4">
									<ul class="nav nav-pills" style="margin-bottom: 10px;">
									    <li class="active" id="lower_tab" hidden="hidden"><a data-toggle="pill" href="#low-sit" hidden="hidden">Lower</a></li>
									    <li id="upper_tab" hidden="hidden"><a data-toggle="pill" href="#up-sit" hidden="hidden">Upper</a></li>
									</ul>
								</div>
							</div>

							  <div class="tab-content">
							    <div id="low-sit" class="tab-pane fade in active">
							      	<div class="col-md-8 col-md-offset-2">
										<div id="lower" >
										
										</div>
										<div class="lower-ico"></div>

									</div>
							    </div>
							    <div id="up-sit" class="tab-pane fade">
							      	<div class="col-md-8 col-md-offset-2" id="upper_show" hidden="hidden">
										<div id="upper">

										</div>
										<div class="upper-ico"></div>
									</div>
							    </div>
							  </div>
						</div>
					</div>
					<p class="rederror" id="errSeats" style="padding-left: 156px; font-size:15px"></p>
					<br>
					<div class="row">
					
						<div class="col-md-12 col-sm-12">
							<div class="col-md-4 col-sm-5 col-md-offset-2 col-sm-offset-1">
								<span>Selected Seats : </span> <span id="seat_number"></span> 
								 <input type="hidden" id="m_bus_seats_val" name="seats">
								<input type="hidden" id="bus_fare_val" name="totalFare">
								<br>
								<div class="form-group">
									<select class="form-control" id="boardingId" name="boardingInfo" onchange="$('#errbd').empty();">
										<option value="#">Select Boarding</option>
										
									</select>
								</div>
								<p class="rederror" id="errbd"></p>
								<div class="form-group">
								<input type="hidden" name="bdPoint" id="bd_point">
								<select class="form-control" id="dropingId" name="droppingInfo" onchange="$('#errdp').empty();">
									<option value="#">Select Dropping</option>
									
								</select>
								</div>
								<p class="rederror" id="errdp"></p>
								<!-- <div class="form-group">
									<textarea class="form-control" rows="4" readonly="readonly" id="bddpTxt"></textarea>
								</div> -->

							</div>
							<div class="col-md-4 col-sm-4">
								<div class="rupcnf">
									<span class="bus-ttl">Total: </span> <span
										class="fa fa-inr st-cnf"></span> <span class="bus-prc" id="bus_fare">0</span>
								</div>
								<br>
								<button class="btn btn-primary"
									style="font-size: 18px; font-weight: 600; margin-left: 50px;" type="submit" id="cnfm_seats">Confirm
									Seats</button>
							</div>
						</div>
						
					</div>
				</form>
				</div>
			</div>

		</div>
	</div>



	<script>
    $(function() {
       startDate: "today",
    $( "#datepicker" ).datepicker();
    });
</script>

	<!-- Script for page context -->
	<script type="text/javascript">
var contextPath = "${pageContext.request.contextPath}";
</script>

	<script type="text/javascript">
	
	var btype=[];
	var bSeatType=[];
	
	 $('#ac_filter').click(function(e){
	 
    $("#show_all").attr('checked', false);
	var mediaElements = $('.bustype');
	if (btype.indexOf(true)==-1) {
		btype.push(true);
	}else{
		var a=btype.indexOf(true);
		btype.splice(a,1);
	}
	
	console.log("ac bus");
	mediaElements.hide();
	
	if (btype.length==0) {
		
		mediaElements.show();
	}
	
	for (var i = 0; i < btype.length; i++) {
		mediaElements.filter('.' +btype[i]).show();
	}
	

});
	
	 $('#non_ac_filter').click(function(e){
		 
		 $("#show_all").attr('checked', false);
		 if (btype.indexOf(false)==-1) {
				btype.push(false);
			}else{
				var a=btype.indexOf(false);
				btype.splice(a,1);
			}
		 
			var mediaElements = $('.bustype');
			console.log("non ac bus");
			mediaElements.hide();
			
			if (btype.length==0) {
				
				mediaElements.show();
			}	
			
			for (var i = 0; i < btype.length; i++) {
				mediaElements.filter('.' +btype[i]).show();
			}

		});
	
	 $('#sleeper_filter').click(function(e){
		 
		 $("#show_all").attr('checked', false);
		 if (bSeatType.indexOf(true)==-1) {
			 bSeatType.push(true);
			}else{
				var a=bSeatType.indexOf(true);
				bSeatType.splice(a,1);
			}
		 
			var mediaElements = $('.busSeattype');
			console.log("non ac bus");
			mediaElements.hide();
			
			if (bSeatType.length==0) {
				
				mediaElements.show();
			}	
			
			for (var i = 0; i < bSeatType.length; i++) {
				mediaElements.filter('.' +bSeatType[i]).show();
			}

		});
	 
 		$('#seater_filter').click(function(e){
		 
 		 $("#show_all").attr('checked', false);
		 if (bSeatType.indexOf(false)==-1) {
			 bSeatType.push(false);
			}else{
				var a=bSeatType.indexOf(false);
				bSeatType.splice(a,1);
			}
		 
			var mediaElements = $('.busSeattype');
			console.log("non ac bus");
			mediaElements.hide();
			
			if (bSeatType.length==0) {
				
				mediaElements.show();
			}	
			
			for (var i = 0; i < bSeatType.length; i++) {
				mediaElements.filter('.' +bSeatType[i]).show();
			}

		});
 
 		$('#show_all').click(function(e){
 			var mediaElementsAc = $('.bustype');
 			var mediaElements = $('.busSeattype');
 			
 			mediaElementsAc.show();
 			mediaElements.show();
 			
 			 btype=[];
 		     bSeatType=[];
 		     
 		    $("#ac_filter").attr('checked', false);
 		    $("#non_ac_filter").attr('checked', false);
 		    $("#sleeper_filter").attr('checked', false);
 		    $("#seater_filter").attr('checked', false);
 		});
 		
 		
	function  getSeatDetails(bId,seater,sleper,eId,jdate,travels,dTime,aTime,
			dur,source,dest,busType,sId,dId,rId,cmision,markup,bpId,dpId,bpDpLayout){
			
			$('#loading').modal({
		        backdrop: 'static',
		       keyboard: true,  
		       show: true
		}); 
		console.log("bpId:"+bpId+",dpId:"+dpId+",bpDpLayout:"+bpDpLayout)
		console.log("Bus Type:: "+busType);
		console.log(dTime);
		$("#m_bus_nm_val").val(travels);
		$("#m_bus_atime_val").val(aTime);
		$("#m_bus_dtime_val").val(dTime);
		$("#m_bus_doj_val").val(jdate);
		$("#m_bus_source_val").val(source);
		$("#m_bus_destination_val").val(dest);
		$("#m_bus_duration_val").val(dur);
		$("#m_bus_Bus_Id").val(bId);
		$("#m_bus_Bus_Type").val(busType);
		
		$("#m_bus_sourceId_val").val(sId);
		$("#m_bus_destinationId_val").val(dId);
		$("#m_bus_routId_val").val(rId);
		$("#m_bus_commission").val(cmision);
		$("#m_bus_markup").val(markup);
		$("#m_bus_engine_Id").val(eId);
		var doj=jdate.split(" ");
		console.log("Split Date:: "+doj[0]);
		
		$("#lower_show").hide();
		$("#upper_show").hide();
		$("#m_bus_nm").html(travels);

		$("#m_bus_nm").html(travels);
		$("#m_bus_doj").html(doj[0]);
		$("#m_bus_toj").html(doj[1]+doj[2]);
		
		$("#upper").html("");
		$("#lower").html("");
		$("#seat_number").text("");
		$("#bus_fare").html("0");
		
		
		$("#bus_fare_val").val("");
		
		$("#m_bus_fares").val("");
		
		$.ajax({
			type : "POST",
//  			headers: headers,
			contentType : "application/json",
			url :contextPath+"/User/Travel/Bus/GetSeatDetails",
			dataType : 'json',
			data : JSON.stringify({
				"busId":bId,
				"seater":seater,
				"sleeper":sleper,
				"engineId":eId,
				"journeyDate":jdate,
				"bpId":bpId,
				"dpId":dpId,
				"bpDpLayout":bpDpLayout
			}),
			success : function(response) {
				$('#loading').modal("hide");
				console.log("response is "+response.code);
				$('#lower_tab').hide();
				$('#upper_tab').hide();
				/* $("#getSeatDetails_submit").removeClass("disabled");
				$("#getSeatDetails_submit").html("Select Seats"); */
				if (response.code.includes("S00")) {
// 					$("#seatBooking").modal("show");
					$('#seatBooking').modal({
				         backdrop: 'static',
				        keyboard: true,  
				        show: true
				}); 
					$('body').css({overflow: 'hidden'});
				console.log("Upper Show "+response.details.upperShow);
				console.log("lower Show "+response.details.lowerShow);
				$('#boardingId').html('<option value="#">Select Boarding Point</option>');
				$('#dropingId').html('<option value="#">Select Dropping Point</option>');
// 				console.log("List of Boarding Point:: "+response.details.listBoardingPoints);
				for (var i = 0; i < response.details.listBoardingPoints.length; i++) {
					$('#boardingId').append('<option value="' + response.details.listBoardingPoints[i].bdid +','+response.details.listBoardingPoints[i].bdPoint+','+response.details.listBoardingPoints[i].prime+','+response.details.listBoardingPoints[i].time+ '">' + response.details.listBoardingPoints[i].bdPoint + '</option>');
					
				}
				
				for (var i = 0; i < response.details.listDropingPoints.length; i++) {
					$('#dropingId').append('<option value="' + response.details.listDropingPoints[i].dpId +','+response.details.listDropingPoints[i].dpName+','+response.details.listDropingPoints[i].prime+','+response.details.listDropingPoints[i].dpTime+ '">' + response.details.listDropingPoints[i].dpName + '</option>');
					
				}
				
				if (response.details.upperShow==true) {
					$("#lower_tab").show();
					$("#upper_tab").show();
					$("#upper_show").show();
					console.log("Upper Show  Exist :: ");
					
					var col=response.details.upper.length;
					
					if (response.details.upper.firstColumn!=null) {

					for(var i=0;i<response.details.upper.firstColumn.length;i++){
						
						if (response.details.upper.firstColumn[i].seatType.includes("booked")) {
							$("#upper").append('<div class="seats-wrp-in-sleep" ><input '+ 'id='+'\"'+response.details.upper.firstColumn[i].name+'"\ '+  'class="seats"'+ 'value="'+response.details.upper.firstColumn[i].name+'|'+response.details.upper.firstColumn[i].fare+'"type="checkbox"/> <label style="background: url(${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/sleep3.png) no-repeat; pointer-events: none;line-height: 25px; cursor: pointer;overflow: hidden;transition: all 0.25s ease 0s;-webkit-transition: all 0.25s;" for='+'"'+response.details.upper.firstColumn[i].name+'" '+'class="sleep"></label></div>');	
						}
						else{
						$("#upper").append('<div class="seats-wrp-in-sleep" ><input '+'id='+'"'+response.details.upper.firstColumn[i].name+'"'+ ' onclick="selectSeats(\''+response.details.upper.firstColumn[i].name+'\','+response.details.upper.firstColumn[i].fare+');"'+ 'class="seats"'+ 'value="'+response.details.upper.firstColumn[i].name+'|'+response.details.upper.firstColumn[i].fare+'"type="checkbox"/> <label title="'+response.details.upper.firstColumn[i].name+'" for='+'"'+response.details.upper.firstColumn[i].name+'" '+'class="sleep"></label></div>');
						}
// 						$("#upper").append('<div style="display: inline;"><input  class="seats"'+ 'value="'+response.details.upper.firstColumn[i].name+'|'+response.details.upper.firstColumn[i].fare+'"type="checkbox"/> <label for="i1" class="seat"></label></div>');
					}
					console.log("First Column Length is:: "+response.details.upper.firstColumn.length);
					$("#upper").append('<br><br>');
					}
					
					if (response.details.upper.secondColumn!=null) {
					
					for(var i=0;i<response.details.upper.secondColumn.length;i++){
						
						if (response.details.upper.secondColumn[i].seatType.includes("booked")) {
							$("#upper").append('<div class="seats-wrp-in-sleep" ><input '+ 'id='+'\"'+response.details.upper.secondColumn[i].name+'"\ '+  'class="seats"'+ 'value="'+response.details.upper.secondColumn[i].name+'|'+response.details.upper.secondColumn[i].fare+'"type="checkbox"/> <label style="background: url(${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/sleep3.png) no-repeat; pointer-events: none;line-height: 25px; cursor: pointer;overflow: hidden;transition: all 0.25s ease 0s;-webkit-transition: all 0.25s;" for='+'"'+response.details.upper.secondColumn[i].name+'" '+'class="sleep"></label></div>');	
						}
						else{
						$("#upper").append('<div class="seats-wrp-in-sleep" ><input '+'id='+'"'+response.details.upper.secondColumn[i].name+'"'+ ' onclick="selectSeats(\''+response.details.upper.secondColumn[i].name+'\','+response.details.upper.secondColumn[i].fare+');"'+ 'class="seats"'+ 'value="'+response.details.upper.secondColumn[i].name+'|'+response.details.upper.secondColumn[i].fare+'"type="checkbox"/> <label title="'+response.details.upper.secondColumn[i].name+'" for='+'"'+response.details.upper.secondColumn[i].name+'" '+'class="sleep"></label></div>');
						}
					
// 						$("#upper").append('<div style="display: inline;"><input  class="seats"'+ 'value="'+response.details.upper.secondColumn[i].name+'|'+response.details.upper.secondColumn[i].fare+'"type="checkbox"/> <label for="i1" class="seat"></label></div>');
					}
					
					console.log("Second Column Length is:: "+response.details.upper.secondColumn.length);
					$("#upper").append('<br><br>');
					}
					
					if (response.details.upper.thirdColumn!=null) {

					for(var i=0;i<response.details.upper.thirdColumn.length;i++){
						
						if (response.details.upper.thirdColumn[i].seatType.includes("booked")) {
							$("#upper").append('<div class="seats-wrp-in-sleep" ><input '+ 'id='+'\"'+response.details.upper.thirdColumn[i].name+'"\ '+  'class="seats"'+ 'value="'+response.details.upper.thirdColumn[i].name+'|'+response.details.upper.thirdColumn[i].fare+'"type="checkbox"/> <label style="background: url(${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/sleep3.png) no-repeat; pointer-events: none;line-height: 25px; cursor: pointer;overflow: hidden;transition: all 0.25s ease 0s;-webkit-transition: all 0.25s;" for='+'"'+response.details.upper.thirdColumn[i].name+'" '+'class="sleep"></label></div>');	
						}
						else{
						$("#upper").append('<div class="seats-wrp-in-sleep" ><input '+'id='+'"'+response.details.upper.thirdColumn[i].name+'"'+ ' onclick="selectSeats(\''+response.details.upper.thirdColumn[i].name+'\','+response.details.upper.thirdColumn[i].fare+');"'+ 'class="seats"'+ 'value="'+response.details.upper.thirdColumn[i].name+'|'+response.details.upper.thirdColumn[i].fare+'"type="checkbox"/> <label title="'+response.details.upper.thirdColumn[i].name+'" for='+'"'+response.details.upper.thirdColumn[i].name+'" '+'class="sleep"></label></div>');
						}
					
// 						$("#upper").append('<div style="display: inline;"><input  class="seats"'+ 'value="'+response.details.upper.thirdColumn[i].name+'|'+response.details.upper.thirdColumn[i].fare+'"type="checkbox"/> <label for="i1" class="seat"></label></div>');
					}
					console.log("Third Column Length is:: "+response.details.upper.thirdColumn.length);
					$("#upper").append('<br><br>');
					}
					
					if (response.details.upper.fourthColumn!=null) {
					
					for(var i=0;i<response.details.upper.fourthColumn.length;i++){
						
						if (response.details.upper.fourthColumn[i].seatType.includes("booked")) {
							$("#upper").append('<div class="seats-wrp-in-sleep" ><input '+ 'id='+'\"'+response.details.upper.fourthColumn[i].name+'"\ '+  'class="seats"'+ 'value="'+response.details.upper.fourthColumn[i].name+'|'+response.details.upper.fourthColumn[i].fare+'"type="checkbox"/> <label style="background: url(${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/sleep3.png) no-repeat; pointer-events: none;line-height: 25px; cursor: pointer;overflow: hidden;transition: all 0.25s ease 0s;-webkit-transition: all 0.25s;" for='+'"'+response.details.upper.fourthColumn[i].name+'" '+'class="sleep"></label></div>');	
						}
						else{
						$("#upper").append('<div class="seats-wrp-in-sleep" ><input '+'id='+'"'+response.details.upper.fourthColumn[i].name+'"'+ ' onclick="selectSeats(\''+response.details.upper.fourthColumn[i].name+'\','+response.details.upper.fourthColumn[i].fare+');"'+ 'class="seats"'+ 'value="'+response.details.upper.fourthColumn[i].name+'|'+response.details.upper.fourthColumn[i].fare+'"type="checkbox"/> <label title="'+response.details.upper.fourthColumn[i].name+'" for='+'"'+response.details.upper.fourthColumn[i].name+'" '+'class="sleep"></label></div>');
						}
						
// 						$("#upper").append('<div style="display: inline;"><input  class="seats"'+ 'value="'+response.details.upper.fourthColumn[i].name+'|'+response.details.upper.fourthColumn[i].fare+'"type="checkbox"/> <label for="i1" class="seat"></label></div>');
					}
					
					console.log("Fourth Column Length is:: "+response.details.upper.fourthColumn.length);
					$("#upper").append('<br><br>');
					}
					
					if (response.details.upper.fifthColumn!=null) {
					for(var i=0;i<response.details.upper.fifthColumn.length;i++){
						
						if (response.details.upper.fifthColumn[i].seatType.includes("booked")) {
							$("#upper").append('<div class="seats-wrp-in-sleep" ><input '+ 'id='+'\"'+response.details.upper.fifthColumn[i].name+'"\ '+  'class="seats"'+ 'value="'+response.details.upper.fifthColumn[i].name+'|'+response.details.upper.fifthColumn[i].fare+'"type="checkbox"/> <label style="background: url(${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/sleep3.png) no-repeat; pointer-events: none;line-height: 25px; cursor: pointer;overflow: hidden;transition: all 0.25s ease 0s;-webkit-transition: all 0.25s;" for='+'"'+response.details.upper.fifthColumn[i].name+'" '+'class="sleep"></label></div>');	
						}
						else{
						$("#upper").append('<div class="seats-wrp-in-sleep" ><input '+'id='+'"'+response.details.upper.fifthColumn[i].name+'"'+ ' onclick="selectSeats(\''+response.details.upper.fifthColumn[i].name+'\','+response.details.upper.fifthColumn[i].fare+');"'+ 'class="seats"'+ 'value="'+response.details.upper.fifthColumn[i].name+'|'+response.details.upper.fifthColumn[i].fare+'"type="checkbox"/> <label title="'+response.details.upper.fifthColumn[i].name+'" for='+'"'+response.details.upper.fifthColumn[i].name+'" '+'class="sleep"></label></div>');
						}
// 						$("#upper").append('<div style="display: inline;"><input  class="seats"'+ 'value="'+response.details.upper.fifthColumn[i].name+'|'+response.details.upper.fifthColumn[i].fare+'"type="checkbox"/> <label for="i1" class="seat"></label></div>');
					}
					
					console.log("Fifth Column Length is:: "+response.details.upper.fifthColumn.length);
					$("#upper").append('<br><br>');
					}	
					
					if (response.details.upper.sixthColumn!=null) {
					for(var i=0;i<response.details.upper.sixthColumn.length;i++){
						
						if (response.details.upper.sixthColumn[i].seatType.includes("booked")) {
							$("#upper").append('<div class="seats-wrp-in-sleep" ><input '+ 'id='+'\"'+response.details.upper.sixthColumn[i].name+'"\ '+  'class="seats"'+ 'value="'+response.details.upper.sixthColumn[i].name+'|'+response.details.upper.sixthColumn[i].fare+'"type="checkbox"/> <label style="background: url(${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/sleep3.png) no-repeat; pointer-events: none;line-height: 25px; cursor: pointer;overflow: hidden;transition: all 0.25s ease 0s;-webkit-transition: all 0.25s;" for='+'"'+response.details.upper.sixthColumn[i].name+'" '+'class="sleep"></label></div>');	
						}
						else{
						$("#upper").append('<div class="seats-wrp-in-sleep" ><input '+'id='+'"'+response.details.upper.sixthColumn[i].name+'"'+ ' onclick="selectSeats(\''+response.details.upper.sixthColumn[i].name+'\','+response.details.upper.sixthColumn[i].fare+');"'+ 'class="seats"'+ 'value="'+response.details.upper.sixthColumn[i].name+'|'+response.details.upper.sixthColumn[i].fare+'"type="checkbox"/> <label title="'+response.details.upper.sixthColumn[i].name+'" for='+'"'+response.details.upper.sixthColumn[i].name+'" '+'class="sleep"></label></div>');
						}
						
// 						$("#upper").append('<div style="display: inline;"><input  class="seats"'+ 'value="'+response.details.upper.sixthColumn[i].name+'|'+response.details.upper.sixthColumn[i].fare+'"type="checkbox"/> <label for="i1" class="seat"></label></div>');
					}
					
					console.log("sixth Column Length is:: "+response.details.upper.sixthColumn.length);
					$("#upper").append('<br><br>');
					}
					
					if (response.details.upper.seventhColumn!=null) {
					
					for(var i=0;i<response.details.upper.seventhColumn.length;i++){
						
						if (response.details.upper.seventhColumn[i].seatType.includes("booked")) {
							$("#upper").append('<div class="seats-wrp-in-sleep" ><input '+ 'id='+'\"'+response.details.upper.seventhColumn[i].name+'"\ '+  'class="seats"'+ 'value="'+response.details.upper.seventhColumn[i].name+'|'+response.details.upper.seventhColumn[i].fare+'"type="checkbox"/> <label style="background: url(${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/sleep3.png) no-repeat; pointer-events: none;line-height: 25px; cursor: pointer;overflow: hidden;transition: all 0.25s ease 0s;-webkit-transition: all 0.25s;" for='+'"'+response.details.upper.seventhColumn[i].name+'" '+'class="sleep"></label></div>');	
						}
						else{
						$("#upper").append('<div class="seats-wrp-in-sleep" ><input '+'id='+'"'+response.details.upper.seventhColumn[i].name+'"'+ ' onclick="selectSeats(\''+response.details.upper.seventhColumn[i].name+'\','+response.details.upper.seventhColumn[i].fare+');"'+ 'class="seats"'+ 'value="'+response.details.upper.seventhColumn[i].name+'|'+response.details.upper.seventhColumn[i].fare+'"type="checkbox"/> <label title="'+response.details.upper.seventhColumn[i].name+'"  for='+'"'+response.details.upper.seventhColumn[i].name+'" '+'class="sleep"></label></div>');
						}
// 						$("#upper").append('<div style="display: inline;"><input  class="seats"'+ 'value="'+response.details.upper.seventhColumn[i].name+'|'+response.details.upper.seventhColumn[i].fare+'"type="checkbox"/> <label for="i1" class="seat"></label></div>');
					}
					
					console.log("Seventh Column Length is:: "+response.details.upper.seventhColumn.length);
					$("#upper").append('<br><br>');
					}
					
					
					if (response.details.upper.eightColumn!=null) {
						
					for(var i=0;i<response.details.upper.eightColumn.length;i++){
						
						if (response.details.upper.eightColumn[i].seatType.includes("booked")) {
							$("#upper").append('<div class="seats-wrp-in-sleep" ><input '+ 'id='+'\"'+response.details.upper.eightColumn[i].name+'"\ '+  'class="seats"'+ 'value="'+response.details.upper.eightColumn[i].name+'|'+response.details.upper.eightColumn[i].fare+'"type="checkbox"/> <label style="background: url(${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/sleep3.png) no-repeat; pointer-events: none;line-height: 25px; cursor: pointer;overflow: hidden;transition: all 0.25s ease 0s;-webkit-transition: all 0.25s;" for='+'"'+response.details.upper.eightColumn[i].name+'" '+'class="sleep"></label></div>');	
						}
						else{
						$("#upper").append('<div class="seats-wrp-in-sleep" ><input '+'id='+'"'+response.details.upper.eightColumn[i].name+'"'+ ' onclick="selectSeats(\''+response.details.upper.eightColumn[i].name+'\','+response.details.upper.eightColumn[i].fare+');"'+ 'class="seats"'+ 'value="'+response.details.upper.eightColumn[i].name+'|'+response.details.upper.eightColumn[i].fare+'"type="checkbox"/> <label title="'+response.details.upper.eightColumn[i].name+'" for='+'"'+response.details.upper.eightColumn[i].name+'" '+'class="sleep"></label></div>');
						}
						
// 						$("#upper").append('<div style="display: inline;"><input  class="seats"'+ 'value="'+response.details.upper.eightColumn[i].name+'|'+response.details.upper.eightColumn[i].fare+'"type="checkbox"/> <label for="i1" class="seat"></label></div>');
					}
					
					console.log("Eight Column Length is:: "+response.details.upper.eightColumn.length);
					$("#upper").append('<br><br>');
					}
					
					if (response.details.upper.ninethColumn!=null) {
					
					for(var i=0;i<response.details.upper.ninethColumn.length;i++){
						
						 if (response.details.upper.ninethColumn[i].seatType.includes("booked")) {
								$("#upper").append('<div class="seats-wrp-in-sleep" ><input '+ 'id='+'\"'+response.details.upper.ninethColumn[i].name+'"\ '+  'class="seats"'+ 'value="'+response.details.upper.ninethColumn[i].name+'|'+response.details.upper.ninethColumn[i].fare+'"type="checkbox"/> <label style="background: url(${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/sleep3.png) no-repeat; pointer-events: none;line-height: 25px; cursor: pointer;overflow: hidden;transition: all 0.25s ease 0s;-webkit-transition: all 0.25s;" for='+'"'+response.details.upper.ninethColumn[i].name+'" '+'class="sleep"></label></div>');	
							}
							else{
							$("#upper").append('<div class="seats-wrp-in-sleep" ><input '+'id='+'"'+response.details.upper.ninethColumn[i].name+'"'+ ' onclick="selectSeats(\''+response.details.upper.ninethColumn[i].name+'\','+response.details.upper.ninethColumn[i].fare+');"'+ 'class="seats"'+ 'value="'+response.details.upper.ninethColumn[i].name+'|'+response.details.upper.ninethColumn[i].fare+'"type="checkbox"/> <label title="'+response.details.upper.ninethColumn[i].name+'" for='+'"'+response.details.upper.ninethColumn[i].name+'" '+'class="sleep"></label></div>');
							}
						 
						
// 						$("#upper").append('<div style="display: inline;"><input  class="seats"'+ 'value="'+response.details.upper.ninethColumn[i].name+'|'+response.details.upper.ninethColumn[i].fare+'"type="checkbox"/> <label for="i1" class="seat"></label></div>');
					}
					console.log("Nineth Column Length is:: "+response.details.upper.ninethColumn.length);
					$("#upper").append('<br><br>');
					}
				}
				
				if (response.details.lowerShow==true) {
					
					
					$("#lower_show").show();
					
					console.log("lower Show  Exist :: ");
					
					var col=response.details.lower.length;
					
					if (response.details.lower.firstColumn!=null) {

					for(var i=0;i<response.details.lower.firstColumn.length;i++){
						
						var cl1='class="seats-wrp-in-sit"';
						
					
						if (response.details.lower.firstColumn[i].seatStyle.includes("ST")) {
							
						if (response.details.lower.firstColumn[i].seatType.includes("booked")) {
							$("#lower").append('<div class="seats-wrp-in-sit" ><input '+ 'id='+'\"'+response.details.lower.firstColumn[i].name+'"\ '+  'class="seats"'+ 'value="'+response.details.lower.firstColumn[i].name+'|'+response.details.lower.firstColumn[i].fare+'"type="checkbox"/> <label style="background: url(${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/seats4.png) no-repeat; pointer-events: none;line-height: 25px; cursor: pointer;overflow: hidden;transition: all 0.25s ease 0s;-webkit-transition: all 0.25s;" title="'+response.details.lower.firstColumn[i].name+'" for='+'"'+response.details.lower.firstColumn[i].name+'" '+'class="seat"></label></div>');	
						}
						else{
						$("#lower").append('<div class="seats-wrp-in-sit" ><input '+'id='+'"'+response.details.lower.firstColumn[i].name+'"'+ ' onclick="selectSeats(\''+response.details.lower.firstColumn[i].name+'\','+response.details.lower.firstColumn[i].fare+');"'+ 'class="seats"'+ 'value="'+response.details.lower.firstColumn[i].name+'|'+response.details.lower.firstColumn[i].fare+'"type="checkbox"/> <label title="'+response.details.lower.firstColumn[i].name+'" for='+'"'+response.details.lower.firstColumn[i].name+'" '+'class="seat"></label></div>');
						}	
					}
					 else{
						 if (response.details.lower.firstColumn[i].seatType.includes("booked")) {
								$("#lower").append('<div class="seats-wrp-in-sleep" ><input '+ 'id='+'\"'+response.details.lower.firstColumn[i].name+'"\ '+  'class="seats"'+ 'value="'+response.details.lower.firstColumn[i].name+'|'+response.details.lower.firstColumn[i].fare+'"type="checkbox"/> <label style="background: url(${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/sleep3.png) no-repeat; pointer-events: none;line-height: 25px; cursor: pointer;overflow: hidden;transition: all 0.25s ease 0s;-webkit-transition: all 0.25s;" title="'+response.details.lower.firstColumn[i].name+'" for='+'"'+response.details.lower.firstColumn[i].name+'" '+'class="sleep"></label></div>');	
							}
							else{
							$("#lower").append('<div class="seats-wrp-in-sleep" ><input '+'id='+'"'+response.details.lower.firstColumn[i].name+'"'+ ' onclick="selectSeats(\''+response.details.lower.firstColumn[i].name+'\','+response.details.lower.firstColumn[i].fare+');"'+ 'class="seats"'+ 'value="'+response.details.lower.firstColumn[i].name+'|'+response.details.lower.firstColumn[i].fare+'"type="checkbox"/> <label title="'+response.details.lower.firstColumn[i].name+'" for='+'"'+response.details.lower.firstColumn[i].name+'" '+'class="sleep"></label></div>');
							}
						 
						 }
						
						
						console.log("First Column i is:: "+i);
					}
// 					console.log("First Column Length is:: "+response.details.lower.firstColumn.length);
					$("#lower").append('<br><br>');
					}
				 
					if (response.details.lower.secondColumn!=null) {
					
					for(var i=0;i<response.details.lower.secondColumn.length;i++){
						
						if (response.details.lower.secondColumn[i].seatStyle.includes("ST")) {
							
						if (response.details.lower.secondColumn[i].seatType.includes("booked")) {
							$("#lower").append('<div  class="seats-wrp-in-sit"  ><input '+ 'id='+'"'+response.details.lower.secondColumn[i].name+'" '+  'class="seats"'+ 'value="'+response.details.lower.secondColumn[i].name+'|'+response.details.lower.secondColumn[i].fare+'"type="checkbox"/> <label style="background: url(${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/seats4.png) no-repeat; pointer-events: none;line-height: 25px; cursor: pointer;overflow: hidden;transition: all 0.25s ease 0s;-webkit-transition: all 0.25s;" title="'+response.details.lower.secondColumn[i].name+'" for='+'"'+response.details.lower.secondColumn[i].name+'" '+'class="seat"></label></div>');	
						}
						else{
						$("#lower").append('<div  class="seats-wrp-in-sit" ><input '+ 'id='+'"'+response.details.lower.secondColumn[i].name+'" '+ ' " onclick="selectSeats(\''+response.details.lower.secondColumn[i].name+'\','+response.details.lower.secondColumn[i].fare+');"'+ 'class="seats"'+ 'value="'+response.details.lower.secondColumn[i].name+'|'+response.details.lower.secondColumn[i].fare+'"type="checkbox"/> <label title="'+response.details.lower.secondColumn[i].name+'" for='+'"'+response.details.lower.secondColumn[i].name+'" '+'class="seat"></label></div>');
						}
					}
						else{
							if (response.details.lower.secondColumn[i].seatType.includes("booked")) {
								$("#lower").append('<div  class="seats-wrp-in-sleep"  ><input '+ 'id='+'"'+response.details.lower.secondColumn[i].name+'" '+  'class="seats"'+ 'value="'+response.details.lower.secondColumn[i].name+'|'+response.details.lower.secondColumn[i].fare+'"type="checkbox"/> <label style="background: url(${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/sleep3.png) no-repeat; pointer-events: none;line-height: 25px; cursor: pointer;overflow: hidden;transition: all 0.25s ease 0s;-webkit-transition: all 0.25s;" title="'+response.details.lower.secondColumn[i].name+'" for='+'"'+response.details.lower.secondColumn[i].name+'" '+'class="sleep"></label></div>');	
							}
							else{
							$("#lower").append('<div  class="seats-wrp-in-sleep" ><input '+ 'id='+'"'+response.details.lower.secondColumn[i].name+'" '+ ' " onclick="selectSeats(\''+response.details.lower.secondColumn[i].name+'\','+response.details.lower.secondColumn[i].fare+');"'+ 'class="seats"'+ 'value="'+response.details.lower.secondColumn[i].name+'|'+response.details.lower.secondColumn[i].fare+'"type="checkbox"/> <label title="'+response.details.lower.secondColumn[i].name+'" for='+'"'+response.details.lower.secondColumn[i].name+'" '+'class="sleep"></label></div>');
							}
						}
						
// 						$("#lower").append('<li><input '+ 'id='+'"'+response.details.lower.secondColumn[i].name+'" '+ 'class="seats"'+ 'value="'+response.details.lower.secondColumn[i].name+'|'+response.details.lower.secondColumn[i].fare+'"type="checkbox"/> <label for='+'"'+response.details.lower.secondColumn[i].name+'" '+'class="seat"></label></li>');
					}
					console.log("Second Column Length is:: "+response.details.lower.secondColumn.length);
					$("#lower").append('<br><br>');
					}
					
					if (response.details.lower.thirdColumn!=null) {

					for(var i=0;i<response.details.lower.thirdColumn.length;i++){
						
						if (response.details.lower.thirdColumn[i].seatStyle.includes("ST")) {
						
						if (response.details.lower.thirdColumn[i].seatType.includes("booked")) {
							$("#lower").append('<div class="seats-wrp-in-sit" ><input '+ 'id='+'"'+response.details.lower.thirdColumn[i].name+'" '+  'class="seats"'+ 'value="'+response.details.lower.thirdColumn[i].name+'|'+response.details.lower.thirdColumn[i].fare+'"type="checkbox"/> <label style="background: url(${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/seats4.png) no-repeat; pointer-events: none;line-height: 25px; cursor: pointer;overflow: hidden;transition: all 0.25s ease 0s;-webkit-transition: all 0.25s;" title="'+response.details.lower.thirdColumn[i].name+'" for='+'"'+response.details.lower.thirdColumn[i].name+'" '+'class="seat"></label></div>');	
						}
						else{
						$("#lower").append('<div class="seats-wrp-in-sit" ><input '+ 'id='+'"'+response.details.lower.thirdColumn[i].name+'" '+ ' " onclick="selectSeats(\''+response.details.lower.thirdColumn[i].name+'\','+response.details.lower.thirdColumn[i].fare+');"'+ 'class="seats"'+ 'value="'+response.details.lower.thirdColumn[i].name+'|'+response.details.lower.thirdColumn[i].fare+'"type="checkbox"/> <label title="'+response.details.lower.thirdColumn[i].name+'" for='+'"'+response.details.lower.thirdColumn[i].name+'" '+'class="seat"></label></div>');
						}
					}
						else {
							if (response.details.lower.thirdColumn[i].seatType.includes("booked")) {
								$("#lower").append('<div class="seats-wrp-in-sleep" ><input '+ 'id='+'"'+response.details.lower.thirdColumn[i].name+'" '+  'class="seats"'+ 'value="'+response.details.lower.thirdColumn[i].name+'|'+response.details.lower.thirdColumn[i].fare+'"type="checkbox"/> <label style="background: url(${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/sleep3.png) no-repeat; pointer-events: none;line-height: 25px; cursor: pointer;overflow: hidden;transition: all 0.25s ease 0s;-webkit-transition: all 0.25s;" title="'+response.details.lower.thirdColumn[i].name+'" for='+'"'+response.details.lower.thirdColumn[i].name+'" '+'class="sleep"></label></div>');	
							}
							else{
							$("#lower").append('<div class="seats-wrp-in-sleep" ><input '+ 'id='+'"'+response.details.lower.thirdColumn[i].name+'" '+ ' " onclick="selectSeats(\''+response.details.lower.thirdColumn[i].name+'\','+response.details.lower.thirdColumn[i].fare+');"'+ 'class="seats"'+ 'value="'+response.details.lower.thirdColumn[i].name+'|'+response.details.lower.thirdColumn[i].fare+'"type="checkbox"/> <label title="'+response.details.lower.thirdColumn[i].name+'" for='+'"'+response.details.lower.thirdColumn[i].name+'" '+'class="sleep"></label></div>');
							}
						}
						
// 						$("#lower").append('<li><input '+ 'id='+'"'+response.details.lower.thirdColumn[i].name+'" '+ 'class="seats"'+ 'value="'+response.details.lower.thirdColumn[i].name+'|'+response.details.lower.thirdColumn[i].fare+'"type="checkbox"/> <label for='+'"'+response.details.lower.thirdColumn[i].name+'" '+'class="seat"></label></li>');
					}
					console.log("Third Column Length is:: "+response.details.lower.thirdColumn.length);
					$("#lower").append('<br><br>');
					}
					
					if (response.details.lower.fourthColumn!=null) {
					
					for(var i=0;i<response.details.lower.fourthColumn.length;i++){
						
						if (response.details.lower.fourthColumn[i].seatStyle.includes("ST")) {
						
						if (response.details.lower.fourthColumn[i].seatType.includes("booked")) {
							$("#lower").append('<div  class="seats-wrp-in-sit"  ><input '+ 'id='+'"'+response.details.lower.fourthColumn[i].name+'" '+  'class="seats"'+ 'value="'+response.details.lower.fourthColumn[i].name+'|'+response.details.lower.fourthColumn[i].fare+'"type="checkbox"/> <label style="background: url(${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/seats4.png) no-repeat; pointer-events: none;line-height: 25px; cursor: pointer;overflow: hidden;transition: all 0.25s ease 0s;-webkit-transition: all 0.25s;" title="'+response.details.lower.fourthColumn[i].name+'" for='+'"'+response.details.lower.fourthColumn[i].name+'" '+'class="seat"></label></div>');	
						}
						else{
						$("#lower").append('<div class="seats-wrp-in-sit" ><input '+ 'id='+'"'+response.details.lower.fourthColumn[i].name+'" '+ ' " onclick="selectSeats(\''+response.details.lower.fourthColumn[i].name+'\','+response.details.lower.fourthColumn[i].fare+');"'+ 'class="seats"'+ 'value="'+response.details.lower.fourthColumn[i].name+'|'+response.details.lower.fourthColumn[i].fare+'"type="checkbox"/> <label title="'+response.details.lower.fourthColumn[i].name+'" for='+'"'+response.details.lower.fourthColumn[i].name+'" '+'class="seat"></label></div>');
						}
					 }
						else {
							if (response.details.lower.fourthColumn[i].seatType.includes("booked")) {
								$("#lower").append('<div  class="seats-wrp-in-sleep"  ><input '+ 'id='+'"'+response.details.lower.fourthColumn[i].name+'" '+  'class="seats"'+ 'value="'+response.details.lower.fourthColumn[i].name+'|'+response.details.lower.fourthColumn[i].fare+'"type="checkbox"/> <label style="background: url(${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/sleep3.png) no-repeat; pointer-events: none;line-height: 25px; cursor: pointer;overflow: hidden;transition: all 0.25s ease 0s;-webkit-transition: all 0.25s;" title="'+response.details.lower.fourthColumn[i].name+'" for='+'"'+response.details.lower.fourthColumn[i].name+'" '+'class="sleep"></label></div>');	
							}
							else{
							$("#lower").append('<div class="seats-wrp-in-sleep" ><input '+ 'id='+'"'+response.details.lower.fourthColumn[i].name+'" '+ ' " onclick="selectSeats(\''+response.details.lower.fourthColumn[i].name+'\','+response.details.lower.fourthColumn[i].fare+');"'+ 'class="seats"'+ 'value="'+response.details.lower.fourthColumn[i].name+'|'+response.details.lower.fourthColumn[i].fare+'"type="checkbox"/> <label title="'+response.details.lower.fourthColumn[i].name+'" for='+'"'+response.details.lower.fourthColumn[i].name+'" '+'class="sleep"></label></div>');
							}
						}
// 						$("#lower").append('<li><input '+ 'id='+'"'+response.details.lower.fourthColumn[i].name+'" '+ 'class="seats"'+ 'value="'+response.details.lower.fourthColumn[i].name+'|'+response.details.lower.fourthColumn[i].fare+'"type="checkbox"/> <label for='+'"'+response.details.lower.fourthColumn[i].name+'" '+'class="seat"></label></li>');
					}
					console.log("Fourth Column Length is:: "+response.details.lower.fourthColumn.length);
					$("#lower").append('<br><br>');
					}
					
					if (response.details.lower.fifthColumn!=null) {
					for(var i=0;i<response.details.lower.fifthColumn.length;i++){
						
						if (response.details.lower.fifthColumn[i].seatStyle.includes("ST")) {
						
						if (response.details.lower.fifthColumn[i].seatType.includes("booked")) {
							$("#lower").append('<div class="seats-wrp-in-sit" ><input '+ 'id='+'"'+response.details.lower.fifthColumn[i].name+'" '+  'class="seats"'+ 'value="'+response.details.lower.fifthColumn[i].name+'|'+response.details.lower.fifthColumn[i].fare+'"type="checkbox"/> <label style="background: url(${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/seats4.png) no-repeat; pointer-events: none;line-height: 25px; cursor: pointer;overflow: hidden;transition: all 0.25s ease 0s;-webkit-transition: all 0.25s;" title="'+response.details.lower.fifthColumn[i].name+'" for='+'"'+response.details.lower.fifthColumn[i].name+'" '+'class="seat"></label></div>');	
						}
						else{
						$("#lower").append('<div class="seats-wrp-in-sit" ><input '+ 'id='+'"'+response.details.lower.fifthColumn[i].name+'" '+ ' " onclick="selectSeats(\''+response.details.lower.fifthColumn[i].name+'\','+response.details.lower.fifthColumn[i].fare+');"'+ 'class="seats"'+ 'value="'+response.details.lower.fifthColumn[i].name+'|'+response.details.lower.fifthColumn[i].fare+'"type="checkbox"/> <label title="'+response.details.lower.fifthColumn[i].name+'" for='+'"'+response.details.lower.fifthColumn[i].name+'" '+'class="seat"></label></div>');
						}
					  }
						else {
							if (response.details.lower.fifthColumn[i].seatType.includes("booked")) {
								$("#lower").append('<div class="seats-wrp-in-sleep" ><input '+ 'id='+'"'+response.details.lower.fifthColumn[i].name+'" '+  'class="seats"'+ 'value="'+response.details.lower.fifthColumn[i].name+'|'+response.details.lower.fifthColumn[i].fare+'"type="checkbox"/> <label style="background: url(${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/sleep3.png) no-repeat; pointer-events: none;line-height: 25px; cursor: pointer;overflow: hidden;transition: all 0.25s ease 0s;-webkit-transition: all 0.25s;" title="'+response.details.lower.fifthColumn[i].name+'" for='+'"'+response.details.lower.fifthColumn[i].name+'" '+'class="sleep"></label></div>');	
							}
							else{
							$("#lower").append('<div class="seats-wrp-in-sleep" ><input '+ 'id='+'"'+response.details.lower.fifthColumn[i].name+'" '+ ' " onclick="selectSeats(\''+response.details.lower.fifthColumn[i].name+'\','+response.details.lower.fifthColumn[i].fare+');"'+ 'class="seats"'+ 'value="'+response.details.lower.fifthColumn[i].name+'|'+response.details.lower.fifthColumn[i].fare+'"type="checkbox"/> <label title="'+response.details.lower.fifthColumn[i].name+'" for='+'"'+response.details.lower.fifthColumn[i].name+'" '+'class="sleep"></label></div>');
							}
						}
// 						$("#lower").append('<li><input '+ 'id='+'"'+response.details.lower.fifthColumn[i].name+'" '+ 'class="seats"'+ 'value="'+response.details.lower.fifthColumn[i].name+'|'+response.details.lower.fifthColumn[i].fare+'"type="checkbox"/> <label for='+'"'+response.details.lower.fifthColumn[i].name+'" '+'class="seat"></label></li>');
					}
					console.log("Fifth Column Length is:: "+response.details.lower.fifthColumn.length);
					$("#lower").append('<br><br>');
					}		
					if (response.details.lower.sixthColumn!=null) {
					for(var i=0;i<response.details.lower.sixthColumn.length;i++){
						
						if (response.details.lower.sixthColumn[i].seatStyle.includes("ST")) {
						
						if (response.details.lower.sixthColumn[i].seatType.includes("booked")) {
							$("#lower").append('<div class="seats-wrp-in-sit" ><input '+ 'id='+'"'+response.details.lower.sixthColumn[i].name+'" '+  'class="seats"'+ 'value="'+response.details.lower.sixthColumn[i].name+'|'+response.details.lower.sixthColumn[i].fare+'"type="checkbox"/> <label style="background: url(${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/seats4.png) no-repeat; pointer-events: none;line-height: 25px; cursor: pointer;overflow: hidden;transition: all 0.25s ease 0s;-webkit-transition: all 0.25s;" title="'+response.details.lower.sixthColumn[i].name+'" for='+'"'+response.details.lower.sixthColumn[i].name+'" '+'class="seat"></label></div>');	
						}
						else{
						$("#lower").append('<div class="seats-wrp-in-sit" ><input '+ 'id='+'"'+response.details.lower.sixthColumn[i].name+'" '+ ' " onclick="selectSeats(\''+response.details.lower.sixthColumn[i].name+'\','+response.details.lower.sixthColumn[i].fare+');"'+ 'class="seats"'+ 'value="'+response.details.lower.sixthColumn[i].name+'|'+response.details.lower.sixthColumn[i].fare+'"type="checkbox"/> <label title="'+response.details.lower.sixthColumn[i].name+'" for='+'"'+response.details.lower.sixthColumn[i].name+'" '+'class="seat"></label></div>');
						}
					 }	
						else {
							if (response.details.lower.sixthColumn[i].seatType.includes("booked")) {
								$("#lower").append('<div class="seats-wrp-in-sleep" ><input '+ 'id='+'"'+response.details.lower.sixthColumn[i].name+'" '+  'class="seats"'+ 'value="'+response.details.lower.sixthColumn[i].name+'|'+response.details.lower.sixthColumn[i].fare+'"type="checkbox"/> <label style="background: url(${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/sleep3.png) no-repeat; pointer-events: none;line-height: 25px; cursor: pointer;overflow: hidden;transition: all 0.25s ease 0s;-webkit-transition: all 0.25s;" title="'+response.details.lower.sixthColumn[i].name+'" for='+'"'+response.details.lower.sixthColumn[i].name+'" '+'class="sleep"></label></div>');	
							}
							else{
							$("#lower").append('<div class="seats-wrp-in-sleep" ><input '+ 'id='+'"'+response.details.lower.sixthColumn[i].name+'" '+ ' " onclick="selectSeats(\''+response.details.lower.sixthColumn[i].name+'\','+response.details.lower.sixthColumn[i].fare+');"'+ 'class="seats"'+ 'value="'+response.details.lower.sixthColumn[i].name+'|'+response.details.lower.sixthColumn[i].fare+'"type="checkbox"/> <label title="'+response.details.lower.sixthColumn[i].name+'" for='+'"'+response.details.lower.sixthColumn[i].name+'" '+'class="sleep"></label></div>');
							}
						}
// 						$("#lower").append('<li><input '+ 'id='+'"'+response.details.lower.sixthColumn[i].name+'" '+ 'class="seats"'+ 'value="'+response.details.lower.sixthColumn[i].name+'|'+response.details.lower.sixthColumn[i].fare+'"type="checkbox"/> <label for='+'"'+response.details.lower.sixthColumn[i].name+'" '+'class="seat"></label></li>');
					}
					console.log("sixth Column Length is:: "+response.details.lower.sixthColumn.length);
					$("#lower").append('<br><br>');
					}
					if (response.details.lower.seventhColumn!=null) {
					
					for(var i=0;i<response.details.lower.seventhColumn.length;i++){
					
						if (response.details.lower.seventhColumn[i].seatStyle.includes("ST")) {
						
						if (response.details.lower.seventhColumn[i].seatType.includes("booked")) {
							$("#lower").append('<div class="seats-wrp-in-sit" ><input '+ 'id='+'"'+response.details.lower.seventhColumn[i].name+'" '+  'class="seats"'+ 'value="'+response.details.lower.seventhColumn[i].name+'|'+response.details.lower.seventhColumn[i].fare+'"type="checkbox"/> <label style="background: url(${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/seats4.png) no-repeat; pointer-events: none;line-height: 25px; cursor: pointer;overflow: hidden;transition: all 0.25s ease 0s;-webkit-transition: all 0.25s;" title="'+response.details.lower.seventhColumn[i].name+'" for='+'"'+response.details.lower.seventhColumn[i].name+'" '+'class="seat"></label></div>');	
						}
						else{
						$("#lower").append('<div class="seats-wrp-in-sit" ><input '+ 'id='+'"'+response.details.lower.seventhColumn[i].name+'" '+ ' " onclick="selectSeats(\''+response.details.lower.seventhColumn[i].name+'\','+response.details.lower.seventhColumn[i].fare+');"'+ 'class="seats"'+ 'value="'+response.details.lower.seventhColumn[i].name+'|'+response.details.lower.seventhColumn[i].fare+'"type="checkbox"/> <label title="'+response.details.lower.seventhColumn[i].name+'" for='+'"'+response.details.lower.seventhColumn[i].name+'" '+'class="seat"></label></div>');
						}
					 }
						else {
							if (response.details.lower.seventhColumn[i].seatType.includes("booked")) {
								$("#lower").append('<div class="seats-wrp-in-sleep" ><input '+ 'id='+'"'+response.details.lower.seventhColumn[i].name+'" '+  'class="seats"'+ 'value="'+response.details.lower.seventhColumn[i].name+'|'+response.details.lower.seventhColumn[i].fare+'"type="checkbox"/> <label style="background: url(${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/sleep3.png) no-repeat; pointer-events: none;line-height: 25px; cursor: pointer;overflow: hidden;transition: all 0.25s ease 0s;-webkit-transition: all 0.25s;" title="'+response.details.lower.seventhColumn[i].name+'" for='+'"'+response.details.lower.seventhColumn[i].name+'" '+'class="sleep"></label></div>');	
							}
							else{
							$("#lower").append('<div class="seats-wrp-in-sleep" ><input '+ 'id='+'"'+response.details.lower.seventhColumn[i].name+'" '+ ' " onclick="selectSeats(\''+response.details.lower.seventhColumn[i].name+'\','+response.details.lower.seventhColumn[i].fare+');"'+ 'class="seats"'+ 'value="'+response.details.lower.seventhColumn[i].name+'|'+response.details.lower.seventhColumn[i].fare+'"type="checkbox"/> <label title="'+response.details.lower.seventhColumn[i].name+'" for='+'"'+response.details.lower.seventhColumn[i].name+'" '+'class="sleep"></label></div>');
							}
						}
// 						$("#lower").append('<li><input '+ 'id='+'"'+response.details.lower.seventhColumn[i].name+'" '+ 'class="seats"'+ 'value="'+response.details.lower.seventhColumn[i].name+'|'+response.details.lower.seventhColumn[i].fare+'"type="checkbox"/> <label for='+'"'+response.details.lower.seventhColumn[i].name+'" '+'class="seat"></label></li>');
					}
					console.log("Seventh Column Length is:: "+response.details.lower.seventhColumn.length);
					$("#lower").append('<br><br>');
					}
					
					if (response.details.lower.eightColumn!=null) {
					for(var i=0;i<response.details.lower.eightColumn.length;i++){
					
						if (response.details.lower.eightColumn[i].seatStyle.includes("ST")) {
						
						if (response.details.lower.eightColumn[i].seatType.includes("booked")) {
							$("#lower").append('<div class="seats-wrp-in-sit" ><input '+ 'id='+'"'+response.details.lower.eightColumn[i].name+'" '+  'class="seats"'+ 'value="'+response.details.lower.eightColumn[i].name+'|'+response.details.lower.eightColumn[i].fare+'"type="checkbox"/> <label style="background: url(${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/seats4.png) no-repeat; pointer-events: none;line-height: 25px; cursor: pointer;overflow: hidden;transition: all 0.25s ease 0s;-webkit-transition: all 0.25s;" title="'+response.details.lower.eightColumn[i].name+'" for='+'"'+response.details.lower.eightColumn[i].name+'" '+'class="seat"></label></div>');	
						}
						else{
						$("#lower").append('<div class="seats-wrp-in-sit" ><input '+ 'id='+'"'+response.details.lower.eightColumn[i].name+'" '+ ' " onclick="selectSeats(\''+response.details.lower.eightColumn[i].name+'\','+response.details.lower.eightColumn[i].fare+');"'+ 'class="seats"'+ 'value="'+response.details.lower.eightColumn[i].name+'|'+response.details.lower.eightColumn[i].fare+'"type="checkbox"/> <label title="'+response.details.lower.eightColumn[i].name+'" for='+'"'+response.details.lower.eightColumn[i].name+'" '+'class="seat"></label></div>');
						}
					 }
						else {
							if (response.details.lower.eightColumn[i].seatType.includes("booked")) {
								$("#lower").append('<div class="seats-wrp-in-sleep" ><input '+ 'id='+'"'+response.details.lower.eightColumn[i].name+'" '+  'class="seats"'+ 'value="'+response.details.lower.eightColumn[i].name+'|'+response.details.lower.eightColumn[i].fare+'"type="checkbox"/> <label style="background: url(${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/sleep3.png) no-repeat; pointer-events: none;line-height: 25px; cursor: pointer;overflow: hidden;transition: all 0.25s ease 0s;-webkit-transition: all 0.25s;" title="'+response.details.lower.eightColumn[i].name+'" for='+'"'+response.details.lower.eightColumn[i].name+'" '+'class="sleep"></label></div>');	
							}
							else{
							$("#lower").append('<div class="seats-wrp-in-sleep" ><input '+ 'id='+'"'+response.details.lower.eightColumn[i].name+'" '+ ' " onclick="selectSeats(\''+response.details.lower.eightColumn[i].name+'\','+response.details.lower.eightColumn[i].fare+');"'+ 'class="seats"'+ 'value="'+response.details.lower.eightColumn[i].name+'|'+response.details.lower.eightColumn[i].fare+'"type="checkbox"/> <label title="'+response.details.lower.eightColumn[i].name+'" for='+'"'+response.details.lower.eightColumn[i].name+'" '+'class="sleep"></label></div>');
							}
						}
// 						$("#lower").append('<li><input '+ 'id='+'"'+response.details.lower.eightColumn[i].name+'" '+ 'class="seats"'+ 'value="'+response.details.lower.eightColumn[i].name+'|'+response.details.lower.eightColumn[i].fare+'"type="checkbox"/> <label for='+'"'+response.details.lower.eightColumn[i].name+'" '+'class="seat"></label></li>');
					}
					console.log("Eight Column Length is:: "+response.details.lower.eightColumn.length);
					$("#lower").append('<br><br>');
					}
					if (response.details.lower.ninethColumn!=null) {
					
					for(var i=0;i<response.details.lower.ninethColumn.length;i++){
					
						if (response.details.lower.ninethColumn[i].seatStyle.includes("ST")) {
						
						if (response.details.lower.ninethColumn[i].seatType.includes("booked")) {
							$("#lower").append('<div class="seats-wrp-in-sit" ><input '+ 'id='+'"'+response.details.lower.ninethColumn[i].name+'" '+  'class="seats"'+ 'value="'+response.details.lower.ninethColumn[i].name+'|'+response.details.lower.ninethColumn[i].fare+'"type="checkbox"/> <label style="background: url(${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/seats4.png) no-repeat; pointer-events: none;line-height: 25px; cursor: pointer;overflow: hidden;transition: all 0.25s ease 0s;-webkit-transition: all 0.25s;" title="'+response.details.lower.ninethColumn[i].name+'" for='+'"'+response.details.lower.ninethColumn[i].name+'" '+'class="seat"></label></div>');	
						}
						else{
						$("#lower").append('<div class="seats-wrp-in-sit" ><input '+ 'id='+'"'+response.details.lower.ninethColumn[i].name+'" '+ ' " onclick="selectSeats(\''+response.details.lower.ninethColumn[i].name+'\','+response.details.lower.ninethColumn[i].fare+');"'+ 'class="seats"'+ 'value="'+response.details.lower.ninethColumn[i].name+'|'+response.details.lower.ninethColumn[i].fare+'"type="checkbox"/> <label title="'+response.details.lower.ninethColumn[i].name+'" for='+'"'+response.details.lower.ninethColumn[i].name+'" '+'class="seat"></label></div>');
						}
					 }
						else {
							if (response.details.lower.ninethColumn[i].seatType.includes("booked")) {
								$("#lower").append('<div class="seats-wrp-in-sleep" ><input '+ 'id='+'"'+response.details.lower.ninethColumn[i].name+'" '+  'class="seats"'+ 'value="'+response.details.lower.ninethColumn[i].name+'|'+response.details.lower.ninethColumn[i].fare+'"type="checkbox"/> <label style="background: url(${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/sleep3.png) no-repeat; pointer-events: none;line-height: 25px; cursor: pointer;overflow: hidden;transition: all 0.25s ease 0s;-webkit-transition: all 0.25s;" title="'+response.details.lower.ninethColumn[i].name+'" for='+'"'+response.details.lower.ninethColumn[i].name+'" '+'class="sleep"></label></div>');	
							}
							else{
							$("#lower").append('<div class="seats-wrp-in-sleep" ><input '+ 'id='+'"'+response.details.lower.ninethColumn[i].name+'" '+ ' " onclick="selectSeats(\''+response.details.lower.ninethColumn[i].name+'\','+response.details.lower.ninethColumn[i].fare+');"'+ 'class="seats"'+ 'value="'+response.details.lower.ninethColumn[i].name+'|'+response.details.lower.ninethColumn[i].fare+'"type="checkbox"/> <label title="'+response.details.lower.ninethColumn[i].name+'" for='+'"'+response.details.lower.ninethColumn[i].name+'" '+'class="sleep"></label></div>');
							}
						}
// 						$("#lower").append('<li><input '+ 'id='+'"'+response.details.lower.ninethColumn[i].name+'" '+ 'class="seats"'+ 'value="'+response.details.lower.ninethColumn[i].name+'|'+response.details.lower.ninethColumn[i].fare+'"type="checkbox"/> <label for='+'"'+response.details.lower.ninethColumn[i].name+'" '+'class="seat"></label></li>');
					}
					console.log("Nineth Column Length is:: "+response.details.lower.ninethColumn.length);
					$("#lower").append('<br><br>');
					} 
					}
				}
				else{
					window.location.href='${pageContext.request.contextPath}/User/Travel/Bus/GetAllAvailableTrips';
				}
			}
		}); 
		
	}
	

	/* function  selectSeats(seatNo,fare)
	{
			
			console.log(seatNo);
			console.log(fare);
			
			$('#errSeats').empty();
			
		var brd=$("#bdId").text();
		$("#m_bus_boarding_val").val(brd);
		var seat=$("#seat_number").text();
		var tFare=$("#bus_fare_val").val();
		var fares=$("#m_bus_fares").val();

		
		if (tFare=="") {
			tFare=0;
			console.log("Ticket Fare:: "+tFare);
		}else {
			
		}
			
		if (seat=='' ||seat==' ') {
			$("#seat_number").text(seatNo);
			console.log('if');
			$("#bus_fare_val").val(parseInt(tFare)+parseInt(fare));
			console.log(parseInt(tFare)+parseInt(fare));
			$("#bus_fare").html(parseInt(tFare)+parseInt(fare));
			$("#m_bus_seats_val").val(seatNo);
			$("#m_bus_fares").val(fare);
			
		
			console.log('Inside if fares are: '+fare);
			
		}else{
			console.log('else');
			var valid=true;
			var useats="";
			var seats=seat.split(" ");
			var faresSplit=fares.split(",");
			var bfare="";
			
			for (var i = 0; i < seats.length; i++) {
// 			console.log("Split Seat"+i+" is:: "+seats[i]);
			
			if (seats[i].includes(seatNo)) {
				valid=false;
				
			}else{
				 useats+=seats[i]+' ';
				 bfare+=faresSplit[i]+' ';
			}
			}
			if (valid===true) {
				console.log('Seat add');
				console.log('fare increase');
				$("#seat_number").text(seat+' '+seatNo);
				 $("#bus_fare_val").val(parseInt(tFare)+parseInt(fare));
				 console.log(parseInt(tFare)+parseInt(fare));
				 $("#bus_fare").html(parseInt(tFare)+parseInt(fare));
				 $("#m_bus_seats_val").val(seat+' '+seatNo);
				 
				 $("#m_bus_fares").val(fares+','+fare);
				 
				 console.log("Inside true Fares are:: "+fares+','+fare);
			}
			else{
				console.log('Seat removed');
				console.log('fare decrised');
				$("#seat_number").text(useats);
				$("#m_bus_seats_val").val(useats);
				$("#bus_fare_val").val(parseInt(tFare)-parseInt(fare));
				console.log(parseInt(tFare)-parseInt(fare));
				$("#bus_fare").html(parseInt(tFare)-parseInt(fare));
				$("#m_bus_fares").val(bfare);
				
				if (useats=='' || useats==' ') {
					$("#m_bus_fares").val("");
					bfare="";
					console.log("Fares Are Empty");
				}
				
				console.log("Inside false Fares are:: "+bfare);
			}
		}
		
		
		} */
		
		var seatsArr=[];
		var totalFare=0;
		var totalFareArr=[];
		
		function  selectSeats(seatNo,fare)
		{
		
			var seatsHtml="";
			var fareHtml="";
			
			$('#errSeats').empty();
			
			if (seatsArr.indexOf(seatNo)==-1) {
			
				seatsArr.push(seatNo);
				totalFare=parseInt(totalFare)+parseInt(fare);
				totalFareArr.push(fare);
			}
			else{
				var a=seatsArr.indexOf(seatNo);
				var f=totalFareArr.indexOf(fare);

				seatsArr.splice(a,1);
				totalFareArr.splice(a,1);
				
				if (totalFare>0) {
					totalFare=parseInt(totalFare)-parseInt(fare);
				}
				
			}
			
			for (var i = 0; i < seatsArr.length; i++) {
				if (i==0) {
					seatsHtml=seatsArr[i];
				}else{
					seatsHtml=seatsHtml+" "+seatsArr[i];
				}
			}
			
			for (var i = 0; i < totalFareArr.length; i++) {
				if (i==0) {
					fareHtml=totalFareArr[i];
				}else{
				fareHtml=fareHtml+","+totalFareArr[i];
				}
			}
			
			
			$("#bus_fare").html(totalFare);
			$("#seat_number").text(seatsHtml);
			
			$("#bus_fare_val").val(totalFare);
			
			$("#m_bus_fares").val(fareHtml);
			$("#m_bus_seats_val").val(seatsHtml);
		}
		
		
		
</script>



<!-- Ajax Calling for Get city List -->
<script>
$(document).ready(function() {

	var minAmt=$('#minAmt').val();     
	var maxAmt=$('#maxAmt').val();   
	
	$(function () {
	      $('#slider-container').slider({
	          range: true,
	          min: parseInt(minAmt),
	          max: parseInt(maxAmt),
	          values: [parseInt(minAmt), parseInt(maxAmt)],
	          create: function() {
	              $("#amount").val("Rs "+ parseInt(minAmt) + " - RS " + parseInt(maxAmt));
	          },
	          slide: function (event, ui) {
	              $("#amount").val("Rs" + ui.values[0] + " - Rs" + ui.values[1]);
	              var mi = ui.values[0];
	              var mx = ui.values[1];
	              filterSystem(mi, mx);
	          }
	      })
	});

	  function filterSystem(minPrice, maxPrice) {
	      $("#computers div.system").hide().filter(function () {
	          var price = parseInt($(this).data("price"), 10);
	          return price >= minPrice && price <= maxPrice;
	      }).show();
	  }
	
	
	$('#bus_src_list').empty();
	$('#bus_dest_list').empty();
	console.log();
	var srcvalue=$('#srcvalue').val();
	var destvalue=$('#destvalue').val();
	var srcvalueId=$('#srcvalueId').val();
	var destvalueId=$('#destvalueId').val();
	$('#bus_src_list').append('<option value="' + srcvalueId + '">' + srcvalue + '</option>');
	$('#bus_dest_list').append('<option value="' + destvalueId + '">' + destvalue + '</option>');
// 	$('.tooltips').hide();
// 	$('#asxc').val("working");
	$(".js-example-basic-multiple").select2();
		$.ajax({
			type : "POST",
//  			headers: headers,
			contentType : "application/json",
			url :contextPath+"/User/Travel/Bus/GetAllCityList",
			dataType : 'json',
			data : JSON.stringify({
				
			}),
			success : function(response) {
				console.log("response is "+response.code);
				console.log("response is "+response.details.length);
				console.log("response is "+response.details[0].cityName);
				
				for(var i=0;i<response.details.length;i++){
					
				$('#bus_src_list').append('<option value="' + response.details[i].cityId + '">' + response.details[i].cityName + '</option>');
				$('#bus_dest_list').append('<option value="' + response.details[i].cityId + '">' + response.details[i].cityName + '</option>');
			
				}
				
			}
		}); 
		
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1; //January is 0!

		var yyyy = today.getFullYear();
		if(dd<10){
		    dd='0'+dd;
		} 
		if(mm<10){
		    mm='0'+mm;
		} 
		var today = dd+'/'+mm+'/'+yyyy;
// 		document.getElementById("date").value = today;
		
// 		$("#depDateicon").val(today);
// 		$("#depDate").val(today);
		
	});
</script>

<!-- Script for date picker -->
<script>
	$(function() {
		$("#depDate").datepicker({
			startDate: '+1d',
			format : "dd-mm-yyyy"
		}).on('change', function() {
			$('.datepicker').hide();
		});
	});
</script>
<script>
$("#getSeatDetails_submit").click(function () {
	
// 	$('#loading').modal("show");

	$('#loading').modal({
        backdrop: 'static',
       keyboard: true,  
       show: true
}); 
	
});

</script>


<script type="text/javascript">
function validateForm(){
	
	console.log("Valid form");
	
	var sCity=$('#bus_src_list').val();
	var dCity=$('#bus_dest_list').val();
	var dDate=$('#depDate').val();
	
	var valid=true;
	console.log(sCity);
	console.log(dCity);
	console.log(dDate);
	
	
	if (sCity=='#') {
		$('#errSrcCity').text("Please select Source city");
		valid=false;
	}
	if (dCity=='#') {
		valid=false;
		$('#errDestCity').text("Please select Destination city");
	}
	if (dDate=='') {
		valid=false;
		$('#errDeptdate').text("Please select Departure Date");
	}
	if (valid==true) {
		console.log("Spinner Added");
// 		var spinnerUrl = "Please wait <img src="+contextPath+"'/resources/images/spinner.gif' style='width:25px;'>"
		$("#submit_Bus_Search").addClass("disabled");
		
		$('#loading').modal({
	        backdrop: 'static',
	       keyboard: true,  
	       show: true
	  }); 
		
// 		$("#submit_Bus_Search").html(spinnerUrl);
		}
	return valid;
 
}

</script>



<script type="text/javascript">
function checkForm(){
	
	console.log("Valid form");
	
	var seats=$('#m_bus_seats_val').val();
	var bdId=$('#boardingId').val();
	var dpId=$('#dropingId').val();
	
	var valid=true;
	 console.log("Seats:: "+seats.length);
	console.log(bdId);
	console.log(dpId); 
	
	
	if (seats.trim()=='') {
		$('#errSeats').text("Please select Seats");
		valid=false;
	}
	else if (seats==' ') {
		$('#errSeats').text("Please select Seats");
		valid=false;
	}
	if (bdId=='#') {
		valid=false;
		$('#errbd').text("Please Select Boarding Location");
	}
	if (dpId=='#') {
		valid=false;
		$('#errdp').text("Please Select Dropping Location");
	}
	
	if (valid==true) {
// 	console.log("Spinner Added");
	var spinnerUrl = "Please wait <img src="+contextPath+"'/resources/images/spinner.gif' style='width:25px;'>"
	$("#cnfm_seats").addClass("disabled");
	$("#cnfm_seats").html(spinnerUrl);
	}
	return valid;
	
	
}

</script>

<!-- ========================Script for price slider==================== -->
    <script>
      $("#ex16b").slider({ min: 5499, max: 17171, value: [0, 17171], focus: true });
    </script>

 <script type="text/javascript">
         	$(function(){
        		
        		var txnErrMsg=$("#txnErrMsg").val();
        		console.log("txnErrMsg :: " + txnErrMsg);
        		if(txnErrMsg.length != 0){
        			$("#myModal").modal("show");
        			$("#txnErrMsgVal").html(txnErrMsg);
        			$('#txnErrMsg').val("");	
        			
        			/* var timeout = setTimeout(function(){
        				$("#myModal").modal("hide");
        				window.location.href='${pageContext.request.contextPath}/User/Travel/Flight/Source';		
        	          }, 3000); */
        			/* var timeout = setTimeout(function(){
        				$("#common_success_true").on('hidden.bs.modal', function ()  {
            				window.location.href='${pageContext.request.contextPath}/User/Travel/Flight/Source';		
    					});
        	          }, 3000); */
        		}
         	});
         </script>

         <script>
         	$(function(){
         		$("#clsesits").click(function(){
				    $('body').css({overflow: ''});
				});
         	})
         	
         </script>
         
         
         	
	<script src="https://harvesthq.github.io/chosen/chosen.jquery.js"></script> 
</body>

</html>