<!DOCTYPE HTML>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<html>
<head>
    <title>VPayQwik | Bus CheckOut</title>

<link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'
	type="image/png" />
    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!--- GOOGLE FONTS - -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600' rel='stylesheet' type='text/css'>
    <!-- /GOOGLE FONTS -->

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

   <link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/icomoon.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/styles.css">
    <script src="${pageContext.request.contextPath}/resources/js/Travel/travel.js"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/userdetails.js"/>"></script>
    <!-- <link rel="stylesheet" href="css/mystyles.css">   -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/new_css/default.css">
  
  <style>
		.rederror {
			font-size: 12px;
    		color: red;
		}
		.age{
			width: 52px;
		}
	</style>
	
	  <script>
  $(document).ready(function(){
      $("#nxt").click(function(){
          $(".collapse").collapse('show');
          $("#prev").show();
          $("#nxt").hide();
      });
      $("#prev").click(function(){
          $(".collapse").collapse('hide');
          $("#prev").hide();
          $("#nxt").show();
      });
  });
  </script>
</head>


<body style="background: #efefef;" onload="noBack();">
<jsp:include page="/WEB-INF/jsp/User/Header.jsp" />
<div class="topline"></div>

    <div class="blank" style="height: 50px;"></div>

    <div class="container-fluid bus-content">
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <div class="panel panel-default">
            <div class="panel-heading">
              <img src="${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/Bus1.png" style="width: 3%;"> &nbsp;&nbsp;
              <span class="rybb">Review your bus booking</span>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-12 col-sm-12">
                  <div class="col-md-3 col-sm-3">
                    <div class="row">
                      <img src="${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/Bus.png" style="width: 16%;">
                      <span class="st-bus">${source}</span>&nbsp;
                      <span class="bus-arr">→</span>&nbsp;
                      <span class="ed-bus">${destination}</span><br>
                      <span class="busdt">${date}</span><br><br>
                      <span class="bus-op">Operator: </span><span class="bus-op1">${travelName}${busType}</span><br>
                      <span class="bus-point">Boarding Point: </span><span class="bus-point1">${boarding}</span>
                    </div>
                  </div>
                  <div class="col-md-5 col-sm-5">
                    <div class="row">
                      <div class="col-md-4 col-sm-4" style="text-align: center;">
                        <span class="Btim">Boarding Time</span><br>
                        <span class="tim">${dTime}</span>
                      </div>
                      <div class="col-md-4 col-sm-4" style="text-align: center;">
                        <img src="${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/clock.png" style="width: 20%;"><br>
                        <span>${duration}</span>
                      </div>
                      <div class="col-md-4 col-sm-4" style="text-align: center;">
                        <span class="Atim">Arrival Time</span><br>
                        <span class="tim">${aTime}</span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="row">
                      <div class="row">
                        <div class="col-md-6 col-sm-6" style="text-align: center;">
                          <span class="Sitno">Seat no(s)</span><br>
                          <span class="sits">${seats}</span>
                        </div>
                        <div class="col-md-6 col-sm-6" style="text-align: center;">
                          <span class="Bpsngr">No of Passengers(s)</span><br>
                          <span class="psngr">${totalSeats}</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div><hr>
              <div class="row">
                <div class="col-md-12 col-sm-12">
                  <div class="col-md-6 col-sm-6">
                    <span class="sit-prc">Total: </span>&nbsp;
                    <span class="fa fa-inr rupsym"></span>&nbsp;
                    <span class="sit-prc">${totalAmt}</span>
                    <h6 style="color: red;">** Note: Dynamic fare is applicable so fare may be increase</h6>
                    <input type="hidden" value="${totalpasSize}" id="noOfPas">
                  </div>
                  
                  <div class="col-md-2 col-md-offset-4">
                    <button class="btn btn-primary contnu" id="nxt">Next</button>
                    <button class="btn btn-primary contnu" id="prev" style="display: none;">Preview</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      
      
      <form action="${pageContext.request.contextPath}/User/Travel/Bus/getTxnIdUpdate" method="post" onsubmit="return validateForm()">
      <div class="panel-group">
          <div id="demo" class="collapse">
            <div class="panel panel-default">
              <div class="panel-heading">
                <img src="${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/Bus1.png" style="width: 3%;">
                <span  class="rybb">Travellers Details</span>
              </div>
 			
 			  
              <div class="panel-body">
              <!-- ============================= USER INFORMATION STARTS HERE ===================== -->
              
               <c:forEach items="${totalpas}" varStatus="loopCount">
                <div class="row">
                  <div class="col-md-12 col-sm-12">
                    <div class="col-md-1 col-sm-1 col-md-offset-1">
                      <span class="adlt">Adult </span>
                      </div>
                        <div class="col-md-2 col-sm-2">
                          <select class="form-control" id="t_title${loopCount.count}" name="title" onchange="$('#errt_title${loopCount.count}').empty();">
                            <option  value="#">Title</option>
                            <option selected="selected" value="Mr.">Mr.</option>
                            <option value="Ms.">Ms.</option>
                            <option value="Mrs.">Mrs.</option>
                            <option value="Miss.">Miss.</option>
                          </select>
                           <p class="rederror" id="errt_title${loopCount.count}"></p>
                        </div>
                      <div class="col-md-3 col-sm-3">
                          <input type="text" class="form-control" id="t_fName${loopCount.count}" name="fName" placeholder="First Name" value="" onkeypress="$('#errt_fName${loopCount.count}').empty();">
<!--                           <a class="tooltips" href="#"><i class="fa fa-exclamation-circle errnme"></i><span class="ad1">This value is required.</span></a> -->
                       <p class="rederror" id="errt_fName${loopCount.count}"></p>
                      </div>
                      <div class="col-md-3 col-sm-3">
                          <input type="text" class="form-control" id="t_lName${loopCount.count}" name="lName" placeholder="Last Name" value="" onkeypress="$('#errt_lName${loopCount.count}').empty();">
<!--                           <a class="tooltips" href="#"><i class="fa fa-exclamation-circle errnme"></i><span class="ad1">This value is required.</span></a> -->
                  		 <p class="rederror" id="errt_lName${loopCount.count}"></p>
                      </div>
                      <div class="col-md-1 col-sm-1">
                          <input type="text" class="form-control numeric" id="t_age${loopCount.count}" name="age" placeholder="Age" value="" onkeypress="$('#errt_age${loopCount.count}').empty();">
<!--                           <a class="tooltips" href="#"><i class="fa fa-exclamation-circle errage"></i><span class="ag">This value is required.</span></a> -->
                    	 <p class="rederror" id="errt_age${loopCount.count}"></p>
                      </div>
                  </div>
                </div><br>
                </c:forEach>
                <!-- ============================= USER INFORMATION ENDS HERE ===================== -->
                
                <br>
                <div class="box">
                  <div class="row">
                    
                      <div class="col-md-4 col-sm-4 col-md-offset-2">
                        <div class="input-group">
<!--                           <span class="input-group-addon fa fa-phone"></span> -->
                          <input type="text" class="form-control" name="email" id="email" placeholder="Enter Email ID" onkeypress="$('#err_email').empty();">
<!--                           <a class="tooltips" href="#"><i class="fa fa-exclamation-circle errcon"></i><span class="cont">This value is required.</span></a> -->
                        <p class="rederror" id="err_email"></p>
                        </div>
                        <span class="msg">Your ticket will be sent to this email address</span>
                      </div>
                      <div class="col-md-4 col-sm-4">
                        <div class="input-group">
<!--                           <span class="input-group-addon fa fa-envelope-o"></span> -->
                          <input type="text" class="form-control numeric"  name="mobile" id="mobile" placeholder="Enter Mobile Number" onkeypress="$('#err_mobile').empty();">
<!--                           <a class="tooltips" href="#"><i class="fa fa-exclamation-circle errcon"></i><span class="cont">This value is required.</span></a> -->
                        <p class="rederror" id="err_mobile"></p>
                        </div>
<!--                         <span class="msg">Your Mobile number will be used only for sending flight related communication.</span> -->
                      </div>
                    
                  </div>
                </div>
                <br>
                <div class="row">
                  <div class="col-md-12 col-sm-12">
                    <div class="col-md-3 col-md-offset-5">
                      <button type="submit" id="payment" class="btn btn-primary" style="font-size: 20px; font-weight: 600;">Continue</button>
                    </div>
                  </div>
                </div>
                <br>
              </div>
            </div>
          </div>
        </div>
        </form>
    </div>
    
    
    <!-- Model for Bus gif -->
<div id="loading" class="modal fade" role="dialog" style="margin-top: 5%;">
		<div class="modal-dialog" style="background: ">

			<!-- Modal content-->
			<div class="modal-content" style="background: transparent; box-shadow: none; border: none;">
					<center><img src="${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/bus1.gif" class="img-responsive"></center>
			</div>

		</div>
	</div>
    
    <!-- <div class="container"> 
              <div class="row">
                <div class="middle-banner"> 
                    <img src="img/banner.png" class="img-responsive"> 
                </div>
              </div>
          </div>
          <br><br> -->
          
  <!-- <footer>
    <div class="container-fluid">
      <a href="AboutUs">About Us</a> <a href="#">Partner
        with us</a> <a href="#">Terms &amp; Conditions</a> <a
        href="#">Customer service</a> <a href="#">Grievance
        policy</a> <a href="#">Recharge Partners</a>
           <div  class="span pull-left" "> 
        
         </div>
         <div  class="span pull-right"> 
        <p>© Copyright MSewa Software Pvt. Ltd.</p>   
         </div>
    </div>
  </footer> -->

      <!-- <div class="trvlers">
            <div class="col-md-4">
                <center><label>
                  <span>Adult</span>
                  <small>(12+ yrs)</small>  
                </label></center>
                <div class="col-md-3 col-sm-3">
                  <button class="decr-btn btn" data-action="decrease"><i class="fa fa-minus" aria-hidden="true"></i></button>
                </div>
                <div class="col-md-6 col-sm-6">
                  <input class="quantity form-control" type="text" name="quantity" value="1"/>
                </div>
                <div class="col-md-3 col-sm-3">
                  <button class="incr-btn btn" data-action="increase"><i class="fa fa-plus" aria-hidden="true"></i></button>
                </div>
            </div>
            <div class="col-md-4">
                <center><label>
                  <span>Children</span>
                  <small>(2-12 yrs)</small>  
                </label></center>
                <div class="col-md-3 col-sm-3">
                  <button class="decr-btn btn" data-action="decrease"><i class="fa fa-minus" aria-hidden="true"></i></button>
                </div>
                <div class="col-md-6 col-sm-6">
                  <input class="quantity form-control" type="text" name="quantity" value="1"/>
                </div>
                <div class="col-md-3 col-sm-3">
                  <button class="incr-btn btn" data-action="increase"><i class="fa fa-plus" aria-hidden="true"></i></button>
                </div>
            </div>
            <div class="col-md-4">
                <center><label>
                  <span>Infants</span>
                  <small>(below 2 yrs)</small>  
                </label></center>
                <div class="col-md-3 col-sm-3">
                  <button class="decr-btn btn" data-action="decrease"><i class="fa fa-minus" aria-hidden="true"></i></button>
                </div>
                <div class="col-md-6 col-sm-6">
                  <input class="quantity form-control" type="text" name="quantity" value="1"/>
                </div>
                <div class="col-md-3 col-sm-3">
                  <button class="incr-btn btn" data-action="increase"><i class="fa fa-plus" aria-hidden="true"></i></button>
                </div>
            </div>
        </div> -->
    

     <!--    <script src="js/jquery.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/slimmenu.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
        <script src="js/bootstrap-timepicker.js"></script>
        <script src="js/nicescroll.js"></script>    
        <script src="js/typeahead.js"></script>
        <script src="js/custom.js"></script>
        
 -->
      <!-- Script for Increment/Decrement -->
      <!-- <script>
        $(".incr-btn").on("click", function (e) {
            var $button = $(this);
            var oldValue = $button.parent().find('.quantity').val();
            $button.parent().find('.incr-btn[data-action="decrease"]').removeClass('inactive');
            if ($button.data('action') == "increase") {
                var newVal = parseFloat(oldValue) + 1;
            } else {
                if (oldValue > 1) {
                    var newVal = parseFloat(oldValue) - 1;
                } else {
                    newVal = 1;
                    $button.addClass('inactive');
                }
            }
            $button.parent().find('.quantity').val(newVal);
            e.preventDefault();
        });
    </script> -->

</body>


<script type="text/javascript">
$(document).ready(function(){
	$('#mobile').keyup(function () { 
	    this.value = this.value.replace(/[^0-9\.]/g,'');
	});
	
	$('#t_age').keyup(function () { 
	    this.value = this.value.replace(/[^0-9\.]/g,'');
	});
});


function validateForm(){
	
	console.log("Valid form");
	var n=$('#noOfPas').val();
	
	console.log("Pass Size:: "+n);
	
	for (var i = 1; i <=n; i++) {
	
	var title=document.getElementById("t_title"+i).value;/* $('#t_title'+i).val(); */
	var fName=document.getElementById("t_fName"+i).value;/* $('#t_fName'+i).val(); */
	var lName=document.getElementById("t_lName"+i).value;/* $('#t_lName'+i).val(); */
	var age=document.getElementById("t_age"+i).value;/* $('#t_age'+i).val(); */
	
	var contactNo=$('#mobile').val();
	var email=$('#email').val();
	var pattern = "[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$"
    var mobilePattern="^(?=(?:[7-9]){1})(?=[0-9]{10}).*";
	var valid=true;
	
	/*  console.log(title);
	console.log(fName);
	console.log(lName); 
	console.log(age); */
	
	if (title=='#') {
		$('#errt_title'+i).text("Please select title");
		valid=false;
	}
	if (fName=='') {
		valid=false;
		$('#errt_fName'+i).text("Please enter first name");
	}
	if (lName=='') {
		valid=false;
		$('#errt_lName'+i).text("Please enter last name");
	}
	if (age=='') {
		valid=false;
		$('#errt_age'+i).text("Please enter age");
	}
	else if (age >100) {
		valid=false;
		$('#errt_age'+i).text("Please enter valid age");
	}
	else if (age <1) {
		valid=false;
		$('#errt_age'+i).text("Please enter valid age");
	}
	if(contactNo=='') {
		$("#err_mobile").html("Please enter mobile number");
		valid = false;
	}
	else if(contactNo.length != 10) {
		$("#err_mobile").html("Please enter 10 digit mobile number");
		valid = false;
	}
	else if(!contactNo.match(mobilePattern)){
		$("#err_mobile").html("Enter valid mobile number");
		valid = false;
	}
	if (email=='') {
		valid=false;
		$('#err_email').text("Please enter email");
	}
	else if(!(email.match(pattern))){
		$("#err_email").html("Please enter valid email");
		valid = false;
	}
	}
	
	 if (valid==true) {
	var contextPath = "${pageContext.request.contextPath}";
// 	var spinnerUrl = "Please wait <img src="+contextPath+"'/resources/images/spinner.gif' style='width:25px;'>";
	$("#payment").addClass("disabled");
	
// 	$("#payment").html(spinnerUrl);

	$('#loading').modal({
        backdrop: 'static',
       keyboard: true,  
       show: true
	}); 
	
	}
	return valid;
 
}

</script>
<script type="text/javascript">

    function noBack() { window.history.forward(); }
    function noForward() { window.history.back(); }
</script>

</html>