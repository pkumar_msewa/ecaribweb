<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Bus CheckOut</title>
  <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
	<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
	<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
	<%@ taglib prefix="sec"
		uri="http://www.springframework.org/security/tags"%>
	<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

    <!--- GOOGLE FONTS - -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600' rel='stylesheet' type='text/css'>
    <!-- /GOOGLE FONTS -->

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'
	type="image/png" />

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/icomoon.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/styles.css">
    <script type="text/javascript" src="<c:url value="/resources/js/userdetails.js"/>"></script>
    <script src="${pageContext.request.contextPath}/resources/js/Travel/travel.js"></script>
    <!-- <link rel="stylesheet" href="css/mystyles.css">   -->
    
    <link rel="stylesheet" href="/resources/css/new_css/default.css">

 <style>
      fieldset.scheduler-border {
        border: 1px groove #ddd !important;
        padding: 0 1.4em 1.4em 1.4em !important;
        margin: 0 0 1.5em 0 !important;
        -webkit-box-shadow:  0px 0px 0px 0px #000;
        box-shadow:  0px 0px 0px 0px #000;
    }

    legend.scheduler-border {
        font-size: 1.2em !important;
        font-weight: bold !important;
        text-align: left !important;

    }
    legend.scheduler-border {
        width:auto; /* Or auto */
        padding:0 10px; /* To give a bit of padding on the left and right */
        border-bottom:none;
    }
    .dt {
          font-size: 35px;
          font-weight: 900;
          position: absolute;
          margin-top: -75px;
    	  margin-left: 26px;
    }
    .mnth {
          position: absolute;
          margin-top: -47px;
    	margin-left: 12px;
    }
    .yr {
          position: absolute;
          margin-top: -46px;
    	  margin-left: 40px;
    }
    .rupee {
          font-size: 17px;
          margin-top: 42px;
    }
    .prc {
        font-size: 19px;
    }
    .ttl {
      font-size: 18px;
    } 
    .src2des {
          font-size: 16px;
        font-weight: 700;
    }
    </style>

</head>

<body style="background: #efefef;" >
<jsp:include page="/WEB-INF/jsp/User/Header.jsp" />
<div class="topline"></div>
    <nav class="navbar" style="background:rgba(255,255,255,1.00); min-height: 60px; margin-bottom: 0px;">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="#"></a>
        </div>    
        <ul class="nav navbar-nav navbar-right">
            
        </ul>
      </div>
    </nav>

    <div class="blank" style="height: 50px;"></div>
    
    <div class="container">
      <div class="row">
        <div class="panel panel-default">
          <div class="panel-body">
            <h4>Booking Confirmation</h4><hr>
            <div class="row">
              <div class="col-md-12">
                <div class="col-md-8" style="border-right: 1px dotted;">
                  <h5>Traveler Details</h5>
                  <div class="">
                    <span>Number of Passengers: </span>&nbsp;<span>${noOfPas}</span>
                  </div>
                  <br>
                  <div class="tabble">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                        <th>Sl. No</th>
                          <th>Firstname</th>
                          <th>Lastname</th>
                          <th>Age</th>
                          <th>Gender</th>
                        </tr>
                      </thead>
                      <tbody>
                      <c:forEach items="${travellerList}" var="pas" varStatus="loopCounter">
                        <tr >
                          <td>${loopCounter.count}</td>
                          <td>${pas.fName}</td>
                          <td>${pas.lName}</td>
                          <td>${pas.age}</td>
                          <td>${pas.gender}</td>
                        </tr>
                        </c:forEach>
                      </tbody>
                    </table>
                  </div>
                </div>
                <div class="col-md-4">
                  <fieldset class="scheduler-border">
                    <legend class="scheduler-border">Trip Details:</legend>
                    <div class="control-group">
                      <div class="src2des">
                        <span>${source}</span>&nbsp; To &nbsp;<span>${destination}</span>
                      </div>
                      <br>
                     
                      <div class="col-md-6">
                        <img src="${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/clndr.png">
                        <span class="dt">${dd}</span>
                        <span class="mnth">${mm}</span>
                        <span class="yr">${yy}</span>
                      </div>
                      <div class="col-md-6">
                        <span class="ttl">Total:</span>&nbsp;<i class="fa fa-inr rupee"></i>&nbsp;<span class="prc">${fare}</span>
                      </div>
                    </div>
                  </fieldset>
                </div>
                <div class="row">
                	<div class=" col-md-12">
                		<div class="col-md-6">
                			
                		</div>
		                <div class="col-md-6">
		                	<div class="col-md-3 col-md-offset-4">
		                		<form action="${pageContext.request.contextPath}/User/Travel/Bus/cancelInitPayment" method="get">
					                  <button class="btn btn-danger" id="cancel_payment">Cancel</button>
					            </form>
		                	</div>
		                	<div class="col-md-4">
								<button class="btn btn-success" id="ticket_payment">Payment</button>		                		
		                	</div>
		                </div>
		            </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  <!-- <footer>
    <div class="container-fluid">
      <a href="AboutUs">About Us</a> <a href="#">Partner
        with us</a> <a href="#">Terms &amp; Conditions</a> <a
        href="#">Customer service</a> <a href="#">Grievance
        policy</a> <a href="#">Recharge Partners</a>
           <div  class="span pull-left" "> 
        
         </div>
         <div  class="span pull-right"> 
        <p>© Copyright MSewa Software Pvt. Ltd.</p>   
         </div>
    </div>
  </footer> -->
  
  
    <div class="modal fade" id="myModal" role="dialog" hidden="hidden">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content" style="margin-top: 40%;">
       
        <div class="modal-body">
          <center>
		  <img src="${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/source-(tick).gif" style="width: 150px; margin-top: -91px;">
		<div style="background: white; margin-top: -20px;
		    padding: 2.5% 2%;">
		<h3><b>Your Payment is successful.<br><small></b></small></h3>
		<center><button type="button" id="payment_ok">OK</button></center>
		</div></center>
        </div>
       
      </div>
      
    </div>
  </div>
  

  <!-- Modal -->
  <div class="modal fade" id="ErrmyModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content" style="margin-top: 40%;">
       
        <div class="modal-body">
          <center>
<%-- 		  <img src="${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/search.gif" style="width: 150px; margin-top: -91px;"> --%>
        <div style="background: white; margin-top: -20px; padding: 2.5% 2%; padding-top: 32px;">
          <h4 id="payErrMsgVal"><b><br></b></h4>
          <center><button type="button" id="payment_fail">OK</button></center>
        </div></center>
        </div>
       
      </div>
      
    </div>
  </div>

<!-- Model for Bus gif -->
<div id="loading" class="modal fade" role="dialog" style="margin-top: 5%;">
		<div class="modal-dialog" style="background: ">

			<!-- Modal content-->
			<div class="modal-content" style="background: transparent; box-shadow: none; border: none;">
					<center><img src="${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/bus1.gif" class="img-responsive"></center>
			</div>

		</div>
	</div>


<script type="text/javascript">
var contextPath = "${pageContext.request.contextPath}";
var spinnerUrl = "Please wait <img src="+contextPath+"'/resources/images/spinner.gif' style='width:25px;'>";
$("#ticket_payment").click(function() {
	
	$("#ticket_payment").addClass("disabled");
	$("#cancel_payment").addClass("disabled");
// 	$("#ticket_payment").html(spinnerUrl);

		$('#loading').modal({
		        backdrop: 'static',
		       keyboard: true,  
		       show: true
		}); 

	$.ajax({
		type : "POST",
//			headers: headers,
		contentType : "application/json",
		url :contextPath+"/User/Travel/Bus/paymentForBookTicket",
		dataType : 'json',
		data : JSON.stringify({
			
		}),
		success : function(response) {
			console.log("response is "+response.code);
			$("#ticket_payment").removeClass("disabled");
			$("#cancel_payment").removeClass("disabled");
			$('#loading').modal("hide");
// 			$("#ticket_payment").html("Payment");
			if (response.code.includes("S00")) {
				
// 				$("#myModal").modal("show");
				
				$('#myModal').modal({
			        backdrop: 'static',
			        keyboard: true, 
			        show: true
			}); 
				/* $('#myModal').on('hidden.bs.modal', function () {
					window.location.href='${pageContext.request.contextPath}/User/Travel/Bus/MyTickets'; 
					}); */
			}
			else{
// 				$("#ErrmyModal").modal("show");
				
				$('#ErrmyModal').modal({
			        backdrop: 'static',
			        keyboard: true, 
			        show: true
			});
    			$("#payErrMsgVal").html(response.message);
				 
				/* $('#ErrmyModal').on('hidden.bs.modal', function () {
					window.location.href='${pageContext.request.contextPath}/User/Travel/Bus/cancelInitPayment'; 
			}); */
				
			}
			
		}
	}); 
});

$("#cancel_payment").click(function() {
	$("#cancel_payment").addClass("disabled");
// 	$("#cancel_payment").html(spinnerUrl);

	$('#loading').modal({
        backdrop: 'static',
       keyboard: true,  
       show: true
	});
	
});
$("#payment_ok").click(function() {
	window.location.href='${pageContext.request.contextPath}/User/Travel/Bus/MyTickets'; 
});

$("#payment_fail").click(function() {
	window.location.href='${pageContext.request.contextPath}/User/Travel/Flight/Source';  
});

</script>

<script type="text/javascript">

window.onbeforeunload = function(e) {
   alert("Not able to close it");
 };
</script> 

<script>
    $(document).ready(function() {
        function disableBack() { window.history.forward() }

        window.onload = disableBack();
        window.onpageshow = function(evt) { if (evt.persisted) disableBack() }
    });
</script>
</body>
</html>