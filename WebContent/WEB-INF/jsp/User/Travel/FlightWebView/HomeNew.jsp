<!DOCTYPE HTML>
<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<html>
<head>
<title>VPayQwik | Travel</title>

<meta content="text/html;charset=utf-8" http-equiv="Content-Type">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" href='<c:url value="/resources/images/favicon.png"/>' type="image/png" />
<link rel="stylesheet" href="/resources/css/font-awesome.min.css">
<link rel="stylesheet" href="/resources/css/new_css/default.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap-chosen.css">

<!--- GOOGLE FONTS - -->
<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600'rel='stylesheet' type='text/css'>
<!-- /GOOGLE FONTS -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/travel_css/bootstrap.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/travel_css/icomoon.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css" />
<script src="${pageContext.request.contextPath}/resources/js/new_js/select2.min.js"></script>
<script type="text/javascript" src="<c:url value="/resources/js/userdetails.js"/>"></script>
<link rel="stylesheet" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/new_css/select2.1.css">

<script type="text/javascript"> var contextPath = "${pageContext.request.contextPath}";</script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/Travel/flight/travelhome.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/travel_css/flight/travelhome.css">
</head>

<jsp:include page="/WEB-INF/jsp/User/Header.jsp" />

<!-- ====================== TOP AREA ================== -->

<body>
	<input type="hidden" value="${sessionId}" id="session">
	<input type="hidden" value="${airlines}" id="airlines">

	<!-- Model for flight gif -->
	<div id="loading_flight" class="modal fade" role="dialog"
		style="margin-top: 7%;">
		<div class="modal-dialog" style="background:">

			<!-- Modal content-->
			<div class="modal-content" style="background: transparent; box-shadow: none; border: none;">
			<img src="${pageContext.request.contextPath}/resources/images/User/travel/img/Flight/flight.gif" class="img-responsive">
			</div>

		</div>
	</div>
	<!-- Model for flight gif -->
	<!-- ====================== TOP AREA ================== -->
	<div class="top-area show-onload" style="margin-top: -21px;">
		<div class="bg-holder full">
			<div class="bg-mask"></div>
			<div class="bg-parallax"
				style="background-image: url(/resources/images/Travel/bg3.gif);"></div>
			<div class="bg-content" style="margin-top: 6%;">
				<div class="container">
					<div class="bking-box" style="margin-top: 10%;">
						<div class="row">
							<div class="col-md-12">
								<div class="tggle-cont">
									<div class="tab-content"
										style="background: rgba(0, 0, 0, 0.6) !important; background: rgba(0, 0, 0, 0.4); border-radius: 3px; padding: 15px; min-height: 155px; -webkit-transition: all .2s ease; -moz-transition: all .2s ease; -ms-transition: all .2s ease; -o-transition: all .2s ease; transition: all .2s ease;">
										<!-- ============================ Flight Tab ============================= -->
										<div class="top-toggle">
											<ul class="nav nav-tabs ">
												<li class="active"><a href="#tab_flight"
													data-toggle="tab"> Flights</a></li>
												<li><a href="#tab_bus" data-toggle="tab"
													id="bus_travel"> Buses</a></li>
											</ul>
										</div>

										<jsp:include page="/WEB-INF/jsp/User/Travel/Flight/flightSearch.jsp" />

										<!-- ============================ Flight Tab End ============================= -->
										<!-- ============================ Bus Tab ============================= -->

										<jsp:include page="/WEB-INF/jsp/User/Travel/Bus/Home.jsp" />
										<!-- ============================ Bus Tab End ============================= -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END TOP AREA  -->

	<!-- TRAVELLERS DETAILS STARTS HERE  -->
	<div class="dropdown1" id="trav_menu" style="display: none;">
		<div class="t_menu">
			<div class="row">
				<div class="col-md-7">
					<input class="quantity" type="text" id="adult" name="quantity"
						value="1" hidden="hidden" /> <span class="quantity" id="adult1">1</span>&nbsp;<span>Adult(s)</span>
				</div>
				<div class="col-md-5">
					<button class="incr-btn btn btn-sm"
						style="background: #0d30e8; color: #fff;"
						onclick="decreaseValue('adult','adult1');">
						<i class="fa fa-minus" aria-hidden="true"></i>
					</button>
					<button class="incr-btn btn btn-sm"
						style="background: #0d30e8; color: #fff;"
						onclick="increaseValue('adult','adult1');">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</button>
				</div>
			</div>
			<hr class="t_divider">
			<div class="row">
				<div class="col-md-7">
					<input class="quantity" type="text" id="childs" name="quantity"
						value="0" hidden="hidden" /> <span class="quantity" id="childs1">0</span>&nbsp;<span>Child</span>&nbsp;<small>(2-12
						YRS)</small>
				</div>
				<div class="col-md-5">
					<button class="incr-btn btn btn-sm"
						style="background: #0d30e8; color: #fff;"
						onclick="decreaseValue('childs','childs1');">
						<i class="fa fa-minus" aria-hidden="true"></i>
					</button>
					<button class="incr-btn btn btn-sm"
						style="background: #0d30e8; color: #fff;"
						onclick="increaseValue('childs','childs1');">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</button>
				</div>
			</div>
			<hr class="t_divider">
			<div class="row">
				<div class="col-md-7">
					<input class="quantity" type="text" name="quantity" value="0"
						id="infants" hidden="hidden" /> <span class="quantity"
						id="infants1">0</span>&nbsp;<span>Infant</span>&nbsp;<small>(Below
						2 YRS)</small>
				</div>
				<div class="col-md-5">
					<button class="incr-btn btn btn-sm"
						style="background: #0d30e8; color: #fff;"
						onclick="decreaseValue('infants','infants1');">
						<i class="fa fa-minus" aria-hidden="true"></i>
					</button>
					<button class="incr-btn btn btn-sm"
						style="background: #0d30e8; color: #fff;"
						onclick="increaseValue('infants','infants1');">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</button>
				</div>
			</div>
			<hr class="t_divider">
			<div class="row">
				<div class="col-md-12">
					<label>Class</label> <select name="flight_pax" id="cabin"
						tabindex=""  class="form-control">
						<option selected="selected" value="Economy">Economy</option>
						<!-- <option value="ER">Premium Economy</option> -->
						<option value="Business">Business</option>
					</select>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-12">
					<button class="btn btn-sm btn-danger pull-right" id="closedivtrevalinformation">Done</button>
				</div>
			</div>
		</div>
	</div>
	<!-- TRAVELLERS DETAILS END HERE  -->
	
	
	<script type="text/javascript">
	
	function increaseValue(id,idset)
	{
		var value = parseInt(document.getElementById(""+id).value, 10);
		if(value<=4)
		{
			var d = parseInt(document.getElementById("totalCounttrevaler").innerHTML);
			var c=parseInt(d);
			c++;
			document.getElementById("totalCounttrevaler").innerHTML=""+c;
			value = isNaN(value) ? 0 : value;
			value++;
			document.getElementById(""+idset).innerHTML = ""+value;
			document.getElementById(""+id).value = ""+value;
		}
	}

	function decreaseValue(id,idset) 
	{
		var value = parseInt(document.getElementById(""+id).value, 10);
		if(value>0)
		{
			var d = parseInt(document.getElementById("totalCounttrevaler").innerHTML);
			var c=parseInt(d);
			c--;
			document.getElementById("totalCounttrevaler").innerHTML=""+c;

		}
		value = isNaN(value) ? 0 : value;
		value < 1 ? value = 1 : '';
		value--;

		document.getElementById(""+idset).innerHTML = ""+value;
		document.getElementById(""+id).value = ""+value;
	}
	</script>
	
<script src="https://harvesthq.github.io/chosen/chosen.jquery.js"></script>
</body>
</html>