<!DOCTYPE HTML>
<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<html>
<head>
<title>VPayQwik | One Way</title>

<meta content="text/html;charset=utf-8" http-equiv="Content-Type">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/bootstrap-chosen.css">
<link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'
	type="image/png" />
<link rel="stylesheet" href="/resources/css/new_css/cloud-admin.css">
<link rel="stylesheet" href="/resources/css/font-awesome.min.css">
<link rel="stylesheet" href="/resources/css/new_css/default.css">
<link rel="stylesheet" href="/resources/css/new_css/responsive.css">
<link rel="stylesheet"
	href="/resources/css/new_css/uniform.default.min.css">
<!--- GOOGLE FONTS - -->
<link
	href='https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700'
	rel='stylesheet' type='text/css'>
<link
	href='https://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600'
	rel='stylesheet' type='text/css'>
<!-- /GOOGLE FONTS -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/travel_css/bootstrap.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<!--<link rel="stylesheet" href="css/font-awesome.css">-->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/travel_css/icomoon.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/travel_css/styles.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/travel_css/mystyles.css">
<%-- 
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/new_css/jquery.ui.all.css"> --%>
<!-- <link rel="stylesheet" href="css/mystyles.css">   -->
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/9.8.1/bootstrap-slider.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/9.8.1/css/bootstrap-slider.min.css">

<link rel="stylesheet" href="/resources/css/new_css/jquery.ui.theme.css"
	type="text/css">
<link rel="stylesheet"
	href="/resources/css/new_css/jquery.ui.slider.css" type="text/css">
<script type="text/javascript"
	src="/resources/js/new_js/jquery-1.7.1.js"></script>
<script type="text/javascript"
	src="/resources/js/new_js/jquery.ui.widget.js"></script>
<script type="text/javascript"
	src="/resources/js/new_js/jquery.ui.mouse.js"></script>
<script type="text/javascript"
	src="/resources/js/new_js/jquery.ui.slider.js"></script>

<script type="text/javascript"
	src="<c:url value="/resources/js/userdetails.js"/>"></script>
<script src="/resources/js/new_js/select2.min.js"></script>

<link rel="stylesheet" href="/resources/css/new_css/newstyles.css">

<link rel="stylesheet" rel="stylesheet"
	href="/resources/css/new_css/select2.1.css">
</head>


<script>

$(document).ready(function(){
	var minAmt=$('#minAmt').val();     
	var maxAmt=$('#maxAmt').val();    
	
	console.log("minAmt:: "+minAmt);
	console.log("maxAmt:: "+maxAmt);
	
$(function () {
      $('#slider-container').slider({
          range: true,
          min: parseInt(minAmt),
          max: parseInt(maxAmt),
          values: [parseInt(minAmt), parseInt(maxAmt)],
          create: function() {
              $("#amount").val("Rs "+ parseInt(minAmt) + " - RS " + parseInt(maxAmt));
          },
          slide: function (event, ui) {
              $("#amount").val("Rs" + ui.values[0] + " - Rs" + ui.values[1]);
              var mi = ui.values[0];
              var mx = ui.values[1];
              filterSystem(mi, mx);
          }
      })
});

  function filterSystem(minPrice, maxPrice) {
      $("#computers div.system").hide().filter(function () {
          var price = parseInt($(this).data("price"), 10);
          return price >= minPrice && price <= maxPrice;
      }).show();
  }
  $( "#date" ).focus(function() {
	  $( this ).blur();
	});
  $( "#returndate" ).focus(function() {
	  $( this ).blur();
	});

});


</script>
<style>
#slider-container {
	width: 100%;
}
</style>
<%-- <link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/demos/style.css"> --%>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css" />

<script>
  $(document).ready(function(){
var contextPath = "${pageContext.request.contextPath}";
var session=$("#session").val();
    $(".js-example-basic-multiple").select2();
//     $("#loading_flight").modal({backdrop: false});
    $("#flght_srch").addClass("disabled");
 	$('#flightsrc').empty();
 	$('#flightdest').empty(); 
 	
 	/*  $('#flightsrc').attr("value","Getting Origin");
 	$('#flightdest').attr("value","Getting Destination");   */
 	
 	$('#flightsrc').append('<option value="#" selected="selected">Getting Origin.....</option>');
 	$('#flightdest').append('<option value="#" selected="selected">Getting Destination.....</option>');
    var org=$('#origin').val();
    var dst=$('#destination').val();
	
					$.ajax({
						type : "POST",
						contentType : "application/json",
						url :contextPath+"/User/Travel/Flight/AirLineNames",
						dataType : 'json',
						data : JSON.stringify({
							"sessionId": session,
					   }),
            			success : function(response) {
            				
            				for(var i=0;i<response.details.length;i++){
            					
            					if (response.details[i].cityCode.includes(org)) {
            					$('#flightsrc').append('<option value="' + response.details[i].cityCode + '" selected="selected">' + response.details[i].cityName + ' ('+response.details[i].cityCode+')'+ '</option>');
            					}
            					else{
            						$('#flightsrc').append('<option value="' + response.details[i].cityCode + '" >' + response.details[i].cityName + ' ('+response.details[i].cityCode+')'+ '</option>');
            					}
            					if (response.details[i].cityCode.includes(dst)) {
            					$('#flightdest').append('<option value="' + response.details[i].cityCode + '" selected="selected">' + response.details[i].cityName + ' ('+response.details[i].cityCode+')'+ '</option>');	
            					}else{
            						$('#flightdest').append('<option value="' + response.details[i].cityCode + '">' + response.details[i].cityName + ' ('+response.details[i].cityCode+')'+ '</option>');	
            					}
            				}
//             				 $("#loading_flight").modal("hide");
            				 
            				 $("#flght_srch").removeClass("disabled");
            				 
            					var today = new Date();
            					var dd = today.getDate();
            					var mm = today.getMonth()+1; //January is 0!

            					var yyyy = today.getFullYear();
            					if(dd<10){
            					    dd='0'+dd;
            					} 
            					if(mm<10){
            					    mm='0'+mm;
            					} 
            					var today = dd+'/'+mm+'/'+yyyy;
//             					document.getElementById("date").value = today; 
            					var end=$("#endDate").val();
            					var bDate=$("#beginDate").val();
            					var nbDate=bDate.split("-");
            					var nbeDate=end.split("-");
            					nwDate=nbDate[2]+"/"+nbDate[1]+"/"+nbDate[0];
            					if (end.includes('')) {
            					nweDate=nbeDate[2]+"/"+nbeDate[1]+"/"+nbeDate[0];
            					$("#returndate").val(nweDate);
            					}
            					$("#returndate").val('');
            					console.log("DAte:: "+nwDate);
//             					var nbDate=bDate.replace("-", "/");
            					$("#date").val(nwDate);
		            			}
			            		});
			 				 });
  
  
            		var today = new Date();
            		  
            	  	$(function() {
            			$("#date").datepicker({
            				startDate: today,
            				format : "dd/mm/yyyy"
            			}).on('change', function() {
            				$('.datepicker').hide();
            			});
            		});
            		
            	  	$(function() {
            			$("#returndate").datepicker({
            				startDate: today,
            				format : "dd/mm/yyyy"
            			}).on('change', function() {
            				$('.datepicker').hide();
            			});
            		});
            	  	
            	  	$(function() {
            			$("#datecal").datepicker({
            				startDate: today,
            				format : "dd/mm/yyyy"
            			}).on('change', function() {
            				$('.datepicker').hide();
            			});
            		});
  
            		
            		function srcdest() {
            			console.log("Compare");
            		$('#errormsgdest').empty();
            		var sr=$('#flightsrc').val();
            		var ds=$('#flightdest').val();
            			
            			if (sr.includes(ds)) {
            			$('#errormsgdest').html("Please select source & destation different.");
//            	 		alert("src dest mis match");
//            	 		document.getElementById("flightdestination").selectedIndex = 0;
            			}
            		}
            		
 </script>


<style>
.rederror {
	font-size: 14px;
	color: red;
}

.nav-tabs>li.active>a, .nav-tabs>li.active>a:hover, .nav-tabs>li.active>a:focus
	{
	background-color: #17bcc8;
}

.nav-tabs>li>a:hover {
	background-color: #17bcc8;
}

#errormsgadult {
	position: absolute;
	margin-top: 30px;
	margin-left: -136px;
}
</style>

<style>
.select2-container {
	box-sizing: border-box;
	display: inline-block;
	margin: 0;
	position: relative;
	vertical-align: middle;
	width: 50% !important;
	float: right;
	background: #fff;
}

.inner_tab_content_Hotel>.select2-container {
	box-sizing: border-box;
	display: inline-block;
	margin: 0;
	position: relative;
	vertical-align: middle;
	width: 100% !important;
	float: right;
	background: #fff;
}

.inner_tab_content_Holiday>.select2-container {
	box-sizing: border-box;
	display: inline-block;
	margin: 0;
	position: relative;
	vertical-align: middle;
	width: 100% !important;
	float: right;
	background: #fff;
}

.dropdown1 {
	background: #fff;
	border: 1px solid #ccc;
	border-radius: 4px;
	width: 280px;
	padding: 5px;
	left: 62%;
	/*     top: 413px; */
	position: absolute;
	z-index: 99;
}

.dropdown1 .t_menu {
	border-radius: 4px;
	box-shadow: none;
	margin-top: 10px;
	wi /dth: 300px;
}

.dropdown1 .t_menu:before {
	content: "";
	border-bottom: 10px solid #fff;
	border-right: 10px solid transparent;
	border-left: 10px solid transparent;
	position: absolute;
	top: -10px;
	right: 16px;
	z-index: 10;
}

.dropdown1 .t_menu:after {
	content: "";
	border-bottom: 12px solid #ccc;
	border-right: 12px solid transparent;
	border-left: 12px solid transparent;
	position: absolute;
	top: -12px;
	right: 14px;
	z-index: 9;
}

.t_divider {
	margin-top: 15px;
	margin-bottom: 15px;
	border: 0;
	border-top: 1px solid #eee;
}

.incr-btn {
	margin-left: 0px;
}

#errormsgdest {
	margin-left: 263px;
}

#errormsgdate {
	position: absolute;
	margin-top: 38px;
	margin-left: -200px;
}
</style>

<script>
    $("#swapVal").on('click', function() {
    	
    	console.log("Change City");
    	
      var fromcurrency =  $('#flight_src_list').val();
      var tocurrency = $('#flight_des_list').val();
      $('#flight_src_list').val(tocurrency).trigger('change');
      $('#flight_des_list').val(fromcurrency).trigger('change');
});
</script>

<script>
            $( "#flight_src_list" ).select2({
                theme: "bootstrap"
            });
            $( "#flight_des_list" ).select2({
                theme: "bootstrap"
            });
    
        </script>



<body style="background: #efefef;">

	<input type="hidden" value="${sourcecode}" id="traveldataval">
	<input type="hidden" value="${flightnumberdata}" id="filterdata1">
	<input type="hidden" value="${origin}" id="origin">
	<input type="hidden" value="${destination}" id="destination">
	<input type="hidden" value="${bDate}" id="beginDate">
	<input type="hidden" value="${isInternational}" id="isInternational">
	<input type="hidden" value="${minAmt}" id="minAmt">
	<input type="hidden" value="${maxAmt}" id="maxAmt">
	<input type="hidden" value="${eDate}" id="endDate"> 
	<input type="hidden" value="${flName}" id="flName"> 

	<input type="hidden" value="${flightdata}" id="flightdata">
	<!-- Include Date Range Picker -->

	<jsp:include page="/WEB-INF/jsp/User/Header.jsp" />

	<!-- ====================== TOP AREA ================== -->
	<!--<div class="top-area show-onload">-->
	<div class="bg-holder full" style="height: auto;">

		<div class="bg-content">
			<div class="container">
				<div class="bking-box">
					<div class="row">
						<div class="col-md-12">
							<div class="tggle-cont">
								<div class="tab-content"
									style="background: rgba(0, 0, 0, 0.6) !important; background: rgba(0, 0, 0, 0.4); border-radius: 3px; padding: 15px; min-height: 74px; -webkit-transition: all .2s ease; -moz-transition: all .2s ease; -ms-transition: all .2s ease; -o-transition: all .2s ease; transition: all .2s ease;">
									<!-- ============================ Flight Tab ============================= -->
									<div class="tab-pane active" id="tab_flight">
										<div class="col-md-4"
											style="padding-left: 0px; padding-right: 0px;">
											<div class="inner_tab_content_Flight">
												<select id="flightdest" class="js-example-basic-multiple"
													style="width: 350px;" name="sourceId">

												</select> <a href="javascript:void(0);" class="beSwapCity"
													data-swap="true"> <i
													class="sprite-booking-engine ico-be-leftRightArow"
													title="Swap Origin City and Destination City" id="swapVal">&nbsp;</i>
												</a> <select id="flightsrc" class="js-example-basic-multiple"
													style="width: 350px;" name="sourceId"
													onchange="srcdest();">

												</select> <span class="rederror" id="errormsgsrc"></span> <span
													class="rederror" id="errormsgdest" onchange="srcdest();"></span>
											</div>
										</div>

										<div class="col-md-4"
											style="padding-left: 0px; padding-right: 0px;">
											<input type="text" class="form-control"
												placeholder="Depart Date" name="date"
												style="width: 50%; float: left; height: 40px;" id="date"
												value=""> <span class="rederror" id="errormsgdate"></span>
											<i
												class="fa fa-calendar D-font-icon icon-inside icon-calendar"></i>
											<input type="text" class="form-control"
												placeholder="Return Date" name="returndate"
												style="width: 50%; height: 40px; cursor: default;"
												id="returndate" value="${endDate}"> <i
												class="fa fa-calendar R-font-icon icon-inside icon-calendar"></i>
												<span class="rederror" id="errDeptdate"></span>
										</div>
										<div class="col-md-4"
											style="padding-left: 0px; padding-right: 0px; height: 40px;">
											<div class="pax-details" id="selTrav">
												<span class="txt-ellipses"> <span class="totalCount"
													id="totalCounttrevaler">${totaltraveller}</span> Traveller(s)<span
													class="flight_cls" id="flight_cls">, Economy</span> <span
													class="rederror" id="errormsgadult"></span>
												</span> <i
													class="sprite-booking-engine ico-be-arrow-down-grey-v2 icon-inside"
													id=""> </i>
											</div>
											<!-- <i class="sprite-booking-engine ico-be-arrow-down-grey-v2 icon-inside" id=""> </i> -->
											<button class="btn btn-block search-btn"
												style="width: 50%; height: 40px;" id="loader"
												onclick="getvalue()">Modify Search</button>
											<div style="display: none;" id="loader">
												<span class="rederror" id="responsemessage"> </span>
											</div>
											<span class="rederror" id="responsemessage"> </span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- ===================== end main row =============== -->
				</div>
				<!-- =============end container============== -->
			</div>
			<!-- =================end bg-content================= -->
		</div>
		<!-- ==============end bg-holder full================= -->
	</div>
	<!-- END TOP AREA  -->

	<div class="dropdown1" id="trav_menu" style="display: none;">
		<div class="t_menu">
			<div class="row">
				<div class="col-md-7">
					<input class="quantity" type="text" id="adult" name="quantity"
						value="${adults}" hidden="hidden" /> <span class="quantity" id="adult1">${adults}</span>&nbsp;<span>Adult(s)</span>
				</div>
				<div class="col-md-5">
					<button class="incr-btn btn btn-sm"
						style="background: #0d30e8; color: #fff;"
						onclick="decreaseValue('adult','adult1');">
						<i class="fa fa-minus" aria-hidden="true"></i>
					</button>
					<button class="incr-btn btn btn-sm"
						style="background: #0d30e8; color: #fff;"
						onclick="increaseValue('adult','adult1');">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</button>
				</div>
			</div>
			<hr class="t_divider">
			<div class="row">
				<div class="col-md-7">
					<input class="quantity" type="text" id="childs" name="quantity"
						value="${childs}" hidden="hidden" /> <span class="quantity" id="childs1">${childs}</span>&nbsp;<span>Child</span>&nbsp;<small>(2-12
						YRS)</small>
				</div>
				<div class="col-md-5">
					<button class="incr-btn btn btn-sm"
						style="background: #0d30e8; color: #fff;"
						onclick="decreaseValue('childs','childs1');">
						<i class="fa fa-minus" aria-hidden="true"></i>
					</button>
					<button class="incr-btn btn btn-sm"
						style="background: #0d30e8; color: #fff;"
						onclick="increaseValue('childs','childs1');">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</button>
				</div>
			</div>
			<hr class="t_divider">
			<div class="row">
				<div class="col-md-7">
					<input class="quantity" type="text" name="quantity" value="${infants}"
						id="infants" hidden="hidden" /> <span class="quantity"
						id="infants1">${infants}</span>&nbsp;<span>Infant</span>&nbsp;<small>(Below
						2 YRS)</small>
				</div>
				<div class="col-md-5">
					<button class="incr-btn btn btn-sm"
						style="background: #0d30e8; color: #fff;"
						onclick="decreaseValue('infants','infants1');">
						<i class="fa fa-minus" aria-hidden="true"></i>
					</button>
					<button class="incr-btn btn btn-sm"
						style="background: #0d30e8; color: #fff;"
						onclick="increaseValue('infants','infants1');">
						<i class="fa fa-plus" aria-hidden="true"></i>
					</button>
				</div>
			</div>
			<hr class="t_divider">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-4">
						<label>Class</label>
					</div>
					<div class="col-md-8">
						<select name="flight_pax" id="flightcabin" tabindex=""
							onchange="changesclass()" class="form-control">
							<option selected="selected" value="Economy">Economy</option>
							<!-- <option value="Premium Economy">Premium Economy</option> -->
							<option value="Business">Business</option>
						</select>
					</div>

				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<button class="btn btn-sm btn-danger pull-right"
						onclick="closedivtrevalinformation();">Done</button>
				</div>
			</div>
		</div>
	</div>
	<!-- TRAVELLERS DETAILS END HERE  -->

	<div class="container-fluid one-way-wrap">
		<div class="col-md-3">
			<div class="panel panel-default">
				<div class="panel-heading">
					<span style="font-size: 20px; font-weight: 800;">Filter By:</span>
					<a style="padding-left: 65px;" href="#" id="clr_filtr">Clear all filter</a>
				</div>
				<div class="panel-body">
					<div class="row" style="margin-bottom: 5px;">
						<div class="col-md-12">
							<span style="font-size: 15px; font-weight: 600;">Stops</span>
						</div>
					</div>
					<!-- <hr style="margin-top: 6px; margin-bottom: 6px;"> -->
					<div class="row">

						<div class="checkbox">
							<label style="font-size: 12px;"><input type="checkbox"
								class="filter_link_stopage" data-filter="all" value="" id="stopageall"
								name="stopageall">Show-All</label>
						</div>

						<c:forEach items="${flightNumbersotpage}" var="u">
							<div class="checkbox">
								<label style="font-size: 12px;"><input type="checkbox"
									class="filter_link_stopage" data-filter="${u.stopage}" value=""
									name="${u.stopage}">${u.stopage}</label>
							</div>
						</c:forEach>

					</div>
					<hr style="margin-top: 6px; margin-bottom: 6px;">

					<div class="row">
						<div class="col-md-12">
							<span style="font-size: 15px; font-weight: 600;">Price
								range</span>
							<div class="checkbox">

								<p>
									<input type="text" id="amount"
										style="border: 0; color: #f6931f; font-weight: bold;" />
								</p>
								<div id="slider-container"></div>
								<div id="slider-range"></div>

							</div>
						</div>
					</div>

				<!-- <hr style="margin-top: 6px; margin-bottom: 6px;">
					<div class="col-md-12">
                	<div class="row" style="margin-bottom: 14px;">
	                  <div class="col-md-12">
	                    <span style="font-size: 15px; font-weight: 600;">Departure Time</span>
	                  </div>
	                </div>
	                <div class="row" style="font-size: 12px;">
	                  <div class="col-md-12 filter-dep">
	                    <ul style="">
	                      <li onclick="filterTime('MORNING')">
	                        <div class="morning_icon" data-filter="6bf"></div>
	                        <div class="bef-time"><span>Before 6AM</span></div>
	                      </li>
	                      <li ng-class="" onclick="filterTime('AFTERNOON')">
	                        <div class="noon_icon" data-filter="6AM-12PM"></div>
	                        <div class="bef-time"><span>6AM-12PM</span></div>
	                      </li>
	                      <li ng-class="" onclick="filterTime('EVENING')">
	                        <div class="evening_icon" data-filter="12PM-6PM"></div>
	                        <div class="bef-time"><span>12PM-6PM</span></div>
	                      </li>
	                      <li ng-class="" onclick="filterTime('NIGHT')">
	                        <div class="night_icon" data-filter="6af"></div>
	                        <div class="bef-time"><span>After 6PM</span></div>
	                      </li>
	                    </ul>
	                  </div>
	                </div>
	                <hr>
                </div> -->

					<hr style="margin-top: 6px; margin-bottom: 6px;">
					<span style="font-size: 15px; font-weight: 600;">Airlines</span>
					<c:forEach items="${flightnumberdata}" var="u">
						<div class="row">
							<div class="col-md-12">
								<div class="checkbox" style="float: left;">
									<label><input type="checkbox" class="filter_link"
										data-filter="${u.airlineName}" value="" name="flnamefilture">${u.flightName}</label>
								</div>
							</div>
						</div>
					</c:forEach>
				</div>
			</div>
		</div>

		<!--============================================== FLIGHTS =========================================================-->

		<div class="col-md-9">
			<div class="panel-body" style="margin-top: -15px;">

				<div class="wrap-contain" style="background: #ffffff;">

					<div class="row">
						<div class="col-md-12 col-sm-12">
							<div class="flight-wrap">
								<div class="row wrap-title" style="">
									<div class="col-md-2 col-sm-2 col-xs-4">
										<a href="#" id="filterAirLine" > <!--  -->Airline</a>
									</div>
									<div class="col-md-2 col-sm-2 col-xs-4">
										<a href="#">Departure</a>
									</div>
									<div class="col-md-2 col-sm-2 col-xs-4">
										<a href="#">Duration</a>
									</div>
									<div class="col-md-2 col-sm-2 col-xs-4">
										<a href="#">Arrival</a>
									</div>
									<div class="col-md-2 col-sm-2 col-xs-4">
										<a href="#">Price</a>
									</div>
									<!-- <div class="col-md-2 col-sm-2 col-xs-4">
										<a href="#">Flight Book</a>
									</div> -->
								</div>
							</div>
						</div>
					</div>

				</div>

			</div>
		</div>

		<!--============================================== FLIGHT ===========================================-->
		<div class="col-md-9" id="xzc">
			<c:forEach items="${flightdata}" var="u" varStatus="loopCount">
				<div class="stopage ${u.stopage}" id="flightcard_${loopCount.count}">
					<div class="media ${u.airlineName}">
					 <div class="flightName ${u.flightName}">
						<div id="computers">
							<div class="system" data-price="${u.amount}">
								<div class="panel panel-default">
									<%--panel panel-default media ${u.airlineName} --%>
									<div class="panel-body">
										<div class="row">
											<div class="col-md-12 col-sm-12 wrap-contain"
												style="background: #ffffff;">

												<div class="row wrap-dtls">
													<div class="col-md-2 col-sm-2 col-xs-4">
														<div class="row">
															 <div class="col-md-5 col-sm-5">
				                        				  <img src="${pageContext.request.contextPath}/resources/images/User/travel/img/airline/${u.airlineName}.png" alt="Not Found" onerror="this.src='${pageContext.request.contextPath}/resources/images/User/travel/img/airline/plane.png';" class="airline-img" style="width: 45px;">
				                      					  </div>
															<div class="col-md-7 col-sm-7">
																<span class="air-nme" ><b id="flsrtNM_${loopCount.count}">${u.flightName}</b></span> <br>
																<span class="air-num">${u.airlineName}-${u.flightNumber}</span>
															</div>
														</div>
													</div>
													<div class="col-md-2 col-sm-2 col-xs-4">
														<i class="fa fa-plane" style="font-size: 20px;"></i> <span
															class="time">${origin}</span> <br> <span class="dep">${u.departureTime}</span>
													</div>
													<div class="col-md-2 col-sm-2 col-xs-4">
														<span class="dur">${u.journeyTime}</span> <br> <img
															class="dur-arrw"
															src="${pageContext.request.contextPath}/resources/images/arrow.svg">
														<br> <span class="stops">${u.stopage}</span>
													</div>
													<div class="col-md-2 col-sm-2 col-xs-4">
														<i class="fa fa-plane fa-flip-vertical"
															style="font-size: 20px;"></i> <span class="time">${destination}</span>
														<br> <span class="dep">${u.arrivalTime}</span>
													</div>
													<div class="col-md-2 col-sm-2 col-xs-4">
														<span class="fa fa-inr"></span> <span class="fare" id="flightPrice_${loopCount.count}">${u.amount}</span>
													</div>
													<div class="col-md-2 col-sm-2 col-xs-4">

														<p id="responsemessage${u.number}"></p>
														<p class="rederror" id="responsemessage${u.number}"></p>
														<div style="display: none; width: 30%;"
															id="loader${u.number}">
															<img src="/resources/images/spinner.gif">

														</div>
														<button type="button" id="bookbuuton${u.number}"
															onclick="openflightdetail('${u.flightNumber}','${u.searchId}','${u.number}','${u.stopage}','${loopCount.count}','${cabinType}')"
															class="btn btn-default btn-srch">Book Flight</button>
														
														<span class="rederror" id="priceErr${loopCount.count}"> </span>
													</div>
												</div>
											</div>
											<!-- <div class="line"></div> -->


											<div class="container-fluid">
												<div class="row">
													<div class="col-md-12">
														<div id="${u.number}" class="collapse">
															<div class="well">
																<ul class="nav nav-tabs">
																	<li class="active"><a data-toggle="tab"
																		href="#fare${u.number}">Fare Details</a></li>
																	<li><a data-toggle="tab"
																		href="#baggage${u.number}">Baggage Information</a></li>

																</ul>
																<div class="tab-content">
																	<!-- Baggage tab ====================================================================== -->
																	<div id="baggage${u.number}" class="tab-pane fade">
																		<table class="table table-bordered table-responsive">
																			<thead>
																				<tr>
																					<th>Flight Details</th>
																					<th>CheckIn(Adult/Child)</th>
																				</tr>
																			</thead>
																			<tbody>
																				<tr>
																					<td>${u.airlineName}-${u.flightNumber}</td>

																					<td>${u.baggageWeight}${u.baggageUnit}/Person</td>

																				</tr>
																			</tbody>
																		</table>
																	</div>
																	<!-- / End of Baggage tab ====================================================================== -->

																	<!-- Fare Details tab ====================================================================== -->
																	<div id="fare${u.number}"
																		class="tab-pane fade in active">
																		<div class="row">
																			<div class="col-md-12 f-rules">
																				<!-- Fare Rules -->
																				<div class="col-md-7 col-sm-7">
																					<div class="fare-box">

																						<h6>
																							<b>Fare Rules</b>
																						</h6>

																						<div class="row">
																							<div class="col-md-6 col-sm-6">
																								<h5>Cancellation Charges</h5>
																								<table class="table table-bordered">
																									<tbody>
																										<tr>
																											<td>Airline Fee</td>
																											<td id="airlinefee${u.number}"><span
																												class="fa fa-inr"></span> 2650</td>
																										</tr>

																									</tbody>
																								</table>
																							</div>
																							<div class="col-md-6 col-sm-6">
																								<h5>Reschedule Charges</h5>
																								<table class="table table-bordered">
																									<tbody>
																										<tr>
																											<td>Airline Fee</td>
																											<td id="changepenalty${u.number}"><span
																												class="fa fa-inr"></span> 2650</td>
																										</tr>

																									</tbody>
																								</table>
																							</div>
																						</div>
																						<div class="row">
																							<div class="col-md-12 col-sm-12">
																								<h6>
																									<b>Terms and Conditions</b>
																								</h6>
																								<div class="terms-box">
																									<ul>
																										<li>Penalty is subject to 4 hours prior
																											to departure and no changes are allowed after
																											that.</li>
																										<li>The charges will be on per passenger
																											per sector</li>
																										<li>Rescheduling Charges =
																											Rescheduling/Change Penalty + Fare Difference
																											(if applicable)</li>
																										<li>Partial cancellation is not allowed
																											on the flight tickets which are book under
																											special discounted fares</li>
																										<li>In case, the customer have not
																											cancelled the ticket within the stipulated
																											time or no show then only statutory taxes are
																											refundable from the respective airlines</li>
																										<li>For infants there is no baggage
																											allowance</li>
																										<li>In certain situations of restricted
																											cases, no amendments and cancellation is
																											allowed</li>
																										<li>Penalty from airlines needs to be
																											reconfirmed before any cancellation or
																											amendments</li>
																										<li>Penalty changes in airline are
																											indicative and can be changes without any
																											prior notice</li>
																									</ul>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																				<!-- Fare Details -->
																				<div class="col-md-5 col-sm-5">
																					<table class="table table-bordered">
																						<tbody>

																							<tr>
																								<td><b>Total(Base Fare)+</b></td>
																								<td id="totalbasefare${u.number}"><span
																									class="fa fa-inr"></span> 2575</td>
																							</tr>
																							<tr>
																								<td><b>Total Tax</b></td>
																								<td id="totaltax${u.number}"><span
																									class="fa fa-inr"></span> 370</td>
																							</tr>
																							<tr>
																								<td><b>Convenience Fee</b></td>
																								<td>200</td>
																							</tr>
																							<tr>
																								<td><b>Grand Total</b></td>
																								<td id="grandtotal${u.number}"><span
																									class="fa fa-inr"></span> 2945</td>
																							</tr>
																						</tbody>
																					</table>
																				</div>
																				<button type="button"
																					onclick="checkoutFlight('${u.flightNumber}','${u.searchId}',${u.number})"
																					class="btn btn-default btn-srch">Book
																					Flight</button>
																			</div>
																		</div>
																	</div>
																	<!-- / End of Fare deatils tab ====================================================================== -->

																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				   </div>
				</div>
			</c:forEach>
		</div>
</div>
		<!-- Model for flight gif -->
		<div id="loading_flight" class="modal fade" role="dialog"
			style="margin-top: 7%;">
			<div class="modal-dialog" style="background:">

				<!-- Modal content-->
				<div class="modal-content"
					style="background: transparent; box-shadow: none; border: none;">
					<center>
						<img
							src="${pageContext.request.contextPath}/resources/images/User/travel/img/Flight/flight.gif"
							class="img-responsive">
					</center>
				</div>

			</div>
		</div>
		<!-- Model for flight gif -->


		<script>
		
		function srcdest() {
			console.log("Compare");
		$('#errormsgdest').empty();
		var sr=$('#flightdestination').val();
		var ds=$('#flightsource').val();
			
			if (sr.includes(ds)) {
			$('#errormsgdest').html("Please select source & destation different.");

			}
		}
		
		
	 var tmp=[];
	 $('.filter_link').click(function(e){
	 
	var mediaElements = $('.media');
	// get the category from the attribute
	var filterVal = $(this).data('filter');

	if (tmp.indexOf(filterVal)==-1) {
		
		tmp.push(filterVal);
		console.log("tmp :: if"+filterVal);
	}
	else {
		var a=tmp.indexOf(filterVal);
		tmp.splice(a,1);
		console.log("tmp :: else"+tmp);
	}
	
	console.log("tmp length"+tmp.length);
	
	mediaElements.hide();
	
	if (tmp.length==0) {
		console.log("Show all");
		 mediaElements.show();
	}
	
	
	for (var i = 0; i <tmp.length; i++) {
		console.log("filterVal:: "+tmp[i]);
	 mediaElements.filter('.' + tmp[i]).show();
	}
	
	
// 	mediaElements.hide().filter('.' + filterVal).show();

});
	 
	 
	 var tmpStop=[];
	 
	 $('.filter_link_stopage').click(function(e){
		 
			var mediaElements = $('.stopage');
			// get the category from the attribute
			
			var filterVal = $(this).data('filter');

			mediaElements.hide();
			
			 if(filterVal === 'all'){
			   mediaElements.show();
// 			 $("input[name='stopage']").attr('checked', false);
			      
			  for (var i = 0; i <tmpStop.length; i++) {
			  $("input[name='"+tmpStop[i]+"']").attr('checked', false);
			   }  
			  tmpStop=[];
			    }else{
			    	mediaElements.hide();
			    	 $("input[name='stopageall']").attr('checked', false);
			    	if (tmpStop.indexOf(filterVal)==-1) {
			    		
			    		tmpStop.push(filterVal);
			    		console.log("tmp Stop:: if"+filterVal);
					}
			    	else {
						var a=tmpStop.indexOf(filterVal);
						tmpStop.splice(a,1);
						console.log("tmp Stop:: else"+tmpStop);
					}

			    	// hide all then filter the ones to show
			       
			    	console.log("tmp Stop length"+tmpStop.length);
			    	if (tmpStop.length==0) {
			    		mediaElements.show();
					}
			    	
			       for (var i = 0; i <tmpStop.length; i++) {
			    	   
					console.log("filterVal::"+i+" "+tmpStop[i]);
					
	 				mediaElements.filter('.' + tmpStop[i]).show();
				  }
			    }
		});

	 
	 $('#clr_filtr').click(function(e){
		 	
		 
			var mediaElements = $('.stopage');
			var mediaElement = $('.media');
			// get the category from the attribute
			
			 mediaElements.show();
			 mediaElement.show();
			 $("input[name='flnamefilture']").attr('checked', false);
			 
			 $("input[name='stopageall']").attr('checked', false);
			 $("input[name='stopage']").attr('checked', false);
			 
			 for (var i = 0; i <tmpStop.length; i++) {
				  $("input[name='"+tmpStop[i]+"']").attr('checked', false);
				   }  
			 
			 tmp=[];
			 tmpStop=[];
			 
		});
	 

	 var airlineList=[];
	 
	 $(document).ready(function(){
	 
		 $("input[name='flnamefilture']").attr('checked', false);
		 
		 $(".filter_link_stopage").attr('checked', false);
		 
		 tmp=[];
		 tmpStop=[];
		 
		 var flightList= $("#flightdata").val();
		 
		 /* for (var i = 1; i <= flightList.length; i++) {
			
			 console.log("flight name: "+$("#flsrtNM_"+i).html());
			 
			 if ($("#flsrtNM_"+i).html()!=null) {
				 if (airlineList.indexOf($("#flsrtNM_"+i).html())==-1) {
					 airlineList.push($("#flsrtNM_"+i).html());
				}
			}
			
		} */
		 
	});
	 
	  function abc() {
         	console.log("Flight sort");
         	var mediaElements = $('.flightName');
      	mediaElements.hide();
      	 airlineList.sort();
      	 console.log("AirLine List: "+airlineList);
      	 
         	for (var i = 0; i < airlineList.length; i++) {
         		
         		if (airlineList[i]!=null) {
         			mediaElements.filter('.' +airlineList[i]).show();
            		 console.log("AirLine of "+i+" : "+airlineList[i]);
				}
         		
				} 
			}
	 
	  
	function openflightdetail(flightNumber,searchId,id,stopage,colNo,cabinType)
	{
		var contextPath = "${pageContext.request.contextPath}";
		var spinnerUrl = "Please wait <img src="+contextPath+"'/resources/images/spinner.gif' style='width:25px;'>"
		
		 console.log("colNo: "+colNo+"numeer:"+id+"searchId:"+searchId);
		
// 		$("#loader"+id).show();
		$("#bookbuuton"+id).html(spinnerUrl);
		$("#bookbuuton"+id).addClass("disabled");
		 document.getElementById("responsemessage"+id).value="";	
		 if(stopage=="Non-Stop")
			 {
				$.ajax({
					type : "POST",
					contentType : "application/json",
					url : "${pageContext.request.contextPath}/User/Travel/Flight/ShowPriceDetail",
					data : JSON.stringify({
						"flightNumber" : "" +flightNumber + "",
						"searchId" : "" +searchId + "",
						"colNo":""+colNo+"",
						"cabin":""+cabinType+"",
						"onewayflight" : "" +"Firstfligt" + ""
						
					}),
					success : function(response) {
					
						var totalFare = new Array();

						totalFare = response.split("@");

						
				 if(totalFare[0]==="S00")
				 {
					 var fares = new Array();
					 fares = totalFare[1].split("#");
					 document.getElementById("totalbasefare"+id).innerHTML = fares[1];
					 document.getElementById("totaltax"+id).innerHTML = fares[2];
					 document.getElementById("grandtotal"+id).innerHTML = fares[3];
					 document.getElementById("airlinefee"+id).innerHTML = fares[4];
					 document.getElementById("changepenalty"+id).innerHTML =fares[5];
					
					 $("#bookbuuton"+id).hide();
						 $("#"+id).show('show');
						 $("#bookbuuton"+id).removeClass("disabled");
						 $("#loader"+id).hide();
				 }
				 else if(totalFare[0]==="F03")
				 {
					 window.location.href='${pageContext.request.contextPath}/User/Travel/Flight/OneWay'; 
				 }
			 else
			 {
				 console.log("direct price");
				 /* $("#loader"+id).hide();
				 $("#"+id).collapse('hide');
				 $("#bookbuuton"+id).show(); */
				 
				 $("#bookbuuton"+id).removeClass("disabled");
				 $("#bookbuuton"+id).html("Book Flight");
				 document.getElementById("priceErr"+colNo).innerHTML="Please Try Again Later";	
			 }
			}
				});
			
			 }
		 else
			 {
			 
		
		$.ajax({
			type : "POST",
			contentType : "application/json",
			url : "${pageContext.request.contextPath}/User/Travel/Flight/ConnectingShowPriceDetail",
			data : JSON.stringify({
				"flightNumber" : "" +flightNumber + "",
				"onewayflight" : "" +"Firstfligt" + "",
				"searchId" : "" +searchId + "",
				"colNo":""+colNo+"",
				"cabin":""+cabinType+""
			}),
			success : function(response) {
			
				var totalFare = new Array();

				totalFare = response.split("@");

				
		 if(totalFare[0]==="S00")
		 {
			 var fares = new Array();
			 fares = totalFare[1].split("#");
			 document.getElementById("totalbasefare"+id).innerHTML = fares[1];
			 document.getElementById("totaltax"+id).innerHTML = fares[2];
			 document.getElementById("grandtotal"+id).innerHTML = fares[3];
			 document.getElementById("airlinefee"+id).innerHTML = fares[4];
			 document.getElementById("changepenalty"+id).innerHTML =fares[5];
			
			 $("#bookbuuton"+id).hide();
				 $("#"+id).show('show');
				 $("#bookbuuton"+id).removeClass("disabled");
				 $("#loader"+id).hide();
		 }
		 else if(totalFare[0]==="F03")
		 {
			 window.location.href='${pageContext.request.contextPath}/User/Travel/Flight/OneWay'; 
		 }
	 else
	 {
		 console.log("connecting price");
		 /* $("#loader"+id).hide();
		 $("#"+id).collapse('hide');
		 $("#bookbuuton"+id).show(); */
		 $("#bookbuuton"+id).html("Book Flight"); 
		 $("#bookbuuton"+id).removeClass("disabled");
		 document.getElementById("priceErr"+colNo).innerHTML="Please Try Again Later";		
				
	 }
		
			}
		});
	
			 }
	}
	
	function checkoutFlight(flightNumber,searchId,id)
	{
		var val= $("#isInternational").val();
		 console.log("International"+val);
		 window.location = "CheckOut";
	}
	 
		function setsrcanddestination() {

			var traveldata = document.getElementById("traveldataval").value;
			var travel_code = new Array();

			travel_code = traveldata.split("!");

			var travel_name = new Array();
			travel_name = travel_code[1].split("@");

			var sourcecode = new Array();
			sourcecode = travel_code[0].split("#");
			
			$(".js-example-basic-multiple").select2();
			
			document.getElementById("flightsrc").innerHTML = "";
			for (i = 0; i < travel_name.length; i++) {
				var option = document.createElement("option");

				option.text = travel_name[i];
				option.value = sourcecode[i];

				var select = document.getElementById("flightsrc");
				if (i == 0) {
					option.text = $('#origin').val();
					option.value = $('#destination').val();
					select.add(option);

				}

				select.add(option);
				console.log("dd");
			}

		 
			document.getElementById("flightdest").innerHTML = "";
			for (i = 0; i < travel_name.length; i++) {
				var option = document.createElement("option");

				option.text = travel_name[i];
				option.value = sourcecode[i];

				var select = document.getElementById("flightdest");
				if (i == 0) {
					option.text = $('#destination').val();
					option.value = $('#destination').val();
					select.add(option);

				}

				select.add(option);

			}
		 
		}
		

	</script>

		<script>
            $( "#flightsrc" ).select2({
                theme: "bootstrap"
            });
            $( "#flightdest" ).select2({
                theme: "bootstrap"
            });
            $( "#location_list" ).select2({
                theme: "bootstrap"
            });
            $( "#bus_src_list" ).select2({
                theme: "bootstrap"
            });
            $( "#bus_des_list" ).select2({
                theme: "bootstrap"
            });
            $( "#holiday_list" ).select2({
                theme: "bootstrap"
            });
        </script>

		<!-- ================ Script for Flight traveller details ============================ -->
		<script>
            $(document).ready(
                function(){
                    $("#selTrav").click(function () {
                        $("#trav_menu").fadeToggle();
                    });

                });
        </script>

		<!-- ================ Script for Room traveller details ============================ -->
		<script>
            $(document).ready(
                function(){
                    $("#rm_ppl").click(function () {
                        $("#htl_menu").fadeToggle();
                    });

                });
            
            	function getvalue() 
            	{
            		$("#loader").show();
            		$("#trav_menu").hide();
            	 	var valid=true;
            		var contextPath = "${pageContext.request.contextPath}";
            		var spinnerUrl = "Please wait <img src="+contextPath+"'/resources/images/spinner.gif' style='width:25px;'>";
            		
            		$("#loader").html(spinnerUrl);
            		$("#loader").addClass("disabled");
            		
            		document.getElementById("errormsgsrc").innerHTML="";
            		document.getElementById("responsemessage").innerHTML="";
            	    document.getElementById("errormsgdest").innerHTML="";
            		var flightdest = document.getElementById("flightdest");
            		var flightdestselect = flightdest.options[flightdest.selectedIndex].value;

            		var flightsrc = document.getElementById("flightsrc");
            		var flightsrcselect = flightsrc.options[flightsrc.selectedIndex].value;
            		var date = document.getElementById("date").value;
            		
            	//	var tripType = document.getElementById("tripType").value;
            		var cabin = document.getElementById("flightcabin").value;
            		var adults = document.getElementById("adult1").innerHTML;
            		var childs = document.getElementById("childs1").innerHTML;
            		var infants = document.getElementById("infants1").innerHTML;
            		var returndate = document.getElementById("returndate").value;
            		 
            		var srchtml=flightsrc.options[flightsrc.selectedIndex].text;
            		var desthtml=flightdest.options[flightdest.selectedIndex].text;
            		
            		if(returndate!=="" && returndate!==null){
            			valid=compareDate(returndate, date);
            			console.log("compareapi:"+valid);
            			document.getElementById("errDeptdate").innerHTML="";
            			if(!valid){
            				document.getElementById("errDeptdate").innerHTML="Departure date can't be greater than return date";
            				 $("#loader").html("Modify Search");
            	           		$("#loader").removeClass("disabled");
            			}
            		}
            		 if(flightsrcselect.length <=1)
            			{
            			 $("#loader").html("Modify Search");
                 		$("#loader").removeClass("disabled");
            			document.getElementById("errormsgsrc").innerHTML="Please Select Source";
            			valid=false;
            			}
            		 else if(flightdestselect.length <=1)
            			{
            			 $("#loader").html("Modify Search");
                  		$("#loader").removeClass("disabled");
            			document.getElementById("errormsgdest").innerHTML="Please Select Destination";
            			} else if (date.length <= 1) {
            				$("#loader").html("Modify Search");
                     		$("#loader").removeClass("disabled");
            				document.getElementById("errormsgdate").innerHTML = "Please Select Date";
            				valid=false;
            			} 
            		 else if(adults<1)
           			{
            			 $("#loader").html("Modify Search");
                  		$("#loader").removeClass("disabled");
           			document.getElementById("errormsgadult").innerHTML="Please Add One Adult";
           			valid=false;
           			}
            		 
            			else if(valid)
            				{
            				if(returndate.length <=1)
            				{
            					$.ajax({
            						type : "POST",
            						contentType : "application/json",
            						url : "${pageContext.request.contextPath}/User/Travel/Flight/OneWay",
            						data : JSON.stringify({
            							"origin" : "" +flightsrcselect + "",

            							"destination" : "" +flightdestselect + "",

            							"beginDate" : "" +date + "",

            							"engineIDs" : "" +"AYTM00011111111110002" + "",

            							"tripType" : "" +"OneWay" + "",

            							"cabin" : "" +cabin + "",

            							"adults" : "" +adults + "",

            							"childs" : "" + childs+ "",

            							"infants" : "" +infants + "",
            							"endDate":"" +date + "",
            							"traceId" : "" + "AYTM00011111111110002"+ "",
            							"srcFullName":""+srchtml+"",
             							"destFullName":""+desthtml+"",
            						}),
            						success : function(response) {

            							console.log("response:: "+response);
            							
            							 var d=response.split("#");
            							 if(d[0]==="S00")
            							 {
            						 window.location = "OneWay";
            					 }
      							 else if(d[0]==="F03")
      							 {
      								 console.log("Session expired");
      								 window.location.href='${pageContext.request.contextPath}/Home'; 
      							 }
      							else if (d[0]==="F02") {
      								$("#loader").hide();
      								$("#loading_flight").hide();
      								$("#flght_srch").removeClass("disabled");
      								document.getElementById("responsemessage").innerHTML="Server Down.Please Try Again Later";	
      							}
            				 else
            				 {
            					 $("#loader").html("Modify Search");
            					 $("#loading_flight").hide();
            					 $("#flght_srch").removeClass("disabled");
            					 $("#flght_srch").html("disabled");
            					 document.getElementById("responsemessage").innerHTML=""+d[1];	
            				 }
            							 
            							
            						}
            					});
            				}
            				else if(valid)
            					{
            					
            					$.ajax({
            						type : "POST",
            						contentType : "application/json",
            						url : "${pageContext.request.contextPath}/User/Travel/Flight/OneWay",
            						data : JSON.stringify({
            							"origin" : "" +flightsrcselect + "",

            							"destination" : "" +flightdestselect + "",

            							"beginDate" : "" +date + "",

            							"engineIDs" : "" +"AYTM00011111111110002" + "",

            							"tripType" : "" +"RoundTrip" + "",

            							"cabin" : "" +cabin + "",

            							"adults" : "" +adults + "",

            							"childs" : "" + childs+ "",

            							"infants" : "" +infants + "",
            							"endDate":"" +returndate + "",
            							"traceId" : "" + "AYTM00011111111110002"+ "",
            							"srcFullName":""+srchtml+"",
             							"destFullName":""+desthtml+"",
             							
            						}),
            						success : function(response) {
            					 
            							 var d=response.split("#");
            							 if(d[0]==="S00")
            							 {
            								 if(d[1]==="DomesticRoundway")
            									 {
            									 window.location = "RoundWay";
            									 }
            								 else
            									 {
            									 window.location = "internationalRoundWay";
            									 }
            								
            							 }
            							 else if(d[0]==="F03")
            							 {
            								 window.location.href='${pageContext.request.contextPath}/Home'; 
            							 }
            							 
            							 else if (d[0]==="F02") {
            								 $("#loader").hide();
            								 $("#loading_flight").hide();
            								 $("#flght_srch").removeClass("disabled");
            								 document.getElementById("responsemessage").innerHTML="Server Down.Please Try Again Later";	
            							 }
            						 else
            						 {
            							 $("#loader").html("Modify Search");
            							$("#loading_flight").hide();
            							 $("#flght_srch").removeClass("disabled");
            							
            							 document.getElementById("responsemessage").innerHTML=""+d[1];	
            						 }
            						}
            					});
            					
            					}
            	}
            	}
            	
            	

           	 function increaseValue(id,idset)
           	 {
                	  var value = parseInt(document.getElementById(""+id).value, 10);
                	  if(value<=4)
                		  {
                		 var d = parseInt(document.getElementById("totalCounttrevaler").innerHTML);
                		 var c=parseInt(d);
                    	 c++;
                    	document.getElementById("totalCounttrevaler").innerHTML=""+c;
                	  value = isNaN(value) ? 0 : value;
                	  value++;
                	  document.getElementById(""+idset).innerHTML = ""+value;
                	  document.getElementById(""+id).value = ""+value;
                		  }
                	}

                	function decreaseValue(id,idset) 
                	{
                	  var value = parseInt(document.getElementById(""+id).value, 10);
                	 if(value>0)
           		  {
           		 
           	
              		var d = parseInt(document.getElementById("totalCounttrevaler").innerHTML);
             		 var c=parseInt(d);
                 	 c--;
                 	document.getElementById("totalCounttrevaler").innerHTML=""+c;
           	
           		  }
                	  value = isNaN(value) ? 0 : value;
                	  value < 1 ? value = 1 : '';
                	  value--;
                	 
                	  document.getElementById(""+idset).innerHTML = ""+value;
                	  document.getElementById(""+id).value = ""+value;
                	
                	 
                	}
                	function changesclass() 
                	{
                		var d =document.getElementById("flightcabin").value;
                	document.getElementById("flight_cls").innerHTML=", "+d;
                	}
               	function closedivtrevalinformation()
               	{
               		$("#trav_menu").hide();
               	}
               	  
               	
               	
              var arr=[$("#asdb").val()];
               	
                $("#fhdsk").click(function () {
                	console.log("Sort");
                	
                	arr.sort();
                	
//                 	console.log("Sort val:: "+arr[0].flightName);
                	
                	 $("#xzc").val(arr);
                });
               
               	
                /* function filterTime(time)
                {
                	console.log("Time: "+time);
                	var mediaElements = $('.dept');
                	var filterVal='02:55'
                	console.log("mediaElements: "+mediaElements);
                	mediaElements.hide().filter('.' + filterVal).show();
                	
                } */
                
                function filterTime(time)
                {
                 var json=$("#flName").val();
                
                console.log("json: "+json.length);
                var s=json.split(",");
                var mediaElements = $('.flightName');
            	mediaElements.hide();
            	
                for (var i = 0; i < s.length; i++) {
                	if (i<s.length-1) {
                		console.log("Flight : "+s[i].replace("[",""));
                		 mediaElements.filter('.'+s[i].replace("[","")).show();
					}
                	else{
                	console.log("Flight Lst: "+s[s.length-1].replace("]",""));
                	 mediaElements.filter('.'+s[s.length-1].replace("]","")).show();
                	}
				}
            	
                /* $("#filterAirLine").click(function () {
                	
                	console.log("Flight sort");
                	var mediaElements = $('.flightName');
                	
                	mediaElements.hide();
                	mediaElements.sort();
                	mediaElements.show();

                }); */
                }
             
                function compareDate(returnDate, date){
					var date1 = new Date(date);
					var date2= new Date(returnDate);
					console.log("return date"+date2.getTime());
					console.log("departure date"+date1.getTime());
					if(date2.getTime() < date1.getTime()){
						return false;
					}
					return true;
				} 
                
        </script>

		<script src="https://harvesthq.github.io/chosen/chosen.jquery.js"></script>
</body>
</html>