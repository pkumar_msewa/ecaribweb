<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*"  isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<sec:csrfMetaTags/>

	<div class="col-md-3">
		<div class="list-group">
		<li class="list-group-item zero-padding">
	<a data-toggle="modal" data-target="#Pic_upload"><img alt="avatar" style="width: 100%;
    height: 300px;" class="img-responsive" src="/resources/images/sample.jpg" id="display_pic"></a>
</li>

<script type="text/javascript">

</script>

<center>
  	<!-- Modal for Upload Picture -->
	  <div class="modal fade" id="Pic_upload" role="dialog">
	    <div class="modal-dialog modal-sm">
	    
	      <!-- Modal content-->
	      <div class="modal-content rchrg_modal">
	        <div class="modal-header" style="background: #005cac;">
	          <button type="button" class="close" data-dismiss="modal">&times;</button>
	          <center><h2 class="modal-title">Upload Picture</h2></center>
	        </div>
	        <div class="modal-body">
	          <div class="row">
	          	<div class="col-md-12 col-sm-12 col-xs-12">
				<form method="post" action="<c:url value="/User/UploadPicture"/>"
					enctype="multipart/form-data"   class="form form-inline">
					<input type="File" name="profilePicture" class="form-control" accept="image/*" required="required" style="width: 86%; background: #EDEDED;">
			       <sec:csrfInput/>
		         <button type="submit" class="btn btn-primary">Upload</button>
				</form>
			</div>
	          </div>
	        </div>
	      </div>
	    </div>
	  </div>
	  <!-- Modal for Upload Picture -->
	  <script>
	  $("#eicon_link").click(function(){
		    alert("The paragraph was clicked.");
		     $("#fname_edit").css('border-bottom','1px solid #000');
		   
		});
		$("#eicon_link").click(function(){
		    alert("The paragraph was clicked.");
		    document.getElementById("fname_edit").style.border="none";
		   
		});
	  </script>
	  
	</center>
			<div class="list-group-item profile-details" style="line-height: 2px; border-bottom: none;">
				<center><h2><b>Welcome</b></h2>
					<h3 style="color: #555555;"><input type="text" readonly="readonly" id="fname_edit" style=" text-align: center;border-bottom: none; float:left;" class="first_name"/></h3><div style="margin-top: 22px;"><a href="#" id="editName"><span id="eicon_link" class="fa fa-pencil-square-o" ></span></a></div>
			       <p class="error" id="error_name"></p>
				</center>
				
			<center><h5><b>Rewards Points:</b> <span class="fa fa-gift" id="user_points"></span></h5></center>
			</div>
			<div class="row">
				<div class="col-md-6 col-sm-6 col-xs-6">
					<center>
						<button class="btn btn-danger" style="width: 100%; font-size: 20px; cursor: text;"><i class="fa fa-dollar"></i> <span id="account_balance"></span></button>
					</center>
				</div>
					<div class="col-md-6 col-sm-6 col-xs-6">
						<center>
							<a class="btn btn-pink" style="font-size: 20px; width: 100%;" data-toggle="modal" data-target="#loadMoneyModal"  data-backdrop="static" data-keyboard="false">Add Money</a>
						</center>
					</div>
			</div>

				<span class="list-group-item" style="border-top: none;"> <b>Mobile Number:</b> <span id="user_mobile"></span></span>
				<span class="list-group-item"> <b>Email:</b> <span id="user_email"></span></span>
				<span class="list-group-item"> <b>Account Number:</b> <span id="account_number"></span></span>
				<span class="list-group-item"> <b>Balance:</b> <span class="fa fa-dollar"></span> <span id="account_bal" class="user_update_balance"></span></span>
				<span class="list-group-item"> <b>Account Type:</b> <span id="account_type"></span></span>
				<span class="list-group-item"> <b>Daily Transaction Limit:</b> <span class="fa fa-dollar"></span> <span id="daily_limit"></span></span>
				<span class="list-group-item"> <b>Monthly Transaction Limit:</b> <span class="fa fa-dollar"></span> <span id="monthly_limit"></span></span>
				<span class="list-group-item"> <b>Change Password</b> <span class="fa fa-pencil-square-o" data-toggle="modal" data-target="#change_pwd" title="Edit"  data-backdrop="static" data-keyboard="false"></span></span>
			    <!-- <span class="list-group-item"><a style="font-size: 20px;width: 180px;margin-top: 13px;margin-left: -3px;" data-toggle="modal" data-target="#change_pwd">Change Password</a></span> -->
			<%-- <div class="col-md-6 col-sm-6 col-xs-6">
						<center>
							<a class="btn btn-pink" style="font-size: 20px;width: 180px;margin-top: 13px;margin-left: -15px;" data-toggle="modal" data-target="#change_pwd">Change Password</a>
						</center>
					</div> --%>
			
			</div>														
		</div>
		<script type="text/javascript" src="<c:url value="/resources/js/userdetails.js"/>"></script>