<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage=""%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<sec:csrfMetaTags />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta http-equiv="X-Frame-Options" content="deny">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <title>UPI Success</title>
    
	<link rel="icon" href='<c:url value="/resources/images/favicon.png"/>' type="image/png" />
	<link rel="stylesheet" href="/resources/css/new_css/cloud-admin.css">
	<link rel="stylesheet" href="/resources/css/font-awesome.min.css">
	<link rel="stylesheet" href="/resources/css/new_css/default.css">
	<link rel="stylesheet" href="/resources/css/new_css/responsive.css">
	<link rel="stylesheet" href="/resources/css/new_css/uniform.default.min.css">
	<link rel='stylesheet' href="/resources/css/css_style.css">
	<!-- <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'> -->

	<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> -->
	<!-- <script type="text/javascript" src="/resources/js/new_js/jquery.min.js"></script> -->
		
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>
	<script type="text/javascript" src="/resources/js/new_js/bootstrap.min.js"></script>
	<!-- <script type="text/javascript" src="/resources/js/new_js/jquery.easypiechart.min.js"></script> -->
	<!-- <script type="text/javascript" src="/resources/js/new_js/jquery.sparkline.min.js"></script> -->
		
	<script src="/resources/js/new_js/script.js"></script>
	<script type="text/javascript" src="<c:url value="/resources/js/aj.js"/>"></script>
	<!-- <script type="text/javascript" src="<c:url value="/resources/js/topup.js"/>"></script> -->
	<script type="text/javascript" src="<c:url value="/resources/js/header.js"/>"></script>
	<%-- <script type="text/javascript" src="<c:url value="/resources/js/userdetails.js"/>"></script> --%>
	<!-- <script type="text/javascript" src="<c:url value="/resources/js/sendmoney.js"/>"></script> -->
	<!-- <script type="text/javascript" src="<c:url value="/resources/js/billpay.js"/>"></script> -->
	<!-- <script type="text/javascript" src="<c:url value="/resources/js/invitefriends.js"/>"></script> -->

	<!-- <script src="${pageContext.request.contextPath}/resources/js/Admin/jquery.twbsPagination.js"></script> -->
	<!-- <script src="${pageContext.request.contextPath}/resources/js/Admin/jquery.twbsPagination.min.js"></script> -->
	<!-- <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/datepicker.css"> -->
		
	<!-- <script src="${pageContext.request.contextPath}/resources/js/datepicker.js"></script> -->

	<!-- <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/new_css/datetimepicker.css"> -->
	<!-- <script src="${pageContext.request.contextPath}/resources/js/new_js/datetimepicker.js"></script> -->
</head>
<style>
	.footer_title {
		bottom: 0;
	    position: absolute;
	    width: 100%;
	}
	.err-msg {
		margin-top: 145px;
	}
	.err-msg .err-hed {
		margin-bottom: 10px;
	}
	.err-msg .err-bdy span {
		font-size: 17px;
		font-weight: 600;
	}
	.err-hed .err-img img {
		width: 200px;
	}
</style>

<script type="text/javascript">
$(document).ready(function(){
			 var timeout = setTimeout(function(){
				window.location = "UpiHome";
		          }, 7000);
		});
	</script>
<body style="overflow-x: hidden;">
	<jsp:include page="/WEB-INF/jsp/User/Header.jsp" />
	
	<div class="container">
		<div class="col-md-12">
			<div class="col-md-8 col-md-offset-2">
				<center>
					<div class="err-msg">
						<div class="err-hed">
							<div class="err-img">
								<img src='<c:url value="/resources/admin/upi/tick.gif"/>'>
							</div>
						</div>
						<div class="err-bdy">
							<span>Your payment was successful</span>
						</div>
					</div>
				</center>
			</div>
		</div>
	</div>
	
	<jsp:include page="/WEB-INF/jsp/User/Footer.jsp" />
</body>
</html>