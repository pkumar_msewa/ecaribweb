<%@taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<sec:csrfMetaTags/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Privacy Policy</title>
<link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'
	type="image/png" />
	<script src="<c:url value='/resources/js/jquery.min.js'/>"></script>
	
<link rel="stylesheet"
	href="<c:url value='/resources/css/css_style.css'/>">
<link rel="stylesheet"
	href="<c:url value='/resources/css/animate.min.css'/>">
<link href='<c:url value='/resources/css/font-family.css'/>'
	rel='stylesheet' type='text/css'/>


<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="<c:url value='/resources/css/bootstrap.min.css'/>" type='text/css'>
<!-- Optional theme -->
<link rel="stylesheet"
	href="<c:url value='/resources/css/bootstrap-theme.min.css'/>" type='text/css'>
<script
	src="<c:url value='/resources/js/bootstrap.js'/>"></script>

	<style>
		.no-js #loader {
			display: none;
		}

		.js #loader {
			display: block;
			position: absolute;
			left: 100px;
			top: 0;
		}

		.se-pre-con {
			position: fixed;
			left: 0px;
			top: 0px;
			width: 100%;
			height: 100%;
			z-index: 9999;
			background: url(/images/pq_large.gif) center no-repeat #fff;
		}
	</style>
	<script src="<c:url value='/resources/js/modernizr.js'/>"></script>

	<script type="text/javascript">
		$(window).load(function() {
			$(".se-pre-con").fadeOut("slow");
		});
	</script>
</head>
<body>
	<div class="se-pre-con"></div>
	<nav class="navbar navbar-default"
		style="min-height: 80px; margin-bottom: 0">
		<div class="container">
			<div class="navbar-header">
				<a class="navbar-brand" href="#"><img
					src="resources/images/vijayalogo.png" alt="logo" style="width: 230px; margin-top: 8px;" ></a>
			</div>	 	
		</div>
		<!-- /.container-fluid -->
	</nav>
	<div class="line" style="height: 4px; background: #17bcc8;"></div>
	<!-----------------end navbar---------------------->
	<div class="container">
		<div class="container" id="aboutus">
			<div class="row">
			
				<h2>Privacy Policy</h2>
				<hr
					style="width: 182px; float: left; border: solid; margin-top: 0; margin-left: 10px; color: #17bcc8;">
				<img src="resources/images/privacypolicy.jpg" class="img-responsive"
					alt="">
				
					<hr>
					
				<div class="col-md-12">
					<p>This Privacy Policy provides you with details about the manner in which your data is collected, stored & used by us. You are advised to read this Privacy Policy carefully. By visiting VPayQwik's website/WAP site/applications you expressly give us consent to use & disclose your personal information in accordance with this Privacy Policy. If you do not agree to the terms of the policy, please do not use or access VPayQwik website, WAP site or mobile applications. Note: Our privacy policy may change at any time without prior notification. To make sure that you are aware of any changes, kindly review the policy periodically. This Privacy Policy shall apply uniformly to VPayQwik desktop website, VPayQwik mobile WAP site & VPayQwik mobile applications</p>
					<hr>
                    
					<h4>General</h4>				
					<p>We will not sell, share or rent your personal information to any 3rd party or use your email address/mobile number for unsolicited emails and/or SMS. Any emails and/or SMS sent by VPayQwik will only be in connection with the provision of agreed services & products and this Privacy Policy. Periodically, we may reveal general statistical information about VPayQwik & its users, such as number of visitors, number and type of goods and services purchased, etc. We reserve the right to communicate your personal information to any third party that makes a legally-compliant request for its disclosure.</p>
					<hr>
					
					<h4>Personal Information</h4>
					<p>Personal Information means and includes all information that can be linked to a specific individual or to identify any individual, such as name, address, mailing address, telephone number, email ID, credit card number, cardholder name, card expiration date, information about your mobile phone, DTH service, data card, electricity connection, Smart Tags and any details that may have been voluntarily provide by the user in connection with availing any of the services on VPayQwik. When you browse through VPayQwik, we may collect information regarding the domain and host from which you access the internet, the Internet Protocol [IP] address of the computer or Internet service provider [ISP] you are using, and anonymous site statistical data.</p>
					<hr>
                 
					<h4>Use of Personal Information</h4>
					<p>We use personal information to provide you with services & products you explicitly requested for, to resolve disputes, troubleshoot concerns, help promote safe services, collect money, measure consumer interest in our services, inform you about offers, products, services, updates, customize your experience, detect & protect us against error, fraud and other criminal activity, enforce our terms and conditions, etc. We also use your contact information to send you offers based on your previous orders and interests. We may occasionally ask you to complete optional online surveys. These surveys may ask you for contact information and demographic information (like zip code, age, gender, etc.). We use this data to customize your experience at VPayQwik, providing you with content that we think you might be interested in and to display content according to your preferences.</p>
					<hr>
					
					<h4>Cookies</h4>
					<p>A "cookie" is a small piece of information stored by a web server on a web browser so it can be later read back from that browser. VPayQwik uses cookie and tracking technology depending on the features offered. No personal information will be collected via cookies and other tracking technology; however, if you previously provided personally identifiable information, cookies may be tied to such information. Aggregate cookie and tracking information may be shared with third parties.</p>
					<hr>
					
					
					<h4>Links to Other Sites</h4>
					<p>Our site links to other websites that may collect personally identifiable information about you. VPayQwik is not responsible for the privacy practices or the content of those linked websites.</p>
					<hr>
                    
                    <h4>Security</h4>
					<p>VPayQwik has stringent security measures in place to protect the loss, misuse, and alteration of the information under our control. Whenever you change or access your account information, we offer the use of a secure server. Once your information is in our possession we adhere to strict security guidelines, protecting it against unauthorized access.</p>
					<hr>

                    <h4>Consent</h4>
					<p>By using VPayQwik and/or by providing your information, you consent to the collection and use of the information you disclose on VPayQwik in accordance with this Privacy Policy, including but not limited to your consent for sharing your information as per this privacy policy.</p>
					<hr>

                 
                </div>
				</div>
				<!----end col-md-12------>
			</div>
			<!----end row----->
			<hr>
		</div>
		<!------end accolades-->

<%--		<div class="row">
			<div class="Accolades">
				<h1>Accolades</h1>
				<img src="resources/images/main/mobile.jpg" class="img-responsive"
					alt="chart">
			</div>
		</div>--%>
		<!---end row-->

	</div>
	<!---------end About Us------------->






	</div>



	<footer>
		<div class="help">
			<div class="container wow bounceIn" data-wow-duration="3s">
				<div class="row">
					<div class="col-sm-3">
						<img src="resources/images/main/help_1.png" alt="">
						<p>Help Line : 080 25011300 or care@vpayqwik.com</p>
					</div>

					<div class="col-sm-3">
						<img src="resources/images/main/help_2.png" alt="">
						<p>The security is our prime consern we ensure your money is
							secure</p>
					</div>

					<div class="col-sm-3">
						<img src="resources/images/main/help_3.png"
							style="padding-bottom: 13px;" alt="">
						<p>VPayQwik makes sure that your every transaction is 100%
							Safe</p>
					</div>

					<div class="col-sm-3">
						<img src="resources/images/main/help_4.png" alt="">
						<p>Spending is earning at VPayQwik We offer various deals to
							distribute happiness</p>
					</div>
				</div>
				<!---end row--->
			</div>
		</div>
		<!-----end help--->

		<div class="container">
			<div class="row">
				<!-- useful links -->
				<div class="col-sm-3 wow bounceInLeft" data-wow-duration="2s">
					<ul class="row footer-links">
						<p>
							<b>Mobile Recharges</b>
						</p>
						<a href="#">Airtel Recharge</a>|
						<a href="#">Vodafone Recharge</a>|
						<a href="#">Aircel Recharge</a>|
						<a href="#">Idea Recharge</a>|
						<a href="#">BSNL Recharge</a>|
						<a href="#">Reliance GSM</a>|
						<a href="#">Reliance CDMA</a>|
						<a href="#">Tata GSM</a>|
						<a href="#">Tata Docomo</a>|
						<a href="#">MTNL</a>|
						<a href="#">MTS</a>|
						<a href="#">T24 Mobile Recharge</a>
						</ul>
				</div>
				<div class="col-sm-3 wow bounceInLeft" data-wow-duration="2s">
					<ul class="row footer-links">
						<p>
							<b>DTH Recharges</b>
						</p>
						<a href="#">Airtel DTH Recharge</a>|
						<a href="#">Videocon D2H Recharge</a>|
						<a href="#">Tata Sky Recharge</a>|
						<a href="#">Reliance DTH Recharge</a>|
						<a href="#">Right Way</a>|
						<a href="#">Dish TV Recharge</a>|
						<a href="#">Sun Direct Recharge</a>
						</ul>
				</div>
				<div class="col-sm-3 wow bounceInRight" data-wow-duration="2s">
					<ul class="row footer-links">
						<p>
							<b>Datacard Recharges</b>
						</p>
						<a href="#">BSNL Datacard</a>|
						<a href="#">Reliance Netconnect</a>|
						<a href="#">Tata Photon</a>|
						<a href="#">Plus</a>|
						<a href="#">Tata Photon Whiz</a>|
						<a href="#">MTS Datacard</a>|
						<a href="#">MTS Blaze</a>|
						<a href="#">MTNL Datacard</a>
						</ul>
				</div>
				<div class="col-sm-3 wow bounceInRight" data-wow-duration="2s">
					<ul class="row footer-links">
						<p>
							<b>Landline Bill Payment</b>
						</p>
						<a href="#">BSNL Landline</a>|
						<a href="#">MTNL Landline Delhi</a>|
						<a href="#">Reliance</a>|
						<a href="#">Tata Docomo</a>
						</ul>
				</div>
			</div>
			<hr>
			<div class="row">
				<p>India's first complete payment application & website,
					VPayQwik is a quickest and safest way for Online Recharge, DTH or
					Datacard Recharge and make mobile or utility Bill Payments for
					Airtel, Aircel,BSNL, Docomo, Idea, MTNL, Vodafone or other
					operators.you do not need to rush to the market to make your DTH or
					mobile bill payments, just log-on to VPayQwik and experience the
					easiest & fastest method of recharges and payments.</p>
				<p>VPayQwik brings to you the various coupons, deals and offers
					to make your payment experience rich every day. Our online recharge
					and bill payment service give you reward points, which can be used
					to avail attractive and lucrative cash back and discount offers.
					Download VPayQwik from your App Store.</p>
				<p>VPayQwik also provides travel ticket service, through
					VPayQwik you can book Air tickes, Bus tickets, Hotels, Car Rental
					and Holiday Packages.Come and experience hassle-free, safe and fast
					ticketing services at VPayQwik.VPayQwik make sure that every
					transaction you do is risk free.</p>
			</div>
			<hr>
			<div class="row" id="footericons">
				<div class="col-sm-8  wow bounceInUp">
					<img src='<c:url value="/resources/images/msewalogo.png"/>' 
					width="100px"  alt="">
				</div>
				<!----end col-md-4--->

				<div class="col-sm-4  wow bounceInUp">
					<ul class="row footer-links">
						<a href="#"> <img src="resources/images/main/fb.gif"
							class="fb"></a>
						<a href="#"> <img src="resources/images/main/tw.gif"
							class="tw"></a>
						<a href="#"> <img src="resources/images/main/yu.gif"
							class="yu"></a>
						<a href="#"> <img src="resources/images/main/in.gif"
							class="in"></a>
					</ul>
				</div>
				<!----end col-md-4--->
				</center>

			</div>
			<!-----end row----->

		</div>
		<div class="menu" id="main">
			<div class="container">
				<div class="row" style="margin-top: 10px; margin-bottom: 10px;">
					<a href="Home">Home</a> <a href="AboutUs">About Us</a> <a
						href="PartnersWithUs">Partner with us</a> <a
						href="Terms&Conditions">Terms & Conditions</a><a
						href="PrivacyPolicy">Privacy Policy</a> <a href="Grievance">Grievance
						policy</a>
				</div>
				<!---row---->
			</div>
			<!----container------->
		</div>
	</footer>

</body>
</html> 