<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html>
<html>
<head>
   <meta charset="utf-8">
    <sec:csrfMetaTags />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>VPayQwik | GCI Lists</title>

    <link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/bootstrap.min.css'/>" >
  	<script type="text/javascript" src="/resources/js/new_js/jquery.min.js"></script>
  	<script type="text/javascript" src="/resources/js/bootstrap.min.js"></script>

    <link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/new_css/cloud-admin.css'/>" >
	<link rel="stylesheet" href="/resources/css/new_css/default.css">
	<link rel="stylesheet" type="text/css"  href="<c:url value='/resources/css/new_css/responsive.css'/>" >
	
	<link href="<c:url value='/resources/font-awesome/css/font-awesome.min.css'/>" rel="stylesheet">
	<!-- DATE RANGE PICKER -->
	<link rel="stylesheet" type="text/css" href="<c:url value='/resources/js/new_js/bootstrap-daterangepicker/daterangepicker-bs3.css'/>">
	<!-- UNIFORM -->
	<link rel="stylesheet" type="text/css" href="<c:url value='/resources/js/new_js/uniform/css/uniform.default.css'/>">
	<!-- FONTS -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
	
   
    <!-- Optional theme -->
    <link rel="stylesheet"
          href="<c:url value='/resources/css/bootstrap-theme.min.css'/>"
          type='text/css'>

    <link href="<c:url value="/resources/css/css_style.css"/>"
          rel='stylesheet' type='text/css'>
    <link rel="stylesheet"
          href="<c:url value='/resources/css/bootstrap.css'/>" type='text/css'>
<%--     <script src="<c:url value='/resources/js/jquery.js'/>"></script>
  --%>   <script src="<c:url value='/resources/js/bootstrap.js'/>"></script>

    <script type="text/javascript"
            src="<c:url value="/resources/js/userdetails.js"/>"></script>
    <script type="text/javascript"
            src="<c:url value="/resources/js/header.js"/>"></script>
    <link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'
          type="image/png" />


</head>

<body>
	<div class="se-pre-con"></div>
	<jsp:include page="/WEB-INF/jsp/User/Header.jsp" />
 <img src="/resources/images/gift_card.jpg" class="img-responsive" alt="Los Angeles">
	<!-----------------end navbar---------------------->

	<!------------- end main-------------------->
<section id="page">
	<div id="main-content">
			<div class="container">
			<div class="divide-20"></div>
			<div class="divide-20"></div>
			<ol class="breadcrumb">
			  <li><a href="${pageContext.request.contextPath}/User/Home">Home</a></li>
			  <li><a href="${pageContext.request.contextPath}/User/GciProducts/GetBrands">Gift Cards</a></li>
			   <li class="Active">Choose Card</li>
			</ol>
					
			
 <h3 style="margin-left: 400px; color:red;">${msg}</h3>
		<div class="row">
			<div class="pull-right"
				style="background: #17bcc8; padding: 10px; margin-right: 21px;">
				<%-- <a
					href="${pageContext.request.contextPath}/User/GciProducts/ShowCart"><i
					class="fa fa-shopping-basket fa-2x" style="color: white;">click</i></a>
 --%>
			</div>
		</div>
		<br>

		<%-- 		<a href="${pageContext.request.contextPath}/User/GciProducts/ShowCart">3<i class="fa fa-shopping-basket fa-2x"></i></a>
	 --%>
		<c:forEach items="${PriceList}" var="gl" varStatus="loopCounter">
			<div class="col-sm-3 col-xs-6">

				<a href="#"><img src="${gl.image}" style="width: 100%;" /></a>

				<h3 style="float: left;">
					<i class="fa fa-inr" aria-hidden="true"></i> <b>${gl.name}</b>
				</h3>
				<div class="pull-right">
					<form
										action="${pageContext.request.contextPath}/User/GciProducts/Order"
										method="post">


										<input type="hidden" value="${gl.hash}" name="brandHash">
										<input type="hidden" value="${gl.name}" name="denomination">
										<input type="hidden" value="${gl.type}"	name="productType"> 
<%-- 										<input type="hidden"value="${gl.quantity}" name="quantity">
 --%>										<input type="hidden" value="${gl.skuId}" name="skuId">
										

										<button type="submit" class="btn btn-warning"
											style="background: red; color: white; border-radius: 0; margin-top: 20%;">Proceed to pay</button>
									</form>
					<br> <br>
				</div>

			</div>
		</c:forEach>
		
		<%-- <button type="button" class="btn btn-warning"
						style="background: red; color: white; border-radius: 0; margin-top: 16px;"
						onclick="addcart('${gl.name}','${gl.type}','${gl.brandname}','${gl.hash}','${gl.image}','${gl.skuId}','${gl.id}')">Add
						to Cart</button> --%>

	</div>
</section>
	<script>
		// Add to Cart
		function addcart(amount, productType, brandName, brandHash, image,
				skuid, productid) {
			$.ajax({
				type : "POST",
				contentType : "application/json",
				url : "/User/GciProducts/AddCart",
				data : JSON.stringify({
					"amount" : "" + amount + "",
					"productType" : "" + productType + "",
					"brandName" : "" + brandName + "",
					"brandHash" : "" + brandHash + "",
					"imagepath" : "" + image + "",
					"skuId" : "" + skuid + "",
					"productid" : "" + productid + "",
				}),
				success : function(response) {
					 alert("Yur Product has been successfully addedd in your cart  .Please  click on cart icon");
					 window.location = "BrandDenominations";
					

				}
			});

		}

		/* //showing cart item
		
		 function showcart()

		 {
		
		
		 $.ajax({
		 type : "POST",
		 contentType : "application/json",
		 url : "/User/GciProducts/ShowCarts",
		 data : JSON.stringify({
		 "productType" :"" +"ttt" +"",
		
		 }), 
		 success : function(response) 
		 {
		 window.location = "ShowCart";
		
		 }
		 });
		
		 }
		 */
	</script>


	<!---end row-->
	<!----end container-->
	<jsp:include page="/WEB-INF/jsp/User/Footer.jsp" />
	<script src="http://code.jquery.com/jquery-2.2.1.min.js"></script>
	<script src="<c:url value='/resources/js/bootstrap.js'/>"></script>

</body>
</html>
