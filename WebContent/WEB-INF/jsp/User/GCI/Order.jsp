<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html>
<html>
<head>
   <meta charset="utf-8">
    <sec:csrfMetaTags />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>VPayQwik | GCI Order</title>

    <link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/bootstrap.min.css'/>" >
  	<script type="text/javascript" src="/resources/js/new_js/jquery.min.js"></script>
  	<script type="text/javascript" src="/resources/js/bootstrap.min.js"></script>

    <link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/new_css/cloud-admin.css'/>" >
	<link rel="stylesheet" href="/resources/css/new_css/default.css">
	<link rel="stylesheet" type="text/css"  href="<c:url value='/resources/css/new_css/responsive.css'/>" >
	
	<link href="<c:url value='/resources/font-awesome/css/font-awesome.min.css'/>" rel="stylesheet">
	<!-- DATE RANGE PICKER -->
	<link rel="stylesheet" type="text/css" href="<c:url value='/resources/js/new_js/bootstrap-daterangepicker/daterangepicker-bs3.css'/>">
	<!-- UNIFORM -->
	<link rel="stylesheet" type="text/css" href="<c:url value='/resources/js/new_js/uniform/css/uniform.default.css'/>">
	<!-- FONTS -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
	
   
    <!-- Optional theme -->
    <link rel="stylesheet"
          href="<c:url value='/resources/css/bootstrap-theme.min.css'/>"
          type='text/css'>

    <link href="<c:url value="/resources/css/css_style.css"/>"
          rel='stylesheet' type='text/css'>
    <link rel="stylesheet"
          href="<c:url value='/resources/css/bootstrap.css'/>" type='text/css'>
    <script src="<c:url value='/resources/js/jquery.js'/>"></script>
    <script src="<c:url value='/resources/js/bootstrap.js'/>"></script>

    <script type="text/javascript"
            src="<c:url value="/resources/js/userdetails.js"/>"></script>
    <script type="text/javascript"
            src="<c:url value="/resources/js/header.js"/>"></script>
    <link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'
          type="image/png" />

<style>
.no-js #loader {
	display: none;
}

.js #loader {
	display: block;
	position: absolute;
	left: 100px;
	top: 0;
}

.se-pre-con {
	position: fixed;
	left: 0px;
	top: 0px;
	width: 100%;
	height: 100%;
	z-index: 9999;
	background: url(/images/pq_large.gif) center no-repeat #fff;
}
</style>
<script src="<c:url value='/resources/js/modernizr.js'/>"></script>

<script type="text/javascript">
	$(window).load(function() {
		$(".se-pre-con").fadeOut("slow");
	});
</script>

</head>

<body>
	<div class="se-pre-con"></div>
	<jsp:include page="/WEB-INF/jsp/User/Header.jsp" />
 <img src="/resources/images/gift_card.jpg" class="img-responsive" alt="Los Angeles">
	<!-----------------end navbar---------------------->
	
	<script type="text/javascript">
	$(document).ready(function(){
		$("#myModal").modal('show');
	});
</script>
	



	<!-- PAGE -->
	<section id="page">
		<div id="main-content">
			<div class="container" style="height: 639px; max-height: 639px; overflow-x: hidden; overflow-y: auto;">
				<div class="divide-20"></div>
		       <ol class="breadcrumb">
				 <li><a href="${pageContext.request.contextPath}/User/Home">Home</a></li>
			  <li><a href="${pageContext.request.contextPath}/User/GciProducts/GetBrands">Gift Cards</a></li>
			   <li><a href="${pageContext.request.contextPath}/User/GciProducts/BrandDenominations">Choose Card</a></li>
				  <li class="active">Billing</li>
				</ol>
			
				<div class="divide-20"></div>
				<div class="row">
				 	<div class="col-md-5">
				   		<div class="panel panel-default" style="">
									  <div class="panel-body">
						<center>
								<form action="${pageContext.request.contextPath}/User/GciProducts/AddOrder" method="post" style="margin-top: 30px;">
									<input type="text" placeholder="Name" name="billingName" required id="n">
									<input type="text" placeholder="Email*" onkeyup="getbusseatandamount()" name="billingEmail"required id="e">
 									<!-- <input type="email" placeholder="Enter your email"onkeyup="getbusseatandamount()" > -->
                                            
									<span class="error" style="padding:0px" id="error_usergender_name"></span>

									<input type="text" placeholder="Address"name="billingAddressLine1"required id="a">
									<input type="text" placeholder="City "name="billingCity" required id="c">
									<input type="text" placeholder="Country"name="billingCountry" required id="co">
									<input type="text" placeholder="State "name="billingState" required id="s">
									<input type="text" placeholder="Personal Message *"name="clientOrderId" required id="cid">
								
				           <input type="hidden" value="${brandHash}" name="brandHash">
				          <input type="hidden" value="${amount}" name="denomination">
				           <input type="hidden" value="${productType}" name="productType">
				             <input type="hidden" value="${quantity}" name="quantity"> 
				             <input type="hidden" value="${productId}" name="quantity">
				             
				       <input type="submit"   style="width: 87%; color:white;background:red; border-bottom: aliceblue; id="myModalcsds" />
	</form>
			 <span style=" color:red;"> ${msg}</span><br>
							
							</div><br>
							
						</div>					
				</div>
						</center>
				<div class="col-md-5 hidden-xs" style="margin-top: -12px;">
				<div class="slider" id="slider"
					style="margin-right: -15px; margin-left: -15px;  margin-top: 15px;"">
					<div class="carousel slide hidden-xs" data-ride="carousel"
						id="mycarousel">
						<ol class="carousel-indicators">
							<li class="active" data-slide-to="0" data-target="#mycarousel"></li>
							<li data-slide-to="1" data-target="#mycarousel"></li>
						</ol>

						<div class="carousel-inner">

							<div class="item active" id="slide1">

								<img src='<c:url value="/resources/images/gift2.jpg"/>' />
							</div>
							<!---end item---->

							<div class="item">
								<img src='<c:url value="/resources/images/gift2.jpg"/>' />
							</div>

							<!---end item---->
						</div>
						<!--end carousel inner------>

					</div>

					<!---end caeousel slider---->
				</div>
				<!---end slider----->
			</div>
					
					
				</div>
			</div>	
		</div>
	<!-- Modal  -->

	

	
<script>
 $(document).ready(function(){
  var date_input=$('input[name="orderDate"]');
  var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
  date_input.datepicker({
	  minDate: 0,
   format: 'dd/mm/yyyy',
   container: container,
   todayHighlight: true,
   autoclose: true,
  })
 })
</script>
	
	
	<script>
	
	function getbusseatandamount()
{
	
	var l=document.getElementById("e").value;
      var atpos = l.indexOf("@");
      var dotpos = l.lastIndexOf(".");
     if(l == "" || atpos<1 || dotpos<atpos+2 )
    {
      
      $("#error_usergender_name").html("Please enter Valid Email Address");
      return false;
    }
     else
    	 {
    	 $("#error_usergender_name").html("");
    	 }
}
    
    
    </script>

	<!---end row-->
	<!----end container-->
	<jsp:include page="/WEB-INF/jsp/User/Footer.jsp" />
	<script src="http://code.jquery.com/jquery-2.2.1.min.js"></script>
	<script src="<c:url value='/resources/js/bootstrap.js'/>"></script>
</html>

