<%@ page contentType="text/html; charset=utf-8" language="java"	errorPage=""%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec"	uri="http://www.springframework.org/security/tags"%>
<html lang="en">
<head>
<meta charset="utf-8">
<sec:csrfMetaTags />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>VPayQwik | GCI Lists</title>
<link rel="stylesheet"
	href="<c:url value="/resources/css/font-awesome.min.css"/>">
<link href='<c:url value='/resources/css/font-family.css'/>'
	rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="<c:url value='/resources/css/font-awesome.css'/>" type='text/css'>

<!-- Optional theme -->
<link rel="stylesheet"
	href="<c:url value='/resources/css/bootstrap-theme.min.css'/>"
	type='text/css'>

<link href="<c:url value="/resources/css/css_style.css"/>"
	rel='stylesheet' type='text/css'>
<link rel="stylesheet"
	href="<c:url value='/resources/css/bootstrap.css'/>" type='text/css'>
<script src="<c:url value='/resources/js/jquery.js'/>"></script>
<script src="<c:url value='/resources/js/bootstrap.min.js'/>"></script>

<script type="text/javascript"
	src="<c:url value="/resources/js/userdetails.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/header.js"/>"></script>
<link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'
	type="image/png" />
<link href='<c:url value="/resources/css/css_style.css"/>'
	rel='stylesheet' type='text/css'>
<style>
.no-js #loader {
	display: none;
}

.js #loader {
	display: block;
	position: absolute;
	left: 100px;
	top: 0;
}

.se-pre-con {
	position: fixed;
	left: 0px;
	top: 0px;
	width: 100%;
	height: 100%;
	z-index: 9999;
	background: url(/images/pq_large.gif) center no-repeat #fff;
}
</style>
<script src="<c:url value='/resources/js/modernizr.js'/>"></script>

<script type="text/javascript">
	$(window).load(function() {
		$(".se-pre-con").fadeOut("slow");
	});
</script>

</head>

<body>
	<div class="se-pre-con"></div>
	<jsp:include page="/WEB-INF/jsp/User/Header.jsp" />
	<jsp:include page="/WEB-INF/jsp/User/Menu.jsp" />

	<!-----------------end navbar---------------------->

	<!------------- end main-------------------->

                
	<div class="background"></div>

	<div class="container" id="aboutus" style="margin-top: -80px;">

		<div class="container" style="margin-left: -12px;">
			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td><b>Item</b></td>
							<td class="price"><b>Product Name</b></td>
							<td class="price"><b>Product Type</b></td>
							<td class="price"><b>Amount</b></td>
							<!-- <td class="quantity">Quantity</td> -->
							<td class="total"><b>Total</b></td>
							<!-- <td class="total"><b>Check Out</b></td> -->
							<td></td>
						</tr>
					</thead>
					<tbody>
					           <h3 style="margin-left: 400px; color:red;">${msg}</h3>
					
						<c:forEach items="${ShowCart}" var="gl" varStatus="loopCounter">
							<tr>
								<td class="cart_product" style="width: 30%;"><a href="#"><img
										src="${gl.imagepath}" style="width: 80%;" /></td>
								<td class="cart_description">
									<h4 style="margin-top: 33%;">
										<b>${gl.brandName}</b>
									</h4>
								</td>
								<td class="cart_price">
									<p style="margin-top: 33%;">${gl.productType}</p>
								</td>
								<td class="cart_price">
									<p style="margin-top: 40%;">
										<i class="fa fa-inr" aria-hidden="true"></i> <b>${gl.amount}</b>
									</p>
								</td>
								<td class="cart_total">
									<h3 style="margin-top: 33%;">
										<i class="fa fa-inr" aria-hidden="true"></i> <b>${gl.totalamount}</b>
									</h3>
								</td>
								
								
								<td class="cart_delete">
									<!-- 	<a class="cart_quantity_delete" href="############"><i class="fa fa-times"></i></a>&nbsp&nbsp&nbsp
					 --> <!-- <button type="button" class="btn btn-warning" style="background: red; color: white; border-radius: 0; margin-top: 20%;"
							onclick="proceed()">Proceed</button> -->


									<form
										action="${pageContext.request.contextPath}/User/GciProducts/Remove"
										method="post">

										<input type="hidden" value="${gl.skuId}" name="skuId">
										<input type="hidden" value="${gl.productId}" name="productId">
										<input type="hidden" value="${gl.amount}" name="amount">
										

										<!-- <button type="button">dsksd</button> -->

										<input type="submit"value="RemoveItem"
											style="background: red; color: white; border-radius: 0; margin-top: 20%; width: 122px;height: 35px;" />
									</form>

									<form
										action="${pageContext.request.contextPath}/User/GciProducts/Order"
										method="post">


										<input type="hidden" value="${gl.brandHash}" name="brandHash">
										<input type="hidden" value="${gl.amount}" name="denomination">
										<input type="hidden" value="${gl.productType}"	name="productType"> 
										<input type="hidden"value="${gl.quantity}" name="quantity">
										<input type="hidden" value="${gl.skuId}" name="skuId">
										

										<button type="submit" class="btn btn-warning"
											style="background: red; color: white; border-radius: 0; margin-top: 20%;">Proceed to pay</button>
									</form>
								</td>
							</tr>
						</c:forEach>

					</tbody>
				</table>
			</div>
		</div>
	</div>

	<jsp:include page="/WEB-INF/jsp/User/Footer.jsp" />

	<script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.scrollUp.min.js"></script>
	<script src="js/jquery.prettyPhoto.js"></script>
	<script src="js/main.js"></script>
</body>
</html>


