<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" language="java"
         errorPage=""%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags"%>
<html lang="en">
<head>
    <meta charset="utf-8">
    <sec:csrfMetaTags />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	
  <link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/bootstrap.min.css'/>" >
  <script type="text/javascript" src="/resources/js/new_js/jquery.min.js"></script>
  <script type="text/javascript" src="/resources/js/bootstrap.min.js"></script>

    <title>VPayQwik | GCI Lists</title>
    <link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/new_css/cloud-admin.css'/>" >
	<link rel="stylesheet" href="/resources/css/new_css/default.css">
	<link rel="stylesheet" type="text/css"  href="<c:url value='/resources/css/new_css/responsive.css'/>" >
	
	<link href="<c:url value='/resources/font-awesome/css/font-awesome.min.css'/>" rel="stylesheet">
	<!-- DATE RANGE PICKER -->
	<link rel="stylesheet" type="text/css" href="<c:url value='/resources/js/new_js/bootstrap-daterangepicker/daterangepicker-bs3.css'/>">
	<!-- UNIFORM -->
	<link rel="stylesheet" type="text/css" href="<c:url value='/resources/js/new_js/uniform/css/uniform.default.css'/>">
	<!-- FONTS -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
	
   
    <!-- Optional theme -->
    <link rel="stylesheet"
          href="<c:url value='/resources/css/bootstrap-theme.min.css'/>"
          type='text/css'>

    <link href="<c:url value="/resources/css/css_style.css"/>"
          rel='stylesheet' type='text/css'>
    <link rel="stylesheet"
          href="<c:url value='/resources/css/bootstrap.css'/>" type='text/css'>
    <script src="<c:url value='/resources/js/jquery.js'/>"></script>
    <script src="<c:url value='/resources/js/bootstrap.js'/>"></script>

    <script type="text/javascript"
            src="<c:url value="/resources/js/userdetails.js"/>"></script>
    <script type="text/javascript"
            src="<c:url value="/resources/js/header.js"/>"></script>
    <link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'
          type="image/png" />

    <style>
        .no-js #loader {
            display: none;
        }

        .js #loader {
            display: block;
            position: absolute;
            left: 100px;
            top: 0;
        }

        .se-pre-con {
            position: fixed;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: url(/images/pq_large.gif) center no-repeat #fff;
        }
    </style>
    <script src="<c:url value='/resources/js/modernizr.js'/>"></script>

    <script type="text/javascript">
        $(window).load(function() {
            $(".se-pre-con").fadeOut("slow");
        });
    </script>


</head>

<body>
<div class="se-pre-con"></div>
<jsp:include page="/WEB-INF/jsp/User/Header.jsp" />
 <img src="/resources/images/gift_card.jpg" class="img-responsive" alt="Los Angeles">
<!-----------------end navbar---------------------->

<!------------- end main-------------------->

<section id="page">
		
		<div id="main-content">
			<div class="container">
			<div class="divide-20"></div>
			<div class="divide-20"></div>
			<ol class="breadcrumb">
			  <li><a href="${pageContext.request.contextPath}/User/Home">Home</a></li>
			  <li class="active">Gift Cards</li>
			  
			</ol>
			
				<div class="row">
					<div id="content" class="col-lg-12">
                       <c:forEach items="${Gcibrands}" var="gl" varStatus="loopCounter">
    <div class="col-sm-3">
        <div class="gci">
        <form action="/User/GciProducts/BrandDenominations" method="post">

            <input type="hidden" value="${gl.hash}" name="hash">

            <input type="hidden" value="${gl.image}" name="image">

            <input type="hidden" value="${gl.name}" name="name">



                <%-- <img src="${gl.image}" class="img-responsive" style="width: 100%;" />
               <input type="submit"> --%>

            <input type="image" src="${gl.image}" style="width: 100%; border-bottom: aliceblue;"/>

<div class="categoryratings-review" style="display: none;">
                    <p></p>
                </div>

                <!-- <span class="showcategoryratings">
                    <span class="showcategoryratings-text">T&Co.</span>
                </span> -->
                    <%--  <button type="button" class="btn btn-info btn-lg" onclick="termscondion('${gl.image}')">T&Co.</button> --%>

               <center> <button type="button" class="btn btn-warning"
                        style="background: red; color: white; border-radius: 0; margin-top: 16px;"
                        onclick="term('${gl.hash}')">Terms and Conditions
                </button></center>
        </form>


</div>
    </div>
</c:forEach>

<div id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">OK</button>
                <h4 class="modal-title">Your Gift Card Terms & Condition </h4>
            </div>
            <div class="modal-body">

                <div id="termsvalue"></div>
            </div>
        </div>
    </div>
</div>
				          </div>
					</div>
				</div>
			</div>
						
		<div class="footer-tools">
			<span class="go-top">
				<i class="fa fa-chevron-up"></i> Top
			</span>
		</div>
					
		<div class="footer_title">
			<div class="container">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12 foot_title" style="padding: 10px; background: #005cac; color: white; text-align: right; font-size: 15px;">
									<p>&copy; <a href="#">Msewa Software</a> Pvt Ltd. | 2017 All Rights Reserved.</p>
					</div>
				</div>
			</div>
		</div>
	</section>

<script>
    // Terms
    function term(hash) {
        var hash_key="hash";
        var default_hash="123456";
        var headers = {};
        headers[hash_key] = default_hash;
        $.ajax({
            type : "POST",
            contentType : "application/json",
            headers:headers,
            url : "/User/GciProducts/GetBrandsInPost",
            data : JSON.stringify({
                "hash" : "" + hash + "",
                /* "productType" : "" + productType + "",
                 "brandName" : "" + brandName + "",
                 "brandHash" : "" + brandHash + "",
                 "imagepath" : "" + image + "",
                 "skuId" : "" + skuid + "",
                 "productid" : "" + productid + "", */
            }),
            success : function(response) {
                // alert(response);
                document.getElementById("termsvalue").innerHTML = "";
                document.getElementById("termsvalue").innerHTML = ""+response;
                $("#myModal").modal();


            }
        });

    }

</script>

<script type="text/javascript" src="/resources/js/new_js/jquery-ui-1.10.3.custom.min.js"></script>
<script type="text/javascript" src="/resources/js/new_js/bootstrap.min.js"></script>

<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery.matchHeight/0.7.0/jquery.matchHeight-min.js'></script>
<script src="<c:url value='/resources/js/index.js'/>"></script>

<script>
    $('.box').matchHeight();
</script>

</body>
</html>

