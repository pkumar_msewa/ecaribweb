<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*"  isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
	<title>Aadhar Verify</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Latest compiled and minified CSS -->
<!-- 	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->

<link rel="stylesheet"
	href="<c:url value='/resources/css/font-awesome.min.css'/>" type='text/css'>
<link href='<c:url value='/resources/css/font-family.css'/>'
	  rel='stylesheet' type='text/css'>

	<!-- HEADER -->
	<header class="navbar clearfix" id="header" style="background-color:#fcd301;">
		<div class="container-fluid">
				<div class="navbar-brand">
					<!-- COMPANY LOGO -->
					<a href="/User/Home" >
						<img src="/resources/images/new_img/logo/logo.png" style="width:90px;margin-top:-14px;margin-left: -40px;" alt="VPayQwik Logo" class="img-responsive">
					</a>					
				</div>
				<ul class="nav navbar-nav pull-right" style="margin-top:12px;">				
					<!-- BEGIN USER LOGIN DROPDOWN -->
			     <li id="wallets">
                    <a href="#"><img src='<c:url value="/resources/images/main/wallet.png"/>' alt="wallet" style="margin-left:10px;width: 25px;margin-top: -7px;"> 
                	<i class="fa fa-dollar"></i> <span id="user_balance" style="color:white;"></Span></a>
                 </li>
					<li class="dropdown user pull-right" id="header-user">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<img  class="small_pic" src="<c:url value="/resources/images/spinner.gif"/>" class="img-circle"
							style="" width="40" height="40"/>
							<span class="username" id="display_first_last_name"></span>
							<i class="fa fa-angle-down"></i>
						</a>
						<ul class="dropdown-menu">
							<li><a href="<c:url value="/User/Home"/>"><i class="fa fa-user"></i> Dashboard</a></li>
							<li><a data-toggle="modal" data-target="#invite-frnds"><i class="fa fa-users"></i> Invite Friends</a></li>
							<%-- <li><a href="<c:url value="/User/Settings"/>"><i class="fa fa-cog"></i> Account Settings</a></li> --%>
							<!-- <li><a href="#"><i class="fa fa-eye"></i> Privacy Settings</a></li> -->
						<%-- 	<li><a href="${pageContext.request.contextPath}/User/Travel/Flight/MyTickets"><i class="fa fa-users"></i> Travel Tickets</a></li> --%>
							<!-- <li id="kyc_update_disable"><a data-toggle="modal" data-target="#updateKyc"><i class="fa fa-refresh"></i>Update KYC</a></li> -->
							<li><a href="<c:url value="/User/Logout"/>"><i class="fa fa-power-off"></i> Log Out</a></li>
						</ul>
					</li>
					<!-- END USER LOGIN DROPDOWN -->
				</ul>
				<!-- END TOP NAVIGATION MENU -->
		</div>
	</header>
	<!--/HEADER -->
	
                    <center>
						<!-- Modal for Invite Friends -->
						<div class="modal fade" id="invite-frnds" role="dialog">
							<div class="modal-dialog">

								<!-- Modal content-->
								<div class="modal-content rchrg_modal">
									<div class="modal-header dth_head">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<center>
											<h2 class="modal-title">
												<b>Invite Friends</b>
											</h2>
										</center>
									</div>
									<div class="modal-body">
										<div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<br> <br>
												<div class="form-group" style="margin-top: -46px;">
												<p id="error_ife" class="label label-danger" ></p>
				                                <p id="success_ife" class="label label-success"></p>
													<div class="col-md-12">
														<form method="post" action="#">
															<div class="group_1">
																<input type="text" name="mobileNo" id="ife_mobileNo" autocomplete="off" maxlength="10"
																	class="form-control numeric" style="margin-right: 50px;" maxlength="10" required="required" placeholder="Mobile Number">
															<p id="error_ife_mobileNo" class="error" style="color: red"></p>
															</div>

															<div class="group_1">
																<input type="text" name="receiversName" id="ife_receiver"
									                         required="required" class="form-control" placeholder="Invitee's Name">
															<p id="error_ife_receivername" class="error" style="color: red"></p>
															</div>
															<button type="button" class="btn" id="ife_submit"
																style="background: #8cc84e; color: #FFFFFF;">Invite</button>
														</form>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

							</div>

						</div>
						<!-- Modal for Invite Friends -->
						
<!-- 						Model for Update KYC -->

	<div class="modal fade" id="updateKyc" role="dialog">
		<div class="modal-dialog modal-sm">
			<!-- Modal content-->
			<div class="modal-content rchrg_modal">
				<div class="modal-header dth_head">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<center>
						<h2 class="modal-title">
							<b>Update Kyc</b>
						</h2>
					</center>
				</div>
				<div class="modal-body">

					<div class="row">
						<div class="col-md-12 text-left">
							<medium>
							<em>As per RBI guidelines, it is mandatory to update your
								KYC details. To ensure continued usage of your wallet, kindly
								provide the below details.</em></medium>
						</div>
					</div>
					<br />
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<br> <br>
							<div class="form-group" style="margin-top: -46px;">
								<p id="error_kyc" class="label label-danger"></p>
								<p id="success_kyc" class="label label-success"></p>
								<div class="row">
								<div class="col-md-12">
									<form method="post" action="#">
										<div class="group_1">
											<input type="text" name="aadharNo" id="aadharId"
												autocomplete="off" onkeypress="removeErrorMesg();"
												class="form-control numeric" style="margin-right: 50px;"
												maxlength="12" required="required"
												placeholder="Enter Aadhar Number">
											<p id="error_aadhar" class="error" style="color: red"></p>
										</div>
									</form>
								</div></div>
								<div class="row">
									<div class="col-md-2" align="left">
										<input type="checkbox" id="check">
									</div>
									<div class="col-md-10" align="left">
										<p style="margin-left: -30px;">
											By clicking this, I hereby accept the <a href="#"
												id="termcondi">terms and conditions</a>
										</p>
									</div>
								</div>
								<div class="col-md-12" align="center">
									<button type="button" class="btn" id="submit_update"
										style="background: #8cc84e; color: #FFFFFF;">Update</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>

	</div>

	<!-- 						Model OTP verification -->


						 <div class="modal fade" id="updateOtp" role="dialog">
							<div class="modal-dialog modal-sm">
								<!-- Modal content-->
								<div class="modal-content rchrg_modal">
									<div class="modal-header dth_head">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<center>
											<h2 class="modal-title">
												<b>OTP Verification</b>
											</h2>
										</center>
									</div>
									<div class="modal-body">
										<div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<br> <br>
												<div class="form-group" style="margin-top: -46px;">
												<p id="error_otp" class="label label-danger" ></p>
				                                <p id="success_otp" class="label label-success"></p>
													<div class="col-md-12">
														<form method="post" action="#">
															<div class="group_1">
															<input type="hidden" name="otpReqId" value="" id="otpReqId"/>
															<input type="hidden" name="aadharNumber" value="" id="aadharNo"/>
																<input type="text" name="otpNo" id="aadhar_otp" autocomplete="off" 
																	class="form-control numeric" style="margin-right: 50px;" maxlength="6" required="required" 
																	placeholder="Enter Otp" onkeypress="removeErrorMesg();" >
															<p id="error_aadhar_otp" class="error" style="color: red"></p>
															</div>
															<button type="button" class="btn" id="submit_otp"
																style="background: #8cc84e; color: #FFFFFF;">Verify</button>
														</form>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

							</div>

						</div>
						
<!-- 						USER Details -->




	<div class="modal fade" id="showDetails" role="dialog" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog modal-sm">
			<!-- Modal content-->
			<div class="modal-content rchrg_modal">
				<div class="modal-header dth_head">
					<button type="button" class="close" onclick="javascript:window.location.reload()"  data-dismiss="modal">&times;</button>
					<div align="center">
						<h2 class="modal-title">
							<b>Aadhar Details</b>
						</h2>
					</div>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<br> <br>
							<div class="form-group" style="margin-top: -46px;">
								<p id="error_otp" class="label label-danger"></p>
								<p id="success_otp" class="label label-success"></p>
								<div class="col-md-12">
									<div class="col-md-12">
										<div class="col-md-3">
											<label><h3>Name:</h3></label>
										</div>
										<div class="col-md-9">
											<p id="cNameId" style="margin-top: 20px;"></p>
										</div>
									</div><br/>
									<div class="col-md-12">

										<div class="col-md-3">
											<label><h3 style="margin-left: -25px;">Address:</h3></label>
										</div>
										<div class="col-md-9"style="margin-top: 18px;">
											<p id="cAddId"></p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>

	</div>

	<!-- 						Terms&condition -->

	<div id="termsModal" class="AadharModalT modal fade" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class=" modal-content">
				<div class="modal-header ">
					<button type="button" class="close" data-dismiss="modal"
						onclick="openKycModel();">&times;</button>
					<h4 style="color: black">
						<b>Terms and Conditions</b>
					</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<h5>I hereby understand/authorize VPayQwik to</h5>
							<p class="text-justify">Use my Aadhaar details for VPayqwik
								wallet and authenticate my identity through the Aadhaar
								Authentication system (Aadhaar based e-KYC services of UIDAI) in
								accordance with the provisions of the Aadhaar (Targeted Delivery
								of Financial and other Subsidies, Benefits and Services) Act
								2016 and the allied rules and regulations notified thereunder.</p>
							<p class="text-justify">Use Aadhaar number and OTP for
								authenticating my identity through the Aadhaar Authentication
								system for obtaining my e-KYC through Aadhaar based e-KYC
								services of UIDAI.</p>
							<p class="text-justify">I understand that the Aadhaar details
								(physical and / or digital, as the case maybe) submitted for
								availing services under wallet will be maintained in wallet till
								the time the account is not inactive in wallet or the timeframe
								decided by RBI, the regulator of wallet, whichever is later.</p>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</center>
</html>
					
					
					
					
		<script>
			function removeErrorMesg(){
				console.log("errormsg");
				$('#error_aadhar_otp').html('<p id="error_aadhar_otp" class="error" style="color: red"></p>');
				$('#error_aadhar').html('<p id="error_aadhar" class="error" style="color: red"></p>');
			}
			
			$(document).ready(function(){
				$("#submit_update").addClass("disabled");
			});
			
			$("#check").click(function(){
				if($("#check").prop('checked') == true){
					$("#submit_update").removeClass("disabled");
				}else{
					$("#submit_update").addClass("disabled");
				}
			});
			
			$("#termcondi").click(function(){
				$("#termsModal").modal('show');
				$("#updateKyc").modal('hide');
			});
			function openKycModel(){
				$("#termsModal").modal('hide');
				$("#updateKyc").modal('show');
			}
		</script>
		<head>
	<style>
		.AadharModalT .modal-content {
			border-radius: 0;
		}
		.AadharModalT h5 {
			font-size: 15px;
			font-weight: 600;
		}
		.AadharModalT p {
			font-size: 12px;
			font-weight: 500;
		}
	</style>
</head>
					