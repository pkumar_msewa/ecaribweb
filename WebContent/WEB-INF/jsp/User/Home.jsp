<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*" errorPage="" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<sec:csrfMetaTags />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="X-Frame-Options" content="deny">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>eCarib | Dashboard</title>

<link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'
	type="image/png" />
<link rel="stylesheet" href="/resources/css/new_css/cloud-admin.css">
<link rel="stylesheet" href="/resources/css/font-awesome.min.css">
<link rel="stylesheet" href="/resources/css/new_css/default.css">
<link rel="stylesheet" href="/resources/css/new_css/responsive.css">
<link rel="stylesheet"
	href="/resources/css/new_css/uniform.default.min.css">
<link rel='stylesheet' href="/resources/css/css_style.css">
<!-- <link
	href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700'
	rel='stylesheet' type='text/css'> -->

<!-- <script
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> -->
	<script type="text/javascript"
	src="/resources/js/new_js/jquery.min.js"></script>
	
	<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>
<script type="text/javascript"
	src="/resources/js/new_js/jquery.easypiechart.min.js"></script>
<script type="text/javascript"
	src="/resources/js/new_js/jquery.sparkline.min.js"></script>
	
<script src="/resources/js/new_js/script.js"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/aj.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/topup.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/header.js"/>"></script>
 <script type="text/javascript"
	src="<c:url value="/resources/js/userdetails.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/sendmoney.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/billpay.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/invitefriends.js"/>"></script>

<script src="${pageContext.request.contextPath}/resources/js/Admin/jquery.twbsPagination.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/Admin/jquery.twbsPagination.min.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/datepicker.css">
	
<script src="${pageContext.request.contextPath}/resources/js/datepicker.js"></script>

	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/new_css/datetimepicker.css">
	<script src="${pageContext.request.contextPath}/resources/js/new_js/datetimepicker.js"></script>
	
<script type="text/javascript">
	$(window).load(function() {
		var errorMsg = document.getElementById("errorMsg").value;
		$("#voucherLoadMoney").removeAttr("disabled");
		
		$("#voucherNum").keyup(function(){
		    this.value = this.value.replace(/[^0-9a-zA-z]/g,'');
		});
		
		console.log("Alert ID :: " + errorMsg);
		if (!errorMsg.length == 0) {
			$("#common_error").modal("show");
			$("#common_success").html(errorMsg);
			var timeout = setTimeout(function() {
				$("#common_error").modal("hide");
				$("#errorMsg").val("");
			}, 300000);
		}

		var successMsg = document.getElementById("successMsg").value;
		console.log("successMsg :: " + successMsg);
		if (!successMsg.length == 0) {
			$("#common_success_true").modal("show");
			$("#common_success_msg").html(successMsg);
			var timeout = setTimeout(function() {
				$("#common_success_true").modal("hide");
				$("#successMsg").val("");
			}, 200000);
		}
	});
</script>
	
<script type="text/javascript">
$(function() {
	var dt = new Date();
	  dt.setFullYear(new Date().getFullYear() - 0);
	  $("#toDate").datepicker({
	    viewMode: "years",
	    endDate: dt,
	    autoclose: 1,
	    format:"yyyy-mm-dd"
 	  });
	  
	  $("#fromDate").datepicker({
		    viewMode: "years",
		    endDate: dt,
		    autoclose: 1,
		    format:"yyyy-mm-dd"
		  });
	  
	  
	 });
</script>
<script>
$(function() {
	 var dt = new Date();
	  dt.setFullYear(new Date().getFullYear() - 0);
	  $("#toDate1").datepicker({
	    viewMode: "years",
	    endDate: dt,
	    autoclose: 1,
	    format:"yyyy-mm-dd"
	  });
	  
	  $("#fromDate1").datepicker({
		    viewMode: "years",
		    endDate: dt,
		    autoclose: 1,
		    format:"yyyy-mm-dd"
		  });
	 });
	</script>
	
		<script>
			$(function() {
				$( "#Insurance_Date" ).datepicker({
					format:"yyyy-mm-dd"
				});
			});
		</script>

		<!-- <script>
			$(function() {
				$( "#upi_dt" ).datetimepicker({
					 maxDate: '0' ,
					 startDate: "today",
					format:"dd-mm-yyyy hh:ii",
					autoclose: 1
				});
			});
		</script> -->

		<script>

	$(document).ready(function(){
	 
	var date = new Date();
    var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
	var date_input=$('.dt-field');
	var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
	date_input.datetimepicker({
	   format: 'dd-mm-yyyy hh:ii',
	   container: container,
	   maxDate: '0',
	   startDate: today,
	   todayHighlight: true,
	   autoclose: true,
	})
	 })

</script>

<style>
.success-popup {
	transition: .3s ease all;
}

.success_wrap {
	border-radius: 0;
}
</style>

<style>
.label-success {
	background-color: #5cb85c;
}

.label-default {
	background-color: #777;
}

.label-warning {
	background-color: #f0ad4e;
}
.dt-field {
	cursor: pointer !important;
}
</style>

<script>
function validate(){
	var voucherNo = $("#voucherNum").val();
	console.log("voucherNo=="+voucherNo);
	valid = true;
	if(voucherNo.length<=0){
		$("#error_voucherNumber").html("Enter voucher number");
		valid = false;
	}
	console.log("voucherNo==-"+voucherNo);
	if(valid==true){
		$("#voucherLoadMoney").attr("disabled","disabled");
	}
	return valid;
}

function transactionListFilter(fromDate1,toDate1,page) {
    var spinnerUrl = "<img src='/resources/images/spinner.gif' height='20' width='20'>"
	var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");
	var csrfHeader = $("meta[name='_csrf_header']").attr("content");
	var csrfToken = $("meta[name='_csrf']").attr("content");
	var hash_key = "hash";
	var default_hash = "123456";
	var headers = {};
	headers[hash_key] = default_hash;
	headers[csrfHeader] = csrfToken;
	var paging = page;
	var size = '10';
	var fromDate = $("#"+fromDate1).val();
	var toDate = $("#"+toDate1).val();
	$.ajax({
		type : "POST",
		headers : headers,
		contentType : "application/json",
		url : "/User/Receipts/ReceiptAjaxFilter",
		dataType : 'json',
		contentType : "application/json",
		data : JSON.stringify({
			"fromDate" : "" + fromDate+ "",
			"page": "" + page +"", 
			"size": "" + 10+"",
			"toDate" : ""+toDate+""
		}),
		success:function(data,textStatus, jqXHR){
			if(data.code.includes("S00")){
			$("#Date_picker").modal('hide');
			renderReceiptTableForFilter(data.info);
			createfilterPaging(data,fromDate1,toDate1);
			}
			else if(data.code.includes("F03")){
				location.reload();
			}else if(data.code.includes("F00")){
				$("#transaction_filter_msg").html(data.message);
			}
 		}
	});
}
function renderReceiptTableForFilter(data) {
	var $table= $('#editedtable');
	if(($table).find('tbody').length) $table.find('tbody').empty();
	$tbody = $table.append('<tbody></tbody>');
	if(data){
		 document.getElementById("editedtable").innerHTML = "";
	
		
		for(var i=0; i < data.length; i++) {
			$tr = $('<tr></tr>');
			var statusClass = '';
			switch(data[i].status) {
				case 'Initiated' :  statusClass = 'label label-info'; break;
				case 'Success' : statusClass = 'label label-success'; break;
				case 'Reversed' : statusClass = 'label label-warning'; break;
				case 'Processing' : statusClass = 'label label-info';break;
				case 'Failed' : statusClass = 'label label-danger'; break;
				case 'Booked' : statusClass = 'label label-success'; break;
				case 'Refunded' : statusClass = 'label label-warning'; break;
				case 'Sent' : statusClass = 'label label-info'; break;
			}
			$tr.append($('<td></td>').text(data[i].date));
			$tr.append($('<td></td>').text(data[i].description));
			$tr.append($('<td></td>').text(data[i].transactionRefNo));
			$tr.append($('<td></td>').text(data[i].amount));
			$tr.append($('<td></td>').append($('<span style="font-size:13px;display:block;max-width:80px;margin-right:20px"></span>').text(data[i].status).addClass(statusClass)));
			$tbody.append($tr);
		}
	}
}
function createfilterPaging(data,fromDate,toDate)
{
	// document.getElementById("").innerHTML = "";
		

    
document.getElementById("rcptPagination").style.display = "none"
 $('#paginationfile').twbsPagination({
	 totalPages:data.totalPages,
	 visiblePages: 5,
	 onPageClick: function (event, page) {
		 transactionListFilter(fromDate,toDate,page-1);
     }
		
 });
}
</script>

<script>
	function getCreditAndDebit() {
		var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");
		var csrfHeader = $("meta[name='_csrf_header']").attr("content");
		var csrfToken = $("meta[name='_csrf']").attr("content");
		var hash_key = "hash";
		var default_hash = "123456";
		var headers = {};
		headers[hash_key] = default_hash;
		headers[csrfHeader] = csrfToken;
		$.ajax({
			url : "/Api/v1/User/Windows/en/User/UpdateCreditAndDebit",
			type : "GET",
			headers : headers,
			datatype : 'json',
			contentType : "application/json",
			success : function(response) {
				var u = response;
				if (u.code.includes("S00")) {
				$("#account_credit").html(u.credit);
				$("#account_debit").html(u.debit);
				}else if(u.code.includes("F03")) {
					location.reload();
				}
			}
		});
	}
	function transactionlist(value) {
		var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");
		var csrfHeader = $("meta[name='_csrf_header']").attr("content");
		var csrfToken = $("meta[name='_csrf']").attr("content");
		var hash_key = "hash";
		var default_hash = "123456";
		var headers = {};
		headers[hash_key] = default_hash;
		headers[csrfHeader] = csrfToken;
		var paging = value;
		var size = '20';
		$.ajax({
			type : "GET",
			headers : headers,
			contentType : 'application/json;charset=UTF-8',
			url : "/User/Receipts/ReceiptAjax",
		 data: "page=" + value + "&size=" + 10,
			dataType : 'json',
			contentType : "application/json",
			success:function(data,textStatus, jqXHR){
				if(data.code.includes("S00")){
					createPaging(data);
					renderReceiptTable(data.info);
				}else if(data.code.includes("F03")){
					location.reload();
				}
	 		}
		});
	}
	function renderReceiptTable(data) {
		var $table= $('#editedtable');
		if(($table).find('tbody').length) $table.find('tbody').empty();
		$tbody = $table.append('<tbody></tbody>');
		if(data){
			for(var i=0; i < data.length; i++) {
				$tr = $('<tr></tr>');
				var statusClass = '';
				switch(data[i].status) {
				case 'Initiated' :  statusClass = 'label label-info'; break;
				case 'Success' : statusClass = 'label label-success'; break;
				case 'Reversed' : statusClass = 'label label-warning'; break;
				case 'Processing' : statusClass = 'label label-info';break;
				case 'Failed' : statusClass = 'label label-danger'; break;
				case 'Booked' : statusClass = 'label label-success'; break;
				case 'Refunded' : statusClass = 'label label-warning'; break;
				case 'Sent' : statusClass = 'label label-info'; break;
				}
				$tr.append($('<td></td>').text(data[i].date));
				$tr.append($('<td></td>').text(data[i].description));
				$tr.append($('<td></td>').text(data[i].transactionRefNo));
				$tr.append($('<td></td>').text(data[i].amount));
				$tr.append($('<td></td>').append($('<span style="font-size:13px;display:block;max-width:80px;margin-right:20px"></span>').text(data[i].status).addClass(statusClass)));
				$tbody.append($tr);
			}
		}
	}
	function createPaging(data) {
		
		
	 $('#rcptPagination').twbsPagination({
		 totalPages:data.totalPages,
		 visiblePages: 5,
		 onPageClick: function (event, page) 
		 {
			  transactionlist(page-1);
		
         }
    		
	 });
	}
		 
	function createReceiptPagination(data) {
		if(data){
			this.receiptArray = data;
			this.dataPerPage = 10;
			this.totalPageNum = Math.round(data.length / dataPerPage );
			var pageNav = $('#rcptPagination');
			pageNav.append('<li><a href="#"><span>&laquo;</span></a></li>');//.on('click',nextPage));
			for(i=1; i <= totalPageNum; i++) {
				$li = $('<li></li>').append('<a href="#">'+i+'</a>');//.on('click',paginationEventHanlder);
				pageNav.append($li);
			}
			pageNav.append('<li><a href="#"><span>&raquo;</span></a></li>');//.on('click', previousPage));
			pageNav.find('li').eq(1).addClass('active');
			pageNav.on('click','li', paginationEventHanlder);
		} 
		
			 
	}
	function renderPageData(index) {
		this.index = index ? index : 1;
		this.firstIndex	= (this.index - 1) * this.dataPerPage;
		this.lastIndex = (this.index * this.dataPerPage) -1;
		var dataToRender = this.receiptArray.slice(this.firstIndex, this.lastIndex);
 		renderReceiptTable(dataToRender);
		 
	} 
	function paginationEventHanlder(event) {
		event.preventDefault();
		$this = $(this);
		totalPageNum = $this.siblings().length - 1;
		$numList = $('#rcptPagination li');
		var index = $numList.index(this);
		if(index == 0) {
			index = $this.siblings('.active').index() - 1;
			console.log(index);
		} else if(index == totalPageNum + 1) {
			index = $this.siblings('.active').index() + 1;
		}
		
		if(index <= 0 || index > totalPageNum) return;
		$this.siblings().removeClass('active');
		$numList.eq(index).addClass('active');
		renderPageData(index);
	}
	
	function start(){
		getCreditAndDebit();
	    transactionlist(0);
	}
	
	$(document).ready(function(){
		start();
	});

	function enabale(id)
	{$("#"+id).hide();}
	
</script>

<!-- ================== UPI Payment Script ============================== -->
					<script>
						$(function() {
						    $('input[name="useVnetWeb"]').on('click', function() {
						    	console.log("UPI Clicked : :  " + $(this).val())
						        if ($(this).val() == 'Upi') {
						            $('#Upi_txt').show();
						            $('#Upi_dt').show();
						        }
						        else {
						            $('#Upi_txt').hide();
						            $('#Upi_dt').hide();
						        }
						    });
						});
					</script>

</head>
<body style="overflow-x: hidden;">

	<div class="se-pre-con"></div>
	<jsp:include page="/WEB-INF/jsp/User/Header.jsp" />
<input type="hidden" name="successMsg" id="successMsg" value="${msg}">
<input type="hidden" name="errorMsg" id="errorMsg" value="${error}">

<input type="hidden" name="flightSusMsg" id="flightSusMsg" value="${flightMsg}">
<input type="hidden" name="flightflMsg" id="flightflMsg" value="${flighterrMsg}">


	<section id="page">
		<div id="main-content">
			<div class="container">
				<!-- <div class="divide-20"></div> -->
				<div class="row">
					<div id="content" class="col-lg-12">
						<div class="row">
							<div class="col-md-12">
								<!-- BOX -->
								<div class="box border">
									<div class="box-body">
										<div class="tabbable header-tabs user-profile">
											<div class="tab-content">
												<div class="tab-pane fade in active" id="pro_overview">
													<div class="row">
														<jsp:include page="/WEB-INF/jsp/User/sidenev.jsp" />
														<div class="col-md-9">
															<div class="row">
																<div class="col-md-7 profile-details">
																	<div class="divide-20"></div>
																	
																	<div class="row">
																		
																	</div>
																	<!-- /BUTTONS -->
																</div>
																<div class="col-md-5">
																	<div class="box border inverse">
																		<div class="box-title">
																			<h4>
																				<i class="fa fa-bars"></i>Statistics
																			</h4>
																			<div class="tools">
																			<a href="javascript:;" class="reload">
																				<i class="fa fa-refresh" id="refresh_button"></i>
																			</a>
																				<a href="javascript:;" class="filter"> <i
																					class="fa fa-filter" data-toggle="modal"
																					data-target="#Date_picker0"></i>
																				</a>
																			</div>
																		</div>
																		<div class="box-body big sparkline-stats">
																			<div class="col-md-6 col-sm-6" id="total" style="margin-top: 5px;">
																				
																					<div class="sparkline-row">
																						<div
																							style="width: 10px; height: 10px; background: #ffcc33; position: absolute; left: -26px; top: 6px;"></div>
																							<span class="title">Total Credit</span>
																						<span class="value"><i class="fa fa-dollar"
																							style="font-size: 20px;"></i> <span id="account_credit" style="20px;"></span></span>
																					</div>
																					<br>
																					<div class="sparkline-row">
																						<div
																							style="width: 10px; height: 10px; background: #5cb85c; position: absolute; left: -26px; top: 6px;"></div>
																							<span class="title">Total Debit</span>
																						<span class="value"><i class="fa fa-dollar"
																							style="font-size: 20px;" ></i> <span id="account_debit" style="20px;"></span></span>
																					</div>
																				
																			</div>
																			<div class="col-md-6 col-sm-6" id="pie_chart">
																				<span class="sparklinepie big">${totalExpense}</span>
																			</div>
																		</div>
																	</div>
																	<!-- /BOX -->
																	<!-- /SAMPLE -->
																</div>
															</div>
															<!-- /ROW 1 -->
															<div class="divide-40"></div>
															<!-- ROW 2 -->
															<div class="row">
																<div class="col-md-12">
																	<div class="box border blue">
																		<div class="box-title" style="background:#e8b42f;">
																			<h4>
																				<i class="fa fa-columns"></i> <span
																					class="hidden-inline-mobile">Transaction History</span>
																			</h4>
																			<div class="tools">
																			<a href="javascript:;" class="reload">
																				<i class="fa fa-refresh" id="trx_refresh_button"></i>
																			</a>
																				<a class="filter" data-toggle="modal"
																					data-target="#Date_picker"> <i
																					class="fa fa-filter" data-toggle="modal"></i>
																				</a>
																			</div>
																		</div>
																		<div class="box-body rcpt_tbl">
																			<div class="tabbable">
																				<div class="tab-content">
																					<div class="tab-pane active" id="sales">
																						<table class="table table-bordered table-hover"
																							id="editedtable">
																							<thead>
																								<tr>
																								<p id="transaction_filter_msg" class="label label-success"></p>
																									<th><i class="fa fa-calendar"></i> Date</th>
																									<th class="hidden-xs">Description</th>
																									<th class="hidden-xs">Transaction RefNo</th>
																									<th><i class="fa fa-dollar"></i> Amount</th>
																									<th><i class="fa fa-bars"></i> Status</th>
																								</tr>
																							</thead>
																						</table>
																					</div>
																				</div>
																			</div>
																			<nav aria-label="Page navigation" class="text-center">
																				<div id="rcptPaginationnew">
                                                                                    <ul id="paginationfile" >
																				</ul>
																				</div>
																				<ul id="rcptPagination" class="pagination">
																				</ul>
																			</nav>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>

												<!-- EDIT ACCOUNT -->
												<div class="tab-pane fade" id="pro_edit">
													<div class="container prof_contain">
														<div class="col-md-6">
															<ul class="nav nav-tabs">
																<li><a data-toggle="tab" href="#password">Change
																		Password</a></li>
																<li class="active"><a data-toggle="tab"
																	href="#profile">Edit Profile</a></li>
															</ul>

															<div class="tab-content">
																<div id="profile" class="tab-pane fade in active">
																	<form class="form-horizontal" modelAttribute="editUser"
																		method="post"
																		action="${pageContext.request.contextPath}/User/EditProfile/Process">
																		<div class="col-md-12 form-vertical">
																			<div class="box border inverse">
																				<div class="box-title">
																					<h4>
																						<i class="fa fa-bars"></i>General Information
																					</h4>
																				</div>
																				<div class="box-body big prof_box">
																					<div class="row">
																						<div class="col-md-12">
																							<!-- <h4>Basic Information</h4> -->
																							<div class="form-group">
																								<!-- <label class="col-md-4 control-label">Name</label>  -->
																								<div class="col-md-6">
																									<input type="text" name="firstName"
																										class="form-control" id="first_name_ep"
																										required="required">
																									<p class="error" id="error_first_name"></p>
																								</div>
																								<div class="col-md-6">
																									<input type="text" name="lastName"
																										class="form-control" id="last_name_ep"
																										required="required">
																									<p class="error" id="error_last_name"></p>
																								</div>
																							</div>
																							<div class="form-group">
																								<!-- <label class="col-md-4 control-label">Birthday</label>  -->
																								<div class="col-md-6">
																									<input type="text" class="form-control"
																										name="address" id="address"
																										required="required">
																									<p class="error" id="error_address"></p>
																								</div>
																							</div>
																							<sec:csrfInput />
																							<center>
																								<input type="submit" value="Update" id="esubmit"
																									class="btn btn-primary modal_btn">
																							</center>
																						</div>
																					</div>
																					<code>${message}</code>
																				</div>
																			</div>
																		</div>
																	</form>
																</div>

																<div id="password" class="tab-pane fade">
																	<code>${message}</code>
																	<form class="form-horizontal" method="post"
																		action="${pageContext.request.contextPath}/User/UpdatePassword/Process">
																		<div class="col-md-12 form-vertical">
																			<div class="box border inverse">
																				<div class="box-title">
																					<h4>
																						<i class="fa fa-bars"></i>Profile
																					</h4>
																				</div>
																				<div class="box-body big prof_box">
																					<div class="row">
																						<div class="col-md-12">
																							<!-- <h4>Change Password</h4> -->

																							<div class="form-group">
																								<div class="col-md-3"></div>
																								<div class="col-md-6">
																									<input type="password" id="current"
																										name="oldPassword" minlength="6" maxlength="6"
																										class="form-control"
																										placeholder="Current Password">
																									<p>${cherror.password}</p>
																								</div>
																								<div class="col-md-3"></div>
																							</div>
																							<div class="form-group">
																								<div class="col-md-3"></div>
																								<div class="col-md-6">
																									<input class="form-control" type="password"
																										id="new" name="newPassword" minlength="6"
																										maxlength="6" placeholder="New Password">
																									<p>${cherror.newPassword}</p>
																								</div>
																								<div class="col-md-3"></div>
																							</div>
																							<div class="form-group">
																								<div class="col-md-3"></div>
																								<div class="col-md-6">
																									<input class="form-control" type="password"
																										id="confirm_new" name="confirmPassword"
																										minlength="6" maxlength="6"
																										placeholder="Confirm Password">
																									<p>${cherror.confirmPassword}</p>
																								</div>
																								<div class="col-md-3"></div>
																							</div>

																							<center>
																								<input type="submit" value="Update Password"
																									class="btn btn-primary modal_btn">
																							</center>
																						</div>
																					</div>
																					<code>${message}</code>
																				</div>
																			</div>
																		</div>
																	</form>
																</div>
															</div>
														</div>
														<div class="col-md-6 hidden-xs">
															<div class="slider"
																style="margin-right: -15px; margin-left: -15px;">
																<div class="carousel slide hidden-xs"
																	data-ride="carousel" id="mycarousel">
																	<ol class="carousel-indicators">
																		<li class="" data-slide-to="0"
																			data-target="#mycarousel"></li>
																		<li data-slide-to="1" data-target="#mycarousel"
																			class="active"></li>
																	</ol>

																	<div class="carousel-inner">

																		<div class="item" id="slide1">
																			<img
																				src="${pageContext.request.contextPath}/resources/images/slider_1.jpg">
																		</div>
																		<!---end item---->

																		<div class="item active">

																			<img
																				src="${pageContext.request.contextPath}/resources/images/slider_2.jpg">
																		</div>
																		<!---end item---->
																	</div>
																	<!--end carousel inner------>

																</div>
																<!---end caeousel slider---->
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<!-- <div class="footer-tools">
						<span class="go-top"> <i class="fa fa-chevron-up"></i> Top
						</span>
					</div> -->

					<jsp:include page="/WEB-INF/jsp/User/Footer.jsp" />


					<!-- Modal Start Here==========================================================================================================
	================================================================================================================== -->
					<center>
						<!-- Modal for Mobile Topup -->
						<div class="modal fade" id="topup_data" role="dialog">
							<div class="modal-dialog">
								<!-- Modal content-->
								<div class="modal-content rchrg_modal">
									<div class="modal-header rchrg_head">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<center>
											<h2 class="modal-title">
												<b>Recharge</b>
											</h2>
										</center>
									</div>
									<div class="modal-body">
										<div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<!-- Nav tabs -->
												<ul class="nav nav-tabs">
													<li class="active" id="PrepaidSubMenu"><a id="menu1"
														href="#prepaid" data-toggle="tab">PrePaid</a></li>
													<li id="PostpaidSubMenu"><a id="menu2"
														href="#postpaid" data-toggle="tab">PostPaid</a></li>
													<li id="DataCardSubMenu"><a id="menu3" href="#data"
														data-toggle="tab">DataCard</a></li>
												</ul>
												<!-- Tab panes -->
												<div class="tab-content">
													<span id="error_pre_topup" class="label label-danger"></span>
													<span id="success_pre_topup" class="label label-success"></span>
													<div class="tab-pane active" id="prepaid">
														<div class="form-group">
															<div class="col-md-12">
																<form method="post" action="#">
																	<div class="form-group">
																		<div class="col-md-12 col-sm-12 col-xs-12">
																			<input type="tel" class="form-control fields numeric"
																				id="pre_mobile" placeholder="Enter Mobile Number"
																				maxlength="10"
																				onkeypress="clearvalue('error_pre_mobile')"
																				onKeyPress="return numbersonly(this, event)">
																			<p id="error_pre_mobile" class="error"
																				style="color: red"></p>
																			<a data-toggle="modal"
																				data-target="#order_confirmation"></a>
																		</div>
																	</div>
																	<div class="form-group">
																		<div class="col-md-6 col-sm-6 col-xs-6">
																			<select class="form-control fields"
																				name="serviceProvider" id="pre_operator"
																				onchange="clearvalue('error_pre_operator')">
																				<option value="#">Select Operator</option>
																				<option value="VACP">Aircel</option>
																				<option value="VATP">Airtel</option>
																				<option value="VBVP">BSNL - Special Tariff</option>
																				<option value="VBGP">BSNL</option>
																				<option value="VIDP">Idea</option>
																				<option value="VMSP">MTNL - Special Tariff</option>
																				<option value="VMMP">MTNL</option>
																				<option value="VMTP">MTS</option>
																				<option value="VRGP">Reliance</option>
																				<option value="VTMP">T24 Mobile - Special Tariff</option>
																				<option value="VTMP">T24 Mobile</option>
																				<option value="VTCP">Tata Docomo CDMA</option>
																				<option value="VTCP">Tata Docomo GSM-Special Tariff</option>
																				<option value="VTCP">Tata Docomo GSM</option>
																				<option value="VUSP">Telenor - Special Tariff</option>
																				<option value="VUGP">Telenor</option>
																				<option value="VVSP">Videocon - Special Tariff</option>
																				<option value="VVGP">Videocon</option>
																				<option value="VVFP">Vodafone</option>
																				<option value="VRJP">Reliance Jio</option>
																			</select>
																			<p id="error_pre_operator" class="error"
																				style="color: red"></p>
																		</div>
																		<div class="col-md-6 col-sm-6 col-xs-6">
																			<select name="serviceProvider" id="pre_circle"
																				onchange="clearvalue('error_pre_circle')"
																				class="form-control fields">
																				<option value="#">Select Area</option>
																				<option value="AP">Andhra Pradesh</option>
																				<option value="AS">Assam</option>
																				<option value="BR">Bihar and Jharkhand</option>
																				<option value="CH">Chennai</option>
																				<option value="DL">Delhi</option>
																				<option value="GJ">Gujarat</option>
																				<option value="HR">Haryana</option>
																				<option value="HP">Himachal Pradesh</option>
																				<option value="JK">Jammu and Kashmir</option>
																				<option value="KN">Karnataka</option>
																				<option value="KL">Kerala</option>
																				<option value="KO">Kolkata</option>
																				<option value="MP">Madhya Pradesh/Chattisgarh</option>
																				<option value="MH">Maharashtra</option>
																				<option value="MU">Mumbai</option>
																				<option value="NE">North East</option>
																				<option value="OR">Orissa</option>
																				<option value="PB">Punjab</option>
																				<option value="RJ">Rajasthan</option>
																				<option value="TN">Tamil Nadu</option>
																				<option value="UW">Uttar Pradesh(W)/Uttranchal</option>
																				<option value="UE">Uttar Pradesh(E)</option>
																				<option value="WB">West Bengal</option>
																			</select>
																			<div id="plan_link"
																				style="text-align: -webkit-right; position: absolute;"></div>
																			<p id="error_pre_circle" class="error"
																				style="color: red"></p>
																		</div>
																	</div>
																	<div class="form-group">
																		<div class="col-md-6 col-sm-6 col-xs-6">
																			<input type="text"
																				class="form-control fields numeric"
																				placeholder="Enter Amount" id="pre_amount"
																				name="amount" class="numeric" min="10" maxLength="8"
																				onkeypress="clearvalue('error_pre_amount')">
																			<p id="error_pre_amount" class="error"
																				style="color: red"></p>
																		</div>
																	</div>
																	<div class="row">
																		<div class="col-md-12 col-sm-12 col-xs-12">
																			<center>
																				<button type="button" id="pre_submit"
																					class="btn mobile-btn">Pay</button>
																			</center>
																		</div>
																	</div>
																</form>
															</div>
														</div>
													</div>
													<div class="tab-pane" id="postpaid">
														<span id="error_post_topup" class="label label-danger"></span>
														<span id="success_post_topup" class="label label-success"></span>
														<form>
															<div class="form-group">
																<div class="col-md-12">
																	<form method="post" action="#">
																		<div class="form-group">
																			<div class="col-md-12 col-sm-12 col-xs-12">
																				<input type="tel"
																					class="form-control fields numeric"
																					id="post_mobile" placeholder="Enter Mobile Number"
																					maxlength="10"
																					onkeypress="clearvalue('error_post_mobile')"
																					onKeyPress="return numbersonly(this, event)">
																				<a data-toggle="modal"
																					data-target="#order_confirmation"></a>
																				<p id="error_post_mobile" class="error"
																					style="color: red"></p>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-md-6 col-sm-6 col-xs-6">
																				<select class="form-control fields"
																					name="serviceProvider" id="post_operator"
																					onchange="clearvalue('error_post_operator')">
																					<option value="#">Select Operator</option>
																					<option value="VACC">Aircel</option>
																					<option value="VATC">Airtel</option>
																					<option value="VBGC">BSNL</option>
																					<option value="VIDC">Idea</option>
																					<option value="VMTC">MTS</option>
																					<option value="VRGC">Reliance</option>
																					<option value="VTDC">Tata Docomo</option>
																					<option value="VVFC">Vodafone</option>
																				</select>
																				<p id="error_post_operator" class="error"
																					style="color: red"></p>
																			</div>
																			<div class="col-md-6 col-sm-6 col-xs-6">
																				<select class="form-control fields"
																					name="serviceProvider" id="post_circle"
																					onchange="clearvalue('error_pre_circle')">
																					<option value="#">Select Area</option>
																					<option value="AP">Andhra Pradesh</option>
																					<option value="AS">Assam</option>
																					<option value="BR">Bihar and Jharkhand</option>
																					<option value="CH">Chennai</option>
																					<option value="DL">Delhi</option>
																					<option value="GJ">Gujarat</option>
																					<option value="HR">Haryana</option>
																					<option value="HP">Himachal Pradesh</option>
																					<option value="JK">Jammu and Kashmir</option>
																					<option value="KN">Karnataka</option>
																					<option value="KL">Kerala</option>
																					<option value="KO">Kolkata</option>
																					<option value="MP">Madhya Pradesh/Chattisgarh</option>
																					<option value="MH">Maharashtra</option>
																					<option value="MU">Mumbai</option>
																					<option value="NE">North East</option>
																					<option value="OR">Orissa</option>
																					<option value="PB">Punjab</option>
																					<option value="RJ">Rajasthan</option>
																					<option value="TN">Tamil Nadu</option>
																					<option value="UW">Uttar Pradesh(W)/Uttranchal</option>
																					<option value="UE">Uttar Pradesh(E)</option>
																					<option value="WB">West Bengal</option>
																				</select>
																				<p id="error_post_circle" class="error"
																					style="color: red"></p>
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-md-6 col-sm-6 col-xs-6">
																				<input type="text"
																					class="form-control fields numeric"
																					id="post_amount" placeholder="Enter Amount"
																					onkeypress="clearvalue('error_post_amount')"
																					maxlength="8" />
																				<p id="error_post_amount" class="error"
																					style="color: red"></p>
																			</div>
																		</div>
																		<div class="row">
																			<div class="col-md-12 col-sm-12 col-xs-12">
																				<center>
																					<button type="button" id="post_submit"
																						class="btn mobile-btn">Pay</button>
																				</center>
																			</div>
																		</div>
																	</form>
																</div>
															</div>
														</form>
													</div>
													<div class="tab-pane" id="data">
														<span id="error_dc_topup" class="label label-danger"></span>
														<span class="success_dc_topup" class="label label-success"></span>
														<div class="form-group">
															<div class="col-md-12">
																<form method="post" action="#">
																	<div class="form-group">
																		<div class="col-md-12 col-sm-12 col-xs-12">
																			<input type="tel" class="form-control fields numeric"
																				id="dc_mobile" placeholder="Enter Mobile Number"
																				maxlength="10"
																				onkeypress="clearvalue('error_dc_mobile')"
																				onKeyPress="return numbersonly(this, event)"
																				required> <span class="highlight"></span><span
																				class="bar"></span>
																			<p id="error_dc_mobile" class="error"
																				style="color: red"></p>
																			<a data-toggle="modal"
																				data-target="#order_confirmation"></a>
																		</div>
																	</div>
																	<div class="form-group">
																		<div class="col-md-6 col-sm-6 col-xs-6">
																			<select class="form-control fields"
																				name="serviceProvider" id="dc_operator"
																				onchange="clearvalue('error_dc_operator')">
																				<option value="#">Select Operator</option>
																				<option value="VACP">Aircel</option>
																				<option value="VATP">Airtel</option>
																				<option value="VBVP">BSNL - Special Tariff</option>
																				<option value="VBGP">BSNL</option>
																				<option value="VIDP">Idea</option>
																				<option value="VMSP">MTNL - Special Tariff</option>
																				<option value="VMMP">MTNL</option>
																				<option value="VMTP">MTS</option>
																				<option value="VRGP">Reliance</option>
																				<option value="VTVP">T24 Mobile - Special Tariff</option>
																				<option value="VTMP">T24 Mobile</option>
																				<option value="VTCP">Tata Docomo CDMA</option>
																				<option value="VTCP">Tata Docomo GSM - Special Tariff</option>
																				<option value="VTCP">Tata Docomo GSM</option>
																				<option value="VUSP">Telenor - Special Tariff</option>
																				<option value="VUGP">Telenor</option>
																				<option value="VVSP">Videocon - Special Tariff</option>
																				<option value="VVGP">Videocon</option>
																				<option value="VVFP">Vodafone</option>
																			</select>
																			<p id="error_dc_operator" class="error"
																				style="color: red"></p>
																		</div>
																		<div class="col-md-6 col-sm-6 col-xs-6">
																			<select class="form-control fields"
																				name="serviceProvider" id="dc_circle"
																				onchange="clearvalue('error_dc_circle')">
																				<option value="#">Select Area</option>
																				<option value="AP">Andhra Pradesh</option>
																				<option value="AS">Assam</option>
																				<option value="BR">Bihar and Jharkhand</option>
																				<option value="CH">Chennai</option>
																				<option value="DL">Delhi</option>
																				<option value="GJ">Gujarat</option>
																				<option value="HR">Haryana</option>
																				<option value="HP">Himachal Pradesh</option>
																				<option value="JK">Jammu and Kashmir</option>
																				<option value="KN">Karnataka</option>
																				<option value="KL">Kerala</option>
																				<option value="KO">Kolkata</option>
																				<option value="MP">Madhya Pradesh/Chattisgarh</option>
																				<option value="MH">Maharashtra</option>
																				<option value="MU">Mumbai</option>
																				<option value="NE">North East</option>
																				<option value="OR">Orissa</option>
																				<option value="PB">Punjab</option>
																				<option value="RJ">Rajasthan</option>
																				<option value="TN">Tamil Nadu</option>
																				<option value="UW">Uttar Pradesh(W)/Uttranchal</option>
																				<option value="UE">Uttar Pradesh(E)</option>
																				<option value="WB">West Bengal</option>

																			</select>
																			<div id="plan_link_dc"
																				style="margin-right: 150px; font-size: 12px;"></div>
																			<p id="error_dc_circle" class="error"
																				style="color: red"></p>
																		</div>
																	</div>
																	<div class="form-group">
																		<div class="col-md-6 col-sm-6 col-xs-6">
																			<input type="text" name="amount"
																				class="form-control fields numeric" min="10"
																				class="numeric" maxlength="8" id="dc_amount"
																				placeholder="Enter Amount" required="required" />
																		</div>
																	</div>
																	<div class="row">
																		<div class="col-md-12 col-sm-12 col-xs-12">
																			<center>
																				<button type="button" id="dc_submit"
																					class="btn mobile-btn">Pay</button>
																			</center>
																		</div>
																	</div>
																</form>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>

									</div>
								</div>
								<!-- Modal for Mobile Topup -->
					</center>


					<center>
						<!-- Modal for DTH -->
						<div class="modal fade" id="dth" role="dialog">
							<div class="modal-dialog">

								<!-- Modal content-->
								<div class="modal-content rchrg_modal">
									<div class="modal-header dth_head">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<center>
											<h2 class="modal-title">
												<b>DTH</b>
											</h2>
										</center>
									</div>
									<div class="modal-body">
										<div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<p id="error_dth_bill" class="label label-danger"></p>
												<p id="sucess_dth_bill" class="label label-success"></p>
												<br> <br>
												<div class="form-group" style="margin-top: -46px;">
													<div class="col-md-12">
														<form method="post" action="#">
															<div class="group_1">
																<select name="serviceProvider" id="dth_provider"
																	onchange="clearDTH('error_dth_provider','error_dth_number','error_dth_amount')"
																	class="form-control">
																	<option value="#">Select DTH Provider</option>
																	<option value="VATV">Airtel Digital TV</option>
																	<option value="VDTV">Dish TV</option>
																	<option value="VRTV">Reliance Digital TV</option>
																	<option value="VSTV">Sun Direct</option>
																	<option value="VOTV">Tata Sky</option>
																	<option value="VVTV">Videocon d2h</option>
																</select>
																<p class="error" id="error_dth_provider"
																	style="color: red"></p>
															</div>

															<div class="group_1">
																<input type="text" name="dthNo"
																	class="form-control numeric" id="dth_number"
																	style="margin-right: 50px;" required
																	onkeypress="clearvalue('error_dth_number'); enabale('dth1')">
																<label id="dth1" style="margin-right: 30px">Account
																	No</label>
																<p class="error" id="error_dth_number"
																	style="color: red"></p>
															</div>

															<div class="group_1">
																<input type="text" class="form-control numeric"
																	name="amount" min="10" id="dth_amount" maxlength="8"
																	onkeypress="clearvalue('error_dth_amount'); enabale('dth2')">
																<p id="error_dth_amount" class="error"
																	style="color: red"></p>
																<label id="dth2">Amount</label>
															</div>
															<button type="button" class="btn" id="dth_submit"
																style="background: #8cc84e; color: #FFFFFF;">Pay</button>
														</form>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

							</div>

						</div>
						<!-- Modal for DTH -->
					</center>

					<center>
						<!-- Modal for Landline -->
						<div class="modal fade" id="ladline" role="dialog">
							<div class="modal-dialog h_modal">
								<!-- Modal content-->
								<div class="modal-content rchrg_modal">
									<div class="modal-header land_head">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<center>
											<h2 class="modal-title">
												<b>Landline</b>
											</h2>
										</center>
									</div>
									<div class="modal-body">
										<div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<p class="label label-danger" id="error_landline_bill"></p>
												<p class="label label-success" id="success_landline_bill"></p>
												<div class="form-group" style="margin-top: -27px;">
													<div class="col-md-12">
														<form method="post" action="#">
															<br>
															<div class="group_1">
																<select name="serviceProvider" id="landline_provider"
																	onchange="clearLandline('error_landline_provider','error_landline_std','error_landline_ca','error_landline_number','error_landline_account_number','error_landline_amount')"
																	class="form-control">
																	<option value="#">Select Landline Provider</option>
																	<option value="VATL">Airtel</option>
																	<option value="VBGL">BSNL</option>
																	<option value="VMDL">MTNL - Delhi</option>
																	<option value="VRGL">Reliance</option>
																	<option value="VTCL">Tata Docomo</option>
																</select>
																<p class="error" id="error_landline_provider"
																	style="color: red"></p>
															</div>

															<div class="group_1">
																<input type="text" class="form-control numeric"
																	name="stdCode" id="landline_std"
																	style="margin-right: 50px;"
																	onkeypress="clearvalue('error_landline_std'); enabale('Lan');">
																<p class="error" id="error_landline_std"
																	style="color: red"></p>
																<label id="Lan">STD Code</label>
															</div>
															<div class="group_1">
																<input type="text" class="form-control numeric"
																	name="caCode" id="landline_ca"
																	style="margin-right: 50px;"
																	onkeypress="clearvalue('error_landline_ca'); enabale('Lan1');">
																<p class="error" id="error_landline_ca"
																	style="color: red"></p>
																<label id="Lan1">CA Number</label>

															</div>
															<div class="group_1">
																<input type="text" class="form-control numeric"
																	name="landlineNumber" id="landline_number"
																	style="margin-right: 50px;"
																	onkeypress="clearvalue('error_landline_number'); enabale('Lan2');">
																<p class="error" id="error_landline_number"
																	style="color: red"></p>
																<label id="Lan2">Landline No</label>
															</div>
															<div class="group_1">
																<input type="text" name="accountNumber"
																	class="form-control numeric"
																	id="landline_account_number"
																	style="margin-right: 50px;"
																	onkeypress="clearvalue('error_landline_account_number'); enabale('Lan3');">
																<p class="error" id="error_landline_account_number"
																	style="color: red"></p>
																<label id="Lan3">Account No</label>
															</div>

															<div class="group_1">
																<input type="text" name="amount" min="10"
																	class="form-control numeric" id="landline_amount"
																	maxlength="8" style="margin-right: 50px;"
																	onkeypress="clearvalue('error_landline_amount'); enabale('Lan4');">
																<label id="Lan4">Enter Exact Amount</label> 
																<a href="#" id="land_get_amount" class="links"
																	style="font-size: 12px;">Get Due Amount</a>
																<p style="margin-top: -30px;color: red" class="error"
																	id="error_landline_amount"></p>
															</div>
															<br> <br>

															<button type="button" class="btn" id="landline_submit"
																style="background: #ffc000; color: white;">Pay</button>
														</form>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>
						<!-- Modal for Landline -->
					</center>

					<center>
						<!-- Modal for Electricity -->
						<div class="modal fade" id="electricity" role="dialog">
							<div class="modal-dialog">

								<!-- Modal content-->
								<div class="modal-content rchrg_modal">
									<div class="modal-header elctr_head">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<center>
											<h2 class="modal-title">
												<b>Electricity</b>
											</h2>
										</center>
									</div>
									<div class="modal-body">
										<div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<div class="form-group">
													<p class="label label-success" id="success_ecity_bill"></p>
													<p class="label label-danger" id="error_ecity_bill"></p>
													<div class="col-md-12">
														<form method="post" action="#">
															<div class="group_1">
																<select name="serviceProvider" id="ecity_provider"
																	onchange="clearEcity('error_ecity_provider','ecity_account_label','error_ecity_account_number','error_ecity_cycle_number','error_ecity_billing_unit','error_ecity_processing_cycle','error_ecity_city_name','error_ecity_amount')"
																	class="form-control">
																	<option value="#">Select Electricity Provider</option>
																	<option value="VARE"> Ajmer Vidyut Vitran Nigam - RAJASTHAN</option>
																	<option value="VAAE">APDCL - ASSAM</option>
																	<option value="VBBE">BESCOM - BENGALURU</option>
																	<option value="VBME">BEST Undertaking - MUMBAI</option>
																	<option value="VBRE">BSES Rajdhani - DELHI</option>
																	<option value="VBYE">BSES Yamuna - DELHI</option>
																	<option value="VCWE">CESC - WEST BENGAL</option>
																	<option value="VCCE">CSEB - CHHATTISGARH</option>
																<!-- 	<option value="VDHE">DHBVN - HARYANA</option>
																	<option value="VDNE">DNHPDCL - DADRA & NAGAR HAVELI</option> -->
																	<option value="VIPE">India Power</option>
																	<option value="VJRE">Jaipur Vidyut Vitran Nigam - RAJASTHAN</option>
																	<option value="VJUE">Jamshedpur Utilities & Services (JUSCO)</option>
																	<option value="VDRE">Jodhpur Vidyut VitranNigam - RAJASTHAN</option>
																	<option value="VMME">Madhya Kshetra Vitaran -MADHYA PRADESH</option>
																	<option value="VMDE">MSEDC - MAHARASHTRA</option>
																	<option value="VNUE">Noida Power - NOIDA</option>
																	<!-- <option value="VDOE">Odisha Discoms - ODISHA</option> -->
																	<option value="VMPE">Paschim Kshetra Vitaran -MADHYA PRADESH</option>
																	<option value="VREE">Reliance Energy - MUMBAI</option>
																	<!-- <option value="VSAE">Southern Power - ANDHRAPRADESH</option>
																	<option value="VSTE">Southern Power -TELANGANA</option> -->
																	<option value="VNDE">Tata Power - DELHI</option>
																	<option value="VTPE">Torrent Power</option>
																	<option value="VTTE">TSECL - TRIPURA</option>
																</select>
																<p class="error" id="error_ecity_provider"
																	style="color: red"></p>
															</div>


															<div class="group_1">
																<input type="text" name="accountNumber"
																	class="form-control numeric" id="ecity_account_number"
																	style="margin-right: 50px;"
																	onkeypress="clearvalue('error_ecity_account_number'); enabale('ecity_account_label')">
																<p class="error" id="error_ecity_account_number"
																	style="color: red"></p>
																<label id="ecity_account_label">Account No</label>
															</div>

															<div class="group_1" id="cycle_number">
																<input type="text" name="cycleNumber"
																	class="form-control numeric" id="ecity_cycle_number"
																	style="margin-right: 50px;"
																	onkeypress="clearvalue('error_ecity_cycle_number'); enabale('eci1')">
																<p class="error" id="error_ecity_cycle_number"
																	style="color: red"></p>
																<label id="eci1">Cycle No</label>
															</div>
															<div class="group_1" id="billing_unit">
																<input type="text" name="cycleNumber"
																	class="form-control numeric" id="ecity_billing_unit"
																	style="margin-right: 50px;"
																	onkeypress="clearvalue('error_ecity_billing_unit'); enabale('eci2')">
																<p class="error" id="error_ecity_billing_unit"
																	style="color: red"></p>
																<label id="eci2">Billing Unit</label>
															</div>
															<div class="group_1" id="processing_cycle">
																<input type="text" name="cycleNumber"
																	id="ecity_processing_cycle"
																	class="form-control numeric"
																	onkeypress="clearvalue('error_ecity_processing_cycle'); enabale('eci3')">
																<p class="error" id="error_ecity_processing_cycle"
																	style="color: red"></p>
																<label id="eci3">Processing Cycle</label>
															</div>
															<div class="group_1" id="city_name">
																<input type="text" name="cycleNumber"
																	id="ecity_city_name" class="form-control"
																	onkeypress="clearvalue('error_ecity_city_name'); enabale('eci4')">
																<p class="error" id="error_ecity_city_name"
																	style="color: red"></p>
																<label id="eci4">City Name</label>
															</div>

															<div class="group_1">
																<input type="text" name="amount" min="10"
																	class="form-control numeric" id="ecity_amount"
																	maxlength="8" style="margin-right: 50px;"
																	onkeypress="clearvalue('error_ecity_amount'); enabale('eci5')">
																<p class="error" id="error_ecity_amount"
																	style="color: red"></p>
																<a href="#" id="elictiricity_get_amount" class="links">Get Due Amount</a> <label id="eci5">Enter Exact Amount</label>
															</div>
															<br>
															<button type="button" class="btn" id="ecity_submit"
																style="background: #0a9f8c; color: white;">Pay</button>
														</form>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- Modal for Electricity -->
					</center>

					<center>
						<!-- Modal for Gas -->
						<div class="modal fade" id="gas" role="dialog">
							<div class="modal-dialog">

								<!-- Modal content-->
								<div class="modal-content rchrg_modal">
									<div class="modal-header gas_head">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<center>
											<h2 class="modal-title">
												<b>Gas</b>
											</h2>
										</center>
									</div>
									<div class="modal-body">
										<div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<p class="label label-danger" id="success_gas_bill"></p>
												<p class="label label-success" id="error_gas_bill"></p>
												<br> <br>
												<!-- <form method="post" action="#"> -->
												<div class="form-group" style="margin-top: -46px;">
													<div class="col-md-12">
														<form method="post" action="#">
															<div class="group_1">
																<select name="serviceProvider" id="gas_provider"
																	onchange="clearGas('error_gas_provider','error_gas_account_number','error_gas_bill_group','error_gas_amount')"
																	class="form-control">
																	<option value="#">Select Gas Provider</option>
																	<option value="VADG">Adani Gas</option>
																	<option value="VGJG">Gujarat Gas</option>
																	<option value="VIPG">Indraprastha Gas</option>
																	<option value="VMMG">Mahanagar Gas</option>
																</select>
																<p class="error" id="error_gas_provider"
																	style="color: red"></p>
															</div>

															<div class="group_1">
																<input type="text" name="accountNumber"
																	class="form-control numeric" id="gas_account_number"
																	style="margin-right: 50px;"
																	onkeypress="clearvalue('error_gas_account_number'); enabale('gas_account_label')">
																<p class="error" id="error_gas_account_number"
																	style="color: red"></p>
																<label id="gas_account_label" style="margin-right: 30px">Account
																	No</label>
															</div>

															<div class="group_1" id="bill_group">
																<input type="text" name="billGroupNumber"
																	id="gas_bill_group" class="form-control numeric"
																	style="margin-right: 50px;"
																	onkeypress="clearvalue('error_gas_bill_group'); enabale('gas1')">
																<p class="error" id="error_gas_bill_group" style="color: red"></p>
																<label id="gas1">Bill Group No</label>
															</div>

															<div class="group_1">
																<input type="text" name="amount" min="10"
																	class="form-control numeric" id="gas_amount"
																	style="margin-right: 50px;" maxlength="8"
																	onkeypress="clearvalue('error_gas_amount'); enabale('gas2')">
																<p class="error" id="error_gas_amount"
																	style="color: red"></p>
																<a href="#" id="gas_get_amount" class="links">Get
																	Due Amount</a> <label id="gas2">Enter Exact Amount</label>
															</div>
															<button type="button" class="btn" id="gas_submit"
																style="background: #ffc000; color: #FFFFFF;">Pay</button>
														</form>
													</div>
												</div>
												<!-- </form> -->
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>
						<!-- Modal for Gas -->
					</center>

					<center>
						<!-- Modal for Insurance -->
						<div class="modal fade" id="insurance" role="dialog">
							<div class="modal-dialog">

								<!-- Modal content-->
								<div class="modal-content rchrg_modal">
									<div class="modal-header ins_head">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<center>
											<h2 class="modal-title">
												<b>Insurance</b>
											</h2>
										</center>
									</div>
									<div class="modal-body">
										<div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<div class="form-group">
													<p class="label label-danger" id="success_ins_bill"></p>
													<p class="label label-success" id="error_ins_bill"></p>
													<div class="col-md-12">
														<form method="post" action="#">
															<div class="group_1">
																<select name="serviceProvider" id="ins_provider"
																	onchange="clearInsurance('error_ins_provider','error_ins_policy_number','error_ins_policy_date','error_ins_amount')"
																	class="form-control">
																	<option value="#">Select Insurance Provider</option>
																	<!-- <option value=“VBAI”>Bharti AXA Life Insurance</option> -->
																	<option value="VIPI">ICICI Prudential Life Insurance</option>
																	<option value=“VILI”>IndiaFirst Life Insurance</option>
																	<option value="VTAI">Tata AIA Life Insurance</option>
																</select>
																<p class="error" id="error_ins_provider"
																	style="color: red"></p>
															</div>

															<div class="group_1">
																<input type="text" name="policyNumber"
																	class="form-control numeric" id="ins_policy_number"
																	style="margin-right: 50px;"
																	onkeypress="clearvalue('error_ins_policy_number'); enabale('i_account_name')">
																<p class="error" id="error_ins_policy_number"
																	style="color: red"></p>
																<label id="i_account_name">Policy No</label>
															</div>

															<div class="group_1">
																<input type="text" class="form-control numeric"
																	name="policyDate" id="Insurance_Date"
																	style="margin-right: 50px;" readonly>
																<p class="error" id="error_ins_policy_date"
																	style="color: red"></p>
																<label>Policy Date</label>
															</div>

															<div class="group_1">
																<input type="text" name="amount" min="10"
																	id="ins_amount" class="form-control numeric"
																	style="margin-right: 50px;" maxlength="8"
																	onkeypress="clearvalue('error_ins_amount'); enabale('Ins2')">
																<p class="error" id="error_ins_amount"
																	style="color: red"></p>
																<label id="Ins2">Enter Amount</label>
															</div>
															<button type="button" class="btn" id="ins_submit"
																style="background: #0a9f8c; color: #FFFFFF;">Pay</button>
														</form>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>
						<!-- Modal for Insurance -->
					</center>

					<center>
						<!-- Modal for SendMoney -->
						<div class="modal fade" id="sendMoney" role="dialog">
							<div class="modal-dialog">

								<!-- Modal content-->
								<div class="modal-content rchrg_modal">
									<div class="modal-header sendm_head">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<center>
											<h2 class="modal-title">
												<b>Send Money</b>
											</h2>
										</center>
									</div>
									<div class="modal-body">
										<div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<!-- Nav tabs -->
												<ul class="nav nav-tabs">
													<li class="active"><a data-toggle="tab"
														href="#send_mob"><i class="fa fa-mobile"></i> <span>Mobile</span></a></li>
													<li><a data-toggle="modal"
														data-target="#banktransfer_submit" href="#send_bank"
														id="bank_submit"> <i class="fa fa-credit-card"></i> <span>Bank
																Account</span></a></li>
												</ul>
												<!-- Tab panes -->
												<div class="tab-content">
													<div class="tab-pane active" id="send_mob">
														<div class="form-group">
														<p class="label label-danger" id="error_mobile_sm"></p>
													    <p class="label label-success" id="success_mobile_sm"></p>
															<div class="col-md-12">
																<form method="post" action="#">
																	<div class="form-group">
																		<div class="col-md-12 col-sm-12 col-xs-12">
																			<input type="tel" class="form-control fields numeric"
																				name="mobileNumber" id="smm_mobile"
																				placeholder="Mobile Number" required="required"
																				maxlength="10" />
																			<p class="error" style="color: red" id="error_smm_mobile"></p>
																		</div>
																	</div>
																	<div class="form-group">
																		<div class="col-md-12 col-sm-12 col-xs-12">
																			<input type="text"
																				class="form-control fields numeric" name="amount"
																				id="smm_amount" placeholder="Amount"
																				required="required" maxlength="8" min="10" />
																			<p id="error_smm_amount" style="color: red" class="error"></p>
																		</div>
																	</div>
																	<div class="form-group">
																		<div class="col-md-12 col-sm-12 col-xs-12">
																			<textarea class="form-control fields" row="5"
																				id="smm_message" placeholder="Comment"></textarea>
																			<p id="error_smm_message"  style="color:red" class="error"></p>
																		</div>
																	</div>
																	<button type="button" class="btn sm-btn" id="smm_submit">Send</button>
																</form>
															</div>
														</div>
														</form>
													</div>
													<div class="tab-pane" id="send_bank">
														<form>
															<div class="form-group">
																<div class="col-md-12">
																	<form class="form-horizontal">
																		<div class="form-group">
																			<div class="col-md-12 col-sm-12 col-xs-12">
																				<input type="tel" class="form-control fields"
																					id="mobile" placeholder="Mobile Number"
																					required="required" maxlength="10" />
																			</div>
																		</div>
																		<div class="form-group">
																			<div class="col-md-6 col-sm-6 col-xs-6">
																				<input type="text" class="form-control fields"
																					id="amount" placeholder="Amount"
																					required="required" maxlength="6" />
																			</div>
																			<div class="col-md-6 col-sm-6 col-xs-6">
																				<textarea class="form-control fields" row="5"
																					id="comment" placeholder="Comment"></textarea>
																			</div>
																		</div>
																		<div class="row">
																			<div class="col-md-12 col-sm-12 col-xs-12">
																				<center>
																					<button type="submit"
																						class="submit btn btn-primary modal_btn">Proceed</button>
																				</center>
																			</div>
																		</div>
																	</form>
																</div>
															</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>
						<!-- Modal for SendMoney -->
						<div id="banktransfer_submit" class="modal fade" role="dialog">
							<div class="modal-dialog">

								<!-- Modal content-->
								<!-- <a href="https://play.google.com/store/apps/details?id=in.msewa.vpayqwik&hl=en"> -->
								<img src="<c:url value='/resources/images/popup_bank.jpg'/>"
									class="img-responsive" width="100%" usemap="#banner" alt="">

								<map name="banner">
									<a
										href="https://itunes.apple.com/us/app/vpayqwik/id1207326500?ls=1&mt=8"
										target="_blank">
										<area shape="rect" coords="55,412,233,464">
									</a>
									<a
										href="https://play.google.com/store/apps/details?id=in.msewa.vpayqwik&amp;hl=en"
										target="_blank">
										<area shape="rect" coords="248,413,425,464">
									</a>
								</map>
							</div>
							</div>
					</center>

					<center>
						<!-- Modal for Redeem Coupon -->
						<div class="modal fade" id="redeem" role="dialog">
							<div class="modal-dialog">

								<!-- Modal content-->
								<div class="modal-content rchrg_modal">
									<div class="modal-header redem_head">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<center>
											<h2 class="modal-title">
												<b>Redeem Coupon</b>
											</h2>
										</center>
									</div>
									<div class="modal-body">
										<div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<form>
													<div class="form-group" style="margin-top: -24px;">
														<div class="col-md-12">
															<p id="redeemId" style="color: #ff0000"></p>
															<form method="post" action="#">
																<div class="form-group">
																	<div class="col-md-12 col-sm-12 col-xs-12">
																		<input type="text" class="form-control fields"
																			"name="couponNumber" id="couponNumber"
																			placeholder="Enter Coupon Code"
																			value="${coupon.couponNumber}" required="required"
																			maxlength="10" />
																	</div>
																</div>
																<div class="row">
																	<div class="col-md-12 col-sm-12 col-xs-12">
																		<center>
																			<button type="button" class="btn mobile-btn" id="redeem_button"
																				>Redeem</button>
																		</center>
																	</div>
																</div>
															</form>
														</div>
													</div>
												</form>
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>
						<!-- Modal for Redeem Coupon -->
					</center>

					<center>
						<!-- Modal for Pay@strore -->
						<div class="modal fade" id="payStore" role="dialog">
							<div class="modal-dialog">

								<!-- Modal content-->
								<div class="modal-content rchrg_modal">
									<div class="modal-header pay_head">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<center>
											<h2 class="modal-title">
												<b>Pay @ Store</b>
											</h2>
										</center>
									</div>
									<div class="modal-body">
										<div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<form>
													<div class="form-group" style="margin-top: -14px;">
														<div class="col-md-12">
															<form class="form-horizontal">
																<div class="form-group">
																	<div class="col-md-12 col-sm-12 col-xs-12">
																		<input type="tel" class="form-control fields"
																			id="mobile" placeholder="Order Id"
																			required="required" maxlength="10" />
																	</div>
																</div>
																<div class="form-group">
																	<div class="col-md-12 col-sm-12 col-xs-12">
																		<select class="form-control fields" id="merchant">
																			<option>Select Merchant</option>
																			<option>2</option>
																			<option>3</option>
																			<option>4</option>
																		</select>
																	</div>
																</div>
																<div class="form-group">
																	<div class="col-md-12 col-sm-12 col-xs-12">
																		<input type="text" class="form-control fields"
																			id="amount" placeholder="Enter Amount"
																			required="required" maxlength="6" />
																	</div>
																</div>
																<div class="form-group">
																	<div class="col-md-12 col-sm-12 col-xs-12">
																		<textarea class="form-control fields" row="5"
																			id="comment" placeholder="Enter message here.."></textarea>
																	</div>
																</div>
																<div class="row">
																	<div class="col-md-12 col-sm-12 col-xs-12">
																		<center>
																			<button type="button" class="btn" disabled="disabled"
																				class="submit btn btn-primary modal_btn">Coming
																				Soon</button>
																		</center>
																	</div>
																</div>
															</form>
														</div>
													</div>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- Modal for Pay@strore -->
					</center>

					
					<center>
						<!-- Modal for Add Money -->
						<div class="modal fade" id="loadMoneyModal" role="dialog">
							<div class="modal-dialog">

								<!-- Modal content-->
								<div class="modal-content rchrg_modal">
									<div class="modal-header addm_head">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<center>
											<h2 class="modal-title">
												<b>Add Money</b>
											</h2>
										</center>
									</div>
									<div class="modal-body">
										<div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12">
											    <form action="${pageContext.request.contextPath}/User/LoadMoney/Voucher/Process" onsubmit="return validate();" method="post">
															<div class="group_1">
																<input type="text" name="voucherNumber" id="voucherNum" autocomplete="off"
																	class="form-control" style="margin-right: 50px;" minlength="16" maxlength="16" required="required" placeholder="Enter 16-digit Voucher Number">
															    <p id="error_voucherNumber" class="error" style="color: red"></p>
															</div>
															<button type="submit" class="btn" id="voucherLoadMoney"
																style="background: #8cc84e; color: #FFFFFF;">LoadMoney</button>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- Modal for Add Money -->
					</center>

					<center>
						<!-- Modal for Others -->
						<div class="modal fade" id="other" role="dialog">
							<div class="modal-dialog">

								<!-- Modal content-->
								<div class="modal-content rchrg_modal">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<!-- <center><h2 class="modal-title"><b>Add Money</b></h2></center> -->
									</div>
									<div class="modal-body">
										<div class="row">
											<%-- <div class="col-md-4">
												<a class="btn btn-danger btn-icon input-block-level"
													href="<c:url value="/MeraEvents/AuthCode"/>"> <i
													class="fa fa-calendar fa-2x"></i>
													<div>Events</div>
												</a>
											</div> --%>
											<div class="col-md-4">
												<a class="btn btn-success btn-icon input-block-level"
													href=" <c:url value="/User/Adlabs/SelectEntertainment"/>">
													<i class="fa fa-motorcycle fa-2x"></i>
													<div>Adventure</div>
												</a>
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>
						<!-- Modal for Others -->
					</center>
					<center>
					<div id="giftCard_Submit" class="modal fade" role="dialog">
							<div class="modal-dialog">

								<!-- Modal content-->
								<!-- <a href="https://play.google.com/store/apps/details?id=in.msewa.vpayqwik&hl=en"> -->
								<img src="<c:url value='/resources/images/gift_cards.jpg'/>"
									class="img-responsive" width="100%" usemap="#banner" alt="">

								<map name="banner">
									<a
										href="https://itunes.apple.com/us/app/vpayqwik/id1207326500?ls=1&mt=8"
										target="_blank">
										<area shape="rect" coords="55,412,233,464">
									</a>
									<a
										href="https://play.google.com/store/apps/details?id=in.msewa.vpayqwik&amp;hl=en"
										target="_blank">
										<area shape="rect" coords="248,413,425,464">
									</a>
								</map>
							</div>
							</div>
                     </center>

					<!-- Modal for Gift Cards -->
					<div class="modal fade" id="gift_card" role="dialog">
						<div class="modal-dialog gift_size">

							<!-- Modal content-->
							<div class="modal-content gift_modal">
								<div class="modal-header gift_head">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<center>
										<h2 class="modal-title">
											<b>Gift Cards</b>
										</h2>
									</center>
								</div>
								<div class="modal-body gift_wrap">
									<div class="row">
										<div class="col-lg-3 col-md-4 col-xs-6 thumb">
											<a class="thumbnail" href="#"> <img
												class="img-responsive"
												src="/resources/images/new_img/giftcards/g1.jpg" alt="">
											</a>
										</div>
										<div class="col-lg-3 col-md-4 col-xs-6 thumb">
											<a class="thumbnail" href="#"> <img
												class="img-responsive"
												src="/resources/images/new_img/giftcards/g2.png" alt="">
											</a>
										</div>
										<div class="col-lg-3 col-md-4 col-xs-6 thumb">
											<a class="thumbnail" href="#"> <img
												class="img-responsive"
												src="/resources/images/new_img/giftcards/g3.png" alt="">
											</a>
										</div>
										<div class="col-lg-3 col-md-4 col-xs-6 thumb">
											<a class="thumbnail" href="#"> <img
												class="img-responsive"
												src="/resources/images/new_img/giftcards/g4.png" alt="">
											</a>
										</div>
										<div class="col-lg-3 col-md-4 col-xs-6 thumb">
											<a class="thumbnail" href="#"> <img
												class="img-responsive"
												src="/resources/images/new_img/giftcards/g5.png" alt="">
											</a>
										</div>
										<div class="col-lg-3 col-md-4 col-xs-6 thumb">
											<a class="thumbnail" href="#"> <img
												class="img-responsive"
												src="/resources/images/new_img/giftcards/g6.png" alt="">
											</a>
										</div>
										<div class="col-lg-3 col-md-4 col-xs-6 thumb">
											<a class="thumbnail" href="#"> <img
												class="img-responsive"
												src="/resources/images/new_img/giftcards/g7.png" alt="">
											</a>
										</div>
										<div class="col-lg-3 col-md-4 col-xs-6 thumb">
											<a class="thumbnail" href="#"> <img
												class="img-responsive"
												src="/resources/images/new_img/giftcards/g8.png" alt="">
											</a>
										</div>
										<div class="col-lg-3 col-md-4 col-xs-6 thumb">
											<a class="thumbnail" href="#"> <img
												class="img-responsive"
												src="/resources/images/new_img/giftcards/g9.png" alt="">
											</a>
										</div>
										<div class="col-lg-3 col-md-4 col-xs-6 thumb">
											<a class="thumbnail" href="#"> <img
												class="img-responsive"
												src="/resources/images/new_img/giftcards/g10.png" alt="">
											</a>
										</div>
										<div class="col-lg-3 col-md-4 col-xs-6 thumb">
											<a class="thumbnail" href="#"> <img
												class="img-responsive"
												src="/resources/images/new_img/giftcards/g11.png" alt="">
											</a>
										</div>
										<div class="col-lg-3 col-md-4 col-xs-6 thumb">
											<a class="thumbnail" href="#"> <img
												class="img-responsive"
												src="/resources/images/new_img/giftcards/g12.png" alt="">
											</a>
										</div>
										<div class="col-lg-3 col-md-4 col-xs-6 thumb">
											<a class="thumbnail" href="#"> <img
												class="img-responsive"
												src="/resources/images/new_img/giftcards/g13.png" alt="">
											</a>
										</div>
										<div class="col-lg-3 col-md-4 col-xs-6 thumb">
											<a class="thumbnail" href="#"> <img
												class="img-responsive"
												src="/resources/images/new_img/giftcards/g14.png" alt="">
											</a>
										</div>
										<div class="col-lg-3 col-md-4 col-xs-6 thumb">
											<a class="thumbnail" href="#"> <img
												class="img-responsive"
												src="/resources/images/new_img/giftcards/g15.png" alt="">
											</a>
										</div>
										<div class="col-lg-3 col-md-4 col-xs-6 thumb">
											<a class="thumbnail" href="#"> <img
												class="img-responsive"
												src="/resources/images/new_img/giftcards/g16.png" alt="">
											</a>
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
					<!-- Modal for Gift Cards -->

					<!-- modal for order confirmation -->
					<div id="order_confirmation" class="modal fade" role="dialog">
						<div class="modal-dialog modal-sm">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h5 class="modal-title">Order Confirmation</h5>
								</div>
								<div class="modal-body">
									<table class="table table-condensed">
										<tr>
											<td><b>Topup </b></td>
											<td id="o_topup"></td>
										</tr>
										<tr>
											<td><b>Mobile </b></td>
											<td id="o_mobile"></td>
										</tr>

										<tr>
											<td><b>Operator </b></td>
											<td id="o_operator"></td>
										</tr>
										<tr>
											<td><b>Area </b></td>
											<td id="o_area"></td>
										</tr>
										<tr>
											<td><b>Amount</b></td>
											<td id="o_amount"></td>
										</tr>


									</table>
									<button type="button" class="btn btn-block btn-info"
										id="confirm_order">Confirm</button>
								</div>
							</div>
						</div>
					</div>

					<div id="order_confirmation_billpay" class="modal fade"
						role="dialog">
						<div class="modal-dialog modal-sm">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h5 class="modal-title">Order Confirmation</h5>
								</div>
								<div class="modal-body">
									<table class="table table-condensed">
										<tr>
											<td><b>Service </b></td>
											<td id="o_service"></td>
										</tr>
										<tr>
											<td><b id="o_account_name">Account Number </b></td>
											<td id="o_account_number"></td>
										</tr>
										<tr>
											<td><b>Bill Amount</b></td>
											<td id="o_bill_amount"></td>
										</tr>
										<tr>
											<td><b>Service Charge</b></td>
											<td id="o_service_amount"></td>
										</tr>
										<tr>
											<td><b>Net Amount</b></td>
											<td id="o_net_amount"></td>
										</tr>
									</table>
									<button type="button" class="btn btn-block btn-info"
										id="confirm_order_billpay">Confirm</button>
								</div>
							</div>
						</div>
					</div>


<!-- Flight -->

    <div class="modal fade" id="flightModal" role="dialog" hidden="hidden">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content" style="margin-top: 40%;">
       
        <div class="modal-body">
          <center>
		  <img src="${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/source-(tick).gif" style="width: 150px; margin-top: -91px;">
		<div style="background: white; margin-top: -20px;
		    padding: 2.5% 2%;">
		<h3><b>Your ticket is successfully booked.<br><small></b></small></h3>
		<center><button type="button" id="payment_ok">OK</button></center>
		</div></center>
        </div>
      </div>
    </div>
  </div>


  <!-- Modal -->
  <div class="modal fade" id="ErrmyModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content" style="margin-top: 40%;">
       
        <div class="modal-body">
          <center>
<%-- 		  <img src="${pageContext.request.contextPath}/resources/images/User/travel/img/Bus/search.gif" style="width: 150px; margin-top: -91px;"> --%>
        <div style="background: white; margin-top: -20px; padding: 2.5% 2%; padding-top: 32px;">
          <h3 id="payErrMsgVal"><b><br><small></b></small></h3>
          <center><button type="button" id="payment_fail">OK</button></center>
        </div></center>
        </div>
       
      </div>
      
    </div>
  </div>


						<!-- Modal for Browse Plans -->

						<div id="browse_plans_modal" role="dialog" class="modal fade">
							<div class="modal-dialog modal-lg">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<h2>
											<b>Choose Plan</b>
										</h2>
									</div>
									<div class="modal-body" id="browse_plans_body">
										<div class="row">
											<div class="col-md-12">
												<ul class="nav nav-tabs">
													<li class="active"><a data-toggle="tab" href="#3g"
														class="size">3G</a></li>
													<li><a data-toggle="tab" href="#2g" class="size">2G</a></li>
													<li><a data-toggle="tab" href="#special" class="size">Special</a></li>
													<li><a data-toggle="tab" href="#regular" class="size">Regular</a></li>
												</ul>
											</div>
										</div>
										<div class="tab-content">
											<div id="3g" class="tab-pane fade in active">
												<table id="3g_plans" class="table table-hover">
												</table>
											</div>
											<div id="2g" class="tab-pane fade">
												<table id="2g_plans" class="table table-hover">
												</table>
											</div>
											<div id="special" class="tab-pane fade">
												<table id="special_plans" class="table table-hover">
												</table>
											</div>
											<div id="regular" class="tab-pane fade">
												<table id="regular_plans" class="table table-hover">
												</table>
											</div>


										</div>


									</div>
								</div>
							</div>
						</div>


						<div id="browse_dc_plans_modal" role="dialog" class="modal fade">
							<div class="modal-dialog modal-lg">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<h2>
											<b>Choose Plan</b>
										</h2>
									</div>
									<div class="modal-body" id="browse_dc_plans_body">
										<div class="row">
											<div class="col-md-12">
												<ul class="nav nav-tabs">
													<li class="active"><a data-toggle="tab" href="#3g_dc"
														class="size">3G</a></li>
													<li><a data-toggle="tab" href="#2g_dc" class="size">2G</a></li>
												</ul>
											</div>
										</div>
										<div class="tab-content">
											<div id="3g_dc" class="tab-pane fade in active">
												<table id="3g_dc_plans" class="table table-hover">
												</table>
											</div>
											<div id="2g_dc" class="tab-pane fade">
												<table id="2g_dc_plans" class="table table-hover">
												</table>
											</div>


										</div>


									</div>
								</div>
							</div>
						</div>
					</center>

					<center>
						<!-- Modal Success Popup -->
						<div class="modal fade success-popup" id="myModal" tabindex="-1"
							role="dialog" aria-labelledby="myModalLabel">
							<div class="modal-dialog modal-sm" role="document">
								<div class="modal-content success_wrap">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal"
											aria-label="Close">
											<span aria-hidden="true">×</span>
										</button>
										<h4 class="modal-title" id="myModalLabel">Thank You !</h4>
									</div>
									<div class="modal-body text-center">
										<img src="img/success.jpg">
										<p class="lead">Contact form successfully submitted. Thank
											you, We will get back to you soon!</p>
									</div>
								</div>
							</div>
						</div>
					</center>


					<center>
						<!-- Modal for Change Password -->
						<div class="modal fade" id="change_pwd" role="dialog">
							<div class="modal-dialog">

								<!-- Modal content-->
								<div class="modal-content rchrg_modal">
									<div class="modal-header gas_head">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<center>
											<h2 class="modal-title">
												<b>Change Password</b>
											</h2>
										</center>
									</div>
									<div class="modal-body">
										<div class="row">
											<div class="col-md-12 col-sm-12 col-xs-12">
													<div class="form-group">
														<p class="label label-danger" id="fpOTP_message"></p>
													    <p class="label label-success" id="success_mobile_sm"></p>
														<div class="col-md-12">
															<form method="post" action="#">
																<div class="form-group">
																	<c:out value="${message}" default="" escapeXml="true" />
																	<div class="col-md-12 col-sm-12 col-xs-12">
																		<input type="Password" class="form-control"
																			id="current_password" name="oldPassword"
																			placeholder="Current Password" minlength="6"
																			maxlength="6"/> <span
																			class="fa fa-eye-slash" id="old_pwd_eye"
																			style="float: right; margin-top: -44px; margin-right: 8px; font-size: 15px;"></span>
																		<p class="error" id="error_current_password"
																			style="color: red"></p>
																	</div>
																</div>
																<div class="form-group">
																	<div class="col-md-12 col-sm-12 col-xs-12">
																		<input type="Password" class="form-control" id="new_password"
																			name="newPassword" placeholder="New Password"
																			minlength="6" maxlength="6" /> <span
																			class="fa fa-eye-slash" id="new_pwd_eye"
																			style="float: right; margin-top: -44px; margin-right: 8px; font-size: 15px;"></span>
																		<p class="error" id="error_new" style="color: red"></p>
																	</div>
																</div>
																<div class="form-group">
																	<div class="col-md-12 col-sm-12 col-xs-12">
																		<input type="Password" class="form-control"
																			id="confirm_new_password" name="confirmPassword"
																			placeholder="Confirm Password" minlength="6"
																			maxlength="6"/> <span
																			class="fa fa-eye-slash" id="confirm_pwd_eye"
																			style="float: right; margin-top: -44px; margin-right: 8px; font-size: 15px;"></span>
																		<p class="error" id="error_confirm" style="color: red"></p>
																	</div>
																</div>
																			<button type="button" id="update_password" class="btn btn-primary"
																				style="background-color: #ffc000; border-color: #ffc000">Update</button>
															</form>
														</div>
													</div>
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>
						<!-- Modal for Change Password -->
					</center>

					<div id="successNotification" role="dialog" class="modal fade">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
								</div>
								<div class="modal-body">
									<center id="success_alert" class="alert alert-success"></center>
								</div>
							</div>
						</div>
					</div>

					<center>
						<!-- Modal for date Picker -->
						<div class="modal fade" id="Date_picker0" role="dialog">
							<div class="modal-dialog modal-sm">

								<!-- Modal content-->
								<div class="modal-content rchrg_modal">
									<div class="modal-header" style="background: #005cac;">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<center>
											<h2 class="modal-title">Statistics</h2>
										</center>
									</div>
									<div class="modal-body">
										<div class="row">
											<center>
												<form action="#" method="post">
													<div class="col-md-5">
														<input type="text" id="fromDate1" name="startDate"
															class="form-control" placeholder="StartDate" readonly />
													</div>
													<div class="col-md-5">
														<input type="text" id="toDate1" name="endDate"
															class="form-control" placeholder="EndDate" readonly />
													</div>
													<div class="col-md-2">
														<button type="button" id="dateFilterReceipt" class="btn btn-primary">
															<span class="fa fa-filter"></span>
														</button>
													</div>
												</form>
											</center>
										</div>
									</div>
								</div>

							</div>

						</div>
						<!-- Modal for DTH -->
					</center>

					<%-- 					<center>
						<!-- Modal for filter -->
						<div class="modal fade" id="receiptModal2" role="dialog">
							<div class="modal-dialog">

								<!-- Modal content-->
								<div class="modal-content rchrg_modal">
									<div class="modal-header dth_head">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<center>
											<h2 class="modal-title">
												<b>Transaction History</b>
											</h2>
										</center>
									</div>
									<div class="modal-body">
										<div class="row">
											<form>
											<div class="col-md-5"><input type="text" class="form-control numeric" id="dob" placeholder="From" required="required"/></span>
											</div><div class="col-md-1"><b>To</b></div>
											<div class="col-md-5"><input type="text" class="form-control numeric" id="dob" placeholder="To" required="required"/></span>
											</div>
											<div class="col-md-12 col-sm-12 col-xs-12">
	          							<center><button type="buttton" class="submit btn btn-primary" style="background-color:#ffc000; border-color: #ffc000">Update</button></center>
	          						         </div>
											</form>
                                           						
										</div>
									</div>
								</div>

							</div>

						</div>
						<!-- Modal for DTH -->	
					</center> --%>
					<center>
						<!-- Modal for date Picker -->
						<div class="modal fade" id="Date_picker" role="dialog">
							<div class="modal-dialog modal-sm">

								<!-- Modal content-->
								<div class="modal-content rchrg_modal">
									<div class="modal-header" style="background: #005cac;">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<center>
											<h2 class="modal-title">Choose Date</h2>
										</center>
									</div>
									<div class="modal-body">
										<div class="row">
											<center>
												<form action="#" method="post" class="form form-horizontal">
													<div class="col-md-5">
														<input type="text" id="fromDate" name="startDate"
															class="form-control" placeholder="StartDate" readonly/>
													</div>
													<div class="col-md-5">
														<input type="text" id="toDate" name="endDate"
															class="form-control" placeholder="EndDate" readonly />
													</div>
													<div class="col-md-2">
														<button type="button" class="btn btn-primary" onclick="transactionListFilter('fromDate','toDate','0')">
															<span class="fa fa-filter"></span>
														</button>
													</div>
												</form>
											</center>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- Modal for date Picker -->
					</center>

<!-- Modal after error verification -->
		<div id="common_error" role="dialog" class="modal fade">
			<div class="modal-dialog">
				<div class="modal-content" style="width: 60%;margin-left: 20%;">
					<!-- <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div> -->
					<div class="modal-body">
						<center id="common_success" class="alert alert-danger"></center>
					</div>
				</div>
			</div>
		</div>

		<div id="common_success_true" role="dialog" class="modal fade">
			<div class="modal-dialog">
				<div class="modal-content" style="width: 60%;margin-left: 20%;">
					<!-- <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div> -->
					<div class="modal-body">
						<center id="common_success_msg" class="alert alert-success"></center>
					</div>
				</div>
			</div>
		</div>

					<!-- /Modal End Here==========================================================================================================
	================================================================================================================== -->

					<!-- 					<script type="text/javascript"
						src="/resources/js/new_js/jquery-ui-1.10.3.custom.min.js"></script>
					-->
					
					<script type="text/javascript"
						src="/resources/js/new_js/bootstrap.min.js"></script>

					<!-- 		<!-- Script for date picker -->
					<!-- 			<script>
			  $(function() {
			   $( "#toDate" ).datepicker({
			    format:"yyyy-mm-dd"
			   });
			   $( "#fromDate" ).datepicker({
			    format:"yyyy-mm-dd"
			   });
			  });
			</script> -->

	<script type="text/javascript">
	$(document).ready(function() {
		function disableBack() {
			window.history.forward();
		}
		window.onload = disableBack();
		window.onpageshow = function(evt) {
			if (evt.persisted)
				disableBack();
		}
	});
	
</script>

<script type="text/javascript">
function clearvalue(val) {
	$("#" + val).text("");

}
</script>

<script type="text/javascript">
	function loadValidate() {
		$("#error_amount").html();
		var spinnerUrl = "Please wait <img src='/resources/images/spinner.gif' height='20' width='20'>"
		var valid = true;
		var amount = $('#amountVal').val();
		var vpAddress = $('#vpAddress').val();
		var radioValue = $("input[name='useVnetWeb']:checked").val();
		/* var expDate = $('#upi_exp_dt').val(); */
		console.log("Amount : " + amount);
		console.log("vpAddress : " + vpAddress);
		console.log("radioValue : " + radioValue);
		/* console.log("expDate : " + expDate); */
		
		if(amount.length <= 0){
			$("#error_amount").html("Enter Amount");
			valid = false;
		}
		
		if(radioValue === "Upi"){
			
			var today = new Date();
			var dd = today.getDate();
			var mm = today.getMonth()+1;
			var yyyy = today.getFullYear();
			var hh = today.getHours();
			var min = today.getMinutes();
			
			if(vpAddress.length == 0){
				$("#error_vpAddress").html("Enter Virtual Address");
				valid = false;
			} 
			
			/* if(expDate.length == 0){
				$("#error_vpdate").html("Select Transaction Exp Date");
				  valid = false;
			}else{
				var curdate = expDate.split(" ");
				console.log("Current Date :: " + curdate[0]);
				console.log("Current Time :: " + curdate[1]);
				
				var curtime = curdate[1].split(":");
				console.log("Current Hour :: " + curtime[0]);
				console.log("Current Minut :: " + curtime[1]);
				console.log("hh :: : " + hh);
				console.log("min :: : " + min); 
				
				if(!hh < curtime[0]){
					$("#error_vpdate").html("Please Select Valid Exp Date");
					  valid = false;
				}else{
					$("#error_vpdate").html("Please Select ");
					  valid = false;
				}
			} */
		} 
		
		/* */
		if(valid == true) {
			if(radioValue === "Upi"){
		       //$("#loadMId").attr("disabled","disabled");
			   $("#loadMId").html(spinnerUrl);  
			} 
			return valid;
		}
		return valid;
	}
	
</script>

		<script>
		$(document).ready(function() {		
			App.setPage("user_profile");  //Set current page
			App.init(); //Initialise plugins and elements
		});
	</script>

					<script>
						$(".myAvatar").click(function() {
							$("#newAvatar").trigger("click");
						});
					</script>
					<script type="text/javascript">

function successAlert(){
    $("#success_pre_topup").fadeTo(2000, 500).slideUp(500, function(){
   $("#success_pre_topup").slideUp(500);
    });   
}
     function dangerAlert(){
         $("#error_pre_topup").fadeTo(2000, 500).slideUp(500, function(){
        $("#error_pre_topup").slideUp(500);
         });   
     }

</script>
					<script>
						$("#PrepaidSubMenu").click(function() {
							$("#pre_mobile").focus();

							$("#post_operator").val("#");
							$("#post_circle").val("#");
							$("#dc_operator").val("#");
							$("#dc_circle").val("#");

							$("#error_post_topup").html("");
							$("#success_post_topup").html("");
							$("#error_post_mobile").html("");
							$("#error_post_operator").html("");
							$("#error_post_circle").html("");
							$("#error_post_amount").html("");
							$("#error_dc_topup").html("");
							$("#success_dc_topup").html("");
							$("#error_dc_mobile").html("");
							$("#error_dc_operator").html("");
							$("#error_dc_circle").html("");
							$("#error_dc_amount").html("");

							$("#pre_amount").val("");
							$("#pre_mobile").val("");
							$("#pre_operator").val("#");
							$("#pre_circle").val("#");
							$("#dc_mobile").val("");
							$("#dc_operator").val("#");
							$("#dc_circle").val("#");
							$("#dc_amount").val("");
							$("#post_mobile").val("");
							$("#post_operator").val("#");
							$("#post_circle").val("#");
							$("#post_amount").val("");
							$("#plan_link").html("");
							$("#plan_link_dc").html("");
						});

						$("#PostpaidSubMenu").click(function() {
							/*  $("#post_mobile").focus(); */

							$("#pre_operator").val("#");
							$("#pre_circle").val("#");
							$("#dc_operator").val("#");
							$("#dc_circle").val("#");

							$("#error_pre_topup").html("");
							$("#success_pre_topup").html("");
							$("#error_pre_mobile").html("");
							$("#error_pre_operator").html("");
							$("#error_pre_circle").html("");
							$("#error_pre_amount").html("");
							$("#error_dc_topup").html("");
							$("#success_dc_topup").html("");
							$("#error_dc_mobile").html("");
							$("#error_dc_operator").html("");
							$("#error_dc_circle").html("");
							$("#error_dc_amount").html("");

							$("#pre_amount").val("");
							$("#pre_mobile").val("");
							$("#pre_operator").val("#");
							$("#pre_circle").val("#");
							$("#post_mobile").val("");
							$("#post_operator").val("#");
							$("#post_circle").val("#");
							$("#post_amount").val("");
							$("#dc_mobile").val("");
							$("#dc_operator").val("#");
							$("#dc_circle").val("#");
							$("#dc_amount").val("");
							$("#plan_link").html("");
							$("#plan_link_dc").html("");
						});

						$("#DataCardSubMenu").click(function() {
							/* $("#dc_mobile").focus(); */

							$("#pre_operator").val("#");
							$("#pre_circle").val("#");
							$("#post_operator").val("#");
							$("#post_circle").val("#");

							$("#error_pre_topup").html("");
							$("#success_pre_topup").html("");
							$("#error_pre_mobile").html("");
							$("#error_pre_operator").html("");
							$("#error_pre_circle").html("");
							$("#error_pre_amount").html("");
							$("#error_post_topup").html("");
							$("#success_post_topup").html("");
							$("#error_post_mobile").html("");
							$("#error_post_operator").html("");
							$("#error_post_circle").html("");
							$("#error_post_amount").html("");

							$("#dc_mobile").val("");
							$("#dc_operator").val("#");
							$("#dc_circle").val("#");
							$("#dc_amount").val("");
							$("#post_mobile").val("");
							$("#post_operator").val("#");
							$("#post_circle").val("#");
							$("#post_amount").val("");
							$("#plan_link").html("");
							$("#plan_link_dc").html("");
						});

						function clearvalue(val) {
							$("#" + val).text("");

						}
					</script>

					<script>
	function clearvalue(val){
		$("#"+val).text("");
	}
	function clearDTH(val1,val2,val3){
		$("#"+val1).text("");
		$("#"+val2).text("");
		$("#"+val3).text("");
	}
	
	function clearLandline(val1,val2,val3,val4,val5,val6){
		$("#"+val1).text("");
		$("#"+val2).text("");
		$("#"+val3).text("");
		$("#"+val4).text("");
		$("#"+val5).text("");
		$("#"+val6).text("");
	}
	function clearEcity(val1,val2,val3,val4,val5,val6,val7,val8){
		$("#"+val1).text("");
		$("#"+val2).text("");
		$("#"+val3).text("");
		$("#"+val4).text("");
		$("#"+val5).text("");
		$("#"+val6).text("");
		$("#"+val7).text("");
		$("#"+val8).text("");
	}
	function clearGas(val1,val2,val3,val4){
		$("#"+val1).text("");
		$("#"+val2).text("");
		$("#"+val3).text("");
		$("#"+val4).text("");
	}
	function clearInsurance(val1,val2,val3,val4){
		$("#"+val1).text("");
		$("#"+val2).text("");
		$("#"+val3).text("");
		$("#"+val4).text("");
	}
	</script>


					<script type="text/javascript">
						$("#old_pwd_eye").click(function(){
							var value = $("#current").attr("type");
							if(value == "password") {
								$(this).attr("class", "fa fa-eye");
								$("#current").attr("type", "text");
							}
							if(value == "text"){
								$(this).attr("class", "fa fa-eye-slash");
								$("#current").attr("type", "password");
							}

						});
						$("#new_pwd_eye").click(function(){
							var value = $("#new").attr("type");
							if(value == "password") {
								$(this).attr("class", "fa fa-eye");
								$("#new").attr("type", "text");
							}
							if(value == "text"){
								$(this).attr("class", "fa fa-eye-slash");
								$("#new").attr("type", "password");
							}

						});
					$("#confirm_pwd_eye").click(function(){
						var value = $("#confirm_new").attr("type");
						if(value == "password") {
							$(this).attr("class", "fa fa-eye");
							$("#confirm_new").attr("type", "text");
						}
						if(value == "text"){
							$(this).attr("class", "fa fa-eye-slash");
							$("#confirm_new").attr("type", "password");
						}

					});
					</script>

					<script>
						$(document).ready(function() {
							$(".menu_pill").click(function() {
								var id = $(".menu_pill").attr("id");
								console.log("ID :: " + id);
								switch (id) {
								case "menu1":
									$("#pre_mobile").focus();
									break;
								case "menu2":
									$("#usr1").focus();
									break;
								case "menu3":
									$("#usr2").focus();
									break;
								default:
									break;
								}
							});
						});
					</script>

					<script>
						function numbersonly(myfield, e, dec) {
							var key;
							var keychar;
							if (window.event)
								key = window.event.keyCode;
							else if (e)
								key = e.which;
							else
								return true;
							keychar = String.fromCharCode(key);

							if ((key == null) || (key == 0) || (key == 8)
									|| (key == 9) || (key == 13) || (key == 27))
								return true;

							else if ((("0123456789").indexOf(keychar) > -1))
								return true;

							else if (dec && (keychar == ".")) {
								myfield.form.elements[dec].focus();
								return false;
							} else
								return false;
						}
					</script>

				<!-- Flight -->
					<script type="text/javascript">
					$("#payment_ok").click(function() {
						window.location.href='${pageContext.request.contextPath}/User/Travel/Flight/MyTickets'; 
					});

					 $("#payment_fail").click(function() {
						window.location.href='${pageContext.request.contextPath}/User/Travel/Flight/MyTickets';  
					}); 
					
				var sc=	$('#flightSusMsg').val();
				var fl=	$('#flightflMsg').val();
				
				var successMsg = document.getElementById("flightSusMsg").value;
				console.log("successMsg :: " + successMsg);
				if (!successMsg.length == 0) {
					/* $("#flightSusMsg").val(""); */
					document.getElementById("flightSusMsg").value="";
					
					$('#flightModal').modal({
				        backdrop: 'static',
				        keyboard: true, 
				        show: true
					});
					
				}
			
				if (fl!="") {
					$('#flightflMsg').val("");
					document.getElementById("flightflMsg").value="";
					$('#payErrMsgVal').html(fl);	
					$('#ErrmyModal').modal("show");
					
				}
					</script>
			   <!-- Flight -->
</body>
</html>