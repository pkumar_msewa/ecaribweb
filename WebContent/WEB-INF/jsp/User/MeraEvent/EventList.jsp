<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage=""%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html lang="en">
<head>
<meta charset="utf-8">
<sec:csrfMetaTags/>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>VPayQwik | Event Lists</title>
	<link rel="stylesheet" href="<c:url value="/resources/css/font-awesome.min.css"/>">
	<link href='<c:url value='/resources/css/font-family.css'/>'
	rel='stylesheet' type='text/css'>
<link rel="stylesheet"
	href="<c:url value='/resources/css/font-awesome.css'/>" type='text/css'>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!-- Optional theme -->
<link rel="stylesheet"
	href="<c:url value='/resources/css/bootstrap-theme.min.css'/>" type='text/css'>

<link href="<c:url value="/resources/css/css_style.css"/>"
	rel='stylesheet' type='text/css'>
<link rel="stylesheet"
	href="<c:url value='/resources/css/bootstrap.css'/>" type='text/css'>
<script
	src="<c:url value='/resources/js/jquery.js'/>"></script>
<script
	src="<c:url value='/resources/js/bootstrap.min.js'/>"></script>

<script type="text/javascript"
	src="<c:url value="/resources/js/userdetails.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/header.js"/>"></script>
<link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'
	type="image/png" />
<link href='<c:url value="/resources/css/css_style.css"/>'
	rel='stylesheet' type='text/css'>
	<style>
		.no-js #loader {
			display: none;
		}

		.js #loader {
			display: block;
			position: absolute;
			left: 100px;
			top: 0;
		}

		.se-pre-con {
			position: fixed;
			left: 0px;
			top: 0px;
			width: 100%;
			height: 100%;
			z-index: 9999;
			background: url(/images/pq_large.gif) center no-repeat #fff;
		}
		
		
		
         .colorho:hover{
    background: rgb(245, 242, 242);
    }
        </style>

	<script src="<c:url value='/resources/js/modernizr.js'/>"></script>

	<script type="text/javascript">
		$(window).load(function() {
			$(".se-pre-con").fadeOut("slow");
		});
	</script>
	
</head>

<body onload="start()">
	<div class="se-pre-con"></div>
    <jsp:include page="/WEB-INF/jsp/User/Header.jsp" />
	<jsp:include page="/WEB-INF/jsp/User/Menu.jsp" />

	<!-----------------end navbar---------------------->

	<!------------- end main-------------------->

	<div class="background"></div>
	<!---blue box---->

	<div class="container" id="aboutus" style=" margin-top: -80px;">
		<div class="row">
<%-- 			<div class="col-md-3" id="Prepaid">
			<jsp:include page="/WEB-INF/jsp/User/MeraEvent/LeftPanel.jsp" />
				<!---form---->
				<div class="tab-content" id="sendmoney">
					<div id="menu1" class="tab-pane fade"><br>
					<code>${message}</code>
						<form method="post"
							action="${pageContext.request.contextPath}/User/UpdatePassword/Process">

							<div class="group_1">
								<input type="password" id="current" name="oldPassword"  minlength="6" maxlength="6" required>
								<i class="fa fa-eye-slash" id="pwd_eye" style="float: right;margin-top: -25px;margin-right: 50px;"></i>
								<span class="highlight"></span> <span class="bar"></span> <label>Current
									Password</label>
								<p>${cherror.password}</p>
							</div>

							<div class="group_1">
								<input type="password" id="new" name="newPassword" minlength="6" maxlength="6" required><i class="fa fa-eye-slash" id="new_pwd_eye" style="float: right;margin-top: -25px;margin-right: 50px;"></i> <span
									class="highlight"></span> <span class="bar"></span> <label>New
									Password</label>
								<p>${cherror.newPassword}</p>
							</div>

							<div class="group_1">
								<input type="password" id="confirm_new" name="confirmPassword" minlength="6" maxlength="6" required>
								<i class="fa fa-eye-slash" id="cnf_pwd_eye" style="float: right;margin-top: -25px;margin-right: 50px;"></i>
								<span class="highlight"></span> <span class="bar"></span> <label>Confirm
									New Password</label>
								<p>${cherror.confirmPassword}</p>
							</div>

								<sec:csrfInput/>
							<button type="submit" class="btn"
								style="width: 80%; background: #ff0000; color: #FFFFFF;">Update Password</button>
						</form>
					</div>
				</div>

			</div> --%>
			<!----end col-md-4-->
<!-- EVENTS -->
			<div class="col-md-12">
					<h2> ${msg} </h2>
						<c:forEach items="${eventList}" var="u" varStatus="loopCounter">
						 <div class="col-md-6 colorho" style="border: solid 1px #17bcc8; padding: 10px; margin-right: -1px; margin-top: 10px;">
								<div style="background: #17bcc8;">
                                <img src="${u.bannerImage}" class="img-responsive"/>
								<h4><b><a style="color: white; margin-left: 10px;" href="${pageContext.request.contextPath}/MeraEvents/${u.eventId}/EventDetails">
								<c:out value="${u.title}" escapeXml="true" default="" /></a></b></h4>
							<hr style="margin-bottom: 4px; margin-top: 5px;">
								<div class="pull-left"><c:out value="${u.cityName}" escapeXml="true" default="" /><br>								
								<b><c:out value="${u.registrationType}" escapeXml="true" default="" /></b>
								</div><div class="pull-right"><b>Date:</b> <c:out value="${u.startDate}" escapeXml="true" default="" /><br>
								<i><c:out value="${u.categoryName}" escapeXml="true" default="" /></i></div>
								<%-- <c:out value="${u.endDate}"
										escapeXml="true" default="" /> --%>
					    	</div>
					    	</div>
					    	
					    	
						</c:forEach>
						</div>
			</div>
			<!-- END EVENTS LIST -->


		</div>
		<!-----end col-md-8-->
	<!---end row-->
	<!----end container-->
	<jsp:include page="/WEB-INF/jsp/User/Footer.jsp" />
	<script src="http://code.jquery.com/jquery-2.2.1.min.js"></script>
<script
	src="<c:url value='/resources/js/bootstrap.js'/>"></script>
	
<style>
         .colorho:hover{
    background: rgb(245, 242, 242);
    }
        </style>
</body>
</html>



