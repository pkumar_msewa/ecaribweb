<%@ page contentType="text/html; charset=utf-8" language="java"
	import="java.sql.*"  isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<link rel="stylesheet"
	href="<c:url value='/resources/css/font-awesome.min.css'/>" type='text/css'>
<link href='<c:url value='/resources/css/font-family.css'/>'
	  rel='stylesheet' type='text/css'>
	  <body  onload="start()">
				<div class="box hidden-xs">
				</div>
				<ul class="nav nav-pills" style="margin-left: 87px">
					<li class="active"><a data-toggle="pill" href="#home"></i> Events</a></li>
				</ul>
				<div class="tab-content" id="sendmoney">
					<div id="home" class="tab-pane fade in active"><br>
						<form modelAttribute="events" method="post"action="${pageContext.request.contextPath}/MeraEvents/EventList">
							<div class="group_1">
									<select name="categoryId" id="category_id" 
									class="form-control" style="border-radius: 0; width: 86%; border: transparent;border-bottom: gray;border-style: solid;border-width: 1.8px; font-family: 'Ubuntu', sans-serif; padding-left: 0; height: 55px; margin-top: -19px; font-weight: bold; color: #928F8F; box-shadow: none;">
									<option value="#">Select Category</option>
									<!-- <option value="0">All categories</option> -->
								</select>
							</div>
							<div class="group_1">
									<select name="cityId" id="city_id" onchange="cityvalidation()"
									class="form-control" style="border-radius: 0; width: 86%; border: transparent;border-bottom: gray;border-style: solid;border-width: 1.8px; font-family: 'Ubuntu', sans-serif; padding-left: 0; height: 55px; margin-top: -19px; font-weight: bold; color: #928F8F; box-shadow: none;">
									<option value="#">Select City</option>
									<!-- <option value="0">All cities</option> -->
								</select>
							</div>
							<sec:csrfInput/>
					<div class="group_1">
							<button type="submit" class="btn" id="esubmit"
								style="width: 80%; background: #ff0000; color: #FFFFFF;" disabled="disabled">Search</button>
</div>
						</form>
					</div>
					
					<div id="menu1" class="tab-pane fade"><br>
					<code>${message}</code>
						<form method="post"
							action="${pageContext.request.contextPath}/User/UpdatePassword/Process">
							${msg}

							<div class="group_1">
								<input type="password" id="current" name="oldPassword"  minlength="6" maxlength="6" required>
								<i class="fa fa-eye-slash" id="pwd_eye" style="float: right;margin-top: -25px;margin-right: 50px;"></i>
								<span class="highlight"></span> <span class="bar"></span> <label>Current
									Password</label>
								<p>${cherror.password}</p>
							</div>

							<div class="group_1">
								<input type="password" id="new" name="newPassword" minlength="6" maxlength="6" required><i class="fa fa-eye-slash" id="new_pwd_eye" style="float: right;margin-top: -25px;margin-right: 50px;"></i> <span
									class="highlight"></span> <span class="bar"></span> <label>New
									Password</label>
								<p>${cherror.newPassword}</p>
							</div>

							<div class="group_1">
								<input type="password" id="confirm_new" name="confirmPassword" minlength="6" maxlength="6" required>
								<i class="fa fa-eye-slash" id="cnf_pwd_eye" style="float: right;margin-top: -25px;margin-right: 50px;"></i>
								<span class="highlight"></span> <span class="bar"></span> <label>Confirm
									New Password</label>
								<p>${cherror.confirmPassword}</p>
							</div>

								<sec:csrfInput/>
							<button type="submit" 
								style="width: 80%; background: #ff0000; color: #FFFFFF;">Update Password</button>
						</form>
					</div>
				</div>
<script type="text/javascript">
	function start() {
		getCategory();
		getCities();
	}
	function getCategory() {
		$.ajax({
			type : "GET",
			contentType : "application/json",
			url : "${pageContext.request.contextPath}/MeraEvents/CategoryList",
			success : function(response) {
				var cur = new Array();
			 	cur = response.split("@@");
			 	var id = new Array();
				id = cur[0].split("#");
			 	var name = new Array();
				name = cur[1].split("$");
			 	var i;
				for (i = 2; i < id.length; i++) {
					var option = document.createElement("option");
					option.text = name[i];
					option.value = id[i];
					var select = document.getElementById("category_id");
					if(i==0) {
						/* option.text = "Select Any Category"; */
						option.value ="#";
						select.add(option);
					}
					select.appendChild(option);
				}
			}
		});
	}
	function getCities() {
		$.ajax({
			type : "POST",
			contentType : "application/json",
			url : "${pageContext.request.contextPath}/MeraEvents/CityList",
			success : function(response) {
				var cur = new Array();
			 	cur = response.split("@@");
			 	var id = new Array();
				id = cur[0].split("#");
			 	var name = new Array();
				name = cur[1].split("$");
			 	var i;
				for (i = 2; i < id.length; i++) {
					var option = document.createElement("option");
					option.text = name[i];
					option.value = id[i];
					var select = document.getElementById("city_id");
					if(i==0) {
						/* option.text = "Select Any City"; */
						option.value ="#";
						select.add(option);
					}
					select.appendChild(option);
				}
			}
		});
	}
	

	function cityvalidation(){
		
		document.getElementById("esubmit").disabled=false;
		
	}
	
</script>	
		
</body>
</html>