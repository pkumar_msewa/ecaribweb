<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	errorPage=""%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<html lang="en">
<head>
<meta charset="utf-8">
<sec:csrfMetaTags />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>VPayQwik | Event Lists</title>
<link rel="stylesheet"
	href="<c:url value="/resources/css/font-awesome.min.css"/>">
<link href='<c:url value='/resources/css/font-family.css'/>'
	rel='stylesheet' type='text/css'>
<link rel="stylesheet"
	href="<c:url value='/resources/css/font-awesome.css'/>" type='text/css'>
<!-- Optional theme -->
<link rel="stylesheet"
	href="<c:url value='/resources/css/bootstrap-theme.min.css'/>"
	type='text/css'>
<link href="<c:url value="/resources/css/css_style.css"/>"
	rel='stylesheet' type='text/css'>
<link rel="stylesheet"
	href="<c:url value='/resources/css/bootstrap.css'/>" type='text/css'>
<script src="<c:url value='/resources/js/jquery.js'/>"></script>
<script src="<c:url value='/resources/js/bootstrap.min.js'/>"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/userdetails.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/header.js"/>"></script>
<link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'
	type="image/png" />
<link href='<c:url value="/resources/css/css_style.css"/>'
	rel='stylesheet' type='text/css'>
</head>
<style>
.box2 {
	background: black;
	width: 20%;
	height: 30px;
}
</style>
<body onload="SaveAttendee()">
	<div class="se-pre-con"></div>
	<jsp:include page="/WEB-INF/jsp/User/Header.jsp" />
	<jsp:include page="/WEB-INF/jsp/User/Menu.jsp" />
	<!-----------------end navbar---------------------->
	<!------------- end main-------------------->
	<div class="background"></div>
	<!---blue box---->
	<div class="container" id="aboutus" style="margin-top: -80px;"><br>
		<h4><b>User Detail</b></h4>

		<input type="hidden" id="member" name="member" value="${quantity}">		
		<input type="hidden" id="event" name="event" value="${event}">
		
		
		

		<script>
			$(document).ready(function() {
				addinputFields();
			});
		</script>
		<script>
			function addinputFields() {
				var number = document.getElementById("member").value;
				var i = 0;
				var c = "";
				/* alert(number); */
				for (i = 1; i <= number; i++) {

					var cb = "<div class='col-md-4'><input style='width:100%;' id='fn"+i+"' class='seats' placeholder='Name'  type='text' 'required'></div><div class='col-md-4'><input style='width:100%;' id='mn"+i+"'class='seats' placeholder='Mobile Number' type='number' 'required'></div><div class='col-md-4' > <input style='width:100%;' id='em"+i+"'class='seats' placeholder='Email Id'  type='email' 'required'></div><br><hr>";
					c += cb;
				}
				/*  alert(c); */
				document.getElementById("seatsdiv").innerHTML = c;
			}

			function insert()
			{
				

				var fname="";
				var mnumber="";
				var email="";
				var number = document.getElementById("member").value;
				var eventId = document.getElementById("event").value;
				var i = 0;

				for (i = 1; i <= number; i++) {
					var fn = document.getElementById("fn" + i).value;
					var mobile = document.getElementById("mn" + i).value;
					var ema = document.getElementById("em" + i).value;
					 var atpos = ema.indexOf("@");
				      var dotpos = ema.lastIndexOf(".");
					
					 if(fn=="" || fn.length<3)
					    {
					      alert(" Please enter your name");
					     
					      return false;
					     
					    }
					
					 else if(mobile==null || mobile.length<10)
					    {
					      alert("Please eter 10 digit Mobile No");
					    
					      return false;
					    }
					    
					    else if(ema == "" || ema<1 || dotpos<atpos+2 )
					    {
					      alert("Please enter Valid Email Address");
					     
					      return false;
					    }
					fname+="#"+fn;
					mnumber+="@"+mobile;
					email+=","+ema;
					 
				}
				
				 var atpos = email.indexOf("@");
			      var dotpos = email.lastIndexOf(".");
			    
			    if(fname=="" || fname.length<3)
			    {
			      alert(" Please enter your name");
			     
			      return false;
			    }
			    else if(mnumber==null || mnumber.length<10)
			    {
			      alert("Please eter 10 digit Mobile No");
			    
			      return false;
			    }
			    
			    else if(email == "" || email<1 || dotpos<atpos+2 )
			    {
			      alert("Please enter Valid Email Address");
			     
			      return false;
			    }
				
				//alert(fname+""+mnumber+""+email+""+eventId)
			 $("#eventspiner").show();
				$.ajax({
					
					type : "POST",
					contentType : "application/json",
					url : "${pageContext.request.contextPath}/MeraEvents/SaveAttendee",
					
					 data : JSON.stringify({
						"fullName" : "" +fname+ "",
						"mobileNo" : "" +mnumber + "",
						"emailId" : "" +email + "",
						"eventId" : "" +eventId + "",
						
					}), 
					success : function(response) 
					{
						/* alert(response)  */
						//window.open('../../User/MeraEvent/ReviewBooking')
						
					 window.location = "../../ReviewBooking"; 

						
					}
				});
			}
		</script>

		<div class="row">
					<div id="seatsdiv"></div>
					<div class="pull-right"  style="display: none; margin-right: 40px;" id="eventspiner">
                  <img  src="/resources/images/s.gif" style="width:50px;"/>
                 </div><br>
<div class="col-md-3 pull-right">
					<button id="btn" style="width: 80%; margin-bottom:20px; background: #ff0000; color: #FFFFFF;" onclick="insert()">Submit</button>
                   </div><br><br>
				</form>
			</div>
	</div>		
	<jsp:include page="/WEB-INF/jsp/User/Footer.jsp" />
	
</body>
</html>