<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage=""%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html lang="en">
<head>
<meta charset="utf-8">
<sec:csrfMetaTags/>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>VPayQwik | Event Lists</title>
	<link rel="stylesheet" href="<c:url value="/resources/css/font-awesome.min.css"/>">
	<link href='<c:url value='/resources/css/font-family.css'/>'
	rel='stylesheet' type='text/css'>
<link rel="stylesheet"
	href="<c:url value='/resources/css/font-awesome.css'/>" type='text/css'>

<!-- Optional theme -->
<link rel="stylesheet"
	href="<c:url value='/resources/css/bootstrap-theme.min.css'/>" type='text/css'>

<link href="<c:url value="/resources/css/css_style.css"/>"
	rel='stylesheet' type='text/css'>
<link rel="stylesheet"
	href="<c:url value='/resources/css/bootstrap.css'/>" type='text/css'>
<script
	src="<c:url value='/resources/js/jquery.js'/>"></script>
<script
	src="<c:url value='/resources/js/bootstrap.min.js'/>"></script>

<script type="text/javascript"
	src="<c:url value="/resources/js/userdetails.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/header.js"/>"></script>
<link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'
	type="image/png" />
<link href='<c:url value="/resources/css/css_style.css"/>'
	rel='stylesheet' type='text/css'>
</head>
<body onload="start()">
	<div class="se-pre-con"></div>
    <jsp:include page="/WEB-INF/jsp/User/Header.jsp" />
	<jsp:include page="/WEB-INF/jsp/User/Menu.jsp" />

	<!-----------------end navbar---------------------->

	<!------------- end main-------------------->

	<div class="background"></div>
	<!---blue box---->

	<div class="container book45" id="money_box" style="    height: 400px;">
		<div class="row">
<!-- FORM -->

	<div class="col-md-4">	
		<h2><b>Review Booking</b></h2>						
			<form method="get" action="${pageContext.request.contextPath}/MeraEvents/eventPayment">				
				
				Event Name : ${eventName}   <br />
				Event ID : ${eventId} <br />
				Ticket ID : ${ticketId}      <br />
				No of Tickets Booked :  ${noOfTicket}   <br />
				Address : ${address}	<br />
				Total Amount To Be Paid :${totalamount}  <br /><br />
				<button type="submit" class="btn" style="width: 80%; background: #ff0000; 
				color: #FFFFFF;">Pay</button>	<br>
                <span style=" color:red;"> ${msg}</span>						
			</form>				
            
	      </div><br>
<style>
@media screen and (max-width: 1024px) {
	.book45{
		height: 328px !important;}
	}
</style>	
	
<!-- END FORM -->

<div class="col-md-8 hidden-xs"  >
				<div class="slider" id="slider"
					style="margin-right: -15px; margin-left: -15px; margin-top: -20px;">
					<div class="carousel slide hidden-xs" data-ride="carousel"
						id="mycarousel">
					<div class="carousel-inner">
							<div class="item active" id="slide1">
								<img src='<c:url value="/resources/images/event_booking.jpg"/>' />
							</div>
							
						</div>
						
					</div>
					
				</div>
				
			</div>		
		</div>
		
		
		
	</div>



	<jsp:include page="/WEB-INF/jsp/User/Footer.jsp" />
	<script src="http://code.jquery.com/jquery-2.2.1.min.js"></script>
<script
	src="<c:url value='/resources/js/bootstrap.js'/>"></script>


</body>
</html>