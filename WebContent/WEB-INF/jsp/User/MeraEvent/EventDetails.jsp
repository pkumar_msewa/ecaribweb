<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" language="java" errorPage=""%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html lang="en">
<head>
<meta charset="utf-8">
<sec:csrfMetaTags/>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>VPayQwik | Event Lists</title>
	<link rel="stylesheet" href="<c:url value="/resources/css/font-awesome.min.css"/>">
	<link href='<c:url value='/resources/css/font-family.css'/>'
	rel='stylesheet' type='text/css'>
<link rel="stylesheet"
	href="<c:url value='/resources/css/font-awesome.css'/>" type='text/css'>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!-- Optional theme -->
<link rel="stylesheet"
	href="<c:url value='/resources/css/bootstrap-theme.min.css'/>" type='text/css'>

<link href="<c:url value="/resources/css/css_style.css"/>"
	rel='stylesheet' type='text/css'>
<link rel="stylesheet"
	href="<c:url value='/resources/css/bootstrap.css'/>" type='text/css'>
<script
	src="<c:url value='/resources/js/jquery.js'/>"></script>
<script
	src="<c:url value='/resources/js/bootstrap.min.js'/>"></script>

<script type="text/javascript"
	src="<c:url value="/resources/js/userdetails.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/header.js"/>"></script>
<link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'
	type="image/png" />
<link href='<c:url value="/resources/css/css_style.css"/>'
	rel='stylesheet' type='text/css'>
	<style>
		.no-js #loader {
			display: none;
		}

		.js #loader {
			display: block;
			position: absolute;
			left: 100px;
			top: 0;
		}

		.se-pre-con {
			position: fixed;
			left: 0px;
			top: 0px;
			width: 100%;
			height: 100%;
			z-index: 9999;
			background: url(/images/pq_large.gif) center no-repeat #fff;
		}
		
		.error{
		color: red;
		}
	</style>
	<script src="<c:url value='/resources/js/modernizr.js'/>"></script>

	<script type="text/javascript">
		$(window).load(function() {
			$(".se-pre-con").fadeOut("slow");
		});
	</script>
	
</head>

<body onload="start()">
	<div class="se-pre-con"></div>
    <jsp:include page="/WEB-INF/jsp/User/Header.jsp" />
	<jsp:include page="/WEB-INF/jsp/User/Menu.jsp" />

	<!-----------------end navbar---------------------->

	<!------------- end main-------------------->

	<div class="background"></div>
	<!---blue box---->

	<div class="container" id="aboutus" style=" margin-top: -80px;">
		<div class="row">
			<%-- <div class="col-md-3" id="Prepaid">
			<jsp:include page="/WEB-INF/jsp/User/MeraEvent/LeftPanel.jsp" />
				<!---form---->
				<div class="tab-content" id="sendmoney">
					<div id="menu1" class="tab-pane fade"><br>
					<code>${message}</code>
						<form method="post"
							action="${pageContext.request.contextPath}/User/UpdatePassword/Process">
							${msg}

							<div class="group_1">
								<input type="password" id="current" name="oldPassword"  minlength="6" maxlength="6" required>
								<i class="fa fa-eye-slash" id="pwd_eye" style="float: right;margin-top: -25px;margin-right: 50px;"></i>
								<span class="highlight"></span> <span class="bar"></span> <label>Current
									Password</label>
								<p>${cherror.password}</p>
							</div>

							<div class="group_1">
								<input type="password" id="new" name="newPassword" minlength="6" maxlength="6" required><i class="fa fa-eye-slash" id="new_pwd_eye" style="float: right;margin-top: -25px;margin-right: 50px;"></i> <span
									class="highlight"></span> <span class="bar"></span> <label>New
									Password</label>
								<p>${cherror.newPassword}</p>
							</div>

							<div class="group_1">
								<input type="password" id="confirm_new" name="confirmPassword" minlength="6" maxlength="6" required>
								<i class="fa fa-eye-slash" id="cnf_pwd_eye" style="float: right;margin-top: -25px;margin-right: 50px;"></i>
								<span class="highlight"></span> <span class="bar"></span> <label>Confirm
									New Password</label>
								<p>${cherror.confirmPassword}</p>
							</div>

								<sec:csrfInput/>
							<button type="submit" class="btn"
								style="width: 80%; background: #ff0000; color: #FFFFFF;">Update Password</button>
						</form>
					</div>
				</div>

			</div> --%>
			<!----end col-md-4-->
<!-- EVENTS Details-->
			<div class="col-md-12">
							<div class="col-md-12">	
							<div style="background: #e4edf4;">
								<img src="${ed.bannerImage}" style="width: 100%;"/>
								<%-- <img src="${ed.thumbImage}" style="width: 100%;"/>&nbsp&nbsp&nbsp --%>
								<%-- <a href="${pageContext.request.contextPath}/MeraEvents/${u.eventId}/EventDetails"><c:out
											value="${u.title}" escapeXml="true" default="" /></a><br /> --%>
								<%-- <c:out value="${ed.eventUrl}" escapeXml="true" default="" /> 
								<c:out value="${ed.url}" escapeXml="true" default="" /> 
							<c:out value="${ed.eventId}" escapeXml="true" default="" /> 	 --%>
							 	<h3>&nbsp&nbsp<b><c:out value="${ed.title}" escapeXml="true" default="" /></b></h3>
								&nbsp&nbsp&nbsp<i><b><c:out value="${ed.startDate}" escapeXml="true" default="" /></b> to
								<b><c:out value="${ed.endDate}" escapeXml="true" default="" /></b></i><br>
								&nbsp&nbsp&nbsp<c:out value="${ed.categoryName}" escapeXml="true" default="" />
								<c:out value="${ed.registrationType}" escapeXml="true" default="" />								
								<c:out value="${ed.venueName}" escapeXml="true" default="" />,
								<c:out value="${ed.cityName}" escapeXml="true" default="" />,
								<c:out value="${ed.stateName}" escapeXml="true" default="" />,
								<c:out value="${ed.countryName}" escapeXml="true" default="" /><br><br>
								<%-- <c:out value="${ed.booknowButtonValue}" escapeXml="true" default="" /> --%>
								
					    	</div>
					    </div>	
					    <br><br>
						<%-- <c:forEach items="${galleryList}" var="gl" varStatus="loopCounter">
							<div class="col-md-1">	
								<img src="${gl.imagePath}" style="width: 100%;"/>&nbsp&nbsp&nbsp
								<img src="${gl.thumbnailPath}" style="width: 100%;"/>&nbsp&nbsp&nbsp
								<a href="${pageContext.request.contextPath}/MeraEvents/${u.eventId}/EventDetails"><c:out
											value="${u.title}" escapeXml="true" default="" /></a><br />
					    	</div>
						</c:forEach> --%>
						
						<c:forEach items="${ticket}" var="u" varStatus="loopCounter">
							<div class="col-md-8" style="   margin-top: 10px;">	
								<div style="border-style: groove; border-color: rgba(221, 221, 221, 0.07);padding: 10PX 20PX 50PX 10PX;">
										<div class="col-xs-4">
									<h4><i><c:out value="${u.ticketName}" escapeXml="true" default="" /></i></h4><br>
									<h4><c:out value="${u.description}" escapeXml="true" default="" /></h4>							
								</div>
								<div class="col-xs-4">
								<h4><i class="fa fa-inr"></i> <c:out value="${u.ticketPrice}" escapeXml="true" default="" /></h4>	
								</div>
								<div class="col-xs-2">									
								<div class="group_1" id="${u.i}noOfTicketList">
									<select name="quantity" id="${u.i}quantity" onchange="calculateTotalAmount(${u.eventId},${u.ticketId},${u.i},${u.ticketPrice});"
									class="form-control" style="border-radius: 0; border: transparent;border-bottom: gray;border-style: solid;border-width: 1.8px; font-family: 'Ubuntu', sans-serif; padding-left: 0; font-weight: bold; color: #928F8F; box-shadow: none;">
									<option value="0">0</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<%-- <option value="#"><c:out value="${u.minOrderQuantity}" escapeXml="true" default="" /></option>
									<option value="0" ><c:out value="${u.maxOrderQuantity}" escapeXml="true" default="" /></option> --%>
								</select>
								<span class="error" id="${u.i}noOfTickets" ></span>
								</div>
								
							</div>
								
								</div>
									
							</div>
						
						
						<div class="col-md-4" style="margin-top: 10px;">
							<%-- <c:out value="${u.eventId}" escapeXml="true" default="" /> --%>
					 <b><h id="${u.i}totalamount"></h></b><br> 
							<c:out value="${totalAmount}" escapeXml="true" default="" />
			           <a  onclick="initiateBooking(${u.i},${u.eventId},${u.ticketId})" class="btn" style="width: 40%; background: #ff0000; color: #FFFFFF;">Book</a>
							
						</div>
						
						
						</c:forEach>
<%-- <jsp:include page="/WEB-INF/jsp/User/MeraEvent/RightPanel.jsp" />		 --%>			
				</div>
			</div>
<!-- END EVENTS DETAILS -->


		</div>
		<!-----end col-md-8-->
	</div>
	<!---end row-->
	<!----end container-->
	<jsp:include page="/WEB-INF/jsp/User/Footer.jsp" />
	<script src="http://code.jquery.com/jquery-2.2.1.min.js"></script>
<script
	src="<c:url value='/resources/js/bootstrap.js'/>"></script>
<script type="text/javascript">


/* var alldetail=""; */
	function calculateTotalAmount(eventId,ticketId,x,ticketPrice){
		var quantity = document.getElementById(x+"quantity").value;
			$("#"+x+"noOfTickets").html("");
		$.ajax({
			type : "GET",
			url : "${pageContext.request.contextPath}/MeraEvents/CalculateTotalAmount",
			data : "eventId=" + eventId + "&ticketId=" + ticketId + "&quantity=" + quantity + "&ticketPrice=" + ticketPrice,
			success : function(response) 
			{
				document.getElementById(x+"totalamount").innerHTML = "Total Amount: "+response;
				//alert(response);
			}
		});
		
	}
	
	function initiateBooking(x,eventid,ticketid){
		var contextpath = "${pageContext.request.contextPath}";
		//href="${pageContext.request.contextPath}/MeraEvents/${u.eventId}/${u.ticketId}/InitiateBooking"
		var noOfTickets = document.getElementById(x+"quantity").value;
		if (noOfTickets==0) {
			 $("#"+x+"noOfTicketList").focus();
			$("#"+x+"noOfTickets").html("please select no of ticket more than 0");
		}else{
			$("#"+x+"noOfTickets").html("");
			window.location = contextpath+"/MeraEvents/"+eventid+"/"+ticketid+"/InitiateBooking";
		}
	}
	
</script>	

</body>
</html>



