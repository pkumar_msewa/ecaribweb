<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	errorPage=""%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<html lang="en">
<head>
<meta charset="utf-8">
<sec:csrfMetaTags />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>VPayQwik | Settings</title>

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<link rel="stylesheet" href="/resources/css/new_css/cloud-admin.css">
    <link rel="stylesheet" href="/resources/css/new_css/daterangepicker-bs3.css">
    <link rel="stylesheet" href="/resources/css/new_css/default.css">
    <link rel="stylesheet" href="/resources/css/new_css/responsive.css">
    <link rel="stylesheet" href="/resources/css/new_css/uniform.default.min.css">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
<script src="<c:url value='/resources/js/jquery.js'/>"></script>
<script src="<c:url value='/resources/js/bootstrap.min.js'/>"></script>

<script type="text/javascript"
	src="<c:url value="/resources/js/userdetails.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/header.js"/>"></script>
<link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'
	type="image/png" />
	 <link rel="stylesheet"
          href="<c:url value="/resources/css/font-awesome.min.css"/>">
    <link href='<c:url value='/resources/css/font-family.css'/>'
          rel='stylesheet' type='text/css'>
    <link rel="stylesheet"
          href="<c:url value='/resources/css/font-awesome.css'/>" type='text/css'>

    <!-- Optional theme -->
    <link rel="stylesheet"
          href="<c:url value='/resources/css/bootstrap-theme.min.css'/>"
          type='text/css'>

    <link href="<c:url value="/resources/css/css_style.css"/>"
          rel='stylesheet' type='text/css'>
    <link rel="stylesheet"
          href="<c:url value='/resources/css/bootstrap.css'/>" type='text/css'>
<%-- <link href='<c:url value="/resources/css/css_style.css"/>'
	rel='stylesheet' type='text/css'> --%>
<style>
.no-js #loader {
	display: none;
}

.js #loader {
	display: block;
	position: absolute;
	left: 100px;
	top: 0;
}

.se-pre-con {
	position: fixed;
	left: 0px;
	top: 0px;
	width: 100%;
	height: 100%;
	z-index: 9999;
	background: url(/images/pq_large.gif) center no-repeat #fff;
}
</style>
<script src="<c:url value='/resources/js/modernizr.js'/>"></script>

<script type="text/javascript">
	$(window).load(function() {
		$(".se-pre-con").fadeOut("slow");
	});
</script>

</head>

<body>

	 <div class="se-pre-con"></div>
	<jsp:include page="/WEB-INF/jsp/User/Header.jsp" />

	<!-----------------end navbar---------------------->

	<!------------- end main-------------------->

	<!---blue box---->
	<div class="container prof_contain">
		<div class="col-md-6">
			<ul class="nav nav-tabs">
				<li><a data-toggle="tab" href="#password">Change Password</a></li>
				<li class="active"><a data-toggle="tab" href="#profile">Edit
						Profile</a></li>
			</ul>

			<div class="tab-content">
				<div id="profile" class="tab-pane fade in active">
					<form class="form-horizontal" modelAttribute="editUser"
						method="post"
						action="${pageContext.request.contextPath}/User/EditProfile/Process">
						<div class="col-md-12 form-vertical">
							<div class="box border inverse">
								<div class="box-title">
									<h4>
										<i class="fa fa-bars"></i>General Information
									</h4>
								</div>
								<div class="box-body big prof_box">
									<div class="row">
										<div class="col-md-12">
											<!-- <h4>Basic Information</h4> -->
											<div class="form-group">
												<!-- <label class="col-md-4 control-label">Name</label>  -->
												<div class="col-md-6">
													<input type="text" name="firstName" class="form-control"
														id="first_name_ep" required="required">
													<p class="error" id="error_first_name"></p>
												</div>
												<div class="col-md-6">
													<input type="text" name="lastName" class="form-control"
														id="last_name_ep" required="required">
													<p class="error" id="error_last_name"></p>
												</div>
											</div>
											<div class="form-group">
												<!-- <label class="col-md-4 control-label">Birthday</label>  -->
												<div class="col-md-6">
													<input type="text" class="form-control" name="address"
														id="address" required="required">
													<p class="error" id="error_address"></p>
												</div>
											</div>
											<sec:csrfInput />
											<center>
												<input type="submit" value="Update" id="esubmit"
													class="btn btn-primary modal_btn">
											</center>
										</div>
									</div><br><br>
									<center><code>${message}</code></center>
								</div>
							</div>
						</div>
					</form>
				</div>

				<div id="password" class="tab-pane fade">
					<form class="form-horizontal" method="post"
						action="${pageContext.request.contextPath}/User/UpdatePassword/Process">
						<div class="col-md-12 form-vertical">
							<div class="box border inverse">
								<div class="box-title">
									<h4>
										<i class="fa fa-bars"></i>Profile
									</h4>
								</div>
								<div class="box-body big prof_box">
									<div class="row">
										<div class="col-md-12">
											<!-- <h4>Change Password</h4> -->

											<div class="form-group">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<input type="password" id="current" name="oldPassword"
														minlength="6" maxlength="6" class="form-control"
														placeholder="Current Password">
													<p>${cherror.password}</p>
												</div>
												<div class="col-md-3"></div>
											</div>
											<div class="form-group">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<input class="form-control" type="password" id="new"
														name="newPassword" minlength="6" maxlength="6"
														placeholder="New Password">
													<p>${cherror.newPassword}</p>
												</div>
												<div class="col-md-3"></div>
											</div>
											<div class="form-group">
												<div class="col-md-3"></div>
												<div class="col-md-6">
													<input class="form-control" type="password"
														id="confirm_new" name="confirmPassword" minlength="6"
														maxlength="6" placeholder="Confirm Password">
													<p>${cherror.confirmPassword}</p>
												</div>
												<div class="col-md-3"></div>
											</div>

											<center>
												<input type="submit" value="Update Password"
													class="btn btn-primary modal_btn">
											</center>
										</div>
									</div>
									<code>${message}</code>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="col-md-6 hidden-xs">
			<div class="slider" style="margin-right: -15px; margin-left: -15px; margin-top: 72px; ">
				<div class="carousel slide hidden-xs" data-ride="carousel"
					id="mycarousel">
					<ol class="carousel-indicators">
						<li class="" data-slide-to="0" data-target="#mycarousel"></li>
						<li data-slide-to="1" data-target="#mycarousel" class="active"></li>
					</ol>

					<div class="carousel-inner">

						<div class="item" id="slide1">
							<img
								src="${pageContext.request.contextPath}/resources/images/slider_1.jpg">
						</div>
						<!---end item---->

						<div class="item active">

							<img
								src="${pageContext.request.contextPath}/resources/images/slider_2.jpg">
						</div>
						<!---end item---->
					</div>
					<!--end carousel inner------>

				</div>
				<!---end caeousel slider---->
			</div>
		</div>
	</div>
	<!---end row-->
	<!----end container-->
	<jsp:include page="/WEB-INF/jsp/User/Footer.jsp" />
	<script src="http://code.jquery.com/jquery-2.2.1.min.js"></script>
	<script type="text/javascript"
		src='<c:url value="/resources/js/wow.js"/>' />
	<script>
		new WOW().init();
	</script>
	<script src="<c:url value='/resources/js/bootstrap.js'/>"></script>

</body>
</html>



