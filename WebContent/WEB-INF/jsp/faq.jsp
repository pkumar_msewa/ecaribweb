<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<sec:csrfMetaTags/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>General Query</title>

<link rel="icon" href='<c:url value="/resources/images/favicon.png"/>' type="image/png" />
<script
	src="<c:url value='/resources/js/jquery.min.js'/>"></script>


<link rel="stylesheet" href="<c:url value='/resources/css/css_style.css'/>" />
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="<c:url value='/resources/css/animate.min.css'/>">
<link href='<c:url value='/resources/css/font-family.css'/>'
	rel='stylesheet' type='text/css'>

<link rel="stylesheet"
	href="<c:url value='/resources/css/font-awesome.css'/>" type='text/css'>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="<c:url value='/resources/css/bootstrap.min.css'/>" type='text/css'>
<!-- Optional theme -->
<link rel="stylesheet"
	href="<c:url value='/resources/css/bootstrap-theme.min.css'/>" type='text/css'>
<script
	src="<c:url value='/resources/js/bootstrap.js'/>"></script>
	<style>
		.no-js #loader {
			display: none;
		}

		.js #loader {
			display: block;
			position: absolute;
			left: 100px;
			top: 0;
		}

		.se-pre-con {
			position: fixed;
			left: 0px;
			top: 0px;
			width: 100%;
			height: 100%;
			z-index: 9999;
			background: url(/images/pq_large.gif) center no-repeat #fff;
		}
	</style>
	<script src="<c:url value='/resources/js/modernizr.js'/>"></script>

	<script type="text/javascript">
		$(window).load(function() {
			$(".se-pre-con").fadeOut("slow");
		});
	</script>

</head>
<body>
	<div class="se-pre-con"></div>
	  <!--   <div class="topline"></div> -->
<div class="se-pre-con"></div>
	<nav class="navbar navbar-default"
		style="min-height: 80px; margin-bottom: 0">
		<div class="container">
			<div class="navbar-header">
				<a class="navbar-brand" href="#"><img
					src="resources/images/vijayalogo.png" alt="logo" style="width: 230px; margin-top: 8px;" ></a>
			</div>	 	
		</div>
		<!-- /.container-fluid -->
	</nav>
	<div class="line" style="height: 4px; background: #17bcc8;"></div>
	
     <div class="container">
       <div class="container" id="aboutus">
            <div class="row"> 
            <h2>General Questions</h2>
             <hr style="width: 249px; float: left; border: solid; margin-top: 0; margin-left: 0px; color: #17bcc8;">
				<img src="resources/images/main/Terms & Conditions.jpg"
					class="img-responsive" alt="">
				<div class="col-md-12">
					<!-- <h2>General Questions</h2> -->
					<br><br><br>
					<div class="panel panel-primary">
						<!--1-->
						<div class="panel-body">
							<b>1. What is VPayQwik?</b>
						</div>
						</b>
						<div class="panel-footer">VPayQwik is a Digital wallet which allows you to Store, Transfer and Spend your money online with various merchants, and much more. For example with VPayQwik, you can top up your mobile, pay postpaid, landline and electricity bills, buy bus tickets, book for events and shop online.</div>
					</div>
					<div class="panel panel-primary">
						<!--2-->
						<div class="panel-body">
							<b>2. What are the benefits of VPayQwik Wallet?</b>
						</div>
						</b>
						<div class="panel-footer">
							VPayQwik Wallet is a smart new way to pay. Here's why <br>

							<ol type="I">
								<li><b>Safety-</b> Your VPayQwik Wallet is secured with a secret password and MPIN. Your MPIN will work as security pin just like an ATM pin, when you want to open VPayQwik app, you have to provide your MPIN for authorization of the app.</li>
								<li><b>Flexible-</b> You are not required to maintain a minimum balance in your VPayQwik Wallet. Load money whenever you want and spend it. We just help you to manage it better and pay quicker &amp; smarter.</li>
								<li><b>Instant-</b> VPayQwik Wallet Money transfer is instant. Its available 24X7 at your convenience. Using VPayQwik Wallet various payments can be made from anywhere, anytime to anyone in less than 10 seconds.</li>
								<li><b>Smart-</b> VPayQwik Wallet money gives you exclusive value to your payments. You get access to a multitude of offers from our merchants for both online and offline payments.</li>
							</ol>
						</div>
					</div>
					</p>

					<div class="panel panel-primary">
						<!--3-->
						<div class="panel-body">
							<b>3. How safe &amp; secure is VPayQwik Wallet?</b>
						</div>
						</b>
						<div class="panel-footer">VPayQwik Wallet is secured by the highest level of encryption. There are multiple layers of security implemented to protect your Wallet from any unauthorized access. Our three tier architecture is designed to prevent your data finance or privacy from being compromised. Whenever you try to open the app, you need to put a Personal Identification Number (MPIN) in to the application for authorization as an extra level of security.</div>
					</div>

					<div class="panel panel-primary">
						<!--4-->
						<div class="panel-body">
							<b>4. How do I sign up for a VPayQwik Wallet?</b>
						</div>
						</b>
						<div class="panel-footer">
							Signing up for VPayQwik Wallet is free and easy. You can opt to sign up in any of the following methods:<br> 
							<i class="fa fa-check"></i> Go to Google Play Store.<br> 
							<i class="fa fa-check"></i> Download VPayQwik -Mobile wallet app.<br>
							<i class="fa fa-check"></i> Fill the registration form through the Register option.<br>
							<center><b>OR</b></center>
							<i class="fa fa-check"></i> Open <a href="www.vpaqwik.com">www.vpayqwik.com</a><br> 
							<i class="fa fa-check"></i> Fill the registration form through the Register option.<br>

						</div>
					</div>

					<div class="panel panel-primary">
						<!--5-->
						<div class="panel-body">
							<b>5. If a transaction done through VPayQwik Wallet is not successful. Is my money refunded?</b>
						</div>
						</b>
						<div class="panel-footer">Yes, your money will be refunded when your transaction fails while paying through VPayQwik Wallet. The money automatically gets refunded to your Wallet within a 7-10 working days. For Refund please refer to the Refund section in FAQ</div>
					</div>

					<div class="panel panel-primary">
						<!--6-->
						<div class="panel-body">
							<b>6. How can I contact VPayQwik Wallet?</b>
						</div>
						</b>
						<div class="panel-footer">For any queries, suggestions or help contact us on Support at <a href="mailto:care@vpaqwik.com">care@vpaqwik.com</a> website or call our Customer Care number <b>08025011300</b> by tapping on the call tap in the app.</div>
					</div>

					<div class="panel panel-primary">
						<!--7-->
						<div class="panel-body">
							<b>7. Why should I open a Wallet when I can use my debit or credit card?</b>
						</div>
						</b>
						<div class="panel-footer">VPayQwik Wallet is completely a payment option, Just need to load money into it and do all the payments for example- Top up Recharge, DTH, Electricity Bill payment, Post Paid, Gas bill, Landline bill, fund transfer and many more. VPayQwik offers you an inbuilt platform where you can make your payments for most of your Utilities, Bills and even do shopping without actually having to go to the physical location and/or their site/app. </div>
					</div>

					<div class="panel panel-primary">
						<!--8-->
						<div class="panel-body">
							<b>8. Can I transfer funds to a phone number outside the country?</b>
						</div>
						</b>
						<div class="panel-footer">No, you can transfer funds only to phone numbers registered within India.</div>
					</div>

					<div class="panel panel-primary">
						<!--9-->
						<div class="panel-body">
							<b>9. How do I load my wallet?</b>
						</div>
						</b>
						<div class="panel-footer">
							On registering with VPayQwik you can load your Wallet in any of the following ways:<br>
							<i class="fa fa-check"></i> Credit card<br> 
							<i class="fa fa-check"></i> Debit card<br> 
							<i class="fa fa-check"></i> Internet Banking<br>
							<i class="fa fa-check"></i> UPI<br><br>
							Follow the steps to load money into your wallet: <br>
							<i class="fa fa-check"></i> Click on Load Money.<br> 
							<i class="fa fa-check"></i> Enter the amount you need to load.<br> 
							<i class="fa fa-check"></i> Click on Vnet Banking if you are a Vijaya Bank user or use Others.<br>
							<i class="fa fa-check"></i> Select type of Payment though the Payment Gateway and enter credentials.<br>
						</div>
					</div>
					<div class="panel panel-primary">
						<!--10-->
						<div class="panel-body">
							<b>10. I forgot my VPayQwik password. Now what?</b>
						</div>
						</b>
						<div class="panel-footer">
							You need not worry if you forgot your password. Follow the steps mentioned below:<br> 
							Reset your password through mobile app itself<br> 
							<i class="fa fa-check"></i> Click on forgot password tab.<br> 
							<i class="fa fa-check"></i> You will receive a verification code on your registered mobile number.<br>
							<i class="fa fa-check"></i> By entering the verification code, you can reset the password.<br> 
							<i class="fa fa-check"></i> If you want to change the password again. Go to setting at profile page ---- change password ---- Choose a new password<br> 
							
						</div>
					</div>
					<div class="panel panel-primary">
						<!--11-->
						<div class="panel-body">
							<b>11. I forgot my VPayQwik Wallet MPIN. What should I do?</b>
						</div>
						</b>
						<div class="panel-footer">
							You no need to worry if you forgot your MPIN. Just you need to do the following: <br>
							<i class="fa fa-check"></i> Click on forgot MPIN<br> 
							<i class="fa fa-check"></i> Enter your password and date of birth<br> 
						</div>
					</div>

					<div class="panel panel-primary">
						<!--12-->
						<div class="panel-body">
							<b>12. How do I redeem my reward points?</b>
						</div>
						</b>
						<div class="panel-footer">Once you do any transaction through VPayQwik, you will earn 1(One) reward point on each transaction of Rs.100. It will be shown in you profile page. In order to redeem those points, please contact us on Support at <a href="mailto:care@vpaqwik.com">care@vpaqwik.com</a> Wallet App or call our Customer Care number <b>08025011300</b> by tapping on the call tap mentioned in mobile app.</div>
					</div>

					<hr>
					<div class="panel panel-primary">
						<!--sign up-->
						<div class="panel-body panel-primary">
							<b>SIGN UP</b>
						</div>
						</b>
					</div>

					<div class="panel panel-primary">
						<!--1-->
						<div class="panel-body">
							<b>1. How do I Register?</b>
						</div>
						</b>
						<div class="panel-footer">
							Signing up for a VPayQwik Wallet is extremely easy. You can opt to sign up through any of the following methods:<br> 
							<i class="fa fa-check"></i> Go to Google Play Store.<br> 
							<i class="fa fa-check"></i> Download VPayQwik -Mobile wallet app.<br> 
							<i class="fa fa-check"></i> Fill the registration form by Register option.<br> 
							<center><b>Or</b></center>
							<i class="fa fa-check"></i> Open <a href="www.vpayqwik.com">www.vpayqwik.com</a><br>
							<i class="fa fa-check"></i> Fill the registration form by Register option.
						</div>
					</div>


					<div class="panel panel-primary">
						<!--2-->
						<div class="panel-body">
							<b>2. What information do I require to Sign Up?</b>
						</div>
						</b>
						<div class="panel-footer">We need your basic information i.e., your name, phone number, email ID for Sign up.</div>
					</div>

					<!-- <div class="panel panel-primary">
						<!--3--
						<div class="panel-body">
							<b>3. Where is my mobile verification code?</b>
						</div>
						</b>
						<div class="panel-footer">Your mobile verification code will
							be sent to your mobile phone number that you have provided to us
							during sign up.</div>
					</div>
					<div class="panel panel-primary">
						<!--4-->
						<div class="panel-body">
							<b>4. Can I get a new mobile verification code?</b>
						</div>
						</b>
						<div class="panel-footer">If you do not receive your
							verification code within 1 minute, click on the resend
							verification tab on the sign up page or call VPayQwik Wallet
							customer service by clicking on call tap in order to get your new
							mobile verification code</div>
					</div>

					<div class="panel panel-primary">
						<!--5--
						<div class="panel-body">
							<b>5. Do I need to sign up to download the VPayQwik Wallet
								Android Application?</b>
						</div>
						</b>
						<div class="panel-footer">No, you do not need to sign up to
							download the VPayQwik Wallet Android application. You can directly
							download the VPayQwik Wallet app by visiting Google Play store and
							then use it to sign up for an VPayQwik Wallet.</div>
					</div> -->
					<hr>
					<div class="panel panel-primary">
						<!--MY ACCOUNT-->
						<div class="panel-body panel-primary">
							<b>MY ACCOUNT</b>
						</div>
						</b>
					</div>


					<div class="panel panel-primary">
						<!--1-->
						<div class="panel-body">
							<b>1.  How do I change my VPayQwik Wallet profile information?</b>
						</div>
						</b>
						<div class="panel-footer">
							To update any of your profile information,
							<i class="fa fa-check"></i> First log in to your VPayQwik.<br>
							<i class="fa fa-check"></i> To change your profile information, click on Settings tab go to Edit Profile.<br>
							<i class="fa fa-check"></i> Date of birth, email id and mobile number cannot be changed once entered.<br>
						</div>
					</div>

					<div class="panel panel-primary">
						<!--3-->
						<div class="panel-body">
							<b>2. How to view Transaction history?</b>
						</div>
						</b>
						<div class="panel-footer">To view your transaction history, Click on statement of transactions.</div>
					</div>
					<div class="panel panel-primary">
						<!--4-->
						<div class="panel-body">
							<b>3. Does VPayQwik Wallet have an expiration date?</b>
						</div>
						</b>
						<div class="panel-footer">Any wallet not transacting for 12 months or more may be treated as expired and the value may be forfeited. Such wallet users will be intimated through SMS information to this effect in accordance with regulatory framework prevailing and amended from time to time.</div>
					</div>

					<!-- <div class="panel panel-primary">
						<!--5--
						<div class="panel-body">
							<b>4. Why is it a good idea to keep my money in my VPayQwik
								Wallet?</b>
						</div>
						</b>
						<div class="panel-footer">It's not a good but a great idea
							to keep money in your VPayQwik Wallet. If you're looking to send
							money to your friend, recharge your phone or pay your electricity
							bill you will not have to enter any card or bank account details
							to complete the transaction. In fact, there are many who are even
							uncomfortable doing so. Besides, with VPayQwik Wallet, you can
							make your transactions anytime, anywhere 24x7 using your mobile
							application</div>
					</div> -->

					<hr>
					<div class="panel panel-primary">
						<!--LOADING, SENDING AND WITHDRAWING MONEY-->
						<div class="panel-body panel-primary">
							<b>LOADING, SENDING AND WITHDRAWING MONEY</b>
						</div>
						</b>
					</div>

					<div class="panel panel-primary">
						<!--1-->
						<div class="panel-body">
							<b>1. How do I load money to my VPayQwik Wallet?</b>
						</div>
						</b>
						<div class="panel-footer">
							<i class="fa fa-check"></i> Click on Load Money.<br>
							<i class="fa fa-check"></i> Enter the amount you need to load.<br> 
							<i class="fa fa-check"></i> Click on Vnet Banking if you are a Vijaya Bank user or use Others.<br>
							<i class="fa fa-check"></i> Select type of Payment though the Payment Gateway and enter credentials.<br>
						</div>
					</div>

					<div class="panel panel-primary">
						<!--2-->
						<div class="panel-body">
							<b>2. How do I send money?</b>
						</div>
						</b>
						<div class="panel-footer">
							<i class="fa fa-check"></i> Log in to your VPayQwik Wallet.<br>
							<i class="fa fa-check"></i> Click on Fund Transfer.<br> 
							<i class="fa fa-check"></i> You can send money to any phone number in India regardless of the beneficiary being a registered VPayQwik user.<br>
							<i class="fa fa-check"></i> Enter the Contact number put the balance and then enter the Message.<br>
							<i class="fa fa-check"></i> Beneficiary will receive the instruction message and have to register with VPayQwik Wallet first to access the funds.<br>
						</div>
					</div>

					<div class="panel panel-primary">
						<!--3-->
						<div class="panel-body">
							<b>3. How do I withdraw money?</b>
						</div>
						</b>
						<div class="panel-footer">
							<i class="fa fa-check"></i> Log in to your VPayQwik Wallet <br>
							<i class="fa fa-check"></i> Click on Fund Transfer, select Bank Vijaya bank.<br> 
							<i class="fa fa-check"></i> Put IFSC code then account number.<br>
							<i class="fa fa-check"></i> Then enter the amount and click on Send button<br>
						</div>
					</div>

					<div class="panel panel-primary">
						<!--4-->
						<div class="panel-body">
							<b>4. Can I cancel send money transaction incase beneficiary is not registered with VPayQwik Wallet?</b>
						</div>
						</b>
						<div class="panel-footer">VPayQwik Wallet allows you to review every send money transaction before you confirm payment. Unfortunately, you cannot cancel a send money transaction once you have confirmed it after review. However, when you are sending money to a phone number or email Id, if your beneficiary does not open a Wallet within 15 to 20 days of your sending the money, the same is credited back into your VPayQwik Wallet.</div>
					</div>

					<div class="panel panel-primary">
						<!--5-->
						<div class="panel-body">
							<b>5. Can I send money to a person who is not a VPayQwik Wallet holder?</b>
						</div>
						</b>
						<div class="panel-footer">
							Yes, you can send money to a person who is not a VPayQwik Wallet holder. The person whom you choose to send the money to, will receive an SMS to sign up for a VPayQwik in order to accept the amount you are sending.<br> In case the person doesn't sign up with VPayQwik Wallet within the stipulated time of 15 to 20 days your Wallet will be credited back with that amount.
						</div>
					</div>

					<div class="panel panel-primary">
						<!--6-->
						<div class="panel-body">
							<b>6. How do I know when money has been received by the beneficiary?</b>
						</div>
						</b>
						<div class="panel-footer">You will receive a Confirmation SMS &amp; Email stating that the beneficiary has received the amount &amp; your VPayQwik Wallet has been debited.</div>
					</div>

					<hr>
					<div class="panel panel-primary">
						<!--RECHARGE & PAY BILLS-->
						<div class="panel-body panel-primary">
							<b>RECHARGE &amp; PAY BILLS</b>
						</div>
						</b>
					</div>

					<div class="panel panel-primary">
						<!--1-->
						<div class="panel-body">
							<b>1.  Can I recharge any mobile, DTH etc?</b>
						</div>
						</b>
						<div class="panel-footer">Yes, At VPayQwik Wallet, we support all operators' recharges across all circles in India. And DTH too.</div>
					</div>

					<div class="panel panel-primary">
						<!--2-->
						<div class="panel-body">
							<b>2. Can I pay any postpaid mobile bills through VPayQwik Wallet?</b>
						</div>
						</b>
						<div class="panel-footer">Absolutely! VPayQwik Wallet users can make their postpaid bill payments. All this convenience comes to you for a minimal convenience charge.</div>
					</div>


					<div class="panel panel-primary">
						<!--3-->
						<div class="panel-body">
							<b>3.  How do I know what I will get by paying a particular recharge amount?</b>
						</div>
						</b>
						<div class="panel-footer">We only display indicative browse plans for recharges. This keeps changing frequently; therefore, any updated information on recharge plans will be available only with your operator.</div>
					</div>

					<div class="panel panel-primary">
						<!--4-->
						<div class="panel-body">
							<b>4. How will I get to know whether my recharge or bill payment transaction has been successful?</b>
						</div>
						</b>
						<div class="panel-footer">
							Once you complete your two-steps process of recharging on VPayQwik mobile app, you will receive the following from us:<br> 
							<i class="fa fa-check"></i> Immediate Success/Failure response on your screen<br> 
							<i class="fa fa-check"></i> Receipt to your email and SMS on your mobile<br> 
							<i class="fa fa-check"></i> SMS from your operator<br>
							However, in case of Post Paid Bill Payment there may be some delay in few cases due to connectivity issues
						</div>
					</div>
					<div class="panel panel-primary">
						<!--5-->
						<div class="panel-body">
							<b>5. My recharge was successful, but I have not yet received my recharge.</b>
						</div>
						</b>
						<div class="panel-footer">At VPayQwik Wallet, all transaction requests are processed immediately. If you do not receive any confirmation SMS from your mobile operator, then kindly contact us on Support at VPayQwik Wallet App or call at our Customer Care number by tapping on the call tap mentioned in mobile app.</div>
					</div>


					<div class="panel panel-primary">
						<!--6-->
						<div class="panel-body">
							<b>6. When I was doing a recharge on mobile app, I got a response that my recharge was unsuccessful. What does this mean?</b>
						</div>
						</b>
						<div class="panel-footer">
							Possible reasons for such an error could be.<br> 
							<i class="fa fa-check"></i> Operator server was temporarily unavailable.<br> 
							<i class="fa fa-check"></i> Entered amount was not accepted by your mobile operator as a valid recharge value.<br>
							<i class="fa fa-check"></i> Sometimes it so happens that we are unable to connect to the mobile operator server due to a temporary network problem.<br> 
							We request you to try again after a while. If the problem persists, please contact us on Support at VPayQwik Wallet App or call at our Customer Care number by tapping on the call tap mentioned in mobile app. we will be happy to help.
						</div>
					</div>

					<div class="panel panel-primary">
						<!--7-->
						<div class="panel-body">
							<b>7. I have switched my operator through MNP. How can I now get a recharge done?</b>
						</div>
						</b>
						<div class="panel-footer">
							You can recharge your mobile with your new operator by following these simple steps on VPayQwik Mobile App.<br> 
							<b>Step 1:</b> Enter the mobile number.<br>
							<b>Step 2:</b> Select your current operator (if it's showing your previous operator).<br> 
							<b>Step 3:</b> Select your state<br> 
							<b>Step 4:</b> Enter recharge value.<br> 
							<b>Step 5:</b> Select your payment method.
						</div>
					</div>
					<hr>
					<div class="panel panel-primary">
						<!--PAYMENT-->
						<div class="panel-body panel-primary">
							<b>PAYMENT</b>
						</div>
						</b>
					</div>

					<div class="panel panel-primary">
						<!--1-->
						<div class="panel-body">
							<b>1. I do not have a credit card. How can I make a payment on VPayQwik Wallet?</b>
						</div>
						</b>
						<div class="panel-footer">
							You can choose any of the following options to make a payment on VPayQwik Wallet:<br> 
							<i class="fa fa-check"></i> VPayQwik Wallet Balance ( if your Wallet has been loaded with money)<br>
							<i class="fa fa-check"></i> Debit cards (MasterCard/ Visa cards)<br> 
							<i class="fa fa-check"></i> Internet banking<br> 
							We recommend you to load money into your VPayQwik Wallet for a simpler, safer and faster transaction on VPayQwik Wallet.
						</div>
					</div>

					<div class="panel panel-primary">
						<!--2-->
						<div class="panel-body">
							<b>2. How do I pay for my recharge on VPayQwik Wallet?</b>
						</div>
						</b>
						<div class="panel-footer">
							We have many payment options like:<br> 
							<i class="fa fa-check"></i> VPayQwik Wallet Balance (in case your Wallet is loaded with money)<br> 
							<i class="fa fa-check"></i> Debit cards (MasterCard/Visa cards)<br>
							<i class="fa fa-check"></i> Credit cards (MasterCard/Visa cards)<br>
							<i class="fa fa-check"></i> Internet Banking<br>
						</div>
					</div>

					<div class="panel panel-primary">
						<!--3-->
						<div class="panel-body">
							<b>3. What is an MPIN (Mobile Personal Identification Number)?</b>
						</div>
						</b>
						<div class="panel-footer">
							You need to go to the settings page and seta  4 numerical digit MPIN for e.g. 8574 which is difficult to guess for others but easy for you to remember. Your MPIN will work as a security password just like an ATM pin, when you want to open VPayQwik app you will have to provide your MPIN to open the app.<br>
						</div>
					</div>

					<div class="panel panel-primary">
						<!--4-->
						<div class="panel-body">
							<b>4. What is an OTP (One Time Password)?</b>
						</div>
						</b>
						<div class="panel-footer">
							One Time Password (OTP) has been introduced as an additional security feature and has been made mandatory by the Reserve Bank of India (RBI). OTP is intended to reduce the possibility of fraudulent money transfer transactions and to safeguard the customer. You will receive your OTP as an SMS when you’re making transactions via your card or bank account.<br>
						</div>
					</div>


					<hr>
					<div class="panel panel-primary">
						<!--REFUND-->
						<div class="panel-body panel-primary">
							<b>REFUND</b>
						</div>
						</b>
					</div>


					<div class="panel panel-primary">
						<!--1-->
						<div class="panel-body">
							<b>1. How do I initiate a refund?</b>
						</div>
						</b>
						<div class="panel-footer">
							Whenever you face such a situation, where you have made the payment but have not received the recharge, just write to us on Support at VPayQwik website or call at our Customer Care number by tapping on the call tap mentioned in mobile app. We will try to resolve it in the quickets possible time. No refund will be made in case of service fulfilment.<br>
						</div>
					</div>

					<div class="panel panel-primary">
						<!--2-->
						<div class="panel-body">
							<b>2. How long does it take to get a refund?</b>
						</div>
						</b>
						<div class="panel-footer">
							We initiate your request as soon as you inform us with the order number. In case of transactions through VPayQwik Wallet refund happens instantaneously. Otherwise refunds can take up to 7-10 working days from the date of your transaction as entities like banks , credit card companies have varied periods of time in which they process the refund. However, if you still have some queries feel free to write to us on Support at VPayQwik Wallet website or call at our Customer Care number by tapping on the call tap mentioned in mobile app.<br>
						</div>
					</div>

					<div class="panel panel-primary">
						<!--3-->
						<div class="panel-body">
							<b>3. I entered a wrong number. How can I get my money back?</b>
						</div>
						</b>
						<div class="panel-footer">
							If the operator has already processed the recharge or bill payment, the transaction will be considered as successful for the number you entered. We regret to inform that there isn't a way to get your money back.<br>
						</div>
					</div>

					<hr>
					<div class="panel panel-primary">
						<!--SECURITY-->
						<div class="panel-body panel-primary">
							<b>SECURITY</b>
						</div>
						</b>
					</div>

					<div class="panel panel-primary">
						<!--1-->
						<div class="panel-body">
							<b>1. Is VPayQwik Wallet safe and secure?</b>
						</div>
						</b>
						<div class="panel-footer">
							There are multiple layers of security implemented to protect your VPayQwik Wallet from any unauthorized access. Whenever you open app, you need to put a Personal Identification Number (MPIN) in your mobile application for authorization as an extra level of security. You are sent alerts on SMS and/or Email so that you are up to date on all activities occurring through your VPayQwik Wallet.<br>
						</div>
					</div>

					<div class="panel panel-primary">
						<!--2-->
						<div class="panel-body">
							<b>2. Will I be informed when my VPayQwik Wallet gets credited or debited?</b>
						</div>
						</b>
						<div class="panel-footer">
							 (If email is registered with us) will be sent whenever a transaction is processed from your Wallet.<br>
						</div>
					</div>

					<div class="panel panel-primary">
						<!--3-->
						<div class="panel-body">
							<b>3. What if I lose my mobile phone?</b>
						</div>
						</b>
						<div class="panel-footer">
							Even if you lose your mobile phone, no one will be able to access your VPayQwik Wallet without knowing your personal VPayQwik Wallet password. Since the password is not stored on the phone, your money is safe. However, do keep us informed by calling our Customer care executive, so that we can block your Wallet as further safeguard.<br>
						</div>
					</div>

					<div class="panel panel-primary">
						<!--4-->
						<div class="panel-body">
							<b>4. What if my registered email ID is hacked?</b>
						</div>
						</b>
						<div class="panel-footer">
							Your E-mail ID is just used as a reference for sending receipts / VPayQwik Wallet transaction summary. So in case your E-mail ID is hacked, there is little or no chance of your VPayQwik Wallet being misused.<br>
						</div>
					</div>

					<div class="panel panel-primary">
						<!--4-->
						<div class="panel-body">
							<b>5. Who is liable for any misuse of my VPayQwik Wallet if I lose my Password?</b>
						</div>
						</b>
						<div class="panel-footer">
							It is always advised to keep your password confidential and in case your account is accessed by any unauthorized person due to loss of password, VPayQwik team will not be liable for any misuse for your VPayQwik Wallet. For more details, please refer to our T&amp;C page.<br>
						</div>
					</div>

					<div class="panel panel-primary">
						<!--4-->
						<div class="panel-body">
							<b>6. What are my options if the Customer care mail or call is not being responded to ?</b>
						</div>
						</b>
						<div class="panel-footer">
							If you're customer care mail or call has not been responded to you ca get in touch with us by raising a ticket through the app by following the steps below:<br>
							<i class="fa fa-check"></i> Login into VPayQwik App<br> 
							<i class="fa fa-check"></i> Click on Top left corner menu button.<br>
							<i class="fa fa-check"></i> Click on Customer support button then Report issue option.<br>
							<i class="fa fa-check"></i> Click on drop down menu select the type of issue.<br>
							<i class="fa fa-check"></i> Describe your issue in the description menu then click on submit.<br>
						</div>
					</div>

					<hr>
					<div class="panel panel-primary">
						<!--BENEFITS-->
						<div class="panel-body panel-primary">
							<b>BENEFITS</b>
						</div>
						</b>
					</div>

					<div class="panel panel-primary">
						<!--1-->
						<div class="panel-body">
							<b>1. Can I book Bus/ Flight Tickets in this App?</b>
						</div>
						</b>
						<div class="panel-footer">
							Yes, you can a book a flight/ Bus with this app. Just need to follow a simple steps:<br>
							<i class="fa fa-check"></i> Click on Travel option from VPayQwik Wallet<br> 
							<i class="fa fa-check"></i> Select Bus or Flight.<br>
							<i class="fa fa-check"></i> Fill all the required field.<br>
							<i class="fa fa-check"></i> Click on proceed button.<br>
						</div>
					</div>

					<div class="panel panel-primary">
						<!--2-->
						<div class="panel-body">
							<b>2. Can I use Scan to pay for Merchant Payments?</b>
						</div>
						</b>
						<div class="panel-footer">
							 Yes, you can use the Scan to pay for Merchant Payments in following methods:<br>
							<i class="fa fa-check"></i> Login to the VPayQwik App.<br> 
							<i class="fa fa-check"></i> Click on Scan to Pay option and there you can see the option to scan the QR code of Merchant.<br>
							<i class="fa fa-check"></i> Click on Scan there you get the Merchant details.<br>
							<i class="fa fa-check"></i> Click on Pay button, and your payment is successful.<br>
						</div>
					</div>

					<div class="panel panel-primary">
						<!--3-->
						<div class="panel-body">
							<b>3. Can we book Giftcard from this App?</b>
						</div>
						</b>
						<div class="panel-footer">
							Yes, you can book Giftcard from this App, as we have multiple of vouchers.<br>
							<i class="fa fa-check"></i> Login to the VPayQwik App.<br> 
							<i class="fa fa-check"></i> Click on Giftcards option.<br>
							<i class="fa fa-check"></i> There we can choose any Vouchers and click on Purchase.<br>
							<i class="fa fa-check"></i> Select the Amount of Voucher<br>
							<i class="fa fa-check"></i> Enter the Name of recipient, Contact number and Mail ID.<br>
							<i class="fa fa-check"></i> Finally, the recipient will get the Giftcard Voucher in the Mail.<br>
						</div>
					</div>

					<div class="panel panel-primary">
						<!--4-->
						<div class="panel-body">
							<b>4. What is the Chat &amp; Pay feature on Vpayqwik?</b>
						</div>
						</b>
						<div class="panel-footer">
							Chat &amp; Pay is a new feature which is added in VPayQwik App by which you can shop by chatting and get the details.<br>
							By this Chat Bot you can book Cabs, Bus tickets , Movie tickets and Hotels etc.. in following steps:<br>
							<i class="fa fa-check"></i> Login to the VPayQwik App.<br>
							<i class="fa fa-check"></i> Click on Chat Bot and you can select any option which you required in this(Household Bills, Movie, Cabs, Events, Hotels, Bus And Laundry)<br>
							<i class="fa fa-check"></i> So that you can start chatting and get the required details.<br>
						</div>
					</div>

					<div class="panel panel-primary">
						<!--4-->
						<div class="panel-body">
							<b>5. What is Bharat QR?</b>
						</div>
						</b>
						<div class="panel-footer">
							<i class="fa fa-check"></i> Log in VPayQwik App<br>
							<i class="fa fa-check"></i> Click on Bharat QR icon<br>
							<i class="fa fa-check"></i> Scan the Bharat QR Code.<br>
							<i class="fa fa-check"></i> Click on scan button.<br>
							<i class="fa fa-check"></i> Enter the Amount then click on Pay button.<br>
							<i class="fa fa-check"></i> Verify MPIN your payment is done.<br>
						</div>
					</div>

					<div class="panel panel-primary">
						<!--4-->
						<div class="panel-body">
							<b>6. What is Adventure Feature in VPayQwik ?</b>
						</div>
						</b>
						<div class="panel-footer">
							<i class="fa fa-check"></i> Login into VPayQwik Wallet.<br> 
							<i class="fa fa-check"></i> Click on Adventure icon.<br>
							<i class="fa fa-check"></i> Tap the Explore button, select the Theme.<br>
							<i class="fa fa-check"></i> Choose the visit date then click on Ok button then Continue button.<br>
							<i class="fa fa-check"></i> Select Ticket or Package.<br>
							<i class="fa fa-check"></i> Click on proceed button.<br>
							<i class="fa fa-check"></i> Fill the required details and click on Pay button.<br>
							<i class="fa fa-check"></i> Your Ticket will be received in email.<br>
						</div>
					</div>



				</div>
				<!----end col-md-12------>
			</div>
			<!----end row----->
		</div>
		<!------end accolades-->
	</div>
	<!---------end About Us------------->









	<footer>
		<div class="help">
			<div class="container wow bounceIn" data-wow-duration="3s">
				<div class="row">
					<div class="col-sm-3">
						<img src="resources/images/main/help_1.png" alt="">
						<p>Help Line : 080 25011300 or care@vpayqwik.com</p>
					</div>

					<div class="col-sm-3">
						<img src="resources/images/main/help_2.png" alt="">
						<p>The security is our prime concern we ensure your money is
							secure</p>
					</div>

					<div class="col-sm-3">
						<img src="resources/images/main/help_3.png"
							style="padding-bottom: 13px;" alt="">
						<p>VPayQwik makes sure that your every transaction is 100% Safe</p>
					</div>

					<div class="col-sm-3">
						<img src="resources/images/main/help_4.png" alt="">
						<p>Spending is earning at VPayQwik We offer various deals to
							distribute happiness</p>
					</div>
				</div>
				<!---end row--->
			</div>
		</div>
		<!-----end help--->

		<div class="container">
			<div class="row">
				<!-- useful links -->
				<div class="col-sm-3 wow bounceInLeft" data-wow-duration="2s">
					<ul class="row footer-links">
						<p>
							<b>Mobile Recharges</b>
						</p>
						<a href="#">Airtel Recharge</a>|
						<a href="#">Vodafone Recharge</a>|
						<a href="#">Aircel Recharge</a>|
						<a href="#">Idea Recharge</a>|
						<a href="#">BSNL Recharge</a>|
						<a href="#">Reliance GSM</a>|
						<a href="#">Reliance CDMA</a>|
						<a href="#">Tata GSM</a>|
						<a href="#">Tata Docomo</a>|
						<a href="#">MTNL</a>|
						<a href="#">MTS</a>|
						<a href="#">T24 Mobile Recharge</a>
						</ul>
				</div>
				<div class="col-sm-3 wow bounceInLeft" data-wow-duration="2s">
					<ul class="row footer-links">
						<p>
							<b>DTH Recharges</b>
						</p>
						<a href="#">Airtel DTH Recharge</a>|
						<a href="#">Videocon D2H Recharge</a>|
						<a href="#">Tata Sky Recharge</a>|
						<a href="#">Reliance DTH Recharge</a>|
						<a href="#">Right Way</a>|
						<a href="#">Dish TV Recharge</a>|
						<a href="#">Sun Direct Recharge</a>
						</ul>
				</div>
				<div class="col-sm-3 wow bounceInRight" data-wow-duration="2s">
					<ul class="row footer-links">
						<p>
							<b>Datacard Recharges</b>
						</p>
						<a href="#">BSNL Datacard</a>|
						<a href="#">Reliance Netconnect</a>|
						<a href="#">Tata Photon</a>|
						<a href="#">Plus</a>|
						<a href="#">Tata Photon Whiz</a>|
						<a href="#">MTS Datacard</a>|
						<a href="#">MTS Blaze</a>|
						<a href="#">MTNL Datacard</a>
						</ul>
				</div>
				<div class="col-sm-3 wow bounceInRight" data-wow-duration="2s">
					<ul class="row footer-links">
						<p>
							<b>Landline Bill Payment</b>
						</p>
						<a href="#">BSNL Landline</a>|
						<a href="#">MTNL Landline Delhi</a>|
						<a href="#">Reliance</a>|
						<a href="#">Tata Docomo</a>
						</ul>
				</div>
			</div>
			<hr>
			<div class="row">
				<p>India's first complete payment application & website, VPayQwik
					is a quickest and safest way for Online Recharge, DTH or Datacard
					Recharge and make mobile or utility Bill Payments for Airtel,
					Aircel,BSNL, Docomo, Idea, MTNL, Vodafone or other operators.you do
					not need to rush to the market to make your DTH or mobile bill
					payments, just log-on to VPayQwik and experience the easiest &
					fastest method of recharges and payments.</p>
				<p>VPayQwik brings to you the various coupons, deals and offers
					to make your payment experience rich every day. Our online recharge
					and bill payment service give you reward points, which can be used
					to avail attractive and lucrative cash back and discount offers.
					Download VPayQwik from your App Store.</p>
				<p>VPayQwik also provides travel ticket service, through VPayQwik
					you can book Air tickes, Bus tickets, Hotels, Car Rental and
					Holiday Packages.Come and experience hassle-free, safe and fast
					ticketing services at VPayQwik.VPayQwik make sure that every
					transaction you do is risk free.</p>
			</div>
			<hr>
			<div class="row" id="footericons">
				<div class="col-sm-8  wow bounceInUp">
				<img src='<c:url value="/resources/images/msewalogo.png"/>' 
					width="100px"  alt="">
				</div>
				<!----end col-md-4--->

				<div class="col-sm-4  wow bounceInUp">
					<ul class="row footer-links">
						<a href="#"> <img src="resources/images/main/fb.gif"
							class="fb"></a>
						<a href="#"> <img src="resources/images/main/tw.gif"
							class="tw"></a>
						<a href="#"> <img src="resources/images/main/yu.gif"
							class="yu"></a>
						<a href="#"> <img src="resources/images/main/in.gif"
							class="in"></a>
					</ul>
				</div>
				<!----end col-md-4--->
				</center>

			</div>
			<!-----end row----->

		</div>
		<div class="menu" id="main">
		<div class="container">
			<div class="row" style="margin-top: 10px; margin-bottom: 10px;">
				<a href="Home">Home</a> <a href="AboutUs">About Us</a> <a
					href="PartnersWithUs">Partner with us</a> <a
					href="Terms&Conditions">Terms & Conditions</a><a href="PrivacyPolicy">Privacy Policy</a> <a href="Grievance">Grievance
					policy</a>  <a href="faq">General
			
					Questions</a>
			</div>
			<!---row---->
		</div>
		<!----container------->
	</div>
	</footer>
</body>
</html>