<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/new-css/custome.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/new-css/style.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/new-css/animate.min.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/new-css/et-line-font.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/new-css/font-awesome.min.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/new-css/dashlogin.css">
  <link rel="stylesheet" type="text/css" href="http://propeller.in/components/button/css/button.css">
  <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="stylesheet" type="text/css" href="http://propeller.in/components/icons/css/google-icons.css">
  
   <link href="https://fonts.googleapis.com/css?family=Asap" rel="stylesheet"><!--Fonts -->
   
<!-- contact section -->
<section id="contact">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-sm-12 text-center">
        <div class="section-title">
          <h1 class="heading bold">CONTACT US</h1>
          <hr>
        </div>
      </div>
      <div class="col-md-6 col-sm-12 contact-info">
        <h2 class="heading bold">CONTACT INFO:</h2>
        <div class="col-md-4 col-sm-4">
           <h3><i class="fa fa-envelope-o" aria-hidden="true" data-wow-delay="0.6s"></i> EMAIL</h3>
          <p>care@ecarib.com</p>
        </div>
        <div class="col-md-6 col-sm-4">
         <h3><i class="fa fa-mobile" aria-hidden="true" data-wow-delay="0.6s"></i> PHONE</h3>
          <p>080 25011300</p>
        </div>
        <div class="col-md-10 col-sm-6">
          <h3>Social Contacts:</h3>
          <ul class="social-icon">
            <!-- <li><a href="#" class="fa fa-facebook wow fadeIn" data-wow-delay="0.3s" target="_blank"></a></li>
            <li><a href="#" class="fa fa-twitter wow fadeIn" data-wow-delay="0.6s" target="_blank"></a></li>
            <li><a href="#" class="fa fa-youtube-play wow fadeIn" data-wow-delay="0.9s" target="_blank"></a></li> -->
            <li><a href="#" class="fa fa-facebook wow fadeIn" data-wow-delay="0.3s"></a></li>
            <li><a href="#" class="fa fa-twitter wow fadeIn" data-wow-delay="0.6s" ></a></li>
            <li><a href="#" class="fa fa-youtube-play wow fadeIn" data-wow-delay="0.9s" ></a></li>
          </ul>
        </div>
      </div>
      <div class="col-md-6 col-sm-12">
        <form action="#" method="get" class="wow fadeInUp" data-wow-delay="0.6s">
          <div class="col-md-6 col-sm-6">
            <input type="text" class="form-control" placeholder="Mobile Number" name="mobile">
          </div>
          <div class="col-md-6 col-sm-6">
            <input type="email" class="form-control" placeholder="Email" name="email">
          </div>
          <div class="col-md-12 col-sm-12">
            <textarea class="form-control" placeholder="Message..." rows="6" name="message"></textarea>
          </div>
          <div class="col-md-offset-2 col-md-8 col-sm-offset-4 col-sm-8">
            <input type="submit" class="form-control" value="SEND MESSAGE">
          </div>
        </form>
      </div>
    </div>
  </div>
</section>

<!-- footer section -->
<footer class="app_download" style="background:#e7be3e; color: #ffffff;">
  <div class="container">
    <div class="row" style="margin-top: 5px;">
      <div class="col-md-8 col-sm-8 col-xs-8 abt_cmpny" style="text-align: left; float: left; margin-top: 15px;">
<%--                 <p><a href="${pageContext.request.contextPath}/AboutUs" target="_blank">About Us</a> | <a href="${pageContext.request.contextPath}/PartnersWithUs" target="_blank">Partner with us</a> | <a href="${pageContext.request.contextPath}/Campaign" target="_blank">Campaign</a> | <a href="${pageContext.request.contextPath}/Terms&Conditions" target="_blank">Terms &amp; Conditions</a> | <a href="${pageContext.request.contextPath}/PrivacyPolicy" target="_blank">Privacy Policy</a> | <a href="${pageContext.request.contextPath}/Grievance" target="_blank">Grievance Policy</a> | <a href="${pageContext.request.contextPath}/faq" target="_blank">FAQs</a> | <a href="${pageContext.request.contextPath}/Blogs" target="_blank">Blogs</a></p>
 --%>    
     <p><a href="#">About Us</a> | <a href="#">Partner with us</a> | <a href="#">Campaign</a> | <a href="#">Terms &amp; Conditions</a> | <a href="#">Privacy Policy</a> | <a href="#">Grievance Policy</a> | <a href="#">FAQs</a> | <a href="#">Blogs</a></p>
 		   </div>
            <div class="col-md-4 col-sm-4 col-xs-4 app_wrap" style="padding:10px; text-align: right;">
            <p style="color: white;">Download our App:
               <!--  <a href="https://itunes.apple.com/in/app/vpayqwik/id1207326500?mt=8" target="_blank"><i class="fa fa-apple" style="font-size: 25px; padding: 0 8px 0 8px; color: white;"></i></a>
                <a href="https://play.google.com/store/apps/details?id=in.msewa.vpayqwik&hl=en" target="_blank"><i class="fa fa-android" style="font-size: 25px; padding: 0 8px 0 8px; color: white;"></i></a> -->
                 <a href="#"><i class="fa fa-apple" style="font-size: 25px; padding: 0 8px 0 8px; color: white;"></i></a>
                <a href="#"><i class="fa fa-android" style="font-size: 25px; padding: 0 8px 0 8px; color: white;"></i></a>
           
            </div></p>
    </div>
  </div>
</footer>

<!-- footer1 section -->
<footer class="footer_first" style="padding: 30px 0;">
  <div class="container">
    <div class="row pay_wrap">

      <div class="col-md-3 col-sm-6 pay_container">
        <h4>Mobile Recharge</h4><hr id="mrchrgline">
<!--           <p><a href="#">Airtel Mobile</a> | <a href="#">Aircel Mobile</a> | <a href="#">BSNL Mobile</a> | <a href="#">Idea Mobile</a> | <a href="#">Reliance GSM</a> | <a href="#">Reliance CDMA</a> | <a href="#">Tata Docomo CDMA</a> | <a href="#">Tata Docomo GSM</a> | <a href="#">MTNL Mobile</a> | <a href="#">MTS Mobile</a> | <a href="#">Reliance Jio</a> | <a href="#">Vodafone Mobile</a></p>
 -->      </div>

      <div class="col-md-3 col-sm-6 pay_container">
        <h4>Mobile Bill Payment</h4><hr id="mbillline">
<!--           <p><a href="#">Airtel Mobile</a> | <a href="#">Aircel Mobile</a> | <a href="#">BSNL Mobile</a> | <a href="#">Idea Mobile</a> | <a href="#">Reliance Mobile</a> | <a href="#">Tata Docomo Mobile</a> | <a href="#">MTNL Mobile</a> | <a href="#">MTS Mobile</a> | <a href="#">Reliance Jio</a> | <a href="#">Vodafone Mobile</a></p>
 -->      </div>

      <div class="col-md-3 col-sm-6 pay_container">
        <h4>DTH Bill Payment</h4><hr id="dbillline">
<!--           <p><a href="#">Airtel Digital Tv</a> | <a href="#">Dish Tv</a> | <a href="#">Reliance Digital Tv</a> | <a href="#">Sun Direct</a> | <a href="#">Tata Sky</a> | <a href="#">Videocon D2H</a></p>
 -->      </div>

      <div class="col-md-3 col-sm-6 pay_container landlne">
        <h4>Landline Bill Payment</h4><hr id="lbillline">
<!--           <p><a href="#">Airtel Landline</a> | <a href="#">BSNL Landline</a> | <a href="#">MTNL Landline - Delhi</a> | <a href="#">Reliance Landline</a> | <a href="#">Tata Docomo Landline</a></p>
 -->      </div>
    </div>

    <div class="row">
      <div class="col-md-3 col-sm-6 pay_container">
        <h4>Electricity Bill Payment</h4><hr id="ebillline">
<!--           <p><a href="#">TSECL - TRIPURA</a> | <a href="#">Torrent Power</a> | <a href="#">Tata Power - Delhi</a> | <a href="#">Southern Power</a> | <a href="#">Reliance Energy-MUMBAI</a> | <a href="#">Paschim Kshetra Vitaran - MP</a> | <a href="#">Odisha Discom</a> | <a href="#">Noida Power</a> | <a href="#">MSEDC - MAHARASHTRA</a> | <a href="#">Madhya Kshetra Vitaran - MP</a> | <a href="#">Jodhpur Vidyuth Vitran Nigam - RAJASTHAN</a></p>
 -->      </div>

      <div class="col-md-3 col-sm-6 pay_container">
        <h4>Gas Bill Payment</h4><hr id="gbillline">
<!--           <p><a href="#">Indraprastha Gas</a> | <a href="#">Mahanagar Gas</a> | <a href="#">Gujarat Gas</a> |<br> <a href="#">Adani Gas</a></p>
 -->      </div>

      <div class="col-md-3 col-sm-6 pay_container">
        <h4>Insurance Bill Payment</h4><hr id="ibillline">
<!--           <p><a href="#">ICICI Prudential Life Insurance</a> | <a href="#">Tata AIA Life Insurance</a> | <a href="#">IndiaFirst Life Insurance</a> | <a href="#">Bharati AXA Life Insurance</a></p>
 -->      </div>
    </div>
  </div>
</footer>

<!-- footer2 section
============================================================================= -->
<footer class="footer_last" style="background-color:#e7be3e;">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12 foot_title">
        <p>&copy; <a href="http://www.msewa.com/" target="_blank" style="color: #ffffff;">Msewa Software Pvt Ltd.</a> | 2018 All Rights Reserved.</p>
      </div>
      <!-- <div class="col-md-6 col-sm-6" style="text-align: right; float: right;">
        <p><a href="about.html" target="_parent">About Us</a> | <a href="faq.html" target="_parent">FAQs</a> | <a href="#" target="_parent">Terms &amp; Conditions</a> | <a href="#" target="_parent">Privacy Policy</a> | <a href="#" target="_parent">Gravence Policy</a></p>
      </div> -->
    </div>
  </div>
</footer>