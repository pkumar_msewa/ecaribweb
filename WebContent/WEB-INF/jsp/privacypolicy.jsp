<%@taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<sec:csrfMetaTags/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Privacy Policy</title>
<link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'
	type="image/png" />
	<script src="<c:url value='/resources/js/jquery.min.js'/>"></script>
	
<link rel="stylesheet"
	href="<c:url value='/resources/css/css_style.css'/>">
<link rel="stylesheet"
	href="<c:url value='/resources/css/animate.min.css'/>">
<link href='<c:url value='/resources/css/font-family.css'/>'
	rel='stylesheet' type='text/css'/>


<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="<c:url value='/resources/css/bootstrap.min.css'/>" type='text/css'>
<!-- Optional theme -->
<link rel="stylesheet"
	href="<c:url value='/resources/css/bootstrap-theme.min.css'/>" type='text/css'>
<script
	src="<c:url value='/resources/js/bootstrap.js'/>"></script>

	<style>
		.no-js #loader {
			display: none;
		}

		.js #loader {
			display: block;
			position: absolute;
			left: 100px;
			top: 0;
		}

		.se-pre-con {
			position: fixed;
			left: 0px;
			top: 0px;
			width: 100%;
			height: 100%;
			z-index: 9999;
			background: url(/images/pq_large.gif) center no-repeat #fff;
		}
	</style>
	<script src="<c:url value='/resources/js/modernizr.js'/>"></script>

	<script type="text/javascript">
		$(window).load(function() {
			$(".se-pre-con").fadeOut("slow");
		});
	</script>
</head>
<body>
	<div class="se-pre-con"></div>
	<nav class="navbar navbar-default"
		style="min-height: 80px; margin-bottom: 0">
		<div class="container">
			<div class="navbar-header">
				<a class="navbar-brand" href="#"><img
					src="resources/images/vijayalogo.png" alt="logo" style="width: 230px; margin-top: 8px;" ></a>
			</div>	 	
		</div>
		<!-- /.container-fluid -->
	</nav>
	<div class="line" style="height: 4px; background: #17bcc8;"></div>
	<!-----------------end navbar---------------------->
	<div class="container">
		<div class="container" id="aboutus">
			<div class="row">
			
				<h2>Privacy Policy</h2>
				<hr
					style="width: 182px; float: left; border: solid; margin-top: 0; margin-left: 0px; color: #17bcc8;">
				<img src="resources/images/privacypolicy.jpg" class="img-responsive"
					alt="">
				
					<hr>
					
				<div class="col-md-12">
					<p>Vijaya Bank built the VPayQwik app as free app. This SERVICE is provided by Vijaya Bank (at no cost) and is intended for use as is.
                      This page is used to inform website visitors regarding our policies with the collection, use, and disclosure of Personal Information if anyone decided to use our Service.
                        If you choose to use our Service, then you agree to the collection and use of information in relation to this policy. The Personal Information that we collect is used for providing and improving the Service. We will not use or share your information with anyone except as described in this Privacy Policy.
                            The terms used in this Privacy Policy have the same meanings as in our Terms and Conditions, which is accessible at VPayQwik unless otherwise defined in this Privacy Policy.</p>

					<hr>
                    
					<h4>Information Collection and Use</h4>				
					<p>For a better experience, while using our Service, we may require you to provide us with certain personally identifiable information [user’s name, address, location, pictures, mobile IMEI] The information that we request is will be retained by us and used as described in this privacy policy.
					The app does use third party services that may collect information used to identify you. Link to privacy policy of third party service providers used by the app:<br>
                             <a href="https://www.google.com/policies/privacy/" target="_blank" id="foot">1. Google Play Services<br></a>
                            <a href="https://uidai.gov.in/home/privacy-policy.html" target="_blank" id="foot"> 2. Unique Identification Authority of India</a><br>
	                         <a href="https://niki.ai/policy/" target="_blank" id="foot">3 .Niki AI</a><br>
	                         <a href="https://www.instantpay.in/policies/privacy" target="_blank" id="foot">4. Instant Pay</a><br>
	                        <a href="https://www.npci.org.in/privacy-and-security-policy" target="_blank" id="foot"> 5. National Payments Corporation of India</a><br>
	                        <a href="https://www.easemytrip.com/privacy-policy.html" target="_blank" id="foot"> 6. EaseMyTrip</a><br>
	                        <a href="http://treatcard.in/privacy-policy/" target="_blank" id="foot"> 7. TreatCard</a><br>
	                        <a href="https://www.housejoy.in/privacy-policy" target="_blank" id="foot"> 8. HouseJoy</a><br>
	                        <a href="http://www.appsfly.io/privacy-policy.html" target="_blank" id="foot"> 9. AppsFly</a><br>
	                        <a href="https://www.travelkhana.com/travelkhana/jsp/policy.jsp" target="_blank" id="foot">10. TravelKhana</a><br>
	                       <a href="https://www.adlabsimagica.com/privacy-policy/" target="_blank" id="foot"> 11. AdlabsImagica</a><br>
                       	   <a href="http://www.giftcardsindia.in/pages/corporate/shop-with-confidence/terms-and-condition" target="_blank" id="foot"> 12. Gift Cards India</a></p>
					<hr>
					
					<h4>Log Data</h4>
					<p>We want to inform you that whenever you use our Service, in a case of an error in the app we collect data and information (through third party products) on your phone called Log Data. This Log Data may include information such as your device Internet Protocol ("IP") address, device name, operating system version, the configuration of the app when utilizing our Service, the time and date of your use of the Service, and other statistics.</p>
					<hr>
                 
					<h4>Cookies</h4>
					<p>Cookies are files with small amount of data that is commonly used an anonymous unique identifier. These are sent to your browser from the website that you visit and are stored on your device internal memory.
                     This Service does not use these "cookies" explicitly. However, the app may use third party code and libraries that use "cookies" to collection information and to improve their services. You have the option to either accept or refuse these cookies and know when a cookie is being sent to your device. If you choose to refuse our cookies, you may not be able to use some portions of this Service.</p>
					<hr>
					
					<h4>Permissions</h4>
					<p>We require permissions and use certain third party services such as Google Analytics, Firebase for notifications, fetching contact number device identification number from user, auto-fetch and reading SMS for quick verification, etc. We do this to enhance the user experience and serve you better.</p>
					<hr>
					
					
					<h4>Service Providers</h4>
					<p>We may employ third-party companies and individuals due to the following reasons:<br>
					         1. To facilitate our Service;<br>
                             2. To provide the Service on our behalf;<br>
	                         3. To perform Service-related services; or<br>
	                         4. To assist us in analyzing how our Service is used.<br>
	                       We want to inform users of this Service that these third parties have access to your Personal Information. The reason is to perform the tasks assigned to them on our behalf. However, they are obligated not to disclose or use the information for any other purpose.</p>
					<hr>
                    
                    <h4>Security</h4>
					<p>We value your trust in providing us your Personal Information, thus we are striving to use commercially acceptable means of protecting it. But remember that no method of transmission over the internet, or method of electronic storage is 100% secure and reliable, and we cannot guarantee its absolute security.</p>
					<hr>

                    <h4>Links to Other Sites</h4>
					<p>This Service may contain links to other sites. If you click on a third-party link, you will be directed to that site. Note that these external sites are not operated by us. Therefore, we strongly advise you to review the Privacy Policy of these websites. We have no control over and assume no responsibility for the content, privacy policies, or practices of any third-party sites or services.</p>
					<hr>
					
					<h4>Children's Privacy</h4>
					<p>These Services do not address anyone under the age of 13. We do not knowingly collect personally identifiable information from children under 13. In the case we discover that a child under 13 has provided us with personal information, we immediately delete this from our servers. If you are a parent or guardian and you are aware that your child has provided us with personal information, please contact us so that we will be able to do necessary actions.</p>
					<hr>
					
					<h4>Changes to This Privacy Policy</h4>
					<p>We may update our Privacy Policy from time to time. Thus, you are advised to review this page periodically for any changes. We will notify you of any changes by posting the new Privacy Policy on this page. These changes are effective immediately after they are posted on this page.</p>

                 
                </div>
				</div>
				<!----end col-md-12------>
			</div>
			<!----end row----->
			<hr>
		</div>
		<!------end accolades-->

<%--		<div class="row">
			<div class="Accolades">
				<h1>Accolades</h1>
				<img src="resources/images/main/mobile.jpg" class="img-responsive"
					alt="chart">
			</div>
		</div>--%>
		<!---end row-->

	</div>
	<!---------end About Us------------->






	</div>



	<footer>
		<div class="help">
			<div class="container wow bounceIn" data-wow-duration="3s">
				<div class="row">
					<div class="col-sm-3">
						<img src="resources/images/main/help_1.png" alt="">
						<p>Help Line : 080 25011300 or care@vpayqwik.com</p>
					</div>

					<div class="col-sm-3">
						<img src="resources/images/main/help_2.png" alt="">
						<p>The security is our prime concern we ensure your money is
							secure</p>
					</div>

					<div class="col-sm-3">
						<img src="resources/images/main/help_3.png"
							style="padding-bottom: 13px;" alt="">
						<p>VPayQwik makes sure that your every transaction is 100%
							Safe</p>
					</div>

					<div class="col-sm-3">
						<img src="resources/images/main/help_4.png" alt="">
						<p>Spending is earning at VPayQwik We offer various deals to
							distribute happiness</p>
					</div>
				</div>
				<!---end row--->
			</div>
		</div>
		<!-----end help--->

		<div class="container">
			<div class="row">
				<!-- useful links -->
				<div class="col-sm-3 wow bounceInLeft" data-wow-duration="2s">
					<ul class="row footer-links">
						<p>
							<b>Mobile Recharges</b>
						</p>
						<a href="#">Airtel Recharge</a>|
						<a href="#">Vodafone Recharge</a>|
						<a href="#">Aircel Recharge</a>|
						<a href="#">Idea Recharge</a>|
						<a href="#">BSNL Recharge</a>|
						<a href="#">Reliance GSM</a>|
						<a href="#">Reliance CDMA</a>|
						<a href="#">Tata GSM</a>|
						<a href="#">Tata Docomo</a>|
						<a href="#">MTNL</a>|
						<a href="#">MTS</a>|
						<a href="#">T24 Mobile Recharge</a>
						</ul>
				</div>
				<div class="col-sm-3 wow bounceInLeft" data-wow-duration="2s">
					<ul class="row footer-links">
						<p>
							<b>DTH Recharges</b>
						</p>
						<a href="#">Airtel DTH Recharge</a>|
						<a href="#">Videocon D2H Recharge</a>|
						<a href="#">Tata Sky Recharge</a>|
						<a href="#">Reliance DTH Recharge</a>|
						<a href="#">Right Way</a>|
						<a href="#">Dish TV Recharge</a>|
						<a href="#">Sun Direct Recharge</a>
						</ul>
				</div>
				<div class="col-sm-3 wow bounceInRight" data-wow-duration="2s">
					<ul class="row footer-links">
						<p>
							<b>Datacard Recharges</b>
						</p>
						<a href="#">BSNL Datacard</a>|
						<a href="#">Reliance Netconnect</a>|
						<a href="#">Tata Photon</a>|
						<a href="#">Plus</a>|
						<a href="#">Tata Photon Whiz</a>|
						<a href="#">MTS Datacard</a>|
						<a href="#">MTS Blaze</a>|
						<a href="#">MTNL Datacard</a>
						</ul>
				</div>
				<div class="col-sm-3 wow bounceInRight" data-wow-duration="2s">
					<ul class="row footer-links">
						<p>
							<b>Landline Bill Payment</b>
						</p>
						<a href="#">BSNL Landline</a>|
						<a href="#">MTNL Landline Delhi</a>|
						<a href="#">Reliance</a>|
						<a href="#">Tata Docomo</a>
						</ul>
				</div>
			</div>
			<hr>
			<div class="row">
				<p>India's first complete payment application & website,
					VPayQwik is a quickest and safest way for Online Recharge, DTH or
					Datacard Recharge and make mobile or utility Bill Payments for
					Airtel, Aircel,BSNL, Docomo, Idea, MTNL, Vodafone or other
					operators.you do not need to rush to the market to make your DTH or
					mobile bill payments, just log-on to VPayQwik and experience the
					easiest & fastest method of recharges and payments.</p>
				<p>VPayQwik brings to you the various coupons, deals and offers
					to make your payment experience rich every day. Our online recharge
					and bill payment service give you reward points, which can be used
					to avail attractive and lucrative cash back and discount offers.
					Download VPayQwik from your App Store.</p>
				<p>VPayQwik also provides travel ticket service, through
					VPayQwik you can book Air tickes, Bus tickets, Hotels, Car Rental
					and Holiday Packages.Come and experience hassle-free, safe and fast
					ticketing services at VPayQwik.VPayQwik make sure that every
					transaction you do is risk free.</p>
			</div>
			<hr>
			<div class="row" id="footericons">
				<div class="col-sm-8  wow bounceInUp">
					<img src='<c:url value="/resources/images/msewalogo.png"/>' 
					width="100px"  alt="">
				</div>
				<!----end col-md-4--->

				<div class="col-sm-4  wow bounceInUp">
					<ul class="row footer-links">
						<a href="#"> <img src="resources/images/main/fb.gif"
							class="fb"></a>
						<a href="#"> <img src="resources/images/main/tw.gif"
							class="tw"></a>
						<a href="#"> <img src="resources/images/main/yu.gif"
							class="yu"></a>
						<a href="#"> <img src="resources/images/main/in.gif"
							class="in"></a>
					</ul>
				</div>
				<!----end col-md-4--->
				</center>

			</div>
			<!-----end row----->

		</div>
		<div class="menu" id="main">
			<div class="container">
				<div class="row" style="margin-top: 10px; margin-bottom: 10px;">
					<a href="Home">Home</a> <a href="AboutUs">About Us</a> <a
						href="PartnersWithUs">Partner with us</a> <a
						href="Terms&Conditions">Terms & Conditions</a><a
						href="PrivacyPolicy">Privacy Policy</a> <a href="Grievance">Grievance
						policy</a>
				</div>
				<!---row---->
			</div>
			<!----container------->
		</div>
	</footer>

</body>
</html> 