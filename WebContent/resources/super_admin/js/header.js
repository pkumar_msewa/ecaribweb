function fillMobileNumber(mobile){

    $("#search").val(mobile);
}


function updateLimit(code) {
    console.log("update limit");
    var csrfHeader = $("meta[name='_csrf_header']").attr("content");
    var csrfToken = $("meta[name='_csrf']").attr("content");
    var headers = {};
    headers[csrfHeader] = csrfToken;

    var tLimit = $("#tlimit-" + code).val();
    var dLimit = $("#dlimit-" + code).val();
    var mLimit = $("#mlimit-" + code).val();
    var bLimit = $("#dlimit-" + code).val();

    $.ajax({
        type: "POST",
        headers: headers,
        contentType: "application/x-www-form-urlencoded",
        url: "/SuperAdmin/EditLimit",
        data: {
            code: code,
            monthlyLimit: mLimit,
            dailyLimit: dLimit,
            transactionLimit: tLimit,
            balanceLimit: bLimit
        },
        success: function (response) {
            console.log(response);
        }
    });
}

function getTransactionDetails(transactionRef) {
    var csrfHeader = $("meta[name='_csrf_header']").attr("content");
    var csrfToken = $("meta[name='_csrf']").attr("content");
    var headers = {};
    headers[csrfHeader] = csrfToken;

    $.ajax({
        type: "GET",
        headers: headers,
        contentType: "application/x-www-form-urlencoded",
        url: "/SuperAdmin/TransactionCheck/" + transactionRef,
        success: function (response) {
            console.log(response);
            if (response.account_no != null) {
                $("#account_no").html(response.account_no);
                $("#agent_id").html(response.agent_id);
                $("#charged_amt").html(response.charged_amt);
                $("#ipay_id").html(response.ipay_id);
                $("#opening_bal").html(response.opening_bal);
                $("#opr_id").html(response.opr_id);
                $("#req_dt").html(response.req_dt);
                $("#res_code").html(response.res_code);
                $("#res_msg").html(response.res_msg);
                $("#sp_key").html(response.sp_key);
                $("#status").html(response.status);
                $("#trans_amt").html(response.trans_amt);
                var resp_status = response.res_code;
                switch (resp_status) {
                    case "TXN":
                        $("#tran_alert").addClass("alert alert-success");
                        break;
                    case "TUP":
                        $("#tran_alert").addClass("alert alert-info");
                        break;
                    case "TRP":
                        $("#tran_alert").addClass("alert alert-default");
                        break;
                    default:
                        $("#tran_alert").addClass("alert alert-warning");
                }
                $("#successNotification").modal('show');
            }
        }
    });

}


function processRefund(transactionRef,count) {
	var spinnerUrl = "<img src='/resources/images/spinner.gif' height='20' width='20'>"
    var csrfHeader = $("meta[name='_csrf_header']").attr("content");
    var csrfToken = $("meta[name='_csrf']").attr("content");
    var headers = {};
    headers[csrfHeader] = csrfToken;
    $("#refundreport"+count).removeClass("fa fa-retweet fa-2x");
    $("#refundreport"+count).html(spinnerUrl);
    $.ajax({
        type: "GET",
        headers: headers,
        contentType: "application/x-www-form-urlencoded",
        url: "/SuperAdmin/" + transactionRef + "/Refund",
        success: function (response) {
        	  if(response.code.includes("S00")){
        		  $("#success_refundNotification").modal('show');
                $("#refundreport"+count).addClass("fa fa-retweet fa-2x disabled");
                $(".refund_body").html(response.message);
                $("#refundreport"+count).html("");
        	  }else if(response.code.includes("F00")){
                  $("#error_refundNotification").modal('show'); 
                  
                  $(".refund_body").html(response.message);
                  $("#refundreport"+count).addClass("fa fa-retweet fa-2x");
                  $("#refundreport"+count).html("");
        	  }
        }

    });
}

function openRefundModal(mobNo,txnId,amount,orderId){
	$('#partialRefundModal #o_transactionId').text(txnId); 
	$('#partialRefundModal #o_mobile').text(mobNo);
	$('#partialRefundModal #o_amount').text(amount);
	$('#partialRefundModal #o_orderId').text(orderId);
}
$(document).ready(function(){
$("#refundProcess").click(function() {
	var spinnerUrl = "Please wait <img src='/resources/images/spinner.gif' height='20' width='20'>"
	var transactionRef=$('#o_transactionId').html();
	var orderId=$('#o_orderId').html();
	var finalAmount=$("#o_netamount").val() || 0;
	 var csrfHeader = $("meta[name='_csrf_header']").attr("content");
     var csrfToken = $("meta[name='_csrf']").attr("content");
     var headers = {};
     headers[csrfHeader] = csrfToken;
     $("#refundProcess").addClass("disabled");
 	$("#refundProcess").html(spinnerUrl);
     $.ajax({
         type: "POST",
         headers: headers,
         contentType : "application/json",
         url: "/Merchant/RefundMTransaction",
 		 dataType : 'json',
 		 data : JSON.stringify({
 			"transactionRefNo" : "" + transactionRef + "",
 			"orderId":"" + orderId +"",
 			"refundAmount":""+finalAmount +""
 		}),
 	   success: function (response) {
       	if (response.code.includes("S00")) {
       	      $("#partialRefundModal").modal('hide');
       	       $("#refundProcess").html('');
               $("#success_refundNotification").modal('show');
               $(".refund_body").html(response.message);
       	}else if(response.code.includes("F00")){
       	 $("#partialRefundModal").modal('hide');
        	$("#refundProcess").html('');
         $("#error_refundNotification").modal('show'); 
         $(".refund_body").html(response.message);
       	 }
       }
	});
  });
});


function processMerchantRefund(transactionRef,count) {
	var spinnerUrl = "<img src='/resources/images/spinner.gif' height='20' width='20'>"
    var csrfHeader = $("meta[name='_csrf_header']").attr("content");
    var csrfToken = $("meta[name='_csrf']").attr("content");
    var headers = {};
    headers[csrfHeader] = csrfToken;
    $("#refundreport"+count).removeClass("fa fa-retweet fa-2x");
    $("#refundreport"+count).html(spinnerUrl);
    $.ajax({
        type: "GET",
        headers: headers,
        contentType: "application/x-www-form-urlencoded",
        url: "/SuperAdmin/" + transactionRef + "/RefundMerchant",
        success: function (response) {
        	console.log("response ::" + response.message);
        	  if(response.code.includes("S00")){
        		  $("#success_refundNotification").modal('show');
                $(".refund_body").html(response.message);
                $("#refundreport"+count).addClass("fa fa-retweet fa-2x");
                $("#refundreport"+count).html("");
        	  }else if(response.code.includes("F00")){
                  $("#error_refundNotification").modal('show'); 
                  $(".refund_body").html(response.message);
                  $("#refundreport"+count).addClass("fa fa-retweet fa-2x");
                  $("#refundreport"+count).html("");
        	  }
        }

    });
}


