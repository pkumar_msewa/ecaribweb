<!DOCTYPE html>
<%@ page contentType="text/html; charset=utf-8" language="java"
	errorPage=""%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<html lang="en">
<head>
<meta charset="utf-8">
<sec:csrfMetaTags />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>VPayQwik | Event Lists</title>
<link rel="stylesheet"
	href="<c:url value="/resources/css/font-awesome.min.css"/>">
<link href='<c:url value='/resources/css/font-family.css'/>'
	rel='stylesheet' type='text/css'>
<link rel="stylesheet"
	href="<c:url value='/resources/css/font-awesome.css'/>" type='text/css'>

<!-- Optional theme -->
<link rel="stylesheet"
	href="<c:url value='/resources/css/bootstrap-theme.min.css'/>"
	type='text/css'>

<link href="<c:url value="/resources/css/css_style.css"/>"
	rel='stylesheet' type='text/css'>
<link rel="stylesheet"
	href="<c:url value='/resources/css/bootstrap.css'/>" type='text/css'>
<script src="<c:url value='/resources/js/jquery.js'/>"></script>
<script src="<c:url value='/resources/js/bootstrap.min.js'/>"></script>

<script type="text/javascript"
	src="<c:url value="/resources/js/userdetails.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/header.js"/>"></script>
<link rel="icon" href='<c:url value="/resources/images/favicon.png"/>'
	type="image/png" />
<link href='<c:url value="/resources/css/css_style.css"/>'
	rel='stylesheet' type='text/css'>
<style>
.no-js #loader {
	display: none;
}

.js #loader {
	display: block;
	position: absolute;
	left: 100px;
	top: 0;
}

.se-pre-con {
	position: fixed;
	left: 0px;
	top: 0px;
	width: 100%;
	height: 100%;
	z-index: 9999;
	background: url(/images/pq_large.gif) center no-repeat #fff;
}
</style>
<script src="<c:url value='/resources/js/modernizr.js'/>"></script>

<script type="text/javascript">
		$(window).load(function() {
			$(".se-pre-con").fadeOut("slow");
		});
	</script>

</head>

<body onload="start()">
	<div class="se-pre-con"></div>
	<jsp:include page="/WEB-INF/jsp/User/Header.jsp" />
	<jsp:include page="/WEB-INF/jsp/User/Menu.jsp" />

	<!-----------------end navbar---------------------->

	<!------------- end main-------------------->

	<div class="background"></div>
	<!---blue box---->
	<span style="color: red;"> ${msg}</span>
	<br>

	<div class="container" style="		background: #FFFFFF;
		margin-top: -80px;
		-webkit-box-shadow: -1px 1px 47px -7px rgba(0, 0, 0, 0.67);
		-moz-box-shadow: -1px 1px 47px -7px rgba(0, 0, 0, 0.67);
		box-shadow: -1px 1px 47px -7px rgba(0, 0, 0, 0.67);
	">
	<div class="col-md-12">
				<div class="slider" id="slider"
					style="margin-right: -15px; margin-left: -15px;  margin-top: 15px;"">
					<div class="carousel slide hidden-xs" data-ride="carousel"
						id="mycarousel">
						<ol class="carousel-indicators">
							<li class="active" data-slide-to="0" data-target="#mycarousel"></li>
							<li data-slide-to="1" data-target="#mycarousel"></li>
						</ol>

						<div class="carousel-inner">

							<div class="item active" id="slide1">

								<img src='<c:url value="/resources/images/adlabslider1.jpg"/>' />
							</div>
							<!---end item---->

							<div class="item">
								<img src='<c:url value="/resources/images/adlabslider2.jpg"/>' />
							</div>

							<!---end item---->
						</div>
						<!--end carousel inner------>

					</div>

					<!---end caeousel slider---->
				</div>
				<!---end slider----->
			</div>
			<div class="col-md-12">
		<c:forEach items="${ticketlist}" var="gl" varStatus="loopCounter">
			<!-- -----for 1 ticket----- -->
			<div class="row" style="    background: white;
    border: solid #f9f8f8;     margin-top: 15px;">
				<div>
				<div class="col-md-3">
					<center>
						<img src="${gl.image_url}" style="width: 60%;" />
					</center>
				</div>
				<div class="col-md-7">
					<h2 style="margin-top: 5%;">
						<i><c:out value="${gl.title}" escapeXml="true" default="" /></i>
					</h2>
					${gl.ticket_des}

				</div>
				<div class="col-md-2">
					<button id="bts" class="btn btn-success" style="margin-top: 26.8%;">Show</button>
				</div>
				</div>
			</div>
			<div class="row">
				<div id="btn2"
					style="background: #fff6f6; display: none; padding: 19px 62px;">
					<!-- for Adult  -->
					<div class="row">
						<div class="col-md-2">
							<h4 style="float: left; margin-top: 26px;">
								<b><i class="fa fa-inr"></i> <c:out value="${gl.acost_per1}"
										escapeXml="true" default="" /></b>
							</h4>
						</div>
						<div class="col-md-3 text-center">
							<b><c:out value="${gl.atype}" escapeXml="true" default="" /></b>

							<select name="quantity" id="${loopCounter.count}quantity"
								onchange="calculateTotalAmount(${loopCounter.count},${gl.acost_per1});"
								class="form-control">
								<option value="0">0</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
							</select>
						</div>
						<div class="col-md-5">
							<b><i class="fa fa-inr"
								style="float: left; margin-top: 21px; font-size: 16pt;"></i><input
								type="text" id="1totalamount" value=""
								style="margin-top: 10px; background: transparent; border-bottom: none; font-size: 20pt; width: 130px; margin-top: auto;" /></b>
						</div>
						<div style="display: none;" id="eventspiner">
							<img src="/resources/images/s.gif"/>
						</div>
						<button type="button" class="btn btn-warning"
							style="background: red; color: white; margin-left: 4%; border-radius: 0; margin-top: 16px;"
							onclick="detail('${gl.aproduct_id}','${gl.acost_per1}')">Proceed
						</button>
					</div>
					<hr>
					<!-- for Senior citizen  -->

					<div class="row">
						<div class="col-md-2">
							<h4 style="float: left; margin-top: 26px;">
								<b><i class="fa fa-inr"></i> <c:out value="${gl.scost_per2}"
										escapeXml="true" default="" /></b>
							</h4>
						</div>
						<div class="col-md-3 text-center">
							<b><c:out value="${gl.sctype}" escapeXml="true" default="" /></b>

							<select name="quantity" id="${loopCounter.count+1}quantity"
								onchange="calculateTotalAmount(${loopCounter.count+1},${gl.scost_per2});"
								class="form-control">
								<option value="0">0</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
							</select>
						</div>
						<div class="col-md-5">
							<b><i class="fa fa-inr"
								style="float: left; margin-top: 21px; font-size: 16pt;"></i><input
								type="text" id="2totalamount" value=""
								style="margin-top: 10px; background: transparent; border-bottom: none; font-size: 20pt; width: 130px; margin-top: auto;" /></b>
						</div>



						<div style="display: none;" id="eventspiner">
							<img src="/resources/images/s.gif" style="width: 15%;" />
						</div>
						<button type="button" class="btn btn-warning"
							style="background: red; color: white; margin-left: 4%; border-radius: 0; margin-top: 16px;"
							onclick="detail('${gl.sproduct_id}','${gl.scost_per2}')">Proceed
						</button>
					</div>
					<hr>

					<!-- for College Student  -->
					<div class="row">
						<div class="col-md-2">
							<h4 style="float: left; margin-top: 26px;">
								<b><i class="fa fa-inr"></i> <c:out value="${gl.ccost_per3}"
										escapeXml="true" default="" /></b>
							</h4>
						</div>
						<div class="col-md-3 text-center">
							<b><c:out value="${gl.ctype}" escapeXml="true" default="" /></b>

							<select name="quantity" id="${loopCounter.count+2}quantity"
								onchange="calculateTotalAmount(${loopCounter.count+2},${gl.ccost_per3});"
								class="form-control">
								<option value="0">0</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
							</select>
						</div>
						<div class="col-md-5">
							<b><i class="fa fa-inr"
								style="float: left; margin-top: 21px; font-size: 16pt;"></i><input
								type="text" id="3totalamount" value=""
								style="margin-top: 10px; background: transparent; border-bottom: none; font-size: 20pt; width: 130px; margin-top: auto;" /></b>
						</div>


						<div style="display: none;" id="eventspiner">
							<img src="/resources/images/s.gif" style="width: 15%;" />
						</div>

						<button type="button" class="btn btn-warning"
							style="background: red; color: white; margin-left: 4%; border-radius: 0; margin-top: 16px;"
							onclick="detail('${gl.cproduct_id}','${gl.ccost_per3}')">Proceed
						</button>
					</div>

					<hr>
					<!-- for Child Ticket  -->
					<div class="row">
						<div class="col-md-2">
							<h4 style="float: left; margin-top: 26px;">
								<b><i class="fa fa-inr"></i> <c:out
										value="${gl.chcost_per4}" escapeXml="true" default="" /></b>
							</h4>
						</div>
						<div class="col-md-3 text-center">
							<b><c:out value="${gl.chtype}" escapeXml="true" default="" /></b>

							<select name="quantity" id="${loopCounter.count+3}quantity"
								onchange="calculateTotalAmount(${loopCounter.count+3},${gl.chcost_per4});"
								class="form-control">
								<option value="0">0</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
							</select>
						</div>
						<div class="col-md-5">
							<b><i class="fa fa-inr"
								style="float: left; margin-top: 21px; font-size: 16pt;"></i><input
								type="text" id="4totalamount" value=""
								style="margin-top: 10px; background: transparent; border-bottom: none; font-size: 20pt; width: 130px; margin-top: auto;" /></b>
						</div>
						<div style="display: none;" id="eventspiner">
							<img src="/resources/images/s.gif" style="width: 15%;" />
						</div>
						<button type="button" class="btn btn-warning"
							style="background: red; color: white; margin-left: 4%; border-radius: 0; margin-top: 16px;"
							onclick="detail('${gl.chproduct_id}','${gl.chcost_per4}')">Proceed
						</button>
					</div>
				</div>
			</div>
			<!-- -----end for 1 ticket----- -->

			<!-- for   Second Ticket Imagica Express   -->
<br>

			<div class="row"style="    background: white;
    border: solid #f9f8f8;     margin-bottom: 15px;">

				<div class="col-md-3">
					<center>
						<img src="${gl.iimage_url}" style="width: 60%;" />
					</center>
				</div>
				<div class="col-md-7">
					<h2 style="margin-top: 5%;">
						<i><c:out value="${gl.ititle}" escapeXml="true" default="" /></i>
					</h2>
					${gl.iticket_des}

				</div>
				<div class="col-md-2">
					<button id="bts1" class="btn btn-success"
						style="margin-top: 26.8%;">Show</button>
				</div>
			</div>
			<div class="row">
				<div id="btn3"
					style="background: #fff6f6; display: none; padding: 19px 62px;">
					<!-- for Adult  -->
					<div class="row">
						<div class="col-md-2">
							<h4 style="float: left; margin-top: 26px;">
								<b><i class="fa fa-inr"></i> <c:out value="${gl.iacost_per1}"
										escapeXml="true" default="" /></b>
							</h4>
						</div>
						<div class="col-md-3 text-center">
							<b><c:out value="${gl.iatype}" escapeXml="true" default="" /></b>

							<select name="quantity" id="${loopCounter.count+4}quantity"
								onchange="calculateTotalAmount(${loopCounter.count+4},${gl.iacost_per1});"
								class="form-control">
								<option value="0">0</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
							</select>
						</div>
						<div class="col-md-5">
							<b><i class="fa fa-inr"
								style="float: left; margin-top: 21px; font-size: 16pt;"></i><input
								type="text" id="5totalamount" value=""
								style="margin-top: 10px; background: transparent; border-bottom: none; font-size: 20pt; width: 130px; margin-top: auto;" /></b>
						</div>
						<div style="display: none;" id="eventspiner">
							<img src="/resources/images/s.gif" style="width: 15%;" />
						</div>
						<button type="button" class="btn btn-warning"
							style="background: red; color: white; margin-left: 4%; border-radius: 0; margin-top: 16px;"
							onclick="detail('${gl.iaproduct_id}','${gl.iacost_per1}')">Proceed
						</button>
					</div>
					<hr>
					<!-- for College  -->

					<div class="row">
						<div class="col-md-2">
							<h4 style="float: left; margin-top: 26px;">
								<b><i class="fa fa-inr"></i> <c:out value="${gl.iscost_per2}"
										escapeXml="true" default="" /></b>
							</h4>
						</div>
						<div class="col-md-3 text-center">
							<b><c:out value="${gl.ictype}" escapeXml="true" default="" /></b>

							<select name="quantity" id="${loopCounter.count+5}quantity"
								onchange="calculateTotalAmount(${loopCounter.count+5},${gl.iscost_per2});"
								class="form-control">
								<option value="0">0</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
							</select>
						</div>
						<div class="col-md-5">
							<b><i class="fa fa-inr"
								style="float: left; margin-top: 21px; font-size: 16pt;"></i><input
								type="text" id="6totalamount" value=""
								style="margin-top: 10px; background: transparent; border-bottom: none; font-size: 20pt; width: 130px; margin-top: auto;" /></b>
						</div>

						<div style="display: none;" id="eventspiner">
							<img src="/resources/images/s.gif" style="width: 15%;" />
						</div>


						<button type="button" class="btn btn-warning"
							style="background: red; color: white; margin-left: 4%; border-radius: 0; margin-top: 16px;"
							onclick="detail('${gl.icproduct_id}','${gl.iscost_per2}')">Proceed
						</button>
					</div>
					<hr>



					<hr>
					<!-- for Child Ticket  -->
					<div class="row">
						<div class="col-md-2">
							<h4 style="float: left; margin-top: 26px;">
								<b><i class="fa fa-inr"></i> <c:out
										value="${gl.ichcost_per3}" escapeXml="true" default="" /></b>
							</h4>
						</div>
						<div class="col-md-3 text-center">
							<b><c:out value="${gl.ichtype}" escapeXml="true" default="" /></b>

							<select name="quantity" id="${loopCounter.count+6}quantity"
								onchange="calculateTotalAmount(${loopCounter.count+6},${gl.ichcost_per3});"
								class="form-control">
								<option value="0">0</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
							</select>
						</div>
						<div class="col-md-5">
							<b><i class="fa fa-inr"
								style="float: left; margin-top: 21px; font-size: 16pt;"></i><input
								type="text" id="7totalamount" value=""
								style="margin-top: 10px; background: transparent; border-bottom: none; font-size: 20pt; width: 130px; margin-top: auto;" /></b>
						</div>
						<div style="display: none;" id="eventspiner">
							<img src="/resources/images/s.gif" style="width: 15%;" />
						</div>

						<button type="button" class="btn btn-warning"
							style="background: red; color: white; margin-left: 4%; border-radius: 0; margin-top: 16px;"
							onclick="detail('${gl.ichproduct_id}','${gl.ichcost_per3}')">Proceed
						</button>
					</div>
					<br>
				</div>
			</div>
			<!-- -----end for 2 ticket----- -->
		</c:forEach>
	</div><br><br>
	</div>
	</div>
	<!-- END EVENTS DETAILS -->
	<script>
$(function(){
 $("#bts").click(function(){
	 
  $("#btn2").toggle();
  $("#bts").html("hide");
	
  }); 
 });
 </script>

	<script>
$(function(){
 $("#bts1").click(function(){
	 
  $("#btn3").toggle();
  $("#bts1").html("hide");
	
  }); 
 });
 </script>


	</div>
	<!-----end col-md-8-->

	<!---end row-->
	<!----end container-->
	<jsp:include page="/WEB-INF/jsp/User/Footer.jsp" />
	<script src="http://code.jquery.com/jquery-2.2.1.min.js"></script>
	<script src="<c:url value='/resources/js/bootstrap.js'/>"></script>
	<script>

  var myBox1="";
	 function calculateTotalAmount(x,myBox2)
	
	{
		  myBox1 = document.getElementById(x+"quantity").value;	
			var myResult = myBox1 * myBox2;
			document.getElementById(x+"totalamount").value = myResult;
			var textbox3 = document.getElementById('textbox3');
			textbox3.value=myResult;
			document.getElementById(x+"totalamount").innerHTML =""+myResult;
			
	} 
		// DETAIL 
		
		function detail(id,amount) {
		
		
		 $("#eventspiner").show();
			$.ajax({
				type : "POST",
				contentType : "application/json",
				url : "/User/Adlabs/AdlabsUserDetail",
				data : JSON.stringify({
					"product_id" : "" +id + "",
					"quantity"  :""+myBox1+"",
					"amount"  :""+amount+""
					
				}),
				success : function(response) {
					location.href = "AdlabsUserDetail";
					
					

				}
			});

		}
	</script>

</body>
</html>



