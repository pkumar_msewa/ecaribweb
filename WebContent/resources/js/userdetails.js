/**
 * 
 */
var eStatus = "";

$(document).ready(function(){
	var spinnerUrl = "Please Wait <img src='/resources/images/spinner.gif' height='20' width='20'>"
	var spinner = "<img src='/resources/images/spinner.gif' height='20' width='20'>"
	var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");
	var csrfHeader = $("meta[name='_csrf_header']").attr("content");
	var csrfToken = $("meta[name='_csrf']").attr("content");
	var headers = {};
	headers[csrfHeader]=csrfToken;

	$(".numeric").keydown(function(event){
        if(event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || (event.keyCode == 65 && event.ctrlKey == true) || (event.keyCode >= 35 && event.keyCode<=39)){
            return;
        }else{
            if(event.shiftKey || (event.keyCode < 48 || event.keyCode >57) && (event.keyCode < 96 || event.keyCode > 105)) {
                event.preventDefault();
            }
        }
    });

	$.ajax({
		type : "GET",
		url : "/User/GetUserDetails",
		headers: headers,
		contentType : "application/json",
		datatype : 'json',
		success : function(response) {
			console.log(response.flag);
			if(response.flag){
				$('#kyc_update_disable').hide();
			}
			var u = response;
			if (u.code.includes("S00")) {
			eStatus = u.emailStatus;
			var usertype=u.flag;
			if(response.flag) {
				$("#hidethis1").detach();
				$("#hidethis2").detach();
			}
			var imageContent = u.imageContent;
			var type = u.image;
			var srcImage = "data:"+type+";base64,"+imageContent
			var firstNameLength = $("#firstName").length;
			var lastNameLength = $("#lastName").length;
			var accountBalanceLength = $("#account_balance").length;
			var userMobileLength = $("#user_mobile").length;
			var userAdressLength = $("#user_address").length;
			var  accountNumberLength = $("#account_number").length;
			var accountTypeLength = $("#account_type").length;
			var accountDesLength = $("#account_type_description").length;
			var dailyLimitLength = $("#daily_limit").length;
			var monthlyLimitLength = $("#monthly_limit").length;
			var emailLength = $("#user_email").length;
			$("#user_points").html(u.points);
			$("#small_display_pic").attr('src',srcImage);
			$("#display_pic").attr('src',srcImage);
            $("#first_name_ep").val(u.firstName);
			$("#loadmoney_username").val(u.firstName+" "+u.lastName);
			$("#loadmoney_phone").val(u.contactNo);
			$("#loadmoney_email").val(u.email);
            $("#last_name_ep").val(u.lastName);
            $("#email").val(u.email);
            $("#address").val(u.address);
			$("#display_first_last_name").html(u.firstName+" "+u.lastName);
			$("#bt_account_number").val(u.accountNumber);
			$("#bt_email").val(u.email);
			$("#bt_mobile_number").val(u.contactNo);
			$(".first_name").val(u.firstName);
			$(".first_NAME").val(u.firstName);
			$(".small_pic").attr('src',srcImage);
			if(firstNameLength){
				$("#first_name").html(u.firstName);
			}
			if(accountBalanceLength){
				$("#account_balance").html(u.balance);
				$("#account_bal").html(u.balance);
				$("#user_balance").html(u.balance);
			}
			if(emailLength){
				$("#user_email").html(u.email);
			}
			if(userMobileLength){
			$("#user_mobile").html(u.contactNo+ "<span class='fa fa-check-circle' style='color:#17bcc8;'></span>");
			}
			if(userAdressLength){
			$("#user_address").html(u.address);
			}
			if(accountNumberLength){
			$("#account_number").html(u.accountNumber);
			}
			if(accountTypeLength){
			$("#account_type").html(u.userType);
			}
			if(accountDesLength){
			$("#account_type_description").html();
			}
			if(dailyLimitLength){
			$("#daily_limit").html(u.dailyTransaction);
			}
			if(monthlyLimitLength){
			$("#monthly_limit").html(u.monthlyTransaction);
			}
			
			if(u.emailStatus == "Inactive"){
				$("#alert_verification_message").html("Your email id is not verified. You've received verification email on "+u.email+". If you want to change your mail go to settings and update");
				$("#verification_alert").modal('show');
				$("#dth_submit").attr("disabled", "disabled");
				$("#landline_submit").attr("disabled", "disabled");
				$("#gas_submit").attr("disabled", "disabled");
				$("#ecity_submit").attr("disabled", "disabled");
				$("#ins_submit").attr("disabled", "disabled");
				//for topup
				$("#pre_submit").attr("disabled", "disabled");
				$("#post_submit").attr("disabled", "disabled");
				$("#dc_submit").attr("disabled", "disabled");
				
				$("#smm_submit").attr("disabled", "disabled");
				$("#lm_submit").attr("disabled", "disabled");
				$("#email_ep").show();
				
			}
			if(u.emailStatus == "Active"){
				$("#user_email").append(" <span class='fa fa-check-circle' style='color:#17bcc8;'></span>");
				$("#email_ep").hide();
				$("#dth_submit").removeClass("disabled");
				$("#landline_submit").removeClass("disabled");
				$("#gas_submit").removeClass("disabled");
				$("#ecity_submit").removeClass("disabled");
				$("#ins_submit").removeClass("disabled");
				//for topup
				$("#pre_submit").removeClass("disabled");
				$("#post_submit").removeClass("disabled");
				$("#dc_submit").removeClass("disabled");
				
				$("#smm_submit").removeClass("disabled");
				$("#lm_submit").removeClass("disabled");
				
			}
		}else if(u.code.includes("F03")){
			window.location = "/Home";
		}
		},
	});

	$("#update_password").click(function() {
		$("#error_current_password").html("");
		$("#error_new").html("");
		$("#error_confirm").html("");
		
		var currentPassword = $("#current_password").val();
		var newPassword = $("#new_password").val();
		var confirmPassword =  $("#confirm_new_password").val();
		var valid = true;
		
		if(currentPassword.length <=0){
			$("#error_current_password").html("Enter Password");
			  valid = false;
		}
		if(newPassword.length <=0){
			$("#error_new").html("Enter New Password.");
			  valid = false;
		}
		if(confirmPassword.length <=0){
			$("#error_confirm").html("Enter Confirm Password.");
			  valid = false;
		}
		if(valid == true) {
		if(newPassword == confirmPassword) {
			 $("#update_password").html(spinnerUrl);
		$.ajax({
			type : "POST",
			headers:headers,
			url : "/User/UpdatePassword/Process",
			contentType : "application/x-www-form-urlencoded",
			data : {
				"newPassword" : "" + newPassword + "",
				"confirmPassword" : "" + confirmPassword + "",
				"password" : "" + currentPassword + ""
			},
			success : function(response) {
				if (response.code.includes("S00")) {
					$("#change_pwd").modal("hide");
					$("#successNotification").modal("show");
					$("#success_alert").html(response.message);
					 var timeout = setTimeout(function(){
				            $("#current_password").val('');
				            $("#new_password").val('');
				            $("#confirm_new_password").val('');
				          }, 4000);
				} else if(response.code.includes("F00")) {
					$("#fpOTP_message").html(response.message);
					 $("#update_password").html('Update');
					 var timeout = setTimeout(function(){
				            $("#fpOTP_message").text('');
				            $("#current_password").val('');
				            $("#new_password").val('');
				            $("#confirm_new_password").val('');
				          }, 4000);
				}else if(response.code.includes("F03")){
					window.location = "/Home";
				}
			}
		});
		}else {
//			$('#fpnewPassword_key').val("Please enter same password in both fields");
			$("#error_confirm").text("Password does not match");
		}
	  }
		var timeout = setTimeout(function(){
			  $("#error_current_password").text('');
	    		$("#error_new").text('');
	    		$("#error_confirm").text('');
		 }, 4000);

	});
	
	
	$("#editName").click(function() {
        var attr = $("#fname_edit").attr("readonly");
		if(attr) {
			$("#fname_edit").removeAttr("readonly");
            $("#fname_edit").focus();
            $("#fname_edit").css('border-bottom','1px solid #000');
            $("#eicon_link").removeClass("fa-pencil-square-o");
			$("#eicon_link").addClass("fa-upload");
			
		} else {
            var name = $('#fname_edit').val();
            var regex = "^[a-zA-Z ]*$";
            var valid = true;
            if(name.length < 3) {
            	 $("#successNotification").modal("show");
            	 $("#success_alert").html("Enter valid name");
            	 valid = false;
			}else if(!name.match(regex)){
				 $("#successNotification").modal("show");
            	 $("#success_alert").html("Enter valid name");
				valid = false;
			}

            if (valid == true) {
            	console.log("true details");
                $("#fname_edit").attr("readonly","readonly");
                $("#eicon_link").removeClass("fa-upload");
                $("#eicon_link").addClass("fa-spinner fa-spin");
                $("#eicon_link").html(spinner);
                $.ajax({
                    type: "POST",
                    headers: headers,
                    url: "/Api/v1/User/Windows/en/User/EditName",
                    contentType: "application/json",
                    datatype: 'json',
                    data: JSON.stringify({
                        "firstName": "" + name + ""
                    }),
                    success: function (response) {
                    	console.log("response ::" + response);
                        if (response.code.includes("S00")) {
                            $("#successNotification").modal("show");
                            $(".username").html(response.details);
                            $("#success_alert").html(response.message);
                            $("#eicon_link").html('');
                            document.getElementById("fname_edit").style.border="none";
                        }else if(response.code.includes("F03")){
                        	window.location = "/Home";
        				}else if(response.code.includes("F04")) {
                        	 $("#successNotification").modal("show");
                             $("#success_alert").html(response.details);
                             $("#eicon_link").html('');
                        } else {
                            $("#fpOTP_message").css("color", "red");
                            $("#fpOTP_message").text(response.message);
                        }
                    }
                });
                $("#eicon_link").removeClass("fa-spinner fa-spin");
                $("#eicon_link").addClass("fa-pencil-square-o");

            }
        }
	});
	
	
	$("#refresh_button").click(function() {
		$.ajax({
			url : "/Api/v1/User/Windows/en/User/UpdateCreditAndDebit",
			type : "GET",
			headers : headers,
			datatype : 'json',
			contentType : "application/json",
			success : function(response) {
				var u = response;
				if (u.code.includes("S00")) {
				$("#account_credit").html(u.credit);
				$("#account_debit").html(u.debit);
				}else if(response.code.includes("F03")){
					window.location = "/Home";
				}
			}
		});
	});
	$("#trx_refresh_button").click(function() {
		var paging = '0';
		var size = '20';
		$.ajax({
			type : "GET",
			headers : headers,
			contentType : 'application/json;charset=UTF-8',
			url : "/User/Receipts/ReceiptAjax",
		 data: "page=" + 0 + "&size=" + 10,
			dataType : 'json',
			contentType : "application/json",
			success:function(data,textStatus, jqXHR){
				console.log(data.code);
				if(data.code.includes("S00")){
					createPaging(data);
					renderReceiptTable(data.info);
				}else if(response.code.includes("F03")){
					window.location = "/Home";
				}
	 		}
		});
	});
	
function renderReceiptTable(data) {
	var $table= $('#editedtable');
	if(($table).find('tbody').length) $table.find('tbody').empty();
	$tbody = $table.append('<tbody></tbody>');
	if(data){
		for(var i=0; i < data.length; i++) {
			$tr = $('<tr></tr>');
			var statusClass = '';
			switch(data[i].status) {
				case 'Initiated' :  statusClass = 'label label-info'; break;
				case 'Success' : statusClass = 'label label-success'; break;
				case 'Reversed' : statusClass = 'label label-warning'; break;
				case 'Processing' : statusClass = 'label label-active'; aabreak;
				case 'Failed' : statusClass = 'label label-danger'; break;
			}
			$tr.append($('<td></td>').text(data[i].date));
			$tr.append($('<td></td>').text(data[i].description));
			$tr.append($('<td></td>').text(data[i].transactionRefNo));
			$tr.append($('<td></td>').text(data[i].amount));
			$tr.append($('<td></td>').append($('<span style="font-size:13px;display:block;max-width:80px;margin-right:20px"></span>').text(data[i].status).addClass(statusClass)));
			$tbody.append($tr);
		}
	}
}
function createPaging(data) {
 $('#rcptPagination').twbsPagination({
	 totalPages:data.totalPages,
	 visiblePages: 5,
	 onPageClick: function (event, page) 
	 {
		  transactionlist(page-1);
	
     }
		
 });
}

});