
/**
 * 
 */

$(document).ready(function(){
	var spinnerUrl = "Please wait <img src='/resources/images/spinner.gif' height='20' width='20'>"
    $(".img_show").hide();
	$("#show_img").click(function () {
		$(".img_show").show(1000);
	});

	var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");
	var csrfHeader = $("meta[name='_csrf_header']").attr("content");
	var csrfToken = $("meta[name='_csrf']").attr("content");
	var hash_key="hash";
	var default_hash="123456";
	var headers = {};
	headers[hash_key] = default_hash;
	headers[csrfHeader] = csrfToken;
	processMessage('${msg}');
    $("#pwd_eye").click(function(){
		var value = $("#password").attr("type");
		if(value == "password") {
			$(this).attr("class", "fa fa-eye");
			$("#password").attr("type", "text");
		}
		if(value == "text"){
			$(this).attr("class", "fa fa-eye-slash");
			$("#password").attr("type", "password");
		}

	});

	$("#cfm_pwd_eye").click(function(){
		var value = $("#confirmpassword").attr("type");
		if(value == "password") {
			$(this).attr("class", "fa fa-eye");
			$("#confirmpassword").attr("type", "text");
		}
		if(value == "text"){
			$(this).attr("class", "fa fa-eye-slash");
			$("#confirmpassword").attr("type", "password");
		}

	});

	$("#login_pwd_eye").click(function(){
		var value = $("#password_login").attr("type");
		if(value == "password") {
			$(this).attr("class", "fa fa-eye");
			$("#password_login").attr("type", "text");
		}
		if(value == "text"){
			$(this).attr("class", "fa fa-eye-slash");
			$("#password_login").attr("type", "password");
		}

	});
	
	$('#password').keyup(function() {
		$('#strength_password').html(checkStrength($('#password').val()))
	});
	
	
	
	 $('#confirmpassword').on('keyup', function () {
		    if ($(this).val() == $('#password').val()) {
		        $('#error_cnfpassword').html('matching').css('color', 'green');
		    } else $('#error_cnfpassword').html('doesnot match with password').css('color', 'red');
		    $("#confirmpassword").html(""); 
		});
	$("#registerButton").click(function() {
    	if($("#termsConditions").is(":checked")){
    $("#error_first_name").html("");
	$("#error_last_name").html("");
	$("#error_password").html("");
	$("#error_cnfpassword").html("");
	$("#error_email").html("");
	$("#error_locationCode").html("");
	$("#error_contact_no").html("");
	$("#error_gender").html("");
	$("#error_dob").html("");
	$("#error_captcha").html("");
	var valid = true;
//	var pattern = "[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$"; //pattern for email
	var pattern = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2}|com|org|net|edu|gov|mil|biz|info|mobi|name|aero|asia|jobs|museum|co|in)"
	var passwordPattern = "[a-zA-z0-9]"; //pattern for password
	var mobilePattern="^(?=(?:[7-9]){1})(?=[0-9]{10}).*";
	var regexp = "^[A-Za-z]*$|^[A-Za-z][A-Za-z ]*[A-Za-z]$";
	var passwordProtect="/^[A-Za-z]+$/";
	var firstName = $('#first_name').val();
	var lastName = $('#last_name').val();
	var password  = $('#password').val();
	var email = $('#email').val() ;
	var captchaResponse = $("#g-recaptcha-response").val();
	var dob = $('#dob').val();
	var locationCode= $('#pincode').val();
	var contactNo = $('#contact_no').val() ;
	var secQuestion = $('#secquestions').val() ;
	var secAnswer = $('#sec_ans').val() ;
	
	
	if(contactNo.length <= 0){
		$("#error_contact_no").html("Enter Your Mobile No.");
		valid = false;
	}else if(contactNo.length != 10) {
		$("#error_contact_no").html("Enter 10 digit mobile number");
		valid = false;
	}else if(!contactNo.match(mobilePattern)){
		$("#error_contact_no").html("Enter valid mobile number");
		valid = false;
	}

	if(password.length <= 0){
		$("#error_password").html("Enter password");
		valid = false;
	}else if(!password.match(regexp)){
		$("#error_password").html("Enter only alphanumeric password");
		valid = false;
	}else if(password.length != 6){
		$("#error_password").html("Password must be 6 characters long");
		valid = false;
	}
	
	if(firstName.length <= 0){
		$("#error_first_name").html("Enter first name");
		valid = false;
	}else if(!firstName.match(regexp)){
		$("#error_first_name").html("Enter Only characters");
		valid = false;
	}
	
	if(lastName.length <= 0){
		$("#error_last_name").html("Enter last name");
		valid = false;
	}else if(!lastName.match(regexp)){
		$("#error_last_name").html("Enter Only characters");
		valid = false;
	}
	
	if(email.length  <= 0){
		$("#error_email").html("Please enter your email Id ");
		valid = false; 
	}else if(email.length  <= 8) {
		$("#error_email").html("please enter 8 or more character must include '@' with at least one alphabetical character(a-z),number and followed by like 'gmail.com'");
		valid = false;
	}else if(!email.match(pattern)){
		$("#error_email").html("please enter 8 or more character must include '@' with at least one alphabetical character(a-z),number and followed by like 'gmail.com'");
		valid = false;
	}
	if(dob.length <=0){
		$("#error_dob").html("Enter your date of birth");
		  valid = false;
	}
	
	/*if(captchaResponse.length <=0){
		$("#error_captcha").html("Enter Captcha");
		  valid = false;
	}*/
	
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1;
	var yyyy = today.getFullYear();

	today = yyyy+'-'+"0"+mm+'-'+dd;
	console.log("current date:::::"+today);
	if(new Date(dob) >= new Date("1998-12-31")) {
				$("#error_dob").html("You must be at least 18 years old to sign up");
				valid = false;
	}
	/*if(locationCode.length <= 0){
		$("#error_pincode").html("Enter location code");
		valid = false;
    }else if(locationCode.length != 6){
		$("#error_pincode").html("Location Code must be 6 characters long only");
	}*/
	
	/*if(secQuestion > 0){
		console.log("inside this");
		console.log("secqu::" + secQuestion);
	if(secAnswer.length <= 0){
		$("#error_sec_ans").html("Answer should not be blank .");
		valid = false;
	  }
	}*/

if(valid == true) {
	$("#registerButton").addClass("disabled");
	$("#registerButton").html(spinnerUrl);
	
	$.ajax({
		type : "POST",
		headers: headers,
		contentType : "application/json",
		url : "/Api/v1/User/Windows/en/WebRegistration/Process",
		dataType : 'json',
		data : JSON.stringify({
			"firstName" : "" + firstName+ "",
			"lastName" : " "+ lastName+"",
			"password" : "" + password + "",
			"contactNo" : "" + contactNo + "",
			"email" : "" + email + "",
			"locationCode" : "" + locationCode + "",
			"gender" : "NA",
			"dateOfBirth" : "" + dob + "",
			"secQuestionCode" : "" + secQuestion + "",
			"secAnswer" : "" + secAnswer + "",
			"captchaResponse" : ""+captchaResponse+"",
		}),
		
		success : function(response) {
		$( ".captcha_link" ).trigger( "click" );
			$("#registerButton").removeClass("disabled");
			$("#registerButton").html("Sign Up");
			if (response.code.includes("S00")) {
				$("#userModal").modal("hide");
				$("#newOTPModal").modal("show");
				$("#reg_otp_username").val($('#contact_no').val());
				$("#regMessage_success").html("Registration Successful OTP sent to "+ $('#contact_no').val()+" and verification mail sent to "+ $('#email').val());
			
			}else if(response.code.includes("F00")){
				if(response.details == null) {
						$("#error_captcha").html(response.message);
				}		
			}else if(response.code.includes("F04")){
				var msg=[];
				msg  = response.details.split("|");
				$("#common_error").modal("show");
				$("#common_success").html(msg[1]);
				var timeout = setTimeout(function(){
		            $("#common_error").modal('hide');
		          }, 3000);
				
			if(msg[1].includes("User already exists")){
				$("#common_error").modal("show");
				$("#common_success").html(msg[1]);
				var timeout = setTimeout(function(){
		            $("#common_error").modal('hide');
		          }, 3000);
			}
			if(msg[1].includes("Email already exists")){
				$("#common_error").modal("show");
				$("#common_success").html(msg[1]);
				var timeout = setTimeout(function(){
		            $("#common_error").modal('hide');
		          }, 3000);
			}
			if(msg[1].includes("Enter valid mobile number")){
				$("#common_error").modal("show");
				$("#common_success").html(msg[1]);
				var timeout = setTimeout(function(){
		            $("#common_error").modal('hide');
		          }, 3000);
			}
			if(msg[1].includes("Not a valid Pincode")){
				$("#common_error").modal("show");
				$("#common_success").html(msg[1]);
				var timeout = setTimeout(function(){
		            $("#common_error").modal('hide');
		          }, 3000);
			}
			    $("#form_error").html(msg);
		}

		}
	});
}

var timeout = setTimeout(function(){
    $("#error_first_name").html("");
	$("#error_last_name").html("");
	$("#error_password").html("");
	$("#error_cnfpassword").html("");
	$("#error_email").html("");
	$("#error_pincode").html("");
	$("#error_contact_no").html("");
	$("#error_gender").html("");
	$("#error_dob").html("");
	$("#error_captcha").html("");
	$("#error_sec_ans").html("");
}, 5000);

}
    });


$("#register_resend_otp").click(function() {

	$("#register_resend_otp").html(spinnerUrl);
	$("#register_resend_otp").addClass("disabled");
/*	var recaptcha_response = $("#g-recaptcha-response-1").val();
	if(recaptcha_response.length <=0){
		$("#error_captcha1").html("Enter Captcha");
		  valid = false;
	}*/
	var valid=true;
	if(valid==true){
	$.ajax({
		type : "POST",
		headers :headers,
		contentType : "application/json",
		url : "/Api/v1/User/Windows/en/WebRegistration/ResendMobileOTP",
		dataType : 'json',
		data : JSON.stringify({
			"mobileNumber" : "" + $('#reg_otp_username').val() + "",
		//	"captchaResponse":""+recaptcha_response+""
		}),
		success : function(response) {
		//	$( ".captcha_link" ).trigger( "click" );
			$("#register_resend_otp").html("Resend OTP");
			$("#register_resend_otp").removeClass("disabled");
			if (response.code.includes("S00")) {
				$("#regMessage_success").html(response.details);
				$("#fpOTP_message").html(response.details);
			}
		}
	});	
	}
	var timeout = setTimeout(function(){
		$("#error_captcha1").html("");
		$("#g-recaptcha-response-1").val('');
	}, 3000);
});

$("#forgot_password_request").click(function(){
	$("#userModal").modal("hide");
	$("#fpOTP_message").text("");
	var oldVal = $("#fp_submit").val();
	$("#forgot_password_request").addClass("disabled");
	var username = $('#fp_username').val();
//	var recaptcha_response = $("#g-recaptcha-response-2").val();
	var mobilePattern="^(?=(?:[7-9]){1})(?=[0-9]{10}).*";
	var valid = true;
	if(username.length != 10){
		$("#fp_error_username").html("Enter 10 digit Mobile Number");
		valid = false;
	}else if(!username.match(mobilePattern)){
		$("#fp_error_username").html("Enter valid mobile number");
		valid = false;
	}
	/*if(recaptcha_response.length <=0){
		$("#error_captcha2").html("Enter Captcha");
		  valid = false;
	}*/
	
	if(valid == false){
		$("#forgot_password_request").removeClass("disabled");
		$("#forgot_password_request").val("Continue");
	}
    if(valid == true){
	$.ajax({
		type : "POST",
		headers : headers,
		contentType : "application/json",
		url : "/ForgotPassword",
		data : JSON.stringify({
			"username" : "" + $('#fp_username').val() + "",
		//	"captchaResponse" : ""+recaptcha_response+""
		}),
		dataType : 'json',
		success : function(response) {
			$( ".captcha_link" ).trigger( "click" );
			$("#forgot_password_request").val(oldVal)
			$("#forgot_password_request").removeClass("disabled");
			if (response.code.includes("S00")) {
				$("#forgotPassword").modal("hide");
				$("#fpOTP").modal("show");
				$("#fpusername_forgot").val($('#fp_username').val());
				$("#fpOTP_message").css("color", "green");
				$("#fpOTP_message").html(response.details);
			} if(response.code.includes("F00")){
				if(response.details == null) {
					$("#error_captcha2").html(response.message);
			}else{
				$("#fp_error_username").html(response.message);
			}
		}
		}
	});
	}
    var timeout = setTimeout(function(){
    	$("#g-recaptcha-response-2").val('');
        $("#fp_error_username").html("");
    	$("#error_captcha2").html("");
    }, 3000);
	
});

$("#forgot_password_resend_otp").click(function() {
	$( ".captcha_link" ).trigger( "click" );
//	var recaptcha_response = $("#g-recaptcha-response-3").val();
	$("#forgot_password_resend_otp").addClass("disabled");
	$.ajax({
		type : "POST",
		headers : headers,
		contentType : "application/json",
		url : "/ForgotPassword",
		dataType : 'json',
		data : JSON.stringify({
			"username":""+$('#fpusername_forgot').val()+"",
		//	"captchaResponse" : ""+recaptcha_response+""
		}),
		success : function(response) {
		//	$( ".captcha_link" ).trigger( "click" );
			$("#forgot_password_resend_otp").removeClass("disabled");
			$("#forgot_password_resend_otp").val("RESEND OTP");
			if (response.code.includes("S00")) {
				$("#forgot_password_modal").modal("hide");
				$("#fpOTP_message").css("color", "green");
				$("#fpOTP_message").html(response.details);
			} else{
				$("#fpOTP_message").html(response.message);
			}
		 } 
	 });
 });

$("#register_verify_mobile").click(function() {
	console.log("register_verify_mobile=");
	$("#register_verify_mobile").attr("disabled","disabled");
	$("#register_verify_mobile").html(spinnerUrl);
	$("#register_resend_otp").attr("disabled","disabled");
	$.ajax({
		type : "POST",
		headers : headers,
		contentType : "application/json",
		url : "/Api/v1/User/Windows/en/Registration/MobileOTP",
		dataType : 'json',
		data : JSON.stringify({
			"mobileNumber" : "" + $('#reg_otp_username').val() + "",
			"key" : "" + $('#verify_reg_otp_key').val() + ""
		}),
		success : function(response) {
			console.log("register_verify_mobile="+response.code);
			$("#register_resend_otp").removeAttr("disabled");
			$("#register_verify_mobile").html("Verify Mobile");
			$("#register_verify_mobile").removeAttr("disabled");
			if (response.code.includes("S00")){
				$("#first_name").val("");
				$("#last_name").val("");
				$("#password").val("");
				$("#confirm_password").val("");
				$("#email").val("");
				$("#contact_no").val("");
				$("#gender").val("");
				$("#locationCode").val("");
				$("#dob").val("");
				$("#newOTPModal").modal('hide');
				$("#verifiedMessage").modal('show');
				$("#success_verification_message").html(response.details+" Please login to continue");
				var timeout = setTimeout(function(){
		            $("#verifiedMessage").modal('hide');
		          }, 3000);
			} else {
				$("#regMessage_success").html(response.details);
			}
		}
	});
});

$("#process_forgot_password_request").click(function() {
	$( ".captcha_link" ).trigger( "click" );
	$("#fpOTP_message").text("");
	var key = $("#fpOTP_key").val();
	var newPassword = $("#fpnewPassword_key").val();
	var confirmPassword =  $('#fpconfirmPassword_key').val();
	var valid = true;
	
	if(key.length ==0){
		$("#error_otp").html("Enter OTP");
		  valid = false;
	}
	if(newPassword.length <=0){
		$("#error_new_password").html("Enter New Password.");
		  valid = false;
	}
	if(confirmPassword.length <=0){
		$("#error_confirm_password").html("Enter Confirm Password.");
		  valid = false;
	}
	if(valid == true) {
		$("#process_forgot_password_request").addClass("disabled");
	if(newPassword == confirmPassword) {
	$.ajax({
		type : "POST",
		headers : headers,
		contentType : "application/json",
		url : "/ChangePasswordWithOTP",
		dataType : 'json',
		data : JSON.stringify({
			"username" : "" + $('#fpusername_forgot').val() + "",
			"newPassword" : "" + newPassword + "",
			"confirmPassword" : "" + confirmPassword + "",
			"key" : "" + key + ""
		}),
		success : function(response) {
		//	$( ".captcha_link" ).trigger( "click" );
			$("#process_forgot_password_request").removeClass("disabled");
			if (response.code.includes("S00")) {
				$("#fpOTP").modal("hide");
				$("#successNotification").modal("show");
				$("#success_alert").html(response.message);
				var timeout = setTimeout(function(){
		            $("#successNotification").modal('hide');
		            $("#fpOTP_key").val('');
		            $("#fpnewPassword_key").val('');
		            $('#fpconfirmPassword_key').val('');
		          }, 3000);
			} else {
				$("#fpOTP_message").css("color", "red");
				$("#fpOTP_message").text(response.message);
			}
		}
	});
	}else {
//		$('#fpnewPassword_key').val("Please enter same password in both fields");
		$("#error_confirm_password").text("Password does not match");
	}
  }	
	var timeout = setTimeout(function(){
		$("#error_otp").html("");
		$("#error_new_password").html("");
		$("#error_confirm_password").html("");
	}, 3000);

});

$("#termsConditions").click(function() {
	var isChecked = $("#termsConditions").is(":checked");
	if (isChecked) {
		$("#registerButton").removeClass("disabled");
	} else {
		$("#registerButton").addClass("disabled");
	}

	
});

	$(".captcha_link").click(function(){
		var $elem = $('.glyphicon-refresh');
		var angle=360;
		$({deg: 0}).animate({deg: angle}, {
			duration: 1000,
			step: function(now) {
				$elem.css({
					transform: 'rotate(' + now + 'deg)'
				});
			}
		});
		$(".captcha_image").attr("src","/Captcha");
	});

	$(".numeric").keydown(function(event){
		if(event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || (event.keyCode == 65 && event.ctrlKey == true) || (event.keyCode >= 35 && event.keyCode<=39)){
			return;
		}else{
			if(event.shiftKey || (event.keyCode < 48 || event.keyCode >57) && (event.keyCode < 96 || event.keyCode > 105)) {
				event.preventDefault();
			}
		}
	});

	$("#forgot_password_modal").click(function(){
		$(".captcha_link").trigger("click");
	})
	
	function checkStrength(password) {
		var strength = 0
		if (password.length < 6) {
			$('#strength_password').removeClass()
			$('#strength_password').addClass('short')
			return 'Too short'
		}
		if (password.length > 7) strength += 1
		// If password contains both lower and uppercase characters, increase strength value.
		if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/)) strength += 1
		// If it has numbers and characters, increase strength value.
		if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/)) strength += 1
		// If it has one special character, increase strength value.
		if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/)) strength += 1
		// If it has two special characters, increase strength value.
		if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/)) strength += 1
		// Calculated strength value, we can return messages
		// If value is less than 2
		if (strength < 2) {
			$('#strength_password').removeClass()
			$('#strength_password').addClass('weak')
			return 'Weak'
		} else if (strength == 2) {
			$('#strength_password').removeClass()
			$('#strength_password').addClass('good')
			return 'Good'
		} else {
			$('#strength_password').removeClass()
			$('#strength_password').addClass('strong')
			return 'Strong'
		}
	}
});

