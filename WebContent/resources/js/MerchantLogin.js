$(document).ready(function(){
	var spinnerUrl = "Please wait <img src='/resources/images/spinner.gif' height='20' width='20'>"
		var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");
	var csrfHeader = $("meta[name='_csrf_header']").attr("content");
	var csrfToken = $("meta[name='_csrf']").attr("content");
	var hash_key="hash";
	var default_hash="123456";
	var headers = {};
	headers[hash_key] = default_hash;
	headers[csrfHeader] = csrfToken;
	
    $(".img_show").hide();
	$("#show_img").click(function () {
		$(".img_show").show(1000);
	});
	
	$("#merchant_loginButton").click(function() {
		var valid = true;
		$("#logMessage_success").html("");
		$("#merchant_error_username").html("");
		$("#merchant_error_password").html("");
		var userNam = $('#merchant_username').val();
		var pwd = $('#merchant_password_login').val();
		
		if(pwd.length == 0){
			$("#merchant_error_password").html("Enter password");
			valid = false;
		}
		if(valid == true) {
			$("#merchant_loginButton").attr("disabled","disabled");
			$("#merchant_loginButton").html(spinnerUrl);
			console.log("inside this controller");
			 $.ajax({
                 type: "POST",
                 headers: headers,
                 contentType: "application/x-www-form-urlencoded",
                 url: "/Merchant/WebProcess",
                 data: {
                     username: userNam,
                     password: pwd
                 },
                 success: function (response) {
                	 console.log(response);
                	 console.log("response ::" +response.code);
                	 if(response.code.includes("S00")){
  						window.location = "/Merchant/Home";
 					}else if (response.code.includes("F05")){
 						 $("#merchntModal").modal("hide"); 
                 	      $("#merchant_loginButton").removeAttr("disabled"); 
                 	      $("#merchant_loginButton").html("Login");
                     	 $("#errorMessage").modal("show");
                     	$("#error_message").html(response.message);
                     	var timeout = setTimeout(function(){
				            $("#errorMessage").modal('hide');
				            $('#merchant_username').val('');
				            $('#merchant_password_login').val('');
				     }, 3000);
 					}else if(response.code.includes("F04")){
 						 $("#merchntModal").modal("hide"); 
 						$("#merchant_loginButton").removeAttr("disabled");
                    	$("#merchant_loginButton").html("Login");
                    	$("#errorMessage").modal("show");
                      	$("#error_message").html(response.message);
                      	var timeout = setTimeout(function(){
 				            $("#errorMessage").modal('hide');
 				           $('#merchant_username').val('');
				            $('#merchant_password_login').val('');
 				        }, 3000);
 					}
                     
                 }
             });
		}
		var timeout = setTimeout(function(){
			$("#merchant_error_username").html("");
			$("#merchant_error_password").html("");
	        }, 3000);
	});
	
	$("#login_verify_mobile").click(function() {
		var valid = true;
		$("#logMessage_success").html("");
		$("#error_otp").html("");
		var userNam = $('#login_otp_username').val();
		var pwd = $('#login_otp_password').val();
		var otp = $('#verify_otp_key').val();
		if(otp.length == 0){
		$("#error_otp1").html("Enter valid OTP");
	     valid = false;
		}else if(otp.length != 6){
			valid = false;
			$("#error_otp").html("Please enter 6 digit OTP");
		}
		
		if(valid == true) {
			$("#login_verify_mobile").html(spinnerUrl);
			 $.ajax({
                 type: "POST",
                 headers: headers,
                 contentType: "application/x-www-form-urlencoded",
                 url: "/Api/v1/User/Android/en/Login/WebProcess",
                 data: {
                     username: userNam,
                     password: pwd,
                     validate:true,
                     mobileToken: otp
                 },
                 success: function (response) {
//                     console.log(response.response);
                     if(response.code.includes("S00")){
  						console.log("Success");
  						window.location = "User/Home";
  						
  					}else if(response.code.includes("F04")){
 						$("#loginOTPVerification").modal("show");
 						$("#logMessage_success").css("color", "red");
 						$("#logMessage_success").html(response.message);
 						$("#loginButton").removeAttr("disabled");
 	                	$("#loginButton").html("Login");
 						$("#login1").hide();
 						$("#login_verify_mobile").html("Continue");
 						var timeout = setTimeout(function(){
 				            $("#logMessage_success").html('');
 				        }, 3000);
 						
 					}
                 }
             });
		}
		var timeout = setTimeout(function(){
	            $("#error_otp1").html('');
	        }, 3000);
	});
	
	$("#login_resend_OTP").click(function() {
		var valid = true;
		$("#logMessage_success").html("");
		$("#error_otp1").html("");
		$('#verify_otp_key').val("");
		var userNam = $('#login_otp_username').val();
		var pwd = $('#login_otp_password').val();
		var otp = $('#verify_otp_key').val();
		
		if(valid == true) {
			 $.ajax({
                 type: "POST",
                 headers: headers,
                 contentType: "application/x-www-form-urlencoded",
                 url: "/Api/v1/User/Android/en/Login/WebProcess",
                 data: {
                     username: userNam,
                     password: pwd
                 },
                 success: function (response) {
//                     console.log(response.response);
                     if(response.code.includes("L01")){
  						$("#logMessage_success").css("color", "green");
  						$("#logMessage_success").html("OTP sent to "+ $('#username').val());
  						$("#loginButton").removeAttr("disabled");
  	                	$("#loginButton").html("Login");
  						$("#login1").hide();
  					}
                 }
             });
		}
	});
	
});