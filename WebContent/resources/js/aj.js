/**
 * 
 */
//For Resend EmailOTP
$(document).ready(function() {
	var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");
	var csrfHeader = $("meta[name='_csrf_header']").attr("content");
	var csrfToken = $("meta[name='_csrf']").attr("content");
	var headers = {};
	headers[csrfHeader] = csrfToken;
	$('#re_send_otp').click(function(){
		$("#re_send_otp").addClass("disabled");
		$.ajax({
			type : "POST",
			headers: headers,
			url : "/User/ReSendEmailOTP",
			contentType : "application/json",
			datatype : 'json',
			success : function(response) {
				$("#re_send_otp").removeClass("disabled");
				$("#alert_verification_message").html('You again received a verification on you registered Email Id');
			}
			
		})
	})
	$("#dateFilterReceipt").click(function() {
	var fromDate = $("#fromDate1").val();
	var toDate = $("#toDate1").val();
	
	$.ajax({
		type : "POST",
		url : "/Api/v1/User/Windows/en/User/UpdateReceiptInAjax",
		headers: headers,
		contentType : "application/json",
		datatype : 'json',
		data : JSON.stringify({
			"fromDate" : "" + fromDate+ "",
			"toDate" : ""+toDate+"",
		}),
		success : function(response) {
			var u = response;
			if(u.code.includes("S00")){
			$("#Date_picker0").modal('hide');
			$("#account_credit").html(u.credit);
			$("#account_debit").html(u.debit);
			 var timeout = setTimeout(function(){
		            $("#fromDate1").val('');
		            $("#toDate1").val('');
		          }, 3000);
			}else if(u.code.includes("F03")){
			location.reload();
			}
		}
	});
  })
})


/**
 * For Redeem Coupon Code
 **/

$(document).ready(function(){
	$("#redeem_button").click(function() {
		$("#redeem_button").addClass("disabled");
		$("#redeem_button").html("Please Wait...");
		var code =$("#couponNumber").val();
		$.ajax({
			type : "POST",
			url : "/User/RedeemCoupon",
			contentType : "application/x-www-form-urlencoded",
			data : {
				promoCode : code
			},
				success : function(response) {
					$("#redeem_button").removeClass("disabled");
					$("#redeem_button").html("Redeem");
						var parsedResponse = JSON.parse(response.response);
						 $("#redeemId").html(parsedResponse.message)
				}
			});
	
	});
	
})