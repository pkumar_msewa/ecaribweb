/**
f * 
 */
$(document).ready(function(){
	var spinnerUrl = "Please wait <img src='/resources/images/spinner.gif'  height='20' width='20'>"
	var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");
	var csrfHeader = $("meta[name='_csrf_header']").attr("content");
	var csrfToken = $("meta[name='_csrf']").attr("content");
	var hash_key="hash";
	var default_hash="123456";
	var headers = {};
	headers[hash_key] = default_hash;
	headers[csrfHeader] = csrfToken;
	
	$("#ife_submit").click(function(){
		$("#ife_submit").addClass("disabled");
		$("#ife_submit").html(spinnerUrl);
//		$("#error_ife_mail").html("");ife_mobileNo
		$("#error_ife_mobileNo").html("");
		$("#error_ife_receiver").html("");
        $("#error_ife").html("");
        $("#success_ife").html("");
		var valid = true;
//		var mail = $("#ife_email").val();
		var mobile=$("#ife_mobileNo").val();
		var pattern = "[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$"
		var receiver = $("#ife_receiver").val();
		var recaptcha_response = $("#g-recaptcha-response").val();
/*		if(!(mail.match(pattern))){
			$("#error_ife_mail").html("enter valid email");
			valid = false;
		}*/
		if(mobile.length != 10) {
			$("#error_ife_mobileNo").html("Enter valid mobile number");
			valid = false;
		}
		if(receiver.length <= 0){
			$("#error_ife_receivername").html("Enter receiver's name");
			valid = false;
		}
		if(valid == false){
			$("#ife_submit").removeClass("disabled");
			$("#ife_submit").html("Invite");
			  var timeout = setTimeout(function(){
			    	$("#error_ife_mobileNo").html("");
					$("#error_ife_receivername").html("");
		          }, 3000);
		}
		if(valid == true){
			
			$.ajax({
				type : "POST",
				headers: headers,
				contentType : "application/x-www-form-urlencoded",
				url : "/User/Invite/Mobile",
				data : {
//					email:mail,
					mobileNo:mobile,
					receiversName:receiver,
					captchaResponse: recaptcha_response
				},
				success : function(response) {
					$(".captcha_link").trigger("click");
    				$("#ife_submit").removeClass("disabled");
					$("#ife_submit").html("Invite");



						if(response.code == "F00"){
						    $("#error_ife").html(response.message);
						    var timeout = setTimeout(function(){
						    	$("#error_ife").html("");
						    	$("#ife_mobileNo").val("");
								$("#ife_receiver").val("");
					          }, 4000);
						}
						if(response.code == "S00"){
						    $("#success_ife").html(response.message);
						    var timeout = setTimeout(function(){
						    	$("#success_ife").html("");
						    	$("#ife_mobileNo").val("");
								$("#ife_receiver").val("");
					          }, 4000);

						}
				}
			});

			
		}
		
		
		
	});
	$("#submit_update").click(function(){
		if($("#check").prop('checked') == true){
		$("#submit_update").addClass("disabled");
		$("#submit_update").html(spinnerUrl);
		$("#error_aadhar").html("");
        $("#error_kyc").html("");
        $("#success_kyc").html("");
		var valid = true;
		var aadharNumber=$("#aadharId").val();
		var pattern = "^(0|[1-9][0-9]*)$"
		if(aadharNumber.length != 12) {
			$("#error_aadhar").html("Enter valid aadhar number");
			valid = false;
		}
		if(valid == false){
			$("#submit_update").removeClass("disabled");
			$("#submit_update").html("Update");
			  var timeout = setTimeout(function(){
			    	$("#error_aadhar").html("");
		          }, 3000);
		}
		if(valid == true){
			
			$.ajax({
				type : "POST",
				headers: headers,
				contentType : "application/x-www-form-urlencoded",
				url : "/User/Kyc/webUpdate",
				data : {
					aadharNumber:aadharNumber,
				},
				success : function(response) {
    				$("#submit_update").removeClass("disabled");
					$("#submit_update").html("Update");
						if(response.code == "F00"){
						    $("#error_kyc").html(response.message);
						    var timeout = setTimeout(function(){
						    	$("#error_kyc").html("");
						    	$("#aadharId").val("");
					          }, 3000);
						}
						if(response.code == "S00"){
							console.log("adhar"+aadharNumber+": otpId"+response.otpRequestId);
							$('#otpReqId').val(response.otpRequestId);
							$('#aadharNo').val(aadharNumber);
							$("#aadharId").val("");
							 $("#success_otp").html(response.message);
							$('#updateKyc').modal("hide");
							$('#updateOtp').modal("show");
						}
						if(response.code == "F03"){
							 $("#error_aadhar").html("Session expired,please login again");
							 var timeout = setTimeout(function(){
								 location.reload();
						          }, 1000);
						}
				}
			});
		}
		}
	});
	$("#submit_otp").click(function(){
		$("#error_aadhar_otp").html("");
        $("#error_otp").html("");
        $("#success_otp").html("");
		var valid = true;
		var otp=$("#aadhar_otp").val();
		var aadharNo=$("#aadharNo").val();
		var otpReqId=$("#otpReqId").val();
		var pattern = "^(0|[1-9][0-9]*)$"
		if(otp.length != 6) {
			$("#error_aadhar_otp").html("Enter valid otp number");
			valid = false;
		}
		console.log(aadharNo+"values"+otpReqId);
		if(valid && (aadharNo.length <=0 || otpReqId.length <=0)) {
			$("#error_aadhar_otp").html("Invalid request,please try later");
			valid = false;
		}
		if(valid == true){
			$("#submit_otp").addClass("disabled");
			$("#submit_otp").html(spinnerUrl);
			$.ajax({
				type : "POST",
				headers: headers,
				contentType : "application/x-www-form-urlencoded",
				url : "/User/Kyc/webUpdateOtp",
				data : {
					key:otp,
					aadharNumber:aadharNo,
					otpreqId:otpReqId
				},
				success : function(response) {
    				$("#submit_otp").removeClass("disabled");
					$("#submit_otp").html("Verify");
						if(response.code == "F00"){
						    $("#error_otp").html(response.message);
						    var timeout = setTimeout(function(){
						    	$("#error_otp").html("");
						    	$("#aadharId").val("");
					          }, 3000);
						}
						if(response.code == "S00"){
							console.log(response);
						    $("#success_otp").html(response.message);
						    $("#aadhar_otp").val("");
						    $('#kyc_update_disable').hide();
						    $('#cNameId').html(response.customerName);
						    $('#cAddId').html(response.customerAddress);
						    $('#updateOtp').hide();
						    $('#showDetails').modal('show');
//						    var timeout = setTimeout(function(){
//						    	$("#success_otp").html("");
//						    	$("#otpReqId").val("");
//						    	$("#aadharNo").val("");
//						    	$('#updateOtp').modal("hide");
//					          }, 3000);

						}
						if(response.code == "F03"){
							 $("#error_otp").html("Session expired,please login again");
							 var timeout = setTimeout(function(){
								 location.reload();
						          }, 1000);
						}
				}
			});
		}
	});


	$(".captcha_link").click(function(){
		var $elem = $('.glyphicon-refresh');
		var angle=360;
		$({deg: 0}).animate({deg: angle}, {
			duration: 1000,
			step: function(now) {
				$elem.css({
					transform: 'rotate(' + now + 'deg)'
				});
			}
		});
		$(".captcha_image").attr("src","/Captcha");
	});
	
});