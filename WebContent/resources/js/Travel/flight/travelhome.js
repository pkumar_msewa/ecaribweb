$(document).ready(function(){

	
$("#flght_srch").on('click', function() {

	var spinnerUrl = "Please wait <img src='/resources/images/spinner.gif' style='width:25px;'>";

	$("#trav_menu").hide();
	document.getElementById("errormsgsrc").innerHTML="";
	document.getElementById("responsemessage").innerHTML="";
	document.getElementById("errormsgdest").innerHTML="";
	var flightdest = document.getElementById("flightdestination");
	var flightdestselect = flightdest.options[flightdest.selectedIndex].value;

	var flightsrc = document.getElementById("flightsource");
	var flightsrcselect = flightsrc.options[flightsrc.selectedIndex].value;
	var date = document.getElementById("date").value;

	var cabin = document.getElementById("cabin").value;
	var adults = document.getElementById("adult").value;
	var childs = document.getElementById("childs").value;
	var infants = document.getElementById("infants").value;
	var returndate = document.getElementById("returndate").value;

	var srchtml=flightsrc.options[flightsrc.selectedIndex].text;
	var desthtml=flightdest.options[flightdest.selectedIndex].text;
	var valid=true;
	
	if(flightsrcselect.length <=1)
	{
		document.getElementById("errormsgsrc").innerHTML="Please Select Source";
		valid=false;
	}
	if(flightdestselect.length <=1)
	{
		document.getElementById("errormsgdest").innerHTML="Please Select Destination";
		valid=false;
	}
	if(date.length <=1)
	{
		document.getElementById("errDeptdate").innerHTML="Please Select Date";
		valid=false;
	}

	if(date.length > 0 && returndate.length > 0){
		valid=compareDate(returndate, date);
		document.getElementById("errDeptdate").innerHTML="";
		if(!valid){
			document.getElementById("errDeptdate").innerHTML="Departure date can't be greater than retrun date";
		}
	}
	if(valid)
	{
		$("#flght_srch").addClass("disabled");
		$("#loading_flight").modal({
			backdrop: 'static',
			keyboard: true,  
			show: true
		}); 
		if(returndate.length <=1)
		{
			$.ajax({
				type : "POST",
				contentType : "application/json",
				url : contextPath+"/User/Travel/Flight/OneWay",
				data : JSON.stringify({
					
					"origin" : "" +flightsrcselect + "",
					"destination" : "" +flightdestselect + "",
					"beginDate" : "" +date + "",
					"engineIDs" : "" +"AYTM00011111111110002" + "",
					"tripType" : "" +"OneWay" + "",
					"cabin" : "" +cabin + "",
					"adults" : "" +adults + "",
					"childs" : "" + childs+ "",
					"infants" : "" +infants + "",
					"endDate":"" + "",
					"traceId" : "" + "AYTM00011111111110002"+ "",
					"srcFullName":""+srchtml+"",
					"destFullName":""+desthtml+"",

				}),
				success : function(response) {

					var d=response.split("#");
					if(d[0]==="S00")
					{
						window.location = "OneWay";
					}
					else if (d[0]==="F03") {
						window.location.href=contextPath+'/Home'; 
					}
					else if (d[0]==="F02") {
						$("#loader").hide();
						$("#loading_flight").hide();
						$("#flght_srch").removeClass("disabled");
						document.getElementById("responsemessage").innerHTML="Server Down.Please Try Again Later";	
					}
					else
					{
						$("#loader").hide();
						$("#loading_flight").hide();
						$("#flght_srch").removeClass("disabled");
						document.getElementById("responsemessage").innerHTML=""+d[1];	
					}
				}
			});
		}
		else
		{
			$.ajax({
				type : "POST",
				contentType : "application/json",
				url : contextPath+"/User/Travel/Flight/OneWay",
				data : JSON.stringify({
					"origin" : "" +flightsrcselect + "",
					"destination" : "" +flightdestselect + "",
					"beginDate" : "" +date + "",
					"engineIDs" : "" +"AYTM00011111111110002" + "",
					"tripType" : "" +"RoundTrip" + "",
					"cabin" : "" +cabin + "",
					"adults" : "" +adults + "",
					"childs" : "" + childs+ "",
					"infants" : "" +infants + "",
					"endDate":"" +returndate + "",
					"traceId" : "" + "AYTM00011111111110002"+ "",
					"srcFullName":""+srchtml+"",
					"destFullName":""+desthtml+"",

				}),
				success : function(response) {

					var d=response.split("#");
					if(d[0]==="S00")
					{
						if(d[1]==="DomesticRoundway")
						{
							window.location = "RoundWay";
						}
						else
						{
							window.location = "internationalRoundWay";
						}
					}
					else if (d[0]==="F03") {
						window.location.href=contextPath+'/Home'; 
					}
					else if (d[0]==="F02") {
						$("#loader").hide();
						$("#loading_flight").hide();
						$("#flght_srch").removeClass("disabled");
						document.getElementById("responsemessage").innerHTML="Server Down.Please Try Again Later";	
					}
					else
					{
						$("#loader").hide();
						$("#loading_flight").hide();
						$("#flght_srch").removeClass("disabled");
						document.getElementById("responsemessage").innerHTML=""+d[1];	
					}
				}
			});
		}
	}

});


function returndate()
{
	$("#returntrip").show();
}

$("#cabin").on('change', function() {

	var d =document.getElementById("cabin").value;
	document.getElementById("flight_cls").innerHTML=", "+d;

});

	$("#rm_ppl").click(function () {
		$("#htl_menu").fadeToggle();
	});

	$("#selTrav").click(function () {
		$("#trav_menu").fadeToggle();
	});


	$('#round').click(function(){
		if($('input[name="returndate"]').prop('disabled'))
		{
			$('input[name="returndate"]').prop('disabled', false)
		}
	});

	$('#one-way').click(function(){
		if($('input[name="returndate"]').prop('disabled', false))
		{
			$('input[name="returndate"]').prop('disabled', true)
		}
	});

	var session=$("#session").val();
	$(".js-example-basic-multiple").select2();
	$("#loading_flight").modal({backdrop: false});
	$("#flght_srch").addClass("disabled");
	$('#flightsource').empty();
	$('#flightdestination').empty(); 
	$('#flightsource').append('<option value="#"> Select Origin </option>');
	$('#flightdestination').append('<option value="#"> Select Destination</option>');

	$.ajax({
		type : "POST",
		contentType : "application/json",
		url :contextPath+"/User/Travel/Flight/AirLineNames",
		dataType : 'json',
		data : JSON.stringify({
			"sessionId": session,
		}),
		success : function(response) {

			for(var i=0;i<response.details.length;i++){

				$('#flightsource').append('<option value="' + response.details[i].cityCode + '">' + response.details[i].cityName + ' ('+response.details[i].cityCode+')'+ '</option>');
				$('#flightdestination').append('<option value="' + response.details[i].cityCode + '">' + response.details[i].cityName + ' ('+response.details[i].cityCode+')'+ '</option>');

			}
			$("#loading_flight").modal("hide");

			$("#flght_srch").removeClass("disabled");

			var today = new Date();
			var dd = today.getDate();
			var mm = today.getMonth()+1; //January is 0!

			var yyyy = today.getFullYear();
			if(dd<10){
				dd='0'+dd;
			} 
			if(mm<10){
				mm='0'+mm;
			} 
			var today = dd+'/'+mm+'/'+yyyy;

			$("#date").val(today);
		}
	});  


$( "#flightsrc" ).select2({
	theme: "bootstrap"
});
$( "#flightdest" ).select2({
	theme: "bootstrap"
});
$( "#location_list" ).select2({
	theme: "bootstrap"
});
$( "#bus_src_list" ).select2({
	theme: "bootstrap"
});
$( "#bus_des_list" ).select2({
	theme: "bootstrap"
});
$( "#holiday_list" ).select2({
	theme: "bootstrap"
});


$(".beSwapCity").on('click', function() {
	var fromcurrency =  $('#flight_src_list').val();
	var tocurrency = $('#flight_des_list').val();
	$('#flight_src_list').val(tocurrency).trigger('change');
	$('#flight_des_list').val(fromcurrency).trigger('change');
});


$("#closedivtrevalinformation").on('click', function() {

	$("#trav_menu").hide();
});


var today = new Date();
var stDate="";

$(function() {
	$("#date").datepicker({
		startDate: today,
		format : "dd/mm/yyyy",

	}).on('change', function() {
		$('.datepicker').hide();
	});
});

$( "#date" ).focus(function() {
	  $( this ).blur();
	});


$(function() {
	$("#returndate").datepicker({
		startDate: $("#date").val(),
		format : "dd/mm/yyyy",

	}).on('change', function() {
		$('.datepicker').hide();
	});  
});

$( "#returndate" ).focus(function() {
	  $( this ).blur();
	});

$(function() {
	$("#datecal").datepicker({
		startDate: today,
		format : "dd/mm/yyyy"
	}).on('change', function() {
		$('.datepicker').hide();
	});
});

$("#flightdestination").on('change', function() {
	$('#errormsgdest').empty();
	var sr=$('#flightdestination').val();
	var ds=$('#flightsource').val();
	
	if (!sr.includes('#')) {
	if (sr.includes(ds)) {
		$('#errormsgdest').html("Please select source & destation different.");
	}
	else{
		$('#errormsgsrc').empty();
	}
 }
});

$("#flightsource").on('change', function() {
	$('#errormsgsrc').empty();
	var sr=$('#flightdestination').val();
	var ds=$('#flightsource').val();

	if (!sr.includes('#')) {
	if (sr.includes(ds)) {
		$('#errormsgdest').html("Please select source & destation different.");
	}
	else{
		$('#errormsgsrc').empty();
	}
 }
});
});
function compareDate(returnDate, date){
var	startDate = date.split("/").reverse().join("-");
	var endDate=returnDate.split("/").reverse().join("-");
	var date1 = new Date(startDate);
	var date2= new Date(endDate);
	if(date2.getTime() < date1.getTime()){
		return false;
	}
	return true;
}