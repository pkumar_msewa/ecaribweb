$(document).ready(function(){
	var today = new Date();

	var minAmt=$('#minAmt').val();     
	var maxAmt=$('#maxAmt').val();    
	$(function () {
		$('#slider-container').slider({
			range: true,
			min: 3000,
			max: 50000,
			values: [3000, 50000],
			create: function() {
				$("#amount").val("Rs 3000 - RS 50000");
			},
			slide: function (event, ui) {
				$("#amount").val("Rs" + ui.values[0] + " - Rs" + ui.values[1]);
				var mi = ui.values[0];
				var mx = ui.values[1];
				filterSystem(mi, mx);
			}
		})
	});

	function filterSystem(minPrice, maxPrice) {
		$("#computers div.system").hide().filter(function () {
			var price = parseInt($(this).data("price"), 10);
			return price >= minPrice && price <= maxPrice;
		}).show();
	}


	var session=$("#session").val();
	
	$("#flght_srch").addClass("disabled");
	$('#flightsrc').empty();
	$('#flightdest').empty(); 
	$(".js-example-basic-multiple").select2();
	$('#flightsrc').append('<option value="#" selected="selected">Getting Origin.....</option>');
	$('#flightdest').append('<option value="#" selected="selected">Getting Destination.....</option>');
	var org=$('#origin').val();
	var dst=$('#destination').val();

	$.ajax({
		type : "POST",
		contentType : "application/json",
		url :contextPath+"/User/Travel/Flight/AirLineNames",
		dataType : 'json',
		data : JSON.stringify({
			"sessionId": session,
		}),
		success : function(response) {

			for(var i=0;i<response.details.length;i++){

				if (response.details[i].cityCode.includes(org)) {
					$('#flightsrc').append('<option value="' + response.details[i].cityCode + '" selected="selected">' + response.details[i].cityName + ' ('+response.details[i].cityCode+')'+ '</option>');
				}
				else{
					$('#flightsrc').append('<option value="' + response.details[i].cityCode + '" >' + response.details[i].cityName + ' ('+response.details[i].cityCode+')'+ '</option>');
				}
				if (response.details[i].cityCode.includes(dst)) {
					$('#flightdest').append('<option value="' + response.details[i].cityCode + '" selected="selected">' + response.details[i].cityName + ' ('+response.details[i].cityCode+')'+ '</option>');	
				}else{
					$('#flightdest').append('<option value="' + response.details[i].cityCode + '">' + response.details[i].cityName + ' ('+response.details[i].cityCode+')'+ '</option>');	
				}
			}
			
			$("#flght_srch").removeClass("disabled");
			var today = new Date();
			var dd = today.getDate();
			var mm = today.getMonth()+1; //January is 0!

			var yyyy = today.getFullYear();
			if(dd<10){
				dd='0'+dd;
			} 
			if(mm<10){
				mm='0'+mm;
			} 
			var today = dd+'/'+mm+'/'+yyyy;
			$("#returndate").val("");
			var bDate=$("#beginDate").val();
			var nbDate=bDate.split("-");
			nwDate=nbDate[2]+"/"+nbDate[1]+"/"+nbDate[0];
			console.log("DAte:: "+nwDate);
			$("#date").val(nwDate);
		}
	});
	
	$(function() {
		$("#date").datepicker({
			startDate: today,
			format : "dd/mm/yyyy"
		}).on('change', function() {
			$('.datepicker').hide();
		});
	});

	$(function() {
		$("#returndate").datepicker({
			startDate: today,
			format : "dd/mm/yyyy"
		}).on('change', function() {
			$('.datepicker').hide();
		});
	});

	$(function() {
		$("#datecal").datepicker({
			startDate: today,
			format : "dd/mm/yyyy"
		}).on('change', function() {
			$('.datepicker').hide();
		});
	});

	$("#swapVal").on('click', function() {
		var fromcurrency =  $('#flight_src_list').val();
		var tocurrency = $('#flight_des_list').val();
		$('#flight_src_list').val(tocurrency).trigger('change');
		$('#flight_des_list').val(fromcurrency).trigger('change');
	});


	var tmp="";
	$('.filter_link').click(function(e){

		var mediaElements = $('.media');
		// get the category from the attribute
		var filterVal = $(this).data('filter');

		tmp=tmp+","+filterVal;

		console.log("TotalfilterVal:: "+tmp);
		var fl=tmp.split(",");

		console.log("fl.lenght:: "+fl[1]);
		mediaElements.hide();
		for (var i = 1; i <=fl.length; i++) {
			console.log("filterVal:: "+fl[i]);
			mediaElements.filter('.' + fl[i]).show();
		}

//		mediaElements.hide().filter('.' + filterVal).show();

	});

	$('.filter_link_stopage').click(function(e){
		var mediaElements = $('.stopage');
		// get the category from the attribute

		var filterVal = $(this).data('filter');

		if(filterVal === 'all'){
			mediaElements.show();
		}else{
			// hide all then filter the ones to show
			mediaElements.hide().filter('.' + filterVal).show();
		}
	});


	$('#clr_filtr').click(function(e){
		var mediaElements = $('.stopage');
		var mediaElement = $('.media');
		// get the category from the attribute

		mediaElements.show();
		mediaElement.show();
		$("input[name='flnamefilture']").attr('checked', false);

		$("input[name='stopageall']").attr('checked', false);
		$("input[name='stopage']").attr('checked', false);
	});


	$( "#flightsrc" ).select2({
		theme: "bootstrap"
	});
	$( "#flightdest" ).select2({
		theme: "bootstrap"
	});
	$( "#location_list" ).select2({
		theme: "bootstrap"
	});
	$( "#bus_src_list" ).select2({
		theme: "bootstrap"
	});
	$( "#bus_des_list" ).select2({
		theme: "bootstrap"
	});
	$( "#holiday_list" ).select2({
		theme: "bootstrap"
	});

	$("#selTrav").click(function () {
		$("#trav_menu").fadeToggle();
	});

	$("#rm_ppl").click(function () {
		$("#htl_menu").fadeToggle();
	});

	$("#closedivtrevalinformation").on('click', function() {
		$("#trav_menu").hide();
	});


	$("#flightdest").on('change', function() {
		$('#errormsgdest').empty();
		var sr=$('#flightdestination').val();
		var ds=$('#flightsource').val();

		if (!sr.includes('#')) {
			if (sr.includes(ds)) {
				$('#errormsgdest').html("Please select source & destation different.");
				console.log("dfihjdsk");
			}
			else{
				$('#errormsgsrc').empty();
				console.log("sjdfikdsam dest msg");
			}
			console.log("sjdfikdsam dest");
		}
	});

	$("#flightsrc").on('change', function() {
		$('#errormsgsrc').empty();
		var sr=$('#flightdestination').val();
		var ds=$('#flightsource').val();

		if (!sr.includes('#')) {
			if (sr.includes(ds)) {
				$('#errormsgdest').html("Please select source & destation different.");
				console.log("dfihjdsk");
			}
			else{
				$('#errormsgsrc').empty();
			}
			console.log("sjdfikdsam src");
		}
	});


	$("#flght_srch").on('click', function() {

		var spinnerUrl = "Please wait <img src='/resources/images/spinner.gif' style='width:25px;'>";

		$("#trav_menu").hide();
		document.getElementById("errormsgsrc").innerHTML="";
		document.getElementById("responsemessage").innerHTML="";
		document.getElementById("errormsgdest").innerHTML="";
		var flightdest = document.getElementById("flightdestination");
		var flightdestselect = flightdest.options[flightdest.selectedIndex].value;

		var flightsrc = document.getElementById("flightsource");
		var flightsrcselect = flightsrc.options[flightsrc.selectedIndex].value;
		var date = document.getElementById("date").value;

		var cabin = document.getElementById("cabin").value;
		var adults = document.getElementById("adult").value;
		var childs = document.getElementById("childs").value;
		var infants = document.getElementById("infants").value;
		var returndate = document.getElementById("returndate").value;

		var srchtml=flightsrc.options[flightsrc.selectedIndex].text;
		var desthtml=flightdest.options[flightdest.selectedIndex].text;
		var valid=true;

		if(flightsrcselect.length <=1)
		{
			document.getElementById("errormsgsrc").innerHTML="Please Select Source";
			valid=false;
		}
		if(flightdestselect.length <=1)
		{
			document.getElementById("errormsgdest").innerHTML="Please Select Destination";
			valid=false;
		}
		if(date.length <=1)
		{
			document.getElementById("errDeptdate").innerHTML="Please Select Date";
			valid=false;
		}

		if(valid==true)
		{
			$("#flght_srch").addClass("disabled");
			$("#loading_flight").modal({
				backdrop: 'static',
				keyboard: true,  
				show: true
			}); 
			if(returndate.length <=1)
			{
				$.ajax({
					type : "POST",
					contentType : "application/json",
					url : contextPath+"/User/Travel/Flight/OneWay",
					data : JSON.stringify({

						"origin" : "" +flightsrcselect + "",
						"destination" : "" +flightdestselect + "",
						"beginDate" : "" +date + "",
						"engineIDs" : "" +"AYTM00011111111110002" + "",
						"tripType" : "" +"OneWay" + "",
						"cabin" : "" +cabin + "",
						"adults" : "" +adults + "",
						"childs" : "" + childs+ "",
						"infants" : "" +infants + "",
						"endDate":"" + "",
						"traceId" : "" + "AYTM00011111111110002"+ "",
						"srcFullName":""+srchtml+"",
						"destFullName":""+desthtml+"",

					}),
					success : function(response) {

						var d=response.split("#");
						if(d[0]==="S00")
						{
							window.location = "OneWay";
						}
						else if (d[0]==="F03") {
							window.location.href=contextPath+'/Home'; 
						}
						else if (d[0]==="F02") {
							$("#loader").hide();
							$("#loading_flight").hide();
							$("#flght_srch").removeClass("disabled");
							document.getElementById("responsemessage").innerHTML="Server Down.Please Try Again Later";	
						}
						else
						{
							$("#loader").hide();
							$("#loading_flight").hide();
							$("#flght_srch").removeClass("disabled");
							document.getElementById("responsemessage").innerHTML=""+d[1];	
						}
					}
				});
			}
			else
			{
				$.ajax({
					type : "POST",
					contentType : "application/json",
					url : contextPath+"/User/Travel/Flight/OneWay",
					data : JSON.stringify({
						"origin" : "" +flightsrcselect + "",
						"destination" : "" +flightdestselect + "",
						"beginDate" : "" +date + "",
						"engineIDs" : "" +"AYTM00011111111110002" + "",
						"tripType" : "" +"RoundTrip" + "",
						"cabin" : "" +cabin + "",
						"adults" : "" +adults + "",
						"childs" : "" + childs+ "",
						"infants" : "" +infants + "",
						"endDate":"" +returndate + "",
						"traceId" : "" + "AYTM00011111111110002"+ "",
						"srcFullName":""+srchtml+"",
						"destFullName":""+desthtml+"",

					}),
					success : function(response) {

						var d=response.split("#");
						if(d[0]==="S00")
						{
							if(d[1]==="DomesticRoundway")
							{
								window.location = "RoundWay";
							}
							else
							{
								window.location = "internationalRoundWay";
							}

						}
						else if (d[0]==="F03") {
							window.location.href=contextPath+'/Home'; 
						}
						else if (d[0]==="F02") {
							$("#loader").hide();
							$("#loading_flight").hide();
							$("#flght_srch").removeClass("disabled");
							document.getElementById("responsemessage").innerHTML="Server Down.Please Try Again Later";	
						}
						else
						{
							$("#loader").hide();
							$("#loading_flight").hide();
							$("#flght_srch").removeClass("disabled");
							document.getElementById("responsemessage").innerHTML=""+d[1];	
						}
					}
				});
			}
		}
	});

});