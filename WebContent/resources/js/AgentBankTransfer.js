
$(document).ready(function(){
		
		
	   	var spinnerUrl = "Please Wait <img src='/resources/images/spinner.gif'  height='20' width='20'>"
	    var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");
	    var csrfHeader = $("meta[name='_csrf_header']").attr("content");
	    var csrfToken = $("meta[name='_csrf']").attr("content");
	   	var hash_key = "hash";
	    var default_hash = "123456";
	    var headers = {};
	    headers[hash_key] = default_hash;
	    headers[csrfHeader] = csrfToken;
	    var ifscPattern = "^[A-Za-z]{4}[a-zA-Z0-9]{7}$";
	    
	    	$("#smm_bankName").select2();
		 	console.log("In agent BanList");
			$.ajax({
				type : "POST",
				headers:headers,
				contentType : "application/json",
//				contentType: "application/x-www-form-urlencoded",
				url : "/Agent/BankList",
				dataType : 'json',
				data : JSON.stringify({
				}),
				success : function(response) {
				console.log("CODE=="+response.code);
				console.log("details=="+response.details2);
				if(response.code.includes("S00")){
					for(var i=0;i<response.details2.length;i++){
						$('#smm_bankName').append('<option value="' + response.details2[i].bankName + '">' + response.details2[i].bankName + '</option>');
					//	$("#agentIfscCode").append('<option value="'+response.details[i]'">');
					  }
					}
				}
			});
			
			/*$("#bankName").change(function() {
				$.ajax({
					type : "POST",
					contentType : "application/json",
					url : "/Admin/Agent/GetBankIfscCode",
					dataType : 'json',
					data : JSON.stringify({
						"agentBankName":""+agentBankName+""
					}),
					success : function(response) {
					console.log("CODE=="+response.code);
					console.log("details=="+response.details2);
					if(response.code.includes("S00")){
						for(var i=0;i<response.details2.length;i++){
							$('#agentBankName').append('<option value="' + response.details2[i].bankName + '">' + response.details2[i].bankName + '</option>');
						//	$("#agentIfscCode").append('<option value="'+response.details[i]'">');
						  }
						}
					}
				});
			});*/
			
			
			$("#smm_ifsc_code").on("blur", function(){
				var ifsc_Code = $("#smm_ifsc_code").val();
				console.log("smm_ifsc_code="+ifsc_Code);
				
				var ifsc_Code = $('#smm_ifsc_code').val();
				if(ifsc_Code.length<=0){
					$('#error_smm_ifsc_code').html("Please enter bank IFSC code");
				}
				else if(!ifsc_Code.match(ifscPattern)){
					$('#error_smm_ifsc_code').html("Please enter valid IFSC code");
				}
				else{
					$('#error_smm_ifsc_code').html(" ");
				}
			});
			
			$("#smm_account_holder_name").bind("keypress", function (event) {
		            if (event.charCode!=0) {
		                var regex = new RegExp("^[a-zA-Z]+$");
		                var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
		                if (!regex.test(key)) {
		                    event.preventDefault();
		                    return false;
		                }
		            }
		     });
			
			$('#smm_account_number').keyup(function () { 
			    this.value = this.value.replace(/[^0-9\.]/g,'');
			});
			
			 $("#smm_account_number").on("blur", function(){
				var accountNumber = $('#smm_account_number').val();
				if(accountNumber.length<=0){
					$("#error_smm_account_number").html("Please enter Account number");
				}
				else if(accountNumber.length>16){
					$("#error_smm_account_number").html("Please enter valid Account number");
				}
				else{
					$('#error_smm_account_number').html(" ");
				}
			});
			 
			
			
			$('#confirm_account_number').keyup(function () { 
			    this.value = this.value.replace(/[^0-9\.]/g,'');
			});
			 $("#confirm_account_number").on("blur", function(){
					var accountNumber = $('#confirm_account_number').val();
					if(accountNumber.length<=0){
						$("#error_confirm_account_number").html("Please enter Account number");
					}
					else if(accountNumber.length>16){
						$("#error_confirm_account_number").html("Please enter valid Account number");
					}
					else{
						$('#error_confirm_account_number').html(" ");
					}
				});
				 
				$('#smm_bank_amount').keyup(function () { 
				    this.value = this.value.replace(/[^0-9\.]/g,'');
				});
				
		$("#smm_submit").click(function(){
			
			 var valid=true;
			 var bankName = $("#smm_bankName").val();
			 var ifscCode = $("#smm_ifsc_code").val();
			 var acoountName = $("#smm_account_holder_name").val();
			 var accountNumber = $("#smm_account_number").val();
			 var confAccountNum = $("#confirm_account_number").val();
			 var amount = $("#smm_bank_amount").val();
			 
			 $("#error_smm_ifsc_code").val(" ");
			 $("#error_smm_account_holder_name").val(" ");
			 $("#error_smm_account_number").val(" ");
			 $("#error_confirm_account_number").val(" ");
			 $("#error_smm_bank_amount").val(" ");
			 
			 if(bankName.length<=0){
				 valid=false;
				 $("#error_smm_bankName").val(" ");
			 }
			 if(ifscCode.length<=0){
				 valid=false;
				 $("#error_smm_ifsc_code").val(" ");
			 }
			 if(acoountName.length<=0){
				 valid=false;
				 $("#error_smm_account_holder_name").val(" ");
			 }
			 if(accountNumber.length<=0){
				 valid=false;
				 $("#error_smm_account_number").val(" ");
			 }
			 if(confAccountNum.length<=0){
				 valid=false;
				 $("#error_confirm_account_number").val(" ");
			 }
			 
			 if(amount.length<=0){
				 valid=false;
				 $("#error_smm_bank_amount").val(" ");
			 }
			 
			 if(valid=true){
				 
				 $.ajax({
						type : "POST",
						contentType : "application/json",
						url : "/Agent/SendMoney/BankTransfer",
						dataType : 'json',
						data : JSON.stringify({
							"bankName":""+ bankName +"",
							"ifscCode":""+ifscCode+"",
							"accountNumber":""+accountNumber+"",
							"amount":""+amount+"",
							"accountName":""+acoountName+""
						}),
						success : function(response) {
						console.log("CODE=="+response.code);
						console.log("details=="+response.details2);
						if(response.code.includes("S00")){
							$("#smm_submit").removeAttr("disabled","disabled");
							$("#smm_submit").html("Send");
								var parsedResponse = JSON.parse(response.response);
								if(parsedResponse.code == "F00"){
								    $("#error_mobile_sm").html(parsedResponse.details);
								    $("#smm_submit").html("Send");
								    var timeout = setTimeout(function(){
								    $("#error_mobile_sm").html(""); 	
								    $("#smm_mobile").val("");
									$("#smm_amount").val("");
									$("#smm_message").val("");
								    },4000);
								}
								if(parsedResponse.code == "S00"){
									$("#account_balance").html(parsedResponse.balance);
									$(".user_update_balance").html(parsedResponse.balance);
									$("#success_mobile_sm").html(parsedResponse.details);
									 var timeout = setTimeout(function(){
										    $("#success_mobile_sm").html("");
											$("#smm_mobile").val("");
											$("#smm_amount").val("");
											$("#smm_message").val("");
											 }, 4000);
								}
						   }
					   }
				  });
			 }
			 
		});
	
});