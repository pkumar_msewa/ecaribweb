$(document).ready(function(){
	var spinnerUrl = "Please wait <img src='/resources/images/spinner.gif' height='20' width='20'>"
		var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");
	var csrfHeader = $("meta[name='_csrf_header']").attr("content");
	var csrfToken = $("meta[name='_csrf']").attr("content");
	var hash_key="hash";
	var default_hash="123456";
	var headers = {};
	headers[hash_key] = default_hash;
	headers[csrfHeader] = csrfToken;
	
    $(".img_show").hide();
	$("#show_img").click(function () {
		$(".img_show").show(1000);
	});
	
	$("#loginButton").click(function() {
		var valid = true;
		$("#logMessage_success").html("");
		$("#error_username").html("");
		$("#error_login_password").html("");
		var userNam = $('#username').val();
		var pwd = $('#password_login').val();
		
		if(userNam.length != 10){
			valid = false;
			$("#error_username").html("Enter valid mobile number");
		}else if(!(userNam.match("^(?=(?:[7-9]){1})(?=[0-9]{10}).*"))){
			valid = false;
			$("#error_username").html("Please enter valid mobile number");
		}
		
		if(pwd.length == 0){
			$("#error_login_password").html("Enter password");
			valid = false;
		}
		if(valid == true) {
			$("#loginButton").attr("disabled","disabled");
			$("#loginButton").html(spinnerUrl);
			 $.ajax({
                 type: "POST",
                 headers: headers,
                 contentType: "application/x-www-form-urlencoded",
                 url: "/Api/v1/User/Android/en/Login/WebProcess",
                 data: {
                     username: userNam,
                     password: pwd
                 },
                 success: function (response) {
                	 if(response.code.includes("S00")){
  						window.location = "/User/Home";
                	 }else if(response.code.includes("L01")){
                		$("#userModal").modal("hide"); 
 						$("#loginOTPVerification").modal("show");
 						$("#logMessage_success").css("color", "green");
 						$("#logMessage_success").html("OTP sent to "+ $('#username').val());
 						$("#login_otp_username").val(userNam);
 						$("#login_otp_password").val(pwd);
 						$("#loginButton").removeAttr("disabled");
 	                	$("#loginButton").html("Login");
 						$("#login1").hide();
 					}else if (response.code.includes("F05")){
 						 $("#userModal").modal("hide"); 
                 	      $("#loginButton").removeAttr("disabled");
                 	      $("#loginButton").html("Login");
                     	 $("#errorMessage").modal("show");
                     	$("#error_message").html(response.message);
                     	var timeout = setTimeout(function(){
				            $("#errorMessage").modal('hide');
				     }, 3000);
 					}else if(response.code.includes("F04")){
 						 $("#userModal").modal("hide"); 
 						console.log("Success::"+response);
 						$("#loginButton").removeAttr("disabled");
                    	$("#loginButton").html("Login");
                    	$("#errorMessage").modal("show");
                      	$("#error_message").html(response.message);
                      	var timeout = setTimeout(function(){
 				            $("#errorMessage").modal('hide');
 				        }, 3000);
 					}
                     
                 }
             });
		}
	});
	
	$("#login_verify_mobile").click(function() {
		var valid = true;
		$("#logMessage_success").html("");
		$("#error_otp").html("");
		var userNam = $('#login_otp_username').val();
		var pwd = $('#login_otp_password').val();
		var otp = $('#verify_otp_key').val();
		if(otp.length == 0){
		$("#error_otp1").html("Enter valid OTP");
	     valid = false;
		}else if(otp.length != 6){
			valid = false;
			$("#error_otp").html("Please enter 6 digit OTP");
		}
		
		if(valid == true) {
			$("#login_verify_mobile").html(spinnerUrl);
			 $.ajax({
                 type: "POST",
                 headers: headers,
                 contentType: "application/x-www-form-urlencoded",
                 url: "/Api/v1/User/Android/en/Login/WebProcess",
                 data: {
                     username: userNam,
                     password: pwd,
                     validate:false,
                     mobileToken: otp
                 },
                 success: function (response) {
//                     console.log(response.response);
                     if(response.code.includes("S00")){
  						console.log("Success");
  						window.location = "User/Home";
  						
  					}else if(response.code.includes("F04")){
 						$("#loginOTPVerification").modal("show");
 						$("#logMessage_success").css("color", "red");
 						$("#logMessage_success").html(response.message);
 						$("#loginButton").removeAttr("disabled");
 	                	$("#loginButton").html("Login");
 						$("#login1").hide();
 						$("#login_verify_mobile").html("Continue");
 						var timeout = setTimeout(function(){
 				            $("#logMessage_success").html('');
 				        }, 3000);
 						
 					}
                 }
             });
		}
		var timeout = setTimeout(function(){
	            $("#error_otp1").html('');
	        }, 3000);
	});
	
	$("#login_resend_OTP").click(function() {
		var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");
		var csrfHeader = $("meta[name='_csrf_header']").attr("content");
		var csrfToken = $("meta[name='_csrf']").attr("content");
		var headers_token = {};
		headers_token[csrfHeader]=csrfToken;
		var valid = true;
		$("#logMessage_success").html("");
		$("#error_otp1").html("");
		$('#verify_otp_key').val("");
		var userNam = $('#login_otp_username').val();
		var pwd = $('#login_otp_password').val();
		var otp = $('#verify_otp_key').val();
		
		if(valid == true) {
			 $.ajax({
                 type: "POST",
                 headers: headers_token,
                 url: "/Api/v1/User/Windows/en/Login/ResendDeviceBindingOTP",
                 contentType: "application/json",
                 datatype: 'json',
                 data: JSON.stringify({
                     "mobileNumber": "" + userNam + "",
                 }),
                 success: function (response) {
                     console.log(response.response);
                     if(response.code.includes("S00")){
  						$("#logMessage_success").css("color", "green");
  						$("#logMessage_success").html("OTP sent to "+ $('#username').val());
  						$("#loginButton").removeAttr("disabled");
  	                	$("#loginButton").html("Login");
  						$("#login1").hide();
  					}
                 }
             });
		}
	});
	
    var count = 0;
    $(".resend-otp").click(function () {
        if (count >= 3) {
        	 $("#common_error_div").modal("show");
             $("#common_error_message").html("Please Login Again .");
        } else count++

    });
    
    $('#common_error_div').on('hidden.bs.modal', function () {
    	window.location = "/Home";
    	})
	
});