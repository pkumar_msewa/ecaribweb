
/**
 * 
 */

$(document).ready(function(){
	var spinnerUrl = "Please wait <img src='/resources/images/spinner.gif' height='20' width='20'>"
    $(".img_show").hide();
	$("#show_img").click(function () {
		$(".img_show").show(1000);
	});

	var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");
	var csrfHeader = $("meta[name='_csrf_header']").attr("content");
	var csrfToken = $("meta[name='_csrf']").attr("content");
	var hash_key="hash";
	var default_hash="123456";
	var headers = {};
	headers[hash_key] = default_hash;
	headers[csrfHeader] = csrfToken;
	processMessage('${msg}');
	var pattern = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2}|com|org|net|edu|gov|mil|biz|info|mobi|name|aero|asia|jobs|museum|co|in)"
	var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
	var email_pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
	var namePattern = /^[a-zA-Z-,]+(\s{0,1}[a-zA-Z-, ])*$/;		
		
    $("#pwd_eye").click(function(){
		var value = $("#password").attr("type");
		if(value == "password") {
			$(this).attr("class", "fa fa-eye");
			$("#password").attr("type", "text");
		}
		if(value == "text"){
			$(this).attr("class", "fa fa-eye-slash");
			$("#password").attr("type", "password");
		}

	});

	$("#cfm_pwd_eye").click(function(){
		var value = $("#confirmpassword").attr("type");
		if(value == "password") {
			$(this).attr("class", "fa fa-eye");
			$("#confirmpassword").attr("type", "text");
		}
		if(value == "text"){
			$(this).attr("class", "fa fa-eye-slash");
			$("#confirmpassword").attr("type", "password");
		}

	});

	$("#login_pwd_eye").click(function(){
		var value = $("#password_login").attr("type");
		if(value == "password") {
			$(this).attr("class", "fa fa-eye");
			$("#password_login").attr("type", "text");
		}
		if(value == "text"){
			$(this).attr("class", "fa fa-eye-slash");
			$("#password_login").attr("type", "password");
		}

	});
	
	 $('#firstName').bind("keypress", function (event) {
	    if (event.charCode!=0) {
	        var regex = new RegExp("^[a-zA-Z]+$");
	        var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
	        if (!regex.test(key)) {
	            event.preventDefault();
	            return false;
	        }
	    }
	  });
	 
	  $('#lastName').bind("keypress", function (event) {
	    if (event.charCode!=0) {
	        var regex = new RegExp("^[a-zA-Z]+$");
	        var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
	        if (!regex.test(key)) {
	            event.preventDefault();
	            return false;
	        }
	      }
	  });
	 
	 $("#lastName").on("blur", function(){
			var lastName = $('#lastName').val();
			if(lastName.length<=0){
				$('#error_last_name').html("Enter last name");
			}
			else if(!lastName.match(namePattern)){
				$('#error_last_name').html("Enter valid last name");
			}
			else{
				$('#error_last_name').html(" ");
			}
	 });
	 
	 $("#firstName").on("blur", function(){
			var first_name = $('#firstName').val();
			if(first_name.length<=0){
				$('#error_first_name').html("Enter first name");
			}
			else if(!first_name.match(namePattern)){
				$('#error_first_name').html("Enter valid first name");
			}
			else{
				$('#error_first_name').html(" ");
			}
	 });
	 
	$('#password').keyup(function() {
		$('#strength_password').html(checkStrength($('#password').val()))
	});
	
	$("#password").on("blur",function(){
		var password = $("#password").val();
		if(password.length<=0){
			$("#error_password").html("Enter password");
		}
		else if(password.length!=6){
			$("#error_password").html("Password must be 6 characters long");
		}
		else{
			$("#error_password").html("");
		}
	});
	
  $("#contact_no").on("blur",function(){
	  var contactNo = $("#contact_no").val();
	if(contactNo.length<=0) {
		$("#error_contact_no").html("Enter mobile number");
	}
	else if(contactNo.length != 10) {
		$("#error_contact_no").html("Enter valid mobile number");
	}
	else{
		$("#error_contact_no").html("");
	 }
   });
  
	 $('#confirmpassword').on('keyup', function () {
		    if ($(this).val() == $('#password').val()) {
		        $('#error_cnfpassword').html('matching').css('color', 'green');
		    } else $('#error_cnfpassword').html('doesnot match with password').css('color', 'red');
		    $("#confirmpassword").html(""); 
		});

	 $("#email").on("blur", function(){
			var emailAddress = $('#email').val();
			if(emailAddress.length<=0){
				$('#error_email').html("Enter email-Id");
			}
		   else if(!emailAddress.match(pattern)){
				$('#error_email').html("Enter valid email-Id");
			}
			else if(!emailAddress.match(email_pattern)){
				$('#error_email').html("Enter valid email-Id");
			 }
			else if(!emailAddress.match(filter)){
				$('#error_email').html("Enter valid email-Id")
			} 
			else{
				$('#error_email').html(" ");
			}
		}); 
	 
	 $("#pincode").on("blur", function(){
		 var locationCode= $('#pincode').val();
		 if(locationCode.length<=0){
			 $("#error_pincode").html("Enter locationCode");
		 }else if(locationCode.length != 6){
			 $("#error_pincode").html("LocationCode must be 6 characters long only");
		 }
		 else{
			 $("#error_pincode").html("");
		 }
	 });
	 
	 $("#g-recaptcha-response").on("blur", function(){
		 var captchaResponse = $("#g-recaptcha-response").val();
		 if(captchaResponse.length<=0){
			 $("#error_captcha").html("Enter captcha");
		 }else {
			 $("#error_captcha").html(" ");
		 }
	 });
	 
	$("#secquestions").on("blur", function(){
		var secQuestion = $('#secquestions').val() ;
		console.log("secQuestion="+secQuestion);
		if(secQuestion.includes("#")){
			$("#error_sec_ques").html("Select security question");
		}
		else{
		    $("#error_sec_ques").html("");
		 }
	});
	
	$("#sec_ans").on("blur", function(){
		var secAnswer = $('#sec_ans').val() ;
		console.log("secAnswer="+secAnswer);
	    if(secAnswer.length<=0){
			$("#error_sec_ans").html("Select security answer");
	    }
	    else{
	    	$("#error_sec_ans").html("");
	    }
	});
	
	$("#dob").on("blur", function(){
		var dob = $('#dob').val() ;
	    if(dob.length<=0){
			$("#error_dob").html("Select Date Of Birth");
	    }
	    else{
	    	$("#error_dob").html("");
	    }
	});
	
	$("#registerButton").click(function() {
    	if($("#termsConditions").is(":checked")){
    	
    $("#error_first_name").html("");
	$("#error_last_name").html("");
	$("#error_password").html("");
	$("#error_cnfpassword").html("");
	$("#error_email").html("");
	$("#error_locationCode").html("");
	$("#error_contact_no").html("");
	$("#error_gender").html("");
	$("#error_dob").html("");
	$("#error_captcha").html("");
	$("#error_pincode").html("");

	var valid = true;
//	var pattern = "[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$"; //pattern for email
	
	var passwordPattern = "[a-zA-z0-9]"; //pattern for password
	var firstName = $('#firstName').val();
	var lastName = $("#lastName").val();
	var password  = $('#password').val();
	var email = $('#email').val() ;
	var captchaResponse = $("#g-recaptcha-response").val();
	var dob = $('#dob').val();
	var locationCode= $('#pincode').val();
	var contactNo = $('#contact_no').val() ;
	var secQuestion = $('#secquestions').val() ;
	var secAnswer = $('#sec_ans').val() ;

	
	if(captchaResponse.length<=0){
		$("#error_captcha").html("Enter captcha");
	}
	
	if(secQuestion.includes("#")){
		$("#error_sec_ques").html("Select security question");
	}
	
	if(secAnswer.length<=0){
		$("#error_sec_ans").html("Select security answer");
		valid = false;
	}
	
	if(contactNo.length<=0) {
		$("#error_contact_no").html("Enter mobile number");
		valid = false;
	}
	else if(contactNo.length != 10) {
		$("#error_contact_no").html("Enter valid mobile number");
		valid = false;
	}

	if(password.length <= 0){
				$("#error_password").html("Enter password");
				valid = false;
	}
	if(firstName.length <= 0){
				$("#error_first_name").html("Enter first name");
				valid = false;
	}
	if(lastName.length <= 0){
		$("#error_last_name").html("Enter last name");
		valid = false;
    }
	if(email.length<=0){
		$("#error_email").html("Enter email");
		valid = false;
	}
	else if(email.length > 0) {
		if(!email.match(pattern)) {
			$("#error_email").html("Enter valid email");
			valid = false;
		}
	}
	
	
	if(dob.length<=0){
		$("#error_dob").html("Enter Date Of Birth");
		valid = false;
	}
	else if(new Date(dob) >= new Date("1999-12-31")) {
				$("#error_dob").html("you must be at least 18 years old to sign up");
				valid = false;
	}
	
	if(password.length != 6){
		$("#error_password").html("Password must be 6 characters long");
		valid = false;
	}
	if(locationCode.length != 6){
		$("#error_pincode").html("LocationCode must be 6 characters long only");
	}

if(valid == true) {
	$("#registerButton").addClass("disabled");
	$("#registerButton").html(spinnerUrl);
	
	$.ajax({
		type : "POST",
		headers: headers,
		contentType : "application/json",
		url : "/Agent/SaveUserToAgent",
		dataType : 'json',
		data : JSON.stringify({
			"firstName" : "" + firstName+ "",
			"lastName" : " " + lastName+"",
			"password" : "" + password + "",
			"contactNo" : "" + contactNo + "",
			"email" : "" + email + "",
			"locationCode" : "" + locationCode + "",
			"gender" : "NA",
			"dateOfBirth" : "" + dob + "",
			"secQuestionCode" : "" + secQuestion + "",
			"secAnswer" : "" + secAnswer + "",
			"captchaResponse" : ""+captchaResponse+"",
		}),
		success : function(response) {
			$( ".captcha_link" ).trigger( "click" );
			$("#registerButton").removeClass("disabled");
			$("#registerButton").html("Sign Up");
			if (response.code.includes("S00")) {
				$("#regMessage").modal("show");
				$("#reg_otp_username").val($('#contact_no').val());
				$("#regMessage_success").html("Registration Successful OTP sent to "+ $('#contact_no').val()+" and verification mail sent to "+ $('#email').val());
			}
			if(response.code.includes("F02")){
				
						$("#error_captcha").html(response.message);
			}
			else if(response.code.includes("F04"))
			{
				
					if(response.message.includes("User Already Exist")) {
						$("#error_contact_no").html(response.message);
					}
					if(response.message.includes("Email already exists")){
						$("#error_email").html(response.message);
					}
					if(response.message.includes("Not a valid Pincode")){
						$("#error_pincode").html(response.message);
					}
					$("#form_error").html(response.message);
			}
			else if(response.code.includes("F09"))
				{
				$("#error_registerAgent").html(response.message);
				}
			
	

		}
	});


}

}
    });


$("#register_resend_otp").click(function() {

	$("#register_resend_otp").html(spinnerUrl);
	$("#register_resend_otp").addClass("disabled");
	var recaptcha_response = $("#g-recaptcha-response-1").val();
	$.ajax({
		type : "POST",
		headers :headers,
		contentType : "application/json",
		url : "/Agent/AgentResendMobileOTP",
		dataType : 'json',
		data : JSON.stringify({
			"mobileNumber" : "" + $('#reg_otp_username').val() + "",
			"captchaResponse":""+recaptcha_response+""
		}),
		success : function(response) {
			$( ".captcha_link" ).trigger( "click" );
			$("#register_resend_otp").html("Resend OTP");
			$("#register_resend_otp").removeClass("disabled");
			console.log("response==="+response.message);
			console.log("response code==="+response.code);
			if (response.code.includes("S00")) {
				$("#regMessage_success").html(response.message);
			}
			else if(response.code.includes("F00")){
				$("#regMessage_success").html(" ");
				$("#error_captcha1").html(response.message);
			}
			else if(response.code.includes("F03")){
				window.location = "/Home";
			}
			else{
				$("#regMessage_success").html(response.message);
			}
		}
	});	
});

$("#forgot_password_request").click(function(){
	var oldVal = $("#fp_submit").val();
	$("#forgot_password_request").addClass("disabled");
	$("#forgot_password_request").val(spinnerUrl);
	var username = $('#fp_username').val();
	var recaptcha_response = $("#g-recaptcha-response-2").val();
	var valid = true;
	if(username.length != 10){
		valid = false;
		$("#fp_error_username").html("Enter Valid Mobile Number");
	}
	if(valid == false){
		$("#forgot_password_request").removeClass("disabled");
		$("#forgot_password_request").val("Continue");
	}
    if(valid == true){
	$.ajax({
		type : "POST",
		headers : headers,
		contentType : "application/json",
		url : "/Agent/ForgotPassword/AgentRequest",
		data : JSON.stringify({
			"username" : "" + $('#fp_username').val() + "",
			"captchaResponse" : ""+recaptcha_response+""
		}),
		dataType : 'json',
		success : function(response) {
			$( ".captcha_link" ).trigger( "click" );
			$("#forgot_password_request").val(oldVal)
			$("#forgot_password_request").removeClass("disabled");
			if (response.code.includes("S00")) {
				$("#forgotPassword").modal("hide");
				$("#fpOTP").modal("show");
				$("#fpusername_forgot").val($('#fp_username').val());
				$("#fpOTP_message").html(response.details);
			} else {
				$("#fp_message").append(response.details);
			}
		}
	});
	}
	
});

$("#forgot_password_resend_otp").click(function() {
	$( ".captcha_link" ).trigger( "click" );
	var recaptcha_response = $("#g-recaptcha-response-3").val();
	$.ajax({
		type : "POST",
		headers : headers,
		contentType : "application/json",
		url : "/Agent/ForgotPassword/AgentRequest",
		dataType : 'json',
		data : JSON.stringify({
			"username":""+$('#fpusername_forgot').val()+"",
			"captchaResponse" : ""+recaptcha_response+""

		}),
		success : function(response) {
			$( ".captcha_link" ).trigger( "click" );
			if (response.code.includes("S00")) {
				$("#fpOTP_message").html(response.details);
			} else{

			}
		}
	});


	
});

$("#register_verify_mobile").click(function() {
	$( ".captcha_link" ).trigger( "click" );
	$.ajax({
		type : "POST",
		headers : headers,
		contentType : "application/json",
		url : "/Agent/SaveUsertoAgentMobileOTP",
		dataType : 'json',
		data : JSON.stringify({
			"mobileNumber" : "" + $('#reg_otp_username').val() + "",
			"key" : "" + $('#verify_reg_otp_key').val() + ""
		}),
		success : function(response) {
			$( ".captcha_link" ).trigger( "click" );
			if (response.code.includes("S00")) {
				$("#firstName").val("");
				$("#lastName").val("");
				$("#password").val("");
				$("#confirm_password").val("");
				$("#email").val("");
				$("#contact_no").val("");
				$("#gender").val("");
				$('#sec_ans').val("");
				$('#pincode').val("");
				$("#dob").val("");
				$('#strength_password').html("");
				 $("#g-recaptcha-response").val("");
				$("#regMessage").modal('hide');
				$("#verifiedMessage").modal('show');
				$("#success_verification_message").html(response.details+" Please login to continue");
			} else {
				$("#regMessage_success").html(response.message);
			}
		}
	});

});

$("#process_forgot_password_request").click(function() {
	$( ".captcha_link" ).trigger( "click" );
	var key = $("#fpOTP_key").val();
	var newPassword = $("#fpnewPassword_key").val();
	var confirmPassword =  $('#fpconfirmPassword_key').val() ;
	if(newPassword === confirmPassword) {
	$.ajax({
		type : "POST",
		headers : headers,
		contentType : "application/json",
		url : "/Agent/ForgotPassword/AgentChangePassword",
		dataType : 'json',
		data : JSON.stringify({
			"username" : "" + $('#fpusername_forgot').val() + "",
			"newPassword" : "" + newPassword + "",
			"confirmPassword" : "" + confirmPassword + "",
			"key" : "" + key + ""
		}),
		success : function(response) {
			$( ".captcha_link" ).trigger( "click" );
			if (response.code.includes("S00")) {
				$("#fpOTP").modal("hide");
				$("#successNotification").modal("show");
				$("#success_alert").html(response.details);
			} else {
				$("#fpusername_forgot").val(response.details.username);
				$("#fpOTP_message").html(response.details.key);
			}
		}
	});
	}else {
		$('#fpnewPassword_key').val("Enter same password in both fields");
	}

});

$("#termsConditions").click(function() {
	var isChecked = $("#termsConditions").is(":checked");
	if (isChecked) {
		$("#registerButton").removeClass("disabled");
	} else {
		$("#registerButton").addClass("disabled");
	}

	
});

	$(".captcha_link").click(function(){
		var $elem = $('.glyphicon-refresh');
		var angle=360;
		$({deg: 0}).animate({deg: angle}, {
			duration: 1000,
			step: function(now) {
				$elem.css({
					transform: 'rotate(' + now + 'deg)'
				});
			}
		});
		$(".captcha_image").attr("src","/Captcha");
	});

	$(".numeric").keydown(function(event){
		if(event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || (event.keyCode == 65 && event.ctrlKey == true) || (event.keyCode >= 35 && event.keyCode<=39)){
			return;
		}else{
			if(event.shiftKey || (event.keyCode < 48 || event.keyCode >57) && (event.keyCode < 96 || event.keyCode > 105)) {
				event.preventDefault();
			}
		}
	});

	$("#forgot_password_modal").click(function(){
		$(".captcha_link").trigger("click");
	})


	function checkStrength(password) {
		var strength = 0
		if (password.length < 6) {
			$('#strength_password').removeClass()
			$('#strength_password').addClass('short')
			return 'Too short'
		}
		if (password.length > 7) strength += 1
		// If password contains both lower and uppercase characters, increase strength value.
		if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/)) strength += 1
		// If it has numbers and characters, increase strength value.
		if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/)) strength += 1
		// If it has one special character, increase strength value.
		if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/)) strength += 1
		// If it has two special characters, increase strength value.
		if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/)) strength += 1
		// Calculated strength value, we can return messages
		// If value is less than 2
		if (strength < 2) {
			$('#strength_password').removeClass()
			$('#strength_password').addClass('weak')
			return 'Weak'
		} else if (strength == 2) {
			$('#strength_password').removeClass()
			$('#strength_password').addClass('good')
			return 'Good'
		} else {
			$('#strength_password').removeClass()
			$('#strength_password').addClass('strong')
			return 'Strong'
		}
	}
	
});

/**
 * 
 */