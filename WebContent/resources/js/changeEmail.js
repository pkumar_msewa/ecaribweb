/**
f * 
 */

$(document).ready(function(){
	var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");
	var csrfHeader = $("meta[name='_csrf_header']").attr("content");
	var csrfToken = $("meta[name='_csrf']").attr("content");
	var hash_key="hash";
	var default_hash="123456";
	var headers = {};
	headers[hash_key] = default_hash;
	headers[csrfHeader] = csrfToken;
	
	$("#ch_email").click(function(){
		$("#ch_email").addClass("disabled");
		$("#ch_email").html("Please Wait..");
		$("#error_ch_email").html("");
		$("#error_ife_receiver").html("");
        $("#error_ife").html("");
        $("#success_ife").html("");
		var valid = true;
		var mail = $("#ch_email").val();
		var pattern = "[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$"
		var receiver = $("#ife_receiver").val();
		if(!(mail.match(pattern))){
			$("#error_ife_mail").html("enter valid email");
			valid = false;
		}
		
		if(valid == false){
			$("#ch_email").removeClass("disabled");
			$("#ch_email").html("Invite");
		}
		if(valid == true){
			$.ajax({
				type : "POST",
				headers: headers,
				contentType : "application/x-www-form-urlencoded",
				url : "/User/Invite/Email",
				data : {
					email:mail,
					receiversName:receiver,
					captchaResponse: recaptcha_response
				},
				success : function(response) {
					$(".captcha_link").trigger("click");
					$("#ch_email").removeClass("disabled");
					$("#ch_email").html("Invite");
						if(response.code == "F00"){
						    $("#error_ife").html(response.message);
						}
						if(response.code == "S00"){
						    $("#success_ife").html(response.message);
							$("#ch_email").val("");
							$("#ife_receiver").val("");
                            $("#g-recaptcha-response").val("")

						}
				}
			});
			
		}
		
	});
	
	});