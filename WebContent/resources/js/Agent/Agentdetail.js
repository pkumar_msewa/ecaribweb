/**
 * 
 */
var eStatus = "";

$(document).ready(function(){
	var spinnerUrl = "Please Wait <img src='/resources/images/spinner.gif' height='20' width='20'>"
	var spinner = "<img src='/resources/images/spinner.gif' height='20' width='20'>"
	var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");
	var csrfHeader = $("meta[name='_csrf_header']").attr("content");
	var csrfToken = $("meta[name='_csrf']").attr("content");
	var headers = {};
	headers[csrfHeader]=csrfToken;

	$(".numeric").keydown(function(event){
        if(event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || (event.keyCode == 65 && event.ctrlKey == true) || (event.keyCode >= 35 && event.keyCode<=39)){
            return;
        }else{
            if(event.shiftKey || (event.keyCode < 48 || event.keyCode >57) && (event.keyCode < 96 || event.keyCode > 105)) {
                event.preventDefault();
            }
        }
    });

	$.ajax({
		type : "GET",
		url : "/Agent/GetAgentDetails",
		headers: headers,
		contentType : "application/json",
		datatype : 'json',
		success : function(response) {
			var u = response;
			console.log("response ::" +u.firstName);
			console.log("amount ::" +u.debitAmount);
			console.log("register ::" +u.totalRegisterUser);
			eStatus = u.emailStatus;
			var usertype=u.flag;
			if(response.flag){
				$("#hidethis1").detach();
				$("#hidethis2").detach();
			}
			var firstNameLength = $("#first_name_user").length;
			var lastNameLength = $("#lastName").length;
			var accountBalanceLength = $("#account_balance").length;
			var userMobileLength = $("#user_mobile").length;
			var userAdressLength = $("#user_address").length;
			var  accountNumberLength = $("#account_number").length;
			var accountTypeLength = $("#account_type").length;
			var accountDesLength = $("#account_type_description").length;
			var dailyLimitLength = $("#daily_limit").length;
			var monthlyLimitLength = $("#monthly_limit").length;
			var emailLength = $("#user_email").length;
			$("#user_points").html(u.points);
			$("#small_display_pic").attr('src',function(){
				if(u.image == "" || u.image == null || u.image == " "){
					return "https://www.vpayqwik.com/resources/images/sample.jpg"
				}else{
					return "https://www.vpayqwik.com"+u.image;
				}
			});
			$("#display_pic").attr('src',function(){
				if(u.image == "" || u.image == null || u.image == " " ){
					return "https://www.vpayqwik.com/resources/images/sample.jpg"
				}else{
					return "https://www.vpayqwik.com"+u.image;
				}
			});
            $("#first_name_user").val(u.firstName);
            $(".user_Register_Agent").html(u.totalRegisterUser);
			$(".debit_Amount_Agent").html(u.debitAmount);
            $(".agent_first_name").val(u.firstName);
			$("#loadmoney_username").val(u.firstName+" "+u.lastName);
			$("#loadmoney_phone").val(u.contactNo);
			$("#loadmoney_email").val(u.email);
            $("#last_name_ep").val(u.lastName);
            $("#email").val(u.email);
            $("#address").val(u.address);
			$("#display_first_last_name").html(u.firstName+" "+u.lastName);
			$("#bt_account_number").val(u.accountNumber);
			$("#bt_email").val(u.email);
			$("#bt_mobile_number").val(u.contactNo);
            
			$("#userregister").val(u.totalregisterUser);
			$("#debitamount").val(u.debitamount);
			
			if(firstNameLength){
				$("#first_name_user").html(u.firstName);
				}
			if(accountBalanceLength){
				$("#account_balance").html(u.balance);
				$("#user_balance").html(u.balance);
			}
			if(emailLength){
				$("#user_email").html(u.email);
			}
			if(userMobileLength){
			$("#user_mobile").html(u.contactNo+ " <span class='glyphicon glyphicon-ok-sign' style='color:#17bcc8;'></span>");
			}
			if(userAdressLength){
			$("#user_address").html(u.address);

			}
			if(accountNumberLength){
			$("#account_number").html(u.accountNumber);
			}
			if(accountTypeLength){
			$("#account_type").html(u.userType);
			}
			if(accountDesLength){
			$("#account_type_description").html();
			}
			if(dailyLimitLength){
			$("#daily_limit").html(u.dailyTransaction);
			}
			if(monthlyLimitLength){
			$("#monthly_limit").html(u.monthlyTransaction);
			}
			
			if(u.emailStatus == "Inactive"){
				$("#alert_verification_message").html("Your email id is not verified. You've received verification email on "+u.email+". If you want to change your mail go to settings and update");
				$("#verification_alert").modal('show');
				$("#dth_submit").attr("disabled", "disabled");
				$("#landline_submit").attr("disabled", "disabled");
				$("#gas_submit").attr("disabled", "disabled");
				$("#ecity_submit").attr("disabled", "disabled");
				$("#ins_submit").attr("disabled", "disabled");
				//for topup
				$("#pre_submit").attr("disabled", "disabled");
				$("#post_submit").attr("disabled", "disabled");
				$("#dc_submit").attr("disabled", "disabled");
				
				$("#smm_submit").attr("disabled", "disabled");
				$("#lm_submit").attr("disabled", "disabled");
				$("#agent_email_ep").show();
				$(".email_logo").show();
				
			}
			if(u.emailStatus == "Active"){
				$("#user_email").append(" <span class='glyphicon glyphicon-ok-sign' style='color:#17bcc8;'></span>");
				$("#agent_email_ep").hide();
				$(".email_logo").hide();
				$("#dth_submit").removeClass("disabled");
				$("#landline_submit").removeClass("disabled");
				$("#gas_submit").removeClass("disabled");
				$("#ecity_submit").removeClass("disabled");
				$("#ins_submit").removeClass("disabled");
				//for topup
				$("#pre_submit").removeClass("disabled");
				$("#post_submit").removeClass("disabled");
				$("#dc_submit").removeClass("disabled");
				
				$("#smm_submit").removeClass("disabled");
				$("#lm_submit").removeClass("disabled");
				
			}
		},
	});

	$("#pwd_eye").click(function(){
		var value = $("#current").attr("type");
		if(value == "password") {
			$(this).attr("class", "fa fa-eye");
			$("#current").attr("type", "text");
		}
		if(value == "text"){
			$(this).attr("class", "fa fa-eye-slash");
			$("#current").attr("type", "password");
		}

	});

	$("#new_pwd_eye").click(function(){
		var value = $("#new").attr("type");
		if(value == "password") {
			$(this).attr("class", "fa fa-eye");
			$("#new").attr("type", "text");
		}
		if(value == "text"){
			$(this).attr("class", "fa fa-eye-slash");
			$("#new").attr("type", "password");
		}

	});

	$("#cnf_pwd_eye").click(function(){
		var value = $("#confirm_new").attr("type");
		if(value == "password") {
			$(this).attr("class", "fa fa-eye");
			$("#confirm_new").attr("type", "text");
		}
		if(value == "text"){
			$(this).attr("class", "fa fa-eye-slash");
			$("#confirm_new").attr("type", "password");
		}

	});
	
	$("#editprofile_validate").click(function() {
		var valid = true;
		var firstName = $('.agent_first_name').val();
		var address = $('#address').val();
		 var regex = "^[a-zA-Z ]*$";
		
		   if(firstName.length < 3) {
			   $("#error_first_name").html("Please enter name");
          	 valid = false;
			}else if(!firstName.match(regex)){
				$("#error_first_name").html("Please enter valid name");
				valid = false;
			}
		   if(address.length <=0) {
			   $("#error_address").html("Please enter address");
          	 valid = false;
			}else if(!address.match(regex)){
				$("#error_address").html("Please enter Valid address");
				valid = false;
			}
	if(valid == true) {
	$.ajax({
		type : "POST",
		headers: headers,
		url : "/Agent/AgentEditProfile/Process",
		contentType : "application/x-www-form-urlencoded",
        data:{
			"firstName" : "" + firstName + "",
			"address" : "" + address + ""
        },
		success : function(response) {
			if (response.code.includes("S00")) {
				$("#successNotification").modal("show");
				$("#success_alert").html(response.message);
			} else if(response.code.includes("F00")) {
				$("#errorNotification").modal("show");
				$("#error_alert").html(response.message);
			}else if(response.code.includes("F03")){
				window.location = "/Home";
			}
		  }
	   });
      }
	var timeout = setTimeout(function(){
		  $("#error_first_name").text('');
  		$("#error_address").text('');
	 }, 4000);

    });
	
	
	$("#agent_update_password").click(function() {
		$("#agent_error_current_password").html("");
		$("#agent_error_new").html("");
		$("#agent_error_confirm").html("");
		
		
		var currentAgentPassword = $('#agent_current_password').val();
		var newAgentPassword = $('#agent_new_password').val();
		var confirmAgentPassword =  $('#agent_confirm_new_password').val();
		var valid = true;
		
		if(currentAgentPassword.length <=0){
			$("#agent_error_current_password").html("Enter Password");
			  valid = false;
		}
		if(newAgentPassword.length <=0){
			$("#agent_error_new").html("Enter New Password.");
			  valid = false;
		}
		if(confirmAgentPassword.length <=0){
			$("#agent_error_confirm").html("Enter Confirm Password.");
			  valid = false;
		}
		if(valid == true) {
			if(currentAgentPassword != newAgentPassword){
		if(newAgentPassword == confirmAgentPassword) {
			 $("#agent_update_password").html(spinnerUrl);
		$.ajax({
			type : "POST",
			headers:headers,
			url : "/Agent/AgentUpdatePassword/Process",
			contentType : "application/x-www-form-urlencoded",
			data : {
				"newPassword" : "" + newAgentPassword + "",
				"confirmPassword" : "" + confirmAgentPassword + "",
				"password" : "" + currentAgentPassword + ""
			},
			success : function(response) {
				if (response.code.includes("S00")) {
					$("#successNotification").modal("show");
					$("#success_alert").html(response.message);
					$("#agent_update_password").html('Update');
					 var timeout = setTimeout(function(){
				            $("#agent_current_password").val('');
				            $("#agent_new_password").val('');
				            $("#agent_confirm_new_password").val('');
				          }, 4000);
				} else if(response.code.includes("F00")) {
					$("#errorNotification").modal("show");
					$("#error_alert").html(response.message);
					$("#agent_update_password").html('Update');
					 var timeout = setTimeout(function(){
				            $("#fpOTP_message").text('');
				            $("#agent_current_password").val('');
				            $("#agent_new_password").val('');
				            $("#agent_confirm_new_password").val('');
				          }, 4000);
				}else if(response.code.includes("F03")){
					window.location = "/Home";
				}
			}
		});
		}else {
			$("#errorNotification").modal("show");
			$("#error_alert").html("new password and confirm Password doesnot match .");
			 $("#agent_current_password").val('');
	            $("#agent_new_password").val('');
	            $("#agent_confirm_new_password").val('');
		  }
	    }else{
	    	$("#errorNotification").modal("show");
			$("#error_alert").html("You have entered the current password again. Please enter new password. ");
			 $("#agent_current_password").val('');
	            $("#agent_new_password").val('');
	            $("#agent_confirm_new_password").val('');
	    }
	  }
		var timeout = setTimeout(function(){
			 $("#agent_error_current_password").text('');
	            $("#agent_error_new").text('');
	            $("#agent_error_confirm").text('');
		 }, 4000);

	});

});